package com.wade.yd;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YoutubeConnector {
    private YouTube youtube;
    private YouTube.Search.List query;
    private Handler handler;

    public YoutubeConnector(Context context) {
        youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
            }
        }).setApplicationName(context.getString(R.string.hisonet)).build();

        try {
            query = youtube.search().list("id,snippet");
            query.setKey(DeveloperKey.DEVELOPER_KEY);
            query.setType("video");
            query.setFields("items(id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url)");
        } catch (IOException e) {
            log( "Could not initialize: " + e.getMessage());
        }

        handler = new Handler(Looper.getMainLooper());
    }

    public List<VideoItem> searchByChannelId(String id) {
        List<VideoItem> items = new ArrayList<VideoItem>();

        query.setChannelId(id);
        try {
            SearchListResponse response = query.execute();
            List<SearchResult> results = response.getItems();

            for (SearchResult result : results) {
                VideoItem item = new VideoItem();
                item.setTitle(result.getSnippet().getTitle());
                item.setThumbnailURL(result.getSnippet().getThumbnails().getDefault().getUrl());
                item.setId(result.getId().getVideoId());
                items.add(item);
            }
            log( "Found "+items.size()+" videos from channel "+id);
        } catch (IOException e) {
            log( "Exception @ searchByChannelId("+id+") :"+e.getMessage());
        }

        return items;
    }
    public List<VideoItem> search(String keywords) {
//        log( "yc.search('"+keywords+"')");
        List<VideoItem> items = new ArrayList<VideoItem>();

        query.setQ(keywords);
        try {
            SearchListResponse response = query.execute();
            List<SearchResult> results = response.getItems();

            for (SearchResult result : results) {
                VideoItem item = new VideoItem();
                item.setTitle(result.getSnippet().getTitle());
                item.setThumbnailURL(result.getSnippet().getThumbnails().getDefault().getUrl());
                item.setId(result.getId().getVideoId());
                items.add(item);
            }
        } catch (IOException e) {
            log( "Exception @ search("+keywords+") :"+e.getMessage());
        }

        return items;
    }

    public List<VideoItem> searchByVideoIds(List<String> ids) {
//        log( "yc.searchByVideoIds('"+ ids.toArray().toString()+"')");
        List<VideoItem> items = new ArrayList<VideoItem>();

        for (String keyword : ids) {
//            log( "searchByVideoIds() for '"+keyword+"'");
            query.setQ(keyword);
            try {
                SearchListResponse response = query.execute();
                List<SearchResult> results = response.getItems();

                for (SearchResult result : results) {
                    if (result.getId().getVideoId().equals(keyword)) {
                        VideoItem item = new VideoItem();
                        item.setTitle(result.getSnippet().getTitle());
                        item.setThumbnailURL(result.getSnippet().getThumbnails().getDefault().getUrl());
                        item.setId(result.getId().getVideoId());
                        items.add(item);
                        break;
                    }
                }
            } catch (IOException e) {
                log( "Exception @ search(" + keyword + ") :" + e.getMessage());
            }
        }

        return items;
    }
    
    private void log(String text) {
        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        msg.setData(bundle);
        msg.what = 1000;
        handler.sendMessage(msg);
    }
}
