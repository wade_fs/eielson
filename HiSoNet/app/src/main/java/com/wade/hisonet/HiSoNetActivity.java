/*
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wade.hisonet;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.squareup.picasso.Picasso;
import com.wade.droidspeech.DroidSpeech;
import com.wade.droidspeech.OnDSListener;
import com.wade.droidspeech.OnDSPermissionsListener;
import com.wade.yd.DeveloperKey;
import com.wade.yd.VideoItem;
import com.wade.yd.YouTubeFailureRecoveryActivity;
import com.wade.yd.YoutubeConnector;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple YouTube Android API demo application which shows how to create a simple application that
 * shows a YouTube Video in a {@link YouTubePlayerFragment}.
 * <p>
 * Note, this sample app extends from {@link YouTubeFailureRecoveryActivity} to handle errors, which
 * itself extends {@link YouTubeBaseActivity}. However, you are not required to extend
 * {@link YouTubeBaseActivity} if using {@link YouTubePlayerFragment}s.
 */
public class HiSoNetActivity extends YouTubeFailureRecoveryActivity
        implements OnDSListener, OnDSPermissionsListener {

    private static final String TAG="MyLog";
    private HiSoNetActivity me;

    private TextView tvDesc;
    private ListView videosFound;
    private Text2Speech text2Speech;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 全螢幕
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Activity GUI
        setContentView(R.layout.fragments_hisonet);
        me = this;

        // Youtube Player API
        YouTubePlayerFragment playerView =
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.player);
        // 底下的在初始化之後，會轉到 onInitializationSuccess | onInitializationFailure
        playerView.initialize(DeveloperKey.DEVELOPER_KEY, this);

        // 通知 Youtube load video result
        ytLoadHandler = new Handler(); // 由 updateVideosFound() 處理結果
        
        // 通知 Youtube Player 播放狀態(Play|Pause)
        playbackEventListener = new MyPlaybackEventListener();
        playHandler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                if (msg.what == 100 && isFinished) {
                    log( "handleMessage: '"+msg.toString()+"'");
//                    me.finish();
                } else if (msg.what == 1000) { // log message from YoutubeConnector
                    Bundle bundle = msg.getData();
                    String text = bundle.getString("text");
                    log(text);
                }
            }
        };

        // 用來擺放資訊，目前用做 log
        tvDesc = findViewById(R.id.tvDesc);
        tvDesc.setMovementMethod(new ScrollingMovementMethod());

        // 畫面左邊的清單
        videosFound = findViewById(R.id.videos);
        videosFound.setOnItemClickListener((parent, view, position, id) -> {
            isRepeat = isRepeatAll = false;
            videosFound.setSelection(position);
            view.setSelected(true);
            log("Play @ "+position+": "+searchResults.get(position).getTitle());
            loadVideo(searchResults.get(position).getId());
        });

        // 語音辨識
        command = new Command();
        text2Speech = new Text2Speech(this);
//        myFavorites = new MyFavorites(getApplicationContext());
//        droidSpeech = new DroidSpeech(this, getFragmentManager());
//        droidSpeech.setOnDroidSpeechListener(this);
//        droidSpeech.setShowRecognitionProgressView(false);
////        droidSpeech.setContinuousSpeechRecognition(true);
////        droidSpeech.setOfflineSpeechRecognition(true);
//        droidSpeech.setTAG(TAG);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (videosFound.getAdapter() != null && videosFound.getAdapter().getCount() > 0)
//            videosFound.setSelection(curPos);
//        log( "onResume() "+keyword+","+hadSearched+","+isMyFavorites);
//        if (!hadSearched) {
//            loadMyFavorites();
//            hadSearched = true;
//            isMyFavorites = true;
//            if (player != null) player.loadPlaylist(WelcomePlayList);
//        }
//        start();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        stop();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        // TODO Auto-generated method stub
//        super.onSaveInstanceState(outState);
//
//        //儲存UI狀態到bundle中
//        outState.putString("keyword", keyword);
//        outState.putBoolean("hadSearched", hadSearched);
//        outState.putStringArrayList("curIds", curIds);
//        outState.putBoolean("isMyFavorites", isMyFavorites);
//        log( "onSaveInstanceState() "+keyword+","+hadSearched+","+isMyFavorites);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle outState) {
//        super.onRestoreInstanceState(outState);
//        if (outState != null) {
//            keyword = outState.getString("keyword");
//            hadSearched = outState.getBoolean("hadSearched");
//            curIds = outState.getStringArrayList("curIds");
//            isMyFavorites = outState.getBoolean("isMyFavorites");
//            log( "onRestoreInstanceState() "+keyword+","+hadSearched+","+isMyFavorites);
//        }
//    }

    /**************************************************************************************************
     * 這下這一區是 for Youtube Player API **************************************************************/

    private YouTubePlayer player;
    private String WelcomePlayList = "PLAvnVYGnc8FTvyDIlY5i5Icb7oQY8-Skr";
    private String LaunchChannel = "UCjHKVtnCaC4IKoJ3PAPaowQ";
    private MyPlaybackEventListener playbackEventListener;

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer,
                                        boolean wasRestored) {

        player = youTubePlayer;
        player.setPlaybackEventListener(playbackEventListener);
        text2Speech.say("準備好語音辨識了，請說話!");

        if (!wasRestored) {
            player.loadPlaylist(WelcomePlayList);
            loadLaunch();
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
        log(errorMessage);
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.player);
    }

    /**************************************************************************************************
     * 這下這一區是 MyPlaybackEventListener **************************************************************/

    private boolean isPlaylist = false;
    private static boolean isFinished = false;
    static Handler playHandler;

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        String playbackState = "NOT_PLAYING";
        String bufferingState = "NO_BUFFER";

        @Override
        public void onPlaying() {
            playbackState = "PLAYING";
            log( "PlaybackState: " + playbackState);
        }

        @Override
        public void onBuffering(boolean isBuffering) {
            bufferingState = isBuffering ? "(BUFFERING)" : "(FULL)";
            log( "BufferingState: " + bufferingState);
            isFinished = false;
        }

        @Override
        public void onStopped() {
            log( "STOP: PlaybackState: " + playbackState +", BufferState: "+bufferingState);
            if (!bufferingState.equals("NO_BUFFER")) {
                if (!(isPlaylist && player.hasNext())) {
//                    log( "沒有下一首了");
//                    finish();
                }
            } else if (playbackState.equals("NOT_PLAYING")) {
                isFinished = true;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message msg = Message.obtain();
                        msg.what = 100;
                        playHandler.sendMessageDelayed(msg, 3000);
                    }
                }).start();
            }
            playbackState = "STOPPED";
        }

        @Override
        public void onPaused() {
            playbackState = "PAUSED";
            log( "PlaybackState: " + playbackState);
//            finish();
        }

        private String formatTime(int millis) {
            int seconds = millis / 1000;
            int minutes = seconds / 60;
            int hours = minutes / 60;

            return (hours == 0 ? "" : hours + ":")
                    + String.format("%02d:%02d", minutes % 60, seconds % 60);
        }

        @Override
        public void onSeekTo(int endPositionMillis) {
            log( String.format("\tSEEKTO: (%s/%s)",
                    formatTime(endPositionMillis),
                    formatTime(player.getDurationMillis())));
        }
    }

    /**************************************************************************************************
     * 這下這一區是 implements OnDSListener, OnDSPermissionsListener，也就是語音辨識結果 *******************/

    Command command;
    private DroidSpeech droidSpeech;

    /* The droid speech supported languages
     *
     * @param currentSpeechLanguage    The current speech language
     * @param supportedSpeechLanguages The supported speech languages
     **************************************************************************************************/
    @Override
    public void onDroidSpeechSupportedLanguages(String currentSpeechLanguage, List<String> supportedSpeechLanguages) {
        log( "Current speech language = " + currentSpeechLanguage);
        log( "Supported speech languages = " + supportedSpeechLanguages.toString());

        if (supportedSpeechLanguages.contains("cmn-")) {
            // Setting the droid speech preferred language as tamil if found
            droidSpeech.setPreferredLanguage("cmn-Hant-TW");
        }
    }

    /**
     * The droid speech rms changed result
     *
     * @param rmsChangedValue The rms changed result
     */
    @Override
    public void onDroidSpeechRmsChanged(float rmsChangedValue) {

    }

    /**
     * The droid speech recognizer live result
     *
     * @param liveSpeechResult The live speech result
     */
    @Override
    public void onDroidSpeechLiveResult(String liveSpeechResult) {

    }

    /**
     * The droid speech recognizer final result
     *
     * @param finalSpeechResult The final speech result
     */
    @Override
    public void onDroidSpeechFinalResult(String finalSpeechResult) {
        log( "onDroidSpeechFinalResult = " + finalSpeechResult);

        int cmd;
        switch (cmd = command.matchCmd(finalSpeechResult)) {
            case Command.CmdPlay:
                isRepeat = isRepeatAll = false;
                performClickAt(curPos);
                break;
            case Command.CmdRepeat:
                isRepeat = true;
                isRepeatAll = false;
                performClickAt(curPos);
                break;
            case Command.CmdRepeatAll:
                isRepeat = false;
                isRepeatAll = true;
                performClickAt(curPos);
                break;
            case Command.CmdPlayWithKey:
                isRepeat = isRepeatAll = false;
                keyword = command.keyword; // +"官方MV";
                searchOnYoutube(true);
                break;
            case Command.CmdPlayNext:
                isRepeat = isRepeatAll = false;
                if (curPos < videosFound.getAdapter().getCount() - 1) {
                    ++curPos;
                }
                performClickAt(curPos);
                break;
            case Command.CmdPlayBack:
                isRepeat = isRepeatAll = false;
                if (curPos > 0) {
                    --curPos;
                }
                performClickAt(curPos);
                break;
            case Command.CmdNext:
                isRepeat = isRepeatAll = false;
                if (curPos < videosFound.getAdapter().getCount() - 1) {
                    ++curPos;
                    isMyFavorites = true;
                    videosFound.setSelection(curPos);
                    videosFound.getAdapter().getView(curPos, null, null).setSelected(true);
                }
                break;
            case Command.CmdBack:
                isRepeat = isRepeatAll = false;
                if (curPos > 0) {
                    --curPos;
                    isMyFavorites = true;
                    videosFound.setSelection(curPos);
                    videosFound.getAdapter().getView(curPos, null, null).setSelected(true);
                }
                break;
            case Command.CmdMyFavorites:
                isRepeat = isRepeatAll = false;
                isMyFavorites = true;
                loadMyFavorites();
                break;
            case Command.CmdPlayList:
                isRepeat = isRepeatAll = false;
                myFavorites.add(curIds.get(curPos));
                isMyFavorites = true;
                loadMyFavorites();
                break;
            case Command.CmdDelete:
                isRepeat = isRepeatAll = false;
                myFavorites.del(curIds.get(curPos));
                isMyFavorites = true;
                loadMyFavorites();
                break;
            case Command.CmdClean:
                isRepeat = isRepeatAll = false;
                myFavorites.clean();
                keyword = "請說出「播放XXX」";
                isMyFavorites = false;
                loadMyFavorites();
                break;
            default: // CmdUnknown
        }
    }

    /**
     * The droid speech recognition was closed by user
     */
    @Override
    public void onDroidSpeechClosedByUser() {
    }

    /**
     * The droid speech recognizer error update
     *
     * @param errorMsg The error message
     */
    @Override
    public void onDroidSpeechError(String errorMsg) {
        stop();
    }

    /**
     * Sends an update with the droid speech audio permission status
     *
     * @param audioPermissionGiven The audio permission given status
     * @param errorMsgIfAny        Error message if any
     */
    @Override
    public void onDroidSpeechAudioPermissionStatus(boolean audioPermissionGiven, String errorMsgIfAny) {
        if (audioPermissionGiven) {
            start();
        } else {
            if (errorMsgIfAny != null) {
                log( "onDroidSpeechAudioPermissionStatus = " + errorMsgIfAny);
            }
            stop();
        }
    }

    public void start() {
        log( "start");
        droidSpeech.startDroidSpeechRecognition();
    }

    public void stop() {
        log( "stop");
        droidSpeech.closeDroidSpeechOperations();
    }

    /**************************************************************************************************
     * 這下這一區是 自定義 工具區 *************************************************************************/

    private List<VideoItem> searchResults;
    private Handler ytLoadHandler;
    boolean hadSearched = false;
    ArrayAdapter<VideoItem> mAdapter;
    MyFavorites myFavorites;
    ArrayList<String> curIds;
    int curPos = 0;
    Boolean isMyFavorites = false;
    String keyword = "請說出「播放XXX」";
    boolean isRepeat = false;
    boolean isRepeatAll = false;

    private void loadVideo(String vid) {
        player.loadVideo(vid);
    }

    private void loadList(String pid) {
        player.loadPlaylist(pid);
    }

    private void loadMyFavorites() {
        new Thread() {
            @Override
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(me);
                log("loadMyFavorites() "+searchResults.size());
                searchResults = yc.searchByVideoIds(myFavorites.get());
                ytLoadHandler.post(() -> updateVideosFound(true));
            }
        }.start();
    }

    private void loadLaunch() {
        new Thread() {
            @Override
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(me);
                searchResults = yc.searchByChannelId(LaunchChannel);
                ytLoadHandler.post(() -> updateVideosFound(false));

                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("text", "loadLaunch("+LaunchChannel+") "+searchResults.size());
                msg.setData(bundle);
                msg.what = 1000;
                ytLoadHandler.sendMessage(msg);
            }
        }.start();
    }

    private void performClickAt(int pos) {
        if (pos < videosFound.getAdapter().getCount()) {
            videosFound.performItemClick(videosFound, pos, pos);
        }
    }

    private void updateVideosFound(boolean autoPlay) {
        hadSearched = true;
        curIds = new ArrayList<>();
        for (VideoItem item : searchResults) {
            curIds.add(item.getId());
        }
        mAdapter = new ArrayAdapter<VideoItem>(getApplicationContext(), R.layout.video_item, searchResults) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.video_item, parent, false);
                }

                ImageView thumbnail = convertView.findViewById(R.id.video_thumbnail);
                TextView title = convertView.findViewById(R.id.video_title);

                VideoItem searchResult = searchResults.get(position);

                Picasso.with(getApplicationContext()).load(searchResult.getThumbnailURL()).into(thumbnail);
                title.setText(searchResult.getTitle());
                return convertView;
            }
        };
        videosFound.setAdapter(mAdapter);
        if (autoPlay) {
            curPos = 0;
            performClickAt(curPos);
        }
    }

    private void searchOnYoutube(boolean autoPlay) {
        log( "searchOnYoutube(" + keyword + ", " + autoPlay + ")");
        isRepeat = isRepeatAll = false;
        new Thread() {
            @Override
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(me);
                searchResults = yc.search(keyword);
                ytLoadHandler.post(() -> updateVideosFound(autoPlay));
            }
        }.start();
    }

    public void log(String msg) {
        tvDesc.append("\n"+msg);
    }

}
