package com.wade.hisonet;

import android.content.Context;
import android.media.AudioManager;

public class Mute {
    boolean muteState;
    int volume;
    AudioManager audioManager;
    Mute (Context context) {
        audioManager = (AudioManager) context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        muteState = false;
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    public void setMute(boolean mute) {
        muteState = mute;
        if (mute) audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_PLAY_SOUND);
        else audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_PLAY_SOUND);
    }
}
