package com.wade.hisonet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Command {
    public static final int CmdUnknown     = 0;
    public static final int CmdPlay        = 1;
    public static final int CmdPlayWithKey = 2;
    public static final int CmdPlayNext    = 3;
    public static final int CmdPlayBack    = 4;
    public static final int CmdMyFavorites = 5;
    public static final int CmdPlayList    = 6;
    public static final int CmdClean       = 7;
    public static final int CmdDelete      = 8;
    public static final int CmdNext        = 9;
    public static final int CmdBack        = 10;
    public static final int CmdRepeat      = 11;
    public static final int CmdRepeatAll   = 12;

    public ArrayList<String> cmdsPlay = new ArrayList<String>() {
        {
            add("play");
            add("播放");

        }};
    public ArrayList<String> cmdsPlayNext = new ArrayList<String>() {
        {
            add("play next");
            add("下一首");
        }};
    public ArrayList<String> cmdsPlayBack = new ArrayList<String>() {
        {
            add("play back");
            add("上一首");
        }};
    public ArrayList<String> cmdsNext = new ArrayList<String>() {
        {
            add("next");
            add("向下");
        }};
    public ArrayList<String> cmdsBack = new ArrayList<String>() {
        {
            add("back");
            add("向上");
        }};
    public ArrayList<String> cmdsPlaylist = new ArrayList<String>() {
        {
            add("add");
            add("加入");
        }};
    public ArrayList<String> cmdsDelete = new ArrayList<String>() {
        {
            add("delete");
            add("刪除");
        }};
    public ArrayList<String> cmdsClean = new ArrayList<String>() {
        {
            add("clean");
            add("清空");
            add("清除");
        }};
    public ArrayList<String> cmdsMyFavorites = new ArrayList<String>() {
        {
            add("my favorite");
            add("我的最愛");
        }};
    public ArrayList<String> cmdsRepeat = new ArrayList<String>() {
        {
            add("repeat");
            add("重覆");
        }};
    public ArrayList<String> cmdsRepeatAll = new ArrayList<String>() {
        {
            add("repeat all");
            add("全部重覆");
        }};

    public Command () {
        initFuzzyDb();
    }

    private HashMap<String, ArrayList<String>> fuzzyDb = new HashMap<>();
    /* 將來可以用 sqlite 來實作 */
    private void initFuzzyDb() {
        ArrayList<String> list;

        list = new ArrayList<String>(
                Arrays.asList("姍", "珊", "山", "三", "杉", "衫", "柵", "潸", "扇", "膻",
                        "煽", "善", "擅", "繕", "禪", "疝", "閃", "陝")
        );
        fuzzyDb.put("刪", list);

        list = new ArrayList<String>(
                Arrays.asList("剝", "菠", "波", "撥", "玻", "薄", "柏", "檗", "擘", "簸")
        );
        fuzzyDb.put("播", list);

        list = new ArrayList<String>(
                Arrays.asList("廈", "嚇", "唬", "匣", "陝", "俠", "葭", "狎", "狹", "峽", "暇", "呷", "蝦")
        );
        fuzzyDb.put("下", list);

        list = new ArrayList<String>(
                Arrays.asList("尚", "喪", "桑", "賞", "晌", "嗓")
        );
        fuzzyDb.put("上", list);

        list = new ArrayList<String>(
                Arrays.asList("醫", "伊", "依", "壹", "揖", "椅", "依", "義", "藝", "役",
                        "佚", "亦", "譯", "億", "詣", "儀", "宜", "誼", "詣", "詒",
                        "遺", "夷", "姨", "疑", "夷", "夷")
        );
        fuzzyDb.put("一", list);

        list = new ArrayList<String>(
                Arrays.asList("守", "手", "獸", "受", "售", "壽", "授", "瘦", "緩", "叟")
        );
        fuzzyDb.put("首", list);

        list = new ArrayList<String>(
                Arrays.asList("副", "附", "富", "福", "付", "傅", "訃", "駙", "婦", "負",
                        "縛", "賦", "父", "府", "俯", "嘸", "拊", "撫", "輔", "腐",
                        "釜", "夫", "苻", "佛", "弗", "幅", "伏", "扶", "拂")
        );
        fuzzyDb.put("覆", list);

        list = new ArrayList<String>(
                Arrays.asList("輕", "青", "卿", "傾", "氫", "蜻", "情", "晴", "擎", "黥",
                        "慶", "磬", "頃")
        );
        fuzzyDb.put("清", list);

        list = new ArrayList<String>(
                Arrays.asList("控", "恐", "孔")
        );
        fuzzyDb.put("空", list);

        list = new ArrayList<String>(
                Arrays.asList("楚", "廚", "鋤", "芻", "蜍", "躕", "雛", "儲", "杵", "楮",
                        "褚", "礎", "處")
        );
        fuzzyDb.put("除", list);

        list = new ArrayList<String>(
                Arrays.asList("醉", "罪", "綴", "墜", "惴", "縋")
        );
        fuzzyDb.put("最", list);

        list = new ArrayList<String>(
                Arrays.asList("艾", "唚", "噫", "嬡", "礙", "曖", "璦", "哀", "挨", "矮",
                        "欸")
        );
        fuzzyDb.put("愛", list);
    }

    // cmd 夏宜手, str 下一首
    private boolean fuzzyComp(String cmd, String str) {
//        Log.d("MyLog", "fuzzyComp('"+cmd+"', '"+str);
        for (int i=0; i<str.length(); ++i) {
            String src = cmd;
            String key = str.substring(i, i+1);
//            Log.d("MyLog", "key = " + key);
            if (fuzzyDb.containsKey(key)) {
//                Log.d("MyLog", "key = " + key + ": fuzzydb values = " + fuzzyDb.get(key).toArray().toString());
                for (String fuzzy : fuzzyDb.get(key)) {
                    if (src.substring(i, i + 1).equals(fuzzy)) {
                        if (i >= src.length() - 1)
                            src = src.substring(0, i) + fuzzy;
                        else
                            src = src.substring(0, i) + fuzzy + src.substring(i + 1);
                    }
                }
                if (src.equals(str)) return true;
            }
        }
        return false;
    }

    // 傳回去主要用途是切割字串，所以長度比較重要, cmd 是語音辨識結果，需要修正
    public String matchCmd0(String cmd, ArrayList<String> cmds) {
        for (String str:cmds) {
            // 辨識到的字比命令短，忽略
            if (cmd.length() < str.length()) continue;
            // 先完整比對
            if (cmd.startsWith(str)) return str;
            // 如果有英文，那麼一定是錯的
            if (str.matches("[a-z]")) continue;
            // 此時需要模糊比對 cmd 的前 str.len 長度
            if (fuzzyComp(cmd, str)) return str;
        }
        return "";
    }

    public String keyword;
    public int matchCmd(String cmd) {
//        Log.d("MyLog", "matchCmd('"+cmd+"')");
        String key;
        // 先比對 播放
        String matched;
        if (!(matched = matchCmd0(cmd, cmdsPlay)).isEmpty()) {
            key = cmd.substring(matched.length());
            if (key.isEmpty()) {
                return CmdPlay;
            } else {
                keyword = key; // +"官方MV";
                return CmdPlayWithKey;
            }
        } else if (!matchCmd0(cmd, cmdsPlayNext).isEmpty()) {
            return CmdPlayNext;
        } else if (!matchCmd0(cmd, cmdsPlayBack).isEmpty()) {
            return CmdPlayBack;
        } else if (!matchCmd0(cmd, cmdsNext).isEmpty()) {
            return CmdNext;
        } else if (!matchCmd0(cmd, cmdsBack).isEmpty()) {
            return CmdBack;
        } else if (!matchCmd0(cmd, cmdsMyFavorites).isEmpty()) {
            return CmdMyFavorites;
        } else if (!matchCmd0(cmd, cmdsPlaylist).isEmpty()) {
            return CmdPlayList;
        } else if (!matchCmd0(cmd, cmdsDelete).isEmpty()) {
            return CmdDelete;
        } else if (!matchCmd0(cmd, cmdsClean).isEmpty()) {
            return CmdClean;
        }
        return CmdUnknown;
    }
}
