package com.wade.hisonet;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Text2Speech implements TextToSpeech.OnInitListener {
    
    HiSoNetActivity me;
    public interface Listener {
        void onDone();
    }

    private static final String UTT_COMPLETED_FEEDBACK = "UTT_COMPLETED_FEEDBACK";

    private final TextToSpeech mTts;

    public Text2Speech(Context context) {
        // TODO: use the 3-arg constructor (API 14) that supports passing the engine.
        // Choose the engine that supports the selected language, if there are several
        // then let the user choose.
        me = (HiSoNetActivity) context;
        mTts = new TextToSpeech(context, this);
        log( "Default TTS engine:" + mTts.getDefaultEngine());
    }

    @Override
    public void onInit(int status) {
        switch (status) {
            case TextToSpeech.SUCCESS:
                mTts.setLanguage(Locale.US);
                say("Hi! So Net.");
                mTts.setLanguage(Locale.TRADITIONAL_CHINESE);
                log( "TextToSpeech engine successfully started");
                break;

            case TextToSpeech.ERROR:
                log( "Error while initializing TextToSpeech engine!");
                break;

            default:
                log( "Unknown TextToSpeech status: " + status);
                break;
        }
    }

    public void say(String text) {
        if (mTts != null) {
            log( "say('"+text+"')");
            mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else log( "Cannot say when not init TTS");
    }

    public void stop() {
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        } else {
            log( "Stop when not init TTS");
        }
    }

    public boolean isLanguageAvailable(String localeAsStr) {
        if (mTts == null) return false;
        else return mTts.isLanguageAvailable(new Locale(localeAsStr)) >= 0;
    }

    // setLanguage(Locale.ZH)

    public void setLanguage(Locale locale) {
        if (mTts != null)
            mTts.setLanguage(locale);
    }

    public void shutdown() {
        if (mTts != null)
            mTts.shutdown();
    }
    
    private void log(String msg) {
        me.log(msg);
    }
}
