/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wade.bodytemperature;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.HashMap;

public class UsbActivity extends Activity {
    private static final String TAG = "MyLog";
    private static final int INTERFACE_CLASS_HID = 3;
    /* USB system service */
    private static final int VID = 0x0E6A;
    private static final int PID = 0x0124;
    private Context context;
    private UsbManager mUsbManager;
    private UsbDevice mUsbDevice;
    private UsbInterface[] mUsbInterface;
    private UsbDeviceConnection mConnection;
    private UsbEndpoint[] mInUsbEndpoint;
    private UsbEndpoint mOutUsbEndpoint;
    private Button btnRead;

    /* UI elements */
    private TextView mStatusView, mResultView;
    protected static final int STD_USB_REQUEST_GET_DESCRIPTOR = 0x06;
    protected static final int LIBUSB_DT_STRING = 0x03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb);
        context = this;
        mStatusView = (TextView) findViewById(R.id.text_status);
        mResultView = (TextView) findViewById(R.id.text_result);

        mUsbManager = getSystemService(UsbManager.class);
        btnRead = (Button)findViewById(R.id.btnRead);
        btnRead.setEnabled(false);
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readTemp();
            }
        });
        // Detach events are sent as a system-wide broadcast
        IntentFilter filter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        handleIntent(getIntent());
    }

    private final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length*2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public int write(byte[] data) {
        if (mConnection == null) return 0;
		int res;
		//res = mConnection.controlTransfer (0x21, 0x09, 0x0300 | data[0], 1, data, size, 1000);
		res = mConnection.controlTransfer (33, 0x09, 768 | data[0], 1, data, 8, 1000);
        Log.d("MyLog", "Write :" + res);
        return res;
    }

    public byte[] read(int ep) {
        if (mConnection == null) return null;
        ByteBuffer buffer = ByteBuffer.allocate(mInUsbEndpoint[ep].getMaxPacketSize());
        UsbRequest request = new UsbRequest();
        request.initialize(mConnection, mInUsbEndpoint[ep]);
        if (request.queue(buffer)) {
            mConnection.requestWait();
            Log.d("MyLog", "request.queue pass");
        } else {
            Log.d("MyLog", "request.queue fail");
            return null;
        }
        Log.d("MyLog", "Read END");
        return buffer.array();
    }

    private byte[] sendCommand(byte[] command, int cmd) {
        //UsbRequest request = write(command);
        //if (res == null) {
        int result = write(command);
        if (result < 0) {
            printStatus("SendCommand("+bytesToHex(command)+") failed");
            return null;
        }
        printResult("CMD:"+bytesToHex(command));
        byte[] res = read(1);
        if (res == null) {
            printStatus("Read failed");
            return null;
        }
        if (cmd == 3) {
            res = read(0);
        }
        return res;
    }

	private void readTemp() {
        if (mConnection == null || mInUsbEndpoint.length == 0 || mOutUsbEndpoint == null) {
            Log.d(TAG, "readTemp()"+(mConnection == null)+"/"+(mInUsbEndpoint.length)+"/"+(mOutUsbEndpoint == null));
            return;
        }
        byte[][] cmds;
        cmds = new byte[5][8];
        cmds[0] = toBytes(0x05, 0x82, 0x08, 0x04, 0x03, 0x07, 0x00, 0x00); // SetGPIOMode
        cmds[1] = toBytes(0x03, 0x83, 0x08, 0x03, 0x07, 0x00, 0x00, 0x00); // SetGPIOData
        cmds[2] = toBytes(0x03, 0x94, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00); // SetStreamMode
        cmds[3] = toBytes(0x05, 0x87, 0x02, 0x46, 0x02, 0x00, 0x00, 0x00); // StreamRead
        cmds[4] = toBytes(0x03, 0x83, 0x08, 0x07, 0x07, 0x00, 0x00, 0x00); // SetGPIOData2
        clearResult();
        printResult(">>>>> BEGIN");
        byte[] res;
        for (int i=0; i<4; i++) {
            res = sendCommand(cmds[i], i);
            if (res != null) {
                printResult(bytesToHex(res));
            } else {
                printResult("NONE");
            }
        }
        printResult("<<<<< END");
	}

    private static byte[] toBytes(int... ints) { // helper function
        byte[] result = new byte[ints.length];
        for (int i = 0; i < ints.length; i++) {
            result[i] = (byte) ints[i];
        }
        return result;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbReceiver);
    }

    /**
     * Broadcast receiver to handle USB disconnect events.
     */
    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("MyLog", "BroadcastReceiver() "+action);
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
//                    btnRead.setEnabled(false);
                    printStatus(getString(R.string.status_removed));
                    printDeviceDescription(device);
                    if (mConnection != null) mConnection.close();
                }
            } else if (action.contains("USB_PERMISSION")) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){
                            Log.d(TAG, "USB_PERMISSION");
                        }
                    }
                    else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };

    private void connectDevice() {
        if (mUsbDevice.getInterfaceCount() != 2) {
            printStatus("裝置不正確1");
            return;
        }
        mConnection = mUsbManager.openDevice(mUsbDevice);
        mUsbInterface = new UsbInterface[2];
        mUsbInterface[0] = mUsbDevice.getInterface(0);
        mUsbInterface[1] = mUsbDevice.getInterface(1);
        if (mUsbInterface[0].getInterfaceClass() != INTERFACE_CLASS_HID) {
            printStatus("裝置不正確 Class != HID");
            return;
        }
        if (mUsbInterface[0].getEndpointCount() != 2) {
            printStatus("裝置不正確 EP0 Count: "+mUsbInterface[0].getEndpointCount());
            return;
        }
        if (mUsbInterface[1].getEndpointCount() != 1) {
            printStatus("裝置不正確 EP1 Count: "+mUsbInterface[1].getEndpointCount());
            return;
        }
        mOutUsbEndpoint = mUsbInterface[0].getEndpoint(0);
        if (mOutUsbEndpoint.getDirection() != UsbConstants.USB_DIR_OUT) {
            printStatus("EP0 Not output");
            return;
        }
        mInUsbEndpoint = new UsbEndpoint[2];
        mInUsbEndpoint[0] = mUsbInterface[0].getEndpoint(1);
        if (mInUsbEndpoint[0].getDirection() != UsbConstants.USB_DIR_IN) {
            printStatus("EP1 Not intput");
            return;
        }
        mInUsbEndpoint[1] = mUsbInterface[1].getEndpoint(0);
        if (mInUsbEndpoint[1].getDirection() != UsbConstants.USB_DIR_IN) {
            printStatus("EP2 Not intput");
            return;
        }
        btnRead.setEnabled(true);
        printStatus("Endpoint Type = "+mOutUsbEndpoint.getType()+"/"+
                mInUsbEndpoint[0].getType()+"/"+mInUsbEndpoint[1].getType());
    }

    /**
     * Determine whether to list all devices or query a specific device from
     * the provided intent.
     * @param intent Intent to query.
     */
    private void handleIntent(Intent intent) {
        mUsbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        // 手動調用 APK, mUsbDevice == null，只有插入事件才不會 null
        if (mUsbDevice != null &&
                mUsbDevice.getVendorId() == VID &&
                mUsbDevice.getProductId() == PID) {
            printDeviceDetails(mUsbDevice);
            connectDevice();
        } else {
//            btnRead.setEnabled(false);
            printDeviceList();
        }
    }

    public void close() {
        if (mConnection != null) {
            mConnection.close();
        }
    }

    /**
     * Print the list of currently visible USB devices.
     */
    private void printDeviceList() {
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();

        if (deviceList.isEmpty()) {
            printResult("未連接任何體溫感測器");
        } else {
            for (UsbDevice device : deviceList.values()) {
                if (device.getVendorId() == VID && device.getProductId() == PID) {
                    printDeviceDetails(device);
                    mUsbDevice = device;
                    connectDevice();
                    return;
                }
            }
            printResult("沒找到正確的體溫感測器");
        }
    }

    /**
     * Print a basic description about a specific USB device.
     * @param device USB device to query.
     */
    private void printDeviceDescription(UsbDevice device) {
        String result = UsbHelper.readDevice(device) + "\n\n";
        printResult(result);
    }

    /**
     * Initiate a control transfer to request the device information
     * from its descriptors.
     *
     * @param device USB device to query.
     */
    private void printDeviceDetails(UsbDevice device) {
        if (mConnection == null) {
            mConnection = mUsbManager.openDevice(device);
        }
        if (mConnection == null) {
            Log.d(TAG, "No Usb Connection");
            return;
        }

        String deviceString = "";
        try {
            //Parse the raw device descriptor
            DeviceDescriptor dd = DeviceDescriptor.fromDeviceConnection(mConnection);
            if (dd == null) return;
            deviceString = dd.toString();
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Invalid device descriptor", e);
        }

        String configString = "";
        try {
            //Parse the raw configuration descriptor
            configString = ConfigurationDescriptor.fromDeviceConnection(mConnection)
                    .toString();
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Invalid config descriptor", e);
        } catch (ParseException e) {
            Log.w(TAG, "Unable to parse config descriptor", e);
        }


		String manufacturer = "", product = "", serial = "";

		byte[] rawDescs = mConnection.getRawDescriptors();
    	try {
    	    byte[] buffer = new byte[255];
    	    int idxMan = rawDescs[14];
    	    int idxPrd = rawDescs[15];

    	    int rdo = mConnection.controlTransfer(UsbConstants.USB_DIR_IN | UsbConstants.USB_TYPE_STANDARD,
    	            STD_USB_REQUEST_GET_DESCRIPTOR, (LIBUSB_DT_STRING << 8) | idxMan, 0x0409, buffer, 0xFF, 0);
    	    manufacturer = new String(buffer, 2, rdo - 2, "UTF-16LE");

	        rdo = mConnection.controlTransfer(UsbConstants.USB_DIR_IN | UsbConstants.USB_TYPE_STANDARD,
	                STD_USB_REQUEST_GET_DESCRIPTOR, (LIBUSB_DT_STRING << 8) | idxPrd, 0x0409, buffer, 0xFF, 0);
	        product = new String(buffer, 2, rdo - 2, "UTF-16LE");

	        serial = mConnection.getSerial();
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }

        printResult("\n"+deviceString +
                "\n" + configString+
                "\n製造：\n"+manufacturer+":"+product+(serial == null?"":serial));
    }

    /* Helpers to display user content */

    private void printStatus(String status) {
        mStatusView.setText(status);
        Log.i(TAG, "Status:"+status);
    }

    private void clearResult() {
        mResultView.setText("");
    }
    private void printResult(String result) {
        mResultView.setText(mResultView.getText().toString()+"\n"+result);
        Log.i(TAG, "Result:"+result);
    }
}
