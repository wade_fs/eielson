/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wade.bodytemperature;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;

import java.util.Locale;

/**
 * Parsing utility for USB device configuration info.
 */
public class UsbHelper {

    /* Descriptor header sizes */
    public static final int DESC_SIZE_DEVICE = 18;
    public static final int DESC_SIZE_CONFIG = 9;
    public static final int DESC_SIZE_INTERFACE = 9;
    public static final int DESC_SIZE_ENDPOINT = 7;
    /* USB descriptor type values */
    public static final int DESC_TYPE_DEVICE = 0x01;
    public static final int DESC_TYPE_CONFIG = 0x02;
    public static final int DESC_TYPE_INTERFACE = 0x04;
    public static final int DESC_TYPE_ENDPOINT = 0x05;
    /* USB power attribute flags */
    public static final int ATTR_BUSPOWER = 0x80;
    public static final int ATTR_SELFPOWER = 0x40;
    public static final int ATTR_REMOTEWAKE = 0x20;

    /**
     * Enumerate the endpoints and interfaces on the connected device.
     *
     * @param device Device to query.
     * @return String description of the device configuration.
     */
    public static String readDevice(UsbDevice device) {
        StringBuilder sb = new StringBuilder();
        sb.append("裝置路徑: " + device.getDeviceName() + "\n");
        sb.append(String.format(
                "裝置類別: %s -> 子類別: 0x%02x -> 協定: 0x%02x\n",
                nameForClass(device.getDeviceClass()),
                device.getDeviceSubclass(), device.getDeviceProtocol()));

        for (int i = 0; i < device.getInterfaceCount(); i++) {
            UsbInterface intf = device.getInterface(i);
            sb.append(String.format(Locale.TRADITIONAL_CHINESE,
                    "-- 界面 %d 類別: %s -> 子類別: 0x%02x -> 協定: 0x%02x\n",
                    intf.getId(),
                    nameForClass(intf.getInterfaceClass()),
                    intf.getInterfaceSubclass(),
                    intf.getInterfaceProtocol()));

            sb.append(String.format(Locale.US, "   -- 端點數: %d\n",
                    intf.getEndpointCount()));
        }

        return sb.toString();
    }

    /* Helper Methods to Provide Readable Names for USB Constants */

    public static String nameForClass(int classType) {
        switch (classType) {
            case UsbConstants.USB_CLASS_APP_SPEC:
                return String.format("Application Specific 0x%02x", classType);
            case UsbConstants.USB_CLASS_AUDIO:
                return "語音";
            case UsbConstants.USB_CLASS_CDC_DATA:
                return "CDC Control";
            case UsbConstants.USB_CLASS_COMM:
                return "通信";
            case UsbConstants.USB_CLASS_CONTENT_SEC:
                return "加密";
            case UsbConstants.USB_CLASS_CSCID:
                return "智慧卡";
            case UsbConstants.USB_CLASS_HID:
                return "互動裝置";
            case UsbConstants.USB_CLASS_HUB:
                return "Hub";
            case UsbConstants.USB_CLASS_MASS_STORAGE:
                return "硬碟";
            case UsbConstants.USB_CLASS_MISC:
                return "無線裝置";
            case UsbConstants.USB_CLASS_PER_INTERFACE:
                return "(Defined Per Interface)";
            case UsbConstants.USB_CLASS_PHYSICA:
                return "實體";
            case UsbConstants.USB_CLASS_PRINTER:
                return "印表機";
            case UsbConstants.USB_CLASS_STILL_IMAGE:
                return "照相機";
            case UsbConstants.USB_CLASS_VENDOR_SPEC:
                return String.format("Vendor Specific 0x%02x", classType);
            case UsbConstants.USB_CLASS_VIDEO:
                return "影像卡";
            case UsbConstants.USB_CLASS_WIRELESS_CONTROLLER:
                return "無線控制器";
            default:
                return String.format("0x%02x", classType);
        }
    }

    public static String nameForEndpointType(int type) {
        switch (type) {
            case UsbConstants.USB_ENDPOINT_XFER_BULK:
                return "塊";
            case UsbConstants.USB_ENDPOINT_XFER_CONTROL:
                return "控制";
            case UsbConstants.USB_ENDPOINT_XFER_INT:
                return "中斷";
            case UsbConstants.USB_ENDPOINT_XFER_ISOC:
                return "即時";
            default:
                return "未知";
        }
    }

    public static String nameForDirection(int direction) {
        switch (direction) {
            case UsbConstants.USB_DIR_IN:
                return "輸入";
            case UsbConstants.USB_DIR_OUT:
                return "輸出";
            default:
                return "方向不明";
        }
    }
}
