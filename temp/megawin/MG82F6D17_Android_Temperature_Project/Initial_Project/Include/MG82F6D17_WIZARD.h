/**
 ******************************************************************************
 *
 * @file        MG82F6D17_WIZARD.h
 *
 * @brief       This is a C code for Wizard interface.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IS" without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the worldwide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 ******************************************************************************
 */

//*** <<< Use Configuration Wizard in Context Menu >>> ***

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Brian_20200605 //bugNum_Authour_Date
 * >> Modify from Xian Qiu email zip file: MG82F6D17_010_GPIO_Wizard.c
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize General Purpose Input/Output(GPIO)
//<i> Check this term, then DRV_GPIO_Wizard_Init() will initialize GPIO.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_GPIO_WIZARD  1

//  <h> Port1 Config
//      <e0> P1.0
        #define MG82F6D17_GPIO_P1_0  1
//          <o0> Mode Assignment
//          <i> Specify P1.0 mode.
//              <0x01010001=> Analog Input Only (P1M0.0=0,P1M1.0=1)(Default)
//              <0x01010101=> Open Drain with Pull-up Resistor (P1M0.0=1, P1M1.0=1)
//              <0x01010000=> Open Drain (P1M0.1=0, P1M1.1=0)
//              <0x01010100=> Push Pull (P1M0.0=1, P1M1.0=0)
            #define P10_MODE  0x01010000

//          <o0> Fast Driving Control
//          <i> P1.0 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P1FDC.0=0)(Default)
//              <1=> Enable Fast Driving (P1FDC.0=1)
            #define P1FDC_0  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN0
//              <20=> RXD1 (S1PS1=0, S1PS0=0)
//              <124=> T2 (T2PS1=0, T2PS0=0)
//              <124=> T2CKO (T2PS1=0, T2PS0=0)
//              <160=> KBI0 (KBI0PS=0)
//              <181=> nINT1 (INT1IS2=0, INT1IS1=1, INT1IS0=1)
            #define AF_P10  0
//      </e> P1.0 End

//      <e0> P1.1
        #define MG82F6D17_GPIO_P1_1  0
//          <o0> Mode Assignment
//          <i> Specify P1.1mode.
//              <0x02020002=> Analog Input Only (P1M0.1=0, P1M1.1=1)(Default)
//              <0x02020202=> Open Drain with Pull-up Resistor (P1M0.1=1, P1M1.1=1)
//              <0x02020000=> Open Drain (P1M0.1=0, P1M1.1=0)
//              <0x02020200=> Push Pull (P1M0.1=1, P1M1.1=0)
            #define P11_MODE  0x02020002

//          <o0> Fast Driving Control
//          <i> P1.1 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P1FDC.1=0)(Default)
//              <1=> Enable Fast Driving (P1FDC.1=1)
            #define P1FDC_1  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN1
//              <21=> TXD1 (S1PS1=0, S1PS0=0)
//              <125=> T2EX (T2PS1=0, T2PS0=0)
//              <161=> KBI1 (KBI0PS=0)
//              <180=> nINT0 (INT0IS2=1, INT0IS1=0, INT0IS0=1)
//              <182=> nINT2 (INT2IS2=0, INT2IS1=1, INT2IS0=0)
            #define AF_P11  0
//      </e> P1.1 End

//      <e0> P1.5
        #define MG82F6D17_GPIO_P1_5  0
//          <o0> Mode Assignment
//          <i> Specify P1.5mode.
//              <0x20200020=> Analog Input Only (P1M0.5=0, P1M1.5=1)(Default)
//              <0x20202020=> Open Drain with Pull-up Resistor (P1M0.6=1, P1M1.6=1)
//              <0x20200000=> Open Drain (P1M0.1=0, P1M1.1=0)
//              <0x20202000=> Push Pull (P1M0.5=1, P1M1.5=0)
            #define P15_MODE  0x20200020

//          <o0> Fast Driving Control
//          <i> P1.5 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P1FDC.5=0)(Default)
//              <1=> Enable Fast Driving (P1FDC.5=1)
            #define P1FDC_5  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN5
//              <141=> MOSI (SPIPS0=0)
//              <164=> KBI4 (KBI4PS1=1, KBI4PS0=1)
//              <165=> KBI5 (KBI4PS1=0, KBI4PS0=0)
//              <181=> nINT1 (INT1IS2=1, INT1IS1=1, INT1IS0=0)
            #define AF_P15  0
//      </e> P1.5 End

//      <e0> P1.6
        #define MG82F6D17_GPIO_P1_6  0
//          <o0> Mode Assignment
//          <i> Specify P1.6mode.
//              <0x40400040=> Analog Input Only (P1M0.6=0, P1M1.6=1)(Default)
//              <0x40404040=> Open Drain with Pull-up Resistor (P1M0.6=1, P1M1.6=1)
//              <0x40400000=> Open Drain (P1M0.1=0, P1M1.1=0)
//              <0x40404000=> Push Pull (P1M0.6=1, P1M1.6=0)
            #define P16_MODE  0x40400040

//          <o0> Fast Driving Control
//          <i> P1.6 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P1FDC.6=0)(Default)
//              <1=> Enable Fast Driving (P1FDC.6=1)
            #define P1FDC_6  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN6
//              <13=> S0MI (SnMIPS=0)
//              <84=> PCA0 ECI (ECIPS0=1)
//              <100=> PWM0A (C0PPS0=0)
//              <142=> MISO (SPIPS0=0)
//              <166=> KBI6 (KBI6PS0=0)
//              <182=> nINT2 (INT2IS2=0,  INT2IS1=1, INT2IS0=1)
            #define AF_P16  0
//      </e> P1.6 End

//      <e0> P1.7
        #define MG82F6D17_GPIO_P1_7  0
//          <o0> Mode Assignment
//          <i> Specify P1.7mode.
//              <0x80800080=> Analog Input Only (P1M0.7=0,  P1M1.7=1)(Default)
//              <0x80808080=> Open Drain with Pull-up Resistor (P1M0.7=1, P1M1.7=1)
//              <0x80800000=> Open Drain (P1M0.7=0, P1M1.7=0)
//              <0x80808000=> Push Pull (P1M0.7=1, P1M1.7=0)
            #define P17_MODE  0x80800080

//          <o0> Fast Driving Control
//          <i> P1.7 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P1FDC.7=0)(Default)
//              <1=> Enable Fast Driving (P1FDC.7=1)
            #define P1FDC_7  0

//          <o0> Alternate Function
//              <0=> GPIO(Default)
//              <1=> AIN7
//              <10=> RXD0 (S0PS1=1, S0PS0=1)
//              <83=> PCA0 CEX4 (C0PS0=0)
//              <101=> PWM0B (C0PPS0=0)
//              <120=> T0 (T0PS1=1, T0PS0=1)
//              <120=> T0CKO (T0PS1=1, T0PS0=1)
//              <122=> T1 (T1PS1=1, T1PS0=0)
//              <122=> T1CKO (T1PS1=1, T1PS0=0)
//              <140=> nSS (SPIPS0=1)
//              <143=> SPICLK (SPIPS0=0)
//              <167=> KBI7 (KBI6PS0=0)
//              <180=> nINT0 (INT0IS2=1,INT0IS1=1, INT0IS0=0)
            #define AF_P17  0
//      </e> P1.7 End

#define P1_MODE     (P10_MODE | P11_MODE | P15_MODE | P16_MODE | P17_MODE)
#define P1_FDC      (P1FDC_0 | (P1FDC_1<<1) | (P1FDC_5<<5) | (P1FDC_6<<6) | (P1FDC_7<<7))

//      <e0> Driving Strength Control
//      <i> Port 1 driving strength.
        #define P1_DRIVEIN_STRENGTH  0

//          <o0> P14 ~ P17
//              <1=> Low Driving Strength (P1DC1=1)
//              <0=> High Driving Strength (P1DC1=0)(Default)
            #define P1DC_1  0

//          <o0> P13 ~ P10
//              <1=> Low Driving Strength (P1DC0=1)
//              <0=> High Driving Strength (P1DC0=0)(Default)
            #define P1DC_0  0
//      </e>
//  </h> Port1 Config End


//  <h> Port2 Config
//      <e0> P2.2
        #define MG82F6D17_GPIO_P2_2  0

//          <o0> Mode Assignment
//          <i> Specify P2.2 mode.
//              <0x04040004=> Analog Input Only (P2M0.2=0, P2M1.2=1)(Default)
//              <0x04040404=> Open Drain with Pull-up Resistor (P2M0.2=1, P2M1.2=1)
//              <0x04040000=> Open Drain (P2M0.2=0, P2M1.2=0)
//              <0x04040400=> Push Pull (P2M0.2=1, P2M1.2=0)
            #define P22_MODE  0x04040004

//          <o0> Fast Driving Control
//          <i> P2.2 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P2FDC.2=0)(Default)
//              <1=> Enable Fast Driving (P2FDC.2=1)
            #define P2FDC_2  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN2
//              <11=> TXD0 (S0PS1=1, S0PS0=1)
//              <60=> TWI0_SCL (TWIPS1=1, TWIPS0=1)
//              <80=> PCA0 CEX0 (C0PS0=0)
//              <120=> T0 (T0PS1=1, T0PS0=0)
//              <120=> T0CKO (T0PS1=1, T0PS0=0)
//              <162=> KBI2 (KBI2PS0=1)
//              <180=> nINT0 (INT0IS2=1, INT0IS1=1, INT0IS0=1)
            #define AF_P22  0
//      </e> P2.2 End

//      <e0> P2.4
        #define MG82F6D17_GPIO_P2_4  0
//          <o0> Mode Assignment
//          <i> Specify P2.4 mode.
//              <0x10100010=> Analog Input Only (P2M0.4=0, P2M1.4=1)(Default)
//              <0x10101010=> Open Drain with Pull-up Resistor (P2M0.4=1, P2M1.4=1)
//              <0x10100000=> Open Drain (P2M0.2=0, P2M1.2=0)
//              <0x10101000=> Push Pull (P2M0.4=1, P2M1.4=0)
            #define P24_MODE  0x10100010

//          <o0> Fast Driving Control
//          <i> P2.4 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P2FDC.4=0)(Default)
//              <1=> Enable Fast Driving (P2FDC.4=1)
            #define P2FDC_4  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN3
//              <61=> TWI0_SDA (TWIPS1=1, TWIPS0=1)
//              <81=> CEX2 (C0PS0=0)
//              <82=> CEX2 (C0PS0=1)
//              <163=> KBI3 (KBI2PS0=1)
//              <181=> nINT1 (INT1IS2=1, INT1IS1=1, INT1IS0=1)
            #define AF_P24  0
//      </e> P2.4 End

#define P2_MODE     (P22_MODE | P24_MODE)
#define P2_FDC      ((P2FDC_2<<2) | (P2FDC_4<<4))

//      <e0> Driving Strength Control
//      <i> Specify Port 2 driving strength.
        #define P2_DRIVEIN_STRENGTH  0
//          <o0> P27 ~ P24
//              <1=> Low Driving Strength (P2DC1=1)
//              <0=> High Driving Strength (P2DC1=0)(Default)
            #define P2DC_1  0
//          <o0> P23 ~ P20
//              <1=> Low Driving Strength (P2DC0=1)
//              <0=> High Driving Strength (P2DC0=0)(Default)
            #define P2DC_0  0
//      </e>
//  </h> Port2 Config End

//  <h> Port3 Config
//      <e0> P3.0
        #define MG82F6D17_GPIO_P3_0  1
//          <o0> Mode Assignment
//          <i> Specify P3.0 mode.
//              <0x01010001=> Push Pull (P3M0.0=0, P3M1.0=1)
//              <0x01010101=> Open Drain (P3M0.0=1, P3M1.0=1)
//              <0x01010000=> Quasi-Bidirectional (P3M0.0=0, P3M1.0=0)(Default)
//              <0x01010100=> Input Only (P3M0.0=1, P3M1.0=0)
            #define P30_MODE  0x01010101

//          <o0> Fast Driving Control
//          <i> P3.0 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P3FDC.0=0)(Default)
//              <1=> Enable Fast Driving (P3FDC.0=1)
            #define P3FDC_0  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> AIN4
//              <10=> RXD0 (S0PS1=0, S0PS0=0)
//              <11=> TXD0 (S0PS1=1, S0PS0=0)
//              <60=> TWI0_SCL (TWIPS1=1, TWIPS0=0)
//              <61=> TWI0_SDA (TWIPS1=0, TWIPS0=0)
//              <80=> PCA0 CEX0 (C0PS0=1)
//              <104=> PWM6 (C0PPS2=1)
//              <124=> T2 (T2PS1=0, T2PS0=1)
//              <124=> T2CKO (T2PS1=0, T2PS0=1)
//              <162=> KBI2 (KBI2PS0=0)
//              <166=> KBI6 (KBI6PS0=1)
//              <180=> nINT0 (INT0IS2=0, INT0IS1=0, INT0IS0=1)
//              <182=> nINT2 (INT2IS2=0, INT2IS1=0, INT2IS0=1)
            #define AF_P30  61
//      </e> P3.0 End

//      <e0> P3.1
        #define MG82F6D17_GPIO_P3_1  1
//          <o0> Mode Assignment
//          <i> Specify P3.1 mode.
//              <0x02020002=> Push Pull (P3M0.1=0, P3M1.1=1)
//              <0x02020202=> Open Drain (P3M0.1=1, P3M1.1=1)
//              <0x02020000=> Quasi-Bidirectional (P3M0.1=0, P3M1.1=0)(Default)
//              <0x02020200=> Input Only (P3M0.1=1, P3M1.1=0)
            #define P31_MODE  0x02020202

//          <o0> Fast Driving Control
//          <i> P3.1 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P3FDC.1=0)(Default)
//              <1=> Enable Fast Driving (P3FDC.1=1)
            #define P3FDC_1  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <10=> RXD0 (S0PS1=1, S0PS0=0)
//              <11=> TXD0 (S0PS1=0, S0PS0=0)
//              <60=> TWI0_SCL (TWIPS1=0, TWIPS0=0)
//              <61=> TWI0_SDA (TWIPS1=1, TWIPS0=0)
//              <83=> PCA0 CEX4 (C0PS0=1)
//              <125=> T2EX (T2PS1=0, T2PS0=1)
//              <105=> PWM7 (C0PPS2=1)
//              <163=> KBI3 (KBI2PS0=0)
//              <167=> KBI7 (KBI6PS0=1)
//              <181=> nINT1 (INT1IS2=0, INT1IS1=0, INT1IS0=1)
            #define AF_P31  60
//      </e> P3.1 End

//      <e0> P3.3
        #define MG82F6D17_GPIO_P3_3  1
//          <o0> Mode Assignment
//          <i> Specify P3.3 mode.
//              <0x08080008=> Push Pull (P3M0.3=0, P3M1.3=1)
//              <0x08080808=> Open Drain (P3M0.3=1, P3M1.3=1)
//              <0x08080000=> Quasi-Bidirectional (P3M0.3=0, P3M1.3=0)(Default)
//              <0x08080800=> Input Only (P3M0.3=1, P3M1.3=0)
            #define P33_MODE  0x08080808

//          <o0> Fast Driving Control
//          <i> P3.3 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P3FDC.3=0)(Default)
//              <1=> Enable Fast Driving (P3FDC.3=1)
            #define P3FDC_3  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> PCA0 CEX1
//              <12=> S0CKO (S0COPS=1)
//              <13=> S0MI (SnMIPS=1)
//              <85=> PCA0 C0CKO (C0COPS=1)
//              <122=> T1 (T1PS1=1, T1PS0=1)
//              <122=> T1CKO (T1PS1=1, T1PS0=1)
//              <126=> T3
//              <126=> T3CKO
//              <140=> nSS (SPIPS0=0)
//              <143=> SPICLK (SPIPS0=1)
//              <161=> KBI1 (KBI0PS=1)
//              <164=> KBI4 (KBI4PS1=0, KBI4PS0=0)
//              <165=> KBI5 (KBI4PS1=1, KBI4PS0=1)
//              <181=> nINT1 (INT1IS2=0, INT1IS1=0, INT1IS0=0)
            #define AF_P33  181
//      </e> P3.3 End

//      <e0> P3.4
        #define MG82F6D17_GPIO_P3_4  0
//          <o0> Mode Assignment
//          <i> Specify P3.4 mode.
//              <0x10100010=> Push Pull (P3M0.4=0, P3M1.4=1)
//              <0x10101010=> Open Drain (P3M0.4=1, P3M1.4=1)
//              <0x10100000=> Quasi-Bidirectional (P3M0.4=0, P3M1.4=0)(Default)
//              <0x10101000=> Input Only (P3M0.4=1, P3M1.4=0)
            #define P34_MODE  0x10100000

//          <o0> Fast Driving Control
//          <i> P3.4 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P3FDC.4=0)(Default)
//              <1=> Enable Fast Driving (P3FDC.4=1)
            #define P3FDC_4  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <20=> RXD1 (S1PS1=1, S1PS0=1)
//              <86=> PCA0 CEX3
//              <102=> PWM2A (C0PPS1=1)
//              <120=> T0 (T0PS1=0, T0PS0=0)
//              <120=> T0CKO (T0PS1=0, T0PS0=0)
//              <127=> T3EX
//              <130=> T5
//              <142=> MISO (SPIPS0=1)
//              <164=> KBI4 (KBI4PS1=0, KBI4PS0=1)
//              <180=> nINT0 (INT0IS2=0, INT0IS1=1, INT0IS0=0)
//              <181=> nINT1 (INT1IS2=1, INT1IS1=0, INT1IS0=1)
            #define AF_P34  0
//      </e> P3.4 End

//      <e0> P3.5
        #define MG82F6D17_GPIO_P3_5  0
//          <o0> Mode Assignment
//          <i> Specify P3.5 mode.
//              <0x20200020=> Push Pull (P3M0.5=0, P3M1.5=1)
//              <0x20202020=> Open Drain (P3M0.5=1, P3M1.5=1)
//              <0x20200000=> Quasi-Bidirectional (P3M0.5=0, P3M1.5=0)(Default)
//              <0x20202000=> Input Only (P3M0.5=1, P3M1.5=0)
            #define P35_MODE  0x20200000

//          <o0> Fast Driving Control
//          <i> P3.5 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P3FDC.5=0)(Default)
//              <1=> Enable Fast Driving (P3FDC.5=1)
            #define P3FDC_5  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <21=> TXD1 (S1PS1=1, S1PS0=1)
//              <87=> PCA0 CEX5
//              <103=> PWM2B (C0PPS1=1)
//              <122=> T1 (T1PS1=0, T1PS0=0)
//              <122=> T1CKO (T1PS1=0, T1PS0=0)
//              <125=> T2EX (T2PS1=1, T2PS0=0)
//              <132=> T6
//              <141=> MOSI (SPIPS0=1)
//              <165=> KBI5 (KBI4PS1=0, KBI4PS0=1)
//              <181=> nINT1 (INT1IS2=0, INT1IS1=1, INT1IS0=0)
            #define AF_P35  0
//      </e> P3.5 End

#define P3_MODE     (P30_MODE | P31_MODE | P33_MODE | P34_MODE | P35_MODE)
#define P3_FDC      ((P3FDC_0<<0) | (P3FDC_1<<1) | (P3FDC_3<<3) | (P3FDC_4<<4) | (P3FDC_5<<5))

//      <e0> Driving Strength Control
//      <i> Specify Port 3 driving strength.
        #define P3_DRIVEIN_STRENGTH  0
//          <o0> P37 ~ P34
//              <1=> Low Driving Strength (P3DC1=1)
//              <0=> High Driving Strength (P3DC1=0)(Default)
            #define P3DC_1  0
//          <o0> P33 ~ P30
//              <1=> Low Driving Strength (P3DC0=1)
//              <0=> High Driving Strength (P3DC0=0)(Default)
            #define P3DC_0  0
//      </e>
//  </h> Port3 Config End

//  <h> Port4 Config
//      <e0> P4.4
        #define MG82F6D17_GPIO_P4_4  0
//          <o0> Mode Assignment
//          <i> Specify P4.4 mode.
//              <0x00000001=> OCDE (Default)
//              <0x10100010=> Analog Input Only (P4M0.4=0, P4M1.4=1)
//              <0x10101010=> Open Drain with Pull-up Resistor (P4M0.4=1, P4M1.4=1)
//              <0x10100000=> Open Drain (P4M0.4=0, P4M1.4=0)
//              <0x10101000=> Push Pull (P4M0.4=1, P4M1.4=0)
            #define P44_MODE  0x00000001

//          <o0> Fast Driving Control
//          <i> P3.4 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P4FDC.4=0)(Default)
//              <1=> Enable Fast Driving (P4FDC.4=1)
            #define P4FDC_4  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> OCD_SCL
//              <2=> Beeper
//              <10=> RXD0 (S0PS1=0, S0PS0=1)
//              <20=> RXD1 (S1PS1=1, S1PS0=0)
//              <84=> PCA ECI (ECIPS0=0)
//              <120=> T0 (T0PS1=0, T0PS0=1)
//              <120=> T0CKO (T0PS1=0, T0PS0=1)
//              <125=> T2EX (T2PS1=1, T2PS0=1)
//              <182=> nINT2 (INT2IS2=0, INT2IS1=0, INT2IS0=0)
            #define AF_P44  0
//      </e> P4.4 End

//      <e0> P4.5
        #define MG82F6D17_GPIO_P4_5  0
//          <o0> Mode Assignment
//          <i> Specify P4.5 mode.
//              <0x00000001=> OCDE (Default)
//              <0x20200020=> Analog Input Only (P4M0.5=0, P4M1.5=1)
//              <0x20202020=> Open Drain with Pull-up Resistor (P4M0.5=1, P4M1.5=1)
//              <0x20200000=> Open Drain (P4M0.5=0, P4M1.5=0)
//              <0x20202000=> Push Pull (P4M0.5=1, P4M1.5=0)
            #define P45_MODE  0x00000001

//          <o0> Fast Driving Control
//          <i> P3.4 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P4FDC.5=0)(Default)
//              <1=> Enable Fast Driving (P4FDC.5=1)
            #define P4FDC_5  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> OCD_SDA
//              <2=> RTCKO
//              <11=> TXD0 (S0PS1=0, S0PS0=1)
//              <21=> TXD1 (S1PS1=1, S1PS0=0)
//              <122=> T1 (T1PS1=0, T1PS0=1)
//              <122=> T1CKO (T1PS1=0, T1PS0=1)
//              <124=> T2 (T2PS1=1, T2PS0=1)
//              <124=> T2CKO (T2PS1=1, T2PS0=1)
//              <180=> nINT0 (INT0IS2=0, INT0IS1=0, INT0IS0=0)
            #define AF_P45  0
//      </e> P4.5 End

//      <e0> P4.7
        #define MG82F6D17_GPIO_P4_7  0
//          <o0> Mode Assignment
//          <i> Specify P4.7 mode.
//              <0x00000001=> Reset (Default)
//              <0x80800080=> Analog Input Only (P4M0.7=0, P4M1.7=1)
//              <0x80808080=> Open Drain with Pull-up Resistor (P4M0.7=1, P4M1.7=1)
//              <0x80800000=> Open Drain (P4M0.7=0, P4M1.7=0)
//              <0x80808000=> Push Pull (P4M0.7=1, P4M1.7=0)
            #define P47_MODE  0x00000001

//          <o0> Fast Driving Control
//          <i> P4.7 fast/slow driving control set-up.
//              <0=> Disable Fast Driving (P4FDC.7=0)(Default)
//              <1=> Enable Fast Driving (P4FDC.7=1)
            #define P4FDC_7  0

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <12=> S0CKO (S0COPS=0)
//              <22=> S1CKO (S1COPS=0)
//              <23=> S1MI (SnMIPS=1)
//              <85=> PCAC0 C0CKO (C0COPS=0)
//              <160=> KBI0 (KBI0PS=1)
//              <180=> nINT0 (INT0IS2=0, INT0IS1=1, INT0IS0=1)
            #define AF_P47  0
//      </e> P4.7 End

#define P4_MODE         (P44_MODE | P45_MODE | P47_MODE)
#define DCON0_OCDE      (P44_MODE==0x00000001)||(P45_MODE==0x00000001)?1:0
#define DCON0_RSTIO     (P47_MODE==0x00000001)?1:0
#define P4_FDC          ((P4FDC_4<<4) | (P4FDC_5<<5))

//      <e0> Driving Strength Control
//      <i> Specify Port 4 driving strength.
        #define P4_DRIVEIN_STRENGTH  0
//          <o0> P44 ~ P46
//              <1=> Low Driving Strength (P4DC1=1)
//              <0=> High Driving Strength (P4DC1=0)(Default)
            #define P4DC_1  0
//      </e>

//      <e0> GPIO Port Protection
//      <i> Users can activate writing protection of Port4 using this option.
//      <i> Then, only PageP access can modify.
        #define MG82F6D17_GPIO_Port4_Protection  0

//          <o0> Port 4
//          <i> Enable, only PageP access can modify P4.
//          <i> Modifications in Page 0~F are not allowed.
//              <0=> Disable (P4CTL=0)(Default)
//              <1=> Enable (P4CTL=1)
            #define SPCON0_P4CTL  0
//      </e>
//  </h> Port4 Config End

//  <h> Port6 Config
//      <e0> P6.0
        #define MG82F6D17_GPIO_P6_0  0
//          <o0> Mode Assignment
//          <i> Specify P6.0 mode.
//              <0x01010001=> Analog Input Only (P6M0.0=0, P6M1.0=1)(Default)
//              <0x01010101=> Open Drain with Pull-up Resistor (P6M0.0=1, P6M1.0=1)
//              <0x01010000=> Open Drain (P6M0.0=0, P6M1.0=0)
//              <0x01010100=> Push Pull (P6M0.0=1, P6M1.0=0)
            #define P60_MODE  0x01010001

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <1=> ECKI
//              <2=> ICKO
//              <20=> RXD1 (S1PS1=0, S1PS0=1)
//              <60=> TWI0_SCL (TWIPS1=0, TWIPS0=1)
//              <100=> PWM0A (C0PPS0=1)
//              <102=> PWM2A (C0PPS1=0)
//              <104=> PWM6 (C0PPS2=0)
//              <124=> T2 (T2PS1=1, T2PS0=0)
//              <124=> T2CKO (T2PS1=1, T2PS0=0)
//              <164=> KBI4 (KBI4PS1=1, KBI4PS0=0)
//              <180=> nINT0 (INT0IS2=1, INT0IS1=0, INT0IS0=0)
            #define AF_P60  0
//      </e> P6.0 End

//      <e0> P6.1
        #define MG82F6D17_GPIO_P6_1  0
//          <o0> Mode Assignment
//          <i> Specify P6.1 mode.
//              <0x02020002=> Analog Input Only (P6M0.1=0, P6M1.1=1)(Default)
//              <0x02020202=> Open Drain with Pull-up Resistor (P6M0.1=1, P6M1.1=1)
//              <0x02020000=> Open Drain (P6M0.1=0, P6M1.1=0)
//              <0x02020200=> Push Pull (P6M0.1=1, P6M1.1=0)
            #define P61_MODE  0x02020002

//          <o0> Alternate Function
//              <0=> GPIO (Default)
//              <21=> TXD1 (S1PS1=0, S1PS0=1)
//              <22=> S1CKO (S1COPS=1)
//              <23=> S1MI (SnMIPS=0)
//              <61=> TWI0_SDA (TWIPS1=0, TWIPS0=1)
//              <101=> PWM0B (C0PPS0=1)
//              <103=> PWM2B (C0PPS1=0)
//              <105=> PWM7 (C0PPS2=1)
//              <165=> KBI5 (KBI4PS1=1, KBI4PS0=0)
//              <181=> nINT1 (INT1IS2=1, INT1IS1=0, INT1IS0=0)
            #define AF_P61  0
//      </e> P6.1 End

#define P6_MODE   (P60_MODE | P61_MODE)

//      <e0> GPIO Port Protection
//      <i> Users can activate writing protection of Port6 using this option.
//      <i> Then, only PageP access can modify.
        #define MG82F6D17_GPIO_Port6_Protection  0
//          <o0> Port 6
//          <i> Enable, only PageP access can modify P6.
//          <i> Modifications in Page 0~F are not allowed.
//              <0=> Disable (P6CTL=0)(Default)
//              <1=> Enable (P6CTL=1)
            #define     SPCON0_P6CTL  0
//      </e>
//  </h> Port6 Config End
//</e> General Purposes Input Output(GPIO) END


#define WizDef_RXD0         10
#define WizDef_TXD0         11
#define WizDef_S0CKO        12
#define WizDef_S0MI         13
#define WizDef_RXD1         20
#define WizDef_TXD1         21
#define WizDef_S1CKO        22
#define WizDef_S1MI         23

#define WizDef_TWI0_SCL     60
#define WizDef_TWI0_SDA     61

#define WizDef_PCA0_CEX0    80
#define WizDef_PCA0_CEX2_0  81
#define WizDef_PCA0_CEX2_1  82
#define WizDef_PCA0_CEX4    83
#define WizDef_PCA0_ECI     84
#define WizDef_PCA0_C0CKO   85
#define WizDef_PCA0_CEX3    86
#define WizDef_PCA0_CEX5    87

#define WizDef_PWM0A        100
#define WizDef_PWM0B        101
#define WizDef_PWM2A        102
#define WizDef_PWM2B        103
#define WizDef_PWM6         104
#define WizDef_PWM7         105

#define WizDef_T0           120
#define WizDef_T0CKO        120
#define WizDef_T0EX         121
#define WizDef_T1           122
#define WizDef_T1CKO        122
#define WizDef_T1EX         123
#define WizDef_T2           124
#define WizDef_T2CKO        124
#define WizDef_T2EX         125
#define WizDef_T3           126
#define WizDef_T3CKO        126
#define WizDef_T3EX         127
#define WizDef_T4           128
#define WizDef_T4CKO        128
#define WizDef_T4EX         129
#define WizDef_T5           130
#define WizDef_T5CKO        130
#define WizDef_T5EX         131
#define WizDef_T6           132
#define WizDef_T6CKO        132
#define WizDef_T6EX         133

#define WizDef_nSS          140
#define WizDef_MOSI         141
#define WizDef_MISO         142
#define WizDef_SPICLK       143

#define WizDef_KBI0         160
#define WizDef_KBI1         161
#define WizDef_KBI2         162
#define WizDef_KBI3         163
#define WizDef_KBI4         164
#define WizDef_KBI5         165
#define WizDef_KBI6         166
#define WizDef_KBI7         167

#define WizDef_nINT0        180
#define WizDef_nINT1        181
#define WizDef_nINT2        182

/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.05_Brian_20200327 //bugNum_Authour_Date
 * >> Divid MG82F6D17_CLK_DRV.h to 3 files
 * >>   MG82F6D17_CLK_Wizard.c : Wizard Table , Init Function
 * >>   2020/04/01 Release
 * #0.06_Brian_20200507 //bugNum_Authour_Date
 * >> Add void function for minus initial function code size
 * >>   Replace __DRV_CLK_SystemClock_Wizard_Init > DRV_CLK_SystemClock_Wizard_Init()
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize System Clock
//<i> Check this term, then will initialize system clock.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_CLK_WIZARD 1

//  <o0> Select Config Mode
//  <i> This only changes the parameters of "__DRV_CLK_SystemClock_Wizard_Init();"
//  <i> System clock 50MHz max. CPU clock 36MHz max.
//      <1=> Easy Mode
//      <2=> Advanced Mode
    #define DRV_CLK_Wizard_Mode 1

//  <e0> Easy Mode
//  <i> The setting only change the parameter of "__DRV_CLK_SystemClock_Wizard_Init ();"
    #define DRV_CLK_Wizard_Easy_Check 1

//      <o0>  Select System Clock and CPU Clock
//      <i> Easy select clock by "__DRV_CLK_Easy_Select(__SELECT__);"
//          <0x80001010=> SYSCLK=CPUCLK=IHRCO=12MHz (Default)
//          <0x80001090=> SYSCLK=CPUCLK=IHRCO=11.059MHz 
//          <0x80000310=> SYSCLK=CPUCLK=ECKI (External clock input (P6.0) as OSCin.)
//          <0x00000210=> SYSCLK=CPUCLK=ILRCO=32KHz
//          <0x80001450=> SYSCLK=CPUCLK=24MHz
//          <0x800014D0=> SYSCLK=CPUCLK=22.118MHz
//          <0x80001858=> SYSCLK=32MHz, CPUCLK=16MHz
//          <0x80001C58=> SYSCLK=48MHz, CPUCLK=24MHz
//          <0x800018D8=> SYSCLK=29.491MHz, CPUCLK=14.736MHz
//          <0x80001CD8=> SYSCLK=44.236MHz, CPUCLK=22.118MHz
        #define DRV_CLK_Easy_Config 0x80001C58
//  </e> Easy Mode

//  <e0> Advanced Mode
//  <i> The setting only change the parameter of "__DRV_CLK_SystemClock_Wizard_Init ();"
    #define DRV_CLK_Wizard_Advanced_Check 0

//      <h> OSCin Clock Source
//          <e0.12> Enable IHRCO
//          <i> Check: Enable IHRCO by "__DRV_CLK_IHRCO_12MHz();" or "__DRV_CLK_IHRCO_11M059KHz();"
//          <i> Uncheck: Disable IHRCO by "__DRV_CLK_IHRCO_Disable();"
//          <i> Select IHRCO by "__DRV_CLK_IHRCO_Select(__SELECT__);"
//          <i> DT reference: CKCON2.4(IHRCOE)
//              <o0.7> IHRCO Frequency Select
//                  <0=> IHRCO = 12MHz (AFS=0)(Default)
//                  <1=> IHRCO = 11.0592MHz (AFS=1)
//          </e>
//          <o0.8..9> OSCin Clock Source Select
//          <i> Select OSCin by "__DRV_CLK_OSCin_Select(__SELECT__);"
//              <0=> OSCin = IHRCO (OSCS1=0, OSCS0=0)(Default)
//              <2=> OSCin = ILRCO (OSCS1=1, OSCS0=0)(32KHz)
//              <3=> OSCin = ECKI: External clock input (P6.0)(OSCS1=1, OSCS0=1)
//      </h> OSCin Clock Source END

//      <e0.6> Enable Clock Multiplier (PLL)
//      <i> Enable/Disable by "__DRV_CLK_PLL_Cmd(__STATE__);"
//      <i> DT reference: CKCON0.6(ENCKM)
//          <o0.4..5> Multiplier Clock Input (5~6.5MHZ)
//          <i> Config PLL by call "__DRV_CLK_PLL_Config(__SELECT__, __MODE__);"
//              <0=> OSCin = 5~6.5MHz (CKMIS1=0, CKMIS0=0)
//              <1=> OSCin = 10~13MHz (CKMIS1=0, CKMIS0=1)(Default)
//              <2=> OSCin = 20~26MHz (CKMIS1=1, CKMIS0=0)
//          <o0.24> Clock Multiplier Mode
//          <i> Config PLL by call "__DRV_CLK_PLL_Config(__SELECT__, __MODE__);"
//              <0=> Select CKM operating for 16X mode. (96MHz)(CKMS0=0)(Default)
//              <1=> Select CKM operating for 24X mode. (144MHz)(CKMS1=0)
//          <o0.10..11> Multiplier Clock Output Select. (MCK)
//          <i> Select MCK by "__DRV_CLK_MCK_Select(__SELECT__);"
//              <0=> MCK = OSCin (MCKS1=0, MCKS0=0)(Default)
//              <1=> MCK = 24MHz, To PCA = 96MHz (MCKS1=0, MCKS0=1)
//              <2=> MCK = 32MHz, To PCA = 96MHz (MCKS1=1, MCKS0=0)
//              <3=> MCK = 48MHz, To PCA = 96MHz (MCKS1=1, MCKS0=1)
//              <1=> MCK = 36MHz, To PCA = 144MHz (MCKS1=0, MCKS0=1)
//              <2=> MCK = 48MHz, To PCA = 144MHz (MCKS1=1, MCKS0=0)
//              <3=> MCK = 72MHz, To PCA = 144MHz (MCKS1=1, MCKS0=1)
//              <1=> MCK = 22.118MHz, To PCA = 88.4736MHz (MCKS1=0, MCKS0=1)
//              <2=> MCK = 29.491MHz, To PCA = 88.4736MHz (MCKS1=1, MCKS0=0)
//              <3=> MCK = 44.236MHz, To PCA = 88.4736MHz (MCKS1=1, MCKS0=1)
//              <1=> MCK = 33.177MHz, To PCA = 132.7104MHz (MCKS1=0, MCKS0=1)
//              <2=> MCK = 44.236MHz, To PCA = 132.7104MHz (MCKS1=1, MCKS0=0)
//              <3=> MCK = 66.354MHz, To PCA = 132.7104MHz (MCKS1=1, MCKS0=1)
//      </e> Enable Clock Multiplier (PLL)

//      <o0.18..19> MCK Divider Output Select. (MCKDO)
//      <i> Select MCKDO by "__DRV_CLK_MCKDO_Select(__SELECT__);"
//          <0=> MCKDO = MCK/1 (MCKDS1=0, MCKDS0=0)(Default)
//          <1=> MCKDO = MCK/2 (MCKDS1=0, MCKDS0=1)
//          <2=> MCKDO = MCK/4 (MCKDS1=1, MCKDS0=0)
//          <3=> MCKDO = MCK/8 (MCKDS1=1, MCKDS0=1)

//      <o0.0..2> System Clock Select. (SYSCLK)(50MHz Max.)
//      <i> Select SYSCLK by "__DRV_CLK_SYSCLK_Select(__SELECT__);"
//          <0=> SYSCLK = MCKDO/1 (SCKS2=0, SCKS1=0, SCKS0=0)(Default)
//          <1=> SYSCLK = MCKDO/2 (SCKS2=0, SCKS1=0, SCKS0=1)
//          <2=> SYSCLK = MCKDO/4 (SCKS2=0, SCKS1=1, SCKS0=0)
//          <3=> SYSCLK = MCKDO/8 (SCKS2=0, SCKS1=1, SCKS0=1)
//          <4=> SYSCLK = MCKDO/16 (SCKS2=1, SCKS1=0, SCKS0=0)
//          <5=> SYSCLK = MCKDO/32 (SCKS2=1, SCKS1=0, SCKS0=1)
//          <6=> SYSCLK = MCKDO/64 (SCKS2=1, SCKS1=1, SCKS0=0)
//          <7=> SYSCLK = MCKDO/128 (SCKS2=1, SCKS1=1, SCKS0=1)

//      <o0.3> CPU Clock Select. (CPUCLK)(36MHz Max.)
//      <i> Select CPUCLK by "__DRV_CLK_CPUCLK_Select(__SELECT__);"
//          <0=> CPUCLK = SYSCLK/1 (CCKS=0)(Default)
//          <1=> CPUCLK = SYSCLK/2 (CCKS=1)

//      <o0.29..31> CPU Clock Range Select (0~36MHz)
//      <i> Select CPUCLK range "__DRV_CLK_CPUCLK_Range_Select(__SELECT__);"
//          <5=> CPUCLK is faster than 25MHz (HSE=1, HSE1=1)
//          <4=> CPUCLK is between 25MHz~6MHz (HSE=1, HSE1=0)(Default)
//          <0=> CPUCLK equal or slower than 6MHz (HSE=0, HSE1=0)

//      <o0.21> MCU Power Down Wakeup Select.
//      <i> Select CPUCLK range "__DRV_CLK_Wakeup_Select(__SELECT__);"
//          <0=> Power down normal wakeup. (120us)(FWKP=0)(Default)
//          <1=> Power down fast wakeup. (30us)(FWKP=1)
        #define DRV_CLK_Advanced_Config 0x80001010
//  </e>    Advanced Mode End

//<o0.6..7> Initialize P60 Mux Function Select
//<i> Only change the parameter of "__DRV_CLK_P60Mux_Wizard_Init();"
//<i> Select P60 Function by "__DRV_CLK_P60Mux_Select(__SELECT__);"
//<i> Enable P60 Fast Driving by "__DRV_CLK_P60FastDrive_Cmd(__STATE__);""
//  <0=> P60 is normal GPIO (P60OC1=0, P60OC0=0)(Default)
//  <1=> P60 = MCK/1 output (P60OC1=0, P60OC0=1)
//  <2=> P60 = MCK/2 output (P60OC1=1, P60OC0=0)
//  <3=> P60 = MCK/4 output (P60OC1=1, P60OC0=1)
#define DRV_CLK_Wizard_P60_Select 0x00

//</e> Initialize System Clock End

#if (MG82F6D17_CLK_WIZARD==1)
    #if (DRV_CLK_Wizard_Mode==1)
        #if (DRV_CLK_Wizard_Easy_Check==1)
            #define Wizard_SysClk_Select  DRV_CLK_Easy_Config
        #else
            //#error "Initialize System Clock error: Please check box at Easy Mode."
            //#define Wizard_SysClk_Select  SYS_12M_CPU_12M
            #undef Wizard_SysClk_Select
        #endif
    #elif (DRV_CLK_Wizard_Mode==2)
        #if (DRV_CLK_Wizard_Advanced_Check==1)
            #define Wizard_SysClk_Select  DRV_CLK_Advanced_Config
        #else
            //#error "Initialize System Clock error: Please check box at Advanced Mode."
            //#define Wizard_SysClk_Select  SYS_12M_CPU_12M
            #undef Wizard_SysClk_Select
        #endif
    #endif
#endif

//@del{
/*
BYTE[3] = u32B3(__SELECT__) = DCON0.7.5
BYTE[3] = u32B3(__SELECT__) = CKCON5.0
BYTE[2] = u32B2(__SELECT__) = CKCON3
BYTE[1] = u32B1(__SELECT__) = CKCON2
BYTE[0] = u32B0(__SELECT__) = CKCON0

CKCON5.0: CKMS0

CKCON3: WDTCS[1:0], FWKP, WDTFS, MCKD[1:0]
    MCKD[1:0]: MCKDO = MCK/1, /2, /4, /8

CKCON2: IHRCOE, MCKS[1:0], OSCS[1:0]
    MCKS[1:0]: PLL/OSCin output to MCK Source selection. (OSCin/PLL...etc)
    OSCS[1:0]: OSCin Source selection.

CKCON0: AFS, ENCKM, CKMIS[1:0], CCKS, SCKS[2:0]
    CKMIS[1:0]: PLL Input Clock divider
*/
//#define Wizard_DCON0_P      (u32B3(Wizard_SysClk_Select) & (HSE_P|HSE1_P) )   //P
//#define Wizard_CKCON5_P     (u32B3(Wizard_SysClk_Select) & CKMS0_P )   //P
//#define Wizard_CKCON3_P     (u32B2(Wizard_SysClk_Select) & (FWKP_P|MCKD1_P|MCKD0_P))   //P
//@del}
#define Wizard_DCON0_P      u32B3(Wizard_SysClk_Select) //P
#define Wizard_CKCON5_P     u32B3(Wizard_SysClk_Select) //P
#define Wizard_CKCON3_P     u32B2(Wizard_SysClk_Select) //P
#define Wizard_CKCON2_P     u32B1(Wizard_SysClk_Select) //P
#define Wizard_CKCON0       u32B0(Wizard_SysClk_Select) // 0~F & P

#define Wizard_DCON0_HSE    (Wizard_SysClk_Select & ((HSE_P|HSE1_P)<<24))
#define Wizard_CKCON5_CKMS  (Wizard_SysClk_Select & (CKMS0_P<<24))

#define Wizard_CKCON3_SEL   (Wizard_SysClk_Select & ((FWKP_P|MCKD1_P|MCKD0_P)<<16))

#define Wizard_CKCON2_OSCS  (Wizard_SysClk_Select & ((OSCS1_P|OSCS0_P)<<8))
#define Wizard_CKCON2_MCKS  (Wizard_SysClk_Select & ((MCKS1_P|MCKS0_P)<<8))
#define Wizard_CKCON2_SEL   (Wizard_SysClk_Select & ((IHRCOE_P|MCKS1_P|MCKS0_P|OSCS1_P|OSCS0_P)<<8))

#define Wizard_CKCON0_AFS   (Wizard_SysClk_Select & AFS)
#define Wizard_CKCON0_ENCKM (Wizard_SysClk_Select & ENCKM)
#define Wizard_CKCON0_CKMIS (Wizard_SysClk_Select & (CKMIS1|CKMIS0))
#define Wizard_CKCON0_SEL   (Wizard_SysClk_Select & (CCKS|SCKS2|SCKS1|SCKS0))

#define Wizard_IHRCO_Check  (Wizard_SysClk_Select & ((IHRCOE_P|OSCS1_P|OSCS0_P)<<8))

/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * >> Add DMA initial void function for code size.
 * #1.01_Kevin_20200901 //bugNum_Authour_Date
 * >> Cancel Timer5/Timer6 Clock & Gating Source Select item
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Direct Memory Access Controller (DMA)
//<i> Check this term, then will initialize DMA Controller.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_DMA_WIZARD 0

//      <O0> DMA Source Selection
//      <i> Select Data Source by "__DRV_DMA_Source_Select(__SELECT__);"
//          <0x00=> Disable (DSS30~00=0000)(Default)
//          <0x10=> S0 RX (DSS30~00=0001)
//          <0x20=> S1 RX (DSS30~00=0010)
//          <0x50=> I2C0 RX (DSS30~00=0101)
//          <0x70=> SPI0 RX (DSS30~00=0111)
//          <0x90=> ADC0 (DSS30~00=1001)
//          <0xF0=> XRAM (DSS30~00=1111)
            #define DRV_DMA_Souece_Select 0x00

//      <O0> DMA Destination Selection
//      <i> Select Data Destination by "__DRV_DMA_Destination_Select(__SELECT__);"
//          <0x00=> Disable (DDS30~00=0000)(Default)
//          <0x01=> S0 TX (DDS30~00=0001)
//          <0x02=> S1 TX (DDS30~00=0010)
//          <0x05=> I2C0 TX (DDS30~00=0101)
//          <0x07=> SPI0 TX (DDS30~00=0111)
//          <0x0D=> CRC (DDS30~00=1101)
//          <0x0F=> XRAM (DDS30~00=1111)
            #define DRV_DMA_Destination_Select 0x00

//      <O0> DMA External_Trigger_Source Selection
//      <i> Select Data Destination by "__DRV_DMA_ExternalTriggerSource_Select(__SELECT__);"
//          <0x00=> Disable, software trigger (EXTS10=0, EXTS00=0)(Default)
//          <0x04=> INT2ET (EXTS10=0, EXTS00=1)
//          <0x0C=> KBIET (EXTS10=1, EXTS00=1)
            #define DRV_DMA_External_TriggerSource_Select 0x00


//      <q0> LOOP Mode Operation (LOOP0)
//      <i> The setting only change the parameter of "__DRV_DMA_LoopMode_Select(__SELECT__);"
            #define DRV_DMA_LoopMode_En 0


//      <q0> CRC16 Write Operation (CRCW0)
//      <i> The setting only change the parameter of "__DRV_DMA_CRC16_Write_Select(__SELECT__);"
            #define DRV_DMA_CRC16_Write_En 0


//  <O0> XRAM Data Length <0x0001-0x02ff>
//  <i> The setting only change the parameter of "__DRV_DMA_SetDataLength(__SELECT__);"
//  <i> The timer5 counter be use to data length counter while DMA source or destination select to XRAM
//  <i> Setting value is limited by XRAM size "0x0001-0x02ff"
            #define DRV_DMA_XRAM_Data_Length 0x0001


//  <O0> XRAM Address Pointer <0x0000-0x02ff>
//  <i> The setting only change the parameter of "__DRV_DMA_SetXRAMAddressPointer(__SELECT__);"
//  <i> The timer6 counter be use to address pointer while DMA source or destination select to XRAM
//  <i> Setting value is limited by XRAM address "0x0000-0x02ff" 
            #define DRV_DMA_XRAM_Address_Pointer 0x0000



//</e> Initialize Direct Memory Access Controller End
/// @endcond __DRV_Wizard_Without_Doxygen

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Watch Dog Timer(WDT)
//<i> Check this term, then DRV_WDT_Wizard_Init() will initialize WDT.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_WDT_WIZARD      0

//  <o0> WDT Clock Source Selection
//  <i> Select WDT clock source.
//  <0=>ILRCO (Default) <1=>P60 <2=>SYSCLK/12 <3=>S0TOF
        #define CKCON3_WDTCS    0

//  <o0> Non-Stopped WDT
//  <0=> Disable (Default) <1=> Enable
//  <i> This enables WDT to keep counting in power-down or idle mode.
        #define WDTCR_NSW       0

//  <o0> WDT Idle Control
//  <0=> Disable (Default) <1=> Enable
//  <i> This enables WDT to keep counting in the idle mode.
        #define WDTCR_WIDL      0

//  <o0> Select Prescaler Output for WDT
//  <i> Determine WDT time base input.
//  <0=> 1/2 <1=> 1/4 <2=> 1/8 <3=> 1/16 <4=> 1/32 <5=> 1/64 <6=> 1/128 <7=> 1/256 (Default)
        #define PreScaler       7

//  <o0> WDT Overflow Source Selection
//  <i> Determine WDT event source.
//  <0=> Select WDT bit-8 overflow as WDT event source (Default) <1=> Select WDT bit-0 overflow as WDT event source
        #define CKCON3_WDTFS    0

//  <o0> WDT Reset Enable
//  <i> Enable/Disable WDT reset.
//  <0=> Disable (Default) <1=> Enable
        #define WDTCR_WREN      0
//</e> Watch Dog Timer(WDT) End
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Real Time Clock(RTC)
//<i> Check this term, then macro DRV_RTC_Wizard_Init() will initialize RTC.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_RTC_WIZARD    0

//  <o0> RTC Clock Source Selection
//  <i> Select RTC clock source.
//  <0=> P60 (Default)  <1=> ILRCO
//  <2=> WDTPS          <3=> WDTOF
//  <4=> SYSCLK         <5=> SYSCLK/12
        #define CKCON4_RCSS      0

//  <o0> RTC Prescaler
//  <i> Determine RTC time base input.
//  <0=> DIV 2^15     <1=> DIV 2^14 (Default)  <2=> DIV 2^13   <3=> DIV 2^12
//  <4=> DIV 2^11     <5=> DIV 2^10   <6=> DIV 2^9    <7=> DIV 2^8
//  <8=> DIV 2^7      <9=> DIV 2^6    <10=> DIV 2^5   <11=> DIV 2^4
//  <12=> DIV 2^3     <13=> DIV 2^2   <14=> DIV 2^1   <15=> DIV 2^0
        #define CKCON4_RTCCS    1

//  <o0> RTC Output enable
//  <0=> Disable (Default) <1=> Enable
//  <i> Select P45 as RTC output. The frequency of RTCO will be a half of RTC overflow rate.
        #define RTCCR_RTCO      0

//  <o0> RTC counter reload value (0~63) <0-63>
//  <i> Assign a number to the RTC reloader, which determines the time period after reloading.
        #define RTCCR_RTCRL     0
//</e> Real Time Clock(RTC) END
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.05_Xian_20200422 //bugNum_Authour_Date
 * #0.07_Brian_20200527 //bugNum_Authour_Date
 * >> Wizard add power management
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Power Management
//<i> Check this term, then DRV_PW_BODx_Wizard_Init() will initialize power control.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_PW_WIZARD 0

//  <e0> Initialize Brown-Out Detector(BODx)
//  <i> Check this term, then macro __DRV_BODx_Wizard_Init() will initialize BODx.
    #define MG82F6D17_BODx_WIZARD   0

//      <h> Initialize BOD0
//      <i> Enable/Disable BOD0 Reset.
//          <o0> BOD0 Reset
//              <0=> Disable (Default)
//              <1=> Enable
            #define PCON2_BO0RE     0
//      </h> Initialize BOD0 END

//      <h> Initialize BOD1
//      <i> Enable/Disable BOD1, specify BOD1 monitored level, awake BOD1 in PD mode, enable/disable BOD1 reset.
//          <o0> BOD1 Enable
//          <i>Enable BOD1 that monitors VDD power dropped at a BO1S specified voltage level.
//              <0=> Disable
//              <1=> Enable (Default)
            #define PCON2_EBOD1     1

//          <o0> Brown-Out Detector 1 Level
//          <i> Brown-out detector 1 monitored level selection.
//              <0=> 2.0V
//              <1=> 2.4V
//              <2=> 3.7V
//              <3=> 4.2V(Default)
            #define PCON2_BO1S       3

//          <o0> Awake BOD1 in PD mode
//              <0=> Disable (Default)
//              <1=> Enable
            #define PCON2_AWBOD1     0

//          <o0> BOD1 Reset
//          <i> Enable/Disable BOD1 Reset.
//              <0=> Disable (Default)
//              <1=> Enable
            #define PCON2_BO1RE      0
//      </h> Initialize BOD1 END

//  </e> Brown-Out Detector(BODx) END

//</e> Initialize Power Management
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 * #1.01_Brian_20200903 //bugNum_Authour_Date
 * >> Delete Easy Mode wizard table
 * >> Delete description: DRV_Timers_Counters_Wizard_Init()
 * >> Delete TIMER_Option, TIMER_Advance_Mode
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0>Initialize Timers/Counters
//<i> Check this term, then will initialize Timers/Counters.
//<i> Date: 2020/09/03     Version: 1.01
#define MG82F6D17_TIMER_WIZARD 1

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  <e>Timer0 Enable
//  <i>Mode0 8bit pwm.
//  <i>Mode1 16 bit timer/counter.
//  <i>Mode2 8bit timer/counter auto-reload.
//  <i>Mode3 two separate timer/counter.
#define TM0_Wizard_init_EN 1
//      <o>Mode
//      <i> Mode0~mode3 select
//          <0x00=>Mode 0 8 Bit PWM (T0M1=0,T0M0=0)(Default)
//          <0x01=>Mode 1 16 Bit Timer/Counter (T0M1=0,T0M0=1)
//          <0x02=>Mode 2 8Bit Timer/Counter Auto Reload (T0M1=1,T0M0=0)
//          <0x03=>Mode 3 Two Separate Timer/Counter (T0M1=1,T0M0=1)
#define TM0_Wizard_Mode 0x01

//===========================================================
//      <e>Mode0 8 Bit PWM Enable
//      <i>Mode0 8 bit pwm enable.
#define TIMER0_Mode0_8Bit_PWM 0
//===========================================================
//      <o>Clock Output <0=>Disable (T0CKOE=0)(Default) <1=>Enable (T0CKOE=1)
//      <i>Note:T0CKOE=1,gpio wizard p3.4 or p4.4 or p2.2 or p1.7 select T0CKO.
#define TIMER0_Mode0_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T0XL=0,T0X12=0,T0C_T=0)(Default)
//          <0x01=> T0 Pin (T0XL=0,T0X12=0,T0C_T=1)
//          <0x02=> SYSCLK (T0XL=0,T0X12=1,T0C_T=0)
//          <0x03=> ILRCO (T0XL=0,T0X12=1,T0C_T=1)
//          <0x04=> SYSCLK/48 (T0XL=1,T0X12=0,T0C_T=0)
//          <0x05=> WDTPS (T0XL=1,T0X12=0,T0C_T=1)
//          <0x06=> SYSCLK/192 (T0XL=1,T0X12=1,T0C_T=0)
//          <0x07=> T1OF (T0XL=1,T0X12=1,T0C_T=1)
#define TIMER0_Mode0_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T0G1=0,T0GATE=0)(Default)
//          <0x01=> INT0ET (T0G1=0,T0GATE=1)
//          <0x02=> TF2 (T0G1=1,T0GATE=0)
//          <0x03=> KBIET (T0G1=1,T0GATE=1)
#define TIMER0_Mode0_Gating_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
#define TIMER0_Mode0_Timer_Interval_High_Byte 0
//      <i>Duty cucle:1/256~256/256(timer interval low byte=0,timer interval high byte=255~0).
//      <o>Timer Interval Low Byte (0~255) <0-255>
#define TIMER0_Mode0_Timer_Interval_Low_Byte 0
//      <i>Example:
//      <i>Timer interval low byte=0(Total interval=255-0+1=256count,aviliable on first cycle).
//      <i>Timer interval high byte=1(high level interval=255count(256-1=255),low level interval=1count).
//      <o>8Bit Timer/Counter Run <0=>Disable (TR0=0)(Default) <1=>Enable (TR0=1)
#define TIMER0_Mode0_8Bit_Timer_Counter_Run 0
//      </e>  //Mode0 8 Bit PWM Enable

//===========================================================
//      <e>Mode1 16 Bit Timer/Counter Enable
//      <i>Mode1 16 bit timer/counter enable.
#define TIMER0_Mode1_16Bit_Timer_Counter 1
//===========================================================
//      <o>Clock Output <0=>Disable (T0CKOE=0)(Default) <1=>Enable (T0CKOE=1)
//      <i>Note:T0CKOE=1,gpio wizard p3.4 or p4.4 or p2.2 or p1.7 select T0CKO.
#define TIMER0_Mode1_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T0XL=0,T0X12=0,T0C_T=0)(Default)
//          <0x01=> T0 Pin (T0XL=0,T0X12=0,T0C_T=1)
//          <0x02=> SYSCLK (T0XL=0,T0X12=1,T0C_T=0)
//          <0x03=> ILRCO (T0XL=0,T0X12=1,T0C_T=1)
//          <0x04=> SYSCLK/48 (T0XL=1,T0X12=0,T0C_T=0)
//          <0x05=> WDTPS (T0XL=1,T0X12=0,T0C_T=1)
//          <0x06=> SYSCLK/192 (T0XL=1,T0X12=1,T0C_T=0)
//          <0x07=> T1OF (T0XL=1,T0X12=1,T0C_T=1)
#define TIMER0_Mode1_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T0G1=0,T0GATE=0)(Default)
//          <0x01=> INT0ET (T0G1=0,T0GATE=1)
//          <0x02=> TF2 (T0G1=1,T0GATE=0)
//          <0x03=> KBIET (T0G1=1,T0GATE=1)
#define TIMER0_Mode1_Gating_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//      <i>Example:
//      <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER0_Mode1_Timer_Interval 61535
//      <o>16Bit Timer/Counter Run <0=>Disable (TR0=0)(Default) <1=>Enable (TR0=1)
#define TIMER0_Mode0_16Bit_Timer_Counter_Run 1
//      </e>  //Mode0 16 Bit Timer/Counter Enable

//===========================================================
//      <e> Mode2 8Bit Timer/Counter Auto Reload Enable
//      <i>Mode2 8bit timer/counter auto reload enable.
#define TIMER0_Mode2_8Bit_Timer_Counter_Auto_Reload 0
//===========================================================
//      <o>Clock Output <0=>Disable (T0CKOE=0)(Default) <1=>Enable (T0CKOE=1)
//      <i>Note:T0CKOE=1,gpio wizard p3.4 or p4.4 or p2.2 or p1.7 select T0CKO.
#define TIMER0_Mode2_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T0XL=0,T0X12=0,T0C_T=0)(Default)
//          <0x01=> T0 Pin (T0XL=0,T0X12=0,T0C_T=1)
//          <0x02=> SYSCLK (T0XL=0,T0X12=1,T0C_T=0)
//          <0x03=> ILRCO (T0XL=0,T0X12=1,T0C_T=1)
//          <0x04=> SYSCLK/48 (T0XL=1,T0X12=0,T0C_T=0)
//          <0x05=> WDTPS (T0XL=1,T0X12=0,T0C_T=1)
//          <0x06=> SYSCLK/192 (T0XL=1,T0X12=1,T0C_T=0)
//          <0x07=> T1OF (T0XL=1,T0X12=1,T0C_T=1)
#define TIMER0_Mode2_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T0G1=0,T0GATE=0)(Default)
//          <0x01=> INT0ET (T0G1=0,T0GATE=1)
//          <0x02=> TF2 (T0G1=1,T0GATE=0)
//          <0x03=> KBIET (T0G1=1,T0GATE=1)
#define TIMER0_Mode2_Gating_Source 0x00
//      <o>Timer Interval (0~255) <0-255>
#define TIMER0_Mode2_Timer_Interval 0
//      <o>8Bit Timer/Counter Run <0=>Disable (TR0=0)(Default) <1=>Enable (TR0=1)
#define TIMER0_Mode2_8Bit_Timer_Counter_Run 0
//      </e>  //Mode2 8Bit Timer/Counter Auto Reload Enable

//===========================================================
//      <e>Mode 3 Two Separate Timer/Counter Enable
//      <i>**Note:when timer0 in mode3,Timer 1 is halt.
#define TIMER0_Mode3_Two_Separate_Timer_Counter 0
//===========================================================
//      <o>Clock Output <0=>Disable (T0CKOE=0)(Default) <1=>Enable (T0CKOE=1)
//      <i>Note:T0CKOE=1,gpio wizard p3.4 or p4.4 or p2.2 or p1.7 select T0CKO.
#define TIMER0_Mode3_T0CKO_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T0XL=0,T0X12=0,T0C_T=0)(Default)
//          <0x01=> T0 Pin (T0XL=0,T0X12=0,T0C_T=1)
//          <0x02=> SYSCLK (T0XL=0,T0X12=1,T0C_T=0)
//          <0x03=> ILRCO (T0XL=0,T0X12=1,T0C_T=1)
//          <0x04=> SYSCLK/48 (T0XL=1,T0X12=0,T0C_T=0)
//          <0x05=> WDTPS (T0XL=1,T0X12=0,T0C_T=1)
//          <0x06=> SYSCLK/192 (T0XL=1,T0X12=1,T0C_T=0)
//          <0x07=> T1OF (T0XL=1,T0X12=1,T0C_T=1)
#define TIMER0_Mode3_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T0G1=0,T0GATE=0)(Default)
//          <0x01=> INT0ET (T0G1=0,T0GATE=1)
//          <0x02=> TF2 (T0G1=1,T0GATE=0)
//          <0x03=> KBIET (T0G1=1,T0GATE=1)
#define TIMER0_Mode3_Gating_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
#define TIMER0_Mode3_Timer_Interval_High_Byte 0
//      <o>Timer Interval Low Byte (0~255) <0-255>
#define TIMER0_Mode3_Timer_Interval_Low_Byte 0
//      <o>8Bit Timer/Counter TL0 Run <0=>Disable (TR0=0)(Default) <1=>Enable (TR0=1)
#define TIMER0_Mode3_8Bit_Timer_Counter_TL0_Run 0
//      <o>8Bit Timer/Counter TH0 Run <0=>Disable (TR1=0)(Default) <1=>Enable (TR1=1)
#define TIMER0_Mode3_8Bit_Timer_Counter_TH0_Run 0
//      </e>  //Mode 3 Two Separate Timer/Counter Enable
//  </e>  //Timer0 Enable

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  <e>Timer1 Enable
//  <i>Mode0 8 bit pwm ready.
//  <i>Mode1 16 bit timer/counter.
//  <i>Mode2 8bit timer/counter auto-reload.
#define TM1_Wizard_init_EN 1
//      <o>Mode
//      <i> Mode0~mode2 select
//          <0x00=>Mode 0 8 Bit PWM (T1M1=0,T1M0=0)(Default)
//          <0x01=>Mode 1 16 Bit Timer/Counter (T1M1=0,T1M0=1)
//          <0x02=>Mode 2 8Bit Timer/Counter Auto Reload (T1M1=1,T1M0=0)
#define TIMER1_Mode  0x01

//===========================================================
//      <e>Mode0 8 Bit PWM Enable
//      <i>Mode0 8 bit pwm enable.
#define TIMER1_Mode0_8Bit_PWM 0
//===========================================================
//      <o>Clock Output <0=>Disable (T1CKOE=0)(Default) <1=>Enable (T1CKOE=1)
//      <i>Note:T1CKOE=1,gpio wizard p3.5 or p4.5 or p1.7 or p3.3 select T1CKO.
#define TIMER1_Mode0_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T1X12=0,T1C_T=0)(Default)
//          <0x01=> T1 Pin (T1X12=0,T1C_T=1)
//          <0x02=> SYSCLK (T1X12=1,T1C_T=0)
//          <0x03=> SYSCLK/48 (T1X12=1,T1C_T=1)
#define TIMER1_Mode0_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T1G1=0,T1GATE=0)(Default)
//          <0x01=> INT1ET (T1G1=0,T1GATE=1)
//          <0x02=> TF3 (T1G1=1,T1GATE=0)
//          <0x03=> TI1 (T1G1=1,T1GATE=1)
#define TIMER1_Mode0_Gating_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//      <i>Duty cucle:1/256~256/256(Timer interval low byte=0,Timer interval high byte=255~0).
#define TIMER1_Mode0_Timer_Interval_High_Byte 0
//      <o>Timer Interval Low Byte (0~255) <0-255>
#define TIMER1_Mode0_Timer_Interval_Low_Byte 0
//      <i>Example:
//      <i>Timer interval low byte=0(Total interval=255-0+1=256count,aviliable on first cycle).
//      <i>Timer interval high byte=1(high level interval=255count(256-1=255),low level interval=1count).
//      <o>8Bit Timer/Counter Run <0=>Disable (TR1=0)(Default) <1=>Enable (TR1=1)
#define TIMER1_Mode0_8Bit_Timer_Counter_Run 0
//      </e>  //Mode0 8 Bit PWM Enable

//===========================================================
//      <e>Mode1 16 Bit Timer/Counter Enable
//      <i>Mode1 16 bit timer/counter enable.
#define TIMER1_Mode1_16Bit_Timer_Counter 1
//===========================================================
//      <o>Clock Output <0=>Disable (T1CKOE=0)(Default) <1=>Enable (T1CKOE=1)
//      <i>Note:T1CKOE=1,gpio wizard p3.5 or p4.5 or p1.7 or p3.3 select T1CKO.
#define TIMER1_Mode1_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T1X12=0,T1C_T=0)(Default)
//          <0x01=> T1 Pin (T1X12=0,T1C_T=1)
//          <0x02=> SYSCLK (T1X12=1,T1C_T=0)
//          <0x03=> SYSCLK/48 (T1X12=1,T1C_T=1)
#define TIMER1_Mode1_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T1G1=0,T1GATE=0)(Default)
//          <0x01=> INT1ET (T1G1=0,T1GATE=1)
//          <0x02=> TF3 (T1G1=1,T1GATE=0)
//          <0x03=> TI1 (T1G1=1,T1GATE=1)
#define TIMER1_Mode1_Gating_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//      <i>Example:
//      <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER1_Mode1_Timer_Interval 65498
//      <o>16Bit Timer/Counter Run <0=>Disable (TR1=0)(Default) <1=>Enable (TR1=1)
#define TIMER1_Mode1_16Bit_Timer_Counter_Run 1
//      </e>  //Mode1 16 Bit Timer/Counter Enable

//===========================================================
//      <e> Mode2 8Bit Timer/Counter Auto Reload Enable
//          <i>Mode2 8bit timer/counter auto reload enable.
#define TIMER1_Mode2_8Bit_Timer_Counter_Auto_Reload 0
//===========================================================
//      <o>Clock Output <0=>Disable (T1CKOE=0)(Default) <1=>Enable (T1CKOE=1)
//      <i>Note:T1CKOE=1,gpio wizard p3.5 or p4.5 or p1.7 or p3.3 select T1CKO.
#define TIMER1_Mode2_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T1X12=0,T1C_T=0)(Default)
//          <0x01=> T1 Pin (T1X12=0,T1C_T=1)
//          <0x02=> SYSCLK (T1X12=1,T1C_T=0)
//          <0x03=> SYSCLK/48 (T1X12=1,T1C_T=1)
#define TIMER1_Mode2_Clock_Source 0x00
//      <o>Gating Source
//          <0x00=> VDD (T1G1=0,T1GATE=0)(Default)
//          <0x01=> INT1ET (T1G1=0,T1GATE=1)
//          <0x02=> TF3 (T1G1=1,T1GATE=0)
//          <0x03=> TI1 (T1G1=1,T1GATE=1)
#define TIMER1_Mode2_Gating_Source 0x00
//      <o>Timer Interval (0~255) <0-255>
#define TIMER1_Mode2_Timer_Interval 0
//      <o>8Bit Timer/Counter Run <0=>Disable (TR1=0)(Default) <1=>Enable (TR1=1)
#define TIMER1_Mode2_8Bit_Timer_Counter_Run 0
//      </e>  //Mode2 8Bit Timer/Counter Auto Reload Enable
//  </e>  //Timer 1 Enable

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  <e>Timer2 Enable
//  <i>Mode0 16bit timer/tounter auto-reload and external interrupt.
//  <i>Mode1 16bit timer/tounter auto-reload with external interrupt.
//  <i>Mode2 16bit timer/tounter capture.
//  <i>Mode3 16bit timer/tounter capture auto_zero.
//  <i>Spilt mode0 two 8bit timer/counter auto-reload and external interrupt.
//  <i>Spilt mode1 two 8bit timer/counter auto-reload with external interrupt.
//  <i>Spilt mode2 two 8bit timer/counter capture.
//  <i>Spilt mode3 two 8bit timer/counter capture with auto zero.
//  <i>Spilt mode4 two 8bit pwm.
#define TM2_Wizard_init_EN 0
//      <o>Mode
//          <0x00=>Mode 0 16Bit Timer/Counter Auto-Reload and External Interrupt (T2SPL=0,T2MS1=0,CP/RL2=0,T2MS0=0)(Default)
//          <0x01=>Mode 1 16Bit Timer/Counter Auto-Reload with External Interrupt (T2SPL=0,T2MS1=0,CP/RL2=0,T2MS0=1)
//          <0x02=>Mode 2 16Bit Timer/Counter Capture (T2SPL=0,T2MS1=0,CP/RL2=1,T2MS0=0)
//          <0x03=>Mode 3 16Bit Timer/Counter Capture Auto_Zero(T2SPL=0,T2MS1=0,CP/RL2=1,T2MS0=1)
//          <0x04=>Split Mode 0 Two 8Bit Timer/Counter Auto-Reload and External Interrupt (T2SPL=1,T2MS1=0,CP/RL2=0,T2MS0=0)
//          <0x05=>Split Mode 1 Two 8Bit Timer/Counter Auto-Reload with External Interrupt (T2SPL=1,T2MS1=0,CP/RL2=0,T2MS0=1)
//          <0x06=>Split Mode 2 Two 8Bit Timer/Counter Capture (T2SPL=1,T2MS1=0,CP/RL2=1,T2MS0=0)
//          <0x07=>Split Mode 3 Two 8Bit Timer/Counter Capture with Auto Zero (T2SPL=1,T2MS1=0,CP/RL2=1,T2MS0=1)
//          <0x08=>Split Mode 4 8Bit Pwm (T2SPL=1,T2MS1=1,CP/RL2=0,T2MS0=0)
//          <i> Mode0~mode3 select.
//          <i> Split mode0~mode4 select.
#define TIMER2_Mode  0x00

//===========================================================
//      <e>Mode0 16Bit Timer/Counter Auto-Reload and External Interrupt Enable
//      <i>Mode 0 16bit auto-reload and external interrupt enable.
#define TIMER2_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Mode0_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Mode0_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Mode0_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Example:
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER2_Mode0_Timer_Interval 0
//      <o>Timer Interval Reload (0~65535) <0-65535>
#define TIMER2_Mode0_Timer_Interval_Reload 0
//          <i>Example:
//          <i>Timer interval reload =1(auto reload by 16 bit timer overflow),Timer interval=1(Total interval=65535-1+1=65535count,aviliable on first cycle).
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Mode0_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Mode0_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Mode0_16Bit_Timer_Counter_Run 0
//      </e>  //Mode0 16Bit Auto-Reload and External Interrupt Enable

//===========================================================
//      <e>Mode 1 16Bit Auto-Reload with External Interrupt Enable
//      <i>Mode 1 16bit timer auto-reload with external interrupt enable.
#define TIMER2_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================

//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Mode1_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Mode1_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Mode1_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Example:
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER2_Mode1_Timer_Interval 0
//      <o>Timer Interval Reload (0~65535) <0-65535>
#define TIMER2_Mode1_Timer_Interval_Reload 0
//          <i>Example:
//          <i>Timer interval reload =1(auto reload by 16bit timer/counter overflow or capture source dectect edge),Timer interval=1(Total interval=65535-1+1=65535count,aviliable on first cycle).
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Mode1_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Mode1_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Mode1_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 1 16Bit Auto-Reload with External Interrupt Enable

//===========================================================
//      <e>Mode 2 16Bit Timer/Counter Capture Enable
//      <i>Mode 2 16bit timer/counter capture enable.
#define TIMER2_Mode2_16Bit_Timer_Counter_Capture 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Mode2_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Mode2_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Mode2_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Note:If capture source dectect edge select ignored,timer2 is a 16 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual,timer 2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer 2 external inputs, that causes the current value in the timer 2 registers, TH2 and TL2,to be captured into registers RCAP2H and RCAP2L(capture reload registers),respectively.
//          <i>Note:In addition, the transition at capture source input causes bit EXF2 in T2CON to be set, and the EXF2 bit (like TF2) can generate an interrupt which vectors to the same location as Timer 2 overflow interrupt.
//          <i>Example: for 16 bit timer or counter
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER2_Mode2_Timer_Interval 0
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Mode2_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Mode2_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Mode2_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 2 16Bit Timer/Counter Capture Enable

//===========================================================
//      <e>Mode 3 16Bit Timer/Counter Capture With Auto Zero Enable
//      <i>Mode 3 16bit timer/counter capture with auto zero enable.
#define TIMER2_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Mode3_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Mode3_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Mode3_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Note:If capture source dectect edge select ignored,timer2 is a 16 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual,timer 2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer 2 external inputs, that causes the current value in the timer 2 registers, TH2 and TL2,to be captured into registers RCAP2H and RCAP2L(capture reload registers),respectively.
//          <i>Note:In addition, the transition at capture source input causes bit EXF2 in T2CON to be set, and the EXF2 bit (like TF2) can generate an interrupt which vectors to the same location as Timer 2 overflow interrupt.
//          <i>Note:Timer 2 mode 3 is the similar function with timer 2 mode 2. There is one difference that the capture source trigger(T2EXES), EXF2 event set signal, not only is the capture source of Timer 2 but also clears the content of TL2 and TH2 to 0x0000H.
//          <i>Example: for 16 bit timer or counter
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER2_Mode3_Timer_Interval 0
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Mode3_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Mode3_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Mode3_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 3 16Bit Timer/Counter Capture With Auto Zero Enable

//===========================================================
//      <e> Split Mode0 Two 8Bit Timer/Counter Auto-Reload and External Interrupt Enable
//      <i>Split mode0 two 8bit timer/counter auto-reload and external interrupt enable.
#define TIMER2_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Split_Mode0_Clock_Output_Enable 0
//      <o>TH2 Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> TL2OF (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Split_Mode0_TH2_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval High Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode0_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval High Byte Reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH2 overflow).
#define TIMER2_Split_Mode0_Timer_Interval_High_Byte_Reload 0
//      <o>TL2 Clock Source
//          <0x00=> SYSCLK/12 (TL2CS=0,TL2X12=0)(Default)
//          <0x01=> SYSCLK (TL2CS=0,TL2X12=0)
//          <0x02=> S0TOF (TL2CS=0,TL2X12=1)
//          <0x03=> INT0ET (TL2CS=0,TL2X12=1)
#define TIMER2_Split_Mode0_TL2_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode0_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL2 overflow).
#define TIMER2_Split_Mode0_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Split_Mode0_Capture_Source 0x00
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Split_Mode0_Overflow_Flag_Ignored 0
//      <o>TL2 Timer/counter Stop <0=>Disable (TR2LC=0)(Default) <1=>Enable (TR2LC=1)
#define TIMER2_Split_Mode0_TL2_Stop 0
//      <i>Note:TL2 stop by TH2 overflow.When TH2 over flow ,TR2L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Split_Mode0_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH2 Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Split_Mode0_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL2 Run <0=>Disable (TR2L=0)(Default) <1=>Enable (TR2L=1)
#define TIMER2_Split_Mode0_8Bit_Timer_Counter_TL2_Run 0
//      </e>  //Split mode0 two 8bit timer auto-reload and external interrupt Enable

//===========================================================
//      <e> Split Mode1 Two 8Bit Timer/Counter Auto-Reload with External Interrupt Enable
//      <i>Split mode1 two 8bit timer/counter auto-reload with external interrupt enable.
#define TIMER2_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Split_Mode1_Clock_Output_Enable 0
//      <o>TH2 Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> TL2OF (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Split_Mode1_TH2_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode1_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH2 overflow or capture source dectect edge).
#define TIMER2_Split_Mode1_Timer_Interval_High_Byte_Reload 0
//      <o>TL2 Clock Source
//          <0x00=> SYSCLK/12 (TL2CS=0,TL2X12=0)(Default)
//          <0x01=> SYSCLK (TL2CS=0,TL2X12=0)
//          <0x02=> S0TOF (TL2CS=0,TL2X12=1)
//          <0x03=> INT0ET (TL2CS=0,TL2X12=1)
#define TIMER2_Split_Mode1_TL2_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode1_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL2 overflow).
#define TIMER2_Split_Mode1_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Split_Mode1_Capture_Source 0x00
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Split_Mode1_Overflow_Flag_Ignored 0
//      <o>TL2 Timer/counter Stop <0=>Disable (TR2LC=0)(Default) <1=>Enable (TR2LC=1)
#define TIMER2_Split_Mode1_TL2_Stop 0
//      <i>Note:TL2 stop by TH2 overflow.When TH2 over flow ,TR2L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Split_Mode1_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH2 Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Split_Mode1_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL2 Run <0=>Disable (TR2L=0)(Default) <1=>Enable (TR2L=1)
#define TIMER2_Split_Mode1_8Bit_Timer_Counter_TL2_Run 0
//      </e>  //Split Mode1 Two 8Bit Timer Auto-Reload and External Interrupt Enable

//===========================================================
//      <e> Split Mode2 Two 8Bit Timer/Counter Capture Enable
//      <i>Split mode2 two 8bit timer/counter Capture enable.
#define TIMER2_Split_Mode2_Two_8Bit_Timer_Counter_Capture 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Split_Mode2_Clock_Output_Enable 0
//      <o>TH2 Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> TL2OF (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Split_Mode2_TH2_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Note:If capture source dectect edge select ignored,timer2 TH2 is a 8 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual, timer2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer2 external inputs, that causes the current value in the timer2 registers, TH2 ,to be captured into registers RCAP2H (capture reload registers).
//          <i>Note:In addition, the transition at capture source input causes bit EXF2 in T2CON to be set, and the EXF2 bit (like TF2) can generate an interrupt which vectors to the same location as Timer2 overflow interrupt.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode2_Timer_Interval_High_Byte 0
//      <o>TL2 Clock Source
//          <0x00=> SYSCLK/12 (TL2CS=0,TL2X12=0)(Default)
//          <0x01=> SYSCLK (TL2CS=0,TL2X12=0)
//          <0x02=> S0TOF (TL2CS=0,TL2X12=1)
//          <0x03=> INT0ET (TL2CS=0,TL2X12=1)
#define TIMER2_Split_Mode2_TL2_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode2_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL2 overflow).
#define TIMER2_Split_Mode2_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Split_Mode2_Capture_Source 0x00
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Split_Mode2_Overflow_Flag_Ignored 0
//      <o>TL2 Timer/counter Stop <0=>Disable (TR2LC=0)(Default) <1=>Enable (TR2LC=1)
#define TIMER2_Split_Mode2_TL2_Stop 0
//      <i>Note:If capture source dectect edge select falling,rising,dual, timer2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer2 external inputs, that causes timer2 TL2 8 bit timer or counter stop ,TR2L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Split_Mode2_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH2 Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Split_Mode2_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL2 Run <0=>Disable (TR2L=0)(Default) <1=>Enable (TR2L=1)
#define TIMER2_Split_Mode2_8Bit_Timer_Counter_TL2_Run 0
//      </e>  //Split Mode2 Two 8Bit Timer/Counter Capture Enable

//===========================================================
//      <e> Split Mode3 Two 8Bit Timer/Counter Capture with Auto Zero Enable
//      <i>Split mode3 two 8bit timer/counter capture with auto zero enable.
#define TIMER2_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Split_Mode3_Clock_Output_Enable 0
//      <o>TH2 Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Split_Mode3_TH2_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Note:If capture source dectect edge select ignored,timer2 TH2 is a 8 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual, timer2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer2 external inputs, that causes the current value in the timer 2 registers, TH2 ,to be captured into registers RCAP2H (capture reload registers).
//          <i>Note:In addition, the transition at capture source input causes bit EXF2 in T2CON to be set, and the EXF2 bit (like TF2) can generate an interrupt which vectors to the same location as Timer2 overflow interrupt.
//          <i>Note:Timer2 split mode3 is the similar function with timer2 split mode 2. There is one difference that the capture source trigger(T2EXES), EXF2 event set signal, not only is the capture source of Timer2 but also clears the content of TH2 to 0x00H.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode3_Timer_Interval_High_Byte 0
//      <o>TL2 Clock Source
//          <0x00=> SYSCLK/12 (TL2CS=0,TL2X12=0)(Default)
//          <0x01=> SYSCLK (TL2CS=0,TL2X12=0)
//          <0x02=> S0TOF (TL2CS=0,TL2X12=1)
//          <0x03=> INT0ET (TL2CS=0,TL2X12=1)
#define TIMER2_Split_Mode3_TL2_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode3_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL2 overflow).
#define TIMER2_Split_Mode3_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Split_Mode3_Capture_Source 0x00
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Split_Mode3_Overflow_Flag_Ignored 0
//      <o>TL2 Timer/counter Stop <0=>Disable (TR2LC=0)(Default) <1=>Enable (TR2LC=1)
#define TIMER2_Split_Mode3_TL2_Stop 0
//      <i>Note:If capture source dectect edge select falling,rising,dual, timer2 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer2 external inputs, that causes timer2 TL2 8 bit timer or counter stop ,TR2L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Split_Mode3_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH2 Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Split_Mode3_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL2 Run <0=>Disable (TR2L=0)(Default) <1=>Enable (TR2L=1)
#define TIMER2_Split_Mode3_8Bit_Timer_Counter_TL2_Run 0
//      </e>  //Split Mode3 Two 8Bit Timer/Counter Capture with Auto Zero Enable

//===========================================================
//      <e> Split Mode4 8Bit Pwm
//      <i>Split mode4 8bit pwm.
#define TIMER2_Split_Mode4_8Bit_Pwm 0
//===========================================================
//      <o>Clock Output <0=>Disable (T2OE=0)(Default) <1=>Enable (T2OE=1)
//      <i>Note:T2OE=1,gpio wizard p1.0 or p3.0 or p6.0 or p4.5 select T2CKO.
#define TIMER2_Split_Mode4_Clock_Output_Enable 0
//      <o>TIMER2 Clock Source
//          <0x00=> SYSCLK/12 (T2CKS=0,T2X12=0,C_T2=0)(Default)
//          <0x01=> T2 Pin (T2CKS=0,T2X12=0,C_T2=1)
//          <0x02=> SYSCLK (T2CKS=0,T2X12=1,C_T2=0)
//          <0x03=> INT0ET (T2CKS=0,T2X12=1,C_T2=1)
//          <0x04=> S0TOF (T2CKS=1,T2X12=0,C_T2=0)
//          <0x05=> T0OF (T2CKS=1,T2X12=0,C_T2=1)
//          <0x06=> Reserved (T2CKS=1,T2X12=1,C_T2=0)
//          <0x07=> KBIET (T2CKS=1,T2X12=1,C_T2=1)
#define TIMER2_Split_Mode4_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>TH2 and RCAP2H are combined to an 8-bit auto-reload counter.
//          <i>TH2 and RCAP2H configures to decide the PWM cycle time.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode4_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>TH2 and RCAP2H are combined to an 8-bit auto-reload counter.
//          <i>TH2 and RCAP2H configures to decide the PWM cycle time.
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH2 overflow).
#define TIMER2_Split_Mode4_Timer_Interval_High_Byte_Reload 0
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>TL2 is the PWM compare register to generate PWM waveform.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER2_Split_Mode4_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>RCAP2L is the PWM buffer register and software will update PWM data in this register. Each TH2 overflow event will set TF2 and load RCAP2L value into TL2.
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval low byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH2 overflow).
#define TIMER2_Split_Mode4_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T2EX Pin (CP2S2=0,CP2S1=0,CP2S0=0)(Default)
//          <0x01=> RXD0 (CP2S2=0,CP2S1=0,CP2S0=1)
//          <0x02=> P60 (CP2S2=0,CP2S1=1,CP2S0=0)
//          <0x03=> INT2ET (CP2S2=0,CP2S1=1,CP2S0=1)
//          <0x04=> ILRCO (CP2S2=1,CP2S1=0,CP2S0=0)
//          <0x05=> Reserved (CP2S2=1,CP2S1=0,CP2S0=1)
//          <0x06=> KBIET (CP2S2=1,CP2S1=1,CP2S0=0)
//          <0x07=> TWI0_SCL (CP2S2=1,CP2S1=1,CP2S0=1)
#define TIMER2_Split_Mode4_Capture_Source 0x00
//      <o>Timer2 Overflow Flag Ignored <0=>Disable (TF2IG=0)(Default) <1=>Enable (TF2IG=1)
#define TIMER2_Split_Mode4_Overflow_Flag_Ignored 0
//      <o>Timer2 T2CKO Invert <0=>Disable (TR2LC=0)(Default) <1=>Enable (TR2LC=1)
#define TIMER2_Split_Mode4_T2CKO_Invert 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN2=0,T2EXH=0)(Default)
//          <0x01=>Rising (EXEN2=0,T2EXH=1)
//          <0x02=>Falling (EXEN2=1,T2EXH=0)
//          <0x03=>Dual (EXEN2=1,T2EXH=1)
#define TIMER2_Split_Mode4_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter Run <0=>Disable (TR2=0)(Default) <1=>Enable (TR2=1)
#define TIMER2_Split_Mode4_8Bit_Timer_Counter_Run 0
//      </e>  //Split Mode3 Two 8Bit Pwm
//  </e>  //Timer2 Enable

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  <e>Timer3 Enable
//  <i>Mode0 16bit timer/tounter auto-reload and external interrupt.
//  <i>Mode1 16bit timer/tounter auto-reload with external interrupt.
//  <i>Mode2 16bit timer/tounter capture.
//  <i>Mode3 16bit timer/tounter capture auto_zero.
//  <i>Spilt mode0 two 8bit timer/counter auto-reload and external interrupt.
//  <i>Spilt mode1 two 8bit timer/counter auto-reload with external interrupt.
//  <i>Spilt mode2 two 8bit timer/counter capture.
//  <i>Spilt mode3 two 8bit timer/counter capture with auto zero.
//  <i>Spilt mode4 two 8bit pwm.
#define TM3_Wizard_init_EN 0
//      <o>Mode
//          <0x00=>Mode 0 16Bit Timer/Counter Auto-Reload and External Interrupt (T3SPL=0,T3MS1=0,CP/RL3=0,T3MS0=0)(Default)
//          <0x01=>Mode 1 16Bit Timer/Counter Auto-Reload with External Interrupt (T3SPL=0,T3MS1=0,CP/RL3=0,T3MS0=1)
//          <0x02=>Mode 2 16Bit Timer/Counter Capture (T3SPL=0,T3MS1=0,CP/RL3=1,T3MS0=0)
//          <0x03=>Mode 3 16Bit Timer/Counter Capture Auto_Zero(T3SPL=0,T3MS1=0,CP/RL3=1,T3MS0=1)
//          <0x04=>Split Mode 0 Two 8Bit Timer/Counter Auto-Reload and External Interrupt (T3SPL=1,T3MS1=0,CP/RL3=0,T3MS0=0)
//          <0x05=>Split Mode 1 Two 8Bit Timer/Counter Auto-Reload with External Interrupt (T3SPL=1,T3MS1=0,CP/RL2=0,T3MS0=1)
//          <0x06=>Split Mode 2 Two 8Bit Timer/Counter Capture (T3SPL=1,T3MS1=0,CP/RL3=1,T3MS0=0)
//          <0x07=>Split Mode 3 Two 8Bit Timer/Counter Capture with Auto Zero (T3SPL=1,T3MS1=0,CP/RL3=1,T2MS3=1)
//          <0x08=>Split Mode 4 8Bit Pwm (T3SPL=1,T3MS1=1,CP/RL3=0,T3MS0=0)
//          <i> Mode0~mode3 select.
//          <i> Split mode0~mode4 select.
#define TIMER3_Mode  0x00

//===========================================================
//      <e>Mode0 16Bit Timer/Counter Auto-Reload and External Interrupt Enable
//      <i>Mode 0 16bit auto-reload and external interrupt enable.
#define TIMER3_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Mode0_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> INT1ET (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Mode0_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Mode0_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Example:
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER3_Mode0_Timer_Interval 0
//      <o>Timer Interval Reload (0~65535) <0-65535>
#define TIMER3_Mode0_Timer_Interval_Reload 0
//          <i>Example:
//          <i>Timer interval reload =1(auto reload by 16 bit timer overflow),Timer interval=1(Total interval=65535-1+1=65535count,aviliable on first cycle).
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Mode0_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Mode0_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Mode0_16Bit_Timer_Counter_Run 0
//      </e>  //Mode0 16Bit Auto-Reload and External Interrupt Enable

//===========================================================
//      <e>Mode 1 16Bit Auto-Reload with External Interrupt Enable
//      <i>Mode 1 16bit timer auto-reload with external interrupt enable.
#define TIMER3_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Mode1_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> INT1ET (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Mode1_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Mode1_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Example:
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER3_Mode1_Timer_Interval 0
//      <o>Timer Interval Reload (0~65535) <0-65535>
#define TIMER3_Mode1_Timer_Interval_Reload 0
//          <i>Example:
//          <i>Timer interval reload =1(auto reload by 16bit timer/counter overflow or capture source dectect edge),Timer interval=1(Total interval=65535-1+1=65535count,aviliable on first cycle).
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Mode1_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Mode1_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Mode1_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 1 16Bit Auto-Reload with External Interrupt Enable

//===========================================================
//      <e>Mode 2 16Bit Timer/Counter Capture Enable
//      <i>Mode 2 16bit timer/counter capture enable.
#define TIMER3_Mode2_16Bit_Timer_Counter_Capture 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Mode2_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> INT1ET (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Mode2_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Mode2_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Note:If capture source dectect edge select ignored,TIMER3 is a 16 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual,timer3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer3 external inputs, that causes the current value in the timer3 registers, TH3 and TL3,to be captured into registers RCAP3H and RCAP3L(capture reload registers),respectively.
//          <i>Note:In addition, the transition at capture source input causes bit EXF3 in T3CON to be set, and the EXF3 bit (like TF3) can generate an interrupt which vectors to the same location as Timer3 overflow interrupt.
//          <i>Example: for 16 bit timer or counter
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER3_Mode2_Timer_Interval 0
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Mode2_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Mode2_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Mode2_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 2 16Bit Timer/Counter Capture Enable

//===========================================================
//      <e>Mode 3 16Bit Timer/Counter Capture With Auto Zero Enable
//      <i>Mode 3 16bit timer/counter capture with auto zero enable.
#define TIMER3_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Mode3_Clock_Output_Enable 0
//      <o>Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> INT1ET (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Mode3_Clock_Source 0x00
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Mode3_Capture_Source 0x00
//      <o>Timer Interval (0~65535) <0-65535>
//          <i>Note:If capture source dectect edge select ignored,TIMER3 is a 16 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual,timer3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 timer3 external inputs, that causes the current value in the timer3 registers, TH3 and TL3,to be captured into registers RCAP3H and RCAP3L(capture reload registers),respectively.
//          <i>Note:In addition, the transition at capture source input causes bit EXF3 in T3CON to be set, and the EXF3 bit (like TF3) can generate an interrupt which vectors to the same location as Timer3 overflow interrupt.
//          <i>Note:Timer3 mode3 is the similar function with timer3 mode2. There is one difference that the capture source trigger(T3EXES), EXF3 event set signal, not only is the capture source of Timer3 but also clears the content of TL3 and TH3 to 0x0000H.
//          <i>Example: for 16 bit timer or counter
//          <i>Timer interval =1(Total interval=65535-1+1=65535count,aviliable on first cycle).
#define TIMER3_Mode3_Timer_Interval 0
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Mode3_Overflow_Flag_Ignored 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Mode3_Capture_Source_Dectect_Edge 0x00
//      <o>16Bit Timer/Counter Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Mode3_16Bit_Timer_Counter_Run 0
//      </e>  //Mode 3 16Bit Timer/Counter Capture With Auto Zero Enable

//===========================================================
//      <e> Split Mode0 Two 8Bit Timer/Counter Auto-Reload and External Interrupt Enable
//      <i>Split mode0 two 8bit timer/counter auto-reload and external interrupt enable.
#define TIMER3_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Split_Mode0_Clock_Output_Enable 0
//      <o>TH3 Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> TL3OF (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Split_Mode0_TH3_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval High Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode0_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval High Byte Reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH3 overflow).
#define TIMER3_Split_Mode0_Timer_Interval_High_Byte_Reload 0
//      <o>TL3 Clock Source
//          <0x00=> SYSCLK/12 (TL3CS=0,TL3X12=0)(Default)
//          <0x01=> SYSCLK (TL3CS=0,TL3X12=0)
//          <0x02=> S1TOF (TL3CS=0,TL3X12=1)
//          <0x03=> INT1ET (TL3CS=0,TL3X12=1)
#define TIMER3_Split_Mode0_TL3_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode0_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL2 overflow).
#define TIMER3_Split_Mode0_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved T (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Split_Mode0_Capture_Source 0x00
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Split_Mode0_Overflow_Flag_Ignored 0
//      <o>TL3 Timer/counter Stop <0=>Disable (TR3LC=0)(Default) <1=>Enable (TR3LC=1)
#define TIMER3_Split_Mode0_TL3_Stop 0
//      <i>Note:TL3 stop by TH3 overflow.When TH3 over flow ,TR3L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Split_Mode0_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH3 Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Split_Mode0_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL3 Run <0=>Disable (TR3L=0)(Default) <1=>Enable (TR3L=1)
#define TIMER3_Split_Mode0_8Bit_Timer_Counter_TL3_Run 0
//      </e>  //Split mode0 two 8bit timer auto-reload and external interrupt Enable

//===========================================================
//      <e> Split Mode1 Two 8Bit Timer/Counter Auto-Reload with External Interrupt Enable
//      <i>Split mode1 two 8bit timer/counter auto-reload with external interrupt enable.
#define TIMER3_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Split_Mode1_Clock_Output_Enable 0
//      <o>TH3 Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> TL3OF (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Split_Mode1_TH3_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode1_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH2 overflow or capture source dectect edge).
#define TIMER3_Split_Mode1_Timer_Interval_High_Byte_Reload 0
//      <o>TL3 Clock Source
//          <0x00=> SYSCLK/12 (TL3CS=0,TL3X12=0)(Default)
//          <0x01=> SYSCLK (TL3CS=0,TL3X12=0)
//          <0x02=> S1TOF (TL3CS=0,TL3X12=1)
//          <0x03=> INT1ET (TL3CS=0,TL3X12=1)
#define TIMER3_Split_Mode1_TL3_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode1_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL3 overflow).
#define TIMER3_Split_Mode1_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved T (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Split_Mode1_Capture_Source 0x00
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Split_Mode1_Overflow_Flag_Ignored 0
//      <o>TL3 Timer/counter Stop <0=>Disable (TR3LC=0)(Default) <1=>Enable (TR3LC=1)
#define TIMER3_Split_Mode1_TL3_Stop 0
//      <i>Note:TL3 stop by TH3 overflow.When TH3 over flow ,TR3L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Split_Mode1_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH3 Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Split_Mode1_8Bit_Timer_Counter_TH3_Run 0
//      <o>8Bit Timer/Counter TL3 Run <0=>Disable (TR3L=0)(Default) <1=>Enable (TR3L=1)
#define TIMER3_Split_Mode1_8Bit_Timer_Counter_TL3_Run 0
//      </e>  //Split Mode1 Two 8Bit Timer Auto-Reload and External Interrupt Enable

//===========================================================
//      <e> Split Mode2 Two 8Bit Timer/Counter Capture Enable
//      <i>Split mode2 two 8bit timer/counter Capture enable.
#define TIMER3_Split_Mode2_Two_8Bit_Timer_Counter_Capture 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Split_Mode2_Clock_Output_Enable 0
//      <o>TH3 Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> TL3OF (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Split_Mode2_TH3_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Note:If capture source dectect edge select ignored,TIMER3 TH2 is a 8 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual, TIMER3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 TIMER3 external inputs, that causes the current value in the timer3 registers, TH3 ,to be captured into registers RCAP3H (capture reload registers).
//          <i>Note:In addition, the transition at capture source input causes bit EXF3 in T3CON to be set, and the EXF3 bit (like TF3) can generate an interrupt which vectors to the same location as Timer3 overflow interrupt.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode2_Timer_Interval_High_Byte 0
//      <o>TL3 Clock Source
//          <0x00=> SYSCLK/12 (TL3CS=0,TL3X12=0)(Default)
//          <0x01=> SYSCLK (TL3CS=0,TL3X12=0)
//          <0x02=> S1TOF (TL3CS=0,TL3X12=1)
//          <0x03=> INT1ET (TL3CS=0,TL3X12=1)
#define TIMER3_Split_Mode2_TL3_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode2_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL3 overflow).
#define TIMER3_Split_Mode2_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved T (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Split_Mode2_Capture_Source 0x00
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Split_Mode2_Overflow_Flag_Ignored 0
//      <o>TL3 Timer/counter Stop <0=>Disable (TR3LC=0)(Default) <1=>Enable (TR3LC=1)
#define TIMER3_Split_Mode2_TL3_Stop 0
//      <i>Note:If capture source dectect edge select falling,rising,dual, TIMER3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 TIMER3 external inputs, that causes TIMER3 TL3 8 bit timer or counter stop ,TR3L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Split_Mode2_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH3 Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Split_Mode2_8Bit_Timer_Counter_TH3_Run 0
//      <o>8Bit Timer/Counter TL3 Run <0=>Disable (TR3L=0)(Default) <1=>Enable (TR3L=1)
#define TIMER3_Split_Mode2_8Bit_Timer_Counter_TL3_Run 0
//      </e>  //Split Mode2 Two 8Bit Timer/Counter Capture Enable

//===========================================================
//      <e> Split Mode3 Two 8Bit Timer/Counter Capture with Auto Zero Enable
//      <i>Split mode3 two 8bit timer/counter capture with auto zero enable.
#define TIMER3_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
//      <i>Pin configuration T3/T3CKO P33 T3EX P34
#define TIMER3_Split_Mode3_Clock_Output_Enable 0
//      <o>TH3 Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> TL3OF (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Split_Mode3_TH3_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>Note:If capture source dectect edge select ignored,TIMER3 TH3 is a 8 bit timer or counter.
//          <i>Note:If capture source dectect edge select falling,rising,dual, TIMER3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 TIMER3 external inputs, that causes the current value in the timer3 registers, TH3 ,to be captured into registers RCAP3H (capture reload registers).
//          <i>Note:In addition, the transition at capture source input causes bit EXF3 in T3CON to be set, and the EXF3 bit (like TF3) can generate an interrupt which vectors to the same location as Timer3 overflow interrupt.
//          <i>Note:TIMER3 split mode3 is the similar function with TIMER3 split mode2. There is one difference that the capture source trigger(T3EXES), EXF3 event set signal, not only is the capture source of Timer3 but also clears the content of TH3 to 0x00H.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode3_Timer_Interval_High_Byte 0
//      <o>TL3 Clock Source
//          <0x00=> SYSCLK/12 (TL3CS=0,TL3X12=0)(Default)
//          <0x01=> SYSCLK (TL3CS=0,TL3X12=0)
//          <0x02=> S1TOF (TL3CS=0,TL3X12=1)
//          <0x03=> INT1ET (TL3CS=0,TL3X12=1)
#define TIMER3_Split_Mode3_TL3_Clock_Source 0x00
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode3_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TL3 overflow).
#define TIMER3_Split_Mode3_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved T (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Split_Mode3_Capture_Source 0x00
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Split_Mode3_Overflow_Flag_Ignored 0
//      <o>TL3 Timer/counter Stop <0=>Disable (TR3LC=0)(Default) <1=>Enable (TR3LC=1)
#define TIMER3_Split_Mode3_TL3_Stop 0
//      <i>Note:If capture source dectect edge select falling,rising,dual, TIMER3 still does the above,but with the added feature that a 1-to-0 or a 0-to-1 or 1-to-0,0-to-1 both, transition at capture source input,one of 8 TIMER3 external inputs, that causes TIMER3 TL3 8 bit timer or counter stop ,TR3L=0.
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Split_Mode3_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter TH3 Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Split_Mode3_8Bit_Timer_Counter_TH2_Run 0
//      <o>8Bit Timer/Counter TL3 Run <0=>Disable (TR3L=0)(Default) <1=>Enable (TR3L=1)
#define TIMER3_Split_Mode3_8Bit_Timer_Counter_TL2_Run 0
//      </e>  //Split Mode3 Two 8Bit Timer/Counter Capture with Auto Zero Enable

//===========================================================
//      <e> Split Mode4 8Bit Pwm
//      <i> Split mode4 8bit pwm.
#define TIMER3_Split_Mode4_8Bit_Pwm 0
//===========================================================
//      <o>Clock Output <0=>Disable (T3OE=0)(Default) <1=>Enable (T3OE=1)
//      <i>Note:T3OE=1,gpio wizard p3.3 select T3CKO.
#define TIMER3_Split_Mode4_Clock_Output_Enable 0
//      <o>TIMER3 Clock Source
//          <0x00=> SYSCLK/12 (T3CKS=0,T3X12=0,C_T3=0)(Default)
//          <0x01=> T3 Pin (T3CKS=0,T3X12=0,C_T3=1)
//          <0x02=> SYSCLK (T3CKS=0,T3X12=1,C_T3=0)
//          <0x03=> INT1ET (T3CKS=0,T3X12=1,C_T3=1)
//          <0x04=> S1TOF (T3CKS=1,T3X12=0,C_T3=0)
//          <0x05=> T0OF (T3CKS=1,T3X12=0,C_T3=1)
//          <0x06=> Reserved (T3CKS=1,T3X12=1,C_T3=0)
//          <0x07=> T1OF (T3CKS=1,T3X12=1,C_T3=1)
#define TIMER3_Split_Mode4_Clock_Source 0x00
//      <o>Timer Interval High Byte (0~255) <0-255>
//          <i>TH3 and RCAP3H are combined to an 8-bit auto-reload counter.
//          <i>TH3 and RCAP3H configures to decide the PWM cycle time.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval high byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode4_Timer_Interval_High_Byte 0
//      <o>Timer Interval High Byte Reload (0~255) <0-255>
//          <i>TH3 and RCAP3H are combined to an 8-bit auto-reload counter.
//          <i>TH3 and RCAP3H configures to decide the PWM cycle time.
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval high byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH3 overflow).
#define TIMER3_Split_Mode4_Timer_Interval_High_Byte_Reload 0
//      <o>Timer Interval Low Byte (0~255) <0-255>
//          <i>TL3 is the PWM compare register to generate PWM waveform.
//          <i>Example: for 8 bit timer or counter
//          <i>Timer interval Low Byte=1(Total interval=255-1+1=255count,aviliable on first cycle).
#define TIMER3_Split_Mode4_Timer_Interval_Low_Byte 0
//      <o>Timer Interval Low Byte Reload (0~255) <0-255>
//          <i>RCAP3L is the PWM buffer register and software will update PWM data in this register. Each TH3 overflow event will set TF3 and load RCAP3L value into TL3.
//          <i>Example: for 8 bit timer or counter
//          <i>timer interval low byte reload =1(Total interval=255-1+1=255count,auto reload by 8bit timer/counter TH3 overflow).
#define TIMER3_Split_Mode4_Timer_Interval_Low_Byte_Reload 0
//      <o>Capture Source
//          <0x00=> T3EX Pin (CP3S2=0,CP3S1=0,CP3S0=0)(Default)
//          <0x01=> INT0ET (CP3S2=0,CP3S1=0,CP3S0=1)
//          <0x02=> P60 (CP3S2=0,CP3S1=1,CP3S0=0)
//          <0x03=> Reserved T (CP3S2=0,CP3S1=1,CP3S0=1)
//          <0x04=> KBIET (CP3S2=1,CP3S1=0,CP3S0=0)
//          <0x05=> Reserved (CP3S2=1,CP3S1=0,CP3S0=1)
//          <0x06=> Reserved (CP3S2=1,CP3S1=1,CP3S0=0)
//          <0x07=> ILRCO (CP3S2=1,CP3S1=1,CP3S0=1)
#define TIMER3_Split_Mode4_Capture_Source 0x00
//      <o>TIMER3 Overflow Flag Ignored <0=>Disable (TF3IG=0)(Default) <1=>Enable (TF3IG=1)
#define TIMER3_Split_Mode4_Overflow_Flag_Ignored 0
//      <o>TIMER3 T3CKO Invert <0=>Disable (TR3LC=0)(Default) <1=>Enable (TR3LC=1)
#define TIMER3_Split_Mode4_T3CKO_Invert 0
//      <o>Capture Source Dectect Edge
//          <0x00=>Ignored (EXEN3=0,T3EXH=0)(Default)
//          <0x01=>Rising (EXEN3=0,T3EXH=1)
//          <0x02=>Falling (EXEN3=1,T3EXH=0)
//          <0x03=>Dual (EXEN3=1,T3EXH=1)
#define TIMER3_Split_Mode4_Capture_Source_Dectect_Edge 0x00
//      <o>8Bit Timer/Counter Run <0=>Disable (TR3=0)(Default) <1=>Enable (TR3=1)
#define TIMER3_Split_Mode4_8Bit_Timer_Counter_Run 0
//      </e>  //Split Mode3 Two 8Bit Pwm
//  </e>  //Timer3 Enable
//</e>  //Initialize Timers/Counters


/**
*****************************************************************************
* @brief        TIMER0 Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (T0CKO PinMux) (Gating Source) (Reload)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER0_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);\
        __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode0_Clock_Source);\
        __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode0_Gating_Source);\
        __DRV_TIMER0_SetHighByte(TIMER0_Mode0_Timer_Interval_High_Byte);\
        __DRV_TIMER0_SetLowByte(TIMER0_Mode0_Timer_Interval_Low_Byte);\
        __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode0_Clock_Output_Enable);\
        __DRV_TIMER0_Run_Cmd(TIMER0_Mode0_8Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (T1CKO PinMux) (Gating Source) (Reload)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER1_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER1_Mode_Select(TIMER1_Mode);\
        __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode0_Clock_Source);\
        __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode0_Gating_Source);\
        __DRV_TIMER1_SetHighByte(TIMER1_Mode0_Timer_Interval_High_Byte);\
        __DRV_TIMER1_SetLowByte(TIMER1_Mode0_Timer_Interval_Low_Byte);\
        __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode0_Clock_Output_Enable);\
        __DRV_TIMER1_Run_Cmd(TIMER1_Mode0_8Bit_Timer_Counter_Run);\
    ;)

/**
*****************************************************************************
* @brief        TIMER0 Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (Gating Source)(Reload)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER0_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);\
        __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode1_Clock_Source);\
        __DRV_TIMER0_Gating_Source_Select(TIMER1_Mode1_Gating_Source);\
        __DRV_TIMER0_Set16BitInterval(TIMER0_Mode1_Timer_Interval);\
        __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode1_Clock_Output_Enable);\
        __DRV_TIMER0_Run_Cmd(TIMER0_Mode0_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (Gating Source) (Reload)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER1_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER1_Mode_Select(TIMER1_Mode);\
        __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode1_Clock_Source);\
        __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode1_Gating_Source);\
        __DRV_TIMER1_Set16BitInterval(TIMER1_Mode1_Timer_Interval);\
        __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode1_Clock_Output_Enable);\
        __DRV_TIMER1_Run_Cmd(TIMER1_Mode1_16Bit_Timer_Counter_Run);\
    ;)

/**
*****************************************************************************
* @brief        TIMER0 Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (Gating Source) (Reload)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER0_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);\
        __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode2_Clock_Source);\
        __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode2_Gating_Source);\
        __DRV_TIMER0_Set8BitIntervalAutoReload(TIMER0_Mode2_Timer_Interval);\
        __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode2_Clock_Output_Enable);\
        __DRV_TIMER0_Run_Cmd(TIMER0_Mode2_8Bit_Timer_Counter_Run);\
    ;)

/**
*****************************************************************************
* @brief        TIMER1 Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (T0CKO PinMux) (Gating Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER1_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER1_Mode_Select(TIMER1_Mode);\
        __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode2_Clock_Source);\
        __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode2_Gating_Source);\
        __DRV_TIMER1_Set8BitIntervalAutoReload(TIMER1_Mode2_Timer_Interval);\
        __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode2_Clock_Output_Enable);\
        __DRV_TIMER1_Run_Cmd(TIMER1_Mode2_8Bit_Timer_Counter_Run);\
    ;)

/**
*****************************************************************************
* @brief        TIMER0 Mode3 Init from Wizard
* @details      Initial (MODE) (Clock Source) (T0CKO PinMux) (Gating Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER0_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_Mode3_Wizard_Init()\
    MWT(\
        __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);\
        __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode3_Clock_Source);\
        __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode3_Gating_Source);\
        __DRV_TIMER0_SetHighByte(TIMER0_Mode3_Timer_Interval_High_Byte);\
        __DRV_TIMER0_SetLowByte(TIMER0_Mode3_Timer_Interval_Low_Byte);\
        __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode3_T0CKO_Clock_Output_Enable);\
        __DRV_TIMER1_Run_Cmd(TIMER0_Mode3_8Bit_Timer_Counter_TH0_Run);\
        __DRV_TIMER0_Run_Cmd(TIMER0_Mode3_8Bit_Timer_Counter_TL0_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode0_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode0_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode0_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode0_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode0_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_Set16BitInterval(TIMER2_Mode0_Timer_Interval);\
        __DRV_TIMER2_Set16BCaptureitInterval(TIMER2_Mode0_Timer_Interval_Reload);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Mode0_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode1_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode1_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode1_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode1_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode1_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_Set16BitInterval(TIMER2_Mode1_Timer_Interval);\
        __DRV_TIMER2_Set16BCaptureitInterval(TIMER2_Mode1_Timer_Interval_Reload);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Mode1_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode2_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode2_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode2_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode2_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode2_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_Set16BitInterval(TIMER2_Mode2_Timer_Interval);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Mode2_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Mode3 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Mode3_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode3_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode3_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode3_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode3_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode3_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_Set16BitInterval(TIMER2_Mode3_Timer_Interval);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Mode3_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Split Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Split_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Split_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode0_TH2_Clock_Source);\
        __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode0_TL2_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode0_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode0_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode0_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode0_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode0_Timer_Interval_High_Byte);\
        __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode0_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode0_Timer_Interval_Low_Byte);\
        __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode0_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode0_8Bit_Timer_Counter_TL2_Run);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode0_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode0_TL2_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Split Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Split_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Split_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode1_TH2_Clock_Source);\
        __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode1_TL2_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode1_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode1_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode1_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode1_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode1_Timer_Interval_High_Byte);\
        __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode1_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode1_Timer_Interval_Low_Byte);\
        __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode1_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode1_8Bit_Timer_Counter_TL2_Run);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode1_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode1_TL2_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Split Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Split_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Split_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode2_TH2_Clock_Source);\
        __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode2_TL2_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode2_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode2_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode2_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode2_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode2_Timer_Interval_High_Byte);\
        __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode2_Timer_Interval_Low_Byte);\
        __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode2_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode2_8Bit_Timer_Counter_TL2_Run);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode2_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode2_TL2_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Split Mode3 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Split_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Split_Mode3_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode3_TH2_Clock_Source);\
        __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode3_TL2_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode3_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode3_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode3_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode3_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode3_Timer_Interval_High_Byte);\
        __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode3_Timer_Interval_Low_Byte);\
        __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode3_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode3_8Bit_Timer_Counter_TL2_Run);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode3_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode3_TL2_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Split Mode4 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER2_Split_Mode4_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Split_Mode4_Wizard_Init()\
    MWT(\
        __DRV_TIMER2_Mode_Select(TIMER2_Mode);\
        __DRV_TIMER2_Clock_Source_Select(TIMER2_Split_Mode4_Clock_Source);\
        __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode4_Capture_Source);\
        __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode4_Clock_Output_Enable);\
        __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode4_Overflow_Flag_Ignored);\
        __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode4_Capture_Source_Dectect_Edge);\
        __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode4_Timer_Interval_High_Byte);\
        __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode4_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode4_Timer_Interval_Low_Byte);\
        __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode4_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER2_T2CKO_Invert_Cmd(TIMER2_Split_Mode4_T2CKO_Invert);\
        __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode4_8Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode0_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode0_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode0_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode0_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode0_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_Set16BitInterval(TIMER3_Mode0_Timer_Interval);\
        __DRV_TIMER3_Set16BCaptureitInterval(TIMER3_Mode0_Timer_Interval_Reload);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Mode0_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode1_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode1_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode1_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode1_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode1_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_Set16BitInterval(TIMER3_Mode1_Timer_Interval);\
        __DRV_TIMER3_Set16BCaptureitInterval(TIMER3_Mode1_Timer_Interval_Reload);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Mode1_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode2_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode2_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode2_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode2_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode2_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_Set16BitInterval(TIMER3_Mode2_Timer_Interval);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Mode2_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Mode3 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Mode3_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode3_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode3_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode3_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode3_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode3_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_Set16BitInterval(TIMER3_Mode3_Timer_Interval);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Mode3_16Bit_Timer_Counter_Run);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Split Mode0 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Split_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Split_Mode0_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode0_TH3_Clock_Source);\
        __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode0_TL3_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode0_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode0_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode0_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode0_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode0_Timer_Interval_High_Byte);\
        __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode0_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode0_Timer_Interval_Low_Byte);\
        __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode0_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode0_8Bit_Timer_Counter_TL3_Run);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode0_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode0_TL3_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Split Mode1 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Split_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Split_Mode1_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode1_TH3_Clock_Source);\
        __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode1_TL3_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode1_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode1_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode1_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode1_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode1_Timer_Interval_High_Byte);\
        __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode1_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode1_Timer_Interval_Low_Byte);\
        __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode1_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode1_8Bit_Timer_Counter_TL3_Run);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode1_8Bit_Timer_Counter_TH3_Run);\
        __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode1_TL3_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Split Mode2 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Split_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Split_Mode2_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode2_TH3_Clock_Source);\
        __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode2_TL3_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode2_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode2_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode2_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode2_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode2_Timer_Interval_High_Byte);\
        __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode2_Timer_Interval_Low_Byte);\
        __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode2_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode2_8Bit_Timer_Counter_TL3_Run);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode2_8Bit_Timer_Counter_TH3_Run);\
        __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode2_TL3_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Split Mode3 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Split_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Split_Mode3_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode3_TH3_Clock_Source);\
        __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode3_TL3_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode3_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode3_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode3_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode3_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode3_Timer_Interval_High_Byte);\
        __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode3_Timer_Interval_Low_Byte);\
        __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode3_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode3_8Bit_Timer_Counter_TL2_Run);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode3_8Bit_Timer_Counter_TH2_Run);\
        __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode3_TL3_Stop);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Split Mode4 Init from Wizard
* @details      Initial (MODE) (Clock Source) (PinMux) (Capture Source) (Dectect Edge)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_TIMER3_Split_Mode4_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_Split_Mode4_Wizard_Init()\
    MWT(\
        __DRV_TIMER3_Mode_Select(TIMER3_Mode);\
        __DRV_TIMER3_Clock_Source_Select(TIMER3_Split_Mode4_Clock_Source);\
        __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode4_Capture_Source);\
        __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode4_Clock_Output_Enable);\
        __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode4_Overflow_Flag_Ignored);\
        __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode4_Capture_Source_Dectect_Edge);\
        __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode4_Timer_Interval_High_Byte);\
        __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode4_Timer_Interval_High_Byte_Reload);\
        __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode4_Timer_Interval_Low_Byte);\
        __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode4_Timer_Interval_Low_Byte_Reload);\
        __DRV_TIMER3_T3CKO_Invert_Cmd(TIMER3_Split_Mode4_T3CKO_Invert);\
        __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode4_8Bit_Timer_Counter_Run);\
    ;)

//@del{
void TIMER_test(void);
//@del}
void DRV_TIMER_Wizard_Init(void);
/// @endcond//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.06_Allen_20200528 //bugNum_Authour_Date
 * >> New
 * #1.00_Allen_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//  <e0>Initialize PCA
//      <i> Check this term, then DRV_PCA_Wizard_Init() will initialize PCA.
//      <i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_PCA_WIZARD  0x00
//      <o0> PCA Start Run
//          <i> PCA Counter Run
//          <i>Use Macro : "__DRV_PCA_Enable();" or "__DRV_PCA_Disable()";
//          <0x00=> Disable (CR=0) (Default)
//          <0x01=> Enable (CR=1)
            #define PCA_ENABLE 0x00
//      <o0> PCA Mode
//          <i>Select the function
//          <0x00=> Input Capture
//          <0x01=> Output Compare
//          <0x02=> PWM
//          <0x03=> PWM with Dead Time
//          <0x04=> 16-bit Software Timer Mode
            #define PCA_MODE 0x02

//====================================================================
//<e0> Initialize PCA Input Capture
//      <i> Initialize the PCA base unit for input capture according to the specified
//      <i> This only changes the parameters of "__DRV_PCA_IC_Wizard_Init()"
        #define PWM_IC_EN  0x00

//      <o0> PCA Clock Source
//          <0x00=> System clock/12 (CPS[2:0]=000) (Default)
//          <0x02=> System clock/2 (CPS[2:0]=001)
//          <0x04=> Timer0 overflow (CPS[2:0]=010)
//          <0x06=> ECI pin (CPS[2:0]=011)
//          <0x08=> CKMIX16 output (CPS[2:0]=100)
//          <0x0A=> System clock/1 (CPS[2:0]=101)
//          <0x0C=> S0BRG overflow (CPS[2:0]=110)
//          <0xE0=> MCK divider output (CPS[2:0]=111)
            #define IC_CKSRC   0x00
            
//      <o0> PCA Input Capture Period (0~65536)<0-65536>
            #define IC_PERIOD  65536
            
//      <q0.4> PCA Input Capture PWM0 & PWM1 Buffer Mode (BME0)
//      <q0.5> PCA Input Capture PWM2 & PWM3 Buffer Mode (BME2)
//      <q0.6> PCA Input Capture PWM4 & PWM5 Buffer Mode (BME4)
            #define IC_BUFFER  0x07
            
//      <e0> Capture Channel 0
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP0=1,CAPN0=0)
//              <0x10=> Negative edge trigger (CAPP0=0,CAPN0=1)
//              <0x30=> Dual edge trigger (CAPP0=1,CAPN0=1)
                #define IC0_EDGE 0x00
//      </e>

//      <e0> Capture Channel 1
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP1=1,CAPN1=0)
//              <0x10=> Negative edge trigger (CAPP1=0,CAPN1=1)
//              <0x30=> Dual edge trigger (CAPP1=1,CAPN1=1)
                #define IC1_EDGE 0x00
//      </e>

//      <e0> Capture Channel 2
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP2=1,CAPN2=0)
//              <0x10=> Negative edge trigger (CAPP2=0,CAPN2=1)
//              <0x30=> Dual edge trigger (CAPP2=1,CAPN2=1)
                #define IC2_EDGE 0x00
//      </e>

//      <e0> Capture Channel 3
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP3=1,CAPN3=0)
//              <0x10=> Negative edge trigger (CAPP3=0,CAPN3=1)
//              <0x30=> Dual edge trigger (CAPP3=1,CAPN3=1)
                #define IC3_EDGE 0x00
//      </e>

//      <e0> Capture Channel 4
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP4=1,CAPN4=0)
//              <0x10=> Negative edge trigger (CAPP4=0,CAPN4=1)
//              <0x30=> Dual edge trigger (CAPP4=1,CAPN4=1)
                #define IC4_EDGE 0x00
//      </e>

//      <e0> Capture Channel 5
//          <o0> PCA Capture Edge
//              <0x20=> Positive edge trigger (CAPP5=1,CAPN5=0)
//              <0x10=> Negative edge trigger (CAPP5=0,CAPN5=1)
//              <0x30=> Dual edge trigger (CAPP5=1,CAPN5=1)
                #define IC5_EDGE 0x00
//      </e>
//</e>
//====================================================================
//<e> Initialize PCA Output Compare
//  <i> Initialize the PCA base unit for output compare according to the specified
//  <i> This only changes the parameters of "__DRV_PCA_OC_Wizard_Init()"
#define PWM_OC_EN  0x00

//  <o0> PCA clock source
//      <0x00=> System clock/12 (CPS[2:0]=000) (Default)
//      <0x02=> System clock/2 (CPS[2:0]=001)
//      <0x04=> Timer0 overflow (CPS[2:0]=010)
//      <0x06=> ECI pin (CPS[2:0]=011)
//      <0x08=> CKMIX16 output (CPS[2:0]=100)
//      <0x0A=> System clock/1 (CPS[2:0]=101)
//      <0x0C=> S0BRG overflow (CPS[2:0]=110)
//      <0xE0=> MCK divider output (CPS[2:0]=111)
        #define OC_CKSRC   0x00
        
//  <o0> PCA Output Compare Period  (0~65536)<0-65536>
        #define OC_PERIOD  65536
//  <q0.4> PCA Output Compare PWM0 & PWM1 Buffer Mode (BME0)
//  <q0.5> PCA Output Compare PWM2 & PWM3 Buffer Mode (BME2)
//  <q0.6> PCA Output Compare PWM4 & PWM5 Buffer Mode (BME4)
//  <q0.7> PCA Output Compare PWM6 & PWM7 Buffer Mode (BME6)
    #define OC_BUFFER  0x00
    
//  <e0> PCA Channel 0
        #define OC0_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P0INV=0) (Default) 
//          <0x01=> Enable (P0INV=1)
            #define OC0_INV  0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P0RS1=0,P0RS0=0) (Default)
//          <0x40=> 10-bit (P0RS1=0,P0RS0=1)
//          <0x80=> 12-bit (P0RS1=1,P0RS0=0)
//          <0xC0=> 16-bit (P0RS1=1,P0RS0=1)
            #define OC0_RES  0x80
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC0_COPM  2048
//  </e>

//  <e0> PCA Channel 1
        #define OC1_EN 0
        
//      <o0> Inverted 
//          <0x00=> Disable (P1INV=0) (Default) 
//          <0x01=> Enable (P1INV=1)
            #define OC1_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P1RS1=0,P1RS0=0) (Default)
//          <0x40=> 10-bit (P1RS1=0,P1RS0=1)
//          <0x80=> 12-bit (P1RS1=1,P1RS0=0)
//          <0xC0=> 16-bit (P1RS1=1,P1RS0=1)
            #define OC1_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC1_COPM  65535
//  </e>

//  <e0> PCA Channel 2
        #define OC2_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P2INV=0) (Default) 
//          <0x01=> Enable (P2INV=1)
            #define OC2_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P2RS1=0,P2RS0=0) (Default)
//          <0x40=> 10-bit (P2RS1=0,P2RS0=1)
//          <0x80=> 12-bit (P2RS1=1,P2RS0=0)
//          <0xC0=> 16-bit (P2RS1=1,P2RS0=1)
            #define OC2_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC2_COPM  65535
//  </e>

//  <e0> PCA Channel 3
        #define OC3_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P3INV=0) (Default) 
//          <0x01=> Enable (P3INV=1) 
            #define OC3_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P3RS1=0,P3RS0=0) (Default)
//          <0x40=> 10-bit (P3RS1=0,P3RS0=1)
//          <0x80=> 12-bit (P3RS1=1,P3RS0=0)
//          <0xC0=> 16-bit (P3RS1=1,P3RS0=1)
            #define OC3_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC3_COPM  65535
//  </e>

//  <e0> PCA Channel 4
        #define OC4_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P4INV=0)(Default) 
//          <0x01=> Enable (P4INV=1) 
            #define OC4_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P4RS1=0,P4RS0=0) (Default)
//          <0x40=> 10-bit (P4RS1=0,P4RS0=1)
//          <0x80=> 12-bit (P4RS1=1,P4RS0=0)
//          <0xC0=> 16-bit (P4RS1=1,P4RS0=1)
            #define OC4_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC4_COPM  65535
//  </e>

//  <e0> PCA Channel 5
        #define OC5_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P5INV=0) (Default) 
//          <0x01=> Enable (P5INV=1) 
            #define OC5_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P5RS1=0,P5RS0=0) (Default)
//          <0x40=> 10-bit (P5RS1=0,P5RS0=1)
//          <0x80=> 12-bit (P5RS1=1,P5RS0=0)
//          <0xC0=> 16-bit (P5RS1=1,P5RS0=1)
            #define OC5_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC5_COPM  65535
//  </e>

//  <e0> PCA Channel 6
        #define OC6_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P6INV=0) (Default) 
//          <0x01=> Enable (P6INV=1) 
            #define OC6_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P6RS1=0,P6RS0=0) (Default)
//          <0x40=> 10-bit (P6RS1=0,P6RS0=1)
//          <0x80=> 12-bit (P6RS1=1,P6RS0=0)
//          <0xC0=> 16-bit (P6RS1=1,P6RS0=1)
            #define OC6_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC6_COPM  65535
//  </e>

//  <e0> PCA Channel 7
        #define OC7_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P7INV=0)(Default) 
//          <0x01=> Enable (P7INV=1) 
            #define OC7_INV 0x00
//      <o0> Resolution
//          <0x00=> 8-bit (P7RS1=0,P7RS0=0) (Default)
//          <0x40=> 10-bit (P7RS1=0,P7RS0=1)
//          <0x80=> 12-bit (P7RS1=1,P7RS0=0)
//          <0xC0=> 16-bit (P7RS1=1,P7RS0=1)
            #define OC7_RES  0x00
//      <o0> Compare Value (0~65535)
//          <i> 8-bit Resolution 0~255
//          <i> 10-bit Resolution 0~1023
//          <i> 12-bit Resolution 0~4095
//          <i> 16-bit Resolution 0~65535
            #define OC7_COPM  65535
//  </e>
//</e>
//====================================================================
//<e> Initialize PCA PWM
//  <i> Initialize the PCA base unit for PWM according to the specified
//  <i> This only changes the parameters of "__DRV_PCA_PWM_Wizard_Init()"
    #define PWM_EN  0x00

//  <o0> PCA clock source
//      <i> Select PCA clock source
//      <0x00=> System clock/12 (CPS[2:0]=000) (Default)
//      <0x02=> System clock/2 (CPS[2:0]=001)
//      <0x04=> Timer0 overflow (CPS[2:0]=010)
//      <0x06=> ECI pin (CPS[2:0]=011)
//      <0x08=> CKMIX16 output (CPS[2:0]=100)
//      <0x0A=> System clock/1 (CPS[2:0]=101)
//      <0x0C=> S0BRG overflow (CPS[2:0]=110)
//      <0xE0=> MCK divider output (CPS[2:0]=111)
        #define PWM_CKSRC   0x00
        
//  <o0> PCA clock source frequency
//      <i> Fill in the clock frequency of the PCA module.
//      <i> Example: 1000000
        #define PWM_CKS_FREQ  1000000
        
//  <o0> PCA PWM frequency
//      <i> Fill in the PWM output frequency.
//      <i> If you select central aligned mode, the output frequency will be divided by 2.
//      <i> Example: 30000
        #define PWM_FREQ  30000
//  <o0> PCA PWM aligned mode
//      <i> Central aligned function is only active on PWM0~5, PWM6/PWM7 not support central aligned.
//      <0x00=> Edge aligned (PCAE=0,C0M0=0) (Default)
//      <0x01=> Central aligned (PCAE=1,C0M0=1)
        #define PWM_ALIGN   0x00
//  <q0.4> PCA PWM0 & PWM1 Buffer Mode (BME0)
//  <q0.5> PCA PWM2 & PWM3 Buffer Mode (BME2)
//  <q0.6> PCA PWM4 & PWM5 Buffer Mode (BME4)
//  <q0.7> PCA PWM6 & PWM7 Buffer Mode (BME6)
        #define PWM_BUFFER  0x00
//  <e0> PCA Channel 0
        #define PWM0_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P0INV=0) (Default) 
//          <0x01=> Enable (P0INV=1)
            #define PWM0_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM0_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 1
        #define PWM1_EN 0
//      <o0> Inverted 
//          <0x00=> Disable (P1INV=0) (Default) 
//          <0x01=> Enable (P1INV=1)
            #define PWM1_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM1_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 2
        #define PWM2_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P2INV=0) (Default) 
//          <0x01=> Enable (P2INV=1)
            #define PWM2_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM2_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 3
        #define PWM3_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P3INV=0) (Default) 
//          <0x01=> Enable (P3INV=1) 
            #define PWM3_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM3_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 4
        #define PWM4_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P4INV=0)(Default) 
//          <0x01=> Enable (P4INV=1) 
            #define PWM4_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM4_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 5
        #define PWM5_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P5INV=0) (Default) 
//          <0x01=> Enable (P5INV=1) 
            #define PWM5_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM5_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 6
        #define PWM6_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P6INV=0) (Default) 
//          <0x01=> Enable (P6INV=1) 
            #define PWM6_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM6_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 7
        #define PWM7_EN 0
//      <o0> Inverted
//          <0x00=> Disable (P7INV=0)(Default) 
//          <0x01=> Enable (P7INV=1) 
            #define PWM7_INV  0x00
//      <o0> Duty cycle percentage (0~100)  <0-100>
//          <i> PWM high level percentage
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM7_DUTY_PERC  50
//  </e>
//</e>
//====================================================================
//<e> Initialize PCA PWM with Dead Time
//  <i> Initialize the PCA base unit for PWM with dead time according to the specified
//  <i> This only changes the parameters of "__DRV_PCA_PWM_DTG_Wizard_Init()"
    #define PWM_DTG_EN  0x00

//  <o0> PCA clock source
//      <i> Select PCA clock source
//      <0x00=> System clock/12 (CPS[2:0]=000) (Default)
//      <0x02=> System clock/2 (CPS[2:0]=001)
//      <0x04=> Timer0 overflow (CPS[2:0]=010)
//      <0x06=> ECI pin (CPS[2:0]=011)
//      <0x08=> CKMIX16 output (CPS[2:0]=100)
//      <0x0A=> System clock/1 (CPS[2:0]=101)
//      <0x0C=> S0BRG overflow (CPS[2:0]=110)
//      <0xE0=> MCK divider output (CPS[2:0]=111)
        #define PWM_DTG_CKSRC   0x00
//  <o0> PCA clock fource frequency
//      <i> Fill in the clock frequency of the PCA module.
//      <i> Example: 1000000
        #define PWM_DTG_CKS_FREQ  1000000
//  <o0> PCA PWM frequency
//      <i> Fill in the PWM output frequency.
//      <i> Example: 30000
        #define PWM_DTG_FREQ  30000
//  <o0> PWM dead time clock source
//      <0x00=> System clock (DTPS[1:0]=00)(Default)
//      <0x40=> System clock/2 (DTPS[1:0]=01)
//      <0x80=> System clock/4 (DTPS[1:0]=10)
//      <0xC0=> System clock/8 (DTPS[1:0]=11)
        #define PWM_DTG_DTCKSRC  0x00
//  <o0> PCA PWM dead time period  (0~63)<0-63>
//      <i> PWM dead time = dead time clock source * dead time period
        #define PWM_DTG_DTPERIOD  1

//  <e0> PCA Channel 0 & PCA Channel 1
        #define PWM01_DTG_EN 0
//      <o0> PWM0 Inverted
//          <0x00=> Disable (P0INV=0) (Default) 
//          <0x01=> Enable (P0INV=1)
            #define PWM0_DTG_INV  0x00
//      <o0> PWM1 Inverted
//          <0x00=> Disable (P1INV=0) (Default) 
//          <0x01=> Enable (P1INV=1)
            #define PWM1_DTG_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> Setting dead-time will affect duty cycle percentage.
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM01_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 2 & PCA Channel 3
        #define PWM23_DTG_EN 0
//      <o0> PWM2 Inverted
//          <0x00=> Disable (P2INV=0) (Default) 
//          <0x01=> Enable (P2INV=1)
            #define PWM2_DTG_INV  0x00
//      <o0> PWM3 Inverted
//          <0x00=> Disable (P3INV=0) (Default) 
//          <0x01=> Enable (P3INV=1) 
            #define PWM3_DTG_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> Setting dead-time will affect duty cycle percentage.
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM23_DUTY_PERC  50
//  </e>

//  <e0> PCA Channel 4 & PCA Channel 5
        #define PWM45_DTG_EN 0
//      <o0> PWM4 Inverted
//          <0x00=> Disable (P4INV=0)(Default) 
//          <0x01=> Enable (P4INV=1) 
            #define PWM4_DTG_INV  0x00
//      <o0> PWM5 Inverted
//          <0x00=> Disable (P5INV=0) (Default) 
//          <0x01=> Enable (P5INV=1) 
            #define PWM5_DTG_INV  0x00
//      <o0> Duty cycle percentage (0~100) <0-100>
//          <i> PWM high level percentage
//          <i> Setting dead-time will affect duty cycle percentage.
//          <i> The higher the PWM frequency, the lower the resolution of duty time.
//          <i> PWM resolution = PCA clock frequency / PWM frequency
            #define PWM45_DUTY_PERC  50
//  </e>
//</e>
//====================================================================
//  <e0> Initialize PCA 16 bit Timer
//      <i> Initialize the PCA base unit for 16 bit Timer according to the specified
//      <i> This only changes the parameters of "__DRV_PCA_Timer_Wizard_Init()"
        #define TIM_EN  0x00

//  <o0> PCA Clock Source
//      <i> Select PCA clock source
//      <0x00=> System clock/12 (CPS[2:0]=000) (Default)
//      <0x02=> System clock/2 (CPS[2:0]=001)
//      <0x04=> Timer0 overflow (CPS[2:0]=010)
//      <0x06=> ECI pin (CPS[2:0]=011)
//      <0x08=> CKMIX16 output (CPS[2:0]=100)
//      <0x0A=> System clock/1 (CPS[2:0]=101)
//      <0x0C=> S0BRG overflow (CPS[2:0]=110)
//      <0xE0=> MCK divider output (CPS[2:0]=111)
        #define TIM_CKSRC   0x00
//  <o0> PCA Timer Period (0~65536)<0-65536>
        #define TIM_PERIOD  65536

//  <e0> Channel 0
        #define PWM0_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM0_VAL 0
//  </e>

//  <e0> Channel 1
        #define PWM1_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM1_VAL 0
//</e>

//  <e0> Channel 2
        #define PWM2_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM2_VAL 0
//  </e>

//  <e0> Channel 3
        #define PWM3_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM3_VAL 0
//  </e>

//  <e0> Channel 4
        #define PWM4_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM4_VAL 0
//  </e>

//  <e0> Channel 5
        #define PWM5_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM5_VAL 0
//  </e>

//  <e0> Channel 6
        #define PWM6_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM6_VAL 0
//  </e>

//  <e0> Channel 7
        #define PWM7_TIM_EN 0
//      <o0> Timer Value(0~65535)<0-65535>
            #define TIM7_VAL 0
//  </e>
//</e>
//===================================================================

//</e> PCA End
/// @endcond
/**
 *******************************************************************************
 * @brief       Initializes the PCA base unit for input capture according to the specified (by Wizard)
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_PCA_IC_Wizard_Init();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_PCA_IC_Wizard_Init()\
      MWT(\
        __DRV_PCA_Disable();\
        CH = 0; CHRL = HIBYTE(65536-(IC_PERIOD));\
        CL = 0; CLRL = LOBYTE(65536-(IC_PERIOD));\
        __DRV_PCA_ClockSource_Select(IC_CKSRC);\
        CMOD |= IC_BUFFER & 0x70;\
        CCAPM0 = IC0_EDGE & 0x30;\
        CCAPM1 = IC1_EDGE & 0x30;\
        CCAPM2 = IC2_EDGE & 0x30;\
        CCAPM3 = IC3_EDGE & 0x30;\
        CCAPM4 = IC4_EDGE & 0x30;\
        CCAPM5 = IC5_EDGE & 0x30;\
        )
void DRV_PCA_Wizard_Init();
//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//  <e0>Initialize Serial Port 0 (UART0)
//      <i> Check this term, then DRV_UART0_Wizard_Init() will initialize UART0.
//      <i> Note: TI0 bit will be set in Serial Port 0 (UART0) Initializtion.
//      <i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_UART0_WIZARD      0
//-----------------------------------------------------------------------------
//      <o>UART0
//          <0=>Easy Mode
//          <1=> Advanced Mode
//          <i>UART0 option select.
#define URT0_Option 0
//      <e>Easy Mode
//          <i>Easy mode enable.
#define URT0_Easy_Mode 0
//      <e>UART0 Enable
//          <i>Ready.
#define URT0_Easy 0
//      <o>Easy Select
//          <0x000000=>Select0(Default)
//          <0x010000=>Select1
//          <0x020000=>Select2
//          <0x030000=>Select3
//          <0x040000=>Select4
//          <0x050000=>Select5
//          <0x000100=>Select6
//          <0x010100=>Select7
//          <0x020100=>Select8
//          <0x030100=>Select9
//          <0x040100=>Select10
//          <0x050100=>Select11
//          <0x000200=>Select12
//          <0x010200=>Select13
//          <0x020200=>Select14
//          <0x030200=>Select15
//          <0x040200=>Select16
//          <0x050200=>Select17
//          <0x000400=>Select18
//          <0x010400=>Select19
//          <0x020400=>Select20
//          <0x030400=>Select21
//          <0x040400=>Select22
//          <0x050400=>Select23
#define URT0_Easy_Select 0x000000
//          <i>Note:Please confirm clock source before you select parameter.

//          <i>Select0 :	9600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select1 :	9600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Rx Only     	IHRCO 12Mhz
//          <i>Select2 :	9600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select3 :	9600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select4 :	9600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	RX Only     	IHRCO 12Mhz
//          <i>Select5 :	9600	8BIt	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz

//          <i>Select6 :	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select7 :	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Rx Only     	IHRCO 12Mhz
//          <i>Select8 :	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select9 :	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select10:	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Rx Only     	IHRCO 12Mhz

//          <i>Select11:	19200	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select12:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select13:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Rx Only     	IHRCO 12Mhz
//          <i>Select14:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select15:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Only     	IHRCO 12Mhz
//          <i>Select16:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Rx Only     	IHRCO 12Mhz
//          <i>Select17:	57600	8Bit	S0BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P30 Tx=P31	Tx Rx Both  	IHRCO 12Mhz

//          <i>Select18:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	RX=P30 TX=P31	Tx Only   	IHRCO 11.0592Mhz
//          <i>Select19:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	RX=P30 TX=P31	Rx Only   	IHRCO 11.0592Mhz
//          <i>Select20:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	RX=P30 TX=P31	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select21:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	RX=P30 TX=P31	Tx Only   	IHRCO 11.0592Mhz
//          <i>Select22:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	RX=P30 TX=P31	Rx Only   	IHRCO 11.0592Mhz
//          <i>Select23:	115200	8Bit	S0BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	RX=P30 TX=P31	Tx Rx Both	IHRCO 11.0592Mhz

//      </e>  //UART0 Enable
//      </e>  //Easy Mode

//      <e>Advanced Mode
//          <i>Advanced mode enable .
#define URT0_Advance_Mode 0
//      <e>UART0 Enable
//          <i>Mode0 shift .
//          <i>Mode1 8bit .
//          <i>Mode2 9bit .
//          <i>Mode3 9bit .
//          <i>Mode4 Spi master .
//          <i>Mode5 Lin bus .
#define URT0_Advance 0

//      <o>Mode
//          <0x00=>Mode 0 Shift (SM30=0,SM00=0,SM10=0)(Default)
//          <0x01=>Mode 1 8Bit (SM30=0,SM00=0,SM10=1)
//          <0x02=>Mode 2 9Bit (SM30=0,SM00=1,SM10=0)
//          <0x03=>Mode 3 9Bit (SM30=0,SM00=1,SM10=1)
//          <0x04=>Mode 4 Spi Master (SM30=1,SM00=0,SM10=0)
//          <0x05=>Mode 5 Lin Bus (SM30=1,SM00=0,SM10=1)
//          <i> Mode0~mode5 select
#define URT0_Mode  0x00
//===========================================================
//      <e>Mode 0 Shift Enable
//===========================================================
//          <i>Mode 0 shift enable .
#define URT0_Mode0_Shift 0
//      <e>Baud Rate Config Enable
#define URT0_Mode0_Shift_Baud_Rate_Config 0
//      <o0>Rx Clock
//          <0=> S0BRG (S0RCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0RCK=0)(Default)
//          <i>Mode 0 S0BRG (S0RCK=0)
//          <i>Shift Clock which directly comes from RX Clock to the alternate output function of TX0 pin (S0RCK=0).
#define URT0_Mode0_Shift_Receive_Clock 0x01
//      <o0.2>Tx Clock
//          <0=> S0BRG (S0TCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <i>Mode 0 S0BRG (S0TCK=1)
//          <i>Clock source select S0BRG (S0TCK=1) .
#define URT0_Mode0_Shift_Transmit_Clock 0x00
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB (S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode0_Shift_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (SMOD2=0)(Default)  <1=>Double Baud Rate (SMOD2=1)
//          <i> Defaule baud date (SMOD2=0).
//          <i> Double baud rate (SMOD2=1).
#define URT0_Mode0_Shift_Baud_Rate 0
//      <o0.6>Source <0=> SYSCLK/12 (S0TX12=0)(Default) <1=> SYSCLK/1 (S0TX12=1)
//          <i>SYSCLK/12 (S0TX12=0).
//          <i>SYSCLK/1  (S0TX12=1).
#define URT0_Mode0_Shift_Clock_Source 0
//      <o>Baud Rate Reload (0~255) <0-255>
#define URT0_Mode0_Shift_Baud_Rate_Reload_Count 0
//          <i>**********     	2*2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=0.
//          <i>**********     	    16         	     	  12x(256-S0BRT)
//          <i>**********     	2*2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=1.
//          <i>**********     	    16         	     	  1x(256-S0BRT)
//      <o>Shift In/Out <0=> Out (REN0=0)(Default) <1=> In (REN0=1)
//      <i>Reception is initiated by the condition REN0=1 and RI0=0.
//      <i>Transmission is initiated by any instruction that uses S0BUF as a destination register.
//      <i>After REN0 on/off , clear RI0
#define URT0_Mode0_Shift_Input_Output 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode0_Shift Enable
//===========================================================
//      <e>Mode 1 8Bit Enable
//===========================================================
//          <i>Mode 1 8bit enable .
#define URT0_Mode1_8Bit 0
//      <e>Baud Rate Config Enable
#define URT0_Mode1_8Bit_Baud_Rate_Config 0
//      <o0>Rx Clock
//          <0=> S0BRG (S0RCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0RCK=0)(Default)
//          <i>Mode 1 S0BRG (S0RCK=1)
//          <i>Clock source select S0BRG (S0RCK=1) .
#define URT0_Mode1_8Bit_Receive_Clock 0x00
//      <o0.2>Tx Clock
//          <0=> S0BRG (S0TCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <i>Mode 1 S0BRG (S0TCK=1)
//          <i>Clock source select S0BRG (S0TCK=1) .
#define URT0_Mode1_8Bit_Transmit_Clock 0x00
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB (S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode1_8Bit_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (SMOD2=0)(Default) <1=>Double Baud Rate (SMOD2=1)
//          <i> Defaule baud date (SMOD2=0).
//          <i> Double baud rate (SMOD2=1).
#define URT0_Mode1_8Bit_Baud_Rate 0
//      <o0.6>Source <0=> SYSCLK/12 (S0TX12=0)(Default) <1=> SYSCLK/1 (S0TX12=1)
//          <i>SYSCLK/12 (S0TX12=0).
//          <i>SYSCLK/1  (S0TX12=1).
#define URT0_Mode1_8Bit_Clock_Source 0
//      <o>Baud Rate Reload (0~255) <0-255>
#define URT0_Mode1_8Bit_Baud_Rate_Reload_Count 0
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=0.
//          <i>**********     	    32         	     	  12x(256-S0BRT)
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=1.
//          <i>**********     	    32         	     	  1x(256-S0BRT)
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 1 8Bit Enable
//===========================================================
//      <e> Mode 2 9Bit Enable
//===========================================================
//          <i>Mode 2 9bit enable.
#define URT0_Mode2_9Bit 0
//      <e> Baud Rate Config Enable
#define URT0_Mode2_9Bit_Baud_Rate_Config 0
//          <i>BaudRate    	RxClock     	TxClock     	EnhanceBaudRate     	Source      	SYSCLK/n
//          <i>172800      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/1    	SYSCLK/64     	IHRCO 11.0592MHZ
//          <i>345600      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X1   	SYSCLK/1    	SYSCLK/32     	IHRCO 11.0592MHZ
//          <i>691200      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X2   	SYSCLK/1    	SYSCLK/16     	IHRCO 11.0592MHZ
//          <i>1382400     	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X4   	SYSCLK/1    	SYSCLK/8      	IHRCO 11.0592MHZ
//          <i>187500      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/1    	SYSCLK/64     	IHRCO 12MHZ
//          <i>375000      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X1    	SYSCLK/1    	SYSCLK/32     	IHRCO 12MHZ
//          <i>750000      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X2    	SYSCLK/1    	SYSCLK/16     	IHRCO 12MHZ
//          <i>1500000     	SYSCLK/2    	SYSCLK/2    	Double Baud Rate X4    	SYSCLK/1    	SYSCLK/8      	IHRCO 12MHZ
//      <o0>Rx Clock
//          <1=> S0BRG (S0RCK=1)
//          <0=> SYSCLK/TIMER/S1BRG (S0RCK=0)(Default)
//          <i>Mode 2 SYSCLK (S0RCK=0)
//          <i>Clock source select SYSCLK/2 (S0RCK=0).
#define URT0_Mode2_9Bit_Receive_Clock 0x00
//      <o0>Tx Clock
//          <1=> S0BRG (S0TCK=1)
//          <0=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <i>Mode 2 SYSCLK (S0TCK=0)
//          <i>Clock source select SYSCLK/2 (S0TCK=0).
#define URT0_Mode2_9Bit_Transmit_Clock 0x00
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB(S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode2_9Bit_Data_Order 0x08
//      <o0.4..5>Enhance Baud Rate
//          <0=>Default Baud Rate (SMOD2=0,SMOD1=0)(Default)
//          <1=>Double Baud Rate X1 (SMOD2=0,SMOD1=1)
//          <2=>Double Baud Rate X2 (SMOD2=1,SMOD1=0)
//          <3=>Double Baud Rate X4 (SMOD2=1,SMOD1=1)
//          <i>Enhance baud rate select default,double x1,x2 and x4.
#define URT0_Mode2_9Bit_Baud_Rate 0
//      <o0.6>Source <0=>SYSCLK/1 (URM0X3=0)(Default)
//          <i>SYSCLK/1 (URM0X3=0).
#define URT0_Mode2_9Bit_Clock_Source 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 2 9Bit Enable
//===========================================================
//      <e>Mode 3 9Bit Enable
//===========================================================
//          <i>Mode 3 9bit enable .
//          <i>Ready.
#define URT0_Mode3_9Bit 0
//      <e>Baud Rate Config Enable
#define URT0_Mode3_9Bit_Baud_Rate_Config 0
//      <o0>Rx Clock
//          <1=> S0BRG (S0RCK=1)
//          <0=> SYSCLK/TIMER/S1BRG (S0RCK=0)(Default)
//          <i>Mode 3 S0BRG (S0RCK=1)
//          <i>Clock source select S0BRG (S0RCK=1) .
#define URT0_Mode3_9Bit_Receive_Clock 0x01
//      <o0.2>Tx Clock
//          <1=> S0BRG (S0TCK=1)
//          <0=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <i>Mode 3 S0BRG (S0TCK=1)
//          <i>Clock source select S0BRG (S0TCK=1) .
#define URT0_Mode3_9Bit_Transmit_Clock 0x04
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB (S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode3_9Bit_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (SMOD2=0)(Default)  <1=>Double Baud Rate (SMOD2=1)
//          <i> Defaule baud date (SMOD2=0).
//          <i> Double baud rate (SMOD2=1).
#define URT0_Mode3_9Bit_Baud_Rate 0
//      <o0.6>Source <0=> SYSCLK/12 (S0TX12=0)(Default) <1=> SYSCLK/1 (S0TX12=1)
//          <i>SYSCLK/12 (S0TX12=0).
//          <i>SYSCLK/1  (S0TX12=1).
#define URT0_Mode3_9Bit_Clock_Source 0
//      <o>Baud Rate Reload (0~255) <0-255>
#define URT0_Mode3_9Bit_Baud_Rate_Reload_Count 0
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=0.
//          <i>**********     	    32         	     	  12x(256-S0BRT)
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=1.
//          <i>**********     	    32         	     	  1x(256-S0BRT)
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 3 9Bit Enable
//===========================================================
//      <e>Mode 4 Spi Master Enable
//===========================================================
//          <i>Mode 4 spi master enable .
//          <i>No support CPOL now.
#define URT0_Mode4_Spi_Master 0
//      <e>Baud Rate Config Enable
#define URT0_Mode4_Spi_Master_Baud_Rate_Config 0
//      <o.0> Time Base of Baud Rate from SYSCLK
//          <0=> SYSCLK/12 (URM0X3=0)(Default)
//          <1=> SYSCLK/4 (URM0X3=1)
//          <i> Note : baud rate from SYSCLK/4 or SYSCLK/12(S0TCK=0) .
#define URT0_Mode4_Spi_Master_Time_Base_SYSCLK 0
//      <o0.2>Tx Clock
//          <0=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <1=> S0BRG (S0TCK=1)
//          <i>Baud rate from SYSCLK/4 or SYSCLK/12 (S0TCK=0).
//          <i>Baud rate from S0BRG (S0TCK=1) .
#define URT0_Mode4_Spi_Master_Transmit_Clock 0x00
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB (S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode4_Spi_Master_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (SMOD2=0)(Default) <1=>Double Baud Rate (SMOD2=1)
//          <i> Note : baud rate from S0BRG(S0TCK=1) .
//          <i> Defaule baud date (SMOD2=0).
//          <i> Double baud rate (SMOD2=1).
#define URT0_Mode4_Spi_Master_Baud_Rate 0
//      <o0.6>Source <0=> SYSCLK/12 (S0TX12=0)(Default) <1=> SYSCLK/1 (S0TX12=1)
//          <i> Note : baud rate from S0BRG(S0TCK=1) .
//          <i>SYSCLK/12 (S0TX12=0).
//          <i>SYSCLK/1  (S0TX12=1).
#define URT0_Mode4_Spi_Master_Clock_Source 0
//      <o>Baud Rate Reload (0~255) <0-255>
#define URT0_Mode4_Spi_Master_Baud_Rate_Reload_Count 0
//          <i> Note : baud rate from S0BRG(S0TCK=1) .
//          <i>**********     	2*2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=0.
//          <i>**********     	    16         	     	  12x(256-S0BRT)
//          <i>**********     	2*2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=1.
//          <i>**********     	    16         	     	  1x(256-S0BRT)
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode4 Spi Master
//===========================================================
//      <e>Mode 5 Lin Bus Enable
//===========================================================
//          <i>Mode 5 lin bus enable .
#define URT0_Mode5_Lin_Bus 0
//      <e>Baud Rate Config Enable
#define URT0_Mode5_Lin_Bus_Baud_Rate_Config 0
//      <o0>Rx Clock
//          <0=> S0BRG (S0RCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0RCK=0)(Default)
//          <i>Mode 1 S0BRG (S0RCK=1)
//          <i>Clock source select S0BRG (S0RCK=1) .
#define URT0_Mode5_Lin_Bus_Receive_Clock 0x00
//      <o0.2>Tx Clock
//          <0=> S0BRG (S0TCK=1)
//          <1=> SYSCLK/TIMER/S1BRG (S0TCK=0)(Default)
//          <i>Mode 1 S0BRG (S0TCK=1)
//          <i>Clock source select S0BRG (S0TCK=1) .
#define URT0_Mode5_Lin_Bus_Transmit_Clock 0x00
//      <o0.3>Data Order <0=> MSB (S0DOR=0) <1=> LSB (S0DOR=1)(Default)
//          <i> Data order select MSB (S0DOR=0) or LSB (S0DOR=1).
#define URT0_Mode5_Lin_Bus_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (SMOD2=0)(Default) <1=>Double Baud Rate (SMOD2=1)
//          <i> Defaule baud date (SMOD2=0).
//          <i> Double baud rate (SMOD2=1).
#define URT0_Mode5_Lin_Bus_Baud_Rate 0
//      <o0.6>Source <0=> SYSCLK/12 (S0TX12=0)(Default) <1=> SYSCLK/1 (S0TX12=1)
//          <i> baud rate clock source
//          <i>SYSCLK/12 (S0TX12=0).
//          <i>SYSCLK/1  (S0TX12=1).
#define URT0_Mode5_Lin_Bus_Clock_Source 0
//      <o>Baud Rate Reload (0~255) <0-255>
#define URT0_Mode5_Lin_Bus_Baud_Rate_Reload_Count 0
//          <i> baud rate reload
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=0.
//          <i>**********     	    32         	     	  12x(256-S0BRT)
//          <i>**********     	  2^SMOD2      	     	     Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S0TX12=1.
//          <i>**********     	    32         	     	  1x(256-S0BRT)
//      <o>Lin Bus Break Length <0=> 13BIT (S0SB16=0)(Default) <1=> 16BIT (S0SB16=1)
//          <i> Select 13-bit sync-break .(S0SB16=0).
//          <i> Select 16-bit sync-break . (S0SB16=1).
#define URT0_Mode5_Lin_Bus_Break_Length 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 5 Lin Bus Enable
//      </e>  //UART0 Enable
//      </e>  //Advanced Mode
//      </e>  //Serial Port 0 (UART0)

//Macro for wizard constant option
#define UART0_SELECT0 UART0_8BIT_CONFIG0
#define UART0_SELECT1 UART0_8BIT_CONFIG1
#define UART0_SELECT2 UART0_8BIT_CONFIG2
#define UART0_SELECT3 UART0_8BIT_CONFIG3
#define UART0_SELECT4 UART0_8BIT_CONFIG4
#define UART0_SELECT5 UART0_8BIT_CONFIG5
#define UART0_SELECT6 UART0_8BIT_CONFIG6
#define UART0_SELECT7 UART0_8BIT_CONFIG7
#define UART0_SELECT8 UART0_8BIT_CONFIG8
#define UART0_SELECT9 UART0_8BIT_CONFIG9
#define UART0_SELECT10 UART0_8BIT_CONFIG10
#define UART0_SELECT11 UART0_8BIT_CONFIG11
#define UART0_SELECT12 UART0_8BIT_CONFIG12
#define UART0_SELECT13 UART0_8BIT_CONFIG13
#define UART0_SELECT14 UART0_8BIT_CONFIG14
#define UART0_SELECT15 UART0_8BIT_CONFIG15
#define UART0_SELECT16 UART0_8BIT_CONFIG16
#define UART0_SELECT17 UART0_8BIT_CONFIG17
#define UART0_SELECT18 UART0_8BIT_CONFIG18
#define UART0_SELECT19 UART0_8BIT_CONFIG19
#define UART0_SELECT20 UART0_8BIT_CONFIG20
#define UART0_SELECT21 UART0_8BIT_CONFIG21
#define UART0_SELECT22 UART0_8BIT_CONFIG22
#define UART0_SELECT23 UART0_8BIT_CONFIG23

/**
*******************************************************************************
* @brief        UART0 Easy Wizard
* @details      Initial UART0 Mode Baud Rate PinMux SerialReception
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_Easy_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Easy_Wizard_Init()\
    MWT(\
        __DRV_URT0_EasyDisableSerialReception(URT0_Easy_Select);\
        __DRV_URT0_Mode_Easy_Select(URT0_Easy_Select);\
        __DRV_URT0_BaudRate_Easy_Select(URT0_Easy_Select);\
        __DRV_URT0_PinMux_Easy_Select(URT0_Easy_Select);\
        __DRV_URT0_EasyEnableSerialReception(URT0_Easy_Select);\
    ;)


/**
*****************************************************************************
* @brief        UART0 Mode0 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_URT0_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode0_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode0_Shift_Data_Order);\
        __DRV_URT0_BaudRateX2_Select(URT0_Mode0_Shift_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT0_BaudRateReloadReg_Write(URT0_Mode0_Shift_Baud_Rate_Reload_Count);\
        __DRV_URT0_RXClockSource_Select(URT0_Mode0_Shift_Receive_Clock);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode0_Shift_Transmit_Clock);\
        __DRV_URT0_BaudRateGeneratorClock_Select(URT0_Mode0_Shift_Clock_Source);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT0_SerialReception_Cmd(URT0_Mode0_Shift_Input_Output);\
    ;)


/**
*****************************************************************************
* @brief        UART0 Mode1 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_URT0_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode1_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode1_8Bit_Data_Order);\
        __DRV_URT0_BaudRateX2_Select(URT0_Mode1_8Bit_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT0_BaudRateReloadReg_Write(URT0_Mode1_8Bit_Baud_Rate_Reload_Count);\
        __DRV_URT0_RXClockSource_Select(URT0_Mode1_8Bit_Receive_Clock);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode1_8Bit_Transmit_Clock);\
        __DRV_URT0_BaudRateGeneratorClock_Select(URT0_Mode1_8Bit_Clock_Source);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT0_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*******************************************************************************
* @brief        UART0 Mode2 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode2_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode2_9Bit_Data_Order);\
        __DRV_URT0_BaudRateX2X4_Select(URT0_Mode2_9Bit_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_RXClockSource_Select(URT0_Mode2_9Bit_Receive_Clock);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode2_9Bit_Transmit_Clock);\
        __DRV_URT0_BaudRateDiv3_Cmd(URT0_Mode2_9Bit_Clock_Source);\
        __DRV_URT0_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*****************************************************************************
* @brief        UART0 Mode3 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_URT0_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode3_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode3_9Bit_Data_Order);\
        __DRV_URT0_BaudRateX2_Select(URT0_Mode3_9Bit_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT0_BaudRateReloadReg_Write(URT0_Mode3_9Bit_Baud_Rate_Reload_Count);\
        __DRV_URT0_RXClockSource_Select(URT0_Mode3_9Bit_Receive_Clock);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode3_9Bit_Transmit_Clock);\
        __DRV_URT0_BaudRateGeneratorClock_Select(URT0_Mode3_9Bit_Clock_Source);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT0_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*****************************************************************************
* @brief        UART0 Mode4 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_URT0_Mode4_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode4_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode4_Spi_Master_Data_Order);\
        __DRV_URT0_BaudRateSysclkDiv4_Cmd(URT0_Mode4_Spi_Master_Time_Base_SYSCLK);\
        __DRV_URT0_BaudRateX2_Select(URT0_Mode4_Spi_Master_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT0_BaudRateReloadReg_Write(URT0_Mode4_Spi_Master_Baud_Rate_Reload_Count);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode4_Spi_Master_Transmit_Clock);\
        __DRV_URT0_BaudRateGeneratorClock_Select(URT0_Mode4_Spi_Master_Clock_Source);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE);\
    ;)


/**
*****************************************************************************
* @brief        UART0 Mode5 Init from Wizard
* @details      Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_URT0_Mode5_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Mode5_Wizard_Init()\
    MWT(\
        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT0_Mode_Select(URT0_Mode);\
        __DRV_URT0_DataOrder_Select(URT0_Mode5_Lin_Bus_Data_Order);\
        __DRV_URT0_BaudRateX2_Select(URT0_Mode5_Lin_Bus_Baud_Rate);\
        __DRV_URT0_S0Control_Cmd(MW_ENABLE);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT0_BaudRateReloadReg_Write(URT0_Mode5_Lin_Bus_Baud_Rate_Reload_Count);\
        __DRV_URT0_RXClockSource_Select(URT0_Mode5_Lin_Bus_Receive_Clock);\
        __DRV_URT0_TXClockSource_Select(URT0_Mode5_Lin_Bus_Transmit_Clock);\
        __DRV_URT0_BaudRateGeneratorClock_Select(URT0_Mode5_Lin_Bus_Clock_Source);\
        __DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT0_Lin_Bus_BreakLength_Select(URT0_Mode5_Lin_Bus_Break_Length);\
        __DRV_URT0_SerialReception_Cmd(MW_ENABLE);\
    ;)

/// @endcond


//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Add system clock initial void function for code size.
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//  <e0>Initialize Serial Port 1 (UART1)
//      <i> Check this term, then DRV_UART1_Wizard_Init() will initialize UART1.
//      <i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_UART1_WIZARD      0
//-----------------------------------------------------------------------------
//      <o>UART1
//          <0=>Easy Mode
//          <1=> Advanced Mode
//          <i>UART1 option select.
#define URT1_Option 0
//      <e>Easy Mode
//          <i>Easy mode enable.
#define URT1_Easy_Mode 0
//      <e>UART1 Enable
//          <i>Ready.
#define URT1_Easy 0
//      <o>Easy Select
//          <0x000003=>Select0(Default)
//          <0x010003=>Select1
//          <0x000103=>Select2
//          <0x010103=>Select3
//          <0x000203=>Select4
//          <0x010203=>Select5
//          <0x020303=>Select6
//          <0x030303=>Select7
//          <0x000303=>Select8
//          <0x010303=>Select9
//          <0x020903=>Select10
//          <0x030903=>Select11
//          <0x000403=>Select12
//          <0x010403=>Select13
//          <0x020a03=>Select14
//          <0x030a03=>Select15
//          <0x000a03=>Select16
//          <0x010a03=>Select17
//          <0x000603=>Select18
//          <0x010603=>Select19
//          <0x000703=>Select20
//          <0x010703=>Select21
//          <0x000803=>Select22
//          <0x010803=>Select23

#define URT1_Easy_Select 0x000003
//          <i>Note:Please confirm clock source before you select parameter.

//          <i>Select0 :	4800	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select1 :	4800	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select2 :	9600	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select3 :	9600	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select4 :	19200	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select5 :	19200	8BIt	S1BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz

//          <i>Select6 :	38400	8Bit	S1BRG	SYSCLK/1  	Default Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select7 :	38400	8Bit	S1BRG	SYSCLK/1  	Default Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select8 :	38400	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select9 :	38400	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select10:	57600	8Bit	S1BRG	SYSCLK/1  	Default Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 11.0592Mhz

//          <i>Select11:	57600	8Bit	S1BRG	SYSCLK/1  	Default Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 11.0592Mhz
//          <i>Select12:	57600	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select13:	57600	8Bit	S1BRG	SYSCLK/1  	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both  	IHRCO 12Mhz
//          <i>Select14:	115200	8Bit	S1BRG	SYSCLK/1	Default Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select15:	115200	8Bit	S1BRG	SYSCLK/1	Default Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select16:	115200	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select17:	115200	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz

//          <i>Select18:	230400	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select19:	230400	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 11.0592Mhz
//          <i>Select20:	250000	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 12Mhz
//          <i>Select21:	250000	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 12Mhz
//          <i>Select22:	750000	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	LSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 12Mhz
//          <i>Select23:	750000	8Bit	S1BRG	SYSCLK/1	Double Baud Rate 	MSB	Pin Select	Rx=P34 Tx=P35	Tx Rx Both	IHRCO 12Mhz

//      </e>  //UART1 Enable
//      </e>  //Easy Mode

//      <e>Advanced Mode
//          <i>Advanced mode enable .
#define URT1_Advance_Mode 0
//      <e>UART1 Enable
//          <i>Mode0 Shift .
//          <i>Mode1 8bit .
//          <i>Mode2 9bit .
//          <i>Mode3 9bit .
//          <i>Mode4 Spi Master .
#define URT1_Advance 0
//      <o>Mode
//          <0x00=>Mode 0 Shift (SM31=0,SM01=0,SM11=0)(Default)
//          <0x01=>Mode 1 8Bit (SM31=0,SM01=0,SM11=1)
//          <0x02=>Mode 2 9Bit (SM31=0,SM01=1,SM11=0)
//          <0x03=>Mode 3 9Bit (SM31=0,SM01=1,SM11=1)
//          <0x04=>Mode 4 Spi Master (SM31=1,SM01=0,SM11=0)
//          <i> Mode0~mode4 select.
#define URT1_Mode  0x00
//===========================================================
//      <e> Mode 0 Shift Enable
//===========================================================
//          <i>Mode 0 shift enable.
#define URT1_Mode0_Shift 0
//      <e> Baud Rate Config Enable
#define URT1_Mode0_Shift_Baud_Rate_Config 0
//      <o0.3>Data Order <0=> MSB (S1DOR=0) <1=> LSB (S1DOR=1)(Default)
//          <i> Data order select MSB (S1DOR=0) or LSB (S1DOR=1).
#define URT1_Mode0_Shift_Data_Order 0x08
//          <o0.6>Source <0=>SYSCLK/12 (S1M0X3=0)(Default) <1=>SYSCLK/4 (S1M0X3=1)
//          <i>SYSCLK/12 (S1M0X3=0).
//          <i>SYSCLK/4 (S1M0X3=1).
#define URT1_Mode0_Shift_Clock_Source 0
//      <o>Shift In/Out <0=> Out (REN1=0)(Default) <1=> In (REN1=1)
//      <i>Reception is initiated by the condition REN1=1 and RI1=0.
//      <i>Transmission is initiated by any instruction that uses S1BUF as a destination register.
//      <i>After REN1 on/off , clear RI1
#define URT1_Mode0_Shift_Input_Output 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 1 Shift Enable
//===========================================================
//      <e>Mode 1 8Bit Enable
//===========================================================
//          <i>Mode 1 8bit enable.
#define URT1_Mode1_8Bit 0
//      <e>Baud rate config enable
#define URT1_Mode1_8Bit_Baud_Rate_Config 0
//      <o0.3>Data Order <0=> MSB (S1DOR=0) <1=> LSB (S1DOR=1)(Default)
//          <i> Data order select MSB (S1DOR=0) or LSB (S1DOR=1).
#define URT1_Mode1_8Bit_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (S1MOD1=0)(Default) <1=>Double Baud Rate (S1MOD1=1)
//          <i> Default baud rate (S1MOD1=0).
//          <i> Double  baud rate (S1MOD1=1).
#define URT1_Mode1_8Bit_Baud_Rate 0
//      <o0.6>Source <0=>SYSCLK/12 (S1TX12=0)(Default) <1=>SYSCLK/1 (S1TX12=1)
//          <i>S1BRG clock source SYSCLK/12 (S1TX12=0) .
//          <i>S1BRG clock source SYSCLK/1  (S1TX12=1) .
#define URT1_Mode1_8Bit_Clock_Source 0
//      <o>Baud rate reload (0~255) <0-255>
#define URT1_Mode1_8Bit_Baud_Rate_Reload_Count 0
//          <i>**********     	  2^S1MOD1      	     	    Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S1TX12=0.
//          <i>**********     	    32         	     	  12x(256-S1BRT)
//          <i>**********     	  2^S1MOD1      	     	    Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S1TX12=1.
//          <i>**********     	    32         	     	  1x(256-S1BRT)
//      </e>  //Baud rate config enable
//      </e>  //Mode 1 8Bit Enable
//===========================================================
//      <e> Mode 2 9Bit Enable
//===========================================================
//          <i>Mode 2 9bit enable.
#define URT1_Mode2_9Bit 0
//      <e> Baud Rate Config Enable
#define URT1_Mode2_9Bit_Baud_Rate_Config 0
//          <i>BaudRate    	RxClock     	TxClock     	EnhanceBaudRate     	Source      	SYSCLK/n
//          <i>172800      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/1    	SYSCLK/64     	IHRCO 11.0592MHZ
//          <i>345600      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate    	SYSCLK/1    	SYSCLK/32     	IHRCO 11.0592MHZ
//          <i>57600      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/3    	SYSCLK/192    	IHRCO 11.0592MHZ
//          <i>115200     	SYSCLK/2    	SYSCLK/2    	Double Baud Rate    	SYSCLK/3    	SYSCLK/96     	IHRCO 11.0592MHZ
//          <i>187500      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/1    	SYSCLK/64     	IHRCO 12MHZ
//          <i>375000      	SYSCLK/2    	SYSCLK/2    	Double Baud Rate    	SYSCLK/1    	SYSCLK/32     	IHRCO 12MHZ
//          <i>62500      	SYSCLK/2    	SYSCLK/2    	Default Baud Rate     	SYSCLK/3    	SYSCLK/192    	IHRCO 12MHZ
//          <i>125000     	SYSCLK/2    	SYSCLK/2    	Double Baud Rate    	SYSCLK/3    	SYSCLK/92     	IHRCO 12MHZ
//      <o0.3>Data Order <0=> MSB (S1DOR=0) <1=> LSB (S1DOR=1)(Default)
//          <i> Data order select MSB (S1DOR=0) or LSB (S1DOR=1).
#define URT1_Mode2_9Bit_Data_Order 0x08
//      <o0.4..5>Enhance Baud Rate
//          <0=>Default Baud Rate (S1MOD1=0)(Default)
//          <1=>Double Baud Rate (S1MOD1=1)
//          <i>Default baud rate (S1MOD1=0).
//          <i>Default baud rate (S1MOD1=1).
#define URT1_Mode2_9Bit_Baud_Rate 0
//          <o0.6>Source <0=>SYSCLK/1 (S1M0X3=0)(Default) <1=>SYSCLK/3 (S1M0X3=1)
//          <i>SYSCLK/1 (S1M0X3=0).
//          <i>SYSCLK/3 (S1M0X3=1).
#define URT1_Mode2_9Bit_Clock_Source 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 2 9Bit Enable
//===========================================================
//      <e>Mode 3 9Bit Enable
//===========================================================
//          <i>Mode 3 9bit enable.
#define URT1_Mode3_9Bit 0
//      <e>Baud rate config enable
#define URT1_Mode3_9Bit_Baud_Rate_Config 0
//      <o0.3>Data Order <0=> MSB (S1DOR=0) <1=> LSB (S1DOR=1)(Default)
//          <i> Data order select MSB (S1DOR=0) or LSB (S1DOR=1).
#define URT1_Mode3_9Bit_Data_Order 0x08
//      <o0.4>Enhance Baud Rate	<0=>Default Baud Rate (S1MOD1=0)(Default) <1=>Double Baud Rate (S1MOD1=1)
//          <i> Default baud rate (S1MOD1=0).
//          <i> Double  baud rate (S1MOD1=1).
#define URT1_Mode3_9Bit_Baud_Rate 0
//      <o0.6>Source <0=>SYSCLK/12 (S1TX12=0)(Default) <1=>SYSCLK/1 (S1TX12=1)
//          <i>S1BRG clock source SYSCLK/12 (S1TX12=0) .
//          <i>S1BRG clock source SYSCLK/1  (S1TX12=1) .
#define URT1_Mode3_9Bit_Clock_Source 0
//      <o>Baud rate reload (0~255) <0-255>
#define URT1_Mode3_9Bit_Baud_Rate_Reload_Count 0
//          <i>**********     	  2^S1MOD1      	     	    Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S1TX12=0.
//          <i>**********     	    32         	     	  12x(256-S1BRT)
//          <i>**********     	  2^S1MOD1      	     	    Fsysclk
//          <i>Baud Rate =    	-----------    	X    	----------------    	;S1TX12=1.
//          <i>**********     	    32         	     	  1x(256-S1BRT)
//      </e>  //Baud rate config enable
//      </e>  //Mode 3 9Bit Enable
//===========================================================
//      <e> Mode 4 SPI Master Enable
//===========================================================
//          <i>Mode 4 spi master enable.
//          <i>No support CPOL now.
#define URT1_Mode4_Spi_Master 0
//      <e> Baud Rate Config Enable
#define URT1_Mode4_Spi_Master_Baud_Rate_Config 0
//      <o0.3>Data Order <0=> MSB (S1DOR=0) <1=> LSB (S1DOR=1)(Default)
//          <i> Data order select MSB (S1DOR=0) or LSB (S1DOR=1).
#define URT1_Mode4_Spi_Master_Data_Order 0x08
//          <o0.6>Source <0=>SYSCLK/12 (S1M0X3=0)(Default) <1=>SYSCLK/4 (S1M0X3=1)
//          <i>SYSCLK/12 (S1M0X3=0).
//          <i>SYSCLK/4 (S1M0X3=1).
#define URT1_Mode4_Spi_Master_Clock_Source 0
//      </e>  //Baud Rate Config Enable
//      </e>  //Mode 4 SPI Master Enable

//      </e>  //UART1 Enable
//      </e>  //Advanced Mode
//      </e>  //Serial Port 1 (UART1)
//Macro for wizard constant option
#define UART1_SELECT0 UART1_8BIT_CONFIG0
#define UART1_SELECT1 UART1_8BIT_CONFIG1
#define UART1_SELECT2 UART1_8BIT_CONFIG2
#define UART1_SELECT3 UART1_8BIT_CONFIG3
#define UART1_SELECT4 UART1_8BIT_CONFIG4
#define UART1_SELECT5 UART1_8BIT_CONFIG5
#define UART1_SELECT6 UART1_8BIT_CONFIG6
#define UART1_SELECT7 UART1_8BIT_CONFIG7
#define UART1_SELECT8 UART1_8BIT_CONFIG8
#define UART1_SELECT9 UART1_8BIT_CONFIG9
#define UART1_SELECT10 UART1_8BIT_CONFIG10
#define UART1_SELECT11 UART1_8BIT_CONFIG11
#define UART1_SELECT12 UART1_8BIT_CONFIG12
#define UART1_SELECT13 UART1_8BIT_CONFIG13
#define UART1_SELECT14 UART1_8BIT_CONFIG14
#define UART1_SELECT15 UART1_8BIT_CONFIG15
#define UART1_SELECT16 UART1_8BIT_CONFIG16
#define UART1_SELECT17 UART1_8BIT_CONFIG17
#define UART1_SELECT18 UART1_8BIT_CONFIG18
#define UART1_SELECT19 UART1_8BIT_CONFIG19
#define UART1_SELECT20 UART1_8BIT_CONFIG20
#define UART1_SELECT21 UART1_8BIT_CONFIG21
#define UART1_SELECT22 UART1_8BIT_CONFIG22
#define UART1_SELECT23 UART1_8BIT_CONFIG23
/**
*******************************************************************************
* @brief       UART1 Easy Wizard
* @details     Config UART1 (Mode) (Baud Rate) (PinMux) (SerialReception)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Easy_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Easy_Wizard_Init()\
    MWT(\
        __DRV_URT1_EasyDisableSerialReception(URT1_Easy_Select);\
        __DRV_URT1_Mode_Easy_Select(URT1_Easy_Select);\
        __DRV_URT1_BaudRate_Easy_Select(URT1_Easy_Select);\
        __DRV_URT1_PinMux_Easy_Select(URT1_Easy_Select);\
        __DRV_URT1_EasyEnableSerialReception(URT1_Easy_Select);\
    ;)


/**
*******************************************************************************
* @brief        UART1 Mode0 Init from Wizard
* @details     Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Mode0_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Mode0_Wizard_Init()\
    MWT(\
        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT1_Mode_Select(URT1_Mode);\
        __DRV_URT1_DataOrder_Select(URT1_Mode0_Shift_Data_Order);\
        __DRV_URT1_BaudRateDiv3_Cmd(URT1_Mode0_Shift_Clock_Source);\
        __DRV_URT1_SerialReception_Cmd(URT1_Mode0_Shift_Input_Output);\
    ;)


/**
*******************************************************************************
* @brief        UART1 Mode1 Init from Wizard
* @details     Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Mode1_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Mode1_Wizard_Init()\
    MWT(\
        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT1_Mode_Select(URT1_Mode);\
        __DRV_URT1_DataOrder_Select(URT1_Mode1_8Bit_Data_Order);\
        __DRV_URT1_BaudRateX2_Select(URT1_Mode1_8Bit_Baud_Rate);\
        __DRV_URT1_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT1_BaudRateReloadReg_Write(URT1_Mode1_8Bit_Baud_Rate_Reload_Count);\
        __DRV_URT1_BaudRateGeneratorClock_Select(URT1_Mode1_8Bit_Clock_Source);\
        __DRV_URT1_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT1_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*******************************************************************************
* @brief        UART1 Mode2 Init from Wizard.
* @details     Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Mode2_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Mode2_Wizard_Init()\
    MWT(\
        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT1_Mode_Select(URT1_Mode);\
        __DRV_URT1_DataOrder_Select(URT1_Mode2_9Bit_Data_Order);\
        __DRV_URT1_BaudRateX2_Select(URT1_Mode2_9Bit_Baud_Rate);\
        __DRV_URT1_BaudRateDiv3_Cmd(URT1_Mode2_9Bit_Clock_Source);\
        __DRV_URT1_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*******************************************************************************
* @brief        UART1 Mode3 Init from Wizard
* @details     Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Mode3_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Mode3_Wizard_Init()\
    MWT(\
        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT1_Mode_Select(URT1_Mode);\
        __DRV_URT1_DataOrder_Select(URT1_Mode3_9Bit_Data_Order);\
        __DRV_URT1_BaudRateX2_Select(URT1_Mode3_9Bit_Baud_Rate);\
        __DRV_URT1_BaudRateGenerator_Cmd(MW_DISABLE);\
        __DRV_URT1_BaudRateReloadReg_Write(URT1_Mode3_9Bit_Baud_Rate_Reload_Count);\
        __DRV_URT1_BaudRateGeneratorClock_Select(URT1_Mode3_9Bit_Clock_Source);\
        __DRV_URT1_BaudRateGenerator_Cmd(MW_ENABLE);\
        __DRV_URT1_SerialReception_Cmd(MW_ENABLE);\
    ;)


/**
*******************************************************************************
* @brief        UART1 Mode4 Init from Wizard
* @details     Initial (SerialReception) (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return      None
* @note        None
* @par         Example
* @code
                __DRV_URT1_Mode4_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT1_Mode4_Wizard_Init()\
    MWT(\
        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);\
        __DRV_URT1_Mode_Select(URT1_Mode);\
        __DRV_URT1_DataOrder_Select(URT1_Mode4_Spi_Master_Data_Order);\
        __DRV_URT1_BaudRateDiv3_Cmd(URT1_Mode4_Spi_Master_Clock_Source);\
    ;)




//@del{
void UART_test(void);
//@del}
/// @endcond


//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Add system clock initial void function for code size.
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//  <e0>Initialize Serial Peripheral Interface
//      <i> Check this term, then DRV_SPI_Wizard_Init() will initialize SPI.
//      <i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_SPI_WIZARD      0
//-----------------------------------------------------------------------------
//      <o>SPI
//          <0=>Easy Mode
//          <1=> Advanced Mode
//          <i>SPI option select.
#define SPI_Option 0
//      <e>Easy Mode
//          <i>Easy mode enable.
#define SPI_Easy_Mode 0
//      <e>SPI Enable
//          <i>Ready.
#define SPI_Easy 0
//      <o>Easy Select
//          <0x010180=>Select0(Default)
//          <0x010181=>Select1
//          <0x011180=>Select2
//          <0x011181=>Select3
//          <0x012180=>Select4
//          <0x012181=>Select5
//          <0x013180=>Select6
//          <0x013181=>Select7
//          <0x000280=>Select8
//          <0x000281=>Select9
//          <0x001280=>Select10
//          <0x001281=>Select11
//          <0x002280=>Select12
//          <0x002281=>Select13
//          <0x003280=>Select14
//          <0x003281=>Select15
//          <0x010300=>Select16
//          <0x010301=>Select17
//          <0x010310=>Select18
//          <0x010311=>Select19
//          <0x010320=>Select20
//          <0x010321=>Select21
//          <0x010330=>Select22
//          <0x010331=>Select23
//          <0x010340=>Select24
//          <0x010341=>Select25
//          <0x010350=>Select26
//          <0x010351=>Select27
//          <0x010400=>Select28
//          <0x010401=>Select29
//          <0x010410=>Select30
//          <0x010411=>Select31
//          <0x010420=>Select32
//          <0x010421=>Select33
//          <0x010430=>Select34
//          <0x010431=>Select35
//          <0x010440=>Select36
//          <0x010441=>Select37
//          <0x010450=>Select38
//          <0x010451=>Select39
#define SPI_Easy_Select 0x010180
//          <i>Note:Please confirm clock source before you select parameter.

//          <i>Select0	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select1	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select2	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=0 CPHA=1 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select3	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=0 CPHA=1 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select4	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=1 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select5	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=1 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select6	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=1 CPHA=1 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select7	:	Slave Select By nSS , Model Control : Normal , Data Order : LSB ,Clock Rate : Edge , Data Mode : CPOL=1 CPHA=1 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).

//          <i>Select8	:	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=0 CPHA=0 , Pin : nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select9	:	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=0 CPHA=0 , Pin : nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select10 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=0 CPHA=1 , Pin : nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select11 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=0 CPHA=1 , Pin : nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select12 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=1 CPHA=0 , Pin : nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select13 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=1 CPHA=0 , Pin : nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select14 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=1 CPHA=1 , Pin : nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select15 :	Slave Select By MSTR , Model Control : Normal , Data Order : MSB , Clock Rate : Edge , Data Mode : CPOL=1 CPHA=1 , Pin : nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).

//          <i>Select16 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/4 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select17 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/4 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select18 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/8 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select19 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/8 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select20 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/16 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select21 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/16 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select22 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/32 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select23 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/32 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select24 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/64 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select25 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/64 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select26 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/2 , Data Mode:CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select27 :	Master Select By nSS , Model Control : Normal , Data Order : LSB , Clock Rate : SYSCLK/2 , Data Mode:CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).

//          <i>Select28 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/4 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select29 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/4 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select30 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/8 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select31 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/8 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select32 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/16 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select33 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/16 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select34 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/32 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select35 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/32 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select36 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/64 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select37 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/64 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).
//          <i>Select38 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/2 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P33) MOSI(P15) MISO(P16) SPICLK(P17).
//          <i>Select39 :	Master Select By MSTR , Model Control : Normal , Data Order:LSB , Clock Rate : SYSCLK/2 , Data Mode : CPOL=0 CPHA=0 , Pin:nSS(P17) MOSI(P35) MISO(P34) SPICLK(P33).


//      </e>  //SPI Enable
//      </e>  //Easy Mode

//      <e>Advanced Mode
//          <i>Advanced mode enable .
#define SPI_Advance_Mode 0
//      <e>SPI Enable
//          <i>Slave mode ready.
//          <i>Master mode ready.
#define SPI_Advance 0
//      <o>Mode
//          <0=>Slave(Default)
//          <1=>Master
//          <i> Slave/master mode select
#define SPI_Mode 0
//      <o>Daisy-Chain Connection
//          <0=>Normal (SPI0M0=0)(Default)
//          <1=>Daisy-Chain Connection (SPI0M0=1)
#define SPI_Daisy_Chain_Connection 0
//===========================================================
//      <e>Slave Mode Enable
//===========================================================
//          <i>Slave Mode enable .
//          <i>Note: gpio wizard p33 nSS p15 MOSI p16 MISO p17 SPICLK or p17 nSS p35 MOSI p34 MISO p33 SPICLK select .
#define SPI_Slave_Mode 0
//      <o> Slave Mode
//          <0x00=> SPI Disable (SPEN=0,SSIG=0,MSTR=0)(Default)
//          <0x01=> Slave Select by nSS (SPEN=1,SSIG=0,MSTR=0)
//          <0x02=> Slave Select by MSTR (SPEN=1,SSIG=1,MSTR=0)
#define SPI_Slave_Mode_Initial 0x00
//      <o>Data Mode
//          <0x00=> Mode0 (CPOL=0,CPHA=0)
//          <0x01=> Mode1 (CPOL=0,CPHA=1)(Default)
//          <0x02=> Mode2 (CPOL=1,CPHA=0)
//          <0x03=> Mode3 (CPOL=1,CPHA=1)
//          <i>Clock Phase Bit (CPHA) allows the user to set the edges for sampling and changing data.
//          <i>The Clock Polarity bit, CPOL, allows the user to set the clock polarity. The following figures show the different settings of Clock Phase Bit, CPHA.
#define SPI_Slave_Data_Mode_CPOL_CPHA 0x01
//      <o>Data Order <0=> MSB (DORD=0)(Default) <1=> LSB (DORD=1)
//          <i> Data order select MSB (DORD=0) or LSB (DORD=1).
#define SPI_Slave_Data_Order 0
//      </e>  //Slave Mode Enable
//===========================================================
//      <e>Master Mode Enable
//===========================================================
//          <i>Master Mode enable .
//          <i>Note: gpio wizard p33 nSS p15 MOSI p16 MISO p17 SPICLK or p17 nSS p35 MOSI p34 MISO p33 SPICLK select .
#define SPI_Master_Mode 0
//      <o> Master Mode
//          <0x00=> SPI Disable (SPEN=0,SSIG=0,MSTR=0)(Default)
//          <0x03=> Master Dual Role Select by nSS (SPEN=1,SSIG=0,MSTR=1)
//          <0x04=> Master Select by MSTR (SPEN=1,SSIG=1,MSTR=1)
#define SPI_Master_Mode_Initial 0x00
//      <o>Clock Rate
//          <0x00=> SYSCLK/4 (SPR2=0,SPR1=0,SPR0=0)(Default)
//          <0x01=> SYSCLK/8 (SPR2=0,SPR1=0,SPR0=1)
//          <0x02=> SYSCLK/16 (SPR2=0,SPR1=1,SPR0=0)
//          <0x03=> SYSCLK/32 (SPR2=0,SPR1=1,SPR0=1)
//          <0x04=> SYSCLK/64 (SPR2=1,SPR1=0,SPR0=0)
//          <0x05=> SYSCLK/2 (SPR2=1,SPR1=0,SPR0=1)
//          <i>Note:SYSCLK is the system clock.
//          <i>ClockRate		SYSCLK/n	SPR2	SPR1	SPR0
//          <i>3Mhz   			SYSCLK/4 	 0   	 0   	 0	IHRCO 12MHZ
//          <i>1.5Mhz 			SYSCLK/8 	 0   	 0   	 1	IHRCO 12MHZ
//          <i>750Khz 			SYSCLK/16	 0   	 1   	 0	IHRCO 12MHZ
//          <i>375Khz 			SYSCLK/32	 0   	 1   	 1	IHRCO 12MHZ
//          <i>187.5hz			SYSCLK/64	 1   	 0   	 0	IHRCO 12MHZ
//          <i>6Mhz   			SYSCLK/2 	 1   	 0   	 1	IHRCO 12MHZ
//          <i>12Mhz  			SYSCLK/4 	 0   	 0   	 0	IHRCO 48MHZ
//          <i>6Mhz   			SYSCLK/8 	 0   	 0   	 1	IHRCO 48MHZ
//          <i>3Mhz   			SYSCLK/16	 0   	 1   	 0	IHRCO 48MHZ
//          <i>1.5Mhz 			SYSCLK/32	 0   	 1   	 1	IHRCO 48MHZ
//          <i>750Khz 			SYSCLK/64	 1   	 0   	 0	IHRCO 48MHZ
//          <i>24Mhz  			SYSCLK/2 	 1   	 0   	 1	IHRCO 48MHZ(Vdd 5V)
#define SPI_Master_Clock_Rate_Config 0x00
//      <o>Data Mode
//          <0x00=> Mode0 (CPOL=0,CPHA=0)
//          <0x01=> Mode1 (CPOL=0,CPHA=1)(Default)
//          <0x02=> Mode2 (CPOL=1,CPHA=0)
//          <0x03=> Mode3 (CPOL=1,CPHA=1)
//          <i>Clock Phase Bit (CPHA) allows the user to set the edges for sampling and changing data.
//          <i>The Clock Polarity bit, CPOL, allows the user to set the clock polarity. The following figures show the different settings of Clock Phase Bit, CPHA.
#define SPI_Master_Data_Mode_CPOL_CPHA 0x01

//      <o>Data Order <0=> MSB (DORD=0)(Default) <1=> LSB (DORD=1)
//          <i> Data order select MSB (DORD=0) or LSB (DORD=1).
#define SPI_Master_Data_Order 0
//      </e>  //Master Mode Enable

//      </e>  //SPI Enable
//      </e>  //Advanced Mode
//      </e>  //Initialize Serial Peripheral Interface

//Macro for wizard constant option
#define SPI_SELECT0 SPI_CONFIG0
#define SPI_SELECT1 SPI_CONFIG1
#define SPI_SELECT2 SPI_CONFIG2
#define SPI_SELECT3 SPI_CONFIG3
#define SPI_SELECT4 SPI_CONFIG4
#define SPI_SELECT5 SPI_CONFIG5
#define SPI_SELECT6 SPI_CONFIG6
#define SPI_SELECT7 SPI_CONFIG7
#define SPI_SELECT8 SPI_CONFIG8
#define SPI_SELECT9 SPI_CONFIG9
#define SPI_SELECT10 SPI_CONFIG10
#define SPI_SELECT11 SPI_CONFIG11
#define SPI_SELECT12 SPI_CONFIG12
#define SPI_SELECT13 SPI_CONFIG13
#define SPI_SELECT14 SPI_CONFIG14
#define SPI_SELECT15 SPI_CONFIG15
#define SPI_SELECT16 SPI_CONFIG16
#define SPI_SELECT17 SPI_CONFIG17
#define SPI_SELECT18 SPI_CONFIG18
#define SPI_SELECT19 SPI_CONFIG19
#define SPI_SELECT20 SPI_CONFIG20
#define SPI_SELECT21 SPI_CONFIG21
#define SPI_SELECT22 SPI_CONFIG22
#define SPI_SELECT23 SPI_CONFIG23
#define SPI_SELECT24 SPI_CONFIG24
#define SPI_SELECT25 SPI_CONFIG25
#define SPI_SELECT26 SPI_CONFIG26
#define SPI_SELECT27 SPI_CONFIG27
#define SPI_SELECT28 SPI_CONFIG28
#define SPI_SELECT29 SPI_CONFIG29
#define SPI_SELECT30 SPI_CONFIG30
#define SPI_SELECT31 SPI_CONFIG31
#define SPI_SELECT32 SPI_CONFIG32
#define SPI_SELECT33 SPI_CONFIG33
#define SPI_SELECT34 SPI_CONFIG34
#define SPI_SELECT35 SPI_CONFIG35
#define SPI_SELECT36 SPI_CONFIG36
#define SPI_SELECT37 SPI_CONFIG37
#define SPI_SELECT38 SPI_CONFIG38
#define SPI_SELECT39 SPI_CONFIG39
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Initial SPI , Mode , Clock Rate , PinMux , Daisy-Chain , Data Order.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Easy_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Easy_Wizard_Init()\
    MWT(\
        __DRV_SPI_Mode_Easy_Select(SPI_Easy_Select);\
        __DRV_SPI_Clock_Rate_Easy_Select(SPI_Easy_Select);\
        __DRV_SPI_Data_Mode_Easy_Select(SPI_Easy_Select);\
        __DRV_SPI_Data_Order_Easy_Select(SPI_Easy_Select);\
        __DRV_SPI_PinMux_Easy_Select(SPI_Easy_Select);\
        __DRV_SPI_Daisy_Chain_Easy_Select(SPI_Easy_Select);\
    ;)
/**
*****************************************************************************
* @brief        SPI Slave Init from Wizard
* @details      Initial (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_SPI_Slave_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Slave_Wizard_Init()\
    MWT(\
        __DRV_SPI_Mode_Select(SPI_Slave_Mode_Initial);\
        __DRV_SPI_Data_Mode_Select(SPI_Slave_Data_Mode_CPOL_CPHA);\
        __DRV_SPI_DataOrder_Select(SPI_Slave_Data_Order);\
        __DRV_SPI_Model_Control_Cmd(SPI_Daisy_Chain_Connection);\
    ;)
/**
*****************************************************************************
* @brief        SPI Master Init from Wizard
* @details      Initial (MODE) (Data Order) (Baud Rate) (Clock Source)
* @return       None
* @note         None
* @par          Example
* @code
                 __DRV_SPI_Master_Wizard_Init()
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Master_Wizard_Init()\
    MWT(\
        __DRV_SPI_Mode_Select(SPI_Master_Mode_Initial);\
        __DRV_SPI_Data_Mode_Select(SPI_Master_Data_Mode_CPOL_CPHA);\
        __DRV_SPI_DataOrder_Select(SPI_Master_Data_Order);\
        __DRV_SPI_Model_Control_Cmd(SPI_Daisy_Chain_Connection);\
        __DRV_SPI_Clock_Rate_Select(SPI_Master_Clock_Rate_Config);\
    ;)
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Two Wire Serial Interface (TWI0/I2C0)
//<i> Check this term, then will initialize I2C0 interface.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_I2C0_WIZARD 1

//  <o0> Select Config Mode
//  <i> This only changes the parameters of "__DRV_I2C0_Wizard_Init();"
//      <1=> Easy Mode
//      <2=> Advanced Mode
    #define DRV_I2C0_Wizard_Mode 2

//  <e0> Easy Mode
//  <i> There are two Transceiver function can be used as following
//  <i> "Sample_I2C0_Master_Byte_Write(I2C0_Address, I2C0_Data)"
//  <i> "Sample_I2C0_Master_Byte_Read(I2C0_Address)"
    #define DRV_I2C0_Wizard_Easy_Check 0

//      <O0>  Master Transceiver Clock Rate
//      <i> Easy select clock by "__DRV_I2C0_Easy_ClockRate_Select(__SELECT__);"
//      <i> Users please note that the system clock must be set to 12MHz or 24MHz
//          <0x80=> 100K (SYSCLK = 12M)
//          <0x81=> 100K (SYSCLK = 24M)
//          <0x02=> 400K (SYSCLK = 12M)
//          <0x03=> 400K (SYSCLK = 24M)
            #define DRV_I2C0_Easy_ClockRate_Config 0x80
//  </e> Easy Mode

//  <e0> Advanced Mode
//  <i> The setting only change the parameter of "__DRV_I2C0_Wizard_Init ();"
    #define DRV_I2C0_Wizard_Advanced_Check 1


//      <e0> Master Mode
        #define DRV_I2C0_Master_Mode_En 0
//          <O0> Basic Clock Rate Select
//          <i> Select OSCin by "__DRV_I2C0_ClockRate_Select(__SELECT__);"
//              <0x00=> SYSCLK/8 (CR2=0,CR1=0,CR0=0)(Default)
//              <0x01=> SYSCLK/16 (CR2=0,CR1=0,CR0=1)
//              <0x02=> SYSCLK/32 (CR2=0,CR1=1,CR0=0)
//              <0x03=> SYSCLK/64 (CR2=0,CR1=1,CR0=1)
//              <0x80=> SYSCLK/128 (CR2=1,CR1=0,CR0=0)
//              <0x81=> SYSCLK/256 (CR2=1,CR1=0,CR0=1)
//              <0x82=> S0TOF/6 (CR2=1,CR1=1,CR0=0)
//              <0x83=> T0OF/6 (CR2=1,CR1=1,CR0=1)
            #define DRV_I2C0_Basic_ClockRate_Config 0x00
//      </e> IHRCO Enable/Disable


//      <e0> Slave Mode
        #define DRV_I2C0_Slave_Mode_En 1
//          <q0> General Call Function Select (GC)
//          <i> Select Slave Address General Call Function Enable or Disable by "__DRV_I2C0_GC_Select(__SELECT__);"
            #define DRV_I2C0_General_Call 0
//          <O0> Slave Address Assignation (SIADR) <0x00-0xff:0x02>
//          <i> Users only need to enter even numbers, from 0x00~0xFF
            #define DRV_I2C0_Slave_Address 0x46
//      </e>
//  </e>    Advanced Mode End

//</e> Initialize Two Wire Serial Interface End
/// @endcond __DRV_Wizard_Without_Doxygen

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Alan_20200529 //bugNum_Authour_Date
 * >> Add beeper initial void function for code size.
 * #1.00_Alan_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Beeper
//<i> Check this term, then macro __DRV_BEEPER_Wizard_Init() will initialize beeper.
//<i> Warning: Enable beeper will disable OCD ICE function.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_BEEPER_WIZARD    0

//  <o0> Beeper Output Selection
//  <i> Select beeper output.
//  <0=> P44 (BPOC1 = 0, BPOC0 = 0)(Default)      <1=> ILRCO/32 (BPOC1 = 0, BPOC0 = 1)
//  <2=> ILRCO/16 (BPOC1 = 1, BPOC0 = 0)          <3=> ILRCO/8 (BPOC1 = 1, BPOC0 = 1)
        #define AUXR3_BPOCx      0
//</e> Beeper END

/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * #1.00_Kevin_20200710 //bugNum_Authour_Date 
 * #1.01_Kevin_20200824 //bugNum_Authour_Date
 * >> Change parameter KBI_PINx_SELECT to __DRV_KBI_PortPinMask_Wizard_Config()
 * >> Change parameter KBI_PINx_PATTERN_TYPE to __DRV_KBI_Pattern_Wizard_Config()
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
/**
 *******************************************************************************
 * @brief       Set KBI pattern base on Wizard
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_Pattern_Wizard_Config();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Pattern_Wizard_Config()\
    MWT(\
        (KBI_PIN0_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_0):(KBPATN &= ~GF_0));\
        (KBI_PIN1_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_1):(KBPATN &= ~GF_1));\
        (KBI_PIN2_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_2):(KBPATN &= ~GF_2));\
        (KBI_PIN3_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_3):(KBPATN &= ~GF_3));\
        (KBI_PIN4_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_4):(KBPATN &= ~GF_4));\
        (KBI_PIN5_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_5):(KBPATN &= ~GF_5));\
        (KBI_PIN6_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_6):(KBPATN &= ~GF_6));\
        (KBI_PIN7_PATTERN_TYPE==MW_ENABLE?(KBPATN |= GF_7):(KBPATN &= ~GF_7));\
    )

/**
 *******************************************************************************
 * @brief       Set KBI Port Pin Mask on Wizard
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_PortPinMask_Wizard_Config();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_PortPinMask_Wizard_Config()\
    MWT(\
        (KBI_PIN0_SELECT==MW_ENABLE?(KBMASK |= GF_0):(KBMASK &= ~GF_0));\
        (KBI_PIN1_SELECT==MW_ENABLE?(KBMASK |= GF_1):(KBMASK &= ~GF_1));\
        (KBI_PIN2_SELECT==MW_ENABLE?(KBMASK |= GF_2):(KBMASK &= ~GF_2));\
        (KBI_PIN3_SELECT==MW_ENABLE?(KBMASK |= GF_3):(KBMASK &= ~GF_3));\
        (KBI_PIN4_SELECT==MW_ENABLE?(KBMASK |= GF_4):(KBMASK &= ~GF_4));\
        (KBI_PIN5_SELECT==MW_ENABLE?(KBMASK |= GF_5):(KBMASK &= ~GF_5));\
        (KBI_PIN6_SELECT==MW_ENABLE?(KBMASK |= GF_6):(KBMASK &= ~GF_6));\
        (KBI_PIN7_SELECT==MW_ENABLE?(KBMASK |= GF_7):(KBMASK &= ~GF_7));\
    )

/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.06_Allen_20200528 //bugNum_Authour_Date
 * >> Split wizard.h
 * #1.00_Allen_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//  <e0>Initialize ADC
//      <i> Check this term, then DRV_ADC_Wizard_Init() will initialize ADC.
//      <i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_ADC_WIZARD        0

//  <o0> ADC Trigger Source
//  <i> ADC trigger mode selection.
//      <0x00=> Software (ADTM[3:0]=0000) (Default)
//      <0x01=> Timer 0 overflow (ADTM[3:0]=0001)
//      <0x02=> Free running mode (ADTM[3:0]=0010)
//      <0x03=> S0BRG overflow (ADTM[3:0]=0011)
//      <0x10=> KBIET (ADTM[3:0]=0100)
//      <0x11=> INT1ET (ADTM[3:0]=0101)
//      <0x12=> INT2ET (ADTM[3:0]=0110)
//      <0x20=> T2EXES (ADTM[3:0]=1000)
//      <0x22=> T3EXES (ADTM[3:0]=1010)
//      <0x30=> PCA0 overflow (ADTM[3:0]=1100)
//      <0x31=> C0CMP6 (ADTM[3:0]=1101)
//      <0x32=> C0CMP6 or C0CMP7 (ADTM[3:0]=1110)
    #define __ADC_TRG__     0x0000

//  <o0> ADC Input Channel
//  <i> Input channel selection for ADC analog multiplexer.
//      <0x00=> AIN0 (ACHS=0,CHS[2:0]=000) (Default)
//      <0x01=> AIN1 (ACHS=0,CHS[2:0]=001)
//      <0x02=> AIN2 (ACHS=0,CHS[2:0]=010)
//      <0x03=> AIN3 (ACHS=0,CHS[2:0]=011)
//      <0x04=> AIN4 (ACHS=0,CHS[2:0]=100)
//      <0x05=> AIN5 (ACHS=0,CHS[2:0]=101)
//      <0x06=> AIN6 (ACHS=0,CHS[2:0]=110)
//      <0x07=> AIN7 (ACHS=0,CHS[2:0]=111)
//      <0x08=> Internal Vref 1.4V (ACHS=1,CHS[2:0]=000)
//      <0x09=> VSS (ACHS=1,CHS[2:0]=001)
    #define __ADC_CHL__     0x00

//  <o0> ADC Clock Source
//  <i> ADC conversion clock select.
//  <i> ADC Sample rate = ADC Clock Source/30.
//      <0x00=> SYSCLK DIV 1 (ADCKS[2:0]=000)(Default)
//      <0x20=> SYSCLK DIV 2 (ADCKS[2:0]=001)
//      <0x40=> SYSCLK DIV 4 (ADCKS[2:0]=010)
//      <0x60=> SYSCLK DIV 8 (ADCKS[2:0]=011)
//      <0x80=> SYSCLK DIV 16 (ADCKS[2:0]=100)
//      <0xA0=> SYSCLK DIV 32 (ADCKS[2:0]=101)
//      <0xC0=> S0BRG overflow DIV 2 (ADCKS[2:0]=110)
//      <0xE0=> Timer 2 overflow DIV 2 (ADCKS[2:0]=111)
    #define __ADC_CLKS__    0x00

//  <o0> ADC Data Alignment
//  <i> Left justified : The most significant 8 bits of conversion result are saved in ADCDH[7:0], while the least significant 4 bits in ADCDL[7:4].
//  <i> Right justified : The most significant 4 bits of conversion result are saved in ADCDH[3:0], while the least significant 8 bits in ADCDL[7:0].
//      <0x00=> Left justified (ADRJ=0)(Default)
//      <0x10=> Right justified (ADRJ=1)
    #define __ADC_DATAALIGNMENT__   0x00

//  <e0> ADC Window Detect
//  <i>Configuration ADC window detect.
    #define __WINDOW_DETECT_EN__    0

//      <o0> Window Detect Mode
//      <i> ADC value
//      <i>____4095
//      <i>|   | Outside
//      <i>|   |<----- High Boundary
//      <i>|   | Inside
//      <i>|   |<----- Low Boundary
//      <i>|   | Outside
//      <i>|__| 0
//          <0x00=> Inside (ADWM0=0)(Default)
//          <0x40=> Outside (ADWM0=1)
        #define __ADC_WD_MODE__     0x00

//      <o0> High Boundary Value is (1~4095)<1-4095>
//      <i>ADC Window detect high boundary value
//      <i>High boundary value cannot be lower than low boundary value.
        #define __ADC_WD_HB__       4095

//      <o0> Low Boundary Value is (0~4094)<0-4094>
//      <i>ADC Window detect low boundary value
//      <i>Low boundary value cannot be higher than high boundary value.
        #define __ADC_WD_LB__       0
//  </e> ADC Window Detect End

//  <e0> ADC Scan Loop
//  <i> Configuration ADC scan loop pin.
    #define __SCAN_LOOP_EN__    0

//      <q0.0> AIN0 Enable
//      <q0.1> AIN1 Enable
//      <q0.2> AIN2 Enable
//      <q0.3> AIN3 Enable
//      <q0.4> AIN4 Enable
//      <q0.5> AIN5 Enable
//      <q0.6> AIN6 Enable
//      <q0.7> AIN7 Enable
        #define SCAN_LOOP_PIN   0x00
//  </e>

//</e> ADC End

/**
 *******************************************************************************
 * @brief	    Initialize the ADC
 * @details
 * @return 	  None
 * @note
 * @par         Example
 * @code
 *    __DRV_ADC_Wizard_Init();
 * @endcode
 * @bug
 *******************************************************************************
 */
#if (__WINDOW_DETECT_EN__==0)&(__SCAN_LOOP_EN__==0)
#define   __DRV_ADC_Wizard_Init()\
      MWT(\
        __DRV_ADC_Enable();\
        __DRV_ADC_TriggerSource_Select(__ADC_TRG__);\
        __DRV_ADC_DataAlignment_Select(__ADC_DATAALIGNMENT__);\
        __DRV_ADC_ClockSource_Select(__ADC_CLKS__);\
        __DRV_ADC_PinMux_Select(__ADC_CHL__);\
        )
#elif (__WINDOW_DETECT_EN__==1)&(__SCAN_LOOP_EN__==0)
#define   __DRV_ADC_Wizard_Init()\
      MWT(\
        __DRV_ADC_Enable();\
        __DRV_ADC_TriggerSource_Select(__ADC_TRG__);\
        __DRV_ADC_DataAlignment_Select(__ADC_DATAALIGNMENT__);\
        __DRV_ADC_ClockSource_Select(__ADC_CLKS__);\
        __DRV_ADC_PinMux_Select(__ADC_CHL__);\
        __DRV_ADC_WindowDetectMode_Select(__ADC_WD_MODE__);\
        __DRV_ADC_SetHigherThreshold(__ADC_WD_HB__);\
        __DRV_ADC_SetLowerThreshold(__ADC_WD_LB__);\
        )
#elif (__WINDOW_DETECT_EN__==0)&(__SCAN_LOOP_EN__==1)
#define   __DRV_ADC_Wizard_Init()\
      MWT(\
        __DRV_ADC_Enable();\
        __DRV_ADC_TriggerSource_Select(__ADC_TRG__);\
        __DRV_ADC_DataAlignment_Select(__ADC_DATAALIGNMENT__);\
        __DRV_ADC_ClockSource_Select(__ADC_CLKS__);\
        __DRV_ADC_PinMux_Select(__ADC_CHL__);\
        __DRV_ScanLoopChannel_Config(SCAN_LOOP_PIN,MW_ENABLE);\
        )
#elif (__WINDOW_DETECT_EN__==1)&(__SCAN_LOOP_EN__==1)
#define   __DRV_ADC_Wizard_Init()\
      MWT(\
        __DRV_ADC_Enable();\
        __DRV_ADC_TriggerSource_Select(__ADC_TRG__);\
        __DRV_ADC_DataAlignment_Select(__ADC_DATAALIGNMENT__);\
        __DRV_ADC_ClockSource_Select(__ADC_CLKS__);\
        __DRV_ADC_PinMux_Select(__ADC_CHL__);\
        __DRV_ADC_WindowDetectMode_Select(__ADC_WD_MODE__);\
        __DRV_ADC_SetHigherThreshold(__ADC_WD_HB__);\
        __DRV_ADC_SetLowerThreshold(__ADC_WD_LB__);\
        __DRV_ScanLoopChannel_Config(SCAN_LOOP_PIN,MW_ENABLE);\
        )
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Alan_20200529 //bugNum_Authour_Date
 * >> Add IAP_Boundary initial void function for code size.
 * #1.00_Alan_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Application-Programming(IAP)
//<i> Check this term, then you can customize the IAP boundary on your demands.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_IAP_WIZARD        0

//  <o> IAP Boundary
//  <i> Set up IAP boundary which must be an even unsigned character(uint8_t).
//  <i> Further, a legal IAP boundary msut not be higher than ISP start address.
    #define IAP_BOUNDARY            0x36
//</e> In-Application-Programming(IAP) END

#if MG82F6D17_IAP_WIZARD
void DRV_IAP_Boundary_Wizard_Init(void);
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 * #1.01_Kevin_20200901 //bugNum_Authour_Date
 * >> Added DMA DCF0/TF5/TF6 Interrupt Enable/Disable Item 
 ******************************************************************************
 */
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
//*****************************************************************************
//<e0> Initialize Global Interrupts
//<i> Check this term if you want to enable interrupts.
//<i> Then, please go to MG82F6D17_INTERRUPT.h to pick the interrupts you need.
//<i> Now DRV_INT_Wizard_Init() will help you initialize them.
//<i> Date: 2020/07/10     Version: 1.00
#define MG82F6D17_INT_ALL_WIZARD    1
//-----------------------------------------------------------------------------
//  <e0> External Interrupt 0
//  <i> External interrupt 0 enable.
//  <i> We also provide further setting details for External Interrupt 0, please go to MG82F6D17_INV_DRV.h.
    #define MG82F6D17_INT0_EN               0

//      <o0> nINT0 Priority
//          <0x0000=> 0 (PX0H=0, PX0L=0)(Lowest)(Default)
//          <0x0001=> 1 (PX0H=0, PX0L=1)
//          <0x0100=> 2 (PX0H=1, PX0L=0)
//          <0x0101=> 3 (PX0H=1, PX0L=1)(Highest)
        #define MG82F6D17_INT0_PRIORITY     0x0000

//      <o0.0..15> nINT0 Trigger Type Select
//      <i> The setting only change the parameter of "__DRV_INT0_TriggerType_Select(__SELECT__);"
//          <0x0000=> Low Level (IT0=0, INT0H=0)(Default)
//          <0x0001=> High Level (IT0=0, INT0H=1)
//          <0x0100=> Falling Edge (IT0=1, INT0H=0)
//          <0x0101=> Rising Edge (IT0=1, INT0H=1)
        #define nINT0_TRIGGER_TYPE          0x0000

//      <o0.0..15> nINT0 FilterMode Type Select
//      <i> The setting only change the parameter of "__DRV_INT0_FilterMode_Select(__SELECT__);"
//          <0x0000=> Disable (X0FLT1=0, X0FLT=0)(Default)
//          <0x0001=> SYSCLK X 3 (X0FLT1=0, X0FLT=1)
//          <0x0100=> SYSCLK/6 X 3 (X0FLT1=1, X0FLT=0)
//          <0x0101=> S0TOF X 3 (X0FLT1=1, X0FLT=1)
        #define nINT0_FILTER_MODE_TYPE      0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Timer 0 Interrupt
//  <i> Timer 0 interrupt enable.
    #define MG82F6D17_TIMER0_IT_EN          1

//      <o0> Timer0 Priority
//          <0x0000=> 0 (PT0H=0, PT0L=0)(Lowest)(Default)
//          <0x0002=> 1 (PT0H=0, PT0L=1)
//          <0x0200=> 2 (PT0H=1, PT0L=0)
//          <0x0202=> 3 (PT0H=1, PT0L=1)(Highest)
        #define MG82F6D17_TIMER0_PRIORITY   0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> External Interrupt 1
//  <i> External interrupt 1 enable.
//  <i> We also provide further setting details for External Interrupt 1, please go to MG82F6D17_INV_DRV.h.
    #define MG82F6D17_INT1_EN               1

//      <o0> nINT1 Priority
//          <0x0000=> 0 (PX1H=0, PX1L=0)(Lowest)(Default)
//          <0x0004=> 1 (PX1H=0, PX1L=1)
//          <0x0400=> 2 (PX1H=1, PX1L=0)
//          <0x0404=> 3 (PX1H=1, PX1L=1)(Highest)
        #define MG82F6D17_INT1_PRIORITY     0x0000

//      <o0.0..15> nINT1 Trigger Type Select
//      <i> The setting only change the parameter of "__DRV_INT1_TriggerType_Select(__SELECT__);"
//          <0x0000=> Low Level (IT1=0, INT1H=0)(Default)
//          <0x0002=> High Level (IT1=0, INT1H=1)
//          <0x0400=> Falling Edge (IT1=1, INT1H=0)
//          <0x0402=> Rising Edge (IT1=1, INT1H=1)
        #define nINT1_TRIGGER_TYPE          0x0400

//      <o0.0..15> nINT1 FilterMode Type Select
//      <i> The setting only change the parameter of "__DRV_INT1_FilterMode_Select(__SELECT__);"
//          <0x0000=> Disable (X1FLT1=0, X1FLT=0)(Default)
//          <0x0002=> SYSCLK X 3 (X1FLT1=0, X1FLT=1)
//          <0x0200=> SYSCLK/6 X 3 (X1FLT1=1, X1FLT=0)
//          <0x0202=> S0TOF X 3 (X1FLT1=1, X1FLT=1)
        #define nINT1_FILTER_MODE_TYPE      0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Timer 1 Interrupt
//  <i> Timer 1 interrupt enable.
        #define MG82F6D17_TIMER1_IT_EN      1

//      <o0> Timer1 Priority
//          <0x0000=> 0 (PT1H=0, PT1L=0)(Lowest)(Default)
//          <0x0008=> 1 (PT1H=0, PT1L=1)
//          <0x0800=> 2 (PT1H=1, PT1L=0)
//          <0x0808=> 3 (PT1H=1, PT1L=1)(Highest)
        #define MG82F6D17_TIMER1_PRIORITY   0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Serial Port 0
//  <i> Serial Port 0 enable.
    #define MG82F6D17_S0_IT_EN              0

//      <o0> Serial Port 0 Priority
//          <0x0000=> 0 (PS0H=0, PS0L=0)(Lowest)(Default)
//          <0x0010=> 1 (PS0H=0, PS0L=1)
//          <0x1000=> 2 (PS0H=1, PS0L=0)
//          <0x1010=> 3 (PS0H=1, PS0L=1)(Highest)
        #define MG82F6D17_S0_PRIORITY       0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Timer 2 Interrupt
//  <i> Timer 2 interrupt enable.
    #define MG82F6D17_TIMER2_IT_EN          0

//      <o0> Timer2 Priority
//          <0x0000=> 0 (PT2H=0, PT2L=0)(Lowest)(Default)
//          <0x0020=> 1 (PT2H=0, PT2L=1)
//          <0x2000=> 2 (PT2H=1, PT2L=0)
//          <0x2020=> 3 (PT2H=1, PT2L=1)(Highest)
        #define MG82F6D17_TIMER2_PRIORITY   0x0000
        
//      <q0> TF2L
//      <i> Timer2 TL2 overflow interrupt enable.
        #define MG82F6D17_TIMER2_TF2L_EN       0

//  </e>
//-----------------------------------------------------------------------------
//  <e0> External Interrupt 2
//  <i> External interrupt 2 enable.
//  <i> We also provide further setting details for External Interrupt 2, please go to MG82F6D17_INV_DRV.h.
    #define MG82F6D17_INT2_EN               0

//      <o0> nINT2 Priority
//          <0x0000=> 0 (PX2H=0, PX2L=0)(Lowest)(Default)
//          <0x0040=> 1 (PX2H=0, PX2L=1)
//          <0x4000=> 2 (PX2H=1, PX2L=0)
//          <0x4040=> 3 (PX2H=1, PX2L=1)(Highest)
        #define MG82F6D17_INT2_PRIORITY     0x0000
        
//      <o0> nINT2 Trigger Type Select
//      <i> The setting only change the parameter of "__DRV_INT2_TriggerType_Select(__SELECT__);"
//          <0x00=> Low Level (IT2=0, INT2H=0)(Default)
//          <0x08=> High Level (IT2=0, INT2H=1)
//          <0x01=> Falling Edge (IT2=1, INT2H=0)
//          <0x09=> Rising Edge (IT2=1, INT2H=1)
        #define nINT2_TRIGGER_TYPE          0x00

//      <o2.0..15> nINT2 FilterMode Type Select
//      <i> The setting only change the parameter of "__DRV_INT2_FilterMode_Select(__SELECT__);"
//          <0x0000=> Disable (X2FLT1=0, X2FLT=0)(Default)
//          <0x0004=> SYSCLK X 3 (X2FLT1=0, X2FLT=1)
//          <0x0400=> SYSCLK/6 X 3 (X2FLT1=1, X2FLT=0)
//          <0x0404=> S0TOF X 3 (X2FLT1=1, X2FLT=1)
        #define nINT2_FILTER_MODE_TYPE      0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> SPI Interrupt
//  <i> SPI interrupt enable.
    #define MG82F6D17_SPI_IT_EN             0

//      <o0> SPI Priority
//          <0x0000=> 0 (PSPIH=0, PSPIL=0)(Lowest)(Default)
//          <0x0001=> 1 (PSPIH=0, PSPIL=1)
//          <0x0100=> 2 (PSPIH=1, PSPIL=0)
//          <0x0101=> 3 (PSPIH=1, PSPIL=1)(Highest)
        #define MG82F6D17_SPI_PRIORITY      0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> ADC Interrupt
//  <i> ADC interrupt enable.
    #define MG82F6D17_ADC_IT_EN             0

//      <o0> ADC Priority
//          <0x0000=> 0 (PADCH=0, PADCL=0)(Lowest)(Default)
//          <0x0002=> 1 (PADCH=0, PADCL=1)
//          <0x0200=> 2 (PADCH=1, PADCL=0)
//          <0x0202=> 3 (PADCH=1, PADCL=1)(Highest)
        #define MG82F6D17_ADC_PRIORITY      0x0000

//      <q0> ADCI
//      <i> ADC conversion interrupt enable.
        #define MG82F6D17_ADC_ADCI_EN       0

//      <q0> SMPF
//      <i> ADC channel sample and hold interrupt enable.
        #define MG82F6D17_ADC_SMPF_EN       0

//      <q0> ADCWI
//      <i> ADC Window campare mode interrupt enable.
        #define MG82F6D17_ADC_ADCWI_EN      0
//  </e>
//-----------------------------------------------------------------------------
//  <e0> PCA Interrupt
//  <i> PCA interrupt enable.
    #define MG82F6D17_PCA_IT_EN            0

//      <o0> PCA Priority
//          <0x0000=> 0 (PPCAH=0, PPCAL=0)(Lowest)(Default)
//          <0x0004=> 1 (PPCAH=0, PPCAL=1)
//          <0x0400=> 2 (PPCAH=1, PPCAL=0)
//          <0x0404=> 3 (PPCAH=1, PPCAL=1)(Highest)
        #define MG82F6D17_PCA_PRIORITY     0x0000
        
//      <q0> Timer/Counter  
//      <i> PCA Timer/Counter interrupt enable.
        #define MG82F6D17_PCA_COUNTER_EN       0        
        
//      <q0> Module 0  
//      <i> PCA Module 0 interrupt enable.
        #define MG82F6D17_PCA_MODULE0_EN       0        
        
//      <q0> Module 1  
//      <i> PCA Module 1 interrupt enable.
        #define MG82F6D17_PCA_MODULE1_EN       0 

//      <q0> Module 2  
//      <i> PCA Module 2 interrupt enable.
        #define MG82F6D17_PCA_MODULE2_EN       0 

//      <q0> Module 3  
//      <i> PCA Module 3 interrupt enable.
        #define MG82F6D17_PCA_MODULE3_EN       0 

//      <q0> Module 4  
//      <i> PCA Module 4 interrupt enable.
        #define MG82F6D17_PCA_MODULE4_EN       0 

//      <q0> Module 5  
//      <i> PCA Module 5 interrupt enable.
        #define MG82F6D17_PCA_MODULE5_EN       0

//      <q0> Module 6  
//      <i> PCA Module 6 interrupt enable.
        #define MG82F6D17_PCA_MODULE6_EN       0
        
//      <q0> Module 7  
//      <i> PCA Module 7 interrupt enable.
        #define MG82F6D17_PCA_MODULE7_EN       0  
                
//  </e>
//-----------------------------------------------------------------------------
//  <e0> System Flag Interrupt
//  <i> System Flag interrupt enable.
    #define MG82F6D17_SYSFLAG_EN            0
//      <o0> System Flag Priority
//          <0x0000=> 0 (PSFH=0, PSFL=0)(Lowest)(Default)
//          <0x0008=> 1 (PSFH=0, PSFL=1)
//          <0x0800=> 2 (PSFH=1, PSFL=0)
//          <0x0808=> 3 (PSFH=1, PSFL=1)(Highest)
        #define MG82F6D17_PSF_PRIORITY      0x0000

//      <q0> WDT Interrupt
//      <i> WDT interrupt enable.
        #define MG82F6D17_WDT_IT_EN         0

//      <q0> RTC Interrupt
//      <i> RTC interrupt enable.
        #define MG82F6D17_RTC_IT_EN         0

//      <q0> BOD0 Interrupt
//      <i> BOD0 interrupt enable.
        #define MG82F6D17_BOD0_IT_EN        0

//      <q0> BOD1 Interrupt
//      <i> BOD1 interrupt enable.
        #define MG82F6D17_BOD1_IT_EN        0
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Keypad Interrupt
//  <i> Keypad interrupt enable.
    #define MG82F6D17_KBI_EN                0
//      <o0> KBI Priority
//          <0x0000=> 0 (PKBH=0, PKBL=0)(Lowest)(Default)
//          <0x0020=> 1 (PKBH=0, PKBL=1)
//          <0x2000=> 2 (PKBH=1, PKBL=0)
//          <0x2020=> 3 (PKBH=1, PKBL=1)(Highest)
        #define MG82F6D17_KBI_PRIORITY          0x0000

//      <o0> KBI Matching Polarity Select
//      <i> The setting only change the parameter of "__DRV_KBI_PatternMatchingType_Select(__SELECT__);"
//          <0x00=> Not equal (PATN_SEL=0)(Default)
//          <0x02=> Equal (PATN_SEL=1)
        #define KBI_PATTERN_MATCHING_SELECTION  0x00

//      <o0> KBI TriggerType Select
//      <i> The setting only change the parameter of "__DRV_KBI_TriggerType_Select(__SELECT__);"
//          <0x00=> Level (KBES=0)(Default)
//          <0x20=> Edge (KBES=1)
        #define KBI_TRIGGER_TYPE                0x00

//      <o0> KBI FilterMode Select
//      <i> The setting only change the parameter of "__DRV_KBI_FilterMode_Select(KBI_FILTER_MODE_TYPE);"
//          <0x00=> Disabled (KBCS1~0=00)(Default)
//          <0x40=> SYSCLK x 3 (KBCS1~0=01)
//          <0x80=> SYSCLK/6 x 3 (KBCS1~0=10)
//          <0xC0=> S0TOF x 3 (KBCS1~0=11)
        #define KBI_FILTER_MODE_TYPE            0x00

//      <e0> KBI Pin0 Enable
//      <i> KBI Pin0 enable or disable by user option
        #define KBI_PIN0_SELECT                 0

//          <o0> KBI_Pin0 Pattern
//          <i> KBI pin0 pattern type select
//              <0x00=> 0 (KBPATN.0=0)
//              <0x01=> 1 (KBPATN.0=1)(Default)
            #define KBI_PIN0_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin1 Enable
//      <i> KBI Pin1 enable or disable by user option
        #define KBI_PIN1_SELECT                 0

//          <o0> KBI_Pin1 Pattern
//          <i> KBI pin1 pattern type select
//              <0x00=> 0 (KBPATN.1=0)
//              <0x01=> 1 (KBPATN.1=1)(Default)
            #define KBI_PIN1_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin2 Enable
//      <i> KBI Pin2 enable or disable by user option
        #define KBI_PIN2_SELECT                 0

//          <o0> KBI_Pin2 Pattern
//          <i> KBI pin2 pattern type select
//              <0x00=> 0 (KBPATN.2=0)
//              <0x01=> 1 (KBPATN.2=1)(Default)
            #define KBI_PIN2_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin3 Enable
//      <i> KBI Pin3 enable or disable by user option
        #define KBI_PIN3_SELECT                 0

//          <o0> KBI_Pin3 Pattern
//          <i> KBI pin3 pattern type select
//              <0x00=> 0 (KBPATN.3=0)
//              <0x01=> 1 (KBPATN.3=1)(Default)
            #define KBI_PIN3_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin4 Enable
//      <i> KBI Pin4 enable or disable by user option
        #define KBI_PIN4_SELECT                 0

//          <o0> KBI_Pin4 Pattern
//          <i> KBI pin4 pattern type select
//              <0x00=> 0 (KBPATN.4=0)
//              <0x01=> 1 (KBPATN.4=1)(Default)
            #define KBI_PIN4_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin5 Enable
//      <i> KBI Pin5 enable or disable by user option
        #define KBI_PIN5_SELECT                 0

//          <o0> KBI_Pin5 Pattern
//          <i> KBI pin5 pattern type select
//              <0x00=> 0 (KBPATN.5=0)
//              <0x01=> 1 (KBPATN.5=1)(Default)
            #define KBI_PIN5_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin6 Enable
//      <i> KBI Pin6 enable or disable by user option
        #define KBI_PIN6_SELECT                 0

//          <o0> KBI_Pin6 Pattern
//          <i> KBI pin6 pattern type select
//              <0x00=> 0 (KBPATN.6=0)
//              <0x01=> 1 (KBPATN.6=1)(Default)
            #define KBI_PIN6_PATTERN_TYPE       0x01
//      </e>

//      <e0> KBI Pin7 Enable
//      <i> KBI Pin7 enable or disable by user option
        #define KBI_PIN7_SELECT                 0

//          <o0> KBI_Pin7 Pattern
//          <i> KBI pin7 pattern type select
//              <0x00=> 0 (KBPATN.7=0)
//              <0x01=> 1 (KBPATN.7=1)(Default)
            #define KBI_PIN7_PATTERN_TYPE       0x01
//      </e>

//  </e> Keypad Interrupt END
//-----------------------------------------------------------------------------
//  <e0> I2C0 Interrupt
//  <i> "Important" User need to add file "MG82F6D17_I2C0_FLOW_CONTROL.c " to Source Group 1 when I2C0 interrupt enable.
//  <i> The void function "Sample_I2C0_ISR_Handle()" will be used.
    #define MG82F6D17_I2C0_IT_EN                1

//      <o0> I2C0 Priority
//          <0x0000=> 0 (PTWI0H=0, PTWI0L=0)(Lowest)(Default)
//          <0x0040=> 1 (PTWI0H=0, PTWI0L=1)
//          <0x4000=> 2 (PTWI0H=1, PTWI0L=0)
//          <0x4040=> 3 (PTWI0H=1, PTWI0L=1)(Highest)
        #define MG82F6D17_I2C0_PRIORITY         0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Serial Port 1
//  <i> Serial Port 1 enable.
    #define MG82F6D17_S1_IT_EN                  0

//      <o0> Serial Port 1 Priority
//          <0x0000=> 0 (PS1H=0, PS1L=0)(Lowest)(Default)
//          <0x0010=> 1 (PS1H=0, PS1L=1)
//          <0x1000=> 2 (PS1H=1, PS1L=0)
//          <0x1010=> 3 (PS1H=1, PS1L=1)(Highest)
        #define MG82F6D17_S1_PRIORITY           0x0000
//  </e>
//-----------------------------------------------------------------------------
//  <e0> Timer 3 Interrupt
//  <i> Timer 3 interrupt enable.
    #define MG82F6D17_TIMER3_IT_EN              0

//      <o0> Timer3 Priority
//          <0x0000=> 0 (PT3H=0, PT3L=0)(Lowest)(Default)
//          <0x0001=> 1 (PT3H=0, PT3L=1)
//          <0x0100=> 2 (PT3H=1, PT3L=0)
//          <0x0101=> 3 (PT3H=1, PT3L=1)(Highest)
        #define MG82F6D17_TIMER3_PRIORITY      0x0000
        
//      <q0> TF3L
//      <i> Timer3 TL3 overflow interrupt enable.
        #define MG82F6D17_TIMER3_TF3L_EN       0
//  </e>
//-----------------------------------------------------------------------------
//  <e0> DMA Interrupt
//  <i> DMA interrupt enable.
    #define MG82F6D17_DMA_IT_EN                 0

//      <o0> DMA Priority
//          <0x00=> 0 (PDMAH=0, PDMAL=0)(Lowest)(Default)
//          <0x40=> 1 (PDMAH=0, PDMAL=1)
//          <0x80=> 2 (PDMAH=1, PDMAL=0)
//          <0xC0=> 3 (PDMAH=1, PDMAL=1)(Highest)
        #define MG82F6D17_DMA_PRIORITY          0x00
        
//      <q0> DCF0 Interrupt
//      <i> DMA DCF0 interrupt enable.
        #define MG82F6D17_DMA_DCF0_EN       0

//      <q0> TF5 Interrupt
//      <i> DMA Timer5 TF5 interrupt enable.
        #define MG82F6D17_DMA_TF5_EN       0

//      <q0> TF6 Interrupt
//      <i> DMA Timer6 TF6 interrupt enable.
        #define MG82F6D17_DMA_TF6_EN      0
//  </e>
//</e> Global Interrupts END

/// @endcond


//*****************************************************************************
//<<< end of configuration section >>>


//@cond __DRV_Wizard_Without_Doxygen
void System_Wizard_Init(void);
//@endcond

