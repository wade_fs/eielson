#include "MG82F6D17_CONFIG.h"

/**
 *******************************************************************************
 * Serial Peripheral Interface Test Group
 *******************************************************************************
 */
void MG82F6D17_SPI_DRV_h(void)
{
    __DRV_SPI_PinMux_Select(SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17);
    __DRV_SPI_PinMux_Select(SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33);
}

void MG82F6D17_SPI_DRV_c(void)
{
    _nop_();
}


void MG82F6D17_SPI_MID_h(void)
{
    _nop_();
}

