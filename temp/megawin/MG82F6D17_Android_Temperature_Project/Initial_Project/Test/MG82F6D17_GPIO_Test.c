#include "MG82F6D17_CONFIG.h"

/**
 *******************************************************************************
 * GPIO Test Group
 *******************************************************************************
 */
void MG82F6D17_GPIO_DRV_h(void)
{
    uint8_t temp8;
    bit   temp1;

    //-------------------------------------//
    __DRV_GPIO_P1ModeSelect(P1ALL_InputOnly      );
    __DRV_GPIO_P1ModeSelect(P1ALL_OpenDrainPullUp);
    __DRV_GPIO_P1ModeSelect(P1ALL_OpenDrain      );
    __DRV_GPIO_P1ModeSelect(P1ALL_PushPull       );
    __DRV_GPIO_P1ModeSelect(P10_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P10_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P10_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P10_PushPull         );
    __DRV_GPIO_P1ModeSelect(P11_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P11_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P11_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P11_PushPull         );
    __DRV_GPIO_P1ModeSelect(P15_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P15_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P15_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P15_PushPull         );
    __DRV_GPIO_P1ModeSelect(P16_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P16_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P16_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P16_PushPull         );
    __DRV_GPIO_P1ModeSelect(P17_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P17_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P17_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P17_PushPull         );

    __DRV_GPIO_P1DeInit();

    __DRV_GPIO_WriteP1(P1,0x11);
    __DRV_GPIO_WriteP1(P10,0);
    __DRV_GPIO_WriteP1(P11,1);
    __DRV_GPIO_WriteP1(P15,0);
    __DRV_GPIO_WriteP1(P16,1);
    __DRV_GPIO_WriteP1(P17,0);

    __DRV_GPIO_ReadP1(temp8);
    temp1 = __DRV_GPIO_GetPin10();
    temp1 = __DRV_GPIO_GetPin11();
    temp1 = __DRV_GPIO_GetPin15();
    temp1 = __DRV_GPIO_GetPin16();
    temp1 = __DRV_GPIO_GetPin17();

    __DRV_GPIO_InversePinP1(P1 );
    __DRV_GPIO_InversePinP1(P10);
    __DRV_GPIO_InversePinP1(P11);
    __DRV_GPIO_InversePinP1(P15);
    __DRV_GPIO_InversePinP1(P16);
    __DRV_GPIO_InversePinP1(P17);

    __DRV_GPIO_SetP1FastDriving(P1PinALL,NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin0,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin1,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin5,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin6,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin7,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1PinALL,FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin0,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin1,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin5,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin6,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin7,  FAST);


    //-------------------------------------//
    __DRV_GPIO_P2ModeSelect(P2ALL_InputOnly      );
    __DRV_GPIO_P2ModeSelect(P2ALL_OpenDrainPullUp);
    __DRV_GPIO_P2ModeSelect(P2ALL_OpenDrain      );
    __DRV_GPIO_P2ModeSelect(P2ALL_PushPull       );
    __DRV_GPIO_P2ModeSelect(P22_InputOnly        );
    __DRV_GPIO_P2ModeSelect(P22_OpenDrainPullUp  );
    __DRV_GPIO_P2ModeSelect(P22_OpenDrain        );
    __DRV_GPIO_P2ModeSelect(P22_PushPull         );
    __DRV_GPIO_P2ModeSelect(P24_InputOnly        );
    __DRV_GPIO_P2ModeSelect(P24_OpenDrainPullUp  );
    __DRV_GPIO_P2ModeSelect(P24_OpenDrain        );
    __DRV_GPIO_P2ModeSelect(P24_PushPull         );

    __DRV_GPIO_P2DeInit();

    __DRV_GPIO_WriteP2(P2,0x22);
    __DRV_GPIO_WriteP2(P22,1);
    __DRV_GPIO_WriteP2(P24,0);

    __DRV_GPIO_ReadP2(temp8);
    temp1 = __DRV_GPIO_GetPin22();
    temp1 = __DRV_GPIO_GetPin24();

    __DRV_GPIO_InversePinP2(P2 );
    __DRV_GPIO_InversePinP2(P22);
    __DRV_GPIO_InversePinP2(P24);

    __DRV_GPIO_SetP2FastDriving(P2PinALL,NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2Pin2,  NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2Pin4,  NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2PinALL,FAST);
    __DRV_GPIO_SetP2FastDriving(P2Pin2,  FAST);
    __DRV_GPIO_SetP2FastDriving(P2Pin4,  FAST);


    //-------------------------------------//
    __DRV_GPIO_P3ModeSelect(P3ALL_QuasiMode);
    __DRV_GPIO_P3ModeSelect(P3ALL_PushPull );
    __DRV_GPIO_P3ModeSelect(P3ALL_InputOnly);
    __DRV_GPIO_P3ModeSelect(P3ALL_OpenDrain);
    __DRV_GPIO_P3ModeSelect(P30_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P30_PushPull   );
    __DRV_GPIO_P3ModeSelect(P30_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P30_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P31_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P31_PushPull   );
    __DRV_GPIO_P3ModeSelect(P31_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P31_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P33_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P33_PushPull   );
    __DRV_GPIO_P3ModeSelect(P33_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P33_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P34_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P34_PushPull   );
    __DRV_GPIO_P3ModeSelect(P34_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P34_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P35_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P35_PushPull   );
    __DRV_GPIO_P3ModeSelect(P35_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P35_OpenDrain  );

    __DRV_GPIO_P3DeInit();

    __DRV_GPIO_WriteP3(P3,0x44);
    __DRV_GPIO_WriteP3(P30,0);
    __DRV_GPIO_WriteP3(P31,1);
    __DRV_GPIO_WriteP3(P33,0);
    __DRV_GPIO_WriteP3(P34,1);
    __DRV_GPIO_WriteP3(P35,0);

    __DRV_GPIO_ReadP3(temp8);
    temp1 = __DRV_GPIO_GetPin30();
    temp1 = __DRV_GPIO_GetPin31();
    temp1 = __DRV_GPIO_GetPin33();
    temp1 = __DRV_GPIO_GetPin34();
    temp1 = __DRV_GPIO_GetPin35();

    __DRV_GPIO_InversePinP3(P3 );
    __DRV_GPIO_InversePinP3(P30);
    __DRV_GPIO_InversePinP3(P31);
    __DRV_GPIO_InversePinP3(P33);
    __DRV_GPIO_InversePinP3(P34);
    __DRV_GPIO_InversePinP3(P35);

    __DRV_GPIO_SetP3FastDriving(P3PinALL,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin0  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin1  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin3  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin4  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin5  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3PinALL,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin0  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin1  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin3  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin4  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin5  ,FAST);


    //-------------------------------------//
    __DRV_GPIO_P4ModeSelect(P4ALL_InputOnly      );
    __DRV_GPIO_P4ModeSelect(P4ALL_OpenDrainPullUp);
    __DRV_GPIO_P4ModeSelect(P4ALL_OpenDrain      );
    __DRV_GPIO_P4ModeSelect(P4ALL_PushPull       );
    __DRV_GPIO_P4ModeSelect(P44_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P44_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P44_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P44_PushPull         );
    __DRV_GPIO_P4ModeSelect(P45_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P45_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P45_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P45_PushPull         );
    __DRV_GPIO_P4ModeSelect(P47_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P47_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P47_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P47_PushPull         );

    __DRV_GPIO_P4DeInit();

    __DRV_GPIO_WriteP4(P4,0x66);
    __DRV_GPIO_WriteP4(P44,0);
    __DRV_GPIO_WriteP4(P45,1);
    __DRV_GPIO_WriteP4(P47,0);

    __DRV_GPIO_ReadP4(temp8);
    temp1 = __DRV_GPIO_GetPin44();
    temp1 = __DRV_GPIO_GetPin45();
    temp1 = __DRV_GPIO_GetPin47();

    __DRV_GPIO_InversePinP4(P4 );
    __DRV_GPIO_InversePinP4(P44);
    __DRV_GPIO_InversePinP4(P45);
    __DRV_GPIO_InversePinP4(P47);

    __DRV_GPIO_SetP4FastDriving(P4PinALL,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin4  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin5  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin7  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4PinALL,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin4  ,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin5  ,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin7  ,FAST);

    __DRV_GPIO_SetOCD2IO(MW_ENABLE);
    __DRV_GPIO_SetOCD2IO(MW_DISABLE);

    __DRV_GPIO_SetRST2IO(MW_ENABLE);
    __DRV_GPIO_SetRST2IO(MW_DISABLE);

    __DRV_GPIO_P4_Protected(MW_ENABLE);
    __DRV_GPIO_P4_Protected(MW_DISABLE);


    //-------------------------------------//
    __DRV_GPIO_P6ModeSelect(P6ALL_InputOnly      );
    __DRV_GPIO_P6ModeSelect(P6ALL_OpenDrainPullUp);
    __DRV_GPIO_P6ModeSelect(P6ALL_OpenDrain      );
    __DRV_GPIO_P6ModeSelect(P6ALL_PushPull       );
    __DRV_GPIO_P6ModeSelect(P60_InputOnly        );
    __DRV_GPIO_P6ModeSelect(P60_OpenDrainPullUp  );
    __DRV_GPIO_P6ModeSelect(P60_OpenDrain        );
    __DRV_GPIO_P6ModeSelect(P60_PushPull         );
    __DRV_GPIO_P6ModeSelect(P61_InputOnly        );
    __DRV_GPIO_P6ModeSelect(P61_OpenDrainPullUp  );
    __DRV_GPIO_P6ModeSelect(P61_OpenDrain        );
    __DRV_GPIO_P6ModeSelect(P61_PushPull         );

    __DRV_GPIO_P6DeInit();

    __DRV_GPIO_WriteP6(P6,0x88);
    __DRV_GPIO_WriteP6(P60,0);
    __DRV_GPIO_WriteP6(P60,1);

    __DRV_GPIO_ReadP6(temp8);
    //temp1 = DRV_GPIO_GetPin60();    //test by MG82F6D17_GPIO_DRV_c()
    //temp1 = DRV_GPIO_GetPin61();    //test by MG82F6D17_GPIO_DRV_c()

    __DRV_GPIO_InversePinP6(P6);
    __DRV_GPIO_InversePinP6(P60);
    __DRV_GPIO_InversePinP6(P61);

    __DRV_GPIO_P6_Protected(MW_ENABLE);
    __DRV_GPIO_P6_Protected(MW_DISABLE);


    //-------------------------------------//
    __DRV_GPIO_P1OutputDrivingConfig(P10_to_P13_Driving,HIGH);
    __DRV_GPIO_P1OutputDrivingConfig(P10_to_P13_Driving,LOW);
    __DRV_GPIO_P1OutputDrivingConfig(P14_to_P17_Driving,HIGH);
    __DRV_GPIO_P1OutputDrivingConfig(P14_to_P17_Driving,LOW);

    __DRV_GPIO_P2OutputDrivingConfig(P20_to_P23_Driving,HIGH);
    __DRV_GPIO_P2OutputDrivingConfig(P20_to_P23_Driving,LOW);
    __DRV_GPIO_P2OutputDrivingConfig(P24_to_P27_Driving,HIGH);
    __DRV_GPIO_P2OutputDrivingConfig(P24_to_P27_Driving,LOW);

    __DRV_GPIO_P3OutputDrivingConfig(P30_to_P33_Driving,HIGH);
    __DRV_GPIO_P3OutputDrivingConfig(P30_to_P33_Driving,LOW);
    __DRV_GPIO_P3OutputDrivingConfig(P34_to_P37_Driving,HIGH);
    __DRV_GPIO_P3OutputDrivingConfig(P34_to_P37_Driving,LOW);

    __DRV_GPIO_P4OutputDrivingConfig(P44_to_P46_Driving,HIGH);
    __DRV_GPIO_P4OutputDrivingConfig(P44_to_P46_Driving,LOW);

}

void MG82F6D17_GPIO_DRV_c(void)
{
    //uint8_t temp8;
    //bool tempb;
    //char tempc;
    bit temp1;

    //temp8 = DRV_GPIO_GetPin60();
    //temp8 = DRV_GPIO_GetPin61();
    //tempb = DRV_GPIO_GetPin60();
    //tempb = DRV_GPIO_GetPin61();
    //tempc = DRV_GPIO_GetPin60();
    //tempc = DRV_GPIO_GetPin61();
    temp1 = DRV_GPIO_GetPin60();
    temp1 = DRV_GPIO_GetPin61();

}

