#include "MG82F6D17_CONFIG.h"

/**
 *******************************************************************************
 * System Clock Test Group
 *******************************************************************************
 */
void MG82F6D17_CLK_DRV_h(void)
{
    __DRV_CLK_OSCin_Select(OSCin_IHRCO);
    __DRV_CLK_OSCin_Select(OSCin_ILRCO);
    __DRV_CLK_OSCin_Select(OSCin_ECKI_P60);

    __DRV_CLK_IHRCO_Select(IHRCO_DISABLE);
    __DRV_CLK_IHRCO_Select(IHRCO_12MHz);
    __DRV_CLK_IHRCO_Select(IHRCO_11M059KHz);
    __DRV_CLK_IHRCO_12MHz();
    __DRV_CLK_IHRCO_11M059KHz();
    __DRV_CLK_IHRCO_Disable();

    __DRV_CLK_PLL_Enable();
    __DRV_CLK_PLL_Disable();
    __DRV_CLK_PLL_Cmd(MW_ENABLE);
    __DRV_CLK_PLL_Cmd(MW_DISABLE);

    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_1,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_2,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_4,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_1,PLLI_X6X8X12);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_2,PLLI_X6X8X12);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_4,PLLI_X6X8X12);

    __DRV_CLK_MCK_Select(MCK_OSCin);
    __DRV_CLK_MCK_Select(MCK_CKMI_X4X6);
    __DRV_CLK_MCK_Select(MCK_CKMI_X5X8);
    __DRV_CLK_MCK_Select(MCK_CKMI_X8X12);

    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_1);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_2);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_4);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_8);

    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_1);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_2);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_4);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_8);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_16);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_32);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_64);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_128);

    __DRV_CLK_CPUCLK_SYSCLK_DIV_1();
    __DRV_CLK_CPUCLK_SYSCLK_DIV_2();
    __DRV_CLK_CPUCLK_Select(CPUCLK_SYSCLK_DIV_1);
    __DRV_CLK_CPUCLK_Select(CPUCLK_SYSCLK_DIV_2);

    __DRV_CLK_CPUCLK_FASTER_25MHz();
    __DRV_CLK_CPUCLK_6MHz_to_24MHz();
    __DRV_CLK_CPUCLK_SLOWER_6MHz();
    __DRV_CLK_CPUCLK_Range_Select(FASTER_CPUCLK);
    __DRV_CLK_CPUCLK_Range_Select(MIDDLE_CPUCLK);
    __DRV_CLK_CPUCLK_Range_Select(SLOWER_CPUCLK);

    __DRV_CLK_NormalWakeup120us();
    __DRV_CLK_FastWakeup30us();
    __DRV_CLK_Wakeup_Select(NORMAL_WAKEUP_120us);
    __DRV_CLK_Wakeup_Select(FAST_WAKEUP_30us);

    __DRV_CLK_P60Mux_Select(P60_GPIO);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_1);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_2);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_4);

    __DRV_CLK_P60FastDrive_Enable();
    __DRV_CLK_P60FastDrive_Disable();
    __DRV_CLK_P60FastDrive_Cmd(MW_ENABLE);
    __DRV_CLK_P60FastDrive_Cmd(MW_DISABLE);

    __DRV_CLK_CKCON0_UnProtected();
    __DRV_CLK_CKCON0_Protected();
    __DRV_CLK_CKCON_Protection_Cmd(MW_ENABLE);
    __DRV_CLK_CKCON_Protection_Cmd(MW_DISABLE);
}

void MG82F6D17_CLK_MID_h(void)
{
    __DRV_CLK_Easy_Select(SYS_12M_CPU_12M);
    __DRV_CLK_Easy_Select(SYS_11M0592_CPU_11M0592);
    __DRV_CLK_Easy_Select(SYS_CPU_ECKI);
    __DRV_CLK_Easy_Select(SYS_32K_CPU_32K);
    __DRV_CLK_Easy_Select(SYS_24M_CPU_24M);
    __DRV_CLK_Easy_Select(SYS_22M118_CPU_22M118);
    __DRV_CLK_Easy_Select(SYS_32M_CPU_16M);
    __DRV_CLK_Easy_Select(SYS_48M_CPU_24M);
    __DRV_CLK_Easy_Select(SYS_29M491_CPU_14M736);
    __DRV_CLK_Easy_Select(SYS_44M236_CPU_22M118);
}

