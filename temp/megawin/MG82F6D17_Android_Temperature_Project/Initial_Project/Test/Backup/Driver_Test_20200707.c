/**
 ******************************************************************************
 *
 * @file    Driver_Test.C
 *
 * @brief       This is the C code format main file.
 *
 * @par     Project
 *          MG82F6D17
 * @version     v0.01
 * @date    2020/06/09
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *          All rights reserved.
 *
 ******************************************************************************
 * @par     Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01
 * #0.02
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"


//*****************************************************************************
//*** <<< Use Configuration Wizard in Context Menu >>> ***
//*****************************************************************************
//------------------------------------------
//<e0> Ch 8. DMA
//------------------------------------------
#define DMA_Test_Group_EN  0
//<q0> MG82F6D17_DMA_DRV.h
#define MG82F6D17_DMA_DRV_h_EN  0
//<q0> MG82F6D17_DMA_MID.h
#define MG82F6D17_DMA_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 9. System Clock
//------------------------------------------
#define SYSCLK_Test_Group_EN  0
//<q0> MG82F6D17_CLK_DRV.h
#define MG82F6D17_CLK_DRV_h_EN  1
//<q0> MG82F6D17_CLK_MID.h
#define MG82F6D17_CLK_MID_h_EN  1
//</e>
//------------------------------------------
//<e0> Ch 10. WDT
//------------------------------------------
#define WDT_Test_Group_EN  0
//<q0> MG82F6D17_WDT_DRV.h
#define MG82F6D17_WDT_DRV_h_EN  0
//<q0> MG82F6D17_WDT_MID.h
#define MG82F6D17_WDT_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 11. RTC
//------------------------------------------
#define RTC_Test_Group_EN  0
//<q0> MG82F6D17_RTC_DRV.h
#define MG82F6D17_RTC_DRV_h_EN  0
//<q0> MG82F6D17_RTC_MID.h
#define MG82F6D17_RTC_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 12. System Reset
//------------------------------------------
#define Reset_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 13. Power Management
//------------------------------------------
#define Power_Test_Group_EN  0
//<q0> MG82F6D17_PW_DRV.h
#define MG82F6D17_PW_DRV_h_EN  1
//<q0> MG82F6D17_PW_MID.h
#define MG82F6D17_PW_MID_h_EN  1
//</e>
//------------------------------------------
//<e0> Ch 14. GPIO
//------------------------------------------
#define GPIO_Test_Group_EN  0
//<q0> MG82F6D17_GPIO_DRV.h
#define MG82F6D17_GPIO_DRV_h_EN  1
//<q0> MG82F6D17_GPIO_DRV.c
#define MG82F6D17_GPIO_DRV_c_EN  1
//</e>
//------------------------------------------
//<e0> Ch 15. Interrupt
//------------------------------------------
#define INT_Test_Group_EN  0
//<q0> MG82F6D17_INT_DRV.h
#define MG82F6D17_INT_DRV_h_EN  0
//<q0> MG82F6D17_INT_MID.h
#define MG82F6D17_INT_MID_h_EN  0
//<q0> MG82F6D17_INT_VECTOR.h
#define MG82F6D17_INT_VECTOR_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 16. Timers/Counters
//------------------------------------------
#define TM_Test_Group_EN  0
//<q0> MG82F6D17_TIMER_DRV.c
#define MG82F6D17_TIMER_DRV_c_EN  0
//<q0> MG82F6D17_TIMER_DRV.h
#define MG82F6D17_TIMER_DRV_h_EN  0
//<q0> MG82F6D17_TIMER_MID.h
#define MG82F6D17_TIMER_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 17. PCA
//------------------------------------------
#define PCA_Test_Group_EN  0
//<q0> MG82F6D17_PCA_DRV.c
#define MG82F6D17_PCA_DRV_c_EN  0
//<q0> MG82F6D17_PCA_DRV.h
#define MG82F6D17_PCA_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 18. SerialPort0
//------------------------------------------
#define S0_Test_Group_EN  0
//<q0> MG82F6D17_UART0_DRV.c
#define MG82F6D17_UART0_DRV_c_EN  0
//<q0> MG82F6D17_UART0_DRV.h
#define MG82F6D17_UART0_DRV_h_EN  0
//<q0> MG82F6D17_UART0_MID.h
#define MG82F6D17_UART0_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 19. SerialPort1
//------------------------------------------
#define S1_Test_Group_EN  0
//<q0> MG82F6D17_UART1_DRV.c
#define MG82F6D17_UART1_DRV_c_EN  0
//<q0> MG82F6D17_UART1_DRV.h
#define MG82F6D17_UART1_DRV_h_EN  0
//<q0> MG82F6D17_UART1_MID.h
#define MG82F6D17_UART1_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 20. SPI
//------------------------------------------
#define SPI_Test_Group_EN  0
//<q0> MG82F6D17_SPI_DRV.c
#define MG82F6D17_SPI_DRV_c_EN  0
//<q0> MG82F6D17_SPI_DRV.h
#define MG82F6D17_SPI_DRV_h_EN  0
//<q0> MG82F6D17_SPI_MID.h
#define MG82F6D17_SPI_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 21. TWI0/I2C0
//------------------------------------------
#define TWI0_Test_Group_EN  0
//<q0> MG82F6D17_I2C0_DRV.h
#define MG82F6D17_I2C0_DRV_h_EN  0
//<q0> MG82F6D17_I2C0_MID.c
#define MG82F6D17_I2C0_MID_c_EN  0
//<q0> MG82F6D17_I2C0_MID.h
#define MG82F6D17_I2C0_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 22. STWI/SI2C
//------------------------------------------
#define STWI_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 23. Beeper
//------------------------------------------
#define Beeper_Test_Group_EN  0
//<q0> MG82F6D17_BEEPER_DRV.h
#define MG82F6D17_BEEPER_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 24. KBI
//------------------------------------------
#define KBI_Test_Group_EN  0
//<q0> MG82F6D17_KBI_DRV.h
#define MG82F6D17_KBI_DRV_h_EN  0
//<q0> MG82F6D17_KBI_MID.h
#define MG82F6D17_KBI_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 25. GPL
//------------------------------------------
#define GPL_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 26. ADC
//------------------------------------------
#define ADC_Test_Group_EN  0
//<q0> MG82F6D17_ADC_DRV.h
#define MG82F6D17_ADC_DRV_h_EN  0
//<q0> MG82F6D17_ADC_MID.h
#define MG82F6D17_ADC_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 28. IAP
//------------------------------------------
#define IAP_Test_Group_EN  0
//<q0> MG82F6D17_IAP_DRV.c
#define MG82F6D17_IAP_DRV_c_EN  0
//<q0> MG82F6D17_IAP_DRV.h
#define MG82F6D17_IAP_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Others
//------------------------------------------
#define Others_Test_Group_EN  0
//<q0> MG82F6D17_COMMON_DRV.c
#define MG82F6D17_COMMON_DRV_c_EN  0
//<q0> MG82F6D17_COMMON_DRV.h
#define MG82F6D17_COMMON_DRV_h_EN  0
//<q0> Brian Compiler Test Group
#define Brian_Compiler_Test_Group_EN  0
//</e>
//*****************************************************************************
//<<< end of configuration section >>>
//*****************************************************************************


#define Test_Reserved_Group 0
#define Test_Reserved_File 0


/**
 *******************************************************************************
 * System Clock Test Group
 *******************************************************************************
 */
#if SYSCLK_Test_Group_EN
void MG82F6D17_CLK_DRV_h ()
{
    __DRV_CLK_OSCin_Select(OSCin_IHRCO);
    __DRV_CLK_OSCin_Select(OSCin_ILRCO);
    __DRV_CLK_OSCin_Select(OSCin_ECKI_P60);

    __DRV_CLK_IHRCO_Select(IHRCO_DISABLE);
    __DRV_CLK_IHRCO_Select(IHRCO_12MHz);
    __DRV_CLK_IHRCO_Select(IHRCO_11M059KHz);
    __DRV_CLK_IHRCO_12MHz();
    __DRV_CLK_IHRCO_11M059KHz();
    __DRV_CLK_IHRCO_Disable();

    __DRV_CLK_PLL_Enable();
    __DRV_CLK_PLL_Disable();
    __DRV_CLK_PLL_Cmd(MW_ENABLE);
    __DRV_CLK_PLL_Cmd(MW_DISABLE);

    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_1,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_2,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_4,PLLI_X4X5X8);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_1,PLLI_X6X8X12);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_2,PLLI_X6X8X12);
    __DRV_CLK_PLL_Config(CKM_OSCin_DIV_4,PLLI_X6X8X12);

    __DRV_CLK_MCK_Select(MCK_OSCin);
    __DRV_CLK_MCK_Select(MCK_CKMI_X4X6);
    __DRV_CLK_MCK_Select(MCK_CKMI_X5X8);
    __DRV_CLK_MCK_Select(MCK_CKMI_X8X12);

    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_1);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_2);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_4);
    __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_8);

    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_1);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_2);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_4);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_8);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_16);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_32);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_64);
    __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_128);

    __DRV_CLK_CPUCLK_SYSCLK_DIV_1();
    __DRV_CLK_CPUCLK_SYSCLK_DIV_2();
    __DRV_CLK_CPUCLK_Select(CPUCLK_SYSCLK_DIV_1);
    __DRV_CLK_CPUCLK_Select(CPUCLK_SYSCLK_DIV_2);

    __DRV_CLK_CPUCLK_FASTER_25MHz();
    __DRV_CLK_CPUCLK_6MHz_to_24MHz();
    __DRV_CLK_CPUCLK_SLOWER_6MHz();
    __DRV_CLK_CPUCLK_Range_Select(FASTER_CPUCLK);
    __DRV_CLK_CPUCLK_Range_Select(MIDDLE_CPUCLK);
    __DRV_CLK_CPUCLK_Range_Select(SLOWER_CPUCLK);

    __DRV_CLK_NormalWakeup120us();
    __DRV_CLK_FastWakeup30us();
    __DRV_CLK_Wakeup_Select(NORMAL_WAKEUP_120us);
    __DRV_CLK_Wakeup_Select(FAST_WAKEUP_30us);

    __DRV_CLK_P60Mux_Select(P60_GPIO);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_1);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_2);
    __DRV_CLK_P60Mux_Select(P60_MCK_DIV_4);

    __DRV_CLK_P60FastDrive_Enable();
    __DRV_CLK_P60FastDrive_Disable();
    __DRV_CLK_P60FastDrive_Cmd(MW_ENABLE);
    __DRV_CLK_P60FastDrive_Cmd(MW_DISABLE);

    __DRV_CLK_CKCON0_UnProtected();
    __DRV_CLK_CKCON0_Protected();
    __DRV_CLK_CKCON_Protection_Cmd(MW_ENABLE);
    __DRV_CLK_CKCON_Protection_Cmd(MW_DISABLE);
}

void MG82F6D17_CLK_MID_h(void)
{
    __DRV_CLK_Easy_Select(SYS_12M_CPU_12M);
    __DRV_CLK_Easy_Select(SYS_11M0592_CPU_11M0592);
    __DRV_CLK_Easy_Select(SYS_CPU_ECKI);
    __DRV_CLK_Easy_Select(SYS_32K_CPU_32K);
    __DRV_CLK_Easy_Select(SYS_24M_CPU_24M);
    __DRV_CLK_Easy_Select(SYS_22M118_CPU_22M118);
    __DRV_CLK_Easy_Select(SYS_32M_CPU_16M);
    __DRV_CLK_Easy_Select(SYS_48M_CPU_24M);
    __DRV_CLK_Easy_Select(SYS_29M491_CPU_14M736);
    __DRV_CLK_Easy_Select(SYS_44M236_CPU_22M118);
}
#endif  //SYSCLK_Test_Group_EN


/**
 *******************************************************************************
 * Power Management Test Group
 *******************************************************************************
 */
#if Power_Test_Group_EN
void MG82F6D17_PW_DRV_h(void)
{
    __DRV_PW_PowerDown_Enable();

    __DRV_PW_IdleMode_Enable();

    __DRV_BODx_SetBOD0RST_Cmd(MW_DISABLE);
    __DRV_BODx_SetBOD0RST_Cmd(MW_ENABLE);

    __DRV_BODx_SetBOD1RST_Cmd(MW_DISABLE);
    __DRV_BODx_SetBOD1RST_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_2V0);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_2V4);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_3V7);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_4V2);

    __DRV_BODx_BOD1_AwakeBOD1inPD(MW_DISABLE);
    __DRV_BODx_BOD1_AwakeBOD1inPD(MW_ENABLE);

    __DRV_BODx_BOD1_MonitorsVDD(MW_DISABLE);
    __DRV_BODx_BOD1_MonitorsVDD(MW_ENABLE);

    __DRV_BODx_BOD0_IT_Cmd(MW_DISABLE);
    __DRV_BODx_BOD0_IT_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_IT_Cmd(MW_DISABLE);
    __DRV_BODx_BOD1_IT_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_ClearFlag();

    __DRV_BODx_BOD0_ClearFlag();
}
#endif  //Power_Test_Group_EN


/**
 *******************************************************************************
 * GPIO Test Group
 *******************************************************************************
 */
#if GPIO_Test_Group_EN
void MG82F6D17_GPIO_DRV_h(void)
{
    uint8_t temp8;
    bit   temp1;

    //-------------------------------------//
    __DRV_GPIO_P1ModeSelect(P1ALL_InputOnly      );
    __DRV_GPIO_P1ModeSelect(P1ALL_OpenDrainPullUp);
    __DRV_GPIO_P1ModeSelect(P1ALL_OpenDrain      );
    __DRV_GPIO_P1ModeSelect(P1ALL_PushPull       );
    __DRV_GPIO_P1ModeSelect(P10_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P10_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P10_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P10_PushPull         );
    __DRV_GPIO_P1ModeSelect(P11_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P11_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P11_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P11_PushPull         );
    __DRV_GPIO_P1ModeSelect(P15_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P15_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P15_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P15_PushPull         );
    __DRV_GPIO_P1ModeSelect(P16_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P16_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P16_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P16_PushPull         );
    __DRV_GPIO_P1ModeSelect(P17_InputOnly        );
    __DRV_GPIO_P1ModeSelect(P17_OpenDrainPullUp  );
    __DRV_GPIO_P1ModeSelect(P17_OpenDrain        );
    __DRV_GPIO_P1ModeSelect(P17_PushPull         );

    __DRV_GPIO_P1DeInit();

    __DRV_GPIO_WriteP1(P1,0x11);
    __DRV_GPIO_WriteP1(P10,0);
    __DRV_GPIO_WriteP1(P11,1);
    __DRV_GPIO_WriteP1(P15,0);
    __DRV_GPIO_WriteP1(P16,1);
    __DRV_GPIO_WriteP1(P17,0);

    __DRV_GPIO_ReadP1(temp8);
    temp1 = __DRV_GPIO_GetPin10();
    temp1 = __DRV_GPIO_GetPin11();
    temp1 = __DRV_GPIO_GetPin15();
    temp1 = __DRV_GPIO_GetPin16();
    temp1 = __DRV_GPIO_GetPin17();

    __DRV_GPIO_InversePinP1(P1 );
    __DRV_GPIO_InversePinP1(P10);
    __DRV_GPIO_InversePinP1(P11);
    __DRV_GPIO_InversePinP1(P15);
    __DRV_GPIO_InversePinP1(P16);
    __DRV_GPIO_InversePinP1(P17);

    __DRV_GPIO_SetP1FastDriving(P1PinALL,NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin0,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin1,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin5,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin6,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1Pin7,  NORMAL);
    __DRV_GPIO_SetP1FastDriving(P1PinALL,FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin0,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin1,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin5,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin6,  FAST);
    __DRV_GPIO_SetP1FastDriving(P1Pin7,  FAST);


    //-------------------------------------//
    __DRV_GPIO_P2ModeSelect(P2ALL_InputOnly      );
    __DRV_GPIO_P2ModeSelect(P2ALL_OpenDrainPullUp);
    __DRV_GPIO_P2ModeSelect(P2ALL_OpenDrain      );
    __DRV_GPIO_P2ModeSelect(P2ALL_PushPull       );
    __DRV_GPIO_P2ModeSelect(P22_InputOnly        );
    __DRV_GPIO_P2ModeSelect(P22_OpenDrainPullUp  );
    __DRV_GPIO_P2ModeSelect(P22_OpenDrain        );
    __DRV_GPIO_P2ModeSelect(P22_PushPull         );
    __DRV_GPIO_P2ModeSelect(P24_InputOnly        );
    __DRV_GPIO_P2ModeSelect(P24_OpenDrainPullUp  );
    __DRV_GPIO_P2ModeSelect(P24_OpenDrain        );
    __DRV_GPIO_P2ModeSelect(P24_PushPull         );

    __DRV_GPIO_P2DeInit();

    __DRV_GPIO_WriteP2(P2,0x22);
    __DRV_GPIO_WriteP2(P22,1);
    __DRV_GPIO_WriteP2(P24,0);

    __DRV_GPIO_ReadP2(temp8);
    temp1 = __DRV_GPIO_GetPin22();
    temp1 = __DRV_GPIO_GetPin24();

    __DRV_GPIO_InversePinP2(P2 );
    __DRV_GPIO_InversePinP2(P22);
    __DRV_GPIO_InversePinP2(P24);

    __DRV_GPIO_SetP2FastDriving(P2PinALL,NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2Pin2,  NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2Pin4,  NORMAL);
    __DRV_GPIO_SetP2FastDriving(P2PinALL,FAST);
    __DRV_GPIO_SetP2FastDriving(P2Pin2,  FAST);
    __DRV_GPIO_SetP2FastDriving(P2Pin4,  FAST);


    //-------------------------------------//
    __DRV_GPIO_P3ModeSelect(P3ALL_QuasiMode);
    __DRV_GPIO_P3ModeSelect(P3ALL_PushPull );
    __DRV_GPIO_P3ModeSelect(P3ALL_InputOnly);
    __DRV_GPIO_P3ModeSelect(P3ALL_OpenDrain);
    __DRV_GPIO_P3ModeSelect(P30_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P30_PushPull   );
    __DRV_GPIO_P3ModeSelect(P30_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P30_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P31_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P31_PushPull   );
    __DRV_GPIO_P3ModeSelect(P31_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P31_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P33_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P33_PushPull   );
    __DRV_GPIO_P3ModeSelect(P33_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P33_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P34_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P34_PushPull   );
    __DRV_GPIO_P3ModeSelect(P34_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P34_OpenDrain  );
    __DRV_GPIO_P3ModeSelect(P35_QuasiMode  );
    __DRV_GPIO_P3ModeSelect(P35_PushPull   );
    __DRV_GPIO_P3ModeSelect(P35_InputOnly  );
    __DRV_GPIO_P3ModeSelect(P35_OpenDrain  );

    __DRV_GPIO_P3DeInit();

    __DRV_GPIO_WriteP3(P3,0x44);
    __DRV_GPIO_WriteP3(P30,0);
    __DRV_GPIO_WriteP3(P31,1);
    __DRV_GPIO_WriteP3(P33,0);
    __DRV_GPIO_WriteP3(P34,1);
    __DRV_GPIO_WriteP3(P35,0);

    __DRV_GPIO_ReadP3(temp8);
    temp1 = __DRV_GPIO_GetPin30();
    temp1 = __DRV_GPIO_GetPin31();
    temp1 = __DRV_GPIO_GetPin33();
    temp1 = __DRV_GPIO_GetPin34();
    temp1 = __DRV_GPIO_GetPin35();

    __DRV_GPIO_InversePinP3(P3 );
    __DRV_GPIO_InversePinP3(P30);
    __DRV_GPIO_InversePinP3(P31);
    __DRV_GPIO_InversePinP3(P33);
    __DRV_GPIO_InversePinP3(P34);
    __DRV_GPIO_InversePinP3(P35);

    __DRV_GPIO_SetP3FastDriving(P3PinALL,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin0  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin1  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin3  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin4  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3Pin5  ,NORMAL);
    __DRV_GPIO_SetP3FastDriving(P3PinALL,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin0  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin1  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin3  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin4  ,FAST);
    __DRV_GPIO_SetP3FastDriving(P3Pin5  ,FAST);


    //-------------------------------------//
    __DRV_GPIO_P4ModeSelect(P4ALL_InputOnly      );
    __DRV_GPIO_P4ModeSelect(P4ALL_OpenDrainPullUp);
    __DRV_GPIO_P4ModeSelect(P4ALL_OpenDrain      );
    __DRV_GPIO_P4ModeSelect(P4ALL_PushPull       );
    __DRV_GPIO_P4ModeSelect(P44_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P44_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P44_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P44_PushPull         );
    __DRV_GPIO_P4ModeSelect(P45_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P45_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P45_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P45_PushPull         );
    __DRV_GPIO_P4ModeSelect(P47_InputOnly        );
    __DRV_GPIO_P4ModeSelect(P47_OpenDrainPullUp  );
    __DRV_GPIO_P4ModeSelect(P47_OpenDrain        );
    __DRV_GPIO_P4ModeSelect(P47_PushPull         );

    __DRV_GPIO_P4DeInit();

    __DRV_GPIO_WriteP4(P4,0x66);
    __DRV_GPIO_WriteP4(P44,0);
    __DRV_GPIO_WriteP4(P45,1);
    __DRV_GPIO_WriteP4(P47,0);

    __DRV_GPIO_ReadP4(temp8);
    temp1 = __DRV_GPIO_GetPin44();
    temp1 = __DRV_GPIO_GetPin45();
    temp1 = __DRV_GPIO_GetPin47();

    __DRV_GPIO_InversePinP4(P4 );
    __DRV_GPIO_InversePinP4(P44);
    __DRV_GPIO_InversePinP4(P45);
    __DRV_GPIO_InversePinP4(P47);

    __DRV_GPIO_SetP4FastDriving(P4PinALL,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin4  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin5  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4Pin7  ,NORMAL);
    __DRV_GPIO_SetP4FastDriving(P4PinALL,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin4  ,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin5  ,FAST);
    __DRV_GPIO_SetP4FastDriving(P4Pin7  ,FAST);

    __DRV_GPIO_SetOCD2IO(MW_ENABLE);
    __DRV_GPIO_SetOCD2IO(MW_DISABLE);

    __DRV_GPIO_SetRST2IO(MW_ENABLE);
    __DRV_GPIO_SetRST2IO(MW_DISABLE);

    __DRV_GPIO_P4_Protected(MW_ENABLE);
    __DRV_GPIO_P4_Protected(MW_DISABLE);


    //-------------------------------------//
    __DRV_GPIO_P6ModeSelect(P6ALL_InputOnly      );
    __DRV_GPIO_P6ModeSelect(P6ALL_OpenDrainPullUp);
    __DRV_GPIO_P6ModeSelect(P6ALL_OpenDrain      );
    __DRV_GPIO_P6ModeSelect(P6ALL_PushPull       );
    __DRV_GPIO_P6ModeSelect(P60_InputOnly        );
    __DRV_GPIO_P6ModeSelect(P60_OpenDrainPullUp  );
    __DRV_GPIO_P6ModeSelect(P60_OpenDrain        );
    __DRV_GPIO_P6ModeSelect(P60_PushPull         );
    __DRV_GPIO_P6ModeSelect(P61_InputOnly        );
    __DRV_GPIO_P6ModeSelect(P61_OpenDrainPullUp  );
    __DRV_GPIO_P6ModeSelect(P61_OpenDrain        );
    __DRV_GPIO_P6ModeSelect(P61_PushPull         );

    __DRV_GPIO_P6DeInit();

    __DRV_GPIO_WriteP6(P6,0x88);
    __DRV_GPIO_WriteP6(P60,0);
    __DRV_GPIO_WriteP6(P60,1);

    __DRV_GPIO_ReadP6(temp8);
    //temp1 = DRV_GPIO_GetPin60();    //test by MG82F6D17_GPIO_DRV_c()
    //temp1 = DRV_GPIO_GetPin61();    //test by MG82F6D17_GPIO_DRV_c()

    __DRV_GPIO_InversePinP6(P6);
    __DRV_GPIO_InversePinP6(P60);
    __DRV_GPIO_InversePinP6(P61);

    __DRV_GPIO_P6_Protected(MW_ENABLE);
    __DRV_GPIO_P6_Protected(MW_DISABLE);


    //-------------------------------------//
    __DRV_GPIO_P1OutputDrivingConfig(P10_to_P13_Driving,HIGH);
    __DRV_GPIO_P1OutputDrivingConfig(P10_to_P13_Driving,LOW);
    __DRV_GPIO_P1OutputDrivingConfig(P14_to_P17_Driving,HIGH);
    __DRV_GPIO_P1OutputDrivingConfig(P14_to_P17_Driving,LOW);

    __DRV_GPIO_P2OutputDrivingConfig(P20_to_P23_Driving,HIGH);
    __DRV_GPIO_P2OutputDrivingConfig(P20_to_P23_Driving,LOW);
    __DRV_GPIO_P2OutputDrivingConfig(P24_to_P27_Driving,HIGH);
    __DRV_GPIO_P2OutputDrivingConfig(P24_to_P27_Driving,LOW);

    __DRV_GPIO_P3OutputDrivingConfig(P30_to_P33_Driving,HIGH);
    __DRV_GPIO_P3OutputDrivingConfig(P30_to_P33_Driving,LOW);
    __DRV_GPIO_P3OutputDrivingConfig(P34_to_P37_Driving,HIGH);
    __DRV_GPIO_P3OutputDrivingConfig(P34_to_P37_Driving,LOW);

    __DRV_GPIO_P4OutputDrivingConfig(P44_to_P46_Driving,HIGH);
    __DRV_GPIO_P4OutputDrivingConfig(P44_to_P46_Driving,LOW);

}

void MG82F6D17_GPIO_DRV_c(void)
{
    //uint8_t temp8;
    //bool tempb;
    //char tempc;
    bit temp1;

    //temp8 = DRV_GPIO_GetPin60();
    //temp8 = DRV_GPIO_GetPin61();
    //tempb = DRV_GPIO_GetPin60();
    //tempb = DRV_GPIO_GetPin61();
    //tempc = DRV_GPIO_GetPin60();
    //tempc = DRV_GPIO_GetPin61();
    temp1 = DRV_GPIO_GetPin60();
    temp1 = DRV_GPIO_GetPin61();

}
#endif  //GPIO_Test_Group_EN


/**
 *******************************************************************************
 * Serial Peripheral Interface Test Group
 *******************************************************************************
 */
#if SPI_Test_Group_EN
void MG82F6D17_SPI_DRV_h(void)
{
    __DRV_SPI_PinMux_Select(SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17);
    __DRV_SPI_PinMux_Select(SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33);
}

void MG82F6D17_SPI_DRV_c(void)
{
    _nop_();
}


void MG82F6D17_SPI_MID_h(void)
{
    _nop_();
}
#endif  //SPI_Test_Group_EN


/**
 *******************************************************************************
 * Brian Compiler Test Group
 *******************************************************************************
 */
#if Brian_Compiler_Test_Group_EN

//Lv0:?    Lv8:?
#define SFRPIx_SFR_ReadModifyWrite(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
    )

//Lv0:?    Lv8:?
#define SFRPIx_SFR_ReadModifyWrite_SFRPI0(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
        __DRV_SFR_PageIndex(0);\
    )

//Lv0:?    Lv8:?
#define PUSH_SFRPIx_SFR_ReadModifyWrite_POP(__STATE__)\
    MWT(\
        _push_(SFRPI);\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
        _pop_(SFRPI);\
    )

#define Test_Dis 0x33
#define Test_En  0xAA

//Lv0:?    Lv8:?
void Func_SFRPIx_SFR_ReadModifyWrite(uint8_t _VAR_)
{
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
}

//Lv0:?    Lv8:?
void Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(uint8_t _VAR_)
{
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
    __DRV_SFR_PageIndex(0);
}

//Lv0:?    Lv8:?
void Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(uint8_t _VAR_)
{
    _push_(SFRPI);
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
    _pop_(SFRPI);
}

void Brian_Compiler_Test_Group()
{
    #if 0
        uint8_t tmp8;
        uint16_t tmp16;
    #endif

    #if 1
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- SFRPI=x, Read Write SFR ---
        //Lv0:?    Lv8:?
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //1
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //2
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //3
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //4
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //5
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //6
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //7
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //8
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //9
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- SFRPI=x, Read Write SFR, SFRPI=0 ---
        //Lv0:?    Lv8:?
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //1
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //2
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //3
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //4
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //5
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //6
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //7
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //8
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //9
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- Push(SFRPI), SFRPI=x, Read Write SFR, Pop(SFRPI) ---
        //Lv0:?    Lv8:?
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //1
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //2
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //3
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //4
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //5
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //6
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //7
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //8
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //9
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- Function: SFRPI=x, Read Write SFR ---
        //Lv0:?    Lv8:?
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //1
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //2
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //3
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //4
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //5
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //6
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //7
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //8
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //9
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- Function: SFRPI=x, Read Write SFR, SFRPI=0 ---
        //Lv0:?    Lv8:?
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //1
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //2
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //3
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //4
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //5
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //6
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //7
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //8
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //9
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- Function: Push(SFRPI), SFRPI=x, Read Write SFR, Pop(SFRPI) ---
        //Lv0:?    Lv8:?
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //1
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //2
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //3
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //4
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //5
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //6
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //7
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //8
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //9
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
    #endif

    #if 0
        nop();  ACC=0x46;
        __DRV_GPIO_InversePinP6(P6);    //Lv0:19    //Lv8:17
        nop();  ACC=0xB9;
        __DRV_SPI_PinMux_Select(SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17);  //Lv0:9     //Lv8:9
        nop();  ACC=0x46;
        __DRV_GPIO_InversePinP6(P60);   //Lv0:9     //Lv8:9
        nop();  ACC=0xB9;
        __DRV_SPI_PinMux_Select(SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33);  //Lv0:9     //Lv8:9
        nop();  ACC=0x46;
        __DRV_SFR_PageIndex(7);     //Lv0:9     //Lv8:9
        AUXR10=AUXR10&(~SPIPS0);
        nop();  ACC=0xB9;
        __DRV_SFR_PageIndex(7);     //Lv0:9     //Lv8:9
        AUXR10=AUXR10&(~SPIPS0);
        __DRV_SFR_PageIndex(0);
        nop();  ACC=0x46;
        _push_(SFRPI);              //Lv0:10     //Lv8:10
        __DRV_SFR_PageIndex(7);
        AUXR10=AUXR10&(~SPIPS0);
        _pop_(SFRPI);
        nop();  ACC=0xB9;
    #endif


    #if 0
        //Complie Level 0: 6 CMD
        //Complie Level 8: 3 CMD
        do{tmp8--;}while(tmp8!=0);
        nop();
        //Complie Level 0: 6 CMD
        //Complie Level 8: 4 CMD
        do{IFD--;}while(IFD!=0);
        nop();
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        for(tmp8=0 ; tmp8<253 ; tmp8++);
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        for(IFD=0 ; IFD<253 ; IFD++);
        nop();
        //Complie Level 0: 8 CMD
        //Complie Level 8: 5 CMD
        for(tmp8=253 ; tmp8!=0 ; tmp8--);
        nop();
        //Complie Level 0: 8 CMD
        //Complie Level 8: 6 CMD
        for(IFD=253 ; IFD!=0 ; IFD--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        while(tmp8--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        while(IFD--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        do{}while(tmp8--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        do{}while(IFD--);
        nop();
        //Complie Level 0: 11 CMD
        //Complie Level 8: 7 CMD
        do{}while(tmp8--, tmp8!=0);
        nop();
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        do{}while(IFD--, IFD!=0);
        nop(); nop();
    #endif
}
#endif  //Brian_Compiler_Test_Group_EN


/**
 *******************************************************************************
 * Summary Test Group
 *******************************************************************************
 */
void MG82F6D17_Test_Group()
{
    //------------------------------------------
    // Ch8. DMA
    //------------------------------------------
    #if DMA_Test_Group_EN
        #if MG82F6D17_DMA_DRV_h_EN
        #endif

        #if MG82F6D17_DMA_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch9. System Clock
    //------------------------------------------
    #if SYSCLK_Test_Group_EN
        #if MG82F6D17_CLK_DRV_h_EN
            MG82F6D17_CLK_DRV_h();
        #endif

        #if MG82F6D17_CLK_MID_h_EN
            MG82F6D17_CLK_MID_h();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch10. WDT
    //------------------------------------------
    #if WDT_Test_Group_EN
        #if MG82F6D17_WDT_DRV_h_EN
        #endif

        #if MG82F6D17_WDT_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch11. RTC
    //------------------------------------------
    #if RTC_Test_Group_EN
        #if MG82F6D17_RTC_DRV_h_EN
        #endif

        #if MG82F6D17_RTC_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif


    //------------------------------------------
    // Ch12. System Reset
    //------------------------------------------
    #if Reset_Test_Group_EN
        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif


    //------------------------------------------
    // Ch13. Power Management
    //------------------------------------------
    #if Power_Test_Group_EN
        #if MG82F6D17_PW_DRV_h_EN
            MG82F6D17_PW_DRV_h();
        #endif

        #if MG82F6D17_PW_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch14. GPIO
    //------------------------------------------
    #if GPIO_Test_Group_EN
        #if MG82F6D17_GPIO_DRV_h_EN
            MG82F6D17_GPIO_DRV_h();
        #endif

        #if MG82F6D17_GPIO_DRV_c_EN
            MG82F6D17_GPIO_DRV_c();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch15. Interrupt
    //------------------------------------------
    #if INT_Test_Group_EN
        #if MG82F6D17_INT_DRV_h_EN
        #endif

        #if MG82F6D17_INT_MID_h_EN
        #endif

        #if MG82F6D17_INT_VECTOR_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch16. Timers/Counters
    //------------------------------------------
    #if TM_Test_Group_EN
        #if MG82F6D17_TIMER_DRV_c_EN
        #endif

        #if MG82F6D17_TIMER_DRV_h_EN
        #endif

        #if MG82F6D17_TIMER_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch17. PCA
    //------------------------------------------
    #if PCA_Test_Group_EN
        #if MG82F6D17_PCA_DRV_c_EN
        #endif

        #if MG82F6D17_PCA_DRV_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch18. SerialPort0
    //------------------------------------------
    #if S0_Test_Group_EN
        #if MG82F6D17_UART0_DRV_c_EN
        #endif

        #if MG82F6D17_UART0_DRV_h_EN
        #endif

        #if MG82F6D17_UART0_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch19. SerialPort1
    //------------------------------------------
    #if S1_Test_Group_EN
        #if MG82F6D17_UART1_DRV_c_EN
        #endif

        #if MG82F6D17_UART1_DRV_h_EN
        #endif

        #if MG82F6D17_UART1_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch20. SPI
    //------------------------------------------
    #if SPI_Test_Group_EN
        #if MG82F6D17_SPI_DRV_c_EN
        #endif

        #if MG82F6D17_SPI_DRV_h_EN
        #endif

        #if MG82F6D17_SPI_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch21. TWI0/I2C0
    //------------------------------------------
    #if TWI0_Test_Group_EN
        #if MG82F6D17_I2C0_DRV_h_EN
        #endif

        #if MG82F6D17_I2C0_MID_c_EN
        #endif

        #if MG82F6D17_I2C0_MID_h_EN
        #endif
    #endif

    //------------------------------------------
    // Ch22. STWI/SI2C
    //------------------------------------------
    #if STWI_Test_Group_EN
        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch23. Beeper
    //------------------------------------------
    #if Beeper_Test_Group_EN
        #if MG82F6D17_BEEPER_DRV_h_EN
        #endif
        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch24. KBI
    //------------------------------------------
    #if KBI_Test_Group_EN
        #if MG82F6D17_KBI_DRV_h_EN
        #endif

        #if MG82F6D17_KBI_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch25. GPL
    //------------------------------------------
    #if GPL_Test_Group_EN
        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch26. ADC
    //------------------------------------------
    #if ADC_Test_Group_EN
        #if MG82F6D17_ADC_DRV_h_EN
        #endif

        #if MG82F6D17_ADC_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch28. IAP
    //------------------------------------------
    #if IAP_Test_Group_EN
        #if MG82F6D17_IAP_DRV_c_EN
        #endif

        #if MG82F6D17_IAP_DRV_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Others
    //------------------------------------------
    #if Others_Test_Group_EN
        #if MG82F6D17_COMMON_DRV_c_EN
        #endif

        #if MG82F6D17_COMMON_DRV_h_EN
        #endif

        #if Brian_Compiler_Test_Group_EN
            Brian_Compiler_Test_Group();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Reserved
    //------------------------------------------
    #if Test_Reserved_Group
        #if Test_Reserved_File
        #endif
    #endif
}



