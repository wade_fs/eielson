#include "MG82F6D17_CONFIG.h"

/**
 *******************************************************************************
 * Power Management Test Group
 *******************************************************************************
 */
void MG82F6D17_PW_DRV_h(void)
{
    __DRV_PW_PowerDown_Enable();

    __DRV_PW_IdleMode_Enable();

    __DRV_BODx_SetBOD0RST_Cmd(MW_DISABLE);
    __DRV_BODx_SetBOD0RST_Cmd(MW_ENABLE);

    __DRV_BODx_SetBOD1RST_Cmd(MW_DISABLE);
    __DRV_BODx_SetBOD1RST_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_2V0);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_2V4);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_3V7);
    __DRV_BODx_BOD1_MonitoredLevelSelect(BOD1_VoltageLevel_4V2);

    __DRV_BODx_BOD1_AwakeBOD1inPD(MW_DISABLE);
    __DRV_BODx_BOD1_AwakeBOD1inPD(MW_ENABLE);

    __DRV_BODx_BOD1_MonitorsVDD(MW_DISABLE);
    __DRV_BODx_BOD1_MonitorsVDD(MW_ENABLE);

    __DRV_BODx_BOD0_IT_Cmd(MW_DISABLE);
    __DRV_BODx_BOD0_IT_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_IT_Cmd(MW_DISABLE);
    __DRV_BODx_BOD1_IT_Cmd(MW_ENABLE);

    __DRV_BODx_BOD1_ClearFlag();

    __DRV_BODx_BOD0_ClearFlag();
}

