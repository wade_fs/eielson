/**
 ******************************************************************************
 *
 * @file    Driver_Test.C
 *
 * @brief       This is the C code format main file.
 *
 * @par     Project
 *          MG82F6D17
 * @version     v0.01
 * @date    2020/06/09
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *          All rights reserved.
 *
 ******************************************************************************
 * @par     Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01
 * #0.02
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"


//*****************************************************************************
//*** <<< Use Configuration Wizard in Context Menu >>> ***
//*****************************************************************************
//------------------------------------------
//<e0> Ch 8. DMA
//------------------------------------------
#define DMA_Test_Group_EN  0
//<q0> MG82F6D17_DMA_DRV.h
#define MG82F6D17_DMA_DRV_h_EN  0
//<q0> MG82F6D17_DMA_MID.h
#define MG82F6D17_DMA_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 9. System Clock
//------------------------------------------
#define SYSCLK_Test_Group_EN  0
//<q0> MG82F6D17_CLK_DRV.h
#define MG82F6D17_CLK_DRV_h_EN  1
//<q0> MG82F6D17_CLK_MID.h
#define MG82F6D17_CLK_MID_h_EN  1
//</e>
//------------------------------------------
//<e0> Ch 10. WDT
//------------------------------------------
#define WDT_Test_Group_EN  0
//<q0> MG82F6D17_WDT_DRV.h
#define MG82F6D17_WDT_DRV_h_EN  0
//<q0> MG82F6D17_WDT_MID.h
#define MG82F6D17_WDT_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 11. RTC
//------------------------------------------
#define RTC_Test_Group_EN  0
//<q0> MG82F6D17_RTC_DRV.h
#define MG82F6D17_RTC_DRV_h_EN  0
//<q0> MG82F6D17_RTC_MID.h
#define MG82F6D17_RTC_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 12. System Reset
//------------------------------------------
#define Reset_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 13. Power Management
//------------------------------------------
#define Power_Test_Group_EN  0
//<q0> MG82F6D17_PW_DRV.h
#define MG82F6D17_PW_DRV_h_EN  1
//<q0> MG82F6D17_PW_MID.h
#define MG82F6D17_PW_MID_h_EN  1
//</e>
//------------------------------------------
//<e0> Ch 14. GPIO
//------------------------------------------
#define GPIO_Test_Group_EN  0
//<q0> MG82F6D17_GPIO_DRV.h
#define MG82F6D17_GPIO_DRV_h_EN  1
//<q0> MG82F6D17_GPIO_DRV.c
#define MG82F6D17_GPIO_DRV_c_EN  1
//</e>
//------------------------------------------
//<e0> Ch 15. Interrupt
//------------------------------------------
#define INT_Test_Group_EN  0
//<q0> MG82F6D17_INT_DRV.h
#define MG82F6D17_INT_DRV_h_EN  0
//<q0> MG82F6D17_INT_MID.h
#define MG82F6D17_INT_MID_h_EN  0
//<q0> MG82F6D17_INT_VECTOR.h
#define MG82F6D17_INT_VECTOR_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 16. Timers/Counters
//------------------------------------------
#define TM_Test_Group_EN  0
//<q0> MG82F6D17_TIMER_DRV.c
#define MG82F6D17_TIMER_DRV_c_EN  0
//<q0> MG82F6D17_TIMER_DRV.h
#define MG82F6D17_TIMER_DRV_h_EN  0
//<q0> MG82F6D17_TIMER_MID.h
#define MG82F6D17_TIMER_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 17. PCA
//------------------------------------------
#define PCA_Test_Group_EN  0
//<q0> MG82F6D17_PCA_DRV.c
#define MG82F6D17_PCA_DRV_c_EN  0
//<q0> MG82F6D17_PCA_DRV.h
#define MG82F6D17_PCA_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 18. SerialPort0
//------------------------------------------
#define S0_Test_Group_EN  0
//<q0> MG82F6D17_UART0_DRV.c
#define MG82F6D17_UART0_DRV_c_EN  0
//<q0> MG82F6D17_UART0_DRV.h
#define MG82F6D17_UART0_DRV_h_EN  0
//<q0> MG82F6D17_UART0_MID.h
#define MG82F6D17_UART0_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 19. SerialPort1
//------------------------------------------
#define S1_Test_Group_EN  0
//<q0> MG82F6D17_UART1_DRV.c
#define MG82F6D17_UART1_DRV_c_EN  0
//<q0> MG82F6D17_UART1_DRV.h
#define MG82F6D17_UART1_DRV_h_EN  0
//<q0> MG82F6D17_UART1_MID.h
#define MG82F6D17_UART1_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 20. SPI
//------------------------------------------
#define SPI_Test_Group_EN  0
//<q0> MG82F6D17_SPI_DRV.c
#define MG82F6D17_SPI_DRV_c_EN  0
//<q0> MG82F6D17_SPI_DRV.h
#define MG82F6D17_SPI_DRV_h_EN  0
//<q0> MG82F6D17_SPI_MID.h
#define MG82F6D17_SPI_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 21. TWI0/I2C0
//------------------------------------------
#define TWI0_Test_Group_EN  0
//<q0> MG82F6D17_I2C0_DRV.h
#define MG82F6D17_I2C0_DRV_h_EN  0
//<q0> MG82F6D17_I2C0_MID.c
#define MG82F6D17_I2C0_MID_c_EN  0
//<q0> MG82F6D17_I2C0_MID.h
#define MG82F6D17_I2C0_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 22. STWI/SI2C
//------------------------------------------
#define STWI_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 23. Beeper
//------------------------------------------
#define Beeper_Test_Group_EN  0
//<q0> MG82F6D17_BEEPER_DRV.h
#define MG82F6D17_BEEPER_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 24. KBI
//------------------------------------------
#define KBI_Test_Group_EN  0
//<q0> MG82F6D17_KBI_DRV.h
#define MG82F6D17_KBI_DRV_h_EN  0
//<q0> MG82F6D17_KBI_MID.h
#define MG82F6D17_KBI_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 25. GPL
//------------------------------------------
#define GPL_Test_Group_EN  0
//</e>
//------------------------------------------
//<e0> Ch 26. ADC
//------------------------------------------
#define ADC_Test_Group_EN  0
//<q0> MG82F6D17_ADC_DRV.h
#define MG82F6D17_ADC_DRV_h_EN  0
//<q0> MG82F6D17_ADC_MID.h
#define MG82F6D17_ADC_MID_h_EN  0
//</e>
//------------------------------------------
//<e0> Ch 28. IAP
//------------------------------------------
#define IAP_Test_Group_EN  0
//<q0> MG82F6D17_IAP_DRV.c
#define MG82F6D17_IAP_DRV_c_EN  0
//<q0> MG82F6D17_IAP_DRV.h
#define MG82F6D17_IAP_DRV_h_EN  0
//</e>
//------------------------------------------
//<e0> Others
//------------------------------------------
#define Others_Test_Group_EN  0
//<q0> MG82F6D17_COMMON_DRV.c
#define MG82F6D17_COMMON_DRV_c_EN  0
//<q0> MG82F6D17_COMMON_DRV.h
#define MG82F6D17_COMMON_DRV_h_EN  0
//<q0> Compiler Test Group
#define Compiler_Test_Group_EN  1
//</e>
//*****************************************************************************
//<<< end of configuration section >>>
//*****************************************************************************


#define Test_Reserved_Group 0
#define Test_Reserved_File 0


void MG82F6D17_CLK_DRV_h(void);
void MG82F6D17_CLK_MID_h(void);
void MG82F6D17_PW_DRV_h(void);
void MG82F6D17_GPIO_DRV_h(void);
void MG82F6D17_GPIO_DRV_c(void);
void MG82F6D17_SPI_DRV_h(void);
void MG82F6D17_SPI_DRV_c(void);
void MG82F6D17_SPI_MID_h(void);

void Compiler_Test_Group(void);




/**
 *******************************************************************************
 * Summary Test Group
 *******************************************************************************
 */
void MG82F6D17_Test_Group()
{
    //------------------------------------------
    // Ch8. DMA
    //------------------------------------------
    #if DMA_Test_Group_EN
        #if MG82F6D17_DMA_DRV_h_EN
        #endif

        #if MG82F6D17_DMA_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch9. System Clock
    //------------------------------------------
    #if SYSCLK_Test_Group_EN
        #if MG82F6D17_CLK_DRV_h_EN
            MG82F6D17_CLK_DRV_h();
        #endif

        #if MG82F6D17_CLK_MID_h_EN
            MG82F6D17_CLK_MID_h();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch10. WDT
    //------------------------------------------
    #if WDT_Test_Group_EN
        #if MG82F6D17_WDT_DRV_h_EN
        #endif

        #if MG82F6D17_WDT_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch11. RTC
    //------------------------------------------
    #if RTC_Test_Group_EN
        #if MG82F6D17_RTC_DRV_h_EN
        #endif

        #if MG82F6D17_RTC_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif


    //------------------------------------------
    // Ch12. System Reset
    //------------------------------------------
    #if Reset_Test_Group_EN
        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif


    //------------------------------------------
    // Ch13. Power Management
    //------------------------------------------
    #if Power_Test_Group_EN
        #if MG82F6D17_PW_DRV_h_EN
            MG82F6D17_PW_DRV_h();
        #endif

        #if MG82F6D17_PW_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch14. GPIO
    //------------------------------------------
    #if GPIO_Test_Group_EN
        #if MG82F6D17_GPIO_DRV_h_EN
            MG82F6D17_GPIO_DRV_h();
        #endif

        #if MG82F6D17_GPIO_DRV_c_EN
            MG82F6D17_GPIO_DRV_c();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch15. Interrupt
    //------------------------------------------
    #if INT_Test_Group_EN
        #if MG82F6D17_INT_DRV_h_EN
        #endif

        #if MG82F6D17_INT_MID_h_EN
        #endif

        #if MG82F6D17_INT_VECTOR_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch16. Timers/Counters
    //------------------------------------------
    #if TM_Test_Group_EN
        #if MG82F6D17_TIMER_DRV_c_EN
        #endif

        #if MG82F6D17_TIMER_DRV_h_EN
        #endif

        #if MG82F6D17_TIMER_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch17. PCA
    //------------------------------------------
    #if PCA_Test_Group_EN
        #if MG82F6D17_PCA_DRV_c_EN
        #endif

        #if MG82F6D17_PCA_DRV_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch18. SerialPort0
    //------------------------------------------
    #if S0_Test_Group_EN
        #if MG82F6D17_UART0_DRV_c_EN
        #endif

        #if MG82F6D17_UART0_DRV_h_EN
        #endif

        #if MG82F6D17_UART0_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch19. SerialPort1
    //------------------------------------------
    #if S1_Test_Group_EN
        #if MG82F6D17_UART1_DRV_c_EN
        #endif

        #if MG82F6D17_UART1_DRV_h_EN
        #endif

        #if MG82F6D17_UART1_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch20. SPI
    //------------------------------------------
    #if SPI_Test_Group_EN
        #if MG82F6D17_SPI_DRV_c_EN
        #endif

        #if MG82F6D17_SPI_DRV_h_EN
        #endif

        #if MG82F6D17_SPI_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch21. TWI0/I2C0
    //------------------------------------------
    #if TWI0_Test_Group_EN
        #if MG82F6D17_I2C0_DRV_h_EN
        #endif

        #if MG82F6D17_I2C0_MID_c_EN
        #endif

        #if MG82F6D17_I2C0_MID_h_EN
        #endif
    #endif

    //------------------------------------------
    // Ch22. STWI/SI2C
    //------------------------------------------
    #if STWI_Test_Group_EN
        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch23. Beeper
    //------------------------------------------
    #if Beeper_Test_Group_EN
        #if MG82F6D17_BEEPER_DRV_h_EN
        #endif
        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch24. KBI
    //------------------------------------------
    #if KBI_Test_Group_EN
        #if MG82F6D17_KBI_DRV_h_EN
        #endif

        #if MG82F6D17_KBI_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch25. GPL
    //------------------------------------------
    #if GPL_Test_Group_EN
        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch26. ADC
    //------------------------------------------
    #if ADC_Test_Group_EN
        #if MG82F6D17_ADC_DRV_h_EN
        #endif

        #if MG82F6D17_ADC_MID_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Ch28. IAP
    //------------------------------------------
    #if IAP_Test_Group_EN
        #if MG82F6D17_IAP_DRV_c_EN
        #endif

        #if MG82F6D17_IAP_DRV_h_EN
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Others
    //------------------------------------------
    #if Others_Test_Group_EN
        #if MG82F6D17_COMMON_DRV_c_EN
        #endif

        #if MG82F6D17_COMMON_DRV_h_EN
        #endif

        #if Compiler_Test_Group_EN
            Compiler_Test_Group();
        #endif

        #if Test_Reserved_File
        #endif
    #endif

    //------------------------------------------
    // Reserved
    //------------------------------------------
    #if Test_Reserved_Group
        #if Test_Reserved_File
        #endif
    #endif
}



