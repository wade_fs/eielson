#include "MG82F6D17_CONFIG.h"

/**
 *******************************************************************************
 * Compiler Test Group
 *******************************************************************************
 */
//Lv0:?    Lv8:?
#define SFRPIx_SFR_ReadModifyWrite(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
    )

//Lv0:?    Lv8:?
#define SFRPIx_SFR_ReadModifyWrite_SFRPI0(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
        __DRV_SFR_PageIndex(0);\
    )

//Lv0:?    Lv8:?
#define PUSH_SFRPIx_SFR_ReadModifyWrite_POP(__STATE__)\
    MWT(\
        _push_(SFRPI);\
        __DRV_SFR_PageIndex(7);\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
        _pop_(SFRPI);\
    )

#define Test_Dis 0x33
#define Test_En  0xAA

//Lv0:?    Lv8:?
void Func_SFRPIx_SFR_ReadModifyWrite(uint8_t _VAR_)
{
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
}

//Lv0:?    Lv8:?
void Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(uint8_t _VAR_)
{
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
    __DRV_SFR_PageIndex(0);
}

//Lv0:?    Lv8:?
void Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(uint8_t _VAR_)
{
    _push_(SFRPI);
    __DRV_SFR_PageIndex(7);
    CKCON0 &= ~ENCKM;
    CKCON0 |= _VAR_;
    _pop_(SFRPI);
}

void Compiler_Test_Group(void)
{
    #if 0
        uint8_t tmp8;
        uint16_t tmp16;
    #endif

    #if 1
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- SFRPI=x, Read Write SFR ---
        //Lv0:?    Lv8:?
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //1
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //2
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //3
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //4
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //5
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //6
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //7
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //8
        SFRPIx_SFR_ReadModifyWrite(MW_DISABLE);     //9
        SFRPIx_SFR_ReadModifyWrite(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- SFRPI=x, Read Write SFR, SFRPI=0 ---
        //Lv0:?    Lv8:?
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //1
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //2
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //3
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //4
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //5
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //6
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //7
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //8
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_DISABLE);     //9
        SFRPIx_SFR_ReadModifyWrite_SFRPI0(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- Push(SFRPI), SFRPI=x, Read Write SFR, Pop(SFRPI) ---
        //Lv0:?    Lv8:?
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //1
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //2
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //3
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //4
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //5
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //6
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //7
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //8
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_DISABLE);     //9
        PUSH_SFRPIx_SFR_ReadModifyWrite_POP(MW_ENABLE);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- Function: SFRPI=x, Read Write SFR ---
        //Lv0:?    Lv8:?
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //1
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //2
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //3
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //4
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //5
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //6
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //7
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //8
        Func_SFRPIx_SFR_ReadModifyWrite(Test_Dis);     //9
        Func_SFRPIx_SFR_ReadModifyWrite(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //--- Function: SFRPI=x, Read Write SFR, SFRPI=0 ---
        //Lv0:?    Lv8:?
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //1
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //2
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //3
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //4
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //5
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //6
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //7
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //8
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_Dis);     //9
        Func_SFRPIx_ReadModifyWrite_SFR_SFRPI0(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //--- Function: Push(SFRPI), SFRPI=x, Read Write SFR, Pop(SFRPI) ---
        //Lv0:?    Lv8:?
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //1
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //2
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //3
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //4
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //5
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //6
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //7
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //8
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_Dis);     //9
        Func_PUSH_SFRPIx_ReadModifyWrite_SFR_POP(Test_En);      //10
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
        //---------------------------------------------------
        nop();  ACC=0x46;
        //---------------------------------------------------
        //---------------------------------------------------
        nop();  ACC=0xB9;
        //---------------------------------------------------
    #endif

    #if 0
        nop();  ACC=0x46;
        __DRV_GPIO_InversePinP6(P6);    //Lv0:19    //Lv8:17
        nop();  ACC=0xB9;
        __DRV_SPI_PinMux_Select(SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17);  //Lv0:9     //Lv8:9
        nop();  ACC=0x46;
        __DRV_GPIO_InversePinP6(P60);   //Lv0:9     //Lv8:9
        nop();  ACC=0xB9;
        __DRV_SPI_PinMux_Select(SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33);  //Lv0:9     //Lv8:9
        nop();  ACC=0x46;
        __DRV_SFR_PageIndex(7);     //Lv0:9     //Lv8:9
        AUXR10=AUXR10&(~SPIPS0);
        nop();  ACC=0xB9;
        __DRV_SFR_PageIndex(7);     //Lv0:9     //Lv8:9
        AUXR10=AUXR10&(~SPIPS0);
        __DRV_SFR_PageIndex(0);
        nop();  ACC=0x46;
        _push_(SFRPI);              //Lv0:10     //Lv8:10
        __DRV_SFR_PageIndex(7);
        AUXR10=AUXR10&(~SPIPS0);
        _pop_(SFRPI);
        nop();  ACC=0xB9;
    #endif


    #if 0
        //Complie Level 0: 6 CMD
        //Complie Level 8: 3 CMD
        do{tmp8--;}while(tmp8!=0);
        nop();
        //Complie Level 0: 6 CMD
        //Complie Level 8: 4 CMD
        do{IFD--;}while(IFD!=0);
        nop();
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        for(tmp8=0 ; tmp8<253 ; tmp8++);
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        for(IFD=0 ; IFD<253 ; IFD++);
        nop();
        //Complie Level 0: 8 CMD
        //Complie Level 8: 5 CMD
        for(tmp8=253 ; tmp8!=0 ; tmp8--);
        nop();
        //Complie Level 0: 8 CMD
        //Complie Level 8: 6 CMD
        for(IFD=253 ; IFD!=0 ; IFD--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        while(tmp8--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        while(IFD--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        do{}while(tmp8--);
        nop();
        //Complie Level 0: 4 CMD
        //Complie Level 8: 4 CMD
        do{}while(IFD--);
        nop();
        //Complie Level 0: 11 CMD
        //Complie Level 8: 7 CMD
        do{}while(tmp8--, tmp8!=0);
        nop();
        //Complie Level 0: ? CMD
        //Complie Level 8: ? CMD
        do{}while(IFD--, IFD!=0);
        nop(); nop();
    #endif
}

