/**
 ******************************************************************************
 *
 * @file        MG82F6D17_I2C0_FLOW_CONTROL.c
 *
 * @brief       This is the C code format main file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.07
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"

extern void CT1711_Read_Temp_Degree(void);
extern xdata BYTE SLAVE_ADDR1[3];
extern xdata BYTE Read_CT1711;
xdata BYTE Slavre_Rx_DATA_Count;
 /**
 *******************************************************************************
 * @brief       I2C0 ByteMode Handle
 * @details     
 * @return      No
 * @note        
 * @par         Example
 * @code        
                Sample_I2C0_ByteMode_Handle();
 * @endcode     
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_I2C0_IT_EN
/// @endcond
void Sample_I2C0_ISR_Handle()
{
    uint8_t lState, lEventFlag;
    
    _push_(SFRPI);
    __DRV_SFR_PageIndex(0);
    
    
    lEventFlag = __DRV_I2C0_GetEventFlag();
    if(lEventFlag == 0)    //Return if SI non set
        return;
        
    lState = __DRV_I2C0_GetEventCode();
    
    switch (lState){
        case 0x00:  // Bus Error
            //TBD by user
            break;
            
        case 0x08:  // A START condition has been transmitted
        case 0x10:  // Repeated start condition
            __DRV_I2C0_Set_STA_STO_AA_000();
            break;
            
        case 0x18:  // MT SLA+W sent and ACK received
            break;
            
        case 0x28:  // MT DATA sent and ACK received
            break;
            
        case 0x30:  // MT DATA sent NACK received
            break;
            
        case 0x20:  // MT SLA+W sent NACK received
        case 0x48:  // MR SLA+R sent NACK received
            break;
        
//-------------------------------------
        case 0x38:  // Arbitration lost
            //TBD by user
            break;
        
//-------------------------------------            
        case 0x40:  // MR SLA+R sent and ACK received
            break;
            
        case 0x50:  // MR Data Received and ACK sent
            break;
            
        case 0x58:  // MR Data Received and NACK sent
            break;

//-------------------------------------
        case 0x68:  // Arbitration lost in SLA+R/W as master,
                    // Own SLA+W has been Received ACK has bee returned
            __DRV_I2C0_Set_STA_STO_AA_001();
            Slavre_Rx_DATA_Count = 0;
            break;
        
        case 0x78:  // Arbitration lost in SLA+R/W as master,
                    // General Call address has been received ACK has been returned
            break;
        
        case 0x60:  // Own SLA+W has bee Received ACK has been returned
        case 0x70:  // General Call address has been received ACK has been returned
            __DRV_I2C0_Set_STA_STO_AA_001();
            break;
        
        case 0x80:  // Data byte has been received ACK has been return
        case 0x90:  // Previously address with General Call address
                    // Data byte has been received ACK has been return
            __DRV_I2C0_Set_STA_STO_AA_001();
            break;
        
        case 0xA0:  // A STOP or repeated START has been received while will addressed as SLV/REC
            __DRV_I2C0_Set_STA_STO_AA_001();
            break;
        
        case 0x88:  // Data byte has been received Not ACK has been return
        case 0x98:  // Previously address with General Call address
                    // Data byte has been received Not ACK has been return
            break;

//-------------------------------------
        case 0xB0:  // Arbitration lost in SLA+R/W as master,
                    // Own SLA+R has been Received ACK has bee returned 
        case 0xA8:  // Own SLA+R has bee Received ACK has been returned
            __DRV_I2C0_Set_STA_STO_AA_001();
            if (Read_CT1711 == 0)
            {
               Read_CT1711 = 1;
               __DRV_I2C0_IT_Disable();
            }
            else
            {
                Slavre_Rx_DATA_Count = 0;
                __DRV_I2C0_SetBufData(SLAVE_ADDR1[Slavre_Rx_DATA_Count]);//Send Data
                Slavre_Rx_DATA_Count++;
                Read_CT1711 = 0;
            }
            break;
            


        case 0xC0:  // Data byte or Last data byte in SIDAT has been transmitted Not ACK has been received
            __DRV_I2C0_Set_STA_STO_AA_001();
            break;

        case 0xB8:  // Data byte in SIDAT has been transmitted ACK has been received
        case 0xC8:  // Last Data byte in SIDAT has been transmitted ACK has been received
            __DRV_I2C0_Set_STA_STO_AA_001();
            __DRV_I2C0_SetBufData(SLAVE_ADDR1[Slavre_Rx_DATA_Count]);//Send Data
            Slavre_Rx_DATA_Count++;
            break;            
        

//-------------------------------------
//        case 0xD0: 
//        case 0xD8:
//        case 0xE0:
//        case 0xE8:
//        case 0xF0:
//-------------------------------------      
        case 0xF8:  // Bus Idle

        default:
            __DRV_I2C0_Set_STA_STO_AA_001();
            break;
    }
    if ( Read_CT1711 == 0)
        __DRV_I2C0_ClearEventFlag();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond




