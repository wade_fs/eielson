/**
 ******************************************************************************
 *
 * @file        MG82F6D17_WIZARD.c
 *
 * @brief       This is a C code for Wizard interface.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IS" without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the worldwide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 ******************************************************************************
 */

#include "MG82F6D17_CONFIG.h"

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #0.07_Brian_20200605 //bugNum_Authour_Date
 * >> Modify from Xian Qiu email zip file: MG82F6D17_010_GPIO_Wizard.c
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing GPIO by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
               DRV_GPIO_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_GPIO_WIZARD
/// @endcond
void DRV_GPIO_Wizard_Init(void)
{
    /*--------------------Port 1---------------------*/
    /*--------------P1_0-------------------*/
    #if MG82F6D17_GPIO_P1_0
        #if (P10_MODE != P10_InputOnly)
            __DRV_GPIO_P1ModeSelect(P10_MODE);
        #endif
        #if (P1FDC_0 != NORMAL)
            __DRV_GPIO_SetP1FastDriving(P1FDC_0 ,FAST);
        #endif
    #endif
    /*--------------P1_1-------------------*/
    #if MG82F6D17_GPIO_P1_1
        #if (P11_MODE != P11_InputOnly)
            __DRV_GPIO_P1ModeSelect(P11_MODE);
        #endif
        #if (P1FDC_1 != NORMAL)
            __DRV_GPIO_SetP1FastDriving((P1FDC_1 << 1),FAST);
        #endif
    #endif
    /*--------------P1_5-------------------*/
    #if MG82F6D17_GPIO_P1_5
        #if (P15_MODE != P15_InputOnly)
            __DRV_GPIO_P1ModeSelect(P15_MODE);
        #endif
        #if (P1FDC_5 != NORMAL)
            __DRV_GPIO_SetP1FastDriving((P1FDC_5 << 5),FAST);
        #endif
    #endif
    /*--------------P1_6-------------------*/
    #if MG82F6D17_GPIO_P1_6
        #if (P16_MODE != P16_InputOnly)
            __DRV_GPIO_P1ModeSelect(P16_MODE);
        #endif
        #if (P1FDC_6 != NORMAL)
            __DRV_GPIO_SetP1FastDriving((P1FDC_6 << 6),FAST);
        #endif
    #endif
    /*--------------P1_7-------------------*/
    #if MG82F6D17_GPIO_P1_7
        #if (P17_MODE != P17_InputOnly)
            __DRV_GPIO_P1ModeSelect(P17_MODE);
        #endif
        #if (P1FDC_7 != NORMAL)
            __DRV_GPIO_SetP1FastDriving((P1FDC_7 << 7),FAST);
        #endif
    #endif
    /*---------P1 Driving Strength---------*/
    #if P1_DRIVEIN_STRENGTH
        #if (P1DC_0 != 0)
            __DRV_GPIO_P1OutputDrivingConfig(P1DC0,P1DC_0);
        #endif
        #if (P1DC_1 != 0)
            __DRV_GPIO_P1OutputDrivingConfig(P1DC1,P1DC_1);
        #endif
    #endif

    /*--------------------Port 2---------------------*/
    /*--------------P2_2-------------------*/
    #if MG82F6D17_GPIO_P2_2
        #if (P22_MODE != P22_InputOnly)
            __DRV_GPIO_P2ModeSelect(P22_MODE);
        #endif
        #if (P2FDC_2 != NORMAL)
            __DRV_GPIO_SetP2FastDriving((P2FDC_2 << 2),FAST);
        #endif
    #endif
    /*--------------P2_4-------------------*/
    #if MG82F6D17_GPIO_P2_4
        #if (P24_MODE != P24_InputOnly)
            __DRV_GPIO_P2ModeSelect(P24_MODE);
        #endif
        #if (P2FDC_4 != NORMAL)
            __DRV_GPIO_SetP2FastDriving((P2FDC_4 << 4),FAST);
        #endif
    #endif
    /*---------P2 Driving Strength---------*/
    #if P2_DRIVEIN_STRENGTH
        #if (P2DC_0 != 0)
            __DRV_GPIO_P2OutputDrivingConfig(P2DC0,P2DC_0);
        #endif
        #if (P2DC_1 != 0)
            __DRV_GPIO_P2OutputDrivingConfig(P2DC1,P2DC_1);
        #endif
    #endif

    /*--------------------Port 3---------------------*/
    /*--------------P3_0-------------------*/
    #if MG82F6D17_GPIO_P3_0
        #if (P30_MODE != P30_QuasiMode)
            __DRV_GPIO_P3ModeSelect(P30_MODE);
        #endif
        #if (P3FDC_0 != NORMAL)
            __DRV_GPIO_SetP3FastDriving(P3FDC_0,FAST);
        #endif
    #endif
    /*--------------P3_1-------------------*/
    #if MG82F6D17_GPIO_P3_1
        #if (P31_MODE != P31_QuasiMode)
            __DRV_GPIO_P3ModeSelect(P31_MODE);
        #endif
        #if (P3FDC_1 != NORMAL)
            __DRV_GPIO_SetP3FastDriving((P3FDC_1<<1),FAST);
        #endif
    #endif
    /*--------------P3_3-------------------*/
    #if MG82F6D17_GPIO_P3_3
        #if (P33_MODE != P33_QuasiMode)
            __DRV_GPIO_P3ModeSelect(P33_MODE);
        #endif
        #if (P3FDC_3 != NORMAL)
            __DRV_GPIO_SetP3FastDriving((P3FDC_3<<3),FAST);
        #endif
    #endif
    /*--------------P3_4-------------------*/
    #if MG82F6D17_GPIO_P3_4
        #if (P34_MODE != P34_QuasiMode)
            __DRV_GPIO_P3ModeSelect(P34_MODE);
        #endif
        #if (P3FDC_4 != NORMAL)
            __DRV_GPIO_SetP3FastDriving((P3FDC_4<<4),FAST);
        #endif
    #endif
    /*--------------P3_5-------------------*/
    #if MG82F6D17_GPIO_P3_5
        #if (P35_MODE != P35_QuasiMode)
            __DRV_GPIO_P3ModeSelect(P35_MODE);
        #endif
        #if (P3FDC_5 != NORMAL)
            __DRV_GPIO_SetP3FastDriving((P3FDC_5<<5),FAST);
        #endif
    #endif
    /*---------P3 Driving Strength---------*/
    #if P3_DRIVEIN_STRENGTH
        #if (P3DC_0 != 0)
            __DRV_GPIO_P3OutputDrivingConfig(P3DC0,P3DC_0);
        #endif
        #if (P3DC_1 != 0)
            __DRV_GPIO_P3OutputDrivingConfig(P3DC1,P3DC_1);
        #endif
    #endif

    /*--------------------Port 4---------------------*/
    /*---------Port 4 Protection-----------*/
    #if MG82F6D17_GPIO_Port4_Protection
        #if (SPCON0_P4CTL != MW_DISABLE)
            __DRV_GPIO_P4_Protected(SPCON0_P4CTL);
        #endif
    #endif
    /*--------------P4_4-------------------*/
    #if MG82F6D17_GPIO_P4_4
        #if (P44_MODE != 1)
            #if (P44_MODE != P44_InputOnly)
                __DRV_GPIO_P4ModeSelect(P44_MODE);
            #endif
        #endif
        #if (P4FDC_4 != NORMAL)
            __DRV_GPIO_SetP4FastDriving((P4FDC_4<<4),FAST);
        #endif
    #endif
    /*--------------P4_5-------------------*/
    #if MG82F6D17_GPIO_P4_5
        #if (P45_MODE != 1)
            #if (P45_MODE != P45_InputOnly)
                __DRV_GPIO_P4ModeSelect(P45_MODE);
            #endif
        #endif
        #if (P4FDC_5 != NORMAL)
            __DRV_GPIO_SetP4FastDriving((P4FDC_5<<5),FAST);
        #endif
    #endif
    /*--------------P4_7-------------------*/
    #if MG82F6D17_GPIO_P4_7
        #if (P47_MODE != 1)
            #if (P47_MODE != P47_InputOnly)
                __DRV_GPIO_P4ModeSelect(P47_MODE);
            #endif
        #endif
        #if (P4FDC_7 != NORMAL)
            __DRV_GPIO_SetP4FastDriving((P4FDC_7<<7),FAST);
        #endif
    #endif
    /*---------P4 Driving Strength---------*/
    #if P4_DRIVEIN_STRENGTH
        #if (P4DC_1 != 0)
            __DRV_GPIO_P4OutputDrivingConfig(P4DC1,P4DC_1);
        #endif
    #endif
    /*-----------P4 OCD Function-----------*/
    #if (P44_MODE != 1)||(P45_MODE != 1)
        __DRV_GPIO_SetOCD2IO(DCON0_OCDE);
    #endif
    #if (P47_MODE != 1)
        __DRV_GPIO_SetRST2IO(DCON0_RSTIO);
    #endif

    /*--------------------Port 6---------------------*/
    /*---------Port 6 Protection-----------*/
    #if MG82F6D17_GPIO_Port6_Protection
        #if (SPCON0_P6CTL != MW_DISABLE)
            __DRV_GPIO_P6_Protected(SPCON0_P6CTL);
        #endif
    #endif
    /*--------------P6_0-------------------*/
    #if MG82F6D17_GPIO_P6_0
        #if (P60_MODE != P60_InputOnly)
            __DRV_GPIO_P6ModeSelect(P60_MODE);
        #endif
    #endif
    #if MG82F6D17_GPIO_P6_1
        #if (P61_MODE != P61_InputOnly)
            __DRV_GPIO_P6ModeSelect(P61_MODE);
        #endif
    #endif

    //-------------------------------------------------
    // AUXRn, XCIFGn Function
    //-------------------------------------------------
    // --- T0/T0CKO ---
    #if   (((MG82F6D17_GPIO_P4_4==1)&(AF_P44==WizDef_T0))|((MG82F6D17_GPIO_P4_4==1)&(AF_P44==WizDef_T0CKO)))
        SFRPI=0; AUXR3 |= T0PS0;
    #elif (((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_T0))|((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_T0CKO)))
        SFRPI=0; AUXR3 |= T0PS1;
    #elif (((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_T0))|((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_T0CKO)))
        SFRPI=0; AUXR3 |= (T0PS1|T0PS0);
    #endif
    //-------------------------------------------------
    // --- Serial Port 0 pin Selection 0 ---
    #if   (((MG82F6D17_GPIO_P4_4==1)&(AF_P44==WizDef_RXD0))|((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_TXD0)))
        SFRPI=0; AUXR3 |= S0PS0;
    #elif (((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_RXD0))|((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_TXD0)))
        SFRPI=7; AUXR10 |= S0PS1;
    #elif (((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_RXD0))|((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_TXD0)))
        SFRPI=0; AUXR3 |= S0PS0;
        SFRPI=7; AUXR10 |= S0PS1;
    #endif
    //-------------------------------------------------
    // --- TWI0/I2C0 Port Selection [1:0] ---
    #if   (((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_TWI0_SCL))|((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_TWI0_SDA)))
        SFRPI=0; AUXR3 |= TWIPS0;
    #elif (((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_TWI0_SCL))|((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_TWI0_SDA)))
        SFRPI=0; AUXR3 |= TWIPS1;
    #elif (((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_TWI0_SCL))|((MG82F6D17_GPIO_P2_4==1)&(AF_P24==WizDef_TWI0_SDA)))
        SFRPI=0; AUXR3 |= (TWIPS1|TWIPS0);
    #endif
    //-------------------------------------------------
    // --- T2/T2CKO, T2EX ---
    #if   (((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_T2))|((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_T2CKO))|((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_T2EX)))
        SFRPI=1; AUXR4 |= T2PS0;
    #elif (((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_T2))|((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_T2CKO))|((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_T2EX)))
        SFRPI=1; AUXR4 |= T2PS1;
    #elif (((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_T2))|((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_T2CKO))|((MG82F6D17_GPIO_P4_4==1)&(AF_P44==WizDef_T2EX)))
        SFRPI=1; AUXR4 |= (T2PS1|T2PS0);
    #endif
    //-------------------------------------------------
    // --- T1/T1CKO ---
    #if   (((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_T1))|((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_T1CKO)))
        SFRPI=1; AUXR4 |= T1PS0;
    #elif (((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_T1))|((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_T1CKO)))
        SFRPI=1; AUXR4 |= T1PS1;
    #elif (((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_T1))|((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_T1CKO)))
        SFRPI=1; AUXR4 |= (T1PS1|T1PS0);
    #endif
    //-------------------------------------------------
    // --- PWM2A, PWM2B ---
    #if   (((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_PWM2A))|((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_PWM2B)))
        SFRPI=2; AUXR5 |= C0PPS1;
    #endif
    //-------------------------------------------------
    // --- PWM0A, PWM0B ---
    #if   (((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_PWM0A))|((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_PWM0B)))
        SFRPI=2; AUXR5 |= C0PPS0;
    #endif
    //-------------------------------------------------
    // --- PCA0: CEX0, CEX2, CEX4 ---
    #if   (((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_PCA0_CEX0))|((MG82F6D17_GPIO_P2_4==1)&(AF_P24==WizDef_PCA0_CEX2_1))|((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_PCA0_CEX4)))
        SFRPI=2; AUXR5 |= C0PS0;
    #endif
    //-------------------------------------------------
    // --- PCA0: ECI ---
    #if   ((MG82F6D17_GPIO_P1_6==1)&(AF_P16==WizDef_PCA0_ECI))
        SFRPI=2; AUXR5 |= ECIPS0;
    #endif
    //-------------------------------------------------
    // --- PCA0: C0CKO ---
    #if   ((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_PCA0_C0CKO))
        SFRPI=2; AUXR5 |= C0COPS;
    #endif
    //-------------------------------------------------
    // --- KBI4, KBI5 ---
    #if   (((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_KBI4))|((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_KBI5)))
        SFRPI=3; AUXR6 |= KBI4PS0;
    #elif (((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_KBI4))|((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_KBI5)))
        SFRPI=3; AUXR6 |= KBI4PS1;
    #elif (((MG82F6D17_GPIO_P1_5==1)&(AF_P15==WizDef_KBI4))|((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_KBI5)))
        SFRPI=3; AUXR6 |= (KBI4PS1|KBI4PS0);
    #endif
    //-------------------------------------------------
    // --- KBI6, KBI7 ---
    #if   (((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_KBI6))|((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_KBI7)))
        SFRPI=3; AUXR6 |= KBI6PS0;
    #endif
    //-------------------------------------------------
    // --- KBI2, KBI3 ---
    #if   (((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_KBI2))|((MG82F6D17_GPIO_P2_4==1)&(AF_P24==WizDef_KBI3)))
        SFRPI=3; AUXR6 |= KBI2PS0;
    #endif
    //-------------------------------------------------
    // --- S0MI, S1MI ---
    #if   (((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_S0MI))|((MG82F6D17_GPIO_P4_7==1)&(AF_P47==WizDef_S1MI)))
        SFRPI=3; AUXR6 |= SnMIPS;
    #endif
    //-------------------------------------------------
    // --- S0CKO ---
    #if   ((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_S0CKO))
        SFRPI=3; AUXR6 |= S0COPS;
    #endif
    //-------------------------------------------------
    // --- PWM6, PWM7 ---
    #if   (((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_PWM6))|((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_PWM7)))
        SFRPI=5; AUXR8 |= C0PPS2;
    #endif
    //-------------------------------------------------
    // --- KBI0, KBI1 ---
    #if   (((MG82F6D17_GPIO_P4_7==1)&(AF_P47==WizDef_KBI0))|((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_KBI1)))
        SFRPI=5; AUXR8 |= KBI0PS0;
    #endif
    //-------------------------------------------------
    // --- S1CKO ---
    #if   ((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_S1CKO))
        SFRPI=5; AUXR8 |= S1COPS;
    #endif
    //-------------------------------------------------
    // --- RXD1, TXD1 ---
    #if   (((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_RXD1))|((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_TXD1)))
        SFRPI=6; AUXR9 |= S1PS0;
    #elif (((MG82F6D17_GPIO_P4_4==1)&(AF_P44==WizDef_RXD1))|((MG82F6D17_GPIO_P4_5==1)&(AF_P45==WizDef_TXD1)))
        SFRPI=6; AUXR9 |= S1PS1;
    #elif (((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_RXD1))|((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_TXD1)))
        SFRPI=6; AUXR9 |= (S1PS1|S1PS0);
    #endif
    //-------------------------------------------------
    // --- nSS, MOSI, MISO, SPICLK ---
    #if   (((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_nSS))&((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_MOSI))&((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_MISO))&((MG82F6D17_GPIO_P3_3==1)&(AF_P33==WizDef_SPICLK)))
        SFRPI=7; AUXR10 |= SPIPS0;
    #endif
    //-------------------------------------------------
    // --- nINT1 ---
    #if   ((MG82F6D17_GPIO_P3_1==1)&(AF_P31==WizDef_nINT1))    //001
        SFRPI=0; XICFG |= INT1IS0;
    #elif ((MG82F6D17_GPIO_P3_5==1)&(AF_P35==WizDef_nINT1))    //010
        SFRPI=0; XICFG |= INT1IS1;
    #elif ((MG82F6D17_GPIO_P1_0==1)&(AF_P10==WizDef_nINT1))    //011
        SFRPI=0; XICFG |= (INT1IS1|INT1IS0);
    #elif ((MG82F6D17_GPIO_P6_1==1)&(AF_P61==WizDef_nINT1))    //100
        SFRPI=1; XICFG1 |= INT1IS2;
    #elif ((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_nINT1))    //101
        SFRPI=1; XICFG1 |= INT1IS2;
        SFRPI=0; XICFG |= INT1IS0;
    #elif ((MG82F6D17_GPIO_P1_5==1)&(AF_P15==WizDef_nINT1))    //110
        SFRPI=1; XICFG1 |= INT1IS2;
        SFRPI=0; XICFG |= INT1IS1;
    #elif ((MG82F6D17_GPIO_P2_4==1)&(AF_P24==WizDef_nINT1))    //111
        SFRPI=1; XICFG1 |= INT1IS2;
        SFRPI=0; XICFG |= (INT1IS1|INT1IS0);
    #endif
    //-------------------------------------------------
    // --- nINT0 ---
    #if   ((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_nINT0))    //001
        SFRPI=0; XICFG |= INT0IS0;
    #elif ((MG82F6D17_GPIO_P3_4==1)&(AF_P34==WizDef_nINT0))    //010
        SFRPI=0; XICFG |= INT0IS1;
    #elif ((MG82F6D17_GPIO_P4_7==1)&(AF_P47==WizDef_nINT0))    //011
        SFRPI=0; XICFG |= (INT0IS1|INT0IS0);
    #elif ((MG82F6D17_GPIO_P6_0==1)&(AF_P60==WizDef_nINT0))    //100
        SFRPI=1; XICFG1 |= INT0IS2;
    #elif ((MG82F6D17_GPIO_P1_1==1)&(AF_P11==WizDef_nINT0))    //101
        SFRPI=1; XICFG1 |= INT0IS2;
        SFRPI=0; XICFG |= INT0IS0;
    #elif ((MG82F6D17_GPIO_P1_7==1)&(AF_P17==WizDef_nINT0))    //110
        SFRPI=1; XICFG1 |= INT0IS2;
        SFRPI=0; XICFG |= INT0IS1;
    #elif ((MG82F6D17_GPIO_P2_2==1)&(AF_P22==WizDef_nINT0))    //111
        SFRPI=1; XICFG1 |= INT0IS2;
        SFRPI=0; XICFG |= (INT0IS1|INT0IS0);
    #endif
    //-------------------------------------------------
    // --- nINT2 ---
    #if   ((MG82F6D17_GPIO_P3_0==1)&(AF_P30==WizDef_nINT2))    //01
        SFRPI=1; XICFG1 |= INT2IS0;
    #elif ((MG82F6D17_GPIO_P1_1==1)&(AF_P11==WizDef_nINT2))    //10
        SFRPI=1; XICFG1 |= INT2IS1;
    #elif ((MG82F6D17_GPIO_P1_6==1)&(AF_P16==WizDef_nINT2))    //11
        SFRPI=1; XICFG1 |= (INT2IS1|INT2IS0);
    #endif
    //-------------------------------------------------
    SFRPI=0;

}

/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Brian_20200527 //bugNum_Authour_Date
 * >> Add system clock initial void function for code size.
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing system clock by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
               DRV_CLK_Wizard_Init();
* @endcode
*******************************************************************************
*/

//@del{
/*
BYTE[3] = u32B3(__SELECT__) = DCON0.7.5
BYTE[3] = u32B3(__SELECT__) = CKCON5.0
BYTE[2] = u32B2(__SELECT__) = CKCON3
BYTE[1] = u32B1(__SELECT__) = CKCON2
BYTE[0] = u32B0(__SELECT__) = CKCON0

CKCON5.0: CKMS0_P

CKCON3: WDTCS[1:0], FWKP, WDTFS, MCKD[1:0]
    MCKD[1:0]: MCKDO = MCK/1, /2, /4, /8

CKCON2: IHRCOE, MCKS[1:0], OSCS[1:0]
    MCKS[1:0]: PLL/OSCin output to MCK Source selection. (OSCin/PLL...etc)
    OSCS[1:0]: OSCin Source selection.

CKCON0: AFS, ENCKM, CKMIS[1:0], CCKS, SCKS[2:0]
    CKMIS[1:0]: PLL Input Clock divider
*/
//@del}

/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_CLK_WIZARD
/// @endcond
void DRV_CLK_Wizard_Init(void)
{
    #if (Wizard_SysClk_Select == SYS_12M_CPU_12M)
        //Default Sysclk = Cpuclk = 12MHz
    #else

        //================================//
        // HSE HSE1   CPUCLK
        //  1   0       6MHz~25MHz (default)
        //  1   1       > 25MHz
        //  0   0       < 6MHz
        //================================//
        #if (Wizard_DCON0_HSE != (HSE_P << 24))
            __DRV_CLK_CPUCLK_FASTER_25MHz();
        #endif

        //================================//
        // IHRCO=12MHz or 11.0592MHz
        // Delay 32us time for stable
        //================================//
        #if Wizard_CKCON0_AFS
            CKCON0 |= (Wizard_CKCON0 & AFS);
            IFD=255; while(IFD--);
        #endif

        //================================//
        // Change OSCin, default IHRCO
        //================================//
        #if (Wizard_CKCON2_OSCS != (OSCin_IHRCO<<8))
            DRV_PageP_Read(CKCON2_P);
            IFD |= (Wizard_CKCON2_P & (OSCS1_P|OSCS0_P));
            DRV_PageP_Write(CKCON2_P, IFD);
        #endif

        //================================//
        // Check PLL Enable/Disable
        //================================//
        #if Wizard_CKCON0_ENCKM
            //-----------------------------------
            // PLL enable and select clock source
            #if (Wizard_CKCON0_CKMIS != CKM_OSCin_DIV_2)
                //CKCON0: AFS, ENCKM, CKMIS[1:0], CCKS, SCKS[2:0]
                CKCON0 &= ~(CKMIS1|CKMIS0);
            #endif
            CKCON0 |= (Wizard_CKCON0 & (ENCKM|CKMIS1|CKMIS0));

            //-----------------------------------
            // PLL 96(default) / 144
            #if (Wizard_CKCON5_CKMS == (CKMS0_P<<24))
                DRV_PageP_Write(CKCON5_P, Wizard_CKCON5_P);
            #endif
            //-----------------------------------
            // Delay 100us for stable
            // Note: Code size: lv0=99, lv8=89
            IFD=255;  while(IFD--);  while(IFD--);  while(IFD--);  while(IFD--);
        #endif  //Wizard_CKCON0_ENCKM

        #if Wizard_CKCON0_SEL
            //CKCON0: AFS, ENCKM, CKMIS[1:0], CCKS, SCKS[2:0]
            CKCON0 |= Wizard_CKCON0_SEL;
        #endif

        #if Wizard_CKCON3_SEL
            //CKCON3: WDTCS[1:0], FWKP, WDTFS, MCKD[1:0]
            DRV_PageP_Read (CKCON3_P);
            IFD |= Wizard_CKCON3_P;
            DRV_PageP_Write(CKCON3_P, IFD);
        #endif

        #if (Wizard_CKCON2_SEL != (IHRCOE_P << 8))
            //CKCON2: IHRCOE,MCKS[1:0],OSCS[1:0]
            //Set to target frequency, or disable IHRCOE
            DRV_PageP_Write(CKCON2_P, Wizard_CKCON2_P);
        #endif

        //@del{
        /*
        #if (Wizard_IHRCO_Check == 0x00000000)
            #error "Clock source error: IHRCO Disable, but OSCin=IHRCO."
        #elif (Wizard_IHRCO_Check == 0x00001200)
            #warning "Warning message: IHRCO Enable, but OSCin=ILRCO."
        #elif (Wizard_IHRCO_Check == 0x00001300)
            #warning "Warning message: IHRCO Enable, but OSCin=ECKI."
        #endif
        */
        //@del}

        //================================//
        // DCON0_P : HSE_P, HSE1_P
        //================================//
        #if (Wizard_DCON0_HSE != (HSE_P << 24))
            __DRV_CLK_CPUCLK_Range_Select(Wizard_DCON0_P);
            //DRV_PageP_Read (DCON0_P);
            //IFD &= (HSE_P|HSE1_P);
            //IFD |= Wizard_DCON0_P;
            //DRV_PageP_Write(DCON0_P, IFD);
        #endif

    #endif  //(Wizard_SysClk_Select == SYS_12M_CPU_12M)

    //================================//
    // P60: ECKI or MCK output
    //================================//
    #if (DRV_CLK_Wizard_P60_Select==0x00)
        //Default disable P60
    #elif (Wizard_CKCON2_OSCS == (OSCin_ECKI_P60<<8))
        //P60=ECKI
    #else
        //Enable P60 Fast drive and ICKO output.
        AUXR0 |= (DRV_CLK_Wizard_P60_Select|P60FD);
    #endif

}
/// @cond __DRV_Wizard_Without_Doxygen
#endif  //MG82F6D17_CLK_WIZARD
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200527 //bugNum_Authour_Date
 * >> Add DMA initial void function for code size.
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 * #1.01_Kevin_20200901 //bugNum_Authour_Date
 * >> Cancel Timer5/Timer6 Clock & Gating Source Select initial function 
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing DMA by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
                DRV_DMA_Wizard_Init();
* @endcode
*******************************************************************************
*/

/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_DMA_WIZARD
/// @endcond
void DRV_DMA_Wizard_Init(void)
{
    #if DRV_DMA_Souece_Select
        __DRV_DMA_Source_Select(DRV_DMA_Souece_Select);
    #endif
    #if DRV_DMA_Destination_Select
        __DRV_DMA_Destination_Select(DRV_DMA_Destination_Select);
    #endif
    #if DRV_DMA_External_TriggerSource_Select
        __DRV_DMA_ExternalTriggerSource_Select(DRV_DMA_External_TriggerSource_Select);
    #endif
    #if DRV_DMA_LoopMode_En
        __DRV_DMA_LoopMode_Select(DRV_DMA_LoopMode_En);
    #endif
    #if DRV_DMA_CRC16_Write_En
        __DRV_DMA_CRC16_Write_Enable();
    #endif
    
    #if (DRV_DMA_Souece_Select == 0xF0) || (DRV_DMA_Destination_Select == 0x0F)
        __DRV_DMA_SetDataLength(DRV_DMA_XRAM_Data_Length);//XRAM Data Length by Timer5 Counter
    #endif

    #if (DRV_DMA_Souece_Select == 0xF0) || (DRV_DMA_Destination_Select == 0x0F)
        __DRV_DMA_SetXRAMAddressPointer(DRV_DMA_XRAM_Address_Pointer);//XRAM Address Pointer by Timer6 Counter
    #endif
    __DRV_DMA_Enable();
}

/// @cond __DRV_Wizard_Without_Doxygen
#endif  //MG82F6D17_DMA_WIZARD
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       WDT Wizard Initialization.
* @details     Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
               DRV_WDT_Wizard_Init();
* @endcode
*******************************************************************************
*/

/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_WDT_WIZARD   
/// @endcond
void DRV_WDT_Wizard_Init(void)
{
    #if (CKCON3_WDTCS != 0)
        __DRV_WDT_ClockSource_Select(CKCON3_WDTCS); 
    #endif
    #if (WDTCR_NSW != 0)   
        __DRV_WDT_NonStopped_Cmd(WDTCR_NSW);
    #endif
    #if (WDTCR_WIDL != 0) 
        __DRV_WDT_IdleControl_Cmd(WDTCR_WIDL);
    #endif
    #if (CKCON3_WDTFS != 0)
        __DRV_WDT_Overflow_Select(CKCON3_WDTFS);
    #endif
    #if (PreScaler != 7)
        __DRV_WDT_SetPrescaler(PreScaler);
    #endif
    #if (WDTCR_WREN != 0)
        __DRV_WDT_Reset_Cmd(WDTCR_WREN);
    #endif
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       RTC Wizard Initialization.
* @details     Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
               DRV_RTC_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_RTC_WIZARD
/// @endcond
void DRV_RTC_Wizard_Init(void)
{
    #if (CKCON4_RCSS != 0)
        __DRV_RTC_ClockSource_Select(CKCON4_RCSS);
    #endif
    #if (CKCON4_RTCCS != 1)
        __DRV_RTC_SetPrescaler(CKCON4_RTCCS);
    #endif
    #if (RTCCR_RTCRL != 0)
        __DRV_RTC_SetReloaderValue(RTCCR_RTCRL);
    #endif
    #if (RTCCR_RTCO != 0)
        __DRV_RTC_SetRTCKO(RTCCR_RTCO);
    #endif
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Brian_20200528 //bugNum_Authour_Date
 * >> 
Add system clock initial function for code size.
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
 *******************************************************************************
 * @brief       Initializing BODx by wizard option.
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *              DRV_PW_BODx_Wizard_Init();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_PW_WIZARD
/// @endcond
void DRV_PW_BODx_Wizard_Init(void)
{
    __DRV_BODx_SetBOD0RST_Cmd(PCON2_BO0RE);
    __DRV_BODx_SetBOD1RST_Cmd(PCON2_BO1RE);
    __DRV_BODx_BOD1_MonitoredLevelSelect(PCON2_BO1S);
    __DRV_BODx_BOD1_AwakeBOD1inPD(PCON2_AWBOD1);
    __DRV_BODx_BOD1_MonitorsVDD(PCON2_EBOD1);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Xian_20200605 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond urt0_wizard_option_#if
#if MG82F6D17_TIMER_WIZARD
/// @endcond

/**
*******************************************************************************
* @brief       TIMER Wizard Initial.
* @details     Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
               DRV_TIMER_Wizard_Init();
* @endcode
*******************************************************************************
*/
void DRV_TIMER_Wizard_Init(void)
{
    /*---------Timer0---------*/
    #if(TM0_Wizard_init_EN)
        /*---------Timer0 Mode0---------*/
        #if(TM0_Wizard_Mode==0x00 && TIMER0_Mode0_8Bit_PWM)
            __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);
            #if(TIMER0_Mode0_Clock_Source!=TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
                __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode0_Clock_Source);
            #endif
            #if(TIMER0_Mode0_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode0_Gating_Source);
            #endif
            #if(TIMER0_Mode0_Timer_Interval_High_Byte!=0)
                __DRV_TIMER0_SetHighByte(TIMER0_Mode0_Timer_Interval_High_Byte);
            #endif
            #if(TIMER0_Mode0_Timer_Interval_Low_Byte!=0)
                __DRV_TIMER0_SetLowByte(TIMER0_Mode0_Timer_Interval_Low_Byte);
            #endif
            #if(TIMER0_Mode0_Clock_Output_Enable!=0)
                __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode0_Clock_Output_Enable);
            #endif
            #if(TIMER0_Mode0_8Bit_Timer_Counter_Run!=0)
                __DRV_TIMER0_Run_Cmd(TIMER0_Mode0_8Bit_Timer_Counter_Run);
            #endif
        #endif
        /*---------Timer0 Mode1---------*/
        #if(TM0_Wizard_Mode==0x01 && TIMER0_Mode1_16Bit_Timer_Counter)
            __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);
            #if(TIMER0_Mode1_Clock_Source!=TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
                __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode1_Clock_Source);
            #endif
            #if(TIMER0_Mode1_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode1_Gating_Source);
            #endif
            #if(TIMER0_Mode1_Timer_Interval!=0)
                __DRV_TIMER0_Set16BitInterval(TIMER0_Mode1_Timer_Interval);
            #endif
            #if(TIMER0_Mode1_Clock_Output_Enable!=0)
                __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode1_Clock_Output_Enable);
            #endif
            #if(TIMER0_Mode0_16Bit_Timer_Counter_Run!=0)
                __DRV_TIMER0_Run_Cmd(TIMER0_Mode0_16Bit_Timer_Counter_Run);
            #endif
        #endif
        /*---------Timer0 Mode2---------*/
        #if(TM0_Wizard_Mode==0x02 && TIMER0_Mode2_8Bit_Timer_Counter_Auto_Reload)
            __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);
            #if(TIMER0_Mode2_Clock_Source!=TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
                __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode2_Clock_Source);
            #endif
            #if(TIMER0_Mode2_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode2_Gating_Source);
            #endif
            #if(TIMER0_Mode2_Timer_Interval!=0)
                __DRV_TIMER0_Set8BitIntervalAutoReload(TIMER0_Mode2_Timer_Interval);
            #endif
            #if(TIMER0_Mode2_Clock_Output_Enable!=0)
                __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode2_Clock_Output_Enable);
            #endif
            #if(TIMER0_Mode2_8Bit_Timer_Counter_Run!=0)
                __DRV_TIMER0_Run_Cmd(TIMER0_Mode2_8Bit_Timer_Counter_Run);
            #endif
        #endif
        /*---------Timer0 Mode3---------*/
        #if(TM0_Wizard_Mode==0x03 && TIMER0_Mode3_Two_Separate_Timer_Counter)
            __DRV_TIMER0_Mode_Select(TM0_Wizard_Mode);
            #if(TIMER0_Mode3_Clock_Source!=TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
                __DRV_TIMER0_Clock_Source_Select(TIMER0_Mode3_Clock_Source);
            #endif
            #if(TIMER0_Mode3_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                __DRV_TIMER0_Gating_Source_Select(TIMER0_Mode3_Gating_Source);
            #endif
            #if(TIMER0_Mode3_Timer_Interval_High_Byte!=0)
                __DRV_TIMER0_SetHighByte(TIMER0_Mode3_Timer_Interval_High_Byte);
            #endif
            #if(TIMER0_Mode3_Timer_Interval_Low_Byte!=0)
                __DRV_TIMER0_SetLowByte(TIMER0_Mode3_Timer_Interval_Low_Byte);
            #endif
            #if(TIMER0_Mode3_T0CKO_Clock_Output_Enable!=0)
                __DRV_TIMER0_T0CKO_Cmd(TIMER0_Mode3_T0CKO_Clock_Output_Enable);
            #endif
            #if(TIMER0_Mode3_8Bit_Timer_Counter_TH0_Run!=0)
                __DRV_TIMER1_Run_Cmd(TIMER0_Mode3_8Bit_Timer_Counter_TH0_Run);
            #endif
            #if(TIMER0_Mode3_8Bit_Timer_Counter_TL0_Run!=0)
                __DRV_TIMER0_Run_Cmd(TIMER0_Mode3_8Bit_Timer_Counter_TL0_Run);
            #endif
        #endif
        /*---------Timer1---------*/
        #if(TM1_Wizard_init_EN)
            /*---------Timer1 Mode0---------*/
            #if(TIMER1_Mode==0x00&&TIMER1_Mode0_8Bit_PWM)
                __DRV_TIMER1_Mode_Select(TIMER1_Mode);
                #if(TIMER1_Mode0_Clock_Source!=TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode0_Clock_Source);
                #endif
                #if(TIMER1_Mode0_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                    __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode0_Gating_Source);
                #endif
                #if(TIMER1_Mode0_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER1_SetHighByte(TIMER1_Mode0_Timer_Interval_High_Byte);
                #endif
                #if(TIMER1_Mode0_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER1_SetLowByte(TIMER1_Mode0_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER1_Mode0_Clock_Output_Enable!=0)
                    __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode0_Clock_Output_Enable);
                #endif
                #if(TIMER1_Mode0_8Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER1_Run_Cmd(TIMER1_Mode0_8Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer1 Mode1---------*/
            #if(TIMER1_Mode==0x01&&TIMER1_Mode1_16Bit_Timer_Counter)
                __DRV_TIMER1_Mode_Select(TIMER1_Mode);
                #if(TIMER1_Mode1_Clock_Source!=TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode1_Clock_Source);
                #endif
                #if(TIMER1_Mode1_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                    __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode1_Gating_Source);
                #endif
                #if(TIMER1_Mode1_Timer_Interval!=0)
                    __DRV_TIMER1_Set16BitInterval(TIMER1_Mode1_Timer_Interval);
                #endif
                #if(TIMER1_Mode1_Clock_Output_Enable!=0)
                    __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode1_Clock_Output_Enable);
                #endif
                #if(TIMER1_Mode1_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER1_Run_Cmd(TIMER1_Mode1_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer1 Mode2---------*/
            #if(TIMER1_Mode==0x02&&TIMER1_Mode2_8Bit_Timer_Counter_Auto_Reload)
                __DRV_TIMER1_Mode_Select(TIMER1_Mode);
                #if(TIMER1_Mode2_Clock_Source!=TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER1_Clock_Source_Select(TIMER1_Mode2_Clock_Source);
                #endif
                #if(TIMER1_Mode2_Gating_Source!=TIMER0_GATING_SOURCE_DISABLE)
                    __DRV_TIMER1_Gating_Source_Select(TIMER1_Mode2_Gating_Source);
                #endif
                #if(TIMER1_Mode2_Timer_Interval!=0)
                    __DRV_TIMER1_Set8BitIntervalAutoReload(TIMER1_Mode2_Timer_Interval);
                #endif
                #if(TIMER1_Mode2_Clock_Output_Enable!=0)
                    __DRV_TIMER1_T1CKO_Cmd(TIMER1_Mode2_Clock_Output_Enable);
                #endif
                #if(TIMER1_Mode2_8Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER1_Run_Cmd(TIMER1_Mode2_8Bit_Timer_Counter_Run);
                #endif
            #endif
        #endif
        /*---------Timer2---------*/
        #if(TM2_Wizard_init_EN)
            /*---------Timer2 Mode0---------*/
            #if(TIMER2_Mode==0x00&&TIMER2_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Mode0_Clock_Source!=TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode0_Clock_Source);
                #endif
                #if(TIMER2_Mode0_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode0_Capture_Source);
                #endif
                #if(TIMER2_Mode0_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode0_Clock_Output_Enable);
                #endif
                #if(TIMER2_Mode0_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode0_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Mode0_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode0_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Mode0_Timer_Interval!=0)
                    __DRV_TIMER2_Set16BitInterval(TIMER2_Mode0_Timer_Interval);
                #endif
                #if(TIMER2_Mode0_Timer_Interval_Reload!=0)
                    __DRV_TIMER2_Set16BCaptureitInterval(TIMER2_Mode0_Timer_Interval_Reload);
                #endif
                #if(TIMER2_Mode0_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Mode0_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer2 Mode1---------*/
            #if(TIMER2_Mode==0x01&&TIMER2_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Mode1_Clock_Source!=TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode1_Clock_Source);
                #endif
                #if(TIMER2_Mode1_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode1_Capture_Source);
                #endif
                #if(TIMER2_Mode1_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode1_Clock_Output_Enable);
                #endif
                #if(TIMER2_Mode1_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode1_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Mode1_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode1_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Mode1_Timer_Interval!=0)
                    __DRV_TIMER2_Set16BitInterval(TIMER2_Mode1_Timer_Interval);
                #endif
                #if(TIMER2_Mode1_Timer_Interval_Reload!=0)
                    __DRV_TIMER2_Set16BCaptureitInterval(TIMER2_Mode1_Timer_Interval_Reload);
                #endif
                #if(TIMER2_Mode1_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Mode1_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer2 Mode2---------*/
            #if(TIMER2_Mode==0x02&&TIMER2_Mode2_16Bit_Timer_Counter_Capture)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Mode2_Clock_Source!=TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode2_Clock_Source);
                #endif
                #if(TIMER2_Mode2_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode2_Capture_Source);
                #endif
                #if(TIMER2_Mode2_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode2_Clock_Output_Enable);
                #endif
                #if(TIMER2_Mode2_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode2_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Mode2_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode2_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Mode2_Timer_Interval!=0)
                    __DRV_TIMER2_Set16BitInterval(TIMER2_Mode2_Timer_Interval);
                #endif
                #if(TIMER2_Mode2_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Mode2_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer2 Mode3---------*/
            #if(TIMER2_Mode==0x03&&TIMER2_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Mode3_Clock_Source!=TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_Clock_Source_Select(TIMER2_Mode3_Clock_Source);
                #endif
                #if(TIMER2_Mode3_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Mode3_Capture_Source);
                #endif
                #if(TIMER2_Mode3_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Mode3_Clock_Output_Enable);
                #endif
                #if(TIMER2_Mode3_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Mode3_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Mode3_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Mode3_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Mode3_Timer_Interval!=0)
                    __DRV_TIMER2_Set16BitInterval(TIMER2_Mode3_Timer_Interval);
                #endif
                #if(TIMER2_Mode3_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Mode3_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer2 Split Mode0---------*/
            #if(TIMER2_Mode==0x04&&TIMER2_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Split_Mode0_TH2_Clock_Source!=TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode0_TH2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode0_TL2_Clock_Source!=TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode0_TL2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode0_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode0_Capture_Source);
                #endif
                #if(TIMER2_Split_Mode0_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode0_Clock_Output_Enable);
                #endif
                #if(TIMER2_Split_Mode0_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode0_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Split_Mode0_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode0_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Split_Mode0_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode0_Timer_Interval_High_Byte);
                #endif
                #if(TIMER2_Split_Mode0_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode0_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode0_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode0_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER2_Split_Mode0_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode0_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode0_8Bit_Timer_Counter_TL2_Run!=0)
                    __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode0_8Bit_Timer_Counter_TL2_Run);
                #endif
                #if(TIMER2_Split_Mode0_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode0_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER2_Split_Mode0_TL2_Stop!=0)
                    __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode0_TL2_Stop);
                #endif
            #endif
            /*---------Timer2 Split Mode1---------*/
            #if(TIMER2_Mode==0x05&&TIMER2_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Split_Mode1_TH2_Clock_Source!=TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode1_TH2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode1_TL2_Clock_Source!=TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode1_TL2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode1_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode1_Capture_Source);
                #endif
                #if(TIMER2_Split_Mode1_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode1_Clock_Output_Enable);
                #endif
                #if(TIMER2_Split_Mode1_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode1_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Split_Mode1_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode1_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Split_Mode1_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode1_Timer_Interval_High_Byte);
                #endif
                #if(TIMER2_Split_Mode1_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode1_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode1_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode1_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER2_Split_Mode1_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode1_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode1_8Bit_Timer_Counter_TL2_Run!=0)
                    __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode1_8Bit_Timer_Counter_TL2_Run);
                #endif
                #if(TIMER2_Split_Mode1_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode1_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER2_Split_Mode1_TL2_Stop!=0)
                    __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode1_TL2_Stop);
                #endif
            #endif
            /*---------Timer2 Split Mode2---------*/
            #if(TIMER2_Mode==0x06&&TIMER2_Split_Mode2_Two_8Bit_Timer_Counter_Capture)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Split_Mode2_TH2_Clock_Source!=TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode2_TH2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode2_TL2_Clock_Source!=TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode2_TL2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode2_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode2_Capture_Source);
                #endif
                #if(TIMER2_Split_Mode2_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode2_Clock_Output_Enable);
                #endif
                #if(TIMER2_Split_Mode2_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode2_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Split_Mode2_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode2_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Split_Mode2_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode2_Timer_Interval_High_Byte);
                #endif
                #if(TIMER2_Split_Mode2_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode2_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER2_Split_Mode2_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode2_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode2_8Bit_Timer_Counter_TL2_Run!=0)
                    __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode2_8Bit_Timer_Counter_TL2_Run);
                #endif
                #if(TIMER2_Split_Mode2_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode2_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER2_Split_Mode2_TL2_Stop!=0)
                    __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode2_TL2_Stop);
                #endif
            #endif
            /*---------Timer2 Split Mode3---------*/
            #if(TIMER2_Mode==0x07&&TIMER2_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Split_Mode3_TH2_Clock_Source!=TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_Split_Mode3_TH2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode3_TL2_Clock_Source!=TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_Split_Mode3_TL2_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode3_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode3_Capture_Source);
                #endif
                #if(TIMER2_Split_Mode3_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode3_Clock_Output_Enable);
                #endif
                #if(TIMER2_Split_Mode3_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode3_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Split_Mode3_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode3_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Split_Mode3_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode3_Timer_Interval_High_Byte);
                #endif
                #if(TIMER2_Split_Mode3_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode3_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER2_Split_Mode3_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode3_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode3_8Bit_Timer_Counter_TL2_Run!=0)
                    __DRV_TIMER2_TL2_Run_Cmd(TIMER2_Split_Mode3_8Bit_Timer_Counter_TL2_Run);
                #endif
                #if(TIMER2_Split_Mode3_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode3_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER2_Split_Mode3_TL2_Stop!=0)
                    __DRV_TIMER2_TR2L_Clear_Cmd(TIMER2_Split_Mode3_TL2_Stop);
                #endif
            #endif
            /*---------Timer2 Split Mode4---------*/
            #if(TIMER2_Mode==0x08&&TIMER2_Split_Mode4_8Bit_Pwm)
                __DRV_TIMER2_Mode_Select(TIMER2_Mode);
                #if(TIMER2_Split_Mode4_Clock_Source!=TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER2_Clock_Source_Select(TIMER2_Split_Mode4_Clock_Source);
                #endif
                #if(TIMER2_Split_Mode4_Capture_Source!=TIMER2_CAPTURE_SOURCE_T2EX_PIN)
                    __DRV_TIMER2_Capture_Source_Select(TIMER2_Split_Mode4_Capture_Source);
                #endif
                #if(TIMER2_Split_Mode4_Clock_Output_Enable!=0)
                    __DRV_TIMER2_Clock_Out_Cmd(TIMER2_Split_Mode4_Clock_Output_Enable);
                #endif
                #if(TIMER2_Split_Mode4_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(TIMER2_Split_Mode4_Overflow_Flag_Ignored);
                #endif
                #if(TIMER2_Split_Mode4_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER2_Capture_Source_Dectect(TIMER2_Split_Mode4_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER2_Split_Mode4_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER2_SetHighByte(TIMER2_Split_Mode4_Timer_Interval_High_Byte);
                #endif
                #if(TIMER2_Split_Mode4_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2H(TIMER2_Split_Mode4_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode4_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER2_SetLowByte(TIMER2_Split_Mode4_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER2_Split_Mode4_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER2_SetRCAP2L(TIMER2_Split_Mode4_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER2_Split_Mode4_T2CKO_Invert!=0)
                    __DRV_TIMER2_T2CKO_Invert_Cmd(TIMER2_Split_Mode4_T2CKO_Invert);
                #endif
                #if(TIMER2_Split_Mode4_8Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER2_Run_Cmd(TIMER2_Split_Mode4_8Bit_Timer_Counter_Run);
                #endif
            #endif
        #endif
        /*---------Timer3---------*/
        #if(TM3_Wizard_init_EN)
            /*---------Timer3 Mode0---------*/
            #if(TIMER3_Mode==0x00&&TIMER3_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Mode0_Clock_Source!=TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode0_Clock_Source);
                #endif
                #if(TIMER3_Mode0_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode0_Capture_Source);
                #endif
                #if(TIMER3_Mode0_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode0_Clock_Output_Enable);
                #endif
                #if(TIMER3_Mode0_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode0_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Mode0_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode0_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Mode0_Timer_Interval!=0)
                    __DRV_TIMER3_Set16BitInterval(TIMER3_Mode0_Timer_Interval);
                #endif
                #if(TIMER3_Mode0_Timer_Interval_Reload!=0)
                    __DRV_TIMER3_Set16BCaptureitInterval(TIMER3_Mode0_Timer_Interval_Reload);
                #endif
                #if(TIMER3_Mode0_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Mode0_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer3 Mode1---------*/
            #if(TIMER3_Mode==0x01&&TIMER3_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Mode1_Clock_Source!=TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode1_Clock_Source);
                #endif
                #if(TIMER3_Mode1_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode1_Capture_Source);
                #endif
                #if(TIMER3_Mode1_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode1_Clock_Output_Enable);
                #endif
                #if(TIMER3_Mode1_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode1_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Mode1_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode1_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Mode1_Timer_Interval!=0)
                    __DRV_TIMER3_Set16BitInterval(TIMER3_Mode1_Timer_Interval);
                #endif
                #if(TIMER3_Mode1_Timer_Interval_Reload!=0)
                    __DRV_TIMER3_Set16BCaptureitInterval(TIMER3_Mode1_Timer_Interval_Reload);
                #endif
                #if(TIMER3_Mode1_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Mode1_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer3 Mode2---------*/
            #if(TIMER3_Mode==0x02&&TIMER3_Mode2_16Bit_Timer_Counter_Capture)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Mode2_Clock_Source!=TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode2_Clock_Source);
                #endif
                #if(TIMER3_Mode2_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode2_Capture_Source);
                #endif
                #if(TIMER3_Mode2_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode2_Clock_Output_Enable);
                #endif
                #if(TIMER3_Mode2_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode2_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Mode2_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode2_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Mode2_Timer_Interval!=0)
                    __DRV_TIMER3_Set16BitInterval(TIMER3_Mode2_Timer_Interval);
                #endif
                #if(TIMER3_Mode2_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Mode2_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer3 Mode3---------*/
            #if(TIMER3_Mode==0x03&&TIMER3_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Mode3_Clock_Source!=TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_Clock_Source_Select(TIMER3_Mode3_Clock_Source);
                #endif
                #if(TIMER3_Mode3_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Mode3_Capture_Source);
                #endif
                #if(TIMER3_Mode3_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Mode3_Clock_Output_Enable);
                #endif
                #if(TIMER3_Mode3_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Mode3_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Mode3_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Mode3_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Mode3_Timer_Interval!=0)
                    __DRV_TIMER3_Set16BitInterval(TIMER3_Mode3_Timer_Interval);
                #endif
                #if(TIMER3_Mode3_16Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Mode3_16Bit_Timer_Counter_Run);
                #endif
            #endif
            /*---------Timer3 Split Mode0---------*/
            #if(TIMER3_Mode==0x04&&TIMER3_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Split_Mode0_TH3_Clock_Source!=TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode0_TH3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode0_TL3_Clock_Source!=TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode0_TL3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode0_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode0_Capture_Source);
                #endif
                #if(TIMER3_Split_Mode0_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode0_Clock_Output_Enable);
                #endif
                #if(TIMER3_Split_Mode0_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode0_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Split_Mode0_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode0_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Split_Mode0_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode0_Timer_Interval_High_Byte);
                #endif
                #if(TIMER3_Split_Mode0_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode0_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode0_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode0_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER3_Split_Mode0_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode0_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode0_8Bit_Timer_Counter_TL3_Run!=0)
                    __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode0_8Bit_Timer_Counter_TL3_Run);
                #endif
                #if(TIMER3_Split_Mode0_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode0_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER3_Split_Mode0_TL3_Stop!=0)
                    __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode0_TL3_Stop);
                #endif
            #endif
            /*---------Timer3 Split Mode1---------*/
            #if(TIMER3_Mode==0x05&&TIMER3_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Split_Mode1_TH3_Clock_Source!=TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode1_TH3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode1_TL3_Clock_Source!=TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode1_TL3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode1_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode1_Capture_Source);
                #endif
                #if(TIMER3_Split_Mode1_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode1_Clock_Output_Enable);
                #endif
                #if(TIMER3_Split_Mode1_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode1_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Split_Mode1_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode1_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Split_Mode1_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode1_Timer_Interval_High_Byte);
                #endif
                #if(TIMER3_Split_Mode1_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode1_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode1_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode1_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER3_Split_Mode1_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode1_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode1_8Bit_Timer_Counter_TL3_Run!=0)
                    __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode1_8Bit_Timer_Counter_TL3_Run);
                #endif
                #if(TIMER3_Split_Mode1_8Bit_Timer_Counter_TH3_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode1_8Bit_Timer_Counter_TH3_Run);
                #endif
                #if(TIMER3_Split_Mode1_TL3_Stop!=0)
                    __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode1_TL3_Stop);
                #endif
            #endif
            /*---------Timer3 Split Mode2---------*/
            #if(TIMER3_Mode==0x06&&TIMER3_Split_Mode2_Two_8Bit_Timer_Counter_Capture)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Split_Mode2_TH3_Clock_Source!=TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode2_TH3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode2_TL3_Clock_Source!=TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode2_TL3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode2_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode2_Capture_Source);
                #endif
                #if(TIMER3_Split_Mode2_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode2_Clock_Output_Enable);
                #endif
                #if(TIMER3_Split_Mode2_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode2_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Split_Mode2_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode2_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Split_Mode2_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode2_Timer_Interval_High_Byte);
                #endif
                #if(TIMER3_Split_Mode2_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode2_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER3_Split_Mode2_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode2_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode2_8Bit_Timer_Counter_TL3_Run!=0)
                    __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode2_8Bit_Timer_Counter_TL3_Run);
                #endif
                #if(TIMER3_Split_Mode2_8Bit_Timer_Counter_TH3_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode2_8Bit_Timer_Counter_TH3_Run);
                #endif
                #if(TIMER3_Split_Mode2_TL3_Stop!=0)
                    __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode2_TL3_Stop);
                #endif
            #endif
            /*---------Timer3 Split Mode3---------*/
            #if(TIMER3_Mode==0x07&&TIMER3_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Split_Mode3_TH3_Clock_Source!=TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_Split_Mode3_TH3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode3_TL3_Clock_Source!=TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_Split_Mode3_TL3_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode3_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode3_Capture_Source);
                #endif
                #if(TIMER3_Split_Mode3_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode3_Clock_Output_Enable);
                #endif
                #if(TIMER3_Split_Mode3_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode3_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Split_Mode3_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode3_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Split_Mode3_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode3_Timer_Interval_High_Byte);
                #endif
                #if(TIMER3_Split_Mode3_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode3_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER3_Split_Mode3_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode3_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode3_8Bit_Timer_Counter_TL2_Run!=0)
                    __DRV_TIMER3_TL3_Run_Cmd(TIMER3_Split_Mode3_8Bit_Timer_Counter_TL2_Run);
                #endif
                #if(TIMER3_Split_Mode3_8Bit_Timer_Counter_TH2_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode3_8Bit_Timer_Counter_TH2_Run);
                #endif
                #if(TIMER3_Split_Mode3_TL3_Stop!=0)
                    __DRV_TIMER3_TR3L_Clear_Cmd(TIMER3_Split_Mode3_TL3_Stop);
                #endif
            #endif
            /*---------Timer3 Split Mode4---------*/
            #if(TIMER3_Mode==0x08&&TIMER3_Split_Mode4_8Bit_Pwm)
                __DRV_TIMER3_Mode_Select(TIMER3_Mode);
                #if(TIMER3_Split_Mode4_Clock_Source!=TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
                    __DRV_TIMER3_Clock_Source_Select(TIMER3_Split_Mode4_Clock_Source);
                #endif
                #if(TIMER3_Split_Mode4_Capture_Source!=TIMER3_CAPTURE_SOURCE_T3EX_PIN)
                    __DRV_TIMER3_Capture_Source_Select(TIMER3_Split_Mode4_Capture_Source);
                #endif
                #if(TIMER3_Split_Mode4_Clock_Output_Enable!=0)
                    __DRV_TIMER3_Clock_Out_Cmd(TIMER3_Split_Mode4_Clock_Output_Enable);
                #endif
                #if(TIMER3_Split_Mode4_Overflow_Flag_Ignored!=0)
                    __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(TIMER3_Split_Mode4_Overflow_Flag_Ignored);
                #endif
                #if(TIMER3_Split_Mode4_Capture_Source_Dectect_Edge!=IGNORED)
                    __DRV_TIMER3_Capture_Source_Dectect(TIMER3_Split_Mode4_Capture_Source_Dectect_Edge);
                #endif
                #if(TIMER3_Split_Mode4_Timer_Interval_High_Byte!=0)
                    __DRV_TIMER3_SetHighByte(TIMER3_Split_Mode4_Timer_Interval_High_Byte);
                #endif
                #if(TIMER3_Split_Mode4_Timer_Interval_High_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3H(TIMER3_Split_Mode4_Timer_Interval_High_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode4_Timer_Interval_Low_Byte!=0)
                    __DRV_TIMER3_SetLowByte(TIMER3_Split_Mode4_Timer_Interval_Low_Byte);
                #endif
                #if(TIMER3_Split_Mode4_Timer_Interval_Low_Byte_Reload!=0)
                    __DRV_TIMER3_SetRCAP3L(TIMER3_Split_Mode4_Timer_Interval_Low_Byte_Reload);
                #endif
                #if(TIMER3_Split_Mode4_T3CKO_Invert!=0)
                    __DRV_TIMER3_T3CKO_Invert_Cmd(TIMER3_Split_Mode4_T3CKO_Invert);
                #endif
                #if(TIMER3_Split_Mode4_8Bit_Timer_Counter_Run!=0)
                    __DRV_TIMER3_Run_Cmd(TIMER3_Split_Mode4_8Bit_Timer_Counter_Run);
                #endif
            #endif
        #endif
    #endif
}

/// @cond urt0_wizard_option_#endif
#endif
/// @endcond

//@del{
void TIMER_test(void)
{
//    #if(TM0_Wizard_init_EN)
//        #if(TM0_Wizard_Mode==0x00)
//            #if(TIMER0_Mode0_8Bit_PWM)
//                    if(DRV_TIMER0_GetTF0())
//                        {
//                            DRV_TIMER0_ClearTF0();

//                        }

//            #endif
//        #elif(TM0_Wizard_Mode==0x01)
//            #if(TIMER0_Mode1_16Bit_Timer_Counter)

//                    if(DRV_TIMER0_GetTF0())
//                        {
//                            DRV_TIMER0_ClearTF0();
//                            //__DRV_TIMER0_Set16BitInterval(TIMER0_Mode1_Timer_Interval);
//                        }

//            #endif
//        #elif(TM0_Wizard_Mode==0x02)
//            #if(TIMER0_Mode2_8Bit_Timer_Counter_Auto_Reload)

//                if(DRV_TIMER0_GetTF0())
//                        {
//                             DRV_TIMER0_ClearTF0();

//                        }

//            #endif
//        #elif(TM0_Wizard_Mode==0x03)
//            #if(TIMER0_Mode3_Two_Separate_Timer_Counter)

//                if(DRV_TIMER0_GetTF0())
//                        {
//                            DRV_TIMER0_ClearTF0();
//                            P30=~P30;
//                        }
//                if(DRV_TIMER1_GetTF1())
//                        {
//                            DRV_TIMER1_ClearTF1();
//                            P31=~P31;
//                        }

//            #endif
//        #endif
//    #elif(TM1_Wizard_init_EN)
//    #if(TIMER1_Mode==0x00)
//            #if(TIMER1_Mode0_8Bit_PWM)

//                      if(DRV_TIMER1_GetTF1())
//                        {
//                            DRV_TIMER1_ClearTF1();

//                        }

//            #endif
//        #elif(TIMER1_Mode==0x01)
//            #if(TIMER1_Mode1_16Bit_Timer_Counter)
//                      if(DRV_TIMER1_GetTF1())
//                        {
//                            DRV_TIMER1_ClearTF1();

//                        }

//            #endif
//        #elif(TIMER1_Mode==0x02)
//            #if(TIMER1_Mode2_8Bit_Timer_Counter_Auto_Reload)

//                      if(DRV_TIMER1_GetTF1())
//                        {
//                            DRV_TIMER1_ClearTF1();

//                        }

//            #endif

//        #endif
//    #endif
//    #if(TM2_Wizard_init_EN)
//        #if(TIMER2_Mode==0x00)
//            #if(TIMER2_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER2_Mode==0x01)
//           #if(TIMER2_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER2_Mode==0x02)
//            #if(TIMER2_Mode2_16Bit_Timer_Counter_Capture)

//            #endif
//        #elif(TIMER2_Mode==0x03)
//            #if(TIMER2_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero)

//            #endif
//        #elif(TIMER2_Mode==0x04)
//            #if(TIMER2_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER2_Mode==0x05)
//           #if(TIMER2_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt)

//            #endif
//        #elif(TIMER2_Mode==0x06)
//            #if(TIMER2_Split_Mode2_Two_8Bit_Timer_Counter_Capture)

//            #endif
//        #elif(TIMER2_Mode==0x07)
//            #if(TIMER2_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero)

//            #endif
//       #elif(TIMER2_Mode==0x08)
//            #if(TIMER2_Split_Mode4_8Bit_Pwm)

//            #endif
//        #endif
//    #endif
//    #if(TM3_Wizard_init_EN)
//        #if(TIMER3_Mode==0x00)
//            #if(TIMER3_Mode0_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER3_Mode==0x01)
//           #if(TIMER3_Mode1_16bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER3_Mode==0x02)
//            #if(TIMER3_Mode2_16Bit_Timer_Counter_Capture)

//            #endif
//        #elif(TIMER3_Mode==0x03)
//            #if(TIMER3_Mode3_16Bit_Timer_Counter_Capture_With_Auto_Zero)

//            #endif
//        #elif(TIMER3_Mode==0x04)
//            #if(TIMER3_Split_Mode0_Two_8Bit_Timer_Counter_Auto_Reload_and_External_Interrupt)

//            #endif
//        #elif(TIMER3_Mode==0x05)
//           #if(TIMER3_Split_Mode1_Two_8Bit_Timer_Counter_Auto_Reload_With_External_Interrupt)

//            #endif
//        #elif(TIMER3_Mode==0x06)
//            #if(TIMER3_Split_Mode2_Two_8Bit_Timer_Counter_Capture)

//            #endif
//        #elif(TIMER3_Mode==0x07)
//            #if(TIMER3_Split_Mode3_Two_8Bit_Timer_Counter_Capture_with_auto_zero)

//            #endif
//       #elif(TIMER3_Mode==0x08)
//            #if(TIMER3_Split_Mode4_8Bit_Pwm)

//            #endif
//        #endif
//    #endif
}
//@del}

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Allen_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond PCA_wizard_option_#if
#if MG82F6D17_PCA_WIZARD
/// @endcond

#if PCA_MODE==1
void DRV_PCA_OC_Wizard_Init() 
{
        _push_(SFRPI);
        __DRV_SFR_PageIndex(0);
        __DRV_PCA_Disable();
        __DRV_PCA_SetCounterValue(65536-(OC_PERIOD));
        __DRV_PCA_SetCounterReloadValue(65536-(OC_PERIOD));
        __DRV_PCA_ClockSource_Select(OC_CKSRC);
        CMOD &=0x8F;
        CMOD |= OC_BUFFER & 0x70;
        __DRV_SFR_PageIndex(1);
        ((OC_BUFFER&(PCA_PWM67_BUFFER))==(0x00)?(CCAPM6 &= ~BME6):(CCAPM6 |= BME6));
        __DRV_SFR_PageIndex(0);
        #if(OC0_EN)
          __DRV_PCA_PWM0_SetInverse(OC0_INV);
          __DRV_PCA_PWM0Resolution_Select(OC0_RES);
          CCAPM0 = (ECOM0 | TOG0 | PWM0);
          OC0_RES==PCA_PWM_8BIT?(CCAP0H = LOBYTE(OC0_COPM)):(CCAP0H = HIBYTE(OC0_COPM));
          CCAP0L = LOBYTE(OC0_COPM);
        #endif    
        #if(OC1_EN)
          __DRV_PCA_PWM1_SetInverse(OC1_INV);
          __DRV_PCA_PWM1Resolution_Select(OC1_RES);
          CCAPM1 = (ECOM1 | TOG1 | PWM1);
          OC1_RES==PCA_PWM_8BIT?(CCAP1H = LOBYTE(OC1_COPM)):(CCAP1H = HIBYTE(OC1_COPM));
          CCAP1L = LOBYTE(OC1_COPM);
        #endif
        #if(OC2_EN)
          __DRV_PCA_PWM2_SetInverse(OC2_INV);
          __DRV_PCA_PWM2Resolution_Select(OC2_RES);
          CCAPM2 = (ECOM2 | TOG2 | PWM2);
          OC2_RES==PCA_PWM_8BIT?(CCAP2H = LOBYTE(OC2_COPM)):(CCAP2H = HIBYTE(OC2_COPM));
          CCAP2L = LOBYTE(OC2_COPM);
        #endif
        #if(OC3_EN)
          __DRV_PCA_PWM3_SetInverse(OC3_INV);
          __DRV_PCA_PWM3Resolution_Select(OC3_RES);
          CCAPM3 = (ECOM3 | TOG3 | PWM3);
          OC3_RES==PCA_PWM_8BIT?(CCAP3H = LOBYTE(OC3_COPM)):(CCAP3H = HIBYTE(OC3_COPM));
          CCAP3L = LOBYTE(OC3_COPM);
        #endif
        #if(OC4_EN)
          __DRV_PCA_PWM4_SetInverse(OC4_INV);
          __DRV_PCA_PWM4Resolution_Select(OC4_RES);
          CCAPM4 = (ECOM4 | TOG4 | PWM4);
          OC4_RES==PCA_PWM_8BIT?(CCAP4H = LOBYTE(OC4_COPM)):(CCAP4H = HIBYTE(OC4_COPM));
          CCAP4L = LOBYTE(OC4_COPM);
        #endif
        #if(OC5_EN)
          __DRV_PCA_PWM5_SetInverse(OC5_INV);
          __DRV_PCA_PWM5Resolution_Select(OC5_RES);
          CCAPM5 = (ECOM5 | TOG5 | PWM5);
          OC5_RES==PCA_PWM_8BIT?(CCAP5H = LOBYTE(OC5_COPM)):(CCAP5H = HIBYTE(OC5_COPM));
          CCAP5L = LOBYTE(OC5_COPM);
        #endif
        #if(OC6_EN)
          __DRV_PCA_PWM6_SetInverse(OC6_INV);
          __DRV_SFR_PageIndex(1);
          PCAPWM6 &= ~(P0RS1 | P0RS0);
          PCAPWM6 |= OC6_RES;
          CCAPM6 |= (ECOM6 | TOG6 | PWM6);
          OC6_RES==PCA_PWM_8BIT?(CCAP6H = LOBYTE(OC6_COPM)):(CCAP6H = HIBYTE(OC6_COPM));
          CCAP6L = LOBYTE(OC6_COPM);
        #endif
        #if(OC7_EN)
          __DRV_PCA_PWM7_SetInverse(OC7_INV);
          __DRV_SFR_PageIndex(1);
          PCAPWM7 &= ~(P0RS1 | P0RS0);
          PCAPWM7 |= OC7_RES;
          CCAPM7 = (ECOM7 | TOG7 | PWM7);
          OC7_RES==PCA_PWM_8BIT?(CCAP7H = LOBYTE(OC7_COPM)):(CCAP7H = HIBYTE(OC7_COPM));
          CCAP7L = LOBYTE(OC7_COPM);
        #endif
        _pop_(SFRPI);
        
}
#endif

#if PCA_MODE==2
void DRV_PCA_PWM_Wizard_Init() 
{
        _push_(SFRPI);
        __DRV_SFR_PageIndex(0);
        __DRV_PCA_Disable();
      #if(PWM_ALIGN)
        __DRV_SFR_PageIndex(8);
        AUXR11 |= C0M0;
      #endif
        __DRV_SFR_PageIndex(1);
        ((PWM_BUFFER&(PCA_PWM67_BUFFER))==(0x00)?(CCAPM6 &= ~BME6):(CCAPM6 |= BME6));
        __DRV_SFR_PageIndex(0);
        CMOD &=0x8F;
        CMOD |= PWM_BUFFER & 0x70;
        __DRV_PCA_SetCounterValue(65536-PWM_CKS_FREQ/PWM_FREQ);
        __DRV_PCA_SetCounterReloadValue(65536-PWM_CKS_FREQ/PWM_FREQ);
        __DRV_PCA_ClockSource_Select(PWM_CKSRC);
        __DRV_PCA_PWM_CentralAligned_Cmd(PWM_ALIGN);
      #if(PWM0_EN)
        __DRV_PCA_PWM0_SetInverse(PWM0_INV);
        CCAPM0 = (ECOM0 | PWM0);
        PCAPWM0 |= (P1RS1 | P1RS0);
        __DRV_PCA_SetPWM0DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM0_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM1_EN)
        __DRV_PCA_PWM1_SetInverse(PWM1_INV);
        CCAPM1 = (ECOM1 | PWM1);
        PCAPWM1 |= (P1RS1 | P1RS0);
        __DRV_PCA_SetPWM1DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM1_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM2_EN)
        __DRV_PCA_PWM2_SetInverse(PWM2_INV);
        CCAPM2 = (ECOM2 | PWM2);
        PCAPWM2 |= (P2RS1 | P2RS0);
        __DRV_PCA_SetPWM2DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM2_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM3_EN)
        __DRV_PCA_PWM3_SetInverse(PWM3_INV);
        CCAPM3 = (ECOM3 | PWM3);
        PCAPWM3 |= (P3RS1 | P3RS0);
        __DRV_PCA_SetPWM3DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM3_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM4_EN)
        __DRV_PCA_PWM4_SetInverse(PWM4_INV);
        CCAPM4 = (ECOM4 | PWM4);
        PCAPWM4 |= (P4RS1 | P4RS0);
        __DRV_PCA_SetPWM4DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM4_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM5_EN)
        __DRV_PCA_PWM5_SetInverse(PWM5_INV);
        CCAPM5 = (ECOM5 | PWM5);
        PCAPWM5 |= (P5RS1 | P5RS0);
        __DRV_PCA_SetPWM5DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM5_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM6_EN)
        __DRV_PCA_PWM6_SetInverse(PWM6_INV);
        __DRV_SFR_PageIndex(1);
        CCAPM6 = (ECOM6 | PWM6);
        PCAPWM6 |= (P6RS1 | P6RS0);
        __DRV_PCA_SetPWM6DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM6_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      #if(PWM7_EN)
        __DRV_PCA_PWM7_SetInverse(PWM7_INV);
        __DRV_SFR_PageIndex(1);
        CCAPM7 = (ECOM7 | PWM7);
        PCAPWM7 |= (P7RS1 | P7RS0);
        __DRV_PCA_SetPWM7DutyValue((PWM_CKS_FREQ/PWM_FREQ*(100-PWM7_DUTY_PERC)/100)+(65536-PWM_CKS_FREQ/PWM_FREQ));
      #endif
      _pop_(SFRPI);
}
#endif


#if PCA_MODE==3
void DRV_PCA_PWM_DTG_Wizard_Init() 
{
        _push_(SFRPI);
        __DRV_SFR_PageIndex(0);
        __DRV_PCA_Disable();
        __DRV_PCA_SetCounterValue(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ);
        __DRV_PCA_SetCounterReloadValue(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ);
        __DRV_PCA_ClockSource_Select(PWM_DTG_CKSRC);
        __DRV_SFR_PageIndex(1);
        PDTCRA = PWM_DTG_DTCKSRC | (PWM_DTG_DTPERIOD & 0x3F);
        __DRV_SFR_PageIndex(0);
      #if(PWM01_DTG_EN)
        __DRV_PCA_PWM0_SetInverse(PWM0_DTG_INV);
        __DRV_PCA_PWM1_SetInverse(PWM1_DTG_INV);
        PCAPWM0 |= (P0RS1 | P0RS0);
        PCAPWM1 |= (P0RS1 | P0RS0);
        CMOD |= BME0;
        CCAPM0 = (ECOM0  | PWM0 | DTE0);
        CCAPM1 = (ECOM1  | PWM1 );
        __DRV_PCA_SetPWM0DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM01_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
        __DRV_PCA_SetPWM1DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM01_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
      #endif  
      #if(PWM23_DTG_EN)
        __DRV_PCA_PWM2_SetInverse(PWM2_DTG_INV);
        __DRV_PCA_PWM3_SetInverse(PWM3_DTG_INV);
        PCAPWM2 |= (P0RS1 | P0RS0);
        PCAPWM3 |= (P0RS1 | P0RS0);
        CMOD |= BME2;
        CCAPM2 = (ECOM2  | PWM2 | DTE2);
        CCAPM3 = (ECOM3  | PWM3 );
        __DRV_PCA_SetPWM2DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM23_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
        __DRV_PCA_SetPWM3DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM23_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
      #endif
      #if(PWM45_DTG_EN)
        __DRV_PCA_PWM4_SetInverse(PWM4_DTG_INV);
        __DRV_PCA_PWM5_SetInverse(PWM5_DTG_INV);
        PCAPWM4 |= (P0RS1 | P0RS0);
        PCAPWM5 |= (P0RS1 | P0RS0);
        CMOD |= BME4;
        CCAPM4 = (ECOM4  | PWM4 | DTE4);
        CCAPM5 = (ECOM5  | PWM5 );
        __DRV_PCA_SetPWM4DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM45_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
        __DRV_PCA_SetPWM5DutyValue((PWM_DTG_CKS_FREQ/PWM_DTG_FREQ*(100-PWM45_DUTY_PERC)/100)+(65536-PWM_DTG_CKS_FREQ/PWM_DTG_FREQ));
      #endif
      _pop_(SFRPI);     
}
#endif

#if PCA_MODE==4
void DRV_PCA_Timer_Wizard_Init() 
{
        _push_(SFRPI);
        __DRV_SFR_PageIndex(0);
        __DRV_PCA_Disable();
        __DRV_PCA_SetCounterValue(65536-(TIM_PERIOD));
        __DRV_PCA_SetCounterReloadValue(65536-(TIM_PERIOD));
        __DRV_PCA_ClockSource_Select(TIM_CKSRC);
        #if(PWM0_TIM_EN)
          CCAPM0 = ECOM0 | MAT0;
          __DRV_PCA_SetPWM0DutyValue(TIM0_VAL);
        #endif
        #if(PWM1_TIM_EN)
          CCAPM1 = ECOM1 | MAT1;
          __DRV_PCA_SetPWM1DutyValue(TIM1_VAL);
        #endif
        #if(PWM2_TIM_EN)
          CCAPM2 = ECOM2 | MAT2;
          __DRV_PCA_SetPWM2DutyValue(TIM2_VAL);
        #endif
        #if(PWM3_TIM_EN)
          CCAPM3 = ECOM3 | MAT3;
          __DRV_PCA_SetPWM3DutyValue(TIM3_VAL);
        #endif
        #if(PWM4_TIM_EN)
          CCAPM4 = ECOM4 | MAT4;
          __DRV_PCA_SetPWM4DutyValue(TIM4_VAL);
        #endif
        #if(PWM5_TIM_EN)
          CCAPM5 = ECOM5 | MAT5;
          __DRV_PCA_SetPWM5DutyValue(TIM5_VAL);
        #endif
        #if(PWM6_TIM_EN)
          __DRV_SFR_PageIndex(1);
          CCAPM6 = ECOM6 | MAT6;
          __DRV_PCA_SetPWM6DutyValue(TIM6_VAL);
        #endif  
        #if(PWM7_TIM_EN)
          __DRV_SFR_PageIndex(1);
          CCAPM7 = ECOM7 | MAT7;
          __DRV_PCA_SetPWM7DutyValue(TIM7_VAL);
        #endif
        _pop_(SFRPI);\
    
}
#endif


/**
*******************************************************************************
* @brief       PCA Initial.
* @details     Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
                DRV_PCA_Wizard_Init();
* @endcode
*******************************************************************************
*/
void DRV_PCA_Wizard_Init()
{  
   
    #if PCA_MODE==0
        __DRV_PCA_IC_Wizard_Init();       
    #endif  
          
    #if PCA_MODE==1
        DRV_PCA_OC_Wizard_Init();
    #endif

    #if PCA_MODE==2
        DRV_PCA_PWM_Wizard_Init();    
    #endif          

    #if PCA_MODE==3
        DRV_PCA_PWM_DTG_Wizard_Init();    
    #endif          

    #if PCA_MODE==4
        __DRV_PCA_Timer_Wizard_Init();
    #endif

    #if(PCA_ENABLE)
        __DRV_PCA_Enable();
    #endif  
}

/// @cond PCA_wizard_option_#endif
#endif
/// @endcond//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond urt0_wizard_option_#if
#if MG82F6D17_UART0_WIZARD
/// @endcond

/**
*******************************************************************************
* @brief       UART0 Wizard Initial.
* @details    Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
                DRV_UART0_Wizard_Init();
* @endcode
*******************************************************************************
*/
void DRV_UART0_Wizard_Init(void)
{
    #if(URT0_Advance_Mode&&URT0_Option)
        #if(URT0_Advance)
            #if(URT0_Mode==0x00)
                #if(URT0_Mode0_Shift)
                    #if(URT0_Mode0_Shift_Baud_Rate_Config)
                        __DRV_URT0_Mode0_Wizard_Init();
                    #endif
                #endif
            #elif(URT0_Mode==0x01)
                #if(URT0_Mode1_8Bit)
                    #if(URT0_Mode1_8Bit_Baud_Rate_Config)
                        __DRV_URT0_Mode1_Wizard_Init();
                    #endif
                #endif
            #elif(URT0_Mode==0x02)
                #if(URT0_Mode2_9Bit)
                    #if(URT0_Mode2_9Bit_Baud_Rate_Config)
                        __DRV_URT0_Mode2_Wizard_Init();
                    #endif
                #endif
            #elif(URT0_Mode==0x03)
                #if(URT0_Mode3_9Bit)
                    #if(URT0_Mode3_9Bit_Baud_Rate_Config)
                        __DRV_URT0_Mode3_Wizard_Init();
                    #endif
                #endif
            #elif(URT0_Mode==0x04)
                #if(URT0_Mode4_Spi_Master)
                    #if(URT0_Mode4_Spi_Master_Baud_Rate_Config)
                        __DRV_URT0_Mode4_Wizard_Init();
                    #endif
                #endif
            #elif(URT0_Mode==0x05)
                #if(URT0_Mode5_Lin_Bus)
                    #if(URT0_Mode5_Lin_Bus_Baud_Rate_Config)
                        __DRV_URT0_Mode5_Wizard_Init();
                    #endif
                #endif
            #endif
        #endif
    #endif
    #if(URT0_Easy_Mode&&(!URT0_Option))
        #if(URT0_Easy)
            __DRV_URT0_Easy_Wizard_Init();
        #endif
    #endif
	TI0=1;//for printf application;
}
/// @cond urt0_wizard_option_#endif
#endif
/// @endcond







//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Add system clock initial void function for code size.
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond urt1_wizard_option_#if
#if MG82F6D17_UART1_WIZARD
/// @endcond
/**
*******************************************************************************
* @brief       UART1 Initial.
* @details    Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
                DRV_UART1_Wizard_Init();
* @endcode
*******************************************************************************
*/
void DRV_UART1_Wizard_Init(void)
{
#if(URT1_Advance_Mode&&URT1_Option)
    #if(URT1_Advance)
        #if(URT1_Mode==0x00)
            #if(URT1_Mode0_Shift)
                #if(URT1_Mode0_Shift_Baud_Rate_Config)
                        __DRV_URT1_Mode0_Wizard_Init();
                #endif
            #endif
        #elif(URT1_Mode==0x01)
            #if(URT1_Mode1_8Bit)
                #if(URT1_Mode1_8Bit_Baud_Rate_Config)
                        __DRV_URT1_Mode1_Wizard_Init();
                #endif
            #endif
        #elif(URT1_Mode==0x02)
            #if(URT1_Mode2_9Bit)
                #if(URT1_Mode2_9Bit_Baud_Rate_Config)
                        __DRV_URT1_Mode2_Wizard_Init();
                #endif
            #endif
        #elif(URT1_Mode==0x03)
            #if(URT1_Mode3_9Bit)
                #if(URT1_Mode3_9Bit_Baud_Rate_Config)
                        __DRV_URT1_Mode3_Wizard_Init();
                #endif
            #endif
        #elif(URT1_Mode==0x04)
            #if(URT1_Mode4_Spi_Master)
                #if(URT1_Mode4_Spi_Master_Baud_Rate_Config)
                        __DRV_URT1_Mode4_Wizard_Init();
                #endif
            #endif
        #endif
    #endif
#endif
#if(URT1_Easy_Mode&&(!URT1_Option))
    #if(URT1_Easy)
            __DRV_URT1_Easy_Wizard_Init();
    #endif
#endif
}
/// @cond urt1_wizard_option_#endif
#endif
/// @endcond

//@del{
void UART_test(void)
{
SFRPI=1;
P6M0=P6M0|(P6M01);
SFRPI=0;
//P1M0=P1M0&(~P1M06);
P33=0;

#if(URT0_Option&&URT0_Advance_Mode&&MG82F6D17_UART0_WIZARD)
    #if(URT0_Mode == 0x02||URT0_Mode == 0x03)
                DRV_URT0_ClearTXData9th();
    #endif
    #if (URT0_Mode==0x00||URT0_Mode==0x01||URT0_Mode==0x02||URT0_Mode==0x03||URT0_Mode==0x04)
        #if (URT0_Mode0_Shift_Input_Output&&URT0_Mode==0x00)
        #else
                DRV_URT0_SendTXData(0x55);
        #endif
    #elif URT0_Mode==0x05
    __DRV_URT0_Lin_Bus_TxRx_Select(UART0_LIN_BUS_TX);
    DRV_URT0_SetSyncBreak();
    DRV_URT0_SendTXData(0x55);
    #endif
#endif
#if(!URT0_Option&&URT0_Easy_Mode&&MG82F6D17_UART0_WIZARD)
    #if(URT0_Mode == 0x02||URT0_Mode == 0x03)
                DRV_URT0_ClearTXData9th();
    #endif
                DRV_URT0_SendTXData(0x55);
#endif
#if(URT1_Option&&URT1_Advance_Mode&&MG82F6D17_UART1_WIZARD)
    #if (URT1_Mode == 0x02||URT1_Mode == 0x03)
                DRV_URT1_ClearTXData9th();
    #endif
    #if (URT1_Mode==0x00||URT1_Mode==0x01||URT1_Mode==0x02||URT1_Mode==0x03||URT1_Mode==0x04)
        #if (URT1_Mode0_Shift_Input_Output&&URT1_Mode==0x00)
        #else
                DRV_URT1_SendTXData(0x55);
        #endif
    #endif
#endif
#if(!URT1_Option&&URT1_Easy_Mode&&MG82F6D17_UART1_WIZARD)
    #if (URT1_Mode == 0x02||URT1_Mode == 0x03)
                DRV_URT1_ClearTXData9th();
    #endif
                DRV_URT1_SendTXData(0x55);
#endif

        while(1)
        {
    #if(URT0_Option&&URT0_Advance_Mode&&MG82F6D17_UART0_WIZARD)
        #if (URT0_Mode == 0x01||URT0_Mode == 0x02||URT0_Mode == 0x03)
                if((DRV_URT0_GetTI0())&&(DRV_URT0_GetRI0()))
                {
                    #if (URT0_Mode == 0x02||URT0_Mode == 0x03)
                        if(DRV_URT0_GetRXData9th())
                            {
                                DRV_URT0_SetTXData9th();
                            }
                        else
                            {
                                DRV_URT0_ClearTXData9th();
                            }
                    #endif
                    DRV_URT0_SendTXData(DRV_URT0_ReceiveRXData());
                    DRV_URT0_ClearRI0();
                    DRV_URT0_ClearTI0();
                }
        #elif URT0_Mode == 0x00
                    if(DRV_URT0_GetRI0())
                    {
                        __DRV_URT0_SerialReception_Cmd(MW_DISABLE);
                        DRV_URT0_ClearRI0();
                    }
       #elif URT0_Mode == 0x04
                    if(DRV_URT0_GetTI0())
                    {
                        DRV_URT0_SendTXData(DRV_URT0_ReceiveRXData());
                        DRV_URT0_ClearTI0();
                    }
       #elif URT0_Mode == 0x05
                    if(DRV_URT0_GetTI0())
                    {
                        DRV_URT0_ClearTI0();
                        if(DRV_URT0_GetSyncBreakFlag())                            //1. Master
                            {
                                DRV_URT0_SetSyncBreakFlag();
                                DRV_URT0_SendTXData(0x55);
                            }
                        else
                            {
                                DRV_URT0_SendTXData(0xAA);
                            }
                    }
       #endif
    #endif
    #if(!URT0_Option&&URT0_Easy_Mode&&MG82F6D17_UART0_WIZARD)
                if((DRV_URT0_GetTI0())&&(DRV_URT0_GetRI0()))
                {
                    DRV_URT0_SendTXData(DRV_URT0_ReceiveRXData());
                    DRV_URT0_ClearTI0();

                }
    #endif
    #if(URT1_Option&&URT1_Advance_Mode&&MG82F6D17_UART1_WIZARD)
       #if (URT1_Mode == 0x01||URT1_Mode == 0x02||URT1_Mode == 0x03)
               if((DRV_URT1_GetTI1())&&(DRV_URT1_GetRI1()))
               {
            #if (URT1_Mode == 0x02||URT1_Mode == 0x03)
                        if(DRV_URT1_GetRXData9th())
                            {
                                DRV_URT1_SetTXData9th();
                            }
                        else
                            {
                                DRV_URT1_ClearTXData9th();
                            }
            #endif
                    DRV_URT1_SendTXData(DRV_URT1_ReceiveRXData());
                    DRV_URT1_ClearRI1();
                    DRV_URT1_ClearTI1();
               }
      #elif URT1_Mode == 0x00
                    if(DRV_URT1_GetRI1())
                    {
                        __DRV_URT1_SerialReception_Cmd(MW_DISABLE);
                        DRV_URT1_ClearRI1();
                    }
      #elif URT1_Mode == 0x04
                    if(DRV_URT1_GetTI1())
                    {
                        DRV_URT1_SendTXData(DRV_URT1_ReceiveRXData());
                        DRV_URT1_ClearTI1();

                    }
      #endif
    #endif
    #if(!URT1_Option&&URT1_Easy_Mode&&MG82F6D17_UART1_WIZARD)
               if((DRV_URT1_GetTI1())&&(DRV_URT1_GetRI1()))
               {
                    DRV_URT1_SendTXData(DRV_URT1_ReceiveRXData());
                    DRV_URT1_ClearRI1();
                    DRV_URT1_ClearTI1();
               }
    #endif
        }
}
//@del}

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Timmins_20200706 //bugNum_Authour_Date
 * >> Add system clock initial void function for code size.
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/// @cond spi_wizard_option_#if
#if MG82F6D17_SPI_WIZARD
/// @endcond

/**
*******************************************************************************
* @brief       SPI Wizard Initial.
* @details    Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
                DRV_SPI_Wizard_Init();
* @endcode
*******************************************************************************
*/
void DRV_SPI_Wizard_Init(void)
{
    #if(SPI_Advance_Mode&&SPI_Option)
        #if(SPI_Advance)
            #if(!SPI_Mode)
                #if(SPI_Slave_Mode)
                    #if(SPI_Slave_Mode_Initial==0x00||SPI_Slave_Mode_Initial==0x01||SPI_Slave_Mode_Initial==0x02)
                        __DRV_SPI_Slave_Wizard_Init();
                    #endif
                #endif
            #elif(SPI_Mode)
                #if(SPI_Master_Mode)
                    #if(SPI_Master_Mode_Initial==0x00||SPI_Master_Mode_Initial==0x03||SPI_Master_Mode_Initial==0x04)
                        __DRV_SPI_Master_Wizard_Init();
                    #endif
                #endif
            #endif
        #endif
    #endif
    #if(SPI_Easy_Mode&&(!SPI_Option))
        #if(SPI_Easy)
            __DRV_SPI_Easy_Wizard_Init();
        #endif
    #endif
}
/// @cond spi_wizard_option_#endif
#endif
/// @endcond



//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200527 //bugNum_Authour_Date
 * >> Add I2C0 Wizard initial
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing I2C0 by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
                DRV_I2C0_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_I2C0_WIZARD
/// @endcond
void DRV_I2C0_Wizard_Init(void)
{
    #if (DRV_I2C0_Wizard_Mode == 1)//Easy Mode
        #if DRV_I2C0_Wizard_Easy_Check
            __DRV_I2C0_ClockRate_Select(DRV_I2C0_Easy_ClockRate_Config);
            __DRV_I2C0_Enable();
        #endif
    #elif (DRV_I2C0_Wizard_Mode == 2)//Advanced Mode
        #if DRV_I2C0_Wizard_Advanced_Check
            #if DRV_I2C0_Master_Mode_En
                __DRV_I2C0_ClockRate_Select(DRV_I2C0_Basic_ClockRate_Config);
                __DRV_I2C0_Enable();
            #elif DRV_I2C0_Slave_Mode_En
                #if DRV_I2C0_General_Call
                    __DRV_I2C0_GC_Enable();
                #endif
                __DRV_I2C0_SetSlaveAddress(DRV_I2C0_Slave_Address);
                __DRV_I2C0_SetACK();
                __DRV_I2C0_Enable();
            #endif
        #endif

    #endif
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif  //MG82F6D17_I2C0_WIZARD
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Alan_20200529 //bugNum_Authour_Date
 * >> Add beeper initial void function for code size.
 * #1.00_Alan_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Beeper Wizard Initialization.
* @details     Config Wizard Option
* @return      None
* @note        None
* @par         Example
* @code
               DRV_BEEPER_Wizard_Init();
* @endcode
*******************************************************************************
*/

/// @cond DRV_BEEPER_Wizard_Init_#if
#if MG82F6D17_BEEPER_WIZARD
/// @endcond

void DRV_BEEPER_Wizard_Init(void)
{
    uint8_t P_data;
    P_data = DRV_PageP_Read(DCON0_P);
    P_data &= ~OCDE_P;
    DRV_PageP_Write(DCON0_P, P_data);
    _push_(SFRPI);
    __DRV_SFR_PageIndex(0);
    P4M0 |= P4M04;
    __DRV_SFR_PageIndex(2);
    P4M1 &=  ~P4M14;
    _pop_(SFRPI);
    __DRV_BEEPER_ModeSelect(AUXR3_BPOCx);

}
/// @cond DRV_BEEPER_Wizard_Init_#if
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200527 //bugNum_Authour_Date
 * >> Add KBI initial void function for code size.
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing KBI by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
                DRV_KBI_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_KBI_EN
/// @endcond
void DRV_KBI_Wizard_Init(void)
{
    __DRV_KBI_Pattern_Wizard_Config();
    __DRV_KBI_PortPinMask_Wizard_Config();
    #if KBI_PATTERN_MATCHING_SELECTION
        __DRV_KBI_PatternMatchingType_Select(KBI_PATTERN_MATCHING_SELECTION);
    #endif
    #if KBI_TRIGGER_TYPE
        __DRV_KBI_TriggerType_Select(KBI_TRIGGER_TYPE);
    #endif
    #if KBI_FILTER_MODE_TYPE
        __DRV_KBI_FilterMode_Select(KBI_FILTER_MODE_TYPE);
    #endif
}

/// @cond __DRV_Wizard_Without_Doxygen
#endif  //MG82F6D17_KBI_EN
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Alan_20200529 //bugNum_Authour_Date
 * >> Add IAP_Boundary initial void function for code size.
 * #1.00_Alan_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing IAP boundary.
* @details
* @return      None
* @note        None
* @par         Example
* @code
               DRV_IAP_Boundary_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond DRV_IAP_Boundary_Wizard_Init_#if
#if MG82F6D17_IAP_WIZARD
/// @endcond
void DRV_IAP_Boundary_Wizard_Init(void)
{
    DRV_PageP_Write(IAPLB_P, IAP_BOUNDARY);
}
/// @cond DRV_IAP_Boundary_Wizard_Init_#if
#endif
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #0.07_Kevin_20200527 //bugNum_Authour_Date
 * >> Add interrupt initial void function for code size.
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 * #1.01_Kevin_20200901 //bugNum_Authour_Date
 * >> Added DMA DCF0/TF5/TF6 Interrupt Enable/Disable initial function 
 ******************************************************************************
 */
//@del}

/**
*******************************************************************************
* @brief       Initializing all interrupts by wizard option.
* @details
* @return      None
* @note        None
* @par         Example
* @code
                DRV_INT_Wizard_Init();
* @endcode
*******************************************************************************
*/
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_INT_ALL_WIZARD
/// @endcond
void DRV_INT_Wizard_Init(void)
{
    //***Check Module Interrupt function Turn on/off***************************
    #if (MG82F6D17_INT0_EN)
        #if MG82F6D17_INT0_PRIORITY
            __DRV_INT0_Priority_Select(MG82F6D17_INT0_PRIORITY);
        #endif 
        #if nINT0_TRIGGER_TYPE
            __DRV_INT0_TriggerType_Select(nINT0_TRIGGER_TYPE);
        #endif
        #if nINT0_FILTER_MODE_TYPE
            __DRV_INT0_FilterMode_Select(nINT0_FILTER_MODE_TYPE);
        #endif
        __DRV_INT0_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_TIMER0_IT_EN)
        #if MG82F6D17_TIMER0_PRIORITY
            __DRV_INT_Timer0_Priority_Select(MG82F6D17_TIMER0_PRIORITY);
        #endif
        __DRV_TIMER0_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_INT1_EN)
        #if MG82F6D17_INT1_PRIORITY
            __DRV_INT1_Priority_Select(MG82F6D17_INT1_PRIORITY);
        #endif
        #if nINT1_TRIGGER_TYPE
            __DRV_INT1_TriggerType_Select(nINT1_TRIGGER_TYPE);
        #endif
        #if nINT1_FILTER_MODE_TYPE
            __DRV_INT1_FilterMode_Select(nINT1_FILTER_MODE_TYPE);
        #endif
        __DRV_INT1_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_TIMER1_IT_EN)
        #if MG82F6D17_TIMER1_PRIORITY
            __DRV_INT_Timer1_Priority_Select(MG82F6D17_TIMER1_PRIORITY);
        #endif
        __DRV_TIMER1_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_S0_IT_EN)
        #if MG82F6D17_S0_PRIORITY
            __DRV_INT_S0_Priority_Select(MG82F6D17_S0_PRIORITY);
        #endif
        __DRV_URT0_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_TIMER2_IT_EN)
        #if MG82F6D17_TIMER2_PRIORITY
            __DRV_INT_Timer2_Priority_Select(MG82F6D17_TIMER2_PRIORITY);
        #endif
        #if (MG82F6D17_TIMER2_TF2L_EN)
            __DRV_TIMER2_TF2L_IT_Cmd(MW_ENABLE);
        #endif
        __DRV_TIMER2_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_INT2_EN)
        #if MG82F6D17_INT2_PRIORITY
            __DRV_INT2_Priority_Select(MG82F6D17_INT2_PRIORITY);
        #endif
        #if nINT2_TRIGGER_TYPE
            __DRV_INT2_TriggerType_Select(nINT2_TRIGGER_TYPE);
        #endif
        #if nINT2_FILTER_MODE_TYPE
            __DRV_INT2_FilterMode_Select(nINT2_FILTER_MODE_TYPE);
        #endif
        __DRV_INT2_IT_Cmd(MW_ENABLE);
    #endif    
    #if (MG82F6D17_SPI_IT_EN)
        #if MG82F6D17_SPI_PRIORITY
            __DRV_INT_SPI_Priority_Select(MG82F6D17_SPI_PRIORITY);
        #endif   
        __DRV_SPI_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_ADC_IT_EN)
        #if MG82F6D17_ADC_PRIORITY
            __DRV_INT_ADC_Priority_Select(MG82F6D17_ADC_PRIORITY);
        #endif
        #if (MG82F6D17_ADC_ADCI_EN == MW_DISABLE)
            __DRV_ADC_IgnoreADCI_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_ADC_SMPF_EN)
            __DRV_ADC_SampleFlag_IT(MW_ENABLE);
        #endif
        #if (MG82F6D17_ADC_ADCWI_EN)
            __DRV_ADC_WindowDetect_IT(MW_ENABLE);
        #endif
        __DRV_ADC_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_PCA_IT_EN)
        #if MG82F6D17_PCA_PRIORITY
            __DRV_INT_PCA_Priority_Select(MG82F6D17_PCA_PRIORITY);
        #endif
        #if (MG82F6D17_PCA_COUNTER_EN)
            __DRV_PCA_Counter_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE0_EN)
            __DRV_PCA_Module0_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE1_EN)
            __DRV_PCA_Module1_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE2_EN)
            __DRV_PCA_Module2_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE3_EN)
            __DRV_PCA_Module3_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE4_EN)
            __DRV_PCA_Module4_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE5_EN)
            __DRV_PCA_Module5_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE6_EN)
            __DRV_PCA_Module6_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_PCA_MODULE7_EN)
            __DRV_PCA_Module7_IT_Cmd(MW_ENABLE);
        #endif
        __DRV_PCA_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_SYSFLAG_EN)
        #if MG82F6D17_PSF_PRIORITY
            __DRV_INT_SYSFLAG_Priority_Select(MG82F6D17_PSF_PRIORITY);
        #endif
        #if (MG82F6D17_WDT_IT_EN)
            __DRV_WDT_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_RTC_IT_EN)
            __DRV_RTC_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_BOD0_IT_EN)
            __DRV_BODx_BOD0_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_BOD1_IT_EN)
            __DRV_BODx_BOD1_IT_Cmd(MW_ENABLE);
        #endif
        __DRV_INT_SystemFlag_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_KBI_EN)
        #if MG82F6D17_KBI_PRIORITY
            __DRV_INT_KBI_Priority_Select(MG82F6D17_KBI_PRIORITY);
        #endif
        DRV_KBI_Wizard_Init();
        __DRV_KBI_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_I2C0_IT_EN)
        #if MG82F6D17_I2C0_PRIORITY
            __DRV_INT_I2C0_Priority_Select(MG82F6D17_I2C0_PRIORITY);
        #endif
        __DRV_I2C0_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_S1_IT_EN)
        #if MG82F6D17_S1_PRIORITY
            __DRV_INT_S1_Priority_Select(MG82F6D17_S1_PRIORITY);
        #endif
        __DRV_URT1_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_TIMER3_IT_EN)
        #if MG82F6D17_TIMER3_PRIORITY
            __DRV_INT_Timer3_Priority_Select(MG82F6D17_TIMER3_PRIORITY);
        #endif
        #if (MG82F6D17_TIMER3_TF3L_EN)
            __DRV_TIMER3_TF3L_IT_Cmd(MW_ENABLE);
        #endif
        __DRV_TIMER3_IT_Cmd(MW_ENABLE);
    #endif
    #if (MG82F6D17_DMA_IT_EN)
        #if MG82F6D17_DMA_PRIORITY
            __DRV_INT_DMA_Priority_Select(MG82F6D17_DMA_PRIORITY);
        #endif
        #if (MG82F6D17_DMA_DCF0_EN)
            __DRV_DMA_DCF0_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_DMA_TF5_EN)
            __DRV_DMA_TF5_IT_Cmd(MW_ENABLE);
        #endif
        #if (MG82F6D17_DMA_TF6_EN)
            __DRV_DMA_TF6_IT_Cmd(MW_ENABLE);
        #endif
        __DRV_DMA_IT_Cmd(MW_ENABLE);
    #endif
    //**************************************************************************
    
    __DRV_INT_ITEA_Enable();    //Global Interrupt Enable
}

/// @cond __DRV_Wizard_Without_Doxygen
#endif  //MG82F6D17_INT_ALL_WIZARD
/// @endcond

//@del{
/**
 ******************************************************************************
 * Modify History:
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 ******************************************************************************
 */
//@del}

/**
 *******************************************************************************
 * @brief       System_Init function can initialize each driver.
 * @details
 * @return      None.
 * @note
 * @par Example
 * @code
 *              System_Wizard_Init();
 * @endcode
 * @bug
 *******************************************************************************
 */

void System_Wizard_Init(void)
{
    //Ch14. GPIO
    #if MG82F6D17_GPIO_WIZARD
        DRV_GPIO_Wizard_Init();
    #endif

    //Ch9. System Clock
    #if MG82F6D17_CLK_WIZARD
        DRV_CLK_Wizard_Init();
    #endif

    //Ch8. DMA
    #if MG82F6D17_DMA_WIZARD
        DRV_DMA_Wizard_Init();
    #endif

    //Ch10. WDT
    #if MG82F6D17_WDT_WIZARD
        DRV_WDT_Wizard_Init();
    #endif

    //Ch11. RTC
    #if MG82F6D17_RTC_WIZARD
        DRV_RTC_Wizard_Init();
    #endif

    //Ch12. System Reset

    //Ch13. Power Management
    #if MG82F6D17_PW_WIZARD
        #if MG82F6D17_BODx_WIZARD
            DRV_PW_BODx_Wizard_Init();
        #endif
    #endif

    //Ch15. Interrupt -> Move to END

    //Ch16. Timers/Counters
    #if MG82F6D17_TIMER_WIZARD
        DRV_TIMER_Wizard_Init();
    #endif

    //Ch17. PCA
    #if MG82F6D17_PCA_WIZARD
        DRV_PCA_Wizard_Init();
    #endif

    //Ch18. SerialPort0
    #if MG82F6D17_UART0_WIZARD
        DRV_UART0_Wizard_Init();
    #endif

    //Ch19. SerialPort1
    #if MG82F6D17_UART1_WIZARD
        DRV_UART1_Wizard_Init();
    #endif

    //Ch20. SPI
    #if MG82F6D17_SPI_WIZARD
        DRV_SPI_Wizard_Init();
    #endif

    //Ch21. TWI0/I2C
    #if MG82F6D17_I2C0_WIZARD
        DRV_I2C0_Wizard_Init();
    #endif

    //Ch22. STWI/SI2C

    //Ch23. Beeper
    #if MG82F6D17_BEEPER_WIZARD
        DRV_BEEPER_Wizard_Init();
    #endif
    
    //Ch24. KBI

    //Ch25. GPL

    //Ch26. ADC
    #if MG82F6D17_ADC_WIZARD
        __DRV_ADC_Wizard_Init();
    #endif

    //Ch28. IAP
    #if MG82F6D17_IAP_WIZARD
       DRV_IAP_Boundary_Wizard_Init();
    #endif

    //Ch15. Interrupt
    #if MG82F6D17_INT_ALL_WIZARD
        DRV_INT_Wizard_Init();
    #endif
}

