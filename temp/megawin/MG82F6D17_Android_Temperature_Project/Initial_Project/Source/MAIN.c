/**
 ******************************************************************************
 *
 * @file        MAIN.C
 *
 * @brief       This is the C code format main file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01
 * #0.02
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"

sbit    CT1711_GPIO   =    P1^0;
sbit    SLEEP_GPIO    =    P3^3;//nINT1 wakeup, 0:Wakeup, 1:Sleep



xdata BYTE SLAVE_ADDR1[3]=0;
xdata BYTE Read_CT1711;
uint16_t delayNmS;
uint16_t delayNuS;

//@del{
extern void MG82F6D17_Test_Group(void);
//@del}


void DelayMs(unsigned int DelayTime)
{
	delayNmS=DelayTime;
	while(delayNmS !=0)
	{
	
	}
}

void Delay10us(unsigned int DelayTime)
{
	delayNuS=DelayTime;
	while(delayNuS !=0)
	{
	
	}
}

void CT1711_Init(void)
{
    CT1711_GPIO = 0; //pull-low DIO pin
    Delay10us(50); //setup delay >400us
    CT1711_GPIO = 1; //pull-high line
}

bit CT1711_Read_Bit(void)
{
    bit bi_data;
    CT1711_GPIO = 0; //pull-low DIO pin with 1us
    Delay10us(1); //delay 10us
//    _nop_();
    CT1711_GPIO = 1; //then release DIO pin
    Delay10us(1); //delay 20us
    bi_data = CT1711_GPIO;//Read 1-bit Data from DIO pin
    _nop_();
    CT1711_GPIO = 1;//then release DIO pin 
    Delay10us(3); //delay 30us
    return bi_data;
}

uint8_t CT1711_Read_Byte(void)
{
        uint8_t byte = 0;
        int i;
        for(i = 8; i > 0; i--) {
               byte <<= 1;
               byte |= CT1711_Read_Bit();
        }
        return byte;
}


void CT1711_Read_Temp_Degree(void) {
   
        CT1711_Init();//Initialization
        DelayMs(150);//delay 150ms

        if (CT1711_Read_Bit())//get CC0 bit;
            SLAVE_ADDR1[2] |= GF_2;
        else
            SLAVE_ADDR1[2] &= ~GF_2;
        Delay10us(1);//delay 10us
        if (CT1711_Read_Bit())//get CC1 bit;
            SLAVE_ADDR1[2] |= GF_1;
        else
            SLAVE_ADDR1[2] &= ~GF_1;   
        Delay10us(1);//delay 10us

//        if ((SLAVE_ADDR1[2]&0x06) == 0x00) {
 
        if (CT1711_Read_Bit())//get signature bit;
            SLAVE_ADDR1[2] |= GF_0;
        else
            SLAVE_ADDR1[2] &= ~GF_0;

        Delay10us(1);//delay 10us
        SLAVE_ADDR1[1] = CT1711_Read_Byte();//Read 1st byte of Temperature,
        // exclude cc0,cc1 and S bit, MSB is 128oC, LSB is 1.0oC of this byte.
        Delay10us(1);//delay 10us
        SLAVE_ADDR1[0] = CT1711_Read_Byte();//Then read 2nd Byte of Temperature,
        // MSB is 0.5oC, LSB bit stands for 0.00390625oC(1/256)
        Delay10us(1);//delay 10us
//        }
}


void main ()
{

    System_Wizard_Init();

    //@del{
    MG82F6D17_Test_Group();
    //@del}
    Read_CT1711 = 0;
    
    while (1)
    {
        if (SLEEP_GPIO)
            PCON0 |= PD;
        else 
            if (Read_CT1711)
            {
                CT1711_Read_Temp_Degree();
                __DRV_I2C0_IT_Enable();
            }
    }

}


