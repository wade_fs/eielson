/**
 ******************************************************************************
 *
 * @file        MG82F6D17_INT_VECTOR.c
 *
 * @brief       This is the C code format main file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01
 * #0.02
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"



extern uint16_t delayNmS;
extern uint16_t delayNuS;

/**
 *******************************************************************************
 * Interrupts ISR
 *******************************************************************************
 */
/**
 *******************************************************************************
 * @brief       External Interrupt 0 Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    ExINT0_IE0_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_INT0_EN
/// @endcond
void ExINT0_IE0_ISR(void)  interrupt INT0_ISR_VECTOR
{
    _push_(SFRPI);
    __DRV_INT0_ClearFlag();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond


/**
 *******************************************************************************
 * @brief       Timer 0 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    TIMER0_TF0_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_TIMER0_IT_EN
/// @endcond
void TIMER0_TF0_ISR(void)   interrupt TIMER0_ISR_VECTOR
{
    _push_(SFRPI);
    TL0=LOBYTE(TIMER0_Mode1_Timer_Interval);
	TH0=HIBYTE(TIMER0_Mode1_Timer_Interval);	
	if(delayNmS!=0) delayNmS--;
    DRV_TIMER0_ClearTF0();

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       External Interrupt 1 Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    ExINT1_IE1_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_INT1_EN
/// @endcond
void ExINT1_IE1_ISR(void)   interrupt INT1_ISR_VECTOR
{
    _push_(SFRPI);
    __DRV_INT1_ClearFlag();//Wakeup from Sleep Mode
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       Timer 1 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    TIMER1_TF1_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_TIMER1_IT_EN
/// @endcond
void TIMER1_TF1_ISR(void)   interrupt TIMER1_ISR_VECTOR
{
    _push_(SFRPI);
    TL1=LOBYTE(TIMER1_Mode1_Timer_Interval);
	TH1=HIBYTE(TIMER1_Mode1_Timer_Interval);
	if(delayNuS!=0) delayNuS--;
    DRV_TIMER1_ClearTF1();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       UART0 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    UART0_S0RI_S0TI_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_S0_IT_EN
/// @endcond
void UART0_S0RI_S0TI_ISR(void)   interrupt S0_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       Timer 2 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    TIMER2_TF2_EXF2_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_TIMER2_IT_EN
/// @endcond
void TIMER2_TF2_EXF2_ISR(void)   interrupt TIMER2_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       External Interrupt 2 Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    ExINT2_IE2_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_INT2_EN
/// @endcond
void ExINT2_IE2_ISR(void)   interrupt INT2_ISR_VECTOR
{
    _push_(SFRPI);
    __DRV_INT2_ClearFlag();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       SPI Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    SPI_SPIF_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_SPI_IT_EN
/// @endcond
void SPI_SPIF_ISR(void)   interrupt SPI_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       ADC Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    ADC_ADCI_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_ADC_IT_EN
/// @endcond
void ADC_ADCI_ISR(void)   interrupt ADC_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       PCA 0 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    PCA0_CF_CCFn_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_PCA_IT_EN
/// @endcond
void PCA0_CF_CCFn_ISR(void)   interrupt PCA0_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       System Flag Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    SYSFlag_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_SYSFLAG_EN
/// @endcond
void SYSFlag_ISR()  interrupt SYSFLAG_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       KBI Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    KBI_KBIF_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_KBI_EN
/// @endcond
void KBI_KBIF_ISR()  interrupt KBI_ISR_VECTOR
{
    _push_(SFRPI);
    __DRV_KBI_ClearFlag();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       I2C0 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    I2C0_SI_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_I2C0_IT_EN
/// @endcond
void I2C0_SI_ISR(void)   interrupt I2C0_ISR_VECTOR
{
    _push_(SFRPI);
    Sample_I2C0_ISR_Handle();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       UART1 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    UART1_S1RI_S1TI_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_S1_IT_EN
/// @endcond
void UART1_S1RI_S1TI_ISR(void)   interrupt S1_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       Timer 3 Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    TIMER3_TF3_EXF3_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_TIMER3_IT_EN
/// @endcond
void TIMER3_TF3_EXF3_ISR(void)   interrupt TIMER3_ISR_VECTOR
{
    _push_(SFRPI);

    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond



/**
 *******************************************************************************
 * @brief       DMA Interrupt Vector Service Routine
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    DMA_DCF0_ISR();
 * @endcode
 *******************************************************************************
 */
/// @cond __DRV_Wizard_Without_Doxygen
#if MG82F6D17_DMA_IT_EN
/// @endcond
void DMA_DCF0_ISR(void)   interrupt DMA_ISR_VECTOR
{
    _push_(SFRPI);
    __DRV_DMA_ClearCompleteFlag();
    _pop_(SFRPI);
}
/// @cond __DRV_Wizard_Without_Doxygen
#endif
/// @endcond

