/**
 ******************************************************************************
 *
 * @file        MG82F6D17_CONFIG.h
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MA82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IS" without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the worldwide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal  
 */ 
#ifndef _MG82F6D17_CONFIG_H
#define _MG82F6D17_CONFIG_H


#include <intrins.h>
#include "REG_MG82F6D17.h"
#include "TYPEDEF.h"
#include "stdio.h"


/**
 ******************************************************************************
 * do-while template
 ******************************************************************************
 */
#define MWT( __stuff__ )  do { __stuff__ } while (0)


/**
 ******************************************************************************
 * Headers
 ******************************************************************************
 */

//COMMON
#include "MG82F6D17_COMMON_DRV.h"
//#include "MG82F6D17_COMMON_MID.h"
#include "MG82F6D17_WIZARD.h"

//Ch8. DMA
#include "MG82F6D17_DMA_DRV.h"
//#include "MG82F6D17_DMA_MID.h"

//Ch9. System Clock
#include "MG82F6D17_CLK_DRV.h"
#include "MG82F6D17_CLK_MID.h"

//Ch10. WDT
#include "MG82F6D17_WDT_DRV.h"
//#include "MG82F6D17_WDT_MID.h"

//Ch11. RTC
#include "MG82F6D17_RTC_DRV.h"
//#include "MG82F6D17_RTC_MID.h"

//Ch12. System Reset
//#include "MG82F6D17_xxxxx_DRV.h"
//#include "MG82F6D17_xxxxx_MID.h"

//Ch13. Power Management
#include "MG82F6D17_PW_DRV.h"
//#include "MG82F6D17_PW_MID.h"

//Ch14. GPIO
#include "MG82F6D17_GPIO_DRV.h"
//#include "MG82F6D17_GPIO_MID.h"

//Ch15. Interrupt
#include "MG82F6D17_INT_DRV.h"
//#include "MG82F6D17_INT_MID.h"
#include "MG82F6D17_INT_VECTOR.h"

//Ch16. Timers/Counters
#include "MG82F6D17_TIMER_DRV.h"
//#include "MG82F6D17_TIMER_MID.h"

//Ch17. PCA
#include "MG82F6D17_PCA_DRV.h"
//#include "MG82F6D17_PCA_MID.h"

//Ch18. SerialPort0
#include "MG82F6D17_UART0_DRV.h"
#include "MG82F6D17_UART0_MID.h"

//Ch19. SerialPort1
#include "MG82F6D17_UART1_DRV.h"
#include "MG82F6D17_UART1_MID.h"

//Ch20. SPI
#include "MG82F6D17_SPI_DRV.h"
#include "MG82F6D17_SPI_MID.h"

//Ch21. TWI0/I2C0
#include "MG82F6D17_I2C0_DRV.h"
//#include "MG82F6D17_I2C0_MID.h"

//Ch22. STWI/SI2C
//#include "MG82F6D17_SI2C0_DRV.h"
//#include "MG82F6D17_SI2C0_MID.h"

//Ch23. Beeper
#include "MG82F6D17_BEEPER_DRV.h"
//#include "MG82F6D17_xxxxx_MID.h"

//Ch24. KBI
#include "MG82F6D17_KBI_DRV.h"
//#include "MG82F6D17_KBI_MID.h"

//Ch25. GPL
//#include "MG82F6D17_GPL_DRV.h"
//#include "MG82F6D17_GPL_MID.h"

//Ch26. ADC
#include "MG82F6D17_ADC_DRV.h"
//#include "MG82F6D17_ADC_MID.h"

//Ch28. IAP
#include "MG82F6D17_IAP_DRV.h"
//#include "MG82F6D17_IAP_MID.h"

#endif


