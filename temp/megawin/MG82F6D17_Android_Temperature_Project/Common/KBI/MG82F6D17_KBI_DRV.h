/**
 ******************************************************************************
 *
 * @file        MG82F6D17_KBI_DRV.H
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_Kevin_20191223 //bugNum_Authour_Date
 * #0.02_Kevin_20200107 //bugNum_Authour_Date
 * #0.03_Kevin_20200107 //bugNum_Authour_Date
 * #0.05_Kevin_20200401 //bugNum_Authour_Date
 * >> Divid MG82F6D17_KBI_DRV.h to 2 files
 * >>   MG82F6D17_KBI_DRV.h : DRV
 * >>   MG82F6D17_KBI_MID.h : Wizard_Initial
 * >> MG82F6D17_KBI_MID.h
 * >>   Modify param data format. (Ref MG82F6D17_KBI_DRV.xlsx)
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

 
#ifndef _MG82F6D17_KBI_DRV_H
#define _MG82F6D17_KBI_DRV_H



/// @cond __DRV_KBI_Pin01Mux_Select
#define KBI0_P10_KBI1_P11     0x00
#define KBI0_P47_KBI1_P33     0x08
/// @endcond

/// @cond __DRV_KBI_Pin23Mux_Select
#define KBI2_P30_KBI3_P31     0x00
#define KBI2_P22_KBI3_P24     0x10
/// @endcond

/// @cond __DRV_KBI_Pin45Mux_Select
#define KBI4_P33_KBI5_P15     0x00
#define KBI4_P34_KBI5_P35     0x40
#define KBI4_P60_KBI5_P61     0x80
#define KBI4_P15_KBI5_P33     0xC0
/// @endcond

/// @cond __DRV_KBI_Pin67Mux_Select
#define KBI6_P16_KBI7_P17     0x00
#define KBI6_P30_KBI7_P31     0x20
/// @endcond

/// @cond __DRV_KBI_TriggerType_Select
#define KBI_TRIGGER_TYPE_LEVEL    0x00
#define KBI_TRIGGER_TYPE_EDGE     0x20
/// @endcond

/// @cond __DRV_KBI_PatternMatchingType_Select
#define KBI_MATCHING_TYPE_NOT_EQUAL_PATTERN    0x00
#define KBI_MATCHING_TYPE_EQUAL_PATTERN        0x02
/// @endcond

/// @cond __DRV_KBI_Priority_Select
#define KBI_PRIORITY_LOWEST           0x0000
#define KBI_PRIORITY_MIDDLE_LOW       0x0020
#define KBI_PRIORITY_MIDDLE_HIGH      0x2000
#define KBI_PRIORITY_HIGHEST          0x2020
/// @endcond

/// @cond __DRV_KBI_FilterMode_Select
#define KBI_FILTER_DISABLE          0x00
#define KBI_FILTER_SYSCLK_X3        0x40
#define KBI_FILTER_SYSCLK_DIV6X3    0x80
#define KBI_FILTER_S0TOF            0xC0
/// @endcond

/**
 *******************************************************************************
 * @brief       Enable KBI interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_IT_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_IT_Enable()\
    MWT(\
        EIE1 |= EKB;\
    )


/**
 *******************************************************************************
 * @brief       Disable KBI interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_IT_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_IT_Disable()\
    MWT(\
        EIE1 &= ~EKB;\
    )


/**
 *******************************************************************************
 * @brief       Enable/Disable KBI interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config inversion control bit
 *  @arg\b      MW_DISABLE : Set KBI interrupt disable (default)
 *  @arg\b      MW_ENABLE : Set KBI interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_IT_Cmd(__STATE__)\
    (__STATE__==MW_ENABLE?(EIE1 |= EKB):(EIE1 &= ~EKB))


/**
 *******************************************************************************
 * @brief       Set KBI interrupt flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_SetFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_SetFlag()\
    MWT(\
        KBCON |= KBIF;\
    )


/**
 *******************************************************************************
 * @brief       Get KBI interrupt flag
 * @details
 * @return      return KBIF status
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_GetFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_GetFlag()    (KBCON & KBIF)



/**
 *******************************************************************************
 * @brief       Clear KBI interrupt flag
 * @details 
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_ClearFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_ClearFlag()\
    MWT(\
        KBCON &= ~KBIF;\
    )



/**
 *******************************************************************************
 * @brief       Set KBI0&KBI1 input pin source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI0 and KBI1 input pin source
 *  @arg\b      KBI0_P10_KBI1_P11: Set KBI0 and KBI1 input pin source to P10 and P11 (default)
 *  @arg\b      KBI0_P47_KBI1_P33: Set KBI0 and KBI1 input pin source to P47 and P33
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_Pin01Mux_Select(KBI0_P10_KBI1_P11);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Pin01Mux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(5);\
        AUXR8 &= ~KBI0PS0;\
        AUXR8 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Set KBI2&KBI3 input pin source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI2 and KBI3 input pin source
 *  @arg\b      KBI2_P30_KBI3_P31: Set KBI2 and KBI3 input pin source to P30 and P31 (default)
 *  @arg\b      KBI2_P22_KBI3_P24: Set KBI2 and KBI3 input pin source to P22 and P24
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_Pin23Mux_Select(KBI2_P22_KBI3_P24);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Pin23Mux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        AUXR6 &= ~KBI2PS0;\
        AUXR6 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )

/**
 *******************************************************************************
 * @brief       Set KBI4&KBI5 input pin source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI4 and KBI5 input pin source
 *  @arg\b      KBI4_P33_KBI5_P15: Set KBI4 and KBI5 input pin source to P33 and P15 (default)
 *  @arg\b      KBI4_P34_KBI5_P35: Set KBI4 and KBI5 input pin source to P34 and P35
 *  @arg\b      KBI4_P60_KBI5_P61: Set KBI4 and KBI5 input pin source to P60 and P61
 *  @arg\b      KBI4_P15_KBI5_P33: Set KBI4 and KBI5 input pin source to P15 and P33
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_Pin45Mux_Select(KBI4_P34_KBI5_P35);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Pin45Mux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        AUXR6 &= ~(KBI4PS1 | KBI4PS0);\
        AUXR6 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )

/**
 *******************************************************************************
 * @brief       Set KBI6&KBI7 input pin source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI6 and KBI7 input pin source
 *  @arg\b      KBI6_P16_KBI7_P17: Set KBI6 and KBI7 input pin source to P16 and P17 (default)
 *  @arg\b      KBI6_P30_KBI7_P31: Set KBI6 and KBI7 input pin source to P30 and P31
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI67_PinMux_Select(KBI6_P16_KBI7_P17);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Pin67Mux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        AUXR6 &= ~KBI6PS0;\
        AUXR6 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )

/**
 *******************************************************************************
 * @brief       Set KBI event trigger type
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI event trigger type
 *  @arg\b      KBI_TRIGGER_TYPE_LEVEL: Set KBI event trigger type to level trigger (default)
 *  @arg\b      KBI_TRIGGER_TYPE_EDGE: Set KBI event trigger type to edge trigger
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_TriggerType_Select(KBI_TRIGGER_TYPE_EDGE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_TriggerType_Select(__SELECT__)\
    MWT(\
        KBCON &= ~KBES;\
        KBCON |= __SELECT__;\
    )


/**
 *******************************************************************************
 * @brief       Set KBI pattern matching polarity type
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI pattern matching polarity type
 *  @arg\b      KBI_MATCHING_TYPE_NOT_EQUAL_PATTERN: Set to not equal pattern trigger KBI interrupt (default)
 *  @arg\b      KBI_MATCHING_TYPE_EQUAL_PATTERN: Set to equal pattern trigger KBI interrupt
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_PatternMatchingType_Select(KBI_MATCHING_TYPE_EQUAL_PATTERN);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_PatternMatchingType_Select(__SELECT__)\
    MWT(\
        KBCON &= ~PATN_SEL;\
        KBCON |= __SELECT__;\
    )

/**
 *******************************************************************************
 * @brief       Set KBI interrupt priority
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI interrupt priority level
 *  @arg\b      KBI_PRIORITY_LOWEST: Set KBI interrupt priority to lowest level (default)
 *  @arg\b      KBI_PRIORITY_MIDDLE_LOW: Set KBI interrupt priority to middle low level
 *  @arg\b      KBI_PRIORITY_MIDDLE_HIGH: Set KBI interrupt priority to middle high level
 *  @arg\b      KBI_PRIORITY_HIGHEST: Set KBI interrupt priority to highest level
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_Priority_Select(KBI_PRIORITY_LOWEST);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_Priority_Select(__SELECT__)\
    MWT(\
        EIP1L &= ~PKBL;\
        EIP1L |= LOBYTE(__SELECT__);\
        EIP1H &= ~PKBH;\
        EIP1H |= HIBYTE(__SELECT__);\
    )

/**
 *******************************************************************************
 * @brief       Set KBI filter mode type
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set KBI filter mode type
 *  @arg\b      KBI_FILTER_DISABLE: Disable KBI filter mode type (default)
 *  @arg\b      KBI_FILTER_SYSCLKx3: Set KBI filter mode type to SYSCLKx3
 *  @arg\b      KBI_FILTER_SYSCLKDIV6x3: Set KBI filter mode type to SYSCLK/6x3
 *  @arg\b      KBI_FILTER_S0TOF: Set KBI filter mode type to S0TOF
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_FilterMode_Select(KBI_FILTER_DISABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_FilterMode_Select(__SELECT__)\
    MWT(\
        KBCON &= ~(KBCS1 | KBCS0);\	 
        KBCON |= __SELECT__;\
    )




/**
 *******************************************************************************
 * @brief       Set KBI pattern
 * @details
 * @param[in]   \_\_PATTERN\_\_ : Set KBI pattern to KBPATN (0x00 ~ 0xFF)
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_SetPattern(0xFF);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_SetPattern(__PATTERN__)\
    MWT(\
        KBPATN = __PATTERN__;\
    )




/**
 *******************************************************************************
 * @brief       Set KBI port pin mask
 * @details
 * @param[in]   \_\_MASK\_\_ : Set KBI interrupt mask bit to KBMASK (0x00 ~ 0xFF)
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_KBI_SetPortPinMask(0xF0);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_KBI_SetPortPinMask(__MASK__)\
    MWT(\
        KBMASK = __MASK__;\
    )





#endif  //_MG82F6D17_KBI_DRV_H