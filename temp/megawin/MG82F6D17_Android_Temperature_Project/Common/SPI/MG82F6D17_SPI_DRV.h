/**
 ******************************************************************************
 *
 * @file        MG82F6D17_SPI_DRV.h
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2020 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS"  without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the world wide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
  @if HIDE
 * Modify History:
 * #1.00_Timmins_20200605 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #1.00_Timmins_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef MG82F6D17_SPI_DRV_H
#define MG82F6D17_SPI_DRV_H

/**
*****************************************************************************
* @brief        SPI Interrupt
* @details      Enable SPI Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_IT_Cmd(__STATE__)\
    __STATE__==0?(EIE1=EIE1&(~ESPI)):(EIE1=EIE1|(ESPI))
/**
*******************************************************************************
* @brief        SPI Mode Select
* @details      Set SPEN SSIG nSS MSTR
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DISABLE
*  @arg\b       SPI_SLAVE_SELECT_BY_nSS
*  @arg\b       SPI_SLAVE_SELECT_BY_MSTR
*  @arg\b       SPI_MASTER_SELECT_BY_nSS
*  @arg\b       SPI_MASTER_SELECT_BY_MSTR
* @return       None
* @note         If nSS pin is driven low, then MSTR will be cleared to ‘0’ by H/W automatically, and SPEN is cleared, MODF is set.
* @par         Example
* @code
               __DRV_SPI_Mode_Select(SPI_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_SPI_Mode_Select
#define SPI_DISABLE               0x00
#define SPI_SLAVE_SELECT_BY_nSS   0x01
#define SPI_SLAVE_SELECT_BY_MSTR  0x02
#define SPI_MASTER_SELECT_BY_nSS  0x03
#define SPI_MASTER_SELECT_BY_MSTR 0x04
///@endcond
#define __DRV_SPI_Mode_Select(__MODE__)\
    MWT(\
        __DRV_SPI_SetSPEN(__MODE__);\
        __DRV_SPI_SetSSIG(__MODE__);\
        __DRV_SPI_SetMSTR(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        Set SPEN
* @details      Enable SPI
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DISABLE
*  @arg\b       SPI_SLAVE_SELECT_BY_nSS
*  @arg\b       SPI_SLAVE_SELECT_BY_MSTR
*  @arg\b       SPI_MASTER_SELECT_BY_nSS
*  @arg\b       SPI_MASTER_SELECT_BY_MSTR
* @return       None
* @note         If nSS pin is driven low, then MSTR will be cleared to ‘0’ by H/W automatically, and SPEN is cleared, MODF is set.
* @par          Example
* @code
                __DRV_SPI_SetSPEN(SPI_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetSPEN(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SPCON=SPCON&(~SPEN)):\
        (__MODE__^0x01)==0?(SPCON=SPCON|(SPEN)):\
        (__MODE__^0x02)==0?(SPCON=SPCON|(SPEN)):\
        (__MODE__^0x03)==0?(SPCON=SPCON|(SPEN)):\
        (__MODE__^0x04)==0?(SPCON=SPCON|(SPEN)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        SPI
* @details      Enable SPI
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DISABLE
*  @arg\b       SPI_SLAVE_SELECT_BY_nSS
*  @arg\b       SPI_SLAVE_SELECT_BY_MSTR
*  @arg\b       SPI_MASTER_SELECT_BY_nSS
*  @arg\b       SPI_MASTER_SELECT_BY_MSTR
* @return       None
* @note         If nSS pin is driven low, then MSTR will be cleared to ‘0’ by H/W automatically, and SPEN is cleared, MODF is set.
* @par          Example
* @code
                __DRV_SPI_SetSSIG(SPI_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetSSIG(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SPCON=SPCON&(~SSIG)):\
        (__MODE__^0x01)==0?(SPCON=SPCON&(~SSIG)):\
        (__MODE__^0x02)==0?(SPCON=SPCON|(SSIG)):\
        (__MODE__^0x03)==0?(SPCON=SPCON&(~SSIG)):\
        (__MODE__^0x04)==0?(SPCON=SPCON|(SSIG)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        SPI
* @details      Enable SPI
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DISABLE
*  @arg\b       SPI_SLAVE_SELECT_BY_nSS
*  @arg\b       SPI_SLAVE_SELECT_BY_MSTR
*  @arg\b       SPI_MASTER_SELECT_BY_nSS
*  @arg\b       SPI_MASTER_SELECT_BY_MSTR
* @return       None
* @note         If nSS pin is driven low, then MSTR will be cleared to ‘0’ by H/W automatically, and SPEN is cleared, MODF is set.
* @par          Example
* @code
                __DRV_SPI_SetMSTR(SPI_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetMSTR(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SPCON=SPCON&(~MSTR)):\
        (__MODE__^0x01)==0?(SPCON=SPCON&(~MSTR)):\
        (__MODE__^0x02)==0?(SPCON=SPCON&(~MSTR)):\
        (__MODE__^0x03)==0?(SPCON=SPCON|(MSTR)):\
        (__MODE__^0x04)==0?(SPCON=SPCON|(MSTR)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        SPI Clock Rate Select(Master Mode)
* @details      Set SPR2 SPR1 SPR0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_4
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_8
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_16
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_32
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_64
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_2
*  @arg\b       SPI_CLOCK_RATE_S0TOF_DIV_6
*  @arg\b       SPI_CLOCK_RATE_T0OF_DIV_6
* @return       None
* @note         None
* @par         Example
* @code
               __DRV_SPI_Clock_Rate_Select(SPI_CLOCK_RATE_SYSCLK_DIV_4)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_SPI_Clock_Rate_Select
#define SPI_CLOCK_RATE_SYSCLK_DIV_4  0x00
#define SPI_CLOCK_RATE_SYSCLK_DIV_8  0x01
#define SPI_CLOCK_RATE_SYSCLK_DIV_16 0x02
#define SPI_CLOCK_RATE_SYSCLK_DIV_32 0x03
#define SPI_CLOCK_RATE_SYSCLK_DIV_64 0x04
#define SPI_CLOCK_RATE_SYSCLK_DIV_2  0x05
#define SPI_CLOCK_RATE_S0TOF_DIV_6   0x06
#define SPI_CLOCK_RATE_T0OF_DIV_6    0x07
///@endcond
#define __DRV_SPI_Clock_Rate_Select(__SELECT__)\
    MWT(\
        __DRV_SPI_SetSPR2(__SELECT__);\
        __DRV_SPI_SetSPR1(__SELECT__);\
        __DRV_SPI_SetSPR0(__SELECT__);\
    ;)
/**
*****************************************************************************
* @brief        SPI Clock Rate Select
* @details      Set SPR2
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_4
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_8
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_16
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_32
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_64
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_2
*  @arg\b       SPI_CLOCK_RATE_S0TOF_DIV_6
*  @arg\b       SPI_CLOCK_RATE_T0OF_DIV_6
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_SetSPR2(SPI_CLOCK_RATE_SYSCLK_DIV_4)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetSPR2(__SELECT__)\
    MWT(\
        (__SELECT__^0x00)==0?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^0x01)==0?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^0x02)==0?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^0x03)==0?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^0x04)==0?(SPSTAT=SPSTAT|(SPR2)):\
        (__SELECT__^0x05)==0?(SPSTAT=SPSTAT|(SPR2)):\
        (__SELECT__^0x06)==0?(SPSTAT=SPSTAT|(SPR2)):\
        (__SELECT__^0x07)==0?(SPSTAT=SPSTAT|(SPR2)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        SPI Clock Rate Select
* @details      Set SPR1
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_4
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_8
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_16
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_32
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_64
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_2
*  @arg\b       SPI_CLOCK_RATE_S0TOF_DIV_6
*  @arg\b       SPI_CLOCK_RATE_T0OF_DIV_6
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_SetSPR1(SPI_CLOCK_RATE_SYSCLK_DIV_4)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetSPR1(__SELECT__)\
    MWT(\
        (__SELECT__^0x00)==0?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^0x01)==0?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^0x02)==0?(SPCON=SPCON|(SPR1)):\
        (__SELECT__^0x03)==0?(SPCON=SPCON|(SPR1)):\
        (__SELECT__^0x04)==0?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^0x05)==0?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^0x06)==0?(SPCON=SPCON|(SPR1)):\
        (__SELECT__^0x07)==0?(SPCON=SPCON|(SPR1)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        SPI Clock Rate Select
* @details      Set SPR0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_4
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_8
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_16
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_32
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_64
*  @arg\b       SPI_CLOCK_RATE_SYSCLK_DIV_2
*  @arg\b       SPI_CLOCK_RATE_S0TOF_DIV_6
*  @arg\b       SPI_CLOCK_RATE_T0OF_DIV_6
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_SetSPR0(SPI_CLOCK_RATE_SYSCLK_DIV_4)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetSPR0(__SELECT__)\
    MWT(\
        (__SELECT__^0x00)==0?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^0x01)==0?(SPCON=SPCON|(SPR0)):\
        (__SELECT__^0x02)==0?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^0x03)==0?(SPCON=SPCON|(SPR0)):\
        (__SELECT__^0x04)==0?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^0x05)==0?(SPCON=SPCON|(SPR0)):\
        (__SELECT__^0x06)==0?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^0x07)==0?(SPCON=SPCON|(SPR0)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        SPI Data Mode Select
* @details      Set CPOL CPHA
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_RISING_TRAILING_EDGE_SAMPLE_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_FALLING_TRAILING_EDGE_SETUP_RISING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_FALLING_TRAILING_EDGE_SAMPLE_RISING
* @return       None
* @note         None
* @par         Example
* @code
               __DRV_SPI_Data_Mode_Select(SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_SPI_Data_Mode_Select
#define SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING 0x00
#define SPI_DATA_LEADING_EDGE_SETUP_RISING_TRAILING_EDGE_SAMPLE_FALLING 0x01
#define SPI_DATA_LEADING_EDGE_SAMPLE_FALLING_TRAILING_EDGE_SETUP_RISING 0x02
#define SPI_DATA_LEADING_EDGE_SETUP_FALLING_TRAILING_EDGE_SAMPLE_RISING 0x03
///@endcond
#define __DRV_SPI_Data_Mode_Select(__MODE__)\
    MWT(\
        __DRV_SPI_SetCPOL(__MODE__);\
        __DRV_SPI_SetCPHA(__MODE__);\
    ;)

/**
*****************************************************************************
* @brief        SPI Data Mode Select
* @details      Set CPOL
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_RISING_TRAILING_EDGE_SAMPLE_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_FALLING_TRAILING_EDGE_SETUP_RISING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_FALLING_TRAILING_EDGE_SAMPLE_RISING
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_SetCPOL(SPI_DATA_LEADING_EDGE_SETUP_FALLING_TRAILING_EDGE_SAMPLE_RISING)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetCPOL(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SPCON=SPCON&(~CPOL)):\
        (__MODE__^0x01)==0?(SPCON=SPCON|(CPOL)):\
        (__MODE__^0x02)==0?(SPCON=SPCON&(~CPOL)):\
        (__MODE__^0x03)==0?(SPCON=SPCON|(CPOL)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        SPI Clock Rate Select
* @details      Set CPHA
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_RISING_TRAILING_EDGE_SAMPLE_FALLING
*  @arg\b       SPI_DATA_LEADING_EDGE_SAMPLE_FALLING_TRAILING_EDGE_SETUP_RISING
*  @arg\b       SPI_DATA_LEADING_EDGE_SETUP_FALLING_TRAILING_EDGE_SAMPLE_RISING
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_SetCPHA(SPI_DATA_LEADING_EDGE_SAMPLE_RISING_TRAILING_EDGE_SETUP_FALLING)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_SetCPHA(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SPCON=SPCON&(~CPHA)):\
        (__MODE__^0x01)==0?(SPCON=SPCON|(CPHA)):\
        (__MODE__^0x02)==0?(SPCON=SPCON&(~CPHA)):\
        (__MODE__^0x03)==0?(SPCON=SPCON|(CPHA)):_nop_();\
    ;)

/**
*******************************************************************************
* @brief        SPI Pin MUX
* @details      Set SIPS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17
*  @arg\b       SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_PinMux_Select(SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_SPI_PinMux_Select
#define SPI_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17 0
#define SPI_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33 1
///@endcond
#define __DRV_SPI_PinMux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __SELECT__==0?(AUXR10=AUXR10&(~SPIPS0)):(AUXR10=AUXR10|(SPIPS0));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        SPI Data Order
* @details      Set DORD
* @param[in]    \_\_ORDER\_\_ :
*  @arg\b       SPI_DATA_ORDER_MSB
*  @arg\b       SPI_DATA_ORDER_LSB
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_DataOrder_Select(SPI_DATA_ORDER_MSB)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_SPI_DataOrder_Select
#define SPI_DATA_ORDER_MSB 0
#define SPI_DATA_ORDER_LSB 1
///@endcond
#define __DRV_SPI_DataOrder_Select(__ORDER__)\
    __ORDER__==0?(SPCON=SPCON&(~DORD)):(SPCON=SPCON|(DORD))
/**
*******************************************************************************
* @brief        SPI Model Control for Daisy-chain connection.
* @details      Set SPI0M0
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Model_Control_Cmd(MW_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Model_Control_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(4);\
        __STATE__==0?(AUXR4=AUXR4&(~SPI0M0)):(AUXR4=AUXR4|(SPI0M0));\
        __DRV_SFR_PageIndex(0);\
    ;)
#endif
///@cond
bool DRV_SPI_GetSPIF(void);
void DRV_SPI_ClearSPIF(void);
bool DRV_SPI_GetWCOL(void);
void DRV_SPI_ClearWCOL(void);
bool DRV_SPI_GetTHRF(void);
bool DRV_SPI_GetSPIBSY(void);
bool DRV_SPI_GetMODF(void);
void DRV_SPI_ClearMODF(void);
void DRV_SPI_SendSPDAT(uint8_t TXData);
uint8_t DRV_SPI_ReceiveSPDAT(void);
//@endcond

