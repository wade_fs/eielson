/**
 ******************************************************************************
 *
 * @file        MG82F6D17_SPI_MID.h
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2020 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS"  without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the world wide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #1.00_Timmins_20200605 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #1.00_Timmins_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef MG82F6D17_SPI_MID_H
#define MG82F6D17_SPI_MID_H






///@cond __DRV_SPI_Easy_Wizard_Init
#define OPTION_ADDRESS_BASE 0x000000
#define OPTION_MATCH 0

#define SPI_CONFIG0 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG1 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG2 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE1|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG3 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE1|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG4 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE2|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG5 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE2|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG6 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE3|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG7 (SPI_MODE_SLAVE_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE3|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)


#define SPI_CONFIG8 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE0|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG9 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE0|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG10 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE1|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG11 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE1|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG12 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE2|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG13 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE2|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG14 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE3|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG15 (SPI_MODE_SLAVE_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER0|SPI_DATA_MODE3|SPI_SLAVE_CLOCK_RATE_BY_EDGE|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)


#define SPI_CONFIG16 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_4|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG17 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_4|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG18 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_8|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG19 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_8|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG20 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_16|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG21 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_16|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG22 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_32|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG23 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_32|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG24 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_64|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG25 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_64|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG26 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_2|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG27 (SPI_MODE_MASTER_BY_nSS|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_2|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)


#define SPI_CONFIG28 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_4|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG29 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_4|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG30 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_8|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG31 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_8|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG32 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_16|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG33 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_16|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG34 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_32|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG35 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_32|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG36 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_64|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG37 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_64|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

#define SPI_CONFIG38 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_2|SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17)

#define SPI_CONFIG39 (SPI_MODE_MASTER_BY_MSTR|SPI_MODEL_CONTROL_NORMAL|SPI_DATA_ORDER1|SPI_DATA_MODE0|SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_2|SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33)

//constant SPI mode control daisy-chain (0~15) option
#define SPI_MODEL_CONTROL_NORMAL (0x000000+OPTION_ADDRESS_BASE)
#define SPI_MODEL_CONTROL_DAISY_CHAIN (0x100000+OPTION_ADDRESS_BASE)
//constant SPI data order (0~15) option
#define SPI_DATA_ORDER0 (0x000000+OPTION_ADDRESS_BASE)
#define SPI_DATA_ORDER1 (0x010000+OPTION_ADDRESS_BASE)
//constant SPI data mode (0~15) option
#define SPI_DATA_MODE0 (0x000000+OPTION_ADDRESS_BASE)
#define SPI_DATA_MODE1 (0x001000+OPTION_ADDRESS_BASE)
#define SPI_DATA_MODE2 (0x002000+OPTION_ADDRESS_BASE)
#define SPI_DATA_MODE3 (0x003000+OPTION_ADDRESS_BASE)
//constant SPI mode (0~15) option
#define SPI_MODE_DISABLE        (0x000000+OPTION_ADDRESS_BASE)
#define SPI_MODE_SLAVE_BY_nSS   (0x000100+OPTION_ADDRESS_BASE)
#define SPI_MODE_SLAVE_BY_MSTR  (0x000200+OPTION_ADDRESS_BASE)
#define SPI_MODE_MASTER_BY_nSS  (0x000300+OPTION_ADDRESS_BASE)
#define SPI_MODE_MASTER_BY_MSTR (0x000400+OPTION_ADDRESS_BASE)
//constant SPI clock rate(0~15) option
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_4  (0x000000+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_8  (0x000010+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_16 (0x000020+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_32 (0x000030+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_64 (0x000040+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_SYSCLK_DIV_2  (0x000050+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_S0TOF_DIV_6   (0x000060+OPTION_ADDRESS_BASE)
#define SPI_MASTER_CLOCK_RATE_T0OF_DIV_6    (0x000070+OPTION_ADDRESS_BASE)
#define SPI_SLAVE_CLOCK_RATE_BY_EDGE    (0x000080+OPTION_ADDRESS_BASE)
//constant SPI pin config (0~15) option
#define SPI_PIN_nSS_P33_MOSI_P15_MISO_P16_SPICLK_P17 (0x000000+OPTION_ADDRESS_BASE)
#define SPI_PIN_nSS_P17_MOSI_P35_MISO_P34_SPICLK_P33 (0x000001+OPTION_ADDRESS_BASE)
///@endcond
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SPEN SSIG MSTR
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Mode_Easy_Select(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Mode_Easy_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(0);\
        __DRV_SPI_EasySetSPEN(__SELECT__);\
        __DRV_SPI_EasySetSSIG(__SELECT__);\
        __DRV_SPI_EasySetMSTR(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SPR2 SPR1 SPR0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Clock_Rate_Easy_Select(UART0_8BIT_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Clock_Rate_Easy_Select(__SELECT__)\
    MWT(\
        __DRV_SPI_EasySetSPR2(__SELECT__);\
        __DRV_SPI_EasySetSPR1(__SELECT__);\
        __DRV_SPI_EasySetSPR0(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set CPOL CPHA
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Data_Mode_Easy_Select(UART0_8BIT_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Data_Mode_Easy_Select(__SELECT__)\
        MWT(\
            __DRV_SPI_EasySetCPOL(__SELECT__);\
            __DRV_SPI_EasySetCPHA(__SELECT__);\
        ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set DORD
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Data_Order_Easy_Select(UART0_8BIT_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Data_Order_Easy_Select(__SELECT__)\
        MWT(\
            __DRV_SPI_EasySetDORD(__SELECT__);\
        ;)

/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SPIPS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_PinMux_Easy_Select(UART0_8BIT_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_PinMux_Easy_Select(__SELECT__)\
    MWT(\
        __DRV_SPI_EasySetSPIPS0(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SPI0M0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_Daisy_Chain_Easy_Select(UART0_8BIT_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_Daisy_Chain_Easy_Select(__SELECT__)\
    MWT(\
        __DRV_SPI_EasySetSPI0M0(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SPEN
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_EasySetSPEN(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPEN(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON|SPEN):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON|SPEN):_nop_());\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set SSIG
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_EasySetSSIG(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSSIG(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON&(~SSIG)):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON|SSIG):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON|SSIG):_nop_());\
    ;)
/**
*******************************************************************************
* @brief        SPI Easy Wizard
* @details      Set MSTR
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_SPI_EasySetMSTR(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetMSTR(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON&(~MSTR)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON|MSTR):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON|MSTR):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set SPR2
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_EasySetSPR2(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPR2(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPSTAT=SPSTAT&(~SPR2)):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPSTAT=SPSTAT|SPR2):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set SPR1
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetSPR1(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPR1(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|SPR1):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON&(~SPR1)):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard.
* @details     Set SPR0
* @param[in]   \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetSPR0(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPR0(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON&(~SPR0)):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON|SPR0):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON|SPR0):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set CPOL
* @param[in]   \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetCPOL(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetCPOL(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON|CPOL):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON&(~CPOL)):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set CPHA
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetCPHA(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetCPHA(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON|CPHA):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON&(~CPHA)):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set DORD
* @param[in]   \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
___DRV_SPI_EasySetDORD(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetDORD(__SELECT__)\
    MWT(\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(SPCON=SPCON&(~DORD)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(SPCON=SPCON|DORD):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(SPCON=SPCON|DORD):_nop_());\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set SPIPS0
* @param[in]   \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetSPIPS0(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPIPS0(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(AUXR10=AUXR10&(~SPIPS0)):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(AUXR10=AUXR10|SPIPS0):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief       SPI Easy Wizard
* @details     Set SPI0M0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b SPI_CONFIG0 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG1 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG2 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG3 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=0 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG4 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG5 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=0,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG6 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG7 :  Slave by nSS,nSS=0 MSTR=0 SSIG=0,CPOL=1 CPHA=1,clock rata by edge,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG8 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG9 :  Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG10 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG11 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=0 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG12 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG13 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=0,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG14 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG15 : Slave by MSTR,MSTR=0 SSIG=1,CPOL=1 CPHA=1,clock rata by edge,MSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG16 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG17 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG18 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG19 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG20 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG21 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG22 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG23 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33),normal.
*  @arg\b SPI_CONFIG24 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17),normal.
*  @arg\b SPI_CONFIG25 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG26 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG27 : Master by nSS,nSS=1 MSTR=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG28 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG29 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/4,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG30 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG31 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/8,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG32 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG33 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/16,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG34 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG35 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/32,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG36 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG37 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/64,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
*  @arg\b SPI_CONFIG38 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p33) MOSI(p15) MISO(p16) SPICLK(p17), normal.
*  @arg\b SPI_CONFIG39 : Master by MSTR,MSTR=1 SSIG=1, CPOL=0 CPHA=0,clock rata by sysclk/2,LSB,nSS(p17) MOSI(p35) MISO(p34) SPICLK(p33), normal.
* @return      None
* @note        None
* @par         Example
* @code
__DRV_SPI_EasySetSPI0M0(SPI_CONFIG0)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_SPI_EasySetSPI0M0(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(4);\
        ((__SELECT__^SPI_SELECT0)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT1)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT2)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT3)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT4)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT5)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT6)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT7)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT8)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT9)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT10)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT11)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT12)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT13)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT14)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT15)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT16)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT17)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT18)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT19)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT20)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT21)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT22)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT23)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT24)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT25)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT26)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT27)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT28)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT29)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT30)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT31)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT32)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT33)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT34)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT35)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT36)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT37)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT38)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):\
        (__SELECT__^SPI_SELECT39)==OPTION_MATCH?(AUXR7=AUXR7&(~SPI0M0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
#endif  //MG82F6D17_SPI_MID_H




