#include "MG82F6D17_CONFIG.h"
/**
 ******************************************************************************
 *
 * @file        MG82F6D17_TIMER_DRV.c
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS"  without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the world wide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
  @if HIDE
 * Modify History:
 * #0.07_Timmins_20200605 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #1.00_Timmins_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */
/**
*******************************************************************************
* @brief       TIMER0 Read overflow flag.
* @details     Read TF0
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER0_GetTF0();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER0_GetTF0(void)
{
    _push_(SFRPI);
    if(TF0==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER1 Read overflow flag.
* @details     Read TF1
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER1_GetTF1();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER1_GetTF1(void)
{
    _push_(SFRPI);
    if(TF1==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER2 Read overflow flag.
* @details     Read TF2
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER2_GetTF2();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER2_GetTF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    if(TF2==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER3 Read overflow flag.
* @details     Read TF3
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER3_GetTF3();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER3_GetTF3(void)
{
    _push_(SFRPI);
    SFRPI = 1;
    if(TF3==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER0 Clear overflow flag.
* @details     Clear TF0
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER0_ClearTF0();
* @endcode
*******************************************************************************
*/
void DRV_TIMER0_ClearTF0(void)
{
    _push_(SFRPI);
    TF0=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER1 Clear overflow flag.
* @details     Clear TF1
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER1_ClearTF1();
* @endcode
*******************************************************************************
*/
void DRV_TIMER1_ClearTF1(void)
{
    _push_(SFRPI);
    TF1=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 Clear overflow flag.
* @details     Clear TF2
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER2_ClearTF2();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_ClearTF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    TF2=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER3 Clear overflow flag.
* @details     Clear TF1
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_ClearTF3();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_ClearTF3(void)
{
    _push_(SFRPI);
    SFRPI=1;
    TF3=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER0 Set overflow flag.
* @details     Set TF0
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER0_SetTF0();
* @endcode
*******************************************************************************
*/
void DRV_TIMER0_SetTF0(void)
{
    _push_(SFRPI);
    TF0=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER1 Set overflow flag.
* @details     Set TF1
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER1_SetTF1();
* @endcode
*******************************************************************************
*/
void DRV_TIMER1_SetTF1(void)
{
    _push_(SFRPI);
    TF1=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 Set overflow flag.
* @details     Set TF2
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER2_SetTF2();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_SetTF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    TF2=1;
    _pop_(SFRPI);
}


/**
*******************************************************************************
* @brief       TIMER3 Set overflow flag.
* @details     Set TF3
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_SetTF3();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_SetTF3(void)
{
    _push_(SFRPI);
    SFRPI=1;
    TF3=1;
    _pop_(SFRPI);
}

/**
*******************************************************************************
* @brief       TIMER2 Read External flag.
* @details     Read EXF2
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER2_GetEXF2();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER2_GetEXF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    if(EXF2==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER2 Clear External flag.
* @details     Clear EXF2
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER2_ClearEXF2();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_ClearEXF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    EXF2=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 Set External flag.
* @details     Set EXF2
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER2_SetEXF2();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_SetEXF2(void)
{
    _push_(SFRPI);
    SFRPI=0;
    EXF2=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 Read Capture High Byte.
* @details     Read RCAP2H
* @return      None
* @note        None
* @par         Example
* @code
                tmp=DRV_TIMER2_GetRCAP2H();
* @endcode
*******************************************************************************
*/
uint8_t DRV_TIMER2_GetRCAP2H(void)
{
	uint8_t tmp;
    _push_(SFRPI);
    SFRPI=0;
    tmp=RCAP2H;
    _pop_(SFRPI);
    return(tmp);
}
/**
*******************************************************************************
* @brief       TIMER2 Read Capture Low Byte.
* @details     Read RCAP2L
* @return      None
* @note        None
* @par         Example
* @code
                tmp=DRV_TIMER2_GetRCAP2L();
* @endcode
*******************************************************************************
*/
uint8_t DRV_TIMER2_GetRCAP2L(void)
{
	uint8_t tmp;
    _push_(SFRPI);
    SFRPI=0;
    tmp=RCAP2L;
    _pop_(SFRPI);
    return(tmp);
}
/**
*******************************************************************************
* @brief       TIMER2 TL2 Set overflow flag.
* @details     Set TF2L
* @return      None
* @note        Before set RCLK_TF2L,please ensure TL2IS=1(__DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(MW_ENABLE)).
* @par         Example
* @code
                DRV_TIMER2_SetTF2L();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_SetTF2L(void)
{
    _push_(SFRPI);
    SFRPI=0;
    RCLK_TF2L=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 TL2 Clear overflow flag.
* @details     Set TF2L
* @return      None
* @note        Before clear RCLK_TF2L,please ensure TL2IS=1(__DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(MW_ENABLE)).
* @par         Example
* @code
                DRV_TIMER2_ClearTF2L();
* @endcode
*******************************************************************************
*/
void DRV_TIMER2_ClearTF2L(void)
{
    _push_(SFRPI);
    SFRPI=0;
    RCLK_TF2L=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER2 TL2 Read overflow flag.
* @details     Set TF2L
* @return      None
* @note        Before read RCLK_TF2L,please ensure TL2IS=1(__DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(MW_ENABLE))and TL2IE=1(__DRV_TIMER2_TF2L_IT_Cmd(MW_ENABLE)).
* @par         Example
* @code
                DRV_TIMER2_GetTF2L();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER2_GetTF2L(void)
{
    _push_(SFRPI);
    SFRPI=0;
    if(RCLK_TF2L)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER3 Read External flag.
* @details     Read EXF3
* @return      TRUE or FALSE
* @note        None
* @par         Example
* @code
                DRV_TIMER3_GetEXF3();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER3_GetEXF3(void)
{
    _push_(SFRPI);
    SFRPI=1;
    if(EXF3==1)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}
/**
*******************************************************************************
* @brief       TIMER3 Clear External flag.
* @details     Clear EXF3
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_ClearEXF3();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_ClearEXF3(void)
{
    _push_(SFRPI);
    SFRPI=1;
    EXF3=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER3 Set External flag.
* @details     Set EXF3
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_SetEXF3();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_SetEXF3(void)
{
    _push_(SFRPI);
    SFRPI=1;
    EXF3=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER3 Read Capture High Byte.
* @details     Read RCAP3H
* @return      None
* @note        None
* @par         Example
* @code
                tmp=DRV_TIMER3_GetRCAP3H();
* @endcode
*******************************************************************************
*/
uint8_t DRV_TIMER3_GetRCAP3H(void)
{
	uint8_t tmp;
    _push_(SFRPI);
    SFRPI=1;
    tmp=RCAP3H;
    _pop_(SFRPI);
    return(tmp);
}
/**
*******************************************************************************
* @brief       TIMER3 Read Capture Low Byte.
* @details     Read RCAP3L
* @return      None
* @note        None
* @par         Example
* @code
                tmp=DRV_TIMER3_GetRCAP3L();
* @endcode
*******************************************************************************
*/
uint8_t DRV_TIMER3_GetRCAP3L(void)
{
	uint8_t tmp;
    _push_(SFRPI);
    SFRPI=1;
    tmp=RCAP3L;
    _pop_(SFRPI);
    return(tmp);
}
/**
*******************************************************************************
* @brief       TIMER3 TL3 Set overflow flag.
* @details     Set TF3L
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_SetTF3L();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_SetTF3L(void)
{
    _push_(SFRPI);
    SFRPI=1;
    TF3L=1;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER3 TL3 Clear overflow flag.
* @details     Set TF3L
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_ClearTF3L();
* @endcode
*******************************************************************************
*/
void DRV_TIMER3_ClearTF3L(void)
{
    _push_(SFRPI);
    SFRPI=1;
    TF3L=0;
    _pop_(SFRPI);
}
/**
*******************************************************************************
* @brief       TIMER3 TL3 Read overflow flag.
* @details     Set TF3L
* @return      None
* @note        None
* @par         Example
* @code
                DRV_TIMER3_GetTF3L();
* @endcode
*******************************************************************************
*/
bool DRV_TIMER3_GetTF3L(void)
{
    _push_(SFRPI);
    SFRPI=1;
    if(TF3L)
    {
        _pop_(SFRPI);
        return TRUE;
    }
    //else
    //{
        _pop_(SFRPI);
        return FALSE;
    //}
}





