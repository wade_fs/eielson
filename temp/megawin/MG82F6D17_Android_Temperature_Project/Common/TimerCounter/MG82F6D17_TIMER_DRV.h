/**
 ******************************************************************************
 *
 * @file        MG82F6D17_TIMER_DRV.h
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V0.07
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS"  without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the world wide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
  @if HIDE
 * Modify History:
 * #0.07_Timmins_20200605 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #1.00_Timmins_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef MG82F6D17_TIMER_DRV_H
#define MG82F6D17_TIMER_DRV_H
/**
*****************************************************************************
* @brief        TIMER0 Interrupt
* @details      Enable Disable TIMER0 Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_IT_Cmd(__STATE__)\
    __STATE__==0?(ET0=0):(ET0=1)
/**
*****************************************************************************
* @brief        TIMER1 Interrupt
* @details      Enable Disable TIMER1 Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_IT_Cmd(__STATE__)\
    __STATE__==0?(ET1=0):(ET1=1)
/**
*****************************************************************************
* @brief        TIMER2 Interrupt
* @details      Enable Disable TIMER2 Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_IT_Cmd(__STATE__)\
    __STATE__==0?(ET2=0):(ET2=1)
/**
*****************************************************************************
* @brief        TIMER2 TF2L Interrupt
* @details      Enable Disable TIMER2 TF2L Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         Before set TCLK_TL2IE,please ensure TL2IS=1((__DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(MW_ENABLE)))
* @par          Example
* @code
                __DRV_TIMER2_TF2L_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_TF2L_IT_Cmd(__STATE__)\
        __STATE__==0?(TCLK_TL2IE=0):(TCLK_TL2IE=1)
/**
*****************************************************************************
* @brief        TIMER3 TF3L Interrupt
* @details      Enable Disable TIMER3 TF3L Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TF3L_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
    */
#define __DRV_TIMER3_TF3L_IT_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(TL3IE=0):(TL3IE=1);\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 Interrupt
* @details      Enable Disable TIMER3 Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_IT_Cmd(__STATE__)\
    __STATE__==0?(EIE2=EIE2&(~ET3)):(EIE2=EIE2|ET3)
/**
*****************************************************************************
* @brief        TIMER0 Run Control
* @details      Enable Disable TIMER0 RUN
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_Run_Cmd(__STATE__)\
    __STATE__==0?(TR0=0):(TR0=1)
/**
*****************************************************************************
* @brief        TIMER1 Run Control
* @details      Enable Disable TIMER1 RUN
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_Run_Cmd(__STATE__)\
    __STATE__==0?(TR1=0):(TR1=1)
/**
*****************************************************************************
* @brief        TIMER2 Run Control
* @details      Enable Disable TIMER2 RUN
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_Run_Cmd(__STATE__)\
    __STATE__==0?(TR2=0):(TR2=1)
/**
*****************************************************************************
* @brief        TIMER3 Run Control
* @details      Enable Disable TIMER3 RUN
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_Run_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(TR3=0):(TR3=1);\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Mode
* @details      Set T0M1 T0M0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_MODE0_8BIT_PWM
*  @arg\b       TIMER0_MODE1_16BIT_TIMER
*  @arg\b       TIMER0_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER0_MODE3_TWO_8BIT
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_Timer0_Mode_Select(TIMER0_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_Timer0_Mode_Select
#define TIMER0_MODE0_8BIT_PWM        0x00
#define TIMER0_MODE1_16BIT_TIMER     0x01
#define TIMER0_MODE2_8BIT_AUTORELOAD 0x02
#define TIMER0_MODE3_TWO_8BIT        0x03
///@endcond
#define __DRV_TIMER0_Mode_Select(__MODE__)\
    MWT(\
        __DRV_TIMER0_SetT0M1(__MODE__);\
        __DRV_TIMER0_SetT0M0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Operating Mode
* @details      Set T0M1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_MODE0_8BIT_PWM
*  @arg\b       TIMER0_MODE1_16BIT_TIMER
*  @arg\b       TIMER0_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER0_MODE3_TWO_8BIT
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0M1(TIMER0_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetT0M1(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T0M1)):\
        (__MODE__^0x01)==0?(TMOD=TMOD&(~T0M1)):\
        (__MODE__^0x02)==0?(TMOD=TMOD|(T0M1)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T0M1)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Operating Mode
* @details      Set T0M0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_MODE0_8BIT_PWM
*  @arg\b       TIMER0_MODE1_16BIT_TIMER
*  @arg\b       TIMER0_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER0_MODE3_TWO_8BIT
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0M0(TIMER0_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetT0M0(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T0M0)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T0M0)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T0M0)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T0M0)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Mode
* @details      Set T1M1 T1M0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_MODE0_8BIT_PWM
*  @arg\b       TIMER1_MODE1_16BIT_TIMER
*  @arg\b       TIMER1_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER1_MODE3_TIMER1_STOPPED
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Mode_Select(TIMER1_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/
    ///@cond __DRV_TIMER1_Mode_Select
#define TIMER1_MODE0_8BIT_PWM        0x00
#define TIMER1_MODE1_16BIT_TIMER     0x01
#define TIMER1_MODE2_8BIT_AUTORELOAD 0x02
#define TIMER1_MODE3_TIMER1_STOPPED  0x03
    ///@endcond
#define __DRV_TIMER1_Mode_Select(__MODE__)\
    MWT(\
        __DRV_TIMER1_SetT1M1(__MODE__);\
        __DRV_TIMER1_SetT1M0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Operating Mode
* @details      Set T1M1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_MODE0_8BIT_PWM
*  @arg\b       TIMER1_MODE1_16BIT_TIMER
*  @arg\b       TIMER1_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER1_MODE3_TIMER1_STOPPED
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1M1(TIMER1_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_SetT1M1(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T1M1)):\
        (__MODE__^0x01)==0?(TMOD=TMOD&(~T1M1)):\
        (__MODE__^0x02)==0?(TMOD=TMOD|(T1M1)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T1M1)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Operating Mode
* @details      Set T1M0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_MODE0_8BIT_PWM
*  @arg\b       TIMER1_MODE1_16BIT_TIMER
*  @arg\b       TIMER1_MODE2_8BIT_AUTORELOAD
*  @arg\b       TIMER1_MODE3_TIMER1_STOPPED
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1M0(TIMER1_MODE0_8BIT_PWM)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetT1M0(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T1M0)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T1M0)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T1M0)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T1M0)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Clock Source
* @details      Set T0XL T0X12 T0C_T
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T0_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_ILRCO
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
*  @arg\b       TIMER0_CLOCK_SOURCE_WDTPS
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_192
*  @arg\b       TIMER0_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_Clock_Source_Select(TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
    ///@cond __DRV_TIMER0_Clock_Source_Select
#define TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12  0x00
#define TIMER0_CLOCK_SOURCE_T0_PIN         0x01
#define TIMER0_CLOCK_SOURCE_SYSCLK         0x02
#define TIMER0_CLOCK_SOURCE_ILRCO          0x03
#define TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48  0x04
#define TIMER0_CLOCK_SOURCE_WDTPS          0x05
#define TIMER0_CLOCK_SOURCE_SYSCLK_DIV_192 0x06
#define TIMER0_CLOCK_SOURCE_T1OF           0x07
    ///@endcond
#define __DRV_TIMER0_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER0_SetT0XL(__MODE__);\
        __DRV_TIMER0_SetT0X12(__MODE__);\
        __DRV_TIMER0_SetT0C_T(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Clock Source
* @details      Set T0XL
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T0_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_ILRCO
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
*  @arg\b       TIMER0_CLOCK_SOURCE_WDTPS
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_192
*  @arg\b       TIMER0_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0XL(TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_SetT0XL(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(AUXR3=AUXR3&(~T0XL)):\
        (__MODE__^0x01)==0?(AUXR3=AUXR3&(~T0XL)):\
        (__MODE__^0x02)==0?(AUXR3=AUXR3&(~T0XL)):\
        (__MODE__^0x03)==0?(AUXR3=AUXR3&(~T0XL)):\
        (__MODE__^0x04)==0?(AUXR3=AUXR3|(T0XL)):\
        (__MODE__^0x05)==0?(AUXR3=AUXR3|(T0XL)):\
        (__MODE__^0x06)==0?(AUXR3=AUXR3|(T0XL)):\
        (__MODE__^0x07)==0?(AUXR3=AUXR3|(T0XL)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Clock Source
* @details      Set T0X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T0_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_ILRCO
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
*  @arg\b       TIMER0_CLOCK_SOURCE_WDTPS
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_192
*  @arg\b       TIMER0_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0X12(TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_SetT0X12(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(AUXR2=AUXR2&(~T0X12)):\
        (__MODE__^0x01)==0?(AUXR2=AUXR2&(~T0X12)):\
        (__MODE__^0x02)==0?(AUXR2=AUXR2|(T0X12)):\
        (__MODE__^0x03)==0?(AUXR2=AUXR2|(T0X12)):\
        (__MODE__^0x04)==0?(AUXR2=AUXR2&(~T0X12)):\
        (__MODE__^0x05)==0?(AUXR2=AUXR2&(~T0X12)):\
        (__MODE__^0x06)==0?(AUXR2=AUXR2|(T0X12)):\
        (__MODE__^0x07)==0?(AUXR2=AUXR2|(T0X12)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Clock Source
* @details      Set T0C_T
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T0_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_ILRCO
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
*  @arg\b       TIMER0_CLOCK_SOURCE_WDTPS
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_192
*  @arg\b       TIMER0_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0C_T(TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_SetT0C_T(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T0C_T)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T0C_T)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T0C_T)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T0C_T)):\
        (__MODE__^0x04)==0?(TMOD=TMOD&(~T0C_T)):\
        (__MODE__^0x05)==0?(TMOD=TMOD|(T0C_T)):\
        (__MODE__^0x06)==0?(TMOD=TMOD&(~T0C_T)):\
        (__MODE__^0x07)==0?(TMOD=TMOD|(T0C_T)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Clock Source
* @details      Set  T1X12 T1C_T
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER1_CLOCK_SOURCE_T1_PIN
*  @arg\b       TIMER1_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER1_CLOCK_SOURCE_SYSCLK_DIV_48
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Clock_Source_Select(TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER1_Clock_Source_Select
#define TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER1_CLOCK_SOURCE_T1_PIN        0x01
#define TIMER1_CLOCK_SOURCE_SYSCLK        0x02
#define TIMER1_CLOCK_SOURCE_SYSCLK_DIV_48 0x03
///@endcond
#define __DRV_TIMER1_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER1_SetT1X12(__MODE__);\
        __DRV_TIMER1_SetT1C_T(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Clock Source
* @details      Set T1X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T1_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1X12(TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetT1X12(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(AUXR2=AUXR2&(~T1X12)):\
        (__MODE__^0x01)==0?(AUXR2=AUXR2&(~T1X12)):\
        (__MODE__^0x02)==0?(AUXR2=AUXR2|(T1X12)):\
        (__MODE__^0x03)==0?(AUXR2=AUXR2|(T1X12)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Clock Source
* @details      Set T1C_T
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER0_CLOCK_SOURCE_T1_PIN
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER0_CLOCK_SOURCE_SYSCLK_DIV_48
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1C_T(TIMER1_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetT1C_T(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T1C_T)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T1C_T)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T1C_T)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T1C_T)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Gating Source
* @details      Set T0G1 T0GATE
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_GATING_SOURCE_DISABLE
*  @arg\b       TIMER0_GATING_SOURCE_INT0ET
*  @arg\b       TIMER0_GATING_SOURCE_TF2
*  @arg\b       TIMER0_GATING_SOURCE_KBI
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_Gating_Source_Select(TIMER0_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
    ///@cond __DRV_TIMER0_Gating_Source_Select
#define TIMER0_GATING_SOURCE_DISABLE 0x00
#define TIMER0_GATING_SOURCE_INT0ET    0x01
#define TIMER0_GATING_SOURCE_TF2     0x02
#define TIMER0_GATING_SOURCE_KBI     0x03
    ///@endcond
#define __DRV_TIMER0_Gating_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER0_SetT0G1(__MODE__);\
        __DRV_TIMER0_SetT0GATE(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Gating Source
* @details      Set T0G1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_GATING_SOURCE_DISABLE
*  @arg\b       TIMER0_GATING_SOURCE_INT0ET
*  @arg\b       TIMER0_GATING_SOURCE_TF2
*  @arg\b       TIMER0_GATING_SOURCE_KBI
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0G1(TIMER0_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_SetT0G1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(6);\
        ((__MODE__^0x00)==0?(AUXR9=AUXR9&(~T0G1)):\
        (__MODE__^0x01)==0?(AUXR9=AUXR9&(~T0G1)):\
        (__MODE__^0x02)==0?(AUXR9=AUXR9|(T0G1)):\
        (__MODE__^0x03)==0?(AUXR9=AUXR9|(T0G1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER0 Gating Source
* @details      Set T0GATE
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER0_GATING_SOURCE_DISABLE
*  @arg\b       TIMER0_GATING_SOURCE_INT0ET
*  @arg\b       TIMER0_GATING_SOURCE_TF2
*  @arg\b       TIMER0_GATING_SOURCE_KBI
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0GATE(TIMER0_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_SetT0GATE(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T0GATE)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T0GATE)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T0GATE)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T0GATE)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Gating Source
* @details      Set T1G1 T1GATE
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_GATING_SOURCE_DISABLE
*  @arg\b       TIMER1_GATING_SOURCE_INT1ET
*  @arg\b       TIMER1_GATING_SOURCE_TF3
*  @arg\b       TIMER1_GATING_SOURCE_TI1
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Gating_Source_Select(TIMER1_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
    ///@cond __DRV_TIMER1_Gating_Source_Select
#define TIMER1_GATING_SOURCE_DISABLE 0x00
#define TIMER1_GATING_SOURCE_INT1ET    0x01
#define TIMER1_GATING_SOURCE_TF3     0x02
#define TIMER1_GATING_SOURCE_TI1     0x03
    ///@endcond
#define __DRV_TIMER1_Gating_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER1_SetT1G1(__MODE__);\
        __DRV_TIMER1_SetT1GATE(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Gating Source
* @details      Set T1G1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_GATING_SOURCE_DISABLE
*  @arg\b       TIMER1_GATING_SOURCE_INT1ET
*  @arg\b       TIMER1_GATING_SOURCE_TF3
*  @arg\b       TIMER1_GATING_SOURCE_TI1
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1G1(TIMER1_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetT1G1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(6);\
        ((__MODE__^0x00)==0?(AUXR9=AUXR9&(~T1G1)):\
        (__MODE__^0x01)==0?(AUXR9=AUXR9&(~T1G1)):\
        (__MODE__^0x02)==0?(AUXR9=AUXR9|(T1G1)):\
        (__MODE__^0x03)==0?(AUXR9=AUXR9|(T1G1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER1 Gating Source
* @details      Set T1GATE
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER1_GATING_SOURCE_DISABLE
*  @arg\b       TIMER1_GATING_SOURCE_INT1ET
*  @arg\b       TIMER1_GATING_SOURCE_TF3
*  @arg\b       TIMER1_GATING_SOURCE_TI1
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1GATE(TIMER1_GATING_SOURCE_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetT1GATE(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(TMOD=TMOD&(~T1GATE)):\
        (__MODE__^0x01)==0?(TMOD=TMOD|(T1GATE)):\
        (__MODE__^0x02)==0?(TMOD=TMOD&(~T1GATE)):\
        (__MODE__^0x03)==0?(TMOD=TMOD|(T1GATE)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        TIMER0 High Byte Register
* @details      Set TH0
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetHighByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetHighByte(__RELOAD__)\
    TH0=__RELOAD__
/**
*******************************************************************************
* @brief        TIMER0 Low Byte Register
* @details      Set TL0
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetLowByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetLowByte(__RELOAD__)\
    TL0=__RELOAD__
/**
*******************************************************************************
* @brief        TIMER0 16 Bit Timer/Counter Reload
* @details      Set TH0 TL0
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_Set16BitInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_Set16BitInterval(__RELOAD__)\
    MWT(\
        TH0=HIBYTE(__RELOAD__);\
        TL0=LOBYTE(__RELOAD__)&0xff;\
    ;)
/**
*******************************************************************************
* @brief        TIMER0 8Bit Timer/Counter Auto Reload
* @details      Set TL0 TH0
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_Set8BitIntervalAutoReload(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER0_Set8BitIntervalAutoReload(__RELOAD__)\
    TL0=TH0=__RELOAD__

/**
*******************************************************************************
* @brief        TIMER1 High Byte Register
* @details      Set TH1
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetHighByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_SetHighByte(__RELOAD__)\
    TH1=__RELOAD__
/**
*******************************************************************************
* @brief        TIMER1 Low Byte Register
* @details      Set TL1
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetLowByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_SetLowByte(__RELOAD__)\
    TL1=__RELOAD__

/**
*******************************************************************************
* @brief        TIMER1 16 Bit Timer/Counter Reload
* @details      Set TH1 TL1
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Set16BitInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_Set16BitInterval(__RELOAD__)\
    MWT(\
        TH1=HIBYTE(__RELOAD__);\
        TL1=LOBYTE(__RELOAD__)&0xff;\
    ;)
/**
*******************************************************************************
* @brief        TIMER1 8Bit Timer/Counter Auto Reload
* @details      Set TL1 TH1
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_Set8BitIntervalAutoReload(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER1_Set8BitIntervalAutoReload(__RELOAD__)\
    TL1=TH1=__RELOAD__

/**
*******************************************************************************
* @brief        TIMER0 T0CKO
* @details      Set T0CKOE
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_T0CKO_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_T0CKO_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(AUXR2=AUXR2&(~T0CKOE)):(AUXR2=AUXR2|(T0CKOE));\
    ;)
/**
*******************************************************************************
* @brief        TIMER1 T1CKO
* @details      Set T1CKOE
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_T1CKO_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_T1CKO_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(AUXR2=AUXR2&(~T1CKOE)):(AUXR2=AUXR2|(T1CKOE));\
    ;)
/**
*******************************************************************************
* @brief        TIMER0 T0CKO Pin MUX
* @details      Set T0PS1 T0PS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER0_T0CKO_P34
*  @arg\b       TIMER0_T0CKO_P44
*  @arg\b       TIMER0_T0CKO_P22
*  @arg\b       TIMER0_T0CKO_P17
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_T0CKO_PinMux_Select(TIMER0_T0CKO_P34)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER0_T0CKO_PinMux_Select
#define TIMER0_T0CKO_P34 0x00
#define TIMER0_T0CKO_P44 0x01
#define TIMER0_T0CKO_P22 0x02
#define TIMER0_T0CKO_P17 0x03
///@endcond
#define __DRV_TIMER0_T0CKO_PinMux_Select(__SELECT__)\
    MWT(\
        __DRV_TIMER0_SetT0PS1(__SELECT__);\
        __DRV_TIMER0_SetT0PS0(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        TIMER0 T0CKO Pin MUX
* @details      Set T0PS1
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER0_T0CKO_P34
*  @arg\b       TIMER0_T0CKO_P44
*  @arg\b       TIMER0_T0CKO_P22
*  @arg\b       TIMER0_T0CKO_P17
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0PS1(TIMER0_T0CKO_P34)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetT0PS1(__SELECT__)\
    MWT(\
        (__SELECT__^0x00)==0?(AUXR3=AUXR3&(~T0PS1)):\
        (__SELECT__^0x01)==0?(AUXR3=AUXR3&(~T0PS1)):\
        (__SELECT__^0x02)==0?(AUXR3=AUXR3|(T0PS1)):\
        (__SELECT__^0x03)==0?(AUXR3=AUXR3|(T0PS1)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        TIMER0 T0CKO Pin MUX
* @details      Set T0PS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER0_T0CKO_P34
*  @arg\b       TIMER0_T0CKO_P44
*  @arg\b       TIMER0_T0CKO_P22
*  @arg\b       TIMER0_T0CKO_P17
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER0_SetT0PS0(TIMER0_T0CKO_P34)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER0_SetT0PS0(__SELECT__)\
    MWT(\
        (__SELECT__^0x00)==0?(AUXR3=AUXR3&(~T0PS0)):\
        (__SELECT__^0x01)==0?(AUXR3=AUXR3|(T0PS0)):\
        (__SELECT__^0x02)==0?(AUXR3=AUXR3&(~T0PS0)):\
        (__SELECT__^0x03)==0?(AUXR3=AUXR3|(T0PS0)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        TIMER1 T1CKO Pin MUX
* @details      Set T1PS1 T1PS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER1_T1CKO_P35
*  @arg\b       TIMER1_T1CKO_P45
*  @arg\b       TIMER1_T1CKO_P17
*  @arg\b       TIMER1_T1CKO_P33
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_T1CKO_PinMux_Select(TIMER1_T1CKO_P35)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER1_T1CKO_PinMux_Select
#define TIMER1_T1CKO_P35 0x00
#define TIMER1_T1CKO_P45 0x01
#define TIMER1_T1CKO_P17 0x02
#define TIMER1_T1CKO_P33 0x03
///@endcond
#define __DRV_TIMER1_T1CKO_PinMux_Select(__SELECT__)\
    MWT(\
        __DRV_TIMER1_SetT1PS1(__SELECT__);\
        __DRV_TIMER1_SetT1PS0(__SELECT__);\
    ;)
/**
*******************************************************************************
* @brief        TIMER1 T1CKO Pin MUX
* @details      Set T1PS1
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER1_T1CKO_P35
*  @arg\b       TIMER1_T1CKO_P45
*  @arg\b       TIMER1_T1CKO_P17
*  @arg\b       TIMER1_T1CKO_P33
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1PS1(TIMER1_T1CKO_P35)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_SetT1PS1(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__SELECT__^0x00)==0?(AUXR4=AUXR4&(~T1PS1)):\
        (__SELECT__^0x01)==0?(AUXR4=AUXR4&(~T1PS1)):\
        (__SELECT__^0x02)==0?(AUXR4=AUXR4|(T1PS1)):\
        (__SELECT__^0x03)==0?(AUXR4=AUXR4|(T1PS1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER1 T1CKO Pin MUX
* @details      Set T1PS0
* @param[in]    \_\_SELECT\_\_ :
*  @arg\b       TIMER1_T1CKO_P35
*  @arg\b       TIMER1_T1CKO_P45
*  @arg\b       TIMER1_T1CKO_P17
*  @arg\b       TIMER1_T1CKO_P33
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER1_SetT1PS0(TIMER1_T1CKO_P35)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER1_SetT1PS0(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__SELECT__^0x00)==0?(AUXR4=AUXR4&(~T1PS0)):\
        (__SELECT__^0x01)==0?(AUXR4=AUXR4|(T1PS0)):\
        (__SELECT__^0x02)==0?(AUXR4=AUXR4&(~T1PS0)):\
        (__SELECT__^0x03)==0?(AUXR4=AUXR4|(T1PS0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TL2 Run Control
* @details      Set TR2L
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TL2_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TL2_Run_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(T2MOD=T2MOD&(~TR2L)):(T2MOD=T2MOD|(TR2L));\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TR2L Clear Control (Mode0~Mode3)
* @details      Set TR2LC
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TR2LC_Clear_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TR2L_Clear_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(T2MOD=T2MOD&(~TR2LC)):(T2MOD=T2MOD|(TR2LC));\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 T2CKO Invert Control (Mode4 8 Bit Pwm )
* @details      Set TR2LC
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_T2CKO_Invert_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_T2CKO_Invert_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(T2MOD=T2MOD&(~TR2LC)):(T2MOD=T2MOD|(TR2LC));\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Operating Mode
* @details      Set T2SPL T2MS1 CP/RL2 T2MS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Mode_Select(TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_Mode_Select
#define TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT           0x00
#define TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT          0x01
#define TIMER2_MODE2_16BIT_CAPTURE                                     0x02
#define TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO                      0x03
#define TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT   0x04
#define TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT 0x05
#define TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE                            0x06
#define TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO             0x07
#define TIMER2_SPLIT_MODE4_8BIT_PWM                                    0x08
///@endcond
#define __DRV_TIMER2_Mode_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_SetT2SPL(__MODE__);\
        __DRV_TIMER2_SetT2MS1(__MODE__);\
        __DRV_TIMER2_SetCP_RL2(__MODE__);\
        __DRV_TIMER2_SetT2MS0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Operating Mode
* @details      Set T2SPL
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2SPL(TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetT2SPL(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(T2MOD=T2MOD&(~T2SPL)):\
        (__MODE__^0x01)==0?(T2MOD=T2MOD&(~T2SPL)):\
        (__MODE__^0x02)==0?(T2MOD=T2MOD&(~T2SPL)):\
        (__MODE__^0x03)==0?(T2MOD=T2MOD&(~T2SPL)):\
        (__MODE__^0x04)==0?(T2MOD=T2MOD|(T2SPL)):\
        (__MODE__^0x05)==0?(T2MOD=T2MOD|(T2SPL)):\
        (__MODE__^0x06)==0?(T2MOD=T2MOD|(T2SPL)):\
        (__MODE__^0x07)==0?(T2MOD=T2MOD|(T2SPL)):\
        (__MODE__^0x08)==0?(T2MOD=T2MOD|(T2SPL)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Operating Mode
* @details      Set T2MS1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2MS1(TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetT2MS1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1&(~T2MS1)):\
        (__MODE__^0x08)==0?(T2MOD1=T2MOD1|(T2MS1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Operating Mode
* @details      Set CP/RL2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetCP_RL2(TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetCP_RL2(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(CP_RL2=0):\
        (__MODE__^0x01)==0?(CP_RL2=0):\
        (__MODE__^0x02)==0?(CP_RL2=1):\
        (__MODE__^0x03)==0?(CP_RL2=1):\
        (__MODE__^0x04)==0?(CP_RL2=0):\
        (__MODE__^0x05)==0?(CP_RL2=0):\
        (__MODE__^0x06)==0?(CP_RL2=1):\
        (__MODE__^0x07)==0?(CP_RL2=1):\
        (__MODE__^0x08)==0?(CP_RL2=0):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Operating Mode
* @details      Set T2MS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER2_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER2_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER2_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER2_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2MS0(TIMER2_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetT2MS0(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(T2MOD=T2MOD&(~T2MS0)):\
        (__MODE__^0x01)==0?(T2MOD=T2MOD|(T2MS0)):\
        (__MODE__^0x02)==0?(T2MOD=T2MOD&(~T2MS0)):\
        (__MODE__^0x03)==0?(T2MOD=T2MOD|(T2MS0)):\
        (__MODE__^0x04)==0?(T2MOD=T2MOD&(~T2MS0)):\
        (__MODE__^0x05)==0?(T2MOD=T2MOD|(T2MS0)):\
        (__MODE__^0x06)==0?(T2MOD=T2MOD&(~T2MS0)):\
        (__MODE__^0x07)==0?(T2MOD=T2MOD|(T2MS0)):\
        (__MODE__^0x08)==0?(T2MOD=T2MOD&(~T2MS0)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Clock Out Control
* @details      Set T2OE
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Clock_Out_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_Clock_Out_Cmd(__STATE__)\
    MWT(\
        __STATE__==0?(T2MOD=T2MOD&(~T2OE)):(T2MOD=T2MOD|(T2OE));\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Clock Source
* @details      Set T2CKS T2X12 C/T2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER2_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Clock_Source_Select(TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_Clock_Source_Select
#define TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER2_CLOCK_SOURCE_T2_PIN        0x01
#define TIMER2_CLOCK_SOURCE_SYSCLK        0x02
#define TIMER2_CLOCK_SOURCE_INT0ET        0x03
#define TIMER2_CLOCK_SOURCE_S0TOF         0x04
#define TIMER2_CLOCK_SOURCE_T0OF          0x05
#define TIMER2_CLOCK_SOURCE_RESERVED      0x06
#define TIMER2_CLOCK_SOURCE_KBIET         0x07
///@endcond
#define __DRV_TIMER2_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_SetT2CKS(__MODE__);\
        __DRV_TIMER2_SetT2X12(__MODE__);\
        __DRV_TIMER2_SetC_T2(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Clock Source
* @details      Set T2CKS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER2_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2CKS(TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetT2CKS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1|(T2CKS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Clock Source
* @details      Set T2X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER2_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2X12(TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetT2X12(__MODE__)\
MWT(\
    (__MODE__^0x00)==0?(T2MOD=T2MOD&(~T2X12)):\
    (__MODE__^0x01)==0?(T2MOD=T2MOD&(~T2X12)):\
    (__MODE__^0x02)==0?(T2MOD=T2MOD|(T2X12)):\
    (__MODE__^0x03)==0?(T2MOD=T2MOD|(T2X12)):\
    (__MODE__^0x04)==0?(T2MOD=T2MOD&(~T2X12)):\
    (__MODE__^0x05)==0?(T2MOD=T2MOD&(~T2X12)):\
    (__MODE__^0x06)==0?(T2MOD=T2MOD|(T2X12)):\
    (__MODE__^0x07)==0?(T2MOD=T2MOD|(T2X12)):_nop_();\
;)
/**
*****************************************************************************
* @brief        TIMER2 Clock Source
* @details      Set C/T2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER2_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetC_T2(TIMER2_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetC_T2(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(C_T2=0):\
        (__MODE__^0x01)==0?(C_T2=1):\
        (__MODE__^0x02)==0?(C_T2=0):\
        (__MODE__^0x03)==0?(C_T2=1):\
        (__MODE__^0x04)==0?(C_T2=0):\
        (__MODE__^0x05)==0?(C_T2=1):\
        (__MODE__^0x06)==0?(C_T2=0):\
        (__MODE__^0x07)==0?(C_T2=1):_nop_();\
    ;)

/**
*****************************************************************************
* @brief        TIMER2 TH2 Clock Source in Split Mode
* @details      Set T2CKS T2X12 C/T2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_TL2OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TH2_Clock_Source_Select(TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_TH2_Clock_Source_Select
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T2_PIN        0x01
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK        0x02
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_TL2OF        0x03
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_S0TOF         0x04
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T0OF          0x05
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_RESERVED      0x06
#define TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_KBIET         0x07
///@endcond
#define __DRV_TIMER2_TH2_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_TH2_SetT2CKS(__MODE__);\
        __DRV_TIMER2_TH2_SetT2X12(__MODE__);\
        __DRV_TIMER2_TH2_SetC_T2(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TH2 Clock Source in Split Mode
* @details      Set T2CKS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_TL2OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TH2_SetT2CKS(TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TH2_SetT2CKS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1&(~T2CKS)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1|(T2CKS)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1|(T2CKS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TH2 Clock Source in Split Mode
* @details      Set T2X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_TL2OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TH2_SetT2X12(TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_TH2_SetT2X12(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(T2MOD=T2MOD&(~T2X12)):\
        (__MODE__^0x01)==0?(T2MOD=T2MOD&(~T2X12)):\
        (__MODE__^0x02)==0?(T2MOD=T2MOD|(T2X12)):\
        (__MODE__^0x03)==0?(T2MOD=T2MOD|(T2X12)):\
        (__MODE__^0x04)==0?(T2MOD=T2MOD&(~T2X12)):\
        (__MODE__^0x05)==0?(T2MOD=T2MOD&(~T2X12)):\
        (__MODE__^0x06)==0?(T2MOD=T2MOD|(T2X12)):\
        (__MODE__^0x07)==0?(T2MOD=T2MOD|(T2X12)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TH2 Clock Source in Split Mode
* @details      Set C/T2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T2_PIN
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_TL2OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_KBIET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TH2_SetC_T2(TIMER2_TH2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TH2_SetC_T2(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(C_T2=0):\
        (__MODE__^0x01)==0?(C_T2=1):\
        (__MODE__^0x02)==0?(C_T2=0):\
        (__MODE__^0x03)==0?(C_T2=1):\
        (__MODE__^0x04)==0?(C_T2=0):\
        (__MODE__^0x05)==0?(C_T2=1):\
        (__MODE__^0x06)==0?(C_T2=0):\
        (__MODE__^0x07)==0?(C_T2=1):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TL2 Clock Source in Split Mode
* @details      Set TL2CS TL2X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_INT0ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TL2_Clock_Source_Select(TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_TL2_Clock_Source_Select
#define TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK        0x01
#define TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_S0TOF         0x02
#define TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_INT0ET        0x03
///@endcond
#define __DRV_TIMER2_TL2_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_TL2_SetTL2CS(__MODE__);\
        __DRV_TIMER2_TL2_SetTL2X12(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TL2 Clock Source in Split Mode
* @details      Set TL2CS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_INT0ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TL2_SetTL2CS(TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TL2_SetTL2CS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~TL2CS)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~TL2CS)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1|(TL2CS)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1|(TL2CS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TL2 Clock Source in Split Mode
* @details      Set TL2X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_S0TOF
*  @arg\b       TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_INT0ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TL2_SetTL2X12(TIMER2_TL2_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_TL2_SetTL2X12(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(T2MOD=T2MOD&(~TL2X12)):\
        (__MODE__^0x01)==0?(T2MOD=T2MOD|(TL2X12)):\
        (__MODE__^0x02)==0?(T2MOD=T2MOD&(~TL2X12)):\
        (__MODE__^0x03)==0?(T2MOD=T2MOD|(TL2X12)):_nop_();\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TF2L/TL2IE,RCLK/TCLK Access Function
* @details      Set TL2IS
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_Access_TF2L_TL2IE_Function_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T2MOD1=T2MOD1&(~TL2IS)):(T2MOD1=T2MOD1|(TL2IS));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Capture Source
* @details      Set CP2S2 CP2S1 CP2S0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CAPTURE_SOURCE_T2EX_PIN
*  @arg\b       TIMER2_CAPTURE_SOURCE_RXD0
*  @arg\b       TIMER2_CAPTURE_SOURCE_P60
*  @arg\b       TIMER2_CAPTURE_SOURCE_INT2ET
*  @arg\b       TIMER2_CAPTURE_SOURCE_ILRCO
*  @arg\b       TIMER2_CAPTURE_SOURCE_RESERVED
*  @arg\b       TIMER2_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER2_CAPTURE_SOURCE_TWI0_SCL
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Capture_Source_Select(TIMER2_CAPTURE_SOURCE_T2EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_Capture_Source_Select
#define TIMER2_CAPTURE_SOURCE_T2EX_PIN  0x00
#define TIMER2_CAPTURE_SOURCE_RXD0      0x01
#define TIMER2_CAPTURE_SOURCE_P60       0x02
#define TIMER2_CAPTURE_SOURCE_INT2ET    0x03
#define TIMER2_CAPTURE_SOURCE_ILRCO 0x04
#define TIMER2_CAPTURE_SOURCE_RESERVED 0x05
#define TIMER2_CAPTURE_SOURCE_KBIET     0x06
#define TIMER2_CAPTURE_SOURCE_TWI0_SCL  0x07
///@endcond
#define __DRV_TIMER2_Capture_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_SetCP2S2(__MODE__);\
        __DRV_TIMER2_SetCP2S1(__MODE__);\
        __DRV_TIMER2_SetCP2S0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Capture Source
* @details      Set CP2S2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b      TIMER2_CAPTURE_SOURCE_T2EX_PIN
*  @arg\b      TIMER2_CAPTURE_SOURCE_RXD0
*  @arg\b      TIMER2_CAPTURE_SOURCE_P60
*  @arg\b      TIMER2_CAPTURE_SOURCE_INT2ET
*  @arg\b      TIMER2_CAPTURE_SOURCE_ILRCO
*  @arg\b      TIMER2_CAPTURE_SOURCE_RESERVED
*  @arg\b      TIMER2_CAPTURE_SOURCE_KBIET
*  @arg\b      TIMER2_CAPTURE_SOURCE_TWI0_SCL
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetCP2S2(TIMER2_CAPTURE_SOURCE_T2EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetCP2S2(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~CP2S2)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~CP2S2)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1&(~CP2S2)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1&(~CP2S2)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1|(CP2S2)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1|(CP2S2)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1|(CP2S2)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1|(CP2S2)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Capture Source
* @details      Set CP2S1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CAPTURE_SOURCE_T2EX_PIN
*  @arg\b       TIMER2_CAPTURE_SOURCE_RXD0
*  @arg\b       TIMER2_CAPTURE_SOURCE_P60
*  @arg\b       TIMER2_CAPTURE_SOURCE_INT2ET
*  @arg\b       TIMER2_CAPTURE_SOURCE_ILRCO
*  @arg\b       TIMER2_CAPTURE_SOURCE_RESERVED
*  @arg\b       TIMER2_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER2_CAPTURE_SOURCE_TWI0_SCL
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetCP2S1(TIMER2_CAPTURE_SOURCE_T2EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetCP2S1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~CP2S1)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1&(~CP2S1)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1|(CP2S1)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1|(CP2S1)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1&(~CP2S1)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1&(~CP2S1)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1|(CP2S1)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1|(CP2S1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Capture Source
* @details      Set CP2S0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_CAPTURE_SOURCE_T2EX_PIN
*  @arg\b       TIMER2_CAPTURE_SOURCE_RXD0
*  @arg\b       TIMER2_CAPTURE_SOURCE_P60
*  @arg\b       TIMER2_CAPTURE_SOURCE_INT2ET
*  @arg\b       TIMER2_CAPTURE_SOURCE_ILRCO
*  @arg\b       TIMER2_CAPTURE_SOURCE_RESERVED
*  @arg\b       TIMER2_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER2_CAPTURE_SOURCE_TWI0_SCL
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetCP2S0(TIMER2_CAPTURE_SOURCE_T2EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetCP2S0(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T2MOD1=T2MOD1&(~CP2S0)):\
        (__MODE__^0x01)==0?(T2MOD1=T2MOD1|(CP2S0)):\
        (__MODE__^0x02)==0?(T2MOD1=T2MOD1&(~CP2S0)):\
        (__MODE__^0x03)==0?(T2MOD1=T2MOD1|(CP2S0)):\
        (__MODE__^0x04)==0?(T2MOD1=T2MOD1&(~CP2S0)):\
        (__MODE__^0x05)==0?(T2MOD1=T2MOD1|(CP2S0)):\
        (__MODE__^0x06)==0?(T2MOD1=T2MOD1&(~CP2S0)):\
        (__MODE__^0x07)==0?(T2MOD1=T2MOD1|(CP2S0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Pin Mux
* @details      Set T2PS1 T2PS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P30_T2EX_P31
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P60_T2EX_P35
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P45_T2EX_P44
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_PinMUX_Select(TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_PinMUX_Select
#define TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11 0x00
#define TIMER2_PINMUX_T2_T2CKO_P30_T2EX_P31 0x01
#define TIMER2_PINMUX_T2_T2CKO_P60_T2EX_P35 0x02
#define TIMER2_PINMUX_T2_T2CKO_P45_T2EX_P44 0x03
///@endcond
#define __DRV_TIMER2_PinMUX_Select(__MODE__)\
    MWT(\
        __DRV_TIMER2_SetT2PS1(__MODE__);\
        __DRV_TIMER2_SetT2PS0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Pin Mux
* @details      Set T2PS1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P30_T2EX_P31
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P60_T2EX_P35
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P45_T2EX_P44
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2PS1(TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetT2PS1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(AUXR4=AUXR4&(~T2PS1)):\
        (__MODE__^0x01)==0?(AUXR4=AUXR4&(~T2PS1)):\
        (__MODE__^0x02)==0?(AUXR4=AUXR4|(T2PS1)):\
        (__MODE__^0x03)==0?(AUXR4=AUXR4|(T2PS1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Pin Mux
* @details      Set T2PS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P30_T2EX_P31
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P60_T2EX_P35
*  @arg\b       TIMER2_PINMUX_T2_T2CKO_P45_T2EX_P44
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2PS0(TIMER2_PINMUX_T2_T2CKO_P10_T2EX_P11)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetT2PS0(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(AUXR4=AUXR4&(~T2PS0)):\
        (__MODE__^0x01)==0?(AUXR4=AUXR4|(T2PS0)):\
        (__MODE__^0x02)==0?(AUXR4=AUXR4&(~T2PS0)):\
        (__MODE__^0x03)==0?(AUXR4=AUXR4|(T2PS0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 TF2 Interrupt Ignored
* @details      Set TF2IG
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_TF2_Interrupt_Ignored_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T2MOD1=T2MOD1&(~TF2IG)):(T2MOD1=T2MOD1|(TF2IG));\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER2 Capture Source Dectect
* @details      Set EXEN2 T2EXH
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Capture_Source_Dectect(IGNORED)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER2_Capture_Source_Dectect
#define IGNORED      0x00
#define FALLING_EDGE 0x01
#define RISING_EDGE  0x02
#define DUAL_EDGE    0x03
///@endcond

#define __DRV_TIMER2_Capture_Source_Dectect(__MODE__)\
    MWT(\
        __DRV_TIMER2_SetEXEN2(__MODE__);\
        __DRV_TIMER2_SetT2EXH(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER2 Capture Source Dectect
* @details      Set EXEN2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetEXEN2(IGNORED)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetEXEN2(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(EXEN2=0):\
        (__MODE__^0x01)==0?(EXEN2=0):\
        (__MODE__^0x02)==0?(EXEN2=1):\
        (__MODE__^0x03)==0?(EXEN2=1):_nop_();\
    ;)

/**
*****************************************************************************
* @brief        TIMER2 Capture Source Dectect
* @details      Set T2EXH
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetT2EXH(IGNORED)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetT2EXH(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(T2MOD=T2MOD&(~T2EXH)):\
        (__MODE__^0x01)==0?(T2MOD=T2MOD|(T2EXH)):\
        (__MODE__^0x02)==0?(T2MOD=T2MOD&(~T2EXH)):\
        (__MODE__^0x03)==0?(T2MOD=T2MOD|(T2EXH)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        TIMER2 High Byte Register
* @details      Set TH2
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetHighByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetHighByte(__RELOAD__)\
    TH2=__RELOAD__
/**
*******************************************************************************
* @brief        TIMER2 Low Byte Register
* @details      Set TL2
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetLowByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetLowByte(__RELOAD__)\
    TL2=__RELOAD__

/**
*******************************************************************************
* @brief        TIMER2 16 Bit Timer/Counter Reload
* @details      Set TH2 TL2
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Set16BitInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_Set16BitInterval(__RELOAD__)\
    MWT(\
        TH2=HIBYTE(__RELOAD__);\
        TL2=LOBYTE(__RELOAD__)&0xff;\
    ;)

/**
*******************************************************************************
* @brief        TIMER2 Capture High Byte Register
* @details      Set RCAP2H
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetRCAP2H(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER2_SetRCAP2H(__RELOAD__)\
    RCAP2H=__RELOAD__
/**
*******************************************************************************
* @brief        TIMER2 Capture Low Byte Register
* @details      Set RCAP2L
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_SetRCAP2L(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_SetRCAP2L(__RELOAD__)\
    RCAP2L=__RELOAD__

/**
*******************************************************************************
* @brief        TIMER2 16 Bit Capture Reload
* @details      Set RCAP2H RCAP2L
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER2_Set16BitCaptureInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER2_Set16BCaptureitInterval(__RELOAD__)\
    MWT(\
        RCAP2H=HIBYTE(__RELOAD__);\
        RCAP2L=LOBYTE(__RELOAD__)&0xff;\
    ;)
/**
*******************************************************************************
* @brief        TIMER Globel Enable Control
* @details      Set TREN0
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       TR0E
*  @arg\b       TR1E
*  @arg\b       TR2E
*  @arg\b       TR3E
*  @arg\b       TR2LE
*  @arg\b       TR3LE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER_Globel_Enable_Cmd(__STATE__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER_Globel_Enable_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        TREN0=TREN0|(__STATE__);\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER Globel Reload Control
* @details      Set TRLC0
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       T0RLC
*  @arg\b       T1RLC
*  @arg\b       T2RLC
*  @arg\b       T3RLC
*  @arg\b       TL2RLC
*  @arg\b       TL3RLC
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER_Globel_Cmd(__STATE__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER_Globel_Reload_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        TRLC0=TRLC0|(__STATE__);\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER Globel Stop Control
* @details      Set TSPC0
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       T0SC
*  @arg\b       T1SC
*  @arg\b       T2SC
*  @arg\b       T3SC
*  @arg\b       TL2SC
*  @arg\b       TL3SC
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER_Globel_Stop_Cmd(__STATE__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER_Globel_Stop_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        TSPC0=TSPC0|(__STATE__);\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TL3 Run Control
* @details      Set TR3L
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TL3_Run_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TL3_Run_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T3MOD=T3MOD&(~TR3L)):(T3MOD=T3MOD|(TR3L));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TR3L Clear Control (Mode0~Mode3)
* @details      Set TR3LC
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TR3LC_Clear_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TR3L_Clear_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T3MOD=T3MOD&(~TR3LC)):(T3MOD=T3MOD|(TR3LC));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 T3CKO Invert Control (Mode4 8 Bit Pwm )
* @details      Set TR3LC
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_T3CKO_Invert_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_T3CKO_Invert_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T3MOD=T3MOD&(~TR3LC)):(T3MOD=T3MOD|(TR3LC));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Operating Mode
* @details      Set T3SPL T3MS1 CP/RL3 T3MS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Mode_Select(TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER3_Mode_Select
#define TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT           0x00
#define TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT          0x01
#define TIMER3_MODE2_16BIT_CAPTURE                                     0x02
#define TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO                      0x03
#define TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT   0x04
#define TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT 0x05
#define TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE                            0x06
#define TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO             0x07
#define TIMER3_SPLIT_MODE4_8BIT_PWM                                    0x08
///@endcond
#define __DRV_TIMER3_Mode_Select(__MODE__)\
    MWT(\
        __DRV_TIMER3_SetT3SPL(__MODE__);\
        __DRV_TIMER3_SetT3MS1(__MODE__);\
        __DRV_TIMER3_SetCP_RL3(__MODE__);\
        __DRV_TIMER3_SetT3MS0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Operating Mode
* @details      Set T3SPL
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3SPL(TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetT3SPL(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~T3SPL)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD&(~T3SPL)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD&(~T3SPL)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD&(~T3SPL)):\
        (__MODE__^0x04)==0?(T3MOD=T3MOD|(T3SPL)):\
        (__MODE__^0x05)==0?(T3MOD=T3MOD|(T3SPL)):\
        (__MODE__^0x06)==0?(T3MOD=T3MOD|(T3SPL)):\
        (__MODE__^0x07)==0?(T3MOD=T3MOD|(T3SPL)):\
        (__MODE__^0x08)==0?(T3MOD=T3MOD|(T3SPL)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Operating Mode
* @details      Set T3MS1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3MS1(TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_SetT3MS1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1&(~T3MS1)):\
        (__MODE__^0x08)==0?(T3MOD1=T3MOD1|(T3MS1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Operating Mode
* @details      Set CP/RL3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetCP_RL3(TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetCP_RL3(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(CP_RL3=0):\
        (__MODE__^0x01)==0?(CP_RL3=0):\
        (__MODE__^0x02)==0?(CP_RL3=1):\
        (__MODE__^0x03)==0?(CP_RL3=1):\
        (__MODE__^0x04)==0?(CP_RL3=0):\
        (__MODE__^0x05)==0?(CP_RL3=0):\
        (__MODE__^0x06)==0?(CP_RL3=1):\
        (__MODE__^0x07)==0?(CP_RL3=1):\
        (__MODE__^0x08)==0?(CP_RL3=0):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Operating Mode
* @details      Set T3MS0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE1_16BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_MODE2_16BIT_CAPTURE
*  @arg\b       TIMER3_MODE3_16BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE0_TWO_8BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE1_TWO_8BIT_AUTORELOAD_WITH_EXTERNAL_INTERRUPT
*  @arg\b       TIMER3_SPLIT_MODE2_TWO_8BIT_CAPTURE
*  @arg\b       TIMER3_SPLIT_MODE3_TWO_8BIT_CAPTURE_WITH_AUTO_ZERO
*  @arg\b       TIMER3_SPLIT_MODE4_8BIT_PWM
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3MS0(TIMER3_MODE0_16BIT_AUTORELOAD_AND_EXTERNAL_INTERRUPT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_SetT3MS0(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~T3MS0)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD|(T3MS0)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD&(~T3MS0)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD|(T3MS0)):\
        (__MODE__^0x04)==0?(T3MOD=T3MOD&(~T3MS0)):\
        (__MODE__^0x05)==0?(T3MOD=T3MOD|(T3MS0)):\
        (__MODE__^0x06)==0?(T3MOD=T3MOD&(~T3MS0)):\
        (__MODE__^0x07)==0?(T3MOD=T3MOD|(T3MS0)):\
        (__MODE__^0x08)==0?(T3MOD=T3MOD&(~T3MS0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Clock Out Control
* @details      Set T3OE
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         Timer3 Pin Configuration T3/T3CKO P33 T3EX P34
* @par          Example
* @code
                __DRV_TIMER3_Clock_Out_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_Clock_Out_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __STATE__==0?(T3MOD=T3MOD&(~T3OE)):(T3MOD=T3MOD|(T3OE));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Clock Source
* @details      Set T3CKS T3X12 C/T3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER3_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Clock_Source_Select(TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER3_Clock_Source_Select
#define TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER3_CLOCK_SOURCE_T3_PIN        0x01
#define TIMER3_CLOCK_SOURCE_SYSCLK        0x02
#define TIMER3_CLOCK_SOURCE_INT0ET        0x03
#define TIMER3_CLOCK_SOURCE_S1TOF         0x04
#define TIMER3_CLOCK_SOURCE_T0OF          0x05
#define TIMER3_CLOCK_SOURCE_RESERVED      0x06
#define TIMER3_CLOCK_SOURCE_T1OF         0x07
///@endcond
#define __DRV_TIMER3_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER3_SetT3CKS(__MODE__);\
        __DRV_TIMER3_SetT3X12(__MODE__);\
        __DRV_TIMER3_SetC_T3(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Clock Source
* @details      Set T3CKS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER3_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3CKS(TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetT3CKS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1|(T3CKS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Clock Source
* @details      Set T3X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER3_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3X12(TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_SetT3X12(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x04)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x05)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x06)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x07)==0?(T3MOD=T3MOD|(T3X12)):_nop_());\
        __DRV_SFR_PageIndex(0);\
;)
/**
*****************************************************************************
* @brief        TIMER3 Clock Source
* @details      Set C/T3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_CLOCK_SOURCE_INT0ET
*  @arg\b       TIMER3_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetC_T3(TIMER3_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetC_T3(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(C_T3=0):\
        (__MODE__^0x01)==0?(C_T3=1):\
        (__MODE__^0x02)==0?(C_T3=0):\
        (__MODE__^0x03)==0?(C_T3=1):\
        (__MODE__^0x04)==0?(C_T3=0):\
        (__MODE__^0x05)==0?(C_T3=1):\
        (__MODE__^0x06)==0?(C_T3=0):\
        (__MODE__^0x07)==0?(C_T3=1):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 TH3 Clock Source in Split Mode
* @details      Set T3CKS T3X12 C/T3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_TL3OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TH3_Clock_Source_Select(TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER3_TH3_Clock_Source_Select
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T3_PIN        0x01
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK        0x02
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_TL3OF        0x03
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_S1TOF         0x04
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T0OF          0x05
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_RESERVED      0x06
#define TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T1OF         0x07
///@endcond
#define __DRV_TIMER3_TH3_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER3_TH3_SetT3CKS(__MODE__);\
        __DRV_TIMER3_TH3_SetT3X12(__MODE__);\
        __DRV_TIMER3_TH3_SetC_T3(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TH3 Clock Source in Split Mode
* @details      Set T3CKS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_TL3OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TH3_SetT3CKS(TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TH3_SetT3CKS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1&(~T3CKS)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1|(T3CKS)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1|(T3CKS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TH3 Clock Source in Split Mode
* @details      Set T3X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_TL3OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TH3_SetT3X12(TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_TH3_SetT3X12(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x04)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x05)==0?(T3MOD=T3MOD&(~T3X12)):\
        (__MODE__^0x06)==0?(T3MOD=T3MOD|(T3X12)):\
        (__MODE__^0x07)==0?(T3MOD=T3MOD|(T3X12)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TH3 Clock Source in Split Mode
* @details      Set C/T3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T3_PIN
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_TL3OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T0OF
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_RESERVED
*  @arg\b       TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_T1OF
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TH3_SetC_T3(TIMER3_TH3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TH3_SetC_T3(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(C_T3=0):\
        (__MODE__^0x01)==0?(C_T3=1):\
        (__MODE__^0x02)==0?(C_T3=0):\
        (__MODE__^0x03)==0?(C_T3=1):\
        (__MODE__^0x04)==0?(C_T3=0):\
        (__MODE__^0x05)==0?(C_T3=1):\
        (__MODE__^0x06)==0?(C_T3=0):\
        (__MODE__^0x07)==0?(C_T3=1):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TL3 Clock Source in Split Mode
* @details      Set TL3CS TL3X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_INT1ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TL3_Clock_Source_Select(TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER3_TL3_Clock_Source_Select
#define TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12 0x00
#define TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK        0x01
#define TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_S1TOF         0x02
#define TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_INT1ET        0x03
///@endcond
#define __DRV_TIMER3_TL3_Clock_Source_Select(__MODE__)\
    MWT(\
        __DRV_TIMER3_TL3_SetTL3CS(__MODE__);\
        __DRV_TIMER3_TL3_SetTL3X12(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 TL3 Clock Source in Split Mode
* @details      Set TL3CS
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_INT1ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TL3_SetTL3CS(TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TL3_SetTL3CS(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~TL3CS)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~TL3CS)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1|(TL3CS)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1|(TL3CS)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TL3 Clock Source in Split Mode
* @details      Set TL3X12
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_S1TOF
*  @arg\b       TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_INT1ET
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TL3_SetTL3X12(TIMER3_TL3_SPLIT_MODE_CLOCK_SOURCE_SYSCLK_DIV_12)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_TL3_SetTL3X12(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~TL3X12)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD|(TL3X12)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD&(~TL3X12)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD|(TL3X12)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 Capture Source
* @details      Set CP3S2 CP3S1 CP3S0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CAPTURE_SOURCE_T3EX_PIN
*  @arg\b       TIMER3_CAPTURE_SOURCE_INT0ET
*  @arg\b       TIMER3_CAPTURE_SOURCE_P60
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED1
*  @arg\b       TIMER3_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED2
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED3
*  @arg\b       TIMER3_CAPTURE_SOURCE_ILRCO
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Capture_Source_Select(TIMER3_CAPTURE_SOURCE_T3EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_TIMER3_Capture_Source_Select
#define TIMER3_CAPTURE_SOURCE_T3EX_PIN      0x00
#define TIMER3_CAPTURE_SOURCE_INT0ET        0x01
#define TIMER3_CAPTURE_SOURCE_P60           0x02
#define TIMER3_CAPTURE_SOURCE_RESERVED1     0x03
#define TIMER3_CAPTURE_SOURCE_KBIET         0x04
#define TIMER3_CAPTURE_SOURCE_RESERVED2     0x05
#define TIMER3_CAPTURE_SOURCE_RESERVED3     0x06
#define TIMER3_CAPTURE_SOURCE_ILRCO         0x07
///@endcond
#define __DRV_TIMER3_Capture_Source_Select(__MODE__)\
    MWT(\
        __DRV_TH3_SetCP3S2(__MODE__);\
        __DRV_TH3_SetCP3S1(__MODE__);\
        __DRV_TH3_SetCP3S0(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Capture Source
* @details      Set CP3S2
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CAPTURE_SOURCE_T3EX_PIN
*  @arg\b       TIMER3_CAPTURE_SOURCE_INT0ET
*  @arg\b       TIMER3_CAPTURE_SOURCE_P60
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED1
*  @arg\b       TIMER3_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED2
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED3
*  @arg\b       TIMER3_CAPTURE_SOURCE_ILRCO
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetCP3S2(TIMER3_CAPTURE_SOURCE_T3EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TH3_SetCP3S2(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~CP2S2)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~CP2S2)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1&(~CP2S2)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1&(~CP2S2)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1|(CP2S2)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1|(CP2S2)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1|(CP2S2)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1|(CP2S2)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Capture Source
* @details      Set CP3S1
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CAPTURE_SOURCE_T3EX_PIN
*  @arg\b       TIMER3_CAPTURE_SOURCE_INT0ET
*  @arg\b       TIMER3_CAPTURE_SOURCE_P60
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED1
*  @arg\b       TIMER3_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED2
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED3
*  @arg\b       TIMER3_CAPTURE_SOURCE_ILRCO
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetCP3S1(TIMER3_CAPTURE_SOURCE_T3EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TH3_SetCP3S1(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~CP2S1)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1&(~CP2S1)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1|(CP2S1)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1|(CP2S1)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1&(~CP2S1)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1&(~CP2S1)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1|(CP2S1)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1|(CP2S1)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Capture Source
* @details      Set CP3S0
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       TIMER3_CAPTURE_SOURCE_T3EX_PIN
*  @arg\b       TIMER3_CAPTURE_SOURCE_INT0ET
*  @arg\b       TIMER3_CAPTURE_SOURCE_P60
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED1
*  @arg\b       TIMER3_CAPTURE_SOURCE_KBIET
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED2
*  @arg\b       TIMER3_CAPTURE_SOURCE_RESERVED3
*  @arg\b       TIMER3_CAPTURE_SOURCE_ILRCO
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetCP3S0(TIMER3_CAPTURE_SOURCE_T3EX_PIN)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TH3_SetCP3S0(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        ((__MODE__^0x00)==0?(T3MOD1=T3MOD1&(~CP2S0)):\
        (__MODE__^0x01)==0?(T3MOD1=T3MOD1|(CP2S0)):\
        (__MODE__^0x02)==0?(T3MOD1=T3MOD1&(~CP2S0)):\
        (__MODE__^0x03)==0?(T3MOD1=T3MOD1|(CP2S0)):\
        (__MODE__^0x04)==0?(T3MOD1=T3MOD1&(~CP2S0)):\
        (__MODE__^0x05)==0?(T3MOD1=T3MOD1|(CP2S0)):\
        (__MODE__^0x06)==0?(T3MOD1=T3MOD1&(~CP2S0)):\
        (__MODE__^0x07)==0?(T3MOD1=T3MOD1|(CP2S0)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 TF3 Interrupt Ignored
* @details      Set TF3IG
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_TF3_Interrupt_Ignored_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        __STATE__==0?(T3MOD1=T3MOD1&(~TF3IG)):(T3MOD1=T3MOD1|(TF3IG));\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 Capture Source Dectect
* @details      Set EXEN3 T3EXH
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Capture_Source_Dectect(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/


#define __DRV_TIMER3_Capture_Source_Dectect(__MODE__)\
    MWT(\
        __DRV_TIMER3_SetEXEN3(__MODE__);\
        __DRV_TIMER3_SetT3EXH(__MODE__);\
    ;)
/**
*****************************************************************************
* @brief        TIMER3 Capture Source Dectect
* @details      Set EXEN3
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetEXEN3(IGNORED)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetEXEN3(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(EXEN3=0):\
        (__MODE__^0x01)==0?(EXEN3=0):\
        (__MODE__^0x02)==0?(EXEN3=1):\
        (__MODE__^0x03)==0?(EXEN3=1):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*****************************************************************************
* @brief        TIMER3 Capture Source Dectect
* @details      Set T3EXH
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       IGNORED
*  @arg\b       FALLING_EDGE
*  @arg\b       RISING_EDGE
*  @arg\b       DUAL_EDGE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetT3EXH(IGNORED)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetT3EXH(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        ((__MODE__^0x00)==0?(T3MOD=T3MOD&(~T3EXH)):\
        (__MODE__^0x01)==0?(T3MOD=T3MOD|(T3EXH)):\
        (__MODE__^0x02)==0?(T3MOD=T3MOD&(~T3EXH)):\
        (__MODE__^0x03)==0?(T3MOD=T3MOD|(T3EXH)):_nop_());\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER3 High Byte Register
* @details      Set TH3
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetHighByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_SetHighByte(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        TH3=__RELOAD__;\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER3 Low Byte Register
* @details      Set TL3
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetLowByte(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetLowByte(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        TL3=__RELOAD__;\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*******************************************************************************
* @brief        TIMER3 16 Bit Timer/Counter Reload
* @details      Set TH3 TL3
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Set16BitInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_Set16BitInterval(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        TH3=HIBYTE(__RELOAD__);\
        TL3=LOBYTE(__RELOAD__)&0xff;\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*******************************************************************************
* @brief        TIMER3 Capture High Byte Register
* @details      Set RCAP3H
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetRCAP3H(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_TIMER3_SetRCAP3H(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        RCAP3H=__RELOAD__;\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        TIMER3 Capture Low Byte Register
* @details      Set RCAP3L
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_SetRCAP3L(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_SetRCAP3L(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        RCAP3L=__RELOAD__;\
        __DRV_SFR_PageIndex(0);\
    ;)

/**
*******************************************************************************
* @brief        TIMER3 16 Bit Capture Reload
* @details      Set RCAP3H RCAP3L
* @param[in]    \_\_RELOAD\_\_ :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_TIMER3_Set16BitCaptureInterval(__RELOAD__)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_TIMER3_Set16BCaptureitInterval(__RELOAD__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        RCAP3H=HIBYTE(__RELOAD__);\
        RCAP3L=LOBYTE(__RELOAD__)&0xff;\
        __DRV_SFR_PageIndex(0);\
    ;)

bool DRV_TIMER0_GetTF0(void);
bool DRV_TIMER1_GetTF1(void);
bool DRV_TIMER2_GetTF2(void);
bool DRV_TIMER3_GetTF3(void);
bool DRV_TIMER2_GetTF2L(void);
bool DRV_TIMER2_GetEXF2(void);
bool DRV_TIMER3_GetTF3L(void);
bool DRV_TIMER3_GetEXF3(void);
void DRV_TIMER0_ClearTF0(void);
void DRV_TIMER1_ClearTF1(void);
void DRV_TIMER2_ClearTF2(void);
void DRV_TIMER3_ClearTF3(void);
void DRV_TIMER2_ClearEXF2(void);
void DRV_TIMER2_ClearTF2L(void);
void DRV_TIMER3_ClearEXF3(void);
void DRV_TIMER3_ClearTF3L(void);
void DRV_TIMER0_SetTF0(void);
void DRV_TIMER1_SetTF1(void);
void DRV_TIMER2_SetTF2(void);
void DRV_TIMER3_SetTF3(void);
void DRV_TIMER2_SetEXF2(void);
void DRV_TIMER2_SetTF2L(void);
void DRV_TIMER3_SetEXF3(void);
void DRV_TIMER3_SetTF3L(void);
uint8_t DRV_TIMER2_GetRCAP2H(void);
uint8_t DRV_TIMER2_GetRCAP2L(void);
uint8_t DRV_TIMER3_GetRCAP3H(void);
uint8_t DRV_TIMER3_GetRCAP3L(void);










#endif

