/**
 ******************************************************************************
 *
 * @file        MG82F6D17_IAP_DRV.h
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS" without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the worldwide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_Alan_20191223 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #0.02_Alan_20200107 //bugNum_Authour_Date
 * #0.03_Alan_20200122 //bugNum_Authour_Date
 * #0.04_Alan_20200210 //bugNum_Authour_Date
 * #0.05_Alan_20200327 //bugNum_Authour_Date
 * #0.06_Alan_20200421 //bugNum_Authour_Date
 * #0.07_Alan_20200529 //bugNum_Authour_Date
 * >> Modify: __DRV_IAP_ReadUID(__UID__) contents
 * #1.00_Alan_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */
#ifndef _MG82F6D17_IAP_DRV_H
#define _MG82F6D17_IAP_DRV_H

/// @cond Parameter_definition

//!@{
//! defgroup IFMT Commands
//__IFMT Commands
#define IFMT_STANDBY                        0x00
#define IFMT_IAP_READ                       0x01
#define IFMT_IAP_PROGRAM                    0x02
#define IFMT_IAP_PAGE_ERASE                 0x03
#define IFMT_FLASH_READ_CRC                 0x80
#define IFMT_FLASH_READ_INC_IFADR           0x81
#define IFMT_FLASH_PROGRAM_INC_IFADR        0x82
//!@}
/// @endcond

/**
 *******************************************************************************
 * @brief       Checking if ISP/IAP operations fail.
 * @details
 * @return      If it fails, it returns 1; otherwise, it returns 0 if it successes.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_GetStatus();
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_GetStatus(void);

/**
 *******************************************************************************
 * @brief       Erasing a full page of flash data without interrupts, a full page stands for 512 bytes. This should be executed first before start byte programming. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   ADDR : specifies the page start address which should be even.
 * @return      If it fails, it returns 1; otherwise, it returns 0 if it successes.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_PageEraseWithoutIT(0x36);
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_PageEraseWithoutIT(uint8_t ADDR);

/**
 *******************************************************************************
 * @brief       Erasing a full page of flash data, a full page stands for 512 bytes. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   ADDR : specifies the page start address which should be even.
 * @return      If it fails, it returns 1; otherwise, it returns 0 if it successes.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_PageErase(0x36);
 * @endcode
 * @note
 * @par Example
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_PageErase(uint8_t ADDR);

/**
 *******************************************************************************
 * @brief       Writing one byte data to flash without interrupts. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   MODE : supports byte program with/without  increasing address.
 *  @arg\b      IFMT_IAP_PROGRAM : writes one byte without increasing address.
 *  @arg\b      IFMT_FLASH_PROGRAM_INC_IFADR : writes one byte with increasing address.
 * @param[in]   ADDR : specifies the flash address, 16-bit, to be written.
 * @param[in]   DATA : the data which will be written onto the specified flash address.
 * @return      If it fails, it returns 1; otherwise, it returns 0 if it successes.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_ByteProgramWithoutIT(IFMT_FLASH_PROGRAM, 0x3600, 0x52);
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_ByteProgramWithoutIT(uint8_t MODE, uint16_t ADDR, uint8_t DATA);

/**
 *******************************************************************************
 * @brief       Writing one byte data to flash. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   MODE : supports byte program with/without increasing address.
 *  @arg\b      IFMT_IAP_PROGRAM : writes one byte without increasing address.
 *  @arg\b      IFMT_FLASH_PROGRAM_INC_IFADR : writes one byte with increasing address.
 * @param[in]   ADDR : specifies the flash address, 16-bit, to be written.
 * @param[in]   DATA : the data which will be written onto the specified flash address.
 * @return      If it fails, it returns 1; otherwise, it returns 0 if it successes.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_ByteProgram(IFMT_FLASH_PROGRAM, 0x3600, 0x52);
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_ByteProgram(uint8_t MODE, uint16_t ADDR, uint8_t DATA);

/**
 *******************************************************************************
 * @brief       Reading one byte from flash without interrupts. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   MODE : supports flash read with/without increasing address.
 *  @arg\b      IFMT_IAP_READ : reads one byte without increasing address.
 *  @arg\b      IFMT_IAP_READ_INC_IFADR : reads one byte with increasing address.
 * @param[in]   ADDR : specifies the flash address, 16-bit, to be read.
 * @return      Data stored in address ADDR.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_FlashReadWithoutIT(IFMT_IAP_READ, 0x3600);
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_FlashReadWithoutIT(uint8_t MODE, uint16_t ADDR);

/**
 *******************************************************************************
 * @brief       Reading one byte from flash. (Caution: This function MUST be operated in the suitable code area.)
 * @details
 * @param[in]   MODE : supports flash read with/without increasing address.
 *  @arg\b      IFMT_IAP_READ : reads one byte without increasing address.
 *  @arg\b      IFMT_IAP_READ_INC_IFADR : reads one byte with increasing address.
 * @param[in]   ADDR : specifies the flash address, 16-bit, to be read.
 * @return      Data stored in address ADDR.
 * @note
 * @par Example
 * @code
 *  DRV_IAP_FlashRead(IFMT_IAP_READ, 0x3600);
 * @endcode
 * @bug
 *******************************************************************************
 */
uint8_t DRV_IAP_FlashRead(uint8_t MODE, uint16_t ADDR);


/**
 *******************************************************************************
 * @brief       UID reading.
 * @details
 * @param[in]   \_\_UID\_\_ : a length 16 uint8_t array to store UID.
 * @return      UID.
 * @note   UID is stored in the address IFADRH = 0, IFADRL = 0xF0.
 * @par Example
 * @code
 *   __DRV_IAP_ReadUID(UID);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_IAP_ReadUID(__UID__)\
    MWT(\
        uint8_t i;\
        ISPCR |= ISPEN;\
        BOREV = 0x22;\
        IFMT = 0x06;\
        IFADRH = 0x00;\
        IFADRL = 0xF0;\
        for(i = 0;i < 16;i++){\
            SCMD = 0x46;\
            SCMD = 0xB9;\
            __UID__[i] = IFD;\
            IFADRL++;\
        }\
        BOREV = 0;\
        IFMT = 0;\
        ISPCR &= ~ISPEN;\
    )

#endif
