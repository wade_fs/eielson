 /**
 ******************************************************************************
 *
 * @file        MG82F6D17_GPIO_DRV.c
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IS" without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the worldwide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.07_Xian_2020611 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#include "MG82F6D17_CONFIG.h"


/**
 *******************************************************************************
 * @brief       Get Port 6 Pin 0 Status
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *      DRV_GPIO_GetPin60();
 * @endcode
 * @bug
 *******************************************************************************
 */
bool DRV_GPIO_GetPin60(void)
{
    _push_(SFRPI);
    SFRPI = 1;
    if(P6 & 0x01){
        _pop_(SFRPI);
        return 1;
    }
    _pop_(SFRPI);
    return 0;
}


/**
 *******************************************************************************
 * @brief       Get Port 6 Pin 1 Status
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *      DRV_GPIO_GetPin61();
 * @endcode
 * @bug
 *******************************************************************************
 */
bool DRV_GPIO_GetPin61(void)
{
    _push_(SFRPI);
    SFRPI = 1;
    if(P6 & 0x02){
        _pop_(SFRPI);
        return 1;
    }
    _pop_(SFRPI);
    return 0;
}

