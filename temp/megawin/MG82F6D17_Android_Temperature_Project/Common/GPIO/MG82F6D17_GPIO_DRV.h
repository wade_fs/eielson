 /**
 ******************************************************************************
 *
 * @file        MG82F6D17_GPIO_DRV.h
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IS" without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the worldwide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_Xian_20191223 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * #0.02_Xian_20200107 //bugNum_Authour_Date
 * #0.03_Xian_20200122 //bugNum_Authour_Date
 * #0.04_Xian_20200210 //bugNum_Authour_Date
 * #0.05_Xian_20200422 //bugNum_Authour_Date
 * #1.00_Xian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */


#ifndef __MG82F6D17_GPIO_DRV_H
#define __MG82F6D17_GPIO_DRV_H

/*--------------------------------------------------------------
GPIO Function :
    1.Port4 Alternated Function Control
        |----P44 , P45 Function : __DRV_GPIO_SetOCD2IO(__STATE__);
        |----P47 Function : __DRV_GPIO_SetRST2IO(__STATE__);
        __STATE__ :
            MW_ENABLE/MW_DISABLE

    2.GPIO Mode Control
        |----Port 1 Mode Assignment : __DRV_GPIO_P1ModeSelect(__MODE__);
        |----Port 2 Mode Assignment : __DRV_GPIO_P2ModeSelect(__MODE__);
        |----Port 3 Mode Assignment : __DRV_GPIO_P3ModeSelect(__MODE__);
        |----Port 4 Mode Assignment : __DRV_GPIO_P4ModeSelect(__MODE__);
        |----Port 6 Mode Assignment : __DRV_GPIO_P6ModeSelect(__MODE__);
        __MODE__ :
            P1ALL_InputOnly         P10_InputOnly           P11_InputOnly           P15_InputOnly           P16_InputOnly           P17_InputOnly
            P1ALL_OpenDrainPullUp   P10_OpenDrainPullUp     P11_OpenDrainPullUp     P15_OpenDrainPullUp     P16_OpenDrainPullUp     P17_OpenDrainPullUp
            P1ALL_OpenDrain         P10_OpenDrain           P11_OpenDrain           P15_OpenDrain           P16_OpenDrain           P17_OpenDrain
            P1ALL_PushPull          P10_PushPull            P11_PushPull            P15_PushPull            P16_PushPull            P17_PushPull

            P2ALL_InputOnly         P22_InputOnly           P24_InputOnly
            P2ALL_OpenDrainPullUp   P22_OpenDrainPullUp     P24_OpenDrainPullUp
            P2ALL_OpenDrain         P22_OpenDrain           P24_OpenDrain
            P2ALL_PushPull          P22_PushPull            P24_PushPull

            P3ALL_QuasiMode         P30_QuasiMode           P31_QuasiMode           P33_QuasiMode           P34_QuasiMode           P35_QuasiMode
            P3ALL_PushPull          P30_PushPull            P31_PushPull            P33_PushPull            P34_PushPull            P35_PushPull
            P3ALL_InputOnly         P30_InputOnly           P31_InputOnly           P33_InputOnly           P34_InputOnly           P35_InputOnly
            P3ALL_OpenDrain         P30_OpenDrain           P31_OpenDrain           P33_OpenDrain           P34_OpenDrain            P35_OpenDrain

            P4ALL_InputOnly         P44_InputOnly           P45_InputOnly           P47_InputOnly
            P4ALL_OpenDrainPullUp   P44_OpenDrainPullUp     P45_OpenDrainPullUp     P47_OpenDrainPullUp
            P4ALL_OpenDrain         P44_OpenDrain           P45_OpenDrain           P47_OpenDrain
            P4ALL_PushPull          P44_PushPull            P45_PushPull            P47_PushPull

            P6ALL_InputOnly         P60_InputOnly           P61_InputOnly
            P6ALL_OpenDrainPullUp   P60_OpenDrainPullUp     P61_OpenDrainPullUp
            P6ALL_OpenDrain         P60_OpenDrain           P61_OpenDrain
            P6ALL_PushPull          P60_PushPull            P61_PushPull

    3.GPIO Output Driving Strength Control
        |----Port1 Driving Strength Control : __DRV_GPIO_P1OutputDrivingConfig(__PORT__,__STATE__);
        |----Port2 Driving Strength Control : __DRV_GPIO_P2OutputDrivingConfig(__PORT__,__STATE__);
        |----Port3 Driving Strength Control : __DRV_GPIO_P3OutputDrivingConfig(__PORT__,__STATE__);
        |----Port4 Driving Strength Control : __DRV_GPIO_P4OutputDrivingConfig(__PORT__,__STATE__);
        __PORT__ :
            P10_to_P13_Driving / P14_to_P17_Driving
            P20_to_P23_Driving / P24_to_P27_Driving
            P30_to_P33_Driving / P34_to_P37_Driving
            P44_to_P46_Driving
        __STATE__ :
            HIGH / LOW

    4.GPIO Port Output Fast Driving Control
        |----Port1 Fast Driving Control : __DRV_GPIO_SetP1FastDriving(__PORT__,__STATE__);
        |----Port2 Fast Driving Control : __DRV_GPIO_SetP2FastDriving(__PORT__,__STATE__);
        |----Port3 Fast Driving Control : __DRV_GPIO_SetP3FastDriving(__PORT__,__STATE__);
        |----Port4 Fast Driving Control : __DRV_GPIO_SetP4FastDriving(__PORT__,__STATE__);
        __PORT__ :
            P1PinALL    P1Pin0      P1Pin1      P1Pin5      P1Pin6      P1Pin7
            P2PinALL    P2Pin0      P2Pin1
            P3PinALL    P3Pin0      P3Pin1      P3Pin3      P3Pin4      P3Pin5
            P4PinALL    P4Pin4      P4Pin5      P4Pin7
            P6PinALL    P6Pin0      P6Pin1
        __STATE__ :
            FAST / NORMAL

    5.GPIO Port Protect
        |----Port4 : __DRV_GPIO_ProtectP4(__STATE__);
        |----Port6 : __DRV_GPIO_ProtectP6(__STATE__);
        __STATE__ :
            MW_ENABLE/MW_DISABLE

    *Other GPIO Function
        |----Default Init : __DRV_GPIO_PxDeInit(void); (x = 1,2,3,4,6)
        |----Write : __DRV_GPIO_WritePx(__PORT__, __VALUE__); (x = 1,2,3,4,6)
        |----Read : __DRV_GPIO_ReadPx(__DATA__); (x = 1,2,3,4,6)
        |----Inverse : __DRV_GPIO_InversePinPx(__PORT_PIN__); (x = 1,2,3,4,6)
--------------------------------------------------------------*/

//@del{
/*
typedef enum{
    HIGH = 1,
    LOW = 0,
}__STRENGTH_Def;

typedef enum{
    FAST = 1,
    NORMAL = 0,
}__SPEED_Def;
*/
//@del}

/// @cond __DRV_GPIO_define

#define HIGH        0
#define LOW         1

#define FAST        1
#define NORMAL      0

//!@{
//! defgroup P1Pinx Pin number
#define P1PinALL                (0xE3)
#define P1Pin0                  (0x01)
#define P1Pin1                  (0x02)
#define P1Pin5                  (0x20)
#define P1Pin6                  (0x40)
#define P1Pin7                  (0x80)
//!@}

//!@{
//! defgroup P2Pinx Pin number
#define P2PinALL                (0x14)
#define P2Pin2                  (0x04)
#define P2Pin4                  (0x10)
//!@}

//!@{
//! defgroup P3Pinx Pin number
#define P3PinALL                (0x3B)
#define P3Pin0                  (0x01)
#define P3Pin1                  (0x02)
#define P3Pin3                  (0x08)
#define P3Pin4                  (0x10)
#define P3Pin5                  (0x20)
//!@}

//!@{
//! defgroup P4Pinx Pin number
#define P4PinALL                (0xB0)
#define P4Pin4                  (0x10)
#define P4Pin5                  (0x20)
#define P4Pin7                  (0x80)
//!@}

//!@{
//! defgroup P1Pinx Pin number
#define P6PinALL                (0x03)
#define P6Pin0                  (0x01)
#define P6Pin1                  (0x02)
//!@}
//posite => high word , value => low word

//!@{
//! defgroup P0IOMODE Port0 IO mode
#define P1ALL_InputOnly         (0xE3E300E3)
#define P1ALL_OpenDrainPullUp   (0xE3E3E3E3)
#define P1ALL_OpenDrain         (0xE3E30000)
#define P1ALL_PushPull          (0xE3E3E300)

#define P10_InputOnly           (0x01010001)
#define P10_OpenDrainPullUp     (0x01010101)
#define P10_OpenDrain           (0x01010000)
#define P10_PushPull            (0x01010100)
#define P11_InputOnly           (0x02020002)
#define P11_OpenDrainPullUp     (0x02020202)
#define P11_OpenDrain           (0x02020000)
#define P11_PushPull            (0x02020200)
#define P15_InputOnly           (0x20200020)
#define P15_OpenDrainPullUp     (0x20202020)
#define P15_OpenDrain           (0x20200000)
#define P15_PushPull            (0x20202000)
#define P16_InputOnly           (0x40400040)
#define P16_OpenDrainPullUp     (0x40404040)
#define P16_OpenDrain           (0x40400000)
#define P16_PushPull            (0x40404000)
#define P17_InputOnly           (0x80800080)
#define P17_OpenDrainPullUp     (0x80808080)
#define P17_OpenDrain           (0x80800000)
#define P17_PushPull            (0x80808000)
//!@}

//!@{
//! defgroup P2IOMODE Port0 IO mode
#define P2ALL_InputOnly         (0x14140014)
#define P2ALL_OpenDrainPullUp   (0x14141414)
#define P2ALL_OpenDrain         (0x14140000)
#define P2ALL_PushPull          (0x14141400)

#define P22_InputOnly           (0x04040004)
#define P22_OpenDrainPullUp     (0x04040404)
#define P22_OpenDrain           (0x04040000)
#define P22_PushPull            (0x04040400)
#define P24_InputOnly           (0x10100010)
#define P24_OpenDrainPullUp     (0x10101010)
#define P24_OpenDrain           (0x10100000)
#define P24_PushPull            (0x10101000)
//!@}

//!@{
//! defgroup P3IOMODE Port0 IO mode
#define P3ALL_QuasiMode         (0x3B3B0000)
#define P3ALL_PushPull          (0x3B3B003B)
#define P3ALL_InputOnly         (0x3B3B3B00)
#define P3ALL_OpenDrain         (0x3B3B3B3B)

#define P30_QuasiMode           (0x01010000)
#define P30_PushPull            (0x01010001)
#define P30_InputOnly           (0x01010100)
#define P30_OpenDrain           (0x01010101)
#define P31_QuasiMode           (0x02020000)
#define P31_PushPull            (0x02020002)
#define P31_InputOnly           (0x02020200)
#define P31_OpenDrain           (0x02020202)
#define P33_QuasiMode           (0x08080000)
#define P33_PushPull            (0x08080008)
#define P33_InputOnly           (0x08080800)
#define P33_OpenDrain           (0x08080808)
#define P34_QuasiMode           (0x10100000)
#define P34_PushPull            (0x10100010)
#define P34_InputOnly           (0x10101000)
#define P34_OpenDrain           (0x10101010)
#define P35_QuasiMode           (0x20200000)
#define P35_PushPull            (0x20200020)
#define P35_InputOnly           (0x20202000)
#define P35_OpenDrain           (0x20202020)
//!@}

//!@{
//! defgroup P4IOMODE Port0 IO mode
#define P4ALL_InputOnly         (0xB0B000B0)
#define P4ALL_OpenDrainPullUp   (0xB0B0B0B0)
#define P4ALL_OpenDrain         (0xB0B00000)
#define P4ALL_PushPull          (0xB0B0B000)

#define P44_InputOnly           (0x10100010)
#define P44_OpenDrainPullUp     (0x10101010)
#define P44_OpenDrain           (0x10100000)
#define P44_PushPull            (0x10101000)
#define P45_InputOnly           (0x20200020)
#define P45_OpenDrainPullUp     (0x20202020)
#define P45_OpenDrain           (0x20200000)
#define P45_PushPull            (0x20202000)
#define P47_InputOnly           (0x80800080)
#define P47_OpenDrainPullUp     (0x80808080)
#define P47_OpenDrain           (0x80800000)
#define P47_PushPull            (0x80808000)
//!@}

//!@{
//! defgroup P6IOMODE Port0 IO mode
#define P6ALL_InputOnly         (0x03030003)
#define P6ALL_OpenDrainPullUp   (0x03030303)
#define P6ALL_OpenDrain         (0x03030000)
#define P6ALL_PushPull          (0x03030300)

#define P60_InputOnly           (0x01010001)
#define P60_OpenDrainPullUp     (0x01010101)
#define P60_OpenDrain           (0x01010000)
#define P60_PushPull            (0x01010100)
#define P61_InputOnly           (0x02020002)
#define P61_OpenDrainPullUp     (0x02020202)
#define P61_OpenDrain           (0x02020000)
#define P61_PushPull            (0x02020200)
//!@}

//!@{
//! defgroup Pack_Port_Pins
#define P10_to_P13_Driving      P1DC0
#define P14_to_P17_Driving      P1DC1
#define P20_to_P23_Driving      P2DC0
#define P24_to_P27_Driving      P2DC1
#define P30_to_P33_Driving      P3DC0
#define P34_to_P37_Driving      P3DC1
#define P44_to_P46_Driving      P4DC1
//!@}
/// @endcond

///@cond
bool DRV_GPIO_GetPin60(void);
bool DRV_GPIO_GetPin61(void);
///@endcond



/**
 *******************************************************************************
 * @brief       Initializing Port1 IO mode
 * @details
 * @param[in]   \_\_MODE\_\_ :  specifies the Port1 mode.
 *  @arg\b      P1ALL_InputOnly (Default)
 *  @arg\b      P1ALL_OpenDrainPullUp
 *  @arg\b      P1ALL_OpenDrain
 *  @arg\b      P1ALL_PushPull
 *  @arg\b      P10_InputOnly
 *  @arg\b      P10_OpenDrainPullUp
 *  @arg\b      P10_OpenDrain
 *  @arg\b      P10_PushPull
 *  @arg\b      P11_InputOnly
 *  @arg\b      P11_OpenDrainPullUp
 *  @arg\b      P11_OpenDrain
 *  @arg\b      P11_PushPull
 *  @arg\b      P15_InputOnly
 *  @arg\b      P15_OpenDrainPullUp
 *  @arg\b      P15_OpenDrain
 *  @arg\b      P15_PushPull
 *  @arg\b      P16_InputOnly
 *  @arg\b      P16_OpenDrainPullUp
 *  @arg\b      P16_OpenDrain
 *  @arg\b      P16_PushPull
 *  @arg\b      P17_InputOnly
 *  @arg\b      P17_OpenDrainPullUp
 *  @arg\b      P17_OpenDrain
 *  @arg\b      P17_PushPull
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P1ModeSelect(P11_OpenDrainPullUp);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P1ModeSelect(__MODE__)\
    MWT(\
        P1M0 &= ~(HIBYTE(HIWORD(__MODE__)));\
        P1M0 |= (HIBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(0); \
        P1M1 &= ~(LOBYTE(HIWORD(__MODE__)));\
        P1M1 |= (LOBYTE(LOWORD(__MODE__)));\
    )



/**
 *******************************************************************************
 * @brief       Initializing Port2 IO mode
 * @details
 * @param[in]   \_\_MODE\_\_ :  specifies the Port2 mode.
 *  @arg\b      P2ALL_InputOnly (Default)
 *  @arg\b      P2ALL_OpenDrainPullUp
 *  @arg\b      P2ALL_OpenDrain
 *  @arg\b      P2ALL_PushPull
 *  @arg\b      P22_InputOnly
 *  @arg\b      P22_OpenDrainPullUp
 *  @arg\b      P22_OpenDrain
 *  @arg\b      P22_PushPull
 *  @arg\b      P24_InputOnly
 *  @arg\b      P24_OpenDrainPullUp
 *  @arg\b      P24_OpenDrain
 *  @arg\b      P24_PushPull
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P2ModeSelect(P22_OpenDrainPullUp);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P2ModeSelect(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(0);\
        P2M0 &= ~(HIBYTE(HIWORD(__MODE__)));\
        P2M0 |= (HIBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(1);\
        P2M1 &= ~(LOBYTE(HIWORD(__MODE__)));\
        P2M1 |= (LOBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Initializing Port3 IO mode
 * @details
 * @param[in]   \_\_MODE\_\_ :  specifies the Port3 mode.
 *  @arg\b      P3ALL_QuasiMode (Default)
 *  @arg\b      P3ALL_PushPull
 *  @arg\b      P3ALL_InputOnly
 *  @arg\b      P3ALL_OpenDrain
 *  @arg\b      P30_QuasiMode
 *  @arg\b      P30_PushPull
 *  @arg\b      P30_InputOnly
 *  @arg\b      P30_OpenDrain
 *  @arg\b      P31_QuasiMode
 *  @arg\b      P31_PushPull
 *  @arg\b      P31_InputOnly
 *  @arg\b      P31_OpenDrain
 *  @arg\b      P33_QuasiMode
 *  @arg\b      P33_PushPull
 *  @arg\b      P33_InputOnly
 *  @arg\b      P33_OpenDrain
 *  @arg\b      P34_QuasiMode
 *  @arg\b      P34_PushPull
 *  @arg\b      P34_InputOnly
 *  @arg\b      P34_OpenDrain
 *  @arg\b      P35_QuasiMode
 *  @arg\b      P35_PushPull
 *  @arg\b      P35_InputOnly
 *  @arg\b      P35_OpenDrain
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P3ModeSelect(P30_QuasiMode);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P3ModeSelect(__MODE__)\
    MWT(\
        P3M0 &= ~(HIBYTE(HIWORD(__MODE__)));\
        P3M0 |= (HIBYTE(LOWORD(__MODE__)));\
        P3M1 &= ~(LOBYTE(HIWORD(__MODE__)));\
        P3M1 |= (LOBYTE(LOWORD(__MODE__)));\
    )



/**
 *******************************************************************************
 * @brief       Initializing Port4 IO mode
 * @details
 * @param[in]   \_\_MODE\_\_ :  specifies the Port4 mode.
 *  @arg\b      P4ALL_InputOnly (Default)
 *  @arg\b      P4ALL_OpenDrainPullUp
 *  @arg\b      P4ALL_OpenDrain
 *  @arg\b      P4ALL_PushPull
 *  @arg\b      P44_InputOnly
 *  @arg\b      P44_OpenDrainPullUp
 *  @arg\b      P44_OpenDrain
 *  @arg\b      P44_PushPull
 *  @arg\b      P45_InputOnly
 *  @arg\b      P45_OpenDrainPullUp
 *  @arg\b      P45_OpenDrain
 *  @arg\b      P45_PushPull
 *  @arg\b      P47_InputOnly
 *  @arg\b      P47_OpenDrainPullUp
 *  @arg\b      P47_OpenDrain
 *  @arg\b      P47_PushPull
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P4ModeSelect(P44_InputOnly);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P4ModeSelect(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(0);\
        P4M0 &= ~(HIBYTE(HIWORD(__MODE__)));\
        P4M0 |= (HIBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(2);\
        P4M1 &= ~(LOBYTE(HIWORD(__MODE__)));\
        P4M1 |= (LOBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port6 IO mode
 * @details
 * @param[in]   \_\_MODE\_\_ :  specifies the Port6 mode.
 *  @arg\b      P6ALL_InputOnly (Default)
 *  @arg\b      P6ALL_OpenDrainPullUp
 *  @arg\b      P6ALL_OpenDrain
 *  @arg\b      P6ALL_PushPull
 *  @arg\b      P60_InputOnly
 *  @arg\b      P60_OpenDrainPullUp
 *  @arg\b      P60_OpenDrain
 *  @arg\b      P60_PushPull
 *  @arg\b      P61_InputOnly
 *  @arg\b      P61_OpenDrainPullUp
 *  @arg\b      P61_OpenDrain
 *  @arg\b      P61_PushPull
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P6ModeSelect(P60_InputOnly);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P6ModeSelect(__MODE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        P6M0 &= ~(HIBYTE(HIWORD(__MODE__)));\
        P6M0 |= (HIBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(3);\
        P6M1 &= ~(LOBYTE(HIWORD(__MODE__)));\
        P6M1 |= (LOBYTE(LOWORD(__MODE__)));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port1 IO mode
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P1DeInit();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P1DeInit()\
    MWT(\
        __DRV_GPIO_P1ModeSelect(P1ALL_InputOnly);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port2 IO mode
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P2DeInit();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P2DeInit()\
    MWT(\
        __DRV_GPIO_P2ModeSelect(P2ALL_InputOnly);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port3 IO mode
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P3DeInit();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P3DeInit()\
    MWT(\
        __DRV_GPIO_P3ModeSelect(P3ALL_QuasiMode);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port4 IO mode
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P4DeInit();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P4DeInit()\
    MWT(\
        __DRV_GPIO_P4ModeSelect(P4ALL_InputOnly);\
    )



/**
 *******************************************************************************
 * @brief       Default initial Port6 IO mode
 * @details
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P6DeInit();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P6DeInit()\
    MWT(\
        __DRV_GPIO_P6ModeSelect(P6ALL_InputOnly);\
    )



/**
 *******************************************************************************
 * @brief       Writing GPIO's Port1 state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies a whole port or port pins.
 * @param[in]   \_\_VALUE\_\_ : specifies the value to be written.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_WriteP1(P10,1);
 *   __DRV_GPIO_WriteP1(P1,0x01);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_WriteP1(__PORT__, __VALUE__)\
    MWT(\
         __PORT__=__VALUE__;\
    )



/**
 *******************************************************************************
 * @brief       Writing GPIO's Port2 state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies a whole port or port pins.
 * @param[in]   \_\_VALUE\_\_ : specifies the value to be written.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Write(P22,1);
 *   __DRV_GPIO_Write(P2,0x04);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_WriteP2(__PORT__, __VALUE__)\
    MWT(\
         __PORT__=__VALUE__;\
    )



/**
 *******************************************************************************
 * @brief       Writing GPIO's Port3 state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies a whole port or port pins.
 * @param[in]   \_\_VALUE\_\_ : specifies the value to be written.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Write(P30,1);
 *   __DRV_GPIO_Write(P3,0x01);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_WriteP3(__PORT__, __VALUE__)\
    MWT(\
         __PORT__=__VALUE__;\
    )



/**
 *******************************************************************************
 * @brief       Writing GPIO's Port4 state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies a whole port or port pins.
 * @param[in]   \_\_VALUE\_\_ : specifies the value to be written.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Write(P45,1);
 *   __DRV_GPIO_Write(P4,0x10);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_WriteP4(__PORT__, __VALUE__)\
    MWT(\
        __PORT__=__VALUE__;\
    )



/**
 *******************************************************************************
 * @brief       Writing GPIO's Port6 state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies a whole port or port pins.
 * @param[in]   \_\_VALUE\_\_ : specifies the value to be written.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Write(P60,1);
 *   __DRV_GPIO_Write(P6,0x01);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_WriteP6(__PORT__, __VALUE__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __PORT__=__VALUE__;\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 state.
 * @details
 * @param[out]   \_\_DATA\_\_ : the place to store data.
 * @return      Port state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Read();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_ReadP1(__DATA__)\
    MWT(\
        __DATA__ = P1;\
    )



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 Pin0 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin10();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin10()      P10



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 Pin1 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin11();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin11()      P11



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 Pin5 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin15();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin15()      P15



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 Pin6 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin16();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin16()      P16



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port1 Pin7 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin17();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin17()      P17



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port2 state.
 * @details
 * @param[out]   \_\_DATA\_\_ : the place to store data.
 * @return      Port state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Read(P2);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_ReadP2(__DATA__)\
    MWT(\
        __DATA__ = P2;\
    )



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port2 Pin2 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin22();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin22()      P22



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port2 Pin4 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin24();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin24()      P24



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 state.
 * @details
 * @param[out]   \_\_DATA\_\_ : the place to store data.
 * @return      Port state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Read(P3);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_ReadP3(__DATA__)\
    MWT(\
        __DATA__ = P3;\
    )



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 Pin0 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin30();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin30()      P30



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 Pin1 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin31();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin31()      P31



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 Pin3 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin33();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin33()      P33



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 Pin4 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin34();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin34()      P34



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port3 Pin5 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin35();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin35()      P35



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port4 state.
 * @details
 * @param[out]   \_\_DATA\_\_ : the place to store data.
 * @return
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Read(P4);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_ReadP4(__DATA__)\
    MWT(\
        __DATA__ = P4;\
    )



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port4 Pin4 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin44();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin44()      P44



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port4 Pin5 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin45();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin45()      P45



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port4 Pin7 state.
 * @details
 * @return      Pin state
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_GetPin47();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_GetPin47()      P47



/**
 *******************************************************************************
 * @brief       Reading GPIO's Port6 state.
 * @details
 * @param[out]   \_\_DATA\_\_ : the place to store data.
 * @return
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_Read(P6);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_ReadP6(__DATA__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __DATA__ = P6;\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Inversing Port1 pin state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P1
 *  @arg\b      P10
 *  @arg\b      P11
 *  @arg\b      P15
 *  @arg\b      P16
 *  @arg\b      P17
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P1_InversePin(P10);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_InversePinP1(__PORT__)\
    MWT(\
        __PORT__ = !(__PORT__);\
    )



/**
 *******************************************************************************
 * @brief       Inversing Port2 pin state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P2
 *  @arg\b      P22
 *  @arg\b      P24
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P2_InversePin(P22);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_InversePinP2(__PORT__)\
    MWT(\
        __PORT__ = !(__PORT__);\
    )



/**
 *******************************************************************************
 * @brief       Inversing Port3 pin state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P3
 *  @arg\b      P30
 *  @arg\b      P31
 *  @arg\b      P33
 *  @arg\b      P34
 *  @arg\b      P35
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P3_InversePin(P35);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_InversePinP3(__PORT__)\
    MWT(\
        __PORT__ = !(__PORT__);\
    )



/**
 *******************************************************************************
 * @brief       Inversing Port4 pin state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P4
 *  @arg\b      P44
 *  @arg\b      P45
 *  @arg\b      P47
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P4_InversePin(P44);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_InversePinP4(__PORT__)\
    MWT(\
        __PORT__ = !(__PORT__);\
    )



/**
 *******************************************************************************
 * @brief       Inversing Port6 pin state.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P6
 *  @arg\b      P60
 *  @arg\b      P61
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_P6_InversePin(P60);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_InversePinP6(__PORT__)\
    MWT(\
        __DRV_SFR_PageIndex(1);\
        __PORT__ = !(__PORT__);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Selecting Port1 driving current strength.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P10_to_P13_Driving
 *  @arg\b      P14_to_P17_Driving
 * @param[in]   \_\_STATE\_\_ :
 *  @arg\b      HIGH : specifies P1 with high driving current strength.
 *  @arg\b      LOW : specifies P1 with low driving current strength.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_OutputDrivingConfig(P10_to_P13_Driving, HIGH);
 *   __DRV_GPIO_OutputDrivingConfig( P14_to_P17_Driving, LOW);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P1OutputDrivingConfig(__PORT__,__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        (__STATE__==LOW) ? (PDRVC0|=__PORT__):(PDRVC0&=~__PORT__);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Selecting Port2 driving current strength.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P20_to_P23_Driving
 *  @arg\b      P24_to_P27_Driving
 * @param[in]   \_\_STATE\_\_ :
 *  @arg\b      HIGH : specifies P1 with high driving current strength.
 *  @arg\b      LOW : specifies P1 with low driving current strength.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_OutputDrivingConfig(P20_to_P23_Driving, HIGH);
 *   __DRV_GPIO_OutputDrivingConfig(P24_to_P27_Driving, LOW);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P2OutputDrivingConfig(__PORT__,__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        (__STATE__==LOW) ? (PDRVC0|=__PORT__):(PDRVC0&=~__PORT__);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Selecting Port3 driving current strength.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P30_to_P33_Driving
 *  @arg\b      P34_to_P37_Driving
 * @param[in]   \_\_STATE\_\_ : enables high driving or low driving.
 *  @arg\b      HIGH : specifies P1 with high driving current strength.
 *  @arg\b      LOW : specifies P1 with low driving current strength.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_OutputDrivingConfig(P30_to_P33_Driving, HIGH);
 *   __DRV_GPIO_OutputDrivingConfig(P30_to_P33_Driving, LOW);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P3OutputDrivingConfig(__PORT__,__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(2);\
        (__STATE__==LOW) ? (PDRVC0|=__PORT__):(PDRVC0&=~__PORT__);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Selecting Port4 driving current strength.
 * @details
 * @param[in]   \_\_PORT\_\_ : specifies port pins
 *  @arg\b      P44_to_P46_Driving
 * @param[in]   \_\_STATE\_\_ : enables high driving or low driving.
 *  @arg\b      HIGH : specifies P1 with high driving current strength.
 *  @arg\b      LOW : specifies P1 with low driving current strength.
 * @return      None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_OutputDrivingConfig(P44_to_P46_Driving, HIGH);
 *   __DRV_GPIO_OutputDrivingConfig(P44_to_P46_Driving, LOW);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P4OutputDrivingConfig(__PORT__,__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        (__STATE__==LOW) ? (PDRVC1|=__PORT__):(PDRVC1&=~__PORT__);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Port1 pin output fast driving control.
 * @details
 * @param[in]   \_\_PORT\_\_ : port pin to enable fast driving.
 *  @arg\b      P1PinALL
 *  @arg\b      P1Pin0
 *  @arg\b      P1Pin1
 *  @arg\b      P1Pin5
 *  @arg\b      P1Pin6
 *  @arg\b      P1Pin7
 * @param[in]   \_\_STATE\_\_ : enables/disables fast driving.
 *  @arg\b      NORMAL : disables fast driving on port pin output. (Default)
 *  @arg\b      FAST : enables fast driving on port pin output.
 * @return  None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_SetP1FastDriving(P1PinALL, NORMAL);
 *   __DRV_GPIO_SetP1FastDriving(P1PinALL, FAST);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetP1FastDriving(__PORT__, __STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        __STATE__==FAST ? (P1FDC|=(__PORT__)) : (P1FDC&=~(__PORT__));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Port2 pin output fast driving control.
 * @details
 * @param[in]   \_\_PORT\_\_ : port pin to enable fast driving.
 *  @arg\b      P2PinALL
 *  @arg\b      P2Pin2
 *  @arg\b      P2Pin4
 * @param[in]   \_\_STATE\_\_ : enables/disables fast driving.
 *  @arg\b      NORMAL : disables fast driving on port pin output. (Default)
 *  @arg\b      FAST : enables fast driving on port pin output.
 * @return  None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_SetP2FastDriving(P2PinALL, NORMAL);
 *   __DRV_GPIO_SetP2FastDriving(P2PinALL, FAST);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetP2FastDriving(__PORT__, __STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(9);\
        __STATE__==FAST ? (P2FDC|=(__PORT__)) : (P2FDC&=~(__PORT__));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Port3 pin output fast driving control.
 * @details
 * @param[in]   \_\_PORT\_\_ : port pin to enable fast driving.
 *  @arg\b      P3PinALL
 *  @arg\b      P3Pin0
 *  @arg\b      P3Pin1
 *  @arg\b      P3Pin3
 *  @arg\b      P3Pin4
 *  @arg\b      P3Pin5
 * @param[in]   \_\_STATE\_\_ : enables/disables fast driving.
 *  @arg\b      NORMAL : disables fast driving on port pin output. (Default)
 *  @arg\b      FAST : enables fast driving on port pin output.
 * @return  None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_SetP3FastDriving(P3PinALL, NORMAL);
 *   __DRV_GPIO_SetP3FastDriving(P3PinALL, FAST);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetP3FastDriving(__PORT__, __STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__==FAST ? (P3FDC|=(__PORT__)) : (P3FDC&=~(__PORT__));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Port4 pin output fast driving control.
 * @details
 * @param[in]   \_\_PORT\_\_ : port pin to enable fast driving.
 *  @arg\b      P4PinALL
 *  @arg\b      P4Pin4
 *  @arg\b      P4Pin5
 *  @arg\b      P4Pin7
 * @param[in]   \_\_STATE\_\_ : enables/disables fast driving.
 *  @arg\b      NORMAL : disables fast driving on port pin output. (Default)
 *  @arg\b      FAST : enables fast driving on port pin output.
 * @return  None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_SetP4FastDriving(P4PinALL, NORMAL);
 *   __DRV_GPIO_SetP4FastDriving(P4PinALL, FAST);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetP4FastDriving(__PORT__, __STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(0xA);\
        __STATE__==FAST ? (P4FDC|=(__PORT__)) : (P4FDC&=~(__PORT__));\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       OCD enable.
 * @details
 * @param[in]   \_\_STATE\_\_ : enables/disables OCD interface on P44 and P45
 *  @arg\b      MW_ENABLE : enables OCD interface on P44 and P45. (Default)
 *  @arg\b      MW_DISABLE : disables OCD interface on P44 and P45.
 * @return None
 * @note
 * @par Example
 * @code
 *   __DRV_GPIO_SetOCD2IO(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetOCD2IO(__STATE__)\
    MWT(\
        uint8_t P_data;\
        P_data = DRV_PageP_Read(DCON0_P);\
        __STATE__ == MW_DISABLE ? (P_data &= ~OCDE_P) : (P_data |= OCDE_P);\
        DRV_PageP_Write(DCON0_P,P_data);\
    )



/**
 *******************************************************************************
 * @brief       Reset function on I/O.
 * @details
 * @param[in]   \_\_STATE\_\_ :
 *  @arg\b      MW_ENABLE : Selects I/O pad function for external reset input, RST. (Default)
 *  @arg\b      MW_DISABLE : Selects I/O pad function for P47.
 * @return      None
 * @note
 * @code
 *   __DRV_GPIO_SetRST2IO(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_SetRST2IO(__STATE__)\
    MWT(\
        uint8_t P_data;\
        P_data = DRV_PageP_Read(DCON0_P);\
        __STATE__ == MW_DISABLE ? (P_data &= ~RSTIO_P) : (P_data |= RSTIO_P);\
        DRV_PageP_Write(DCON0_P,P_data);\
    )



/**
 *******************************************************************************
 * @brief       P6 SFR access control.
 * @details
 * @param[in]   \_\_STATE\_\_ : enables/disables P6 SFR protection.
 *  @arg\b      MW_ENABLE : it will disable the P6 SFR modifications in page 0~F. P6 in page 0~F only keeps SFR read function.
 *                      But software always owns the modification capability in SFR Page P.
 *  @arg\b      MW_DISABLE : modifications and read functions are both available in page 0~F and Page P. (Default)
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P6_Protected(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
  */
#define __DRV_GPIO_P6_Protected(__STATE__)\
    MWT(\
        uint8_t P_data;\
        P_data = DRV_PageP_Read(SPCON0_P);\
        __STATE__ == MW_ENABLE ? (P_data |= P6CTL_P) : (P_data &= ~P6CTL_P);\
        DRV_PageP_Write(SPCON0_P,P_data);\
    )



/**
 *******************************************************************************
 * @brief       P4 SFR access control.
 * @details
 * @param[in]   \_\_STATE\_\_ : enables/disables P4 SFR protection.
 *  @arg\b      MW_ENABLE : it will disable the P4 SFR modifications in page 0~F. P4 in page 0~F only keeps SFR read function.
 *                      But software always owns the modification capability in SFR Page P.
 *  @arg\b      MW_DISABLE : modifications and read functions are both available in page 0~F and Page P. (Default)
 * @return      None
 * @note
 * @par Example
 * @code
 *  __DRV_GPIO_P4_Protected( MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_GPIO_P4_Protected(__STATE__)\
    MWT(\
        uint8_t P_data;\
        P_data = DRV_PageP_Read(SPCON0_P);\
        __STATE__ == MW_ENABLE ? (P_data |= P4CTL_P) : (P_data &= ~P4CTL_P);\
        DRV_PageP_Write(SPCON0_P,P_data);\
    )


#endif

