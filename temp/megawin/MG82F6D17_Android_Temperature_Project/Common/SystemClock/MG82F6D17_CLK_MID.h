/**
 ******************************************************************************
 *
 * @file        MG82F6D17_CLK_MID.H
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_Brian_20191223 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * #0.02_Brian_20200116 //bugNum_Authour_Date
 * #0.03_Brian_20200122 //bugNum_Authour_Date
 * #0.04_Brian_20200210 //bugNum_Authour_Date
 * >> Modify : IFD = (IFD & ~OSCin_ECKI_P60) | __SELECT__;\
 * >>     to > IFD &= ~OSCin_ECKI_P60;\
 * >>          IFD | =__SELECT__;\
 * >> Modify : for(IFD=0;IFD<253;IFD++);\
 * >>     to > IFD=255;\
 * >>          do{IFD--;}while(IFD!=0);\
 * >> Modify : CKCON0 = (CKCON0 & ~SYSCLK_MCKDO_DIV_128) | __SELECT__;\
 * >>     to > CKCON0 |= SYSCLK_MCKDO_DIV_128;\
 * >>          CKCON0 &= (~SYSCLK_MCKDO_DIV_128 | __SELECT__);\
 * #0.04_Brian_20200227 //bugNum_Authour_Date
 * >> Modify : __DRV_CLK_P60Mux_Select(__SELECT__) : Delete IFD for temp value
 * >>     to > AUXR0 &= ~P60_MCK_DIV_4;\
 * >>          AUXR0 |= __SELECT__;\
 * #0.05_Brian_20200327 //bugNum_Authour_Date
 * >> Divid MG82F6D17_CLK_DRV.h to 3 files
 * >>   MG82F6D17_CLK_Wizard.c : Wizard Table , Init Function
 * >>   MG82F6D17_CLK_DRV.h : DRV
 * >>   MG82F6D17_CLK_MID.h : Easy Function
 * >> MG82F6D17_CLK_MID.h
 * >>   Modify param data format. (Ref MG82F6D17_CLK_DRV.xlsx)
 * >>   Combine easy mode and advanced mode.
 * #0.07_Brian_2020525 //bugNum_Authour_Date
 * >> Delete some description about wizard option.
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef _MG82F6D17_CLK_MID_H
#define _MG82F6D17_CLK_MID_H


/**
 *******************************************************************************
 * @brief       Easy select system clock and cpu clock.
 * @details
 * @param[in]   \_\_SELECT\_\_ : System Clock Selection.
 *  @arg\b      SYS_12M_CPU_12M : SYSCLK=CPUCLK=12MHz (Default)
 *  @arg\b      SYS_11M0592_CPU_11M0592 : SYSCLK=CPUCLK=11.059MHz
 *  @arg\b      SYS_CPU_ECKI : SYSCLK=CPUCLK=ECKI (External clock input (P6.0))
 *  @arg\b      SYS_32K_CPU_32K : SYSCLK=CPUCLK=32KHz
 *  @arg\b      SYS_24M_CPU_24M : SYSCLK=CPUCLK=24MHz
 *  @arg\b      SYS_22M118_CPU_22M118 : SYSCLK=CPUCLK=22.118MHz
 *  @arg\b      SYS_32M_CPU_16M : SYSCLK=32MHz, CPUCLK=16MHz
 *  @arg\b      SYS_48M_CPU_24M : SYSCLK=48MHz, CPUCLK=24MHz
 *  @arg\b      SYS_29M491_CPU_14M736 : SYSCLK=29.491MHz, CPUCLK=14.736MHz
 *  @arg\b      SYS_44M236_CPU_22M118 : SYSCLK=44.236MHz, CPUCLK=22.118MHz
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_CLK_EasySelect(SYS_12M_CPU_12M);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_Easy_Select
#define SYS_12M_CPU_12M          0x80001010
#define SYS_11M0592_CPU_11M0592  0x80001090
#define SYS_CPU_ECKI             0x80000310
#define SYS_32K_CPU_32K          0x00000210
#define SYS_24M_CPU_24M          0x80001450
#define SYS_22M118_CPU_22M118    0x800014D0
#define SYS_32M_CPU_16M          0x80001858
#define SYS_48M_CPU_24M          0x80001C58
#define SYS_29M491_CPU_14M736    0x800018D8
#define SYS_44M236_CPU_22M118    0x80001CD8
/// @endcond

//@del{
/* from Easy Mode Selection
BYTE[3] = u32B3(__SELECT__) = DCON0.75
BYTE[3] = u32B3(__SELECT__) = CKCON5.0
BYTE[2] = u32B2(__SELECT__) = CKCON3
BYTE[1] = u32B1(__SELECT__) = CKCON2
BYTE[0] = u32B0(__SELECT__) = CKCON0
*/
//@del}

#define __DRV_CLK_Easy_Select(__SELECT__)\
    MWT(\
        /* === Select CPU Range === */\
        __DRV_CLK_CPUCLK_FASTER_25MHz();\
        /* --------------------------------------- */\
        /* === Enable IHRCO 12MHz/11.0592MHz === */\
        __DRV_CLK_IHRCO_12MHz();\
        CKCON0 |= (u32B0(__SELECT__) & AFS);\
        /* --------------------------------------- */\
        /* === delay 32us time for stable === */\
        IFD=255;\
        do{IFD--;}while(IFD!=0);\
        /* --------------------------------------- */\
        /* === Setup OSCin === */\
        __DRV_CLK_OSCin_Select(u32B1(__SELECT__)&(OSCS1_P|OSCS0_P));\
        /* --------------------------------------- */\
        /* === Setup PLL, SYSCLK, CPUCLK === */\
        /* --- CKCON0 : ENCKM, CKMIS, CCKS, SCKS --- */\
        /*DRV_PageP_Write(CKCON0_P, u32B0(__SELECT__));*/\
        CKCON0=u32B0(__SELECT__);\
        /* --- CKCON5 : CKMS0 --- */\
        DRV_PageP_Write(CKCON5_P, (u32B3(__SELECT__)&CKMS0_P));\
        /* --------------------------------------- */\
        /* === delay 100us for stable === */\
        IFD=255;\
        do{IFD--;}while(IFD!=0);\
        do{IFD--;}while(IFD!=0);\
        do{IFD--;}while(IFD!=0);\
        do{IFD--;}while(IFD!=0);\
        /* --------------------------------------- */\
        /* === CKCON3.MCKD[1:0] === */\
        /*__DRV_CLK_MCKDO_Select(u32B2(__SELECT__)&(MCKD1_P|MCKD0_P));*/\
        /* === CKCON3.5=FWKP === */\
        /*__DRV_CLK_Wakeup_Select(u32B2(__SELECT__)&FWKP_P);*/\
        /* === CKCON3 : FWKP,MCKD[1:0] === */\
        DRV_PageP_Write(CKCON3_P, (u32B2(__SELECT__)&0x2C));\
        /* === CKCON2 : IHRCOE,MCKS[1:0],OSCS[1:0] === */\
        DRV_PageP_Write(CKCON2_P, (u32B1(__SELECT__)&0x1F));\
        __DRV_CLK_CPUCLK_Range_Select(u32B3(__SELECT__)&(HSE_P|HSE1_P));\
        /* --------------------------------------- */;\
    )


#endif  //_MG82F6D17_CLK_MID_H


