/**
 ******************************************************************************
 *
 * @file        MG82F6D17_CLK_DRV.H
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_Brian_20191223 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * #0.02_Brian_20200116 //bugNum_Authour_Date
 * #0.03_Brian_20200122 //bugNum_Authour_Date
 * #0.04_Brian_20200210 //bugNum_Authour_Date
 * >> Modify : IFD = (IFD & ~OSCin_ECKI_P60) | __SELECT__;\
 * >>     to > IFD &= ~OSCin_ECKI_P60;\
 * >>          IFD | =__SELECT__;\
 * >> Modify : for(IFD=0;IFD<253;IFD++);\
 * >>     to > IFD=255;\
 * >>          do{IFD--;}while(IFD!=0);\
 * >> Modify : CKCON0 = (CKCON0 & ~SYSCLK_MCKDO_DIV_128) | __SELECT__;\
 * >>     to > CKCON0 |= SYSCLK_MCKDO_DIV_128;\
 * >>          CKCON0 &= (~SYSCLK_MCKDO_DIV_128 | __SELECT__);\
 * #0.04_Brian_20200227 //bugNum_Authour_Date
 * >> Modify : __DRV_CLK_P60Mux_Select(__SELECT__) : Delete IFD for temp value
 * >>     to > AUXR0 &= ~P60_MCK_DIV_4;\
 * >>          AUXR0 |= __SELECT__;\
 * #0.05_Brian_20200327 //bugNum_Authour_Date
 * >> Divid MG82F6D17_CLK_DRV.h to 3 files
 * >>   MG82F6D17_CLK_Wizard.c : Wizard Table , Init Function
 * >>   MG82F6D17_CLK_DRV.h : DRV
 * >>   MG82F6D17_CLK_MID.h : Easy Function
 * >> MG82F6D17_CLK_DRV.h
 * >>   Modify __DRV_CLK_OSCin_Select(__SELECT__)
 * >>     if OSCin=ECKI, set P60=1 & open-drain mode
 * #0.07_Brian_2020525 //bugNum_Authour_Date
 * >> Rename ENABLE/DISABLE to MW_ENABLE/MW_DISABLE
 * #1.00_Brian_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef _MG82F6D17_CLK_DRV_H
#define _MG82F6D17_CLK_DRV_H

/**
 *******************************************************************************
 * @brief       Select IHRCO 12MHz
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_IHRCO_12MHz();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_IHRCO_12MHz()\
    MWT(\
        DRV_PageP_Read(CKCON2_P);\
        IFD |= IHRCOE_P;\
        DRV_PageP_Write(CKCON2_P, IFD);\
        CKCON0 &= ~AFS;\
    )



/**
 *******************************************************************************
 * @brief       Select IHRCO 11.059MHz
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_IHRCO_11M059KHz();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_IHRCO_11M059KHz()\
    MWT(\
        DRV_PageP_Read(CKCON2_P);\
        IFD |= IHRCOE_P;\
        DRV_PageP_Write(CKCON2_P, IFD);\
        CKCON0 |= AFS;\
    )



/**
 *******************************************************************************
 * @brief       Disable IHRCO
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_IHRCO_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_IHRCO_Disable()\
    MWT(\
        DRV_PageP_Read(CKCON2_P);\
        IFD &= ~IHRCOE_P;\
        DRV_PageP_Write(CKCON2_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       Select IHRCO 12MHz / 11.059MHz / Disable
 * @details
 * @param[in]   \_\_SELECT\_\_ : Select IHRCO 12MHz / 11.059MHz / Disable
 *  @arg\b      IHRCO_DISABLE : Disable internal high frequency.
 *  @arg\b      IHRCO_12MHz : Internal 12MHz input. (Default)
 *  @arg\b      IHRCO_11M059KHz : Internal 11.0592MHz input.
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_IHRCO_Select(IHRCO_12MHz);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_IHRCO_Select
#define IHRCO_DISABLE       0   /*!< Disable IHECO */
#define IHRCO_12MHz         1   /*!< Select IHECO clock select 12MHz */
#define IHRCO_11M059KHz     2   /*!< Select IHECO clock select 11.0952MHz */
/// @endcond

#define __DRV_CLK_IHRCO_Select(__SELECT__)\
    MWT(\
        DRV_PageP_Read(CKCON2_P);\
        __SELECT__ == IHRCO_DISABLE ? (IFD &= ~IHRCOE_P) : (IFD |= IHRCOE_P);\
        DRV_PageP_Write(CKCON2_P, IFD);\
        __SELECT__ == IHRCO_11M059KHz ? (CKCON0 |= AFS) : (CKCON0 &= ~AFS);\
    )



/**
 *******************************************************************************
 * @brief       OSCin source selection
 * @details
 * @param[in]   \_\_SELECT\_\_ : OSCin source selection.
 *  @arg\b      OSCin_IHRCO : Internal high frequency input. (12MHz/11.0592MHz) (Default)
 *  @arg\b      OSCin_ILRCO : Internal Low frequency input.
 *  @arg\b      OSCin_ECKI_P60 : External OSC from P6.0 (0~25MHz).
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_OSCin_Select(OSCin_IHRCO);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_OSCin_Select
#define OSCin_IHRCO     0x00                    /*!<  */
//#define OSCin_XTAL    OSCS0_P                 /*!< HW not support */
#define OSCin_ILRCO     OSCS1_P                 /*!<  */
#define OSCin_ECKI_P60  (OSCS1_P | OSCS0_P)     /*!<  */
/// @endcond

#define __DRV_CLK_OSCin_Select(__SELECT__)\
    MWT(\
        /* --------------------------------------- */\
        /* === Set P60 Open-Drain Output, P60=1 === */\
        (__SELECT__==OSCin_ECKI_P60) ? _push_(SFRPI) : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? __DRV_SFR_PageIndex(0x01) : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? P6M0&=~0x01 : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? P60=1 : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? SFRPI=0x03 : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? P6M1&=~0x01 : nop();\
        (__SELECT__==OSCin_ECKI_P60) ? _pop_(SFRPI) : nop();\
        /* --------------------------------------- */\
        DRV_PageP_Read(CKCON2_P);\
        IFD &= ~OSCin_ECKI_P60;\
        IFD |= __SELECT__;\
        DRV_PageP_Write(CKCON2_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       Multiplier (PLL) Enable
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CKM_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_PLL_Enable()      CKCON0 |= ENCKM



/**
 *******************************************************************************
 * @brief       Multiplier (PLL) Disable
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CKM_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_PLL_Disable()     CKCON0 &= ~ENCKM



/**
 *******************************************************************************
 * @brief       Multiplier (PLL) Enable / Disable
 * @details
 * @param[in]   \_\_STATE\_\_ : Multiplier Enable / Disable
 *  @arg\b      MW_ENABLE : Multiplier (PLL) Enable
 *  @arg\b      MW_DISALBE : Multiplier (PLL) Disable (Defulat)
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_PLL_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_PLL_Cmd(__STATE__)\
    MWT(\
        __STATE__ == MW_ENABLE ? (CKCON0 |= ENCKM) : (CKCON0 &= ~ENCKM);\
    )



/**
 *******************************************************************************
 * @brief       Multiplier (PLL) Config
 * @details
 * @param[in]   \_\_SELECT\_\_ : Clock multiplier input selection.
 *  @arg\b      CKM_OSCin_DIV_1 : OSCin=5~7MHz
 *  @arg\b      CKM_OSCin_DIV_2 : OSCin=10~14MHz (Default)
 *  @arg\b      CKM_OSCin_DIV_4 : OSCin=20~28MHz
 * @param[in]   \_\_MODE\_\_ : CKM mode selection.
 *  @arg\b      PLLI_X4X5X8 : Multiplier Mode Select X4/X5.33/X8 (Default)
 *  @arg\b      PLLI_X6X8X12 : Multiplier Mode Select X6/X8/X12
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_PLL_Config(CKM_OSCin_DIV_2, PLLI_X4X5X8);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_PLL_Config
#define CKM_OSCin_DIV_1                     0x00        /*!< OSCin=5~7MHz */
#define CKM_OSCin_DIV_2                     CKMIS0      /*!< OSCin=10~14MHz (Default) */
#define CKM_OSCin_DIV_4                     CKMIS1      /*!< OSCin=20~28MHz */
#define PLLI_X4X5X8                         0x00        /*!< Multiplier Mode Select X4 / X5.33 / X8 */
#define PLLI_X6X8X12                        CKMS0_P     /*!< Multiplier Mode Select X6 / X8 / X12 */
/// @endcond
#define __DRV_CLK_PLL_Config(__SELECT__, __MODE__)\
    MWT(\
        IFD = CKCON0;\
        IFD &= ~(CKMIS1|CKMIS0);\
        IFD |= __SELECT__;\
        CKCON0 = IFD;\
        DRV_PageP_Read(CKCON5_P);\
        __MODE__ == PLLI_X6X8X12 ? (IFD |= CKMS0_P ) : (IFD &= ~CKMS0_P);\
        DRV_PageP_Write(CKCON5_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       Multiplier clock select (MCK)
 * @details
 * @param[in]   \_\_SELECT\_\_ : MCK source selection.
 *  @arg\b      MCK_OSCin : OSCin (Default)
 *  @arg\b      MCK_CKMI_X4X6 : 24MHz/36MHz/22.118MHz/33.177MHz
 *  @arg\b      MCK_CKMI_X5X8 : 32MHz/48MHz/29.491MHz/44.236MHz
 *  @arg\b      MCK_CKMI_X8X12 : 48MHz/72MHz/44.236MHz/66.354MHz
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_MCK_Select(MCK_OSCin);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_MCK_Select
#define MCK_OSCin           0x00                    /*!<  */
#define MCK_CKMI_X4X6       MCKS0_P                 /*!<  */
#define MCK_CKMI_X5X8       MCKS1_P                 /*!< X5 is X5.33 */
#define MCK_CKMI_X8X12      (MCKS1_P | MCKS0_P)     /*!<  */
/// @endcond
#define __DRV_CLK_MCK_Select(__SELECT__)\
    MWT(\
        DRV_PageP_Read(CKCON2_P);\
        IFD &= ~MCK_CKMI_X8X12;\
        IFD |= __SELECT__;\
        DRV_PageP_Write(CKCON2_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       Select multi-clock divide output select (MCKDO)
 * @details
 * @param[in]   \_\_SELECT\_\_ : MCK divider output selection.
 *  @arg\b      MCKDO_MCK_DIV_1 : MCKDO = MCK / 1 (Default)
 *  @arg\b      MCKDO_MCK_DIV_2 : MCKDO = MCK / 2
 *  @arg\b      MCKDO_MCK_DIV_4 : MCKDO = MCK / 4
 *  @arg\b      MCKDO_MCK_DIV_8 : MCKDO = MCK / 8
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_MCKDO_Select(MCKDO_MCK_DIV_1);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_MCKDO_Select
#define MCKDO_MCK_DIV_1     0x00                    /*!<  */
#define MCKDO_MCK_DIV_2     MCKD0_P                 /*!<  */
#define MCKDO_MCK_DIV_4     MCKD1_P                 /*!<  */
#define MCKDO_MCK_DIV_8     (MCKD1_P | MCKD0_P)     /*!<  */
/// @endcond
#define __DRV_CLK_MCKDO_Select(__SELECT__)\
    MWT(\
        DRV_PageP_Read(CKCON3_P);\
        IFD &= ~MCKDO_MCK_DIV_8;\
        IFD |= __SELECT__;\
        DRV_PageP_Write(CKCON3_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       Select system clock divide from MCKDO (SYSCLK)
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable system clock selection.
 *  @arg\b      SYSCLK_MCKDO_DIV_1 : SYSCLK = MCKDO / 1 (Default)
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 2
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 4
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 8
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 16
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 32
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 64
 *  @arg\b      SYSCLK_MCKDO_DIV_2 : SYSCLK = MCKDO / 128
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_SYSCLK_Select(SYSCLK_MCKDO_DIV_1);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_SYSCLK_Select
#define SYSCLK_MCKDO_DIV_1          0x00                        //SYSCLK = MCKDO / 1 (Default)
#define SYSCLK_MCKDO_DIV_2          SCKS0                       //SYSCLK = MCKDO / 2
#define SYSCLK_MCKDO_DIV_4          SCKS1                       //SYSCLK = MCKDO / 4
#define SYSCLK_MCKDO_DIV_8          (SCKS1 | SCKS0)             //SYSCLK = MCKDO / 8
#define SYSCLK_MCKDO_DIV_16         SCKS2                       //SYSCLK = MCKDO / 16
#define SYSCLK_MCKDO_DIV_32         (SCKS2 | SCKS0)             //SYSCLK = MCKDO / 32
#define SYSCLK_MCKDO_DIV_64         (SCKS2 | SCKS1)             //SYSCLK = MCKDO / 64
#define SYSCLK_MCKDO_DIV_128        (SCKS2 | SCKS1 | SCKS0)     //SYSCLK = MCKDO / 128
/// @endcond
#define __DRV_CLK_SYSCLK_Select(__SELECT__)\
    MWT(\
        CKCON0 |= SYSCLK_MCKDO_DIV_128;\
        CKCON0 &= (~SYSCLK_MCKDO_DIV_128 | __SELECT__);\
    )



/**
 *******************************************************************************
 * @brief       Select CPU clock divide from system clock (CPUCLK)
 * @details
 * @param[in]   \_\_SELECT\_\_ : CPU clock select.
 *  @arg\b      CPUCLK_SYSCLK_DIV_1 : CPUCLK = SYSCLK = MCKDO / 1 (Default)
 *  @arg\b      CPUCLK_SYSCLK_DIV_2 : CPUCLK = SYSCLK = MCKDO / 2
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_Select(CPUCLK_SYSCLK_DIV_1);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_CPUCLK_Select
#define CPUCLK_SYSCLK_DIV_1     0x00    /*!<  */
#define CPUCLK_SYSCLK_DIV_2     CCKS    /*!<  */
/// @endcond
#define __DRV_CLK_CPUCLK_Select(__SELECT__)\
    MWT(\
        (__SELECT__ == CPUCLK_SYSCLK_DIV_2 ? (CKCON0 |= CCKS) : (CKCON0 &= ~CCKS));\
    )



/**
 *******************************************************************************
 * @brief       Select CPU clock = system clock / 1
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_SYSCLK_DIV_1();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CPUCLK_SYSCLK_DIV_1()     CKCON0 = CKCON0 & (~CCKS)



/**
 *******************************************************************************
 * @brief       Select CPU clock = system clock / 2
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_SYSCLK_DIV_2();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CPUCLK_SYSCLK_DIV_2()     CKCON0 = CKCON0 | CCKS



/**
 *******************************************************************************
 * @brief       MCU CPUCLK range config
 * @details
 * @param[in]   \_\_SELECT\_\_ : CPUCLK range config.
 *  @arg\b      FASTER_CPUCLK : CPUCLK > 25MHz
 *  @arg\b      MIDDLE_CPUCLK : 25MHz > CPUCLK > 6MHz
 *  @arg\b      SLOWER_CPUCLK : CPUCLK < 6MHz or CPUCLK = 6MHz
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_Range_Select(MIDDLE_CPUCLK);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_CPUCLK_Range_Select
#define SLOWER_CPUCLK       0x00                /*!<  */
#define MIDDLE_CPUCLK       HSE_P               /*!<  */
#define FASTER_CPUCLK       (HSE_P | HSE1_P)    /*!<  */
/// @endcond
#define __DRV_CLK_CPUCLK_Range_Select(__SELECT__)\
    MWT(\
        DRV_PageP_Read (DCON0_P);\
        IFD &= ~FASTER_CPUCLK;\
        IFD |= __SELECT__;\
        DRV_PageP_Write(DCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       MCU for ultra-high speed operation (CPUCLK > 25MHz)
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_FASTER_25MHz();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CPUCLK_FASTER_25MHz()\
    MWT(\
        DRV_PageP_Read (DCON0_P);\
        IFD |= (HSE_P | HSE1_P);\
        DRV_PageP_Write(DCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       MCU full speed operation (25MHz > CPUCLK > 6MHz)
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_6MHz_to_24MHz();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CPUCLK_6MHz_to_24MHz()\
    MWT(\
        DRV_PageP_Read (DCON0_P);\
        IFD |= HSE_P;\
        IFD &= ~HSE1_P;\
        DRV_PageP_Write(DCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       CPU running in lower speed mode (6MHz > CPUCLK)
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CPUCLK_SLOWER_6MHz();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CPUCLK_SLOWER_6MHz()\
    MWT(\
        DRV_PageP_Read (DCON0_P);\
        IFD &= ~(HSE_P | HSE1_P);\
        DRV_PageP_Write(DCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       MCU normal wakeup from power down.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_NormalWakeup120us();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_NormalWakeup120us()\
    MWT(\
        DRV_PageP_Read(CKCON3_P);\
        IFD &= ~FWKP_P;\
        DRV_PageP_Write(CKCON3_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       MCU fast wakeup from power down.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_FastWakeup30us();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_FastWakeup30us()\
    MWT(\
        DRV_PageP_Read(CKCON3_P);\
        IFD |= FWKP_P;\
        DRV_PageP_Write(CKCON3_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       MCU wakeup select from power down.
 * @details
 * @param[in]   \_\_SELECT\_\_ : Power down wakeup select
 *  @arg\b      NORMAL_WAKEUP_120us : Power down normal wakeup. (120us)(Default)
 *  @arg\b      FAST_WAKEUP_30us : Power down fast wakeup. (30us)
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_Wakeup_Select(NORMAL_WAKEUP_120us);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_Wakeup_Select
#define NORMAL_WAKEUP_120us     0x00
#define FAST_WAKEUP_30us        FWKP_P
/// @endcond
#define __DRV_CLK_Wakeup_Select(__SELECT__)\
    MWT(\
        DRV_PageP_Read (CKCON3_P);\
        __SELECT__ == FAST_WAKEUP_30us ? (IFD |= FWKP_P ):(IFD &= ~FWKP_P);\
        DRV_PageP_Write(CKCON3_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       P6.0 multi-function select
 * @param[in]   \_\_SELECT\_\_ : P6.0 multi-function
 *  @arg\b      P60_GPIO : P6.0 is GPIO function (Default)
 *  @arg\b      P60_MCK_DIV_1 : P6.0 output MCK/1
 *  @arg\b      P60_MCK_DIV_2 : P6.0 output MCK/2
 *  @arg\b      P60_MCK_DIV_4 : P6.0 output MCK/4
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_P60_Select(P60_MCK_DIV_1);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_CLK_P60Mux_Select
#define P60_GPIO            0x00
#define P60_MCK_DIV_1       P60OC0
#define P60_MCK_DIV_2       P60OC1
#define P60_MCK_DIV_4       (P60OC0 | P60OC1)
/// @endcond
#define __DRV_CLK_P60Mux_Select(__SELECT__)\
    MWT(\
        AUXR0 &= ~P60_MCK_DIV_4;\
        AUXR0 |= __SELECT__;\
    )



/**
 *******************************************************************************
 * @brief       Enable P60 fast driving.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_P60FastDrive_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_P60FastDrive_Enable()     AUXR0 |= P60FD



/**
 *******************************************************************************
 * @brief       Disable P60 fast driving.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_P60FastDrive_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_P60FastDrive_Disable()    AUXR0 &= ~P60FD



/**
 *******************************************************************************
 * @brief       P60 fast drive Enable / Disalbe.
 * @details
 * @param[in]   \_\_STATE\_\_ : Enable / Disable
 *  @arg\b      MW_ENABLE : P6.0 fast drive enable.
 *  @arg\b      MW_DISALBE : P6.0 fast drive disable. (Defulat)
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_P60FastDrive_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_P60FastDrive_Cmd(__STATE__)\
    MWT(\
        __STATE__ == MW_ENABLE ? (AUXR0 |= P60FD ):(AUXR0 &= ~P60FD);\
    )



/**
 *******************************************************************************
 * @brief       CKCON0 unProtected (Defulat)
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CKCON0_UnProtected();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CKCON0_UnProtected()\
    MWT(\
        DRV_PageP_Read (SPCON0_P);\
        IFD &= ~CKCTL0_P;\
        DRV_PageP_Write(SPCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       CKCON0 Protected
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CKCON0_Protected();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CKCON0_Protected()\
    MWT(\
        DRV_PageP_Read (SPCON0_P);\
        IFD |= CKCTL0_P;\
        DRV_PageP_Write(SPCON0_P, IFD);\
    )



/**
 *******************************************************************************
 * @brief       CKCON0 access control
 * @details
 * @param[in]   \_\_STATE\_\_ : Enable / Disable
 *  @arg\b      MW_ENABLE : CKCON0 Protected
 *  @arg\b      MW_DISALBE : CKCON0 unProtected (Defulat)
 * @return      None
 * @note
 * @par         Example
 * @code
 *  __DRV_CLK_CKCON_Protection_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_CLK_CKCON_Protection_Cmd(__STATE__)\
    MWT(\
        DRV_PageP_Read (SPCON0_P);\
        __STATE__== MW_ENABLE ? (IFD |= CKCTL0_P) : (IFD &= ~CKCTL0_P);\
        DRV_PageP_Write(SPCON0_P, IFD);\
    )



#endif  //_MG82F6D17_CLK_DRV_H


