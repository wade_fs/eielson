/**
 ******************************************************************************
 *
 * @file        MG82F6D17_DMA_DRV.H
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     v1.01
 * @date        2020/09/01
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 * #1.01_Kevin_20200901 //bugNum_Authour_Date
 * >> Added DMA DCF0/TF5/TF6 interrupt Enable/Disable Cmd
 * >> Cancel Timer5/Timer6 Clock & Gating Source Select macro 
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

 
#ifndef _MG82F6D17_DMA_DRV_H
#define _MG82F6D17_DMA_DRV_H



/**
 *******************************************************************************
 * @brief       Enable DMA Hardware
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_Enable()\
    MWT(\
        DMACR0 |= DMAE0;\
    )


/**
 *******************************************************************************
 * @brief       Disable DMA Hardware
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_Disable()\
    MWT(\
        DMACR0 &= ~DMAE0;\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable DMA Hardware
 * @details
 * @param[in]   \_\_STATE\_\_ : config DMA hardware control bit
 *  @arg\b      MW_DISABLE : Set DMA hardware disable (default)
 *  @arg\b      MW_ENABLE : Set DMA hardware enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_Cmd(__STATE__)\
        __STATE__==MW_ENABLE?(DMACR0 |= DMAE0):(DMACR0 &= ~DMAE0)


/**
 *******************************************************************************
 * @brief       Enable DMA Hardware Interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_IT_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_IT_Enable()\
    MWT(\
        EDMA = MW_ENABLE;\
    )



/**
 *******************************************************************************
 * @brief       Disable DMA Hardware Interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_IT_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_IT_Disable()\
    MWT(\
        EDMA = MW_DISABLE;\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable DMA Hardware Interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config DMA hardware interrupt control bit
 *  @arg\b      MW_DISABLE : Set DMA hardware interrupt disable (default)
 *  @arg\b      MW_ENABLE : Set DMA hardware interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_IT_Cmd(__STATE__)\
    MWT(\
        EDMA =__STATE__;\
    )





/**
 *******************************************************************************
 * @brief       Enable/Disable DMA DCF0 Interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config DMA DCF0 hardware interrupt control bit
 *  @arg\b      MW_DISABLE : Set DMA DCF0 hardware interrupt disable (default)
 *  @arg\b      MW_ENABLE : Set DMA DCF0 hardware interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_DCF0_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_DCF0_IT_Cmd(__STATE__)\
        __STATE__==MW_ENABLE?(DMACR0 |= DIE0):(DMACR0 &= ~DIE0)




/**
 *******************************************************************************
 * @brief       Enable/Disable DMA TF5 Interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config DMA TF5 hardware interrupt control bit
 *  @arg\b      MW_DISABLE : Set DMA TF5 hardware interrupt disable (default)
 *  @arg\b      MW_ENABLE : Set DMA TF5 hardware interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_TF5_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_TF5_IT_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        T5IE =__STATE__;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Enable/Disable DMA TF6 Interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config DMA TF6 hardware interrupt control bit
 *  @arg\b      MW_DISABLE : Set DMA TF6 hardware interrupt disable (default)
 *  @arg\b      MW_ENABLE : Set DMA TF6 hardware interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_TF6_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_TF6_IT_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(4);\
        T6IE =__STATE__;\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Set DMA Transfer Start
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_SetTransferStart();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_SetTransferStart()\
    MWT(\
        DMACR0 |= DMAS0;\
    )


/**
 *******************************************************************************
 * @brief       Set DMA Transfer Suspend
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_SetTransferSuspend();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_SetTransferSuspend()\
    MWT(\
        DMACR0 &= ~DMAS0;\
    )




/**
 *******************************************************************************
 * @brief       Clear DMA Complete Flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_ClearCompleteFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_DMA_ClearCompleteFlag()\
    MWT(\
        DMACR0 &= ~DCF0;\
    )




/**
 *******************************************************************************
 * @brief       Polling for DMA transfer complete.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_PollForTransferComplete();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define   __DRV_DMA_PollForTransferComplete()\
          while((DMACR0 & DCF0) == DCF0)\



/**
 *******************************************************************************
 * @brief       Select DMA loop mode operation
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA loop mode operation
 *  @arg\b      MW_DISABLE:Set DMA loop mode disable
 *  @arg\b      MW_ENABLE:Set DMA loop mode enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_LoopMode_Select(MW_DISABLE);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_DMA_LoopMode_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        DMACG0 &= ~LOOP0;\
        DMACG0 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Select DMA data copy to CRC16 
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA data is copied to CRC16 concurrnetly
 *  @arg\b      DMA_DataToCRC16_DISABLE: Disable the DMA data is copied to CRC16
 *  @arg\b      DMA_DataToCRC16_ENABLE: Enable the DMA data is copied to CRC16
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_CRC16_Write_Select(DMA_DataToCRC16_DISABLE);
 * @endcode
 * @bug
 *******************************************************************************
*/
/// @cond __DRV_DMA_CRC16_Write_Select
#define DMA_DataToCRC16_DISABLE       0x00
#define DMA_DataToCRC16_ENABLE        CRCW0
/// @endcond

#define __DRV_DMA_CRC16_Write_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        DMACG0 &= ~CRCW0;\
        DMACG0 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )

/**
 *******************************************************************************
 * @brief       Enable DMA data copy to CRC16 
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_CRC16_Write_Enable();
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_DMA_CRC16_Write_Enable()\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        DMACG0 |= CRCW0;\
        __DRV_SFR_PageIndex(0);\
    )
    
    
/**
 *******************************************************************************
 * @brief       Disable DMA data copy to CRC16 
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_CRC16_Write_Disable();
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_DMA_CRC16_Write_Disable()\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        DMACG0 &= ~CRCW0;\
        __DRV_SFR_PageIndex(0);\
    )




/**
 *******************************************************************************
 * @brief       Select DMA External Trigger Source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA external trigger source
 *  @arg\b      DMA_EXTERNAL_TRIGGER_SOURCE_DISABLED : Set DMA external trigger source to disabled (Default)
 *  @arg\b      DMA_EXTERNAL_TRIGGER_SOURCE_INT2ET : Set DMA external trigger source to INT2ET
 *  @arg\b      DMA_EXTERNAL_TRIGGER_SOURCE_KBIET : Set DMA external trigger source to KBIET
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_ExternalTriggerSource_Select(DMA_EXTERNAL_TRIGGER_SOURCE_INT2ET);
 * @endcode
 * @bug
 *******************************************************************************
*/
/// @cond __DRV_DMA_ExternalTriggerSource_Select
#define DMA_EXTERNAL_TRIGGER_SOURCE_DISABLED     0x00
#define DMA_EXTERNAL_TRIGGER_SOURCE_INT2ET       EXTS00
#define DMA_EXTERNAL_TRIGGER_SOURCE_KBIET        (EXTS10 | EXTS00)
/// @endcond
#define __DRV_DMA_ExternalTriggerSource_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(8);\
        DMACG0 &= ~DMA_EXTERNAL_TRIGGER_SOURCE_KBIET;\
        DMACG0 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Select DMA Data Source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA data is copied to CRC16 concurrnetly
 *  @arg\b      DMA_DATA_SOURCE_DISABLED : Set DMA data source to disabled (Default)
 *  @arg\b      DMA_DATA_SOURCE_S0_RX : Set DMA data source to S0 RX
 *  @arg\b      DMA_DATA_SOURCE_S1_RX : Set DMA data source to S1 RX
 *  @arg\b      DMA_DATA_SOURCE_I2C0_RX : Set DMA data source to I2C0 RX
 *  @arg\b      DMA_DATA_SOURCE_SPI0_RX : Set DMA data source to SPI0 RX
 *  @arg\b      DMA_DATA_SOURCE_ADC0 : Set DMA data source to ADC0
 *  @arg\b      DMA_DATA_SOURCE_XRAM : Set DMA data source to XRAM
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_Source_Select(DMA_DATA_SOURCE_XRAM);
 * @endcode
 * @bug
 *******************************************************************************
*/
/// @cond __DRV_DMA_Source_Select
#define DMA_DATA_SOURCE_DISABLED     0x00
#define DMA_DATA_SOURCE_S0_RX        DSS00
#define DMA_DATA_SOURCE_S1_RX        DSS10
#define DMA_DATA_SOURCE_I2C0_RX     (DSS20 | DSS00)
#define DMA_DATA_SOURCE_SPI0_RX     (DSS20 | DSS10 | DSS00)
#define DMA_DATA_SOURCE_ADC0        (DSS30 | DSS00)
#define DMA_DATA_SOURCE_XRAM        (DSS30 | DSS20 | DSS10 | DSS00)
/// @endcond
#define __DRV_DMA_Source_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(9);\
        DMADS0 &= ~DMA_DATA_SOURCE_XRAM;\
        DMADS0 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Select DMA Data Destination
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA data is copied to CRC16 concurrnetly
 *  @arg\b      DMA_DATA_DESTINATION_DISABLED : Set DMA data destination to disabled (Default)
 *  @arg\b      DMA_DATA_DESTINATION_S0_RX : Set DMA data destination to S0 RX
 *  @arg\b      DMA_DATA_DESTINATION_S1_RX : Set DMA data destination to S1 RX
 *  @arg\b      DMA_DATA_DESTINATION_I2C0_RX : Set DMA data destination to I2C0 RX
 *  @arg\b      DMA_DATA_DESTINATION_SPI0_RX : Set DMA data destination to SPI0 RX
 *  @arg\b      DMA_DATA_DESTINATION_CRC : Set DMA data destination to CRC
 *  @arg\b      DMA_DATA_DESTINATION_XRAM : Set DMA data destination to XRAM
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_Destination_Select(DMA_DATA_DESTINATION_I2C0_RX);
 * @endcode
 * @bug
 *******************************************************************************
*/
/// @cond __DRV_DMA_Destination_Select
#define DMA_DATA_DESTINATION_DISABLED     0x00
#define DMA_DATA_DESTINATION_S0_RX        DDS00
#define DMA_DATA_DESTINATION_S1_RX        DDS10
#define DMA_DATA_DESTINATION_I2C0_RX     (DDS20 | DDS00)
#define DMA_DATA_DESTINATION_SPI0_RX     (DDS20 | DDS10 | DDS00)
#define DMA_DATA_DESTINATION_CRC        (DDS30 | DDS20 | DDS00)
#define DMA_DATA_DESTINATION_XRAM        (DDS30 | DDS20 | DDS10 | DDS00)
/// @endcond
#define __DRV_DMA_Destination_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(9);\
        DMADS0 &= ~DMA_DATA_DESTINATION_XRAM;\
        DMADS0 |= __SELECT__;\
        __DRV_SFR_PageIndex(0);\
    )




/**
 *******************************************************************************
 * @brief       Set Timer5 DMA to XRAM Data Length Counter 
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA data length counter value
 *  @arg\b      0x0000~0x02FF : Set DMA to XRAM data length counter value
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_SetDataLength(0x00ff);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_DMA_SetDataLength(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        TL5 = TLR5 = ~LOBYTE(__SELECT__);\
        TH5 = THR5 = ~HIBYTE(__SELECT__);\
        __DRV_SFR_PageIndex(0);\
    )




/**
 *******************************************************************************
 * @brief       Set Timer6 DMA to XRAM Address Pointer Counter 
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable DMA to XRAM Address Pointer counter value
 *  @arg\b      0x0000~0x02FF : Set DMA to XRAM Address Pointer counter value
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_DMA_SetXRAMAddressPointer(0x00ff);
 * @endcode
 * @bug
 *******************************************************************************
*/

#define __DRV_DMA_SetXRAMAddressPointer(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(4);\
        TL6 = TLR6 = LOBYTE(__SELECT__);\
        TH6 = THR6 = HIBYTE(__SELECT__);\
        __DRV_SFR_PageIndex(0);\
    )




#endif  //_MG82F6D17_DMA_DRV_H