/**
 ******************************************************************************
 *
 * @file        MG82F6D17_I2C0_DRV.H
 *
 * @brief       This is the C code format driver head file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     v1.00
 * @date        2020/07/10
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 *      The Demo software is provided "AS IF"  without any warranty, either
 *      expressed or implied, including, but not limited to, the implied warranties
 *      of merchantability and fitness for a particular purpose.  The author will
 *      not be liable for any special, incidental, consequential or indirect
 *      damages due to loss of data or any other reason.
 *      These statements agree with the world wide and local dictated laws about
 *      authorship and violence against these laws.
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.07_Kevin_20200525 //bugNum_Authour_Date
 * #1.00_Kevin_20200710 //bugNum_Authour_Date
 * >> Initial Version
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

 
#ifndef _MG82F6D17_I2C0_DRV_H
#define _MG82F6D17_I2C0_DRV_H


/**
 *******************************************************************************
 * @brief       Enable I2C0 Hardware
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Enable()\
    MWT(\
        SICON |= ENSI;\
    )


/**
 *******************************************************************************
 * @brief       Disable I2C0 Hardware
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Disable()\
    MWT(\
        SICON &= ~ENSI;\
    )


/**
 *******************************************************************************
 * @brief       Enable/Disable I2C0 Hardware
 * @details
 * @param[in]   \_\_STATE\_\_ : config I2C0 hardware control bit
 *  @arg\b      MW_DISABLE : Set I2C0 hardware disable (Default)
 *  @arg\b      MW_ENABLE : Set I2C0 hardware enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Cmd(__STATE__)\
        __STATE__==MW_ENABLE?(SICON |= ENSI):(SICON &= ~ENSI)



/**
 *******************************************************************************
 * @brief       Enable I2C0 Hardware Interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_IT_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_IT_Enable()\
    MWT(\
        EIE1 |= ETWI0;\
    )



/**
 *******************************************************************************
 * @brief       Disable I2C0 Hardware Interrupt
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_IT_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_IT_Disable()\
    MWT(\
        EIE1 &= ~ETWI0;\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable I2C0 Hardware Interrupt
 * @details
 * @param[in]   \_\_STATE\_\_ : config I2C0 hardware interrupt control bit
 *  @arg\b      MW_DISABLE : Set I2C0 hardware interrupt disable (Default)
 *  @arg\b      MW_ENABLE : Set I2C0 hardware interrupt enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_IT_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_IT_Cmd(__STATE__)\
        __STATE__==MW_ENABLE?(EIE1 |= ETWI0):(EIE1 &= ~ETWI0)


/**
 *******************************************************************************
 * @brief       Set I2C0 START flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetSTART();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_SetSTART()\
    MWT(\
        SICON |= STA;\
    )


/**
 *******************************************************************************
 * @brief       Clear I2C0 START flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_ClearSTART();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_ClearSTART()\
    MWT(\
        SICON &= ~STA;\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STOP flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetSTOP();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_SetSTOP()\
    MWT(\
        SICON |= STO;\
    )


/**
 *******************************************************************************
 * @brief       Polling for I2C0 stop event conversion complete.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PollForStopComplete();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_PollForStopComplete()\
    MWT(\
        while((SICON & STO) == STO);\
    )



/**
 *******************************************************************************
 * @brief        Set I2C0 state event flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetEventFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_SetEventFlag()\
    MWT(\
        SICON |= SI;\
    )


/**
 *******************************************************************************
 * @brief       Clear I2C0 state event flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_ClearEventFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_ClearEventFlag()\
    MWT(\
        SICON &= ~SI;\
    )


/**
 *******************************************************************************
 * @brief       Get I2C0 state event flag
 * @details
 * @return      SI : return SI bit status
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GetEventFlag();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_GetEventFlag()    SICON & SI



/**
 *******************************************************************************
 * @brief       Polling for I2C0 state event conversion complete.
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PollForStateComplete();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_PollForStateComplete()\
    MWT(\
          while((SICON & SI) != SI);\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 ACK flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetACK();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_SetACK()\
    MWT(\
        SICON |= AA;\
    )


/**
 *******************************************************************************
 * @brief       Clear I2C0 ACK flag
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetACK();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_ClearACK()\
    MWT(\
        SICON &= ~AA;\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 000
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_000();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_000()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 001
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_001();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_001()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
      SICON |= AA;\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 010
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_010();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_010()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
      SICON |= STO;\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 100
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_100();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_100()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
      SICON |= STA;\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 101
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_101();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_101()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
      SICON |= (STA | AA);\
    )


/**
 *******************************************************************************
 * @brief       Set I2C0 STA STO AA 110
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_Set_STA_STO_AA_110();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_Set_STA_STO_AA_110()\
    MWT(\
      SICON &= ~(STA | STO | AA);\
      SICON |= (STA | STO);\
    )


/**
 *******************************************************************************
 * @brief       Enable I2C0 GC Function
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GC_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_GC_Enable()\
    MWT(\
        SIADR |= GC;\
    )


/**
 *******************************************************************************
 * @brief       Disable I2C0 GC Function
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GC_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_GC_Disable()\
    MWT(\
        SIADR &= ~GC;\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable I2C0 GC Function
 * @details
 * @param[in]   \_\_STATE\_\_ : config I2C0 GC functiont control bit
 *  @arg\b      MW_DISABLE : Set I2C0 GC functiont disable (default)
 *  @arg\b      MW_ENABLE : Set I2C0 GC functiont enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GC_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_GC_Cmd(__STATE__)\
        __STATE__==MW_ENABLE?(SIADR |= GC):(SIADR &= ~GC)



/**
 *******************************************************************************
 * @brief       Get I2C0 state event code
 * @details
 * @return      SISTA : return SISTA status
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GetEventCode();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_GetEventCode()    SISTA



/**
 *******************************************************************************
 * @brief       Set I2C0 slave device address 
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable I2C0 slave address
 *  @arg\b      0x00~0xFF : Set the slave device address to SIADR (even)
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetSlaveAddress(0x00~0xFF);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_SetSlaveAddress(__SELECT__)\
    MWT(\
        SIADR |= (__SELECT__<<1)&~GC;\
    )
    


/**
 *******************************************************************************
 * @brief       Set I2C0 slave device address write
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable I2C0 slave address write
 *  @arg\b      0x00~0xFF : Set the slave device address to SIDAT (even)
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetAddressWrite(0x00~0xFF);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_SetAddressWrite(__SELECT__)\
    MWT(\
        SIDAT = __SELECT__&0xFE;\
    )



/**
 *******************************************************************************
 * @brief       Set I2C0 slave device address read
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable I2C0 slave address read
 *  @arg\b      0x00~0xFF : Set the slave device address to SIDAT (even)
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetAddressRead(0x00~0xFF);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_SetAddressRead(__SELECT__)\
    MWT(\
        SIDAT = __SELECT__|0x01;\
    )



/**
 *******************************************************************************
 * @brief       Set I2C0 data
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable I2C0 buffer data
 *  @arg\b      0x00~0xFF : Set the data to SIDAT 
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_SetBufData(0x00~0xFF);
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_SetBufData(__SELECT__)\
    MWT(\
        SIDAT = __SELECT__;\
    )


/**
 *******************************************************************************
 * @brief       Get I2C0 data
 * @details
 * @return      SIDAT : return I2C0 data
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_GetBufData();
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_GetBufData() SIDAT




/**
 *******************************************************************************
 * @brief       Check I2C0 Bus Idle State
 * @details 
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_State_BusIdle();
 * @endcode
 * @bug
 *******************************************************************************
*/
#define __DRV_I2C0_State_BusIdle()\
    MWT(\
          while(SISTA != 0xF8);\
    )


/**
 *******************************************************************************
 * @brief       Select I2C0 clock rate divide from SYSCLK
 * @details
 * @param[in]   \_\_SELECT\_\_ : Programmable I2C0 clock rate selection.
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_8 : I2C0_SCL = SYSCLK / 8 (Default)
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_16 : I2C0_SCL = SYSCLK / 16
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_32 : I2C0_SCL = SYSCLK / 32
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_64 : I2C0_SCL = SYSCLK / 64
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_128 : I2C0_SCL = SYSCLK / 128
 *  @arg\b      I2C0_SCL_SYSCLK_DIV_256 : I2C0_SCL = SYSCLK / 256
 *  @arg\b      I2C0_SCL_S0TOF_DIV_6 : I2C0_SCL = S0TOF / 6
 *  @arg\b      I2C0_SCL_T0OF_DIV_6 : I2C0_SCL = T0OF / 6
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_ClockRate_Select(I2C0_SCL_SYSCLK_DIV_16);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_I2C0_ClockRate_Select
#define I2C0_SCL_SYSCLK_DIV_8          0x00                        //I2C0_SCL = SYSCLK / 8 (Default)
#define I2C0_SCL_SYSCLK_DIV_16         CR0                         //I2C0_SCL = SYSCLK / 16
#define I2C0_SCL_SYSCLK_DIV_32         CR1                         //I2C0_SCL = SYSCLK / 32
#define I2C0_SCL_SYSCLK_DIV_64         (CR1 | CR0)                 //I2C0_SCL = SYSCLK / 64
#define I2C0_SCL_SYSCLK_DIV_128        CR2                         //I2C0_SCL = SYSCLK / 128
#define I2C0_SCL_SYSCLK_DIV_256        (CR2 | CR0)                 //I2C0_SCL = SYSCLK / 256
#define I2C0_SCL_S0TOF_DIV_6           (CR2 | CR1)                 //I2C0_SCL = S0TOF / 6
#define I2C0_SCL_T0OF_DIV_6            (CR2 | CR1 | CR0)           //I2C0_SCL = T0OF / 6
/// @endcond
#define __DRV_I2C0_ClockRate_Select(__SELECT__)\
    MWT(\
        SICON &= ~I2C0_SCL_T0OF_DIV_6;\
        SICON |= __SELECT__;\
    )




/**
 *******************************************************************************
 * @brief       Set I2C0 input pin source
 * @details
 * @param[in]   \_\_SELECT\_\_ : Set external I2C0 input pin source
 *  @arg\b      I2C0_INPUT_PIN_SELECT_P31_P30 : Set I2C0 input pin source to SCL=P31, SDA=P30 (Default)
 *  @arg\b      I2C0_INPUT_PIN_SELECT_P60_P61 : Set I2C0 input pin source to SCL=P60, SDA=P61
 *  @arg\b      I2C0_INPUT_PIN_SELECT_P30_P31 : Set I2C0 input pin source to SCL=P30, SDA=P31
 *  @arg\b      I2C0_INPUT_PIN_SELECT_P22_P24 : Set I2C0 input pin source to SCL=P22, SDA=P24
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PinMux_Select(I2C0_INPUT_PIN_SELECT_P31_P30);
 * @endcode
 * @bug
 *******************************************************************************
 */
/// @cond __DRV_I2C0_PinMux_Select
#define I2C0_INPUT_PIN_SELECT_P31_P30     0x00
#define I2C0_INPUT_PIN_SELECT_P60_P61     TWIPS0
#define I2C0_INPUT_PIN_SELECT_P30_P31     TWIPS1
#define I2C0_INPUT_PIN_SELECT_P22_P24     (TWIPS1 | TWIPS0)
/// @endcond
#define __DRV_I2C0_PinMux_Select(__SELECT__)\
    MWT(\
        __DRV_SFR_PageIndex(0);\
        AUXR3 &= ~I2C0_INPUT_PIN_SELECT_P22_P24;\
        AUXR3 |= __SELECT__;\
    )



/**
 *******************************************************************************
 * @brief       Enable I2C0 Pre-Assert Acknowledge for DMA Application
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PAA_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_PAA_Enable()\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        AUXR10 |= PAA;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Disable I2C0 Pre-Assert Acknowledge for DMA Application
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PAA_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_PAA_Disable()\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        AUXR10 &= ~PAA;\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable I2C0 Pre-Assert Acknowledge for DMA Application
 * @details
 * @param[in]   \_\_STATE\_\_ : config I2C0 Pre-Assert Acknowledge control bit
 *  @arg\b      MW_DISABLE : Set I2C0 Pre-Assert Acknowledge disable
 *  @arg\b      MW_ENABLE : Set I2C0 Pre-Assert Acknowledge enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_PAA_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_PAA_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__==MW_ENABLE?(AUXR10 |= PAA):(AUXR10 &= ~PAA);\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Enable I2C0 TWICF function
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_TWICF_Enable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_TWICF_Enable()\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        AUXR10 |= TWICF;\
        __DRV_SFR_PageIndex(0);\
    )


/**
 *******************************************************************************
 * @brief       Disable I2C0 TWICF function
 * @details
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_TWICF_Disable();
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_TWICF_Disable()\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        AUXR10 &= ~TWICF;\
        __DRV_SFR_PageIndex(0);\
    )



/**
 *******************************************************************************
 * @brief       Enable/Disable I2C0 TWICF function
 * @details
 * @param[in]   \_\_STATE\_\_ : config I2C0 TWICF function control bit
 *  @arg\b      MW_DISABLE : Set I2C0 TWICF function disable
 *  @arg\b      MW_ENABLE : Set I2C0 TWICF function enable
 * @return      None
 * @note
 * @par         Example
 * @code
 *    __DRV_I2C0_TWICF_Cmd(MW_ENABLE);
 * @endcode
 * @bug
 *******************************************************************************
 */
#define __DRV_I2C0_TWICF_Cmd(__STATE__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        __STATE__==MW_ENABLE?(AUXR10 |= TWICF):(AUXR10 &= ~TWICF);\
        __DRV_SFR_PageIndex(0);\
    )



void Sample_I2C0_ISR_Handle(void);
void Sample_I2C0_Master_Byte_Write(uint8_t I2C0_Address, uint8_t I2C0_Data);
uint8_t Sample_I2C0_Master_Byte_Read(uint8_t I2C0_Address);


#endif  //_MG82F6D17_I2C0_DRV_H