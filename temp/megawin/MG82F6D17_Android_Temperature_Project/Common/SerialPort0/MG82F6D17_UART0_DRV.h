/**
 ******************************************************************************
 *
 * @file        MG82F6D17_UART0_DRV.h
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG82F6D17
 * @version     V1.00
 * @date        2020/06/05
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2020 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer
 *		The Demo software is provided "AS IS"  without any warranty, either
 *		expressed or implied, including, but not limited to, the implied warranties
 *		of merchantability and fitness for a particular purpose.  The author will
 *		not be liable for any special, incidental, consequential or indirect
 *		damages due to loss of data or any other reason.
 *		These statements agree with the world wide and local dictated laws about
 *		authorship and violence against these laws.
 ******************************************************************************
  @if HIDE
 * Modify History:
 * #1.00__Timmins_20200605 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal
 */

#ifndef MG82F6D17_UART0_DRV_H
#define MG82F6D17_UART0_DRV_H

/**
*****************************************************************************
* @brief        UART0 Interrupt
* @details      Enable UART0 Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_IT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_IT_Cmd(__STATE__)\
    ((__STATE__==0)?(ES0=0):(ES0=1))

/**
*****************************************************************************
* @brief        UART0 Block TI0
* @details      Enable UART0 Block TI0
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_Block_S0IntFlag_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_Block_S0IntFlag_Cmd(__STATE__)\
    ((__STATE__==0)?(S0CFG=S0CFG&(~BTI)):(S0CFG=S0CFG|BTI))
/**
*****************************************************************************
* @brief        UART0 TI0 To System Flag Interrupt
* @details      Enable UART0 TI0 To System Flag Interrupt
* @param[in]    \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_TI0toSYSINT_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_TI0toSYSINT_Cmd(__STATE__)\
        ((__STATE__==0)?(S0CFG=S0CFG&(~UTIE)):(S0CFG=S0CFG|UTIE))
/**
*******************************************************************************
* @brief        UART0 mode
* @details      Set SM30 SM00 SM10
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       UART0_MODE0_SHIFT_REG
*  @arg\b       UART0_MODE1_8BIT
*  @arg\b       UART0_MODE2_9BIT
*  @arg\b       UART0_MODE3_9BIT
*  @arg\b       UART0_MODE4_SPI_MASTER
*  @arg\b       UART0_MODE5_LIN
* @return       None
* @note         None
* @par         Example
* @code
               __DRV_URT0_Mode_Select(UART0_Mode0_SHIFT_REG)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_Mode_Select
#define UART0_MODE0_SHIFT_REG 0x00
#define UART0_MODE1_8BIT 0x01
#define UART0_MODE2_9BIT 0x02
#define UART0_MODE3_9BIT 0x03
#define UART0_MODE4_SPI_MASTER 0x04
#define UART0_MODE5_LIN 0x05
///@endcond
#define __DRV_URT0_Mode_Select(__MODE__)\
    MWT(__DRV_URT0_SetSM30(__MODE__);\
        __DRV_URT0_SetSM00(__MODE__);\
        __DRV_URT0_SetSM10(__MODE__);\
    ;)
/**
*******************************************************************************
* @brief        UART0 Pin MUX
* @details      Set S0PS1 S0PS0
* @param[in]    \_\_RXTX\_\_ :
*  @arg\b       UART0_RX_P30_TX_P31
*  @arg\b       UART0_RX_P44_TX_P45
*  @arg\b       UART0_RX_P31_TX_P30
*  @arg\b       UART0_RX_P17_TX_P22
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_PinMux_Select(UART0_RX_P30_TX_P31)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_PinMux_Select
#define UART0_RX_P30_TX_P31 0x00
#define UART0_RX_P44_TX_P45 0x01
#define UART0_RX_P31_TX_P30 0x02
#define UART0_RX_P17_TX_P22 0x03
///@endcond
#define __DRV_URT0_PinMux_Select(__RXTX__)\
    MWT(\
        __DRV_URT0_SetS0PS1(__RXTX__);\
        __DRV_URT0_SetS0PS0(__RXTX__);\
    ;)
/**
*******************************************************************************
* @brief        UART01 S01MI_Pin MUX
* @details      Set SnMIPS (UART0/1 Spi Master Input Pin Select )
* @param[in]    \_\_RECEPTION\_\_ :
*  @arg\b       UART0_S0MI_P16_UART1_S1MI_P61
*  @arg\b       UART0_S0MI_P33_UART1_S1MI_P47
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT01_S01MI_PinMux_Select(UART0_S0MI_P16_UART1_S1MI_P61)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT01_S01MI_PinMux_Select
#define UART0_S0MI_P16_UART1_S1MI_P61 0
#define UART0_S0MI_P33_UART1_S1MI_P47 1
///@endcond
#define __DRV_URT01_S01MI_PinMux_Select(__RECEPTION__)\
    MWT(\
        __DRV_SFR_PageIndex(3);\
        __RECEPTION__==0?(AUXR6=AUXR6&(~SnMIPS)):(AUXR6=AUXR6|(SnMIPS));\
        __DRV_SFR_PageIndex(0);\
    ;)
/**
*******************************************************************************
* @brief        UART0 Lin TxRx
* @details      Set TXRX0 (UART0 Lin Bus Tx/Rx Select )
* @param[in]    \_\_RXTX\_\_ :
*  @arg\b       UART0_LIN_BUS_RX
*  @arg\b       UART0_LIN_BUS_TX
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_Lin_Bus_TxRx_Select(UART0_LIN_BUS_RX)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_Lin_Bus_TxRx_Select
#define UART0_LIN_BUS_RX 0
#define UART0_LIN_BUS_TX 1
///@endcond
#define __DRV_URT0_Lin_Bus_TxRx_Select(__RXTX__)\
    ((__RXTX__)==0?(S0CFG1=S0CFG1&(~TXRX0)):(S0CFG1=S0CFG1|(TXRX0)))
/**
*******************************************************************************
* @brief        UART0 Lin break length
* @details      Set S0SB16 (UART0 Lin Bus Break Length Select )
* @param[in]    \_\_LENGTH\_\_ :
*  @arg\b       UART0_LIN_BUS_BREAK13
*  @arg\b       UART0_LIN_BUS_BREAK16
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_Lin_Bus_BreakLength_Select(UART0_LIN_BUS_BREAK13)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_Break_Length_Select
#define UART0_LIN_BUS_BREAK13 0
#define UART0_LIN_BUS_BREAK16 1
///@endcond
#define __DRV_URT0_Lin_Bus_BreakLength_Select(__LENGTH__)\
    ((__LENGTH__)==0?(S0CFG1=S0CFG1&(~S0SB16)):(S0CFG1=S0CFG1|(S0SB16)))
/**
*******************************************************************************
* @brief        UART0 Baud Rate Generator
* @details      Set SMOD2 SMOD1
* @param[in]    \_\_TIME\_\_ :
*  @arg\b       UART0_DEFAULT_BAUD_RATE
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X1
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X2
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X4
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_BaudRateX2X4_Select(UART0_Default_Baud_Rate)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_BaudRateX2X4_Select
#define UART0_DEFAULT_BAUD_RATE 0
#define UART0_DOUBLE_BAUD_RATE_X1 16
#define UART0_DOUBLE_BAUD_RATE_X2 32
#define UART0_DOUBLE_BAUD_RATE_X4 48
///@endcond
#define __DRV_URT0_BaudRateX2X4_Select(__TIME__)\
    MWT(\
        __DRV_URT0_SetSMOD2(__TIME__);\
        __DRV_URT0_SetSMOD1(__TIME__);\
    ;)
/**
*******************************************************************************
* @brief        Set SFR
* @details      Set SM30
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       UART0_MODE0_SHIFT_REG
*  @arg\b       UART0_MODE1_8BIT
*  @arg\b       UART0_MODE2_9BIT
*  @arg\b       UART0_MODE3_9BIT
*  @arg\b       UART0_MODE4_SPI_MASTER
*  @arg\b       UART0_MODE5_LIN
* @return       None
* @note         None
* @par         Example
* @code
               __DRV_URT0_SetSM30(UART0_MODE1_8BIT)
* @endcode
@bug
*******************************************************************************
*/
#define __DRV_URT0_SetSM30(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(S0CFG=S0CFG&(~SM30)):\
        (__MODE__^0x01)==0?(S0CFG=S0CFG&(~SM30)):\
        (__MODE__^0x02)==0?(S0CFG=S0CFG&(~SM30)):\
        (__MODE__^0x03)==0?(S0CFG=S0CFG&(~SM30)):\
        (__MODE__^0x04)==0?(S0CFG=S0CFG|(SM30)):\
        (__MODE__^0x05)==0?(S0CFG=S0CFG|(SM30)):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        Set SFR
* @details      Set SM00
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       UART0_MODE0_SHIFT_REG
*  @arg\b       UART0_MODE1_8BIT
*  @arg\b       UART0_MODE2_9BIT
*  @arg\b       UART0_MODE3_9BIT
*  @arg\b       UART0_MODE4_SPI_MASTER
*  @arg\b       UART0_MODE5_LIN :
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_SetSM00(UART0_MODE2_9BIT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetSM00(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SM00_FE=0):\
        (__MODE__^0x01)==0?(SM00_FE=0):\
        (__MODE__^0x02)==0?(SM00_FE=1):\
        (__MODE__^0x03)==0?(SM00_FE=1):\
        (__MODE__^0x04)==0?(SM00_FE=0):\
        (__MODE__^0x05)==0?(SM00_FE=0):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        Set SFR
* @details      Set SM10
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       UART0_MODE0_SHIFT_REG
*  @arg\b       UART0_MODE1_8BIT
*  @arg\b       UART0_MODE2_9BIT
*  @arg\b       UART0_MODE3_9BIT
*  @arg\b       UART0_MODE4_SPI_MASTER
*  @arg\b       UART0_MODE5_LIN
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_SetSM10(UART0_MODE3_9BIT)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetSM10(__MODE__)\
    MWT(\
        (__MODE__^0x00)==0?(SM10=0):\
        (__MODE__^0x01)==0?(SM10=1):\
        (__MODE__^0x02)==0?(SM10=0):\
        (__MODE__^0x03)==0?(SM10=1):\
        (__MODE__^0x04)==0?(SM10=0):\
        (__MODE__^0x05)==0?(SM10=1):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        UART0 Pin MUX
* @details      Set S0PS1
* @param[in]    \_\_RXTX\_\_ :
*  @arg\b       UART0_RX_P30_TX_P31
*  @arg\b       UART0_RX_P44_TX_P45
*  @arg\b       UART0_RX_P31_TX_P30
*  @arg\b       UART0_RX_P17_TX_P22
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_SetS0PS1(UART0_RX_P30_TX_P31)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetS0PS1(__RXTX__)\
    MWT(\
        __DRV_SFR_PageIndex(7);\
        (__RXTX__^0x00)==0?AUXR10=AUXR10&(~S0PS1):\
        (__RXTX__^0x01)==0?AUXR10=AUXR10&(~S0PS1):\
        (__RXTX__^0x02)==0?AUXR10=AUXR10|(S0PS1):\
        (__RXTX__^0x03)==0?AUXR10=AUXR10|(S0PS1):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        UART0 Pin MUX
* @details      Set S0PS0
* @param[in]    \_\_RXTX\_\_ :
*  @arg\b       UART0_RX_P30_TX_P31
*  @arg\b       UART0_RX_P44_TX_P45
*  @arg\b       UART0_RX_P31_TX_P30
*  @arg\b       UART0_RX_P17_TX_P22
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_SetS0PS0(UART0_RX_P30_TX_P31)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetS0PS0(__RXTX__)\
    MWT(\
        __DRV_SFR_PageIndex(0);\
        (__RXTX__^0x00)==0?AUXR3=AUXR3&(~S0PS0):\
        (__RXTX__^0x01)==0?AUXR3=AUXR3|(S0PS0):\
        (__RXTX__^0x02)==0?AUXR3=AUXR3&(~S0PS0):\
        (__RXTX__^0x03)==0?AUXR3=AUXR3|(S0PS0):_nop_();\
    ;)
/**
*******************************************************************************
* @brief        UART0 Data Order
* @details      Set S0DOR
* @param[in]    \_\_ORDER\_\_ :
*  @arg\b       UART0_DATA_ORDER_LSB
*  @arg\b       UART0_DATA_ORDER_MSB
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_DataOrder_Select(UART0_DATA_ORDER_LSB)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_DataOrder_Select
#define UART0_DATA_ORDER_LSB 0x08
#define UART0_DATA_ORDER_MSB 0x00
///@endcond
#define __DRV_URT0_DataOrder_Select(__ORDER__)\
    ((__ORDER__==0x08)?(S0CFG=S0CFG|(S0DOR)):(S0CFG=S0CFG&(~S0DOR)))
/**
*******************************************************************************
* @brief        UART0 S0BRG Timer Mode.
* @details      Set S0DOR
* @param[in]    \_\_MODE\_\_ :
*  @arg\b       S0BRG_8BIT_TIMER_MODE
*  @arg\b       S0BRG_16BIT_TIMER_MODE
* @return       None
* @note         S0DOR=1 S0TCK=1 S0RCK=1 SM30=0 SM00=1 SM00=0 8BIT TIMER MODE
* @par          Example
* @code
                __DRV_URT0_TimerMode_Select(S0BRG_8BIT_TIMER_MODE)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_TimerMode_Select
#define S0BRG_8BIT_TIMER_MODE 1
#define S0BRG_16BIT_TIMER_MODE 0
///@endcond
#define __DRV_URT0_TimerMode_Select(__MODE__)\
    ((__MODE__==1)?(S0CFG=S0CFG|(S0DOR)):(S0CFG=S0CFG&(~S0DOR)))
/**
*******************************************************************************
* @brief        UART0 Enhance Baud Rate
* @details      Set SMOD2
* @param[in]   \_\_TIME\_\_ :
*  @arg\b       UART0_DEFAULT_BAUD_RATE
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X1
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X2
*  @arg\b       UART0_DOUBLE_BAUD_RATE_X4
* @return       None
* @note         None
* @par          Example
* @code
                __DRV_URT0_SetSMOD2(UART0_Double_Baud_Rate_X1)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetSMOD2(__TIME__)\
    MWT(\
        (__TIME__^0)==0?S0CFG=S0CFG&(~SMOD2):\
        (__TIME__^16)==0?S0CFG=S0CFG&(~SMOD2):\
        (__TIME__^32)==0?S0CFG=S0CFG|(SMOD2):\
        (__TIME__^48)==0?S0CFG=S0CFG|(SMOD2):_nop_();\
    ;)
/**
*******************************************************************************
* @brief       UART0 Enhance Baud Rate
* @details     Set SMOD1
* @param[in]   \_\_TIME\_\_ :
*  @arg\b      UART0_DEFAULT_BAUD_RATE
*  @arg\b      UART0_DOUBLE_BAUD_RATE_X1
*  @arg\b      UART0_DOUBLE_BAUD_RATE_X2
*  @arg\b      UART0_DOUBLE_BAUD_RATE_X4
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_SetSMOD1(UART0_DOUBLE_BAUD_RATE_X2)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SetSMOD1(__TIME__)\
    MWT(\
        (__TIME__^0)==0?PCON0=PCON0&(~SMOD1):\
        (__TIME__^16)==0?PCON0=PCON0|(SMOD1):\
        (__TIME__^32)==0?PCON0=PCON0&(~SMOD1):\
        (__TIME__^48)==0?PCON0=PCON0|(SMOD1):_nop_();\
    ;)
/**
*******************************************************************************
* @brief       UART0 Baud Rate Generator
* @details     Set SMOD2
* @param[in]   \_\_TIME\_\_ :
*  @arg\b      UART0_DEFAULT_BAUD_RATE
*  @arg\b      UART0_DOUBLE_BAUD_RATE_X1
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_BaudRateX2_Select(UART0_DEFAULT_BAUD_RATE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_BaudRateX2_Select(__TIME__)\
    ((__TIME__^16)==0?(S0CFG=S0CFG|(SMOD2)):(S0CFG=S0CFG&(~SMOD2)))
/**
*******************************************************************************
* @brief       UART0 S0BRG Clock Source
* @details     Set S0TX12
* @param[in]   \_\_SOURCE\_\_ :
*  @arg\b      S0BRG_CLOCK_SOURCE_SYSCLK_DIV_1
*  @arg\b      S0BRG_CLOCK_SOURCE_SYSCLK_DIV_12
* @return      None
* @note        Before set S0TX12  Set SMOD3=1 first
* @par         Example
* @code
__DRV_URT0_BaudRateGeneratorClock_Select(S0BRG_CLOCK_SOURCE_SYSCLK_DIV_1)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_BaudRateGeneratorClock_Select
#define S0BRG_CLOCK_SOURCE_SYSCLK_DIV_1 64
#define S0BRG_CLOCK_SOURCE_SYSCLK_DIV_12 0
///@endcond
#define __DRV_URT0_BaudRateGeneratorClock_Select(__SOURCE__)\
    ((__SOURCE__==0)?(S0CR1=S0CR1&(~S0TX12)):(S0CR1=S0CR1|(S0TX12)))
/**
*******************************************************************************
* @brief       UART0 S0CR1
* @details     Set SMOD3
* @param[in]   \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_S0Control_Select(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_S0Control_Cmd(__STATE__)\
    ((__STATE__==1)?(S0CFG=S0CFG|(SMOD3)):(S0CFG=S0CFG&(~SMOD3)))
/**
*******************************************************************************
* @brief       UART0 Receive Clock
* @details     Set S0RCK
* @param[in]   \_\_SOURCE\_\_ :
*  @arg\b      RECEIVE_CLOCK_S0BRG
*  @arg\b      RECEIVE_CLOCK_TIMER_S1BRG
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_RXClockSource_Select(RECEIVE_CLOCK_S0BRG)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_RXClockSource_Select
#define RECEIVE_CLOCK_S0BRG 0x00
#define RECEIVE_CLOCK_SYSCLK_TIMER_S1BRG 0x01
///@endcond
#define __DRV_URT0_RXClockSource_Select(__SOURCE__)\
    ((__SOURCE__==0x00)?(S0CR1=S0CR1|(S0RCK)):(S0CR1=S0CR1&(~S0RCK)))
/**
*******************************************************************************
* @brief       UART0 Transmit Clock
* @details     Set S0TCK
* @param[in]   \_\_SOURCE\_\_ :
*  @arg\b      TRANSMIT_CLOCK_S0BRG
*  @arg\b      TRANSMIT_CLOCK_TIMER_S1BRG
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_TXClockSource_Select(TRANSMIT_CLOCK_S0BRG)
* @endcode
* @bug
*******************************************************************************
*/
///@cond __DRV_URT0_TXClockSource_Select
#define TRANSMIT_CLOCK_S0BRG 0x00
#define TRANSMIT_CLOCK_SYSCLK_TIMER_S1BRG 0x04
///@endcond
#define __DRV_URT0_TXClockSource_Select(__SOURCE__)\
    ((__SOURCE__)==0x00?(S0CR1=S0CR1|(S0TCK)):(S0CR1=S0CR1&(~S0TCK)))
/**
*******************************************************************************
* @brief       UART0 S0BRG Operation Enable/Disable
* @details     Set S0TR
* @param[in]   \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return      None
* @note        Before write S0BRT and S0BRC,S0BRG operation stop first
* @par         Example
* @code
__DRV_URT0_BaudRateGenerator_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/

#define __DRV_URT0_BaudRateGenerator_Cmd(__STATE__)\
    ((__STATE__)==1?(S0CR1=S0CR1|(S0TR)):(S0CR1=S0CR1&(~S0TR)))
/**
*******************************************************************************
* @brief        UART0  Baud Rate Generator Reload Register
* @details      Write S0BRT S0BRC
* @param[in]    \_\_RELOAD\_\_ :
*  @arg\b S0BRG_BRGRL_2400_1X_12000000_12T
*  @arg\b S0BRG_BRGRL_4800_1X_11059200_12T
*  @arg\b S0BRG_BRGRL_9600_1X_11059200_12T
*  @arg\b S0BRG_BRGRL_2400_1X_12000000_1T
*  @arg\b S0BRG_BRGRL_4800_1X_12000000_1T
*  @arg\b S0BRG_BRGRL_9600_1X_12000000_1T
*  @arg\b S0BRG_BRGRL_19200_1X_12000000_1T
*  @arg\b S0BRG_BRGRL_38400_1X_12000000_1T
*  @arg\b S0BRG_BRGRL_57600_1X_11059200_1T
*  @arg\b S0BRG_BRGRL_115200_1X_11059200_1T
*  @arg\b S0BRG_BRGRL_2400_2X_12000000_12T
*  @arg\b S0BRG_BRGRL_4800_2X_12000000_12T
*  @arg\b S0BRG_BRGRL_9600_2X_11059200_12T
*  @arg\b S0BRG_BRGRL_19200_2X_11059200_12T
*  @arg\b S0BRG_BRGRL_57600_2X_11059200_12T
*  @arg\b S0BRG_BRGRL_4800_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_9600_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_19200_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_38400_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_57600_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_115200_2X_11059200_1T
*  @arg\b S0BRG_BRGRL_230400_2X_11059200_1T
*  @arg\b S0BRG_BRGRL_250000_2X_12000000_1T
*  @arg\b S0BRG_BRGRL_750000_2X_12000000_1T
* @return       None
* @note         Before write S0BRT and S0BRC,S0BRG operation stop first
* @par          Example
* @code
__DRV_URT0_BaudRateReloadReg_Write(S0BRG_BRGRL_9600_2X_12000000_1T)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_BaudRateReloadReg_Write(__RELOAD__)\
    (S0BRT=S0BRC=__RELOAD__)
/**
*******************************************************************************
* @brief       UART0 Reception
* @details     Enable REN0
* @param[in]   \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_SerialReception_Cmd(MW_ENABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_SerialReception_Cmd(__STATE__)\
    ((__STATE__)==1?(REN0=1):(REN0=0))
/**
*******************************************************************************
* @brief     UART0 In Mode 2 SYSCLK/32 or SYSCLK/64,SYSCLK/96 or SYSCLK/192
* @details   Disable URM0X3
* @param[in] \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_BaudRateDiv3_Cmd(MW_DISABLE)
* @endcode
* @bug
*******************************************************************************
*/
#define __DRV_URT0_BaudRateDiv3_Cmd(__STATE__)\
    ((__STATE__)==64?(S0CFG=S0CFG|(URM0X3)):(S0CFG=S0CFG&(~URM0X3)))
    /**
*******************************************************************************
* @brief     UART0 In Mode 4 SYSCLK/4,SYSCLK/12
* @details   Set URM0X3
* @param[in] \_\_STATE\_\_ :
*  @arg\b       MW_DISABLE
*  @arg\b       MW_ENABLE
* @return      None
* @note        None
* @par         Example
* @code
__DRV_URT0_BaudRateSysclkDiv4_Cmd(MW_DISABLE)
* @endcode
* @bug
*******************************************************************************
    */
#define __DRV_URT0_BaudRateSysclkDiv4_Cmd(__STATE__)\
    ((__STATE__)==1?(S0CFG=S0CFG|(URM0X3)):(S0CFG=S0CFG&(~URM0X3)))

///@cond
bool DRV_URT0_GetTI0(void);
bool DRV_URT0_GetRI0(void);
void DRV_URT0_ClearTI0(void);
void DRV_URT0_ClearRI0(void);
void DRV_URT0_ClearSM20(void);
void DRV_URT0_SetSM20(void);
void DRV_URT0_SetTXData9th(void);
void DRV_URT0_ClearTXData9th(void);
bool DRV_URT0_GetRXData9th(void);
void DRV_URT0_SendTXData(uint8_t TXData);
uint8_t DRV_URT0_ReceiveRXData(void);

void DRV_URT0_SetSyncBreakFlag(void);
bool DRV_URT0_GetSyncBreakFlag(void);
void DRV_URT0_SetSyncBreak(void);
bool DRV_URT0_GetSyncBreak(void);
void DRV_URT0_SetAutoBaudRate(void);
uint8_t DRV_URT0_GetBaudRate(void);
void DRV_URT0_SetTransmitErrorFlag(void);
bool DRV_URT0_GetTransmitErrorFlag(void);
///@endcond
#endif

