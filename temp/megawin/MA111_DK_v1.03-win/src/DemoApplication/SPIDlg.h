#pragma once
#include "afxwin.h"

//--------------------------------------------------------
// Copy From v1.70 by Tony Chu 2010/02/23
typedef struct _HEXCTRLCOLOR
{
	DWORD dwMask;
	COLORREF crAddrBack;
	COLORREF crAddrText;
	COLORREF crHexBack;
	COLORREF crHexText;
	COLORREF crAsciiBack;
	COLORREF crAsciText;
	COLORREF crBackground;
	COLORREF crSelBorder;
}HEXCTRLCOLOR;

#define	HM_SETDATA		WM_USER + 1
#define	HM_GETDATA		WM_USER + 2
#define HM_LINELENGTH	WM_USER + 8
#define HM_COLOR		WM_USER + 9
#define HM_FONTSIZE		WM_USER + 11
#define HM_CHECKSUM		WM_USER + 13
#define HM_LIMITSIZE	WM_USER + 14
//--------------------------------------------------------
// CSPIDlg dialog

class CSPIDlg : public CDialog
{
	DECLARE_DYNAMIC(CSPIDlg)

public:
	CSPIDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSPIDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SPI };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboSPIMode;
	CComboBox m_comboSPIBitOrder;
	CComboBox m_comboSPIClockRate;
	CComboBox m_comboSPIOperationType;
	virtual BOOL OnInitDialog();
	void InitialInterface();
	CTCEdit m_editDummyByte;
	afx_msg void OnBnClickedButtonSpiSetStreamMode();
	afx_msg void OnBnClickedButtonSpiGetStreamMode();
	CTCEdit m_editSPIAuxBufferSize;
	DWORD SelectFilePath(int nIDDlgItem, bool bOpen);
	afx_msg void OnBnClickedButtonSelectForWrite();
	HWND m_hEditBuffer;
	afx_msg void OnChangeEditSpiAuxBufferSize();
	afx_msg void OnCbnSelchangeComboSpiOperatingType();
};
