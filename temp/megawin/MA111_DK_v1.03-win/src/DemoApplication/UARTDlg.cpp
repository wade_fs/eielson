 // UARTDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DemoApplication.h"
#include "TCEdit.h"
#include "UARTDlg.h"
#include "afxdialogex.h"
#include "TWIDlg.h"
#include "MWBridgeDLL.h"
#include "GPIOPWMDlg.h"
#include "OtherDlg.h"
#include "SPIDlg.h"
#include "DemoApplicationDlg.h"

// CUARTDlg dialog
extern sMWBridge gMWBridge;
IMPLEMENT_DYNAMIC(CUARTDlg, CDialogEx)

CUARTDlg::CUARTDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUARTDlg::IDD, pParent)
{

}

CUARTDlg::~CUARTDlg()
{
}

void CUARTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_UART_MODE, m_comboUARTMode);
	DDX_Control(pDX, IDC_COMBO_BAUD_RATE, m_comboBaudRate);
	DDX_Control(pDX, IDC_COMBO_STOP_BIT, m_comboStopBit);
	DDX_Control(pDX, IDC_COMBO_PARITY_SELECTION, m_comboParitySelection);
	DDX_Control(pDX, IDC_COMBO_BIT_ORDER, m_comboUARTBitOrder);
	DDX_Control(pDX, IDC_COMBO_BREAK_TIMING, m_comboBreakTiming);
	DDX_Control(pDX, IDC_COMBO_RTS_POLARITY_SELECTION, m_comboRTSPolaritySelection);
	DDX_Control(pDX, IDC_COMBO_CTS_POLARITY_SELECTION, m_comboCTSPolaritySelection);
	DDX_Control(pDX, IDC_COMBO_DE_POLARITY_SELECTION, m_comboDEPolaritySelection);
	DDX_Control(pDX, IDC_EDIT_SADDR1, m_editSADDR1);
	DDX_Control(pDX, IDC_EDIT_SADDR2, m_editSADDR2);
	DDX_Control(pDX, IDC_EDIT_UART_TARGET_ADDRESS, m_editUartTargetAddress);
	DDX_Control(pDX, IDC_EDIT_SLAVE_ADDRESS, m_editUartSlaveAddress);
}


BEGIN_MESSAGE_MAP(CUARTDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_UART_SET_STREAM_MODE, &CUARTDlg::OnBnClickedButtonUartSetStreamMode)
	ON_BN_CLICKED(IDC_BUTTON_UART_GET_STREAM_MODE, &CUARTDlg::OnBnClickedButtonUartGetStreamMode)
	ON_BN_CLICKED(IDC_BUTTON_GET_STATUS_UART, &CUARTDlg::OnBnClickedButtonGetStatusUart)
END_MESSAGE_MAP()


// CUARTDlg message handlers


BOOL CUARTDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


DWORD CUARTDlg::InitialInterface()
{
	m_comboUARTMode.AddString(_T("General"));
	m_comboUARTMode.AddString(_T("Master"));
	m_comboUARTMode.AddString(_T("Slave"));
	m_comboUARTMode.AddString(_T("Master and Slave"));
	m_comboUARTMode.SetCurSel(0);

	m_comboBaudRate.AddString(_T("      600"));
	m_comboBaudRate.AddString(_T("    1200"));
	m_comboBaudRate.AddString(_T("    2400"));
	m_comboBaudRate.AddString(_T("    4800"));
	m_comboBaudRate.AddString(_T("    9600"));
	m_comboBaudRate.AddString(_T("  19200"));
	m_comboBaudRate.AddString(_T("  38400"));
	m_comboBaudRate.AddString(_T("  51200"));
	m_comboBaudRate.AddString(_T("  57600"));
	m_comboBaudRate.AddString(_T("102400"));
	m_comboBaudRate.AddString(_T("115200"));
	m_comboBaudRate.AddString(_T("230400"));	
	m_comboBaudRate.SetCurSel(0);

	m_comboStopBit.AddString(_T("1"));
	m_comboStopBit.AddString(_T("2"));
	m_comboStopBit.SetCurSel(0);

//	m_comboParitySelection.AddString(_T("None"));
	m_comboParitySelection.AddString(_T("Even"));
	m_comboParitySelection.AddString(_T("Space"));
	m_comboParitySelection.AddString(_T("Odd"));
	m_comboParitySelection.AddString(_T("Mark"));
	m_comboParitySelection.SetCurSel(0);
	
	m_comboUARTBitOrder.AddString(_T("LSB first"));
	m_comboUARTBitOrder.AddString(_T("MSB first"));
	m_comboUARTBitOrder.SetCurSel(0);

	m_comboBreakTiming.AddString(_T("  2 ms"));
	m_comboBreakTiming.AddString(_T("  4 ms"));
	m_comboBreakTiming.AddString(_T("  8 ms"));
	m_comboBreakTiming.AddString(_T("16 ms"));
	m_comboBreakTiming.AddString(_T("32 ms"));
	m_comboBreakTiming.AddString(_T("48 ms"));
	m_comboBreakTiming.AddString(_T("64 ms"));
	m_comboBreakTiming.AddString(_T("80 ms"));
	m_comboBreakTiming.SetCurSel(0);
	
	m_comboRTSPolaritySelection.AddString(_T("Active Low"));
	m_comboRTSPolaritySelection.AddString(_T("Active High"));
	m_comboRTSPolaritySelection.SetCurSel(0);

	m_comboCTSPolaritySelection.AddString(_T("Active Low"));
	m_comboCTSPolaritySelection.AddString(_T("Active High"));
	m_comboCTSPolaritySelection.SetCurSel(0);

	m_comboDEPolaritySelection.AddString(_T("Active Low"));
	m_comboDEPolaritySelection.AddString(_T("Active High"));
	m_comboDEPolaritySelection.SetCurSel(0);

	m_editSADDR1.SetLimitText(2);
	m_editSADDR1.SetWindowTextW(_T("00"));
	m_editSADDR2.SetLimitText(2);
	m_editSADDR2.SetWindowTextW(_T("00"));
	m_editUartTargetAddress.SetLimitText(2);
	m_editUartTargetAddress.SetWindowTextW(_T("00"));
	m_editUartSlaveAddress.SetLimitText(2);
	m_editUartSlaveAddress.SetWindowTextW(_T("00"));

	return 0;
}


void CUARTDlg::OnBnClickedButtonUartSetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	UINT ButtonState;

	StreamSetting.Mode = UART;
	StreamSetting.UART.Mode = eUARTMode(m_comboUARTMode.GetCurSel());
	StreamSetting.UART.BaudRate = eBaudRate(m_comboBaudRate.GetCurSel());
	StreamSetting.UART.BitOrder = eBitOrder(m_comboUARTBitOrder.GetCurSel());
	StreamSetting.UART.StopBit = eStopBit(m_comboStopBit.GetCurSel());
	StreamSetting.UART.ParitySelection = eParitySelection(m_comboParitySelection.GetCurSel());

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PARITY_CHECK_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.ParityCheck = Enable;
	else
		StreamSetting.UART.ParityCheck = Disable;

	StreamSetting.UART.BreakTiming = eBreakTiming(m_comboBreakTiming.GetCurSel());

	ButtonState = IsDlgButtonChecked(IDC_CHECK_REN_CONTROL_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.RENControl = Enable;
	else
		StreamSetting.UART.RENControl = Disable;
	
	ButtonState = IsDlgButtonChecked(IDC_CHECK_SET_BREAK);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.BreakControl = SetBreak;
	else
		StreamSetting.UART.BreakControl = ClearBreak;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_RX_BREAK_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.RXBreak = Enable;
	else
		StreamSetting.UART.RXBreak = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_RTS_OUTPUT_CONTROL_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.RTSOutputFlowControl = Enable;
	else
		StreamSetting.UART.RTSOutputFlowControl = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_CTS_INPUT_FLOW_CONTROL_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.CTSInputFlowControl = Enable;
	else
		StreamSetting.UART.CTSInputFlowControl = Disable;

	
	ButtonState = IsDlgButtonChecked(IDC_CHECK_DE_OUTPUT_CONTROL_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.UART.DEOutputControl = Enable;
	else
		StreamSetting.UART.DEOutputControl = Disable;

	StreamSetting.UART.CTSPolaritySelection = eActive(m_comboCTSPolaritySelection.GetCurSel());
	StreamSetting.UART.RTSPolaritySelection = eActive(m_comboRTSPolaritySelection.GetCurSel());
	StreamSetting.UART.DEPolaritySelection = eActive(m_comboDEPolaritySelection.GetCurSel());

	m_editSADDR1.GetWindowText(tmpBuffer, MAX_PATH);
	StreamSetting.UART.SADDR1 = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
	m_editSADDR2.GetWindowText(tmpBuffer, MAX_PATH);
	StreamSetting.UART.SADDR2 = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));

	dwResult = SetStreamMode(&gMWBridge, StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		((CDemoApplicationDlg*)GetParent())->m_Options.Mode = UART;
		_stprintf_s(tmpBuffer, _T("SetStreamMode UART OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetStreamMode UART Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CUARTDlg::OnBnClickedButtonUartGetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetStreamMode(&gMWBridge, &StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		if (StreamSetting.Mode == UART)
		    _stprintf_s(tmpBuffer, _T("GetStreamMode UART OK !"));
		else
			_stprintf_s(tmpBuffer, _T("GetStreamMode OK ! But not in UART Mode !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetStreamMode UART Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}

	m_comboUARTMode.SetCurSel(StreamSetting.UART.Mode);
	m_comboBaudRate.SetCurSel(StreamSetting.UART.BaudRate);
	m_comboStopBit.SetCurSel(StreamSetting.UART.StopBit);
	m_comboParitySelection.SetCurSel(StreamSetting.UART.ParitySelection);

	if (StreamSetting.UART.ParityCheck == Enable)
		CheckDlgButton(IDC_CHECK_PARITY_CHECK_ENABLE, BST_CHECKED);
	else
	    CheckDlgButton(IDC_CHECK_PARITY_CHECK_ENABLE, BST_UNCHECKED);

	m_comboUARTBitOrder.SetCurSel(StreamSetting.UART.BitOrder);
	m_comboBreakTiming.SetCurSel(StreamSetting.UART.BreakTiming);

	if (StreamSetting.UART.RENControl == Enable)
		CheckDlgButton(IDC_CHECK_REN_CONTROL_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_REN_CONTROL_ENABLE, BST_UNCHECKED);

	if (StreamSetting.UART.BreakControl == SetBreak)
		CheckDlgButton(IDC_CHECK_SET_BREAK, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_SET_BREAK, BST_UNCHECKED);

	if (StreamSetting.UART.RXBreak == Enable)
		CheckDlgButton(IDC_CHECK_RX_BREAK_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_RX_BREAK_ENABLE, BST_UNCHECKED);


	if (StreamSetting.UART.RTSOutputFlowControl == Enable)
		CheckDlgButton(IDC_CHECK_RTS_OUTPUT_CONTROL_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_RTS_OUTPUT_CONTROL_ENABLE, BST_UNCHECKED);

	if (StreamSetting.UART.CTSInputFlowControl == SetBreak)
		CheckDlgButton(IDC_CHECK_CTS_INPUT_FLOW_CONTROL_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_CTS_INPUT_FLOW_CONTROL_ENABLE, BST_UNCHECKED);

	if (StreamSetting.UART.DEOutputControl == Enable)
		CheckDlgButton(IDC_CHECK_DE_OUTPUT_CONTROL_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_DE_OUTPUT_CONTROL_ENABLE, BST_UNCHECKED);

	m_comboRTSPolaritySelection.SetCurSel(StreamSetting.UART.RTSPolaritySelection);
	m_comboCTSPolaritySelection.SetCurSel(StreamSetting.UART.CTSPolaritySelection);
	m_comboDEPolaritySelection.SetCurSel(StreamSetting.UART.DEPolaritySelection);

	
	_stprintf_s(tmpBuffer, _T("%02X"), StreamSetting.UART.SADDR1);
	m_editSADDR1.SetWindowText(tmpBuffer);

	_stprintf_s(tmpBuffer, _T("%02X"), StreamSetting.UART.SADDR2);
	m_editSADDR2.SetWindowText(tmpBuffer);
}


void CUARTDlg::OnBnClickedButtonGetStatusUart()
{
	sStatus Status;
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetStatus(&gMWBridge, &Status);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetStatus OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetStatus Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}


	if (Status.UART.RXOverRun == TRUE)
		CheckDlgButton(IDC_CHECK_UART_STATUS_RX_OVER_RUN, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_UART_STATUS_RX_OVER_RUN, BST_UNCHECKED);

	if (Status.UART.RXDParityError == TRUE)
		CheckDlgButton(IDC_CHECK_UART_STATUS_PARITY_ERROR, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_UART_STATUS_PARITY_ERROR, BST_UNCHECKED);

	if (Status.UART.RXDFramError == TRUE)
		CheckDlgButton(IDC_CHECK_UART_STATUS_FRAME_ERROR, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_UART_STATUS_FRAME_ERROR, BST_UNCHECKED);

	if (Status.UART.RXDBreakDetection == TRUE)
		CheckDlgButton(IDC_CHECK_UART_STATUS_BREAK_DETECTION, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_UART_STATUS_BREAK_DETECTION, BST_UNCHECKED);

}
