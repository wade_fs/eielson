// SPIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DemoApplication.h"
#include "TCEdit.h"
#include "SPIDlg.h"
#include "MWBridgeDLL.h"
#include "afxdialogex.h"
#include "GPIOPWMDlg.h"
#include "OtherDlg.h"
#include "UARTDlg.h"
#include "TWIDlg.h"
#include "DemoApplicationDlg.h"

// CSPIDlg dialog
extern sMWBridge gMWBridge;
IMPLEMENT_DYNAMIC(CSPIDlg, CDialog)

CSPIDlg::CSPIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSPIDlg::IDD, pParent),
	m_hEditBuffer(NULL)
{

}

CSPIDlg::~CSPIDlg()
{
}

void CSPIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_SPI_MODE, m_comboSPIMode);
	DDX_Control(pDX, IDC_COMBO_SPI_BIT_ORDER, m_comboSPIBitOrder);
	DDX_Control(pDX, IDC_COMBO_SPI_CLOCK_RATE, m_comboSPIClockRate);
	DDX_Control(pDX, IDC_COMBO_SPI_OPERATING_TYPE, m_comboSPIOperationType);
	DDX_Control(pDX, IDC_EDIT_SPI_DUMMY_BYTE, m_editDummyByte);
	DDX_Control(pDX, IDC_EDIT_SPI_AUX_BUFFER_SIZE, m_editSPIAuxBufferSize);
}


BEGIN_MESSAGE_MAP(CSPIDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SPI_SET_STREAM_MODE, &CSPIDlg::OnBnClickedButtonSpiSetStreamMode)
	ON_BN_CLICKED(IDC_BUTTON_SPI_GET_STREAM_MODE, &CSPIDlg::OnBnClickedButtonSpiGetStreamMode)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_FOR_WRITE, &CSPIDlg::OnBnClickedButtonSelectForWrite)
	ON_EN_CHANGE(IDC_EDIT_SPI_AUX_BUFFER_SIZE, &CSPIDlg::OnChangeEditSpiAuxBufferSize)
	ON_CBN_SELCHANGE(IDC_COMBO_SPI_OPERATING_TYPE, &CSPIDlg::OnCbnSelchangeComboSpiOperatingType)
END_MESSAGE_MAP()


// CSPIDlg message handlers


BOOL CSPIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CSPIDlg::InitialInterface()
{
	m_comboSPIMode.AddString(_T("0"));
	m_comboSPIMode.AddString(_T("1"));
	m_comboSPIMode.AddString(_T("2"));
	m_comboSPIMode.AddString(_T("3"));
	m_comboSPIMode.SetCurSel(0);

	m_comboSPIBitOrder.AddString(_T("LSB first"));
	m_comboSPIBitOrder.AddString(_T("MSB first"));	
	m_comboSPIBitOrder.SetCurSel(0);

	m_comboSPIClockRate.AddString(_T("2 MHz"));
	m_comboSPIClockRate.AddString(_T("6 MHz"));
	m_comboSPIClockRate.SetCurSel(0);

	m_comboSPIOperationType.AddString(_T("Idle"));
	m_comboSPIOperationType.AddString(_T("Sequential Write"));
	m_comboSPIOperationType.AddString(_T("Sequential Read"));
	m_comboSPIOperationType.AddString(_T("Random Read"));
	m_comboSPIOperationType.AddString(_T("Standard Write with Read"));
	m_comboSPIOperationType.SetCurSel(0);

	m_editDummyByte.SetWindowText(_T("00"));
	m_editDummyByte.SetLimitText(2);
	m_editSPIAuxBufferSize.SetLimitText(8);
	m_editSPIAuxBufferSize.SetWindowText(_T("00000000"));
	m_editSPIAuxBufferSize.EnableWindow(FALSE);

	DWORD hc[] = { 0x55, 0xC8C8C8, 0, 0xC8C8C8, 0, 0xC8C8C8, 0, 0xC8C8C8, 0 };	
	m_hEditBuffer = ::GetDlgItem(m_hWnd, IDC_EDIT_BUFFER);
	::SendMessage(m_hEditBuffer, HM_COLOR, (WPARAM)&hc, 0);           // Set Color
	::SendMessage(m_hEditBuffer, HM_FONTSIZE, 14, 0);                 // Set Font Size
	::SendMessage(m_hEditBuffer, HM_LINELENGTH, MAKELONG(0, 4), 0);   // Set Line Length
}


void CSPIDlg::OnBnClickedButtonSpiSetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	UINT ButtonState;

	StreamSetting.Mode = SPI;

	StreamSetting.SPI.ModeSelection = eModeSelection(m_comboSPIMode.GetCurSel());
	StreamSetting.SPI.BitOrder = eBitOrder(m_comboSPIBitOrder.GetCurSel());

	ButtonState = IsDlgButtonChecked(IDC_CHECK_SPI_nSS_OUTPUT_CONTROL);
	if (ButtonState == BST_CHECKED)
		StreamSetting.SPI.nSSOutputControl = Enable;
	else
		StreamSetting.SPI.nSSOutputControl = Disable;

	StreamSetting.SPI.ClockRate = eClockRate(m_comboSPIClockRate.GetCurSel());

	dwResult = SetStreamMode(&gMWBridge, StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		((CDemoApplicationDlg*)GetParent())->m_Options.Mode = SPI;
		_stprintf_s(tmpBuffer, _T("SetStreamMode SPI OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetStreamMode SPI Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CSPIDlg::OnBnClickedButtonSpiGetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetStreamMode(&gMWBridge, &StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		if (StreamSetting.Mode == SPI)
		    _stprintf_s(tmpBuffer, _T("GetStreamMode SPI OK !"));
		else
			_stprintf_s(tmpBuffer, _T("GetStreamMode OK ! But not in SPI Mode !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetStreamMode SPI Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	if (StreamSetting.Mode == SPI)
	{
		m_comboSPIMode.SetCurSel(StreamSetting.SPI.ModeSelection);
		m_comboSPIBitOrder.SetCurSel(StreamSetting.SPI.BitOrder);
		if (StreamSetting.SPI.nSSOutputControl == Enable)
			CheckDlgButton(IDC_CHECK_SPI_nSS_OUTPUT_CONTROL, BST_CHECKED);
		else
			CheckDlgButton(IDC_CHECK_SPI_nSS_OUTPUT_CONTROL, BST_UNCHECKED);
        
		m_comboSPIClockRate.SetCurSel(StreamSetting.SPI.ClockRate);		
	}

}


DWORD CSPIDlg::SelectFilePath(int nIDDlgItem, bool bOpen)
{
	TCHAR szFilters[] = _T("Binary files (*.bin)|*.bin|All files (*.*)|*.*||");
	CFileException ex;
	CString strTmp;
	CFile TempFile;
	TCHAR szFileName[MAX_PATH] = { 0 };
	TCHAR ExePath[MAX_PATH] = { 0 };;
	DWORD dwSize = 0;
	PTCHAR pch = NULL;

	CFileDialog dlgOpen(bOpen, _T("All files"), _T("*.*"),
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);
	if (dlgOpen.DoModal() == IDOK)
	{
		strTmp = "";
		strTmp = dlgOpen.GetPathName();
		if (bOpen == TRUE)
		{
			if (!TempFile.Open(strTmp, CFile::modeRead | CFile::shareDenyWrite, &ex))
			{
				TCHAR szError[1024];
				ex.GetErrorMessage(szError, 1024);
				MessageBox(szError, _T("ERROR"), MB_OK);
				return -1;
			}
			else
			{
				dwSize = static_cast<DWORD>(TempFile.GetLength());
				TempFile.Close();
				if (nIDDlgItem != 0)
					((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
				return dwSize;
			}
		}
		else
		{
			if (!TempFile.Open(strTmp, CFile::modeRead | CFile::shareDenyWrite, &ex))
			{
				if (ex.m_cause == CFileException::fileNotFound)
				{
					if (!TempFile.Open(strTmp, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &ex))
					{
						TCHAR szError[1024];
						ex.GetErrorMessage(szError, 1024);
						MessageBox(szError, _T("ERROR"), MB_OK);
						return -1;
					}
					else
					{
						TempFile.Close();
						if (nIDDlgItem != 0)
							((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
						return 0;
					}
				}
				else
				{
					TempFile.Close();
					if (nIDDlgItem != 0)
						((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
					return 0;
				}
			}
			else
			{
				TempFile.Close();
				if (nIDDlgItem != 0)
					((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
				return 0;
			}
		}
	}
	else
	{
		if (nIDDlgItem != 0)
			((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(_T(""));
		return 0;
	}
}


void CSPIDlg::OnBnClickedButtonSelectForWrite()
{
	DWORD dwTxFileSize = 0;
	TCHAR tmpBuffer[MAX_PATH] = { 0 };
	TCHAR SPITxFilePath[MAX_PATH] = { 0 };
	int Index = 0;
	CFile SPITxFile;
	CFileException ex;
	BYTE * pTxBuffer = NULL;

	Index = m_comboSPIOperationType.GetCurSel();
	if (Index == SPI_RandomRead) // OperationType = Random Read
	{
		dwTxFileSize = SelectFilePath(IDC_EDIT_AUX_BUFFER_PATH, TRUE);
		_stprintf_s(tmpBuffer, MAX_PATH, _T("%X"), dwTxFileSize);
		((CEdit*)(GetDlgItem(IDC_EDIT_SPI_AUX_BUFFER_SIZE)))->SetWindowText(tmpBuffer);

		GetDlgItemText(IDC_EDIT_AUX_BUFFER_PATH, SPITxFilePath, MAX_PATH);
		if (!SPITxFile.Open(SPITxFilePath, CFile::modeRead | CFile::shareDenyWrite, &ex))
		{
			TCHAR szError[1024];
			ex.GetErrorMessage(szError, 1024);
			((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, szError);
			return;
		}
		else
		{
			dwTxFileSize = static_cast<DWORD>(SPITxFile.GetLength());
			pTxBuffer = new BYTE[dwTxFileSize];
			if (pTxBuffer != NULL)
			{
				 SPITxFile.Read(pTxBuffer, dwTxFileSize);			
				 ::SendMessage(m_hEditBuffer, HM_SETDATA, (WPARAM)pTxBuffer, dwTxFileSize);
				 SPITxFile.Close();
			}
		}
	}	
	else if (Index == SPI_StandardWriteWithRead)
	{
		SelectFilePath(IDC_EDIT_AUX_BUFFER_PATH, FALSE);		
	}
}


void CSPIDlg::OnChangeEditSpiAuxBufferSize()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	int Index = 0;
	DWORD dwSize = 0;
	TCHAR tmpBuffer[0x10] = { 0 };
	BYTE *pBuffer = NULL;

	Index = m_comboSPIOperationType.GetCurSel();
	if (Index == 3) // OperationType = Sequence Read
	{
		m_editSPIAuxBufferSize.GetWindowText(tmpBuffer, 0x10);
		dwSize = _tcstoul(tmpBuffer, NULL, 16);
		if (dwSize != 0)
		{
			pBuffer = new BYTE[dwSize];
			if (pBuffer != NULL)
			{
				memset(pBuffer, 0, dwSize);
				::SendMessage(m_hEditBuffer, HM_LIMITSIZE, dwSize, 0);
				::SendMessage(m_hEditBuffer, HM_SETDATA, (WPARAM)pBuffer, dwSize);				
				delete[]pBuffer;
				pBuffer = NULL;
			}
			
		}
	}
}


void CSPIDlg::OnCbnSelchangeComboSpiOperatingType()
{
	int Index;
	Index = m_comboSPIOperationType.GetCurSel();
	if (Index != SPI_RandomRead)
		m_editSPIAuxBufferSize.EnableWindow(FALSE);
	else
		m_editSPIAuxBufferSize.EnableWindow(TRUE);
}
