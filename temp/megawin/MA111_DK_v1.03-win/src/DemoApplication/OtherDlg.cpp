// OtherDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DemoApplication.h"
#include "TCEdit.h"
#include "GPIOPWMDlg.h"
#include "afxdialogex.h"
#include "MWBridgeDLL.h"
#include "SPIDlg.h"
#include "TWIDlg.h"
#include "UARTDlg.h"
#include "OtherDlg.h"
#include "DemoApplicationDlg.h"

extern sMWBridge gMWBridge;
// COtherDlg dialog

IMPLEMENT_DYNAMIC(COtherDlg, CDialog)

COtherDlg::COtherDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COtherDlg::IDD, pParent)
{

}

COtherDlg::~COtherDlg()
{
}

void COtherDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_ICKO_CONFIGURE, m_comboICKOConfigure);
	DDX_Control(pDX, IDC_EDIT_PWM0_DUTY, m_editPWM0Duty);
	DDX_Control(pDX, IDC_EDIT_PWM1_DUTY, m_editPWM1Duty);
	DDX_Control(pDX, IDC_EDIT_PWM2_DUTY, m_editPWM2Duty);
}


BEGIN_MESSAGE_MAP(COtherDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SET_GPIO_ALTERNATE_FUNCTION, &COtherDlg::OnBnClickedButtonSetGpioAlternateFunction)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_ALTERNATE_FUNCTION, &COtherDlg::OnBnClickedButtonGetGpioAlternateFunction)
	ON_BN_CLICKED(IDC_BUTTON_SET_PWM_DUTY_VALUE, &COtherDlg::OnBnClickedButtonSetPwmDutyValue)
	ON_BN_CLICKED(IDC_BUTTON_GET_PWM_DUTY_VALUE, &COtherDlg::OnBnClickedButtonGetPwmDutyValue)
	ON_BN_CLICKED(IDC_CHECK_PWM0_MASK, &COtherDlg::OnBnClickedCheckPwm0Mask)
	ON_BN_CLICKED(IDC_CHECK_PWM1_MASK, &COtherDlg::OnBnClickedCheckPwm1Mask)
	ON_BN_CLICKED(IDC_CHECK_PWM2_MASK, &COtherDlg::OnBnClickedCheckPwm2Mask)
END_MESSAGE_MAP()


// COtherDlg message handlers


BOOL COtherDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void COtherDlg::InitialInterface()
{
	m_comboICKOConfigure.AddString(_T("Output Disable"));
	m_comboICKOConfigure.AddString(_T("Output 24MHz"));
	m_comboICKOConfigure.AddString(_T("Output 12MHz"));
	m_comboICKOConfigure.AddString(_T("Output 6MHz"));
	m_comboICKOConfigure.SetCurSel(0);
	CheckDlgButton(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_LOW, BST_CHECKED);
	CheckDlgButton(IDC_RADIO_WKP0_ACTIVE_LOW, BST_CHECKED);
	CheckDlgButton(IDC_RADIO_WKP1_ACTIVE_LOW, BST_CHECKED);
	m_editPWM0Duty.SetLimitText(2);
	m_editPWM0Duty.SetWindowText(_T("00"));
	m_editPWM1Duty.SetLimitText(2);
	m_editPWM1Duty.SetWindowText(_T("00"));
	m_editPWM2Duty.SetLimitText(2);
	m_editPWM2Duty.SetWindowText(_T("00"));
}


void COtherDlg::OnBnClickedButtonSetGpioAlternateFunction()
{
	sGPIOAlternateSetting GPIOAlternateSetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	CString strTmp;	

	ButtonState = IsDlgButtonChecked(IDC_CHECK_USB_REMOTE_WAKEUP_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.USBRemoteWakeup = Enable;
	else
	    GPIOAlternateSetting.USBRemoteWakeup = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_USB_RESET_EVENT_OUTPUT_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.USBResetEventOutput = Enable;
	else
		GPIOAlternateSetting.USBResetEventOutput = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_USB_SUSPEND_EVENT_OUTPUT_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.USBSuspendEventOutput = Enable;
	else
		GPIOAlternateSetting.USBSuspendEventOutput	= Disable;

	ButtonState = IsDlgButtonChecked(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_LOW);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.USBSuspendPolarity = Low;
	else
		GPIOAlternateSetting.USBSuspendPolarity = High;

	GPIOAlternateSetting.ICKOConfigured = eICKOConfigure(m_comboICKOConfigure.GetCurSel());

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM0_OUTPUT_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.PWM0Output = Enable;
	else
		GPIOAlternateSetting.PWM0Output = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM1_OUTPUT_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.PWM1Output = Enable;
	else
		GPIOAlternateSetting.PWM1Output = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM2_OUTPUT_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.PWM2Output = Enable;
	else
		GPIOAlternateSetting.PWM2Output = Disable;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_WKP0_WAKEUP_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.WKP0Wakeup = Enable;
	else
		GPIOAlternateSetting.WKP0Wakeup = Disable;

	ButtonState = IsDlgButtonChecked(IDC_RADIO_WKP0_ACTIVE_LOW);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.WKP0ActiveLevel = Low;
	else
		GPIOAlternateSetting.WKP0ActiveLevel = High;
	
	ButtonState = IsDlgButtonChecked(IDC_CHECK_WKP1_WAKEUP_ENABLE);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.WKP1Wakeup = Enable;
	else
		GPIOAlternateSetting.WKP1Wakeup = Disable;

	ButtonState = IsDlgButtonChecked(IDC_RADIO_WKP1_ACTIVE_LOW);
	if (ButtonState == BST_CHECKED)
		GPIOAlternateSetting.WKP1ActiveLevel = Low;
	else
		GPIOAlternateSetting.WKP1ActiveLevel = High;

	dwResult = SetGPIOAlternateFunction(&gMWBridge, GPIOAlternateSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("SetGPIOAlternateFunction OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetGPIOAlternateFunction Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
}


void COtherDlg::OnBnClickedButtonGetGpioAlternateFunction()
{
	sGPIOAlternateSetting GPIOAlternateSetting;
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetGPIOAlternateFunction(&gMWBridge, &GPIOAlternateSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("GetGPIOAlternateFunction OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetGPIOAlternateFunction Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}

	if (GPIOAlternateSetting.USBRemoteWakeup == Enable)
		CheckDlgButton(IDC_CHECK_USB_REMOTE_WAKEUP_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_USB_REMOTE_WAKEUP_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.USBResetEventOutput == Enable)
		CheckDlgButton(IDC_CHECK_USB_RESET_EVENT_OUTPUT_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_USB_RESET_EVENT_OUTPUT_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.USBSuspendEventOutput == Enable)
		CheckDlgButton(IDC_CHECK_USB_SUSPEND_EVENT_OUTPUT_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_USB_SUSPEND_EVENT_OUTPUT_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.USBSuspendPolarity == Low)
	{
		CheckDlgButton(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_LOW, BST_CHECKED);
		CheckDlgButton(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_HIGH, BST_UNCHECKED);
	}
	else
	{
		CheckDlgButton(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_LOW, BST_UNCHECKED);
		CheckDlgButton(IDC_RADIO_USB_SUSPEND_POLARITY_ACTIVE_HIGH, BST_CHECKED);
	}
	   
	m_comboICKOConfigure.SetCurSel(GPIOAlternateSetting.ICKOConfigured);

	if (GPIOAlternateSetting.PWM0Output == Enable)
		CheckDlgButton(IDC_CHECK_PWM0_OUTPUT_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_PWM0_OUTPUT_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.PWM1Output == Enable)
		CheckDlgButton(IDC_CHECK_PWM1_OUTPUT_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_PWM1_OUTPUT_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.PWM2Output == Enable)
		CheckDlgButton(IDC_CHECK_PWM2_OUTPUT_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_PWM2_OUTPUT_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.WKP0Wakeup == Enable)
		CheckDlgButton(IDC_CHECK_WKP0_WAKEUP_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_WKP0_WAKEUP_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.WKP0ActiveLevel == Low)
	{
		CheckDlgButton(IDC_RADIO_WKP0_ACTIVE_LOW, BST_CHECKED);
		CheckDlgButton(IDC_RADIO_WKP0_ACTIVE_HIGH, BST_UNCHECKED);
	}
	else
	{
		CheckDlgButton(IDC_RADIO_WKP0_ACTIVE_LOW, BST_UNCHECKED);
		CheckDlgButton(IDC_RADIO_WKP0_ACTIVE_HIGH, BST_CHECKED);
	}

	if (GPIOAlternateSetting.WKP1Wakeup == Enable)
		CheckDlgButton(IDC_CHECK_WKP1_WAKEUP_ENABLE, BST_CHECKED);
	else
		CheckDlgButton(IDC_CHECK_WKP1_WAKEUP_ENABLE, BST_UNCHECKED);

	if (GPIOAlternateSetting.WKP1ActiveLevel == Low)
	{
		CheckDlgButton(IDC_RADIO_WKP1_ACTIVE_LOW, BST_CHECKED);
		CheckDlgButton(IDC_RADIO_WKP1_ACTIVE_HIGH, BST_UNCHECKED);
	}
	else
	{
		CheckDlgButton(IDC_RADIO_WKP1_ACTIVE_LOW, BST_UNCHECKED);
		CheckDlgButton(IDC_RADIO_WKP1_ACTIVE_HIGH, BST_CHECKED);
	}


}


void COtherDlg::OnBnClickedButtonSetPwmDutyValue()
{
	sPWMDutySetting PWMDutySetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	CString strTmp;
	int i;

	for (i = 0; i < 3; i++)
	{
		ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM0_MASK + i);
		if (ButtonState == BST_CHECKED)
			PWMDutySetting.PWM[i].Setting = Enable;
		else
			PWMDutySetting.PWM[i].Setting = Disable;

		GetDlgItemText(IDC_EDIT_PWM0_DUTY + i, strTmp);
		PWMDutySetting.PWM[i].Value = static_cast<BYTE>(_tcstoul(strTmp.GetBuffer(strTmp.GetLength()), NULL, 16));
	}

	dwResult = SetPWMDutyValue(&gMWBridge, PWMDutySetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("SetPWMDutyValue OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetPWMDutyValue Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void COtherDlg::OnBnClickedButtonGetPwmDutyValue()
{
	sPWMDutySetting PWMDutySetting;
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	CString strTmp;
	int i;

	for (i = 0; i < 3; i++)
	{
		PWMDutySetting.PWM[i].Setting = Enable;
	}
	dwResult = GetPWMDutyValue(&gMWBridge, &PWMDutySetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetPWMDutyValue OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetPWMDutyValue Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}

	for (i = 0; i < 3; i++)
	{
		strTmp.Format(_T("%02X"), PWMDutySetting.PWM[i].Value);
		SetDlgItemText(IDC_EDIT_PWM0_DUTY + i, strTmp);
	}

}


void COtherDlg::OnBnClickedCheckPwm0Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM0_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_PWM0_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_PWM0_MASK, _T(""));
	}
}


void COtherDlg::OnBnClickedCheckPwm1Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM1_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_PWM1_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_PWM1_MASK, _T(""));
	}
}


void COtherDlg::OnBnClickedCheckPwm2Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_PWM2_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_PWM2_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_PWM2_MASK, _T(""));
	}
}
