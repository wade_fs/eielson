// GPIOPWMDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DemoApplication.h"
#include "TCEdit.h"
#include "GPIOPWMDlg.h"
#include "afxdialogex.h"
#include "MWBridgeDLL.h"
#include "OtherDlg.h"
#include "SPIDlg.h"
#include "TWIDlg.h"
#include "UARTDlg.h"
#include "DemoApplicationDlg.h"


extern sMWBridge gMWBridge;
// CGPIOPWMDlg dialog
TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

IMPLEMENT_DYNAMIC(CGPIOPWMDlg, CDialog)

CGPIOPWMDlg::CGPIOPWMDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGPIOPWMDlg::IDD, pParent)
{

}

CGPIOPWMDlg::~CGPIOPWMDlg()
{
}

void CGPIOPWMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CGPIOPWMDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_GPIO0_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio0Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO1_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio1Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO2_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio2Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO3_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio3Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO4_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio4Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO5_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio5Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO6_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio6Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO7_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio7Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO8_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio8Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIO9_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpio9Latch)
	ON_BN_CLICKED(IDC_CHECK_GPIOA_LATCH, &CGPIOPWMDlg::OnBnClickedCheckGpioaLatch)
	ON_BN_CLICKED(IDC_CHECK_GPIO0_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio0Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO1_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio1Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO2_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio2Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO3_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio3Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO4_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio4Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO5_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio5Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO6_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio6Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO7_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio7Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO8_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio8Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIO9_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpio9Mask)
	ON_BN_CLICKED(IDC_CHECK_GPIOA_MASK, &CGPIOPWMDlg::OnBnClickedCheckGpioaMask)
	ON_BN_CLICKED(IDC_CHECK_GPIO0_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio0Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO1_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio1Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO2_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio2Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO3_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio3Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO4_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio4Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO5_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio5Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO6_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio6Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO7_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio7Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO8_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio8Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIO9_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpio9Pulse)
	ON_BN_CLICKED(IDC_CHECK_GPIOA_PULSE, &CGPIOPWMDlg::OnBnClickedCheckGpioaPulse)
	ON_BN_CLICKED(IDC_BUTTON_SET_GPIO_MODE, &CGPIOPWMDlg::OnBnClickedButtonSetGpioMode)
	ON_BN_CLICKED(IDC_BUTTON_SET_GPIO_DATA, &CGPIOPWMDlg::OnBnClickedButtonSetGpioData)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_MODE, &CGPIOPWMDlg::OnBnClickedButtonGetGpioMode)
	ON_BN_CLICKED(IDC_BUTTON_SET_GPIO_PULSE, &CGPIOPWMDlg::OnBnClickedButtonSetGpioPulse)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_DATA, &CGPIOPWMDlg::OnBnClickedButtonGetGpioData)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_PIN_STATUS, &CGPIOPWMDlg::OnBnClickedButtonGetGpioPinStatus)
	ON_BN_CLICKED(IDC_BUTTON_SET_GPIO_INTERRUPT, &CGPIOPWMDlg::OnBnClickedButtonSetGpioInterrupt)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_INTERRUPT, &CGPIOPWMDlg::OnBnClickedButtonGetGpioInterrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO0_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio0Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO1_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio1Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO2_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio2Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO3_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio3Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO4_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio4Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO5_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio5Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO6_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio6Interrupt)
	ON_BN_CLICKED(IDC_CHECK_GPIO7_INTERRUPT, &CGPIOPWMDlg::OnBnClickedCheckGpio7Interrupt)
	ON_BN_CLICKED(IDC_BUTTON_GET_GPIO_STATUS, &CGPIOPWMDlg::OnBnClickedButtonGetGpioStatus)
END_MESSAGE_MAP()


// CGPIOPWMDlg message handlers


BOOL CGPIOPWMDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CGPIOPWMDlg::InitialInterface()
{
	int i = 0;

	for (i = 0; i < 11; i++)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->AddString(_T("0"));
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->AddString(_T("1"));
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->AddString(_T("2"));
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->AddString(_T("3"));
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->SetCurSel(0);
	}

	for (i = 0; i < 11; i++)
	{
		CheckDlgButton(IDC_CHECK_GPIO0_LATCH + i, BST_CHECKED);		
		((CButton*)GetDlgItem(IDC_CHECK_GPIO0_LATCH + i))->SetWindowTextW(_T("H"));
	}

}


void CGPIOPWMDlg::OnBnClickedCheckGpio0Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO0_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO0_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio1Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO1_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO1_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO1_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio2Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO2_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO2_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO2_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio3Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO3_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO3_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO3_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio4Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO4_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO4_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO4_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio5Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO5_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO5_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO5_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio6Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO6_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO6_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO6_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio7Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO7_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO7_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO7_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio8Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO8_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO8_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO8_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio9Latch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO9_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO9_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO9_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpioaLatch()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIOA_LATCH);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIOA_LATCH, _T("H"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIOA_LATCH, _T("L"));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio0Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO0_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO0_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio1Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO1_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO1_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO1_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio2Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO2_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO2_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO2_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio3Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO3_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO3_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO3_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio4Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO4_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO4_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO4_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio5Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO5_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO5_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO5_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio6Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO6_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO6_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO6_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio7Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO7_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO7_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO7_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio8Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO8_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO8_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO8_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio9Mask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO9_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO9_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO9_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpioaMask()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIOA_MASK);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIOA_MASK, _T("M"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIOA_MASK, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio0Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO0_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO0_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio1Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO1_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO1_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO1_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio2Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO2_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO2_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO2_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio3Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO3_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO3_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO3_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio4Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO4_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO4_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO4_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio5Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO5_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO5_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO5_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio6Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO6_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO6_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO6_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio7Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO7_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO7_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO7_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio8Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO8_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO8_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO8_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpio9Pulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO9_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIO9_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIO9_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedCheckGpioaPulse()
{
	UINT ButtonState = 0;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIOA_PULSE);
	if (ButtonState == BST_CHECKED)
	{
		SetDlgItemText(IDC_CHECK_GPIOA_PULSE, _T("O"));
	}
	else
	{
		SetDlgItemText(IDC_CHECK_GPIOA_PULSE, _T(""));
	}
}


void CGPIOPWMDlg::OnBnClickedButtonSetGpioMode()
{
	int index;
	sGPIOModeSetting GPIOModeSetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	int i;
	for (i = GPIO0; i <= GPIOA; i++)
	{
	    index = ((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->GetCurSel();
		GPIOModeSetting.GPIO[i].Mode = eMode(index);

		ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_MASK + i);
		if (ButtonState == BST_CHECKED)
			GPIOModeSetting.GPIO[i].Setting = Enable;
		else
			GPIOModeSetting.GPIO[i].Setting = Disable;
	}    

	dwResult = SetGPIOMode(&gMWBridge, GPIOModeSetting);

	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOMode OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOMode Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	
}

void CGPIOPWMDlg::OnBnClickedButtonGetGpioMode()
{

	sGPIOModeSetting GPIOModeSetting;
	DWORD dwResult = ERROR_SUCCESS;
	int i;
	
	dwResult = GetGPIOMode(&gMWBridge, &GPIOModeSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOMode OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOMode Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
	
	for (i = GPIO0; i <= GPIOA; i++)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_GPIO0_MODE + i))->SetCurSel(GPIOModeSetting.GPIO[i].Mode);
	}
	
}


void CGPIOPWMDlg::OnBnClickedButtonSetGpioData()
{
	sGPIODataSetting GPIODataSetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	int i;

	for (i = GPIO0; i <= GPIOA; i++)
	{
		ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_LATCH + i);
		if (ButtonState == BST_CHECKED)
			GPIODataSetting.GPIO[i].Data = OutputHigh;
		else
			GPIODataSetting.GPIO[i].Data = OutputLow;

		ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_MASK + i);
		if (ButtonState == BST_CHECKED)
			GPIODataSetting.GPIO[i].Setting = Enable;
		else
			GPIODataSetting.GPIO[i].Setting = Disable;
	}

	dwResult = SetGPIOData(&gMWBridge, GPIODataSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOData OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOData Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
}


void CGPIOPWMDlg::OnBnClickedButtonGetGpioData()
{
	sGPIODataSetting GPIODataSetting;
	DWORD dwResult = ERROR_SUCCESS;
	int i;
	
	dwResult = GetGPIOData(&gMWBridge, &GPIODataSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOData OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOData Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}	

	for (i = GPIO0; i <= GPIOA; i++)
	{
		if (GPIODataSetting.GPIO[i].Data == OutputHigh)
		{
			SetDlgItemText(IDC_CHECK_GPIO0_LATCH + i, _T("H"));
			CheckDlgButton(IDC_CHECK_GPIO0_LATCH + i, BST_CHECKED);
		}
		else
		{
			SetDlgItemText(IDC_CHECK_GPIO0_LATCH + i, _T("L"));
			CheckDlgButton(IDC_CHECK_GPIO0_LATCH + i, BST_UNCHECKED);
		}
	}
}


void CGPIOPWMDlg::OnBnClickedButtonSetGpioPulse()
{
	sGPIOPulseSetting GPIOPulseSetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	int i;

	for (i = GPIO0; i <= GPIOA; i++)
	{
		ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_PULSE + i);
		if (ButtonState == BST_CHECKED)
			GPIOPulseSetting.GPIO[i] = OutputPulse;
		else
			GPIOPulseSetting.GPIO[i] = None;
	}
	
	dwResult = SetGPIOPulse(&gMWBridge, GPIOPulseSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOPulse OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOPulse Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
}



void CGPIOPWMDlg::OnBnClickedButtonGetGpioPinStatus()
{
	sGPIOPinStatus GPIOPinStatus;
	DWORD dwResult = ERROR_SUCCESS;
	int i;
	
	dwResult = GetGPIOPinStatus(&gMWBridge, &GPIOPinStatus);
	if (dwResult == ERROR_SUCCESS)
	{
	
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOData OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOData Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
    
	for (i = GPIO0; i <= GPIOA; i++)
	{
		if (GPIOPinStatus.GPIO[i] == InputHigh)
		{
			SetDlgItemText(IDC_CHECK_GPIO0_INPUT + i, _T("H"));
			CheckDlgButton(IDC_CHECK_GPIO0_INPUT + i, BST_CHECKED);
		}
		else
		{
			SetDlgItemText(IDC_CHECK_GPIO0_INPUT + i, _T("L"));
			CheckDlgButton(IDC_CHECK_GPIO0_INPUT + i, BST_UNCHECKED);
		}
	}
}


void CGPIOPWMDlg::OnBnClickedButtonSetGpioInterrupt()
{
	sGPIOInterruptSetting GPIOInterruptSetting;
	int ButtonState;
	DWORD dwResult = ERROR_SUCCESS;
	int i;

	for (i = GPIO0; i < GPIO8; i++)
	{
		ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_INTERRUPT + i);
		if (ButtonState == BST_CHECKED)
			GPIOInterruptSetting.GPIO[i] = Enable;
		else
			GPIOInterruptSetting.GPIO[i] = Disable;
	}
	
	dwResult = SetGPIOInterrupt(&gMWBridge, GPIOInterruptSetting);
	
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH,  _T("SetGPIOInterrupt OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("SetGPIOInterrupt Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
	
}


void CGPIOPWMDlg::OnBnClickedButtonGetGpioInterrupt()
{
	sGPIOInterruptSetting GPIOInterruptSetting;
	DWORD dwResult = ERROR_SUCCESS;
	int i;

	dwResult = GetGPIOInterrupt(&gMWBridge, &GPIOInterruptSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOInterrupt OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetGPIOInterrupt Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}

	for (i = GPIO0; i < GPIO8; i++)
	{
		if (GPIOInterruptSetting.GPIO[i] == Enable)
		{
			SetDlgItemText(IDC_CHECK_GPIO0_INTERRUPT + i, _T("V"));
			CheckDlgButton(IDC_CHECK_GPIO0_INTERRUPT + i, BST_CHECKED);
		}
		else
		{
			SetDlgItemText(IDC_CHECK_GPIO0_INTERRUPT + i, _T(""));
			CheckDlgButton(IDC_CHECK_GPIO0_INTERRUPT + i, BST_UNCHECKED);
		}
	}

}


void CGPIOPWMDlg::OnBnClickedCheckGpio0Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO0_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO0_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO0_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio1Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO1_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO1_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO1_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio2Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO2_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO2_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO2_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio3Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO3_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO3_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO3_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio4Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO4_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO4_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO4_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio5Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO5_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO5_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO5_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio6Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO6_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO6_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO6_INTERRUPT, _T(""));
}


void CGPIOPWMDlg::OnBnClickedCheckGpio7Interrupt()
{
	int ButtonState;
	ButtonState = IsDlgButtonChecked(IDC_CHECK_GPIO7_INTERRUPT);
	if (ButtonState == BST_CHECKED)
		SetDlgItemText(IDC_CHECK_GPIO7_INTERRUPT, _T("V"));
	else
		SetDlgItemText(IDC_CHECK_GPIO7_INTERRUPT, _T(""));
}





void CGPIOPWMDlg::OnBnClickedButtonGetGpioStatus()
{
	sStatus Status;
	DWORD dwResult = ERROR_SUCCESS;
	int i;

	dwResult = GetStatus(&gMWBridge, &Status);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetStatus OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("GetStatus Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
		return;
	}
		
	for (i = GPIO0; i <= GPIO7; i++)
	{
		if (Status.GPIO.GPIOChange[i] == TRUE)
		{
			SetDlgItemText(IDC_CHECK_GPIO0_CHANGE + i, _T("C"));
			CheckDlgButton(IDC_CHECK_GPIO0_CHANGE + i, BST_CHECKED);
		}
		else
		{
			SetDlgItemText(IDC_CHECK_GPIO0_CHANGE + i, _T(""));
			CheckDlgButton(IDC_CHECK_GPIO0_CHANGE + i, BST_UNCHECKED);
		}
	}
}
