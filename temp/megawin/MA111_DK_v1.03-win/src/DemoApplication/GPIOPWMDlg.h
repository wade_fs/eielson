#pragma once
#include "afxwin.h"


// CGPIOPWMDlg dialog

class CGPIOPWMDlg : public CDialog
{
	DECLARE_DYNAMIC(CGPIOPWMDlg)

public:
	CGPIOPWMDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGPIOPWMDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_GPIO_PWM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	void InitialInterface();
	afx_msg void OnBnClickedCheckGpio0Latch();
	afx_msg void OnBnClickedCheckGpio1Latch();
	afx_msg void OnBnClickedCheckGpio2Latch();
	afx_msg void OnBnClickedCheckGpio3Latch();
	afx_msg void OnBnClickedCheckGpio4Latch();
	afx_msg void OnBnClickedCheckGpio5Latch();
	afx_msg void OnBnClickedCheckGpio6Latch();
	afx_msg void OnBnClickedCheckGpio7Latch();
	afx_msg void OnBnClickedCheckGpio8Latch();
	afx_msg void OnBnClickedCheckGpio9Latch();
	afx_msg void OnBnClickedCheckGpioaLatch();
	afx_msg void OnBnClickedCheckGpio0Mask();
	afx_msg void OnBnClickedCheckGpio1Mask();
	afx_msg void OnBnClickedCheckGpio2Mask();
	afx_msg void OnBnClickedCheckGpio3Mask();
	afx_msg void OnBnClickedCheckGpio4Mask();
	afx_msg void OnBnClickedCheckGpio5Mask();
	afx_msg void OnBnClickedCheckGpio6Mask();
	afx_msg void OnBnClickedCheckGpio7Mask();
	afx_msg void OnBnClickedCheckGpio8Mask();
	afx_msg void OnBnClickedCheckGpio9Mask();
	afx_msg void OnBnClickedCheckGpioaMask();
	afx_msg void OnBnClickedCheckGpio0Pulse();
	afx_msg void OnBnClickedCheckGpio1Pulse();
	afx_msg void OnBnClickedCheckGpio2Pulse();
	afx_msg void OnBnClickedCheckGpio3Pulse();
	afx_msg void OnBnClickedCheckGpio4Pulse();
	afx_msg void OnBnClickedCheckGpio5Pulse();
	afx_msg void OnBnClickedCheckGpio6Pulse();
	afx_msg void OnBnClickedCheckGpio7Pulse();
	afx_msg void OnBnClickedCheckGpio8Pulse();
	afx_msg void OnBnClickedCheckGpio9Pulse();
	afx_msg void OnBnClickedCheckGpioaPulse();
	afx_msg void OnBnClickedButtonSetGpioMode();
	afx_msg void OnBnClickedButtonSetGpioData();
	afx_msg void OnBnClickedButtonGetGpioMode();
	afx_msg void OnBnClickedButtonSetGpioPulse();
	afx_msg void OnBnClickedButtonGetGpioData();
	afx_msg void OnBnClickedButtonGetGpioPinStatus();
	afx_msg void OnBnClickedButtonSetGpioInterrupt();
	afx_msg void OnBnClickedButtonGetGpioInterrupt();
	afx_msg void OnBnClickedCheckGpio0Interrupt();
	afx_msg void OnBnClickedCheckGpio1Interrupt();
	afx_msg void OnBnClickedCheckGpio2Interrupt();
	afx_msg void OnBnClickedCheckGpio3Interrupt();
	afx_msg void OnBnClickedCheckGpio4Interrupt();
	afx_msg void OnBnClickedCheckGpio5Interrupt();
	afx_msg void OnBnClickedCheckGpio6Interrupt();
	afx_msg void OnBnClickedCheckGpio7Interrupt();
	afx_msg void OnBnClickedButtonGetGpioStatus();
};
