
// DemoApplicationDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxext.h"
#include "afxwin.h"
#include "MWBridgeDLL.h"

#define WM_USER_UPDATE_STATE                    WM_APP+1

// CDemoApplicationDlg dialog
class CDemoApplicationDlg : public CDialogEx
{
// Construction
public:
	CDemoApplicationDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_DEMOAPPLICATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_tabMode;
	CStatusBar m_StatusBar;
	unsigned long CreateStatusBar();
	unsigned long InitialTabCtrl();
	CGPIOPWMDlg m_GPIOPWMDlg;
	CUARTDlg m_UARTDlg;
	CSPIDlg m_SPIDlg;
	CTWIDlg m_TWIDlg;
	COtherDlg m_OtherDlg;
	void InitialInterface();
	afx_msg void OnTcnSelchangeTabMode(NMHDR *pNMHDR, LRESULT *pResult);
	CComboBox m_comboDeviceIndex;
	CTCEdit m_editVendorID;
	CTCEdit m_editProductID;
	afx_msg void OnBnClickedButtonConnectBridge();
	afx_msg void OnBnClickedButtonDisconnectBridge();
	afx_msg void OnBnClickedButtonResetBridge();
	CComboBox m_comboDelayTime;
	CComboBox m_comboResetTime;
	afx_msg void OnBnClickedButtonGetDllVersion();
	afx_msg void OnBnClickedButtonGetFwVersion();
	afx_msg void OnBnClickedButtonClearBuffer();
	CComboBox m_comboBufferClearType;
	afx_msg void OnBnClickedButtonSetStreamModeIdle();
	afx_msg void OnBnClickedButtonGetStreamModeIdle();
	afx_msg void OnBnClickedButtonSelectOutputFile();
	afx_msg void OnBnClickedButtonSelectInputFile();
	DWORD SelectFilePath(int nIDDlgItem, bool bOpen);
	afx_msg void OnBnClickedButtonStreamWrite();
	afx_msg void OnBnClickedButtonStreamRead();
	sOptions m_Options;
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg void OnEnChangeEditReadTimeout();
	afx_msg void OnEnChangeEditWriteTimeout();
	afx_msg void OnBnClickedButtonSelectInputFile21();
	afx_msg void OnBnClickedButtonStreamRead2();
};
