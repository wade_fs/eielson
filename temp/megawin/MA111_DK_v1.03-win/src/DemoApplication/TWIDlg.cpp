// TWIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DemoApplication.h"
#include "TCEdit.h"
#include "TWIDlg.h"
#include "MWBridgeDLL.h"
#include "afxdialogex.h"
#include "GPIOPWMDlg.h"
#include "OtherDlg.h"
#include "SPIDlg.h"
#include "UARTDlg.h"
#include "DemoApplicationDlg.h"
// CTWIDlg dialog

extern sMWBridge gMWBridge;
IMPLEMENT_DYNAMIC(CTWIDlg, CDialog)

CTWIDlg::CTWIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTWIDlg::IDD, pParent),
	m_hEditAuxBuffer(NULL)
{

}

CTWIDlg::~CTWIDlg()
{
}

void CTWIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_TWI_SPEED_SELECTION, m_comboTWISpeedSelection);
	DDX_Control(pDX, IDC_COMBO_TWI_SCL_TIMEOUT_SELECTION, m_comboTWISCLTimeoutSelection);
	DDX_Control(pDX, IDC_COMBO_TWI_OPERATION_TYPE, m_comboTWIOperationType);
	DDX_Control(pDX, IDC_EDIT_TWI_DEVICE_ADDRESS, m_editTWIDeviceAddress);
	DDX_Control(pDX, IDC_EDIT_TWI_AUX_BUFFER_SIZE, m_editTWIAuxBufferSize);
}


BEGIN_MESSAGE_MAP(CTWIDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_TWI_SET_STREAM_MODE, &CTWIDlg::OnBnClickedButtonTwiSetStreamMode)
	ON_BN_CLICKED(IDC_BUTTON_TWI_GET_STREAM_MODE, &CTWIDlg::OnBnClickedButtonTwiGetStreamMode)
	ON_EN_CHANGE(IDC_EDIT_TWI_AUX_BUFFER_SIZE, &CTWIDlg::OnChangeEditTwiAuxBufferSize)
END_MESSAGE_MAP()


// CTWIDlg message handlers


void CTWIDlg::InitialInterface()
{
	m_comboTWISCLTimeoutSelection.AddString(_T("    20 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("    40 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("    80 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("  160 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("  320 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("  640 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("  960 ms"));
	m_comboTWISCLTimeoutSelection.AddString(_T("1280 ms"));
	m_comboTWISCLTimeoutSelection.SetCurSel(0);

	m_comboTWISpeedSelection.AddString(_T("    25 K"));
	m_comboTWISpeedSelection.AddString(_T("    50 K"));
	m_comboTWISpeedSelection.AddString(_T("    75 K"));
	m_comboTWISpeedSelection.AddString(_T("  100 K"));
	m_comboTWISpeedSelection.SetCurSel(0);

	m_comboTWIOperationType.AddString(_T("Idle"));
	m_comboTWIOperationType.AddString(_T("Sequential Write"));
	m_comboTWIOperationType.AddString(_T("Sequential Read"));
	m_comboTWIOperationType.AddString(_T("RandomRead"));
	m_comboTWIOperationType.SetCurSel(0);

	CheckDlgButton(IDC_RADIO_TWI_NO_ACTION, BST_CHECKED);
	m_editTWIDeviceAddress.SetWindowTextW(_T("00"));
	m_editTWIDeviceAddress.SetLimitText(2);
	m_editTWIAuxBufferSize.SetLimitText(2);
	m_editTWIAuxBufferSize.SetWindowText(_T("00"));

	DWORD hc[] = { 0x55, 0xC8C8C8, 0, 0xC8C8C8, 0, 0xC8C8C8, 0, 0xC8C8C8, 0 };	
	m_hEditAuxBuffer = ::GetDlgItem(m_hWnd, IDC_EDIT_TWI_AUX_BUFFER);
	::SendMessage(m_hEditAuxBuffer, HM_COLOR, (WPARAM)&hc, 0);           // Set Color
	::SendMessage(m_hEditAuxBuffer, HM_FONTSIZE, 14, 0);                 // Set Font Size
	::SendMessage(m_hEditAuxBuffer, HM_LINELENGTH, MAKELONG(0, 4), 0);   // Set Line Length

}


BOOL CTWIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CTWIDlg::OnBnClickedButtonTwiSetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	UINT ButtonState;

	StreamSetting.Mode = TWI;
	ButtonState = IsDlgButtonChecked(IDC_RADIO_TWI_NO_ACTION);
	if (ButtonState == BST_CHECKED)
		StreamSetting.TWI.ResetControl = NoAction;
	else
		StreamSetting.TWI.ResetControl = ResetTWIEngine;

	ButtonState = IsDlgButtonChecked(IDC_CHECK_TWI_SCL_TIMEOUT_DETECTION_ENABLE);
	if (ButtonState == BST_CHECKED)
		StreamSetting.TWI.SCLTimeoutDetectionControl = Enable;
	else
		StreamSetting.TWI.SCLTimeoutDetectionControl = Disable;

	StreamSetting.TWI.SCLTimeoutSelection = eSCLTimeoutSelection(m_comboTWISCLTimeoutSelection.GetCurSel());
	StreamSetting.TWI.SpeedSelection = eSpeedSelection(m_comboTWISpeedSelection.GetCurSel());

	dwResult = SetStreamMode(&gMWBridge, StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		((CDemoApplicationDlg*)GetParent())->m_Options.Mode = TWI;
		_stprintf_s(tmpBuffer, _T("SetStreamMode TWI OK !"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetStreamMode TWI Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CTWIDlg::OnBnClickedButtonTwiGetStreamMode()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetStreamMode(&gMWBridge, &StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		if (StreamSetting.Mode == TWI)
		    _stprintf_s(tmpBuffer, _T("GetStreamMode TWI OK !"));
		else
			_stprintf_s(tmpBuffer, _T("GetStreamMode OK ! But not in TWI Mode"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetStreamMode TWI Fail ! Error Code = %8X"), dwResult);
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}

	if (StreamSetting.Mode == TWI)
	{
		if (StreamSetting.TWI.ResetControl == NoAction)
		{
			CheckDlgButton(IDC_RADIO_TWI_NO_ACTION, BST_CHECKED);
			CheckDlgButton(IDC_RADIO_TWI_RESET_TWI_ENGINE, BST_UNCHECKED);
		}
		else
		{
			CheckDlgButton(IDC_RADIO_TWI_NO_ACTION, BST_UNCHECKED);
			CheckDlgButton(IDC_RADIO_TWI_RESET_TWI_ENGINE, BST_CHECKED);
		}
		if (StreamSetting.TWI.SCLTimeoutDetectionControl == Enable)
			CheckDlgButton(IDC_CHECK_TWI_SCL_TIMEOUT_DETECTION_ENABLE, BST_CHECKED);
		else
			CheckDlgButton(IDC_CHECK_TWI_SCL_TIMEOUT_DETECTION_ENABLE, BST_UNCHECKED);

		m_comboTWISCLTimeoutSelection.SetCurSel(StreamSetting.TWI.SCLTimeoutSelection);
		m_comboTWISpeedSelection.SetCurSel(StreamSetting.TWI.SpeedSelection);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetStreamMode Not In TWI Mode!"));
		((CDemoApplicationDlg*)GetParent())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


BOOL CTWIDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}
	else
	{
		return CDialog::PreTranslateMessage(pMsg);
	}
}


void CTWIDlg::OnChangeEditTwiAuxBufferSize()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	int Index = 0;
	DWORD dwSize = 0;
	TCHAR tmpBuffer[0x10] = { 0 };
	BYTE *pBuffer = NULL;

	Index = m_comboTWIOperationType.GetCurSel();
	if (Index == 3) // OperationType = Sequence Read
	{
		m_editTWIAuxBufferSize.GetWindowText(tmpBuffer, 0x10);
		dwSize = _tcstoul(tmpBuffer, NULL, 16);
		if (dwSize > 4)
		{
			m_editTWIAuxBufferSize.SetWindowText(_T("4"));
			dwSize = 4;
		}
		if (dwSize == 0)
		{
			m_editTWIAuxBufferSize.SetWindowText(_T("1"));
			dwSize = 1;
		}

		if (dwSize != 0) 
		{
			
			pBuffer = new BYTE[dwSize];
			if (pBuffer != NULL)
			{
				memset(pBuffer, 0, dwSize);
				::SendMessage(m_hEditAuxBuffer, HM_LIMITSIZE, dwSize, 0);
				::SendMessage(m_hEditAuxBuffer, HM_SETDATA, (WPARAM)pBuffer, dwSize);
				delete[]pBuffer;
				pBuffer = NULL;
			}

		}
	}
}
