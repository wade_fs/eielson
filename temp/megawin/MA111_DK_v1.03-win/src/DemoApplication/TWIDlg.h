#pragma once
#include "afxwin.h"


// CTWIDlg dialog

class CTWIDlg : public CDialog
{
	DECLARE_DYNAMIC(CTWIDlg)

public:
	CTWIDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTWIDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_TWI };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboTWISpeedSelection;
	void InitialInterface();
	virtual BOOL OnInitDialog();
	CComboBox m_comboTWISCLTimeoutSelection;
	CComboBox m_comboTWIOperationType;
	afx_msg void OnBnClickedButtonTwiSetStreamMode();
	afx_msg void OnBnClickedButtonTwiGetStreamMode();
	CTCEdit m_editTWIDeviceAddress;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	HWND m_hEditAuxBuffer;
	afx_msg void OnChangeEditTwiAuxBufferSize();
	CTCEdit m_editTWIAuxBufferSize;
};
