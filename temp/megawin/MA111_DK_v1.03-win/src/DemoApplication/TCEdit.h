#pragma once


// CTCEdit
enum enumEditMode               // Declare enum type Days
{
   NORMAL_MODE,
   INTEGER_MODE,
   HEX_MODE 
} ;

class CTCEdit : public CEdit
{
	DECLARE_DYNAMIC(CTCEdit)

public:
	CTCEdit();
	virtual ~CTCEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
private:
	enumEditMode m_EditMode;
public:
	enumEditMode GetEditMode(void);
	DWORD SetEditMode(enumEditMode EditMode);
};

