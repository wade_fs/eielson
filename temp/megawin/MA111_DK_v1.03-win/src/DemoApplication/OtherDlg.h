#pragma once
#include "afxwin.h"


// COtherDlg dialog

class COtherDlg : public CDialog
{
	DECLARE_DYNAMIC(COtherDlg)

public:
	COtherDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COtherDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_OTHERS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboICKOConfigure;
	virtual BOOL OnInitDialog();
	void InitialInterface();
	afx_msg void OnBnClickedButtonSetGpioAlternateFunction();
	afx_msg void OnBnClickedButtonGetGpioAlternateFunction();
	afx_msg void OnBnClickedButtonSetPwmDutyValue();
	afx_msg void OnBnClickedButtonGetPwmDutyValue();
	CTCEdit m_editPWM0Duty;
	CTCEdit m_editPWM1Duty;
	CTCEdit m_editPWM2Duty;
	afx_msg void OnBnClickedCheckPwm0Mask();
	afx_msg void OnBnClickedCheckPwm1Mask();
	afx_msg void OnBnClickedCheckPwm2Mask();
};
