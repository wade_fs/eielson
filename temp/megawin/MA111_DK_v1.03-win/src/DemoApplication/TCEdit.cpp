// TCEdit.cpp : ��@��
//

#include "stdafx.h"
#include "TCEdit.h"


// CTCEdit

IMPLEMENT_DYNAMIC(CTCEdit, CEdit)

CTCEdit::CTCEdit()
{  
	m_EditMode = HEX_MODE;	
}

CTCEdit::~CTCEdit()
{
}

BEGIN_MESSAGE_MAP(CTCEdit, CEdit)
	ON_WM_CHAR()
END_MESSAGE_MAP()

void CTCEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CString strTmp;
	CString strTmpOld;
	switch (m_EditMode)
	{
	case NORMAL_MODE:
	     CEdit::OnChar(nChar, nRepCnt, nFlags);
	     break;
	case INTEGER_MODE:	     
		 if (((nChar >= 0x30) && (nChar <= 0x39)) || (nChar == 0x08))
  		     CEdit::OnChar(nChar, nRepCnt, nFlags);		
	     break;
	case HEX_MODE:
	 	 if (((nChar >= 0x30) && (nChar <= 0x39)) || 
  			 ((nChar >= 'a') && (nChar <= 'f')) ||
			 ((nChar >= 'A') && (nChar <= 'F')) ||
             (nChar == 8))
		 {	
             CEdit::OnChar(nChar, nRepCnt, nFlags);		
		 } 
	     break;
	}
}

enumEditMode CTCEdit::GetEditMode(void)
{
	return m_EditMode;
}

DWORD CTCEdit::SetEditMode(enumEditMode EditMode)
{
    m_EditMode = EditMode;
	if (m_EditMode == HEX_MODE)
	    this->ModifyStyle(0, ES_UPPERCASE, 0);
	else
	    this->ModifyStyle(ES_UPPERCASE, 0, 0);
	return ERROR_SUCCESS;
}


