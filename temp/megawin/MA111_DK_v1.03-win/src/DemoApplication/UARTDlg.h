#pragma once
#include "afxwin.h"


// CUARTDlg dialog

class CUARTDlg : public CDialog
{
	DECLARE_DYNAMIC(CUARTDlg)

public:
	CUARTDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUARTDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_UART };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboUARTMode;
	CComboBox m_comboBaudRate;
	CComboBox m_comboStopBit;
	CComboBox m_comboParitySelection;
	CComboBox m_comboUARTBitOrder;
	CComboBox m_comboBreakTiming;
	CComboBox m_comboRTSPolaritySelection;
	CComboBox m_comboCTSPolaritySelection;
	CComboBox m_comboDEPolaritySelection;
	virtual BOOL OnInitDialog();
	DWORD InitialInterface();
	CTCEdit m_editSADDR1;
	CTCEdit m_editSADDR2;
	CTCEdit m_editUartTargetAddress;
	CTCEdit m_editUartSlaveAddress;
	afx_msg void OnBnClickedButtonUartSetStreamMode();
	afx_msg void OnBnClickedButtonUartGetStreamMode();
	afx_msg void OnBnClickedButtonGetStatusUart();
};
