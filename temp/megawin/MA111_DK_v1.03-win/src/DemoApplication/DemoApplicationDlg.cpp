
// DemoApplicationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TCEdit.h"
#include "DemoApplication.h"
#include "GPIOPWMDlg.h"
#include "UARTDlg.h"
#include "SPIDlg.h"
#include "TWIDlg.h"
#include "OtherDlg.h"
#include "DemoApplicationDlg.h"
#include "afxdialogex.h"

#include "MWBridgeDLL.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define CHILD_DIALOG_CX                                  10
#define CHILD_DIALOG_CY                                 125
#define CHILD_DIALOG_WIDTH                              480
#define CHILD_DIALOG_HEIGHT                             410

// CDemoApplicationDlg dialog
const UINT BASED_CODE indicators[] =            // used for status bar
{
	ID_INDICATOR_MSG,
	ID_INDICATOR_STATUS,
	ID_INDICATOR_VERSION
};

sMWBridge gMWBridge;
CFile TxFile;
CFile RxFile;
CFile RxFile2;

CWinThread * pReadThread = NULL;
CWinThread * pReadThread2 = NULL;
CWinThread * pWriteThread = NULL;
UINT Thread_Read(LPVOID pParam);
UINT Thread_Read2(LPVOID pParam);
UINT Thread_Write(LPVOID pParam);
BYTE * pTxBuffer = NULL;
DWORD dwTxFileSize = 0;
BYTE * pRxBuffer = NULL;
BYTE * pRxBuffer2 = NULL;
DWORD dwRxSize = 0;
DWORD dwRxSize2 = 0;

CDemoApplicationDlg::CDemoApplicationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDemoApplicationDlg::IDD, pParent)
	, m_GPIOPWMDlg(false)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoApplicationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_MODE, m_tabMode);
	DDX_Control(pDX, IDC_COMBO_DEVICE_INDEX, m_comboDeviceIndex);
	DDX_Control(pDX, IDC_EDIT_VENDOR_ID, m_editVendorID);
	DDX_Control(pDX, IDC_EDIT_PRODUCT_ID, m_editProductID);
	DDX_Control(pDX, IDC_COMBO_DELAY_TIME, m_comboDelayTime);
	DDX_Control(pDX, IDC_COMBO_RESET_TIME, m_comboResetTime);
	DDX_Control(pDX, IDC_COMBO_BUFFER_CLEAR_TYPE, m_comboBufferClearType);
}

BEGIN_MESSAGE_MAP(CDemoApplicationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_MODE, &CDemoApplicationDlg::OnTcnSelchangeTabMode)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT_BRIDGE, &CDemoApplicationDlg::OnBnClickedButtonConnectBridge)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT_BRIDGE, &CDemoApplicationDlg::OnBnClickedButtonDisconnectBridge)
	ON_BN_CLICKED(IDC_BUTTON_RESET_BRIDGE, &CDemoApplicationDlg::OnBnClickedButtonResetBridge)
	ON_BN_CLICKED(IDC_BUTTON_GET_DLL_VERSION, &CDemoApplicationDlg::OnBnClickedButtonGetDllVersion)
	ON_BN_CLICKED(IDC_BUTTON_GET_FW_VERSION, &CDemoApplicationDlg::OnBnClickedButtonGetFwVersion)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_BUFFER, &CDemoApplicationDlg::OnBnClickedButtonClearBuffer)
	ON_BN_CLICKED(IDC_BUTTON_SET_STREAM_MODE_IDLE, &CDemoApplicationDlg::OnBnClickedButtonSetStreamModeIdle)
	ON_BN_CLICKED(IDC_BUTTON_GET_STREAM_MODE_IDLE, &CDemoApplicationDlg::OnBnClickedButtonGetStreamModeIdle)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_OUTPUT_FILE, &CDemoApplicationDlg::OnBnClickedButtonSelectOutputFile)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_INPUT_FILE, &CDemoApplicationDlg::OnBnClickedButtonSelectInputFile)
	ON_BN_CLICKED(IDC_BUTTON_STREAM_WRITE, &CDemoApplicationDlg::OnBnClickedButtonStreamWrite)
	ON_BN_CLICKED(IDC_BUTTON_STREAM_READ, &CDemoApplicationDlg::OnBnClickedButtonStreamRead)
	ON_EN_CHANGE(IDC_EDIT_READ_TIMEOUT, &CDemoApplicationDlg::OnEnChangeEditReadTimeout)
	ON_EN_CHANGE(IDC_EDIT_WRITE_TIMEOUT, &CDemoApplicationDlg::OnEnChangeEditWriteTimeout)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_INPUT_FILE21, &CDemoApplicationDlg::OnBnClickedButtonSelectInputFile21)
	ON_BN_CLICKED(IDC_BUTTON_STREAM_READ2, &CDemoApplicationDlg::OnBnClickedButtonStreamRead2)
END_MESSAGE_MAP()


// CDemoApplicationDlg message handlers

BOOL CDemoApplicationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	InitialInterface();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDemoApplicationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDemoApplicationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



unsigned long CDemoApplicationDlg::CreateStatusBar()
{
	TCHAR tmpBuffer[0x20];

	m_StatusBar.Create(this); //We create the status bar
	m_StatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)); //Set the number of panes 

	CRect rect;
	GetClientRect(&rect);
	//Size the two panes
	m_StatusBar.SetPaneInfo(0, ID_INDICATOR_MSG, SBPS_NORMAL, rect.Width() - 200);
	m_StatusBar.SetPaneInfo(1, ID_INDICATOR_STATUS, SBPS_NORMAL, 100);
	m_StatusBar.SetPaneInfo(2, ID_INDICATOR_VERSION, SBPS_STRETCH, 100);

	//This is where we actually draw it on the screen
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, ID_INDICATOR_VERSION);

	m_StatusBar.SetPaneText(1, _T(""));
	m_StatusBar.SetPaneText(0, _T(""));

	_stprintf_s(tmpBuffer, _T("   %S"), __DATE__);
	m_StatusBar.SetPaneText(2, tmpBuffer);

	return ERROR_SUCCESS;
}


unsigned long CDemoApplicationDlg::InitialTabCtrl()
{
	TC_ITEM TabCtrlItem;

	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = _T("GPIO && PWM");
	m_tabMode.InsertItem(0, &TabCtrlItem);
	TabCtrlItem.pszText = _T("Uart Mode");
	m_tabMode.InsertItem(1, &TabCtrlItem);
	TabCtrlItem.pszText = _T("SPI Mode");
	m_tabMode.InsertItem(2, &TabCtrlItem);
	TabCtrlItem.pszText = _T("TWI Mdoe");
	m_tabMode.InsertItem(3, &TabCtrlItem);
	TabCtrlItem.pszText = _T("Alternate");
	m_tabMode.InsertItem(4, &TabCtrlItem);

	return 0;
}


void CDemoApplicationDlg::InitialInterface()
{
	CreateStatusBar();
	InitialTabCtrl();
	m_GPIOPWMDlg.Create(IDD_DIALOG_GPIO_PWM, this);
	m_GPIOPWMDlg.MoveWindow(CHILD_DIALOG_CX, CHILD_DIALOG_CY, CHILD_DIALOG_WIDTH, CHILD_DIALOG_HEIGHT);
	m_GPIOPWMDlg.ShowWindow(SW_SHOW);

	m_UARTDlg.Create(IDD_DIALOG_UART, this);
	m_UARTDlg.MoveWindow(CHILD_DIALOG_CX, CHILD_DIALOG_CY, CHILD_DIALOG_WIDTH, CHILD_DIALOG_HEIGHT);
	m_UARTDlg.ShowWindow(SW_HIDE);

	m_SPIDlg.Create(IDD_DIALOG_SPI, this);
	m_SPIDlg.MoveWindow(CHILD_DIALOG_CX, CHILD_DIALOG_CY, CHILD_DIALOG_WIDTH, CHILD_DIALOG_HEIGHT);
	m_SPIDlg.ShowWindow(SW_HIDE);

	m_TWIDlg.Create(IDD_DIALOG_TWI, this);
	m_TWIDlg.MoveWindow(CHILD_DIALOG_CX, CHILD_DIALOG_CY, CHILD_DIALOG_WIDTH, CHILD_DIALOG_HEIGHT);
	m_TWIDlg.ShowWindow(SW_HIDE);

	m_OtherDlg.Create(IDD_DIALOG_OTHERS, this);
	m_OtherDlg.MoveWindow(CHILD_DIALOG_CX, CHILD_DIALOG_CY, CHILD_DIALOG_WIDTH, CHILD_DIALOG_HEIGHT);
	m_OtherDlg.ShowWindow(SW_HIDE);

	m_comboDeviceIndex.AddString(_T("1"));
	m_comboDeviceIndex.AddString(_T("2"));
	m_comboDeviceIndex.SetCurSel(0);

	m_editVendorID.SetLimitText(4);
	m_editVendorID.SetWindowText(_T("0E6A"));
	m_editProductID.SetLimitText(4);
	m_editProductID.SetWindowText(_T("0124"));

	m_comboDelayTime.AddString(_T("  16 ms"));
	m_comboDelayTime.AddString(_T("  32 ms"));
	m_comboDelayTime.AddString(_T("  64 ms"));
	m_comboDelayTime.AddString(_T("128 ms"));
	m_comboDelayTime.SetCurSel(0);

	m_comboResetTime.AddString(_T("512 ms"));
	m_comboResetTime.AddString(_T("    1 s"));
	m_comboResetTime.AddString(_T(" 1.5 s"));
	m_comboResetTime.AddString(_T("    2 s"));
	m_comboResetTime.AddString(_T(" 2.5 s"));
	m_comboResetTime.AddString(_T("    3 s"));
	m_comboResetTime.AddString(_T(" 3.5 s"));
	m_comboResetTime.AddString(_T("    4 s"));
	m_comboResetTime.SetCurSel(0);

	m_comboBufferClearType.AddString(_T("Clear DLL"));
	m_comboBufferClearType.AddString(_T("Clear Bridge"));
	m_comboBufferClearType.AddString(_T("Clear All"));
	m_comboBufferClearType.SetCurSel(0);

	SetDlgItemText(IDC_EDIT_READ_TIMEOUT, _T("5000"));
	SetDlgItemText(IDC_EDIT_WRITE_TIMEOUT, _T("5000"));

	m_Options.SPI.pAUXBuffer = NULL;
	m_Options.TWI.pAUXBuffer = NULL;	
}

void CDemoApplicationDlg::OnTcnSelchangeTabMode(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	int iDfuMode = m_tabMode.GetCurSel();
	switch (iDfuMode)
	{
	case 0:
		m_GPIOPWMDlg.ShowWindow(SW_SHOW);
		m_UARTDlg.ShowWindow(SW_HIDE);
		m_SPIDlg.ShowWindow(SW_HIDE);
		m_TWIDlg.ShowWindow(SW_HIDE);
		m_OtherDlg.ShowWindow(SW_HIDE);
		break;
	case 1:
		m_GPIOPWMDlg.ShowWindow(SW_HIDE);
		m_UARTDlg.ShowWindow(SW_SHOW);
		m_SPIDlg.ShowWindow(SW_HIDE);
		m_TWIDlg.ShowWindow(SW_HIDE);
		m_OtherDlg.ShowWindow(SW_HIDE);
		break;
	case 2:
		m_GPIOPWMDlg.ShowWindow(SW_HIDE);
		m_UARTDlg.ShowWindow(SW_HIDE);
		m_SPIDlg.ShowWindow(SW_SHOW);
		m_TWIDlg.ShowWindow(SW_HIDE);
		m_OtherDlg.ShowWindow(SW_HIDE);
		break;
	case 3:
		m_GPIOPWMDlg.ShowWindow(SW_HIDE);
		m_UARTDlg.ShowWindow(SW_HIDE);
		m_SPIDlg.ShowWindow(SW_HIDE);
		m_TWIDlg.ShowWindow(SW_SHOW);	
		m_OtherDlg.ShowWindow(SW_HIDE);
		break;
	case 4:
		m_GPIOPWMDlg.ShowWindow(SW_HIDE);
		m_UARTDlg.ShowWindow(SW_HIDE);
		m_SPIDlg.ShowWindow(SW_HIDE);
		m_TWIDlg.ShowWindow(SW_HIDE);
		m_OtherDlg.ShowWindow(SW_SHOW);
		break;
	}
	*pResult = 0;
}


void CDemoApplicationDlg::OnBnClickedButtonConnectBridge()
{
	DWORD dwResult = ERROR_SUCCESS;
	UINT Index = 0;
	DWORD dwVID = 0;
	DWORD dwPID = 0;
	CString strTmp;
	DWORD dwBufferSize = 64 * 1024;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	UINT ReadTimeOut;
	UINT WriteTimeOut;

	m_editVendorID.GetWindowText(strTmp);	
	dwVID = _tcstoul(strTmp.GetBuffer(strTmp.GetLength()), NULL, 16);
	m_editProductID.GetWindowText(strTmp);
	dwPID = _tcstoul(strTmp.GetBuffer(strTmp.GetLength()), NULL, 16);
	Index = m_comboDeviceIndex.GetCurSel() + 1;

	gMWBridge.VID = dwVID;
	gMWBridge.PID = dwPID;
	ReadTimeOut = GetDlgItemInt(IDC_EDIT_READ_TIMEOUT);
	WriteTimeOut = GetDlgItemInt(IDC_EDIT_WRITE_TIMEOUT);
	gMWBridge.ReadTimeOut = ReadTimeOut;
	gMWBridge.WriteTimeOut = WriteTimeOut;

	dwBufferSize = 0x1000000;
	dwResult = ConnectBridge(&gMWBridge, Index, dwBufferSize);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("ConnectBridge OK !"));
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("ConnectBridge Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}		
}


void CDemoApplicationDlg::OnBnClickedButtonDisconnectBridge()
{
	DWORD dwResult = ERROR_SUCCESS;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = DisconnectBridge(&gMWBridge);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("DisconnectBridge OK !"));
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("DisconnectBridge Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CDemoApplicationDlg::OnBnClickedButtonResetBridge()
{
	DWORD dwResult = ERROR_SUCCESS;
	int DelayTimeIndex;
	int ResetTimeIndex;
	eDelayTime DelayTime;
	eResetTime ResetTime;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	DelayTimeIndex = m_comboDelayTime.GetCurSel();
	switch (DelayTimeIndex)
	{
	case 0: DelayTime = Time_16ms; break;
	case 1: DelayTime = Time_32ms; break;
	case 2: DelayTime = Time_64ms; break;
	case 3: DelayTime = Time_128ms; break;		  
	}

	ResetTimeIndex = m_comboResetTime.GetCurSel();
	switch (ResetTimeIndex)
	{
	case 0: ResetTime = Time_0_5s; break;
	case 1: ResetTime = Time_1_0s; break;
	case 2: ResetTime = Time_1_5s; break;
	case 3: ResetTime = Time_2_0s; break;
	case 4: ResetTime = Time_2_5s; break;
	case 5: ResetTime = Time_3_0s; break;
	case 6: ResetTime = Time_3_5s; break;
	case 7: ResetTime = Time_4_0s; break;
	}

	dwResult = ResetBridge(&gMWBridge, DelayTime, ResetTime);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("ResetBridge OK !"));
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("ResetBridge Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CDemoApplicationDlg::OnBnClickedButtonGetDllVersion()
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD dwDLLVersion = 0;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetDLLVersion(&dwDLLVersion);	
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("GetDLLVersion OK !, DLL Version = %08X"), dwDLLVersion);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetDLLVersion Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}

}


void CDemoApplicationDlg::OnBnClickedButtonGetFwVersion()
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD dwFWVersion = 0;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	/*
	dwResult = GetFWVersion(&dwFWVersion);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("GetFWVersion OK !, FW Version = %08X"), dwFWVersion);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetDLLVersion Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	*/
}


void CDemoApplicationDlg::OnBnClickedButtonClearBuffer()
{
	DWORD dwResult = ERROR_SUCCESS;
	int ClearTypeIndex;	
	eClear ClearType;	
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	ClearTypeIndex = m_comboBufferClearType.GetCurSel();
	switch (ClearTypeIndex)
	{
	case 0: ClearType = ClearDLL; break;
	case 1: ClearType = ClearBridge; break;
	case 2: ClearType = ClearAll; break;	
	}

	dwResult = ClearBuffer(&gMWBridge, ClearType);
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, _T("ClearBuffer OK !"));
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("ClearBuffer Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CDemoApplicationDlg::OnBnClickedButtonSetStreamModeIdle()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;	
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	
	StreamSetting.Mode = Idle;

	dwResult = SetStreamMode(&gMWBridge, StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{	
		m_Options.Mode = UART;
		_stprintf_s(tmpBuffer, _T("SetStreamMode Idle OK !"));
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("SetStreamMode Idel Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CDemoApplicationDlg::OnBnClickedButtonGetStreamModeIdle()
{
	DWORD dwResult = ERROR_SUCCESS;
	sStreamSetting StreamSetting;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	dwResult = GetStreamMode(&gMWBridge, &StreamSetting);
	if (dwResult == ERROR_SUCCESS)
	{
		if (StreamSetting.Mode == Idle)
		    _stprintf_s(tmpBuffer, _T("GetStreamMode Idle OK !"));
		else
		{
			if (StreamSetting.Mode == UART)
			   _stprintf_s(tmpBuffer, _T("GetStreamMode OK !, but now is in UART Mode"));
			if (StreamSetting.Mode == SPI)
				_stprintf_s(tmpBuffer, _T("GetStreamMode OK !, but now is in SPI Mode"));
			if (StreamSetting.Mode == TWI)
				_stprintf_s(tmpBuffer, _T("GetStreamMode OK !, but now is in TWI Mode"));
		}
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, _T("GetStreamMode Fail ! Error Code = %8X"), dwResult);
		m_StatusBar.SetPaneText(0, tmpBuffer);
	}
}


void CDemoApplicationDlg::OnBnClickedButtonSelectOutputFile()
{
	SelectFilePath(IDC_EDIT_FILE_PATH_TO_TRANSFER, TRUE);
}


void CDemoApplicationDlg::OnBnClickedButtonSelectInputFile()
{
	SelectFilePath(IDC_EDIT_FILE_PATH_TO_SAVE, FALSE);
}


DWORD CDemoApplicationDlg::SelectFilePath(int nIDDlgItem, bool bOpen)
{
	TCHAR szFilters[] = _T("Binary files (*.bin)|*.bin|All files (*.*)|*.*||");
	CFileException ex;
	CString strTmp;
	CFile TempFile;

	CFileDialog dlgOpen(bOpen, _T("All files"), _T("*.*"),
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);
	if (dlgOpen.DoModal() == IDOK)
	{
		strTmp = "";
		strTmp = dlgOpen.GetPathName();
		if (bOpen == TRUE)
		{
			if (!TempFile.Open(strTmp, CFile::modeRead | CFile::shareDenyWrite, &ex))
			{
				TCHAR szError[1024];
				ex.GetErrorMessage(szError, 1024);
				MessageBox(szError, _T("ERROR"), MB_OK);
				return -1;
			}
			else
			{
				TempFile.Close();
				if (nIDDlgItem != 0)
					((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
				return 0;
			}
		}
		else
		{
			if (!TempFile.Open(strTmp, CFile::modeRead | CFile::shareDenyWrite, &ex))
			{
				if (ex.m_cause == CFileException::fileNotFound)
				{
					if (!TempFile.Open(strTmp, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &ex))
					{
						TCHAR szError[1024];
						ex.GetErrorMessage(szError, 1024);
						MessageBox(szError, _T("ERROR"), MB_OK);
						return -1;
					}
					else
					{
						TempFile.Close();
						if (nIDDlgItem != 0)
							((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
						return 0;
					}
				}
				else
				{
					TempFile.Close();
					if (nIDDlgItem != 0)
						((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
					return 0;
				}
			}
			else
			{
				TempFile.Close();
				if (nIDDlgItem != 0)
					((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(strTmp);
				return 0;
			}
		}
	}
	else
	{
		if (nIDDlgItem != 0)
			((CEdit*)(GetDlgItem(nIDDlgItem)))->SetWindowText(_T(""));
		return 0;
	}
}


void CDemoApplicationDlg::OnBnClickedButtonStreamWrite()
{
	DWORD dwResult = ERROR_SUCCESS;	
	DWORD NumbberOfByteWritten = 0;
	UINT ButtonState;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	TCHAR txFilePath[MAX_PATH + 1];
	CFileException ex;
	


	if (m_Options.Mode == UART) 
	{
		ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_TARGET_ADDRESS_ACTIVE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.TADDRActive = TRUE;		
		else
			m_Options.UART.TADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_UART_TARGET_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.TADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		
	    ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_SLAVE_ADDRESS_ENABLE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.SADDRActive = TRUE;
		else
			m_Options.UART.SADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_SLAVE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.SADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));		
	}
	else if (m_Options.Mode == TWI)
	{
		m_Options.TWI.Mode = eTWIMode(((CComboBox*)FromHandle(::GetDlgItem(m_TWIDlg.m_hWnd, IDC_COMBO_TWI_OPERATION_TYPE)))->GetCurSel());
		::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_DEVICE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.TWI.Address = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		/*
		if (m_Options.TWI.Mode == TWI_RandomRead)
		{
			::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);
			m_Options.TWI.AUXBufferSize = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
			if (m_Options.TWI.pAUXBuffer != NULL)
				delete []m_Options.TWI.pAUXBuffer;
			m_Options.TWI.pAUXBuffer = new BYTE[m_Options.TWI.AUXBufferSize];
			if (m_Options.TWI.pAUXBuffer == NULL)
			{
				m_StatusBar.SetPaneText(0, _T("TWI : Not Enough Memory !"));
				return;
			}
		}
		*/
	}
	else if (m_Options.Mode == SPI)
	{		
		m_Options.SPI.Mode = eSPIMode(((CComboBox*)FromHandle(::GetDlgItem(m_SPIDlg.m_hWnd, IDC_COMBO_SPI_OPERATING_TYPE)))->GetCurSel());
		::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_DUMMY_BYTE, tmpBuffer, MAX_PATH);
		m_Options.SPI.WriteDummy = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		//if (m_Options.SPI.Mode == SPI_StandardWriteWithRead)
		//{
		//	::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);		
		//}
	}
		
	GetDlgItemText(IDC_EDIT_FILE_PATH_TO_TRANSFER, txFilePath, MAX_PATH);
	if (!TxFile.Open(txFilePath, CFile::modeRead | CFile::shareDenyWrite, &ex))
	{
		TCHAR szError[1024];
		ex.GetErrorMessage(szError, 1024);
		m_StatusBar.SetPaneText(0, szError);
		return;
	}
	else
	{
		dwTxFileSize = static_cast<DWORD>(TxFile.GetLength());
		if (pTxBuffer != NULL)
			delete[]pTxBuffer;
		pTxBuffer = new BYTE[dwTxFileSize];
		if (pTxBuffer != NULL)
		{
			TxFile.Read(pTxBuffer, dwTxFileSize);
			if (m_Options.Mode == SPI)
			{
				m_Options.SPI.AUXBufferSize = dwTxFileSize;
				m_Options.SPI.pAUXBuffer = new BYTE[m_Options.SPI.AUXBufferSize];
				if (m_Options.SPI.pAUXBuffer == NULL)
				{
					m_StatusBar.SetPaneText(0, _T("SPI : Not Enough Memory !"));
					return;
				}
			}
			pWriteThread = AfxBeginThread(Thread_Write, &m_Options);
			pWriteThread->m_bAutoDelete = TRUE;
		}
	}	
}


void CDemoApplicationDlg::OnBnClickedButtonStreamRead()
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD NumbberOfByteRead = 0;
	UINT ButtonState;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	TCHAR txFilePath[MAX_PATH + 1];
	CFileException ex;


	if (m_Options.Mode == UART)
	{
		ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_TARGET_ADDRESS_ACTIVE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.TADDRActive = TRUE;
		else
			m_Options.UART.TADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_UART_TARGET_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.TADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));

		ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_SLAVE_ADDRESS_ENABLE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.SADDRActive = TRUE;
		else
			m_Options.UART.SADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_SLAVE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.SADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
	}
	else if (m_Options.Mode == TWI)
	{
		m_Options.TWI.Mode = eTWIMode(((CComboBox*)FromHandle(::GetDlgItem(m_TWIDlg.m_hWnd, IDC_COMBO_TWI_OPERATION_TYPE)))->GetCurSel());
		::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_DEVICE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.TWI.Address = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		if (m_Options.TWI.Mode == TWI_RandomRead)
		{
			::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);
			m_Options.TWI.AUXBufferSize = static_cast<DWORD>(_tcstoul(tmpBuffer, NULL, 16));
			if (m_Options.TWI.pAUXBuffer != NULL)
				delete[]m_Options.TWI.pAUXBuffer;
			m_Options.TWI.pAUXBuffer = new BYTE[m_Options.TWI.AUXBufferSize];
			if (m_Options.TWI.pAUXBuffer == NULL)
			{				
				m_StatusBar.SetPaneText(0, _T("TWI : Not Enough Memory !"));
				return;
			}			
			::SendMessage(m_TWIDlg.m_hEditAuxBuffer, WM_USER + 2, (WPARAM)(m_Options.TWI.pAUXBuffer), (LPARAM)m_Options.TWI.AUXBufferSize);
		
		}		
	}
	else if (m_Options.Mode == SPI)
	{
		m_Options.SPI.Mode = eSPIMode(((CComboBox*)FromHandle(::GetDlgItem(m_SPIDlg, IDC_COMBO_SPI_OPERATING_TYPE)))->GetCurSel());
		::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_DUMMY_BYTE, tmpBuffer, MAX_PATH);
		m_Options.SPI.WriteDummy = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		if (m_Options.SPI.Mode == SPI_RandomRead)
		{
			::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);
			m_Options.SPI.AUXBufferSize = static_cast<DWORD>(_tcstoul(tmpBuffer, NULL, 16));
			if (m_Options.SPI.pAUXBuffer != NULL)
				delete []m_Options.SPI.pAUXBuffer;
			m_Options.SPI.pAUXBuffer = new BYTE[m_Options.SPI.AUXBufferSize];
			if (m_Options.SPI.pAUXBuffer == NULL)
			{
				m_StatusBar.SetPaneText(0, _T("SPI : Not Enough Memory !"));
				return;
			}
			::SendMessage(m_SPIDlg.m_hEditBuffer, WM_USER + 2, (WPARAM)m_Options.SPI.pAUXBuffer, (LPARAM)m_Options.SPI.AUXBufferSize); // Get Data
		}
	}

	GetDlgItemText(IDC_EDIT_FILE_PATH_TO_SAVE, txFilePath, MAX_PATH);
	if (!RxFile.Open(txFilePath, CFile::modeWrite | CFile::shareDenyWrite | CFile::modeCreate, &ex))
	{
		TCHAR szError[1024];
		ex.GetErrorMessage(szError, 1024);
		m_StatusBar.SetPaneText(0, szError);
		return;
	}
	else
	{		
		GetDlgItemText(IDC_EDIT_STREAM_READ_SIZE, tmpBuffer, MAX_PATH);
		dwRxSize = _tcstoul(tmpBuffer, NULL, 16);
		if (dwRxSize != 0)
		{
			pRxBuffer = new BYTE[dwRxSize];

			pReadThread = AfxBeginThread(Thread_Read, &m_Options);
			pReadThread->m_bAutoDelete = TRUE;
		}
	}
}


LRESULT CDemoApplicationDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class
	switch (message)
	{
	case WM_USER_UPDATE_STATE:
		return 1;
		break;
	default:
		return CDialogEx::WindowProc(message, wParam, lParam);
	}
}

UINT Thread_Write(LPVOID pParam)
{
	DWORD dwResult;
	DWORD dwByteWritten;
	sOptions *pOptions;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };
	TCHAR RxFilePath[MAX_PATH + 1] = { 0 };
	CFileException ex;

	pOptions = (sOptions *)pParam;
	dwResult = StreamWrite(&gMWBridge, dwTxFileSize, pTxBuffer, &dwByteWritten, *pOptions);

	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("Stream Write OK, dwByteWritten = 0x%X"), dwByteWritten);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmpBuffer);
		if (pOptions->Mode == SPI)
		{
			if (pOptions->SPI.Mode == SPI_StandardWriteWithRead)
			{
				::GetDlgItemText(((CDemoApplicationDlg*)theApp.GetMainWnd())->m_SPIDlg, IDC_EDIT_AUX_BUFFER_PATH, RxFilePath, MAX_PATH);
				if (!RxFile.Open(RxFilePath, CFile::modeWrite | CFile::shareDenyWrite | CFile::modeCreate, &ex))
				{
					TCHAR szError[1024];
					ex.GetErrorMessage(szError, 1024);
					((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, szError);
					return -1;
				}
				else
				{
					RxFile.Write(pOptions->SPI.pAUXBuffer, pOptions->SPI.AUXBufferSize);
					RxFile.Close();
				}
			}
		}
	}
	else
	{
		CString tmp;		
		tmp.Format(_T("Stream Write Fail, Error Code = %08X"), dwResult);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmp);
	}
	delete[]pTxBuffer;
	pTxBuffer = NULL;
	AfxEndThread(0);
	return (0);
}

UINT Thread_Read(LPVOID lpParam)
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD NumbberOfByteRead;
	sOptions *pOptions;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	pOptions = (sOptions *)lpParam;
	dwResult = StreamRead(&gMWBridge, dwRxSize, pRxBuffer, &NumbberOfByteRead, *pOptions);
	if (NumbberOfByteRead != 0)
		RxFile.Write(pRxBuffer, NumbberOfByteRead);
	RxFile.Close();
	delete[]pRxBuffer;
	
	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("Stream Read OK, NumbberOfByteRead = 0x%X"), NumbberOfByteRead);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("Stream Read Fail, dwResult = 0x%X, NumbberOfByteRead = 0x%X"), dwResult, NumbberOfByteRead);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	pRxBuffer = NULL;	
	AfxEndThread(0);
	return (0);
}

void CDemoApplicationDlg::OnEnChangeEditReadTimeout()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	int ReadTimeOut = 0;
	DWORD dwResult = ERROR_SUCCESS;
	ReadTimeOut = GetDlgItemInt(IDC_EDIT_READ_TIMEOUT);
	if ((gMWBridge.Handle != NULL) && (gMWBridge.Handle != INVALID_HANDLE_VALUE))
	{
		dwResult = SetStreamReadTimeOut(&gMWBridge, ReadTimeOut);
		if (dwResult != ERROR_SUCCESS)
			m_StatusBar.SetPaneText(0, _T("Set Stream Read Timeout FAIL !"));
	}
}


void CDemoApplicationDlg::OnEnChangeEditWriteTimeout()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	int WriteTimeOut = 0;
	DWORD dwResult = ERROR_SUCCESS;
	WriteTimeOut = GetDlgItemInt(IDC_EDIT_WRITE_TIMEOUT);
	if ((gMWBridge.Handle != NULL) && (gMWBridge.Handle != INVALID_HANDLE_VALUE))
	{
		dwResult = SetStreamWriteTimeOut(&gMWBridge, WriteTimeOut);
		if (dwResult != ERROR_SUCCESS)
			m_StatusBar.SetPaneText(0, _T("Set Stream Write Timeout FAIL !"));
	}
}


void CDemoApplicationDlg::OnBnClickedButtonSelectInputFile21()
{
	SelectFilePath(IDC_EDIT_FILE_PATH_TO_SAVE2, FALSE);
}


void CDemoApplicationDlg::OnBnClickedButtonStreamRead2()
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD NumbberOfByteRead = 0;
	UINT ButtonState;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	TCHAR txFilePath[MAX_PATH + 1];
	CFileException ex;


	if (m_Options.Mode == UART)
	{
		ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_TARGET_ADDRESS_ACTIVE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.TADDRActive = TRUE;
		else
			m_Options.UART.TADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_UART_TARGET_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.TADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));

		ButtonState = ::IsDlgButtonChecked(m_UARTDlg.m_hWnd, IDC_CHECK_SLAVE_ADDRESS_ENABLE);
		if (ButtonState == BST_CHECKED)
			m_Options.UART.SADDRActive = TRUE;
		else
			m_Options.UART.SADDRActive = FALSE;

		::GetDlgItemText(m_UARTDlg, IDC_EDIT_SLAVE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.UART.SADDR = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
	}
	else if (m_Options.Mode == TWI)
	{
		m_Options.TWI.Mode = eTWIMode(((CComboBox*)FromHandle(::GetDlgItem(m_TWIDlg.m_hWnd, IDC_COMBO_TWI_OPERATION_TYPE)))->GetCurSel());
		::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_DEVICE_ADDRESS, tmpBuffer, MAX_PATH);
		m_Options.TWI.Address = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		::GetDlgItemText(m_TWIDlg, IDC_EDIT_TWI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);
		m_Options.TWI.AUXBufferSize = static_cast<DWORD>(_tcstoul(tmpBuffer, NULL, 16));
		m_Options.TWI.pAUXBuffer = new BYTE[m_Options.TWI.AUXBufferSize];
		if (m_Options.TWI.pAUXBuffer == NULL)
		{
			m_StatusBar.SetPaneText(0, _T("TWI : Not Enough Memory !"));
			return;
		}
	}
	else if (m_Options.Mode == SPI)
	{
		m_Options.SPI.Mode = eSPIMode(((CComboBox*)FromHandle(::GetDlgItem(m_SPIDlg, IDC_COMBO_SPI_OPERATING_TYPE)))->GetCurSel());
		::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_DUMMY_BYTE, tmpBuffer, MAX_PATH);
		m_Options.SPI.WriteDummy = static_cast<BYTE>(_tcstoul(tmpBuffer, NULL, 16));
		::GetDlgItemText(m_SPIDlg, IDC_EDIT_SPI_AUX_BUFFER_SIZE, tmpBuffer, MAX_PATH);
		m_Options.SPI.AUXBufferSize = static_cast<DWORD>(_tcstoul(tmpBuffer, NULL, 16));
		m_Options.SPI.pAUXBuffer = new BYTE[m_Options.SPI.AUXBufferSize];
		if (m_Options.SPI.pAUXBuffer == NULL)
		{
			m_StatusBar.SetPaneText(0, _T("SPI : Not Enough Memory !"));
			return;
		}
	}

	GetDlgItemText(IDC_EDIT_FILE_PATH_TO_SAVE2, txFilePath, MAX_PATH);
	if (!RxFile2.Open(txFilePath, CFile::modeWrite | CFile::shareDenyWrite | CFile::modeCreate, &ex))
	{
		TCHAR szError[1024];
		ex.GetErrorMessage(szError, 1024);
		m_StatusBar.SetPaneText(0, szError);
		return;
	}
	else
	{
		GetDlgItemText(IDC_EDIT_STREAM_READ_SIZE2, tmpBuffer, MAX_PATH);
		dwRxSize2 = _tcstoul(tmpBuffer, NULL, 16);
		if (dwRxSize2 != 0)
		{
			pRxBuffer2 = new BYTE[dwRxSize2];

			pReadThread2 = AfxBeginThread(Thread_Read2, &m_Options);
			pReadThread2->m_bAutoDelete = TRUE;
		}
	}
}


UINT Thread_Read2(LPVOID lpParam)
{
	DWORD dwResult = ERROR_SUCCESS;
	DWORD NumbberOfByteRead;
	sOptions *pOptions;
	TCHAR tmpBuffer[MAX_PATH + 1] = { 0 };

	pOptions = (sOptions *)lpParam;
	dwResult = StreamRead(&gMWBridge, dwRxSize2, pRxBuffer2, &NumbberOfByteRead, *pOptions);
	if (NumbberOfByteRead != 0)
		RxFile2.Write(pRxBuffer2, NumbberOfByteRead);
	RxFile2.Close();
	delete[]pRxBuffer2;

	if (dwResult == ERROR_SUCCESS)
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("Stream Read 2 OK, NumbberOfByteRead = 0x%X"), NumbberOfByteRead);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	else
	{
		_stprintf_s(tmpBuffer, MAX_PATH, _T("Stream Read 2 Fail, dwResult = 0x%X, NumbberOfByteRead = 0x%X"), dwResult, NumbberOfByteRead);
		((CDemoApplicationDlg*)theApp.GetMainWnd())->m_StatusBar.SetPaneText(0, tmpBuffer);
	}
	pRxBuffer2 = NULL;
	AfxEndThread(0);
	return (0);
}
