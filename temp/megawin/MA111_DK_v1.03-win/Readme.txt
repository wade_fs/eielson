Symptoms
Some floating-point math library functions in the Visual C++ 2013 x64 C Runtime (CRT) do not correctly check whether certain AVX and FMA3 instructions are available before the functions try to use them. If the instructions are not available for use, calls to these functions cause an illegal instruction exception (0xc000001d). Affected functions include log, log10, and pow, and others. 

This problem is most likely to occur in the following scenarios:
�DOn an old version of the Windows operating system that does not support AVX state saving (for example, Windows Vista) if it's used on a computer that supports the AVX instruction set
�DIf AVX state saving is explicitly disabled in the operating system boot configuration
�DIn the Windows Preinstall Environment (Windows PE) 

Note Windows PE does not support AVX state saving.




Cause
This problem occurs because the Visual C++ 2013 x64 C Runtime does not check whether the operating system supports AVX state saving before it tries to run AVX or FMA3 instructions.




Resolution
This update for Microsoft Visual C++ 2013 Update 5 Redistributable Package is released as a download-only update and isn��t distributed through Windows Update. Redistribution of this update is allowed, subject to the same license terms as granted by the Microsoft Visual Studio 2013 Update 5 release.




Microsoft Download Center
https://support.microsoft.com/en-us/help/3179560/update-for-visual-c-2013-and-visual-c-redistributable-package