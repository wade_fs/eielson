/********************************************************
*   Copyright (C) 2018 by Megawin Technology Co., Ltd.	*
*														*
*	Project		: MWBridge								* 
*	File format	: H file of the MWBridge.DLL			*
*	Author		: Megawin								*
*	Version		: V 1.0.1.0								*
********************************************************/

#ifndef __MEGAWIN_Bridge_DLL
#define __MEGAWIN_Bridge_DLL

#ifdef MWBridgeAPI
// All functions/variables are being exported     
#else
#define MWBridgeAPI extern "C" __declspec(dllimport)
#endif

//=====Error Code=====//
// If the function succeeds, the return value is zero.
// It means ERROR_SUCCESS.
#define CANNOT_FINDE_THE_DEVICE_SPECIFIED	0xE0000001L
#define INSUFFICIENT_MOMORY_AVAILABLE		0xE0000002L
#define GET_INPUT_REPORT_SIZE_FAIL			0xE0000003L
#define DELETE_INPUT_QUEUE_FAIL				0xE0000004L
#define SET_INPUT_REPORT_SIZE_FAIL			0xE0000005L
#define HIDD_GETATTRIBUTES_FAIL				0xE0000006L
#define DEVICE_NOT_CONNECT					0xE0000007L
#define INVALID_PARAMETER					0xE0000008L
#define DLLOBJECT_IS_CONNECT				0xE0000009L
#define OPEN_EXISTING_RETURNED				0xE000000AL
#define SET_CMD_FAIL						0xE000000BL
#define GET_CFG_FAIL						0xE000000CL
#define WRONG_MODE_SETTING					0xE000000DL
#define MODE_IS_IDLE						0xE000000EL
#define WRONG_OPTIONS_SETTING				0xE000000FL
#define NOT_FIND_ADDR_TO_READ				0xE0000010L
#define READ_DATA_TIMEOUT					0xE0000011L
#define WRITE_DATA_TIMEOUT					0xE0000012L
#define TWI_SCL_TIMEOUT						0xE0000013L
#define TWI_TX_TERMINATED					0xE0000014L
#define SET_CMD_BUS_FAIL					0xE0000015L
#define SET_CMD_TIMEOUT						0xE0000016L
//=====Type Define=====//
#define GPIO0		0
#define GPIO1		1
#define GPIO2		2
#define GPIO3		3
#define GPIO4		4
#define GPIO5		5
#define GPIO6		6
#define GPIO7		7
#define GPIO8		8
#define GPIO9		9
#define GPIOA		10
#define PWM0		0
#define PWM1		1
#define PWM2		2

struct sMWBridge{
    DWORD	VID;			        // Need to match user device's "Vendor ID".
    DWORD	PID;			        // Need to match user device's "Product ID".
    DWORD	ReadTimeOut;		    // Specifies the read data time-out interval, in milliseconds.
    DWORD	WriteTimeOut;		    // Specifies the write data time-out interval, in milliseconds.
	HANDLE	Handle;					// Do not modify this value, reserved for DLL
};

enum eStreamMode {
	Idle,
	UART,
	SPI,
	TWI,
};

enum eResetType {
	SoftwareResetToDFUandReconnectUSB,
	SoftwareResetAndReconnectUSB,
	OnlyReconnectUSB,
};

enum eDelayTime {
	Time_16ms,
	Time_32ms,
	Time_64ms,
	Time_128ms,
};
enum eResetTime {
	Time_0_5s,
	Time_1_0s,
	Time_1_5s,
	Time_2_0s,
	Time_2_5s,
	Time_3_0s,
	Time_3_5s,
	Time_4_0s,
};

enum eClear {
	ClearDLL,
	ClearBridge,
	ClearAll,
};

struct sUARTStatus {
	BOOL	RXOverRun;
	BOOL	RXDParityError;
	BOOL	RXDFramError;
	BOOL	RXDBreakDetection;
};


struct sGPIOStatus {
	BOOL	GPIOChange[8];
};

struct sStatus {
	sUARTStatus	UART;
	sGPIOStatus	GPIO;
};

enum eBaudRate {
	BaudRate_600,
	BaudRate_1200,
	BaudRate_2400,
	BaudRate_4800,
	BaudRate_9600,
	BaudRate_19200,
	BaudRate_38400,
	BaudRate_51200,
	BaudRate_57600,
	BaudRate_102400,
	BaudRate_115200,
	BaudRate_230400,
};

enum eBitOrder {
	LSBfirst,
	MSBfirst,
};

enum eBreakTiming {
	Timing_2ms,
	Timing_4ms,
	Timing_8ms,
	Timing_16ms,
	Timing_32ms,
	Timing_48ms,
	Timing_64ms,
	Timing_80ms,
};

enum eParitySelection {
	Even,
	Space,
	Odd,
	Mark,
};

enum eBOOL {
	Disable,
	Enable,
};

enum eStopBit {
	Bit_1,
	Bit_2,
};

enum eBreakControl {
	ClearBreak,
	SetBreak,
};

enum eActive {
	Low,
	High,
};

enum eModeSelection {
	Mode0,
	Mode1,
	Mode2,
	Mode3,
};

enum eClockRate {
	ClockRate_2MHz,
	ClockRate_6MHz,
};

enum eResetControl {
	NoAction,
	ResetTWIEngine,
};

enum eSpeedSelection {
	Speed_25K,
	Speed_50K,
	Speed_75K,
	Speed_100K,
};

enum eSCLTimeoutSelection {
	MoreThan_20ms,
	MoreThan_40ms,
	MoreThan_80ms,
	MoreThan_160ms,
	MoreThan_320ms,
	MoreThan_640ms,
	MoreThan_960ms,
	MoreThan_1280ms,
};

enum eUARTMode {
	UART_General,
	UART_Master,
	UART_Slave,
	UART_MasterAndSlave,
};

struct sUARTSetting {
	eBaudRate			BaudRate;
	eBitOrder			BitOrder;
	eBreakTiming		BreakTiming;
	eBOOL				RENControl;
	eBOOL				ParityCheck;
	eParitySelection	ParitySelection;
	eStopBit			StopBit;
	eBreakControl		BreakControl;
	eBOOL				RXBreak;
	eBOOL				RTSOutputFlowControl;
	eActive				RTSPolaritySelection;
	eBOOL				CTSInputFlowControl;
	eActive				CTSPolaritySelection;
	eBOOL				DEOutputControl;
	eActive				DEPolaritySelection;
	eUARTMode			Mode;
	BYTE				SADDR1;
	BYTE				SADDR2;
};

struct sSPISetting {
	eModeSelection		ModeSelection;
	eBitOrder			BitOrder;
	eBOOL				nSSOutputControl;
	eClockRate			ClockRate;
};

struct sTWISetting {
	eResetControl			ResetControl;
	eBOOL					SCLTimeoutDetectionControl;
	eSpeedSelection			SpeedSelection;
	eSCLTimeoutSelection	SCLTimeoutSelection;
};

struct sStreamSetting {
	eStreamMode		Mode;
	sUARTSetting	UART;
	sSPISetting		SPI;
	sTWISetting		TWI;
};

struct sUARTOptions {
	BOOL	TADDRActive;
	BYTE	TADDR;
	BOOL	SADDRActive;
	BYTE	SADDR;
};

enum eSPIMode {
	SPI_Idle,
	SPI_SequentialWrite,
	SPI_SequentialRead,
	SPI_RandomRead,
	SPI_StandardWriteWithRead,
};
struct sSPIOptions {
	eSPIMode	Mode;
	BYTE		WriteDummy;
	PBYTE		pAUXBuffer;
	DWORD		AUXBufferSize;
};

enum eTWIMode {
	TWI_Idle,
	TWI_SequentialWrite,
	TWI_SequentialRead,
	TWI_RandomRead,
};

struct sTWIOptions {
	eTWIMode	Mode;
	BYTE		Address;
	PBYTE		pAUXBuffer;
	DWORD		AUXBufferSize;
};

struct sOptions {
	eStreamMode		Mode;
	sUARTOptions	UART;
	sSPIOptions		SPI;
	sTWIOptions		TWI;
};

enum eICKOConfigure {
	OutputDisabled,
	Output_24MHz,
	Output_12MHz,
	Output_6MHz,
};

struct sGPIOAlternateSetting {
	eBOOL			USBRemoteWakeup;
	eBOOL			USBResetEventOutput;
	eBOOL			USBSuspendEventOutput;
	eActive			USBSuspendPolarity;
	eICKOConfigure	ICKOConfigured;
	eBOOL			PWM0Output;
	eBOOL			PWM1Output;
	eBOOL			PWM2Output;
	eBOOL			WKP0Wakeup;
	eActive			WKP0ActiveLevel;
	eBOOL			WKP1Wakeup;
	eActive			WKP1ActiveLevel;
};

enum eMode {
	ModeDisable,
	OpenDrainOutput,
	OpenDrainOutputWithPullUpResistor,
	PushPullOutput,
};
struct sModeSetting {
	eBOOL	Setting;
	eMode	Mode;
};

struct sGPIOModeSetting {
	sModeSetting	GPIO[11];
};

enum eData {
	OutputLow,
	OutputHigh,
};
struct sDataSetting {
	eBOOL	Setting;
	eData	Data;
};

struct sGPIODataSetting {
	sDataSetting	GPIO[11];
};

struct sGPIOInterruptSetting {
	eBOOL	GPIO[8];
};

enum ePulseSetting {
	None,
	OutputPulse,
};

struct sGPIOPulseSetting {
	ePulseSetting	GPIO[11];
};

enum ePinStatus {
	InputLow,
	InputHigh,
};

struct sGPIOPinStatus {
	ePinStatus	GPIO[11];
};

struct sPWMDuty {
	eBOOL	Setting;
	BYTE	Value;
};

struct sPWMDutySetting {
	sPWMDuty	PWM[3];
};

//=====Function Define=====//
MWBridgeAPI DWORD __stdcall
ConnectBridge(sMWBridge* pMWBridge, UINT Index = 1, DWORD BufferSize = 1024 * 64);

MWBridgeAPI DWORD __stdcall
DisconnectBridge(sMWBridge* pMWBridge);

MWBridgeAPI DWORD __stdcall
SetStreamReadTimeOut(sMWBridge* pMWBridge, DWORD Times);

MWBridgeAPI DWORD __stdcall
SetStreamWriteTimeOut(sMWBridge* pMWBridge, DWORD Times);

MWBridgeAPI DWORD __stdcall
ResetBridge(sMWBridge* pMWBridge, eDelayTime DealyTime = Time_16ms, eResetTime ResetTime = Time_0_5s);

MWBridgeAPI DWORD __stdcall
ClearBuffer(sMWBridge* pMWBridge, eClear ClearType);

MWBridgeAPI DWORD __stdcall
GetDLLVersion(DWORD* pDLLVersion);

MWBridgeAPI DWORD __stdcall
GetStatus(sMWBridge* pMWBridge, sStatus* pStatus);

MWBridgeAPI DWORD __stdcall
SetStreamMode(sMWBridge* pMWBridge, sStreamSetting StreamSetting);

MWBridgeAPI DWORD __stdcall
GetStreamMode(sMWBridge* pMWBridge, sStreamSetting* pStreamSetting);

MWBridgeAPI DWORD __stdcall
StreamWrite(sMWBridge* pMWBridge, DWORD NumberOfBytesToWrite, BYTE* pBuffer, DWORD* pNumberOfBytewsWritten, sOptions Options);

MWBridgeAPI DWORD __stdcall
StreamRead(sMWBridge* pMWBridge, DWORD NumberOfBytesToRead, BYTE* pBuffer, DWORD* pNumberOfBytesRead, sOptions Options);

MWBridgeAPI DWORD __stdcall
SetGPIOAlternateFunction(sMWBridge* pMWBridge, sGPIOAlternateSetting GPIOAlternateSetting);

MWBridgeAPI DWORD __stdcall
GetGPIOAlternateFunction(sMWBridge* pMWBridge, sGPIOAlternateSetting* pGPIOAlternateSetting);

MWBridgeAPI DWORD __stdcall
SetGPIOMode(sMWBridge* pMWBridge, sGPIOModeSetting ModeSetting);

MWBridgeAPI DWORD __stdcall
GetGPIOMode(sMWBridge* pMWBridge, sGPIOModeSetting* pModeSetting);

MWBridgeAPI DWORD __stdcall
SetGPIOData(sMWBridge* pMWBridge, sGPIODataSetting DataSetting);

MWBridgeAPI DWORD __stdcall
GetGPIOData(sMWBridge* pMWBridge, sGPIODataSetting* pDataSetting);

MWBridgeAPI DWORD __stdcall
SetGPIOInterrupt(sMWBridge* pMWBridge, sGPIOInterruptSetting InterruptSetting);

MWBridgeAPI DWORD __stdcall
GetGPIOInterrupt(sMWBridge* pMWBridge, sGPIOInterruptSetting* pInterruptSetting);

MWBridgeAPI DWORD __stdcall
SetGPIOPulse(sMWBridge* pMWBridge, sGPIOPulseSetting PulseSetting);

MWBridgeAPI DWORD __stdcall
GetGPIOPinStatus(sMWBridge* pMWBridge, sGPIOPinStatus* pPinStatus);

MWBridgeAPI DWORD __stdcall
SetPWMDutyValue(sMWBridge* pMWBridge, sPWMDutySetting PWMDutySetting);

MWBridgeAPI DWORD __stdcall
GetPWMDutyValue(sMWBridge* pMWBridge, sPWMDutySetting* pPWMDutySetting);

#endif