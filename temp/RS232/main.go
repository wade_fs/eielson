package main

import (
	"flag"
	"fmt"
	"github.com/goburrow/serial"
	"log"
	"os"
	"runtime"
	"strconv"
	"time"
)

var (
	Help bool
	Port string
	Baudrate string
)

func init() {
	flag.BoolVar(&Help, "h", false, "Usage")
	if runtime.GOOS == "windows" {
		flag.StringVar(&Port, "p", "COM1", "COM Port")
	} else {
		flag.StringVar(&Port, "p", "/dev/ttyUSB0", "COM Port")
	}
	flag.StringVar(&Baudrate, "b", "9600", "Baudrate")
	flag.Usage = usage
}

func usage() {
	fmt.Fprintf(os.Stderr, `Usage: %s [-h] -p COM-PORT [-b 9600]
Options:
`, os.Args[0])
    flag.PrintDefaults()
}

// HEAD(FF), MSB, LSB, SUM
func getTemp(data []byte, n int) (float32, error) {
	if n <= 5 {
		return 0, fmt.Errorf("Wrong data byte number")
	}
	i := 0
	for ; i<n && data[i] != 0xFF; i++ {
	}
	
	if i>=n || data[i] != 0xFF {
		return 0, fmt.Errorf("Wrong data format")
	}
	i++ // skip HEAD(0xFF)
	sign := 1
	if data[i] > 0 {
		sign = -1
	}
	sum := data[i] + data[i+1] + data[i+2]
	if sum != data[i+3] {
		return 0, fmt.Errorf("Checksum error", sum, "!=", data[i+3])
	}
	i++ // skip sign

	var (
		msb, lsb, temp_val uint32
		temp float32
	)
	msb = uint32(data[i])
	lsb = uint32(data[i+1])
	temp_val = msb << 8 + lsb
	if sign == -1 {
		temp_val = ^temp_val
		temp_val = temp_val & 0xFFFF
		temp_val++
    	temp = float32(-3.90625) * float32(temp_val) / float32(1000.0)
	} else {
    	temp = float32(3.90625) * float32(temp_val) / float32(1000.0)
	}
	return temp, nil
}

func main() {
	flag.Parse()
	if Help {
		flag.Usage()
		return
	}

	//開啟SerialPort
	br, err := strconv.Atoi(Baudrate)
	if err != nil {
		log.Fatal("Wrong baudrate")
	}
	port, err := serial.Open(
		&serial.Config{
			Address:  Port,
			BaudRate: br,
			DataBits: 8,
			StopBits: 1,
			Parity:   "N",
			Timeout: 3 * time.Second, //Timeout時間決定port.read()的等待時間上限
		})
	if err != nil {
		log.Fatal("Comport open fail")
	}
	defer port.Close() //程式結束時關閉SerialPort

	for {
		data := make([]byte, 1024)
		time.Sleep(1000 * time.Millisecond) //等待回傳所需的時間1000ms
		n, err := port.Read(data)           //讀資料回來
		if err != nil {
			log.Fatal(err)
		}
		if temp, err := getTemp(data[:n], n); err != nil {
			log.Fatal(err)
		} else {
			fmt.Println("Temp:", temp)
		}
	}
}
