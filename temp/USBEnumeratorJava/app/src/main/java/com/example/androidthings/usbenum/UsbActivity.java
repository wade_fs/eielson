/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.androidthings.usbenum;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class UsbActivity extends Activity {
    private static final String TAG = "UsbEnumerator";
    private static final int INTERFACE_CLASS_HID = 3;
    /* USB system service */
    private static final int VID = 0x0E6A;
    private static final int PID = 0x0124;
    private UsbManager mUsbManager;
    private UsbDevice mUsbDevice;
    private UsbInterface mUsbInterface;
    private UsbDeviceConnection mConnection;
    private UsbEndpoint mInUsbEndpoint;
    private UsbEndpoint mOutUsbEndpoint;

    /* UI elements */
    private TextView mStatusView, mResultView;
    protected static final int STD_USB_REQUEST_GET_DESCRIPTOR = 0x06;
    protected static final int LIBUSB_DT_STRING = 0x03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb);

        mStatusView = (TextView) findViewById(R.id.text_status);
        mResultView = (TextView) findViewById(R.id.text_result);

        mUsbManager = getSystemService(UsbManager.class);

        // Detach events are sent as a system-wide broadcast
        IntentFilter filter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        handleIntent(getIntent());
    }

    private static byte[] toBytes(int... ints) { // helper function
        byte[] result = new byte[ints.length];
        for (int i = 0; i < ints.length; i++) {
            result[i] = (byte) ints[i];
        }
        return result;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbReceiver);
    }

    /**
     * Broadcast receiver to handle USB disconnect events.
     */
    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("MyLog", "BroadcastReceiver() "+action);
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    printStatus(getString(R.string.status_removed));
                    printDeviceDescription(device);
                }
            }
        }
    };

    private void connectDevice() {
        if (mUsbDevice.getInterfaceCount() != 2) {
            printStatus("裝置不正確1");
            return;
        }
        mUsbInterface = mUsbDevice.getInterface(0);
        if (mUsbInterface.getInterfaceClass() != INTERFACE_CLASS_HID) {
            printStatus("裝置不正確2");
            return;
        }
        if (mUsbInterface.getEndpointCount() != 2) {
            printStatus("裝置不正確3");
            return;
        }
        mConnection = mUsbManager.openDevice(mUsbDevice);
        mOutUsbEndpoint = mUsbInterface.getEndpoint(0);
        mInUsbEndpoint = mUsbInterface.getEndpoint(1);
        byte[][] cmds;
        cmds = new byte[5][];
        cmds[0] = toBytes(0x05, 0x82, 0x08, 0x04, 0x03, 0x07); // SetGPIOMode
        cmds[1] = toBytes(0x03, 0x83, 0x08, 0x03, 0x07);       // SetGPIOData
        cmds[2] = toBytes(0x03, 0x94, 0x00, 0x03);             // SetStreamMode
        cmds[3] = toBytes(0x05, 0x87, 0x02, 0x46, 0x02, 0x00); // StreamRead
        cmds[4] = toBytes(0x03, 0x83, 0x08, 0x07, 0x07);       // SetGPIOData2
//        while (true) {
            for (int i=0; i<5; i++) {
                write(cmds[i]);
            }
            printResult("命令已傳送，讀取資料中");
            byte[] readBuffer = read(8);
            if (readBuffer.length > 0) {
                printResult(new String(readBuffer));
            }
//        }
    }

    /**
     * Determine whether to list all devices or query a specific device from
     * the provided intent.
     * @param intent Intent to query.
     */
    private void handleIntent(Intent intent) {
        mUsbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        // 手動調用 APK, mUsbDevice == null，只有插入事件才不會 null
        if (mUsbDevice != null &&
                mUsbDevice.getVendorId() == VID &&
                mUsbDevice.getProductId() == PID) {
            connectDevice();
        } else {
            printDeviceList();
        }
    }

    public void close() {
        if (mConnection != null) {
            mConnection.close();
        }
    }

    public void write(byte[] data) {
        write(data, 0, data.length);
    }

    public void write(byte[] data, int size) {
        write(data, 0, size);
    }

    public void write(byte[] data, int offset, int size) {
        Log.d("MyLog", "Write BEGIN"+new String(data));
        if (offset != 0) {
            data = Arrays.copyOfRange(data, offset, size);
        }
        if (mOutUsbEndpoint == null) {
            printStatus("mOutUsbEndpoint is null");
        } else {
            mConnection.bulkTransfer(mOutUsbEndpoint, data, size, 1000);
        }
        Log.d("MyLog", "Write END");
    }

    public byte[] read(int size) {
        return read(size, -1);
    }

    public byte[] read(int size, int timeout) {
        byte[] buffer = new byte[size];
        int bytesRead = mConnection.bulkTransfer(mInUsbEndpoint, buffer, size, timeout);
        if (bytesRead < size) {
            buffer = Arrays.copyOfRange(buffer, 0, bytesRead);
        }
        return buffer;
    }

    /**
     * Print the list of currently visible USB devices.
     */
    private void printDeviceList() {
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();

        if (deviceList.isEmpty()) {
            printResult("未連接任何體溫感測器");
        } else {
            for (UsbDevice device : deviceList.values()) {
                if (device.getVendorId() == VID && device.getProductId() == PID) {
                    printDeviceDetails(device);
                    return;
                }
            }
            printResult("沒找到正確的體溫感測器");
        }
    }

    /**
     * Print a basic description about a specific USB device.
     * @param device USB device to query.
     */
    private void printDeviceDescription(UsbDevice device) {
        String result = UsbHelper.readDevice(device) + "\n\n";
        printResult(result);
    }

    /**
     * Initiate a control transfer to request the device information
     * from its descriptors.
     *
     * @param device USB device to query.
     */
    private void printDeviceDetails(UsbDevice device) {
        UsbDeviceConnection connection = mUsbManager.openDevice(device);

        String deviceString = "";
        try {
            //Parse the raw device descriptor
            deviceString = DeviceDescriptor.fromDeviceConnection(connection)
                    .toString();
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Invalid device descriptor", e);
        }

        String configString = "";
        try {
            //Parse the raw configuration descriptor
            configString = ConfigurationDescriptor.fromDeviceConnection(connection)
                    .toString();
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Invalid config descriptor", e);
        } catch (ParseException e) {
            Log.w(TAG, "Unable to parse config descriptor", e);
        }


		String manufacturer = "", product = "", serial = "";

		byte[] rawDescs = connection.getRawDescriptors();
    	try {
    	    byte[] buffer = new byte[255];
    	    int idxMan = rawDescs[14];
    	    int idxPrd = rawDescs[15];

    	    int rdo = connection.controlTransfer(UsbConstants.USB_DIR_IN | UsbConstants.USB_TYPE_STANDARD,
    	            STD_USB_REQUEST_GET_DESCRIPTOR, (LIBUSB_DT_STRING << 8) | idxMan, 0x0409, buffer, 0xFF, 0);
    	    manufacturer = new String(buffer, 2, rdo - 2, "UTF-16LE");

	        rdo = connection.controlTransfer(UsbConstants.USB_DIR_IN | UsbConstants.USB_TYPE_STANDARD,
	                STD_USB_REQUEST_GET_DESCRIPTOR, (LIBUSB_DT_STRING << 8) | idxPrd, 0x0409, buffer, 0xFF, 0);
	        product = new String(buffer, 2, rdo - 2, "UTF-16LE");

	        serial = connection.getSerial();
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }

        printResult(deviceString + "\n\n" + configString+"\n\n"+manufacturer+":"+product+":"+serial);
        connection.close();
    }

    /* Helpers to display user content */

    private void printStatus(String status) {
        mStatusView.setText(status);
        Log.i(TAG, status);
    }

    private void printResult(String result) {
        mResultView.setText(result);
        Log.i(TAG, result);
    }
}
