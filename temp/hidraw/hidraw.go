package main

import(
	"fmt"
	"github.com/GeertJohan/go.hid"
)

func main() {
	// open the MSI GT leds device 0e6a:0124
	// 可以用 hid.OpenPath("/dev/input....")
	hidraw, err := hid.Open(0x0E6A, 0x0124, "")
	if err != nil {
		fmt.Printf("Could not open hidraw device: %s\n", err)
		return
	}
	defer hidraw.Close()

	data := make([]byte, 16)
	cmds := [][]byte{
		{ 0x05, 0x82, 0x08, 0x04, 0x03, 0x07 },
        { 0x03, 0x83, 0x08, 0x03, 0x07 },
        { 0x03, 0x94, 0x00, 0x03 },
        { 0x05, 0x87, 0x02, 0x46, 0x02, 0x00 },
        { 0x03, 0x83, 0x08, 0x07, 0x07}}

	for i:=0; i<4; i++ {
		hidraw.Write(cmds[i])
		res, err := hidraw.ReadTimeout(data, 1000)
		if err != nil {
			fmt.Printf("Could not read. err = %s\n", err)
			return
		}
		fmt.Println("Read", i, res, "=", string(data))
	}
	res, err := hidraw.ReadTimeout(data, 1000)
	if err != nil {
		fmt.Printf("Could not read. err = %s\n", err)
		return
	}
	fmt.Println("Read", res, "=", string(data))
}
