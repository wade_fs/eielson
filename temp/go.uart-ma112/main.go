package main

import (
	"encoding/hex"
	"fmt"
	"os"
	"os/signal"

	"syscall"

	"log"
)


func main() {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	var buf = make([]byte, 8)
	var temp = make([]byte, 5)
	var idx = 0

	go func() {
		for {
			tty, err := os.OpenFile("/dev/ttyACM0", os.O_RDWR|syscall.O_NOCTTY, 0)
			if err != nil {
				log.Fatalf("Cannot open tty port: %v\n", err)
			}
			defer tty.Close()

			select {
			case <-done:
				log.Println("Quit!")
				return
			default:
				nr, _ := tty.Read(buf)
				tty.Close()
				res := make([]byte, nr)
				copy(res[0:nr], buf[0:nr])
				fmt.Printf("[%d] %s\n", nr, hex.EncodeToString(res))
				if nr > 1 {
					continue
				}
				if buf[0] == 0xff {
					idx = 0
					break
				}
				temp[idx] = buf[0]
				idx++
				if idx == 5 {
					var sum byte = 0
					for i:=1; i<4; i++ {
						sum = sum + temp[i]
					}

					dst := make([]byte, hex.EncodedLen(5))
					if sum == temp[4] {
						hex.Encode(dst, temp[:5])
						log.Printf("Write:%s\n", dst)
					} else {
						log.Printf("Wrong:%s\n", dst)
					}
					idx = 0
				}
			}
		}
	}()

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println("SIG:", sig)
		close(done)
	}()

	fmt.Println("Ctrl+C to quit")
	<-done
	fmt.Println("exiting")
}

