#ifndef PL2303GLIB_H
#define PL2303GLIB_H

#ifdef __cplusplus
extern "C" {
#endif
#include <wchar.h>

typedef void*                   HANDLE;
typedef unsigned char           uint8_t;
typedef unsigned short          uint16_t;
typedef unsigned int            uint32_t;
//typedef unsigned long long      uint64_t;

typedef signed char             int8_t;
typedef short                   int16_t;
typedef int                     int32_t;
//typedef long long               int64_t;

#define PROLIFIC_VID                0x067B
#define ERROR_SUCCESS               0
#define ERROR_BUFFER_TOO_SMALL      -1
#define ERROR_READ_REGISTER_FAIL    -2
#define ERROR_WRITE_REGISTER_FAIL   -3
#define ERROR_READ_DATA_FAIL        -4
#define ERROR_READ_TIMEOUT          -5
#define ERROR_WRITE_DATA_FAIL       -6
#define ERROR_WRITE_TIMEOUT         -7
#define ERROR_DEVICE_NOT_EXIST      -8
#define ERROR_NOT_GPIO_PIN          -9
#define ERROR_DEVICE_OPEN_FAIL      -10
#define ERROR_DATA_LENGTH_TOO_LARGE -11
#define ERROR_OTHER_FAIL            -12
#define ERROR_I2C_BUS_BUSY          -13
#define ERROR_I2C_ADDRESS_NACK      -14
#define ERROR_I2C_DATA_NACK         -15
#define ERROR_I2C_PROCESSING        -16

#define BIT0       0x01
#define BIT1       0x02
#define BIT2       0x04
#define BIT3       0x08
#define BIT4       0x10
#define BIT5       0x20
#define BIT6       0x40
#define BIT7       0x80

#define NON_BLOCKING 0
//I2C
enum I2C_TRANSFERT_TYPE{
    I2C_NO_START_NO_STOP = 0,
    I2C_START_NO_STOP = 1,
    I2C_NO_START_STOP = 2,
    I2C_START_STOP = 3,
};

enum SPI_MODE {
    SPI_MODE0 = 0x00,	//Mode 0
    SPI_MODE1 = 0x10,	//Mode 1
    SPI_MODE2 = 0x20,	//Mode 2
    SPI_MODE3 = 0x30,	//Mode 3
};

//SPI
enum SPI_CS_CONTROL{
    SPI_CS_NO_TOGGLE = 0,
    SPI_CS_LOW_BEFORE_DATA,
    SPI_CS_HIGH_AFTER_DATA,
    SPI_CS_LOW_BEFORE_AND_HIGH_AFTER_DATA,
};

enum SPI_SELECT{
    SPI_SELECT0 = 0x00,
    SPI_SELECT1 = 0x10,
};
//UART
//UART MODE
enum UART_FLOW_CONTROL {
    UART_RTS_CTS_DTR_DSR_FLOW_CONTROL = 0x10,   //1 00
    UART_DTR_DSR_FLOW_CONTROL = 0x14,           //1 01
    UART_RTS_CTS_FLOW_CONTROL = 0x18,           //1 10
    UART_SW_FLOW_CONTROL = 0x0C,                //0 11
    UART_DISABLE_FLOW_CONTROL = 0x1C,           //1 11
};

//STOP BIT
enum UART_STOP_BIT {
    UART_ONE_STOP_BIT = 0,
    UART_ONE_POINT_FIVE_STOP_BIT = 1,
    UART_TWO_STOP_BIT = 2,
};

//PARITY
enum UART_PARITY_TYPE {
    UART_PARITY_NONE = 0,
    UART_PARITY_ODD = 1,
    UART_PARITY_EVEN = 2,
    UART_PARITY_MARK = 3,
    UART_PARITY_SPACE = 4,
};

//DATA BIT
enum UART_DATA_BIT {
    UART_DATA_BIT_5 = 5,
    UART_DATA_BIT_6 = 6,
    UART_DATA_BIT_7 = 7,
    UART_DATA_BIT_8 = 8,
};

//GPIO
enum GPIO_PIN {
    GPA0 = (0x0000 + BIT0),
    GPA1 = (0x0000 + BIT1),
    GPA2 = (0x0000 + BIT2),
    GPA3 = (0x0000 + BIT3),
    GPA4 = (0x0000 + BIT4),
    GPA5 = (0x0000 + BIT5),
    GPA6 = (0x0000 + BIT6),
    GPA7 = (0x0000 + BIT7),
    GPB0 = (0x0100 + BIT0),
    GPB1 = (0x0100 + BIT1),
    GPB2 = (0x0100 + BIT2),
    GPB3 = (0x0100 + BIT3),
    GPB4 = (0x0100 + BIT4),
    GPB5 = (0x0100 + BIT5),
    GPB6 = (0x0100 + BIT6),
    GPB7 = (0x0100 + BIT7)
};

enum GPIO_DIR {
    GPIO_INPUT_DIR = 0,
    GPIO_OUTPUT_DIR = 1,
};

enum GPIO_VALUE {
    GPIO_LOW = 0,
    GPIO_HIGH = 1,
};


//SDK Version
void GetSDKVersion(uint32_t *SDKVersion);

//Device API Start
int32_t InitDevice(void);
int32_t ExitDevice(void);
int32_t EnumHidDeviceArray(uint32_t *HidDeviceCount);
int32_t EnumDeviceByVid(uint32_t* HidDeviceCount, uint16_t VID);
int32_t EnumDeviceByVidPid(uint32_t* HidDeviceCount, uint16_t VID, uint16_t PID);
int32_t GetVidPidByIndex(uint32_t DeviceIndex, uint16_t *VID, uint16_t *PID);
int32_t GetVidPidByHandle(HANDLE hDeviceHandle, uint16_t *VID, uint16_t *PID);
int32_t GetVidPidSerialNumberByIndex(uint32_t DeviceIndex, uint16_t *VID, uint16_t *PID, wchar_t* SerialNumber, uint16_t length);
int32_t OpenDeviceHandle(uint32_t DeviceIndex, HANDLE *hDeviceHandle);
int32_t CloseDeviceHandle(HANDLE hDeviceHandle);
//Device API End

//UART API Start
int32_t UartReset(HANDLE hDeviceHandle);
int32_t SetXonXoffSymbol(HANDLE hDeviceHandle, uint8_t Xon, uint8_t Xoff);
int32_t SetUartConfig(HANDLE hDeviceHandle, uint32_t BaudRate, enum UART_STOP_BIT StopBIt, enum UART_PARITY_TYPE ParityType, enum UART_DATA_BIT DataBit, enum UART_FLOW_CONTROL FlowControl);
int32_t GetUartConfig(HANDLE hDeviceHandle, uint32_t *BaudRate, uint8_t *StopBIt, uint8_t *ParityType, uint8_t *DataBit, uint8_t *FlowControl);
int32_t WriteUart(HANDLE hDeviceHandle, uint8_t *Buffer, uint16_t NumberOfBytesToWrite, uint16_t *NumberOfBytesWritten);
int32_t ReadUart(HANDLE hDeviceHandle, uint8_t *Buffer, uint16_t NumberOfBytesToRead, uint16_t *NumberOfBytesRead, uint32_t TimeOutms);
//UART API End

//I2C API Start
int32_t SetI2CFrequency(HANDLE hDeviceHandle, uint8_t FreqDiv);
int32_t I2CReset(HANDLE hDeviceHandle);
int32_t SetI2CDeviceAddress(HANDLE hDeviceHandle, uint8_t DeviceAddress);
int32_t I2CRead(HANDLE hDeviceHandle, uint8_t *Buffer, uint16_t NumberOfBytesToRead, uint16_t *NumberOfBytesRead, uint32_t TimeOutms);
int32_t I2CWrite(HANDLE hDeviceHandle, uint8_t *Buffer, uint16_t NumberOfBytesToWrite, uint16_t *NumberOfBytesWritten, uint32_t TimeOutms);
int32_t I2CWriteRead(HANDLE hDeviceHandle, uint8_t *WriteBuffer, uint16_t NumberOfBytesToWrite, uint8_t *ReadBuffer, uint16_t NumberOfBytesToRead, uint16_t *NumberOfBytesRead, uint32_t TimeOutms);
//I2C API End

//SPI API Start
int32_t SetSPIFrequency(HANDLE hDeviceHandle, uint8_t FreqDiv, enum SPI_MODE spiMode);
int32_t SPIRead(HANDLE hDeviceHandle, enum SPI_SELECT SelectSPI, uint8_t *Buffer, uint16_t NumberOfBytesToRead, uint16_t *NumberOfBytesRead, uint32_t TimeOutms);
int32_t SPIWrite(HANDLE hDeviceHandle, enum SPI_SELECT SelectSPI, uint8_t *Buffer, uint16_t NumberOfBytesToWrite, uint16_t *NumberOfBytesWritten, uint32_t TimeOutms);
int32_t SPIWriteRead(HANDLE hDeviceHandle, enum SPI_SELECT SelectSPI, uint8_t *WriteBuffer, uint16_t NumberOfBytesToWrite, uint8_t *ReadBuffer, uint16_t NumberOfBytesToRead, uint16_t *NumberOfBytesRead, uint32_t TimeOutms);
int32_t SPIReset(HANDLE hDeviceHandle);
//SPI API End

//GPIO API Start
int32_t SetGPIODir(HANDLE hDeviceHandle, enum GPIO_PIN GPIOPin, enum GPIO_DIR GPIODir);
int32_t SetGPIO(HANDLE hDeviceHandle, enum GPIO_PIN GPIOPin, enum GPIO_VALUE Value);
int32_t GetGPIO(HANDLE hDeviceHandle, enum GPIO_PIN GPIOPin, uint8_t *Value);
//GPIO API End

#ifdef __cplusplus
}
#endif

#endif // PL2303GLIB_H
