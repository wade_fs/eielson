#include <stdio.h>
#include "HidDeviceSdkApi.h"
//
//  main.m
//  HidSPIample
//
//  Copyright © 2018年 Prolific Technology Inc. All rights reserved.
//

/******************************************************************************
This is a sample code for demonstrating how to use the SDK
Target : Read a register value
******************************************************************************/

int main() {
    HANDLE handle;
    int32_t retStatus = ERROR_SUCCESS;
    uint32_t devNum = 0;
    uint32_t timeout = 1000;
    uint16_t readBackLength = 0;
    
    //Initial API 
    retStatus = InitDevice();

    //Enumerate device by vid
    //devNum is a number that how many device's vid is 0x067B
    retStatus = EnumDeviceByVidPid(&devNum, 0x067b, 0x2362);
    if (retStatus < ERROR_SUCCESS) {
        printf("Not found device, status:%d\n", retStatus);
        return 0;
    }
    
    //Open device
    retStatus = OpenDeviceHandle(0, &handle);
    if (retStatus < ERROR_SUCCESS) {
        printf("Open device failed, status:%d\n", retStatus);
        return 0;
    }
    
    //nSPIFreqDiv = 20, hence, SPI Frequency(KHz) = 24000/20 = 1,200 KHz
    uint8_t nSPIFreqDiv = 20;	//
    retStatus = SetSPIFrequency(handle, nSPIFreqDiv, SPI_MODE3);
    if (retStatus < ERROR_SUCCESS) {
        printf("SPI set frequency failed, status:%d\n", retStatus);
        return 0;
    }
    
    //Send Read Identification(RDID) 0x9F Command
    //Write register first, and then, read value
    uint8_t writeBuffer[1] = {0x00};
    uint8_t readBuffer[3] = {0x00};
    uint8_t nSpiCommand = 0x9f;
    writeBuffer[0] = nSpiCommand;
    retStatus = SPIWriteRead(handle, SPI_SELECT0, writeBuffer, 1, readBuffer, 3, &readBackLength, timeout);
    if (retStatus < ERROR_SUCCESS) {
        printf("SPI Read Identification(RDID) Command Fail!\n");
        return 0;
    }else{
        printf("ManufactureId=0x%x, MemoryType=0x%x, MemoryDenisty=0x%x\n", readBuffer[0], readBuffer[1], readBuffer[2]);
    }
    
    //Close Device
    retStatus = CloseDeviceHandle(handle);
    
    //Clean infomation the InitDevice create
    ExitDevice();
    return 0;
}
