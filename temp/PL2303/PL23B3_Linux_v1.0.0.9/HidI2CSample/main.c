#include <errno.h>
#include <time.h>

#include <stdio.h>
#include "HidDeviceSdkApi.h"
#include <sys/wait.h>
#include <unistd.h>
//
//  main.m
//  HidI2CSample
//
//  Copyright © 2018年 Prolific Technology Inc. All rights reserved.
//

int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

/******************************************************************************
This is a sample code for demonstrating how to use the SDK with a two-byte address defined EEPROM 24C0512
Target : A loopback for writing 10 bytes data from the address 0x0000 to 0x0009
******************************************************************************/

/*****************************************************************************
If the SDK is used to set register
for example:
    Register: 0x0A
    Command: 0x55
    
    writebuffer[2] = {0x0A, 0x55};
    I2CWrite(handle, writeBuffer, 2, &wirttenLength, 1000);
******************************************************************************/

int main() {
    HANDLE handle;
    int32_t retStatus = ERROR_SUCCESS;
    uint32_t devNum = 0;
    uint16_t wirttenLength = 0;
    uint16_t readBackLength = 0;

    //Initial API    
    retStatus = InitDevice();
	
    //Enumerate device by vid
    //devNum is a number that how many device's vid is 0x067B
    retStatus = EnumDeviceByVid(&devNum, 0x067b);
    if (retStatus < ERROR_SUCCESS) {
        printf("Not found device, status:%d\n", retStatus);
        return 0;
    }

    //Open device
    retStatus = OpenDeviceHandle(0, &handle);
    if (retStatus < ERROR_SUCCESS) {
        printf("Open device failed, status:%d\n", retStatus);
        return 0;
    }
    
    //currentFreqDiv = 100, hence, I2C Frequency(KHz) = 24000/100 KHz= 240 KHz
    uint8_t currentFreqDiv = 100;
    retStatus = SetI2CFrequency(handle, currentFreqDiv);
    if (retStatus < 0) {
        printf("Set I2C frequency failed, status:%d\n", retStatus);
        retStatus = CloseDeviceHandle(handle);
        return 0;
    }

    //Set I2C slave address
    uint8_t slaveDeviceAddress = 0x46;
    retStatus = SetI2CDeviceAddress(handle, slaveDeviceAddress);
    if (retStatus < 0) {
        printf("Set I2C device address failed, status:%d\n", retStatus);
        retStatus = CloseDeviceHandle(handle);
        return 0;
    }

    
    //I2C write
    //Start Address: 0x0000
    //Data : A string "I2CSample\0", \0 means NULL_character
    //Length : 10 bytes + 2 bytes Start Address
    uint8_t writeBuffer[12] = { 0x00, 0x00, 'I', '2', 'C', 'S', 'a', 'm', 'p', 'l', 'e', '\0' };
/*
    retStatus = I2CWrite(handle, writeBuffer, 12, &wirttenLength, 1000);
    if (retStatus < 0) {
        printf("I2C write failed, status:%d\n", retStatus);
        retStatus = CloseDeviceHandle(handle);
        return 0;
    }
    printf("write data: %s\n", &writeBuffer[2]);
*/
    
    //After the process of write, needs sleep to wait the i2c write is finished
    //The delay time is depended on the application
    uint32_t delay_us = 1000 * 2; //10ms
    usleep(delay_us); //sleep 10ms 

    //Write address first, and then, read data
    //Start Address: 0x0000
    //Length : 10 bytes
    writeBuffer[0] = 0x00;
    writeBuffer[1] = 0x00;
	
    uint8_t readBuffer[10] = { 0x00 };
    retStatus = I2CWriteRead(handle, writeBuffer, 2, readBuffer, 10, &readBackLength, 1000);
    if (retStatus < 0) {
        printf("I2C read failed, status:%d\n", retStatus);
        retStatus = CloseDeviceHandle(handle);
        return 0;
    }else{
        printf("read data: %s\n", readBuffer);
    }

    //Close Device
    retStatus = CloseDeviceHandle(handle);

    //Clean infomation the InitDevice create
    ExitDevice();
    return 0;
}
