#include <errno.h>
#include <stdio.h>
#include <time.h>
#include "HidDeviceSdkApi.h"
//
//  main.m
//  HidUARTSample
//
//  Copyright © 2018年 Prolific Technology Inc. All rights reserved.
//

int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

/******************************************************************************
This is a sample code for demonstrating how to use the SDK
Target : A loopback for writing 10 bytes data
******************************************************************************/

int main() {
    HANDLE handle;
    int32_t retStatus = ERROR_SUCCESS;
    uint32_t devNum = 0;
    uint32_t baudRate;
    uint8_t dataBit;
    uint8_t parity;
    uint8_t stopBit;
    uint8_t flowControl;
    uint8_t length = 10;
    wchar_t serialNumber[256];
    uint16_t vid = 0x067B, pid = 0x2330;
	
    //uint8_t writeBuffer[11] = {'0','1','2','3','4','5','6','7','8','9','\0'};
        
    //Initial API  
    retStatus = InitDevice();

    //Enumerate device by vid
    //devNum is a number that how many device's vid is 0x067B
    retStatus = EnumDeviceByVid(&devNum, 0x067b);
    if (retStatus < ERROR_SUCCESS) {
        printf("Not found device, status:%d\n", retStatus);
        return 0;
    }
    
    //Open device
    retStatus = OpenDeviceHandle(0, &handle);
    if (retStatus < ERROR_SUCCESS) {
        printf("Open device failed, status:%d\n", retStatus);
        return 0;
    }

    retStatus = GetVidPidSerialNumberByIndex(0, &vid, &pid, serialNumber, 256);
    printf("serial number:%ls\n", serialNumber);

    //BaudRate: 9600, 1 stop bit, none parity, data bit 8, no flow control    
    retStatus = SetUartConfig(handle, 9600, UART_ONE_STOP_BIT, UART_PARITY_NONE, UART_DATA_BIT_8, UART_DISABLE_FLOW_CONTROL);
    if (retStatus < ERROR_SUCCESS) {
        printf("set uart configure failed, status:%d\n", retStatus);
        return 0;
    }
    
    // Read uart configure value
    retStatus = GetUartConfig(handle, &baudRate, &stopBit, &parity, &dataBit, &flowControl);
    if (retStatus < ERROR_SUCCESS) {
        printf("get uart configure failed, status:%d\n", retStatus);
        return 0;
    }else{
        printf("baud rate:%u, stop bit:%d, parity:%d, data bit:%d, flowcontrol:%d\n", baudRate, stopBit, parity, dataBit, flowControl);
    }
    
    //Write data
    //uint16_t writtenLength = 0;
    //printf("write data %s\n", writeBuffer);
    //retStatus = WriteUart(handle, writeBuffer, length, &writtenLength);
    //if (retStatus < ERROR_SUCCESS) {
    //    printf("write data failed, status:%d\n", retStatus);
    //    return 0;
    //}

    uint16_t readLength = 0;
    uint8_t readBuffer[11] = {0x00};
    readBuffer[10] = '\0';
    
    //Read data
	for (int i=0; i<5; i++) {
    	retStatus = ReadUart(handle, readBuffer, length, &readLength, 10 * 1000);
    	if (retStatus < ERROR_SUCCESS) {
    	    printf("read data failed, status:%d\n", retStatus);
    	    return 0;
    	}else{
    	    printf("read data %s\n", readBuffer);
    	}
		msleep(1000);
	}

    //Close Device
    retStatus = CloseDeviceHandle(handle);
    	
    //Clean infomation the InitDevice create
    retStatus = ExitDevice();
    return 0;
}
