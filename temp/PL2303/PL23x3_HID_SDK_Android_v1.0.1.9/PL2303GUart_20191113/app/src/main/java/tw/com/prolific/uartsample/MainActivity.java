package tw.com.prolific.uartsample;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import tw.com.prolific.pl2303ghidsdk.ErrorCode;
import tw.com.prolific.pl2303ghidsdk.HidToUart;
import tw.com.prolific.pl2303ghidsdk.UartObject;
import tw.com.prolific.pl2303ghidsdk.UartSetting;

public class MainActivity extends AppCompatActivity {
    private static final String DEFINE_ACTION_USB_PERMISSION = "tw.com.prolific.uartsample.USB_PERMISSION";

    private static final int DEFAULT_DATA_LENGTH = 64;
    private static final int PACKATE_DATA_LENGTH = 512;

    private UartObject m_uartSettingObject = new UartObject();
    private UartObject m_readUartSettingObject = new UartObject();

    private final static int m_nDeviceVid = 0x067b;
    private int m_nUsbDeviceCount = 0;
    private int m_nDeviceIndex = 0;

    //Usb Permission
    private PendingIntent m_pendingIntent;
    private IntentFilter m_intentFilter;
    HidToUart m_hidToUart;
    private byte[] m_byWriteBuffer = null;

    private static final int MSG_SHOW_INFO = 0x00;
    private static final int MSG_UPDATE_UI = 0x01;

    private final String INFO_MESSAGE = "infoBody";

    //UI component
    private EditText m_edtxtBaudRate;
    private Spinner m_spDataBit;
    private Spinner m_spStopBit;
    private Spinner m_spParity;
    //private Spinner m_spFlowCtrl;
    private Button  m_btnClear;
    private Button  m_btnSend;
    private ScrollView m_svLog;
    private TextView m_txtLog;
    private EditText m_edtxtData;

    private String m_strLog = "";

    UartSetting m_uartSetting = new UartSetting();
    Thread readThread = null;
    boolean isStopRead = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_edtxtBaudRate = (EditText) findViewById(R.id.edtxtBaudRate);
        m_edtxtBaudRate.addTextChangedListener(new OnBaudrateTextWatchr());

        m_spDataBit = (Spinner) findViewById(R.id.spDatabit);
        ArrayAdapter<String> dataBitAdapter=new ArrayAdapter<String>(this,R.layout.custom_spinner, getResources().getStringArray(R.array.DataBit));
        dataBitAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        m_spDataBit.setAdapter(dataBitAdapter);
        m_spDataBit.setOnItemSelectedListener(new OnDataBitItemSelectedListener());

        m_spStopBit = (Spinner) findViewById(R.id.spStopBit);
        ArrayAdapter<String> stopBitAdapter=new ArrayAdapter<String>(this,R.layout.custom_spinner, getResources().getStringArray(R.array.StopBit));
        stopBitAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        m_spStopBit.setAdapter(stopBitAdapter);
        m_spStopBit.setOnItemSelectedListener(new OnStopBitItemSelectedListener());

        m_spParity = (Spinner) findViewById(R.id.spParity);
        ArrayAdapter<String> parityAdapter=new ArrayAdapter<String>(this,R.layout.custom_spinner, getResources().getStringArray(R.array.Parity));
        parityAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        m_spParity.setAdapter(parityAdapter);
        m_spParity.setOnItemSelectedListener(new OnParityItemSelectedListener());

        m_txtLog = (TextView) findViewById(R.id.txtLog);
        m_svLog = (ScrollView) findViewById(R.id.svLog);
        m_edtxtData =(EditText) findViewById(R.id.edtxtData);

        // get service
        m_hidToUart = new HidToUart(MainActivity.this, DEFINE_ACTION_USB_PERMISSION);
        //Step1: Register the broadcast receiver in onCreate() method
        //IntentFilter intentFilter = new IntentFilter(DEFINE_ACTION_USB_PERMISSION);
        m_intentFilter = new IntentFilter(DEFINE_ACTION_USB_PERMISSION);
        m_intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        m_intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        if(broadCastUsbReceiver != null) {
            registerReceiver(broadCastUsbReceiver, m_intentFilter);
        }

        //Setup Uart Object Default Value
        m_uartSettingObject.FlowControl = UartSetting.FlowControl.DISABLE;
        m_uartSettingObject.BaudRate = 9600;
        m_uartSettingObject.StopBit = UartSetting.StopBit.ONE;
        m_uartSettingObject.DataBit = UartSetting.DataBit.EIGHT;
        m_uartSettingObject.Parity =  UartSetting.Parity.NONE;

        m_spDataBit.setSelection(m_uartSettingObject.DataBit.getValue()-5);
    }

    public void onStart() {
        super.onStart();
        m_hidToUart.dumpMsg("Enter onStart");

        // retrieves the USB devices, which has VID/PID ID associated which is/are attached to the system
        m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);
        if( 0== m_nUsbDeviceCount) {
            Toast.makeText(this, "no more devices found", Toast.LENGTH_SHORT).show();
        } else {
            //m_strDeviceName = m_hidToI2C.getDeviceName(1);
            if(false == m_hidToUart.hasPermissionByDeviceIndex(0)) {
                m_hidToUart.askPermissionByDeviceIndex(0);
            }

            m_hidToUart.dumpMsg("m_nUsbDeviceCount="+ m_nUsbDeviceCount);
            Toast.makeText(this, "The "+ m_nUsbDeviceCount + " devices are attached", Toast.LENGTH_SHORT).show();
            try {
                if(readThread==null) {
                    getDeviceConfig(m_nDeviceIndex);
                    readThread = new Thread(readRunnable);
                    readThread.start();
                } else if(!readThread.isAlive()) {
                    readThread.join();
                    readThread = null;
                    readThread = new Thread(readRunnable);
                    readThread.start();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        m_hidToUart.dumpMsg("Leave onStart");
    }

    protected void onDestroy() {
        m_hidToUart.dumpMsg("onDestroy");
        try {
            if(readThread!= null && readThread.isAlive()) {
                isStopRead = true;
                readThread.join();
                readThread = null;
                m_hidToUart.dumpMsg("onDestroy Stop ReadThread.");
            }
        } catch (InterruptedException e) {
            m_hidToUart.dumpMsg("onDestroy Stop ReadThread exception.");
            e.printStackTrace();
            readThread = null;
        }

        if(0!= m_nUsbDeviceCount) {
            unregisterReceiver(this.broadCastUsbReceiver);
        }
        super.onDestroy();
    }

    private final BroadcastReceiver broadCastUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            m_hidToUart.dumpMsg("BroadcastReceiver-onReceive");
            String strAction = intent.getAction();
            if(strAction.equals(DEFINE_ACTION_USB_PERMISSION)) {
                m_hidToUart.dumpMsg("DEFINE_ACTION_USB_PERMISSION");
                synchronized (this) {
                    UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(usbDevice !=null) {
                            String strDeviceName = usbDevice.getDeviceName();
                            m_hidToUart.dumpMsg("%s Attached " + strDeviceName);
                            /*
                            UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                            //call method to set up device communication, Call Permission Request
                            if(false == usbManager.hasPermission(usbDevice)) {
                                usbManager.requestPermission(usbDevice, m_pendingIntent);
                            }
                            */

                            getDeviceConfig(m_nDeviceIndex);
                            try {
                                if(readThread==null) {
                                    readThread = new Thread(readRunnable);
                                    readThread.start();
                                } else if(!readThread.isAlive()) {
                                    readThread.join();
                                    readThread = null;
                                    readThread = new Thread(readRunnable);
                                    readThread.start();
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        m_hidToUart.dumpMsg("Request Permission Pass");
                    } else {
                        m_hidToUart.dumpMsg("Request Permission Fail");
                    }
                }
            }
            if( UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(strAction) ) {
                m_hidToUart.dumpMsg("ACTION_USB_DEVICE_ATTACHED");
                //refresh enumdevice
                synchronized (this) {
                    m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);

                    getDeviceConfig(m_nDeviceIndex);
                    try {
                        if(readThread==null) {
                            readThread = new Thread(readRunnable);
                            readThread.start();
                        } else if(!readThread.isAlive()) {
                            readThread.join();
                            readThread = null;
                            readThread = new Thread(readRunnable);
                            readThread.start();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                m_hidToUart.dumpMsg("DeivceCount=" + m_nUsbDeviceCount);
            }
            if( UsbManager.ACTION_USB_DEVICE_DETACHED.equals(strAction) ) {
                m_hidToUart.dumpMsg("ACTION_USB_DEVICE_DETACHED");
                //Remove Device, Cleans and closes communication with the device
                synchronized (this) {
                    m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);
                    try {
                        if(readThread.isAlive()) {
                            isStopRead = true;
                            readThread.join();
                            readThread = null;
                            m_hidToUart.dumpMsg("detached Stop ReadThread.");
                        }
                    } catch (InterruptedException e) {
                        m_hidToUart.dumpMsg("detached Stop ReadThread exception.");
                        e.printStackTrace();
                        readThread = null;
                    }
                }
                m_hidToUart.dumpMsg("DeivceCount=" + m_nUsbDeviceCount);
            }
        }//onReceive
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    //Menu item event
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_about:
                onAbout();
                return true;
            case R.id.action_finish:
                onFinishTest();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //menu about event
    void onAbout() {
        //show about
        String strVersion = "";
        strVersion = m_hidToUart.getSdkVersion();
        strVersion = "SDK版本: " + strVersion;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.about);
        alertDialog.setMessage(strVersion);
        alertDialog.setPositiveButton("確定", null);
        alertDialog.show();
    }

    //menu finish event
    void onFinishTest() {
        finish();
    }

    //btnSend event
    public void onSend(View view) {
        try {
            byte[] data = m_edtxtData.getText().toString().getBytes();
            if(data.length ==0) {
                Toast.makeText(MainActivity.this, "No data.", Toast.LENGTH_SHORT).show();
                return;
            }
            if(null == m_hidToUart)
                return;

            /*getDeviceConfig(m_nDeviceIndex);
            if(readThread==null) {
                readThread = news Thread(readRunnable);
                readThread.start();
            } else if(!readThread.isAlive()) {
                readThread.start();
            }

            Thread.sleep(100);*/

            m_hidToUart.dumpMsg("Enter onSend");

            if(m_uartSettingObject.DataBit == UartSetting.DataBit.FIVE)
                FillDataBuffer(data,0x1F);
            else if(m_uartSettingObject.DataBit == UartSetting.DataBit.SIX)
                FillDataBuffer(data,0x3F);
            else if(m_uartSettingObject.DataBit == UartSetting.DataBit.SEVEN)
                FillDataBuffer(data,0x7F);
            else
                FillDataBuffer(data,0xFF);

            ShowInfo("Data processing OK!!" + "\nUse DataLength: " + data.length + " bytes");
            //ShowProgress
            //("Process", "Writing Data...");
            //Set Uart Config
            int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
            if(res<0) {
                m_hidToUart.dumpMsg("onSend set config failed.");
                ShowInfo("Set uart config failed before write data. res: " + res);
                return;
            }

            //Starting write Data
            int remainLength = data.length, nPackageLength = 0;
            int nDataStart = 0;
            byte[] byTempWriteBuffer;

            while( remainLength !=0 ) {
                if(remainLength < PACKATE_DATA_LENGTH) {
                    nPackageLength = remainLength;
                    remainLength = 0;
                }
                else {
                    nPackageLength = PACKATE_DATA_LENGTH;
                    remainLength -= PACKATE_DATA_LENGTH;
                }
                byTempWriteBuffer = new byte[nPackageLength];

                //src, start, dist, start
                System.arraycopy(m_byWriteBuffer, nDataStart, byTempWriteBuffer, 0, nPackageLength);

                res = m_hidToUart.uartWrite(m_nDeviceIndex, byTempWriteBuffer, byTempWriteBuffer.length, 1000);
                if(res < 0) {
                    m_hidToUart.dumpMsg("PL2303GUART_Write failed.");
                    ShowInfo("PL2303GUART_Write failed. res: " + res);
                    m_hidToUart.closeDevice();
                    return;
                }
                m_hidToUart.dumpMsg("Write data " +nPackageLength +" bytes");
                nDataStart += nPackageLength;
            }//finish write data

            m_hidToUart.dumpMsg("Leave onSend.");
            ShowInfo("Writing Data Finished.");
        }catch(NullPointerException e) {
            Toast.makeText(MainActivity.this, "No data2.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private Runnable readRunnable = new Runnable(){ //Read Thread

        @Override
        public void run() {
            if(null == m_hidToUart)
                return;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            m_hidToUart.dumpMsg("Enter readRunnable");

            byte[] readBuffer = new byte[DEFAULT_DATA_LENGTH];

            int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
            if(res<0) {
                m_hidToUart.dumpMsg("readRunnable setUartConfig Failed.");
                ShowInfo("readRunnable setUartConfig Failed");
                return;
            }

            ShowInfo("Starting Reading Data");

            while(true){
                try {
                    if(isStopRead) {
                        m_hidToUart.dumpMsg("readRunnable Stopped.");
                        ShowInfo("readRunnable Stopped.");
                        break;
                    }
                    res = m_hidToUart.uartRead(m_nDeviceIndex, readBuffer, readBuffer.length, 1000);
                    if(res== ErrorCode.ERROR_READ_DATA_FAIL) {
                        Thread.sleep(100);
                        //ShowInfo("readRunnable read data failed.");
                        continue;
                    }
                    else if(res<0) {
                        m_hidToUart.dumpMsg("readRunnable uartRead Failed.");
                        //ShowInfo("readRunnable uartRead Failed");
                        return;
                    }
                    else { //Read data success
                        //ShowInfo("readRunnable read data success with "+readBuffer.length + " bytes");
                        ShowBuffer(readBuffer, 5);
                        readBuffer = new byte[DEFAULT_DATA_LENGTH];
                    }

                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    ShowInfo("readRunnable Thread InterruptedException.");
                    return;
                }
            }
            ShowInfo("readRunnable Thread Finished.");
        }
    };

    //btnClear event
    public void onClear(View view) {
        showLog("", true);
    }

    synchronized public void showLog(String strLog, boolean isClear){
        if(isClear)
            m_strLog = "";
        m_strLog = m_strLog + strLog +"\n";
        m_txtLog.setText(m_strLog);
        m_svLog.fullScroll(ScrollView.FOCUS_DOWN);
    }

	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes, int len) {
	    char[] hexChars = new char[bytes.length * 2];
		int l = len;
		if (l > bytes.length) {
			l = bytes.length;
		}
	    for (int j = 0; j < l; j++) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = HEX_ARRAY[v >>> 4];
	        hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	private static int getUnsignedByte(byte b) {
	    return b & 0x0FF;
	}
	public static String byteToHex(byte b) {
	    char[] hexChars = new char[2];
	    int v = (int)(b & 0xFF);
	    hexChars[0] = HEX_ARRAY[v >>> 4];
	    hexChars[1] = HEX_ARRAY[v & 0x0F];
	    return new String(hexChars);
	}
    private void ShowBuffer(byte[] buffer, int len) {
        Message msg = new Message();
        msg.what = MSG_SHOW_INFO;
        Intent intent = new Intent();
		int bit_sign = buffer[1];
		int temp_byte0 = getUnsignedByte(buffer[2]);
		int temp_byte1 = getUnsignedByte(buffer[3]);
		byte checksum = (byte)(buffer[1] + buffer[2] + buffer[3]);
		if (checksum != buffer[4]) {
        	intent.putExtra(INFO_MESSAGE, "Checksum Error:"+byteToHex(checksum)+"/"+byteToHex(buffer[4]));
		} else {
			int val = (temp_byte0 << 8) + temp_byte1;
			double temp_val = -1;

			if (bit_sign == 1) { // 負數
				val =~ val;
				val &= 0xFFFF;
				val++;
				temp_val = -3.90625 * val / 1000.00;
			} else {
				temp_val = 3.90625 * val / 1000.00;
			}
        	intent.putExtra(INFO_MESSAGE, 
				String.format("%2.1f\t%s", temp_val, bytesToHex(buffer,5)));
		}
        msg.obj = intent;
        mHandler.sendMessage(msg);
    }

    private void ShowInfo(String str) {
        Message msg = new Message();
        msg.what = MSG_SHOW_INFO;
        Intent intent = new Intent();
        intent.putExtra(INFO_MESSAGE,str);
        msg.obj = intent;
        mHandler.sendMessage(msg);
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case MSG_SHOW_INFO:
                    String str1 = ((Intent)msg.obj).getExtras().getString(INFO_MESSAGE);
                    showLog(str1, false);
                    break;
                case MSG_UPDATE_UI:
                    m_spDataBit.setSelection(m_readUartSettingObject.DataBit.getValue() -5);
                    m_spParity.setSelection(m_readUartSettingObject.Parity.getValue());
                    m_spStopBit.setSelection(m_readUartSettingObject.StopBit.getValue());
                    m_edtxtBaudRate.setText(String.format("%s", m_readUartSettingObject.BaudRate));
                    break;
            }
            super.handleMessage(msg);
        }
    };

    void getDeviceConfig(int index) {
        if(null == m_hidToUart)
            return;
        m_hidToUart.dumpMsg("Enter getDeviceConfig");
        int res = m_hidToUart.openDeviceByIndex(index);
        if(res<0) {
            m_hidToUart.dumpMsg("getDeviceConfig Open device failed.");
            return;
        }

        //Read Uart Config and compare
        res = m_hidToUart.getUartConfig(m_nDeviceIndex, m_readUartSettingObject);
        if(res<0) {
            m_hidToUart.dumpMsg("getDeviceConfig get default setting failed.");
            return;
        }

        m_edtxtBaudRate.setText(String.format("%s", m_readUartSettingObject.BaudRate));
        m_spDataBit.setSelection(m_readUartSettingObject.DataBit.getValue()-5);
        m_spStopBit.setSelection(m_readUartSettingObject.StopBit.getValue());
        m_spParity.setSelection(m_readUartSettingObject.Parity.getValue());

        m_hidToUart.dumpMsg("Leave getDeviceConfig");
    }


    private void FillDataBuffer(byte[] byBuffer, int dwMaskChar) {
        int nDataLength = byBuffer.length;
        m_byWriteBuffer = new byte[nDataLength];
        for(int index=0; index< nDataLength; index++)
            m_byWriteBuffer[index] = (byte) (byBuffer[index] & dwMaskChar);
    }

    public class OnStopBitItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(null== m_hidToUart) {
                ShowInfo("Library not Open.");
                return;
            }
            Spinner spinner = (Spinner) parent;

            UartSetting.StopBit beforeSetting = m_uartSettingObject.StopBit;
            if(beforeSetting.getValue() == position)
                return;

            if (position == 1)
                m_uartSettingObject.StopBit = UartSetting.StopBit.ONE_POINT_FIVE;
            else if(position ==2)
                m_uartSettingObject.StopBit = UartSetting.StopBit.TWO;
            else
                m_uartSettingObject.StopBit = UartSetting.StopBit.ONE;

            int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
            if(res<0) {
                m_hidToUart.dumpMsg("set stop bit failed.");
                ShowInfo("set stop bit "+ spinner.getSelectedItem().toString() +" failed.");
                m_uartSettingObject.StopBit = beforeSetting;
                spinner.setSelection(m_uartSettingObject.StopBit.getValue());
                return;
            }
            ShowInfo("set stop bit "+ spinner.getSelectedItem().toString() +" success.");
            m_hidToUart.dumpMsg("set stop bit success.");
        }//public void onItemSelected
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }//OnStopBitItemSelectedListener

    public class OnDataBitItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(null== m_hidToUart) {
                ShowInfo("Library not Open.");
                return;
            }
            Spinner spinner = (Spinner) parent;

            UartSetting.DataBit beforeSetting = m_uartSettingObject.DataBit;
            if((beforeSetting.getValue()-5) == position)
                return;

            switch (position) {
                case 0:
                    m_uartSettingObject.DataBit = UartSetting.DataBit.FIVE;
                    break;
                case 1:
                    m_uartSettingObject.DataBit = UartSetting.DataBit.SIX;
                    break;
                case 2:
                    m_uartSettingObject.DataBit = UartSetting.DataBit.SEVEN;
                    break;
                case 3:
                default:
                    m_uartSettingObject.DataBit = UartSetting.DataBit.EIGHT;
                    break;
            }

            int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
            if(res<0) {
                m_hidToUart.dumpMsg("set data bit failed.");
                ShowInfo("set data bit "+ spinner.getSelectedItem().toString() +" failed.");
                m_uartSettingObject.DataBit = beforeSetting;
                spinner.setSelection((m_uartSettingObject.DataBit.getValue()-5));
                return;
            }
            ShowInfo("set data bit "+ spinner.getSelectedItem().toString() +" success.");
            m_hidToUart.dumpMsg("set data bit success.");

        }//public void onItemSelected
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }//OnDataBitItemSelectedListener

    public class OnParityItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(null== m_hidToUart) {
                ShowInfo("Library not Open.");
                return;
            }
            Spinner spinner = (Spinner) parent;

            UartSetting.Parity beforeSetting = m_uartSettingObject.Parity;
            if(beforeSetting.getValue() == position)
                return;

            switch (position) {
                case 1:
                    m_uartSettingObject.Parity = UartSetting.Parity.ODD;
                    break;
                case 2:
                    m_uartSettingObject.Parity = UartSetting.Parity.EVEN;
                    break;
                case 3:
                    m_uartSettingObject.Parity = UartSetting.Parity.MARK;
                    break;
                case 4:
                    m_uartSettingObject.Parity = UartSetting.Parity.SPACE;
                    break;
                default:
                    m_uartSettingObject.Parity = UartSetting.Parity.NONE;
                    break;
            }

            int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
            if(res<0) {
                m_hidToUart.dumpMsg("set Parity failed.");
                ShowInfo("set Parity "+ spinner.getSelectedItem().toString() +" failed.");
                m_uartSettingObject.Parity = beforeSetting;
                spinner.setSelection(m_uartSettingObject.Parity.getValue());
                return;
            }
            ShowInfo("set Parity "+ spinner.getSelectedItem().toString() +" success.");
            m_hidToUart.dumpMsg("set Parity success.");

        }//public void onItemSelected
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }//OnParityItemSelectedListener

    public class OnBaudrateTextWatchr implements TextWatcher {
        int beforeBaudrate;
        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            try {
                beforeBaudrate = Integer.parseInt(charSequence.toString());
            }catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            //get Clock value,and covert clock to Hz
            try {
                if(null== m_hidToUart) {
                    ShowInfo("Library not Open.");
                    return;
                }

                int tmpBaudrate = Integer.parseInt(editable.toString());
                m_uartSettingObject.BaudRate = tmpBaudrate;

                int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
                if(res<0) {
                    m_hidToUart.dumpMsg("set Baudrate failed.");
                    ShowInfo("set Baudrate "+ tmpBaudrate +" failed.");
                    m_uartSettingObject.BaudRate = beforeBaudrate;
                    m_edtxtBaudRate.setText(editable);
                    return;
                }
                ShowInfo("set Baudrate "+ tmpBaudrate +" success.");
            }catch(NumberFormatException e) {
                m_uartSettingObject.BaudRate = beforeBaudrate;
                m_edtxtBaudRate.setText(editable);
                e.printStackTrace();
            }
        }
    }//OnFrequencyTextWatchr

    /*public class OnBaudRateItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(position !=0){ //if user selected 0, we don't change baud rate.

            }else {
                if(isGetUartSetting) {
                    //show the last setting item
                }
            }
        }//public void onItemSelected
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }//OnBaudRateItemSelectedListener*/

    /*public class OnFlowCtrlItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        }//public void onItemSelected
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }//OnFlowCtrlItemSelectedListener*/
}
