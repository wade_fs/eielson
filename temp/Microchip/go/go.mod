module mcp2221a

go 1.14

require (
	github.com/ardnew/mcp2221a v0.1.1
	github.com/karalabe/hid v1.0.0
)
