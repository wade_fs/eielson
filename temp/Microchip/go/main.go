package main

import (
	"flag"
	"fmt"
  	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

var (
	m *MCP2221A
	st   uint
	pin  byte
)

func init() {
	// 分幾個來初始化，首先是命令列參數
	var vids, pids, pins string
	flag.StringVar(&vids, "vid", fmt.Sprintf("%04X", VID), "VID of hidraw, use dmesg to see")
	flag.StringVar(&pids, "pid", fmt.Sprintf("%04X", PID), "PID of hidraw, use dmesg to see")
	flag.StringVar(&pins, "pin", "3", "GPIO to control")
	flag.UintVar(&st,  "sleep", 2000, "Millisecond to sleep")
	flag.Parse()
	vid, err := strconv.ParseInt(vids, 16, 16)
	if err != nil {
		log.Fatalf("vid not legal", vids)
	}
	pid, err := strconv.ParseInt(pids, 16, 16)
	if err != nil {
		log.Fatalf("pid not legal", pids)
	}
	pinn, err := strconv.ParseInt(pins, 10, 8)
	if err != nil {
		log.Fatalf("pin not legal", pins)
	}
	pin = byte(pinn)
	log.Printf("vid:pid = %04X:%04X, sleep=%d pin=%d\n", vid, pid, st, pin)

	// 初始化裝置
	mm, err := New(0, uint16(vid), uint16(pid))
	if nil != err {
		log.Fatalf("Open(): %v", err)
	}
	m = mm

	// 捕捉 Ctrl-C
	SetupCloseHandler()

	// 重設裝置
	log.Print(PackageVersion(), "\nWait to reset default...\n")
	if err := m.Reset(5 * time.Second); nil != err {
		log.Fatalf("Reset(): %v", err)
	}

	// 接下來照 CT1711 的範例來寫
	ct1711_init()
}

func ct1711_init() {
	setPin(0)
	time.Sleep(500 * time.Microsecond)
	setPin(1)
}

func readBit() byte {
	var data byte
	setPin(0)
	pin = pin+0	// noop
	setPin(1)
	time.Sleep(20 * time.Microsecond)
	data = getPin()
	pin = pin+0
	setPin(1)
	time.Sleep(30 * time.Microsecond)
	return data
}

func readByte() byte {
	var data byte = 0
	for i:=8; i>0; i-- {
		data <<= 1;
		data |= readBit()
	}
	return data
}

func readTemp() float32 {
	var temp float32 = 0.0
	var cc0, cc1, sign byte
	var byte0, byte1 byte
	val := 0

	ct1711_init()
	time.Sleep(150 * time.Millisecond)
	cc0 = readBit()
	time.Sleep(10 * time.Microsecond)
	cc1 = readBit()
	time.Sleep(10 * time.Microsecond)
	cc0 = cc0 & 0x01
	cc1 = cc1 & 0x01
	if ( cc0 == 0x00) && (cc1 == 0x00) {
		sign = readBit()
		time.Sleep(10 * time.Microsecond)
		byte0 = readByte()
		time.Sleep(10 * time.Microsecond)
		byte1 = readByte()
		time.Sleep(10 * time.Microsecond)
		val = int((byte0 << 8) + byte1)
		if sign == 0x01 {
			val = val ^ 0xFFFF
			val &= 0xFFFF
			val++
			temp = -3.90625 * float32(val) / 1000.0
		} else {
			temp = 3.90625 * float32(val) / 1000.0
		}
		return temp
	}
	return 0.0
}

func setPin(val byte) {
	//m.GPIO.SetConfig(pin, 0, ModeGPIO, DirOutput)
	if err := m.GPIO.Set(pin, val); nil != err {
		log.Fatalf("GPIO..Set(): %v", err)
	}
}

func getPin() byte {
	if get, err := m.GPIO.Get(pin); err != nil {
		log.Fatalf("GPIO.Get(): %v", err)
	} else {
		return get
	}
	return 0
}

func SetupCloseHandler() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		m.Close()
		os.Exit(0)
	}()
}

func main() {
	defer m.Close()

	// set pin 0 to GPIO output mode
	if err := m.GPIO.SetConfig(pin, 0, ModeGPIO, DirInput); nil != err {
		log.Fatalf("GPIO.SetConfig(): %v", err)
	}

	// repeatedly toggle the pin, and then read and print its value, every 500 ms
	for {
		temp := readTemp()
		log.Printf("Pin[%d] = %f", pin, temp)
		time.Sleep(time.Duration(st) * time.Millisecond)
	}
}
