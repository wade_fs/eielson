-------------------------------------------------------------------
        Release Notes for the MCP2221 I2C/SMBus Terminal
-------------------------------------------------------------------

                Microchip Technology, Inc.




-------------------------------------------------------------------
System Requirements
-------------------------------------------------------------------
Operating System: 	Windows XP SP3 or newer
Other: 				.NET4 Framework (client profile)


-------------------------------------------------------------------
Versioning History
-------------------------------------------------------------------
Version 2.0.1 (03/26/2020)
- fixed command list refresh issue when changing protocol after
	sending commands
- fixed command list refresh issue when deleting a command line
	by pressing the "Backspace" key
- fixed VID/PID pop up warning typo
- allow both 'x' and 'X' for hex values in the start and end 
	address fields for the bus scan
- fixed PEC calculation to include the address byte
	
Version 2.0.0 (Released 07/21/2014):
- added support for controlling the following MCP2221 features:
	- GPIO, ADC, DAC, Clock Out, IOC Clear, Dedicated Functions
	
Version 1.0.0 (Released 05/08/2014):
 - Initial release of the software


-------------------------------------------------------------------
Contact Information
-------------------------------------------------------------------
Main Website: 		http://www.microchip.com
Technical Support: 	http://support.microchip.com

