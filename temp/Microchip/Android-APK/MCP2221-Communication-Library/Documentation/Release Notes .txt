-------------------------------------------------------------------
	Release Notes for the MCP2221 Communication Library
-------------------------------------------------------------------

		Microchip Technology, Inc.




-------------------------------------------------------------------
System Requirements
-------------------------------------------------------------------
Operating System: 	Android 4.0 or Higher


-------------------------------------------------------------------
Versioning History
-------------------------------------------------------------------

Version 2.0 (Released 16-Feb-2015):
 - Added support for controlling the MCP2221 pin functions:
	- ADC, clock out, DAC, GPIO
 - Added support MCP2221 serial port communication

Version 1.0:
 - Initial release



-------------------------------------------------------------------
Contact Information
-------------------------------------------------------------------
Main Website: 		http://www.microchip.com
Technical Support: 	http://support.microchip.com
