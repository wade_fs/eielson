//©[2016] Microchip Technology Inc.and its subsidiaries.You may use this software and any derivatives exclusively with Microchip products.
//
//THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
//WARRANTIES OF NON - INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY 
//OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
//IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER
//RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.TO THE FULLEST EXTENT ALLOWED
//BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
//DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
//MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.
package main;

import com.microchip.mcp2221.Constants;
import com.microchip.mcp2221.HidFeatures;

/**
 *
 * @author W732
 */
public class Main {

    public static final int DEFAULT_VID = 0x04D8;
    public static final int DEFAULT_PID = 0x00DD;

    public static int result = 0;

    //DEVICE1
    public static int mcpIndex1 = 0;
    public static String mcpSN1 = "0000003816";
    public static long mcpHandle1 = -1;

    //DEVICE2
    public static int mcpIndex2 = 1;
    public static String mcpSN2 = "0000003815";
    public static long mcpHandle2 = -1;

    //wrong values for index and serial number
    public static int mcpIndexW = 10;
    public static String mcpSNW = "abc";
    public static long mcpHandleW = -1;

    public static String libVersion;

    public static int noOfDev = 0;
    public static int speed;

    public static byte whichToGet;
    public static byte whichToSet;

    public static int numberOfBytesToRead;
    public static int numberOfBytesToWrite;
    public static byte slaveAddress;
    public static boolean use7bitAddress;
    public static byte[] dataToWrite = new byte[65535];
    public static byte[] dataToRead = new byte[65535];

    public static void main(String[] args) throws InterruptedException {
        //create object to call native methods from HidFeatures
        HidFeatures chip = new HidFeatures();

        //*** Load library
        System.out.println("*** Load Dll");
        result = chip.Mcp2221_LoadDll();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Load DLL: " + result);
        }

        System.out.println("=================================================");

        //*** Get Library Version
        System.out.println("*** Get Library Version");
        libVersion = chip.Mcp2221_GetLibraryVersion();
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get Library Version: " + result);
        }

        System.out.println("Library version: " + libVersion);

        System.out.println("=================================================");

        //*** Get Connected Devices
        System.out.println("*** Get number of connected devices");
        noOfDev = chip.Mcp2221_GetConnectedDevices(DEFAULT_VID, DEFAULT_PID);
        if (noOfDev < 0) {
            System.out.println("!!! Get number of connected devices: " + noOfDev);
        } else {
            System.out.println("Number of connected devices: " + noOfDev);
        }

        System.out.println("=================================================");

        //*** Open connection by index for DEV1
        System.out.println("*** Open connection by index");
        mcpHandle1 = chip.Mcp2221_OpenByIndex(DEFAULT_VID, DEFAULT_PID, mcpIndex1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by index for dev1: " + result);
        }
        System.out.println("Handle for DEV1: " + mcpHandle1);

        //*** Open connection by index for DEV2
        mcpHandle2 = chip.Mcp2221_OpenByIndex(DEFAULT_VID, DEFAULT_PID, mcpIndex2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by index for dev2: " + result);
        }
        System.out.println("Handle for DEV2: " + mcpHandle2);

        //*** Open connection with a wrong index
        mcpHandleW = chip.Mcp2221_OpenByIndex(DEFAULT_VID, DEFAULT_PID, mcpIndexW);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_ERR_NO_SUCH_INDEX) {
            System.out.println("!!! Open connection using a wrong index: " + result);
        }
        System.out.println("Handle for opening connection with a wrong index: " + mcpHandleW);

        System.out.println("=================================================");

        //*** Close all connection
        System.out.println("*** Close all connection");
        result = chip.Mcp2221_CloseAll();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Close all connection : " + result);
        }

        System.out.println("=================================================");

        //*** Open connection by serial number for DEV1
        System.out.println("*** Open connection by serial number");
        mcpHandle1 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for dev1: " + result);
        }
        System.out.println("Handle DEV1: " + mcpHandle1);

        //*** Open connection by serial number for DEV2
        mcpHandle2 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for dev2: " + result);
        }
        System.out.println("Handle DEV2: " + mcpHandle2);

        //*** Open connection with wrong serial number
        mcpHandleW = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSNW);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_ERR_DEVICE_NOT_FOUND) {
            System.out.println("!!! Open connection using a wrong serial number: " + result);
        }
        System.out.println("Handle for opening connection with a wrong: " + mcpHandleW);

        System.out.println("=================================================");

        //*** Close every opened connection
        System.out.println("*** Close connection");
        result = chip.Mcp2221_Close(mcpHandle1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Close connection for DEV1: " + result);
        }
        result = chip.Mcp2221_Close(mcpHandle2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Close connection for DEV1: " + result);
        }

        System.out.println("=================================================");

        //*** Open connection by serial number for all 3 devices
        System.out.println("*** Open connection by serial number");
        mcpHandle1 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for DEV1: " + result);
        }
        System.out.println("Handle DEV1: " + mcpHandle1);
        mcpHandle2 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for DEV2: " + result);
        }
        System.out.println("Handle DEV2: " + mcpHandle2);

        System.out.println("=================================================");

        /*
         //*** Reset all connected devices 
         System.out.println("*** Reset devices");
         result = chip.Mcp2221_Reset(mcpHandle1);
         if(result != Constants.E_NO_ERR){
         System.out.println("!!! Reset dev1: " + result);
         }    
         result = chip.Mcp2221_Reset(mcpHandle2);
         if(result != Constants.E_NO_ERR){
         System.out.println("!!! Reset dev2: " + result);
         }   
         */
        //*** Close all connection
        result = chip.Mcp2221_CloseAll();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Close all connection: " + result);
        }
        //Thread.sleep(20000);

        System.out.println("=================================================");

        //*** Open connection by serial number for all 3 devices
        System.out.println("*** Open connection by serial number");
        mcpHandle1 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for DEV1: " + result);
        }
        System.out.println("Handle DEV1: " + mcpHandle1);
        mcpHandle2 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Open connection by serial number for DEV2: " + result);
        }
        System.out.println("Handle DEV2: " + mcpHandle2);

        System.out.println("=================================================");

        //*** Set speed for DEV1(min value)
        System.out.println("*** Set speed");
        speed = 46875;
        result = chip.Mcp2221_SetSpeed(mcpHandle1, speed);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set min value for speed: " + result);
        }

        //***Set speed for DEV2(max value)
        speed = 500000;
        result = chip.Mcp2221_SetSpeed(mcpHandle2, speed);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set max value for speed: " + result);
        }

        //*** Set speed for DEV2(wrong value)
        speed = 46874;
        result = chip.Mcp2221_SetSpeed(mcpHandle2, speed);
        if (result != Constants.E_ERR_INVALID_SPEED) {
            System.out.println("!!! Set wrong value for speed: " + result);
        }

        //*** Set speed for DEV1(middle value)
        speed = 250000;
        result = chip.Mcp2221_SetSpeed(mcpHandle1, speed);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set speed: " + result);
        }

        System.out.println("=================================================");

        //*** Get manufacturer string
        System.out.println("*** Get manufacturer descriptor");
        String manufacturerDes1 = new String();
        String manufacturerDes2 = new String();
        manufacturerDes1 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println(" Get manufacturer descriptor for dev1: " + result);
        }

        manufacturerDes2 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get manufacturer descriptor for dev2: " + result);
        }
        System.out.println("Manufacturer descriptor for DEV1: " + manufacturerDes1);
        System.out.println("Manufacturer descriptor for DEV2: " + manufacturerDes2);

        System.out.println("=================================================");

        //*** Set manufacturer descriptor
        System.out.println("*** Set manufacturer descriptor");
        String manufacturerDes1_temp = new String();
        String manufacturerDes2_temp = new String();

        manufacturerDes1_temp = "ABCDEF";
        result = chip.Mcp2221_SetManufacturerDescriptor(mcpHandle1, manufacturerDes1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set manufacturer descriptor for dev1: " + result);
        }
        manufacturerDes2_temp = "123456";
        result = chip.Mcp2221_SetManufacturerDescriptor(mcpHandle2, manufacturerDes2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set manufacturer descriptor for dev2: " + result);
        }

        //*** Get manufacturer descriptor
        manufacturerDes1 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get manufacturer descriptor for dev1: " + result);
        }

        manufacturerDes2 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get manufacturer descriptor for dev2: " + result);
        }

        System.out.println("Manufacturer descriptor for DEV1: " + manufacturerDes1);
        System.out.println("Manufacturer descriptor for DEV2: " + manufacturerDes2);

        //*** Set manufacturer descriptor (default value)
        System.out.println();
        System.out.println("*** Set manufacturer descriptor - default value");
        manufacturerDes1_temp = "Microchip Technology Inc.";
        result = chip.Mcp2221_SetManufacturerDescriptor(mcpHandle1, manufacturerDes1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set manufacturer descriptor for dev1: " + result);
        }
        manufacturerDes2_temp = "Microchip Technology Inc.";
        result = chip.Mcp2221_SetManufacturerDescriptor(mcpHandle2, manufacturerDes2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set manufacturer descriptor for dev2: " + result);
        }

        //*** Get manufacturer descriptor
        manufacturerDes1 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get manufacturer descriptor for dev1: " + result);
        }

        manufacturerDes2 = chip.Mcp2221_GetManufacturerDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get manufacturer descriptor for dev2: " + result);
        }

        System.out.println("Manufacturer descriptor for DEV1: " + manufacturerDes1);
        System.out.println("Manufacturer descriptor for DEV2: " + manufacturerDes2);

        System.out.println("=================================================");

        //*** Get product descriptor
        System.out.println("*** Get product descriptor");
        String productDes1 = new String();
        String productDes2 = new String();

        productDes1 = chip.Mcp2221_GetProductDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev1: " + productDes1);
        productDes2 = chip.Mcp2221_GetProductDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev2: " + productDes2);

        System.out.println("=================================================");

        //*** Set product descriptor
        System.out.println("*** Set product descriptor");
        String productDes1_temp = new String();
        String productDes2_temp = new String();
        productDes1_temp = "ABCDEF";
        result = chip.Mcp2221_SetProductDescriptor(mcpHandle1, productDes1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set product descriptor for dev1: " + result);
        }
        productDes2_temp = "123456";
        result = chip.Mcp2221_SetProductDescriptor(mcpHandle2, productDes2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set product descriptor for dev2: " + result);
        }

        //*** Get product descriptor
        productDes1 = chip.Mcp2221_GetProductDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev1: " + productDes1);
        productDes2 = chip.Mcp2221_GetProductDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev2: " + productDes2);

        //*** Set product descriptor - default value
        System.out.println();
        System.out.println("*** Set product descriptor - default value");
        productDes1_temp = "MCP2221 USB-I2C/UART Combo";
        productDes2_temp = "MCP2221 USB-I2C/UART Combo";
        result = chip.Mcp2221_SetProductDescriptor(mcpHandle1, productDes1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set product descriptor for dev1: " + result);
        }
        result = chip.Mcp2221_SetProductDescriptor(mcpHandle2, productDes2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set product descriptor for dev2: " + result);
        }

        //*** Get product descriptor
        productDes1 = chip.Mcp2221_GetProductDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev1: " + productDes1);
        productDes2 = chip.Mcp2221_GetProductDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get product descriptor: " + result);
        }
        System.out.println("Product descriptor for dev2: " + productDes2);

        System.out.println("=================================================");

        //*** Get serial number descriptor
        System.out.println("*** Get serial number descriptor");
        String serialNumber1;
        String serialNumber2;
        
        serialNumber1 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev1: " + result);
        }
        System.out.println("Serial number descriptor for dev1: " + serialNumber1);
        serialNumber2 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev2: " + result);
        }
        System.out.println("Serial number descriptor for dev2: " + serialNumber2);

        System.out.println("=================================================");

        //*** Set serial number descriptor
        System.out.println("*** Set serial number descriptor");
        String serialNumber1_temp;
        String serialNumber2_temp;

        serialNumber1_temp = "0000051044";
        result = chip.Mcp2221_SetSerialNumberDescriptor(mcpHandle1, serialNumber1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number descriptor for dev1: " + result);
        }
        serialNumber2_temp = "0000026267";
        result = chip.Mcp2221_SetSerialNumberDescriptor(mcpHandle2, serialNumber2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number descriptor for dev2: " + result);
        }

        //*** Get serial number descriptor
        serialNumber1 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev1: " + result);
        }
        System.out.println("Serial number descriptor for dev1: " + serialNumber1);
        serialNumber2 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev2: " + result);
        }
        System.out.println("Serial number descriptor for dev2: " + serialNumber2);

        //*** Set serial number descriptor - initial values
        System.out.println();
        System.out.println("*** Set serial number descriptor - initial values");
        serialNumber1_temp = mcpSN1;
        serialNumber2_temp = mcpSN2;
        result = chip.Mcp2221_SetSerialNumberDescriptor(mcpHandle1, serialNumber1_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number descriptor for dev1: " + result);
        }
        result = chip.Mcp2221_SetSerialNumberDescriptor(mcpHandle2, serialNumber2_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number descriptor for dev2: " + result);
        }

        //*** Get serial number descriptor
        serialNumber1 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev1: " + result);
        }
        System.out.println("Serial number descriptor for dev1: " + serialNumber1);
        serialNumber2 = chip.Mcp2221_GetSerialNumberDescriptor(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number descriptor for dev2: " + result);
        }
        System.out.println("Serial number descriptor for dev2: " + serialNumber2);

        System.out.println("=================================================");

        //*** Get factory serial number
        System.out.println("*** Get factory serial number");
        String serialNo1 = new String();
        String serialNo2 = new String();

        serialNo1 = chip.Mcp2221_GetFactorySerialNumber(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get factory serial number for dev1: " + result);
        }
        System.out.println("Factory serial number for dev1: " + serialNo1);
        serialNo2 = chip.Mcp2221_GetFactorySerialNumber(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get factory serial number for dev2: " + result);
        }
        System.out.println("Factory serial number for dev2: " + serialNo2);

        System.out.println("=================================================");

        //*** Get VID & PID
        System.out.println("*** Get VID & PID");
        int vid1[] = new int[1];
        int pid1[] = new int[1];
        int vid2[] = new int[1];
        int pid2[] = new int[1];

        result = chip.Mcp2221_GetVidPid(mcpHandle1, vid1, pid1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev1: " + result);
            vid1[0] = -1;
            pid1[0] = -1;
        }
        System.out.println("VID1: " + vid1[0] + "; PID1: " + pid1[0]);
        result = chip.Mcp2221_GetVidPid(mcpHandle2, vid2, pid2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev2: " + result);
            vid2[0] = -1;
            pid2[0] = -1;
        }
        System.out.println("VID2: " + vid2[0] + "; PID2: " + pid2[0]);

        System.out.println("=================================================");

        //*** Set vid and pid
        System.out.println("*** Set VID & PID");
        int vid_temp;
        int pid_temp;
        vid_temp = 0x4D5;
        pid_temp = 0xDE;

        result = chip.Mcp2221_SetVidPid(mcpHandle1, vid_temp, pid_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set vid and pid for dev1: " + result);
        }
        result = chip.Mcp2221_SetVidPid(mcpHandle2, vid_temp, pid_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set vid and pid for dev2: " + result);
        }

        //*** Get VID & PID
        vid1 = new int[1];
        pid1 = new int[1];
        result = chip.Mcp2221_GetVidPid(mcpHandle1, vid1, pid1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev1: " + result);
            vid1[0] = -1;
            pid1[0] = -1;
        }
        System.out.println("VID1: " + vid1[0] + "; PID1: " + pid1[0]);
        vid2 = new int[1];
        pid2 = new int[1];
        result = chip.Mcp2221_GetVidPid(mcpHandle2, vid2, pid2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev2: " + result);
            vid2[0] = -1;
            pid2[0] = -1;
        }
        System.out.println("VID2: " + vid2[0] + "; PID2: " + pid2[0]);

        //*** Set vid and pid - default values
        System.out.println();
        System.out.println("*** Set VID and PID - default values");
        vid_temp = 1240;
        pid_temp = 221;
        result = chip.Mcp2221_SetVidPid(mcpHandle1, vid_temp, pid_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set vid and pid for dev1: " + result);
        }
        result = chip.Mcp2221_SetVidPid(mcpHandle2, vid_temp, pid_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set vid and pid for dev2: " + result);
        }

        //*** Get VID & PID
        vid1 = new int[1];
        pid1 = new int[1];
        result = chip.Mcp2221_GetVidPid(mcpHandle1, vid1, pid1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev1: " + result);
            vid1[0] = -1;
            pid1[0] = -1;
        }
        System.out.println("VID1: " + vid1[0] + "; PID1: " + pid1[0]);
        vid2 = new int[1];
        pid2 = new int[1];
        result = chip.Mcp2221_GetVidPid(mcpHandle2, vid2, pid2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get vid and pid for dev2: " + result);
            vid2[0] = -1;
            pid2[0] = -1;
        }
        System.out.println("VID2: " + vid2[0] + "; PID2: " + pid2[0]);

        System.out.println("=================================================");

        //*** Get initial pin values
        System.out.println("*** Get initial pin values");
        byte ledUtxInitVal1[] = new byte[1];
        byte ledUrxInitVal1[] = new byte[1];
        byte ledI2cInitVal1[] = new byte[1];
        byte sspndInitVal1[] = new byte[1];
        byte usbCfgInitVal1[] = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle1, ledUtxInitVal1, ledUrxInitVal1, ledI2cInitVal1, sspndInitVal1, usbCfgInitVal1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev1: " + result);
            ledUtxInitVal1[0] = -1;
            ledUrxInitVal1[0] = -1;
            ledI2cInitVal1[0] = -1;
            sspndInitVal1[0] = -1;
            usbCfgInitVal1[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev1: " + ledUtxInitVal1[0]);
        System.out.println("ledUrxInitVal for dev1: " + ledUrxInitVal1[0]);
        System.out.println("ledI2cInitVal for dev1: " + ledI2cInitVal1[0]);
        System.out.println("sspndInitVal for dev1: " + sspndInitVal1[0]);
        System.out.println("usbCfgInitVal for dev1: " + usbCfgInitVal1[0]);

        byte ledUtxInitVal2[] = new byte[1];
        byte ledUrxInitVal2[] = new byte[1];
        byte ledI2cInitVal2[] = new byte[1];
        byte sspndInitVal2[] = new byte[1];
        byte usbCfgInitVal2[] = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle2, ledUtxInitVal2, ledUrxInitVal2, ledI2cInitVal2, sspndInitVal2, usbCfgInitVal2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev2: " + result);
            ledUtxInitVal2[0] = -1;
            ledUrxInitVal2[0] = -1;
            ledI2cInitVal2[0] = -1;
            sspndInitVal2[0] = -1;
            usbCfgInitVal2[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev2: " + ledUtxInitVal2[0]);
        System.out.println("ledUrxInitVal for dev2: " + ledUrxInitVal2[0]);
        System.out.println("ledI2cInitVal for dev2: " + ledI2cInitVal2[0]);
        System.out.println("sspndInitVal for dev2: " + sspndInitVal2[0]);
        System.out.println("usbCfgInitVal for dev2: " + usbCfgInitVal2[0]);

        System.out.println("=================================================");

        //*** Set initial pin values
        System.out.println("*** Set initial pin values");
        byte ledUtxInitVal_temp = 0x02;
        byte ledUrxInitVal_temp = 0x05;
        byte ledI2cInitVal_temp = 0x0A;
        byte sspndInitVal_temp = 0x54;
        byte usbCfgInitVal_temp = (byte) 0xAB;

        result = chip.Mcp2221_SetInitialPinValues(mcpHandle1, ledUtxInitVal_temp, ledUrxInitVal_temp, ledI2cInitVal_temp, sspndInitVal_temp, usbCfgInitVal_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set initial pin values for dev1: " + result);
        }
        ledUtxInitVal_temp = 0;
        ledUrxInitVal_temp = 1;
        ledI2cInitVal_temp = (byte) 0xFF;
        sspndInitVal_temp = 0;
        usbCfgInitVal_temp = (byte) 0xFF;

        result = chip.Mcp2221_SetInitialPinValues(mcpHandle2, ledUtxInitVal_temp, ledUrxInitVal_temp, ledI2cInitVal_temp, sspndInitVal_temp, usbCfgInitVal_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set initial pin values for dev2: " + result);
        }

        //*** Get initial pin values
        ledUtxInitVal1 = new byte[1];
        ledUrxInitVal1 = new byte[1];
        ledI2cInitVal1 = new byte[1];
        sspndInitVal1 = new byte[1];
        usbCfgInitVal1 = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle1, ledUtxInitVal1, ledUrxInitVal1, ledI2cInitVal1, sspndInitVal1, usbCfgInitVal1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev1: " + result);
            ledUtxInitVal1[0] = -1;
            ledUrxInitVal1[0] = -1;
            ledI2cInitVal1[0] = -1;
            sspndInitVal1[0] = -1;
            usbCfgInitVal1[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev1: " + ledUtxInitVal1[0]);
        System.out.println("ledUrxInitVal for dev1: " + ledUrxInitVal1[0]);
        System.out.println("ledI2cInitVal for dev1: " + ledI2cInitVal1[0]);
        System.out.println("sspndInitVal for dev1: " + sspndInitVal1[0]);
        System.out.println("usbCfgInitVal for dev1: " + usbCfgInitVal1[0]);

        ledUtxInitVal2 = new byte[1];
        ledUrxInitVal2 = new byte[1];
        ledI2cInitVal2 = new byte[1];
        sspndInitVal2 = new byte[1];
        usbCfgInitVal2 = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle2, ledUtxInitVal2, ledUrxInitVal2, ledI2cInitVal2, sspndInitVal2, usbCfgInitVal2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev2: " + result);
            ledUtxInitVal2[0] = -1;
            ledUrxInitVal2[0] = -1;
            ledI2cInitVal2[0] = -1;
            sspndInitVal2[0] = -1;
            usbCfgInitVal2[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev2: " + ledUtxInitVal2[0]);
        System.out.println("ledUrxInitVal for dev2: " + ledUrxInitVal2[0]);
        System.out.println("ledI2cInitVal for dev2: " + ledI2cInitVal2[0]);
        System.out.println("sspndInitVal for dev2: " + sspndInitVal2[0]);
        System.out.println("usbCfgInitVal for dev2: " + usbCfgInitVal2[0]);

        //*** Set initial pin values - initial values
        System.out.println();
        System.out.println("*** Set initial pin values - initial values");
        ledUtxInitVal_temp = 0x01;
        ledUrxInitVal_temp = 0x01;
        ledI2cInitVal_temp = 0x01;
        sspndInitVal_temp = 0x01;
        usbCfgInitVal_temp = 0x01;
        result = chip.Mcp2221_SetInitialPinValues(mcpHandle1, ledUtxInitVal_temp, ledUrxInitVal_temp, ledI2cInitVal_temp, sspndInitVal_temp, usbCfgInitVal_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set initial pin values for dev1: " + result);
        }
        result = chip.Mcp2221_SetInitialPinValues(mcpHandle2, ledUtxInitVal_temp, ledUrxInitVal_temp, ledI2cInitVal_temp, sspndInitVal_temp, usbCfgInitVal_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set initial pin values for dev2: " + result);
        }

        //*** Get initial pin values
        ledUtxInitVal1 = new byte[1];
        ledUrxInitVal1 = new byte[1];
        ledI2cInitVal1 = new byte[1];
        sspndInitVal1 = new byte[1];
        usbCfgInitVal1 = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle1, ledUtxInitVal1, ledUrxInitVal1, ledI2cInitVal1, sspndInitVal1, usbCfgInitVal1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev1: " + result);
            ledUtxInitVal1[0] = -1;
            ledUrxInitVal1[0] = -1;
            ledI2cInitVal1[0] = -1;
            sspndInitVal1[0] = -1;
            usbCfgInitVal1[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev1: " + ledUtxInitVal1[0]);
        System.out.println("ledUrxInitVal for dev1: " + ledUrxInitVal1[0]);
        System.out.println("ledI2cInitVal for dev1: " + ledI2cInitVal1[0]);
        System.out.println("sspndInitVal for dev1: " + sspndInitVal1[0]);
        System.out.println("usbCfgInitVal for dev1: " + usbCfgInitVal1[0]);

        ledUtxInitVal2 = new byte[1];
        ledUrxInitVal2 = new byte[1];
        ledI2cInitVal2 = new byte[1];
        sspndInitVal2 = new byte[1];
        usbCfgInitVal2 = new byte[1];
        result = chip.Mcp2221_GetInitialPinValues(mcpHandle2, ledUtxInitVal2, ledUrxInitVal2, ledI2cInitVal2, sspndInitVal2, usbCfgInitVal2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get initial pin values for dev2: " + result);
            ledUtxInitVal2[0] = -1;
            ledUrxInitVal2[0] = -1;
            ledI2cInitVal2[0] = -1;
            sspndInitVal2[0] = -1;
            usbCfgInitVal2[0] = -1;
        }
        System.out.println("ledUtxInitVal for dev2: " + ledUtxInitVal2[0]);
        System.out.println("ledUrxInitVal for dev2: " + ledUrxInitVal2[0]);
        System.out.println("ledI2cInitVal for dev2: " + ledI2cInitVal2[0]);
        System.out.println("sspndInitVal for dev2: " + sspndInitVal2[0]);
        System.out.println("usbCfgInitVal for dev2: " + usbCfgInitVal2[0]);

        System.out.println("=================================================");

        //*** Get USB Power Attributes
        System.out.println("*** Get USB Power Attributes");
        short powerAttributes1[] = new short[1];
        int currentReq1[] = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle1, powerAttributes1, currentReq1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev1: " + result);
            powerAttributes1[0] = -1;
            currentReq1[0] = -1;
        }
        System.out.println("USB Power Attributes for dev1: " + powerAttributes1[0]);
        System.out.println("Requested current for dev1: " + currentReq1[0]);

        short powerAttributes2[] = new short[1];
        int currentReq2[] = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle2, powerAttributes2, currentReq2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev2: " + result);
            powerAttributes2[0] = -1;
            currentReq2[0] = -1;
        }
        System.out.println("USB Power Attributes for dev2: " + powerAttributes2[0]);
        System.out.println("Requested current for dev2: " + currentReq2[0]);

        System.out.println("=================================================");

        //*** Set USB Power Attributes
        System.out.println("*** Set USB Power Attributes");
        short usbPowerAttributes_temp = 128;
        int currentReq_temp = 2;

        result = chip.Mcp2221_SetUsbPowerAttributes(mcpHandle1, usbPowerAttributes_temp, currentReq_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set USB power attributes for dev1: " + result);
        }
        result = chip.Mcp2221_SetUsbPowerAttributes(mcpHandle2, usbPowerAttributes_temp, currentReq_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set USB power attributes for dev2: " + result);
        }

        //*** Get USB power attributes
        powerAttributes1 = new short[1];
        currentReq1 = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle1, powerAttributes1, currentReq1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev1: " + result);
            powerAttributes1[0] = -1;
            currentReq1[0] = -1;
        }
        System.out.println("USB Power Attributes for dev1: " + powerAttributes1[0]);
        System.out.println("Requested current for dev1: " + currentReq1[0]);

        powerAttributes2 = new short[1];
        currentReq2 = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle2, powerAttributes2, currentReq2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev2: " + result);
            powerAttributes2[0] = -1;
            currentReq2[0] = -1;
        }
        System.out.println("USB Power Attributes for dev2: " + powerAttributes2[0]);
        System.out.println("Requested current for dev2: " + currentReq2[0]);

        //*** Set USB Power Attributes - initial values
        System.out.println();
        System.out.println("*** Set USB Power Attributes - initial values");
        usbPowerAttributes_temp = (byte) 0x80;
        currentReq_temp = 100;
        result = chip.Mcp2221_SetUsbPowerAttributes(mcpHandle1, usbPowerAttributes_temp, currentReq_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set USB power attributes for dev1: " + result);
        }
        result = chip.Mcp2221_SetUsbPowerAttributes(mcpHandle2, usbPowerAttributes_temp, currentReq_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set USB power attributes for dev2: " + result);
        }

        //*** Get USB power attributes
        powerAttributes1 = new short[1];
        currentReq1 = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle1, powerAttributes1, currentReq1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev1: " + result);
            powerAttributes1[0] = -1;
            currentReq1[0] = -1;
        }
        System.out.println("USB Power Attributes for dev1: " + powerAttributes1[0]);
        System.out.println("Requested current for dev1: " + currentReq1[0]);

        powerAttributes2 = new short[1];
        currentReq2 = new int[1];
        result = chip.Mcp2221_GetUsbPowerAttributes(mcpHandle2, powerAttributes2, currentReq2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get USB power attributes for dev2: " + result);
            powerAttributes2[0] = -1;
            currentReq2[0] = -1;
        }
        System.out.println("USB Power Attributes for dev2: " + powerAttributes2[0]);
        System.out.println("Requested current for dev2: " + currentReq2[0]);

        System.out.println("=================================================");

        //*** Get interrupt edge setting
        System.out.println("*** Get interrupt edge setting");
        System.out.println("> Flash settings");
        whichToGet = 0;
        byte interruptPinMode1[] = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        byte interruptPinMode2[] = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);
        System.out.println();

        System.out.println("> SRAM settings");
        whichToGet = 1;
        interruptPinMode1 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        interruptPinMode2 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);

        System.out.println("=================================================");

        //*** Set interrupt edge setting
        System.out.println("*** Set interrupt edge setting");
        //Flash settings
        whichToSet = 0;
        byte interruptPinMode_temp = 3;
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle1, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev1: " + result);
        }
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle2, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev2: " + result);
        }
        //SRAM settings
        whichToSet = 1;
        interruptPinMode_temp = 3;
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle1, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev1: " + result);
        }
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle2, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev2: " + result);
        }

        //*** Get interrupt edge setting
        System.out.println("> Flash settings");
        whichToGet = 0;
        interruptPinMode1 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        interruptPinMode2 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);
        System.out.println();

        System.out.println("> SRAM settings");
        whichToGet = 1;
        interruptPinMode1 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        interruptPinMode2 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);

        //*** Set interrupt edge setting - initial values
        System.out.println();
        System.out.println("*** Set interrupt edge setting - initial values");
        interruptPinMode_temp = 0;
        //Flash settings
        whichToSet = 0;
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle1, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev1: " + result);
        }
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle2, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev2: " + result);
        }
        //SRAM settings
        whichToSet = 1;
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle1, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev1: " + result);
        }
        result = chip.Mcp2221_SetInterruptEdgeSetting(mcpHandle2, whichToSet, interruptPinMode_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set interrupt edge setting for dev2: " + result);
        }

        //*** Get interrupt edge setting
        System.out.println("> Flash settings");
        whichToGet = 0;
        interruptPinMode1 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        interruptPinMode2 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);
        System.out.println();

        System.out.println("> SRAM settings");
        whichToGet = 1;
        interruptPinMode1 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle1, whichToGet, interruptPinMode1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev1: " + result);
            interruptPinMode1[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev1: " + interruptPinMode1[0]);
        interruptPinMode2 = new byte[1];
        result = chip.Mcp2221_GetInterruptEdgeSetting(mcpHandle2, whichToGet, interruptPinMode2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt edge setting for dev2: " + result);
            interruptPinMode2[0] = -1;
        }
        System.out.println("Interrupt pin mode for dev2: " + interruptPinMode2[0]);

        System.out.println("=================================================");

        //*** Clear interrupt pin flag
        System.out.println("*** Clear interrupt pin flag");
        result = chip.Mcp2221_ClearInterruptPinFlag(mcpHandle1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Clear interrupt pin flag for dev1: " + result);
        }
        result = chip.Mcp2221_ClearInterruptPinFlag(mcpHandle2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Clear interrupt pin flag for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get interrupt pin flag
        System.out.println("*** Get interrupt pin flag");
        byte flagValue1[] = new byte[1];
        byte flagValue2[] = new byte[1];
        flagValue1 = new byte[1];
        result = chip.Mcp2221_GetInterruptPinFlag(mcpHandle1, flagValue1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt pin flag for dev1: " + result);
            flagValue1[0] = -1;
        }
        flagValue2 = new byte[1];
        result = chip.Mcp2221_GetInterruptPinFlag(mcpHandle2, flagValue2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get interrupt pin flag for dev2: " + result);
            flagValue2[0] = -1;
        }
        System.out.println("Interrupt pin flag for dev1: " + flagValue1[0]);
        System.out.println("Interrupt pin flag for dev2: " + flagValue2[0]);

        System.out.println("=================================================");

        //*** Get hardware revision
        System.out.println("*** Get hardware revision");
        String hwRevision1 = new String();
        String hwRevision2 = new String();
        hwRevision1 = chip.Mcp2221_GetHardwareRevision(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get hardware revision for dev1: " + result);
        }
        hwRevision2 = chip.Mcp2221_GetHardwareRevision(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get hardware revision for dev2: " + result);
        }

        System.out.println("Hardware revision for dev1: " + hwRevision1);
        System.out.println("Hardware revision for dev2: " + hwRevision2);

        System.out.println("=================================================");

        //*** Get firmware revision
        System.out.println("*** Get firmware revision");
        String fwRevision1 = new String();
        String fwRevision2 = new String();
        fwRevision1 = chip.Mcp2221_GetFirmwareRevision(mcpHandle1);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get firmware revision for dev1: " + result);
        }
        fwRevision2 = chip.Mcp2221_GetFirmwareRevision(mcpHandle2);
        result = chip.Mcp2221_GetLastError();
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get firmware revision for dev2: " + result);
        }

        System.out.println("Firmware revision for dev1: " + fwRevision1);
        System.out.println("Firmware revision for dev2: " + fwRevision2);

        System.out.println("=================================================");

        //*** Get clock settings
        System.out.println("*** Get clock settings");
        byte dutyCycle1[] = new byte[1];
        byte clockDivider1[] = new byte[1];
        byte dutyCycle2[] = new byte[1];
        byte clockDivider2[] = new byte[1];
        System.out.println("> Flash settings");
        whichToGet = 0;
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);
        System.out.println("> SRAM settings");
        whichToGet = 1;
        dutyCycle1 = new byte[1];
        clockDivider1 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        dutyCycle2 = new byte[1];
        clockDivider2 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);

        System.out.println("=================================================");

        //*** Set clock settings
        System.out.println("*** Set clock settings");
        byte clockDivider_temp = 6;
        byte dutyCycle_temp = 3;
        //Flash settings
        whichToSet = 0;
        result = chip.Mcp2221_SetClockSettings(mcpHandle1, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetClockSettings(mcpHandle2, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev2: " + result);
        }
        //SRAM settings
        whichToSet = 1;
        result = chip.Mcp2221_SetClockSettings(mcpHandle1, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetClockSettings(mcpHandle2, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev2: " + result);
        }

        //*** Get clock settings
        dutyCycle1 = new byte[1];
        clockDivider1 = new byte[1];
        dutyCycle2 = new byte[1];
        clockDivider2 = new byte[1];
        System.out.println("> Flash settings");
        whichToGet = 0;
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);
        System.out.println("> SRAM settings");
        whichToGet = 1;
        dutyCycle1 = new byte[1];
        clockDivider1 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        dutyCycle2 = new byte[1];
        clockDivider2 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);

        //*** Set clock settings - initial values
        System.out.println();
        System.out.println("*** Set clock settings - initial values");
        dutyCycle_temp = 0;
        clockDivider_temp = 1;
        //Flash settings
        whichToSet = 0;
        result = chip.Mcp2221_SetClockSettings(mcpHandle1, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetClockSettings(mcpHandle2, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev2: " + result);
        }
        //SRAM settings
        whichToSet = 1;
        result = chip.Mcp2221_SetClockSettings(mcpHandle1, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetClockSettings(mcpHandle2, whichToSet, dutyCycle_temp, clockDivider_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set clock settings for dev2: " + result);
        }

        //*** Get clock settings
        dutyCycle1 = new byte[1];
        clockDivider1 = new byte[1];
        dutyCycle2 = new byte[1];
        clockDivider2 = new byte[1];
        System.out.println("> Flash settings");
        whichToGet = 0;
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);
        System.out.println("> SRAM settings");
        whichToGet = 1;
        dutyCycle1 = new byte[1];
        clockDivider1 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle1, whichToGet, dutyCycle1, clockDivider1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev1: " + result);
            dutyCycle1[0] = -1;
            clockDivider1[0] = -1;
        }
        System.out.println("Duty cycle for dev1: " + dutyCycle1[0]);
        System.out.println("Clock divider for dev1: " + clockDivider1[0]);
        dutyCycle2 = new byte[1];
        clockDivider2 = new byte[1];
        result = chip.Mcp2221_GetClockSettings(mcpHandle2, whichToGet, dutyCycle2, clockDivider2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get clock settings for dev2: " + result);
            dutyCycle2[0] = -1;
            clockDivider2[0] = -1;
        }
        System.out.println("Duty cycle for dev2: " + dutyCycle2[0]);
        System.out.println("Clock divider for dev2: " + clockDivider2[0]);

        System.out.println("=================================================");

        //*** Get serial number enumeration enable
        System.out.println("*** Get serial number enumeration enable");
        byte snEnumEnabled1[] = new byte[1];
        byte snEnumEnabled2[] = new byte[1];
        snEnumEnabled1 = new byte[1];
        result = chip.Mcp2221_GetSerialNumberEnumerationEnable(mcpHandle1, snEnumEnabled1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number enumeration enable for dev1: " + result);
            snEnumEnabled1[0] = -1;
        }
        snEnumEnabled2 = new byte[1];
        result = chip.Mcp2221_GetSerialNumberEnumerationEnable(mcpHandle2, snEnumEnabled2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number enumeration enable for dev2: " + result);
            snEnumEnabled2[0] = -1;
        }
        System.out.println("Serial number enumeration enable for dev1: " + snEnumEnabled1[0]);
        System.out.println("Serial number enumeration enable for dev2: " + snEnumEnabled2[0]);

        System.out.println("=================================================");

        //*** Set serial number enumeration enable
        System.out.println("*** Set serial number enumeration enable");
        byte snEnumEnabled_temp = 1;
        result = chip.Mcp2221_SetSerialNumberEnumerationEnable(mcpHandle1, snEnumEnabled_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number enumeration enable for dev1: " + result);
        }
        result = chip.Mcp2221_SetSerialNumberEnumerationEnable(mcpHandle2, snEnumEnabled_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set serial number enumeration enable for dev2: " + result);
        }

        //*** Get serial number enumeration enable
        snEnumEnabled1 = new byte[1];
        result = chip.Mcp2221_GetSerialNumberEnumerationEnable(mcpHandle1, snEnumEnabled1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number enumeration enable for dev1: " + result);
            snEnumEnabled1[0] = -1;
        }
        System.out.println("Serial number enumeration for dev1: " + snEnumEnabled1[0]);
        snEnumEnabled2 = new byte[1];
        result = chip.Mcp2221_GetSerialNumberEnumerationEnable(mcpHandle2, snEnumEnabled2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get serial number enumeration enable for dev2: " + result);
            snEnumEnabled2[0] = -1;
        }
        System.out.println("Serial number enumeration for dev2: " + snEnumEnabled2[0]);

        System.out.println("=================================================");

        //*** Set GPIO settings 
        System.out.println("*** Set GPIO settings");
        byte pinFunctions[] = new byte[4];
        byte pinDirections[] = new byte[4];
        byte outputValues[] = new byte[4];
        for (int i = 0; i < 4; i++) {
            pinFunctions[i] = 0x00;
            pinDirections[i] = 0x00;
            outputValues[i] = 0x00;
        }
        //Flash settings
        whichToSet = 0;
        result = chip.Mcp2221_SetGpioSettings(mcpHandle1, whichToSet, pinFunctions, pinDirections, outputValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetGpioSettings(mcpHandle2, whichToSet, pinFunctions, pinDirections, outputValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO settings for dev2: " + result);
        }

        //SRAM settings
        whichToSet = 1;
        result = chip.Mcp2221_SetGpioSettings(mcpHandle1, whichToSet, pinFunctions, pinDirections, outputValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO settings for dev1: " + result);
        }
        result = chip.Mcp2221_SetGpioSettings(mcpHandle2, whichToSet, pinFunctions, pinDirections, outputValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO settings for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get GPIO settings
        System.out.println("*** Get GPIO settings");
        byte pinFunctions1[] = new byte[4];
        byte pinDirections1[] = new byte[4];
        byte outputValues1[] = new byte[4];
        byte pinFunctions2[] = new byte[4];
        byte pinDirections2[] = new byte[4];
        byte outputValues2[] = new byte[4];
        //Flash settings
        System.out.println("> Flash settings");
        whichToGet = 0;
        result = chip.Mcp2221_GetGpioSettings(mcpHandle1, whichToGet, pinFunctions1, pinDirections1, outputValues1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions1[0] = -1;
                pinDirections1[0] = -1;
                outputValues1[0] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV1: Pin " + i + " -> function: " + pinFunctions1[i]);
            System.out.println("DEV1: Pin " + i + " -> direction:" + pinDirections1[i]);
            System.out.println("DEV1: Pin " + i + " -> output value: " + outputValues1[i]);
        }
        result = chip.Mcp2221_GetGpioSettings(mcpHandle2, whichToGet, pinFunctions2, pinDirections2, outputValues2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions2[0] = -1;
                pinDirections2[0] = -1;
                outputValues2[0] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV2: Pin " + i + " -> function: " + pinFunctions2[i]);
            System.out.println("DEV2: Pin " + i + " -> direction:" + pinDirections2[i]);
            System.out.println("DEV2: Pin " + i + " -> output value: " + outputValues2[i]);
        }
        //SRAM settings
        System.out.println("> SRAM settings");
        whichToGet = 1;
        result = chip.Mcp2221_GetGpioSettings(mcpHandle1, whichToGet, pinFunctions1, pinDirections1, outputValues1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions1[0] = -1;
                pinDirections1[0] = -1;
                outputValues1[0] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV1: Pin " + i + " -> function: " + pinFunctions1[i]);
            System.out.println("DEV1: Pin " + i + " -> direction:" + pinDirections1[i]);
            System.out.println("DEV1: Pin " + i + " -> output value: " + outputValues1[i]);
        }
        result = chip.Mcp2221_GetGpioSettings(mcpHandle2, whichToGet, pinFunctions2, pinDirections2, outputValues2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions2[i] = -1;
                pinDirections2[i] = -1;
                outputValues2[i] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV2: Pin " + i + " -> function: " + pinFunctions2[i]);
            System.out.println("DEV2: Pin " + i + " -> direction:" + pinDirections2[i]);
            System.out.println("DEV2: Pin " + i + " -> output value: " + outputValues2[i]);
        }

        //*** Get GPIO settings 
        //Flash settings
        whichToGet = 0;
        pinFunctions1 = new byte[4];
        pinDirections1 = new byte[4];
        outputValues1 = new byte[4];
        result = chip.Mcp2221_GetGpioSettings(mcpHandle1, whichToGet, pinFunctions1, pinDirections1, outputValues1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions1[0] = -1;
                pinDirections1[0] = -1;
                outputValues1[0] = -1;
            }
        }
        pinFunctions2 = new byte[4];
        pinDirections2 = new byte[4];
        outputValues2 = new byte[4];
        result = chip.Mcp2221_GetGpioSettings(mcpHandle2, whichToGet, pinFunctions2, pinDirections2, outputValues2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions2[0] = -1;
                pinDirections2[0] = -1;
                outputValues2[0] = -1;
            }
        }

        //SRAM settings
        whichToGet = 1;
        pinFunctions1 = new byte[4];
        pinDirections1 = new byte[4];
        outputValues1 = new byte[4];
        result = chip.Mcp2221_GetGpioSettings(mcpHandle1, whichToGet, pinFunctions1, pinDirections1, outputValues1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions1[0] = -1;
                pinDirections1[0] = -1;
                outputValues1[0] = -1;
            }
        }
        pinFunctions2 = new byte[4];
        pinDirections2 = new byte[4];
        outputValues2 = new byte[4];
        result = chip.Mcp2221_GetGpioSettings(mcpHandle2, whichToGet, pinFunctions2, pinDirections2, outputValues2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO settings for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                pinFunctions2[0] = -1;
                pinDirections2[0] = -1;
                outputValues2[0] = -1;
            }
        }

        System.out.println("=================================================");

        //*** Set GPIO values
        byte gpioValues[] = new byte[4];
        gpioValues[0] = 0;
        gpioValues[1] = 1;
        gpioValues[2] = 0;
        gpioValues[3] = 1;
        result = chip.Mcp2221_SetGpioValues(mcpHandle1, gpioValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO values for dev1: " + result);
        }
        result = chip.Mcp2221_SetGpioValues(mcpHandle2, gpioValues);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO values for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get GPIO values
        byte gpioValues1[] = new byte[4];
        byte gpioValues2[] = new byte[4];
        byte gpioValues3[] = new byte[4];
        gpioValues1 = new byte[4];
        result = chip.Mcp2221_GetGpioValues(mcpHandle1, gpioValues1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! GPIO values for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                gpioValues1[i] = -1;
            }
        }
        gpioValues2 = new byte[4];
        result = chip.Mcp2221_GetGpioValues(mcpHandle2, gpioValues2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO values for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                gpioValues2[i] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV1: Pin " + i + " -> GPIO value: " + gpioValues1[i]);
            System.out.println("DEV2: Pin " + i + " -> GPIO value: " + gpioValues2[i]);
        }

        System.out.println("=================================================");

        //*** Set GPIO direction
        System.out.println("*** Set GPIO direction");
        byte gpioDir[] = new byte[4];
        for (int i = 0; i < 4; i++) {
            gpioDir[i] = 0;
        }
        result = chip.Mcp2221_SetGpioDirection(mcpHandle1, gpioDir);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO direction for dev1: " + result);
        }
        result = chip.Mcp2221_SetGpioDirection(mcpHandle2, gpioDir);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set GPIO direction for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get GPIO direction
        System.out.println("*** Get GPIO direction");
        byte gpioDir1[] = new byte[4];
        byte gpioDir2[] = new byte[4];
        gpioDir1 = new byte[4];
        result = chip.Mcp2221_GetGpioDirection(mcpHandle1, gpioDir1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO direction for dev1: " + result);
            for (int i = 0; i < 4; i++) {
                gpioDir1[i] = -1;
            }
        }
        gpioDir2 = new byte[4];
        result = chip.Mcp2221_GetGpioDirection(mcpHandle2, gpioDir2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get GPIO direction for dev2: " + result);
            for (int i = 0; i < 4; i++) {
                gpioDir2[i] = -1;
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("DEV1: Pin " + i + " -> direction: " + gpioDir1[i]);
            System.out.println("DEV2: Pin " + i + " -> direction: " + gpioDir2[i]);
        }

        System.out.println("=================================================");

        //*** Set DAC Vref
        System.out.println("*** Set DAC Vref");
        byte dacVref_temp;
        //Flash settings
        whichToSet = 0;
        dacVref_temp = 1;
        result = chip.Mcp2221_SetDacVref(mcpHandle1, whichToSet, dacVref_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC Vref for dev1: " + result);
        }
        dacVref_temp = 2;
        result = chip.Mcp2221_SetDacVref(mcpHandle2, whichToSet, dacVref_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC Vref for dev2: " + result);
        }

        System.out.println();
        //SRAM settings
        whichToSet = 1;
        dacVref_temp = 2;
        result = chip.Mcp2221_SetDacVref(mcpHandle1, whichToSet, dacVref_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC Vref for dev1: " + result);
        }
        dacVref_temp = 3;
        result = chip.Mcp2221_SetDacVref(mcpHandle2, whichToSet, dacVref_temp);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC Vref for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get DAC Vref
        System.out.println("*** Get DAC Vref");
        byte dacVref1[] = new byte[1];
        byte dacVref2[] = new byte[1];
        //Flash settings
        System.out.println("> Flash settings");
        whichToGet = 0;
        dacVref1 = new byte[1];
        result = chip.Mcp2221_GetDacVref(mcpHandle1, whichToGet, dacVref1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC Vref for dev1: " + result);
            dacVref1[0] = -1;
        }
        dacVref2 = new byte[1];
        result = chip.Mcp2221_GetDacVref(mcpHandle2, whichToGet, dacVref2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!!  Get DAC Vref for dev2: " + result);
            dacVref2[0] = -1;
        }
        System.out.println("DAC Vref for dev1: " + dacVref1[0]);
        System.out.println("DAC Vref for dev2: " + dacVref2[0]);

        System.out.println();
        //SRAM settings
        System.out.println("> SRAM settings");
        whichToGet = 1;
        dacVref1 = new byte[1];
        result = chip.Mcp2221_GetDacVref(mcpHandle1, whichToGet, dacVref1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC Vref for dev1: " + result);
            dacVref1[0] = -1;
        }
        dacVref2 = new byte[1];
        result = chip.Mcp2221_GetDacVref(mcpHandle2, whichToGet, dacVref2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC Vref for dev2: " + result);
            dacVref2[0] = -1;
        }
        System.out.println("DAC Vref for dev1: " + dacVref1[0]);
        System.out.println("DAC Vref for dev2: " + dacVref2[0]);

        System.out.println("=================================================");

        //*** Set DAC value
        System.out.println("*** Set DAC value");
        byte dacValue;
        //Flash settings
        whichToSet = 0;
        dacValue = 0;
        result = chip.Mcp2221_SetDacValue(mcpHandle1, whichToSet, dacValue);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC value for dev1: " + result);
        }
        dacValue = 31;
        result = chip.Mcp2221_SetDacValue(mcpHandle2, whichToSet, dacValue);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC value for dev2: " + result);
        }

        //SRAM settings
        whichToSet = 1;
        dacValue = 0;
        result = chip.Mcp2221_SetDacValue(mcpHandle1, whichToSet, dacValue);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC value for dev1: " + result);
        }
        dacValue = 31;
        result = chip.Mcp2221_SetDacValue(mcpHandle2, whichToSet, dacValue);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set DAC value for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get DAC value
        System.out.println("*** Get DAC value");
        byte dacValue1[] = new byte[1];
        byte dacValue2[] = new byte[1];
        //Flash settings
        System.out.println("> Flash settings");
        whichToGet = 0;
        dacValue1 = new byte[1];
        result = chip.Mcp2221_GetDacValue(mcpHandle1, whichToGet, dacValue1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC value for dev1: " + result);
            dacValue1[0] = -1;
        }
        dacValue2 = new byte[1];
        result = chip.Mcp2221_GetDacValue(mcpHandle2, whichToGet, dacValue2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC value for dev2: " + result);
            dacValue2[0] = -1;
        }
        System.out.println("DAC value for dev1: " + dacValue1[0]);
        System.out.println("DAC value for dev2: " + dacValue2[0]);

        System.out.println();
        //SRAM settings
        System.out.println("> SRAM settings");
        whichToGet = 1;
        dacValue1 = new byte[1];
        result = chip.Mcp2221_GetDacValue(mcpHandle1, whichToGet, dacValue1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC value for dev1: " + result);
            dacValue1[0] = -1;
        }
        dacValue2 = new byte[1];
        result = chip.Mcp2221_GetDacValue(mcpHandle2, whichToGet, dacValue2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get DAC value for dev2: " + result);
            dacValue2[0] = -1;
        }
        System.out.println("DAC value for dev1: " + dacValue1[0]);
        System.out.println("DAC value for dev2: " + dacValue2[0]);

        System.out.println("=================================================");

        //*** Set ADC Vref
        System.out.println("*** Set ADC Vref");
        byte adcVref;
        //Flash settings
        whichToSet = 0;
        adcVref = 0;
        result = chip.Mcp2221_SetAdcVref(mcpHandle1, whichToSet, adcVref);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set ADC Vref for dev1: " + result);
        }
        adcVref = 2;
        result = chip.Mcp2221_SetAdcVref(mcpHandle2, whichToSet, adcVref);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set ADC Vref for dev2: " + result);
        }

        //SRAM settings
        whichToSet = 1;
        adcVref = 3;
        result = chip.Mcp2221_SetAdcVref(mcpHandle1, whichToSet, adcVref);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set ADC Vref for dev1: " + result);
        }
        adcVref = 0;
        result = chip.Mcp2221_SetAdcVref(mcpHandle2, whichToSet, adcVref);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set ADC Vref for dev2: " + result);
        }

        System.out.println("=================================================");

        //*** Get ADC Vref
        System.out.println("*** Get ADC Vref");
        byte adcVref1[] = new byte[1];
        byte adcVref2[] = new byte[1];
        //Flash settings
        System.out.println("> Flash settings");
        whichToGet = 0;
        adcVref1 = new byte[1];
        result = chip.Mcp2221_GetAdcVref(mcpHandle1, whichToGet, adcVref1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get ADC Vref for dev1: " + result);
            adcVref1[0] = -1;
        }
        adcVref2 = new byte[1];
        result = chip.Mcp2221_GetAdcVref(mcpHandle2, whichToGet, adcVref2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get ADC Vref for dev2: " + result);
            adcVref2[0] = -1;
        }
        System.out.println("ADC Vref for dev1: " + adcVref1[0]);
        System.out.println("ADC Vref for dev2: " + adcVref2[0]);

        System.out.println();
        //SRAM settings
        System.out.println("> SRAM settings");
        whichToGet = 1;
        adcVref1 = new byte[1];
        result = chip.Mcp2221_GetAdcVref(mcpHandle1, whichToGet, adcVref1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get ADC Vref for dev1: " + result);
            adcVref1[0] = -1;
        }
        adcVref2 = new byte[1];
        result = chip.Mcp2221_GetAdcVref(mcpHandle2, whichToGet, adcVref2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get ADC Vref for dev2: " + result);
            adcVref2[0] = -1;
        }
        System.out.println("ADC Vref for dev1: " + adcVref1[0]);
        System.out.println("ADC Vref for dev2: " + adcVref2[0]);

        System.out.println("=================================================");

        //*** Get ADC data
        System.out.println("*** Get ADC data");
        int adcValue1[] = new int[3];
        int adcValue2[] = new int[3];
        adcValue1 = new int[3];
        result = chip.Mcp2221_GetAdcData(mcpHandle1, adcValue1);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Get ADC data for dev1: " + result);
            adcValue1[0] = -1;
            adcValue1[1] = -1;
            adcValue1[2] = -1;
        }
        adcValue2 = new int[3];
        result = chip.Mcp2221_GetAdcData(mcpHandle2, adcValue2);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!!  Get ADC data for dev2: " + result);
            adcValue2[0] = -1;
            adcValue2[1] = -1;
            adcValue2[2] = -1;
        }
        for (int i = 0; i < 3; i++) {
            System.out.println("DEV1: adcValue[" + i + "]: " + adcValue1[i]);
            System.out.println("DEV2: adcValue[" + i + "]: " + adcValue2[i]);
        }

        System.out.println("\t//********************************************************************//\n"
                + "        //************************** TRANSFER DATA ***************************//\n"
                + "        //********************************************************************//");

        result = chip.Mcp2221_CloseAll();
        //Open connection for DEV2 
        mcpHandle2 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN2);

        //*** Set advanced communication parameters
        System.out.println("*** Set advanced comm params");
        short timeout;
        short maxRetries;
        timeout = 0;
        maxRetries = 0;
        result = chip.Mcp2221_SetAdvancedCommParams(mcpHandle2, timeout, maxRetries);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! Set advanced comm params for dev2: " + result);
        }

        System.out.println("=================================================");

        result = chip.Mcp2221_CloseAll();

        mcpHandle2 = chip.Mcp2221_OpenBySN(DEFAULT_VID, DEFAULT_PID, mcpSN2);

        //*** I2C Read 5 bytes
        System.out.println("*** I2c Read");
        numberOfBytesToRead = 5;
        slaveAddress = (byte) 0xAF;
        use7bitAddress = false;
        result = chip.Mcp2221_I2cRead(mcpHandle2, numberOfBytesToRead, slaveAddress, use7bitAddress, dataToRead);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! I2c Read: " + result);
        } else {
            System.out.println("Successful I2c Read.");
        }

        //*** I2C Write 5 bytes
        System.out.println("*** I2c Write");
        numberOfBytesToWrite = 5;
        slaveAddress = (byte) 0xAF;
        dataToWrite = new byte[5];
        for (int i = 0; i < 5; i++) {
            dataToWrite[i] = 0x02;
        }
        use7bitAddress = false;
        result = chip.Mcp2221_I2cWrite(mcpHandle2, numberOfBytesToWrite, slaveAddress, use7bitAddress, dataToWrite);
        if (result != Constants.E_NO_ERR) {
            System.out.println("!!! I2c Write: " + result);
        } else {
            System.out.println("Successful I2c Write.");
        }

        //================ SMBUS OPERATION =====================//
        //IMPORTANT
        // for the following test to pass an aardvark traffic generator is connected to the MCP2221
        // the slave address is set to 0x39 (7 bit) and the response it sends is: 04 DA 00 01 02 1B
        result = chip.Mcp2221_CloseAll();
        //Device used for smbus operation: VID = 0x04D5; PID = 0x00DE, index = 0

        int custom_vid = 0x04D5;
        int custom_pid = 0x00DE;
        int index3 = 0;
        
        use7bitAddress = true;
        boolean usePec = false;
        byte smbCmd = (byte)0x9B;
        numberOfBytesToRead = 4;
        numberOfBytesToWrite = 4;
        byte smbus_slaveAddr = 0x39;
        
        
        long mcpHandle3 = chip.Mcp2221_OpenByIndex(custom_vid, custom_pid, index3);
        if (mcpHandle3 != -1) {
            
            //*** Smbus Block Read
            System.out.println("*** Smbus Block Read");
            dataToRead = new byte[numberOfBytesToRead];
            result = chip.Mcp2221_SmbusBlockRead(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, (byte)numberOfBytesToRead, dataToRead);
            if(result != Constants.E_NO_ERR){
                System.out.println("!!! Smbus Block Read: " + result);
            }
            
            //*** Smbus Block Write
            System.out.println("*** Smbus Block Write");
            dataToWrite = new byte[numberOfBytesToWrite];
            result = chip.Mcp2221_SmbusBlockWrite(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, (byte)numberOfBytesToWrite, dataToWrite);
            if(result != Constants.E_NO_ERR)
                System.out.println("!!! Smbus Block Write: " + result);
            
            //*** Smbus Read Word
            System.out.println("*** Smbus Read Word");
            dataToRead = new byte[2];
            result = chip.Mcp2221_SmbusReadWord(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, dataToRead);
            if(result != Constants.E_NO_ERR)
                System.out.println("!!! Smbus Read Word: " + result);
            
            //*** Smbus Write Word
            System.out.println("*** Smbus Write Word");
            dataToWrite = new byte[2];
            result = chip.Mcp2221_SmbusWriteWord(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, dataToWrite);
            if(result != Constants.E_NO_ERR)
                System.out.println("!!! Smbus Write Word: " + result);
            
            //*** Smbus Read Byte
            System.out.println("*** Smbus Read Byte");
            dataToRead = new byte[1];
            result = chip.Mcp2221_SmbusReadByte(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, dataToRead);
            if(result != Constants.E_NO_ERR)
                System.out.println("!!! Smbus Read Byte: " + result);
            
            //*** Smbus Write Byte
            System.out.println("*** Smbus Write Byte");
            dataToWrite[0] = 0x03;
            result = chip.Mcp2221_SmbusWriteByte(mcpHandle3, smbus_slaveAddr, use7bitAddress, usePec, smbCmd, dataToWrite[0]);
            if(result != Constants.E_NO_ERR)
                System.out.println("!!! Smbus Read Byte: " + result);
        } else {
            System.out.println("!!! Error to open a connection with custom device.");
        }

    }

}
