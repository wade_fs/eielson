//©[2016] Microchip Technology Inc.and its subsidiaries.You may use this software and any derivatives exclusively with Microchip products.
//
//THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
//WARRANTIES OF NON - INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY 
//OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
//IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER
//RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.TO THE FULLEST EXTENT ALLOWED
//BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
//DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
//MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

package gui;


import com.microchip.mcp2221.Constants;
import com.microchip.mcp2221.HidFeatures;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import javax.swing.ButtonGroup;
import javax.swing.SpringLayout;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import main.Device;
import main.Main;
import static main.Main.numberOfDevices;

public class MainForm extends javax.swing.JFrame {
    
    SpringLayout layout;
    ButtonGroup grp_dir_GP0;
    ButtonGroup grp_dir_GP1;
    ButtonGroup grp_dir_GP2;
    ButtonGroup grp_dir_GP3;
    ButtonGroup grp_val_GP0;
    ButtonGroup grp_val_GP1;
    ButtonGroup grp_val_GP2;
    ButtonGroup grp_val_GP3;
    HidFeatures chip;
    
    public static int vid = 0x4D8;
    public static int pid = 0xDD;
    
    public static ArrayList<Device> listConnDev;
    public static ArrayList<Device> listOpenedDev;
    private int result = -1;
    
    //Default speed
    public static final int DEFAULT_SPEED = 100000;
    public static boolean flag_closeAllConnections = false;
    
    public MainForm() {
        initComponents();
        
        //change default icon 
        ImageIcon icon = new ImageIcon("mchp.png");
        this.setIconImage(icon.getImage());
        
        //set initial text for Status Label
        jLabel_currentSts.setText("-");
        
        //make disable selected device cmb and operation button if no device can be selected
        if(jCmb_selectDev.getItemCount() <= 0)
        {
            jCmb_selectDev.setEnabled(false);
            jBtn_operation.setEnabled(false);
        }
            
        
        
        layout = new SpringLayout();
        chip = new HidFeatures();  
        
        listOpenedDev = new ArrayList<>();
        
        jTxt_VID.setText("4D8");
        jTxt_PID.setText("DD");

        //Create new Button Group
        grp_dir_GP0 = new ButtonGroup();
        grp_dir_GP1 = new ButtonGroup();
        grp_dir_GP2 = new ButtonGroup();
        grp_dir_GP3 = new ButtonGroup();
        
        grp_val_GP0 = new ButtonGroup();
        grp_val_GP1 = new ButtonGroup();
        grp_val_GP2 = new ButtonGroup();
        grp_val_GP3 = new ButtonGroup();
        
        //Set size for Operation panel
        jPanel_operations.setPreferredSize(new Dimension(650, 315));
        jPanel_operations.setMinimumSize(new Dimension(650, 315));
        jPanel_operations.setMaximumSize(new Dimension(650, 315));
        
        //remove all components from Operation panel
        jPanel_operations.removeAll();
        
        //set SpringLayout for jPanel_operations
        jPanel_operations.setLayout(layout);
        
        //repaint all components and put layout constraints for each of them
        
        //*** Select device
        jPanel_operations.add(jLabel_selectDev);  
        jPanel_operations.add(jCmb_selectDev);
        layout.putConstraint(SpringLayout.WEST, jLabel_selectDev,
                    100,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_selectDev,
                    5,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_selectDev.setLocation(110, 10);
        layout.putConstraint(SpringLayout.WEST, jCmb_selectDev, 
                    200,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_selectDev,
                    0,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_selectDev.setLocation(210, 10);
        
        jCmb_selectDev.setPreferredSize(new Dimension(150, 25));
        jCmb_selectDev.setMinimumSize(new Dimension(150, 25));
        
        
        //*** Protocol
        jPanel_operations.add(jLabel_protocol);
        jPanel_operations.add(jCmb_protocol);
        layout.putConstraint(SpringLayout.WEST, jLabel_protocol,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_protocol,
                    80,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_protocol.setLocation(20, 90);
        layout.putConstraint(SpringLayout.WEST, jCmb_protocol,
                    70, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_protocol,
                    75,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_protocol.setLocation(80, 85);
        
        jCmb_protocol.setPreferredSize(new Dimension(110, 25));
        jCmb_protocol.setMinimumSize(new Dimension(110, 25));
        
        
        //*** Address Length
        jPanel_operations.add(jLabel_addressLen);
        jPanel_operations.add(jCmb_addressLen);
        layout.putConstraint(SpringLayout.WEST, jLabel_addressLen,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_addressLen,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_addressLen.setLocation(20, 140);
        layout.putConstraint(SpringLayout.WEST, jCmb_addressLen,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_addressLen,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_addressLen.setLocation(120, 135);
        
        jCmb_addressLen.setPreferredSize(new Dimension(110, 25));
        jCmb_addressLen.setMinimumSize(new Dimension(110, 25));
        
        
        //*** Address
        jPanel_operations.add(jLabel_address);
        jPanel_operations.add(jTxt_address);
        layout.putConstraint(SpringLayout.WEST, jLabel_address,
                    300, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_address,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_address.setLocation(310,140);
        layout.putConstraint(SpringLayout.WEST, jTxt_address,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_address,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_address.setLocation(410, 135);
        
        jTxt_address.setPreferredSize(new Dimension(110, 25));
        jTxt_address.setMinimumSize(new Dimension(110, 25));
        
        
        //*** Speed
        jPanel_operations.add(jLabel_speed);
        jPanel_operations.add(jTxt_speed);
        layout.putConstraint(SpringLayout.WEST, jLabel_speed,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_speed,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_speed.setLocation(20, 180);
        layout.putConstraint(SpringLayout.WEST, jTxt_speed,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_speed,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_speed.setLocation(120, 175);
        
        jTxt_speed.setPreferredSize(new Dimension(110, 25));
        jTxt_speed.setMinimumSize(new Dimension(110, 25));
        
        
        //*** Operation
        jPanel_operations.add(jLabel_operation);
        jPanel_operations.add(jCmb_operation);
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    300, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(310, 180);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_operation.setLocation(410, 175);
        
        jCmb_operation.setPreferredSize(new Dimension(110, 25));
        jCmb_operation.setMinimumSize(new Dimension(110, 25));
        
        
        //*** Data
        jPanel_operations.add(jLabel_data);
        jPanel_operations.add(jTxt_data);
        layout.putConstraint(SpringLayout.WEST, jLabel_data,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_data,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_data.setLocation(20, 220);
        layout.putConstraint(SpringLayout.WEST, jTxt_data,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_data,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_data.setLocation(120, 215);
        
        jTxt_data.setPreferredSize(new Dimension(515, 25));
        jTxt_data.setMinimumSize(new Dimension(515, 25));  
        
        
        //*** Operation Button
        jPanel_operations.add(jBtn_operation);
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                465,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation, 
                245,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setLocation(475, 255);
        jBtn_operation.setPreferredSize(new Dimension(160, 25));
        jBtn_operation.setMinimumSize(new Dimension(160, 25));
        jBtn_operation.setText("I2C Write");
        
        repaint();
        
        //Load DLL
        result = chip.Mcp2221_LoadDll();
        if(result != 0){
            JOptionPane.showMessageDialog(null, "MCP2221 Native DLL  could not be load");
        }else{
            getInitialDev();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_devices = new javax.swing.JPanel();
        jCmb_listDev = new javax.swing.JComboBox();
        jBtn_openConn = new javax.swing.JButton();
        jLabel_listDev = new javax.swing.JLabel();
        jLabel_openedConn = new javax.swing.JLabel();
        jBtn_close = new javax.swing.JButton();
        jBtn_closeAll = new javax.swing.JButton();
        jLabel_VID = new javax.swing.JLabel();
        jLabel_PID = new javax.swing.JLabel();
        jTxt_PID = new javax.swing.JTextField();
        jTxt_VID = new javax.swing.JTextField();
        list_connDev = new java.awt.List();
        jPanel_operations = new javax.swing.JPanel();
        jLabel_selectDev = new javax.swing.JLabel();
        jLabel_protocol = new javax.swing.JLabel();
        jLabel_addressLen = new javax.swing.JLabel();
        jCmb_addressLen = new javax.swing.JComboBox();
        jLabel_address = new javax.swing.JLabel();
        jTxt_address = new javax.swing.JTextField();
        jLabel_operation = new javax.swing.JLabel();
        jCmb_operation = new javax.swing.JComboBox();
        jLabel_data = new javax.swing.JLabel();
        jTxt_data = new javax.swing.JTextField();
        jLabel_speed = new javax.swing.JLabel();
        jCmb_protocol = new javax.swing.JComboBox();
        jTxt_speed = new javax.swing.JTextField();
        jCmb_selectDev = new javax.swing.JComboBox();
        jLabel_pec = new javax.swing.JLabel();
        jCmb_pec = new javax.swing.JComboBox();
        jLabel_cmd = new javax.swing.JLabel();
        jTxt_cmd = new javax.swing.JTextField();
        jLabel_GP0 = new javax.swing.JLabel();
        jLabel_GP1 = new javax.swing.JLabel();
        jCmb_GP0 = new javax.swing.JComboBox();
        jCmb_GP1 = new javax.swing.JComboBox();
        jLabel_GP2 = new javax.swing.JLabel();
        jLabel_GP3 = new javax.swing.JLabel();
        jCmb_GP2 = new javax.swing.JComboBox();
        jCmb_GP3 = new javax.swing.JComboBox();
        jBtn_GP0_out = new javax.swing.JRadioButton();
        jBtn_GP0_in = new javax.swing.JRadioButton();
        jBtn_GP1_out = new javax.swing.JRadioButton();
        jBtn_GP1_in = new javax.swing.JRadioButton();
        jBtn_GP2_out = new javax.swing.JRadioButton();
        jBtn_GP2_in = new javax.swing.JRadioButton();
        jBtn_GP3_out = new javax.swing.JRadioButton();
        jBtn_GP3_in = new javax.swing.JRadioButton();
        jLabel_vref = new javax.swing.JLabel();
        jCmb_vref = new javax.swing.JComboBox();
        jTxt_dac = new javax.swing.JTextField();
        jCheckBox_GP1 = new javax.swing.JCheckBox();
        jCheckBox_GP2 = new javax.swing.JCheckBox();
        jCheckBox_GP3 = new javax.swing.JCheckBox();
        jCheckBox_GP0 = new javax.swing.JCheckBox();
        jBtn_operation = new javax.swing.JButton();
        jCmb_GP0_val = new javax.swing.JComboBox();
        jCmb_GP1_val = new javax.swing.JComboBox();
        jCmb_GP2_val = new javax.swing.JComboBox();
        jCmb_GP3_val = new javax.swing.JComboBox();
        jPanel_result = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTxt_result = new javax.swing.JTextArea();
        jBtn_clear = new javax.swing.JButton();
        jLabel_sts = new javax.swing.JLabel();
        jLabel_currentSts = new javax.swing.JLabel();
        jLabel_noDev = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem_exit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setForeground(new java.awt.Color(255, 0, 102));
        setResizable(false);

        jPanel_devices.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel_devices.setForeground(new java.awt.Color(153, 153, 153));
        jPanel_devices.setName("jPanel_devices"); // NOI18N

        jCmb_listDev.setName(""); // NOI18N

        jBtn_openConn.setText("Open connection");
        jBtn_openConn.setName("jBtn_openConn"); // NOI18N
        jBtn_openConn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_openConnActionPerformed(evt);
            }
        });

        jLabel_listDev.setText("List of Devices");
        jLabel_listDev.setName(""); // NOI18N

        jLabel_openedConn.setText("Opened connections");
        jLabel_openedConn.setName("jLabel_openedConn"); // NOI18N

        jBtn_close.setText("Close");
        jBtn_close.setName("jBtn_close"); // NOI18N
        jBtn_close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_closeActionPerformed(evt);
            }
        });

        jBtn_closeAll.setText("Close All");
        jBtn_closeAll.setName("jBtn_closeAll"); // NOI18N
        jBtn_closeAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_closeAllActionPerformed(evt);
            }
        });

        jLabel_VID.setText("VID:");
        jLabel_VID.setName(""); // NOI18N

        jLabel_PID.setText("PID:");
        jLabel_PID.setName("jLabel_PID"); // NOI18N

        jTxt_PID.setName(""); // NOI18N
        jTxt_PID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxt_PIDKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_PIDKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_PIDKeyTyped(evt);
            }
        });

        jTxt_VID.setName(""); // NOI18N
        jTxt_VID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxt_VIDKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_VIDKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_VIDKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel_devicesLayout = new javax.swing.GroupLayout(jPanel_devices);
        jPanel_devices.setLayout(jPanel_devicesLayout);
        jPanel_devicesLayout.setHorizontalGroup(
            jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_devicesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(list_connDev, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel_devicesLayout.createSequentialGroup()
                        .addComponent(jLabel_VID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxt_VID, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                        .addComponent(jLabel_PID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxt_PID, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_devicesLayout.createSequentialGroup()
                        .addGroup(jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCmb_listDev, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_listDev)
                            .addComponent(jLabel_openedConn))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_devicesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jBtn_openConn))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_devicesLayout.createSequentialGroup()
                        .addComponent(jBtn_close, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBtn_closeAll)))
                .addContainerGap())
        );
        jPanel_devicesLayout.setVerticalGroup(
            jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_devicesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_VID)
                    .addComponent(jLabel_PID)
                    .addComponent(jTxt_PID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxt_VID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel_listDev)
                .addGap(3, 3, 3)
                .addComponent(jCmb_listDev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtn_openConn)
                .addGap(24, 24, 24)
                .addComponent(jLabel_openedConn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(list_connDev, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_devicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtn_closeAll)
                    .addComponent(jBtn_close)))
        );

        jLabel_PID.getAccessibleContext().setAccessibleName("");

        jPanel_operations.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel_operations.setName("jPanel_operations"); // NOI18N

        jLabel_selectDev.setText("Selected device: ");
        jLabel_selectDev.setName("jLabel_selectDev"); // NOI18N

        jLabel_protocol.setText("Protocol:");
        jLabel_protocol.setName("jLabel_protocol"); // NOI18N

        jLabel_addressLen.setText("Address Length:");
        jLabel_addressLen.setName("jLabel_addressLen"); // NOI18N

        jCmb_addressLen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "7 bit", "8 bit" }));
        jCmb_addressLen.setName("jCmb_addressLen"); // NOI18N

        jLabel_address.setText("Address (hex):");
        jLabel_address.setName("jLabel_address"); // NOI18N

        jTxt_address.setName("jTxt_address"); // NOI18N
        jTxt_address.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_addressKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_addressKeyTyped(evt);
            }
        });

        jLabel_operation.setText("Operation:");
        jLabel_operation.setName("jLabel_operation"); // NOI18N

        jCmb_operation.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Write", "Read" }));
        jCmb_operation.setAlignmentX(2.0F);
        jCmb_operation.setName("jCmb_operation"); // NOI18N
        jCmb_operation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_operationActionPerformed(evt);
            }
        });

        jLabel_data.setText("Data (hex):");
        jLabel_data.setName("jLabel_data"); // NOI18N

        jTxt_data.setName("jTxt_data"); // NOI18N
        jTxt_data.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxt_dataFocusLost(evt);
            }
        });
        jTxt_data.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_dataKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_dataKeyTyped(evt);
            }
        });

        jLabel_speed.setText("Speed  (bps):");
        jLabel_speed.setName("jLabel_speed"); // NOI18N

        jCmb_protocol.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I2C", "SMBUS", "CONFIGURE", "GPIO", "DAC", "ADC" }));
        jCmb_protocol.setName("jCmb_protocol"); // NOI18N
        jCmb_protocol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_protocolActionPerformed(evt);
            }
        });

        jTxt_speed.setName("jTxt_speed"); // NOI18N
        jTxt_speed.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxt_speedFocusLost(evt);
            }
        });
        jTxt_speed.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_speedKeyTyped(evt);
            }
        });

        jCmb_selectDev.setName("jCmb_selectDev"); // NOI18N

        jLabel_pec.setText("PEC:");

        jCmb_pec.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ON", "OFF" }));
        jCmb_pec.setMinimumSize(new java.awt.Dimension(110, 25));
        jCmb_pec.setPreferredSize(new java.awt.Dimension(110, 25));

        jLabel_cmd.setText("Command (hex):");

        jTxt_cmd.setMinimumSize(new java.awt.Dimension(110, 25));
        jTxt_cmd.setPreferredSize(new java.awt.Dimension(110, 25));
        jTxt_cmd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_cmdKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_cmdKeyTyped(evt);
            }
        });

        jLabel_GP0.setText("GP0:");

        jLabel_GP1.setText("GP1:");

        jCmb_GP0.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Output", "Input", "SSPND", "LED UART RX" }));
        jCmb_GP0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_GP0ActionPerformed(evt);
            }
        });

        jCmb_GP1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Output", "Input", "Clock Out", "ADC1", "LED UART TX", "Interrupt detection" }));
        jCmb_GP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_GP1ActionPerformed(evt);
            }
        });

        jLabel_GP2.setText("GP2:");

        jLabel_GP3.setText("GP3:");

        jCmb_GP2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Output", "Input", "USBCFG", "ADC2", "DAC1" }));
        jCmb_GP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_GP2ActionPerformed(evt);
            }
        });

        jCmb_GP3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Output", "Input", "LED I2C", "ADC3", "DAC2" }));
        jCmb_GP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCmb_GP3ActionPerformed(evt);
            }
        });

        jBtn_GP0_out.setText("Output");
        jBtn_GP0_out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP0_outActionPerformed(evt);
            }
        });

        jBtn_GP0_in.setText("Input");
        jBtn_GP0_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP0_inActionPerformed(evt);
            }
        });

        jBtn_GP1_out.setText("Output");
        jBtn_GP1_out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP1_outActionPerformed(evt);
            }
        });

        jBtn_GP1_in.setText("Input");
        jBtn_GP1_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP1_inActionPerformed(evt);
            }
        });

        jBtn_GP2_out.setText("Output");
        jBtn_GP2_out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP2_outActionPerformed(evt);
            }
        });

        jBtn_GP2_in.setText("Input");
        jBtn_GP2_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP2_inActionPerformed(evt);
            }
        });

        jBtn_GP3_out.setText("Output");
        jBtn_GP3_out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP3_outActionPerformed(evt);
            }
        });

        jBtn_GP3_in.setText("Input");
        jBtn_GP3_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_GP3_inActionPerformed(evt);
            }
        });

        jLabel_vref.setText("Voltage Reference:");

        jCmb_vref.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vdd", "1.024 V", "2.048 V", "4.096 V" }));

        jTxt_dac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxt_dacKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxt_dacKeyTyped(evt);
            }
        });

        jCheckBox_GP1.setText("GP1");
        jCheckBox_GP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_GP1ActionPerformed(evt);
            }
        });

        jCheckBox_GP2.setText("GP2");
        jCheckBox_GP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_GP2ActionPerformed(evt);
            }
        });

        jCheckBox_GP3.setText("GP3");
        jCheckBox_GP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_GP3ActionPerformed(evt);
            }
        });

        jCheckBox_GP0.setText("GP0");
        jCheckBox_GP0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_GP0ActionPerformed(evt);
            }
        });

        jBtn_operation.setText("Send");
        jBtn_operation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_operationActionPerformed(evt);
            }
        });

        jCmb_GP0_val.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Change", "Low", "High" }));

        jCmb_GP1_val.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Change", "Low", "High" }));

        jCmb_GP2_val.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Change", "Low", "High" }));

        jCmb_GP3_val.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Change", "Low", "High" }));

        javax.swing.GroupLayout jPanel_operationsLayout = new javax.swing.GroupLayout(jPanel_operations);
        jPanel_operations.setLayout(jPanel_operationsLayout);
        jPanel_operationsLayout.setHorizontalGroup(
            jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel_operationsLayout.createSequentialGroup()
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_addressLen)
                            .addComponent(jLabel_speed)
                            .addComponent(jLabel_data))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addComponent(jTxt_data, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jBtn_operation))
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel_vref)
                                            .addComponent(jCmb_addressLen, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jCmb_vref, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTxt_dac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jCmb_GP1_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                            .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jTxt_speed)
                                                .addComponent(jCmb_pec, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(80, 80, 80)
                                            .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                    .addComponent(jLabel_operation)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jCmb_operation, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                    .addComponent(jLabel_cmd)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jTxt_cmd, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                    .addComponent(jLabel_address)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(jTxt_address, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(jCmb_GP3_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jLabel_pec, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addComponent(jLabel_selectDev)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCmb_selectDev, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(70, 70, 70))
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_operationsLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel_GP0))
                                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel_protocol, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jCmb_protocol, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(29, 29, 29)
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jCheckBox_GP0)
                                            .addComponent(jCheckBox_GP2))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jCheckBox_GP1)
                                            .addComponent(jCheckBox_GP3))
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                .addGap(45, 45, 45)
                                                .addComponent(jLabel_GP1))
                                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                .addGap(54, 54, 54)
                                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jCmb_GP2_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jCmb_GP0_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCmb_GP0, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCmb_GP1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addComponent(jLabel_GP3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCmb_GP3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                        .addComponent(jLabel_GP2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jCmb_GP2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                                .addComponent(jBtn_GP0_in)
                                                .addGap(10, 10, 10))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_operationsLayout.createSequentialGroup()
                                                .addComponent(jBtn_GP0_out)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jBtn_GP1_in)
                                            .addComponent(jBtn_GP1_out)))
                                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jBtn_GP2_out)
                                            .addComponent(jBtn_GP2_in))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jBtn_GP3_in)
                                            .addComponent(jBtn_GP3_out))))
                                .addGap(0, 0, Short.MAX_VALUE))))))
        );
        jPanel_operationsLayout.setVerticalGroup(
            jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_selectDev)
                            .addComponent(jCmb_selectDev, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_vref)
                            .addComponent(jCmb_vref, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTxt_dac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel_protocol)
                                .addGap(7, 7, 7)
                                .addComponent(jCmb_protocol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_operationsLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jCheckBox_GP1)
                                    .addComponent(jCheckBox_GP0)
                                    .addComponent(jCmb_GP0_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCmb_GP1_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jCheckBox_GP2)
                                    .addComponent(jCheckBox_GP3)
                                    .addComponent(jCmb_GP2_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCmb_GP3_val, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCmb_addressLen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_addressLen)
                            .addComponent(jTxt_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_address))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_speed)
                            .addComponent(jTxt_speed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_operation)
                            .addComponent(jCmb_operation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_pec)
                            .addComponent(jCmb_pec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_cmd)
                            .addComponent(jTxt_cmd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel_operationsLayout.createSequentialGroup()
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCmb_GP0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_GP0)
                            .addComponent(jLabel_GP2)
                            .addComponent(jCmb_GP2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCmb_GP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_GP1)
                            .addComponent(jLabel_GP3)
                            .addComponent(jCmb_GP3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBtn_GP0_out)
                            .addComponent(jBtn_GP1_out))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtn_GP0_in)
                            .addComponent(jBtn_GP1_in))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtn_GP2_out)
                            .addComponent(jBtn_GP3_out))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtn_GP2_in)
                            .addComponent(jBtn_GP3_in))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtn_operation)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel_operationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxt_data, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_data))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel_result.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jTxt_result.setEditable(false);
        jTxt_result.setColumns(20);
        jTxt_result.setRows(5);
        jScrollPane1.setViewportView(jTxt_result);

        jBtn_clear.setText("Clear");
        jBtn_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn_clearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel_resultLayout = new javax.swing.GroupLayout(jPanel_result);
        jPanel_result.setLayout(jPanel_resultLayout);
        jPanel_resultLayout.setHorizontalGroup(
            jPanel_resultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_resultLayout.createSequentialGroup()
                .addGroup(jPanel_resultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBtn_clear)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 676, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel_resultLayout.setVerticalGroup(
            jPanel_resultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_resultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtn_clear))
        );

        jLabel_sts.setText("Status:");

        jMenuBar1.setName("jMenuBar"); // NOI18N

        jMenu1.setText("File");
        jMenu1.setName("jMenu_file"); // NOI18N

        jMenuItem_exit.setText("Exit");
        jMenuItem_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_exitActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_exit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel_operations, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel_result, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel_devices, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel_noDev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel_sts)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel_currentSts)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel_devices, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel_operations, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel_result, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_sts)
                    .addComponent(jLabel_currentSts)
                    .addComponent(jLabel_noDev))
                .addGap(5, 5, 5))
        );

        jPanel_operations.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCmb_protocolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_protocolActionPerformed
        
        //Get selected item from Protocol ComboBox
        Object selectedItem = jCmb_protocol.getSelectedItem();
        
        switch(selectedItem.toString()){
            case "I2C":
                createLayoutI2C();
                jLabel_currentSts.setText("");
                jTxt_address.setText("");
                jTxt_speed.setText("");
                jTxt_data.setText("");
                break;
            case "SMBUS":
                createLayoutSMBUS();
                jLabel_currentSts.setText("");
                jTxt_address.setText("");
                jTxt_speed.setText("");
                jTxt_cmd.setText("");
                jTxt_data.setText("");
                break;
            case "CONFIGURE":
                jLabel_currentSts.setText("");
                createLayoutCONFIGURE();
                break;
            case "GPIO":
                jLabel_currentSts.setText("");
                createLayoutGPIO();
                break;
            case "DAC":
                jLabel_currentSts.setText("");
                createLayoutDAC();
                jTxt_dac.setText("");
                break;
            case "ADC":
                jLabel_currentSts.setText("");
                createLayoutADC();
                break;
        }
    }//GEN-LAST:event_jCmb_protocolActionPerformed

    private void jCmb_operationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_operationActionPerformed
        //Get selected item from Protocol ComboBox
        Object selectedItem = jCmb_operation.getSelectedItem();
            if(selectedItem != null){
                switch(selectedItem.toString()){
                case "Read":
                    jLabel_data.setText("Number of Bytes: ");
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jTxt_data.setPreferredSize(new Dimension(110, 25));
                    jTxt_data.setMinimumSize(new Dimension(110, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("I2C Read");
                    jLabel_currentSts.setText("");
                    break;
                case "Write":
                    jLabel_data.setText("Data (hex): ");
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jTxt_data.setPreferredSize(new Dimension(515, 25));
                    jTxt_data.setMinimumSize(new Dimension(515, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("I2C Write");
                    jLabel_currentSts.setText("");
                    break;
                case "Write Byte":
                    jLabel_data.setText("Data (hex): ");
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jTxt_data.setPreferredSize(new Dimension(515, 25));
                    jTxt_data.setMinimumSize(new Dimension(515, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("SMBUS Write Byte");
                    jLabel_currentSts.setText("");
                    break;
                case "Read Byte":
                    jLabel_data.setVisible(false);
                    jTxt_data.setVisible(false);
                    jBtn_operation.setText("SMBUS Read Byte");
                    jLabel_currentSts.setText("");
                    break;
                case "Write Word":
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jLabel_data.setText("Data (hex): ");
                    jTxt_data.setPreferredSize(new Dimension(515, 25));
                    jTxt_data.setMinimumSize(new Dimension(515, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("SMBUS Write Word");
                    jLabel_currentSts.setText("");
                    break;
                case "Read Word":
                    jLabel_data.setVisible(false);
                    jTxt_data.setVisible(false);
                    jBtn_operation.setText("SMBUS Read Word");
                    jLabel_currentSts.setText("");
                    break;
                case "Block Write":
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jLabel_data.setText("Data (hex): ");
                    jTxt_data.setPreferredSize(new Dimension(515, 25));
                    jTxt_data.setMinimumSize(new Dimension(515, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("SMBUS Block Write");
                    jLabel_currentSts.setText("");
                    break;
                case "Block Read":
                    jLabel_data.setVisible(true);
                    jTxt_data.setVisible(true);
                    jLabel_data.setText("Number of Bytes: ");
                    jTxt_data.setPreferredSize(new Dimension(110, 25));
                    jTxt_data.setMinimumSize(new Dimension(110, 25));
                    jTxt_data.setText("");
                    jBtn_operation.setText("SMBUS Block Read");
                    jLabel_currentSts.setText("");
                    break;
                case "Set":
                    // 'Set' item is used for 'CONFIGURE' and 'GPIO' protocols
                    if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                        createLayoutCONFIGURE_Set();
                        jLabel_currentSts.setText("");
                    }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                        createLayoutGPIO_Set();
                        jLabel_currentSts.setText("");
                    }
                    break;
                case "Get":
                    // 'Get' item is used for 'CONFIGURE' and 'GPIO' protocols
                    if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                        createLayoutCONFIGURE_Get();
                        jLabel_currentSts.setText("");
                    }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                        createLayoutGPIO_Get();
                        jLabel_currentSts.setText("");
                    }
                    break;
                case "Get Vref":
                    createLayout_GetVref();
                    jLabel_currentSts.setText("");
                    break;
                case "Set Vref":
                    createLayout_SetVref();
                    jLabel_currentSts.setText("");
                    break;
                case "Set Value":
                    createLayoutDAC_Set();
                    jLabel_currentSts.setText("");
                    break;
                case "Get Value":
                    createLayoutDAC_Get();
                    jLabel_currentSts.setText("");
                    break;
                case "Get Data":
                    // 'Get Data' item is used for 'ADC' protocol
                    createLayoutADC_GetData();
                    jLabel_currentSts.setText("");
                    break;
            }
        }
        
    }//GEN-LAST:event_jCmb_operationActionPerformed

    
    //***** GP0 Button Group
    private void jBtn_GP0_outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP0_outActionPerformed
        if(jCmb_protocol.getSelectedItem().equals("GPIO")){
            jCmb_GP0_val.setVisible(true);
            jPanel_operations.add(jCmb_GP0_val);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP0_val, 
                        120, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP0_val, 
                        160, 
                        SpringLayout.NORTH, jPanel_operations);
            jCmb_GP0_val.setLocation(130, 170);

            //remove items from GP0 ComboBox and add values for GPIO
            jCmb_GP0_val.removeAllItems();
            jCmb_GP0_val.addItem("No Change");
            jCmb_GP0_val.addItem("High");
            jCmb_GP0_val.addItem("Low");

            repaint();
        }
    }//GEN-LAST:event_jBtn_GP0_outActionPerformed

    private void jBtn_GP0_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP0_inActionPerformed
        jCmb_GP0_val.setVisible(false);
    }//GEN-LAST:event_jBtn_GP0_inActionPerformed

    
    //***** GP1 Button Group
    private void jBtn_GP1_outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP1_outActionPerformed
        if(jCmb_protocol.getSelectedItem().equals("GPIO")){
            jCmb_GP1_val.setVisible(true);
            jPanel_operations.add(jCmb_GP1_val);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP1_val, 
                        420, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP1_val, 
                        160, 
                        SpringLayout.NORTH, jPanel_operations);
            jCmb_GP1_val.setLocation(430, 170);

            //remove items from GP1 ComboBox and add values for GPIO
            jCmb_GP1_val.removeAllItems();
            jCmb_GP1_val.addItem("No Change");
            jCmb_GP1_val.addItem("High");
            jCmb_GP1_val.addItem("Low");

            repaint();
        }   
    }//GEN-LAST:event_jBtn_GP1_outActionPerformed

    private void jBtn_GP1_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP1_inActionPerformed
        jCmb_GP1_val.setVisible(false);
    }//GEN-LAST:event_jBtn_GP1_inActionPerformed

    
    //***** GP2 Button Group
    private void jBtn_GP2_outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP2_outActionPerformed
        if(jCmb_protocol.getSelectedItem().equals("GPIO")){
            jCmb_GP2_val.setVisible(true);
            jPanel_operations.add(jCmb_GP2_val);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP2_val, 
                        120, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP2_val, 
                        230, 
                        SpringLayout.NORTH, jPanel_operations);
            jCmb_GP2_val.setLocation(130, 240);

            //remove items from GP2 ComboBox and add values for GPIO
            jCmb_GP2_val.removeAllItems();
            jCmb_GP2_val.addItem("No Change");
            jCmb_GP2_val.addItem("High");
            jCmb_GP2_val.addItem("Low");

            repaint();
        }  
    }//GEN-LAST:event_jBtn_GP2_outActionPerformed

    private void jBtn_GP2_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP2_inActionPerformed
        jCmb_GP2_val.setVisible(false);
    }//GEN-LAST:event_jBtn_GP2_inActionPerformed

    
    //***** GP3 Button Group
    private void jBtn_GP3_outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP3_outActionPerformed
        if(jCmb_protocol.getSelectedItem().equals("GPIO")){
            jCmb_GP3_val.setVisible(true);
            jPanel_operations.add(jCmb_GP3_val);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP3_val, 
                        420, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP3_val, 
                        230, 
                        SpringLayout.NORTH, jPanel_operations);
            jCmb_GP3_val.setLocation(430, 240);

            //remove items from GP3 ComboBox and add values for GPIO
            jCmb_GP3_val.removeAllItems();
            jCmb_GP3_val.addItem("No Change");
            jCmb_GP3_val.addItem("High");
            jCmb_GP3_val.addItem("Low");

            repaint();
        }
    }//GEN-LAST:event_jBtn_GP3_outActionPerformed

    private void jBtn_GP3_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_GP3_inActionPerformed
        jCmb_GP3_val.setVisible(false);
    }//GEN-LAST:event_jBtn_GP3_inActionPerformed

    
    
    //======================== GPIO CHECK BOXES ==============================//
    
    private void jCheckBox_GP0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_GP0ActionPerformed
        if(jCmb_operation.getSelectedItem().equals("Set")){
            if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                createLayoutCONFIGURE_Set();
            }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                createLayoutGPIO_Set();
            }
        }else if(jCmb_operation.getSelectedItem().equals("Get")){
            if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                createLayoutCONFIGURE_Get();
            }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                createLayoutGPIO_Get();
            }
        }
    }//GEN-LAST:event_jCheckBox_GP0ActionPerformed

    private void jCheckBox_GP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_GP1ActionPerformed
         if(jCmb_operation.getSelectedItem().equals("Set")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Set();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Set();
             }
         }else if(jCmb_operation.getSelectedItem().equals("Get")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Get();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Get();
             }
         }
    }//GEN-LAST:event_jCheckBox_GP1ActionPerformed
   
    private void jCheckBox_GP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_GP2ActionPerformed
        if(jCmb_operation.getSelectedItem().equals("Set")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Set();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Set();
             }
         }else if(jCmb_operation.getSelectedItem().equals("Get")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Get();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Get();
             }
         }
    }//GEN-LAST:event_jCheckBox_GP2ActionPerformed
  
    private void jCheckBox_GP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_GP3ActionPerformed
        if(jCmb_operation.getSelectedItem().equals("Set")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Set();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Set();
             }
         }else if(jCmb_operation.getSelectedItem().equals("Get")){
             if(jCmb_protocol.getSelectedItem().equals("CONFIGURE")){
                 createLayoutCONFIGURE_Get();
             }else if(jCmb_protocol.getSelectedItem().equals("GPIO")){
                 createLayoutGPIO_Get();
             }
         }
    }//GEN-LAST:event_jCheckBox_GP3ActionPerformed

    
    
    //VID TextField validation
    private void jTxt_VIDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_VIDKeyPressed
        Pattern pattern = Pattern.compile("[0-9a-f]");
        Matcher matcher;
        StringBuilder str = new StringBuilder();
        str.append(evt.getKeyChar());   
        
        matcher = pattern.matcher(str.toString().toLowerCase());
        boolean rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_VIDKeyPressed

    private void jTxt_VIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_VIDKeyTyped
        Pattern pattern = Pattern.compile("[0-9a-f]");
        Matcher matcher;
        StringBuilder str = new StringBuilder();
        str.append(evt.getKeyChar());   
        
        matcher = pattern.matcher(str.toString().toLowerCase());
        boolean rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_VIDKeyTyped

    private void jTxt_VIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_VIDKeyReleased
        if(jTxt_VID.getText().equals("")){
        }else{
            try{
                short temp_vid = Short.parseShort(jTxt_VID.getText(), 16);
                vid = temp_vid;
            }catch(Exception ex){
                jTxt_VID.setText("");
                JOptionPane.showMessageDialog(null, "It must be a short value.");
            }
        }
    }//GEN-LAST:event_jTxt_VIDKeyReleased

    
    //PID TextField validation
    private void jTxt_PIDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_PIDKeyPressed
        Pattern pattern = Pattern.compile("[0-9a-f]");
        Matcher matcher;
        StringBuilder str = new StringBuilder();
        str.append(evt.getKeyChar());
        
        matcher = pattern.matcher(str.toString().toLowerCase());
        boolean rslt = matcher.matches();
        
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_PIDKeyPressed

    private void jTxt_PIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_PIDKeyReleased
        if(jTxt_PID.getText().equals("")){
        }else{
            try{
                short temp_pid = Short.parseShort(jTxt_PID.getText(), 16);
                pid = temp_pid;
            }catch(Exception ex){
                jTxt_PID.setText("");
                JOptionPane.showMessageDialog(null, "It must be a short value");
            }
        }
    }//GEN-LAST:event_jTxt_PIDKeyReleased

    private void jTxt_PIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_PIDKeyTyped
        Pattern pattern = Pattern.compile("[0-9a-f]");
        Matcher matcher;
        StringBuilder str = new StringBuilder();
        str.append(evt.getKeyChar());
        
        matcher = pattern.matcher(str.toString().toLowerCase());
        boolean rslt = matcher.matches();
        
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_PIDKeyTyped

    //open a connection for selected device from Device List ComboBox
    private void jBtn_openConnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_openConnActionPerformed
        long tempMcpHandle;
        int rslt;

        String tempSelectedItem = (String)jCmb_listDev.getSelectedItem();
        if(tempSelectedItem != null){
            Pattern pattern1 = Pattern.compile("Device [0-9]*");
            Matcher matcher1 = pattern1.matcher(tempSelectedItem);
            boolean rsltMatch1 = matcher1.matches();
            
            if(rsltMatch1 == true){
                int tempIndex = jCmb_listDev.getSelectedIndex();
                tempMcpHandle = chip.Mcp2221_OpenByIndex(vid, pid, tempIndex);
                if(tempMcpHandle != -1){
                    String strDev = "Unknown #" + tempIndex;
                    list_connDev.add(strDev);
                    
                    //make enable selected device cmb and operation button
                    jCmb_selectDev.setEnabled(true);
                    jBtn_operation.setEnabled(true);
                    
                    jCmb_selectDev.addItem(strDev);
                    jLabel_currentSts.setText("Connection opened successfully");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));

                    //create a new device with index and associated handle
                    Device dev = new Device(strDev, tempMcpHandle);
                    synchronized(listOpenedDev){
                        listOpenedDev.add(dev);
                    }
                    jCmb_listDev.removeItem(tempSelectedItem);
                    //listDev.add(null);
                }else{
                    rslt = chip.Mcp2221_GetLastError();
                    switch(rslt){
                        case -101:
                            jLabel_currentSts.setText("Device not found. Error code: -101");
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -103:
                            jLabel_currentSts.setText("Device not found. Error code: -103" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -105:
                            jLabel_currentSts.setText("Open device error. Error code: -105" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -106:
                            jLabel_currentSts.setText("Connection already opened. Error code: -106" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                    }
                }
            }else{
                int tempIndex = jCmb_listDev.getSelectedIndex();
                tempMcpHandle = chip.Mcp2221_OpenBySN(vid, pid, tempSelectedItem);
                if(tempMcpHandle != -1){
                    list_connDev.add(tempSelectedItem);
                    
                    //make enable selected device cmb and operation button 
                    jCmb_selectDev.setEnabled(true);
                    jBtn_operation.setEnabled(true);
                    
                    jCmb_selectDev.addItem(tempSelectedItem);
                    jLabel_currentSts.setText("Connection opened successfully");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));

                    //create a new device with serial number and associated handle
                    //create a new device with index and handle
                    Device dev = new Device(tempSelectedItem, tempMcpHandle);
                    synchronized(listOpenedDev){
                        listOpenedDev.add(dev);
                    }
                    
                    jCmb_listDev.removeItem(tempSelectedItem);

                }else{
                    rslt = chip.Mcp2221_GetLastError();
                    switch(rslt){
                        case -101:
                            jLabel_currentSts.setText("Device not found. Error code: -101");
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -103:
                            jLabel_currentSts.setText("Device not found. Error code: -103" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -105:
                            jLabel_currentSts.setText("Open device error. Error code: -105" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                        case -106:
                            jLabel_currentSts.setText("Connection already opened. Error code: -106" );
                            jLabel_currentSts.setForeground(Color.red);
                            break;
                    }
                }
            }
        }
    }//GEN-LAST:event_jBtn_openConnActionPerformed

    private void jBtn_closeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_closeActionPerformed
        int result = -1;
        int rslt = -1;
        Iterator<Device> it = listOpenedDev.iterator();
        while(it.hasNext()){
            Device dev = it.next();
            if(dev.getSerialNumber().equals(list_connDev.getSelectedItem())){
                int tempVid[] = new int[1];
                int tempPid[] = new int[1];
                rslt = chip.Mcp2221_GetVidPid(dev.getHandle(), tempVid, tempPid);
                result = chip.Mcp2221_Close(dev.getHandle());
                if(result != 0){
                    jLabel_currentSts.setText("Could not close the device. Error code: " + result);
                    jLabel_currentSts.setForeground(Color.red);
                }else{
                    if(tempVid[0] == vid && tempPid[0] == pid){
                        Pattern pattern = Pattern.compile("Unknown #[0-9]*");
                        Matcher matcher = pattern.matcher(dev.getSerialNumber());
                        boolean rsltMatch = matcher.matches();
                        if(rsltMatch != true){
                            jCmb_listDev.addItem(dev.getSerialNumber());
                        }else{
                            String str = matcher.group().replaceAll("[^0-9]", "");
                            try{
                                int index = Integer.parseInt(str);
                                String stringTemp = "Device " + (index + 1);
                                jCmb_listDev.addItem(stringTemp);
                            }catch(Exception ex){ 
                            }
                        }
                    }           
                    it.remove();
                    list_connDev.remove(dev.getSerialNumber());  
                    jCmb_selectDev.removeItem(dev.getSerialNumber());
                    
                    //make disable selected device cmb and operation button if no device can be selected
                    if(jCmb_selectDev.getItemCount() <= 0){
                        jCmb_selectDev.setEnabled(false);
                        jBtn_operation.setEnabled(false);
                    }
                    jLabel_currentSts.setText("Connection successfully closed");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));
                    //if device is still connected, add it into jCmb_listDev 
                }    
            }
        }
        
        
    }//GEN-LAST:event_jBtn_closeActionPerformed

    private void jBtn_closeAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_closeAllActionPerformed
        int result;
        result = chip.Mcp2221_CloseAll();
        if(result != 0){
            jLabel_currentSts.setText("Could not close all connections. Error code: " + result);
            jLabel_currentSts.setForeground(Color.red);
        }else{
            Iterator<Device> it = listOpenedDev.iterator();
            while(it.hasNext()){
                Device dev = it.next();
//                Pattern pattern = Pattern.compile("Unknown #[0-9]*");
//                    Matcher matcher = pattern.matcher(dev.getSerialNumber());
//                    boolean rsltMatch = matcher.matches();
//                    if(rsltMatch != true){
//                        jCmb_listDev.addItem(dev.getSerialNumber());
//                    }else{
//                        String str = matcher.group().replaceAll("[^0-9]", "");
//                        try{
//                            int index = Integer.parseInt(str);
//                            String stringTemp = "Device " + (index + 1);
//                            jCmb_listDev.addItem(stringTemp);
//                        }catch(Exception ex){   
//                        }
//                    }
                it.remove();              
            }
            flag_closeAllConnections = true;
            Main.listDevices();
            list_connDev.removeAll();
            jCmb_selectDev.removeAllItems();
            
            //make disable selected device cmb and operation button if no device can be selected
            jCmb_selectDev.setEnabled(false);
            jBtn_operation.setEnabled(false);
            
            jLabel_currentSts.setText("All connections successfully closed");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
        }
    }//GEN-LAST:event_jBtn_closeAllActionPerformed
    
    private void jBtn_operationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_operationActionPerformed
        switch(jCmb_protocol.getSelectedItem().toString()){
            case "I2C":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Read":
                        i2cRead();
                        break;
                    case "Write":
                        i2cWrite();
                        break;
                }
                break;
            case "SMBUS":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Read Byte":
                        smbusReadByte();
                        break;
                    case "Write Byte":
                        smbusWriteByte();
                        break;
                    case "Read Word":
                        smbusReadWord();
                        break;
                    case "Write Word":
                        smbusWriteWord();
                        break;
                    case "Block Read":
                        smbusBlockRead();
                        break;
                    case "Block Write":
                        smbusBlockWrite();
                        break;
                }
                break;
            case "CONFIGURE":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Get":
                        getPinConfig();
                        break;
                    case "Set":
                        setPinConfig();
                        break;
                }
                break;
            case "GPIO":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Get":
                        getGPIO();
                        break;
                    case "Set":
                        setGPIO();
                        break;
                }
                break;
            case "DAC":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Set Vref":
                        setVref();
                        break;
                    case "Get Vref":
                        getVref();
                        break;
                    case "Set Value":
                        setDacValue();
                        break;
                    case "Get Value":
                        getDacValue();
                        break;
                }
                break;
            case "ADC":
                switch(jCmb_operation.getSelectedItem().toString()){
                    case "Set Vref":
                        setVref();
                        break;
                    case "Get Vref":
                        getVref();
                        break;
                    case "Get Data":
                        getAdcData();
                        break;
                }
                break;
        } 
    }//GEN-LAST:event_jBtn_operationActionPerformed

    
    
    //==========================================================================
    //=========================== VALIDATION FIELD =============================
    //==========================================================================
    
    //*** SLAVE ADDRESS
    private void jTxt_addressKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_addressKeyTyped
        Pattern pattern = Pattern.compile("[0-9a-f]");
        StringBuilder tempKey = new StringBuilder();
        tempKey.append(evt.getKeyChar());
        
        Matcher matcher = pattern.matcher(tempKey.toString().toLowerCase());
        boolean rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_addressKeyTyped

    private void jTxt_addressKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_addressKeyReleased
        if(jTxt_address.getText().equals("")){         
        }else{
            try{
                int temp_address = Integer.parseInt(jTxt_address.getText(), 16);
                if(temp_address < 0 || temp_address > 255){
                    JOptionPane.showMessageDialog(null, "Invalid value. Slave address must be between 0 and FF.");
                    jTxt_address.setText("");
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid value. Slave address must be between 0 and FF.");
                jTxt_address.setText("");
            }
        } 
    }//GEN-LAST:event_jTxt_addressKeyReleased
    
    //*** SPEED
    private void jTxt_speedKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_speedKeyTyped
        Pattern pattern = Pattern.compile("[0-9]");
        StringBuilder tempKey = new StringBuilder();
        tempKey.append(evt.getKeyChar());
        
        Matcher matcher = pattern.matcher(tempKey);
        boolean rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_speedKeyTyped

    private void jTxt_speedFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxt_speedFocusLost
        if(jTxt_speed.getText().equals("")){
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Invalid value. Speed must be between 46875 and 500000 bps.");
                    jTxt_speed.setText("");
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid value. Speed must be between 46875 and 500000 bps.");
                jTxt_speed.setText("");
            }
        }       
    }//GEN-LAST:event_jTxt_speedFocusLost

    //*** DATA/NUMBER OF BYTES TO READ
    private void jTxt_dataKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_dataKeyTyped
        StringBuilder tempKey = new StringBuilder();
        boolean rslt;
        if(jCmb_operation.getSelectedItem().equals("Read") || jCmb_operation.getSelectedItem().equals("Block Read")){
            Pattern pattern_i2cRead = Pattern.compile("[0-9]");
                tempKey.append(evt.getKeyChar());
                Matcher matcher_i2cRead = pattern_i2cRead.matcher(tempKey);
                rslt = matcher_i2cRead.matches();
                // BACKSPACE = 8; ENTER = 10; SHIFT = 16
                // CTRL = 17; CAPSLOCK = 20; DELETE = 127
                if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                        && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                        && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
                    evt.consume();
                }
        }else if(jCmb_operation.getSelectedItem().equals("Write") || jCmb_operation.getSelectedItem().equals("Write Byte")
                || jCmb_operation.getSelectedItem().equals("Write Word") || jCmb_operation.getSelectedItem().equals("Block Write")){
            Pattern pattern_i2cWrite = Pattern.compile("[0-9a-f,]");
            tempKey.append(evt.getKeyChar());
            Matcher matcher_i2cWrite = pattern_i2cWrite.matcher(tempKey.toString().toLowerCase());
            rslt = matcher_i2cWrite.matches();
            // BACKSPACE = 8; ENTER = 10; SHIFT = 16
            // CTRL = 17; CAPSLOCK = 20; DELETE = 127
            if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                    && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                    && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
                evt.consume();
            }
        }
    }//GEN-LAST:event_jTxt_dataKeyTyped

    private void jTxt_dataKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_dataKeyReleased
        if(jTxt_data.getText().equals("")){        
        }else{
            if(jCmb_operation.getSelectedItem().equals("Read")){
                try{
                    int tempNoBytes = Integer.parseInt(jTxt_data.getText());
                    if(tempNoBytes < 1 || tempNoBytes > 65535){
                        JOptionPane.showMessageDialog(null, "Invalid value. Number of bytes to read must be between 1 and 65535.");
                        jTxt_data.setText("");
                    }
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Invalid value. Number of bytes to read must be between 1 and 65535.");
                    jTxt_data.setText("");
                }
            }else if(jCmb_operation.getSelectedItem().equals(("Block Read"))){
                try{
                    int tempNoBytes = Integer.parseInt(jTxt_data.getText());
                    if(tempNoBytes < 1 || tempNoBytes > 255){
                        JOptionPane.showMessageDialog(null, "Invalid value. Number of bytes to read must be between 1 and 255.");
                        jTxt_data.setText("");
                    }
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Invalid value. Number of bytes to read must be between 1 and 255.");
                    jTxt_data.setText("");
                }
            }
        }  
    }//GEN-LAST:event_jTxt_dataKeyReleased

    private void jTxt_dataFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxt_dataFocusLost
        String tempData = new String();
        tempData = jTxt_data.getText();
        String[] tempArrayData = tempData.split(",");
        int[] bufferToWrite = new int[65535];
        int dataToWrite;
        int counter = 0;
        //for Write Operations
        if(jTxt_data.getText().equals("")){     
        }else{
            if(jCmb_operation.getSelectedItem().equals("Write") || jCmb_operation.getSelectedItem().equals("Block Write")){
                
                try{
                    for(String str: tempArrayData){
                        bufferToWrite[counter] = Integer.parseInt(str, 16);
                        if(bufferToWrite[counter] < 0 || bufferToWrite[counter] > 255){
                            JOptionPane.showMessageDialog(null, "Data to write must be an array containing just value between 0 and FF.");
                            jTxt_data.setText("");
                            break;
                        }else{
                            counter++;
                        }
                    }
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Invalid input for 'Data'");
                    jTxt_data.setText("");
                }
            }else if(jCmb_operation.getSelectedItem().equals("Write Byte")){
                try{
                    dataToWrite = Integer.parseInt(tempData, 16);
                    if(dataToWrite < 0 || dataToWrite > 255){
                        JOptionPane.showMessageDialog(null, "Data to write must be a byte.");
                        jTxt_data.setText("");
                    }
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Data to write must be a byte.");
                    jTxt_data.setText("");
                }
            }else if(jCmb_operation.getSelectedItem().equals("Write Word")){
                try{
                    for(String str: tempArrayData){
                        bufferToWrite[counter] = Integer.parseInt(str, 16);
                        if(bufferToWrite[counter] < 0 || bufferToWrite[counter] > 255){
                            JOptionPane.showMessageDialog(null, "Data to write must be an array containing just values between 0 and FF.");
                            jTxt_data.setText("");
                            break;
                        }else{
                            counter++;
                        }
                    }  
                    if(counter != 2){
                        JOptionPane.showMessageDialog(null, "Data to write must have 2 bytes.");
                        jTxt_data.setText("");
                    }
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Invalid input for 'Data'.");
                    jTxt_data.setText("");
                } 
            }
        }
    }//GEN-LAST:event_jTxt_dataFocusLost

    //*** COMMAND (SMBUS)
    private void jTxt_cmdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_cmdKeyTyped
        Pattern pattern = Pattern.compile("[0-9a-f]");
        StringBuilder tempKey = new StringBuilder();
        tempKey.append(evt.getKeyChar());
        Matcher matcher = pattern.matcher(tempKey.toString().toLowerCase());
        boolean rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
            if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                    && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                    && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
                evt.consume();
            }
    }//GEN-LAST:event_jTxt_cmdKeyTyped

    private void jTxt_cmdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_cmdKeyReleased
        if(jTxt_cmd.getText().equals("")){         
        }else{
            try{
                int temp_command = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(temp_command < 0 || temp_command > 255){
                    JOptionPane.showMessageDialog(null, "Invalid value. Command must be between 0 and FF.");
                    jTxt_cmd.setText("");
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid value. Command must be between 0 and FF.");
                jTxt_cmd.setText("");
            }
        } 
    }//GEN-LAST:event_jTxt_cmdKeyReleased

    private void jCmb_GP0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_GP0ActionPerformed
        if(jCmb_GP0.getSelectedItem() != null){
            switch(jCmb_GP0.getSelectedItem().toString()){
                case "Output":
                    jCmb_GP0_val.setVisible(true);
                    break;
                case "Input":
                    jCmb_GP0_val.setVisible(false);
                    break;
                case "SSPND":
                    jCmb_GP0_val.setVisible(false);
                    break;
                case "LED UART RX":
                    jCmb_GP0_val.setVisible(false);
            }
        }    
    }//GEN-LAST:event_jCmb_GP0ActionPerformed

    private void jCmb_GP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_GP1ActionPerformed
        if(jCmb_GP1.getSelectedItem() != null){
            switch(jCmb_GP1.getSelectedItem().toString()){
                case "Output":
                    jCmb_GP1_val.setVisible(true);
                    break;
                case "Input":
                    jCmb_GP1_val.setVisible(false);
                    break;
                case "Clock out":
                    jCmb_GP1_val.setVisible(false);
                    break;
                case "ADC1":
                    jCmb_GP1_val.setVisible(false);
                    break;
                case "LED UART TX":
                    jCmb_GP1_val.setVisible(false);
                    break;
                case "Interrupt detection":
                    jCmb_GP1_val.setVisible(false);
                    break;
            }
        }
    }//GEN-LAST:event_jCmb_GP1ActionPerformed

    private void jCmb_GP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_GP2ActionPerformed
        if(jCmb_GP2.getSelectedItem() != null){
            switch(jCmb_GP2.getSelectedItem().toString()){
                case "Output":
                    jCmb_GP2_val.setVisible(true);
                    break;
                case "Input":
                    jCmb_GP2_val.setVisible(false);
                    break;
                case "USBCFG":
                    jCmb_GP2_val.setVisible(false);
                    break;
                case "ADC2":
                    jCmb_GP2_val.setVisible(false);
                    break;
                case "DAC1":
                    jCmb_GP2_val.setVisible(false);
                    break;
            }
        }
    }//GEN-LAST:event_jCmb_GP2ActionPerformed

    private void jCmb_GP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCmb_GP3ActionPerformed
        if(jCmb_GP3.getSelectedItem() != null){
            switch(jCmb_GP3.getSelectedItem().toString()){
                case "Output":
                    jCmb_GP3_val.setVisible(true);
                    break;
                case "Input":
                    jCmb_GP3_val.setVisible(false);
                    break;
                case "LED I2C":
                    jCmb_GP3_val.setVisible(false);
                    break;
                case "ADC3":
                    jCmb_GP3_val.setVisible(false);
                    break;
                case "DAC2":
                    jCmb_GP3_val.setVisible(false);
                    break;
            }
        }
    }//GEN-LAST:event_jCmb_GP3ActionPerformed

    
    //Data validation for DAC value(0-31)
    private void jTxt_dacKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_dacKeyReleased
        if(jTxt_dac.getText() != ""){
            try{
                int tempDacValue = Integer.parseInt(jTxt_dac.getText());
                if(tempDacValue < 0 || tempDacValue > 31){
                    JOptionPane.showMessageDialog(null, "Invalid value. Dac value must be between 0 and 31.");
                    jTxt_dac.setText("");
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid value. DAC value must be between 0 and 31.");
                jTxt_dac.setText("");
            }
        }
    }//GEN-LAST:event_jTxt_dacKeyReleased

    private void jTxt_dacKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxt_dacKeyTyped
        Pattern pattern = Pattern.compile("[0-9]");
        StringBuilder tempKey = new StringBuilder();
        boolean rslt;
        tempKey.append(evt.getKeyChar());
        Matcher matcher = pattern.matcher(tempKey);
        rslt = matcher.matches();
        // BACKSPACE = 8; ENTER = 10; SHIFT = 16
        // CTRL = 17; CAPSLOCK = 20; DELETE = 127
        if(rslt == false && evt.getKeyCode() != 8 && evt.getKeyCode() != 10 
                && evt.getKeyCode() != 16 && evt.getKeyCode() != 17 
                && evt.getKeyCode() != 20 && evt.getKeyCode() != 127){
            evt.consume();
        }
    }//GEN-LAST:event_jTxt_dacKeyTyped

    private void jBtn_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn_clearActionPerformed
        jTxt_result.setText("");
    }//GEN-LAST:event_jBtn_clearActionPerformed

    private void jMenuItem_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_exitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem_exitActionPerformed

    
    
    
    
    
    
    //==========================================================================
    //=========================== OPERATIONS ===================================
    //==========================================================================
    
    
    //******************************* I2C ************************************//
    
    //LAYOUT
    private void createLayoutI2C(){
        
        //Make visible all fields for I2C protocol
        jLabel_addressLen.setVisible(true);
        jCmb_addressLen.setVisible(true);
        jLabel_address.setVisible(true);
        jTxt_address.setVisible(true);
        jLabel_speed.setVisible(true);
        jTxt_speed.setVisible(true);
        jLabel_data.setVisible(true);
        jTxt_data.setVisible(true);
        jBtn_operation.setVisible(true);
                
        //Make invisible all fields used for other protocols
        jLabel_pec.setVisible(false);
        jCmb_pec.setVisible(false);
        jLabel_cmd.setVisible(false);
        jTxt_cmd.setVisible(false);
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        jCmb_GP3_val.setVisible(false);
        
        //*** Operation
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    300, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(310, 180);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_operation.setLocation(410, 175);
        
        jCmb_operation.setPreferredSize(new Dimension(110, 25));
        jCmb_operation.setMinimumSize(new Dimension(110, 25));
        
        //Remove all items from Operation ComboBox and add 'Write' and 'Read' items
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Write");
        jCmb_operation.addItem("Read");
        
        //*** Data
        layout.putConstraint(SpringLayout.WEST, jLabel_data,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_data,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_data.setLocation(20, 220);
        layout.putConstraint(SpringLayout.WEST, jTxt_data,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_data,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_data.setLocation(120, 215);
        
        jTxt_data.setPreferredSize(new Dimension(515, 25));
        jTxt_data.setMinimumSize(new Dimension(515, 25));
        //*** Operation Button
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                465,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation,
                245,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setPreferredSize(new Dimension(160, 25));
        jBtn_operation.setMinimumSize(new Dimension(160, 25));
        
        if(jCmb_operation.getSelectedItem().equals("Write")){
            jBtn_operation.setText("I2C Write");
        }else if(jCmb_operation.getSelectedItem().equals("Read")){
            jBtn_operation.setText("I2C Read");
        }
        
        repaint();
    }
   
    //I2C READ 
    private void i2cRead(){
        int result = -1;
        Device selectedDev = new Device();
        int numberOfBytes;
        byte slaveAddress;
        boolean use7bitAddress = true;
        int speed = DEFAULT_SPEED;
        byte[] dataToRead = new byte[65535];
        //String to display in hexadecimal format slave address
        StringBuilder sbAddress = new StringBuilder();
        
        //Device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();         
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber)){
                    selectedDev = dev;
                }
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Adress Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempSlaveAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempSlaveAddress < 0 || tempSlaveAddress > 255){
                    JOptionPane.showMessageDialog(null, "Slave address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempSlaveAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 50000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }
        }
        
        //Number of Bytes
        if(jTxt_data.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Number of Bytes' field.");
            return;
        }else{
            try{
                int tempNoBytes = Integer.parseInt(jTxt_data.getText());
                if(tempNoBytes < 1 || tempNoBytes > 65535){
                    JOptionPane.showMessageDialog(null, "Number of bytes to read must be between 1 and 65535.");
                    return;
                }else{
                    numberOfBytes = tempNoBytes;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Number of Bytes'.");
                return;
            }
        }
        
        //set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set speed failed");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jLabel_currentSts.setText(" Successful setting Speed");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //I2c Read
        result = chip.Mcp2221_I2cRead(selectedDev.getHandle(), numberOfBytes, slaveAddress, use7bitAddress, dataToRead);
        if(result == Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Successful I2c Read");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            
            jTxt_result.append("> I2c Read: Address = " + sbAddress + " ; Number of bytes = " + numberOfBytes + "\n");
            jTxt_result.append("\tData: ");
            StringBuilder sbData;
            for(int i=0; i<(numberOfBytes -1); i++){
                sbData = new StringBuilder();
                sbData.append(String.format("%02X", dataToRead[i]));
                jTxt_result.append(sbData + ",");
            }
            sbData = new StringBuilder();
            sbData.append(String.format("%02X", dataToRead[numberOfBytes - 1]));
            jTxt_result.append(sbData + "\n");
        }else{
            jLabel_currentSts.setText(" I2c Read failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> I2c Read: Address = " + sbAddress + " ; Number of bytes = " + numberOfBytes + "\n");
            jTxt_result.append("\tError code: " + result + "\n");
        }
    }
    
    //I2C WRITE
    private void i2cWrite(){
        Device selectedDev =  new Device();
        int numberOfBytes;
        byte slaveAddress;
        boolean use7bitAddress = true;
        int speed = DEFAULT_SPEED;
        byte[] dataToWrite = new byte[65535];
        //String to display in hexadecimal format slave address
        StringBuilder sbAddress = new StringBuilder();
        
        //Device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();         
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber)){
                    selectedDev = dev;
                }
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Adress Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempSlaveAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempSlaveAddress < 0 || tempSlaveAddress > 255){
                    JOptionPane.showMessageDialog(null, "Slave address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempSlaveAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
                return;
            }
        }       
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 50000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }
        }
        
        //Data
        int[] tempData = new int[65535];
        int counter = 0;
        if(jTxt_data.getText().equals("")){
            //JOptionPane.showMessageDialog(null, "0 bytes to write");
            numberOfBytes = 0;
        }else{
            String tempStr = new String();
            tempStr = jTxt_data.getText();
            String[] tempArray = tempStr.split(",");
            try{
                for(String str : tempArray){
                    tempData[counter] = Integer.parseInt(str, 16);
                    if(tempData[counter] < 0 || tempData[counter] > 255){
                        JOptionPane.showMessageDialog(null, "Data to write must be an array containing just value between 0 and FF.");
                        return;
                    }else{
                        dataToWrite[counter] = (byte)tempData[counter];
                        counter++;   
                    }
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Data'.");
                return;
            }
        }
        numberOfBytes = counter;
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jLabel_currentSts.setText(" Successful setting Speed");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //I2C Write
        result = chip.Mcp2221_I2cWrite(selectedDev.getHandle(), numberOfBytes, slaveAddress, use7bitAddress, dataToWrite);
        if(result == Constants.E_NO_ERR){
            jLabel_currentSts.setText("Successful I2c Write");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            
            jTxt_result.append("> I2c Write: Address = " + sbAddress + " ; Data = ");
            if(numberOfBytes > 0){
                StringBuilder sbData;
                for(int i=0; i<(numberOfBytes -1); i++){
                    sbData = new StringBuilder();
                    sbData.append(String.format("%02X", dataToWrite[i]));
                    jTxt_result.append(sbData + ",");
                }
                sbData = new StringBuilder();
                sbData.append(String.format("%02X", dataToWrite[numberOfBytes - 1]));
                jTxt_result.append(sbData + "\n");
            }else
                jTxt_result.append("\n");
            jTxt_result.append("\tSuccessful I2C Write." + "\n");
        }else{
            jLabel_currentSts.setText(" I2c Write failed ");
            jLabel_currentSts.setForeground(Color.red);
            
            jTxt_result.append("> I2c Write: Address = " + sbAddress + " ; Data = ");
            if(numberOfBytes > 0){
               StringBuilder sbData;
                for(int i=0; i<(numberOfBytes -1); i++){
                    sbData = new StringBuilder();
                    sbData.append(String.format("%02X", dataToWrite[i]));
                    jTxt_result.append(sbData + ",");
                }
                sbData = new StringBuilder();
                sbData.append(String.format("%02X", dataToWrite[numberOfBytes - 1]));
                jTxt_result.append(sbData + "\n"); 
            }else
                jTxt_result.append("\n");
            jTxt_result.append("\tError code: " + result + "\n");
        }
    }
    
    
    
    //******************************* SMBUS **********************************//
    
    //CREATE LAYOUT
    private void createLayoutSMBUS(){
        //Make visible all fields used for SMBUS protocol
        jLabel_addressLen.setVisible(true);
        jCmb_addressLen.setVisible(true);
        jLabel_address.setVisible(true);
        jTxt_address.setVisible(true);
        jLabel_speed.setVisible(true);
        jTxt_speed.setVisible(true);
        jLabel_pec.setVisible(true);
        jCmb_pec.setVisible(true);
        jLabel_cmd.setVisible(true);
        jTxt_cmd.setVisible(true);
        jLabel_data.setVisible(true);
        jTxt_data.setVisible(true);
        
        //Make invisible all fields used for other protocols
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        jCmb_GP3_val.setVisible(false);

        //*** Operation
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    300, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(310, 180);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_operation.setLocation(410, 175);
        
        jCmb_operation.setPreferredSize(new Dimension(110, 25));
        jCmb_operation.setMinimumSize(new Dimension(110, 25));
        
        //Remove all items from Operation ComboBox and add items for SMBUS protocol
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Write Byte");
        jCmb_operation.addItem("Read Byte");
        jCmb_operation.addItem("Write Word");
        jCmb_operation.addItem("Read Word");
        jCmb_operation.addItem("Block Write");
        jCmb_operation.addItem("Block Read");
        
        //*** PEC
        jPanel_operations.add(jLabel_pec);
        jPanel_operations.add(jCmb_pec);
        layout.putConstraint(SpringLayout.WEST, jLabel_pec,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_pec,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_pec.setLocation(20, 220);
        layout.putConstraint(SpringLayout.WEST, jCmb_pec,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_pec,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_pec.setLocation(120, 215);
        
        jCmb_pec.setPreferredSize(new Dimension(110, 25));
        jCmb_pec.setMinimumSize(new Dimension(110, 25));
            
        //*** Command
        jPanel_operations.add(jLabel_cmd);
        jPanel_operations.add(jTxt_cmd);
        layout.putConstraint(SpringLayout.WEST, jLabel_cmd,
                    300, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_cmd,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_cmd.setLocation(310, 220);
        layout.putConstraint(SpringLayout.WEST, jTxt_cmd,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_cmd,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_cmd.setLocation(410, 215);
        
        jCmb_operation.setPreferredSize(new Dimension(110, 25));
        jCmb_operation.setMinimumSize(new Dimension(110, 25));
        
 
        //*** Data
        jPanel_operations.add(jLabel_data);
        jPanel_operations.add(jTxt_data);
        layout.putConstraint(SpringLayout.WEST, jLabel_data,
                    10, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_data,
                    250,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_data.setLocation(20, 260);
        layout.putConstraint(SpringLayout.WEST, jTxt_data,
                    110, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_data,
                    245,
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_data.setLocation(120, 255);
        
        jTxt_data.setPreferredSize(new Dimension(515, 25));
        jTxt_data.setMinimumSize(new Dimension(515, 25));
        
        //*** Operation Button
        jPanel_operations.add(jBtn_operation);
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                465,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation, 
                270,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setLocation(475, 280);
        
        switch(jCmb_operation.getSelectedItem().toString()){
            case "Write Byte":
                jBtn_operation.setText("SMBUS Write Byte");
                break;
            case "Read Byte":
                jBtn_operation.setText("SMBUS Read Byte");
                break;
            case "Write Word":
                jBtn_operation.setText("SMBUS Write Word");
                break;
            case "Read Word":
                jBtn_operation.setText("SMBUS Read Word");
                break;
            case "Block Write":
                jBtn_operation.setText("SMBUS Block Write");
                break;
            case "Block Read":
                jBtn_operation.setText("SMBUS Block Read");
                break;
        }
        
        repaint();
        
        //Get initial connected devices with DEFAULT VID(0x04D8) and DEFAULT PID(0x00DD)
        getInitialDev();
    }
    
    //SMBUS READ BYTE
    private void smbusReadByte(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        byte[] dataToRead = new byte[1];
        //Strings to display in hexadecimal format command byte and slave address
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber)){
                    selectedDev = dev;
                }
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
                break;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempSlaveAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempSlaveAddress < 0 || tempSlaveAddress > 255){
                    JOptionPane.showMessageDialog(null, "Slave address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempSlaveAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 50000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }
        }
        
        //PEC
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
                int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(tempCommand < 0 || tempCommand > 255){
                    JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                    return;
                }else{
                    command = (byte)tempCommand;
                    sbCommand.append(String.format("%02X", command));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'.");
                return;
            }
        }
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jLabel_currentSts.setText(" Successful setting Speed");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //SMBUS READ BYTE
        result = chip.Mcp2221_SmbusReadByte(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, dataToRead);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" SMBus Read Byte failed ");
            jLabel_currentSts.setForeground(Color.red);
            if(usePec)
                jTxt_result.append("> SMBus Read Byte: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + "\n");
            else
                jTxt_result.append("> SMBus Read Byte: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + "\n");
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText(" Successful SMBus Read Byte ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            if(usePec)
                jTxt_result.append("> SMBus Read Byte: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + "\n");
            else
                jTxt_result.append("> SMBus Read Byte: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + "\n");
            StringBuilder sbData = new StringBuilder();
            sbData.append(String.format("%02X", dataToRead[0]));
            jTxt_result.append("\tData: " + sbData + "\n");
        }
    }
    
    //SMBUS WRITE BYTE
    private void smbusWriteByte(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        byte dataToWrite = 0;
        //Strings to display in hexadecimal format command byte and slave address
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
                break;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field");
            return;
        }else{
            try{
                int tempSlaveAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempSlaveAddress < 0 || tempSlaveAddress > 255){
                    JOptionPane.showMessageDialog(null, "Slave address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempSlaveAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 500000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
            }
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
                int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(tempCommand < 0 || tempCommand > 255){
                    JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                    return;
                }else{
                    command = (byte)tempCommand;
                    sbCommand.append(String.format("%02X", command));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'.");
                return;
            }
        }
        
        //PEC
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jLabel_currentSts.setText(" Successful setting Speed ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //Data to write
        if(jTxt_data.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Data' field.");
            return;
        }else{
            try{
                int tempData = Integer.parseInt(jTxt_data.getText(), 16);
                if(tempData < 0 || tempData > 255){
                    JOptionPane.showMessageDialog(null, "Data must be a value between 0 and FF.");
                    return;
                }else{
                    dataToWrite  = (byte)tempData;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Data'.");
                return;
            }
        }
        
        //SMBUS WRITE BYTE
        result = chip.Mcp2221_SmbusWriteByte(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, dataToWrite);
        if(result != 0){
            jLabel_currentSts.setText(" SMBus Write Byte failed ");
            jLabel_currentSts.setForeground(Color.red);
            if(usePec)
                jTxt_result.append("> SMBus Write Byte: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Data = " + dataToWrite + "\n");
            else
                jTxt_result.append("> SMBus Write Byte: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Data = " + dataToWrite + "\n");
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText(" Successful SMBus Write Byte");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            if(usePec)
                jTxt_result.append("> SMBus Write Byte: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Data = " + dataToWrite + "\n");
            else
                jTxt_result.append("> SMBus Write Byte: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Data = " + dataToWrite + "\n");
            jTxt_result.append("\tSuccessful SMBus Write Byte.\n");
        }
    }
    
    //SMBUS READ WORD
    private void smbusReadWord(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress = 0;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        byte[] dataToRead = new byte[2];
        //Strings to display in hexadecimal format command byte and slave address
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
                break;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempAddress < 0 || tempAddress > 255){
                    JOptionPane.showMessageDialog(null, "Slave address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 500000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }
        }
        
        //Use Pec
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
                int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(tempCommand < 0 || tempCommand > 255){
                    JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                    return;
                }else{
                    command = (byte)tempCommand;
                    sbCommand.append(String.format("%02X", command));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'.");
                return;
            }
        }
        
        //Set speed if its value is not default (100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
                return;
            }else{
                jLabel_currentSts.setText(" Successful setting Speed ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //SMBUS READ WORD
        result = chip.Mcp2221_SmbusReadWord(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, dataToRead);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" SMBus Read Word failed ");
            jLabel_currentSts.setForeground(Color.red);
            if(usePec)
                jTxt_result.append("> SMBus Read Word: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + "\n");
            else
                jTxt_result.append("> SMBus Read Word: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + "\n");       
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText(" Successful SMBus Read Word ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            if(usePec)
                jTxt_result.append("> SMBus Read Word: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + "\n");
            else
                jTxt_result.append("> SMBus Read Word: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + "\n");
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            sb1.append(String.format("%02X", dataToRead[0]));
            sb2.append(String.format("%02X", dataToRead[1]));
            jTxt_result.append("\tData: " + sb1 + ", " + sb2 + "\n");    
        }
    }
    
    //SMBUS WRITE WORD
    private void smbusWriteWord(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress = 0;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        byte[] dataToWrite = new byte[2];
        int counter = 0;
        //Strings to display in hexadecimal format command byte and slave address
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempAddress < 0 || tempAddress > 255){
                    JOptionPane.showMessageDialog(null, "Address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address' field.");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 500000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'");
                return;
            }
        }
        
        //Pec
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
               int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
               if(tempCommand < 0 || tempCommand > 255){
                   JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                   return;
               }else{
                   command = (byte)tempCommand;
                   sbCommand.append(String.format("%02X", command));
               }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'");
                return;
            }
        }
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
                return;        
            }else{
                jLabel_currentSts.setText(" Successful setting Speed ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //Data to write
        if(jTxt_data.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Data' field.");
            return;
        }else{
            String[] tempArray = jTxt_data.getText().split(",");
            int tempValue;
            try{
                for(String str: tempArray){
                    tempValue = Integer.parseInt(str, 16);
                    if(tempValue < 0 || tempValue > 255){
                        JOptionPane.showMessageDialog(null, "Buffer to write must contain just values between 0 and FF.");
                        return;
                    }else{
                        dataToWrite[counter] = (byte)tempValue;
                        counter++;
                    }
                }
                if(counter != 2){
                    JOptionPane.showMessageDialog(null, "Length buffer must be 2.");
                    return;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Data'.");
                return;
            }
            
            //SMBUS WRITE WORD
            result = chip.Mcp2221_SmbusWriteWord(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, dataToWrite);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" SMBus Write Word failed ");
                jLabel_currentSts.setForeground(Color.red);
                if(usePec)
                    jTxt_result.append("> SMBus Write Word: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Data = " + dataToWrite[0] + "," + dataToWrite[1] + "\n");
                else
                    jTxt_result.append("> SMBus Write Word: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Data = " + dataToWrite[0] + "," + dataToWrite[1] + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jLabel_currentSts.setText(" Successful SMBus Write Word ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
                if(usePec)
                    jTxt_result.append("> SMBus Write Word: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Data = " + dataToWrite[0] + "," + dataToWrite[1] + "\n");
                else
                    jTxt_result.append("> SMBus Write Word: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Data = " + dataToWrite[0] + "," + dataToWrite[1] + "\n");
                jTxt_result.append("\tSuccessful Smbus Write Word" + "\n");
            }
        }
    }
    
    //SMBUS BLOCK READ
    private void smbusBlockRead(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        short bytesCount = 0;
        byte[] dataToRead = new byte[65535];
        //Strings to display in hexadecimal format slave address and command byte
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
                break;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field");
            return;
        }else{
            try{
                int tempAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempAddress < 0 || tempAddress > 255){
                    JOptionPane.showMessageDialog(null, "Address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value for speed(100 kbps).");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 500000 bps.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }
        }
        
        //PEC
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
                int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(tempCommand < 0 || tempCommand > 255){
                    JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                    return;
                }else{
                    command = (byte)tempCommand;
                    sbCommand.append(String.format("%02X", command));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'.");
                return;
            }
        }
        
        //Number of bytes
        if(jTxt_data.getText().equals("")){
           JOptionPane.showMessageDialog(null, "Empty 'Number of Bytes' field.");
           return;
        }else{
            try{
                int tempBytes = Integer.parseInt(jTxt_data.getText());
                if(tempBytes < 1 || tempBytes > 255){
                    JOptionPane.showMessageDialog(null, "Number of bytes must be between 1 and 255");
                    return;
                }else{
                    bytesCount = (short)tempBytes;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Number of Bytes'.");
                return;
            }
        }
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
                return;        
            }else{
                jLabel_currentSts.setText(" Successful setting Speed ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //SMBUS BLOCK READ
        result = chip.Mcp2221_SmbusBlockRead(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, bytesCount, dataToRead);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" SMBus Block Read failed ");
            jLabel_currentSts.setForeground(Color.red);
            if(usePec)
                jTxt_result.append("> SMBus Block Read: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount + "\n");
            else
                jTxt_result.append("> SMBus Block Read: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount + "\n");
            jTxt_result.append("\tError code: " + result + "\n");
            
        }else{
            jLabel_currentSts.setText(" Successful SMBus Block Read ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            if(usePec)
                jTxt_result.append("> SMBus Block Read: Address = " + sbAddress + " , PEC = ON ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount + "\n");
            else
                jTxt_result.append("> SMBus Block Read: Address = " + sbAddress + " , PEC = OFF ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount + "\n");
            jTxt_result.append("\tData: ");
            StringBuilder sb;
            for(int i=0; i<(bytesCount -1); i++){
                sb = new StringBuilder();
                sb.append(String.format("%02X", dataToRead[i]));
                jTxt_result.append(sb + ",");
            }
            sb = new StringBuilder();
            sb.append(String.format("%02X", dataToRead[bytesCount - 1]));
            jTxt_result.append(sb + "\n");
        } 
    }
    
    //SMBUS BLOCK WRITE
    private void smbusBlockWrite(){
        int result = -1;
        Device selectedDev = new Device();
        boolean use7bitAddress = true;
        byte slaveAddress;
        int speed = DEFAULT_SPEED;
        boolean usePec = true;
        byte command;
        byte[] dataToWrite = new byte[255];
        short bytesCount;
        int counter = 0;
        //Strings to display in hexadecimal format slave address and command byte
        StringBuilder sbAddress = new StringBuilder();
        StringBuilder sbCommand = new StringBuilder();
        
        //Selected dev
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        // Address Length
        switch(jCmb_addressLen.getSelectedItem().toString()){
            case "7 bit":
                use7bitAddress = true;
                break;
            case "8 bit":
                use7bitAddress = false;
                break;
        }
        
        //Address
        if(jTxt_address.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Address' field.");
            return;
        }else{
            try{
                int tempAddress = Integer.parseInt(jTxt_address.getText(), 16);
                if(tempAddress < 0 || tempAddress > 255){
                    JOptionPane.showMessageDialog(null, "Address must be between 0 and FF.");
                    return;
                }else{
                    slaveAddress = (byte)tempAddress;
                    sbAddress.append(String.format("%02X", slaveAddress));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Address'.");
                return;
            }
        }
        
        //Speed
        if(jTxt_speed.getText().equals("")){
            JOptionPane.showMessageDialog(null, "It's setting default value (100kbps) for speed.");
            jTxt_speed.setText("100000");
            speed = DEFAULT_SPEED;
        }else{
            try{
                int tempSpeed = Integer.parseInt(jTxt_speed.getText());
                if(tempSpeed < 46875 || tempSpeed > 500000){
                    JOptionPane.showMessageDialog(null, "Speed must be between 46875 and 500000.");
                    return;
                }else{
                    speed = tempSpeed;
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Speed'.");
                return;
            }   
        }
        
        //PEC
        switch(jCmb_pec.getSelectedItem().toString()){
            case "ON":
                usePec = true;
                break;
            case "OFF":
                usePec = false;
                break;
        }
        
        //Command
        if(jTxt_cmd.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Empty 'Command' field.");
            return;
        }else{
            try{
                int tempCommand = Integer.parseInt(jTxt_cmd.getText(), 16);
                if(tempCommand < 0 || tempCommand > 255){
                    JOptionPane.showMessageDialog(null, "Command must be between 0 and FF.");
                    return;
                }else{
                    command = (byte)tempCommand;
                    sbCommand.append(String.format("%02X", command));
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Invalid input for 'Command'.");
                return;
            }
        }
        
        //Data to write
        if(jTxt_data.getText().equals("")){
            dataToWrite = new byte[65535];
            bytesCount = 0;    
        }else{
            int tempValue;
            String[] tempArray = jTxt_data.getText().split(",");
            for(String str:tempArray){
                try{
                    tempValue = Integer.parseInt(str);
                    if(tempValue < 0 || tempValue > 255){
                        JOptionPane.showMessageDialog(null, "Buffer to write must contain just values between 0 and FF.");
                        return;
                    }else{
                        dataToWrite[counter] = (byte)tempValue;
                        counter++;
                    }                             
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Invalid input for 'Data'");
                    return;
                }
            }               
            if(counter > 255){
                JOptionPane.showMessageDialog(null, "Buffer to write must have maxim length 255 bytes.");
                return;
            }else{
                bytesCount = (short)counter;
            }
        }
        
        //Set speed if its value is not default(100000)
        if(speed != DEFAULT_SPEED){
            result = chip.Mcp2221_SetSpeed(selectedDev.getHandle(), speed);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set Speed failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Set Speed: Speed = " + speed + "\n");
                jTxt_result.append("\tError code: " + result + "\n");
                return;        
            }else{
                jLabel_currentSts.setText(" Successful setting Speed ");
                jLabel_currentSts.setForeground(new Color(0, 170, 0));
            }
        }
        
        //SMBUS BLOCK WRITE
        result = chip.Mcp2221_SmbusBlockWrite(selectedDev.getHandle(), slaveAddress, use7bitAddress, usePec, command, bytesCount, dataToWrite);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" SMBus Block Write failed ");
            jLabel_currentSts.setForeground(Color.red);
            StringBuilder sb;
            if(usePec)
                jTxt_result.append(" >SMBus Block Write: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount);
            else
                jTxt_result.append(" >SMBus Block Write: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount);
            jTxt_result.append(" ; Data: ");
                for(int i=0; i<(bytesCount -1); i++){
                    sb = new StringBuilder();
                    sb.append(String.format("%02X", dataToWrite[i]));
                    jTxt_result.append(sb + ",");
                }
            sb = new StringBuilder();
            sb.append(String.format("%02X", dataToWrite[bytesCount - 1]));
            jTxt_result.append(sb + "\n");
            
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText( "Successful SMBus Block Write ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            StringBuilder sb;
            if(usePec)
                jTxt_result.append(" >SMBus Block Write: Address = " + sbAddress + " ; PEC = ON ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount);
            else
                jTxt_result.append(" >SMBus Block Write: Address = " + sbAddress + " ; PEC = OFF ; Command = " + sbCommand + " ; Bytes Count = " + bytesCount );
            jTxt_result.append(" ; Data: ");
                for(int i=0; i<(bytesCount -1); i++){
                    sb = new StringBuilder();
                    sb.append(String.format("%02X", dataToWrite[i]));
                    jTxt_result.append(sb + ",");
                }
            sb = new StringBuilder();
            sb.append(String.format("%02X", dataToWrite[bytesCount - 1]));
            jTxt_result.append(sb + "\n");
            jTxt_result.append("\tSuccessful SMBus Block Write." + "\n");
        }
    }
    
    
    
    //****************************** CONFIGURE *******************************//
    
    //LAYOUT
    private void createLayoutCONFIGURE(){      
        //Make invisible all fields used for other protocols
        jLabel_addressLen.setVisible(false);
        jCmb_addressLen.setVisible(false);
        jLabel_address.setVisible(false);
        jTxt_address.setVisible(false);
        jLabel_speed.setVisible(false);
        jTxt_speed.setVisible(false);
        jLabel_pec.setVisible(false);
        jCmb_pec.setVisible(false);
        jLabel_cmd.setVisible(false);
        jTxt_cmd.setVisible(false);
        jLabel_data.setVisible(false);
        jTxt_data.setVisible(false);
        
        jLabel_GP0.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jCmb_GP0.setVisible(false);
        
        jLabel_GP1.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jCmb_GP1.setVisible(false);
        
        jLabel_GP2.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jCmb_GP2.setVisible(false);
        
        jLabel_GP3.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jCmb_GP3.setVisible(false);
        
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        
        
        jCheckBox_GP0.setVisible(true);
        jCheckBox_GP1.setVisible(true);
        jCheckBox_GP2.setVisible(true);
        jCheckBox_GP3.setVisible(true);
        
        //Operation
        jPanel_operations.add(jLabel_operation);
        jPanel_operations.add(jCmb_operation);
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    10,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    80,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        
        jLabel_operation.setLocation(20, 140);
        jCmb_operation.setLocation(90, 135);
        jCmb_operation.setPreferredSize(new Dimension(80, 25));
        jCmb_operation.setMinimumSize(new Dimension(80, 25));
        
        //Remove 'Read' and 'Write' items from Operation ComboBox and add 'Set' and 'Get' items
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Set");
        jCmb_operation.addItem("Get");
        
        //*** GP0 CheckBox
        jPanel_operations.add(jCheckBox_GP0);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP0,
                    200,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP0,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP0.setLocation(210, 140);
        //*** GP1 CheckBox
        jPanel_operations.add(jCheckBox_GP1);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP1,
                    300,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP1,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP1.setLocation(310, 140);
        //*** GP2 CheckBox
        jPanel_operations.add(jCheckBox_GP2);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP2,
                    400,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP2,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP2.setLocation(410, 140);
        //*** GP3 CheckBox
        jPanel_operations.add(jCheckBox_GP3);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP3,
                    500,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP3,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP3.setLocation(510, 140);
        
        //*** Operation Button
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                465,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation,
                245,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setLocation(475, 255);
        if(jCmb_operation.getSelectedItem().equals("Set")){
            jBtn_operation.setText("Set Pin Configuration");
        }else if(jCmb_operation.getSelectedItem().equals("Get")){
            jBtn_operation.setText("Get Pin Configuration");
        }
        
        //Create special layout for 'Set' or 'Get' operation
        if(jCmb_operation.getSelectedItem().equals("Set")){
            createLayoutCONFIGURE_Set();
        }else if (jCmb_operation.getSelectedItem().equals("Get")){
            createLayoutCONFIGURE_Get();
        }
        
        repaint();
    }
    
    //create layout for 'CONFIGURE' protocol and 'Set' operation
    private void createLayoutCONFIGURE_Set(){
        //*** GP0
        if(jCheckBox_GP0.isSelected() == true){
            jPanel_operations.add(jLabel_GP0);
            jPanel_operations.add(jCmb_GP0);
            jPanel_operations.add(jCmb_GP0_val);
            
            jLabel_GP0.setVisible(true);
            jCmb_GP0.setVisible(true);
            jCmb_GP0_val.setVisible(false);
                              
            layout.putConstraint(SpringLayout.WEST, jLabel_GP0,
                    10,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP0,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP0,
                    50, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP0,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP0_val,
                    200,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP0_val,
                    165,
                    SpringLayout.WEST, jPanel_operations);
            
            jLabel_GP0.setLocation(20, 180);
            jCmb_GP0.setLocation(60, 175);
            
            jCmb_GP0_val.setLocation(240, 175);
            
            jCmb_GP0.setPreferredSize(new Dimension(140, 25));
            jCmb_GP0.setMinimumSize(new Dimension(140, 25));
            
            //add configuration items for GP0 ComboBox
            jCmb_GP0.removeAllItems();
            jCmb_GP0.addItem("Output");
            jCmb_GP0.addItem("Input");
            jCmb_GP0.addItem("SSPND");
            jCmb_GP0.addItem("LED UART RX");
            
            if(jCmb_GP0.getSelectedItem().equals("Output"))
                jCmb_GP0_val.setVisible(true);
        }else{
            jLabel_GP0.setVisible(false);
            jCmb_GP0.setVisible(false);
            jCmb_GP0_val.setVisible(false);
        }
        
        //*** GP1
        if(jCheckBox_GP1.isSelected() == true){
            jPanel_operations.add(jLabel_GP1);
            jPanel_operations.add(jCmb_GP1);
            jPanel_operations.add(jCmb_GP1_val);
            
            jLabel_GP1.setVisible(true);
            jCmb_GP1.setVisible(true);
            jCmb_GP1_val.setVisible(false);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP1,
                    360,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP1,
                    170,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP1,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP1,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP1_val,
                    550,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP1_val,
                    165,
                    SpringLayout.NORTH, jPanel_operations);
            
            jLabel_GP1.setLocation(370, 180);
            jCmb_GP1.setLocation(410, 175);
            jCmb_GP1_val.setLocation(460, 175);
            
            jCmb_GP1.setPreferredSize(new Dimension(140, 25));
            jCmb_GP1.setMinimumSize(new Dimension(140, 25));
            
            //add configuration items for GP1 ComboBox
            jCmb_GP1.removeAllItems();
            jCmb_GP1.addItem("Output");
            jCmb_GP1.addItem("Input");
            jCmb_GP1.addItem("Clock out");
            jCmb_GP1.addItem("ADC1");
            jCmb_GP1.addItem("LED UART TX");
            jCmb_GP1.addItem("Interrupt detection");
            
            if(jCmb_GP1.getSelectedItem().equals("Output"))
                jCmb_GP1_val.setVisible(true);
        }else{
            jLabel_GP1.setVisible(false);
            jCmb_GP1.setVisible(false);
            jCmb_GP1_val.setVisible(false);
        }
        
        //*** GP2
        if(jCheckBox_GP2.isSelected() == true){
            jPanel_operations.add(jLabel_GP2);
            jPanel_operations.add(jCmb_GP2);
            jPanel_operations.add(jCmb_GP2_val);
            
            jLabel_GP2.setVisible(true);
            jCmb_GP2.setVisible(true);
            jCmb_GP2_val.setVisible(false);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP2,
                    10,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP2,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP2,
                    50, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP2,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP2_val,
                    200,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP2_val,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
            
            
            jLabel_GP2.setLocation(20, 220);
            jCmb_GP2.setLocation(60, 215);
            jCmb_GP2_val.setLocation(240, 215);
            
            jCmb_GP2.setPreferredSize(new Dimension(140, 25));
            jCmb_GP2.setMinimumSize(new Dimension(140, 25));
            
            //add configuration items for GP2 ComboBox
            jCmb_GP2.removeAllItems();
            jCmb_GP2.addItem("Output");
            jCmb_GP2.addItem("Input");
            jCmb_GP2.addItem("USBCFG");
            jCmb_GP2.addItem("ADC2");
            jCmb_GP2.addItem("DAC1");
            
            if(jCmb_GP2.getSelectedItem().equals("Output"))
                jCmb_GP2_val.setVisible(true);
        }else{
            jLabel_GP2.setVisible(false);
            jCmb_GP2.setVisible(false);
            jCmb_GP2_val.setVisible(false);
        }
        
        //*** GP3
        if(jCheckBox_GP3.isSelected() == true){
            jPanel_operations.add(jLabel_GP3);
            jPanel_operations.add(jCmb_GP3);
            jPanel_operations.add(jCmb_GP3_val);
            
            jLabel_GP3.setVisible(true);
            jCmb_GP3.setVisible(true);
            jCmb_GP3_val.setVisible(false);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP3,
                    360,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP3,
                    210,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP3,
                    400, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP3,
                    205,
                    SpringLayout.NORTH, jPanel_operations);
            layout.putConstraint(SpringLayout.WEST, jCmb_GP3_val,
                    550, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jCmb_GP3_val,
                    205, 
                    SpringLayout.NORTH, jPanel_operations);
            
            jLabel_GP3.setLocation(370, 220);
            jCmb_GP3.setLocation(410, 215);
            jCmb_GP3_val.setLocation(560, 215);
            
            jCmb_GP3.setPreferredSize(new Dimension(140, 25));
            jCmb_GP3.setMinimumSize(new Dimension(140, 25));
            
            //add configuration items for GP3 ComboBox
            jCmb_GP3.removeAllItems();
            jCmb_GP3.addItem("Output");
            jCmb_GP3.addItem("Input");
            jCmb_GP3.addItem("LED I2C");
            jCmb_GP3.addItem("ADC3");
            jCmb_GP3.addItem("DAC2");
            
            if(jCmb_GP3.getSelectedItem().equals("Output"))
                jCmb_GP3_val.setVisible(true);
        }else{
            jLabel_GP3.setVisible(false);
            jCmb_GP3.setVisible(false);
            jCmb_GP3_val.setVisible(false);
        }
        
        //*** Operation Button
        jBtn_operation.setVisible(true);
        jPanel_operations.add(jBtn_operation);
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                490,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation,
                260,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setLocation(500, 270);
        jBtn_operation.setText("Set Pin Configuration");
        
        repaint();
    }
    
    //create layout for 'CONFIGURE' protocol and 'Get' operation
    private void createLayoutCONFIGURE_Get(){
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jCmb_GP3_val.setVisible(false);
        
        //*** Operation Button
        jBtn_operation.setText("Get Pin Configuration");
    }
    
    //Get configuration
    private void getPinConfig(){
        int result = -1;
        Device selectedDev = new Device();
        byte whichToGet = Constants.RUNTIME_SETTINGS;
        byte[] pinFunctions = new byte[4];
        byte[] pinDirections = new byte[4];
        byte[] outputValues = new byte[4];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        result = chip.Mcp2221_GetGpioSettings(selectedDev.getHandle(), whichToGet, pinFunctions, pinDirections, outputValues);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Get Pin Configuration failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Get Pin Configuration \n");
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText(" Successful getting Pin Configuration ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            jTxt_result.append("> Get Pin Configuration:" + "\n");
            //***GP0
            if(jCheckBox_GP0.isSelected()){
                jTxt_result.append("\tGP0 = ");
                switch(pinFunctions[0]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("SSPND, ");
                        break;
                    case 2:
                        jTxt_result.append("LED UART RX, ");
                        break;
                } 
                switch(pinDirections[0]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[0]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP1
            if(jCheckBox_GP1.isSelected()){
                jTxt_result.append("\tGP1 = ");
                switch(pinFunctions[1]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("Clock Out, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC1, ");
                        break;
                    case 3:
                        jTxt_result.append("LED UART TX, ");
                        break;
                    case 4:
                        jTxt_result.append("Interrupt Detection, ");
                        break;
                }
                switch(pinDirections[1]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[1]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP2
            if(jCheckBox_GP2.isSelected()){
                jTxt_result.append("\tGP2 = ");
                switch(pinFunctions[2]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("USBFG, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC2, ");
                        break;
                    case 3:
                        jTxt_result.append("DAC1, ");
                        break;
                }
                switch(pinDirections[2]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[2]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP3
            if(jCheckBox_GP3.isSelected()){
                jTxt_result.append("\tGP3 = ");
                switch(pinFunctions[3]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("LED I2C, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC3, ");
                        break;
                    case 3:
                        jTxt_result.append("DAC2, ");
                        break;
                }
                switch(pinDirections[3]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[3]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
        }
        
        
        
        
        
        
        
    }
    
    //Set configuration
    private void setPinConfig(){
        int result = -1;
        Device selectedDev = new Device();
        byte whichToGet = Constants.RUNTIME_SETTINGS;
        byte whichToSet = Constants.RUNTIME_SETTINGS;
        byte[] pinFunctions = new byte[4];
        byte[] pinDirections = new byte[4];
        byte[] outputValues = new byte[4];
        byte[] currentPinFunctions = new byte[4];
        byte[] currentPinDirections = new byte[4];
        byte[] currentOutputValues = new byte[4];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //first read current settings
        result = chip.Mcp2221_GetGpioSettings(selectedDev.getHandle(), whichToGet, currentPinFunctions, currentPinDirections, currentOutputValues);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Get Pin Configuration failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Reading current pin configuration failed. Error code: " + result + "\n");
            return;
        }else{
            
        }
        
        //***GP0
        if(jCheckBox_GP0.isSelected()){
            switch(jCmb_GP0.getSelectedItem().toString()){
                case "Output":
                    pinFunctions[0] = 0;
                    pinDirections[0] = 0;
                    switch(jCmb_GP0_val.getSelectedItem().toString()){
                        case "Low":
                            outputValues[0] = 0;
                            break;
                        case "High":
                            outputValues[1] = 1;
                            break;
                    }
                    break;
                case "Input":
                    pinFunctions[0] = 0;
                    pinDirections[0] = 1;
                    outputValues[0] = (byte)0xFF;
                    break;
                case "SSPND":
                    pinFunctions[0] = 1;
                    pinDirections[0] = (byte)0xFF;
                    outputValues[0] = (byte)0xFF;
                    break;
                case "LED UART RX":
                    pinFunctions[0] = 2;
                    pinDirections[0] = (byte)0xFF;
                    outputValues[0] = (byte)0xFF;
                    break;
            }
        }else{
            pinFunctions[0] = currentPinFunctions[0];
            pinDirections[0] = (byte)0xFF;
            outputValues[0] = (byte)0xFF;
        }
        
        //*** GP1
        if(jCheckBox_GP1.isSelected()){
            switch(jCmb_GP1.getSelectedItem().toString()){
                case "Output":
                    pinFunctions[1] = 0;
                    pinDirections[1] = 0;
                    switch(jCmb_GP1_val.getSelectedItem().toString()){
                        case "Low":
                            outputValues[1] = 0;
                            break;
                        case "High":
                            outputValues[1] = 1;
                            break;   
                    }
                    break;
                case "Input":
                    pinFunctions[1] = 0;
                    pinDirections[1] = 1;
                    outputValues[1] = (byte)0xFF;
                    break;
                case "Clock out":
                    pinFunctions[1] = 1;
                    pinDirections[1] = (byte)0xFF;
                    outputValues[1] = (byte)0xFF;
                    break;
                case "ADC1":
                    pinFunctions[1] = 2;
                    pinDirections[1] = (byte)0xFF;
                    outputValues[1] = (byte)0xFF;
                    break;
                case "LED UART TX":
                    pinFunctions[1] = 3;
                    pinDirections[1] = (byte)0xFF;
                    outputValues[1] = (byte)0xFF;
                    break;
                case "Interrupt detection":
                    pinFunctions[1] = 4;
                    pinDirections[1] = (byte)0xFF;
                    outputValues[1] = (byte)0xFF;
                    break;
            }
        }else{
            pinFunctions[1] = currentPinFunctions[1];
            pinDirections[1] = (byte)0xFF;
            outputValues[1] = (byte)0xFF;
        }
        
        //*** GP2
        if(jCheckBox_GP2.isSelected()){
            switch(jCmb_GP2.getSelectedItem().toString()){
                case "Output":
                    pinFunctions[2] = 0;
                    pinDirections[2] = 0;
                    switch(jCmb_GP2_val.getSelectedItem().toString()){
                        case "Low":
                            outputValues[2] = 0;
                            break;
                        case "High":
                            outputValues[2] = 1;
                            break;
                    }
                    break;
                case "Input":
                    pinFunctions[2] = 0;
                    pinDirections[2] = 1;
                    outputValues[2] = (byte)0xFF;
                    break;
                case "USBCFG":
                    pinFunctions[2] = 1;
                    pinDirections[2] = (byte)0xFF;
                    outputValues[2] = (byte)0xFF;
                    break;
                case "ADC2":
                    pinFunctions[2] = 2;
                    pinDirections[2] = (byte)0xFF;
                    outputValues[2] = (byte)0xFF;
                    break;
                case "DAC1":
                    pinFunctions[2] = 3;
                    pinDirections[2] = (byte)0xFF;
                    outputValues[2] = (byte)0xFF;
                    break;
            }
        }else{
            pinFunctions[2] = currentPinFunctions[2];
            pinDirections[2] = (byte)0xFF;
            outputValues[2] = (byte)0xFF;
        }
        
        //*** GP3
        if(jCheckBox_GP3.isSelected()){
            switch(jCmb_GP3.getSelectedItem().toString()){
                case "Output":
                    pinFunctions[3] = 0;
                    pinDirections[3] = 0;
                    switch(jCmb_GP3_val.getSelectedItem().toString()){
                        case "Low":
                            outputValues[3] = 0;
                            break;
                        case "High":
                            outputValues[3] = 1;
                            break;
                    }
                    break;
                case "Input":
                    pinFunctions[3] = 0;
                    pinDirections[3] = 1;
                    outputValues[3] = (byte)0xFF;
                    break;
                case "LED I2C":
                    pinFunctions[3] = 1;
                    pinDirections[3] = (byte)0xFF;
                    outputValues[3] = (byte)0xFF;
                    break;
                case "ADC3":
                    pinFunctions[3] = 2;
                    pinDirections[3] = (byte)0xFF;
                    outputValues[3] = (byte)0xFF;
                    break;
                case "DAC2":
                    pinFunctions[3] = 3;
                    pinDirections[3] = (byte)0xFF;
                    outputValues[3] = (byte)0xFF;
                    break;
            }
        }else{
            pinFunctions[3] = currentPinFunctions[3];
            pinDirections[3] = (byte)0xFF;
            outputValues[3] = (byte)0xFF;
        }
        
        
        //set new pin configuration
        result = chip.Mcp2221_SetGpioSettings(selectedDev.getHandle(), whichToSet, pinFunctions, pinDirections, outputValues);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Set Pin Configuration failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Set Pin Configuration\n");
            jTxt_result.append("\tError code: " + result + "\n");
        }else{
            jLabel_currentSts.setText(" Successful setting Pin Configuration ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            jTxt_result.append("> Set Pin Configuration:" + "\n");
            //***GP0
            if(jCheckBox_GP0.isSelected()){
                jTxt_result.append("\tGP0 = ");
                switch(pinFunctions[0]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("SSPND, ");
                        break;
                    case 2:
                        jTxt_result.append("LED UART RX, ");
                        break;
                } 
                switch(pinDirections[0]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[0]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP1
            if(jCheckBox_GP1.isSelected()){
                jTxt_result.append("\tGP1 = ");
                switch(pinFunctions[1]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("Clock Out, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC1, ");
                        break;
                    case 3:
                        jTxt_result.append("LED UART TX, ");
                        break;
                    case 4:
                        jTxt_result.append("Interrupt Detection, ");
                        break;
                }
                switch(pinDirections[1]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[1]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP2
            if(jCheckBox_GP2.isSelected()){
                jTxt_result.append("\tGP2 = ");
                switch(pinFunctions[2]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("USBFG, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC2, ");
                        break;
                    case 3:
                        jTxt_result.append("DAC1, ");
                        break;
                }
                switch(pinDirections[2]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[2]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
            //***GP3
            if(jCheckBox_GP3.isSelected()){
                jTxt_result.append("\tGP3 = ");
                switch(pinFunctions[3]){
                    case 0:
                        jTxt_result.append("GPIO, ");
                        break;
                    case 1:
                        jTxt_result.append("LED I2C, ");
                        break;
                    case 2:
                        jTxt_result.append("ADC3, ");
                        break;
                    case 3:
                        jTxt_result.append("DAC2, ");
                        break;
                }
                switch(pinDirections[3]){
                    case 0:
                        jTxt_result.append("Output, ");
                        break;
                    case 1:
                        jTxt_result.append("Input, ");
                        break;
                }
                switch(outputValues[3]){
                    case 0:
                        jTxt_result.append("Low");
                        break;
                    case 1:
                        jTxt_result.append("High");
                        break;
                }
                jTxt_result.append("\n");
            }
        }
    }
    
    //******************************* GPIO ***********************************//
    
    //LAYOUT
    private void createLayoutGPIO(){
        //Set invisible all fields used for other protocols
        jLabel_addressLen.setVisible(false);
        jCmb_addressLen.setVisible(false);
        jLabel_address.setVisible(false);
        jTxt_address.setVisible(false);
        jLabel_speed.setVisible(false);
        jTxt_speed.setVisible(false);
        jLabel_pec.setVisible(false);
        jCmb_pec.setVisible(false);
        jLabel_cmd.setVisible(false);
        jTxt_cmd.setVisible(false);
        jLabel_data.setVisible(false);
        jTxt_data.setVisible(false);
        jCmb_GP0.setVisible(false);
        jCmb_GP1.setVisible(false);
        jCmb_GP2.setVisible(false);
        jCmb_GP3.setVisible(false);
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        
        jCmb_GP0_val.setVisible(false);  
        jCmb_GP1_val.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        jCmb_GP3_val.setVisible(false);
        
        jCheckBox_GP0.setVisible(true);
        jCheckBox_GP1.setVisible(true);
        jCheckBox_GP2.setVisible(true);
        jCheckBox_GP3.setVisible(true);        
        
        //Make visible all fields used for GPIO protocol
        jBtn_GP0_out.setVisible(true);
        jBtn_GP0_in.setVisible(true);
        jBtn_GP1_out.setVisible(true);
        jBtn_GP1_in.setVisible(true);
        jBtn_GP2_out.setVisible(true);
        jBtn_GP2_in.setVisible(true);
        jBtn_GP3_out.setVisible(true);
        jBtn_GP3_in.setVisible(true);
        
        //*** Operation
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    10,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    80,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(20, 140);
        jCmb_operation.setLocation(120, 135);
        jCmb_operation.setPreferredSize(new Dimension(80, 25));
        jCmb_operation.setMinimumSize(new Dimension(80, 25));
        
        //Remove 'Read' and 'Write' items from Operation ComboBox and add 'Set' and 'Get' items
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Set");
        jCmb_operation.addItem("Get");
        
        //*** GP0 CheckBox
        jPanel_operations.add(jCheckBox_GP0);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP0,
                    200,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP0,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP0.setLocation(210, 140);
        //*** GP1 CheckBox
        jPanel_operations.add(jCheckBox_GP1);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP1,
                    300,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP1,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP1.setLocation(310, 140);
        //*** GP2 CheckBox
        jPanel_operations.add(jCheckBox_GP2);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP2,
                    400,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP2,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP2.setLocation(410, 140);
        //*** GP3 CheckBox
        jPanel_operations.add(jCheckBox_GP3);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP3,
                    500,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP3,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP3.setLocation(510, 140);
        
        repaint();
        
        if(jCmb_operation.getSelectedItem().equals("Set")){
            createLayoutGPIO_Set();
        }else if(jCmb_operation.getSelectedItem().equals("Get")){
            createLayoutGPIO_Get();
        }
    }
       
    //create layout for 'GPIO' protocol and 'Get' operation
    private void createLayoutGPIO_Set(){
        //*** GP0
        if(jCheckBox_GP0.isSelected() == true){
            jPanel_operations.add(jLabel_GP0);
            jPanel_operations.add(jBtn_GP0_out);
            jPanel_operations.add(jBtn_GP0_in);
            
            jLabel_GP0.setVisible(true);
            jBtn_GP0_out.setVisible(true);
            jBtn_GP0_in.setVisible(true);
            
            //add radion buttons in a group
            grp_dir_GP0.add(jBtn_GP0_out);
            grp_dir_GP0.add(jBtn_GP0_in);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP0,
                    10,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP0,
                    180,
                    SpringLayout.NORTH, jPanel_operations);
            jLabel_GP0.setLocation(20, 190);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP0_out,
                    50, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP0_out,
                    165, 
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP0_out.setLocation(60, 175);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP0_in, 
                    50, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP0_in,
                    195, 
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP0_in.setLocation(60, 205);
            
            repaint();
            
            //Check which radion button is selected
            if(jBtn_GP0_out.isSelected() == true){
                jCmb_GP0_val.setVisible(true);
                jPanel_operations.add(jCmb_GP0_val);
                layout.putConstraint(SpringLayout.WEST, jCmb_GP0_val,
                        120,
                        SpringLayout.WEST, jPanel_operations);
                layout.putConstraint(SpringLayout.NORTH, jCmb_GP0_val,
                        160,
                        SpringLayout.NORTH, jPanel_operations);
                jCmb_GP0_val.setLocation(130, 170);
                
                //remove items from GP0 ComboBox and add values for GPIO
                jCmb_GP0_val.removeAllItems();
                jCmb_GP0_val.addItem("No Change");
                jCmb_GP0_val.addItem("High");
                jCmb_GP0_val.addItem("Low");
                
                repaint();
            }else{
                jCmb_GP0_val.setVisible(false);
            }
        }else{
            jLabel_GP0.setVisible(false);
            jBtn_GP0_out.setVisible(false);
            jBtn_GP0_in.setVisible(false);
            jCmb_GP0_val.setVisible(false);
        }
        
        //*** GP1
        if(jCheckBox_GP1.isSelected() == true){
            jLabel_GP1.setVisible(true);
            jBtn_GP1_out.setVisible(true);
            jBtn_GP1_in.setVisible(true);
            
            jPanel_operations.add(jLabel_GP1);
            jPanel_operations.add(jBtn_GP1_out);
            jPanel_operations.add(jBtn_GP1_in);
            
            //add radio button in a group
            grp_dir_GP1.add(jBtn_GP1_out);
            grp_dir_GP1.add(jBtn_GP1_in);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP1,
                        300,
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP1,
                        180,
                        SpringLayout.NORTH, jPanel_operations);
            jLabel_GP1.setLocation(310, 190);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP1_out,
                        340, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP1_out, 
                        165, 
                        SpringLayout.NORTH, jPanel_operations);
            jBtn_GP1_out.setLocation(350, 175);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP1_in,
                        340, 
                        SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP1_in, 
                        195, 
                        SpringLayout.NORTH, jPanel_operations);
            jBtn_GP1_in.setLocation(350, 205);
            
            repaint();
            
            //check which radion button is selected
            if(jBtn_GP1_out.isSelected() == true){
                jCmb_GP1_val.setVisible(true);
                jPanel_operations.add(jCmb_GP1_val);
                layout.putConstraint(SpringLayout.WEST, jCmb_GP1_val,
                        420,
                        SpringLayout.WEST,jPanel_operations);
                layout.putConstraint(SpringLayout.NORTH, jCmb_GP1_val,
                        160,
                        SpringLayout.NORTH, jPanel_operations);     
                jCmb_GP1_val.removeAllItems();
                jCmb_GP1_val.addItem("No Change");
                jCmb_GP1_val.addItem("High");
                jCmb_GP1_val.addItem("Low");
            }else{
                jCmb_GP1_val.setVisible(false);
            }
        }else{
            jLabel_GP1.setVisible(false);
            jBtn_GP1_out.setVisible(false);
            jBtn_GP1_in.setVisible(false);
            jCmb_GP1_val.setVisible(false);
        }
        
        //*** GP2
        if(jCheckBox_GP2.isSelected() == true){
            jPanel_operations.add(jLabel_GP2);
            jPanel_operations.add(jBtn_GP2_out);
            jPanel_operations.add(jBtn_GP2_in);
            
            jLabel_GP2.setVisible(true);
            jBtn_GP2_out.setVisible(true);
            jBtn_GP2_in.setVisible(true);
            
            //add radion buttons in a group
            grp_dir_GP2.add(jBtn_GP2_out);
            grp_dir_GP2.add(jBtn_GP2_in);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP2,
                    10,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP2,
                    250,
                    SpringLayout.NORTH, jPanel_operations);
            jLabel_GP2.setLocation(20, 260);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP2_out,
                    50,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP2_out,
                    235,
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP2_out.setLocation(60, 245);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP2_in,
                    50,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP2_in,
                    265,
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP2_in.setLocation(60, 275);
            
            repaint();
            
            //check which radion button is selected
            if(jBtn_GP2_out.isSelected() == true){
                jCmb_GP2_val.setVisible(true);
                jPanel_operations.add(jCmb_GP2_val);
                layout.putConstraint(SpringLayout.WEST, jCmb_GP2_val,
                        120,
                        SpringLayout.WEST, jPanel_operations);
                layout.putConstraint(SpringLayout.NORTH, jCmb_GP2_val,
                        230, 
                        SpringLayout.NORTH, jPanel_operations);
                jCmb_GP2_val.setLocation(130, 240);
                jCmb_GP2_val.removeAllItems();
                jCmb_GP2_val.addItem("No Change");
                jCmb_GP2_val.addItem("High");
                jCmb_GP2_val.addItem("Low");
            }else{
                jCmb_GP2_val.setVisible(false);
            }
        }else{
            jLabel_GP2.setVisible(false);
            jBtn_GP2_out.setVisible(false);
            jBtn_GP2_in.setVisible(false);
            jCmb_GP2_val.setVisible(false);
        }
        
        //*** GP3
        if(jCheckBox_GP3.isSelected() == true){
            jLabel_GP3.setVisible(true);
            jBtn_GP3_out.setVisible(true);
            jBtn_GP3_in.setVisible(true);
            
            jPanel_operations.add(jLabel_GP3);
            jPanel_operations.add(jBtn_GP3_out);
            jPanel_operations.add(jBtn_GP3_in);
            
            //Add radio buttons for GP3 in a group
            grp_dir_GP3.add(jBtn_GP3_out);
            grp_dir_GP3.add(jBtn_GP3_in);
            
            layout.putConstraint(SpringLayout.WEST, jLabel_GP3,
                    300,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jLabel_GP3,
                    250,
                    SpringLayout.NORTH, jPanel_operations);
            jLabel_GP3.setLocation(310, 260);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP3_out, 
                    340,
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP3_out, 
                    235, 
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP3_out.setLocation(350, 245);
            layout.putConstraint(SpringLayout.WEST, jBtn_GP3_in, 
                    340, 
                    SpringLayout.WEST, jPanel_operations);
            layout.putConstraint(SpringLayout.NORTH, jBtn_GP3_in,
                    265,
                    SpringLayout.NORTH, jPanel_operations);
            jBtn_GP3_in.setLocation(350, 275);
            
            repaint();
            
            //check which radio button is selected
            if(jBtn_GP3_out.isSelected() == true){
                jCmb_GP3_val.setVisible(true);
                jPanel_operations.add(jCmb_GP3_val);
                layout.putConstraint(SpringLayout.WEST, jCmb_GP3_val,
                        420,
                        SpringLayout.WEST, jPanel_operations);
                layout.putConstraint(SpringLayout.NORTH, jCmb_GP3_val,
                        230,
                        SpringLayout.NORTH, jPanel_operations);
                jCmb_GP3_val.setLocation(430, 240);
                jCmb_GP3_val.removeAllItems();
                jCmb_GP3_val.addItem("No Change");
                jCmb_GP3_val.addItem("High");
                jCmb_GP3_val.addItem("Low");
            }else{
                jCmb_GP3_val.setVisible(false);
            }
        }else{
            jLabel_GP3.setVisible(false);
            jBtn_GP3_out.setVisible(false);
            jBtn_GP3_in.setVisible(false);
            jCmb_GP3_val.setVisible(false);
        }
        
        //Operation Button
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                480,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation,
                270,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setLocation(490, 280);
        jBtn_operation.setText("Set GPIO Configuration");
        
    }
    
    //create layout for 'GPIO' protocol and 'Set' operation
    private void createLayoutGPIO_Get(){
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jCmb_GP3_val.setVisible(false);
        
        //*** Operation Button
        layout.putConstraint(SpringLayout.WEST, jBtn_operation,
                465,
                SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jBtn_operation,
                245,
                SpringLayout.NORTH, jPanel_operations);
        jBtn_operation.setPreferredSize(new Dimension(160, 25));
        jBtn_operation.setMinimumSize(new Dimension(160, 25));
        
        jBtn_operation.setText("Get GPIO Configuration");
    }
 
    //Get GPIO
    private void getGPIO(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte[] gpioValues = new byte[4];
        byte[] gpioDir = new byte[4];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        //Get GPIO direction
        result = chip.Mcp2221_GetGpioDirection(selectedDev.getHandle(), gpioDir);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Get GPIO Direction failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Get GPIO Direction\n");
            jTxt_result.append("\tError code: " + result + "\n");
            return;
        }else{
            //Get GPIO values
            result = chip.Mcp2221_GetGpioValues(selectedDev.getHandle(), gpioValues);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Get GPIO Values failed ");
                jLabel_currentSts.setForeground(Color.red);
                jTxt_result.append("> Get GPIO Values\n");
                jTxt_result.append("\tError code: " + result + "\n");
            }else{
                jTxt_result.append("> Get GPIO Direction and Values: " + "\n");
                //***GP0
                if(jCheckBox_GP0.isSelected()){
                    switch(gpioDir[0]){
                        case 0:
                            switch(gpioValues[0]){
                                case 0:
                                    jTxt_result.append("\tGP0 = Output - Low" + "\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP0 = Output - High" + "\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP0 not set for GPIO operation" + "\n");
                                    break;
                            }
                            break;
                        case 1:
                            switch(gpioValues[0]){
                                case 0:
                                    jTxt_result.append("\tGP0 = Input - Low" + "\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP0 = Input - High" + "\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP0 not set for GPIO operation" + "\n");
                                    break;
                            }
                            break;
                        case (byte)0xEF:
                            jTxt_result.append("\tGP0 not set for GPIO operation" + "\n");
                            break;
                    }
                }
                //***GP1
                if(jCheckBox_GP1.isSelected()){
                    switch(gpioDir[1]){
                        case 0:
                            switch(gpioValues[1]){
                                case 0:
                                    jTxt_result.append("\tGP1 = Output - Low" + "\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP1 = Output - High" + "\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP1 not set for GPIO operation" + "\n");
                                    break;
                            }
                            break;
                        case 1:
                            switch(gpioValues[1]){
                                case 0:
                                    jTxt_result.append("\tGP1 = Input - Low" + "\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP1 = Input - High" + "\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP1 not set for GPIO operation" + "\n");
                                    break;
                            }
                            break;
                        case (byte)0xEF:
                            jTxt_result.append("\tGP1 not set for GPIO operation" + "\n");
                    }
                }
                //***GP2
                if(jCheckBox_GP2.isSelected()){
                    switch(gpioDir[2]){
                        case 0:
                            switch(gpioValues[2]){
                                case 0:
                                    jTxt_result.append("\tGP2 = Output - Low\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP2 = Output - High\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP2 not set for GPIO operation\n");
                            }
                            break;
                        case 1:
                            switch(gpioValues[2]){
                                case 0:
                                    jTxt_result.append("\tGP2 = Input - Low\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP2 = Input - High\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP2 not set for GPIO operation\n");
                            }
                            break;
                        case (byte)0xEF:
                            jTxt_result.append("\tGP2 not set for GPIO operation\n");
                            break;
                    }
                }
                //***GP3
                if(jCheckBox_GP3.isSelected()){
                    switch(gpioDir[3]){
                        case 0:
                            switch(gpioValues[3]){
                                case 0:
                                    jTxt_result.append("\tGP3 = Output - Low\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP3 = Output - High\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP3 not set for GPIO operation\n");
                                    break;
                            }
                            break;
                        case 1:
                            switch(gpioValues[3]){
                                case 0:
                                    jTxt_result.append("\tGP3 = Input - Low\n");
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP3 = Input - High\n");
                                    break;
                                case (byte)0xEE:
                                    jTxt_result.append("\tGP3 not set for GPIO operation\n");
                                    break;
                            }
                            break;
                        case (byte)0xEF:
                            jTxt_result.append("\tGP3 not set for GPIO operation\n");
                            break;
                    }
                }
            }
        }
    }
    
    //Set GPIO
    private void setGPIO(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte[] gpioDir = new byte[4];
        byte[] currentDir = new byte[4];
        byte[] gpioValues = new byte[4];
        byte[] currentValues = new byte[4];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected");
            return;
        }
        
         //============== DIRECTION ================//
        
        //*** GP0
        if(jCheckBox_GP0.isSelected()){
            if(jBtn_GP0_out.isSelected())
                gpioDir[0] = 0;
            else if(jBtn_GP0_in.isSelected())
                gpioDir[0] = 1;
            else 
                gpioDir[0] = (byte)0xFF;
        }else{
            gpioDir[0] = (byte)0xFF;
        }
        
        //*** GP1
        if(jCheckBox_GP1.isSelected()){
            if(jBtn_GP1_out.isSelected())
                gpioDir[1] = 0;
            else if(jBtn_GP1_in.isSelected())
                gpioDir[1] = 1;
            else 
                gpioDir[1] = (byte)0xFF;
        }else{
            gpioDir[1] = (byte)0xFF;
        }
        
        //*** GP2
        if(jCheckBox_GP2.isSelected()){
            if(jBtn_GP2_out.isSelected())
                gpioDir[2] = 0;
            else if(jBtn_GP2_in.isSelected())
                gpioDir[2] = 1;
            else
                gpioDir[2] = (byte)0xFF;
        }else{
            gpioDir[2] = (byte)0xFF;
        }
               
        //*** GP3
        if(jCheckBox_GP3.isSelected()){
            if(jBtn_GP3_out.isSelected())
                gpioDir[3] = 0;
            else if(jBtn_GP3_in.isSelected())
                gpioDir[3] = 1;
            else
                gpioDir[3] = (byte)0xFF;
        }else{
            gpioDir[3] = (byte)0xFF;
        }
        
        //set GPIO direction
        result = chip.Mcp2221_SetGpioDirection(selectedDev.getHandle(), gpioDir);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Set GPIO Direction failed ");
            jLabel_currentSts.setForeground(Color.red);
            
            jTxt_result.append("> Set GPIO Direction\n");
            jTxt_result.append("\tError code: " + result + "\n");
            return;
        }else{
            //============== VALUES ==================//
            
            //*** GP0
            if(gpioDir[0] == 0){    //GP0 is output
                switch(jCmb_GP0_val.getSelectedItem().toString()){
                    case "No Change":
                        gpioValues[0] = (byte)0xFF;
                        break;
                    case "Low":
                        gpioValues[0] = 0;
                        break;
                    case "High":
                        gpioValues[0] = 1;
                        break;
                }
            }else{
                gpioValues[0] = (byte)0xFF;
            }
            
            //*** GP1
            if(gpioDir[1] == 0){    //GP1 is output
                switch(jCmb_GP1_val.getSelectedItem().toString()){
                    case "No Change":
                        gpioValues[1] = (byte)0xFF;
                        break;
                    case "Low":
                        gpioValues[1] = 0;
                        break;
                    case "High":
                        gpioValues[1] = 1;
                        break;
                }
            }else{
                gpioValues[1] = (byte)0xFF;
            }
            
            //*** GP2
            if(gpioDir[2] == 0){    //GP2 is output
                switch(jCmb_GP2_val.getSelectedItem().toString()){
                    case "No Change":
                        gpioValues[2] = (byte)0xFF;
                        break;
                    case "Low":
                        gpioValues[2] = 0;
                        break;
                    case "High":
                        gpioValues[2] = 1;
                        break;
                }
            }else{
                gpioValues[2] = (byte)0xFF;
            }
            
            //*** GP3
            if(gpioDir[3] == 0){    //GP3 is output
                switch(jCmb_GP3_val.getSelectedItem().toString()){
                    case "No Change":
                        gpioValues[3] = (byte)0xFF;
                        break;
                    case "Low":
                        gpioValues[3] = 0;
                        break;
                    case "High":
                        gpioValues[3] = 1;
                        break;
                }
            }else{
                gpioValues[3] = (byte)0xFF;
            }
            
            //set GPIO values
            result = chip.Mcp2221_SetGpioValues(selectedDev.getHandle(), gpioValues);
            if(result != Constants.E_NO_ERR){
                jLabel_currentSts.setText(" Set GPIO Values failed ");
                jLabel_currentSts.setForeground(Color.red);
                
                jTxt_result.append("> Set GPIO Values\n");
                jTxt_result.append("\tError code: " + result + "\n");
                return;
            }else{
                //get current settings to display in Result TextField
                result = chip.Mcp2221_GetGpioDirection(selectedDev.getHandle(), currentDir);
                if(result != Constants.E_NO_ERR){
                    jLabel_currentSts.setText(" Get current GPIO Direction failed ");
                    jLabel_currentSts.setForeground(Color.red);
                    
                    jTxt_result.append("> Get current GPIO Direction\n");
                    jTxt_result.append("\tError code: " + result + "\n");
                    return;
                }else{
                    result = chip.Mcp2221_GetGpioValues(selectedDev.getHandle(), currentValues);
                    if(result != Constants.E_NO_ERR){
                        jLabel_currentSts.setText(" Get current GPIO values ");
                        jLabel_currentSts.setForeground(Color.red);
                        
                        jTxt_result.append("> Get current GPIO Values\n");
                        jTxt_result.append("\tError code: " + result  + "\n");
                        return;
                    }else{
                        jLabel_currentSts.setText(" Successful setting GPIO direction and values ");
                        jLabel_currentSts.setForeground(new Color(0, 170, 0));
                        
                        jTxt_result.append("> Set GPIO Direction and Values\n");
                        
                        //display result in text field
                        
                        //*** GP0
                        if(jCheckBox_GP0.isSelected()){
                            switch(currentDir[0]){
                                case 0:
                                    jTxt_result.append("\tGP0 = Output - ");
                                    switch(currentValues[0]){
                                        case 0:
                                            jTxt_result.append("Low\n");
                                            break;
                                        case 1:
                                            jTxt_result.append("High\n");
                                            break;
                                    }
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP0 = Input\n");
                                    break;
                                case (byte)0xEF:
                                    jTxt_result.append("\tGP0 not set for GPIO operation\n");
                            }
                        }
                        
                        //*** GP1
                        if(jCheckBox_GP1.isSelected()){
                            switch(currentDir[1]){
                                case 0:
                                    jTxt_result.append("\tGP1 = Output - ");
                                    switch(currentValues[1]){
                                        case 0:
                                            jTxt_result.append("Low\n");
                                            break;
                                        case 1:
                                            jTxt_result.append("High\n");
                                            break;
                                    }
                                break;
                                case 1:
                                    jTxt_result.append("\tGP1 = Input\n");
                                    break;
                                case (byte)0xEF:
                                    jTxt_result.append("\tGP1 not set for GPIO operation\n");
                                    break;
                            }
                        }
                        
                        //*** GP2
                        if(jCheckBox_GP2.isSelected()){
                            switch(currentDir[2]){
                                case 0:
                                    jTxt_result.append("\tGP2 = Output - ");
                                    switch(currentValues[2]){
                                        case 0:
                                            jTxt_result.append("Low\n");
                                            break;
                                        case 1:
                                            jTxt_result.append("High\n");
                                            break;
                                    }
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP2 = Input\n");
                                    break;
                                case (byte)0xEF:
                                    jTxt_result.append("\tGP2 not set for GPIO operation\n");
                                    break;
                            }
                        }
                        
                        //*** GP3
                        if(jCheckBox_GP3.isSelected()){
                            switch(currentDir[3]){
                                case 0:
                                    jTxt_result.append("\tGP3 = Output - ");
                                    switch(currentValues[3]){
                                        case 0:
                                            jTxt_result.append("Low\n");
                                            break;
                                        case 1:
                                            jTxt_result.append("High\n");
                                            break;
                                    }
                                    break;
                                case 1:
                                    jTxt_result.append("\tGP1 = Input\n");
                                    break;
                                case (byte)0xEF:
                                    jTxt_result.append("\tGP3 not set for GPIO operation\n");
                                    break;
                            }
                        }    
                    }
                }
            }
        }
    }
    
    
    //******************************** DAC ***********************************//
    
    //LAYOUT
    private void createLayoutDAC(){    
        //Make invisible all fields used for other protocols
        jLabel_addressLen.setVisible(false);
        jCmb_addressLen.setVisible(false);
        jLabel_address.setVisible(false);
        jTxt_address.setVisible(false);
        jLabel_speed.setVisible(false);
        jTxt_speed.setVisible(false);
        jLabel_pec.setVisible(false);
        jCmb_pec.setVisible(false);
        jLabel_cmd.setVisible(false);
        jTxt_cmd.setVisible(false);
        jLabel_data.setVisible(false);
        jTxt_data.setVisible(false);    
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        jCmb_GP3_val.setVisible(false);
                
        //*** Operation
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    10,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    70,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(20, 140);
        jCmb_operation.setLocation(120, 135);
        jCmb_operation.setPreferredSize(new Dimension(100, 25));
        jCmb_operation.setMinimumSize(new Dimension(100, 25));
        
        //Remove 'Read' and 'Write' items from Operation ComboBox and add 'Set' and 'Get' items
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Set Vref");
        jCmb_operation.addItem("Get Vref");
        jCmb_operation.addItem("Set Value");
        jCmb_operation.addItem("Get Value");
        
        createLayout_SetVref();        
    }
    
    //create layout for DAC Set Value
    private void createLayoutDAC_Set(){
        //Make invisible some fields used for other protocols
        jCmb_vref.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        
        jLabel_vref.setVisible(true);
        jLabel_vref.setText("DAC Value:");
        jTxt_dac.setVisible(true);
        
        jPanel_operations.add(jTxt_dac);
        layout.putConstraint(SpringLayout.WEST, jTxt_dac, 
                    70, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jTxt_dac, 
                    175, 
                    SpringLayout.NORTH, jPanel_operations);
        jTxt_dac.setLocation(80, 185);
        
        jTxt_dac.setPreferredSize(new Dimension(100, 25));
        jTxt_dac.setMinimumSize(new Dimension(100, 25));
        
        //*** Operation Button
        jBtn_operation.setText("Set DAC Value");
        repaint();
    }
    
    //create layout for DAC Get Value
    private void createLayoutDAC_Get(){
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        
        //*** Operation Button
        jBtn_operation.setText("Get DAC Value");
    }
    
    //Get DAC value
    private void getDacValue(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte whichToGet = Constants.RUNTIME_SETTINGS;
        byte[] dacValue = new byte[1];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }           
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        
        result = chip.Mcp2221_GetDacValue(selectedDev.getHandle(), whichToGet, dacValue);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Get DAC value failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Get DAC value\n");
            jTxt_result.append("\tError code: " + result + "\n");
            return;
        }else{
            jLabel_currentSts.setText(" Successful getting DAC value");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            jTxt_result.append("> Get DAC value\n");
            jTxt_result.append("\tValue = " + dacValue[0] + "\n");
        }
        
        
        
        
        
        
        
        
    }
    
    //Set DAC value
    private void setDacValue(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte whichToSet = Constants.RUNTIME_SETTINGS;
        byte dacValue;
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        try{
            int tempDacValue = Integer.parseInt(jTxt_dac.getText());
            if(tempDacValue < 0 || tempDacValue > 31){
                JOptionPane.showMessageDialog(null, "Invalid DAC value. It must be between 0 and 31.");
                return;
            }else{
                dacValue = (byte)tempDacValue;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Invalid input for DAC value.");
            return;
        }
        
        result = chip.Mcp2221_SetDacValue(selectedDev.getHandle(), whichToSet, dacValue);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Set DAC value failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Set DAC value\n");
            jTxt_result.append("\tError code: " + result + "\n");
            return;
        }else{
            jLabel_currentSts.setText(" Successful setting DAC value ");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            jTxt_result.append("> Set DAC Value \n");
            jTxt_result.append("\t DAC Value = " + dacValue + "\n");
        }
        
        
        
        
    }
    
    
    
    //******************************** ADC ***********************************//
    
    //LAYOUT
    private void createLayoutADC(){
        //Make invisible all fields used for other protocols
        jLabel_addressLen.setVisible(false);
        jCmb_addressLen.setVisible(false);
        jLabel_address.setVisible(false);
        jTxt_address.setVisible(false);
        jLabel_speed.setVisible(false);
        jTxt_speed.setVisible(false);
        jLabel_pec.setVisible(false);
        jCmb_pec.setVisible(false);
        jLabel_cmd.setVisible(false);
        jTxt_cmd.setVisible(false);
        jLabel_data.setVisible(false);
        jTxt_data.setVisible(false);    
        jLabel_GP0.setVisible(false);
        jCmb_GP0.setVisible(false);
        jBtn_GP0_out.setVisible(false);
        jBtn_GP0_in.setVisible(false);
        jLabel_GP1.setVisible(false);
        jCmb_GP1.setVisible(false);
        jBtn_GP1_out.setVisible(false);
        jBtn_GP1_in.setVisible(false);
        jLabel_GP2.setVisible(false);
        jCmb_GP2.setVisible(false);
        jBtn_GP2_out.setVisible(false);
        jBtn_GP2_in.setVisible(false);
        jLabel_GP3.setVisible(false);
        jCmb_GP3.setVisible(false);
        jBtn_GP3_out.setVisible(false);
        jBtn_GP3_in.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        jCmb_GP0_val.setVisible(false);
        jCmb_GP1_val.setVisible(false);
        jCmb_GP2_val.setVisible(false);
        jCmb_GP3_val.setVisible(false);
                
        
        //*** Operation
        layout.putConstraint(SpringLayout.WEST, jLabel_operation,
                    10,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_operation,
                    130,
                    SpringLayout.NORTH, jPanel_operations);
        layout.putConstraint(SpringLayout.WEST, jCmb_operation,
                    70,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_operation,
                    125,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_operation.setLocation(20, 140);
        jCmb_operation.setLocation(120, 135);
        jCmb_operation.setPreferredSize(new Dimension(100, 25));
        jCmb_operation.setMinimumSize(new Dimension(100, 25));
        
        //Remove 'Read' and 'Write' items from Operation ComboBox and add 'Set' and 'Get' items
        jCmb_operation.removeAllItems();
        jCmb_operation.addItem("Set Vref");
        jCmb_operation.addItem("Get Vref");
        jCmb_operation.addItem("Get Data");
        
        createLayout_SetVref();
    }
          
    //create layout for ADC Get Data 
    private void createLayoutADC_GetData(){
        jLabel_vref.setVisible(true);
        jCmb_vref.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(true);
        jCheckBox_GP2.setVisible(true);
        jCheckBox_GP3.setVisible(true);
        

        jLabel_vref.setText("Get ADC Data: ");
        
        // GP1
        jPanel_operations.add(jCheckBox_GP1);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP1, 
                    150, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP1, 
                    180, 
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP1.setLocation(160, 190);
        // GP2
        jPanel_operations.add(jCheckBox_GP2);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP2, 
                    270, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP2, 
                    180, 
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP2.setLocation(280, 190);
        // GP3
        jPanel_operations.add(jCheckBox_GP3);
        layout.putConstraint(SpringLayout.WEST, jCheckBox_GP3, 
                    380, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCheckBox_GP3, 
                    180, 
                    SpringLayout.NORTH, jPanel_operations);
        jCheckBox_GP3.setLocation(390, 190);
        
        //*** Operation Button
        jBtn_operation.setText("Get ADC Data");
        
        repaint();
    } 

    //create layout for Set voltage reference
    private void createLayout_SetVref(){
        jPanel_operations.add(jLabel_vref);
        jPanel_operations.add(jCmb_vref);
        
        jLabel_vref.setVisible(true);
        jLabel_vref.setText("Voltage Reference:");
        jCmb_vref.setVisible(true);
        
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        
        layout.putConstraint(SpringLayout.WEST, jLabel_vref,
                    10,
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jLabel_vref,
                    180,
                    SpringLayout.NORTH, jPanel_operations);
        jLabel_vref.setLocation(20, 190);
        layout.putConstraint(SpringLayout.WEST, jCmb_vref,
                    120, 
                    SpringLayout.WEST, jPanel_operations);
        layout.putConstraint(SpringLayout.NORTH, jCmb_vref,
                    175,
                    SpringLayout.NORTH, jPanel_operations);
        jCmb_vref.setLocation(130, 185);
        
        jCmb_vref.setPreferredSize(new Dimension(100, 25));
        jCmb_vref.setMinimumSize(new Dimension(100, 25));
        
        //*** Operation Button
        if(jCmb_protocol.getSelectedItem().equals("DAC")){
            jBtn_operation.setText("Set Vref DAC");
        } else if(jCmb_protocol.getSelectedItem().equals("ADC")){
            jBtn_operation.setText("Set Vref ADC");
        }
        
        repaint();
    }
    
    //create layout for Get voltage reference
    private void createLayout_GetVref(){
        jLabel_vref.setVisible(false);
        jCmb_vref.setVisible(false);
        jTxt_dac.setVisible(false);
        jCheckBox_GP0.setVisible(false);
        jCheckBox_GP1.setVisible(false);
        jCheckBox_GP2.setVisible(false);
        jCheckBox_GP3.setVisible(false);
        
        //*** Operation Button
        if(jCmb_protocol.getSelectedItem().equals("DAC")){
            jBtn_operation.setText("Get Vref DAC");
        } else if(jCmb_protocol.getSelectedItem().equals("ADC")){
            jBtn_operation.setText("Get Vref ADC");
        }
    }
    
    //Get Vref values for ADC or DAC
    private void getVref(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte whichToGet = Constants.RUNTIME_SETTINGS;
        byte[] vref = new byte[1];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev:listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        
        //Check if it must be get the Vref for ADC or DAC
        switch(jCmb_protocol.getSelectedItem().toString()){
            case "DAC":
                vref = new byte[1];
                result = chip.Mcp2221_GetDacVref(selectedDev.getHandle(),whichToGet, vref);
                if(result != Constants.E_NO_ERR){
                    jLabel_currentSts.setText(" Get DAC Vref failed ");
                    jLabel_currentSts.setForeground(Color.red);
                    jTxt_result.append("> Get DAC Vref\n");
                    jTxt_result.append("\tError code: " + result + "\n");
                    return;
                }else{
                    jLabel_currentSts.setText(" Successful getting DAC Vref ");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));
                    jTxt_result.append("> Get DAC Vref\n");
                    switch(vref[0]){
                        case 0:
                            jTxt_result.append("\tDAC Vref = Vdd\n");
                            break;
                        case 1:
                            jTxt_result.append("\tDAC Vref = 1.024V\n");
                            break;
                        case 2:
                            jTxt_result.append("\tDAC Vref = 2.048V\n");
                            break;
                        case 3:
                            jTxt_result.append("\tDAC Vref = 4.096V\n");
                            break;
                    }
                }
                break;
            case "ADC":
                vref = new byte[1];
                result = chip.Mcp2221_GetAdcVref(selectedDev.getHandle(), whichToGet, vref);
                if(result != Constants.E_NO_ERR){
                    jLabel_currentSts.setText(" Get ADC Vref failed ");
                    jLabel_currentSts.setForeground(Color.red);
                    jTxt_result.append("> Get ADC Vref\n");
                    jTxt_result.append("\tError code: " + result + "\n");
                    return;
                }else{
                    jLabel_currentSts.setText(" Successful getting ADC Vref ");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));
                    jTxt_result.append("> Get ADC Vref \n");
                    switch(vref[0]){
                        case 0:
                            jTxt_result.append("\tADC Vref = Vdd\n");
                            break;
                        case 1:
                            jTxt_result.append("\tADC Vref = 1.024V\n");
                            break;
                        case 2:
                            jTxt_result.append("\tADC Vref = 2.048V\n");
                            break;
                        case 3:
                            jTxt_result.append("\tADC Vref = 4.096V\n");
                            break;
                    }
                }
                break;
        }    
    }
    
    //Set Vref values for ADc or DAC
    private void setVref(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        byte whichToSet = Constants.RUNTIME_SETTINGS;
        byte vref = 0;
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected");
            return;
        }
        
        
        switch(jCmb_protocol.getSelectedItem().toString()){
            case "DAC":
                switch(jCmb_vref.getSelectedItem().toString()){
                    case "Vdd":
                        vref = 0;
                        break;
                    case "1.024 V":
                        vref = 1;
                        break;
                    case "2.048 V":
                        vref = 2;
                        break;
                    case "4.096 V":
                        vref = 3;
                        break;
                }
                result = chip.Mcp2221_SetDacVref(selectedDev.getHandle(), whichToSet, vref);
                if(result != Constants.E_NO_ERR){
                    jLabel_currentSts.setText(" Set DAC Vref failed ");
                    jLabel_currentSts.setForeground(Color.red);
                    jTxt_result.append("> Set DAC Vref\n");
                    jTxt_result.append("\tError code: " + result + "\n");
                    return;
                }else{
                    jLabel_currentSts.setText(" Successful setting DAC Vref ");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));
                    jTxt_result.append("> Set DAC Vref\n");
                    switch(vref){
                        case 0:
                            jTxt_result.append("\t DAC Vref = Vdd\n");
                            break;
                        case 1:
                            jTxt_result.append("\t DAC Vref = 1.024V\n");
                            break;
                        case 2:
                            jTxt_result.append("\t DAC Vref = 2.048V\n");
                            break;
                        case 3:
                            jTxt_result.append("\t DAC Vref = 4.096V\n");
                            break;
                    }
                }
                break;
            case "ADC": 
                String str = jCmb_vref.getSelectedItem().toString();
                switch(jCmb_vref.getSelectedItem().toString()){
                    case "Vdd":
                        vref = 0;
                        break;
                    case "1.024 V":
                        vref = 1;
                        break;
                    case "2.048 V":
                        vref = 2;
                        break;
                    case "4.096 V":
                        vref = 3;
                        break;                    
                }
                result = chip.Mcp2221_SetAdcVref(selectedDev.getHandle(), whichToSet, vref);
                if(result != Constants.E_NO_ERR){
                    jLabel_currentSts.setText(" Set ADC Vref failed ");
                    jLabel_currentSts.setForeground(Color.red);
                    jTxt_result.append("> Set ADC Vref\n");
                    jTxt_result.append("\tError code: " + result + "\n");
                    return;
                }else{
                    jLabel_currentSts.setText(" Successful setting ADC Vref");
                    jLabel_currentSts.setForeground(new Color(0, 170, 0));
                    jTxt_result.append("> Set ADC Vref \n");
                    switch(vref){
                        case 0:
                            jTxt_result.append("\t ADC Vref = Vdd\n");
                            break;
                        case 1:
                            jTxt_result.append("\t ADC Vref = 1.024V\n");
                            break;
                        case 2:
                            jTxt_result.append("\t ADC Vref = 2.048V\n");
                            break;
                        case 3:
                            jTxt_result.append("\t ADC Vref = 4.096V\n");
                            break;
                    }
                }
                break;
        }
    }
    
    //Get ADC value
    private void getAdcData(){
        int result = -1;
        Device selectedDev = new Device();
        selectedDev.setHandle(-1);
        int[] adcData = new int[3];
        
        //Selected device
        if(jCmb_selectDev.getSelectedItem() != null){
            String tempSerialNumber = jCmb_selectDev.getSelectedItem().toString();
            for(Device dev: listOpenedDev){
                if(dev.getSerialNumber().equals(tempSerialNumber))
                    selectedDev = dev;
            }
            if(selectedDev.getHandle() == -1){
                JOptionPane.showMessageDialog(null, "Invalid handle for selected device.");
                return;
            }
        }else{
            JOptionPane.showMessageDialog(null, "An opened device must be selected.");
            return;
        }
        
        result = chip.Mcp2221_GetAdcData(selectedDev.getHandle(), adcData);
        if(result != Constants.E_NO_ERR){
            jLabel_currentSts.setText(" Get ADC data failed ");
            jLabel_currentSts.setForeground(Color.red);
            jTxt_result.append("> Get ADC Data\n");
            jTxt_result.append("\tError code: " + result + "\n");
            return;
        }else{
            jLabel_currentSts.setText(" Successful setting ADC Data");
            jLabel_currentSts.setForeground(new Color(0, 170, 0));
            jTxt_result.append("> Get ADC Data: \n");
            if(jCheckBox_GP1.isSelected())
                jTxt_result.append("\tGP1 = " + adcData[0] + "\n");
            if(jCheckBox_GP2.isSelected())
                jTxt_result.append("\tGP2 = " + adcData[1] + "\n");
            if(jCheckBox_GP3.isSelected())
                jTxt_result.append("\tGP3 = " + adcData[2] + "\n");
        }
    }
    
    
    //get initial number of connected devices with DEFAULT VID and PID
    private void getInitialDev(){   
        int result = -1;
        
        int initNoDev = 0;
        StringBuilder str = new StringBuilder();
        
        //Load DLL
        result = chip.Mcp2221_LoadDll();
        if(result != 0){
            JOptionPane.showMessageDialog(null, "MCP2221 Native DLL  could not be load");
        }else{
            initNoDev = chip.Mcp2221_GetConnectedDevices(vid, pid);
            if(initNoDev > 0){
                Main.numberOfDevices = initNoDev;
                str.append(initNoDev);
                if(initNoDev == 1){
                    jLabel_noDev.setText(numberOfDevices + " Device Connected (VID: " + jTxt_VID.getText() + ",   PID: " + jTxt_PID.getText() + ")");
                }else{
                    jLabel_noDev.setText(numberOfDevices + " Devices Connected (VID: " + jTxt_VID.getText() + ",   PID: " + jTxt_PID.getText() + ")");
                }    
//                jLabel_currentSts.setText("Connected");
//                jLabel_currentSts.setForeground(new Color(0, 204, 0));
                
                //Put all connected devices in a comboBox
                String tempSerialNumber = new String();
                long tempMcpHandle;
                for(int i=0; i<initNoDev; i++){
                    tempSerialNumber = new String();
                    tempMcpHandle = chip.Mcp2221_OpenByIndex(vid, pid, i);
                    byte snEnumEnabled[] = new byte[1];
                    result = chip.Mcp2221_GetSerialNumberEnumerationEnable(tempMcpHandle, snEnumEnabled);
                    if(result == 0){
                        if(snEnumEnabled[0] == 1){
                            tempSerialNumber = chip.Mcp2221_GetSerialNumberDescriptor(tempMcpHandle);
                            jCmb_listDev.addItem(tempSerialNumber);
                        }else if(snEnumEnabled[0] == 0){
                            jCmb_listDev.addItem("Device " + (i + 1));
                        }
                    }else{
                        jCmb_listDev.addItem("Device " + (i + 1));
                    }              
                    result = chip.Mcp2221_Close(tempMcpHandle);
                }            
            }else{
                Main.numberOfDevices = 0;
                jLabel_noDev.setText(numberOfDevices + " Devices Connected (VID: " + jTxt_VID.getText() + ",   PID: " + jTxt_PID.getText() + ")");
//                jLabel_currentSts.setText("Not Connected");
//                jLabel_currentSts.setForeground(Color.red);
                
                jCmb_listDev.removeAllItems();
            }
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton jBtn_GP0_in;
    private javax.swing.JRadioButton jBtn_GP0_out;
    private javax.swing.JRadioButton jBtn_GP1_in;
    private javax.swing.JRadioButton jBtn_GP1_out;
    private javax.swing.JRadioButton jBtn_GP2_in;
    private javax.swing.JRadioButton jBtn_GP2_out;
    private javax.swing.JRadioButton jBtn_GP3_in;
    private javax.swing.JRadioButton jBtn_GP3_out;
    private javax.swing.JButton jBtn_clear;
    private javax.swing.JButton jBtn_close;
    private javax.swing.JButton jBtn_closeAll;
    private javax.swing.JButton jBtn_openConn;
    private javax.swing.JButton jBtn_operation;
    private javax.swing.JCheckBox jCheckBox_GP0;
    private javax.swing.JCheckBox jCheckBox_GP1;
    private javax.swing.JCheckBox jCheckBox_GP2;
    private javax.swing.JCheckBox jCheckBox_GP3;
    private javax.swing.JComboBox jCmb_GP0;
    private javax.swing.JComboBox jCmb_GP0_val;
    private javax.swing.JComboBox jCmb_GP1;
    private javax.swing.JComboBox jCmb_GP1_val;
    private javax.swing.JComboBox jCmb_GP2;
    private javax.swing.JComboBox jCmb_GP2_val;
    private javax.swing.JComboBox jCmb_GP3;
    private javax.swing.JComboBox jCmb_GP3_val;
    private javax.swing.JComboBox jCmb_addressLen;
    public static javax.swing.JComboBox jCmb_listDev;
    private javax.swing.JComboBox jCmb_operation;
    private javax.swing.JComboBox jCmb_pec;
    public static javax.swing.JComboBox jCmb_protocol;
    public static javax.swing.JComboBox jCmb_selectDev;
    private javax.swing.JComboBox jCmb_vref;
    private javax.swing.JLabel jLabel_GP0;
    private javax.swing.JLabel jLabel_GP1;
    private javax.swing.JLabel jLabel_GP2;
    private javax.swing.JLabel jLabel_GP3;
    private javax.swing.JLabel jLabel_PID;
    private javax.swing.JLabel jLabel_VID;
    private javax.swing.JLabel jLabel_address;
    private javax.swing.JLabel jLabel_addressLen;
    private javax.swing.JLabel jLabel_cmd;
    public static javax.swing.JLabel jLabel_currentSts;
    private javax.swing.JLabel jLabel_data;
    private javax.swing.JLabel jLabel_listDev;
    public static javax.swing.JLabel jLabel_noDev;
    private javax.swing.JLabel jLabel_openedConn;
    private javax.swing.JLabel jLabel_operation;
    private javax.swing.JLabel jLabel_pec;
    private javax.swing.JLabel jLabel_protocol;
    private javax.swing.JLabel jLabel_selectDev;
    private javax.swing.JLabel jLabel_speed;
    private javax.swing.JLabel jLabel_sts;
    private javax.swing.JLabel jLabel_vref;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem_exit;
    private javax.swing.JPanel jPanel_devices;
    private javax.swing.JPanel jPanel_operations;
    public static javax.swing.JPanel jPanel_result;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTextField jTxt_PID;
    public static javax.swing.JTextField jTxt_VID;
    public javax.swing.JTextField jTxt_address;
    public javax.swing.JTextField jTxt_cmd;
    private javax.swing.JTextField jTxt_dac;
    public static javax.swing.JTextField jTxt_data;
    public static javax.swing.JTextArea jTxt_result;
    public javax.swing.JTextField jTxt_speed;
    public static java.awt.List list_connDev;
    // End of variables declaration//GEN-END:variables
}
