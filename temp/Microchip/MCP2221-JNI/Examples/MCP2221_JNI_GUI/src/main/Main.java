//©[2016] Microchip Technology Inc.and its subsidiaries.You may use this software and any derivatives exclusively with Microchip products.
//
//THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
//WARRANTIES OF NON - INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY 
//OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
//IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER
//RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.TO THE FULLEST EXTENT ALLOWED
//BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
//DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
//MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

package main;

import com.microchip.mcp2221.HidFeatures;
import gui.MainForm;
import static gui.MainForm.jCmb_listDev;
import static gui.MainForm.pid;
import static gui.MainForm.vid;
import java.util.Iterator;
import javax.swing.JOptionPane;

/**
 *
 * @author m16142
 */
public class Main {
    public static Thread thread;
    public static int numberOfDevices = 0;
    public static int result;
    
    static HidFeatures chip = new HidFeatures();

      /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
            
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
        
        result = chip.Mcp2221_LoadDll();
        if(result != 0){
            JOptionPane.showMessageDialog(null, "MCP2221 Native DLL  could not be load");
        }
        
        //GET NUMBER OF CONNECTED DEVICES 
        thread = new Thread(){
            public void run(){
                 while (true){
                    if(result == 0){
                        listDevices();
                        try
                        {
                            Thread.sleep(50);
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                        }    
                    }
                }
            }
        };
        thread.start();
    }
    
    
    
    public static void listDevices(){
        int tempNoDev = chip.Mcp2221_GetConnectedDevices(MainForm.vid, MainForm.pid);
        //if no error occured
        if(tempNoDev > 0){
            //if number of connected devices was changed
            if(tempNoDev != numberOfDevices || MainForm.flag_closeAllConnections == true){
                MainForm.flag_closeAllConnections = false;
                numberOfDevices = tempNoDev;
                StringBuilder str_dev = new StringBuilder();
                str_dev.append(numberOfDevices);
                if(MainForm.jTxt_data != null){
                    if(numberOfDevices == 1){
                        MainForm.jLabel_noDev.setText(numberOfDevices + " Device Connected (VID: " + MainForm.jTxt_VID.getText() + ",   PID: " + MainForm.jTxt_PID.getText() + ")");
                    }else{
                        MainForm.jLabel_noDev.setText(numberOfDevices + " Devices Connected (VID: " + MainForm.jTxt_VID.getText() + ",   PID: " + MainForm.jTxt_PID.getText() + ")");
                    }                 
                }
                
                //Put all connected devices in a ComboBox
                String tempSerialNumber = new String();
                long tempMcpHandle = -1;
                
                if(MainForm.jCmb_listDev != null){
                    MainForm.jCmb_listDev.removeAllItems();
                    for(int i = 0; i < numberOfDevices; i++){
                        tempSerialNumber = new String();
                        tempMcpHandle = chip.Mcp2221_OpenByIndex(vid, pid, i);

                        if(tempMcpHandle != -1){
                            byte snEnumEnabled[] = new byte[1];
                            result = chip.Mcp2221_GetSerialNumberEnumerationEnable(tempMcpHandle, snEnumEnabled);
                            if(result == 0){   
                                if(snEnumEnabled[0] == 1){
                                    //if serial number enumeration is enabled, display device using its serial number
                                    tempSerialNumber = chip.Mcp2221_GetSerialNumberDescriptor(tempMcpHandle);
                                    jCmb_listDev.addItem(tempSerialNumber);
                                }else if(snEnumEnabled[0] == 0){
                                    //if serial number enumeration is disable, display device using its index
                                    jCmb_listDev.addItem("Device " + (i + 1));
                                    String str = "Device " + (i + 1);
                                }
                            }else{
                                jCmb_listDev.addItem("Device " + (i + 1));
                                String str = "Device " + (i + 1);
                            }
                            result = chip.Mcp2221_Close(tempMcpHandle);
                        }else{
                            
                        }
                    }
                    
                    if(MainForm.listOpenedDev != null){
                        //check if opened connections are still available
                        Iterator<Device> iterator = MainForm.listOpenedDev.iterator();
                        while(iterator.hasNext()){
                            Device dev = iterator.next();
                            String testSN = chip.Mcp2221_GetSerialNumberDescriptor(dev.getHandle());
                            if(testSN == null){
                                result = chip.Mcp2221_GetLastError();
                                if(result != 0){
                                    result = chip.Mcp2221_Close(dev.getHandle());
                                    iterator.remove();                            
                                    MainForm.list_connDev.remove(dev.getSerialNumber());
                                }
                            }
                        }
                    }
                }   
            }
        }else{
            if(MainForm.jLabel_currentSts != null){
                numberOfDevices = 0;
                MainForm.jLabel_noDev.setText(numberOfDevices + " Devices Connected (VID: " + MainForm.jTxt_VID.getText() + ",   PID: " + MainForm.jTxt_PID.getText() + ")");
                
                //remove all items from List Devices ComboBox
                if(MainForm.jCmb_listDev != null){
                    MainForm.jCmb_listDev.removeAllItems();
                } 
                
                if(MainForm.listOpenedDev != null){
                    //check if opened connections are still available
                    Iterator<Device> iterator = MainForm.listOpenedDev.iterator();
                    while(iterator.hasNext()){
                        Device dev = iterator.next();
                        String testSN = chip.Mcp2221_GetSerialNumberDescriptor(dev.getHandle());
                        if(testSN == null){
                            result = chip.Mcp2221_GetLastError();
                            if(result != 0){
                                result = chip.Mcp2221_Close(dev.getHandle());
                                iterator.remove();                            
                                MainForm.list_connDev.remove(dev.getSerialNumber());
                            }
                        }
                    }
                }
            } 
        }
    }
}


