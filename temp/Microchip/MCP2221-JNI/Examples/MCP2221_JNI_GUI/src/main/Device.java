//©[2016] Microchip Technology Inc.and its subsidiaries.You may use this software and any derivatives exclusively with Microchip products.
//
//THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
//WARRANTIES OF NON - INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY 
//OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
//IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER
//RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.TO THE FULLEST EXTENT ALLOWED
//BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
//DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
//MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

package main;

public class Device {
    private int index;
    private String serialNumber;
    private long handle;
    
    public Device(){
        
    }
    
    public Device(int index, String serialNumber, long handle){
        this.index = index;
        this.serialNumber = serialNumber;
        this.handle = handle;
    }
    
    public Device(int index, long handle){
        this.index = index;
        this.handle = handle;
    }
    
    public Device(String serialNumber, long handle){
        this.serialNumber = serialNumber;
        this.handle = handle;
    }
    
    public Device(String serialNumber){
        this.serialNumber = serialNumber;
    }
    
    public Device(int index){
        this.index = index;
    }
    
    //GET and SET for index
    public void setIndex(int index){
        this.index = index;
    }
     public int getIndex(){
         return this.index;
     }
     
     //GET and SET for serial number
     public void setSerialNumber(String serialNumber){
         this.serialNumber = serialNumber;
     }
     
     public String getSerialNumber(){
         return this.serialNumber;
     }
     
     //GET and SET for handle
     public void setHandle(long handle){
         this.handle = handle;
     }
     
     public long getHandle(){
         return this.handle;
     }
    
}
