/*
 * Copyright 2014 Mapzen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wade.smartlauncher.util

import android.annotation.TargetApi
import android.app.Activity
import android.app.Application
import android.content.Context
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log

import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.Locale

class Speakerbox(private val application: Application) : TextToSpeech.OnInitListener {

    val textToSpeech: TextToSpeech = TextToSpeech(application, this)

    /**
     * Callbacks are registered for upon initialization. Set an activity on the Speakerbox
     * object to have this class take care of shutting down the TextToSpeech object or register
     * for [android.app.Application.ActivityLifecycleCallbacks] in your application and
     */
    val callbacks: Application.ActivityLifecycleCallbacks

    /**
     * If set, this class will shut itself down when the activity is destroyed. Only set an
     * activity if you want the Speakerbox's state to be tied to the activity lifecycle
     */
    private var activity: Activity? = null

    private var initialized = false
    var isMuted = false
        private set
    private var playOnInit: String? = null
    private var queueMode = TextToSpeech.QUEUE_FLUSH

    private val samples = LinkedHashMap<String, String>()
    private val unwantedPhrases = ArrayList<String>()

    private val onStartRunnables = HashMap<String, Runnable>()
    private val onDoneRunnables = HashMap<String, Runnable>()
    private val onErrorRunnables = HashMap<String, Runnable>()

    internal var audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> textToSpeech.setPitch(FOCUS_PITCH)
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> textToSpeech.setPitch(DUCK_PITCH)
        }
    }

    internal var utteranceProgressListener: UtteranceProgressListener = object : UtteranceProgressListener() {
        override fun onStart(utteranceId: String) {
            detectAndRun(utteranceId, onStartRunnables)
        }

        override fun onDone(utteranceId: String) {
            if (detectAndRun(utteranceId, onDoneRunnables)) {
                // because either onDone or onError will be called for an utteranceId, cleanup other
                if (onErrorRunnables.containsKey(utteranceId)) {
                    onErrorRunnables.remove(utteranceId)
                }
            }
        }

        override fun onError(utteranceId: String) {
            if (detectAndRun(utteranceId, onErrorRunnables)) {
                // because either onDone or onError will be called for an utteranceId, cleanup other
                if (onDoneRunnables.containsKey(utteranceId)) {
                    onDoneRunnables.remove(utteranceId)
                }
            }
        }
    }

    val availableLanguages: Set<Locale>
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        get() = textToSpeech.availableLanguages

    init {
        this.textToSpeech.setOnUtteranceProgressListener(utteranceProgressListener)
        this.callbacks = object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle) {}

            override fun onActivityStarted(activity: Activity) {}

            override fun onActivityResumed(activity: Activity) {}

            override fun onActivityPaused(activity: Activity) {}

            override fun onActivityStopped(activity: Activity) {}

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

            override fun onActivityDestroyed(activity: Activity) {
                if (this@Speakerbox.activity === activity) {
                    shutdown()
                }
            }
        }
        application.registerActivityLifecycleCallbacks(callbacks)
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            initialized = true
            if (playOnInit != null) {
                playInternal(playOnInit!!, UTTERANCE_ID_NONE)
            }
        } else {
            Log.e(TAG, "Initialization failed.")
        }
    }

    fun setActivity(activity: Activity) {
        this.activity = activity
        enableVolumeControl(this.activity)
    }

    fun play(text: CharSequence) {
        play(text.toString(), null, null, null)
    }

    /**
     * Play text and perform action when text starts playing
     *
     * Runnable is run on the main thread
     * @param text
     * @param onStart
     */
    fun playAndOnStart(text: String, onStart: Runnable) {
        play(text, onStart, null, null)
    }

    /**
     * Play text and perform action when text finishes playing
     *
     * Runnable is run on the main thread
     * @param text
     * @param onDone
     */
    fun playAndOnDone(text: String, onDone: Runnable) {
        play(text, null, onDone, null)
    }

    /**
     * Play text and perform action when error occurs in playback
     *
     * Runnable is run on the main thread
     * @param text
     * @param onError
     */
    fun playAndOnError(text: String, onError: Runnable) {
        play(text, null, null, onError)
    }

    /**
     * Play text and perform actions when text starts playing, text finishes playing or
     * text incurs error playing. Note that {@param onDone} and {@param onError} are mutually
     * exclusive and only one will be called. All runnables are run on the main thread
     *
     * @param text
     * @param onStart
     */
    fun play(text: String, onStart: Runnable?, onDone: Runnable?, onError: Runnable?) {
        var text = text
        if (doesNotContainUnwantedPhrase(text)) {
            text = applyRemixes(text)
            if (initialized) {
                val utteranceId = SystemClock.currentThreadTimeMillis().toString()
                if (onStart != null) {
                    onStartRunnables[utteranceId] = onStart
                }
                if (onDone != null) {
                    onDoneRunnables[utteranceId] = onDone
                }
                if (onError != null) {
                    onErrorRunnables[utteranceId] = onError
                }
                playInternal(text, utteranceId)
            } else {
                playOnInit = text
            }
        }
    }

    fun stop() {
        textToSpeech.stop()
    }

    private fun applyRemixes(text: String): String {
        var text = text
        for (key in samples.keys) {
            if (text.contains(key)) {
                text = text.replace(key, samples[key]!!)
            }
        }

        return text
    }

    private fun playInternal(text: String, utteranceId: String) {
        if (isMuted) {
            return
        }
        Log.d(TAG, "Playing: \"$text\"")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(text, queueMode, null, utteranceId)
        } else {
            val params = HashMap<String, String>()
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = utteranceId
            textToSpeech.speak(text, queueMode, params)
        }
    }

    fun dontPlayIfContains(text: String) {
        unwantedPhrases.add(text)
    }

    private fun doesNotContainUnwantedPhrase(text: String): Boolean {
        for (invalid in unwantedPhrases) {
            if (text.contains(invalid)) {
                return false
            }
        }
        return true
    }

    fun mute() {
        isMuted = true
        if (textToSpeech.isSpeaking) {
            textToSpeech.stop()
        }
    }

    fun unmute() {
        isMuted = false
    }

    fun remix(original: String, remix: String) {
        samples[original] = remix
    }

    fun requestAudioFocus() {
        val am = application.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        am.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK)
    }

    fun abandonAudioFocus() {
        val am = application.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        am.abandonAudioFocus(audioFocusChangeListener)
    }

    fun enableVolumeControl(activity: Activity?) {
        if (activity != null) {
            activity.volumeControlStream = AudioManager.STREAM_MUSIC
        }
    }

    fun disableVolumeControl(activity: Activity?) {
        if (activity != null) {
            activity.volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE
        }
    }

    fun setQueueMode(queueMode: Int) {
        this.queueMode = queueMode
    }

    fun setLanguage(locale: Locale) {
        textToSpeech.language = locale
    }

    /**
     * Shutdown the [TextToSpeech] object and unregister activity lifecycle callbacks
     */
    fun shutdown() {
        textToSpeech.shutdown()
        application.unregisterActivityLifecycleCallbacks(callbacks)
    }

    /**
     * Find the runnable for a given utterance id, run it on the main thread and then remove
     * it from the map
     * @param utteranceId the id key to use
     * @param hashMap utteranceIds to runnable map to use
     * @return whether value was found
     */
    private fun detectAndRun(utteranceId: String, hashMap: HashMap<String, Runnable>): Boolean {
        if (hashMap.containsKey(utteranceId)) {
            val runnable = hashMap[utteranceId]
            val handler = Handler(Looper.getMainLooper())
            handler.post(runnable)
            hashMap.remove(utteranceId)
            return true
        } else {
            return false
        }
    }

    companion object {
        internal val TAG = Speakerbox::class.java!!.getSimpleName()

        /**
         * Pitch when we have focus
         */
        private val FOCUS_PITCH = 1.0f
        /**
         * Pitch when we should duck audio for another app
         */
        private val DUCK_PITCH = 0.5f
        /**
         * ID for when no text is spoken
         */
        val UTTERANCE_ID_NONE = "-1"
    }
}
