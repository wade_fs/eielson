package com.wade.smartlauncher.interfaces;

public interface LabelProvider {
    String getLabel();
}
