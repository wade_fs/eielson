package com.wade.smartlauncher.viewutil;

import android.view.View;

import com.wade.smartlauncher.model.Item;
import com.wade.smartlauncher.util.RevertibleAction;

public interface DesktopCallBack<V extends View> extends RevertibleAction {
    boolean addItemToPoint(Item item, int x, int y);

    boolean addItemToPage(Item item, int page);

    boolean addItemToCell(Item item, int x, int y);

    void removeItem(V view, boolean animate);
}
