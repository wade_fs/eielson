package com.wade.smartlauncher.interfaces;

import com.wade.smartlauncher.util.App;

import java.util.List;

public interface AppUpdateListener {

    /**
     * @param apps list of apps
     * @return true, if the listener should be removed
     */
    boolean onAppUpdated(List<App> apps);
}
