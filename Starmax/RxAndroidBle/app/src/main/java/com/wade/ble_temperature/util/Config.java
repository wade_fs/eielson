package com.wade.ble_temperature.util;

public class Config {
    public static final int QUEUE_PROCESS_PERIOD = 100;
    public static final int QUEUE_PROCESS_TIMEOUT_PERIOD = 100;
    public static final String ADMIN_ID = "ADMIN";
    public static final String ADMIN_ENROLL = "ADMIN";
    public static final int  disTimeOut = 500;
    public static final int  conTimeOut = 15000;
    public static final int  scanTimeout = 1000;
    public static final String deviceNameTag = "deviceNameTag";
    public static final String deviceBddrTag = "bdAddrTag";
    public static final String deviceModelTag = "modelTag";
    public static final String ADMINPWD_Tag = ADMIN_ID+"PWD";
    public static final String ADMINCARD_Tag = ADMIN_ID+"CARD";
    public static final String ALARM_TEMP_TAG = "ALARM_TEMP_TAG";
    public static final String MEASURE_INTERVAL_TAG = "MEASURE_INTERVAL_TAG";
    public static final String TEMP_UNIT_TAG = "TEMP_UNIT_TAG";
    public static final String TEMP_VALUE_TYPE_TAG = "TEMP_VALUE_TYPE_TAG";
    public static final String TYPEFACE = "fonts/Montserrat_Regular.ttf";
    public static final int SCAN_DELAY = 1000;
    public static final int SCAN_ERROR_DELAY = 5000;
    public static final long BUTTON_DEBOUNCE = 1000;
    public static final double TEMP_EAR = 38;
    public static final double TEMP_FOREHEAD = 37.5;
    public static final double ALARM_TYPE_DEF = TEMP_EAR;
}
