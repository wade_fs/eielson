package com.wade.ble_temperature.util;

import android.util.Log;
import android.util.SparseArray;

import com.polidea.rxandroidble2.scan.ScanResult;
import com.radi.util.Encode;

import net.bluepacket.bletemp.BLETempProtocol;

import java.util.Objects;

public class BluePacket {
    private final static String TAG = "MyLog";
    public static Encode encode = Encode.getInstance();
    // 02:01:06:11:09:42:4C:45:20:5F:
    // 20:50:45:52:49:50:48:45:52:41:
    // 4C:11:07:12:A2:4D:2E:FE:14:48:
    // 8E:93:D2:17:3C:FF:E0:00:00:09:  //
    // FF:09:00:15:55:17:9E:00:04:00:  // MandufactureData 15:55:17:9E:00:04, 含有 Device Mac 中的後三碼 09:00:15
    // 00:00:00:00:00:00:00:00:00:00:
    // 00:00
    public AdvData parsingADVData(ScanResult result){
//        Util.debugMessage(TAG, "parsingADVData(1) "+bytesToHex(result.getScanRecord().getBytes()), debugFlag);

        byte[] rawData = Objects.requireNonNull(result.getScanRecord()).getBytes();
        SparseArray<byte[]> mandufacturerData = result.getScanRecord().getManufacturerSpecificData();

        if(mandufacturerData.size() <= 0)
            return null;
        if(mandufacturerData.valueAt(0).length < 6)
            return null;
//        Util.debugMessage(TAG, "parsingADVData(mandufacturerData) "+bytesToHex(mandufacturerData.valueAt(0)), true);

        byte [] rawBD = { rawData[38], rawData[39], mandufacturerData.valueAt(0)[0] };
        String bdAddress = encode.byteArrayToHexString(rawBD);
//        Util.debugMessage(TAG, "parsingADVData(rawBD) "+bytesToHex(rawBD)+" / "+bdAddress, true);

//        String newBDAddress = BLETempProtocol.bp_address;
//        for(int i=0; i<bdAddress.length(); i+=2)
//            newBDAddress += ":" + bdAddress.substring(i,i+2);
//        Util.debugMessage(TAG, "bdAddress = " + newBDAddress, debugFlag);

        // mandufacturerData[0] = 15:55:17:9E:00:04
        byte customID =  mandufacturerData.valueAt(0)[1];
        byte[] tempArray = {mandufacturerData.valueAt(0)[2],mandufacturerData.valueAt(0)[3],mandufacturerData.valueAt(0)[4]};
        double temp = BLETempProtocol.getInstance().calculateTemp(tempArray); // 17:9E:00 = 23.6171875
        byte tempUnit =  mandufacturerData.valueAt(0)[4];
        byte deviceType = mandufacturerData.valueAt(0)[5];

        Log.d(TAG, "parsingADVData(tempArray) "+ HexString.bytesToHex(tempArray) + " ==> temp = "+temp);
        return new AdvData(bdAddress, customID, temp, tempUnit, deviceType);
    }

}
