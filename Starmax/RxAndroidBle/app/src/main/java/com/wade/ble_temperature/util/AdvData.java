package com.wade.ble_temperature.util;

public class AdvData {
    private String BDAddress;
    private byte CustomID;
    private double temp;
    private byte tempUnit;
    private byte deviceType;
    public AdvData() {

    }


    public AdvData(String bdAddress, byte CustomID, double temp,byte tempUnit,byte deviceType) {

        this.BDAddress = bdAddress;
        this.CustomID = CustomID;
        this.temp = temp;
        this.tempUnit = tempUnit;
        this.deviceType = deviceType;

    }

    public String getBDAddress(){return BDAddress;}
    public byte getCustomID(){return CustomID;}
    public double getTemp(){return temp;}
    public byte getTempUnit(){return tempUnit;}
    public byte getDeviceType(){return deviceType;}

    public void clear() {
        this.BDAddress ="";
        this.CustomID = 0;
        this.temp = 0;
        this.tempUnit = 0;
        this.deviceType = 0;
    }
}
