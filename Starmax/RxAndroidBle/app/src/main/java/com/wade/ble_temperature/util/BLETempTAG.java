package com.wade.ble_temperature.util;

import com.polidea.rxandroidble2.RxBleDevice;

public class BLETempTAG {
    public String dev_name;
    public String dev_addr;
    public byte CustomID;
    public double temp;
    public String tempUnit;
    public byte deviceType;
    public String nickName;
    public boolean isAlarm;
    public BLETempTAG() {
        clear();
    }


    public BLETempTAG(RxBleDevice device, byte CustomID, double temp, String tempUnit, byte deviceType) {
        this.dev_name = device.getName();
        this.dev_addr = device.getMacAddress();
        this.temp = temp;
        this.tempUnit = tempUnit;
        this.deviceType = deviceType;

    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public void setAlarm(boolean isAlarm){this.isAlarm = isAlarm;}
    public boolean getAlarm(){return isAlarm;}
    public void clear() {
        this.dev_name = "";
        this.dev_addr = "";
        this.temp = 0;
        this.tempUnit = "";
        this.deviceType = 0;
        this.nickName = "";
        this.isAlarm = false;
    }
    @Override
    public String toString() {
        return "{name="+dev_name+", addr="+dev_addr+", cid="+CustomID+", temp="+temp+tempUnit+", nick="+nickName+", alarm="+isAlarm+"}";
    }
}
