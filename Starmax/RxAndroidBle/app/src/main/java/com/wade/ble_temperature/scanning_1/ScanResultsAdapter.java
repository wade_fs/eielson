package com.wade.ble_temperature.scanning_1;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.polidea.rxandroidble2.RxBleDevice;

import com.wade.ble_temperature.util.AdvData;
import com.wade.ble_temperature.util.BLETempTAG;
import com.wade.ble_temperature.util.BluePacket;
import com.wade.ble_temperature.util.HexString;
import com.polidea.rxandroidble2.scan.ScanResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.Locale;

class ScanResultsAdapter extends RecyclerView.Adapter<ScanResultsAdapter.ViewHolder> {
    private final static String TAG="MyLog";
    private final static String BLUEPACKET_TEMP_MAC_PRE = "00:12:A1";
    private BluePacket bluePacket = new BluePacket();
    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(android.R.id.text1)
        TextView line1;
        @BindView(android.R.id.text2)
        TextView line2;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface OnAdapterItemClickListener {

        void onAdapterViewClick(View view);
    }

    private static final Comparator<ScanResult> SORTING_COMPARATOR = (lhs, rhs) ->
            lhs.getBleDevice().getMacAddress().compareTo(rhs.getBleDevice().getMacAddress());
    private final List<ScanResult> data = new ArrayList<>();
    private OnAdapterItemClickListener onAdapterItemClickListener;
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (onAdapterItemClickListener != null) {
                onAdapterItemClickListener.onAdapterViewClick(v);
            }
        }
    };

    void addScanResult(ScanResult bleScanResult) {
        // Not the best way to ensure distinct devices, just for sake on the demo.
        if (bleScanResult.getBleDevice().getMacAddress().startsWith(BLUEPACKET_TEMP_MAC_PRE)) {
            // 先檢查是否已存在，若是修改它後返回
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getBleDevice().equals(bleScanResult.getBleDevice())) {
                    data.set(i, bleScanResult);
                    notifyItemChanged(i);
                    Log.d(TAG, "addScanResult(Change) "+ bleScanResult.getBleDevice().getMacAddress());
                    return;
                }
            }
            Log.d(TAG, "addScanResult(ADD) "+ HexString.bytesToHex(bleScanResult.getScanRecord().getBytes(), ":"));
            data.add(bleScanResult);
            Collections.sort(data, SORTING_COMPARATOR);
            notifyDataSetChanged();
        }
    }

    void clearScanResults() {
        data.clear();
        notifyDataSetChanged();
    }

    ScanResult getItemAtPosition(int childAdapterPosition) {
        return data.get(childAdapterPosition);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    final byte SWITCH_MODE_BEACON = 4;
    final byte TEMP_UINT_FAHRENHEIT = 1;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ScanResult rxBleScanResult = data.get(position);
        final RxBleDevice bleDevice = rxBleScanResult.getBleDevice();
//        double temp = 0.0;
//        byte[] adv = rxBleScanResult.getScanRecord().getBytes();
        AdvData advData = bluePacket.parsingADVData(rxBleScanResult);
        if(advData == null || advData.getDeviceType() != SWITCH_MODE_BEACON) {
            Log.d(TAG, "scanUpdate(BAD ADV) "+(advData==null?"Null ADV":advData.getDeviceType()));
            return;
        }
        /* 底下還沒有要加 BLETempTAG, 因為它需要 Sqlite DB */
//        BLETempTAG tag = new BLETempTAG(bleDevice, (byte) 0x00, advData.getTemp(), getTempUnit((byte) 0) , (byte) 0x00);
//        tag.temp = advData.getTemp();
//        tag.tempUnit = getTempUnit(advData.getTempUnit());
//        tag.deviceType = advData.getDeviceType();
//        tag.CustomID = advData.getCustomID();
//        tag.setNickName("BluePacket");
//        tag.setAlarm(true);

        if (advData.getTempUnit() == 0 && advData.getTemp() < -10.0 || advData.getTemp() < 14.0) return;
        holder.line1.setText(String.format(Locale.getDefault(), "%s (%s)", bleDevice.getMacAddress(), bleDevice.getName()));
        if (advData.getTemp()>36.5) holder.line2.setTextColor(Color.RED);
        else holder.line2.setTextColor(Color.BLACK);
        holder.line2.setText(String.format(Locale.getDefault(), "Temp: %.2f%s, RSSI: %d",
                advData.getTemp(), getTempUnit((byte) advData.getTempUnit()), rxBleScanResult.getRssi()));
    }

    private String getTempUnit(byte tempUnit){
        if(tempUnit == TEMP_UINT_FAHRENHEIT) // GlobalData.protocol.TEMP_UINT_FAHRENHEIT())
            return "\u2109";
        return "\u2103";
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.two_line_list_item, parent, false);
        itemView.setOnClickListener(onClickListener);
        return new ViewHolder(itemView);
    }

    void setOnAdapterItemClickListener(OnAdapterItemClickListener onAdapterItemClickListener) {
        this.onAdapterItemClickListener = onAdapterItemClickListener;
    }
}
