package com.zhj.bluetooth.sdkdemo.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseAdapter;
import com.zhj.bluetooth.sdkdemo.base.BaseViewHolder;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthSportItem;

import java.util.List;

import butterknife.BindView;

public class StepsHistoryAdapter extends BaseAdapter<HealthSportItem,StepsHistoryAdapter.ViewHolder> {
    public StepsHistoryAdapter(Context mContext, List<HealthSportItem> mList) {
        super(mContext, mList);
    }

    @Override
    protected void onNormalBindViewHolder(StepsHistoryAdapter.ViewHolder holder, HealthSportItem itemBean, int position) {
        holder.tvDate.setText("時間"+"\n"+itemBean.getYear()+"-"+itemBean.getMonth()+"-"+itemBean.getDay() + "  "+itemBean.getHour()+":"+itemBean.getMinuter());
        holder.tvSteps.setText("步數"+"\n"+itemBean.getStepCount());
        //卡路里(cal):  步長(cm)*體重(kg)*步數*0.78/100000
        //距離(m):步長(cm)*步數/100
        //預設步長75cm  體重60kg
        holder.tvCal.setText("卡路里"+"\n"+itemBean.getStepCount() * 75 * 60 * 0.78 / 100000);
        holder.tvDistance.setText("距離"+"\n"+itemBean.getStepCount() * 75 / 100);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_steps, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvSteps)
        TextView tvSteps;
        @BindView(R.id.tvCal)
        TextView tvCal;
        @BindView(R.id.tvDistance)
        TextView tvDistance;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
