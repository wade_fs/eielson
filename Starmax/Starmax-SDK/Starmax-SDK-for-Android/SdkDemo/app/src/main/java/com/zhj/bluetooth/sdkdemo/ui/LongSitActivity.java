package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.LongSit;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class LongSitActivity extends BaseActivity {

    @BindView(R.id.tvOpenState)
    TextView tvOpenState;
    @BindView(R.id.tvStartTime)
    TextView tvStartTime;
    @BindView(R.id.tvEndTime)
    TextView tvEndTime;
    @BindView(R.id.tvRepeti)
    TextView tvRepeti;
    @BindView(R.id.tvInterval)
    TextView tvInterval;
    @Override
    protected int getContentView() {
        return R.layout.activity_long_sit;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("久坐提醒信息");
        BleSdkWrapper.getLongSit(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                LongSit longSit = (LongSit) result.data;
                tvOpenState.setText("久坐開關："+longSit.isOnOff());
                tvStartTime.setText("開始時間："+longSit.getStartHour()+":00"); //單位小時
                tvEndTime.setText("結束時間："+longSit.getEndHour()+":00");//單位小時
                //重複周期從星期一開始  每一個Boolean值對應當天是否選中
                StringBuilder sb = new StringBuilder();
                for (boolean b : longSit.getWeeks()){
                    sb.append(b);
                    sb.append(",");
                }
                tvRepeti.setText("重複周期："+sb.toString());
                tvInterval.setText("檢測周期時間："+longSit.getInterval());//單位 5 分鐘  設置時請設置5的倍數
            }

            @Override
            public void setSuccess() {

            }
        });
    }
}
