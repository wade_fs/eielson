package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.DeviceState;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.UserBean;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class UserInfoActivity extends BaseActivity {

    @BindView(R.id.tvSex)
    TextView tvSex;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvHight)
    TextView tvHight;
    @BindView(R.id.tvWeight)
    TextView tvWeight;
    @BindView(R.id.tvStepDistance)
    TextView tvStepDistance;
    @Override
    protected int getContentView() {
        return R.layout.activity_user_info;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("用戶信息");
        BleSdkWrapper.getUserInfo(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                UserBean userBean = (UserBean) result.data;
                //0x00:男 0x01:女 0x02:其他
                tvSex.setText("性別："+userBean.getGender());
                tvAge.setText("年齡："+userBean.getAge());
                tvHight.setText("身高："+userBean.getHeight()); //單位CM
                tvWeight.setText("體重："+userBean.getWeight());//單位0.1KG 如果返回600  就是60KG  設置的時候也需要相對應的KG*10
                tvStepDistance.setText("步長："+userBean.getStepDistance());//單位CM
            }

            @Override
            public void setSuccess() {

            }
        });
    }
}
