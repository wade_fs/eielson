package com.zhj.bluetooth.sdkdemo;

import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.ui.AlarmActivity;
import com.zhj.bluetooth.sdkdemo.ui.DeviceInfoActivity;
import com.zhj.bluetooth.sdkdemo.ui.DeviceStateActivity;
import com.zhj.bluetooth.sdkdemo.ui.GoalActivity;
import com.zhj.bluetooth.sdkdemo.ui.HeartTestActivity;
import com.zhj.bluetooth.sdkdemo.ui.LongSitActivity;
import com.zhj.bluetooth.sdkdemo.ui.MessageContreActivity;
import com.zhj.bluetooth.sdkdemo.ui.NoticeActivity;
import com.zhj.bluetooth.sdkdemo.ui.RateHistoryActivity;
import com.zhj.bluetooth.sdkdemo.ui.ScanDeviceReadyActivity;
import com.zhj.bluetooth.sdkdemo.ui.SleepDataActivity;
import com.zhj.bluetooth.sdkdemo.ui.StepsDataActivity;
import com.zhj.bluetooth.sdkdemo.ui.UserInfoActivity;
import com.zhj.bluetooth.sdkdemo.util.DialogHelperNew;
import com.zhj.bluetooth.sdkdemo.util.IntentUtil;
import com.zhj.bluetooth.sdkdemo.util.Md5Util;
import com.zhj.bluetooth.sdkdemo.util.PermissionUtil;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.BLEDevice;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.UserBean;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallbackWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleManager;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.util.Constants;
import com.zhj.bluetooth.zhjbluetoothsdk.util.LogUtil;
import com.zhj.bluetooth.zhjbluetoothsdk.util.SPHelper;

import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements PermissionUtil.RequsetResult{

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMac)
    TextView tvMac;
    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }
    @Override
    protected void initView() {
        super.initView();
        titleName.setText("首頁");
        BleSdkWrapper.init(this, new BleCallbackWrapper() {

            @Override
            public void complete(int resultCode, Object data) {
                super.complete(resultCode, data);
                if(resultCode == Constants.BLE_RESULT_CODE.SUCCESS){
                    checkBind();
                    //重連
                    BleManager.getInstance().reConnect();
                }else{
                    LogUtil.d("SDK不能使用");
                }
            }

            @Override
            public void setSuccess() {

            }
        });

    }

    private void checkBind(){
        if(BleSdkWrapper.isBind()){
            rightText.setText("解除綁定");
            if(BleSdkWrapper.isConnected()){
                tvTitle.setText("已綁定設備(已連接)");
            }else{
                tvTitle.setText("已綁定設備(未連接)");
            }
            BLEDevice bleDevice = SPHelper.getBindBLEDevice(this);
            tvMac.setText("藍芽地址："+bleDevice.mDeviceAddress);
        }else{
            rightText.setText("綁定設備");
            tvTitle.setText("未綁定設備");
            tvMac.setText("");
        }
        rightText.setOnClickListener(view -> {
            if(BleSdkWrapper.isBind()){
                BleSdkWrapper.unBind();
                tvTitle.setText("未綁定設備");
                tvMac.setText("");
                rightText.setText("綁定設備");
            }else{
                IntentUtil.goToActivity(MainActivity.this, ScanDeviceReadyActivity.class);
            }
        });
    }

    @OnClick(R.id.btnGetSleep)
    void togGetSleep(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, SleepDataActivity.class);
        }else{
            showToast("設備未連接");
        }
    }


    @OnClick(R.id.btnGetSteps)
    void toGetSteps(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, StepsDataActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnSetHeartTest)
    void toSetHeartTest(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, HeartTestActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnSendMessage)
    void toSendMessage(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, MessageContreActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnDeviceInfo)
    void getDeviceInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, DeviceInfoActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnDeviceState)
    void getDeviceState(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, DeviceStateActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnUserInfo)
    void getUserInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, UserInfoActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnAlarmInfo)
    void getAlarmInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, AlarmActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnLongSitInfo)
    void getLongSitInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, LongSitActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnGoalInfo)
    void getGoalInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, GoalActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnNoticeInfo)
    void getNoticeInfo(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, NoticeActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @OnClick(R.id.btnGetRateHistory)
    void getRateHistory(){
        if(BleSdkWrapper.isConnected()){
            IntentUtil.goToActivity(this, RateHistoryActivity.class);
        }else{
            showToast("設備未連接");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
