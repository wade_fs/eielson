package com.zhj.bluetooth.sdkdemo.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.ui.adapter.AlarmListAdapter;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.Alarm;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.BLEDevice;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class AlarmActivity extends BaseActivity {

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    private AlarmListAdapter mAdapter;

    @Override
    protected int getContentView() {
        return R.layout.activity_alarm;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("閙鐘信息");
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        BleSdkWrapper.getAlarmList(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                List<Alarm> alarmList = (List<Alarm>) result.data;
                mAdapter = new AlarmListAdapter(AlarmActivity.this,alarmList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void setSuccess() {
            }
        });
    }
}
