package com.zhj.bluetooth.sdkdemo.util;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.MyAppcation;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Describe 工具欄
 */

public class CommonUtil {
    /**
     * 是否是24小時
     *
     * @return
     */
    public static boolean is24Hour() {
//        int timeStyle = (int) SPUtils.get(Constant.TIME_STYLE,0);
        boolean is24;
//        if (timeStyle == 0) {//跟隨系統
        ContentResolver cv = MyAppcation.getInstance().getContentResolver();
        // 獲取當前系統設置
        String time_12_24 = android.provider.Settings.System.getString(cv,
                android.provider.Settings.System.TIME_12_24);
        is24 = "24".equals(time_12_24) ? true : false;
//        }else{
//            is24 = timeStyle == Constants.TIME_MODE_24;
//        }
        return true;
    }

    public static int format24To12(int hour) {
        int h = hour % 12;
        if (hour == 12) {
            h = h == 0 ? 12 : h;
        } else {
            h = h == 0 ? 0 : h;
        }
        return h;
    }

    public static boolean isAM(int hour) {
        return hour < 12;
    }

    /**
     * 將一天中的分鐘序列數變為對應的hh:mm形式
     *
     * @param mins 00:00為第一分鐘， mins = h * 60 + m;範圍1~1440
     * @return
     */
    public static String timeFormatter(int mins, boolean is24, String[] amOrPm, boolean isUnit) {
        if (mins >= 0 && mins < 1440) {
            int h = getHourAndMin(mins, is24)[0];
            int min = mins % 60;
            if (is24) {
                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            } else {
                String m = "";
                if (isUnit) {
                    if (amOrPm != null) {
                        m = mins <= 12 * 60 ? amOrPm[0] : amOrPm[1];
                    } else {
                        m = mins <= 12 * 60 ? "am" : "pm";
                    }
                }
//                if(m.equals("下午")||m.equals("上午")){
//                    return m+ String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
//                }else {
                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min) + m;
//                }
            }
        } else if (mins >= 1440) {
            mins -= 1440;
            int h = 0;
            int min = 0;
            if (mins > 0) {
                h = getHourAndMin(mins, is24)[0];
                min = mins % 60;
            }
            if (is24) {
                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            } else {
                String m = "";
                if (isUnit) {
                    if (amOrPm != null) {
                        m = mins <= 12 * 60 ? amOrPm[0] : amOrPm[1];
                    } else {
                        m = mins <= 12 * 60 ? "am" : "pm";
                    }
                }

                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min) + m;
            }
        }

//        Log.e("Util", "timeFormatter Error : mins is out of range [0 , 1440).");
//        return "--:--";
        return "00:00";
    }

    /**
     * 將一天中的分鐘序列數變為對應的hh:mm形式
     *
     * @param mins 00:00為第一分鐘， mins = h * 60 + m;範圍1~1440
     * @return
     */
    public static String timeFormatter(int mins, boolean is24, String[] amOrPm, boolean isUnit, boolean isStart) {
        if (mins >= 0 && mins < 1440) {
            int h = getHourAndMin(mins, is24)[0];
            int min = mins % 60;
            if (!isStart && min != 0) {
                h += 1;
            }
            min = 0;
            if (is24) {
                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            } else {
                String m = "";
                if (isUnit) {
                    if (amOrPm != null) {
                        m = mins < 12 * 60 ? amOrPm[0] : amOrPm[1];
                    } else {
                        m = mins < 12 * 60 ? "am" : "pm";
                    }
                }
                return m + String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            }
        } else if (mins >= 1440) {
            mins -= 1440;
            int h = 0;
            int min = 0;
            if (mins > 0) {
                h = getHourAndMin(mins, is24)[0];
                min = mins % 60;
            }
            if (!isStart && min != 0) {
                h += 1;
            }
            min = 0;
            if (is24) {
                return String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            } else {
                String m = "";
                if (isUnit) {
                    if (amOrPm != null) {
                        m = mins < 12 * 60 ? amOrPm[0] : amOrPm[1];
                    } else {
                        m = mins < 12 * 60 ? "am" : "pm";
                    }
                }
                return m + String.format("%1$02d:%2$02d", h == 24 ? 0 : h, min);
            }
        }

//        Log.e("Util", "timeFormatter Error : mins is out of range [0 , 1440).");
//        return "--:--";
        return "00:00";
    }

    public static int[] getHourAndMin(int mins, boolean is24) {
        int h = mins / 60;
        // 0 ,12,24都是12點 ， 下午的-12
        h = is24 ? h : (h % 12 == 0 ? 12 : h > 12 ? h - 12 : h);
        return new int[]{h, mins % 60};
    }


    /**
     * @param h
     * @param min
     * @param is24
     * @return
     */

    /**
     * @param time 00:00
     * @param is24
     * @return
     */




    /**
     * 判斷GPS是否開啟，GPS或者AGPS開啟一個就認為是開啟的
     *
     * @param context
     * @return true 表示開啟
     */
    public static boolean isOPen(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 通過GPS衛星定位，定位級別可以精確到街（通過24顆衛星定位，在室外和空曠的地方定位準確、速度快）
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通過WLAN或移動網絡(3G/2G)確定的位置（也稱作AGPS，輔助GPS定位。主要用於在室內或遮蓋物（建築群或茂密的深林等）密集的地方定位）
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }

    /**
     * 強制幫用戶打開GPS
     *
     * @param context
     */
    public static void openGPS(Context context) {
        Intent GPSIntent = new Intent();
        GPSIntent.setClassName("com.android.settings",
                "com.android.settings.widget.SettingsAppWidgetProvider");
        GPSIntent.addCategory("android.intent.category.ALTERNATIVE");
        GPSIntent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(context, 0, GPSIntent, 0).send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根據星期開始日獲取周
     * 如果開始日為周日則 返回 日 一 二 三 四
     * 如果開始日為 六 則返回 六 日 一 二
     * 如果開始日為一 則返回 一 二 三 四
     *
     * @param context
     * @param weekStartDay
     * @return
     */

    /**
     * 把發送閙鐘的week轉換為顯示的week
     * 發送閙鐘的week 固定是從星期一開始的
     * 顯示的week 根據開始星期日決定的
     *
     * @return 返回星期一為開始日的數組
     * @startWeek 0:周六，1：周日，2：周一
     */
    public static boolean[] alarmToShowAlarm(boolean[] week, int startWeek) {
        boolean[] tempAlarm = new boolean[7];
        if (startWeek == 0) {
            tempAlarm[0] = week[2];
            tempAlarm[1] = week[3];
            tempAlarm[2] = week[4];
            tempAlarm[3] = week[5];
            tempAlarm[4] = week[6];
            tempAlarm[5] = week[0];
            tempAlarm[6] = week[1];
        } else if (startWeek == 1) {
            tempAlarm[0] = week[1];
            tempAlarm[1] = week[2];
            tempAlarm[2] = week[3];
            tempAlarm[3] = week[4];
            tempAlarm[4] = week[5];
            tempAlarm[5] = week[6];
            tempAlarm[6] = week[0];
        } else {
            tempAlarm = Arrays.copyOf(week, week.length);
        }
        return tempAlarm;
    }


    /**
     * @param week
     * @param startWeek 0:周六，1：周日，2：周一
     * @return 將星期一為開始日的數組，轉換成其他一種
     */
    public static boolean[] alarmToShowAlarm2(boolean[] week, int startWeek) {
        boolean[] tempAlarm = new boolean[7];
        if (startWeek == 0) {
            tempAlarm[0] = week[5];
            tempAlarm[1] = week[6];
            tempAlarm[2] = week[0];
            tempAlarm[3] = week[1];
            tempAlarm[4] = week[2];
            tempAlarm[5] = week[3];
            tempAlarm[6] = week[4];
        } else if (startWeek == 1) {
            tempAlarm[0] = week[6];
            tempAlarm[1] = week[0];
            tempAlarm[2] = week[1];
            tempAlarm[3] = week[2];
            tempAlarm[4] = week[3];
            tempAlarm[5] = week[4];
            tempAlarm[6] = week[5];
        } else {
            tempAlarm = Arrays.copyOf(week, week.length);
        }
        return tempAlarm;
    }

    /**
     * 是否有軌跡
     *
     * @param type
     * @return
     */
    public static boolean hasOrbit(int type) {
        //0x01;// 走路
        //0x02;// 跑步
        //0x03;// 騎行
        //0x04;// 徒步
        int[] types = {1, 2, 3, 4};
        for (int t : types) {
            if (t == type) {
                return true;
            }
        }
        return false;
    }

    public static String noHeartRate(String s) {
        if (TextUtils.isEmpty(s) || s.equals("0")) {
            return "--";
        }
        return s + "";
    }

    public static String noBloodPressure(int systolicPressure, int diastolicPressure) {
        if (systolicPressure == 0 || diastolicPressure == 0) {
            return "--/--";
        }
        return systolicPressure + "/" + diastolicPressure;
    }

    public static String noPace(int speed) {
        if (speed == 0) {
            return "--";
        }
        StringBuffer avgPace = new StringBuffer();
        avgPace.append(speed / 60);
        avgPace.append("'");
        avgPace.append(speed % 60);  //轉換字元串
        avgPace.append("\"");
        return avgPace.toString();
    }


    public static void adjustTvTextSize(TextView tv, int maxWidth, String text) {
        int avaiWidth = maxWidth - tv.getPaddingLeft() - tv.getPaddingRight() - 10;

        if (avaiWidth <= 0) {
            return;
        }

        TextPaint textPaintClone = new TextPaint(tv.getPaint());
        // note that Paint text size works in px not sp
        float trySize = textPaintClone.getTextSize();

        while (textPaintClone.measureText(text) > avaiWidth) {
            trySize--;
            textPaintClone.setTextSize(trySize);
        }

        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, trySize);
    }

    private static DecimalFormat df;
    private static DecimalFormat decimalFormat;

    static {
        Locale.setDefault(Locale.CHINA);
        df = new DecimalFormat("#,###");
        decimalFormat = new DecimalFormat("###,###,###,##0.00");
    }

    public static String formatThree(int value) {
        return df.format(value);
    }

    public static String formatThree(float value) {
        return df.format(value);
    }

    public static String formatNumber(int num) {
        String formatNum;
        if (num > 10000) {
            formatNum = "10,000+";
        } else {
            formatNum = df.format(num);
        }
        return formatNum;
    }

    /**
     * 保留兩位並且三位用“，”隔開
     *
     * @param num
     * @return
     */
    public static String formatDistance(float num) {
        return decimalFormat.format(num);
    }

    public static SimpleDateFormat getFormat(String format) {
        return new SimpleDateFormat(format);
    }

    /**
     * 計算月數
     *
     * @return
     */
    private static int calculationDaysOfMonth(int year, int month) {
        int day = 0;
        switch (month) {
            // 31天
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            // 30天
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;
            // 計算2月天數
            case 2:
                day = year % 100 == 0 ? year % 400 == 0 ? 29 : 28
                        : year % 4 == 0 ? 29 : 28;
                break;
        }

        return day;
    }


    /**
     *    目標時間選擇列表（無單位）
     * @return
     */
    public static List<Float> getTimeList() {
        final List<Float> times = new ArrayList<>();
        times.add(5f);
        for (int i = 10; i <= 6000; i += 10) {
            times.add(i*1f);
        }
        return times;
    }

    /**
     *    目標距離選擇列表（無單位）
     * @return
     */
    public static List<Float> getDistanceList() {
        final List<Float> distances = new ArrayList<>();
        distances.add(0.5f);
        for (int i = 1; i <= 100; i++) {
            distances.add(i*1f);
        }
        return distances;
    }
    /**
     *    目標卡路里選擇列表（無單位）
     * @return
     */
    public static List<Float> gettCalorieList() {
        final List<Float> distances = new ArrayList<>();
        for (int i = 300; i <= 9000; i+= 300) {
            distances.add(i*1f);
        }
        return distances;
    }
}
