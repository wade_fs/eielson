package com.zhj.bluetooth.sdkdemo.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * 頁面跳轉intent工具類
 * Created by wzl on 2018/6/17.
 */

public class IntentUtil {

    /**
     * 跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午5:31
     * @param c
     *         上下文對象
     * @param cls
     *         指定跳轉的類
     */
    public static void goToActivity(Context c, Class cls) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        c.startActivity(intent);
    }

    /**
     * 攜帶bundle數據跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午5:33
     */
    public static void goToActivity(Context c, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        intent.putExtras(bundle);
        c.startActivity(intent);
    }

    /**
     * 跳轉指定頁面並返回
     * author wzl
     * date 2018/6/17 下午5:35
     * @param requstCode
     *         請求碼
     */
    public static void goToActivityForResult(Context c, Class cls, int requstCode) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        ((Activity) c).startActivityForResult(intent, requstCode);
    }

    /**
     * 攜帶bundle數據跳轉指定頁面並返回
     */
    public static void goToActivityForResult(Context c, Class cls, Bundle bundle, int requstCode) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        intent.putExtras(bundle);
        ((Activity) c).startActivityForResult(intent, requstCode);
    }

    /**
     * 跳轉到指定頁面並關閉當前頁面
     * author wzl
     * date 2018/6/17 下午5:59
     */
    public static void goToActivityAndFinish(Context c, Class cls) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        c.startActivity(intent);
        ((Activity) c).finish();
    }

    /**
     * 跳轉到指定頁面並關閉當前頁面
     * author wzl
     * date 2018/6/17 下午5:59
     */
    public static void goToActivityAndFinish(Context c, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        intent.putExtras(bundle);
        c.startActivity(intent);
        ((Activity) c).finish();
    }

    /**
     * 跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午6:02
     */
    public static void goToActivityAndFinishTop(Context c, Class cls) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        // FLAG_ACTIVITY_CLEAR_TOP 銷毀目標Activity和它之上的所有Activity，重新創建目標Activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        c.startActivity(intent);
        ((Activity) c).finish();
    }

    /**
     * 跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午6:02
     */
    public static void goToActivityAndFinishTop(Context c, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(c, cls);
        intent.putExtras(bundle);
        // FLAG_ACTIVITY_CLEAR_TOP 銷毀目標Activity和它之上的所有Activity，重新創建目標Activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        c.startActivity(intent);
        ((Activity) c).finish();
    }

    /**
     * 跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午6:02
     */
    public static void goToActivityAndFinishSingleTop(Context c, Class clz, Bundle bundle) {
        Intent intent = new Intent(c, clz);
        intent.putExtras(bundle);
        //當該activity處于task棧頂時，可以復用，直接onNewIntent
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Activity a = (Activity) c;
        a.startActivity(intent);
        a.finish();
    }

    /**
     * 跳轉到指定頁面
     * author wzl
     * date 2018/6/17 下午6:02
     */
    public static void goToActivityAndFinishSingleTop(Context c, Class clz) {
        Intent intent = new Intent(c, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Activity a = (Activity) c;
        a.startActivity(intent);
        a.finish();
    }


}
