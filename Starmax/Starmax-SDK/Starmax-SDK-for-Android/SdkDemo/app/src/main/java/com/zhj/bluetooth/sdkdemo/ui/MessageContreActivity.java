package com.zhj.bluetooth.sdkdemo.ui;

import android.view.View;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.views.ItemToggleLayout;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.AppNotice;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.DeviceState;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;
import butterknife.OnClick;

public class MessageContreActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_message;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("消息提醒");
        //發送前請打開設備狀態裡面的消息開關
        DeviceState deviceState = new DeviceState();
        deviceState.isNotice = 1;
        BleSdkWrapper.setDeviceState(deviceState, new BleCallback() {
            @Override
            public void complete(int i, Object o) {

            }

            @Override
            public void setSuccess() {

            }
        });
    }

    @OnClick(R.id.tvSendMessage)
    void sendMessage(){
            /**
             * CATEGORYID_INCOMING = 0x00,//來電
             * CATEGORYID_SMS = 0x01,
             * CATEGORYID_WEIXIN = 0x02
             * CATEGORYID_MQQ = 0x03,
             * CATEGORYID_FACEBOOK = 0x04,
             * CATEGORYID_SKYPE = 0x05,
             * CATEGORYID_TWITTER = 0x06,
             * CATEGORYID_WHATISAPP = 0x07,
             * CATEGORYID_LINE = 0x08 ,
             * CATEGORYID_EMAIL = 0x09 ,
             * CATEGORYID_INSTAGRAM = 0x0A ,
             * CATEGORYID_LINKEDIN = 0x0B ,
             * CATEGORYID_UNKNOW = 0xFF,  //自定義消息
             */

            BleSdkWrapper.setMessage(0x03, "QQ提醒", "來QQ消息了", new BleCallback() {
                @Override
                public void complete(int i, Object o) {

                }

                @Override
                public void setSuccess() {

                }
            });
        }

}
