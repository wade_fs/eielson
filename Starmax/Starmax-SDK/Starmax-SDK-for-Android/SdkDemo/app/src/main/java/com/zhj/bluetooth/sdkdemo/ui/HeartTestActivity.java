package com.zhj.bluetooth.sdkdemo.ui;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.util.CustomToggleButton;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;
import com.zhj.bluetooth.zhjbluetoothsdk.util.LogUtil;

import butterknife.BindView;

public class HeartTestActivity extends BaseActivity {

    @BindView(R.id.mSwitch)
    CustomToggleButton mSwitch;
    @Override
    protected int getContentView() {
        return R.layout.activity_heart_test;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("心率檢測");
        //獲取心率檢測
        BleSdkWrapper.getHeartOpen(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                boolean isOpen = (boolean) result.data;
                LogUtil.d(isOpen+"");
                mSwitch.setSwitchState(isOpen);
            }

            @Override
            public void setSuccess() {

            }
        });
        //設置心率開關
        mSwitch.setOnSwitchListener(isSwitchOn -> BleSdkWrapper.setHeartTest(isSwitchOn, new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                LogUtil.d("設置成功");
            }

            @Override
            public void setSuccess() {

            }
        }));

        //獲取電池電量
//        BleSdkWrapper.getPower(new BleCallback() {
//            @Override
//            public void complete(int i, Object o) {
//                HandlerBleDataResult result = (HandlerBleDataResult) o;
//                int power = (int) result.data;
//                LogUtil.d(power+"");
//            }
//
//            @Override
//            public void setSuccess() {
//
//            }
//        });
    }
}
