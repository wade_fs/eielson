package com.zhj.bluetooth.sdkdemo;

import android.Manifest;
import android.app.Application;

import com.zhj.bluetooth.sdkdemo.util.PermissionUtil;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.util.LogUtil;

public class MyAppcation extends Application {

    private static  MyAppcation app;
    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static MyAppcation getInstance() {
        return app;
    }

}
