package com.zhj.bluetooth.sdkdemo.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.ui.adapter.SleepsHistoryAdapter;
import com.zhj.bluetooth.sdkdemo.ui.adapter.StepsHistoryAdapter;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthHeartRateItem;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthSleepItem;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthSportItem;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;
import com.zhj.bluetooth.zhjbluetoothsdk.util.LogUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SleepDataActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_sleep;
    }

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @Override
    protected void initView() {
        super.initView();
        titleName.setText("歷史步數和睡眠數據");
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
       syncStepHistory(true);
    }
    List<HealthSportItem> sportItemsAll = new ArrayList<>();//歷史步數數據
    List<HealthSleepItem> sleepItemsAll = new ArrayList<>();//歷史睡眠
    Calendar calendar;
    StepsHistoryAdapter stepsHistoryAdapter;
    SleepsHistoryAdapter sleepsHistoryAdapter;
    //歷史睡眠數據
    private void syncStepHistory(boolean isFirst){
        if (isFirst) {
            calendar = Calendar.getInstance();
        }
        final int year=calendar.get(Calendar.YEAR);
        final int month=calendar.get(Calendar.MONTH)+1;
        final int day=calendar.get(Calendar.DATE);
        BleSdkWrapper.getStepOrSleepHistory(year, month, day, new BleCallback() {
            @Override
            public void complete(int resultCode, Object data) {
                if (data instanceof HandlerBleDataResult){
                    HandlerBleDataResult result = (HandlerBleDataResult) data;
                    if (result.isComplete){
                        if (result.hasNext){//是否還有更多的歷史數據
                            List<HealthSportItem> sportItems = (List<HealthSportItem>) result.data;//歷史步數數據
                            List<HealthSleepItem> sleepItems = result.sleepItems;//歷史睡眠數數據
                            sportItemsAll.addAll(sportItems);
                            sleepItemsAll.addAll(sleepItems);
                            calendar.add(Calendar.DATE,-1);
                            syncStepHistory(false);
                        }else{
                            if(sportItemsAll != null){
                                stepsHistoryAdapter = new StepsHistoryAdapter(SleepDataActivity.this,sportItemsAll);
                                mRecyclerView.setAdapter(stepsHistoryAdapter);
                            }
                            if(sleepItemsAll != null){
                                sleepsHistoryAdapter = new SleepsHistoryAdapter(SleepDataActivity.this,sleepItemsAll);
                            }
                        }
                    }
                }
            }

            @Override
            public void setSuccess() {

            }
        });
    }


    @OnClick(R.id.tvShowSteps)
    void showHistorySteps(){
        if(stepsHistoryAdapter != null){
            mRecyclerView.setAdapter(stepsHistoryAdapter);
        }else{
            showToast("無歷史步數數據");
        }
    }

    @OnClick(R.id.tvShowSleeps)
    void showHistorySleeps(){
        if(sleepsHistoryAdapter != null){
            mRecyclerView.setAdapter(sleepsHistoryAdapter);
        }else{
            showToast("無歷史睡眠數據");
        }
    }

}
