package com.zhj.bluetooth.sdkdemo.presenter;

import android.bluetooth.BluetoothGatt;

import com.zhj.bluetooth.sdkdemo.MyAppcation;
import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BasePersenter;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.BLEDevice;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BaseAppBleListener;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleScanTool;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.util.SPHelper;

import static com.zhj.bluetooth.sdkdemo.ui.ScanDeviceReadyActivity.CMD_CONNECTING;
import static com.zhj.bluetooth.sdkdemo.ui.ScanDeviceReadyActivity.CMD_SCAN;


/**
 * Created by Administrator on 2019/7/8.
 */

public class ScanDevicePresenter extends BasePersenter<ScanDeviceContract.View> implements ScanDeviceContract.Presenter{

    @Override
    public void startScanBle(int cmd) {
        if (BleSdkWrapper.isConnected()) {
            BleSdkWrapper.disConnect();
        }
        BleSdkWrapper.startScanDevices(scanCallback);
    }

    @Override
    public void stopScanBle() {
        BleSdkWrapper.stopScanDevices();
        BleScanTool.getInstance().removeScanDeviceListener(scanCallback);
    }

    private BLEDevice connectDevice;
    @Override
    public void connecting(int cmd,BLEDevice device) {
        if (device == null) {
            return;
        }
        connectDevice = device;
        stopScanBle();
        if (BleSdkWrapper.isConnected()) {
            BleSdkWrapper.disConnect();
        }
        BleSdkWrapper.setBleListener(baseAppBleListener);
        BleSdkWrapper.connect(device);
    }
    private BaseAppBleListener baseAppBleListener = new BaseAppBleListener(){
        @Override
        public void onBLEConnected(BluetoothGatt bluetoothGatt) {
            super.onBLEConnected(bluetoothGatt);
        }

        @Override
        public void initComplete() {
            super.initComplete();
            SPHelper.saveBLEDevice(MyAppcation.getInstance(),connectDevice);
            mView.showMsg("連接成功");
            mView.requestSuccess(CMD_CONNECTING,connectDevice);
            BleSdkWrapper.removeListener(baseAppBleListener);
        }

        @Override
        public void onBLEDisConnected(String s) {
            super.onBLEDisConnected(s);
            BleSdkWrapper.removeListener(baseAppBleListener);
            mView.showMsg("連接失敗");
            mView.requestFaild();
        }

        @Override
        public void onBLEConnectTimeOut() {
            super.onBLEConnectTimeOut();
            BleSdkWrapper.removeListener(baseAppBleListener);
            mView.showMsg("連接超時");
            mView.requestFaild();
        }
    };

    private  BleScanTool.ScanDeviceListener scanCallback=new BleScanTool.ScanDeviceListener() {
        @Override
        public void onFind(BLEDevice device) {
            mView.requestSuccess(CMD_SCAN,device);
        }

        @Override
        public void onFinish() {
            BleSdkWrapper.stopScanDevices();
        }
    };

    @Override
    public void detachView() {
        super.detachView();
        BleSdkWrapper.removeListener(baseAppBleListener);
        BleScanTool.getInstance().removeScanDeviceListener(scanCallback);
    }

}
