package com.zhj.bluetooth.sdkdemo.base;


import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePersenter<V extends IBaseView> {
    protected V mView;

    public void attachView(V view) {
        this.mView = view;
    }

    public void detachView() {
        this.mView = null;
        dispose();
    }

    //將所有正在處理的Subscription都添加到CompositeSubscription中。統一退出的時候註銷觀察
    private CompositeDisposable mCompositeDisposable;

    protected void addDisposable(Disposable subscription) {
        if (mCompositeDisposable == null || mCompositeDisposable.isDisposed()) {
            //csb 如果解綁了的話添加 sb 需要新的實例否則綁定時無效的
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(subscription);
    }

    //在界面退出等需要解綁觀察者的情況下調用此方法統一解綁，防止Rx造成的內存泄漏
    public void dispose() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
        }
    }

}
