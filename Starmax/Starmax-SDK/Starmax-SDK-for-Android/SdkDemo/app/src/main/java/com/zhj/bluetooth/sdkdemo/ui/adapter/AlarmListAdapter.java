package com.zhj.bluetooth.sdkdemo.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseAdapter;
import com.zhj.bluetooth.sdkdemo.base.BaseViewHolder;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.Alarm;

import java.util.List;

import butterknife.BindView;


/**
 * Created by Administrator on 2019/7/11.
 */

public class AlarmListAdapter extends BaseAdapter<Alarm,AlarmListAdapter.ViewHolder> {

    public AlarmListAdapter(Context mContext, List<Alarm> mList) {
        super(mContext, mList);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onNormalBindViewHolder(AlarmListAdapter.ViewHolder holder, Alarm itemBean, int position) {
        holder.tvOpen.setText("閙鐘開關："+itemBean.getOn_off());
        //閙鐘周期從星期一開始  每一個Boolean值對應當天是否選中
        StringBuilder sb = new StringBuilder();
        for (boolean b : itemBean.getWeekRepeat()){
            sb.append(b);
            sb.append(",");
        }
        holder.tvCycle.setText("閙鐘周期："+sb.toString());

        holder.tvTime.setText("閙鐘時間："+itemBean.getAlarmHour()+":"+ itemBean.getAlarmMinute());
        //0x00:其他 0x01:喝水 0x02:吃藥 0x03:吃飯 0x04:運動 0x05:睡覺 0x06:起床 0x07:約會 0x08:聚會 0x09:會議
        holder.tvType.setText("閙鐘類型："+itemBean.getAlarmType());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_alarm_list,parent,false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvOpen)
        TextView tvOpen;
        @BindView(R.id.tvCycle)
        TextView tvCycle;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvType)
        TextView tvType;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
