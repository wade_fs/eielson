package com.zhj.bluetooth.sdkdemo.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;


import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseAdapter;
import com.zhj.bluetooth.sdkdemo.base.BaseMvpActivity;
import com.zhj.bluetooth.sdkdemo.presenter.ScanDeviceContract;
import com.zhj.bluetooth.sdkdemo.presenter.ScanDevicePresenter;
import com.zhj.bluetooth.sdkdemo.util.CommonDialog;
import com.zhj.bluetooth.sdkdemo.util.CommonUtil;
import com.zhj.bluetooth.sdkdemo.util.DialogHelperNew;
import com.zhj.bluetooth.sdkdemo.util.RecyclerRefreshLayout;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.BLEDevice;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallbackWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleScanTool;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.util.BaseMessage;
import com.zhj.bluetooth.zhjbluetoothsdk.util.SharePreferenceUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;



public class ScanDeviceReadyActivity extends BaseMvpActivity<ScanDevicePresenter> implements ScanDeviceContract.View , BaseAdapter.OnItemClickListener, RecyclerRefreshLayout.SuperRefreshLayoutListener{

    protected final static int ACCESS_FINE_LOCATION_REQUEST_CODE = 100;
    public static final int CMD_SCAN = 0X00;
    public static final int CMD_CONNECTING = 0X01;

    @BindView(R.id.refresh_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.mRefreshLayout)
    RecyclerRefreshLayout mRefreshLayout;
    private ScanDeviceAdapter mAdapter;
    @Override
    protected int getContentView() {
        return R.layout.activity_scan_device;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("綁定");
        mRefreshLayout.setSuperRefreshLayoutListener(this);
        mRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.CYAN);
        mRefreshLayout.setCanLoadMore(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (!CommonUtil.isOPen(this)){
            DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title),
                    getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), view -> {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, 1000);
                    }, view -> ScanDeviceReadyActivity.this.finish());
        }else{
            getScanDevice();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1000){
            if (!CommonUtil.isOPen(this)){
                ScanDeviceReadyActivity.this.finish();
            }else{
                getScanDevice();
            }
        }else if(requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE){
            if (!checkSelfPermission(permissionsLocation)) {
                requestPermissions(ACCESS_FINE_LOCATION_REQUEST_CODE, permissionsLocation);
            }else{
                mPresenter.startScanBle(CMD_SCAN);
            }
        }
    }

    private void getScanDevice(){
        mRefreshLayout.post(() -> {
            mRefreshLayout.setRefreshing(true);
            showList.clear();
            if(mAdapter != null){
                mAdapter.setData(showList);
            }
            if (checkSelfPermission(permissionsLocation)){
                mPresenter.startScanBle(CMD_SCAN);
            }else{
                requestPermissions(ACCESS_FINE_LOCATION_REQUEST_CODE,permissionsLocation);
            }
        });
    }

    private String[] permissionsLocation = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private Dialog mDialog;
    @Override
    public void requestPermissionsFail(int requestCode) {
        super.requestPermissionsFail(requestCode);
        if (requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsLocation[0])) {
                mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title),
                        getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), view -> {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent,ACCESS_FINE_LOCATION_REQUEST_CODE);
                        }, view -> {
                            mDialog.dismiss();
                            ScanDeviceReadyActivity.this.finish();
                        });
            }else{
                ScanDeviceReadyActivity.this.finish();
            }
        }
    }
    @Override
    public void requestPermissionsSuccess(int requestCode) {
        if (requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE) {
            mPresenter.startScanBle(CMD_SCAN);
        }
    }


    private List<BLEDevice> showList = new ArrayList<>();
    int i1 ;
    int i2 ;
    int i3 ;
    int i4 ;
    @Override
    public void requestSuccess(int code, BLEDevice device) {
        switch (code){
            case CMD_SCAN:
                mRefreshLayout.onComplete();
                if(!showList.contains(device)){
                    showList.add(device);
                    Collections.sort(showList);
                    if(mAdapter == null){
                        mAdapter = new ScanDeviceAdapter(this,showList);
                        mRecyclerView.setAdapter(mAdapter);
                    }else{
                        mAdapter.setData(showList);
                    }
                    mAdapter.setOnItemClickListener(this);
                }
                break;
            case CMD_CONNECTING:
                //生成隨機配對碼並下發給手環
                Random random = new Random();
                i1 = random.nextInt(10);
                i2 = random.nextInt(10);
                i3 = random.nextInt(10);
                i4 = random.nextInt(10);
                BleSdkWrapper.setPairingcode(i1,i2,i3,i4, new BleCallback() {
                    @Override
                    public void complete(int i, Object o) {
                        showPairingDialog();
                    }

                    @Override
                    public void setSuccess() {

                    }
                });
        }
    }


    private Dialog showPairingDialog(){
        Dialog dialog = new Dialog(this, R.style.center_dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_pairing,null);
        EditText et1 = view.findViewById(R.id.et1);
        EditText et2 = view.findViewById(R.id.et2);
        EditText et3 = view.findViewById(R.id.et3);
        EditText et4 = view.findViewById(R.id.et4);
        et1.setFocusable(true);
        et1.setFocusableInTouchMode(true);
        et1.requestFocus();
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() >=1){
                    et2.requestFocus();
                }
            }
        });
        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() >=1){
                    et3.requestFocus();
                }
            }
        });
        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() >=1){
                    et4.requestFocus();
                }
            }
        });

        view.findViewById(R.id.tvCanle).setOnClickListener(v -> {
            dialog.dismiss();
            //退出輸入  解綁
            BleSdkWrapper.exitPairingcode(false, new BleCallback() {
                @Override
                public void complete(int i, Object o) {
                    mAdapter.connecting(-1);
                    isConnecting = false;
                    BleSdkWrapper.unBind();
                }

                @Override
                public void setSuccess() {

                }
            });
        });
        view.findViewById(R.id.tvSure).setOnClickListener(v ->{
            dialog.dismiss();
            if(TextUtils.isEmpty(et1.getText().toString()) || TextUtils.isEmpty(et2.getText().toString()) ||
                    TextUtils.isEmpty(et3.getText().toString()) || TextUtils.isEmpty(et4.getText().toString()) ){
                showToast("請輸入完整的配對碼");
            }else{
                if(Integer.parseInt(et1.getText().toString()) == i1 &&
                        Integer.parseInt(et2.getText().toString()) == i2 &&
                        Integer.parseInt(et3.getText().toString()) == i3 &&
                        Integer.parseInt(et4.getText().toString()) == i4 ){
                    BleSdkWrapper.exitPairingcode(true, new BleCallback() {
                        @Override
                        public void complete(int i, Object o) {
                            isConnecting = false;
                            ScanDeviceReadyActivity.this.finish();
                        }

                        @Override
                        public void setSuccess() {

                        }
                    });
                }else{
                    showToast("配對碼輸入錯誤，請重新輸入！");
                    showPairingDialog();
                }
            }
        });
        dialog.setContentView(view);
        dialog.setCancelable(false);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = this.getResources().getDisplayMetrics(); // 獲取屏幕寬、高用
        lp.width = (int) (d.widthPixels * 0.8);
        dialogWindow.setAttributes(lp);
        dialog.show();
        return dialog;
    }

    @Override
    public void requestFaild() {
        if(mAdapter != null){
            isConnecting = false;
            mAdapter.connecting(-1);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        mRefreshLayout.onComplete();
        mPresenter.stopScanBle();
        hideLoading();
    }

    private boolean isConnecting = false;
    @Override
    public void onItemClick(View view, int position) {
        //連結設備
        if (!BleScanTool.getInstance().isBluetoothOpen()){
            showToast("藍芽未打開");
        }else{
            if(!isConnecting){
                mAdapter.connecting(position);
                isConnecting = true;
                mPresenter.connecting(CMD_CONNECTING,showList.get(position));
            }
        }
    }

    @Override
    public void onRefreshing() {
        if(isConnecting){
            showToast("正在綁定好");
        }else{
            isOpenBle();
        }
    }
    private void isOpenBle(){
        if (!BleScanTool.getInstance().isBluetoothOpen()){
            mRefreshLayout.onComplete();
            CommonDialog commonDialog=new CommonDialog.Builder(this)
                    .isVertical(false).setTitle("藍芽未打開")
                    .setLeftButton(R.string.cancel, (dialog, which) -> ScanDeviceReadyActivity.this.finish())
                    .setMessage("請開啟藍芽用於連接設備")
                    .setRightButton("設置", (dialog, which) -> BleScanTool.getInstance().openBluetooth())
                    .create();
            commonDialog.show();
        }else{
            getScanDevice();
        }
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onBackPressed() {
        if(isConnecting){
            showToast("正在綁定");
        }else{
            this.finish();
        }

    }
}
