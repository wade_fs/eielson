package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthHeartRate;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthSport;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class StepsDataActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_steps;
    }

    @BindView(R.id.tvSteps)
    TextView tvSteps;
    @BindView(R.id.tvCal)
    TextView tvCal;
    @BindView(R.id.tvDistance)
    TextView tvDistance;
    @BindView(R.id.tvRate)
    TextView tvRate;

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("當前步數");
        //獲取當前步數
        BleSdkWrapper.getCurrentStep(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                HealthSport sport = (HealthSport) result.data;
                tvSteps.setText("當前步數："+sport.getTotalStepCount()+"步");
                tvCal.setText("當前卡路里："+sport.getTotalCalory()+"千卡");//單位千卡
                tvDistance.setText("當前距離："+sport.getTotalDistance()+"米");//單位米
                getCurrentRate();
            }

            @Override
            public void setSuccess() {

            }
        });
    }


    private void getCurrentRate(){
        //獲取當前心率
        BleSdkWrapper.getHeartRate(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                HealthHeartRate heartRate = (HealthHeartRate) result.data;
                tvRate.setText("當前心率："+heartRate.getSilentHeart());
            }
            @Override
            public void setSuccess() {
            }
        });
    }
}
