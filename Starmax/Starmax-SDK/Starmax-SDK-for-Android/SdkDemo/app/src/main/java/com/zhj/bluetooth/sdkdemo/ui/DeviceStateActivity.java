package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.DeviceState;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class DeviceStateActivity extends BaseActivity {

    @BindView(R.id.tvScreenLight)
    TextView tvScreenLight;
    @BindView(R.id.tvScreenTime)
    TextView tvScreenTime;
    @BindView(R.id.tvScreenTheme)
    TextView tvScreenTheme;
    @BindView(R.id.tvLanguage)
    TextView tvLanguage;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.tvTimeFormat)
    TextView tvTimeFormat;
    @BindView(R.id.tvUpHander)
    TextView tvUpHander;
    @BindView(R.id.tvMusic)
    TextView tvMusic;
    @BindView(R.id.tvNotice)
    TextView tvNotice;
    @BindView(R.id.tvHandHabits)
    TextView tvHandHabits;

    @Override
    protected int getContentView() {
        return R.layout.activity_device_state;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("設備狀態");
        BleSdkWrapper.getDeviceState(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                DeviceState deviceState = (DeviceState) result.data;
                tvScreenLight.setText("屏幕亮度："+deviceState.screenLight);
                tvScreenTime.setText("亮屏時長："+deviceState.screenTime);
                tvScreenTheme.setText("主題："+deviceState.theme);
                //  0x00：英文 0x01：中文 0x02: 俄羅斯語 0x03: 烏克蘭語 0x04：法語 0x05：西班牙語
                //  0x06：葡萄牙語 0x07：德語 0x08：日本 0x09：波蘭 0x0A：意大利
                //0x0B：羅馬尼亞 0x0C: 繁體中文 0x0D: 韓語
                tvLanguage.setText("語言："+deviceState.language);
                //0x00:公制 0x01:英制
                tvUnit.setText("單位："+deviceState.unit);
                //0x00：24 小時制 0x01：12 小時制
                tvTimeFormat.setText("時間制："+deviceState.timeFormat);
                //0x00:關閉 0x01:開啟
                tvUpHander.setText("抬手亮屏："+deviceState.upHander);
                //0x00:關閉 0x01:開啟
                tvMusic.setText("音樂控制："+deviceState.isMusic);
                //0x00:關閉 0x01:開啟
                tvNotice.setText("消息開關："+deviceState.isNotice);
                //0x01:左手 0x02:右手 其他：無效
                tvHandHabits.setText("左右手習慣方式:"+deviceState.handHabits);
            }

            @Override
            public void setSuccess() {

            }
        });
    }
}
