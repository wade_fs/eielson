package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.DeviceState;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.Goal;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class GoalActivity extends BaseActivity {

    @BindView(R.id.tvSleepTimeState)
    TextView tvSleepTimeState;
    @BindView(R.id.tvSleepTime)
    TextView tvSleepTime;
    @BindView(R.id.tvStepsState)
    TextView tvStepsState;
    @BindView(R.id.tvSteps)
    TextView tvSteps;
    @BindView(R.id.tvCalState)
    TextView tvCalState;
    @BindView(R.id.tvCal)
    TextView tvCal;
    @BindView(R.id.tvDistanceState)
    TextView tvDistanceState;
    @BindView(R.id.tvDistance)
    TextView tvDistance;

    @Override
    protected int getContentView() {
        return R.layout.activity_goal;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("目標信息");
        BleSdkWrapper.getTarget(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                Goal goal = (Goal) result.data;
                tvSleepTimeState.setText("睡眠目標開關："+goal.sleepstate);//0x01:開 0x00:關
                tvSleepTime.setText("睡眠目標時間："+goal.goalSleep+"小時");//單位小時
                tvStepsState.setText("步數目標開關："+goal.stepstate);//0x01:開 0x00:關
                tvSteps.setText("步數目標："+goal.goalStep+"步");//單位步
                tvCalState.setText("卡路里目標開關："+goal.calstate);//0x01:開 0x00:關
                tvCal.setText("卡路里目標："+goal.goalCal+"千卡");//單位千卡
                tvDistanceState.setText("距離目標開關："+goal.distancestate);//0x01:開 0x00:關
                tvDistance.setText("距離目標："+goal.goalDistanceKm+"千米");//單位千米
            }

            @Override
            public void setSuccess() {

            }
        });
    }
}
