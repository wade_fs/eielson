package com.zhj.bluetooth.sdkdemo.ui;

import android.widget.TextView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.BLEDevice;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;

import butterknife.BindView;

public class DeviceInfoActivity extends BaseActivity {
    @BindView(R.id.tvDeviceProduct)
    TextView tvDeviceProduct;
    @BindView(R.id.tvDeviceAddress)
    TextView tvDeviceAddress;
    @BindView(R.id.tvDeviceVersion)
    TextView tvDeviceVersion;
    @BindView(R.id.tvDevicePower)
    TextView tvDevicePower;
    @Override
    protected int getContentView() {
        return R.layout.activity_device_info;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("設備信息");
        //設備信息裡面的設備名稱和信號值再搜索設備的時候已經返回了
        BleSdkWrapper.getDeviceInfo(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                BLEDevice bleDevice = (BLEDevice) result.data;
                tvDeviceProduct.setText("設備型號："+bleDevice.mDeviceProduct);
                tvDeviceAddress.setText("設備地址："+bleDevice.mDeviceAddress);
                tvDeviceVersion.setText("設備版本:"+bleDevice.mDeviceVersion);
                getPower();
            }

            @Override
            public void setSuccess() {

            }
        });
    }

    private void getPower(){
        //獲取設備電量
        BleSdkWrapper.getPower(new BleCallback() {
            @Override
            public void complete(int i, Object o) {
                HandlerBleDataResult result = (HandlerBleDataResult) o;
                int power = (int) result.data;
                tvDevicePower.setText("設備電量:"+power);
            }

            @Override
            public void setSuccess() {

            }
        });
    }
}
