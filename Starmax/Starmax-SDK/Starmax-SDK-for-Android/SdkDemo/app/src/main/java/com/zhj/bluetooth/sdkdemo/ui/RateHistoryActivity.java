package com.zhj.bluetooth.sdkdemo.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.bluetooth.sdkdemo.R;
import com.zhj.bluetooth.sdkdemo.base.BaseActivity;
import com.zhj.bluetooth.sdkdemo.ui.adapter.RateHistoryAdapter;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthActivity;
import com.zhj.bluetooth.zhjbluetoothsdk.bean.HealthHeartRateItem;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleCallback;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.BleSdkWrapper;
import com.zhj.bluetooth.zhjbluetoothsdk.ble.HandlerBleDataResult;
import com.zhj.bluetooth.zhjbluetoothsdk.util.LogUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;

public class RateHistoryActivity extends BaseActivity {
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @Override
    protected int getContentView() {
        return R.layout.activity_rate_history;
    }

    @Override
    protected void initView() {
        super.initView();
        titleName.setText("歷史心率");
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        syncHeartHistory(true);
    }
    List<HealthHeartRateItem> healthHeartRateItemsAll = new ArrayList<>();
    Calendar calendar;
    //歷史心率數據
    private void syncHeartHistory(boolean isFirst){
        if (isFirst) {
            calendar = Calendar.getInstance();
        }
        final int year=calendar.get(Calendar.YEAR);
        final int month=calendar.get(Calendar.MONTH)+1;
        final int day=calendar.get(Calendar.DATE);
        BleSdkWrapper.getHistoryHeartRateData( year, month, day, new BleCallback() {
            @Override
            public void complete(int resultCode, Object data) {
                if (data instanceof HandlerBleDataResult){
                    HandlerBleDataResult result = (HandlerBleDataResult) data;
                    if (result.isComplete){
                        if (result.hasNext){//是否還有更多的歷史數據
                            List<HealthHeartRateItem> healthHeartRateItems = (List<HealthHeartRateItem>) result.data;
                            healthHeartRateItemsAll.addAll(healthHeartRateItems);
                            calendar.add(Calendar.DATE,-1);
                            syncHeartHistory(false);
                        }else{
                            LogUtil.d("接收數據結束");
                            RateHistoryAdapter rateHistoryAdapter = new RateHistoryAdapter(RateHistoryActivity.this,healthHeartRateItemsAll);
                            mRecyclerView.setAdapter(rateHistoryAdapter);
                        }
                    }
                }
            }

            @Override
            public void setSuccess() {

            }
        });
    }

    //歷史活動數據
    private void synchActiviy(){
        BleSdkWrapper.getActivity(new BleCallback() {
            @Override
            public void setSuccess() {
            }

            @Override
            public void complete(int resultCode, Object data) {
                if (data!=null){
                    if (data instanceof HandlerBleDataResult){
                        HandlerBleDataResult result = (HandlerBleDataResult) data;
                        //上一次完成了。是否還有下一次數據
                        if (result.isComplete){
                            if (result.hasNext){
                                synchActiviy();
                            }else{
                                List<HealthActivity> healthActivities = (List<HealthActivity>) result.data;
                                LogUtil.d("接收完成"+healthActivities.size());
                            }
                        }
                    }
                }
            }
        });
    }
}
