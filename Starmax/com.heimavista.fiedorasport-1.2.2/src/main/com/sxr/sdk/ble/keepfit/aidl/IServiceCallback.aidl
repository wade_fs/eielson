// IServiceCallback.aidl
package com.sxr.sdk.ble.keepfit.aidl;

interface IServiceCallback {
	//0
	void onAuthSdkResult(int errorCode);
	//1
	void onScanCallback(String deviceName, String deviceMacAddress, int rssi);
	//2
	void onConnectStateChanged(int state);
	void onAuthDeviceResult(int errorCode);
	//3
	void onGetDeviceTime(int result, String time);

	//14
	void onSetDeviceTime(int result);
	void onSetUserInfo(int result);
	void onGetCurSportData(int type,long timestamp,int step,int distance,int cal,int cursleeptime,int totalrunningtime,int steptime);
	void onSendVibrationSignal(int result);
	void onSetPhontMode(int result);
	void onSetIdleTime(int result);
	void onSetSleepTime(int result);
	void onGetDeviceBatery(int batery,int statu);
	void onGetDeviceInfo(int version,String deviceMacAddress,String vendorCode,String productCode,int crcResult);//crcResult 1：成功  0:失败
	void onSetAlarm(int result);
	void onSetDeviceMode(int result);
	void onSetNotify(int result);
	void onGetSenserData(int result,long timestamp,int heartrate,int sleepstatu);  
	void setAutoHeartMode(int result);
	void onSetDeviceInfo(int result);
	void onSetHourFormat(int result);
	void onGetDataByDay(int type,long timestamp,int step,int heartrate);
	void onGetDataByDayEnd(int type,long timestamp);
	
	void onGetDeviceAction(int type);
	
    //  读取手环功能项
    void onGetBandFunction(int result,in boolean[] list);
    //  发送手机系统使用语言
    void onSetLanguage(int result);
    // 回复天气
    void onSendWeather(int result);
    //设置防丢
    void onSetAntiLost(int result);
    //打开关闭血压，血氧，疲劳度
    void onSetBloodPressureMode(int result);
    //当手环采集到有效血压血氧值后，会通过此命令返回数据给app
    void onReceiveSensorData(int heartrate, int Systolicpressure, int Diastolicpressure, int Oxygen, int Fatiguevalue);
    //取得某天多运动模式数据
    void onGetMultipleSportData(int flag,String recorddate,int mode,int value);
    //设置目标步数
    void onSetGoalStep(int result);
    //设置心率区间
    void onSetDeviceHeartRateArea(int result);
    //心率 血压血氧结束测试返回
    void onSensorStateChange(int type , int state); //type 1:心率  2：血压血氧  , state 1:打开  0：关闭
    //返回当前运动模式数据
    void onReadCurrentSportData(int mode,String recorddate,int step,int cal); //mode :当前运动模式，0为不在多运动模式  recorddate :时间     step :在步行，跑步，登山模式下为当前计步值，其它模式下无效 cal:当前卡路里值

    // 返回升级信息
    void onGetOtaInfo(boolean isUpdate, String otaInfo, String path);
    // 升级过程
    void onGetOtaUpdate(int step, int progress);

    // 设置机器串码
    void onSetDeviceCode(int result);
    // 获取机器串码
    void onGetDeviceCode(in byte[] bytes);

    // 获取蓝牙数据
    void onCharacteristicChanged(String uuid, in byte[] bytes);
    // 写入蓝牙数据
    void onCharacteristicWrite(String uuid, in byte[] bytes, int status);

    // 在线心电
    void onSetEcgMode(int result, int state);
    void onGetEcgValue(int state, in int[] values);

    // 离线心电
    void onGetEcgHistory(long timestamp, int number);
    void onGetEcgStartEnd(int id, int state, long timestamp);
    void onGetEcgHistoryData(int id, in int[] values);

    // 设置手环名称
    void onSetDeviceName(int result);

    // 获取设备信号强度
    void onGetDeviceRssi(int rssi);
    // 设置提醒时间
    void onSetReminder(int result);
    // 设置提醒内容
    void onSetReminderText(int result);
}

