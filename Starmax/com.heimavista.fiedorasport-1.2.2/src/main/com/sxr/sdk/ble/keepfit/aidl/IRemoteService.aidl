// IRemoteService.aidl
package com.sxr.sdk.ble.keepfit.aidl;

// 注意这里需要import
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import com.sxr.sdk.ble.keepfit.aidl.BleClientOption;

interface IRemoteService {    
    void registerCallback(IServiceCallback cb);
    void unregisterCallback(IServiceCallback cb);
    //0
    int isAuthrize();
    int setOption(in BleClientOption opt);
    //1
    int setScanMode(int scanMode);
    int scanDevice(boolean enable);
    //2
    int connectBt(String name, String addr);
    boolean isConnectBt();
    String getConnectedDevice();
    void disconnectBt(boolean enable);

	//1
    int setDeviceTime();
    //2
    int setUserInfo();
    //3
    int getCurSportData();
    //4
    int sendVibrationSignal(int times);
    //5
    int setPhontMode(boolean enable);
    //6
    int setIdleTime(int time,int startHour,int startMinute,int endHour,int endMinute);
    //7
    int setSleepTime(int startHourNoon,int startMinuteNoon,int endHourNoon,int endMinuteNoon,int startHourNight,int startMinuteNight,int endHourNight,int endMinuteNight);
    //8
    int getDeviceBatery();
    //9
    int getDeviceInfo();
   	//10
    int setAlarm();
    //11
     int setDeviceMode(int type);
  	//12  消息推送
  	boolean setNotify(String id,int type,String title,String content);
  	//13
    int setHeartRateMode(boolean enable,int time);
    //14
    int setAutoHeartMode(boolean enable,int startHour,int startMinute,int endHour,int endMinute,int interval , int duration);
    //15
    int setDeviceInfo();
    //16
    int setHourFormat(int type);
    //17
    int getDataByDay(int type,int day);
    // 18 发送手机系统使用语言
    int setLanguage();
    // 19 发送天气数据
    int sendWeather();
    // 20 打开关闭断连防丢提醒
    int setAntiLost(boolean enable);
    // 21 打开关闭血压，血氧，疲劳度
    int setBloodPressureMode(boolean enable);
    // 22 取得某天多运动模式数据
    int getMultipleSportData(int day);
    // 23 设置运动目标步数
    int setGoalStep(int step);
    // 24 获取手环功能项
    int getBandFunction();
    //25设置心率区间
    int setDeviceHeartRateArea(boolean enable,int max ,int min);
    //26 开关SDK日志功能 默认关闭 测试用
    int openSDKLog(boolean enable,String logPath,String fileName);

    //27 获取固件升级信息
    int getOtaInfo(boolean auto);
    // 设置机器串码
    int setDeviceCode(in byte[] bytes);
    // 获取机器串码
    int getDeviceCode();

    // 设置UUID
    int setUuid(in String[] uuidRead, in String[] uuidWrite, boolean bOpen);
    // 发送蓝牙数据
    int writeCharacteristic(String uuidWrite, in byte[] bytes);

    // 开启关闭心电
    int setEcgMode(boolean enable);
    // 获取心电历史
    int getEcgHistory(int timestamp);
    // 设置手环名称
    int setDeviceName(String name);
    // 获取设备信号强度
    int getDeviceRssi();
    // 设置提醒时间
    int setReminder(int time,int startHour,int startMinute,int endHour,int endMinute, int id, int type);
    // 设置提醒内容
    int setReminderText(int id, String text);
}

