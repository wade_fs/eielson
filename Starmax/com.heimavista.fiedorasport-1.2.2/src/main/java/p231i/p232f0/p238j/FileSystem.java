package p231i.p232f0.p238j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import p244j.C5049s;
import p244j.Okio;
import p244j.Sink;

/* renamed from: i.f0.j.a */
public interface FileSystem {

    /* renamed from: a */
    public static final FileSystem f11644a = new C4999a();

    /* renamed from: i.f0.j.a$a */
    /* compiled from: FileSystem */
    class C4999a implements FileSystem {
        C4999a() {
        }

        /* renamed from: a */
        public C5049s mo27619a(File file) {
            return Okio.m18224c(file);
        }

        /* renamed from: b */
        public Sink mo27621b(File file) {
            try {
                return Okio.m18221b(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return Okio.m18221b(file);
            }
        }

        /* renamed from: c */
        public void mo27622c(File file) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                int length = listFiles.length;
                int i = 0;
                while (i < length) {
                    File file2 = listFiles[i];
                    if (file2.isDirectory()) {
                        mo27622c(file2);
                    }
                    if (file2.delete()) {
                        i++;
                    } else {
                        throw new IOException("failed to delete " + file2);
                    }
                }
                return;
            }
            throw new IOException("not a readable directory: " + file);
        }

        /* renamed from: d */
        public boolean mo27623d(File file) {
            return file.exists();
        }

        /* renamed from: e */
        public void mo27624e(File file) {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        /* renamed from: f */
        public Sink mo27625f(File file) {
            try {
                return Okio.m18214a(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return Okio.m18214a(file);
            }
        }

        /* renamed from: g */
        public long mo27626g(File file) {
            return file.length();
        }

        /* renamed from: a */
        public void mo27620a(File file, File file2) {
            mo27624e(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }
    }

    /* renamed from: a */
    C5049s mo27619a(File file);

    /* renamed from: a */
    void mo27620a(File file, File file2);

    /* renamed from: b */
    Sink mo27621b(File file);

    /* renamed from: c */
    void mo27622c(File file);

    /* renamed from: d */
    boolean mo27623d(File file);

    /* renamed from: e */
    void mo27624e(File file);

    /* renamed from: f */
    Sink mo27625f(File file);

    /* renamed from: g */
    long mo27626g(File file);
}
