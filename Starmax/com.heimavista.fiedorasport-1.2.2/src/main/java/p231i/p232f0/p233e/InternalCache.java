package p231i.p232f0.p233e;

import p231i.C5023y;
import p231i.Response;

/* renamed from: i.f0.e.f */
public interface InternalCache {
    /* renamed from: a */
    CacheRequest mo27358a(Response a0Var);

    /* renamed from: a */
    void mo27359a();

    /* renamed from: a */
    void mo27360a(Response a0Var, Response a0Var2);

    /* renamed from: a */
    void mo27361a(CacheStrategy cVar);

    /* renamed from: a */
    void mo27362a(C5023y yVar);

    /* renamed from: b */
    Response mo27363b(C5023y yVar);
}
