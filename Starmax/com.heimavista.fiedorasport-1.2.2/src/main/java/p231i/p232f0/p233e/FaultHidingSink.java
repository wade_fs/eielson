package p231i.p232f0.p233e;

import java.io.IOException;
import p244j.Buffer;
import p244j.ForwardingSink;
import p244j.Sink;

/* renamed from: i.f0.e.e */
class FaultHidingSink extends ForwardingSink {

    /* renamed from: Q */
    private boolean f11371Q;

    FaultHidingSink(Sink rVar) {
        super(rVar);
    }

    /* renamed from: a */
    public void mo27444a(Buffer cVar, long j) {
        if (this.f11371Q) {
            cVar.skip(j);
            return;
        }
        try {
            super.mo27444a(cVar, j);
        } catch (IOException e) {
            this.f11371Q = true;
            mo27433a(e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo27433a(IOException iOException) {
        throw null;
    }

    public void close() {
        if (!this.f11371Q) {
            try {
                super.close();
            } catch (IOException e) {
                this.f11371Q = true;
                mo27433a(e);
            }
        }
    }

    public void flush() {
        if (!this.f11371Q) {
            try {
                super.flush();
            } catch (IOException e) {
                this.f11371Q = true;
                mo27433a(e);
            }
        }
    }
}
