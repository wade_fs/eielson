package p231i.p232f0.p233e;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import p231i.C5015r;
import p231i.C5023y;
import p231i.CacheControl;
import p231i.Response;
import p231i.p232f0.Internal;
import p231i.p232f0.p235g.HttpDate;
import p231i.p232f0.p235g.HttpHeaders;

/* renamed from: i.f0.e.c */
public final class CacheStrategy {

    /* renamed from: a */
    public final C5023y f11317a;

    /* renamed from: b */
    public final Response f11318b;

    CacheStrategy(C5023y yVar, Response a0Var) {
        this.f11317a = yVar;
        this.f11318b = a0Var;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        if (r3.mo27317b().mo27379b() == false) goto L_0x0059;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m17380a(p231i.Response r3, p231i.C5023y r4) {
        /*
            int r0 = r3.mo27321d()
            r1 = 200(0xc8, float:2.8E-43)
            r2 = 0
            if (r0 == r1) goto L_0x005a
            r1 = 410(0x19a, float:5.75E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 414(0x19e, float:5.8E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 501(0x1f5, float:7.02E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 203(0xcb, float:2.84E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 204(0xcc, float:2.86E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 307(0x133, float:4.3E-43)
            if (r0 == r1) goto L_0x0031
            r1 = 308(0x134, float:4.32E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 404(0x194, float:5.66E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 405(0x195, float:5.68E-43)
            if (r0 == r1) goto L_0x005a
            switch(r0) {
                case 300: goto L_0x005a;
                case 301: goto L_0x005a;
                case 302: goto L_0x0031;
                default: goto L_0x0030;
            }
        L_0x0030:
            goto L_0x0059
        L_0x0031:
            java.lang.String r0 = "Expires"
            java.lang.String r0 = r3.mo27318b(r0)
            if (r0 != 0) goto L_0x005a
            i.d r0 = r3.mo27317b()
            int r0 = r0.mo27381d()
            r1 = -1
            if (r0 != r1) goto L_0x005a
            i.d r0 = r3.mo27317b()
            boolean r0 = r0.mo27380c()
            if (r0 != 0) goto L_0x005a
            i.d r0 = r3.mo27317b()
            boolean r0 = r0.mo27379b()
            if (r0 == 0) goto L_0x0059
            goto L_0x005a
        L_0x0059:
            return r2
        L_0x005a:
            i.d r3 = r3.mo27317b()
            boolean r3 = r3.mo27386i()
            if (r3 != 0) goto L_0x006f
            i.d r3 = r4.mo27821b()
            boolean r3 = r3.mo27386i()
            if (r3 != 0) goto L_0x006f
            r2 = 1
        L_0x006f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.CacheStrategy.m17380a(i.a0, i.y):boolean");
    }

    /* renamed from: i.f0.e.c$a */
    /* compiled from: CacheStrategy */
    public static class C4956a {

        /* renamed from: a */
        final long f11319a;

        /* renamed from: b */
        final C5023y f11320b;

        /* renamed from: c */
        final Response f11321c;

        /* renamed from: d */
        private Date f11322d;

        /* renamed from: e */
        private String f11323e;

        /* renamed from: f */
        private Date f11324f;

        /* renamed from: g */
        private String f11325g;

        /* renamed from: h */
        private Date f11326h;

        /* renamed from: i */
        private long f11327i;

        /* renamed from: j */
        private long f11328j;

        /* renamed from: k */
        private String f11329k;

        /* renamed from: l */
        private int f11330l = -1;

        public C4956a(long j, C5023y yVar, Response a0Var) {
            this.f11319a = j;
            this.f11320b = yVar;
            this.f11321c = a0Var;
            if (a0Var != null) {
                this.f11327i = a0Var.mo27314B();
                this.f11328j = a0Var.mo27331z();
                C5015r g = a0Var.mo27323g();
                int b = g.mo27739b();
                for (int i = 0; i < b; i++) {
                    String a = g.mo27737a(i);
                    String b2 = g.mo27740b(i);
                    if ("Date".equalsIgnoreCase(a)) {
                        this.f11322d = HttpDate.m17493a(b2);
                        this.f11323e = b2;
                    } else if ("Expires".equalsIgnoreCase(a)) {
                        this.f11326h = HttpDate.m17493a(b2);
                    } else if ("Last-Modified".equalsIgnoreCase(a)) {
                        this.f11324f = HttpDate.m17493a(b2);
                        this.f11325g = b2;
                    } else if ("ETag".equalsIgnoreCase(a)) {
                        this.f11329k = b2;
                    } else if ("Age".equalsIgnoreCase(a)) {
                        this.f11330l = HttpHeaders.m17494a(b2, -1);
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* renamed from: b */
        private long m17382b() {
            Date date = this.f11322d;
            long j = 0;
            if (date != null) {
                j = Math.max(0L, this.f11328j - date.getTime());
            }
            int i = this.f11330l;
            if (i != -1) {
                j = Math.max(j, TimeUnit.SECONDS.toMillis((long) i));
            }
            long j2 = this.f11328j;
            return j + (j2 - this.f11327i) + (this.f11319a - j2);
        }

        /* renamed from: c */
        private long m17383c() {
            long j;
            long j2;
            CacheControl b = this.f11321c.mo27317b();
            if (b.mo27381d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) b.mo27381d());
            }
            if (this.f11326h != null) {
                Date date = this.f11322d;
                if (date != null) {
                    j2 = date.getTime();
                } else {
                    j2 = this.f11328j;
                }
                long time = this.f11326h.getTime() - j2;
                if (time > 0) {
                    return time;
                }
                return 0;
            } else if (this.f11324f == null || this.f11321c.mo27313A().mo27827g().mo27766k() != null) {
                return 0;
            } else {
                Date date2 = this.f11322d;
                if (date2 != null) {
                    j = date2.getTime();
                } else {
                    j = this.f11327i;
                }
                long time2 = j - this.f11324f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* renamed from: d */
        private CacheStrategy m17384d() {
            if (this.f11321c == null) {
                return new CacheStrategy(this.f11320b, null);
            }
            if (this.f11320b.mo27824d() && this.f11321c.mo27322e() == null) {
                return new CacheStrategy(this.f11320b, null);
            }
            if (!CacheStrategy.m17380a(this.f11321c, this.f11320b)) {
                return new CacheStrategy(this.f11320b, null);
            }
            CacheControl b = this.f11320b.mo27821b();
            if (b.mo27385h() || m17381a(this.f11320b)) {
                return new CacheStrategy(this.f11320b, null);
            }
            CacheControl b2 = this.f11321c.mo27317b();
            if (b2.mo27378a()) {
                return new CacheStrategy(null, this.f11321c);
            }
            long b3 = m17382b();
            long c = m17383c();
            if (b.mo27381d() != -1) {
                c = Math.min(c, TimeUnit.SECONDS.toMillis((long) b.mo27381d()));
            }
            long j = 0;
            long millis = b.mo27383f() != -1 ? TimeUnit.SECONDS.toMillis((long) b.mo27383f()) : 0;
            if (!b2.mo27384g() && b.mo27382e() != -1) {
                j = TimeUnit.SECONDS.toMillis((long) b.mo27382e());
            }
            if (!b2.mo27385h()) {
                long j2 = millis + b3;
                if (j2 < j + c) {
                    Response.C4938a w = this.f11321c.mo27328w();
                    if (j2 >= c) {
                        w.mo27341a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (b3 > 86400000 && m17385e()) {
                        w.mo27341a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new CacheStrategy(null, w.mo27342a());
                }
            }
            String str = this.f11329k;
            String str2 = "If-Modified-Since";
            if (str != null) {
                str2 = "If-None-Match";
            } else if (this.f11324f != null) {
                str = this.f11325g;
            } else if (this.f11322d == null) {
                return new CacheStrategy(this.f11320b, null);
            } else {
                str = this.f11323e;
            }
            C5015r.C5016a a = this.f11320b.mo27823c().mo27736a();
            Internal.f11301a.mo27405a(a, str2, str);
            C5023y.C5024a f = this.f11320b.mo27826f();
            f.mo27830a(a.mo27747a());
            return new CacheStrategy(f.mo27835a(), this.f11321c);
        }

        /* renamed from: e */
        private boolean m17385e() {
            return this.f11321c.mo27317b().mo27381d() == -1 && this.f11326h == null;
        }

        /* renamed from: a */
        public CacheStrategy mo27417a() {
            CacheStrategy d = m17384d();
            return (d.f11317a == null || !this.f11320b.mo27821b().mo27387j()) ? d : new CacheStrategy(null, null);
        }

        /* renamed from: a */
        private static boolean m17381a(C5023y yVar) {
            return (yVar.mo27820a("If-Modified-Since") == null && yVar.mo27820a("If-None-Match") == null) ? false : true;
        }
    }
}
