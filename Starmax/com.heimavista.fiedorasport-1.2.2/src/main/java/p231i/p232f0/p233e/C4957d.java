package p231i.p232f0.p233e;

import com.facebook.appevents.AppEventsConstants;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import p231i.p232f0.C4951c;
import p231i.p232f0.p238j.FileSystem;
import p231i.p232f0.p239k.Platform;
import p241io.jsonwebtoken.JwtParser;
import p244j.BufferedSink;
import p244j.C5049s;
import p244j.Okio;
import p244j.Sink;

/* renamed from: i.f0.e.d */
/* compiled from: DiskLruCache */
public final class C4957d implements Closeable, Flushable {

    /* renamed from: j0 */
    static final Pattern f11331j0 = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: P */
    final FileSystem f11332P;

    /* renamed from: Q */
    final File f11333Q;

    /* renamed from: R */
    private final File f11334R;

    /* renamed from: S */
    private final File f11335S;

    /* renamed from: T */
    private final File f11336T;

    /* renamed from: U */
    private final int f11337U;

    /* renamed from: V */
    private long f11338V;

    /* renamed from: W */
    final int f11339W;

    /* renamed from: X */
    private long f11340X = 0;

    /* renamed from: Y */
    BufferedSink f11341Y;

    /* renamed from: Z */
    final LinkedHashMap<String, C4962d> f11342Z = new LinkedHashMap<>(0, 0.75f, true);

    /* renamed from: a0 */
    int f11343a0;

    /* renamed from: b0 */
    boolean f11344b0;

    /* renamed from: c0 */
    boolean f11345c0;

    /* renamed from: d0 */
    boolean f11346d0;

    /* renamed from: e0 */
    boolean f11347e0;

    /* renamed from: f0 */
    boolean f11348f0;

    /* renamed from: g0 */
    private long f11349g0 = 0;

    /* renamed from: h0 */
    private final Executor f11350h0;

    /* renamed from: i0 */
    private final Runnable f11351i0 = new C4958a();

    /* renamed from: i.f0.e.d$a */
    /* compiled from: DiskLruCache */
    class C4958a implements Runnable {
        C4958a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r5.f11352P.f11348f0 = true;
            r5.f11352P.f11341Y = p244j.Okio.m18211a(p244j.Okio.m18213a());
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0033 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                i.f0.e.d r0 = p231i.p232f0.p233e.C4957d.this
                monitor-enter(r0)
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ all -> 0x0045 }
                boolean r1 = r1.f11345c0     // Catch:{ all -> 0x0045 }
                r2 = 0
                r3 = 1
                if (r1 != 0) goto L_0x000d
                r1 = 1
                goto L_0x000e
            L_0x000d:
                r1 = 0
            L_0x000e:
                i.f0.e.d r4 = p231i.p232f0.p233e.C4957d.this     // Catch:{ all -> 0x0045 }
                boolean r4 = r4.f11346d0     // Catch:{ all -> 0x0045 }
                r1 = r1 | r4
                if (r1 == 0) goto L_0x0017
                monitor-exit(r0)     // Catch:{ all -> 0x0045 }
                return
            L_0x0017:
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ IOException -> 0x001d }
                r1.mo27431g()     // Catch:{ IOException -> 0x001d }
                goto L_0x0021
            L_0x001d:
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ all -> 0x0045 }
                r1.f11347e0 = r3     // Catch:{ all -> 0x0045 }
            L_0x0021:
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ IOException -> 0x0033 }
                boolean r1 = r1.mo27427d()     // Catch:{ IOException -> 0x0033 }
                if (r1 == 0) goto L_0x0043
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ IOException -> 0x0033 }
                r1.mo27429e()     // Catch:{ IOException -> 0x0033 }
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ IOException -> 0x0033 }
                r1.f11343a0 = r2     // Catch:{ IOException -> 0x0033 }
                goto L_0x0043
            L_0x0033:
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ all -> 0x0045 }
                r1.f11348f0 = r3     // Catch:{ all -> 0x0045 }
                i.f0.e.d r1 = p231i.p232f0.p233e.C4957d.this     // Catch:{ all -> 0x0045 }
                j.r r2 = p244j.Okio.m18213a()     // Catch:{ all -> 0x0045 }
                j.d r2 = p244j.Okio.m18211a(r2)     // Catch:{ all -> 0x0045 }
                r1.f11341Y = r2     // Catch:{ all -> 0x0045 }
            L_0x0043:
                monitor-exit(r0)     // Catch:{ all -> 0x0045 }
                return
            L_0x0045:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0045 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.C4958a.run():void");
        }
    }

    /* renamed from: i.f0.e.d$b */
    /* compiled from: DiskLruCache */
    class C4959b extends FaultHidingSink {
        static {
            Class<C4957d> cls = C4957d.class;
        }

        C4959b(Sink rVar) {
            super(rVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo27433a(IOException iOException) {
            C4957d.this.f11344b0 = true;
        }
    }

    /* renamed from: i.f0.e.d$e */
    /* compiled from: DiskLruCache */
    public final class C4963e implements Closeable {

        /* renamed from: P */
        private final String f11367P;

        /* renamed from: Q */
        private final long f11368Q;

        /* renamed from: R */
        private final C5049s[] f11369R;

        C4963e(String str, long j, C5049s[] sVarArr, long[] jArr) {
            this.f11367P = str;
            this.f11368Q = j;
            this.f11369R = sVarArr;
        }

        /* renamed from: a */
        public C4960c mo27441a() {
            return C4957d.this.mo27418a(this.f11367P, this.f11368Q);
        }

        public void close() {
            for (C5049s sVar : this.f11369R) {
                C4951c.m17352a(sVar);
            }
        }

        /* renamed from: a */
        public C5049s mo27442a(int i) {
            return this.f11369R[i];
        }
    }

    static {
        Class<C4957d> cls = C4957d.class;
    }

    C4957d(FileSystem aVar, File file, int i, int i2, long j, Executor executor) {
        this.f11332P = aVar;
        this.f11333Q = file;
        this.f11337U = i;
        this.f11334R = new File(file, "journal");
        this.f11335S = new File(file, "journal.tmp");
        this.f11336T = new File(file, "journal.bkp");
        this.f11339W = i2;
        this.f11338V = j;
        this.f11350h0 = executor;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    /* renamed from: a */
    public static C4957d m17387a(FileSystem aVar, File file, int i, int i2, long j) {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 > 0) {
            return new C4957d(aVar, file, i, i2, j, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), C4951c.m17350a("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    /* renamed from: e */
    private void m17388e(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                str2 = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.f11342Z.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i, indexOf2);
            }
            C4962d dVar = this.f11342Z.get(str2);
            if (dVar == null) {
                dVar = new C4962d(str2);
                this.f11342Z.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.f11363e = true;
                dVar.f11364f = null;
                dVar.mo27440a(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f11364f = new C4960c(dVar);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* renamed from: f */
    private void m17389f(String str) {
        if (!f11331j0.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    /* renamed from: t */
    private synchronized void m17390t() {
        if (mo27425c()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* renamed from: u */
    private BufferedSink m17391u() {
        return Okio.m18211a(new C4959b(this.f11332P.mo27625f(this.f11334R)));
    }

    /* renamed from: v */
    private void m17392v() {
        this.f11332P.mo27624e(this.f11335S);
        Iterator<C4962d> it = this.f11342Z.values().iterator();
        while (it.hasNext()) {
            C4962d next = it.next();
            int i = 0;
            if (next.f11364f == null) {
                while (i < this.f11339W) {
                    this.f11340X += next.f11360b[i];
                    i++;
                }
            } else {
                next.f11364f = null;
                while (i < this.f11339W) {
                    this.f11332P.mo27624e(next.f11361c[i]);
                    this.f11332P.mo27624e(next.f11362d[i]);
                    i++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.f11343a0 = r0 - r9.f11342Z.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (r1.mo28063j() == false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
        mo27429e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0070, code lost:
        r9.f11341Y = m17391u();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x007a=Splitter:B:23:0x007a, B:16:0x005d=Splitter:B:16:0x005d} */
    /* renamed from: w */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m17393w() {
        /*
            r9 = this;
            java.lang.String r0 = ", "
            i.f0.j.a r1 = r9.f11332P
            java.io.File r2 = r9.f11334R
            j.s r1 = r1.mo27619a(r2)
            j.e r1 = p244j.Okio.m18212a(r1)
            java.lang.String r2 = r1.mo28066m()     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = r1.mo28066m()     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = r1.mo28066m()     // Catch:{ all -> 0x00a8 }
            java.lang.String r5 = r1.mo28066m()     // Catch:{ all -> 0x00a8 }
            java.lang.String r6 = r1.mo28066m()     // Catch:{ all -> 0x00a8 }
            java.lang.String r7 = "libcore.io.DiskLruCache"
            boolean r7 = r7.equals(r2)     // Catch:{ all -> 0x00a8 }
            if (r7 == 0) goto L_0x007a
            java.lang.String r7 = "1"
            boolean r7 = r7.equals(r3)     // Catch:{ all -> 0x00a8 }
            if (r7 == 0) goto L_0x007a
            int r7 = r9.f11337U     // Catch:{ all -> 0x00a8 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x00a8 }
            boolean r4 = r7.equals(r4)     // Catch:{ all -> 0x00a8 }
            if (r4 == 0) goto L_0x007a
            int r4 = r9.f11339W     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ all -> 0x00a8 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x00a8 }
            if (r4 == 0) goto L_0x007a
            java.lang.String r4 = ""
            boolean r4 = r4.equals(r6)     // Catch:{ all -> 0x00a8 }
            if (r4 == 0) goto L_0x007a
            r0 = 0
        L_0x0053:
            java.lang.String r2 = r1.mo28066m()     // Catch:{ EOFException -> 0x005d }
            r9.m17388e(r2)     // Catch:{ EOFException -> 0x005d }
            int r0 = r0 + 1
            goto L_0x0053
        L_0x005d:
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r2 = r9.f11342Z     // Catch:{ all -> 0x00a8 }
            int r2 = r2.size()     // Catch:{ all -> 0x00a8 }
            int r0 = r0 - r2
            r9.f11343a0 = r0     // Catch:{ all -> 0x00a8 }
            boolean r0 = r1.mo28063j()     // Catch:{ all -> 0x00a8 }
            if (r0 != 0) goto L_0x0070
            r9.mo27429e()     // Catch:{ all -> 0x00a8 }
            goto L_0x0076
        L_0x0070:
            j.d r0 = r9.m17391u()     // Catch:{ all -> 0x00a8 }
            r9.f11341Y = r0     // Catch:{ all -> 0x00a8 }
        L_0x0076:
            p231i.p232f0.C4951c.m17352a(r1)
            return
        L_0x007a:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r7.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r8 = "unexpected journal header: ["
            r7.append(r8)     // Catch:{ all -> 0x00a8 }
            r7.append(r2)     // Catch:{ all -> 0x00a8 }
            r7.append(r0)     // Catch:{ all -> 0x00a8 }
            r7.append(r3)     // Catch:{ all -> 0x00a8 }
            r7.append(r0)     // Catch:{ all -> 0x00a8 }
            r7.append(r5)     // Catch:{ all -> 0x00a8 }
            r7.append(r0)     // Catch:{ all -> 0x00a8 }
            r7.append(r6)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "]"
            r7.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x00a8 }
            r4.<init>(r0)     // Catch:{ all -> 0x00a8 }
            throw r4     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r0 = move-exception
            p231i.p232f0.C4951c.m17352a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.m17393w():void");
    }

    /* renamed from: b */
    public synchronized void mo27423b() {
        if (!this.f11345c0) {
            if (this.f11332P.mo27623d(this.f11336T)) {
                if (this.f11332P.mo27623d(this.f11334R)) {
                    this.f11332P.mo27624e(this.f11336T);
                } else {
                    this.f11332P.mo27620a(this.f11336T, this.f11334R);
                }
            }
            if (this.f11332P.mo27623d(this.f11334R)) {
                try {
                    m17393w();
                    m17392v();
                    this.f11345c0 = true;
                    return;
                } catch (IOException e) {
                    Platform d = Platform.m17817d();
                    d.mo27629a(5, "DiskLruCache " + this.f11333Q + " is corrupt: " + e.getMessage() + ", removing", e);
                    mo27419a();
                    this.f11346d0 = false;
                } catch (Throwable th) {
                    this.f11346d0 = false;
                    throw th;
                }
            }
            mo27429e();
            this.f11345c0 = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        return null;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized p231i.p232f0.p233e.C4957d.C4963e mo27424c(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r3.mo27423b()     // Catch:{ all -> 0x0050 }
            r3.m17390t()     // Catch:{ all -> 0x0050 }
            r3.m17389f(r4)     // Catch:{ all -> 0x0050 }
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r0 = r3.f11342Z     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0050 }
            i.f0.e.d$d r0 = (p231i.p232f0.p233e.C4957d.C4962d) r0     // Catch:{ all -> 0x0050 }
            r1 = 0
            if (r0 == 0) goto L_0x004e
            boolean r2 = r0.f11363e     // Catch:{ all -> 0x0050 }
            if (r2 != 0) goto L_0x001a
            goto L_0x004e
        L_0x001a:
            i.f0.e.d$e r0 = r0.mo27438a()     // Catch:{ all -> 0x0050 }
            if (r0 != 0) goto L_0x0022
            monitor-exit(r3)
            return r1
        L_0x0022:
            int r1 = r3.f11343a0     // Catch:{ all -> 0x0050 }
            int r1 = r1 + 1
            r3.f11343a0 = r1     // Catch:{ all -> 0x0050 }
            j.d r1 = r3.f11341Y     // Catch:{ all -> 0x0050 }
            java.lang.String r2 = "READ"
            j.d r1 = r1.mo28033a(r2)     // Catch:{ all -> 0x0050 }
            r2 = 32
            j.d r1 = r1.writeByte(r2)     // Catch:{ all -> 0x0050 }
            j.d r4 = r1.mo28033a(r4)     // Catch:{ all -> 0x0050 }
            r1 = 10
            r4.writeByte(r1)     // Catch:{ all -> 0x0050 }
            boolean r4 = r3.mo27427d()     // Catch:{ all -> 0x0050 }
            if (r4 == 0) goto L_0x004c
            java.util.concurrent.Executor r4 = r3.f11350h0     // Catch:{ all -> 0x0050 }
            java.lang.Runnable r1 = r3.f11351i0     // Catch:{ all -> 0x0050 }
            r4.execute(r1)     // Catch:{ all -> 0x0050 }
        L_0x004c:
            monitor-exit(r3)
            return r0
        L_0x004e:
            monitor-exit(r3)
            return r1
        L_0x0050:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.mo27424c(java.lang.String):i.f0.e.d$e");
    }

    public synchronized void close() {
        if (this.f11345c0) {
            if (!this.f11346d0) {
                C4962d[] dVarArr = (C4962d[]) this.f11342Z.values().toArray(new C4962d[this.f11342Z.size()]);
                for (C4962d dVar : dVarArr) {
                    if (dVar.f11364f != null) {
                        dVar.f11364f.mo27435a();
                    }
                }
                mo27431g();
                this.f11341Y.close();
                this.f11341Y = null;
                this.f11346d0 = true;
                return;
            }
        }
        this.f11346d0 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public boolean mo27427d() {
        int i = this.f11343a0;
        return i >= 2000 && i >= this.f11342Z.size();
    }

    public synchronized void flush() {
        if (this.f11345c0) {
            m17390t();
            mo27431g();
            this.f11341Y.flush();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public void mo27431g() {
        while (this.f11340X > this.f11338V) {
            mo27421a(this.f11342Z.values().iterator().next());
        }
        this.f11347e0 = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        return r7;
     */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean mo27428d(java.lang.String r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r6.mo27423b()     // Catch:{ all -> 0x0029 }
            r6.m17390t()     // Catch:{ all -> 0x0029 }
            r6.m17389f(r7)     // Catch:{ all -> 0x0029 }
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r0 = r6.f11342Z     // Catch:{ all -> 0x0029 }
            java.lang.Object r7 = r0.get(r7)     // Catch:{ all -> 0x0029 }
            i.f0.e.d$d r7 = (p231i.p232f0.p233e.C4957d.C4962d) r7     // Catch:{ all -> 0x0029 }
            r0 = 0
            if (r7 != 0) goto L_0x0017
            monitor-exit(r6)
            return r0
        L_0x0017:
            boolean r7 = r6.mo27421a(r7)     // Catch:{ all -> 0x0029 }
            if (r7 == 0) goto L_0x0027
            long r1 = r6.f11340X     // Catch:{ all -> 0x0029 }
            long r3 = r6.f11338V     // Catch:{ all -> 0x0029 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0027
            r6.f11347e0 = r0     // Catch:{ all -> 0x0029 }
        L_0x0027:
            monitor-exit(r6)
            return r7
        L_0x0029:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.mo27428d(java.lang.String):boolean");
    }

    /* renamed from: i.f0.e.d$d */
    /* compiled from: DiskLruCache */
    private final class C4962d {

        /* renamed from: a */
        final String f11359a;

        /* renamed from: b */
        final long[] f11360b;

        /* renamed from: c */
        final File[] f11361c;

        /* renamed from: d */
        final File[] f11362d;

        /* renamed from: e */
        boolean f11363e;

        /* renamed from: f */
        C4960c f11364f;

        /* renamed from: g */
        long f11365g;

        C4962d(String str) {
            this.f11359a = str;
            int i = C4957d.this.f11339W;
            this.f11360b = new long[i];
            this.f11361c = new File[i];
            this.f11362d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append((char) JwtParser.SEPARATOR_CHAR);
            int length = sb.length();
            for (int i2 = 0; i2 < C4957d.this.f11339W; i2++) {
                sb.append(i2);
                this.f11361c[i2] = new File(C4957d.this.f11333Q, sb.toString());
                sb.append(".tmp");
                this.f11362d[i2] = new File(C4957d.this.f11333Q, sb.toString());
                sb.setLength(length);
            }
        }

        /* renamed from: b */
        private IOException m17412b(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27440a(String[] strArr) {
            if (strArr.length == C4957d.this.f11339W) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.f11360b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        m17412b(strArr);
                        throw null;
                    }
                }
                return;
            }
            m17412b(strArr);
            throw null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27439a(BufferedSink dVar) {
            for (long j : this.f11360b) {
                dVar.writeByte(32).mo28055f(j);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C4963e mo27438a() {
            if (Thread.holdsLock(C4957d.this)) {
                C5049s[] sVarArr = new C5049s[C4957d.this.f11339W];
                long[] jArr = (long[]) this.f11360b.clone();
                int i = 0;
                int i2 = 0;
                while (i2 < C4957d.this.f11339W) {
                    try {
                        sVarArr[i2] = C4957d.this.f11332P.mo27619a(this.f11361c[i2]);
                        i2++;
                    } catch (FileNotFoundException unused) {
                        while (i < C4957d.this.f11339W && sVarArr[i] != null) {
                            C4951c.m17352a(sVarArr[i]);
                            i++;
                        }
                        try {
                            C4957d.this.mo27421a(this);
                            return null;
                        } catch (IOException unused2) {
                            return null;
                        }
                    }
                }
                return new C4963e(this.f11359a, this.f11365g, sVarArr, jArr);
            }
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        return null;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized p231i.p232f0.p233e.C4957d.C4960c mo27418a(java.lang.String r6, long r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            r5.mo27423b()     // Catch:{ all -> 0x0074 }
            r5.m17390t()     // Catch:{ all -> 0x0074 }
            r5.m17389f(r6)     // Catch:{ all -> 0x0074 }
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r0 = r5.f11342Z     // Catch:{ all -> 0x0074 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x0074 }
            i.f0.e.d$d r0 = (p231i.p232f0.p233e.C4957d.C4962d) r0     // Catch:{ all -> 0x0074 }
            r1 = -1
            r3 = 0
            int r4 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x0023
            if (r0 == 0) goto L_0x0021
            long r1 = r0.f11365g     // Catch:{ all -> 0x0074 }
            int r4 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r4 == 0) goto L_0x0023
        L_0x0021:
            monitor-exit(r5)
            return r3
        L_0x0023:
            if (r0 == 0) goto L_0x002b
            i.f0.e.d$c r7 = r0.f11364f     // Catch:{ all -> 0x0074 }
            if (r7 == 0) goto L_0x002b
            monitor-exit(r5)
            return r3
        L_0x002b:
            boolean r7 = r5.f11347e0     // Catch:{ all -> 0x0074 }
            if (r7 != 0) goto L_0x006b
            boolean r7 = r5.f11348f0     // Catch:{ all -> 0x0074 }
            if (r7 == 0) goto L_0x0034
            goto L_0x006b
        L_0x0034:
            j.d r7 = r5.f11341Y     // Catch:{ all -> 0x0074 }
            java.lang.String r8 = "DIRTY"
            j.d r7 = r7.mo28033a(r8)     // Catch:{ all -> 0x0074 }
            r8 = 32
            j.d r7 = r7.writeByte(r8)     // Catch:{ all -> 0x0074 }
            j.d r7 = r7.mo28033a(r6)     // Catch:{ all -> 0x0074 }
            r8 = 10
            r7.writeByte(r8)     // Catch:{ all -> 0x0074 }
            j.d r7 = r5.f11341Y     // Catch:{ all -> 0x0074 }
            r7.flush()     // Catch:{ all -> 0x0074 }
            boolean r7 = r5.f11344b0     // Catch:{ all -> 0x0074 }
            if (r7 == 0) goto L_0x0056
            monitor-exit(r5)
            return r3
        L_0x0056:
            if (r0 != 0) goto L_0x0062
            i.f0.e.d$d r0 = new i.f0.e.d$d     // Catch:{ all -> 0x0074 }
            r0.<init>(r6)     // Catch:{ all -> 0x0074 }
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r7 = r5.f11342Z     // Catch:{ all -> 0x0074 }
            r7.put(r6, r0)     // Catch:{ all -> 0x0074 }
        L_0x0062:
            i.f0.e.d$c r6 = new i.f0.e.d$c     // Catch:{ all -> 0x0074 }
            r6.<init>(r0)     // Catch:{ all -> 0x0074 }
            r0.f11364f = r6     // Catch:{ all -> 0x0074 }
            monitor-exit(r5)
            return r6
        L_0x006b:
            java.util.concurrent.Executor r6 = r5.f11350h0     // Catch:{ all -> 0x0074 }
            java.lang.Runnable r7 = r5.f11351i0     // Catch:{ all -> 0x0074 }
            r6.execute(r7)     // Catch:{ all -> 0x0074 }
            monitor-exit(r5)
            return r3
        L_0x0074:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.mo27418a(java.lang.String, long):i.f0.e.d$c");
    }

    /* renamed from: i.f0.e.d$c */
    /* compiled from: DiskLruCache */
    public final class C4960c {

        /* renamed from: a */
        final C4962d f11354a;

        /* renamed from: b */
        final boolean[] f11355b;

        /* renamed from: c */
        private boolean f11356c;

        /* renamed from: i.f0.e.d$c$a */
        /* compiled from: DiskLruCache */
        class C4961a extends FaultHidingSink {
            C4961a(Sink rVar) {
                super(rVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo27433a(IOException iOException) {
                synchronized (C4957d.this) {
                    C4960c.this.mo27437c();
                }
            }
        }

        C4960c(C4962d dVar) {
            this.f11354a = dVar;
            this.f11355b = dVar.f11363e ? null : new boolean[C4957d.this.f11339W];
        }

        /* renamed from: a */
        public Sink mo27434a(int i) {
            synchronized (C4957d.this) {
                if (this.f11356c) {
                    throw new IllegalStateException();
                } else if (this.f11354a.f11364f != this) {
                    Sink a = Okio.m18213a();
                    return a;
                } else {
                    if (!this.f11354a.f11363e) {
                        this.f11355b[i] = true;
                    }
                    try {
                        C4961a aVar = new C4961a(C4957d.this.f11332P.mo27621b(this.f11354a.f11362d[i]));
                        return aVar;
                    } catch (FileNotFoundException unused) {
                        return Okio.m18213a();
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.e.d.a(i.f0.e.d$c, boolean):void
         arg types: [i.f0.e.d$c, int]
         candidates:
          i.f0.e.d.a(java.lang.String, long):i.f0.e.d$c
          i.f0.e.d.a(i.f0.e.d$c, boolean):void */
        /* renamed from: b */
        public void mo27436b() {
            synchronized (C4957d.this) {
                if (!this.f11356c) {
                    if (this.f11354a.f11364f == this) {
                        C4957d.this.mo27420a(this, true);
                    }
                    this.f11356c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public void mo27437c() {
            if (this.f11354a.f11364f == this) {
                int i = 0;
                while (true) {
                    C4957d dVar = C4957d.this;
                    if (i < dVar.f11339W) {
                        try {
                            dVar.f11332P.mo27624e(this.f11354a.f11362d[i]);
                        } catch (IOException unused) {
                        }
                        i++;
                    } else {
                        this.f11354a.f11364f = null;
                        return;
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.e.d.a(i.f0.e.d$c, boolean):void
         arg types: [i.f0.e.d$c, int]
         candidates:
          i.f0.e.d.a(java.lang.String, long):i.f0.e.d$c
          i.f0.e.d.a(i.f0.e.d$c, boolean):void */
        /* renamed from: a */
        public void mo27435a() {
            synchronized (C4957d.this) {
                if (!this.f11356c) {
                    if (this.f11354a.f11364f == this) {
                        C4957d.this.mo27420a(this, false);
                    }
                    this.f11356c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }
    }

    /* renamed from: c */
    public synchronized boolean mo27425c() {
        return this.f11346d0;
    }

    /* renamed from: b */
    public C4960c mo27422b(String str) {
        return mo27418a(str, -1);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public synchronized void mo27429e() {
        if (this.f11341Y != null) {
            this.f11341Y.close();
        }
        BufferedSink a = Okio.m18211a(this.f11332P.mo27621b(this.f11335S));
        try {
            a.mo28033a("libcore.io.DiskLruCache").writeByte(10);
            a.mo28033a(AppEventsConstants.EVENT_PARAM_VALUE_YES).writeByte(10);
            a.mo28055f((long) this.f11337U).writeByte(10);
            a.mo28055f((long) this.f11339W).writeByte(10);
            a.writeByte(10);
            for (C4962d dVar : this.f11342Z.values()) {
                if (dVar.f11364f != null) {
                    a.mo28033a("DIRTY").writeByte(32);
                    a.mo28033a(dVar.f11359a);
                    a.writeByte(10);
                } else {
                    a.mo28033a("CLEAN").writeByte(32);
                    a.mo28033a(dVar.f11359a);
                    dVar.mo27439a(a);
                    a.writeByte(10);
                }
            }
            a.close();
            if (this.f11332P.mo27623d(this.f11334R)) {
                this.f11332P.mo27620a(this.f11334R, this.f11336T);
            }
            this.f11332P.mo27620a(this.f11335S, this.f11334R);
            this.f11332P.mo27624e(this.f11336T);
            this.f11341Y = m17391u();
            this.f11344b0 = false;
            this.f11348f0 = false;
        } catch (Throwable th) {
            a.close();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f4, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo27420a(p231i.p232f0.p233e.C4957d.C4960c r10, boolean r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            i.f0.e.d$d r0 = r10.f11354a     // Catch:{ all -> 0x00fb }
            i.f0.e.d$c r1 = r0.f11364f     // Catch:{ all -> 0x00fb }
            if (r1 != r10) goto L_0x00f5
            r1 = 0
            if (r11 == 0) goto L_0x0047
            boolean r2 = r0.f11363e     // Catch:{ all -> 0x00fb }
            if (r2 != 0) goto L_0x0047
            r2 = 0
        L_0x000f:
            int r3 = r9.f11339W     // Catch:{ all -> 0x00fb }
            if (r2 >= r3) goto L_0x0047
            boolean[] r3 = r10.f11355b     // Catch:{ all -> 0x00fb }
            boolean r3 = r3[r2]     // Catch:{ all -> 0x00fb }
            if (r3 == 0) goto L_0x002d
            i.f0.j.a r3 = r9.f11332P     // Catch:{ all -> 0x00fb }
            java.io.File[] r4 = r0.f11362d     // Catch:{ all -> 0x00fb }
            r4 = r4[r2]     // Catch:{ all -> 0x00fb }
            boolean r3 = r3.mo27623d(r4)     // Catch:{ all -> 0x00fb }
            if (r3 != 0) goto L_0x002a
            r10.mo27435a()     // Catch:{ all -> 0x00fb }
            monitor-exit(r9)
            return
        L_0x002a:
            int r2 = r2 + 1
            goto L_0x000f
        L_0x002d:
            r10.mo27435a()     // Catch:{ all -> 0x00fb }
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00fb }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fb }
            r11.<init>()     // Catch:{ all -> 0x00fb }
            java.lang.String r0 = "Newly created entry didn't create value for index "
            r11.append(r0)     // Catch:{ all -> 0x00fb }
            r11.append(r2)     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00fb }
            r10.<init>(r11)     // Catch:{ all -> 0x00fb }
            throw r10     // Catch:{ all -> 0x00fb }
        L_0x0047:
            int r10 = r9.f11339W     // Catch:{ all -> 0x00fb }
            if (r1 >= r10) goto L_0x007f
            java.io.File[] r10 = r0.f11362d     // Catch:{ all -> 0x00fb }
            r10 = r10[r1]     // Catch:{ all -> 0x00fb }
            if (r11 == 0) goto L_0x0077
            i.f0.j.a r2 = r9.f11332P     // Catch:{ all -> 0x00fb }
            boolean r2 = r2.mo27623d(r10)     // Catch:{ all -> 0x00fb }
            if (r2 == 0) goto L_0x007c
            java.io.File[] r2 = r0.f11361c     // Catch:{ all -> 0x00fb }
            r2 = r2[r1]     // Catch:{ all -> 0x00fb }
            i.f0.j.a r3 = r9.f11332P     // Catch:{ all -> 0x00fb }
            r3.mo27620a(r10, r2)     // Catch:{ all -> 0x00fb }
            long[] r10 = r0.f11360b     // Catch:{ all -> 0x00fb }
            r3 = r10[r1]     // Catch:{ all -> 0x00fb }
            i.f0.j.a r10 = r9.f11332P     // Catch:{ all -> 0x00fb }
            long r5 = r10.mo27626g(r2)     // Catch:{ all -> 0x00fb }
            long[] r10 = r0.f11360b     // Catch:{ all -> 0x00fb }
            r10[r1] = r5     // Catch:{ all -> 0x00fb }
            long r7 = r9.f11340X     // Catch:{ all -> 0x00fb }
            long r7 = r7 - r3
            long r7 = r7 + r5
            r9.f11340X = r7     // Catch:{ all -> 0x00fb }
            goto L_0x007c
        L_0x0077:
            i.f0.j.a r2 = r9.f11332P     // Catch:{ all -> 0x00fb }
            r2.mo27624e(r10)     // Catch:{ all -> 0x00fb }
        L_0x007c:
            int r1 = r1 + 1
            goto L_0x0047
        L_0x007f:
            int r10 = r9.f11343a0     // Catch:{ all -> 0x00fb }
            r1 = 1
            int r10 = r10 + r1
            r9.f11343a0 = r10     // Catch:{ all -> 0x00fb }
            r10 = 0
            r0.f11364f = r10     // Catch:{ all -> 0x00fb }
            boolean r10 = r0.f11363e     // Catch:{ all -> 0x00fb }
            r10 = r10 | r11
            r2 = 10
            r3 = 32
            if (r10 == 0) goto L_0x00bb
            r0.f11363e = r1     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            java.lang.String r1 = "CLEAN"
            j.d r10 = r10.mo28033a(r1)     // Catch:{ all -> 0x00fb }
            r10.writeByte(r3)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            java.lang.String r1 = r0.f11359a     // Catch:{ all -> 0x00fb }
            r10.mo28033a(r1)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            r0.mo27439a(r10)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            r10.writeByte(r2)     // Catch:{ all -> 0x00fb }
            if (r11 == 0) goto L_0x00d9
            long r10 = r9.f11349g0     // Catch:{ all -> 0x00fb }
            r1 = 1
            long r1 = r1 + r10
            r9.f11349g0 = r1     // Catch:{ all -> 0x00fb }
            r0.f11365g = r10     // Catch:{ all -> 0x00fb }
            goto L_0x00d9
        L_0x00bb:
            java.util.LinkedHashMap<java.lang.String, i.f0.e.d$d> r10 = r9.f11342Z     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = r0.f11359a     // Catch:{ all -> 0x00fb }
            r10.remove(r11)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = "REMOVE"
            j.d r10 = r10.mo28033a(r11)     // Catch:{ all -> 0x00fb }
            r10.writeByte(r3)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = r0.f11359a     // Catch:{ all -> 0x00fb }
            r10.mo28033a(r11)     // Catch:{ all -> 0x00fb }
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            r10.writeByte(r2)     // Catch:{ all -> 0x00fb }
        L_0x00d9:
            j.d r10 = r9.f11341Y     // Catch:{ all -> 0x00fb }
            r10.flush()     // Catch:{ all -> 0x00fb }
            long r10 = r9.f11340X     // Catch:{ all -> 0x00fb }
            long r0 = r9.f11338V     // Catch:{ all -> 0x00fb }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x00ec
            boolean r10 = r9.mo27427d()     // Catch:{ all -> 0x00fb }
            if (r10 == 0) goto L_0x00f3
        L_0x00ec:
            java.util.concurrent.Executor r10 = r9.f11350h0     // Catch:{ all -> 0x00fb }
            java.lang.Runnable r11 = r9.f11351i0     // Catch:{ all -> 0x00fb }
            r10.execute(r11)     // Catch:{ all -> 0x00fb }
        L_0x00f3:
            monitor-exit(r9)
            return
        L_0x00f5:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00fb }
            r10.<init>()     // Catch:{ all -> 0x00fb }
            throw r10     // Catch:{ all -> 0x00fb }
        L_0x00fb:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p233e.C4957d.mo27420a(i.f0.e.d$c, boolean):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo27421a(C4962d dVar) {
        C4960c cVar = dVar.f11364f;
        if (cVar != null) {
            cVar.mo27437c();
        }
        for (int i = 0; i < this.f11339W; i++) {
            this.f11332P.mo27624e(dVar.f11361c[i]);
            long j = this.f11340X;
            long[] jArr = dVar.f11360b;
            this.f11340X = j - jArr[i];
            jArr[i] = 0;
        }
        this.f11343a0++;
        this.f11341Y.mo28033a("REMOVE").writeByte(32).mo28033a(dVar.f11359a).writeByte(10);
        this.f11342Z.remove(dVar.f11359a);
        if (mo27427d()) {
            this.f11350h0.execute(this.f11351i0);
        }
        return true;
    }

    /* renamed from: a */
    public void mo27419a() {
        close();
        this.f11332P.mo27622c(this.f11333Q);
    }
}
