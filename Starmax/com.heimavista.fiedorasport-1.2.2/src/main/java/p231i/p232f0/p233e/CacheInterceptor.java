package p231i.p232f0.p233e;

import com.facebook.appevents.AppEventsConstants;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import p231i.C5015r;
import p231i.C5023y;
import p231i.Interceptor;
import p231i.Protocol;
import p231i.Response;
import p231i.ResponseBody;
import p231i.p232f0.C4951c;
import p231i.p232f0.Internal;
import p231i.p232f0.p233e.CacheStrategy;
import p231i.p232f0.p235g.HttpHeaders;
import p231i.p232f0.p235g.HttpMethod;
import p231i.p232f0.p235g.RealResponseBody;
import p244j.Buffer;
import p244j.BufferedSink;
import p244j.BufferedSource;
import p244j.C5049s;
import p244j.Okio;
import p244j.Sink;
import p244j.Timeout;

/* renamed from: i.f0.e.a */
public final class CacheInterceptor implements Interceptor {

    /* renamed from: a */
    final InternalCache f11312a;

    /* renamed from: i.f0.e.a$a */
    /* compiled from: CacheInterceptor */
    class C4955a implements C5049s {

        /* renamed from: P */
        boolean f11313P;

        /* renamed from: Q */
        final /* synthetic */ BufferedSource f11314Q;

        /* renamed from: R */
        final /* synthetic */ CacheRequest f11315R;

        /* renamed from: S */
        final /* synthetic */ BufferedSink f11316S;

        C4955a(CacheInterceptor aVar, BufferedSource eVar, CacheRequest bVar, BufferedSink dVar) {
            this.f11314Q = eVar;
            this.f11315R = bVar;
            this.f11316S = dVar;
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            try {
                long b = this.f11314Q.mo27415b(cVar, j);
                if (b == -1) {
                    if (!this.f11313P) {
                        this.f11313P = true;
                        this.f11316S.close();
                    }
                    return -1;
                }
                cVar.mo28031a(this.f11316S.mo28054f(), cVar.mo28051e() - b, b);
                this.f11316S.mo28064k();
                return b;
            } catch (IOException e) {
                if (!this.f11313P) {
                    this.f11313P = true;
                    this.f11315R.mo27364a();
                }
                throw e;
            }
        }

        public void close() {
            if (!this.f11313P && !C4951c.m17354a(this, 100, TimeUnit.MILLISECONDS)) {
                this.f11313P = true;
                this.f11315R.mo27364a();
            }
            this.f11314Q.close();
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return this.f11314Q.mo27416h();
        }
    }

    public CacheInterceptor(InternalCache fVar) {
        this.f11312a = fVar;
    }

    /* renamed from: b */
    static boolean m17374b(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    /* renamed from: a */
    public Response mo27414a(Interceptor.C5019a aVar) {
        InternalCache fVar = this.f11312a;
        Response b = fVar != null ? fVar.mo27363b(aVar.mo27495d()) : null;
        CacheStrategy a = new CacheStrategy.C4956a(System.currentTimeMillis(), aVar.mo27495d(), b).mo27417a();
        C5023y yVar = a.f11317a;
        Response a0Var = a.f11318b;
        InternalCache fVar2 = this.f11312a;
        if (fVar2 != null) {
            fVar2.mo27361a(a);
        }
        if (b != null && a0Var == null) {
            C4951c.m17352a(b.mo27315a());
        }
        if (yVar == null && a0Var == null) {
            Response.C4938a aVar2 = new Response.C4938a();
            aVar2.mo27339a(aVar.mo27495d());
            aVar2.mo27338a(Protocol.HTTP_1_1);
            aVar2.mo27332a(504);
            aVar2.mo27340a("Unsatisfiable Request (only-if-cached)");
            aVar2.mo27335a(C4951c.f11305c);
            aVar2.mo27343b(-1);
            aVar2.mo27333a(System.currentTimeMillis());
            return aVar2.mo27342a();
        } else if (yVar == null) {
            Response.C4938a w = a0Var.mo27328w();
            w.mo27334a(m17370a(a0Var));
            return w.mo27342a();
        } else {
            try {
                Response a2 = aVar.mo27491a(yVar);
                if (a2 == null && b != null) {
                }
                if (a0Var != null) {
                    if (a2.mo27321d() == 304) {
                        Response.C4938a w2 = a0Var.mo27328w();
                        w2.mo27337a(m17372a(a0Var.mo27323g(), a2.mo27323g()));
                        w2.mo27343b(a2.mo27314B());
                        w2.mo27333a(a2.mo27331z());
                        w2.mo27334a(m17370a(a0Var));
                        w2.mo27344b(m17370a(a2));
                        Response a3 = w2.mo27342a();
                        a2.mo27315a().close();
                        this.f11312a.mo27359a();
                        this.f11312a.mo27360a(a0Var, a3);
                        return a3;
                    }
                    C4951c.m17352a(a0Var.mo27315a());
                }
                Response.C4938a w3 = a2.mo27328w();
                w3.mo27334a(m17370a(a0Var));
                w3.mo27344b(m17370a(a2));
                Response a4 = w3.mo27342a();
                if (this.f11312a != null) {
                    if (HttpHeaders.m17503b(a4) && CacheStrategy.m17380a(a4, yVar)) {
                        return m17371a(this.f11312a.mo27358a(a4), a4);
                    }
                    if (HttpMethod.m17509a(yVar.mo27825e())) {
                        try {
                            this.f11312a.mo27362a(yVar);
                        } catch (IOException unused) {
                        }
                    }
                }
                return a4;
            } finally {
                if (b != null) {
                    C4951c.m17352a(b.mo27315a());
                }
            }
        }
    }

    /* renamed from: a */
    private static Response m17370a(Response a0Var) {
        if (a0Var == null || a0Var.mo27315a() == null) {
            return a0Var;
        }
        Response.C4938a w = a0Var.mo27328w();
        w.mo27335a((ResponseBody) null);
        return w.mo27342a();
    }

    /* renamed from: a */
    private Response m17371a(CacheRequest bVar, Response a0Var) {
        Sink b;
        if (bVar == null || (b = bVar.mo27365b()) == null) {
            return a0Var;
        }
        C4955a aVar = new C4955a(this, a0Var.mo27315a().mo27348b(), bVar, Okio.m18211a(b));
        String b2 = a0Var.mo27318b("Content-Type");
        long a = a0Var.mo27315a().mo27347a();
        Response.C4938a w = a0Var.mo27328w();
        w.mo27335a(new RealResponseBody(b2, a, Okio.m18212a(aVar)));
        return w.mo27342a();
    }

    /* renamed from: a */
    private static C5015r m17372a(C5015r rVar, C5015r rVar2) {
        C5015r.C5016a aVar = new C5015r.C5016a();
        int b = rVar.mo27739b();
        for (int i = 0; i < b; i++) {
            String a = rVar.mo27737a(i);
            String b2 = rVar.mo27740b(i);
            if ((!"Warning".equalsIgnoreCase(a) || !b2.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_YES)) && (m17373a(a) || !m17374b(a) || rVar2.mo27738a(a) == null)) {
                Internal.f11301a.mo27405a(aVar, a, b2);
            }
        }
        int b3 = rVar2.mo27739b();
        for (int i2 = 0; i2 < b3; i2++) {
            String a2 = rVar2.mo27737a(i2);
            if (!m17373a(a2) && m17374b(a2)) {
                Internal.f11301a.mo27405a(aVar, a2, rVar2.mo27740b(i2));
            }
        }
        return aVar.mo27747a();
    }

    /* renamed from: a */
    static boolean m17373a(String str) {
        return "Content-Length".equalsIgnoreCase(str) || "Content-Encoding".equalsIgnoreCase(str) || "Content-Type".equalsIgnoreCase(str);
    }
}
