package p231i.p232f0.p239k;

import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import org.conscrypt.Conscrypt;
import org.conscrypt.OpenSSLProvider;
import p231i.Protocol;

/* renamed from: i.f0.k.b */
public class ConscryptPlatform extends Platform {
    private ConscryptPlatform() {
    }

    /* renamed from: c */
    public static Platform m17795c() {
        try {
            Class.forName("org.conscrypt.ConscryptEngineSocket");
            if (!Conscrypt.isAvailable()) {
                return null;
            }
            Conscrypt.setUseEngineSocketByDefault(true);
            return new ConscryptPlatform();
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    /* renamed from: f */
    private Provider m17796f() {
        return new OpenSSLProvider();
    }

    /* renamed from: a */
    public void mo27632a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        if (Conscrypt.isConscrypt(sSLSocket)) {
            if (str != null) {
                Conscrypt.setUseSessionTickets(sSLSocket, true);
                Conscrypt.setHostname(sSLSocket, str);
            }
            Conscrypt.setApplicationProtocols(sSLSocket, (String[]) Platform.m17814a(list).toArray(new String[0]));
            return;
        }
        super.mo27632a(sSLSocket, str, list);
    }

    /* renamed from: b */
    public String mo27634b(SSLSocket sSLSocket) {
        if (Conscrypt.isConscrypt(sSLSocket)) {
            return Conscrypt.getApplicationProtocol(sSLSocket);
        }
        return super.mo27634b(sSLSocket);
    }

    /* renamed from: b */
    public SSLContext mo27644b() {
        try {
            return SSLContext.getInstance("TLS", m17796f());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }
}
