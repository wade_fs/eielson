package p231i.p232f0.p239k;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import p231i.Protocol;
import p231i.p232f0.C4951c;

/* renamed from: i.f0.k.c */
final class Jdk9Platform extends Platform {

    /* renamed from: c */
    final Method f11657c;

    /* renamed from: d */
    final Method f11658d;

    Jdk9Platform(Method method, Method method2) {
        this.f11657c = method;
        this.f11658d = method2;
    }

    /* renamed from: c */
    public static Jdk9Platform m17800c() {
        try {
            return new Jdk9Platform(SSLParameters.class.getMethod("setApplicationProtocols", String[].class), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.lang.ReflectiveOperationException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: a */
    public void mo27632a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> a = Platform.m17814a(list);
            this.f11657c.invoke(sSLParameters, a.toArray(new String[a.size()]));
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to set ssl parameters", (Exception) e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.lang.ReflectiveOperationException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: b */
    public String mo27634b(SSLSocket sSLSocket) {
        try {
            String str = (String) this.f11658d.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to get selected protocols", (Exception) e);
        }
    }
}
