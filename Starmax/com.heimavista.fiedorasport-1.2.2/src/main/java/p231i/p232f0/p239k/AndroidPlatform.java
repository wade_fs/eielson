package p231i.p232f0.p239k;

import android.os.Build;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import p231i.Protocol;
import p231i.p232f0.C4951c;
import p231i.p232f0.p240l.CertificateChainCleaner;
import p231i.p232f0.p240l.TrustRootIndex;

/* renamed from: i.f0.k.a */
class AndroidPlatform extends Platform {

    /* renamed from: c */
    private final OptionalMethod<Socket> f11645c;

    /* renamed from: d */
    private final OptionalMethod<Socket> f11646d;

    /* renamed from: e */
    private final OptionalMethod<Socket> f11647e;

    /* renamed from: f */
    private final OptionalMethod<Socket> f11648f;

    /* renamed from: g */
    private final C5002c f11649g = C5002c.m17792a();

    /* renamed from: i.f0.k.a$a */
    /* compiled from: AndroidPlatform */
    static final class C5000a extends CertificateChainCleaner {

        /* renamed from: a */
        private final Object f11650a;

        /* renamed from: b */
        private final Method f11651b;

        C5000a(Object obj, Method method) {
            this.f11650a = obj;
            this.f11651b = method;
        }

        /* renamed from: a */
        public List<Certificate> mo27636a(List<Certificate> list, String str) {
            try {
                return (List) this.f11651b.invoke(this.f11650a, (X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str);
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public boolean equals(Object obj) {
            return obj instanceof C5000a;
        }

        public int hashCode() {
            return 0;
        }
    }

    /* renamed from: i.f0.k.a$b */
    /* compiled from: AndroidPlatform */
    static final class C5001b implements TrustRootIndex {

        /* renamed from: a */
        private final X509TrustManager f11652a;

        /* renamed from: b */
        private final Method f11653b;

        C5001b(X509TrustManager x509TrustManager, Method method) {
            this.f11653b = method;
            this.f11652a = x509TrustManager;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
         arg types: [java.lang.String, java.lang.IllegalAccessException]
         candidates:
          i.f0.c.a(i.s, boolean):java.lang.String
          i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
          i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
          i.f0.c.a(java.lang.Object, java.lang.Object):boolean
          i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
          i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
        /* renamed from: a */
        public X509Certificate mo27639a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.f11653b.invoke(this.f11652a, x509Certificate);
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw C4951c.m17342a("unable to get issues and signature", (Exception) e);
            } catch (InvocationTargetException unused) {
                return null;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C5001b)) {
                return false;
            }
            C5001b bVar = (C5001b) obj;
            if (!this.f11652a.equals(bVar.f11652a) || !this.f11653b.equals(bVar.f11653b)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.f11652a.hashCode() + (this.f11653b.hashCode() * 31);
        }
    }

    AndroidPlatform(Class<?> cls, OptionalMethod<Socket> eVar, OptionalMethod<Socket> eVar2, OptionalMethod<Socket> eVar3, OptionalMethod<Socket> eVar4) {
        this.f11645c = eVar;
        this.f11646d = eVar2;
        this.f11647e = eVar3;
        this.f11648f = eVar4;
    }

    /* renamed from: c */
    public static Platform m17779c() {
        Class<?> cls;
        OptionalMethod eVar;
        OptionalMethod eVar2;
        Class<byte[]> cls2 = byte[].class;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException unused) {
            try {
                cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
            } catch (ClassNotFoundException unused2) {
                return null;
            }
        }
        Class<?> cls3 = cls;
        OptionalMethod eVar3 = new OptionalMethod(null, "setUseSessionTickets", Boolean.TYPE);
        OptionalMethod eVar4 = new OptionalMethod(null, "setHostname", String.class);
        if (m17780f()) {
            OptionalMethod eVar5 = new OptionalMethod(cls2, "getAlpnSelectedProtocol", new Class[0]);
            eVar = new OptionalMethod(null, "setAlpnProtocols", cls2);
            eVar2 = eVar5;
        } else {
            eVar2 = null;
            eVar = null;
        }
        return new AndroidPlatform(cls3, eVar3, eVar4, eVar2, eVar);
    }

    /* renamed from: f */
    private static boolean m17780f() {
        if (Security.getProvider("GMSCore_OpenSSL") != null) {
            return true;
        }
        try {
            Class.forName("android.net.Network");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* renamed from: a */
    public void mo27631a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e) {
            if (C4951c.m17355a(e)) {
                throw new IOException(e);
            }
            throw e;
        } catch (SecurityException e2) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e2);
            throw iOException;
        } catch (ClassCastException e3) {
            if (Build.VERSION.SDK_INT == 26) {
                IOException iOException2 = new IOException("Exception in connect");
                iOException2.initCause(e3);
                throw iOException2;
            }
            throw e3;
        }
    }

    /* renamed from: b */
    public String mo27634b(SSLSocket sSLSocket) {
        byte[] bArr;
        OptionalMethod<Socket> eVar = this.f11647e;
        if (eVar == null || !eVar.mo27648a(sSLSocket) || (bArr = (byte[]) this.f11647e.mo27651d(sSLSocket, new Object[0])) == null) {
            return null;
        }
        return new String(bArr, C4951c.f11306d);
    }

    /* renamed from: i.f0.k.a$c */
    /* compiled from: AndroidPlatform */
    static final class C5002c {

        /* renamed from: a */
        private final Method f11654a;

        /* renamed from: b */
        private final Method f11655b;

        /* renamed from: c */
        private final Method f11656c;

        C5002c(Method method, Method method2, Method method3) {
            this.f11654a = method;
            this.f11655b = method2;
            this.f11656c = method3;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public Object mo27642a(String str) {
            Method method = this.f11654a;
            if (method != null) {
                try {
                    Object invoke = method.invoke(null, new Object[0]);
                    this.f11655b.invoke(invoke, str);
                    return invoke;
                } catch (Exception unused) {
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo27643a(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.f11656c.invoke(obj, new Object[0]);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        /* renamed from: a */
        static C5002c m17792a() {
            Method method;
            Method method2;
            Method method3 = null;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                Method method4 = cls.getMethod("get", new Class[0]);
                method = cls.getMethod("open", String.class);
                method2 = cls.getMethod("warnIfOpen", new Class[0]);
                method3 = method4;
            } catch (Exception unused) {
                method2 = null;
                method = null;
            }
            return new C5002c(method3, method, method2);
        }
    }

    /* renamed from: b */
    public boolean mo27635b(String str) {
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            return m17778b(str, cls, cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]));
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return super.mo27635b(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to determine cleartext support", e);
        }
    }

    /* renamed from: b */
    private boolean m17778b(String str, Class<?> cls, Object obj) {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", String.class).invoke(obj, str)).booleanValue();
        } catch (NoSuchMethodException unused) {
            return m17777a(str, cls, obj);
        }
    }

    /* renamed from: a */
    public void mo27632a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        if (str != null) {
            this.f11645c.mo27650c(sSLSocket, true);
            this.f11646d.mo27650c(sSLSocket, str);
        }
        OptionalMethod<Socket> eVar = this.f11648f;
        if (eVar != null && eVar.mo27648a(sSLSocket)) {
            this.f11648f.mo27651d(sSLSocket, Platform.m17815b(list));
        }
    }

    /* renamed from: b */
    public TrustRootIndex mo27633b(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
            declaredMethod.setAccessible(true);
            return new C5001b(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException unused) {
            return super.mo27633b(x509TrustManager);
        }
    }

    /* renamed from: a */
    public void mo27629a(int i, String str, Throwable th) {
        int min;
        int i2 = 5;
        if (i != 5) {
            i2 = 3;
        }
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    /* renamed from: a */
    public Object mo27628a(String str) {
        return this.f11649g.mo27642a(str);
    }

    /* renamed from: a */
    public void mo27630a(String str, Object obj) {
        if (!this.f11649g.mo27643a(obj)) {
            mo27629a(5, str, (Throwable) null);
        }
    }

    /* renamed from: a */
    private boolean m17777a(String str, Class<?> cls, Object obj) {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (NoSuchMethodException unused) {
            return super.mo27635b(str);
        }
    }

    /* renamed from: a */
    public CertificateChainCleaner mo27627a(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new C5000a(cls.getConstructor(X509TrustManager.class).newInstance(x509TrustManager), cls.getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class));
        } catch (Exception unused) {
            return super.mo27627a(x509TrustManager);
        }
    }
}
