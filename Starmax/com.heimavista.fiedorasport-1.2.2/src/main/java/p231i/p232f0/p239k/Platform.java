package p231i.p232f0.p239k;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import p231i.OkHttpClient;
import p231i.Protocol;
import p231i.p232f0.p240l.BasicCertificateChainCleaner;
import p231i.p232f0.p240l.BasicTrustRootIndex;
import p231i.p232f0.p240l.CertificateChainCleaner;
import p231i.p232f0.p240l.TrustRootIndex;
import p244j.Buffer;

/* renamed from: i.f0.k.f */
public class Platform {

    /* renamed from: a */
    private static final Platform f11670a = m17816c();

    /* renamed from: b */
    private static final Logger f11671b = Logger.getLogger(OkHttpClient.class.getName());

    /* renamed from: b */
    static byte[] m17815b(List<Protocol> list) {
        Buffer cVar = new Buffer();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol wVar = list.get(i);
            if (wVar != Protocol.HTTP_1_0) {
                cVar.writeByte(wVar.toString().length());
                cVar.mo28033a(wVar.toString());
            }
        }
        return cVar.mo28061i();
    }

    /* renamed from: c */
    private static Platform m17816c() {
        Platform c;
        Platform c2 = AndroidPlatform.m17779c();
        if (c2 != null) {
            return c2;
        }
        if (m17818e() && (c = ConscryptPlatform.m17795c()) != null) {
            return c;
        }
        Jdk9Platform c3 = Jdk9Platform.m17800c();
        if (c3 != null) {
            return c3;
        }
        Platform c4 = JdkWithJettyBootPlatform.m17803c();
        if (c4 != null) {
            return c4;
        }
        return new Platform();
    }

    /* renamed from: d */
    public static Platform m17817d() {
        return f11670a;
    }

    /* renamed from: e */
    public static boolean m17818e() {
        if ("conscrypt".equals(System.getProperty("okhttp.platform"))) {
            return true;
        }
        return "Conscrypt".equals(Security.getProviders()[0].getName());
    }

    /* renamed from: a */
    public String mo27652a() {
        return "OkHttp";
    }

    /* renamed from: a */
    public void mo27631a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        socket.connect(inetSocketAddress, i);
    }

    /* renamed from: a */
    public void mo27645a(SSLSocket sSLSocket) {
    }

    /* renamed from: a */
    public void mo27632a(SSLSocket sSLSocket, String str, List<Protocol> list) {
    }

    /* renamed from: b */
    public String mo27634b(SSLSocket sSLSocket) {
        return null;
    }

    /* renamed from: b */
    public boolean mo27635b(String str) {
        return true;
    }

    /* renamed from: a */
    public void mo27629a(int i, String str, Throwable th) {
        f11671b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    /* renamed from: a */
    public Object mo27628a(String str) {
        if (f11671b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    /* renamed from: a */
    public void mo27630a(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        mo27629a(5, str, (Throwable) obj);
    }

    /* renamed from: a */
    public static List<String> m17814a(List<Protocol> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol wVar = list.get(i);
            if (wVar != Protocol.HTTP_1_0) {
                arrayList.add(wVar.toString());
            }
        }
        return arrayList;
    }

    /* renamed from: b */
    public SSLContext mo27644b() {
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }

    /* renamed from: b */
    public TrustRootIndex mo27633b(X509TrustManager x509TrustManager) {
        return new BasicTrustRootIndex(x509TrustManager.getAcceptedIssuers());
    }

    /* renamed from: a */
    public CertificateChainCleaner mo27627a(X509TrustManager x509TrustManager) {
        return new BasicCertificateChainCleaner(mo27633b(x509TrustManager));
    }
}
