package p231i.p232f0.p239k;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

/* renamed from: i.f0.k.e */
class OptionalMethod<T> {

    /* renamed from: a */
    private final Class<?> f11667a;

    /* renamed from: b */
    private final String f11668b;

    /* renamed from: c */
    private final Class[] f11669c;

    OptionalMethod(Class<?> cls, String str, Class... clsArr) {
        this.f11667a = cls;
        this.f11668b = str;
        this.f11669c = clsArr;
    }

    /* renamed from: a */
    public boolean mo27648a(Socket socket) {
        return m17807a(socket.getClass()) != null;
    }

    /* renamed from: b */
    public Object mo27649b(T t, Object... objArr) {
        Method a = m17807a(t.getClass());
        if (a == null) {
            return null;
        }
        try {
            return a.invoke(t, objArr);
        } catch (IllegalAccessException unused) {
            return null;
        }
    }

    /* renamed from: c */
    public Object mo27650c(T t, Object... objArr) {
        try {
            return mo27649b(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: d */
    public Object mo27651d(T t, Object... objArr) {
        try {
            return mo27647a(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: a */
    public Object mo27647a(T t, Object... objArr) {
        Method a = m17807a(t.getClass());
        if (a != null) {
            try {
                return a.invoke(t, objArr);
            } catch (IllegalAccessException e) {
                AssertionError assertionError = new AssertionError("Unexpectedly could not call: " + a);
                assertionError.initCause(e);
                throw assertionError;
            }
        } else {
            throw new AssertionError("Method " + this.f11668b + " not supported for object " + ((Object) t));
        }
    }

    /* renamed from: a */
    private Method m17807a(Class<?> cls) {
        Class<?> cls2;
        String str = this.f11668b;
        if (str == null) {
            return null;
        }
        Method a = m17808a(cls, str, this.f11669c);
        if (a == null || (cls2 = this.f11667a) == null || cls2.isAssignableFrom(a.getReturnType())) {
            return a;
        }
        return null;
    }

    /* renamed from: a */
    private static Method m17808a(Class<?> cls, String str, Class[] clsArr) {
        try {
            Method method = cls.getMethod(str, clsArr);
            try {
                if ((method.getModifiers() & 1) != 0) {
                    return method;
                }
            } catch (NoSuchMethodException unused) {
                return method;
            }
        } catch (NoSuchMethodException unused2) {
        }
        return null;
    }
}
