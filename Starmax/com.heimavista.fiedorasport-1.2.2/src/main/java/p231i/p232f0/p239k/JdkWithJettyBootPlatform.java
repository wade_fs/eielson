package p231i.p232f0.p239k;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;
import p231i.Protocol;
import p231i.p232f0.C4951c;

/* renamed from: i.f0.k.d */
class JdkWithJettyBootPlatform extends Platform {

    /* renamed from: c */
    private final Method f11659c;

    /* renamed from: d */
    private final Method f11660d;

    /* renamed from: e */
    private final Method f11661e;

    /* renamed from: f */
    private final Class<?> f11662f;

    /* renamed from: g */
    private final Class<?> f11663g;

    /* renamed from: i.f0.k.d$a */
    /* compiled from: JdkWithJettyBootPlatform */
    private static class C5003a implements InvocationHandler {

        /* renamed from: a */
        private final List<String> f11664a;

        /* renamed from: b */
        boolean f11665b;

        /* renamed from: c */
        String f11666c;

        C5003a(List<String> list) {
            this.f11664a = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = C4951c.f11304b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f11665b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f11664a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f11664a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f11666c = str;
                            return str;
                        }
                    }
                    String str2 = this.f11664a.get(0);
                    this.f11666c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f11666c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    JdkWithJettyBootPlatform(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f11659c = method;
        this.f11660d = method2;
        this.f11661e = method3;
        this.f11662f = cls;
        this.f11663g = cls2;
    }

    /* renamed from: c */
    public static Platform m17803c() {
        try {
            Class<?> cls = Class.forName("org.eclipse.jetty.alpn.ALPN");
            Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
            Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider");
            Class<?> cls4 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider");
            Method method = cls.getMethod("put", SSLSocket.class, cls2);
            return new JdkWithJettyBootPlatform(method, cls.getMethod("get", SSLSocket.class), cls.getMethod("remove", SSLSocket.class), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.lang.ReflectiveOperationException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: a */
    public void mo27632a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        List<String> a = Platform.m17814a(list);
        try {
            Object newProxyInstance = Proxy.newProxyInstance(Platform.class.getClassLoader(), new Class[]{this.f11662f, this.f11663g}, new C5003a(a));
            this.f11659c.invoke(null, sSLSocket, newProxyInstance);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to set alpn", (Exception) e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.lang.ReflectiveOperationException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: b */
    public String mo27634b(SSLSocket sSLSocket) {
        try {
            C5003a aVar = (C5003a) Proxy.getInvocationHandler(this.f11660d.invoke(null, sSLSocket));
            if (!aVar.f11665b && aVar.f11666c == null) {
                Platform.m17817d().mo27629a(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable) null);
                return null;
            } else if (aVar.f11665b) {
                return null;
            } else {
                return aVar.f11666c;
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to get selected protocol", (Exception) e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.lang.ReflectiveOperationException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: a */
    public void mo27645a(SSLSocket sSLSocket) {
        try {
            this.f11661e.invoke(null, sSLSocket);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw C4951c.m17342a("unable to remove alpn", (Exception) e);
        }
    }
}
