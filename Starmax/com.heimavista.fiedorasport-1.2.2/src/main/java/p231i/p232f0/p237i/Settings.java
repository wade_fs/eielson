package p231i.p232f0.p237i;

import java.util.Arrays;

/* renamed from: i.f0.i.m */
public final class Settings {

    /* renamed from: a */
    private int f11641a;

    /* renamed from: b */
    private final int[] f11642b = new int[10];

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27611a() {
        this.f11641a = 0;
        Arrays.fill(this.f11642b, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo27613b() {
        if ((this.f11641a & 2) != 0) {
            return this.f11642b[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo27616c(int i) {
        return (this.f11641a & 32) != 0 ? this.f11642b[5] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public boolean mo27618d(int i) {
        return ((1 << i) & this.f11641a) != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo27614b(int i) {
        return (this.f11641a & 16) != 0 ? this.f11642b[4] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo27615c() {
        if ((this.f11641a & 128) != 0) {
            return this.f11642b[7];
        }
        return 65535;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo27617d() {
        return Integer.bitCount(this.f11641a);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Settings mo27610a(int i, int i2) {
        if (i >= 0) {
            int[] iArr = this.f11642b;
            if (i < iArr.length) {
                this.f11641a = (1 << i) | this.f11641a;
                iArr[i] = i2;
            }
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo27609a(int i) {
        return this.f11642b[i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27612a(Settings mVar) {
        for (int i = 0; i < 10; i++) {
            if (mVar.mo27618d(i)) {
                mo27610a(i, mVar.mo27609a(i));
            }
        }
    }
}
