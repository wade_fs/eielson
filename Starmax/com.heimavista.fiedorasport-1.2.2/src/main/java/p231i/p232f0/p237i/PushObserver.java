package p231i.p232f0.p237i;

import java.util.List;
import p244j.BufferedSource;

/* renamed from: i.f0.i.l */
public interface PushObserver {

    /* renamed from: a */
    public static final PushObserver f11640a = new C4998a();

    /* renamed from: i.f0.i.l$a */
    /* compiled from: PushObserver */
    class C4998a implements PushObserver {
        C4998a() {
        }

        /* renamed from: a */
        public void mo27605a(int i, ErrorCode bVar) {
        }

        /* renamed from: a */
        public boolean mo27606a(int i, BufferedSource eVar, int i2, boolean z) {
            eVar.skip((long) i2);
            return true;
        }

        /* renamed from: a */
        public boolean mo27607a(int i, List<Header> list) {
            return true;
        }

        /* renamed from: a */
        public boolean mo27608a(int i, List<Header> list, boolean z) {
            return true;
        }
    }

    /* renamed from: a */
    void mo27605a(int i, ErrorCode bVar);

    /* renamed from: a */
    boolean mo27606a(int i, BufferedSource eVar, int i2, boolean z);

    /* renamed from: a */
    boolean mo27607a(int i, List<Header> list);

    /* renamed from: a */
    boolean mo27608a(int i, List<Header> list, boolean z);
}
