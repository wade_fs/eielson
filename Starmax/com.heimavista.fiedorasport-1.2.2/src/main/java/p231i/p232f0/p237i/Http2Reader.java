package p231i.p232f0.p237i;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p231i.p232f0.C4951c;
import p231i.p232f0.p237i.Hpack;
import p244j.Buffer;
import p244j.BufferedSource;
import p244j.ByteString;
import p244j.C5049s;
import p244j.Timeout;

/* renamed from: i.f0.i.h */
final class Http2Reader implements Closeable {

    /* renamed from: T */
    static final Logger f11593T = Logger.getLogger(Http2.class.getName());

    /* renamed from: P */
    private final BufferedSource f11594P;

    /* renamed from: Q */
    private final C4992a f11595Q = new C4992a(this.f11594P);

    /* renamed from: R */
    private final boolean f11596R;

    /* renamed from: S */
    final Hpack.C4975a f11597S = new Hpack.C4975a(4096, this.f11595Q);

    /* renamed from: i.f0.i.h$a */
    /* compiled from: Http2Reader */
    static final class C4992a implements C5049s {

        /* renamed from: P */
        private final BufferedSource f11598P;

        /* renamed from: Q */
        int f11599Q;

        /* renamed from: R */
        byte f11600R;

        /* renamed from: S */
        int f11601S;

        /* renamed from: T */
        int f11602T;

        /* renamed from: U */
        short f11603U;

        C4992a(BufferedSource eVar) {
            this.f11598P = eVar;
        }

        /* renamed from: a */
        private void m17678a() {
            int i = this.f11601S;
            int a = Http2Reader.m17664a(this.f11598P);
            this.f11602T = a;
            this.f11599Q = a;
            byte readByte = (byte) (this.f11598P.readByte() & 255);
            this.f11600R = (byte) (this.f11598P.readByte() & 255);
            if (Http2Reader.f11593T.isLoggable(Level.FINE)) {
                Http2Reader.f11593T.fine(Http2.m17596a(true, this.f11601S, this.f11599Q, readByte, this.f11600R));
            }
            this.f11601S = this.f11598P.readInt() & Integer.MAX_VALUE;
            if (readByte != 9) {
                Http2.m17597b("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (this.f11601S != i) {
                Http2.m17597b("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            while (true) {
                int i = this.f11602T;
                if (i == 0) {
                    this.f11598P.skip((long) this.f11603U);
                    this.f11603U = 0;
                    if ((this.f11600R & 4) != 0) {
                        return -1;
                    }
                    m17678a();
                } else {
                    long b = this.f11598P.mo27415b(cVar, Math.min(j, (long) i));
                    if (b == -1) {
                        return -1;
                    }
                    this.f11602T = (int) (((long) this.f11602T) - b);
                    return b;
                }
            }
        }

        public void close() {
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return this.f11598P.mo27416h();
        }
    }

    /* renamed from: i.f0.i.h$b */
    /* compiled from: Http2Reader */
    interface C4993b {
        /* renamed from: a */
        void mo27551a();

        /* renamed from: a */
        void mo27552a(int i, int i2, int i3, boolean z);

        /* renamed from: a */
        void mo27553a(int i, int i2, List<Header> list);

        /* renamed from: a */
        void mo27554a(int i, long j);

        /* renamed from: a */
        void mo27555a(int i, ErrorCode bVar);

        /* renamed from: a */
        void mo27556a(int i, ErrorCode bVar, ByteString fVar);

        /* renamed from: a */
        void mo27557a(boolean z, int i, int i2);

        /* renamed from: a */
        void mo27558a(boolean z, int i, int i2, List<Header> list);

        /* renamed from: a */
        void mo27559a(boolean z, int i, BufferedSource eVar, int i2);

        /* renamed from: a */
        void mo27560a(boolean z, Settings mVar);
    }

    Http2Reader(BufferedSource eVar, boolean z) {
        this.f11594P = eVar;
        this.f11596R = z;
    }

    /* renamed from: b */
    private void m17668b(C4993b bVar, int i, byte b, int i2) {
        if (i < 8) {
            Http2.m17597b("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.f11594P.readInt();
            int readInt2 = this.f11594P.readInt();
            int i3 = i - 8;
            ErrorCode a = ErrorCode.m17566a(readInt2);
            if (a != null) {
                ByteString fVar = ByteString.f11876T;
                if (i3 > 0) {
                    fVar = this.f11594P.mo28037a((long) i3);
                }
                bVar.mo27556a(readInt, a, fVar);
                return;
            }
            Http2.m17597b("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            Http2.m17597b("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    /* renamed from: c */
    private void m17669c(C4993b bVar, int i, byte b, int i2) {
        short s = 0;
        if (i2 != 0) {
            boolean z = (b & 1) != 0;
            if ((b & 8) != 0) {
                s = (short) (this.f11594P.readByte() & 255);
            }
            if ((b & 32) != 0) {
                m17666a(bVar, i2);
                i -= 5;
            }
            bVar.mo27558a(z, i2, -1, m17665a(m17663a(i, b, s), s, b, i2));
            return;
        }
        Http2.m17597b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    /* renamed from: d */
    private void m17670d(C4993b bVar, int i, byte b, int i2) {
        boolean z = false;
        if (i != 8) {
            Http2.m17597b("TYPE_PING length != 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.f11594P.readInt();
            int readInt2 = this.f11594P.readInt();
            if ((b & 1) != 0) {
                z = true;
            }
            bVar.mo27557a(z, readInt, readInt2);
        } else {
            Http2.m17597b("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    /* renamed from: e */
    private void m17671e(C4993b bVar, int i, byte b, int i2) {
        if (i != 5) {
            Http2.m17597b("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            m17666a(bVar, i2);
        } else {
            Http2.m17597b("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    /* renamed from: f */
    private void m17672f(C4993b bVar, int i, byte b, int i2) {
        short s = 0;
        if (i2 != 0) {
            if ((b & 8) != 0) {
                s = (short) (this.f11594P.readByte() & 255);
            }
            bVar.mo27553a(i2, this.f11594P.readInt() & Integer.MAX_VALUE, m17665a(m17663a(i - 4, b, s), s, b, i2));
            return;
        }
        Http2.m17597b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    /* renamed from: g */
    private void m17673g(C4993b bVar, int i, byte b, int i2) {
        if (i != 4) {
            Http2.m17597b("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            int readInt = this.f11594P.readInt();
            ErrorCode a = ErrorCode.m17566a(readInt);
            if (a != null) {
                bVar.mo27555a(i2, a);
                return;
            }
            Http2.m17597b("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            Http2.m17597b("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.i.h.b.a(boolean, i.f0.i.m):void
     arg types: [int, i.f0.i.m]
     candidates:
      i.f0.i.h.b.a(int, long):void
      i.f0.i.h.b.a(int, i.f0.i.b):void
      i.f0.i.h.b.a(boolean, i.f0.i.m):void */
    /* renamed from: h */
    private void m17674h(C4993b bVar, int i, byte b, int i2) {
        if (i2 != 0) {
            Http2.m17597b("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b & 1) != 0) {
            if (i == 0) {
                bVar.mo27551a();
            } else {
                Http2.m17597b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i % 6 == 0) {
            Settings mVar = new Settings();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short readShort = this.f11594P.readShort() & 65535;
                int readInt = this.f11594P.readInt();
                switch (readShort) {
                    case 2:
                        if (!(readInt == 0 || readInt == 1)) {
                            Http2.m17597b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            throw null;
                        }
                    case 3:
                        readShort = 4;
                        break;
                    case 4:
                        readShort = 7;
                        if (readInt >= 0) {
                            break;
                        } else {
                            Http2.m17597b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    case 5:
                        if (readInt >= 16384 && readInt <= 16777215) {
                            break;
                        } else {
                            Http2.m17597b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                            throw null;
                        }
                        break;
                }
                mVar.mo27610a(readShort, readInt);
            }
            bVar.mo27560a(false, mVar);
        } else {
            Http2.m17597b("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            throw null;
        }
    }

    /* renamed from: i */
    private void m17675i(C4993b bVar, int i, byte b, int i2) {
        if (i == 4) {
            long readInt = ((long) this.f11594P.readInt()) & 2147483647L;
            if (readInt != 0) {
                bVar.mo27554a(i2, readInt);
                return;
            }
            Http2.m17597b("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        Http2.m17597b("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.i.h.a(boolean, i.f0.i.h$b):boolean
     arg types: [int, i.f0.i.h$b]
     candidates:
      i.f0.i.h.a(i.f0.i.h$b, int):void
      i.f0.i.h.a(boolean, i.f0.i.h$b):boolean */
    /* renamed from: a */
    public void mo27561a(C4993b bVar) {
        if (!this.f11596R) {
            ByteString a = this.f11594P.mo28037a((long) Http2.f11510a.mo28104l());
            if (f11593T.isLoggable(Level.FINE)) {
                f11593T.fine(C4951c.m17345a("<< CONNECTION %s", a.mo28099h()));
            }
            if (!Http2.f11510a.equals(a)) {
                Http2.m17597b("Expected a connection header but was %s", a.mo28107o());
                throw null;
            }
        } else if (!mo27562a(true, bVar)) {
            Http2.m17597b("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    public void close() {
        this.f11594P.close();
    }

    /* renamed from: a */
    public boolean mo27562a(boolean z, C4993b bVar) {
        try {
            this.f11594P.mo28052e(9);
            int a = m17664a(this.f11594P);
            if (a < 0 || a > 16384) {
                Http2.m17597b("FRAME_SIZE_ERROR: %s", Integer.valueOf(a));
                throw null;
            }
            byte readByte = (byte) (this.f11594P.readByte() & 255);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.f11594P.readByte() & 255);
                int readInt = this.f11594P.readInt() & Integer.MAX_VALUE;
                if (f11593T.isLoggable(Level.FINE)) {
                    f11593T.fine(Http2.m17596a(true, readInt, a, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        m17667a(bVar, a, readByte2, readInt);
                        break;
                    case 1:
                        m17669c(bVar, a, readByte2, readInt);
                        break;
                    case 2:
                        m17671e(bVar, a, readByte2, readInt);
                        break;
                    case 3:
                        m17673g(bVar, a, readByte2, readInt);
                        break;
                    case 4:
                        m17674h(bVar, a, readByte2, readInt);
                        break;
                    case 5:
                        m17672f(bVar, a, readByte2, readInt);
                        break;
                    case 6:
                        m17670d(bVar, a, readByte2, readInt);
                        break;
                    case 7:
                        m17668b(bVar, a, readByte2, readInt);
                        break;
                    case 8:
                        m17675i(bVar, a, readByte2, readInt);
                        break;
                    default:
                        this.f11594P.skip((long) a);
                        break;
                }
                return true;
            }
            Http2.m17597b("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
            throw null;
        } catch (IOException unused) {
            return false;
        }
    }

    /* renamed from: a */
    private List<Header> m17665a(int i, short s, byte b, int i2) {
        C4992a aVar = this.f11595Q;
        aVar.f11602T = i;
        aVar.f11599Q = i;
        aVar.f11603U = s;
        aVar.f11600R = b;
        aVar.f11601S = i2;
        this.f11597S.mo27520c();
        return this.f11597S.mo27518a();
    }

    /* renamed from: a */
    private void m17667a(C4993b bVar, int i, byte b, int i2) {
        short s = 0;
        if (i2 != 0) {
            boolean z = true;
            boolean z2 = (b & 1) != 0;
            if ((b & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b & 8) != 0) {
                    s = (short) (this.f11594P.readByte() & 255);
                }
                bVar.mo27559a(z2, i2, this.f11594P, m17663a(i, b, s));
                this.f11594P.skip((long) s);
                return;
            }
            Http2.m17597b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        Http2.m17597b("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    /* renamed from: a */
    private void m17666a(C4993b bVar, int i) {
        int readInt = this.f11594P.readInt();
        bVar.mo27552a(i, readInt & Integer.MAX_VALUE, (this.f11594P.readByte() & 255) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }

    /* renamed from: a */
    static int m17664a(BufferedSource eVar) {
        return (eVar.readByte() & 255) | ((eVar.readByte() & 255) << 16) | ((eVar.readByte() & 255) << 8);
    }

    /* renamed from: a */
    static int m17663a(int i, byte b, short s) {
        if ((b & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        Http2.m17597b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }
}
