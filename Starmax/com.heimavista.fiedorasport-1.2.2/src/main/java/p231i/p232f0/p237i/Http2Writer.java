package p231i.p232f0.p237i;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p231i.p232f0.C4951c;
import p231i.p232f0.p237i.Hpack;
import p244j.Buffer;
import p244j.BufferedSink;

/* renamed from: i.f0.i.j */
final class Http2Writer implements Closeable {

    /* renamed from: V */
    private static final Logger f11626V = Logger.getLogger(Http2.class.getName());

    /* renamed from: P */
    private final BufferedSink f11627P;

    /* renamed from: Q */
    private final boolean f11628Q;

    /* renamed from: R */
    private final Buffer f11629R = new Buffer();

    /* renamed from: S */
    private int f11630S = 16384;

    /* renamed from: T */
    private boolean f11631T;

    /* renamed from: U */
    final Hpack.C4976b f11632U = new Hpack.C4976b(this.f11629R);

    Http2Writer(BufferedSink dVar, boolean z) {
        this.f11627P = dVar;
        this.f11628Q = z;
    }

    /* renamed from: a */
    public synchronized void mo27586a() {
        if (this.f11631T) {
            throw new IOException("closed");
        } else if (this.f11628Q) {
            if (f11626V.isLoggable(Level.FINE)) {
                f11626V.fine(C4951c.m17345a(">> CONNECTION %s", Http2.f11510a.mo28099h()));
            }
            this.f11627P.write(Http2.f11510a.mo28106n());
            this.f11627P.flush();
        }
    }

    /* renamed from: b */
    public int mo27598b() {
        return this.f11630S;
    }

    public synchronized void close() {
        this.f11631T = true;
        this.f11627P.close();
    }

    public synchronized void flush() {
        if (!this.f11631T) {
            this.f11627P.flush();
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: b */
    public synchronized void mo27599b(Settings mVar) {
        if (!this.f11631T) {
            int i = 0;
            mo27588a(0, mVar.mo27617d() * 6, (byte) 4, (byte) 0);
            while (i < 10) {
                if (mVar.mo27618d(i)) {
                    this.f11627P.writeShort(i == 4 ? 3 : i == 7 ? 4 : i);
                    this.f11627P.writeInt(mVar.mo27609a(i));
                }
                i++;
            }
            this.f11627P.flush();
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: a */
    public synchronized void mo27593a(Settings mVar) {
        if (!this.f11631T) {
            this.f11630S = mVar.mo27616c(this.f11630S);
            if (mVar.mo27613b() != -1) {
                this.f11632U.mo27521a(mVar.mo27613b());
            }
            mo27588a(0, 0, (byte) 4, (byte) 1);
            this.f11627P.flush();
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: b */
    private void m17722b(int i, long j) {
        while (j > 0) {
            int min = (int) Math.min((long) this.f11630S, j);
            long j2 = (long) min;
            j -= j2;
            mo27588a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.f11627P.mo27444a(this.f11629R, j2);
        }
    }

    /* renamed from: a */
    public synchronized void mo27589a(int i, int i2, List<Header> list) {
        if (!this.f11631T) {
            this.f11632U.mo27524a(list);
            long e = this.f11629R.mo28051e();
            int min = (int) Math.min((long) (this.f11630S - 4), e);
            long j = (long) min;
            int i3 = (e > j ? 1 : (e == j ? 0 : -1));
            mo27588a(i, min + 4, (byte) 5, i3 == 0 ? (byte) 4 : 0);
            this.f11627P.writeInt(i2 & Integer.MAX_VALUE);
            this.f11627P.mo27444a(this.f11629R, j);
            if (i3 > 0) {
                m17722b(i, e - j);
            }
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: a */
    public synchronized void mo27595a(boolean z, int i, int i2, List<Header> list) {
        if (!this.f11631T) {
            mo27597a(z, i, list);
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: a */
    public synchronized void mo27591a(int i, ErrorCode bVar) {
        if (this.f11631T) {
            throw new IOException("closed");
        } else if (bVar.f11481P != -1) {
            mo27588a(i, 4, (byte) 3, (byte) 0);
            this.f11627P.writeInt(bVar.f11481P);
            this.f11627P.flush();
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    public synchronized void mo27596a(boolean z, int i, Buffer cVar, int i2) {
        if (!this.f11631T) {
            byte b = 0;
            if (z) {
                b = (byte) 1;
            }
            mo27587a(i, b, cVar, i2);
        } else {
            throw new IOException("closed");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27587a(int i, byte b, Buffer cVar, int i2) {
        mo27588a(i, i2, (byte) 0, b);
        if (i2 > 0) {
            this.f11627P.mo27444a(cVar, (long) i2);
        }
    }

    /* renamed from: a */
    public synchronized void mo27594a(boolean z, int i, int i2) {
        if (!this.f11631T) {
            mo27588a(0, 8, (byte) 6, z ? (byte) 1 : 0);
            this.f11627P.writeInt(i);
            this.f11627P.writeInt(i2);
            this.f11627P.flush();
        } else {
            throw new IOException("closed");
        }
    }

    /* renamed from: a */
    public synchronized void mo27592a(int i, ErrorCode bVar, byte[] bArr) {
        if (this.f11631T) {
            throw new IOException("closed");
        } else if (bVar.f11481P != -1) {
            mo27588a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.f11627P.writeInt(i);
            this.f11627P.writeInt(bVar.f11481P);
            if (bArr.length > 0) {
                this.f11627P.write(bArr);
            }
            this.f11627P.flush();
        } else {
            Http2.m17594a("errorCode.httpCode == -1", new Object[0]);
            throw null;
        }
    }

    /* renamed from: a */
    public synchronized void mo27590a(int i, long j) {
        if (this.f11631T) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            Http2.m17594a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            throw null;
        } else {
            mo27588a(i, 4, (byte) 8, (byte) 0);
            this.f11627P.writeInt((int) j);
            this.f11627P.flush();
        }
    }

    /* renamed from: a */
    public void mo27588a(int i, int i2, byte b, byte b2) {
        if (f11626V.isLoggable(Level.FINE)) {
            f11626V.fine(Http2.m17596a(false, i, i2, b, b2));
        }
        int i3 = this.f11630S;
        if (i2 > i3) {
            Http2.m17594a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(i3), Integer.valueOf(i2));
            throw null;
        } else if ((Integer.MIN_VALUE & i) == 0) {
            m17721a(this.f11627P, i2);
            this.f11627P.writeByte(b & 255);
            this.f11627P.writeByte(b2 & 255);
            this.f11627P.writeInt(i & Integer.MAX_VALUE);
        } else {
            Http2.m17594a("reserved bit set: %s", Integer.valueOf(i));
            throw null;
        }
    }

    /* renamed from: a */
    private static void m17721a(BufferedSink dVar, int i) {
        dVar.writeByte((i >>> 16) & 255);
        dVar.writeByte((i >>> 8) & 255);
        dVar.writeByte(i & 255);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27597a(boolean z, int i, List<Header> list) {
        if (!this.f11631T) {
            this.f11632U.mo27524a(list);
            long e = this.f11629R.mo28051e();
            int min = (int) Math.min((long) this.f11630S, e);
            long j = (long) min;
            int i2 = (e > j ? 1 : (e == j ? 0 : -1));
            byte b = i2 == 0 ? (byte) 4 : 0;
            if (z) {
                b = (byte) (b | 1);
            }
            mo27588a(i, min, (byte) 1, b);
            this.f11627P.mo27444a(this.f11629R, j);
            if (i2 > 0) {
                m17722b(i, e - j);
                return;
            }
            return;
        }
        throw new IOException("closed");
    }
}
