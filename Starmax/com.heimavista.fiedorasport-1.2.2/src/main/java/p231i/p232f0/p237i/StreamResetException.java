package p231i.p232f0.p237i;

import java.io.IOException;

/* renamed from: i.f0.i.n */
public final class StreamResetException extends IOException {

    /* renamed from: P */
    public final ErrorCode f11643P;

    public StreamResetException(ErrorCode bVar) {
        super("stream was reset: " + bVar);
        this.f11643P = bVar;
    }
}
