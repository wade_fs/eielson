package p231i.p232f0.p237i;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import p244j.AsyncTimeout;
import p244j.Buffer;
import p244j.BufferedSource;
import p244j.C5049s;
import p244j.Sink;
import p244j.Timeout;

/* renamed from: i.f0.i.i */
public final class Http2Stream {

    /* renamed from: a */
    long f11604a = 0;

    /* renamed from: b */
    long f11605b;

    /* renamed from: c */
    final int f11606c;

    /* renamed from: d */
    final Http2Connection f11607d;

    /* renamed from: e */
    private List<Header> f11608e;

    /* renamed from: f */
    private boolean f11609f;

    /* renamed from: g */
    private final C4995b f11610g;

    /* renamed from: h */
    final C4994a f11611h;

    /* renamed from: i */
    final C4996c f11612i = new C4996c();

    /* renamed from: j */
    final C4996c f11613j = new C4996c();

    /* renamed from: k */
    ErrorCode f11614k = null;

    /* renamed from: i.f0.i.i$c */
    /* compiled from: Http2Stream */
    class C4996c extends AsyncTimeout {
        C4996c() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public IOException mo27583b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* access modifiers changed from: protected */
        /* renamed from: i */
        public void mo27584i() {
            Http2Stream.this.mo27570b(ErrorCode.CANCEL);
        }

        /* renamed from: k */
        public void mo27585k() {
            if (mo28023h()) {
                throw mo27583b((IOException) null);
            }
        }
    }

    Http2Stream(int i, Http2Connection gVar, boolean z, boolean z2, List<Header> list) {
        if (gVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list != null) {
            this.f11606c = i;
            this.f11607d = gVar;
            this.f11605b = (long) gVar.f11546d0.mo27615c();
            this.f11610g = new C4995b((long) gVar.f11545c0.mo27615c());
            this.f11611h = new C4994a();
            this.f11610g.f11623T = z2;
            this.f11611h.f11617R = z;
        } else {
            throw new NullPointerException("requestHeaders == null");
        }
    }

    /* renamed from: a */
    public void mo27566a(ErrorCode bVar) {
        if (m17691d(bVar)) {
            this.f11607d.mo27539b(this.f11606c, bVar);
        }
    }

    /* renamed from: b */
    public void mo27570b(ErrorCode bVar) {
        if (m17691d(bVar)) {
            this.f11607d.mo27543c(this.f11606c, bVar);
        }
    }

    /* renamed from: c */
    public int mo27571c() {
        return this.f11606c;
    }

    /* renamed from: d */
    public Sink mo27573d() {
        synchronized (this) {
            if (!this.f11609f) {
                if (!mo27575f()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.f11611h;
    }

    /* renamed from: e */
    public C5049s mo27574e() {
        return this.f11610g;
    }

    /* renamed from: f */
    public boolean mo27575f() {
        if (this.f11607d.f11532P == ((this.f11606c & 1) == 1)) {
            return true;
        }
        return false;
    }

    /* renamed from: g */
    public synchronized boolean mo27576g() {
        if (this.f11614k != null) {
            return false;
        }
        if ((this.f11610g.f11623T || this.f11610g.f11622S) && ((this.f11611h.f11617R || this.f11611h.f11616Q) && this.f11609f)) {
            return false;
        }
        return true;
    }

    /* renamed from: h */
    public Timeout mo27577h() {
        return this.f11612i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public void mo27578i() {
        boolean g;
        synchronized (this) {
            this.f11610g.f11623T = true;
            g = mo27576g();
            notifyAll();
        }
        if (!g) {
            this.f11607d.mo27541c(this.f11606c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002f, code lost:
        r2.f11612i.mo27585k();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0034, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: j */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<p231i.p232f0.p237i.Header> mo27579j() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.mo27575f()     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0035
            i.f0.i.i$c r0 = r2.f11612i     // Catch:{ all -> 0x003d }
            r0.mo28022g()     // Catch:{ all -> 0x003d }
        L_0x000c:
            java.util.List<i.f0.i.c> r0 = r2.f11608e     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x0018
            i.f0.i.b r0 = r2.f11614k     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x0018
            r2.mo27580k()     // Catch:{ all -> 0x002e }
            goto L_0x000c
        L_0x0018:
            i.f0.i.i$c r0 = r2.f11612i     // Catch:{ all -> 0x003d }
            r0.mo27585k()     // Catch:{ all -> 0x003d }
            java.util.List<i.f0.i.c> r0 = r2.f11608e     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0026
            r1 = 0
            r2.f11608e = r1     // Catch:{ all -> 0x003d }
            monitor-exit(r2)
            return r0
        L_0x0026:
            i.f0.i.n r0 = new i.f0.i.n     // Catch:{ all -> 0x003d }
            i.f0.i.b r1 = r2.f11614k     // Catch:{ all -> 0x003d }
            r0.<init>(r1)     // Catch:{ all -> 0x003d }
            throw r0     // Catch:{ all -> 0x003d }
        L_0x002e:
            r0 = move-exception
            i.f0.i.i$c r1 = r2.f11612i     // Catch:{ all -> 0x003d }
            r1.mo27585k()     // Catch:{ all -> 0x003d }
            throw r0     // Catch:{ all -> 0x003d }
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x003d }
            java.lang.String r1 = "servers cannot read response headers"
            r0.<init>(r1)     // Catch:{ all -> 0x003d }
            throw r0     // Catch:{ all -> 0x003d }
        L_0x003d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Stream.mo27579j():java.util.List");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public void mo27580k() {
        try {
            wait();
        } catch (InterruptedException unused) {
            throw new InterruptedIOException();
        }
    }

    /* renamed from: l */
    public Timeout mo27581l() {
        return this.f11613j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public synchronized void mo27572c(ErrorCode bVar) {
        if (this.f11614k == null) {
            this.f11614k = bVar;
            notifyAll();
        }
    }

    /* renamed from: i.f0.i.i$a */
    /* compiled from: Http2Stream */
    final class C4994a implements Sink {

        /* renamed from: P */
        private final Buffer f11615P = new Buffer();

        /* renamed from: Q */
        boolean f11616Q;

        /* renamed from: R */
        boolean f11617R;

        C4994a() {
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            this.f11615P.mo27444a(cVar, j);
            while (this.f11615P.mo28051e() >= 16384) {
                m17710a(false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.i.g.a(int, boolean, j.c, long):void
         arg types: [int, int, ?[OBJECT, ARRAY], int]
         candidates:
          i.f0.i.g.a(int, j.e, int, boolean):void
          i.f0.i.g.a(int, boolean, j.c, long):void */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
            if (r8.f11615P.mo28051e() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
            if (r8.f11615P.mo28051e() <= 0) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
            m17710a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
            r0 = r8.f11618S;
            r0.f11607d.mo27532a(r0.f11606c, true, (p244j.Buffer) null, 0L);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
            r2 = r8.f11618S;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.f11616Q = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
            r8.f11618S.f11607d.flush();
            r8.f11618S.mo27564a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r8.f11618S.f11611h.f11617R != false) goto L_0x003a;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r8 = this;
                i.f0.i.i r0 = p231i.p232f0.p237i.Http2Stream.this
                monitor-enter(r0)
                boolean r1 = r8.f11616Q     // Catch:{ all -> 0x0050 }
                if (r1 == 0) goto L_0x0009
                monitor-exit(r0)     // Catch:{ all -> 0x0050 }
                return
            L_0x0009:
                monitor-exit(r0)     // Catch:{ all -> 0x0050 }
                i.f0.i.i r0 = p231i.p232f0.p237i.Http2Stream.this
                i.f0.i.i$a r0 = r0.f11611h
                boolean r0 = r0.f11617R
                r1 = 1
                if (r0 != 0) goto L_0x003a
                j.c r0 = r8.f11615P
                long r2 = r0.mo28051e()
                r4 = 0
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x002d
            L_0x001f:
                j.c r0 = r8.f11615P
                long r2 = r0.mo28051e()
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x003a
                r8.m17710a(r1)
                goto L_0x001f
            L_0x002d:
                i.f0.i.i r0 = p231i.p232f0.p237i.Http2Stream.this
                i.f0.i.g r2 = r0.f11607d
                int r3 = r0.f11606c
                r4 = 1
                r5 = 0
                r6 = 0
                r2.mo27532a(r3, r4, r5, r6)
            L_0x003a:
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this
                monitor-enter(r2)
                r8.f11616Q = r1     // Catch:{ all -> 0x004d }
                monitor-exit(r2)     // Catch:{ all -> 0x004d }
                i.f0.i.i r0 = p231i.p232f0.p237i.Http2Stream.this
                i.f0.i.g r0 = r0.f11607d
                r0.flush()
                i.f0.i.i r0 = p231i.p232f0.p237i.Http2Stream.this
                r0.mo27564a()
                return
            L_0x004d:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x004d }
                throw r0
            L_0x0050:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0050 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Stream.C4994a.close():void");
        }

        public void flush() {
            synchronized (Http2Stream.this) {
                Http2Stream.this.mo27569b();
            }
            while (this.f11615P.mo28051e() > 0) {
                m17710a(false);
                Http2Stream.this.f11607d.flush();
            }
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return Http2Stream.this.f11613j;
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: a */
        private void m17710a(boolean z) {
            long min;
            synchronized (Http2Stream.this) {
                Http2Stream.this.f11613j.mo28022g();
                while (Http2Stream.this.f11605b <= 0 && !this.f11617R && !this.f11616Q && Http2Stream.this.f11614k == null) {
                    try {
                        Http2Stream.this.mo27580k();
                    } catch (Throwable th) {
                        Http2Stream.this.f11613j.mo27585k();
                        throw th;
                    }
                }
                Http2Stream.this.f11613j.mo27585k();
                Http2Stream.this.mo27569b();
                min = Math.min(Http2Stream.this.f11605b, this.f11615P.mo28051e());
                Http2Stream.this.f11605b -= min;
            }
            Http2Stream.this.f11613j.mo28022g();
            try {
                Http2Stream.this.f11607d.mo27532a(Http2Stream.this.f11606c, z && min == this.f11615P.mo28051e(), this.f11615P, min);
            } finally {
                Http2Stream.this.f11613j.mo27585k();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27568a(List<Header> list) {
        boolean z;
        synchronized (this) {
            z = true;
            this.f11609f = true;
            if (this.f11608e == null) {
                this.f11608e = list;
                z = mo27576g();
                notifyAll();
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.f11608e);
                arrayList.add(null);
                arrayList.addAll(list);
                this.f11608e = arrayList;
            }
        }
        if (!z) {
            this.f11607d.mo27541c(this.f11606c);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27569b() {
        C4994a aVar = this.f11611h;
        if (aVar.f11616Q) {
            throw new IOException("stream closed");
        } else if (!aVar.f11617R) {
            ErrorCode bVar = this.f11614k;
            if (bVar != null) {
                throw new StreamResetException(bVar);
            }
        } else {
            throw new IOException("stream finished");
        }
    }

    /* renamed from: d */
    private boolean m17691d(ErrorCode bVar) {
        synchronized (this) {
            if (this.f11614k != null) {
                return false;
            }
            if (this.f11610g.f11623T && this.f11611h.f11617R) {
                return false;
            }
            this.f11614k = bVar;
            notifyAll();
            this.f11607d.mo27541c(this.f11606c);
            return true;
        }
    }

    /* renamed from: i.f0.i.i$b */
    /* compiled from: Http2Stream */
    private final class C4995b implements C5049s {

        /* renamed from: P */
        private final Buffer f11619P = new Buffer();

        /* renamed from: Q */
        private final Buffer f11620Q = new Buffer();

        /* renamed from: R */
        private final long f11621R;

        /* renamed from: S */
        boolean f11622S;

        /* renamed from: T */
        boolean f11623T;

        C4995b(long j) {
            this.f11621R = j;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27582a(BufferedSource eVar, long j) {
            boolean z;
            boolean z2;
            boolean z3;
            while (j > 0) {
                synchronized (Http2Stream.this) {
                    z = this.f11623T;
                    z2 = true;
                    z3 = this.f11620Q.mo28051e() + j > this.f11621R;
                }
                if (z3) {
                    eVar.skip(j);
                    Http2Stream.this.mo27570b(ErrorCode.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    eVar.skip(j);
                    return;
                } else {
                    long b = eVar.mo27415b(this.f11619P, j);
                    if (b != -1) {
                        j -= b;
                        synchronized (Http2Stream.this) {
                            if (this.f11620Q.mo28051e() != 0) {
                                z2 = false;
                            }
                            this.f11620Q.mo28030a(this.f11619P);
                            if (z2) {
                                Http2Stream.this.notifyAll();
                            }
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x005d, code lost:
            r10 = r7.f11624U.f11607d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0061, code lost:
            monitor-enter(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r7.f11624U.f11607d.f11543a0 += r8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0080, code lost:
            if (r7.f11624U.f11607d.f11543a0 < ((long) (r7.f11624U.f11607d.f11545c0.mo27615c() / 2))) goto L_0x0096;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0082, code lost:
            r7.f11624U.f11607d.mo27527a(0, r7.f11624U.f11607d.f11543a0);
            r7.f11624U.f11607d.f11543a0 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
            monitor-exit(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0097, code lost:
            return r8;
         */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public long mo27415b(p244j.Buffer r8, long r9) {
            /*
                r7 = this;
                r0 = 0
                int r2 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
                if (r2 < 0) goto L_0x009e
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this
                monitor-enter(r2)
                r7.m17714b()     // Catch:{ all -> 0x009b }
                r7.m17713a()     // Catch:{ all -> 0x009b }
                j.c r3 = r7.f11620Q     // Catch:{ all -> 0x009b }
                long r3 = r3.mo28051e()     // Catch:{ all -> 0x009b }
                int r5 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
                if (r5 != 0) goto L_0x001d
                r8 = -1
                monitor-exit(r2)     // Catch:{ all -> 0x009b }
                return r8
            L_0x001d:
                j.c r3 = r7.f11620Q     // Catch:{ all -> 0x009b }
                j.c r4 = r7.f11620Q     // Catch:{ all -> 0x009b }
                long r4 = r4.mo28051e()     // Catch:{ all -> 0x009b }
                long r9 = java.lang.Math.min(r9, r4)     // Catch:{ all -> 0x009b }
                long r8 = r3.mo27415b(r8, r9)     // Catch:{ all -> 0x009b }
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                long r3 = r10.f11604a     // Catch:{ all -> 0x009b }
                long r3 = r3 + r8
                r10.f11604a = r3     // Catch:{ all -> 0x009b }
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                long r3 = r10.f11604a     // Catch:{ all -> 0x009b }
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                i.f0.i.g r10 = r10.f11607d     // Catch:{ all -> 0x009b }
                i.f0.i.m r10 = r10.f11545c0     // Catch:{ all -> 0x009b }
                int r10 = r10.mo27615c()     // Catch:{ all -> 0x009b }
                int r10 = r10 / 2
                long r5 = (long) r10     // Catch:{ all -> 0x009b }
                int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r10 < 0) goto L_0x005c
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                i.f0.i.g r10 = r10.f11607d     // Catch:{ all -> 0x009b }
                i.f0.i.i r3 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                int r3 = r3.f11606c     // Catch:{ all -> 0x009b }
                i.f0.i.i r4 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                long r4 = r4.f11604a     // Catch:{ all -> 0x009b }
                r10.mo27527a(r3, r4)     // Catch:{ all -> 0x009b }
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x009b }
                r10.f11604a = r0     // Catch:{ all -> 0x009b }
            L_0x005c:
                monitor-exit(r2)     // Catch:{ all -> 0x009b }
                i.f0.i.i r10 = p231i.p232f0.p237i.Http2Stream.this
                i.f0.i.g r10 = r10.f11607d
                monitor-enter(r10)
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r2 = r2.f11607d     // Catch:{ all -> 0x0098 }
                long r3 = r2.f11543a0     // Catch:{ all -> 0x0098 }
                long r3 = r3 + r8
                r2.f11543a0 = r3     // Catch:{ all -> 0x0098 }
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r2 = r2.f11607d     // Catch:{ all -> 0x0098 }
                long r2 = r2.f11543a0     // Catch:{ all -> 0x0098 }
                i.f0.i.i r4 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r4 = r4.f11607d     // Catch:{ all -> 0x0098 }
                i.f0.i.m r4 = r4.f11545c0     // Catch:{ all -> 0x0098 }
                int r4 = r4.mo27615c()     // Catch:{ all -> 0x0098 }
                int r4 = r4 / 2
                long r4 = (long) r4     // Catch:{ all -> 0x0098 }
                int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r6 < 0) goto L_0x0096
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r2 = r2.f11607d     // Catch:{ all -> 0x0098 }
                r3 = 0
                i.f0.i.i r4 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r4 = r4.f11607d     // Catch:{ all -> 0x0098 }
                long r4 = r4.f11543a0     // Catch:{ all -> 0x0098 }
                r2.mo27527a(r3, r4)     // Catch:{ all -> 0x0098 }
                i.f0.i.i r2 = p231i.p232f0.p237i.Http2Stream.this     // Catch:{ all -> 0x0098 }
                i.f0.i.g r2 = r2.f11607d     // Catch:{ all -> 0x0098 }
                r2.f11543a0 = r0     // Catch:{ all -> 0x0098 }
            L_0x0096:
                monitor-exit(r10)     // Catch:{ all -> 0x0098 }
                return r8
            L_0x0098:
                r8 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x0098 }
                throw r8
            L_0x009b:
                r8 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x009b }
                throw r8
            L_0x009e:
                java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "byteCount < 0: "
                r0.append(r1)
                r0.append(r9)
                java.lang.String r9 = r0.toString()
                r8.<init>(r9)
                throw r8
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Stream.C4995b.mo27415b(j.c, long):long");
        }

        public void close() {
            synchronized (Http2Stream.this) {
                this.f11622S = true;
                this.f11620Q.mo28039a();
                Http2Stream.this.notifyAll();
            }
            Http2Stream.this.mo27564a();
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return Http2Stream.this.f11612i;
        }

        /* renamed from: a */
        private void m17713a() {
            if (!this.f11622S) {
                ErrorCode bVar = Http2Stream.this.f11614k;
                if (bVar != null) {
                    throw new StreamResetException(bVar);
                }
                return;
            }
            throw new IOException("stream closed");
        }

        /* renamed from: b */
        private void m17714b() {
            Http2Stream.this.f11612i.mo28022g();
            while (this.f11620Q.mo28051e() == 0 && !this.f11623T && !this.f11622S && Http2Stream.this.f11614k == null) {
                try {
                    Http2Stream.this.mo27580k();
                } finally {
                    Http2Stream.this.f11612i.mo27585k();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27567a(BufferedSource eVar, int i) {
        this.f11610g.mo27582a(eVar, (long) i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27564a() {
        boolean z;
        boolean g;
        synchronized (this) {
            z = !this.f11610g.f11623T && this.f11610g.f11622S && (this.f11611h.f11617R || this.f11611h.f11616Q);
            g = mo27576g();
        }
        if (z) {
            mo27566a(ErrorCode.CANCEL);
        } else if (!g) {
            this.f11607d.mo27541c(this.f11606c);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27565a(long j) {
        this.f11605b += j;
        if (j > 0) {
            notifyAll();
        }
    }
}
