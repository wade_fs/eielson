package p231i.p232f0.p237i;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import p231i.C5015r;
import p231i.C5023y;
import p231i.Interceptor;
import p231i.OkHttpClient;
import p231i.Protocol;
import p231i.Response;
import p231i.ResponseBody;
import p231i.p232f0.C4951c;
import p231i.p232f0.Internal;
import p231i.p232f0.p234f.StreamAllocation;
import p231i.p232f0.p235g.HttpCodec;
import p231i.p232f0.p235g.HttpHeaders;
import p231i.p232f0.p235g.RealResponseBody;
import p231i.p232f0.p235g.RequestLine;
import p231i.p232f0.p235g.StatusLine;
import p244j.Buffer;
import p244j.ByteString;
import p244j.C5049s;
import p244j.ForwardingSource;
import p244j.Okio;
import p244j.Sink;

/* renamed from: i.f0.i.f */
public final class Http2Codec implements HttpCodec {

    /* renamed from: e */
    private static final ByteString f11514e = ByteString.m18170d("connection");

    /* renamed from: f */
    private static final ByteString f11515f = ByteString.m18170d("host");

    /* renamed from: g */
    private static final ByteString f11516g = ByteString.m18170d("keep-alive");

    /* renamed from: h */
    private static final ByteString f11517h = ByteString.m18170d("proxy-connection");

    /* renamed from: i */
    private static final ByteString f11518i = ByteString.m18170d("transfer-encoding");

    /* renamed from: j */
    private static final ByteString f11519j = ByteString.m18170d("te");

    /* renamed from: k */
    private static final ByteString f11520k = ByteString.m18170d("encoding");

    /* renamed from: l */
    private static final ByteString f11521l = ByteString.m18170d("upgrade");

    /* renamed from: m */
    private static final List<ByteString> f11522m = C4951c.m17349a(f11514e, f11515f, f11516g, f11517h, f11519j, f11518i, f11520k, f11521l, Header.f11484f, Header.f11485g, Header.f11486h, Header.f11487i);

    /* renamed from: n */
    private static final List<ByteString> f11523n = C4951c.m17349a(f11514e, f11515f, f11516g, f11517h, f11519j, f11518i, f11520k, f11521l);

    /* renamed from: a */
    private final Interceptor.C5019a f11524a;

    /* renamed from: b */
    final StreamAllocation f11525b;

    /* renamed from: c */
    private final Http2Connection f11526c;

    /* renamed from: d */
    private Http2Stream f11527d;

    /* renamed from: i.f0.i.f$a */
    /* compiled from: Http2Codec */
    class C4977a extends ForwardingSource {

        /* renamed from: Q */
        boolean f11528Q = false;

        /* renamed from: R */
        long f11529R = 0;

        C4977a(C5049s sVar) {
            super(sVar);
        }

        /* renamed from: a */
        private void m17606a(IOException iOException) {
            if (!this.f11528Q) {
                this.f11528Q = true;
                Http2Codec fVar = Http2Codec.this;
                fVar.f11525b.mo27476a(false, fVar, this.f11529R, iOException);
            }
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            try {
                long b = mo28110a().mo27415b(cVar, j);
                if (b > 0) {
                    this.f11529R += b;
                }
                return b;
            } catch (IOException e) {
                m17606a(e);
                throw e;
            }
        }

        public void close() {
            super.close();
            m17606a(null);
        }
    }

    public Http2Codec(OkHttpClient vVar, Interceptor.C5019a aVar, StreamAllocation gVar, Http2Connection gVar2) {
        this.f11524a = aVar;
        this.f11525b = gVar;
        this.f11526c = gVar2;
    }

    /* renamed from: a */
    public Sink mo27485a(C5023y yVar, long j) {
        return this.f11527d.mo27573d();
    }

    /* renamed from: b */
    public void mo27488b() {
        this.f11526c.flush();
    }

    /* renamed from: b */
    public static List<Header> m17599b(C5023y yVar) {
        C5015r c = yVar.mo27823c();
        ArrayList arrayList = new ArrayList(c.mo27739b() + 4);
        arrayList.add(new Header(Header.f11484f, yVar.mo27825e()));
        arrayList.add(new Header(Header.f11485g, RequestLine.m17527a(yVar.mo27827g())));
        String a = yVar.mo27820a("Host");
        if (a != null) {
            arrayList.add(new Header(Header.f11487i, a));
        }
        arrayList.add(new Header(Header.f11486h, yVar.mo27827g().mo27768m()));
        int b = c.mo27739b();
        for (int i = 0; i < b; i++) {
            ByteString d = ByteString.m18170d(c.mo27737a(i).toLowerCase(Locale.US));
            if (!f11522m.contains(d)) {
                arrayList.add(new Header(d, c.mo27740b(i)));
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public void mo27487a(C5023y yVar) {
        if (this.f11527d == null) {
            this.f11527d = this.f11526c.mo27526a(m17599b(yVar), yVar.mo27819a() != null);
            this.f11527d.mo27577h().mo28115a((long) this.f11524a.mo27490a(), TimeUnit.MILLISECONDS);
            this.f11527d.mo27581l().mo28115a((long) this.f11524a.mo27493b(), TimeUnit.MILLISECONDS);
        }
    }

    /* renamed from: a */
    public void mo27486a() {
        this.f11527d.mo27573d().close();
    }

    /* renamed from: a */
    public Response.C4938a mo27483a(boolean z) {
        Response.C4938a a = m17598a(this.f11527d.mo27579j());
        if (!z || Internal.f11301a.mo27399a(a) != 100) {
            return a;
        }
        return null;
    }

    /* renamed from: a */
    public static Response.C4938a m17598a(List<Header> list) {
        C5015r.C5016a aVar = new C5015r.C5016a();
        int size = list.size();
        C5015r.C5016a aVar2 = aVar;
        StatusLine kVar = null;
        for (int i = 0; i < size; i++) {
            Header cVar = list.get(i);
            if (cVar != null) {
                ByteString fVar = cVar.f11488a;
                String o = cVar.f11489b.mo28107o();
                if (fVar.equals(Header.f11483e)) {
                    kVar = StatusLine.m17539a("HTTP/1.1 " + o);
                } else if (!f11523n.contains(fVar)) {
                    Internal.f11301a.mo27405a(aVar2, fVar.mo28107o(), o);
                }
            } else if (kVar != null && kVar.f11444b == 100) {
                aVar2 = new C5015r.C5016a();
                kVar = null;
            }
        }
        if (kVar != null) {
            Response.C4938a aVar3 = new Response.C4938a();
            aVar3.mo27338a(Protocol.HTTP_2);
            aVar3.mo27332a(kVar.f11444b);
            aVar3.mo27340a(kVar.f11445c);
            aVar3.mo27337a(aVar2.mo27747a());
            return aVar3;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    /* renamed from: a */
    public ResponseBody mo27484a(Response a0Var) {
        StreamAllocation gVar = this.f11525b;
        gVar.f11409f.mo27726e(gVar.f11408e);
        return new RealResponseBody(a0Var.mo27318b("Content-Type"), HttpHeaders.m17496a(a0Var), Okio.m18212a(new C4977a(this.f11527d.mo27574e())));
    }
}
