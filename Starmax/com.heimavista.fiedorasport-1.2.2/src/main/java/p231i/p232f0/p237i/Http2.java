package p231i.p232f0.p237i;

import java.io.IOException;
import p231i.p232f0.C4951c;
import p244j.ByteString;

/* renamed from: i.f0.i.e */
public final class Http2 {

    /* renamed from: a */
    static final ByteString f11510a = ByteString.m18170d("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    /* renamed from: b */
    private static final String[] f11511b = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

    /* renamed from: c */
    static final String[] f11512c = new String[64];

    /* renamed from: d */
    static final String[] f11513d = new String[256];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static {
        int i = 0;
        int i2 = 0;
        while (true) {
            String[] strArr = f11513d;
            if (i2 >= strArr.length) {
                break;
            }
            strArr[i2] = C4951c.m17345a("%8s", Integer.toBinaryString(i2)).replace(' ', '0');
            i2++;
        }
        String[] strArr2 = f11512c;
        strArr2[0] = "";
        strArr2[1] = "END_STREAM";
        int[] iArr = {1};
        strArr2[8] = "PADDED";
        for (int i3 : iArr) {
            f11512c[i3 | 8] = f11512c[i3] + "|PADDED";
        }
        String[] strArr3 = f11512c;
        strArr3[4] = "END_HEADERS";
        strArr3[32] = "PRIORITY";
        strArr3[36] = "END_HEADERS|PRIORITY";
        int[] iArr2 = {4, 32, 36};
        for (int i4 : iArr2) {
            for (int i5 : iArr) {
                int i6 = i5 | i4;
                f11512c[i6] = f11512c[i5] + '|' + f11512c[i4];
                f11512c[i6 | 8] = f11512c[i5] + '|' + f11512c[i4] + "|PADDED";
            }
        }
        while (true) {
            String[] strArr4 = f11512c;
            if (i < strArr4.length) {
                if (strArr4[i] == null) {
                    strArr4[i] = f11513d[i];
                }
                i++;
            } else {
                return;
            }
        }
    }

    private Http2() {
    }

    /* renamed from: a */
    static IllegalArgumentException m17594a(String str, Object... objArr) {
        throw new IllegalArgumentException(C4951c.m17345a(str, objArr));
    }

    /* renamed from: b */
    static IOException m17597b(String str, Object... objArr) {
        throw new IOException(C4951c.m17345a(str, objArr));
    }

    /* renamed from: a */
    static String m17596a(boolean z, int i, int i2, byte b, byte b2) {
        String[] strArr = f11511b;
        String a = b < strArr.length ? strArr[b] : C4951c.m17345a("0x%02x", Byte.valueOf(b));
        String a2 = m17595a(b, b2);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = a;
        objArr[4] = a2;
        return C4951c.m17345a("%s 0x%08x %5d %-13s %s", objArr);
    }

    /* renamed from: a */
    static String m17595a(byte b, byte b2) {
        if (b2 == 0) {
            return "";
        }
        if (!(b == 2 || b == 3)) {
            if (b == 4 || b == 6) {
                if (b2 == 1) {
                    return "ACK";
                }
                return f11513d[b2];
            } else if (!(b == 7 || b == 8)) {
                String[] strArr = f11512c;
                String str = b2 < strArr.length ? strArr[b2] : f11513d[b2];
                if (b != 5 || (b2 & 4) == 0) {
                    return (b != 0 || (b2 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED");
                }
                return str.replace("HEADERS", "PUSH_PROMISE");
            }
        }
        return f11513d[b2];
    }
}
