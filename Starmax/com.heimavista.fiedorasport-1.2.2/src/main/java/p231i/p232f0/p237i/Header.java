package p231i.p232f0.p237i;

import p231i.p232f0.C4951c;
import p244j.ByteString;

/* renamed from: i.f0.i.c */
public final class Header {

    /* renamed from: d */
    public static final ByteString f11482d = ByteString.m18170d(":");

    /* renamed from: e */
    public static final ByteString f11483e = ByteString.m18170d(":status");

    /* renamed from: f */
    public static final ByteString f11484f = ByteString.m18170d(":method");

    /* renamed from: g */
    public static final ByteString f11485g = ByteString.m18170d(":path");

    /* renamed from: h */
    public static final ByteString f11486h = ByteString.m18170d(":scheme");

    /* renamed from: i */
    public static final ByteString f11487i = ByteString.m18170d(":authority");

    /* renamed from: a */
    public final ByteString f11488a;

    /* renamed from: b */
    public final ByteString f11489b;

    /* renamed from: c */
    final int f11490c;

    public Header(String str, String str2) {
        this(ByteString.m18170d(str), ByteString.m18170d(str2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Header)) {
            return false;
        }
        Header cVar = (Header) obj;
        if (!this.f11488a.equals(cVar.f11488a) || !this.f11489b.equals(cVar.f11489b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((527 + this.f11488a.hashCode()) * 31) + this.f11489b.hashCode();
    }

    public String toString() {
        return C4951c.m17345a("%s: %s", this.f11488a.mo28107o(), this.f11489b.mo28107o());
    }

    public Header(ByteString fVar, String str) {
        this(fVar, ByteString.m18170d(str));
    }

    public Header(ByteString fVar, ByteString fVar2) {
        this.f11488a = fVar;
        this.f11489b = fVar2;
        this.f11490c = fVar.mo28104l() + 32 + fVar2.mo28104l();
    }
}
