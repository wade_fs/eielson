package p231i.p232f0.p237i;

/* renamed from: i.f0.i.b */
public enum ErrorCode {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8),
    COMPRESSION_ERROR(9),
    CONNECT_ERROR(10),
    ENHANCE_YOUR_CALM(11),
    INADEQUATE_SECURITY(12),
    HTTP_1_1_REQUIRED(13);
    

    /* renamed from: P */
    public final int f11481P;

    private ErrorCode(int i) {
        this.f11481P = i;
    }

    /* renamed from: a */
    public static ErrorCode m17566a(int i) {
        ErrorCode[] values = values();
        for (ErrorCode bVar : values) {
            if (bVar.f11481P == i) {
                return bVar;
            }
        }
        return null;
    }
}
