package p231i.p232f0.p237i;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p231i.p232f0.C4951c;
import p231i.p232f0.NamedRunnable;
import p231i.p232f0.p237i.Http2Reader;
import p231i.p232f0.p239k.Platform;
import p244j.Buffer;
import p244j.BufferedSink;
import p244j.BufferedSource;
import p244j.ByteString;

/* renamed from: i.f0.i.g */
public final class Http2Connection implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public static final ExecutorService f11531j0 = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), C4951c.m17350a("OkHttp Http2Connection", true));

    /* renamed from: P */
    final boolean f11532P;

    /* renamed from: Q */
    final C4985h f11533Q;

    /* renamed from: R */
    final Map<Integer, Http2Stream> f11534R = new LinkedHashMap();

    /* renamed from: S */
    final String f11535S;

    /* renamed from: T */
    int f11536T;

    /* renamed from: U */
    int f11537U;

    /* renamed from: V */
    boolean f11538V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public final ScheduledExecutorService f11539W;

    /* renamed from: X */
    private final ExecutorService f11540X;

    /* renamed from: Y */
    final PushObserver f11541Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public boolean f11542Z;

    /* renamed from: a0 */
    long f11543a0 = 0;

    /* renamed from: b0 */
    long f11544b0;

    /* renamed from: c0 */
    Settings f11545c0 = new Settings();

    /* renamed from: d0 */
    final Settings f11546d0 = new Settings();

    /* renamed from: e0 */
    boolean f11547e0 = false;

    /* renamed from: f0 */
    final Socket f11548f0;

    /* renamed from: g0 */
    final Http2Writer f11549g0;

    /* renamed from: h0 */
    final C4988j f11550h0;

    /* renamed from: i0 */
    final Set<Integer> f11551i0 = new LinkedHashSet();

    /* renamed from: i.f0.i.g$a */
    /* compiled from: Http2Connection */
    class C4978a extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11552Q;

        /* renamed from: R */
        final /* synthetic */ ErrorCode f11553R;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4978a(String str, Object[] objArr, int i, ErrorCode bVar) {
            super(str, objArr);
            this.f11552Q = i;
            this.f11553R = bVar;
        }

        /* renamed from: b */
        public void mo27409b() {
            try {
                Http2Connection.this.mo27539b(this.f11552Q, this.f11553R);
            } catch (IOException unused) {
                Http2Connection.this.m17613e();
            }
        }
    }

    /* renamed from: i.f0.i.g$b */
    /* compiled from: Http2Connection */
    class C4979b extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11555Q;

        /* renamed from: R */
        final /* synthetic */ long f11556R;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4979b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.f11555Q = i;
            this.f11556R = j;
        }

        /* renamed from: b */
        public void mo27409b() {
            try {
                Http2Connection.this.f11549g0.mo27590a(this.f11555Q, this.f11556R);
            } catch (IOException unused) {
                Http2Connection.this.m17613e();
            }
        }
    }

    /* renamed from: i.f0.i.g$c */
    /* compiled from: Http2Connection */
    class C4980c extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11558Q;

        /* renamed from: R */
        final /* synthetic */ List f11559R;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4980c(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.f11558Q = i;
            this.f11559R = list;
        }

        /* renamed from: b */
        public void mo27409b() {
            if (Http2Connection.this.f11541Y.mo27607a(this.f11558Q, this.f11559R)) {
                try {
                    Http2Connection.this.f11549g0.mo27591a(this.f11558Q, ErrorCode.CANCEL);
                    synchronized (Http2Connection.this) {
                        Http2Connection.this.f11551i0.remove(Integer.valueOf(this.f11558Q));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    /* renamed from: i.f0.i.g$d */
    /* compiled from: Http2Connection */
    class C4981d extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11561Q;

        /* renamed from: R */
        final /* synthetic */ List f11562R;

        /* renamed from: S */
        final /* synthetic */ boolean f11563S;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4981d(String str, Object[] objArr, int i, List list, boolean z) {
            super(str, objArr);
            this.f11561Q = i;
            this.f11562R = list;
            this.f11563S = z;
        }

        /* renamed from: b */
        public void mo27409b() {
            boolean a = Http2Connection.this.f11541Y.mo27608a(this.f11561Q, this.f11562R, this.f11563S);
            if (a) {
                try {
                    Http2Connection.this.f11549g0.mo27591a(this.f11561Q, ErrorCode.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.f11563S) {
                synchronized (Http2Connection.this) {
                    Http2Connection.this.f11551i0.remove(Integer.valueOf(this.f11561Q));
                }
            }
        }
    }

    /* renamed from: i.f0.i.g$e */
    /* compiled from: Http2Connection */
    class C4982e extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11565Q;

        /* renamed from: R */
        final /* synthetic */ Buffer f11566R;

        /* renamed from: S */
        final /* synthetic */ int f11567S;

        /* renamed from: T */
        final /* synthetic */ boolean f11568T;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4982e(String str, Object[] objArr, int i, Buffer cVar, int i2, boolean z) {
            super(str, objArr);
            this.f11565Q = i;
            this.f11566R = cVar;
            this.f11567S = i2;
            this.f11568T = z;
        }

        /* renamed from: b */
        public void mo27409b() {
            try {
                boolean a = Http2Connection.this.f11541Y.mo27606a(this.f11565Q, this.f11566R, this.f11567S, this.f11568T);
                if (a) {
                    Http2Connection.this.f11549g0.mo27591a(this.f11565Q, ErrorCode.CANCEL);
                }
                if (a || this.f11568T) {
                    synchronized (Http2Connection.this) {
                        Http2Connection.this.f11551i0.remove(Integer.valueOf(this.f11565Q));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    /* renamed from: i.f0.i.g$f */
    /* compiled from: Http2Connection */
    class C4983f extends NamedRunnable {

        /* renamed from: Q */
        final /* synthetic */ int f11570Q;

        /* renamed from: R */
        final /* synthetic */ ErrorCode f11571R;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4983f(String str, Object[] objArr, int i, ErrorCode bVar) {
            super(str, objArr);
            this.f11570Q = i;
            this.f11571R = bVar;
        }

        /* renamed from: b */
        public void mo27409b() {
            Http2Connection.this.f11541Y.mo27605a(this.f11570Q, this.f11571R);
            synchronized (Http2Connection.this) {
                Http2Connection.this.f11551i0.remove(Integer.valueOf(this.f11570Q));
            }
        }
    }

    /* renamed from: i.f0.i.g$h */
    /* compiled from: Http2Connection */
    public static abstract class C4985h {

        /* renamed from: a */
        public static final C4985h f11581a = new C4986a();

        /* renamed from: i.f0.i.g$h$a */
        /* compiled from: Http2Connection */
        class C4986a extends C4985h {
            C4986a() {
            }

            /* renamed from: a */
            public void mo27452a(Http2Stream iVar) {
                iVar.mo27566a(ErrorCode.REFUSED_STREAM);
            }
        }

        /* renamed from: a */
        public void mo27451a(Http2Connection gVar) {
        }

        /* renamed from: a */
        public abstract void mo27452a(Http2Stream iVar);
    }

    /* renamed from: i.f0.i.g$i */
    /* compiled from: Http2Connection */
    final class C4987i extends NamedRunnable {

        /* renamed from: Q */
        final boolean f11582Q;

        /* renamed from: R */
        final int f11583R;

        /* renamed from: S */
        final int f11584S;

        C4987i(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", Http2Connection.this.f11535S, Integer.valueOf(i), Integer.valueOf(i2));
            this.f11582Q = z;
            this.f11583R = i;
            this.f11584S = i2;
        }

        /* renamed from: b */
        public void mo27409b() {
            Http2Connection.this.mo27536a(this.f11582Q, this.f11583R, this.f11584S);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    static {
        Class<Http2Connection> cls = Http2Connection.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    Http2Connection(C4984g gVar) {
        C4984g gVar2 = gVar;
        this.f11541Y = gVar2.f11578f;
        boolean z = gVar2.f11579g;
        this.f11532P = z;
        this.f11533Q = gVar2.f11577e;
        this.f11537U = z ? 1 : 2;
        if (gVar2.f11579g) {
            this.f11537U += 2;
        }
        if (gVar2.f11579g) {
            this.f11545c0.mo27610a(7, 16777216);
        }
        this.f11535S = gVar2.f11574b;
        this.f11539W = new ScheduledThreadPoolExecutor(1, C4951c.m17350a(C4951c.m17345a("OkHttp %s Writer", this.f11535S), false));
        if (gVar2.f11580h != 0) {
            ScheduledExecutorService scheduledExecutorService = this.f11539W;
            C4987i iVar = new C4987i(false, 0, 0);
            int i = gVar2.f11580h;
            scheduledExecutorService.scheduleAtFixedRate(iVar, (long) i, (long) i, TimeUnit.MILLISECONDS);
        }
        this.f11540X = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), C4951c.m17350a(C4951c.m17345a("OkHttp %s Push Observer", this.f11535S), true));
        this.f11546d0.mo27610a(7, 65535);
        this.f11546d0.mo27610a(5, 16384);
        this.f11544b0 = (long) this.f11546d0.mo27615c();
        this.f11548f0 = gVar2.f11573a;
        this.f11549g0 = new Http2Writer(gVar2.f11576d, this.f11532P);
        this.f11550h0 = new C4988j(new Http2Reader(gVar2.f11575c, this.f11532P));
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m17613e() {
        try {
            mo27534a(ErrorCode.PROTOCOL_ERROR, ErrorCode.PROTOCOL_ERROR);
        } catch (IOException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo27540b(int i) {
        return i != 0 && (i & 1) == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public synchronized Http2Stream mo27541c(int i) {
        Http2Stream remove;
        remove = this.f11534R.remove(Integer.valueOf(i));
        notifyAll();
        return remove;
    }

    public void close() {
        mo27534a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    public void flush() {
        this.f11549g0.flush();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public void mo27546g(long j) {
        this.f11544b0 += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* renamed from: b */
    public synchronized int mo27538b() {
        return this.f11546d0.mo27614b(Integer.MAX_VALUE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private p231i.p232f0.p237i.Http2Stream m17610b(int r11, java.util.List<p231i.p232f0.p237i.Header> r12, boolean r13) {
        /*
            r10 = this;
            r6 = r13 ^ 1
            r4 = 0
            i.f0.i.j r7 = r10.f11549g0
            monitor-enter(r7)
            monitor-enter(r10)     // Catch:{ all -> 0x0078 }
            int r0 = r10.f11537U     // Catch:{ all -> 0x0075 }
            r1 = 1073741823(0x3fffffff, float:1.9999999)
            if (r0 <= r1) goto L_0x0013
            i.f0.i.b r0 = p231i.p232f0.p237i.ErrorCode.REFUSED_STREAM     // Catch:{ all -> 0x0075 }
            r10.mo27533a(r0)     // Catch:{ all -> 0x0075 }
        L_0x0013:
            boolean r0 = r10.f11538V     // Catch:{ all -> 0x0075 }
            if (r0 != 0) goto L_0x006f
            int r8 = r10.f11537U     // Catch:{ all -> 0x0075 }
            int r0 = r10.f11537U     // Catch:{ all -> 0x0075 }
            int r0 = r0 + 2
            r10.f11537U = r0     // Catch:{ all -> 0x0075 }
            i.f0.i.i r9 = new i.f0.i.i     // Catch:{ all -> 0x0075 }
            r0 = r9
            r1 = r8
            r2 = r10
            r3 = r6
            r5 = r12
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0075 }
            if (r13 == 0) goto L_0x003c
            long r0 = r10.f11544b0     // Catch:{ all -> 0x0075 }
            r2 = 0
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 == 0) goto L_0x003c
            long r0 = r9.f11605b     // Catch:{ all -> 0x0075 }
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 != 0) goto L_0x003a
            goto L_0x003c
        L_0x003a:
            r13 = 0
            goto L_0x003d
        L_0x003c:
            r13 = 1
        L_0x003d:
            boolean r0 = r9.mo27576g()     // Catch:{ all -> 0x0075 }
            if (r0 == 0) goto L_0x004c
            java.util.Map<java.lang.Integer, i.f0.i.i> r0 = r10.f11534R     // Catch:{ all -> 0x0075 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0075 }
            r0.put(r1, r9)     // Catch:{ all -> 0x0075 }
        L_0x004c:
            monitor-exit(r10)     // Catch:{ all -> 0x0075 }
            if (r11 != 0) goto L_0x0055
            i.f0.i.j r0 = r10.f11549g0     // Catch:{ all -> 0x0078 }
            r0.mo27595a(r6, r8, r11, r12)     // Catch:{ all -> 0x0078 }
            goto L_0x005e
        L_0x0055:
            boolean r0 = r10.f11532P     // Catch:{ all -> 0x0078 }
            if (r0 != 0) goto L_0x0067
            i.f0.i.j r0 = r10.f11549g0     // Catch:{ all -> 0x0078 }
            r0.mo27589a(r11, r8, r12)     // Catch:{ all -> 0x0078 }
        L_0x005e:
            monitor-exit(r7)     // Catch:{ all -> 0x0078 }
            if (r13 == 0) goto L_0x0066
            i.f0.i.j r11 = r10.f11549g0
            r11.flush()
        L_0x0066:
            return r9
        L_0x0067:
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0078 }
            java.lang.String r12 = "client streams shouldn't have associated stream IDs"
            r11.<init>(r12)     // Catch:{ all -> 0x0078 }
            throw r11     // Catch:{ all -> 0x0078 }
        L_0x006f:
            i.f0.i.a r11 = new i.f0.i.a     // Catch:{ all -> 0x0075 }
            r11.<init>()     // Catch:{ all -> 0x0075 }
            throw r11     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r11 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0075 }
            throw r11     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r11 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0078 }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Connection.m17610b(int, java.util.List, boolean):i.f0.i.i");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized Http2Stream mo27525a(int i) {
        return this.f11534R.get(Integer.valueOf(i));
    }

    /* renamed from: i.f0.i.g$g */
    /* compiled from: Http2Connection */
    public static class C4984g {

        /* renamed from: a */
        Socket f11573a;

        /* renamed from: b */
        String f11574b;

        /* renamed from: c */
        BufferedSource f11575c;

        /* renamed from: d */
        BufferedSink f11576d;

        /* renamed from: e */
        C4985h f11577e = C4985h.f11581a;

        /* renamed from: f */
        PushObserver f11578f = PushObserver.f11640a;

        /* renamed from: g */
        boolean f11579g;

        /* renamed from: h */
        int f11580h;

        public C4984g(boolean z) {
            this.f11579g = z;
        }

        /* renamed from: a */
        public C4984g mo27549a(Socket socket, String str, BufferedSource eVar, BufferedSink dVar) {
            this.f11573a = socket;
            this.f11574b = str;
            this.f11575c = eVar;
            this.f11576d = dVar;
            return this;
        }

        /* renamed from: a */
        public C4984g mo27548a(C4985h hVar) {
            this.f11577e = hVar;
            return this;
        }

        /* renamed from: a */
        public C4984g mo27547a(int i) {
            this.f11580h = i;
            return this;
        }

        /* renamed from: a */
        public Http2Connection mo27550a() {
            return new Http2Connection(this);
        }
    }

    /* renamed from: a */
    public Http2Stream mo27526a(List<Header> list, boolean z) {
        return m17610b(0, list, z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo27543c(int i, ErrorCode bVar) {
        try {
            this.f11539W.execute(new C4978a("OkHttp %s stream %d", new Object[]{this.f11535S, Integer.valueOf(i)}, i, bVar));
        } catch (RejectedExecutionException unused) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.f11544b0), r8.f11549g0.mo27598b());
        r6 = (long) r3;
        r8.f11544b0 -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        throw new java.io.InterruptedIOException();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo27532a(int r9, boolean r10, p244j.Buffer r11, long r12) {
        /*
            r8 = this;
            r0 = 0
            r1 = 0
            int r3 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x000d
            i.f0.i.j r12 = r8.f11549g0
            r12.mo27596a(r10, r9, r11, r0)
            return
        L_0x000d:
            int r3 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x0062
            monitor-enter(r8)
        L_0x0012:
            long r3 = r8.f11544b0     // Catch:{ InterruptedException -> 0x005a }
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 > 0) goto L_0x0030
            java.util.Map<java.lang.Integer, i.f0.i.i> r3 = r8.f11534R     // Catch:{ InterruptedException -> 0x005a }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r9)     // Catch:{ InterruptedException -> 0x005a }
            boolean r3 = r3.containsKey(r4)     // Catch:{ InterruptedException -> 0x005a }
            if (r3 == 0) goto L_0x0028
            r8.wait()     // Catch:{ InterruptedException -> 0x005a }
            goto L_0x0012
        L_0x0028:
            java.io.IOException r9 = new java.io.IOException     // Catch:{ InterruptedException -> 0x005a }
            java.lang.String r10 = "stream closed"
            r9.<init>(r10)     // Catch:{ InterruptedException -> 0x005a }
            throw r9     // Catch:{ InterruptedException -> 0x005a }
        L_0x0030:
            long r3 = r8.f11544b0     // Catch:{ all -> 0x0058 }
            long r3 = java.lang.Math.min(r12, r3)     // Catch:{ all -> 0x0058 }
            int r4 = (int) r3     // Catch:{ all -> 0x0058 }
            i.f0.i.j r3 = r8.f11549g0     // Catch:{ all -> 0x0058 }
            int r3 = r3.mo27598b()     // Catch:{ all -> 0x0058 }
            int r3 = java.lang.Math.min(r4, r3)     // Catch:{ all -> 0x0058 }
            long r4 = r8.f11544b0     // Catch:{ all -> 0x0058 }
            long r6 = (long) r3     // Catch:{ all -> 0x0058 }
            long r4 = r4 - r6
            r8.f11544b0 = r4     // Catch:{ all -> 0x0058 }
            monitor-exit(r8)     // Catch:{ all -> 0x0058 }
            long r12 = r12 - r6
            i.f0.i.j r4 = r8.f11549g0
            if (r10 == 0) goto L_0x0053
            int r5 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r5 != 0) goto L_0x0053
            r5 = 1
            goto L_0x0054
        L_0x0053:
            r5 = 0
        L_0x0054:
            r4.mo27596a(r5, r9, r11, r3)
            goto L_0x000d
        L_0x0058:
            r9 = move-exception
            goto L_0x0060
        L_0x005a:
            java.io.InterruptedIOException r9 = new java.io.InterruptedIOException     // Catch:{ all -> 0x0058 }
            r9.<init>()     // Catch:{ all -> 0x0058 }
            throw r9     // Catch:{ all -> 0x0058 }
        L_0x0060:
            monitor-exit(r8)     // Catch:{ all -> 0x0058 }
            throw r9
        L_0x0062:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Connection.mo27532a(int, boolean, j.c, long):void");
    }

    /* renamed from: c */
    public void mo27542c() {
        mo27535a(true);
    }

    /* renamed from: i.f0.i.g$j */
    /* compiled from: Http2Connection */
    class C4988j extends NamedRunnable implements Http2Reader.C4993b {

        /* renamed from: Q */
        final Http2Reader f11586Q;

        /* renamed from: i.f0.i.g$j$a */
        /* compiled from: Http2Connection */
        class C4989a extends NamedRunnable {

            /* renamed from: Q */
            final /* synthetic */ Http2Stream f11588Q;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C4989a(String str, Object[] objArr, Http2Stream iVar) {
                super(str, objArr);
                this.f11588Q = iVar;
            }

            /* renamed from: b */
            public void mo27409b() {
                try {
                    Http2Connection.this.f11533Q.mo27452a(this.f11588Q);
                } catch (IOException e) {
                    Platform d = Platform.m17817d();
                    d.mo27629a(4, "Http2Connection.Listener failure for " + Http2Connection.this.f11535S, e);
                    try {
                        this.f11588Q.mo27566a(ErrorCode.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        /* renamed from: i.f0.i.g$j$b */
        /* compiled from: Http2Connection */
        class C4990b extends NamedRunnable {
            C4990b(String str, Object... objArr) {
                super(str, objArr);
            }

            /* renamed from: b */
            public void mo27409b() {
                Http2Connection gVar = Http2Connection.this;
                gVar.f11533Q.mo27451a(gVar);
            }
        }

        /* renamed from: i.f0.i.g$j$c */
        /* compiled from: Http2Connection */
        class C4991c extends NamedRunnable {

            /* renamed from: Q */
            final /* synthetic */ Settings f11591Q;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C4991c(String str, Object[] objArr, Settings mVar) {
                super(str, objArr);
                this.f11591Q = mVar;
            }

            /* renamed from: b */
            public void mo27409b() {
                try {
                    Http2Connection.this.f11549g0.mo27593a(this.f11591Q);
                } catch (IOException unused) {
                    Http2Connection.this.m17613e();
                }
            }
        }

        C4988j(Http2Reader hVar) {
            super("OkHttp %s", Http2Connection.this.f11535S);
            this.f11586Q = hVar;
        }

        /* renamed from: a */
        public void mo27551a() {
        }

        /* renamed from: a */
        public void mo27552a(int i, int i2, int i3, boolean z) {
        }

        /* renamed from: a */
        public void mo27559a(boolean z, int i, BufferedSource eVar, int i2) {
            if (Http2Connection.this.mo27540b(i)) {
                Http2Connection.this.mo27529a(i, eVar, i2, z);
                return;
            }
            Http2Stream a = Http2Connection.this.mo27525a(i);
            if (a == null) {
                Http2Connection.this.mo27543c(i, ErrorCode.PROTOCOL_ERROR);
                eVar.skip((long) i2);
                return;
            }
            a.mo27567a(eVar, i2);
            if (z) {
                a.mo27578i();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.i.h.a(boolean, i.f0.i.h$b):boolean
         arg types: [int, i.f0.i.g$j]
         candidates:
          i.f0.i.h.a(i.f0.i.h$b, int):void
          i.f0.i.h.a(boolean, i.f0.i.h$b):boolean */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1 = p231i.p232f0.p237i.ErrorCode.PROTOCOL_ERROR;
            r0 = p231i.p232f0.p237i.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r2 = r4.f11587R;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
            r2 = th;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo27409b() {
            /*
                r4 = this;
                i.f0.i.b r0 = p231i.p232f0.p237i.ErrorCode.INTERNAL_ERROR
                i.f0.i.h r1 = r4.f11586Q     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                r1.mo27561a(r4)     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
            L_0x0007:
                i.f0.i.h r1 = r4.f11586Q     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                r2 = 0
                boolean r1 = r1.mo27562a(r2, r4)     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                if (r1 == 0) goto L_0x0011
                goto L_0x0007
            L_0x0011:
                i.f0.i.b r1 = p231i.p232f0.p237i.ErrorCode.NO_ERROR     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                i.f0.i.b r0 = p231i.p232f0.p237i.ErrorCode.CANCEL     // Catch:{ IOException -> 0x001c }
                i.f0.i.g r2 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ IOException -> 0x0025 }
                goto L_0x0022
            L_0x0018:
                r2 = move-exception
                r1 = r0
                goto L_0x002c
            L_0x001b:
                r1 = r0
            L_0x001c:
                i.f0.i.b r1 = p231i.p232f0.p237i.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x002b }
                i.f0.i.b r0 = p231i.p232f0.p237i.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x002b }
                i.f0.i.g r2 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ IOException -> 0x0025 }
            L_0x0022:
                r2.mo27534a(r1, r0)     // Catch:{ IOException -> 0x0025 }
            L_0x0025:
                i.f0.i.h r0 = r4.f11586Q
                p231i.p232f0.C4951c.m17352a(r0)
                return
            L_0x002b:
                r2 = move-exception
            L_0x002c:
                i.f0.i.g r3 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ IOException -> 0x0031 }
                r3.mo27534a(r1, r0)     // Catch:{ IOException -> 0x0031 }
            L_0x0031:
                i.f0.i.h r0 = r4.f11586Q
                p231i.p232f0.C4951c.m17352a(r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Connection.C4988j.mo27409b():void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0071, code lost:
            r0.mo27568a(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0074, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
            r0.mo27578i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo27558a(boolean r10, int r11, int r12, java.util.List<p231i.p232f0.p237i.Header> r13) {
            /*
                r9 = this;
                i.f0.i.g r12 = p231i.p232f0.p237i.Http2Connection.this
                boolean r12 = r12.mo27540b(r11)
                if (r12 == 0) goto L_0x000e
                i.f0.i.g r12 = p231i.p232f0.p237i.Http2Connection.this
                r12.mo27531a(r11, r13, r10)
                return
            L_0x000e:
                i.f0.i.g r12 = p231i.p232f0.p237i.Http2Connection.this
                monitor-enter(r12)
                i.f0.i.g r0 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                i.f0.i.i r0 = r0.mo27525a(r11)     // Catch:{ all -> 0x007a }
                if (r0 != 0) goto L_0x0070
                i.f0.i.g r0 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                boolean r0 = r0.f11538V     // Catch:{ all -> 0x007a }
                if (r0 == 0) goto L_0x0021
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                return
            L_0x0021:
                i.f0.i.g r0 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                int r0 = r0.f11536T     // Catch:{ all -> 0x007a }
                if (r11 > r0) goto L_0x0029
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                return
            L_0x0029:
                int r0 = r11 % 2
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                int r1 = r1.f11537U     // Catch:{ all -> 0x007a }
                r2 = 2
                int r1 = r1 % r2
                if (r0 != r1) goto L_0x0035
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                return
            L_0x0035:
                i.f0.i.i r0 = new i.f0.i.i     // Catch:{ all -> 0x007a }
                i.f0.i.g r5 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                r6 = 0
                r3 = r0
                r4 = r11
                r7 = r10
                r8 = r13
                r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x007a }
                i.f0.i.g r10 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                r10.f11536T = r11     // Catch:{ all -> 0x007a }
                i.f0.i.g r10 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                java.util.Map<java.lang.Integer, i.f0.i.i> r10 = r10.f11534R     // Catch:{ all -> 0x007a }
                java.lang.Integer r13 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x007a }
                r10.put(r13, r0)     // Catch:{ all -> 0x007a }
                java.util.concurrent.ExecutorService r10 = p231i.p232f0.p237i.Http2Connection.f11531j0     // Catch:{ all -> 0x007a }
                i.f0.i.g$j$a r13 = new i.f0.i.g$j$a     // Catch:{ all -> 0x007a }
                java.lang.String r1 = "OkHttp %s stream %d"
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007a }
                r3 = 0
                i.f0.i.g r4 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x007a }
                java.lang.String r4 = r4.f11535S     // Catch:{ all -> 0x007a }
                r2[r3] = r4     // Catch:{ all -> 0x007a }
                r3 = 1
                java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x007a }
                r2[r3] = r11     // Catch:{ all -> 0x007a }
                r13.<init>(r1, r2, r0)     // Catch:{ all -> 0x007a }
                r10.execute(r13)     // Catch:{ all -> 0x007a }
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                return
            L_0x0070:
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                r0.mo27568a(r13)
                if (r10 == 0) goto L_0x0079
                r0.mo27578i()
            L_0x0079:
                return
            L_0x007a:
                r10 = move-exception
                monitor-exit(r12)     // Catch:{ all -> 0x007a }
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Connection.C4988j.mo27558a(boolean, int, int, java.util.List):void");
        }

        /* renamed from: a */
        public void mo27555a(int i, ErrorCode bVar) {
            if (Http2Connection.this.mo27540b(i)) {
                Http2Connection.this.mo27528a(i, bVar);
                return;
            }
            Http2Stream c = Http2Connection.this.mo27541c(i);
            if (c != null) {
                c.mo27572c(bVar);
            }
        }

        /* JADX WARN: Type inference failed for: r1v13, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo27560a(boolean r11, p231i.p232f0.p237i.Settings r12) {
            /*
                r10 = this;
                i.f0.i.g r0 = p231i.p232f0.p237i.Http2Connection.this
                monitor-enter(r0)
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                i.f0.i.m r1 = r1.f11546d0     // Catch:{ all -> 0x0094 }
                int r1 = r1.mo27615c()     // Catch:{ all -> 0x0094 }
                if (r11 == 0) goto L_0x0014
                i.f0.i.g r11 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                i.f0.i.m r11 = r11.f11546d0     // Catch:{ all -> 0x0094 }
                r11.mo27611a()     // Catch:{ all -> 0x0094 }
            L_0x0014:
                i.f0.i.g r11 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                i.f0.i.m r11 = r11.f11546d0     // Catch:{ all -> 0x0094 }
                r11.mo27612a(r12)     // Catch:{ all -> 0x0094 }
                r10.m17648a(r12)     // Catch:{ all -> 0x0094 }
                i.f0.i.g r11 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                i.f0.i.m r11 = r11.f11546d0     // Catch:{ all -> 0x0094 }
                int r11 = r11.mo27615c()     // Catch:{ all -> 0x0094 }
                r12 = -1
                r2 = 0
                r4 = 1
                r5 = 0
                if (r11 == r12) goto L_0x0064
                if (r11 == r1) goto L_0x0064
                int r11 = r11 - r1
                long r11 = (long) r11     // Catch:{ all -> 0x0094 }
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                boolean r1 = r1.f11547e0     // Catch:{ all -> 0x0094 }
                if (r1 != 0) goto L_0x0040
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                r1.mo27546g(r11)     // Catch:{ all -> 0x0094 }
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                r1.f11547e0 = r4     // Catch:{ all -> 0x0094 }
            L_0x0040:
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                java.util.Map<java.lang.Integer, i.f0.i.i> r1 = r1.f11534R     // Catch:{ all -> 0x0094 }
                boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0094 }
                if (r1 != 0) goto L_0x0065
                i.f0.i.g r1 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                java.util.Map<java.lang.Integer, i.f0.i.i> r1 = r1.f11534R     // Catch:{ all -> 0x0094 }
                java.util.Collection r1 = r1.values()     // Catch:{ all -> 0x0094 }
                i.f0.i.g r5 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                java.util.Map<java.lang.Integer, i.f0.i.i> r5 = r5.f11534R     // Catch:{ all -> 0x0094 }
                int r5 = r5.size()     // Catch:{ all -> 0x0094 }
                i.f0.i.i[] r5 = new p231i.p232f0.p237i.Http2Stream[r5]     // Catch:{ all -> 0x0094 }
                java.lang.Object[] r1 = r1.toArray(r5)     // Catch:{ all -> 0x0094 }
                r5 = r1
                i.f0.i.i[] r5 = (p231i.p232f0.p237i.Http2Stream[]) r5     // Catch:{ all -> 0x0094 }
                goto L_0x0065
            L_0x0064:
                r11 = r2
            L_0x0065:
                java.util.concurrent.ExecutorService r1 = p231i.p232f0.p237i.Http2Connection.f11531j0     // Catch:{ all -> 0x0094 }
                i.f0.i.g$j$b r6 = new i.f0.i.g$j$b     // Catch:{ all -> 0x0094 }
                java.lang.String r7 = "OkHttp %s settings"
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0094 }
                i.f0.i.g r8 = p231i.p232f0.p237i.Http2Connection.this     // Catch:{ all -> 0x0094 }
                java.lang.String r8 = r8.f11535S     // Catch:{ all -> 0x0094 }
                r9 = 0
                r4[r9] = r8     // Catch:{ all -> 0x0094 }
                r6.<init>(r7, r4)     // Catch:{ all -> 0x0094 }
                r1.execute(r6)     // Catch:{ all -> 0x0094 }
                monitor-exit(r0)     // Catch:{ all -> 0x0094 }
                if (r5 == 0) goto L_0x0093
                int r0 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
                if (r0 == 0) goto L_0x0093
                int r0 = r5.length
            L_0x0084:
                if (r9 >= r0) goto L_0x0093
                r1 = r5[r9]
                monitor-enter(r1)
                r1.mo27565a(r11)     // Catch:{ all -> 0x0090 }
                monitor-exit(r1)     // Catch:{ all -> 0x0090 }
                int r9 = r9 + 1
                goto L_0x0084
            L_0x0090:
                r11 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0090 }
                throw r11
            L_0x0093:
                return
            L_0x0094:
                r11 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0094 }
                throw r11
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p237i.Http2Connection.C4988j.mo27560a(boolean, i.f0.i.m):void");
        }

        /* renamed from: a */
        private void m17648a(Settings mVar) {
            try {
                Http2Connection.this.f11539W.execute(new C4991c("OkHttp %s ACK Settings", new Object[]{Http2Connection.this.f11535S}, mVar));
            } catch (RejectedExecutionException unused) {
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.i.g.a(i.f0.i.g, boolean):boolean
         arg types: [i.f0.i.g, int]
         candidates:
          i.f0.i.g.a(java.util.List<i.f0.i.c>, boolean):i.f0.i.i
          i.f0.i.g.a(int, long):void
          i.f0.i.g.a(int, i.f0.i.b):void
          i.f0.i.g.a(int, java.util.List<i.f0.i.c>):void
          i.f0.i.g.a(i.f0.i.b, i.f0.i.b):void
          i.f0.i.g.a(i.f0.i.g, boolean):boolean */
        /* renamed from: a */
        public void mo27557a(boolean z, int i, int i2) {
            if (z) {
                synchronized (Http2Connection.this) {
                    boolean unused = Http2Connection.this.f11542Z = false;
                    Http2Connection.this.notifyAll();
                }
                return;
            }
            try {
                Http2Connection.this.f11539W.execute(new C4987i(true, i, i2));
            } catch (RejectedExecutionException unused2) {
            }
        }

        /* renamed from: a */
        public void mo27556a(int i, ErrorCode bVar, ByteString fVar) {
            Http2Stream[] iVarArr;
            fVar.mo28104l();
            synchronized (Http2Connection.this) {
                iVarArr = (Http2Stream[]) Http2Connection.this.f11534R.values().toArray(new Http2Stream[Http2Connection.this.f11534R.size()]);
                Http2Connection.this.f11538V = true;
            }
            for (Http2Stream iVar : iVarArr) {
                if (iVar.mo27571c() > i && iVar.mo27575f()) {
                    iVar.mo27572c(ErrorCode.REFUSED_STREAM);
                    Http2Connection.this.mo27541c(iVar.mo27571c());
                }
            }
        }

        /* renamed from: a */
        public void mo27554a(int i, long j) {
            if (i == 0) {
                synchronized (Http2Connection.this) {
                    Http2Connection.this.f11544b0 += j;
                    Http2Connection.this.notifyAll();
                }
                return;
            }
            Http2Stream a = Http2Connection.this.mo27525a(i);
            if (a != null) {
                synchronized (a) {
                    a.mo27565a(j);
                }
            }
        }

        /* renamed from: a */
        public void mo27553a(int i, int i2, List<Header> list) {
            Http2Connection.this.mo27530a(i2, list);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27527a(int i, long j) {
        try {
            this.f11539W.execute(new C4979b("OkHttp Window Update %s stream %d", new Object[]{this.f11535S, Integer.valueOf(i)}, i, j));
        } catch (RejectedExecutionException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27536a(boolean z, int i, int i2) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.f11542Z;
                this.f11542Z = true;
            }
            if (z2) {
                m17613e();
                return;
            }
        }
        try {
            this.f11549g0.mo27594a(z, i, i2);
        } catch (IOException unused) {
            m17613e();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27539b(int i, ErrorCode bVar) {
        this.f11549g0.mo27591a(i, bVar);
    }

    /* renamed from: a */
    public void mo27533a(ErrorCode bVar) {
        synchronized (this.f11549g0) {
            synchronized (this) {
                if (!this.f11538V) {
                    this.f11538V = true;
                    int i = this.f11536T;
                    this.f11549g0.mo27592a(i, bVar, C4951c.f11303a);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27534a(ErrorCode bVar, ErrorCode bVar2) {
        Http2Stream[] iVarArr = null;
        try {
            mo27533a(bVar);
            e = null;
        } catch (IOException e) {
            e = e;
        }
        synchronized (this) {
            if (!this.f11534R.isEmpty()) {
                iVarArr = (Http2Stream[]) this.f11534R.values().toArray(new Http2Stream[this.f11534R.size()]);
                this.f11534R.clear();
            }
        }
        if (iVarArr != null) {
            for (Http2Stream iVar : iVarArr) {
                try {
                    iVar.mo27566a(bVar2);
                } catch (IOException e2) {
                    if (e != null) {
                        e = e2;
                    }
                }
            }
        }
        try {
            this.f11549g0.close();
        } catch (IOException e3) {
            if (e == null) {
                e = e3;
            }
        }
        try {
            this.f11548f0.close();
        } catch (IOException e4) {
            e = e4;
        }
        this.f11539W.shutdown();
        this.f11540X.shutdown();
        if (e != null) {
            throw e;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27535a(boolean z) {
        if (z) {
            this.f11549g0.mo27586a();
            this.f11549g0.mo27599b(this.f11545c0);
            int c = this.f11545c0.mo27615c();
            if (c != 65535) {
                this.f11549g0.mo27590a(0, (long) (c - 65535));
            }
        }
        new Thread(this.f11550h0).start();
    }

    /* renamed from: a */
    public synchronized boolean mo27537a() {
        return this.f11538V;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27530a(int i, List<Header> list) {
        synchronized (this) {
            if (this.f11551i0.contains(Integer.valueOf(i))) {
                mo27543c(i, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.f11551i0.add(Integer.valueOf(i));
            try {
                this.f11540X.execute(new C4980c("OkHttp %s Push Request[%s]", new Object[]{this.f11535S, Integer.valueOf(i)}, i, list));
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27531a(int i, List<Header> list, boolean z) {
        try {
            this.f11540X.execute(new C4981d("OkHttp %s Push Headers[%s]", new Object[]{this.f11535S, Integer.valueOf(i)}, i, list, z));
        } catch (RejectedExecutionException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27529a(int i, BufferedSource eVar, int i2, boolean z) {
        Buffer cVar = new Buffer();
        long j = (long) i2;
        eVar.mo28052e(j);
        eVar.mo27415b(cVar, j);
        if (cVar.mo28051e() == j) {
            this.f11540X.execute(new C4982e("OkHttp %s Push Data[%s]", new Object[]{this.f11535S, Integer.valueOf(i)}, i, cVar, i2, z));
            return;
        }
        throw new IOException(cVar.mo28051e() + " != " + i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27528a(int i, ErrorCode bVar) {
        this.f11540X.execute(new C4983f("OkHttp %s Push Reset[%s]", new Object[]{this.f11535S, Integer.valueOf(i)}, i, bVar));
    }
}
