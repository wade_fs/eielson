package p231i.p232f0.p237i;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import p231i.p232f0.C4951c;
import p244j.Buffer;
import p244j.BufferedSource;
import p244j.ByteString;
import p244j.C5049s;
import p244j.Okio;

/* renamed from: i.f0.i.d */
final class Hpack {

    /* renamed from: a */
    static final Header[] f11491a = {new Header(Header.f11487i, ""), new Header(Header.f11484f, "GET"), new Header(Header.f11484f, "POST"), new Header(Header.f11485g, "/"), new Header(Header.f11485g, "/index.html"), new Header(Header.f11486h, "http"), new Header(Header.f11486h, "https"), new Header(Header.f11483e, "200"), new Header(Header.f11483e, "204"), new Header(Header.f11483e, "206"), new Header(Header.f11483e, "304"), new Header(Header.f11483e, "400"), new Header(Header.f11483e, "404"), new Header(Header.f11483e, "500"), new Header("accept-charset", ""), new Header("accept-encoding", "gzip, deflate"), new Header("accept-language", ""), new Header("accept-ranges", ""), new Header("accept", ""), new Header("access-control-allow-origin", ""), new Header("age", ""), new Header("allow", ""), new Header("authorization", ""), new Header("cache-control", ""), new Header("content-disposition", ""), new Header("content-encoding", ""), new Header("content-language", ""), new Header("content-length", ""), new Header("content-location", ""), new Header("content-range", ""), new Header("content-type", ""), new Header("cookie", ""), new Header("date", ""), new Header("etag", ""), new Header("expect", ""), new Header("expires", ""), new Header("from", ""), new Header("host", ""), new Header("if-match", ""), new Header("if-modified-since", ""), new Header("if-none-match", ""), new Header("if-range", ""), new Header("if-unmodified-since", ""), new Header("last-modified", ""), new Header("link", ""), new Header("location", ""), new Header("max-forwards", ""), new Header("proxy-authenticate", ""), new Header("proxy-authorization", ""), new Header("range", ""), new Header("referer", ""), new Header("refresh", ""), new Header("retry-after", ""), new Header("server", ""), new Header("set-cookie", ""), new Header("strict-transport-security", ""), new Header("transfer-encoding", ""), new Header("user-agent", ""), new Header("vary", ""), new Header("via", ""), new Header("www-authenticate", "")};

    /* renamed from: b */
    static final Map<ByteString, Integer> f11492b = m17568a();

    /* renamed from: i.f0.i.d$a */
    /* compiled from: Hpack */
    static final class C4975a {

        /* renamed from: a */
        private final List<Header> f11493a;

        /* renamed from: b */
        private final BufferedSource f11494b;

        /* renamed from: c */
        private final int f11495c;

        /* renamed from: d */
        private int f11496d;

        /* renamed from: e */
        Header[] f11497e;

        /* renamed from: f */
        int f11498f;

        /* renamed from: g */
        int f11499g;

        /* renamed from: h */
        int f11500h;

        C4975a(int i, C5049s sVar) {
            this(i, i, sVar);
        }

        /* renamed from: b */
        private int m17571b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f11497e.length;
                while (true) {
                    length--;
                    if (length < this.f11498f || i <= 0) {
                        Header[] cVarArr = this.f11497e;
                        int i3 = this.f11498f;
                        System.arraycopy(cVarArr, i3 + 1, cVarArr, i3 + 1 + i2, this.f11499g);
                        this.f11498f += i2;
                    } else {
                        Header[] cVarArr2 = this.f11497e;
                        i -= cVarArr2[length].f11490c;
                        this.f11500h -= cVarArr2[length].f11490c;
                        this.f11499g--;
                        i2++;
                    }
                }
                Header[] cVarArr3 = this.f11497e;
                int i32 = this.f11498f;
                System.arraycopy(cVarArr3, i32 + 1, cVarArr3, i32 + 1 + i2, this.f11499g);
                this.f11498f += i2;
            }
            return i2;
        }

        /* renamed from: d */
        private void m17573d() {
            int i = this.f11496d;
            int i2 = this.f11500h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                m17575e();
            } else {
                m17571b(i2 - i);
            }
        }

        /* renamed from: e */
        private void m17575e() {
            Arrays.fill(this.f11497e, (Object) null);
            this.f11498f = this.f11497e.length - 1;
            this.f11499g = 0;
            this.f11500h = 0;
        }

        /* renamed from: f */
        private void m17578f(int i) {
            m17570a(-1, new Header(m17572c(i), mo27519b()));
        }

        /* renamed from: g */
        private void m17580g(int i) {
            this.f11493a.add(new Header(m17572c(i), mo27519b()));
        }

        /* renamed from: h */
        private void m17581h() {
            ByteString b = mo27519b();
            Hpack.m17567a(b);
            this.f11493a.add(new Header(b, mo27519b()));
        }

        /* renamed from: a */
        public List<Header> mo27518a() {
            ArrayList arrayList = new ArrayList(this.f11493a);
            this.f11493a.clear();
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public void mo27520c() {
            while (!this.f11494b.mo28063j()) {
                byte readByte = this.f11494b.readByte() & 255;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    m17576e(mo27517a(readByte, 127) - 1);
                } else if (readByte == 64) {
                    m17579g();
                } else if ((readByte & 64) == 64) {
                    m17578f(mo27517a(readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    this.f11496d = mo27517a(readByte, 31);
                    int i = this.f11496d;
                    if (i < 0 || i > this.f11495c) {
                        throw new IOException("Invalid dynamic table size update " + this.f11496d);
                    }
                    m17573d();
                } else if (readByte == 16 || readByte == 0) {
                    m17581h();
                } else {
                    m17580g(mo27517a(readByte, 15) - 1);
                }
            }
        }

        C4975a(int i, int i2, C5049s sVar) {
            this.f11493a = new ArrayList();
            this.f11497e = new Header[8];
            this.f11498f = this.f11497e.length - 1;
            this.f11499g = 0;
            this.f11500h = 0;
            this.f11495c = i;
            this.f11496d = i2;
            this.f11494b = Okio.m18212a(sVar);
        }

        /* renamed from: a */
        private int m17569a(int i) {
            return this.f11498f + 1 + i;
        }

        /* renamed from: a */
        private void m17570a(int i, Header cVar) {
            this.f11493a.add(cVar);
            int i2 = cVar.f11490c;
            if (i != -1) {
                i2 -= this.f11497e[m17569a(i)].f11490c;
            }
            int i3 = this.f11496d;
            if (i2 > i3) {
                m17575e();
                return;
            }
            int b = m17571b((this.f11500h + i2) - i3);
            if (i == -1) {
                int i4 = this.f11499g + 1;
                Header[] cVarArr = this.f11497e;
                if (i4 > cVarArr.length) {
                    Header[] cVarArr2 = new Header[(cVarArr.length * 2)];
                    System.arraycopy(cVarArr, 0, cVarArr2, cVarArr.length, cVarArr.length);
                    this.f11498f = this.f11497e.length - 1;
                    this.f11497e = cVarArr2;
                }
                int i5 = this.f11498f;
                this.f11498f = i5 - 1;
                this.f11497e[i5] = cVar;
                this.f11499g++;
            } else {
                this.f11497e[i + m17569a(i) + b] = cVar;
            }
            this.f11500h += i2;
        }

        /* renamed from: d */
        private boolean m17574d(int i) {
            return i >= 0 && i <= Hpack.f11491a.length - 1;
        }

        /* renamed from: f */
        private int m17577f() {
            return this.f11494b.readByte() & 255;
        }

        /* renamed from: g */
        private void m17579g() {
            ByteString b = mo27519b();
            Hpack.m17567a(b);
            m17570a(-1, new Header(b, mo27519b()));
        }

        /* renamed from: e */
        private void m17576e(int i) {
            if (m17574d(i)) {
                this.f11493a.add(Hpack.f11491a[i]);
                return;
            }
            int a = m17569a(i - Hpack.f11491a.length);
            if (a >= 0) {
                Header[] cVarArr = this.f11497e;
                if (a < cVarArr.length) {
                    this.f11493a.add(cVarArr[a]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public ByteString mo27519b() {
            int f = m17577f();
            boolean z = (f & 128) == 128;
            int a = mo27517a(f, 127);
            if (z) {
                return ByteString.m18167a(Huffman.m17739b().mo27604a(this.f11494b.mo28050d((long) a)));
            }
            return this.f11494b.mo28037a((long) a);
        }

        /* renamed from: c */
        private ByteString m17572c(int i) {
            if (m17574d(i)) {
                return Hpack.f11491a[i].f11488a;
            }
            int a = m17569a(i - Hpack.f11491a.length);
            if (a >= 0) {
                Header[] cVarArr = this.f11497e;
                if (a < cVarArr.length) {
                    return cVarArr[a].f11488a;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public int mo27517a(int i, int i2) {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int f = m17577f();
                if ((f & 128) == 0) {
                    return i2 + (f << i4);
                }
                i2 += (f & 127) << i4;
                i4 += 7;
            }
        }
    }

    /* renamed from: i.f0.i.d$b */
    /* compiled from: Hpack */
    static final class C4976b {

        /* renamed from: a */
        private final Buffer f11501a;

        /* renamed from: b */
        private final boolean f11502b;

        /* renamed from: c */
        private int f11503c;

        /* renamed from: d */
        private boolean f11504d;

        /* renamed from: e */
        int f11505e;

        /* renamed from: f */
        Header[] f11506f;

        /* renamed from: g */
        int f11507g;

        /* renamed from: h */
        int f11508h;

        /* renamed from: i */
        int f11509i;

        C4976b(Buffer cVar) {
            this(4096, true, cVar);
        }

        /* renamed from: a */
        private void m17587a(Header cVar) {
            int i = cVar.f11490c;
            int i2 = this.f11505e;
            if (i > i2) {
                m17589b();
                return;
            }
            m17588b((this.f11509i + i) - i2);
            int i3 = this.f11508h + 1;
            Header[] cVarArr = this.f11506f;
            if (i3 > cVarArr.length) {
                Header[] cVarArr2 = new Header[(cVarArr.length * 2)];
                System.arraycopy(cVarArr, 0, cVarArr2, cVarArr.length, cVarArr.length);
                this.f11507g = this.f11506f.length - 1;
                this.f11506f = cVarArr2;
            }
            int i4 = this.f11507g;
            this.f11507g = i4 - 1;
            this.f11506f[i4] = cVar;
            this.f11508h++;
            this.f11509i += i;
        }

        /* renamed from: b */
        private void m17589b() {
            Arrays.fill(this.f11506f, (Object) null);
            this.f11507g = this.f11506f.length - 1;
            this.f11508h = 0;
            this.f11509i = 0;
        }

        C4976b(int i, boolean z, Buffer cVar) {
            this.f11503c = Integer.MAX_VALUE;
            this.f11506f = new Header[8];
            this.f11507g = this.f11506f.length - 1;
            this.f11508h = 0;
            this.f11509i = 0;
            this.f11505e = i;
            this.f11502b = z;
            this.f11501a = cVar;
        }

        /* renamed from: b */
        private int m17588b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f11506f.length;
                while (true) {
                    length--;
                    if (length < this.f11507g || i <= 0) {
                        Header[] cVarArr = this.f11506f;
                        int i3 = this.f11507g;
                        System.arraycopy(cVarArr, i3 + 1, cVarArr, i3 + 1 + i2, this.f11508h);
                        Header[] cVarArr2 = this.f11506f;
                        int i4 = this.f11507g;
                        Arrays.fill(cVarArr2, i4 + 1, i4 + 1 + i2, (Object) null);
                        this.f11507g += i2;
                    } else {
                        Header[] cVarArr3 = this.f11506f;
                        i -= cVarArr3[length].f11490c;
                        this.f11509i -= cVarArr3[length].f11490c;
                        this.f11508h--;
                        i2++;
                    }
                }
                Header[] cVarArr4 = this.f11506f;
                int i32 = this.f11507g;
                System.arraycopy(cVarArr4, i32 + 1, cVarArr4, i32 + 1 + i2, this.f11508h);
                Header[] cVarArr22 = this.f11506f;
                int i42 = this.f11507g;
                Arrays.fill(cVarArr22, i42 + 1, i42 + 1 + i2, (Object) null);
                this.f11507g += i2;
            }
            return i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27524a(List<Header> list) {
            int i;
            int i2;
            if (this.f11504d) {
                int i3 = this.f11503c;
                if (i3 < this.f11505e) {
                    mo27522a(i3, 31, 32);
                }
                this.f11504d = false;
                this.f11503c = Integer.MAX_VALUE;
                mo27522a(this.f11505e, 31, 32);
            }
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                Header cVar = list.get(i4);
                ByteString m = cVar.f11488a.mo28105m();
                ByteString fVar = cVar.f11489b;
                Integer num = Hpack.f11492b.get(m);
                if (num != null) {
                    i2 = num.intValue() + 1;
                    if (i2 > 1 && i2 < 8) {
                        if (C4951c.m17356a(Hpack.f11491a[i2 - 1].f11489b, fVar)) {
                            i = i2;
                        } else if (C4951c.m17356a(Hpack.f11491a[i2].f11489b, fVar)) {
                            i = i2;
                            i2++;
                        }
                    }
                    i = i2;
                    i2 = -1;
                } else {
                    i2 = -1;
                    i = -1;
                }
                if (i2 == -1) {
                    int i5 = this.f11507g + 1;
                    int length = this.f11506f.length;
                    while (true) {
                        if (i5 >= length) {
                            break;
                        }
                        if (C4951c.m17356a(this.f11506f[i5].f11488a, m)) {
                            if (C4951c.m17356a(this.f11506f[i5].f11489b, fVar)) {
                                i2 = Hpack.f11491a.length + (i5 - this.f11507g);
                                break;
                            } else if (i == -1) {
                                i = (i5 - this.f11507g) + Hpack.f11491a.length;
                            }
                        }
                        i5++;
                    }
                }
                if (i2 != -1) {
                    mo27522a(i2, 127, 128);
                } else if (i == -1) {
                    this.f11501a.writeByte(64);
                    mo27523a(m);
                    mo27523a(fVar);
                    m17587a(cVar);
                } else if (!m.mo28096b(Header.f11482d) || Header.f11487i.equals(m)) {
                    mo27522a(i, 63, 64);
                    mo27523a(fVar);
                    m17587a(cVar);
                } else {
                    mo27522a(i, 15, 0);
                    mo27523a(fVar);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27522a(int i, int i2, int i3) {
            if (i < i2) {
                this.f11501a.writeByte(i | i3);
                return;
            }
            this.f11501a.writeByte(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f11501a.writeByte(128 | (i4 & 127));
                i4 >>>= 7;
            }
            this.f11501a.writeByte(i4);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27523a(ByteString fVar) {
            if (!this.f11502b || Huffman.m17739b().mo27602a(fVar) >= fVar.mo28104l()) {
                mo27522a(fVar.mo28104l(), 127, 0);
                this.f11501a.mo28032a(fVar);
                return;
            }
            Buffer cVar = new Buffer();
            Huffman.m17739b().mo27603a(fVar, cVar);
            ByteString c = cVar.mo28047c();
            mo27522a(c.mo28104l(), 127, 128);
            this.f11501a.mo28032a(c);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo27521a(int i) {
            int min = Math.min(i, 16384);
            int i2 = this.f11505e;
            if (i2 != min) {
                if (min < i2) {
                    this.f11503c = Math.min(this.f11503c, min);
                }
                this.f11504d = true;
                this.f11505e = min;
                m17586a();
            }
        }

        /* renamed from: a */
        private void m17586a() {
            int i = this.f11505e;
            int i2 = this.f11509i;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                m17589b();
            } else {
                m17588b(i2 - i);
            }
        }
    }

    /* renamed from: a */
    private static Map<ByteString, Integer> m17568a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f11491a.length);
        int i = 0;
        while (true) {
            Header[] cVarArr = f11491a;
            if (i >= cVarArr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(cVarArr[i].f11488a)) {
                linkedHashMap.put(f11491a[i].f11488a, Integer.valueOf(i));
            }
            i++;
        }
    }

    /* renamed from: a */
    static ByteString m17567a(ByteString fVar) {
        int l = fVar.mo28104l();
        int i = 0;
        while (i < l) {
            byte a = fVar.mo28089a(i);
            if (a < 65 || a > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + fVar.mo28107o());
            }
        }
        return fVar;
    }
}
