package p231i.p232f0.p234f;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.List;
import p231i.Address;
import p231i.Call;
import p231i.ConnectionPool;
import p231i.EventListener;
import p231i.Interceptor;
import p231i.OkHttpClient;
import p231i.Route;
import p231i.p232f0.C4951c;
import p231i.p232f0.Internal;
import p231i.p232f0.p234f.RouteSelector;
import p231i.p232f0.p235g.HttpCodec;
import p231i.p232f0.p237i.ConnectionShutdownException;
import p231i.p232f0.p237i.ErrorCode;
import p231i.p232f0.p237i.StreamResetException;

/* renamed from: i.f0.f.g */
public final class StreamAllocation {

    /* renamed from: a */
    public final Address f11404a;

    /* renamed from: b */
    private RouteSelector.C4964a f11405b;

    /* renamed from: c */
    private Route f11406c;

    /* renamed from: d */
    private final ConnectionPool f11407d;

    /* renamed from: e */
    public final Call f11408e;

    /* renamed from: f */
    public final EventListener f11409f;

    /* renamed from: g */
    private final Object f11410g;

    /* renamed from: h */
    private final RouteSelector f11411h;

    /* renamed from: i */
    private int f11412i;

    /* renamed from: j */
    private RealConnection f11413j;

    /* renamed from: k */
    private boolean f11414k;

    /* renamed from: l */
    private boolean f11415l;

    /* renamed from: m */
    private boolean f11416m;

    /* renamed from: n */
    private HttpCodec f11417n;

    /* renamed from: i.f0.f.g$a */
    /* compiled from: StreamAllocation */
    public static final class C4965a extends WeakReference<StreamAllocation> {

        /* renamed from: a */
        public final Object f11418a;

        C4965a(StreamAllocation gVar, Object obj) {
            super(gVar);
            this.f11418a = obj;
        }
    }

    public StreamAllocation(ConnectionPool jVar, Address aVar, Call eVar, EventListener pVar, Object obj) {
        this.f11407d = jVar;
        this.f11404a = aVar;
        this.f11408e = eVar;
        this.f11409f = pVar;
        this.f11411h = new RouteSelector(aVar, m17470h(), eVar, pVar);
        this.f11410g = obj;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [int, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: g */
    private Socket m17469g() {
        RealConnection cVar = this.f11413j;
        if (cVar == null || !cVar.f11386k) {
            return null;
        }
        return m17467a(false, false, true);
    }

    /* renamed from: h */
    private RouteDatabase m17470h() {
        return Internal.f11301a.mo27401a(this.f11407d);
    }

    /* renamed from: a */
    public HttpCodec mo27472a(OkHttpClient vVar, Interceptor.C5019a aVar, boolean z) {
        try {
            HttpCodec a = m17466a(aVar.mo27494c(), aVar.mo27490a(), aVar.mo27493b(), vVar.mo27801q(), vVar.mo27807y(), z).mo27448a(vVar, aVar, this);
            synchronized (this.f11407d) {
                this.f11417n = a;
            }
            return a;
        } catch (IOException e) {
            throw new RouteException(e);
        }
    }

    /* renamed from: b */
    public synchronized RealConnection mo27477b() {
        return this.f11413j;
    }

    /* renamed from: c */
    public boolean mo27478c() {
        RouteSelector.C4964a aVar;
        return this.f11406c != null || ((aVar = this.f11405b) != null && aVar.mo27469b()) || this.f11411h.mo27466a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [int, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: d */
    public void mo27479d() {
        RealConnection cVar;
        Socket a;
        synchronized (this.f11407d) {
            cVar = this.f11413j;
            a = m17467a(true, false, false);
            if (this.f11413j != null) {
                cVar = null;
            }
        }
        C4951c.m17353a(a);
        if (cVar != null) {
            this.f11409f.mo27723b(this.f11408e, cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [int, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: e */
    public void mo27480e() {
        RealConnection cVar;
        Socket a;
        synchronized (this.f11407d) {
            cVar = this.f11413j;
            a = m17467a(false, true, false);
            if (this.f11413j != null) {
                cVar = null;
            }
        }
        C4951c.m17353a(a);
        if (cVar != null) {
            this.f11409f.mo27723b(this.f11408e, cVar);
        }
    }

    /* renamed from: f */
    public Route mo27481f() {
        return this.f11406c;
    }

    public String toString() {
        RealConnection b = mo27477b();
        return b != null ? b.toString() : this.f11404a.toString();
    }

    /* renamed from: b */
    private void m17468b(RealConnection cVar) {
        int size = cVar.f11389n.size();
        for (int i = 0; i < size; i++) {
            if (cVar.f11389n.get(i).get() == this) {
                cVar.f11389n.remove(i);
                return;
            }
        }
        throw new IllegalStateException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0.mo27455a(r9) != false) goto L_0x0018;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private p231i.p232f0.p234f.RealConnection m17466a(int r4, int r5, int r6, int r7, boolean r8, boolean r9) {
        /*
            r3 = this;
        L_0x0000:
            i.f0.f.c r0 = r3.m17465a(r4, r5, r6, r7, r8)
            i.j r1 = r3.f11407d
            monitor-enter(r1)
            int r2 = r0.f11387l     // Catch:{ all -> 0x0019 }
            if (r2 != 0) goto L_0x000d
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            return r0
        L_0x000d:
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            boolean r1 = r0.mo27455a(r9)
            if (r1 != 0) goto L_0x0018
            r3.mo27479d()
            goto L_0x0000
        L_0x0018:
            return r0
        L_0x0019:
            r4 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p234f.StreamAllocation.m17466a(int, int, int, int, boolean, boolean):i.f0.f.c");
    }

    /* renamed from: a */
    private RealConnection m17465a(int i, int i2, int i3, int i4, boolean z) {
        Socket g;
        Socket socket;
        RealConnection cVar;
        RealConnection cVar2;
        Route c0Var;
        RealConnection cVar3;
        boolean z2;
        boolean z3;
        RouteSelector.C4964a aVar;
        synchronized (this.f11407d) {
            if (this.f11415l) {
                throw new IllegalStateException("released");
            } else if (this.f11417n != null) {
                throw new IllegalStateException("codec != null");
            } else if (!this.f11416m) {
                RealConnection cVar4 = this.f11413j;
                g = m17469g();
                socket = null;
                if (this.f11413j != null) {
                    cVar2 = this.f11413j;
                    cVar = null;
                } else {
                    cVar = cVar4;
                    cVar2 = null;
                }
                if (!this.f11414k) {
                    cVar = null;
                }
                if (cVar2 == null) {
                    Internal.f11301a.mo27400a(this.f11407d, this.f11404a, this, null);
                    if (this.f11413j != null) {
                        cVar3 = this.f11413j;
                        c0Var = null;
                        z2 = true;
                    } else {
                        c0Var = this.f11406c;
                        cVar3 = cVar2;
                    }
                } else {
                    cVar3 = cVar2;
                    c0Var = null;
                }
                z2 = false;
            } else {
                throw new IOException("Canceled");
            }
        }
        C4951c.m17353a(g);
        if (cVar != null) {
            this.f11409f.mo27723b(this.f11408e, cVar);
        }
        if (z2) {
            this.f11409f.mo27712a(this.f11408e, cVar3);
        }
        if (cVar3 != null) {
            return cVar3;
        }
        if (c0Var != null || ((aVar = this.f11405b) != null && aVar.mo27469b())) {
            z3 = false;
        } else {
            this.f11405b = this.f11411h.mo27467b();
            z3 = true;
        }
        synchronized (this.f11407d) {
            if (!this.f11416m) {
                if (z3) {
                    List<Route> a = this.f11405b.mo27468a();
                    int size = a.size();
                    int i5 = 0;
                    while (true) {
                        if (i5 >= size) {
                            break;
                        }
                        Route c0Var2 = a.get(i5);
                        Internal.f11301a.mo27400a(this.f11407d, this.f11404a, this, c0Var2);
                        if (this.f11413j != null) {
                            cVar3 = this.f11413j;
                            this.f11406c = c0Var2;
                            z2 = true;
                            break;
                        }
                        i5++;
                    }
                }
                if (!z2) {
                    if (c0Var == null) {
                        c0Var = this.f11405b.mo27470c();
                    }
                    this.f11406c = c0Var;
                    this.f11412i = 0;
                    cVar3 = new RealConnection(this.f11407d, c0Var);
                    mo27474a(cVar3, false);
                }
            } else {
                throw new IOException("Canceled");
            }
        }
        if (z2) {
            this.f11409f.mo27712a(this.f11408e, cVar3);
            return cVar3;
        }
        cVar3.mo27450a(i, i2, i3, i4, z, this.f11408e, this.f11409f);
        m17470h().mo27460a(cVar3.mo27457c());
        synchronized (this.f11407d) {
            this.f11414k = true;
            Internal.f11301a.mo27408b(this.f11407d, cVar3);
            if (cVar3.mo27456b()) {
                socket = Internal.f11301a.mo27402a(this.f11407d, this.f11404a, this);
                cVar3 = this.f11413j;
            }
        }
        C4951c.m17353a(socket);
        this.f11409f.mo27712a(this.f11408e, cVar3);
        return cVar3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [boolean, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: a */
    public void mo27476a(boolean z, HttpCodec cVar, long j, IOException iOException) {
        RealConnection cVar2;
        Socket a;
        boolean z2;
        this.f11409f.mo27722b(this.f11408e, j);
        synchronized (this.f11407d) {
            if (cVar != null) {
                if (cVar == this.f11417n) {
                    if (!z) {
                        this.f11413j.f11387l++;
                    }
                    cVar2 = this.f11413j;
                    a = m17467a(z, false, true);
                    if (this.f11413j != null) {
                        cVar2 = null;
                    }
                    z2 = this.f11415l;
                }
            }
            throw new IllegalStateException("expected " + this.f11417n + " but was " + cVar);
        }
        C4951c.m17353a(a);
        if (cVar2 != null) {
            this.f11409f.mo27723b(this.f11408e, cVar2);
        }
        if (iOException != null) {
            this.f11409f.mo27715a(this.f11408e, iOException);
        } else if (z2) {
            this.f11409f.mo27709a(this.f11408e);
        }
    }

    /* renamed from: a */
    public HttpCodec mo27471a() {
        HttpCodec cVar;
        synchronized (this.f11407d) {
            cVar = this.f11417n;
        }
        return cVar;
    }

    /* renamed from: a */
    private Socket m17467a(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.f11417n = null;
        }
        if (z2) {
            this.f11415l = true;
        }
        RealConnection cVar = this.f11413j;
        if (cVar != null) {
            if (z) {
                cVar.f11386k = true;
            }
            if (this.f11417n == null && (this.f11415l || this.f11413j.f11386k)) {
                m17468b(this.f11413j);
                if (this.f11413j.f11389n.isEmpty()) {
                    this.f11413j.f11390o = System.nanoTime();
                    if (Internal.f11301a.mo27407a(this.f11407d, this.f11413j)) {
                        socket = this.f11413j.mo27458d();
                        this.f11413j = null;
                        return socket;
                    }
                }
                socket = null;
                this.f11413j = null;
                return socket;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [boolean, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: a */
    public void mo27475a(IOException iOException) {
        boolean z;
        RealConnection cVar;
        Socket a;
        synchronized (this.f11407d) {
            if (iOException instanceof StreamResetException) {
                StreamResetException nVar = (StreamResetException) iOException;
                if (nVar.f11643P == ErrorCode.REFUSED_STREAM) {
                    this.f11412i++;
                }
                if (nVar.f11643P != ErrorCode.REFUSED_STREAM || this.f11412i > 1) {
                    this.f11406c = null;
                }
                z = false;
                cVar = this.f11413j;
                a = m17467a(z, false, true);
                if (this.f11413j != null || !this.f11414k) {
                    cVar = null;
                }
            } else {
                if (this.f11413j != null && (!this.f11413j.mo27456b() || (iOException instanceof ConnectionShutdownException))) {
                    if (this.f11413j.f11387l == 0) {
                        if (!(this.f11406c == null || iOException == null)) {
                            this.f11411h.mo27465a(this.f11406c, iOException);
                        }
                        this.f11406c = null;
                    }
                }
                z = false;
                cVar = this.f11413j;
                a = m17467a(z, false, true);
                cVar = null;
            }
            z = true;
            cVar = this.f11413j;
            a = m17467a(z, false, true);
            cVar = null;
        }
        C4951c.m17353a(a);
        if (cVar != null) {
            this.f11409f.mo27723b(this.f11408e, cVar);
        }
    }

    /* renamed from: a */
    public void mo27474a(RealConnection cVar, boolean z) {
        if (this.f11413j == null) {
            this.f11413j = cVar;
            this.f11414k = z;
            cVar.f11389n.add(new C4965a(this, this.f11410g));
            return;
        }
        throw new IllegalStateException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket
     arg types: [int, int, int]
     candidates:
      i.f0.f.g.a(i.v, i.t$a, boolean):i.f0.g.c
      i.f0.f.g.a(boolean, boolean, boolean):java.net.Socket */
    /* renamed from: a */
    public Socket mo27473a(RealConnection cVar) {
        if (this.f11417n == null && this.f11413j.f11389n.size() == 1) {
            Socket a = m17467a(true, false, false);
            this.f11413j = cVar;
            cVar.f11389n.add(this.f11413j.f11389n.get(0));
            return a;
        }
        throw new IllegalStateException();
    }
}
