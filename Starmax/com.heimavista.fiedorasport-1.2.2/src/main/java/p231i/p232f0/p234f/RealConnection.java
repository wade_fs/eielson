package p231i.p232f0.p234f;

import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import p231i.Address;
import p231i.C5023y;
import p231i.Call;
import p231i.Connection;
import p231i.ConnectionPool;
import p231i.EventListener;
import p231i.Handshake;
import p231i.HttpUrl;
import p231i.Interceptor;
import p231i.OkHttpClient;
import p231i.Protocol;
import p231i.Response;
import p231i.Route;
import p231i.p232f0.C4951c;
import p231i.p232f0.C4954d;
import p231i.p232f0.Internal;
import p231i.p232f0.p235g.HttpCodec;
import p231i.p232f0.p235g.HttpHeaders;
import p231i.p232f0.p236h.Http1Codec;
import p231i.p232f0.p237i.ErrorCode;
import p231i.p232f0.p237i.Http2Codec;
import p231i.p232f0.p237i.Http2Connection;
import p231i.p232f0.p237i.Http2Stream;
import p231i.p232f0.p239k.Platform;
import p231i.p232f0.p240l.OkHostnameVerifier;
import p244j.BufferedSink;
import p244j.BufferedSource;
import p244j.C5049s;
import p244j.Okio;

/* renamed from: i.f0.f.c */
public final class RealConnection extends Http2Connection.C4985h implements Connection {

    /* renamed from: b */
    private final ConnectionPool f11377b;

    /* renamed from: c */
    private final Route f11378c;

    /* renamed from: d */
    private Socket f11379d;

    /* renamed from: e */
    private Socket f11380e;

    /* renamed from: f */
    private Handshake f11381f;

    /* renamed from: g */
    private Protocol f11382g;

    /* renamed from: h */
    private Http2Connection f11383h;

    /* renamed from: i */
    private BufferedSource f11384i;

    /* renamed from: j */
    private BufferedSink f11385j;

    /* renamed from: k */
    public boolean f11386k;

    /* renamed from: l */
    public int f11387l;

    /* renamed from: m */
    public int f11388m = 1;

    /* renamed from: n */
    public final List<Reference<StreamAllocation>> f11389n = new ArrayList();

    /* renamed from: o */
    public long f11390o = Long.MAX_VALUE;

    public RealConnection(ConnectionPool jVar, Route c0Var) {
        this.f11377b = jVar;
        this.f11378c = c0Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(i.s, boolean):java.lang.String
     arg types: [i.s, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(i.s, boolean):java.lang.String */
    /* renamed from: e */
    private C5023y m17436e() {
        C5023y.C5024a aVar = new C5023y.C5024a();
        aVar.mo27831a(this.f11378c.mo27371a().mo27311k());
        aVar.mo27834a("Host", C4951c.m17343a(this.f11378c.mo27371a().mo27311k(), true));
        aVar.mo27834a("Proxy-Connection", "Keep-Alive");
        aVar.mo27834a("User-Agent", C4954d.m17369a());
        return aVar.mo27835a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x007e A[Catch:{ IOException -> 0x00e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo27450a(int r17, int r18, int r19, int r20, boolean r21, p231i.Call r22, p231i.EventListener r23) {
        /*
            r16 = this;
            r7 = r16
            r8 = r22
            r9 = r23
            i.w r0 = r7.f11382g
            if (r0 != 0) goto L_0x0132
            i.c0 r0 = r7.f11378c
            i.a r0 = r0.mo27371a()
            java.util.List r0 = r0.mo27300b()
            i.f0.f.b r10 = new i.f0.f.b
            r10.<init>(r0)
            i.c0 r1 = r7.f11378c
            i.a r1 = r1.mo27371a()
            javax.net.ssl.SSLSocketFactory r1 = r1.mo27310j()
            if (r1 != 0) goto L_0x0074
            i.k r1 = p231i.ConnectionSpec.f11709g
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0067
            i.c0 r0 = r7.f11378c
            i.a r0 = r0.mo27371a()
            i.s r0 = r0.mo27311k()
            java.lang.String r0 = r0.mo27761g()
            i.f0.k.f r1 = p231i.p232f0.p239k.Platform.m17817d()
            boolean r1 = r1.mo27635b(r0)
            if (r1 == 0) goto L_0x0046
            goto L_0x0074
        L_0x0046:
            i.f0.f.e r1 = new i.f0.f.e
            java.net.UnknownServiceException r2 = new java.net.UnknownServiceException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "CLEARTEXT communication to "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = " not permitted by network security policy"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.<init>(r0)
            r1.<init>(r2)
            throw r1
        L_0x0067:
            i.f0.f.e r0 = new i.f0.f.e
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "CLEARTEXT communication not enabled for client"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0074:
            r11 = 0
            r12 = r11
        L_0x0076:
            i.c0 r0 = r7.f11378c     // Catch:{ IOException -> 0x00e7 }
            boolean r0 = r0.mo27373c()     // Catch:{ IOException -> 0x00e7 }
            if (r0 == 0) goto L_0x0097
            r1 = r16
            r2 = r17
            r3 = r18
            r4 = r19
            r5 = r22
            r6 = r23
            r1.m17431a(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x00e7 }
            java.net.Socket r0 = r7.f11379d     // Catch:{ IOException -> 0x00e7 }
            if (r0 != 0) goto L_0x0092
            goto L_0x00b4
        L_0x0092:
            r13 = r17
            r14 = r18
            goto L_0x009e
        L_0x0097:
            r13 = r17
            r14 = r18
            r7.m17432a(r13, r14, r8, r9)     // Catch:{ IOException -> 0x00e5 }
        L_0x009e:
            r15 = r20
            r7.m17434a(r10, r15, r8, r9)     // Catch:{ IOException -> 0x00e3 }
            i.c0 r0 = r7.f11378c     // Catch:{ IOException -> 0x00e3 }
            java.net.InetSocketAddress r0 = r0.mo27374d()     // Catch:{ IOException -> 0x00e3 }
            i.c0 r1 = r7.f11378c     // Catch:{ IOException -> 0x00e3 }
            java.net.Proxy r1 = r1.mo27372b()     // Catch:{ IOException -> 0x00e3 }
            i.w r2 = r7.f11382g     // Catch:{ IOException -> 0x00e3 }
            r9.mo27719a(r8, r0, r1, r2)     // Catch:{ IOException -> 0x00e3 }
        L_0x00b4:
            i.c0 r0 = r7.f11378c
            boolean r0 = r0.mo27373c()
            if (r0 == 0) goto L_0x00ce
            java.net.Socket r0 = r7.f11379d
            if (r0 == 0) goto L_0x00c1
            goto L_0x00ce
        L_0x00c1:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Too many tunnel connections attempted: 21"
            r0.<init>(r1)
            i.f0.f.e r1 = new i.f0.f.e
            r1.<init>(r0)
            throw r1
        L_0x00ce:
            i.f0.i.g r0 = r7.f11383h
            if (r0 == 0) goto L_0x00e2
            i.j r1 = r7.f11377b
            monitor-enter(r1)
            i.f0.i.g r0 = r7.f11383h     // Catch:{ all -> 0x00df }
            int r0 = r0.mo27538b()     // Catch:{ all -> 0x00df }
            r7.f11388m = r0     // Catch:{ all -> 0x00df }
            monitor-exit(r1)     // Catch:{ all -> 0x00df }
            goto L_0x00e2
        L_0x00df:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00df }
            throw r0
        L_0x00e2:
            return
        L_0x00e3:
            r0 = move-exception
            goto L_0x00ee
        L_0x00e5:
            r0 = move-exception
            goto L_0x00ec
        L_0x00e7:
            r0 = move-exception
            r13 = r17
            r14 = r18
        L_0x00ec:
            r15 = r20
        L_0x00ee:
            java.net.Socket r1 = r7.f11380e
            p231i.p232f0.C4951c.m17353a(r1)
            java.net.Socket r1 = r7.f11379d
            p231i.p232f0.C4951c.m17353a(r1)
            r7.f11380e = r11
            r7.f11379d = r11
            r7.f11384i = r11
            r7.f11385j = r11
            r7.f11381f = r11
            r7.f11382g = r11
            r7.f11383h = r11
            i.c0 r1 = r7.f11378c
            java.net.InetSocketAddress r3 = r1.mo27374d()
            i.c0 r1 = r7.f11378c
            java.net.Proxy r4 = r1.mo27372b()
            r5 = 0
            r1 = r23
            r2 = r22
            r6 = r0
            r1.mo27720a(r2, r3, r4, r5, r6)
            if (r12 != 0) goto L_0x0124
            i.f0.f.e r1 = new i.f0.f.e
            r1.<init>(r0)
            r12 = r1
            goto L_0x0127
        L_0x0124:
            r12.mo27464a(r0)
        L_0x0127:
            if (r21 == 0) goto L_0x0131
            boolean r0 = r10.mo27447a(r0)
            if (r0 == 0) goto L_0x0131
            goto L_0x0076
        L_0x0131:
            throw r12
        L_0x0132:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p234f.RealConnection.mo27450a(int, int, int, int, boolean, i.e, i.p):void");
    }

    /* renamed from: b */
    public boolean mo27456b() {
        return this.f11383h != null;
    }

    /* renamed from: c */
    public Route mo27457c() {
        return this.f11378c;
    }

    /* renamed from: d */
    public Socket mo27458d() {
        return this.f11380e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.f11378c.mo27371a().mo27311k().mo27761g());
        sb.append(":");
        sb.append(this.f11378c.mo27371a().mo27311k().mo27765j());
        sb.append(", proxy=");
        sb.append(this.f11378c.mo27372b());
        sb.append(" hostAddress=");
        sb.append(this.f11378c.mo27374d());
        sb.append(" cipherSuite=");
        Handshake qVar = this.f11381f;
        sb.append(qVar != null ? qVar.mo27730a() : "none");
        sb.append(" protocol=");
        sb.append(this.f11382g);
        sb.append('}');
        return sb.toString();
    }

    /* renamed from: a */
    private void m17431a(int i, int i2, int i3, Call eVar, EventListener pVar) {
        C5023y e = m17436e();
        HttpUrl g = e.mo27827g();
        int i4 = 0;
        while (i4 < 21) {
            m17432a(i, i2, eVar, pVar);
            e = m17430a(i2, i3, e, g);
            if (e != null) {
                C4951c.m17353a(this.f11379d);
                this.f11379d = null;
                this.f11385j = null;
                this.f11384i = null;
                pVar.mo27719a(eVar, this.f11378c.mo27374d(), this.f11378c.mo27372b(), null);
                i4++;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private void m17432a(int i, int i2, Call eVar, EventListener pVar) {
        Socket socket;
        Proxy b = this.f11378c.mo27372b();
        Address a = this.f11378c.mo27371a();
        if (b.type() == Proxy.Type.DIRECT || b.type() == Proxy.Type.HTTP) {
            socket = a.mo27309i().createSocket();
        } else {
            socket = new Socket(b);
        }
        this.f11379d = socket;
        pVar.mo27718a(eVar, this.f11378c.mo27374d(), b);
        this.f11379d.setSoTimeout(i2);
        try {
            Platform.m17817d().mo27631a(this.f11379d, this.f11378c.mo27374d(), i);
            try {
                this.f11384i = Okio.m18212a(Okio.m18222b(this.f11379d));
                this.f11385j = Okio.m18211a(Okio.m18217a(this.f11379d));
            } catch (NullPointerException e) {
                if ("throw with null exception".equals(e.getMessage())) {
                    throw new IOException(e);
                }
            }
        } catch (ConnectException e2) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.f11378c.mo27374d());
            connectException.initCause(e2);
            throw connectException;
        }
    }

    /* renamed from: a */
    private void m17434a(ConnectionSpecSelector bVar, int i, Call eVar, EventListener pVar) {
        if (this.f11378c.mo27371a().mo27310j() == null) {
            this.f11382g = Protocol.HTTP_1_1;
            this.f11380e = this.f11379d;
            return;
        }
        pVar.mo27728g(eVar);
        m17433a(bVar);
        pVar.mo27713a(eVar, this.f11381f);
        if (this.f11382g == Protocol.HTTP_2) {
            this.f11380e.setSoTimeout(0);
            Http2Connection.C4984g gVar = new Http2Connection.C4984g(true);
            gVar.mo27549a(this.f11380e, this.f11378c.mo27371a().mo27311k().mo27761g(), this.f11384i, this.f11385j);
            gVar.mo27548a(super);
            gVar.mo27547a(i);
            this.f11383h = gVar.mo27550a();
            this.f11383h.mo27542c();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011f A[Catch:{ all -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0125 A[Catch:{ all -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0128  */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m17433a(p231i.p232f0.p234f.ConnectionSpecSelector r8) {
        /*
            r7 = this;
            i.c0 r0 = r7.f11378c
            i.a r0 = r0.mo27371a()
            javax.net.ssl.SSLSocketFactory r1 = r0.mo27310j()
            r2 = 0
            java.net.Socket r3 = r7.f11379d     // Catch:{ AssertionError -> 0x0118 }
            i.s r4 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0118 }
            java.lang.String r4 = r4.mo27761g()     // Catch:{ AssertionError -> 0x0118 }
            i.s r5 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0118 }
            int r5 = r5.mo27765j()     // Catch:{ AssertionError -> 0x0118 }
            r6 = 1
            java.net.Socket r1 = r1.createSocket(r3, r4, r5, r6)     // Catch:{ AssertionError -> 0x0118 }
            javax.net.ssl.SSLSocket r1 = (javax.net.ssl.SSLSocket) r1     // Catch:{ AssertionError -> 0x0118 }
            i.k r8 = r8.mo27446a(r1)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            boolean r3 = r8.mo27684c()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r3 == 0) goto L_0x0041
            i.f0.k.f r3 = p231i.p232f0.p239k.Platform.m17817d()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            i.s r4 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r4 = r4.mo27761g()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.util.List r5 = r0.mo27303e()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.mo27632a(r1, r4, r5)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
        L_0x0041:
            r1.startHandshake()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            javax.net.ssl.SSLSession r3 = r1.getSession()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            boolean r4 = r7.m17435a(r3)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r4 == 0) goto L_0x0108
            i.q r4 = p231i.Handshake.m17924a(r3)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            javax.net.ssl.HostnameVerifier r5 = r0.mo27302d()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            i.s r6 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r6 = r6.mo27761g()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            boolean r3 = r5.verify(r6, r3)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r3 == 0) goto L_0x00b6
            i.g r3 = r0.mo27298a()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            i.s r0 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = r0.mo27761g()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.util.List r5 = r4.mo27732c()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.mo27662a(r0, r5)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            boolean r8 = r8.mo27684c()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r8 == 0) goto L_0x0085
            i.f0.k.f r8 = p231i.p232f0.p239k.Platform.m17817d()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r2 = r8.mo27634b(r1)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
        L_0x0085:
            r7.f11380e = r1     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.net.Socket r8 = r7.f11380e     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            j.s r8 = p244j.Okio.m18222b(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            j.e r8 = p244j.Okio.m18212a(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r7.f11384i = r8     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.net.Socket r8 = r7.f11380e     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            j.r r8 = p244j.Okio.m18217a(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            j.d r8 = p244j.Okio.m18211a(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r7.f11385j = r8     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r7.f11381f = r4     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r2 == 0) goto L_0x00a8
            i.w r8 = p231i.Protocol.m18040a(r2)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            goto L_0x00aa
        L_0x00a8:
            i.w r8 = p231i.Protocol.HTTP_1_1     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
        L_0x00aa:
            r7.f11382g = r8     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            if (r1 == 0) goto L_0x00b5
            i.f0.k.f r8 = p231i.p232f0.p239k.Platform.m17817d()
            r8.mo27645a(r1)
        L_0x00b5:
            return
        L_0x00b6:
            java.util.List r8 = r4.mo27732c()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r2 = 0
            java.lang.Object r8 = r8.get(r2)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.security.cert.X509Certificate r8 = (java.security.cert.X509Certificate) r8     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            javax.net.ssl.SSLPeerUnverifiedException r2 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.<init>()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r4 = "Hostname "
            r3.append(r4)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            i.s r0 = r0.mo27311k()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = r0.mo27761g()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = " not verified:\n    certificate: "
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = p231i.CertificatePinner.m17844a(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = "\n    DN: "
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.security.Principal r0 = r8.getSubjectDN()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = r0.getName()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = "\n    subjectAltNames: "
            r3.append(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.util.List r8 = p231i.p232f0.p240l.OkHostnameVerifier.m17836a(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r3.append(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r8 = r3.toString()     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            r2.<init>(r8)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            throw r2     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
        L_0x0108:
            java.io.IOException r8 = new java.io.IOException     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            java.lang.String r0 = "a valid ssl session was not established"
            r8.<init>(r0)     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
            throw r8     // Catch:{ AssertionError -> 0x0112, all -> 0x0110 }
        L_0x0110:
            r8 = move-exception
            goto L_0x0126
        L_0x0112:
            r8 = move-exception
            r2 = r1
            goto L_0x0119
        L_0x0115:
            r8 = move-exception
            r1 = r2
            goto L_0x0126
        L_0x0118:
            r8 = move-exception
        L_0x0119:
            boolean r0 = p231i.p232f0.C4951c.m17355a(r8)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x0125
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0115 }
            r0.<init>(r8)     // Catch:{ all -> 0x0115 }
            throw r0     // Catch:{ all -> 0x0115 }
        L_0x0125:
            throw r8     // Catch:{ all -> 0x0115 }
        L_0x0126:
            if (r1 == 0) goto L_0x012f
            i.f0.k.f r0 = p231i.p232f0.p239k.Platform.m17817d()
            r0.mo27645a(r1)
        L_0x012f:
            p231i.p232f0.C4951c.m17353a(r1)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.p232f0.p234f.RealConnection.m17433a(i.f0.f.b):void");
    }

    /* renamed from: a */
    private boolean m17435a(SSLSession sSLSession) {
        return !"NONE".equals(sSLSession.getProtocol()) && !"SSL_NULL_WITH_NULL_NULL".equals(sSLSession.getCipherSuite());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(i.s, boolean):java.lang.String
     arg types: [i.s, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(i.s, boolean):java.lang.String */
    /* renamed from: a */
    private C5023y m17430a(int i, int i2, C5023y yVar, HttpUrl sVar) {
        String str = "CONNECT " + C4951c.m17343a(sVar, true) + " HTTP/1.1";
        while (true) {
            Http1Codec aVar = new Http1Codec(null, null, this.f11384i, this.f11385j);
            this.f11384i.mo27416h().mo28115a((long) i, TimeUnit.MILLISECONDS);
            this.f11385j.mo27513h().mo28115a((long) i2, TimeUnit.MILLISECONDS);
            aVar.mo27506a(yVar.mo27823c(), str);
            aVar.mo27486a();
            Response.C4938a a = aVar.mo27483a(false);
            a.mo27339a(yVar);
            Response a2 = a.mo27342a();
            long a3 = HttpHeaders.m17496a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            C5049s b = aVar.mo27508b(a3);
            C4951c.m17361b(b, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b.close();
            int d = a2.mo27321d();
            if (d != 200) {
                if (d == 407) {
                    C5023y a4 = this.f11378c.mo27371a().mo27306g().mo27346a(this.f11378c, a2);
                    if (a4 == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    } else if ("close".equalsIgnoreCase(a2.mo27318b("Connection"))) {
                        return a4;
                    } else {
                        yVar = a4;
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a2.mo27321d());
                }
            } else if (this.f11384i.mo28054f().mo28063j() && this.f11385j.mo28054f().mo28063j()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        }
    }

    /* renamed from: a */
    public boolean mo27453a(Address aVar, Route c0Var) {
        if (this.f11389n.size() >= this.f11388m || this.f11386k || !Internal.f11301a.mo27406a(this.f11378c.mo27371a(), aVar)) {
            return false;
        }
        if (aVar.mo27311k().mo27761g().equals(mo27457c().mo27371a().mo27311k().mo27761g())) {
            return true;
        }
        if (this.f11383h == null || c0Var == null || c0Var.mo27372b().type() != Proxy.Type.DIRECT || this.f11378c.mo27372b().type() != Proxy.Type.DIRECT || !this.f11378c.mo27374d().equals(c0Var.mo27374d()) || c0Var.mo27371a().mo27302d() != OkHostnameVerifier.f11674a || !mo27454a(aVar.mo27311k())) {
            return false;
        }
        try {
            aVar.mo27298a().mo27662a(aVar.mo27311k().mo27761g(), mo27449a().mo27732c());
            return true;
        } catch (SSLPeerUnverifiedException unused) {
            return false;
        }
    }

    /* renamed from: a */
    public boolean mo27454a(HttpUrl sVar) {
        if (sVar.mo27765j() != this.f11378c.mo27371a().mo27311k().mo27765j()) {
            return false;
        }
        if (sVar.mo27761g().equals(this.f11378c.mo27371a().mo27311k().mo27761g())) {
            return true;
        }
        if (this.f11381f == null || !OkHostnameVerifier.f11674a.mo27658a(sVar.mo27761g(), (X509Certificate) this.f11381f.mo27732c().get(0))) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public HttpCodec mo27448a(OkHttpClient vVar, Interceptor.C5019a aVar, StreamAllocation gVar) {
        Http2Connection gVar2 = this.f11383h;
        if (gVar2 != null) {
            return new Http2Codec(vVar, aVar, gVar, gVar2);
        }
        this.f11380e.setSoTimeout(aVar.mo27490a());
        this.f11384i.mo27416h().mo28115a((long) aVar.mo27490a(), TimeUnit.MILLISECONDS);
        this.f11385j.mo27513h().mo28115a((long) aVar.mo27493b(), TimeUnit.MILLISECONDS);
        return new Http1Codec(vVar, gVar, this.f11384i, this.f11385j);
    }

    /* renamed from: a */
    public boolean mo27455a(boolean z) {
        int soTimeout;
        if (this.f11380e.isClosed() || this.f11380e.isInputShutdown() || this.f11380e.isOutputShutdown()) {
            return false;
        }
        Http2Connection gVar = this.f11383h;
        if (gVar != null) {
            return !gVar.mo27537a();
        }
        if (z) {
            try {
                soTimeout = this.f11380e.getSoTimeout();
                this.f11380e.setSoTimeout(1);
                if (this.f11384i.mo28063j()) {
                    this.f11380e.setSoTimeout(soTimeout);
                    return false;
                }
                this.f11380e.setSoTimeout(soTimeout);
                return true;
            } catch (SocketTimeoutException unused) {
            } catch (IOException unused2) {
                return false;
            } catch (Throwable th) {
                this.f11380e.setSoTimeout(soTimeout);
                throw th;
            }
        }
        return true;
    }

    /* renamed from: a */
    public void mo27452a(Http2Stream iVar) {
        iVar.mo27566a(ErrorCode.REFUSED_STREAM);
    }

    /* renamed from: a */
    public void mo27451a(Http2Connection gVar) {
        synchronized (this.f11377b) {
            this.f11388m = gVar.mo27538b();
        }
    }

    /* renamed from: a */
    public Handshake mo27449a() {
        return this.f11381f;
    }
}
