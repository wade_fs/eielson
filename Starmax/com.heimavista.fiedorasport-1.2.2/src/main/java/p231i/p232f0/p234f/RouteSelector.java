package p231i.p232f0.p234f;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import p231i.Address;
import p231i.Call;
import p231i.EventListener;
import p231i.HttpUrl;
import p231i.Route;
import p231i.p232f0.C4951c;

/* renamed from: i.f0.f.f */
public final class RouteSelector {

    /* renamed from: a */
    private final Address f11394a;

    /* renamed from: b */
    private final RouteDatabase f11395b;

    /* renamed from: c */
    private final Call f11396c;

    /* renamed from: d */
    private final EventListener f11397d;

    /* renamed from: e */
    private List<Proxy> f11398e = Collections.emptyList();

    /* renamed from: f */
    private int f11399f;

    /* renamed from: g */
    private List<InetSocketAddress> f11400g = Collections.emptyList();

    /* renamed from: h */
    private final List<Route> f11401h = new ArrayList();

    /* renamed from: i.f0.f.f$a */
    /* compiled from: RouteSelector */
    public static final class C4964a {

        /* renamed from: a */
        private final List<Route> f11402a;

        /* renamed from: b */
        private int f11403b = 0;

        C4964a(List<Route> list) {
            this.f11402a = list;
        }

        /* renamed from: a */
        public List<Route> mo27468a() {
            return new ArrayList(this.f11402a);
        }

        /* renamed from: b */
        public boolean mo27469b() {
            return this.f11403b < this.f11402a.size();
        }

        /* renamed from: c */
        public Route mo27470c() {
            if (mo27469b()) {
                List<Route> list = this.f11402a;
                int i = this.f11403b;
                this.f11403b = i + 1;
                return list.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    public RouteSelector(Address aVar, RouteDatabase dVar, Call eVar, EventListener pVar) {
        this.f11394a = aVar;
        this.f11395b = dVar;
        this.f11396c = eVar;
        this.f11397d = pVar;
        m17455a(aVar.mo27311k(), aVar.mo27305f());
    }

    /* renamed from: c */
    private boolean m17457c() {
        return this.f11399f < this.f11398e.size();
    }

    /* renamed from: d */
    private Proxy m17458d() {
        if (m17457c()) {
            List<Proxy> list = this.f11398e;
            int i = this.f11399f;
            this.f11399f = i + 1;
            Proxy proxy = list.get(i);
            m17456a(proxy);
            return proxy;
        }
        throw new SocketException("No route to " + this.f11394a.mo27311k().mo27761g() + "; exhausted proxy configurations: " + this.f11398e);
    }

    /* renamed from: a */
    public boolean mo27466a() {
        return m17457c() || !this.f11401h.isEmpty();
    }

    /* renamed from: b */
    public C4964a mo27467b() {
        if (mo27466a()) {
            ArrayList arrayList = new ArrayList();
            while (m17457c()) {
                Proxy d = m17458d();
                int size = this.f11400g.size();
                for (int i = 0; i < size; i++) {
                    Route c0Var = new Route(this.f11394a, d, this.f11400g.get(i));
                    if (this.f11395b.mo27462c(c0Var)) {
                        this.f11401h.add(c0Var);
                    } else {
                        arrayList.add(c0Var);
                    }
                }
                if (!arrayList.isEmpty()) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.addAll(this.f11401h);
                this.f11401h.clear();
            }
            return new C4964a(arrayList);
        }
        throw new NoSuchElementException();
    }

    /* renamed from: a */
    public void mo27465a(Route c0Var, IOException iOException) {
        if (!(c0Var.mo27372b().type() == Proxy.Type.DIRECT || this.f11394a.mo27307h() == null)) {
            this.f11394a.mo27307h().connectFailed(this.f11394a.mo27311k().mo27769n(), c0Var.mo27372b().address(), iOException);
        }
        this.f11395b.mo27461b(c0Var);
    }

    /* renamed from: a */
    private void m17455a(HttpUrl sVar, Proxy proxy) {
        List<Proxy> list;
        if (proxy != null) {
            this.f11398e = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.f11394a.mo27307h().select(sVar.mo27769n());
            if (select == null || select.isEmpty()) {
                list = C4951c.m17349a(Proxy.NO_PROXY);
            } else {
                list = C4951c.m17348a(select);
            }
            this.f11398e = list;
        }
        this.f11399f = 0;
    }

    /* renamed from: a */
    private void m17456a(Proxy proxy) {
        String str;
        int i;
        this.f11400g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f11394a.mo27311k().mo27761g();
            i = this.f11394a.mo27311k().mo27765j();
        } else {
            SocketAddress address = proxy.address();
            if (address instanceof InetSocketAddress) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                str = m17454a(inetSocketAddress);
                i = inetSocketAddress.getPort();
            } else {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        } else if (proxy.type() == Proxy.Type.SOCKS) {
            this.f11400g.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            this.f11397d.mo27716a(this.f11396c, str);
            List<InetAddress> a = this.f11394a.mo27301c().mo27708a(str);
            if (!a.isEmpty()) {
                this.f11397d.mo27717a(this.f11396c, str, a);
                int size = a.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.f11400g.add(new InetSocketAddress(a.get(i2), i));
                }
                return;
            }
            throw new UnknownHostException(this.f11394a.mo27301c() + " returned no addresses for " + str);
        }
    }

    /* renamed from: a */
    static String m17454a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }
}
