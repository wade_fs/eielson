package p231i.p232f0.p234f;

import p231i.C5023y;
import p231i.Interceptor;
import p231i.OkHttpClient;
import p231i.Response;
import p231i.p232f0.p235g.RealInterceptorChain;

/* renamed from: i.f0.f.a */
public final class ConnectInterceptor implements Interceptor {

    /* renamed from: a */
    public final OkHttpClient f11372a;

    public ConnectInterceptor(OkHttpClient vVar) {
        this.f11372a = vVar;
    }

    /* renamed from: a */
    public Response mo27414a(Interceptor.C5019a aVar) {
        RealInterceptorChain gVar = (RealInterceptorChain) aVar;
        C5023y d = gVar.mo27495d();
        StreamAllocation i = gVar.mo27500i();
        return gVar.mo27492a(d, i, i.mo27472a(this.f11372a, aVar, !d.mo27825e().equals("GET")), i.mo27477b());
    }
}
