package p231i.p232f0.p234f;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;
import p231i.ConnectionSpec;
import p231i.p232f0.Internal;

/* renamed from: i.f0.f.b */
public final class ConnectionSpecSelector {

    /* renamed from: a */
    private final List<ConnectionSpec> f11373a;

    /* renamed from: b */
    private int f11374b = 0;

    /* renamed from: c */
    private boolean f11375c;

    /* renamed from: d */
    private boolean f11376d;

    public ConnectionSpecSelector(List<ConnectionSpec> list) {
        this.f11373a = list;
    }

    /* renamed from: b */
    private boolean m17427b(SSLSocket sSLSocket) {
        for (int i = this.f11374b; i < this.f11373a.size(); i++) {
            if (this.f11373a.get(i).mo27682a(sSLSocket)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public ConnectionSpec mo27446a(SSLSocket sSLSocket) {
        ConnectionSpec kVar;
        int i = this.f11374b;
        int size = this.f11373a.size();
        while (true) {
            if (i >= size) {
                kVar = null;
                break;
            }
            kVar = this.f11373a.get(i);
            if (kVar.mo27682a(sSLSocket)) {
                this.f11374b = i + 1;
                break;
            }
            i++;
        }
        if (kVar != null) {
            this.f11375c = m17427b(sSLSocket);
            Internal.f11301a.mo27403a(kVar, sSLSocket, this.f11376d);
            return kVar;
        }
        throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.f11376d + ", modes=" + this.f11373a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
    }

    /* renamed from: a */
    public boolean mo27447a(IOException iOException) {
        this.f11376d = true;
        if (!this.f11375c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        boolean z = iOException instanceof SSLHandshakeException;
        if ((z && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if (z || (iOException instanceof SSLProtocolException)) {
            return true;
        }
        return false;
    }
}
