package p231i.p232f0.p234f;

import java.util.LinkedHashSet;
import java.util.Set;
import p231i.Route;

/* renamed from: i.f0.f.d */
public final class RouteDatabase {

    /* renamed from: a */
    private final Set<Route> f11391a = new LinkedHashSet();

    /* renamed from: a */
    public synchronized void mo27460a(Route c0Var) {
        this.f11391a.remove(c0Var);
    }

    /* renamed from: b */
    public synchronized void mo27461b(Route c0Var) {
        this.f11391a.add(c0Var);
    }

    /* renamed from: c */
    public synchronized boolean mo27462c(Route c0Var) {
        return this.f11391a.contains(c0Var);
    }
}
