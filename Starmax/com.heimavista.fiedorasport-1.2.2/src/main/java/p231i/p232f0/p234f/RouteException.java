package p231i.p232f0.p234f;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: i.f0.f.e */
public final class RouteException extends RuntimeException {

    /* renamed from: Q */
    private static final Method f11392Q;

    /* renamed from: P */
    private IOException f11393P;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", Throwable.class);
        } catch (Exception unused) {
            method = null;
        }
        f11392Q = method;
    }

    public RouteException(IOException iOException) {
        super(iOException);
        this.f11393P = iOException;
    }

    /* renamed from: a */
    public IOException mo27463a() {
        return this.f11393P;
    }

    /* renamed from: a */
    public void mo27464a(IOException iOException) {
        m17451a(iOException, this.f11393P);
        this.f11393P = iOException;
    }

    /* renamed from: a */
    private void m17451a(IOException iOException, IOException iOException2) {
        Method method = f11392Q;
        if (method != null) {
            try {
                method.invoke(iOException, iOException2);
            } catch (IllegalAccessException | InvocationTargetException unused) {
            }
        }
    }
}
