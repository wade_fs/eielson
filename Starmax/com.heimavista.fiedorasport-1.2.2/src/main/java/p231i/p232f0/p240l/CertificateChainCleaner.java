package p231i.p232f0.p240l;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.X509TrustManager;
import p231i.p232f0.p239k.Platform;

/* renamed from: i.f0.l.c */
public abstract class CertificateChainCleaner {
    /* renamed from: a */
    public static CertificateChainCleaner m17834a(X509TrustManager x509TrustManager) {
        return Platform.m17817d().mo27627a(x509TrustManager);
    }

    /* renamed from: a */
    public abstract List<Certificate> mo27636a(List<Certificate> list, String str);
}
