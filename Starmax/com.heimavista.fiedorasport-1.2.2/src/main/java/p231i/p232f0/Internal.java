package p231i.p232f0;

import java.net.Socket;
import javax.net.ssl.SSLSocket;
import p231i.Address;
import p231i.C5015r;
import p231i.ConnectionPool;
import p231i.ConnectionSpec;
import p231i.Response;
import p231i.Route;
import p231i.p232f0.p234f.RealConnection;
import p231i.p232f0.p234f.RouteDatabase;
import p231i.p232f0.p234f.StreamAllocation;

/* renamed from: i.f0.a */
public abstract class Internal {

    /* renamed from: a */
    public static Internal f11301a;

    /* renamed from: a */
    public abstract int mo27399a(Response.C4938a aVar);

    /* renamed from: a */
    public abstract RealConnection mo27400a(ConnectionPool jVar, Address aVar, StreamAllocation gVar, Route c0Var);

    /* renamed from: a */
    public abstract RouteDatabase mo27401a(ConnectionPool jVar);

    /* renamed from: a */
    public abstract Socket mo27402a(ConnectionPool jVar, Address aVar, StreamAllocation gVar);

    /* renamed from: a */
    public abstract void mo27403a(ConnectionSpec kVar, SSLSocket sSLSocket, boolean z);

    /* renamed from: a */
    public abstract void mo27404a(C5015r.C5016a aVar, String str);

    /* renamed from: a */
    public abstract void mo27405a(C5015r.C5016a aVar, String str, String str2);

    /* renamed from: a */
    public abstract boolean mo27406a(Address aVar, Address aVar2);

    /* renamed from: a */
    public abstract boolean mo27407a(ConnectionPool jVar, RealConnection cVar);

    /* renamed from: b */
    public abstract void mo27408b(ConnectionPool jVar, RealConnection cVar);
}
