package p231i.p232f0;

/* renamed from: i.f0.b */
public abstract class NamedRunnable implements Runnable {

    /* renamed from: P */
    protected final String f11302P;

    public NamedRunnable(String str, Object... objArr) {
        this.f11302P = C4951c.m17345a(str, objArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo27409b();

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f11302P);
        try {
            mo27409b();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
