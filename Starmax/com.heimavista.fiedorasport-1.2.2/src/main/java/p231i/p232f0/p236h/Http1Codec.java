package p231i.p232f0.p236h;

import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import p231i.C5015r;
import p231i.C5023y;
import p231i.HttpUrl;
import p231i.OkHttpClient;
import p231i.Response;
import p231i.ResponseBody;
import p231i.p232f0.C4951c;
import p231i.p232f0.Internal;
import p231i.p232f0.p234f.StreamAllocation;
import p231i.p232f0.p235g.HttpCodec;
import p231i.p232f0.p235g.HttpHeaders;
import p231i.p232f0.p235g.RealResponseBody;
import p231i.p232f0.p235g.RequestLine;
import p231i.p232f0.p235g.StatusLine;
import p244j.Buffer;
import p244j.BufferedSink;
import p244j.BufferedSource;
import p244j.C5049s;
import p244j.ForwardingTimeout;
import p244j.Okio;
import p244j.Sink;
import p244j.Timeout;

/* renamed from: i.f0.h.a */
public final class Http1Codec implements HttpCodec {

    /* renamed from: a */
    final OkHttpClient f11446a;

    /* renamed from: b */
    final StreamAllocation f11447b;

    /* renamed from: c */
    final BufferedSource f11448c;

    /* renamed from: d */
    final BufferedSink f11449d;

    /* renamed from: e */
    int f11450e = 0;

    /* renamed from: f */
    private long f11451f = 262144;

    /* renamed from: i.f0.h.a$b */
    /* compiled from: Http1Codec */
    private abstract class C4969b implements C5049s {

        /* renamed from: P */
        protected final ForwardingTimeout f11452P;

        /* renamed from: Q */
        protected boolean f11453Q;

        /* renamed from: R */
        protected long f11454R;

        private C4969b() {
            this.f11452P = new ForwardingTimeout(Http1Codec.this.f11448c.mo27416h());
            this.f11454R = 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo27512a(boolean z, IOException iOException) {
            Http1Codec aVar = Http1Codec.this;
            int i = aVar.f11450e;
            if (i != 6) {
                if (i == 5) {
                    aVar.mo27507a(this.f11452P);
                    Http1Codec aVar2 = Http1Codec.this;
                    aVar2.f11450e = 6;
                    StreamAllocation gVar = aVar2.f11447b;
                    if (gVar != null) {
                        gVar.mo27476a(!z, aVar2, this.f11454R, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + Http1Codec.this.f11450e);
            }
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            try {
                long b = Http1Codec.this.f11448c.mo27415b(cVar, j);
                if (b > 0) {
                    this.f11454R += b;
                }
                return b;
            } catch (IOException e) {
                mo27512a(false, e);
                throw e;
            }
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return this.f11452P;
        }
    }

    /* renamed from: i.f0.h.a$c */
    /* compiled from: Http1Codec */
    private final class C4970c implements Sink {

        /* renamed from: P */
        private final ForwardingTimeout f11456P = new ForwardingTimeout(Http1Codec.this.f11449d.mo27513h());

        /* renamed from: Q */
        private boolean f11457Q;

        C4970c() {
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            if (this.f11457Q) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                Http1Codec.this.f11449d.mo28046c(j);
                Http1Codec.this.f11449d.mo28033a("\r\n");
                Http1Codec.this.f11449d.mo27444a(cVar, j);
                Http1Codec.this.f11449d.mo28033a("\r\n");
            }
        }

        public synchronized void close() {
            if (!this.f11457Q) {
                this.f11457Q = true;
                Http1Codec.this.f11449d.mo28033a("0\r\n\r\n");
                Http1Codec.this.mo27507a(this.f11456P);
                Http1Codec.this.f11450e = 3;
            }
        }

        public synchronized void flush() {
            if (!this.f11457Q) {
                Http1Codec.this.f11449d.flush();
            }
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return this.f11456P;
        }
    }

    /* renamed from: i.f0.h.a$d */
    /* compiled from: Http1Codec */
    private class C4971d extends C4969b {

        /* renamed from: T */
        private final HttpUrl f11459T;

        /* renamed from: U */
        private long f11460U = -1;

        /* renamed from: V */
        private boolean f11461V = true;

        C4971d(HttpUrl sVar) {
            super();
            this.f11459T = sVar;
        }

        /* renamed from: a */
        private void m17560a() {
            if (this.f11460U != -1) {
                Http1Codec.this.f11448c.mo28066m();
            }
            try {
                this.f11460U = Http1Codec.this.f11448c.mo28069p();
                String trim = Http1Codec.this.f11448c.mo28066m().trim();
                if (this.f11460U < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f11460U + trim + "\"");
                } else if (this.f11460U == 0) {
                    this.f11461V = false;
                    HttpHeaders.m17500a(Http1Codec.this.f11446a.mo27791g(), this.f11459T, Http1Codec.this.mo27511e());
                    mo27512a(true, null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (super.f11453Q) {
                throw new IllegalStateException("closed");
            } else if (!this.f11461V) {
                return -1;
            } else {
                long j2 = this.f11460U;
                if (j2 == 0 || j2 == -1) {
                    m17560a();
                    if (!this.f11461V) {
                        return -1;
                    }
                }
                long b = super.mo27415b(cVar, Math.min(j, this.f11460U));
                if (b != -1) {
                    this.f11460U -= b;
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                mo27512a(false, protocolException);
                throw protocolException;
            }
        }

        public void close() {
            if (!super.f11453Q) {
                if (this.f11461V && !C4951c.m17354a(this, 100, TimeUnit.MILLISECONDS)) {
                    mo27512a(false, null);
                }
                super.f11453Q = true;
            }
        }
    }

    /* renamed from: i.f0.h.a$e */
    /* compiled from: Http1Codec */
    private final class C4972e implements Sink {

        /* renamed from: P */
        private final ForwardingTimeout f11463P = new ForwardingTimeout(Http1Codec.this.f11449d.mo27513h());

        /* renamed from: Q */
        private boolean f11464Q;

        /* renamed from: R */
        private long f11465R;

        C4972e(long j) {
            this.f11465R = j;
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            if (!this.f11464Q) {
                C4951c.m17351a(cVar.mo28051e(), 0, j);
                if (j <= this.f11465R) {
                    Http1Codec.this.f11449d.mo27444a(cVar, j);
                    this.f11465R -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.f11465R + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        public void close() {
            if (!this.f11464Q) {
                this.f11464Q = true;
                if (this.f11465R <= 0) {
                    Http1Codec.this.mo27507a(this.f11463P);
                    Http1Codec.this.f11450e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        public void flush() {
            if (!this.f11464Q) {
                Http1Codec.this.f11449d.flush();
            }
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return this.f11463P;
        }
    }

    /* renamed from: i.f0.h.a$f */
    /* compiled from: Http1Codec */
    private class C4973f extends C4969b {

        /* renamed from: T */
        private long f11467T;

        C4973f(Http1Codec aVar, long j) {
            super();
            this.f11467T = j;
            if (this.f11467T == 0) {
                mo27512a(true, null);
            }
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!super.f11453Q) {
                long j2 = this.f11467T;
                if (j2 == 0) {
                    return -1;
                }
                long b = super.mo27415b(cVar, Math.min(j2, j));
                if (b != -1) {
                    this.f11467T -= b;
                    if (this.f11467T == 0) {
                        mo27512a(true, null);
                    }
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                mo27512a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }

        public void close() {
            if (!super.f11453Q) {
                if (this.f11467T != 0 && !C4951c.m17354a(this, 100, TimeUnit.MILLISECONDS)) {
                    mo27512a(false, null);
                }
                super.f11453Q = true;
            }
        }
    }

    /* renamed from: i.f0.h.a$g */
    /* compiled from: Http1Codec */
    private class C4974g extends C4969b {

        /* renamed from: T */
        private boolean f11468T;

        C4974g(Http1Codec aVar) {
            super();
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (super.f11453Q) {
                throw new IllegalStateException("closed");
            } else if (this.f11468T) {
                return -1;
            } else {
                long b = super.mo27415b(cVar, j);
                if (b != -1) {
                    return b;
                }
                this.f11468T = true;
                mo27512a(true, null);
                return -1;
            }
        }

        public void close() {
            if (!super.f11453Q) {
                if (!this.f11468T) {
                    mo27512a(false, null);
                }
                super.f11453Q = true;
            }
        }
    }

    public Http1Codec(OkHttpClient vVar, StreamAllocation gVar, BufferedSource eVar, BufferedSink dVar) {
        this.f11446a = vVar;
        this.f11447b = gVar;
        this.f11448c = eVar;
        this.f11449d = dVar;
    }

    /* renamed from: f */
    private String m17540f() {
        String b = this.f11448c.mo28044b(this.f11451f);
        this.f11451f -= (long) b.length();
        return b;
    }

    /* renamed from: a */
    public Sink mo27485a(C5023y yVar, long j) {
        if ("chunked".equalsIgnoreCase(yVar.mo27820a("Transfer-Encoding"))) {
            return mo27509c();
        }
        if (j != -1) {
            return mo27504a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    /* renamed from: b */
    public void mo27488b() {
        this.f11449d.flush();
    }

    /* renamed from: c */
    public Sink mo27509c() {
        if (this.f11450e == 1) {
            this.f11450e = 2;
            return new C4970c();
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* renamed from: d */
    public C5049s mo27510d() {
        if (this.f11450e == 4) {
            StreamAllocation gVar = this.f11447b;
            if (gVar != null) {
                this.f11450e = 5;
                gVar.mo27479d();
                return new C4974g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* renamed from: e */
    public C5015r mo27511e() {
        C5015r.C5016a aVar = new C5015r.C5016a();
        while (true) {
            String f = m17540f();
            if (f.length() == 0) {
                return aVar.mo27747a();
            }
            Internal.f11301a.mo27404a(aVar, f);
        }
    }

    /* renamed from: b */
    public C5049s mo27508b(long j) {
        if (this.f11450e == 4) {
            this.f11450e = 5;
            return new C4973f(this, j);
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* renamed from: a */
    public void mo27487a(C5023y yVar) {
        mo27506a(yVar.mo27823c(), RequestLine.m17528a(yVar, this.f11447b.mo27477b().mo27457c().mo27372b().type()));
    }

    /* renamed from: a */
    public ResponseBody mo27484a(Response a0Var) {
        StreamAllocation gVar = this.f11447b;
        gVar.f11409f.mo27726e(gVar.f11408e);
        String b = a0Var.mo27318b("Content-Type");
        if (!HttpHeaders.m17503b(a0Var)) {
            return new RealResponseBody(b, 0, Okio.m18212a(mo27508b(0)));
        }
        if ("chunked".equalsIgnoreCase(a0Var.mo27318b("Transfer-Encoding"))) {
            return new RealResponseBody(b, -1, Okio.m18212a(mo27505a(a0Var.mo27313A().mo27827g())));
        }
        long a = HttpHeaders.m17496a(a0Var);
        if (a != -1) {
            return new RealResponseBody(b, a, Okio.m18212a(mo27508b(a)));
        }
        return new RealResponseBody(b, -1, Okio.m18212a(mo27510d()));
    }

    /* renamed from: a */
    public void mo27486a() {
        this.f11449d.flush();
    }

    /* renamed from: a */
    public void mo27506a(C5015r rVar, String str) {
        if (this.f11450e == 0) {
            this.f11449d.mo28033a(str).mo28033a("\r\n");
            int b = rVar.mo27739b();
            for (int i = 0; i < b; i++) {
                this.f11449d.mo28033a(rVar.mo27737a(i)).mo28033a(": ").mo28033a(rVar.mo27740b(i)).mo28033a("\r\n");
            }
            this.f11449d.mo28033a("\r\n");
            this.f11450e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* renamed from: a */
    public Response.C4938a mo27483a(boolean z) {
        int i = this.f11450e;
        if (i == 1 || i == 3) {
            try {
                StatusLine a = StatusLine.m17539a(m17540f());
                Response.C4938a aVar = new Response.C4938a();
                aVar.mo27338a(a.f11443a);
                aVar.mo27332a(a.f11444b);
                aVar.mo27340a(a.f11445c);
                aVar.mo27337a(mo27511e());
                if (z && a.f11444b == 100) {
                    return null;
                }
                if (a.f11444b == 100) {
                    this.f11450e = 3;
                    return aVar;
                }
                this.f11450e = 4;
                return aVar;
            } catch (EOFException e) {
                IOException iOException = new IOException("unexpected end of stream on " + this.f11447b);
                iOException.initCause(e);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.f11450e);
        }
    }

    /* renamed from: a */
    public Sink mo27504a(long j) {
        if (this.f11450e == 1) {
            this.f11450e = 2;
            return new C4972e(j);
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* renamed from: a */
    public C5049s mo27505a(HttpUrl sVar) {
        if (this.f11450e == 4) {
            this.f11450e = 5;
            return new C4971d(sVar);
        }
        throw new IllegalStateException("state: " + this.f11450e);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27507a(ForwardingTimeout iVar) {
        Timeout g = iVar.mo28120g();
        iVar.mo28112a(Timeout.f11916d);
        g.mo28113a();
        g.mo28116b();
    }
}
