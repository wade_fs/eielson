package p231i.p232f0.p235g;

import java.net.Proxy;
import p231i.C5023y;
import p231i.HttpUrl;

/* renamed from: i.f0.g.i */
public final class RequestLine {
    /* renamed from: a */
    public static String m17528a(C5023y yVar, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(yVar.mo27825e());
        sb.append(' ');
        if (m17529b(yVar, type)) {
            sb.append(yVar.mo27827g());
        } else {
            sb.append(m17527a(yVar.mo27827g()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    /* renamed from: b */
    private static boolean m17529b(C5023y yVar, Proxy.Type type) {
        return !yVar.mo27824d() && type == Proxy.Type.HTTP;
    }

    /* renamed from: a */
    public static String m17527a(HttpUrl sVar) {
        String c = sVar.mo27756c();
        String e = sVar.mo27758e();
        if (e == null) {
            return c;
        }
        return c + '?' + e;
    }
}
