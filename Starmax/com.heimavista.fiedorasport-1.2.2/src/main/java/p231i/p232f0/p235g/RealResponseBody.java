package p231i.p232f0.p235g;

import p231i.ResponseBody;
import p244j.BufferedSource;

/* renamed from: i.f0.g.h */
public final class RealResponseBody extends ResponseBody {

    /* renamed from: P */
    private final long f11437P;

    /* renamed from: Q */
    private final BufferedSource f11438Q;

    public RealResponseBody(String str, long j, BufferedSource eVar) {
        this.f11437P = j;
        this.f11438Q = eVar;
    }

    /* renamed from: a */
    public long mo27347a() {
        return this.f11437P;
    }

    /* renamed from: b */
    public BufferedSource mo27348b() {
        return this.f11438Q;
    }
}
