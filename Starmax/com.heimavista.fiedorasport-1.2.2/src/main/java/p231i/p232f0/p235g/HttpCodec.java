package p231i.p232f0.p235g;

import p231i.C5023y;
import p231i.Response;
import p231i.ResponseBody;
import p244j.Sink;

/* renamed from: i.f0.g.c */
public interface HttpCodec {
    /* renamed from: a */
    Response.C4938a mo27483a(boolean z);

    /* renamed from: a */
    ResponseBody mo27484a(Response a0Var);

    /* renamed from: a */
    Sink mo27485a(C5023y yVar, long j);

    /* renamed from: a */
    void mo27486a();

    /* renamed from: a */
    void mo27487a(C5023y yVar);

    /* renamed from: b */
    void mo27488b();
}
