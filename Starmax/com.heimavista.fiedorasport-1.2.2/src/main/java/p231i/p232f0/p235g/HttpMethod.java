package p231i.p232f0.p235g;

/* renamed from: i.f0.g.f */
public final class HttpMethod {
    /* renamed from: a */
    public static boolean m17509a(String str) {
        return str.equals("POST") || str.equals("PATCH") || str.equals("PUT") || str.equals("DELETE") || str.equals("MOVE");
    }

    /* renamed from: b */
    public static boolean m17510b(String str) {
        return !str.equals("GET") && !str.equals("HEAD");
    }

    /* renamed from: c */
    public static boolean m17511c(String str) {
        return !str.equals("PROPFIND");
    }

    /* renamed from: d */
    public static boolean m17512d(String str) {
        return str.equals("PROPFIND");
    }

    /* renamed from: e */
    public static boolean m17513e(String str) {
        return str.equals("POST") || str.equals("PUT") || str.equals("PATCH") || str.equals("PROPPATCH") || str.equals("REPORT");
    }
}
