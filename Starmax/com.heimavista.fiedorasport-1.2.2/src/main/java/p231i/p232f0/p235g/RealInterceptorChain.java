package p231i.p232f0.p235g;

import java.util.List;
import p231i.C5023y;
import p231i.Call;
import p231i.Connection;
import p231i.EventListener;
import p231i.Interceptor;
import p231i.Response;
import p231i.p232f0.p234f.RealConnection;
import p231i.p232f0.p234f.StreamAllocation;

/* renamed from: i.f0.g.g */
public final class RealInterceptorChain implements Interceptor.C5019a {

    /* renamed from: a */
    private final List<Interceptor> f11425a;

    /* renamed from: b */
    private final StreamAllocation f11426b;

    /* renamed from: c */
    private final HttpCodec f11427c;

    /* renamed from: d */
    private final RealConnection f11428d;

    /* renamed from: e */
    private final int f11429e;

    /* renamed from: f */
    private final C5023y f11430f;

    /* renamed from: g */
    private final Call f11431g;

    /* renamed from: h */
    private final EventListener f11432h;

    /* renamed from: i */
    private final int f11433i;

    /* renamed from: j */
    private final int f11434j;

    /* renamed from: k */
    private final int f11435k;

    /* renamed from: l */
    private int f11436l;

    public RealInterceptorChain(List<Interceptor> list, StreamAllocation gVar, HttpCodec cVar, RealConnection cVar2, int i, C5023y yVar, Call eVar, EventListener pVar, int i2, int i3, int i4) {
        this.f11425a = list;
        this.f11428d = cVar2;
        this.f11426b = gVar;
        this.f11427c = cVar;
        this.f11429e = i;
        this.f11430f = yVar;
        this.f11431g = eVar;
        this.f11432h = pVar;
        this.f11433i = i2;
        this.f11434j = i3;
        this.f11435k = i4;
    }

    /* renamed from: a */
    public int mo27490a() {
        return this.f11434j;
    }

    /* renamed from: b */
    public int mo27493b() {
        return this.f11435k;
    }

    /* renamed from: c */
    public int mo27494c() {
        return this.f11433i;
    }

    /* renamed from: d */
    public C5023y mo27495d() {
        return this.f11430f;
    }

    /* renamed from: e */
    public Call mo27496e() {
        return this.f11431g;
    }

    /* renamed from: f */
    public Connection mo27497f() {
        return this.f11428d;
    }

    /* renamed from: g */
    public EventListener mo27498g() {
        return this.f11432h;
    }

    /* renamed from: h */
    public HttpCodec mo27499h() {
        return this.f11427c;
    }

    /* renamed from: i */
    public StreamAllocation mo27500i() {
        return this.f11426b;
    }

    /* renamed from: a */
    public Response mo27491a(C5023y yVar) {
        return mo27492a(yVar, this.f11426b, this.f11427c, this.f11428d);
    }

    /* renamed from: a */
    public Response mo27492a(C5023y yVar, StreamAllocation gVar, HttpCodec cVar, RealConnection cVar2) {
        if (this.f11429e < this.f11425a.size()) {
            this.f11436l++;
            if (this.f11427c != null && !this.f11428d.mo27454a(yVar.mo27827g())) {
                throw new IllegalStateException("network interceptor " + this.f11425a.get(this.f11429e - 1) + " must retain the same host and port");
            } else if (this.f11427c == null || this.f11436l <= 1) {
                RealInterceptorChain gVar2 = new RealInterceptorChain(this.f11425a, gVar, cVar, cVar2, this.f11429e + 1, yVar, this.f11431g, this.f11432h, this.f11433i, this.f11434j, this.f11435k);
                Interceptor tVar = this.f11425a.get(this.f11429e);
                Response a = tVar.mo27414a(gVar2);
                if (cVar != null && this.f11429e + 1 < this.f11425a.size() && gVar2.f11436l != 1) {
                    throw new IllegalStateException("network interceptor " + tVar + " must call proceed() exactly once");
                } else if (a == null) {
                    throw new NullPointerException("interceptor " + tVar + " returned null");
                } else if (a.mo27315a() != null) {
                    return a;
                } else {
                    throw new IllegalStateException("interceptor " + tVar + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.f11425a.get(this.f11429e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }
}
