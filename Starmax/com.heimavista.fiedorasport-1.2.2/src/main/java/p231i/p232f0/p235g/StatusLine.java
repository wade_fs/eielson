package p231i.p232f0.p235g;

import java.net.ProtocolException;
import p231i.Protocol;

/* renamed from: i.f0.g.k */
public final class StatusLine {

    /* renamed from: a */
    public final Protocol f11443a;

    /* renamed from: b */
    public final int f11444b;

    /* renamed from: c */
    public final String f11445c;

    public StatusLine(Protocol wVar, int i, String str) {
        this.f11443a = wVar;
        this.f11444b = i;
        this.f11445c = str;
    }

    /* renamed from: a */
    public static StatusLine m17539a(String str) {
        Protocol wVar;
        String str2;
        int i = 9;
        if (str.startsWith("HTTP/1.")) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            }
            int charAt = str.charAt(7) - '0';
            if (charAt == 0) {
                wVar = Protocol.HTTP_1_0;
            } else if (charAt == 1) {
                wVar = Protocol.HTTP_1_1;
            } else {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else if (str.startsWith("ICY ")) {
            wVar = Protocol.HTTP_1_0;
            i = 4;
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        int i2 = i + 3;
        if (str.length() >= i2) {
            try {
                int parseInt = Integer.parseInt(str.substring(i, i2));
                if (str.length() <= i2) {
                    str2 = "";
                } else if (str.charAt(i2) == ' ') {
                    str2 = str.substring(i + 4);
                } else {
                    throw new ProtocolException("Unexpected status line: " + str);
                }
                return new StatusLine(wVar, parseInt, str2);
            } catch (NumberFormatException unused) {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f11443a == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1");
        sb.append(' ');
        sb.append(this.f11444b);
        if (this.f11445c != null) {
            sb.append(' ');
            sb.append(this.f11445c);
        }
        return sb.toString();
    }
}
