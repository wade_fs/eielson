package p231i.p232f0.p235g;

import java.net.ProtocolException;
import p231i.C5023y;
import p231i.Interceptor;
import p231i.Response;
import p231i.p232f0.C4951c;
import p231i.p232f0.p234f.RealConnection;
import p231i.p232f0.p234f.StreamAllocation;
import p244j.Buffer;
import p244j.BufferedSink;
import p244j.ForwardingSink;
import p244j.Okio;
import p244j.Sink;

/* renamed from: i.f0.g.b */
public final class CallServerInterceptor implements Interceptor {

    /* renamed from: a */
    private final boolean f11420a;

    /* renamed from: i.f0.g.b$a */
    /* compiled from: CallServerInterceptor */
    static final class C4966a extends ForwardingSink {

        /* renamed from: Q */
        long f11421Q;

        C4966a(Sink rVar) {
            super(rVar);
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            super.mo27444a(cVar, j);
            this.f11421Q += j;
        }
    }

    public CallServerInterceptor(boolean z) {
        this.f11420a = z;
    }

    /* renamed from: a */
    public Response mo27414a(Interceptor.C5019a aVar) {
        Response a0Var;
        RealInterceptorChain gVar = (RealInterceptorChain) aVar;
        HttpCodec h = gVar.mo27499h();
        StreamAllocation i = gVar.mo27500i();
        RealConnection cVar = (RealConnection) gVar.mo27497f();
        C5023y d = gVar.mo27495d();
        long currentTimeMillis = System.currentTimeMillis();
        gVar.mo27498g().mo27725d(gVar.mo27496e());
        h.mo27487a(d);
        gVar.mo27498g().mo27714a(gVar.mo27496e(), d);
        Response.C4938a aVar2 = null;
        if (HttpMethod.m17510b(d.mo27825e()) && d.mo27819a() != null) {
            if ("100-continue".equalsIgnoreCase(d.mo27820a("Expect"))) {
                h.mo27488b();
                gVar.mo27498g().mo27727f(gVar.mo27496e());
                aVar2 = h.mo27483a(true);
            }
            if (aVar2 == null) {
                gVar.mo27498g().mo27724c(gVar.mo27496e());
                C4966a aVar3 = new C4966a(h.mo27485a(d, d.mo27819a().mo27837a()));
                BufferedSink a = Okio.m18211a(aVar3);
                d.mo27819a().mo27838a(a);
                a.close();
                gVar.mo27498g().mo27710a(gVar.mo27496e(), aVar3.f11421Q);
            } else if (!cVar.mo27456b()) {
                i.mo27479d();
            }
        }
        h.mo27486a();
        if (aVar2 == null) {
            gVar.mo27498g().mo27727f(gVar.mo27496e());
            aVar2 = h.mo27483a(false);
        }
        aVar2.mo27339a(d);
        aVar2.mo27336a(i.mo27477b().mo27449a());
        aVar2.mo27343b(currentTimeMillis);
        aVar2.mo27333a(System.currentTimeMillis());
        Response a2 = aVar2.mo27342a();
        int d2 = a2.mo27321d();
        if (d2 == 100) {
            Response.C4938a a3 = h.mo27483a(false);
            a3.mo27339a(d);
            a3.mo27336a(i.mo27477b().mo27449a());
            a3.mo27343b(currentTimeMillis);
            a3.mo27333a(System.currentTimeMillis());
            a2 = a3.mo27342a();
            d2 = a2.mo27321d();
        }
        gVar.mo27498g().mo27711a(gVar.mo27496e(), a2);
        if (!this.f11420a || d2 != 101) {
            Response.C4938a w = a2.mo27328w();
            w.mo27335a(h.mo27484a(a2));
            a0Var = w.mo27342a();
        } else {
            Response.C4938a w2 = a2.mo27328w();
            w2.mo27335a(C4951c.f11305c);
            a0Var = w2.mo27342a();
        }
        if ("close".equalsIgnoreCase(a0Var.mo27313A().mo27820a("Connection")) || "close".equalsIgnoreCase(a0Var.mo27318b("Connection"))) {
            i.mo27479d();
        }
        if ((d2 != 204 && d2 != 205) || a0Var.mo27315a().mo27347a() <= 0) {
            return a0Var;
        }
        throw new ProtocolException("HTTP " + d2 + " had non-zero Content-Length: " + a0Var.mo27315a().mo27347a());
    }
}
