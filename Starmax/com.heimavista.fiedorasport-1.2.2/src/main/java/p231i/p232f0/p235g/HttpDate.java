package p231i.p232f0.p235g;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import p231i.p232f0.C4951c;

/* renamed from: i.f0.g.d */
public final class HttpDate {

    /* renamed from: a */
    private static final ThreadLocal<DateFormat> f11422a = new C4967a();

    /* renamed from: b */
    private static final String[] f11423b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z", "EEE MMM d yyyy HH:mm:ss z"};

    /* renamed from: c */
    private static final DateFormat[] f11424c = new DateFormat[f11423b.length];

    /* renamed from: i.f0.g.d$a */
    /* compiled from: HttpDate */
    class C4967a extends ThreadLocal<DateFormat> {
        C4967a() {
        }

        /* access modifiers changed from: protected */
        public DateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.setTimeZone(C4951c.f11307e);
            return simpleDateFormat;
        }
    }

    /* renamed from: a */
    public static Date m17493a(String str) {
        if (str.length() == 0) {
            return null;
        }
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = f11422a.get().parse(str, parsePosition);
        if (parsePosition.getIndex() == str.length()) {
            return parse;
        }
        synchronized (f11423b) {
            int length = f11423b.length;
            for (int i = 0; i < length; i++) {
                DateFormat dateFormat = f11424c[i];
                if (dateFormat == null) {
                    dateFormat = new SimpleDateFormat(f11423b[i], Locale.US);
                    dateFormat.setTimeZone(C4951c.f11307e);
                    f11424c[i] = dateFormat;
                }
                parsePosition.setIndex(0);
                Date parse2 = dateFormat.parse(str, parsePosition);
                if (parsePosition.getIndex() != 0) {
                    return parse2;
                }
            }
            return null;
        }
    }

    /* renamed from: a */
    public static String m17492a(Date date) {
        return f11422a.get().format(date);
    }
}
