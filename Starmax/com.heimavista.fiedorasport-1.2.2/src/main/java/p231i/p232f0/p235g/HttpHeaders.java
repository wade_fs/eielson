package p231i.p232f0.p235g;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import p231i.C5015r;
import p231i.C5023y;
import p231i.Cookie;
import p231i.CookieJar;
import p231i.HttpUrl;
import p231i.Response;
import p231i.p232f0.C4951c;

/* renamed from: i.f0.g.e */
public final class HttpHeaders {
    static {
        Pattern.compile(" +([^ \"=]*)=(:?\"([^\"]*)\"|([^ \"=]*)) *(:?,|$)");
    }

    /* renamed from: a */
    public static long m17496a(Response a0Var) {
        return m17497a(a0Var.mo27323g());
    }

    /* renamed from: b */
    public static boolean m17504b(C5015r rVar) {
        return m17505c(rVar).contains("*");
    }

    /* renamed from: c */
    public static boolean m17506c(Response a0Var) {
        return m17504b(a0Var.mo27323g());
    }

    /* renamed from: d */
    private static Set<String> m17507d(Response a0Var) {
        return m17505c(a0Var.mo27323g());
    }

    /* renamed from: e */
    public static C5015r m17508e(Response a0Var) {
        return m17499a(a0Var.mo27327v().mo27313A().mo27823c(), a0Var.mo27323g());
    }

    /* renamed from: a */
    public static long m17497a(C5015r rVar) {
        return m17498a(rVar.mo27738a("Content-Length"));
    }

    /* renamed from: b */
    public static boolean m17503b(Response a0Var) {
        if (a0Var.mo27313A().mo27825e().equals("HEAD")) {
            return false;
        }
        int d = a0Var.mo27321d();
        if (((d >= 100 && d < 200) || d == 204 || d == 304) && m17496a(a0Var) == -1 && !"chunked".equalsIgnoreCase(a0Var.mo27318b("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    /* renamed from: c */
    public static Set<String> m17505c(C5015r rVar) {
        Set<String> emptySet = Collections.emptySet();
        int b = rVar.mo27739b();
        Set<String> set = emptySet;
        for (int i = 0; i < b; i++) {
            if ("Vary".equalsIgnoreCase(rVar.mo27737a(i))) {
                String b2 = rVar.mo27740b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String str : b2.split(",")) {
                    set.add(str.trim());
                }
            }
        }
        return set;
    }

    /* renamed from: a */
    private static long m17498a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    /* renamed from: a */
    public static boolean m17501a(Response a0Var, C5015r rVar, C5023y yVar) {
        for (String str : m17507d(a0Var)) {
            if (!C4951c.m17356a(rVar.mo27741b(str), yVar.mo27822b(str))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static C5015r m17499a(C5015r rVar, C5015r rVar2) {
        Set<String> c = m17505c(rVar2);
        if (c.isEmpty()) {
            return new C5015r.C5016a().mo27747a();
        }
        C5015r.C5016a aVar = new C5015r.C5016a();
        int b = rVar.mo27739b();
        for (int i = 0; i < b; i++) {
            String a = rVar.mo27737a(i);
            if (c.contains(a)) {
                aVar.mo27746a(a, rVar.mo27740b(i));
            }
        }
        return aVar.mo27747a();
    }

    /* renamed from: b */
    public static int m17502b(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == 9)) {
            i++;
        }
        return i;
    }

    /* renamed from: a */
    public static void m17500a(CookieJar mVar, HttpUrl sVar, C5015r rVar) {
        if (mVar != CookieJar.f11731a) {
            List<Cookie> a = Cookie.m17880a(sVar, rVar);
            if (!a.isEmpty()) {
                mVar.mo27702a(sVar, a);
            }
        }
    }

    /* renamed from: a */
    public static int m17495a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    /* renamed from: a */
    public static int m17494a(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
