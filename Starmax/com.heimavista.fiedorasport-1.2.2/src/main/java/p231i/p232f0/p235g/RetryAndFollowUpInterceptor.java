package p231i.p232f0.p235g;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import p231i.Address;
import p231i.C5023y;
import p231i.Call;
import p231i.CertificatePinner;
import p231i.EventListener;
import p231i.HttpUrl;
import p231i.Interceptor;
import p231i.OkHttpClient;
import p231i.RequestBody;
import p231i.Response;
import p231i.ResponseBody;
import p231i.Route;
import p231i.p232f0.C4951c;
import p231i.p232f0.p234f.RouteException;
import p231i.p232f0.p234f.StreamAllocation;
import p231i.p232f0.p237i.ConnectionShutdownException;

/* renamed from: i.f0.g.j */
public final class RetryAndFollowUpInterceptor implements Interceptor {

    /* renamed from: a */
    private final OkHttpClient f11439a;

    /* renamed from: b */
    private final boolean f11440b;

    /* renamed from: c */
    private Object f11441c;

    /* renamed from: d */
    private volatile boolean f11442d;

    public RetryAndFollowUpInterceptor(OkHttpClient vVar, boolean z) {
        this.f11439a = vVar;
        this.f11440b = z;
    }

    /* renamed from: a */
    public boolean mo27502a() {
        return this.f11442d;
    }

    /* renamed from: a */
    public void mo27501a(Object obj) {
        this.f11441c = obj;
    }

    /* renamed from: a */
    public Response mo27414a(Interceptor.C5019a aVar) {
        C5023y d = aVar.mo27495d();
        RealInterceptorChain gVar = (RealInterceptorChain) aVar;
        Call e = gVar.mo27496e();
        EventListener g = gVar.mo27498g();
        StreamAllocation gVar2 = new StreamAllocation(this.f11439a.mo27789e(), m17531a(d.mo27827g()), e, g, this.f11441c);
        Response a0Var = null;
        int i = 0;
        while (!this.f11442d) {
            try {
                Response a = gVar.mo27492a(d, gVar2, null, null);
                if (a0Var != null) {
                    Response.C4938a w = a.mo27328w();
                    Response.C4938a w2 = a0Var.mo27328w();
                    w2.mo27335a((ResponseBody) null);
                    w.mo27345c(w2.mo27342a());
                    a = w.mo27342a();
                }
                C5023y a2 = m17532a(a, gVar2.mo27481f());
                if (a2 == null) {
                    if (!this.f11440b) {
                        gVar2.mo27480e();
                    }
                    return a;
                }
                C4951c.m17352a(a.mo27315a());
                int i2 = i + 1;
                if (i2 <= 20) {
                    a2.mo27819a();
                    if (!m17533a(a, a2.mo27827g())) {
                        gVar2.mo27480e();
                        gVar2 = new StreamAllocation(this.f11439a.mo27789e(), m17531a(a2.mo27827g()), e, g, this.f11441c);
                    } else if (gVar2.mo27471a() != null) {
                        throw new IllegalStateException("Closing the body of " + a + " didn't close its backing stream. Bad interceptor?");
                    }
                    a0Var = a;
                    d = a2;
                    i = i2;
                } else {
                    gVar2.mo27480e();
                    throw new ProtocolException("Too many follow-up requests: " + i2);
                }
            } catch (RouteException e2) {
                if (!m17534a(e2.mo27463a(), gVar2, false, d)) {
                    throw e2.mo27463a();
                }
            } catch (IOException e3) {
                if (!m17534a(e3, gVar2, !(e3 instanceof ConnectionShutdownException), d)) {
                    throw e3;
                }
            } catch (Throwable th) {
                gVar2.mo27475a((IOException) null);
                gVar2.mo27480e();
                throw th;
            }
        }
        gVar2.mo27480e();
        throw new IOException("Canceled");
    }

    /* renamed from: a */
    private Address m17531a(HttpUrl sVar) {
        CertificatePinner gVar;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (sVar.mo27762h()) {
            SSLSocketFactory A = this.f11439a.mo27783A();
            hostnameVerifier = this.f11439a.mo27797m();
            sSLSocketFactory = A;
            gVar = this.f11439a.mo27787c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            gVar = null;
        }
        return new Address(sVar.mo27761g(), sVar.mo27765j(), this.f11439a.mo27793i(), this.f11439a.mo27808z(), sSLSocketFactory, hostnameVerifier, gVar, this.f11439a.mo27804v(), this.f11439a.mo27803u(), this.f11439a.mo27802t(), this.f11439a.mo27790f(), this.f11439a.mo27805w());
    }

    /* renamed from: a */
    private boolean m17534a(IOException iOException, StreamAllocation gVar, boolean z, C5023y yVar) {
        gVar.mo27475a(iOException);
        if (!this.f11439a.mo27807y()) {
            return false;
        }
        if (z) {
            yVar.mo27819a();
        }
        if (m17535a(iOException, z) && gVar.mo27478c()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    private boolean m17535a(IOException iOException, boolean z) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                return false;
            }
            return true;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: a */
    private C5023y m17532a(Response a0Var, Route c0Var) {
        String b;
        HttpUrl b2;
        Proxy proxy;
        if (a0Var != null) {
            int d = a0Var.mo27321d();
            String e = a0Var.mo27313A().mo27825e();
            RequestBody zVar = null;
            if (d == 307 || d == 308) {
                if (!e.equals("GET") && !e.equals("HEAD")) {
                    return null;
                }
            } else if (d == 401) {
                return this.f11439a.mo27785a().mo27346a(c0Var, a0Var);
            } else {
                if (d != 503) {
                    if (d == 407) {
                        if (c0Var != null) {
                            proxy = c0Var.mo27372b();
                        } else {
                            proxy = this.f11439a.mo27803u();
                        }
                        if (proxy.type() == Proxy.Type.HTTP) {
                            return this.f11439a.mo27804v().mo27346a(c0Var, a0Var);
                        }
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    } else if (d != 408) {
                        switch (d) {
                            case 300:
                            case 301:
                            case 302:
                            case 303:
                                break;
                            default:
                                return null;
                        }
                    } else if (!this.f11439a.mo27807y()) {
                        return null;
                    } else {
                        a0Var.mo27313A().mo27819a();
                        if ((a0Var.mo27329x() == null || a0Var.mo27329x().mo27321d() != 408) && m17530a(a0Var, 0) <= 0) {
                            return a0Var.mo27313A();
                        }
                        return null;
                    }
                } else if ((a0Var.mo27329x() == null || a0Var.mo27329x().mo27321d() != 503) && m17530a(a0Var, Integer.MAX_VALUE) == 0) {
                    return a0Var.mo27313A();
                } else {
                    return null;
                }
            }
            if (!this.f11439a.mo27795k() || (b = a0Var.mo27318b("Location")) == null || (b2 = a0Var.mo27313A().mo27827g().mo27754b(b)) == null) {
                return null;
            }
            if (!b2.mo27768m().equals(a0Var.mo27313A().mo27827g().mo27768m()) && !this.f11439a.mo27796l()) {
                return null;
            }
            C5023y.C5024a f = a0Var.mo27313A().mo27826f();
            if (HttpMethod.m17510b(e)) {
                boolean d2 = HttpMethod.m17512d(e);
                if (HttpMethod.m17511c(e)) {
                    f.mo27833a("GET", (RequestBody) null);
                } else {
                    if (d2) {
                        zVar = a0Var.mo27313A().mo27819a();
                    }
                    f.mo27833a(e, zVar);
                }
                if (!d2) {
                    f.mo27832a("Transfer-Encoding");
                    f.mo27832a("Content-Length");
                    f.mo27832a("Content-Type");
                }
            }
            if (!m17533a(a0Var, b2)) {
                f.mo27832a("Authorization");
            }
            f.mo27831a(b2);
            return f.mo27835a();
        }
        throw new IllegalStateException();
    }

    /* renamed from: a */
    private int m17530a(Response a0Var, int i) {
        String b = a0Var.mo27318b("Retry-After");
        if (b == null) {
            return i;
        }
        if (b.matches("\\d+")) {
            return Integer.valueOf(b).intValue();
        }
        return Integer.MAX_VALUE;
    }

    /* renamed from: a */
    private boolean m17533a(Response a0Var, HttpUrl sVar) {
        HttpUrl g = a0Var.mo27313A().mo27827g();
        return g.mo27761g().equals(sVar.mo27761g()) && g.mo27765j() == sVar.mo27765j() && g.mo27768m().equals(sVar.mo27768m());
    }
}
