package p231i.p232f0.p235g;

import java.util.List;
import p231i.C5015r;
import p231i.C5023y;
import p231i.Cookie;
import p231i.CookieJar;
import p231i.Interceptor;
import p231i.MediaType;
import p231i.RequestBody;
import p231i.Response;
import p231i.p232f0.C4951c;
import p231i.p232f0.C4954d;
import p244j.GzipSource;
import p244j.Okio;

/* renamed from: i.f0.g.a */
public final class BridgeInterceptor implements Interceptor {

    /* renamed from: a */
    private final CookieJar f11419a;

    public BridgeInterceptor(CookieJar mVar) {
        this.f11419a = mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(i.s, boolean):java.lang.String
     arg types: [i.s, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(i.s, boolean):java.lang.String */
    /* renamed from: a */
    public Response mo27414a(Interceptor.C5019a aVar) {
        C5023y d = aVar.mo27495d();
        C5023y.C5024a f = d.mo27826f();
        RequestBody a = d.mo27819a();
        if (a != null) {
            MediaType b = a.mo27839b();
            if (b == null) {
                long a2 = a.mo27837a();
                if (a2 != -1) {
                    f.mo27834a("Content-Length", Long.toString(a2));
                    f.mo27832a("Transfer-Encoding");
                } else {
                    f.mo27834a("Transfer-Encoding", "chunked");
                    f.mo27832a("Content-Length");
                }
            } else {
                b.toString();
                throw null;
            }
        }
        boolean z = false;
        if (d.mo27820a("Host") == null) {
            f.mo27834a("Host", C4951c.m17343a(d.mo27827g(), false));
        }
        if (d.mo27820a("Connection") == null) {
            f.mo27834a("Connection", "Keep-Alive");
        }
        if (d.mo27820a("Accept-Encoding") == null && d.mo27820a("Range") == null) {
            z = true;
            f.mo27834a("Accept-Encoding", "gzip");
        }
        List<Cookie> a3 = this.f11419a.mo27701a(d.mo27827g());
        if (!a3.isEmpty()) {
            f.mo27834a("Cookie", m17482a(a3));
        }
        if (d.mo27820a("User-Agent") == null) {
            f.mo27834a("User-Agent", C4954d.m17369a());
        }
        Response a4 = aVar.mo27491a(f.mo27835a());
        HttpHeaders.m17500a(this.f11419a, d.mo27827g(), a4.mo27323g());
        Response.C4938a w = a4.mo27328w();
        w.mo27339a(d);
        if (z && "gzip".equalsIgnoreCase(a4.mo27318b("Content-Encoding")) && HttpHeaders.m17503b(a4)) {
            GzipSource jVar = new GzipSource(a4.mo27315a().mo27348b());
            C5015r.C5016a a5 = a4.mo27323g().mo27736a();
            a5.mo27750c("Content-Encoding");
            a5.mo27750c("Content-Length");
            w.mo27337a(a5.mo27747a());
            w.mo27335a(new RealResponseBody(a4.mo27318b("Content-Type"), -1, Okio.m18212a(jVar)));
        }
        return w.mo27342a();
    }

    /* renamed from: a */
    private String m17482a(List<Cookie> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            Cookie lVar = list.get(i);
            sb.append(lVar.mo27695a());
            sb.append('=');
            sb.append(lVar.mo27697b());
        }
        return sb.toString();
    }
}
