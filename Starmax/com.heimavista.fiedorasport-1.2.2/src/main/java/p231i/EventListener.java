package p231i;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;

/* renamed from: i.p */
public abstract class EventListener {

    /* renamed from: a */
    public static final EventListener f11740a = new C5012a();

    /* renamed from: i.p$a */
    /* compiled from: EventListener */
    class C5012a extends EventListener {
        C5012a() {
        }
    }

    /* renamed from: i.p$b */
    /* compiled from: EventListener */
    class C5013b implements C5014c {
        C5013b() {
        }

        /* renamed from: a */
        public EventListener mo27729a(Call eVar) {
            return EventListener.this;
        }
    }

    /* renamed from: i.p$c */
    /* compiled from: EventListener */
    public interface C5014c {
        /* renamed from: a */
        EventListener mo27729a(Call eVar);
    }

    /* renamed from: a */
    static C5014c m17900a(EventListener pVar) {
        return new C5013b();
    }

    /* renamed from: a */
    public void mo27709a(Call eVar) {
    }

    /* renamed from: a */
    public void mo27710a(Call eVar, long j) {
    }

    /* renamed from: a */
    public void mo27711a(Call eVar, Response a0Var) {
    }

    /* renamed from: a */
    public void mo27712a(Call eVar, Connection iVar) {
    }

    /* renamed from: a */
    public void mo27713a(Call eVar, Handshake qVar) {
    }

    /* renamed from: a */
    public void mo27714a(Call eVar, C5023y yVar) {
    }

    /* renamed from: a */
    public void mo27715a(Call eVar, IOException iOException) {
    }

    /* renamed from: a */
    public void mo27716a(Call eVar, String str) {
    }

    /* renamed from: a */
    public void mo27717a(Call eVar, String str, List<InetAddress> list) {
    }

    /* renamed from: a */
    public void mo27718a(Call eVar, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    /* renamed from: a */
    public void mo27719a(Call eVar, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol wVar) {
    }

    /* renamed from: a */
    public void mo27720a(Call eVar, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol wVar, IOException iOException) {
    }

    /* renamed from: b */
    public void mo27721b(Call eVar) {
    }

    /* renamed from: b */
    public void mo27722b(Call eVar, long j) {
    }

    /* renamed from: b */
    public void mo27723b(Call eVar, Connection iVar) {
    }

    /* renamed from: c */
    public void mo27724c(Call eVar) {
    }

    /* renamed from: d */
    public void mo27725d(Call eVar) {
    }

    /* renamed from: e */
    public void mo27726e(Call eVar) {
    }

    /* renamed from: f */
    public void mo27727f(Call eVar) {
    }

    /* renamed from: g */
    public void mo27728g(Call eVar) {
    }
}
