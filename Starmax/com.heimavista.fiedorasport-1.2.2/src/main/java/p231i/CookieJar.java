package p231i;

import java.util.Collections;
import java.util.List;

/* renamed from: i.m */
public interface CookieJar {

    /* renamed from: a */
    public static final CookieJar f11731a = new C5009a();

    /* renamed from: i.m$a */
    /* compiled from: CookieJar */
    class C5009a implements CookieJar {
        C5009a() {
        }

        /* renamed from: a */
        public List<Cookie> mo27701a(HttpUrl sVar) {
            return Collections.emptyList();
        }

        /* renamed from: a */
        public void mo27702a(HttpUrl sVar, List<Cookie> list) {
        }
    }

    /* renamed from: a */
    List<Cookie> mo27701a(HttpUrl sVar);

    /* renamed from: a */
    void mo27702a(HttpUrl sVar, List<Cookie> list);
}
