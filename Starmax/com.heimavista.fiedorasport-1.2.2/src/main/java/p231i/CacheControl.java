package p231i;

import java.util.concurrent.TimeUnit;

/* renamed from: i.d */
public final class CacheControl {

    /* renamed from: n */
    public static final CacheControl f11272n;

    /* renamed from: a */
    private final boolean f11273a;

    /* renamed from: b */
    private final boolean f11274b;

    /* renamed from: c */
    private final int f11275c;

    /* renamed from: d */
    private final int f11276d;

    /* renamed from: e */
    private final boolean f11277e;

    /* renamed from: f */
    private final boolean f11278f;

    /* renamed from: g */
    private final boolean f11279g;

    /* renamed from: h */
    private final int f11280h;

    /* renamed from: i */
    private final int f11281i;

    /* renamed from: j */
    private final boolean f11282j;

    /* renamed from: k */
    private final boolean f11283k;

    /* renamed from: l */
    private final boolean f11284l;

    /* renamed from: m */
    String f11285m;

    static {
        C4948a aVar = new C4948a();
        aVar.mo27391b();
        aVar.mo27390a();
        C4948a aVar2 = new C4948a();
        aVar2.mo27393d();
        aVar2.mo27389a(Integer.MAX_VALUE, TimeUnit.SECONDS);
        f11272n = aVar2.mo27390a();
    }

    private CacheControl(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, String str) {
        this.f11273a = z;
        this.f11274b = z2;
        this.f11275c = i;
        this.f11276d = i2;
        this.f11277e = z3;
        this.f11278f = z4;
        this.f11279g = z5;
        this.f11280h = i3;
        this.f11281i = i4;
        this.f11282j = z6;
        this.f11283k = z7;
        this.f11284l = z8;
        this.f11285m = str;
    }

    /* renamed from: k */
    private String m17304k() {
        StringBuilder sb = new StringBuilder();
        if (this.f11273a) {
            sb.append("no-cache, ");
        }
        if (this.f11274b) {
            sb.append("no-store, ");
        }
        if (this.f11275c != -1) {
            sb.append("max-age=");
            sb.append(this.f11275c);
            sb.append(", ");
        }
        if (this.f11276d != -1) {
            sb.append("s-maxage=");
            sb.append(this.f11276d);
            sb.append(", ");
        }
        if (this.f11277e) {
            sb.append("private, ");
        }
        if (this.f11278f) {
            sb.append("public, ");
        }
        if (this.f11279g) {
            sb.append("must-revalidate, ");
        }
        if (this.f11280h != -1) {
            sb.append("max-stale=");
            sb.append(this.f11280h);
            sb.append(", ");
        }
        if (this.f11281i != -1) {
            sb.append("min-fresh=");
            sb.append(this.f11281i);
            sb.append(", ");
        }
        if (this.f11282j) {
            sb.append("only-if-cached, ");
        }
        if (this.f11283k) {
            sb.append("no-transform, ");
        }
        if (this.f11284l) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    /* renamed from: a */
    public boolean mo27378a() {
        return this.f11284l;
    }

    /* renamed from: b */
    public boolean mo27379b() {
        return this.f11277e;
    }

    /* renamed from: c */
    public boolean mo27380c() {
        return this.f11278f;
    }

    /* renamed from: d */
    public int mo27381d() {
        return this.f11275c;
    }

    /* renamed from: e */
    public int mo27382e() {
        return this.f11280h;
    }

    /* renamed from: f */
    public int mo27383f() {
        return this.f11281i;
    }

    /* renamed from: g */
    public boolean mo27384g() {
        return this.f11279g;
    }

    /* renamed from: h */
    public boolean mo27385h() {
        return this.f11273a;
    }

    /* renamed from: i */
    public boolean mo27386i() {
        return this.f11274b;
    }

    /* renamed from: j */
    public boolean mo27387j() {
        return this.f11282j;
    }

    public String toString() {
        String str = this.f11285m;
        if (str != null) {
            return str;
        }
        String k = m17304k();
        this.f11285m = k;
        return k;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static p231i.CacheControl m17303a(p231i.C5015r r22) {
        /*
            r0 = r22
            int r1 = r22.mo27739b()
            r6 = 0
            r7 = 1
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = -1
            r12 = -1
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = -1
            r17 = -1
            r18 = 0
            r19 = 0
            r20 = 0
        L_0x001a:
            if (r6 >= r1) goto L_0x0146
            java.lang.String r2 = r0.mo27737a(r6)
            java.lang.String r4 = r0.mo27740b(r6)
            java.lang.String r3 = "Cache-Control"
            boolean r3 = r2.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0031
            if (r8 == 0) goto L_0x002f
            goto L_0x0039
        L_0x002f:
            r8 = r4
            goto L_0x003a
        L_0x0031:
            java.lang.String r3 = "Pragma"
            boolean r2 = r2.equalsIgnoreCase(r3)
            if (r2 == 0) goto L_0x013f
        L_0x0039:
            r7 = 0
        L_0x003a:
            r2 = 0
        L_0x003b:
            int r3 = r4.length()
            if (r2 >= r3) goto L_0x013f
            java.lang.String r3 = "=,;"
            int r3 = p231i.p232f0.p235g.HttpHeaders.m17495a(r4, r2, r3)
            java.lang.String r2 = r4.substring(r2, r3)
            java.lang.String r2 = r2.trim()
            int r5 = r4.length()
            if (r3 == r5) goto L_0x0099
            char r5 = r4.charAt(r3)
            r0 = 44
            if (r5 == r0) goto L_0x0099
            char r0 = r4.charAt(r3)
            r5 = 59
            if (r0 != r5) goto L_0x0066
            goto L_0x0099
        L_0x0066:
            int r3 = r3 + 1
            int r0 = p231i.p232f0.p235g.HttpHeaders.m17502b(r4, r3)
            int r3 = r4.length()
            if (r0 >= r3) goto L_0x0089
            char r3 = r4.charAt(r0)
            r5 = 34
            if (r3 != r5) goto L_0x0089
            int r0 = r0 + 1
            java.lang.String r3 = "\""
            int r3 = p231i.p232f0.p235g.HttpHeaders.m17495a(r4, r0, r3)
            java.lang.String r0 = r4.substring(r0, r3)
            r5 = 1
            int r3 = r3 + r5
            goto L_0x009d
        L_0x0089:
            r5 = 1
            java.lang.String r3 = ",;"
            int r3 = p231i.p232f0.p235g.HttpHeaders.m17495a(r4, r0, r3)
            java.lang.String r0 = r4.substring(r0, r3)
            java.lang.String r0 = r0.trim()
            goto L_0x009d
        L_0x0099:
            r5 = 1
            int r3 = r3 + 1
            r0 = 0
        L_0x009d:
            java.lang.String r5 = "no-cache"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00a9
            r5 = -1
            r9 = 1
            goto L_0x013a
        L_0x00a9:
            java.lang.String r5 = "no-store"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00b5
            r5 = -1
            r10 = 1
            goto L_0x013a
        L_0x00b5:
            java.lang.String r5 = "max-age"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00c5
            r5 = -1
            int r0 = p231i.p232f0.p235g.HttpHeaders.m17494a(r0, r5)
            r11 = r0
            goto L_0x013a
        L_0x00c5:
            java.lang.String r5 = "s-maxage"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00d5
            r5 = -1
            int r0 = p231i.p232f0.p235g.HttpHeaders.m17494a(r0, r5)
            r12 = r0
            goto L_0x013a
        L_0x00d5:
            java.lang.String r5 = "private"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00e0
            r5 = -1
            r13 = 1
            goto L_0x013a
        L_0x00e0:
            java.lang.String r5 = "public"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00eb
            r5 = -1
            r14 = 1
            goto L_0x013a
        L_0x00eb:
            java.lang.String r5 = "must-revalidate"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x00f6
            r5 = -1
            r15 = 1
            goto L_0x013a
        L_0x00f6:
            java.lang.String r5 = "max-stale"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x0109
            r2 = 2147483647(0x7fffffff, float:NaN)
            int r0 = p231i.p232f0.p235g.HttpHeaders.m17494a(r0, r2)
            r16 = r0
            r5 = -1
            goto L_0x013a
        L_0x0109:
            java.lang.String r5 = "min-fresh"
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x0119
            r5 = -1
            int r0 = p231i.p232f0.p235g.HttpHeaders.m17494a(r0, r5)
            r17 = r0
            goto L_0x013a
        L_0x0119:
            r5 = -1
            java.lang.String r0 = "only-if-cached"
            boolean r0 = r0.equalsIgnoreCase(r2)
            if (r0 == 0) goto L_0x0125
            r18 = 1
            goto L_0x013a
        L_0x0125:
            java.lang.String r0 = "no-transform"
            boolean r0 = r0.equalsIgnoreCase(r2)
            if (r0 == 0) goto L_0x0130
            r19 = 1
            goto L_0x013a
        L_0x0130:
            java.lang.String r0 = "immutable"
            boolean r0 = r0.equalsIgnoreCase(r2)
            if (r0 == 0) goto L_0x013a
            r20 = 1
        L_0x013a:
            r0 = r22
            r2 = r3
            goto L_0x003b
        L_0x013f:
            r5 = -1
            int r6 = r6 + 1
            r0 = r22
            goto L_0x001a
        L_0x0146:
            if (r7 != 0) goto L_0x014b
            r21 = 0
            goto L_0x014d
        L_0x014b:
            r21 = r8
        L_0x014d:
            i.d r0 = new i.d
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p231i.CacheControl.m17303a(i.r):i.d");
    }

    /* renamed from: i.d$a */
    /* compiled from: CacheControl */
    public static final class C4948a {

        /* renamed from: a */
        boolean f11286a;

        /* renamed from: b */
        boolean f11287b;

        /* renamed from: c */
        int f11288c = -1;

        /* renamed from: d */
        int f11289d = -1;

        /* renamed from: e */
        int f11290e = -1;

        /* renamed from: f */
        boolean f11291f;

        /* renamed from: g */
        boolean f11292g;

        /* renamed from: h */
        boolean f11293h;

        /* renamed from: a */
        public C4948a mo27389a(int i, TimeUnit timeUnit) {
            if (i >= 0) {
                long seconds = timeUnit.toSeconds((long) i);
                this.f11289d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
                return this;
            }
            throw new IllegalArgumentException("maxStale < 0: " + i);
        }

        /* renamed from: b */
        public C4948a mo27391b() {
            this.f11286a = true;
            return this;
        }

        /* renamed from: c */
        public C4948a mo27392c() {
            this.f11287b = true;
            return this;
        }

        /* renamed from: d */
        public C4948a mo27393d() {
            this.f11291f = true;
            return this;
        }

        /* renamed from: a */
        public CacheControl mo27390a() {
            return new CacheControl(this);
        }
    }

    CacheControl(C4948a aVar) {
        this.f11273a = aVar.f11286a;
        this.f11274b = aVar.f11287b;
        this.f11275c = aVar.f11288c;
        this.f11276d = -1;
        this.f11277e = false;
        this.f11278f = false;
        this.f11279g = false;
        this.f11280h = aVar.f11289d;
        this.f11281i = aVar.f11290e;
        this.f11282j = aVar.f11291f;
        this.f11283k = aVar.f11292g;
        this.f11284l = aVar.f11293h;
    }
}
