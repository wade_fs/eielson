package p231i;

import java.net.InetSocketAddress;
import java.net.Proxy;

/* renamed from: i.c0 */
public final class Route {

    /* renamed from: a */
    final Address f11269a;

    /* renamed from: b */
    final Proxy f11270b;

    /* renamed from: c */
    final InetSocketAddress f11271c;

    public Route(Address aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.f11269a = aVar;
            this.f11270b = proxy;
            this.f11271c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    /* renamed from: a */
    public Address mo27371a() {
        return this.f11269a;
    }

    /* renamed from: b */
    public Proxy mo27372b() {
        return this.f11270b;
    }

    /* renamed from: c */
    public boolean mo27373c() {
        return this.f11269a.f11207i != null && this.f11270b.type() == Proxy.Type.HTTP;
    }

    /* renamed from: d */
    public InetSocketAddress mo27374d() {
        return this.f11271c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Route) {
            Route c0Var = (Route) obj;
            return c0Var.f11269a.equals(this.f11269a) && c0Var.f11270b.equals(this.f11270b) && c0Var.f11271c.equals(this.f11271c);
        }
    }

    public int hashCode() {
        return ((((527 + this.f11269a.hashCode()) * 31) + this.f11270b.hashCode()) * 31) + this.f11271c.hashCode();
    }

    public String toString() {
        return "Route{" + this.f11271c + "}";
    }
}
