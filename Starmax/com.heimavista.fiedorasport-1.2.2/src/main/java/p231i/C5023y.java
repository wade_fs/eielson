package p231i;

import java.util.List;
import p231i.C5015r;
import p231i.p232f0.p235g.HttpMethod;

/* renamed from: i.y */
/* compiled from: Request */
public final class C5023y {

    /* renamed from: a */
    final HttpUrl f11842a;

    /* renamed from: b */
    final String f11843b;

    /* renamed from: c */
    final C5015r f11844c;

    /* renamed from: d */
    final RequestBody f11845d;

    /* renamed from: e */
    final Object f11846e;

    /* renamed from: f */
    private volatile CacheControl f11847f;

    C5023y(C5024a aVar) {
        this.f11842a = aVar.f11848a;
        this.f11843b = aVar.f11849b;
        this.f11844c = aVar.f11850c.mo27747a();
        this.f11845d = aVar.f11851d;
        Object obj = aVar.f11852e;
        this.f11846e = obj == null ? this : obj;
    }

    /* renamed from: a */
    public String mo27820a(String str) {
        return this.f11844c.mo27738a(str);
    }

    /* renamed from: b */
    public List<String> mo27822b(String str) {
        return this.f11844c.mo27741b(str);
    }

    /* renamed from: c */
    public C5015r mo27823c() {
        return this.f11844c;
    }

    /* renamed from: d */
    public boolean mo27824d() {
        return this.f11842a.mo27762h();
    }

    /* renamed from: e */
    public String mo27825e() {
        return this.f11843b;
    }

    /* renamed from: f */
    public C5024a mo27826f() {
        return new C5024a(this);
    }

    /* renamed from: g */
    public HttpUrl mo27827g() {
        return this.f11842a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request{method=");
        sb.append(this.f11843b);
        sb.append(", url=");
        sb.append(this.f11842a);
        sb.append(", tag=");
        Object obj = this.f11846e;
        if (obj == this) {
            obj = null;
        }
        sb.append(obj);
        sb.append('}');
        return sb.toString();
    }

    /* renamed from: i.y$a */
    /* compiled from: Request */
    public static class C5024a {

        /* renamed from: a */
        HttpUrl f11848a;

        /* renamed from: b */
        String f11849b;

        /* renamed from: c */
        C5015r.C5016a f11850c;

        /* renamed from: d */
        RequestBody f11851d;

        /* renamed from: e */
        Object f11852e;

        public C5024a() {
            this.f11849b = "GET";
            this.f11850c = new C5015r.C5016a();
        }

        /* renamed from: a */
        public C5024a mo27831a(HttpUrl sVar) {
            if (sVar != null) {
                this.f11848a = sVar;
                return this;
            }
            throw new NullPointerException("url == null");
        }

        /* renamed from: b */
        public C5024a mo27836b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                HttpUrl d = HttpUrl.m17955d(str);
                if (d != null) {
                    mo27831a(d);
                    return this;
                }
                throw new IllegalArgumentException("unexpected url: " + str);
            }
            throw new NullPointerException("url == null");
        }

        /* renamed from: a */
        public C5024a mo27834a(String str, String str2) {
            this.f11850c.mo27751c(str, str2);
            return this;
        }

        C5024a(C5023y yVar) {
            this.f11848a = yVar.f11842a;
            this.f11849b = yVar.f11843b;
            this.f11851d = yVar.f11845d;
            this.f11852e = yVar.f11846e;
            this.f11850c = yVar.f11844c.mo27736a();
        }

        /* renamed from: a */
        public C5024a mo27832a(String str) {
            this.f11850c.mo27750c(str);
            return this;
        }

        /* renamed from: a */
        public C5024a mo27830a(C5015r rVar) {
            this.f11850c = rVar.mo27736a();
            return this;
        }

        /* renamed from: a */
        public C5024a mo27829a(CacheControl dVar) {
            String dVar2 = dVar.toString();
            if (dVar2.isEmpty()) {
                mo27832a("Cache-Control");
                return this;
            }
            mo27834a("Cache-Control", dVar2);
            return this;
        }

        /* renamed from: a */
        public C5024a mo27833a(String str, RequestBody zVar) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (zVar != null && !HttpMethod.m17510b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (zVar != null || !HttpMethod.m17513e(str)) {
                this.f11849b = str;
                this.f11851d = zVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        /* renamed from: a */
        public C5023y mo27835a() {
            if (this.f11848a != null) {
                return new C5023y(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    /* renamed from: a */
    public RequestBody mo27819a() {
        return this.f11845d;
    }

    /* renamed from: b */
    public CacheControl mo27821b() {
        CacheControl dVar = this.f11847f;
        if (dVar != null) {
            return dVar;
        }
        CacheControl a = CacheControl.m17303a(this.f11844c);
        this.f11847f = a;
        return a;
    }
}
