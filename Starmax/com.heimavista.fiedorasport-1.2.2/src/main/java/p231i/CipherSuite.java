package p231i;

import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* renamed from: i.h */
public final class CipherSuite {

    /* renamed from: b */
    static final Comparator<String> f11683b = new C5006a();

    /* renamed from: c */
    private static final Map<String, CipherSuite> f11684c = new TreeMap(f11683b);

    /* renamed from: d */
    public static final CipherSuite f11685d = m17852a("SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);

    /* renamed from: e */
    public static final CipherSuite f11686e = m17852a("TLS_RSA_WITH_AES_128_CBC_SHA", 47);

    /* renamed from: f */
    public static final CipherSuite f11687f = m17852a("TLS_RSA_WITH_AES_256_CBC_SHA", 53);

    /* renamed from: g */
    public static final CipherSuite f11688g = m17852a("TLS_RSA_WITH_AES_128_GCM_SHA256", 156);

    /* renamed from: h */
    public static final CipherSuite f11689h = m17852a("TLS_RSA_WITH_AES_256_GCM_SHA384", 157);

    /* renamed from: i */
    public static final CipherSuite f11690i = m17852a("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", 49171);

    /* renamed from: j */
    public static final CipherSuite f11691j = m17852a("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", 49172);

    /* renamed from: k */
    public static final CipherSuite f11692k = m17852a("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", 49195);

    /* renamed from: l */
    public static final CipherSuite f11693l = m17852a("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", 49196);

    /* renamed from: m */
    public static final CipherSuite f11694m = m17852a("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", 49199);

    /* renamed from: n */
    public static final CipherSuite f11695n = m17852a("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", 49200);

    /* renamed from: o */
    public static final CipherSuite f11696o = m17852a("TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52392);

    /* renamed from: p */
    public static final CipherSuite f11697p = m17852a("TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", 52393);

    /* renamed from: a */
    final String f11698a;

    /* renamed from: i.h$a */
    /* compiled from: CipherSuite */
    class C5006a implements Comparator<String> {
        C5006a() {
        }

        /* renamed from: a */
        public int compare(String str, String str2) {
            int min = Math.min(str.length(), str2.length());
            int i = 4;
            while (i < min) {
                char charAt = str.charAt(i);
                char charAt2 = str2.charAt(i);
                if (charAt == charAt2) {
                    i++;
                } else if (charAt < charAt2) {
                    return -1;
                } else {
                    return 1;
                }
            }
            int length = str.length();
            int length2 = str2.length();
            if (length == length2) {
                return 0;
            }
            if (length < length2) {
                return -1;
            }
            return 1;
        }
    }

    static {
        m17852a("SSL_RSA_WITH_NULL_MD5", 1);
        m17852a("SSL_RSA_WITH_NULL_SHA", 2);
        m17852a("SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);
        m17852a("SSL_RSA_WITH_RC4_128_MD5", 4);
        m17852a("SSL_RSA_WITH_RC4_128_SHA", 5);
        m17852a("SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);
        m17852a("SSL_RSA_WITH_DES_CBC_SHA", 9);
        m17852a("SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);
        m17852a("SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);
        m17852a("SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);
        m17852a("SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);
        m17852a("SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);
        m17852a("SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);
        m17852a("SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);
        m17852a("SSL_DH_anon_WITH_RC4_128_MD5", 24);
        m17852a("SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);
        m17852a("SSL_DH_anon_WITH_DES_CBC_SHA", 26);
        m17852a("SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);
        m17852a("TLS_KRB5_WITH_DES_CBC_SHA", 30);
        m17852a("TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);
        m17852a("TLS_KRB5_WITH_RC4_128_SHA", 32);
        m17852a("TLS_KRB5_WITH_DES_CBC_MD5", 34);
        m17852a("TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);
        m17852a("TLS_KRB5_WITH_RC4_128_MD5", 36);
        m17852a("TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);
        m17852a("TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);
        m17852a("TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);
        m17852a("TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);
        m17852a("TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);
        m17852a("TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);
        m17852a("TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);
        m17852a("TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);
        m17852a("TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);
        m17852a("TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);
        m17852a("TLS_RSA_WITH_NULL_SHA256", 59);
        m17852a("TLS_RSA_WITH_AES_128_CBC_SHA256", 60);
        m17852a("TLS_RSA_WITH_AES_256_CBC_SHA256", 61);
        m17852a("TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);
        m17852a("TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);
        m17852a("TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);
        m17852a("TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);
        m17852a("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);
        m17852a("TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);
        m17852a("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);
        m17852a("TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);
        m17852a("TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);
        m17852a("TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);
        m17852a("TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", TsExtractor.TS_STREAM_TYPE_E_AC3);
        m17852a("TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);
        m17852a("TLS_PSK_WITH_RC4_128_SHA", TsExtractor.TS_STREAM_TYPE_DTS);
        m17852a("TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);
        m17852a("TLS_PSK_WITH_AES_128_CBC_SHA", 140);
        m17852a("TLS_PSK_WITH_AES_256_CBC_SHA", 141);
        m17852a("TLS_RSA_WITH_SEED_CBC_SHA", 150);
        m17852a("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);
        m17852a("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);
        m17852a("TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);
        m17852a("TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);
        m17852a("TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);
        m17852a("TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);
        m17852a("TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);
        m17852a("TLS_FALLBACK_SCSV", 22016);
        m17852a("TLS_ECDH_ECDSA_WITH_NULL_SHA", 49153);
        m17852a("TLS_ECDH_ECDSA_WITH_RC4_128_SHA", 49154);
        m17852a("TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", 49155);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", 49156);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", 49157);
        m17852a("TLS_ECDHE_ECDSA_WITH_NULL_SHA", 49158);
        m17852a("TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", 49159);
        m17852a("TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", 49160);
        m17852a("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", 49161);
        m17852a("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", 49162);
        m17852a("TLS_ECDH_RSA_WITH_NULL_SHA", 49163);
        m17852a("TLS_ECDH_RSA_WITH_RC4_128_SHA", 49164);
        m17852a("TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", 49165);
        m17852a("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", 49166);
        m17852a("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", 49167);
        m17852a("TLS_ECDHE_RSA_WITH_NULL_SHA", 49168);
        m17852a("TLS_ECDHE_RSA_WITH_RC4_128_SHA", 49169);
        m17852a("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", 49170);
        m17852a("TLS_ECDH_anon_WITH_NULL_SHA", 49173);
        m17852a("TLS_ECDH_anon_WITH_RC4_128_SHA", 49174);
        m17852a("TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", 49175);
        m17852a("TLS_ECDH_anon_WITH_AES_128_CBC_SHA", 49176);
        m17852a("TLS_ECDH_anon_WITH_AES_256_CBC_SHA", 49177);
        m17852a("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", 49187);
        m17852a("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", 49188);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", 49189);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", 49190);
        m17852a("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", 49191);
        m17852a("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", 49192);
        m17852a("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", 49193);
        m17852a("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", 49194);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", 49197);
        m17852a("TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", 49198);
        m17852a("TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", 49201);
        m17852a("TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", 49202);
        m17852a("TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", 49205);
        m17852a("TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", 49206);
    }

    private CipherSuite(String str) {
        if (str != null) {
            this.f11698a = str;
            return;
        }
        throw new NullPointerException();
    }

    /* renamed from: a */
    public static synchronized CipherSuite m17851a(String str) {
        CipherSuite hVar;
        synchronized (CipherSuite.class) {
            hVar = f11684c.get(str);
            if (hVar == null) {
                hVar = new CipherSuite(str);
                f11684c.put(str, hVar);
            }
        }
        return hVar;
    }

    public String toString() {
        return this.f11698a;
    }

    /* renamed from: a */
    static List<CipherSuite> m17853a(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String str : strArr) {
            arrayList.add(m17851a(str));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: a */
    private static CipherSuite m17852a(String str, int i) {
        return m17851a(str);
    }

    /* renamed from: a */
    public String mo27670a() {
        return this.f11698a;
    }
}
