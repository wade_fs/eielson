package p231i;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p231i.RealCall;
import p231i.p232f0.C4951c;

/* renamed from: i.n */
/* compiled from: Dispatcher */
public final class C5010n {

    /* renamed from: a */
    private int f11732a = 64;

    /* renamed from: b */
    private int f11733b = 5;

    /* renamed from: c */
    private Runnable f11734c;

    /* renamed from: d */
    private ExecutorService f11735d;

    /* renamed from: e */
    private final Deque<RealCall.C5022a> f11736e = new ArrayDeque();

    /* renamed from: f */
    private final Deque<RealCall.C5022a> f11737f = new ArrayDeque();

    /* renamed from: g */
    private final Deque<RealCall> f11738g = new ArrayDeque();

    /* renamed from: b */
    private int m17891b(RealCall.C5022a aVar) {
        int i = 0;
        for (RealCall.C5022a aVar2 : this.f11737f) {
            if (!aVar2.mo27817c().f11838T && aVar2.mo27818d().equals(aVar.mo27818d())) {
                i++;
            }
        }
        return i;
    }

    /* renamed from: c */
    private void m17892c() {
        if (this.f11737f.size() < this.f11732a && !this.f11736e.isEmpty()) {
            Iterator<RealCall.C5022a> it = this.f11736e.iterator();
            while (it.hasNext()) {
                RealCall.C5022a next = it.next();
                if (m17891b(next) < this.f11733b) {
                    it.remove();
                    this.f11737f.add(next);
                    mo27703a().execute(next);
                }
                if (this.f11737f.size() >= this.f11732a) {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    /* renamed from: a */
    public synchronized ExecutorService mo27703a() {
        if (this.f11735d == null) {
            this.f11735d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), C4951c.m17350a("OkHttp Dispatcher", false));
        }
        return this.f11735d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27707b(RealCall xVar) {
        m17890a(this.f11738g, xVar, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo27705a(RealCall xVar) {
        this.f11738g.add(xVar);
    }

    /* renamed from: b */
    public synchronized int mo27706b() {
        return this.f11737f.size() + this.f11738g.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27704a(RealCall.C5022a aVar) {
        m17890a(this.f11737f, aVar, true);
    }

    /* renamed from: a */
    private <T> void m17890a(Deque<T> deque, T t, boolean z) {
        int b;
        Runnable runnable;
        synchronized (this) {
            if (deque.remove(t)) {
                if (z) {
                    m17892c();
                }
                b = mo27706b();
                runnable = this.f11734c;
            } else {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        if (b == 0 && runnable != null) {
            runnable.run();
        }
    }
}
