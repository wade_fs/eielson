package p231i;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import p231i.p232f0.C4951c;
import p231i.p232f0.p240l.CertificateChainCleaner;
import p244j.ByteString;

/* renamed from: i.g */
public final class CertificatePinner {

    /* renamed from: c */
    public static final CertificatePinner f11675c = new C5004a().mo27665a();

    /* renamed from: a */
    private final Set<C5005b> f11676a;

    /* renamed from: b */
    private final CertificateChainCleaner f11677b;

    /* renamed from: i.g$a */
    /* compiled from: CertificatePinner */
    public static final class C5004a {

        /* renamed from: a */
        private final List<C5005b> f11678a = new ArrayList();

        /* renamed from: a */
        public CertificatePinner mo27665a() {
            return new CertificatePinner(new LinkedHashSet(this.f11678a), null);
        }
    }

    /* renamed from: i.g$b */
    /* compiled from: CertificatePinner */
    static final class C5005b {

        /* renamed from: a */
        final String f11679a;

        /* renamed from: b */
        final String f11680b;

        /* renamed from: c */
        final String f11681c;

        /* renamed from: d */
        final ByteString f11682d;

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo27666a(String str) {
            if (!this.f11679a.startsWith("*.")) {
                return str.equals(this.f11680b);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 == this.f11680b.length()) {
                String str2 = this.f11680b;
                if (str.regionMatches(false, indexOf + 1, str2, 0, str2.length())) {
                    return true;
                }
            }
            return false;
        }

        public boolean equals(Object obj) {
            if (obj instanceof C5005b) {
                C5005b bVar = (C5005b) obj;
                return this.f11679a.equals(bVar.f11679a) && this.f11681c.equals(bVar.f11681c) && this.f11682d.equals(bVar.f11682d);
            }
        }

        public int hashCode() {
            return ((((527 + this.f11679a.hashCode()) * 31) + this.f11681c.hashCode()) * 31) + this.f11682d.hashCode();
        }

        public String toString() {
            return this.f11681c + this.f11682d.mo28092a();
        }
    }

    CertificatePinner(Set<C5005b> set, CertificateChainCleaner cVar) {
        this.f11676a = set;
        this.f11677b = cVar;
    }

    /* renamed from: b */
    static ByteString m17845b(X509Certificate x509Certificate) {
        return ByteString.m18167a(x509Certificate.getPublicKey().getEncoded()).mo28103k();
    }

    /* renamed from: a */
    public void mo27662a(String str, List<Certificate> list) {
        List<C5005b> a = mo27661a(str);
        if (!a.isEmpty()) {
            CertificateChainCleaner cVar = this.f11677b;
            if (cVar != null) {
                list = cVar.mo27636a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a.size();
                ByteString fVar = null;
                ByteString fVar2 = null;
                for (int i2 = 0; i2 < size2; i2++) {
                    C5005b bVar = a.get(i2);
                    if (bVar.f11681c.equals("sha256/")) {
                        if (fVar == null) {
                            fVar = m17845b(x509Certificate);
                        }
                        if (bVar.f11682d.equals(fVar)) {
                            return;
                        }
                    } else if (bVar.f11681c.equals("sha1/")) {
                        if (fVar2 == null) {
                            fVar2 = m17843a(x509Certificate);
                        }
                        if (bVar.f11682d.equals(fVar2)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + bVar.f11681c);
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                sb.append("\n    ");
                sb.append(m17844a((Certificate) x509Certificate2));
                sb.append(": ");
                sb.append(x509Certificate2.getSubjectDN().getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            int size4 = a.size();
            for (int i4 = 0; i4 < size4; i4++) {
                sb.append("\n    ");
                sb.append(a.get(i4));
            }
            throw new SSLPeerUnverifiedException(sb.toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof CertificatePinner) {
            CertificatePinner gVar = (CertificatePinner) obj;
            if (!C4951c.m17356a(this.f11677b, gVar.f11677b) || !this.f11676a.equals(gVar.f11676a)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        CertificateChainCleaner cVar = this.f11677b;
        return ((cVar != null ? cVar.hashCode() : 0) * 31) + this.f11676a.hashCode();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public List<C5005b> mo27661a(String str) {
        List<C5005b> emptyList = Collections.emptyList();
        for (C5005b bVar : this.f11676a) {
            if (bVar.mo27666a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(bVar);
            }
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public CertificatePinner mo27660a(CertificateChainCleaner cVar) {
        return C4951c.m17356a(this.f11677b, cVar) ? this : new CertificatePinner(this.f11676a, cVar);
    }

    /* renamed from: a */
    public static String m17844a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + m17845b((X509Certificate) certificate).mo28092a();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    /* renamed from: a */
    static ByteString m17843a(X509Certificate x509Certificate) {
        return ByteString.m18167a(x509Certificate.getPublicKey().getEncoded()).mo28102j();
    }
}
