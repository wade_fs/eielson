package p231i;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import p231i.p232f0.C4951c;

/* renamed from: i.q */
public final class Handshake {

    /* renamed from: a */
    private final TlsVersion f11742a;

    /* renamed from: b */
    private final CipherSuite f11743b;

    /* renamed from: c */
    private final List<Certificate> f11744c;

    /* renamed from: d */
    private final List<Certificate> f11745d;

    private Handshake(TlsVersion d0Var, CipherSuite hVar, List<Certificate> list, List<Certificate> list2) {
        this.f11742a = d0Var;
        this.f11743b = hVar;
        this.f11744c = list;
        this.f11745d = list2;
    }

    /* renamed from: a */
    public static Handshake m17924a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite != null) {
            CipherSuite a = CipherSuite.m17851a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol != null) {
                TlsVersion a2 = TlsVersion.m17320a(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException unused) {
                    certificateArr = null;
                }
                if (certificateArr != null) {
                    list = C4951c.m17349a(certificateArr);
                } else {
                    list = Collections.emptyList();
                }
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                if (localCertificates != null) {
                    list2 = C4951c.m17349a(localCertificates);
                } else {
                    list2 = Collections.emptyList();
                }
                return new Handshake(a2, a, list, list2);
            }
            throw new IllegalStateException("tlsVersion == null");
        }
        throw new IllegalStateException("cipherSuite == null");
    }

    /* renamed from: b */
    public List<Certificate> mo27731b() {
        return this.f11745d;
    }

    /* renamed from: c */
    public List<Certificate> mo27732c() {
        return this.f11744c;
    }

    /* renamed from: d */
    public TlsVersion mo27733d() {
        return this.f11742a;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Handshake)) {
            return false;
        }
        Handshake qVar = (Handshake) obj;
        if (!this.f11742a.equals(qVar.f11742a) || !this.f11743b.equals(qVar.f11743b) || !this.f11744c.equals(qVar.f11744c) || !this.f11745d.equals(qVar.f11745d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((((527 + this.f11742a.hashCode()) * 31) + this.f11743b.hashCode()) * 31) + this.f11744c.hashCode()) * 31) + this.f11745d.hashCode();
    }

    /* renamed from: a */
    public static Handshake m17923a(TlsVersion d0Var, CipherSuite hVar, List<Certificate> list, List<Certificate> list2) {
        if (d0Var == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (hVar != null) {
            return new Handshake(d0Var, hVar, C4951c.m17348a(list), C4951c.m17348a(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    /* renamed from: a */
    public CipherSuite mo27730a() {
        return this.f11743b;
    }
}
