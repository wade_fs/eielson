package p231i;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import p231i.p232f0.C4951c;
import p244j.Buffer;

/* renamed from: i.s */
public final class HttpUrl {

    /* renamed from: i */
    private static final char[] f11748i = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a */
    final String f11749a;

    /* renamed from: b */
    private final String f11750b;

    /* renamed from: c */
    private final String f11751c;

    /* renamed from: d */
    final String f11752d;

    /* renamed from: e */
    final int f11753e;

    /* renamed from: f */
    private final List<String> f11754f;

    /* renamed from: g */
    private final String f11755g;

    /* renamed from: h */
    private final String f11756h;

    /* renamed from: i.s$a */
    /* compiled from: HttpUrl */
    public static final class C5017a {

        /* renamed from: a */
        String f11757a;

        /* renamed from: b */
        String f11758b = "";

        /* renamed from: c */
        String f11759c = "";

        /* renamed from: d */
        String f11760d;

        /* renamed from: e */
        int f11761e = -1;

        /* renamed from: f */
        final List<String> f11762f = new ArrayList();

        /* renamed from: g */
        List<String> f11763g;

        /* renamed from: h */
        String f11764h;

        /* renamed from: i.s$a$a */
        /* compiled from: HttpUrl */
        enum C5018a {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public C5017a() {
            this.f11762f.add("");
        }

        /* renamed from: f */
        private boolean m17981f(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        /* renamed from: g */
        private boolean m17982g(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        /* renamed from: a */
        public C5017a mo27772a(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.f11761e = i;
            return this;
        }

        /* renamed from: b */
        public C5017a mo27776b(String str) {
            if (str != null) {
                String a = m17973a(str, 0, str.length());
                if (a != null) {
                    this.f11760d = a;
                    return this;
                }
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            throw new NullPointerException("host == null");
        }

        /* renamed from: c */
        public C5017a mo27778c(String str) {
            if (str != null) {
                this.f11759c = HttpUrl.m17946a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("password == null");
        }

        /* renamed from: d */
        public C5017a mo27779d(String str) {
            if (str != null) {
                if (str.equalsIgnoreCase("http")) {
                    this.f11757a = "http";
                } else if (str.equalsIgnoreCase("https")) {
                    this.f11757a = "https";
                } else {
                    throw new IllegalArgumentException("unexpected scheme: " + str);
                }
                return this;
            }
            throw new NullPointerException("scheme == null");
        }

        /* renamed from: e */
        public C5017a mo27780e(String str) {
            if (str != null) {
                this.f11758b = HttpUrl.m17946a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("username == null");
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f11757a);
            sb.append("://");
            if (!this.f11758b.isEmpty() || !this.f11759c.isEmpty()) {
                sb.append(this.f11758b);
                if (!this.f11759c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.f11759c);
                }
                sb.append('@');
            }
            if (this.f11760d.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.f11760d);
                sb.append(']');
            } else {
                sb.append(this.f11760d);
            }
            int b = mo27775b();
            if (b != HttpUrl.m17954c(this.f11757a)) {
                sb.append(':');
                sb.append(b);
            }
            HttpUrl.m17953b(sb, this.f11762f);
            if (this.f11763g != null) {
                sb.append('?');
                HttpUrl.m17951a(sb, this.f11763g);
            }
            if (this.f11764h != null) {
                sb.append('#');
                sb.append(this.f11764h);
            }
            return sb.toString();
        }

        /* renamed from: f */
        private static int m17980f(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        /* renamed from: e */
        private static int m17979e(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt >= 'a' && charAt <= 'z') || (charAt >= 'A' && charAt <= 'Z')) {
                while (true) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                    char charAt2 = str.charAt(i);
                    if ((charAt2 < 'a' || charAt2 > 'z') && ((charAt2 < 'A' || charAt2 > 'Z') && !((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                        if (charAt2 == ':') {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        /* renamed from: a */
        public C5017a mo27773a(String str) {
            this.f11763g = str != null ? HttpUrl.m17956e(HttpUrl.m17946a(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public C5017a mo27777c() {
            int size = this.f11762f.size();
            for (int i = 0; i < size; i++) {
                this.f11762f.set(i, HttpUrl.m17946a(this.f11762f.get(i), "[]", true, true, false, true));
            }
            List<String> list = this.f11763g;
            if (list != null) {
                int size2 = list.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.f11763g.get(i2);
                    if (str != null) {
                        this.f11763g.set(i2, HttpUrl.m17946a(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            String str2 = this.f11764h;
            if (str2 != null) {
                this.f11764h = HttpUrl.m17946a(str2, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public int mo27775b() {
            int i = this.f11761e;
            return i != -1 ? i : HttpUrl.m17954c(this.f11757a);
        }

        /* renamed from: b */
        private static int m17975b(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(HttpUrl.m17944a(str, i, i2, "", false, false, false, true, null));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        }

        /* renamed from: a */
        public HttpUrl mo27774a() {
            if (this.f11757a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.f11760d != null) {
                return new HttpUrl(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:58)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0044 A[SYNTHETIC] */
        /* renamed from: d */
        private void m17978d(java.lang.String r11, int r12, int r13) {
            /*
                r10 = this;
                if (r12 != r13) goto L_0x0003
                return
            L_0x0003:
                char r0 = r11.charAt(r12)
                r1 = 47
                java.lang.String r2 = ""
                r3 = 1
                if (r0 == r1) goto L_0x001e
                r1 = 92
                if (r0 != r1) goto L_0x0013
                goto L_0x001e
            L_0x0013:
                java.util.List<java.lang.String> r0 = r10.f11762f
                int r1 = r0.size()
                int r1 = r1 - r3
                r0.set(r1, r2)
                goto L_0x0029
            L_0x001e:
                java.util.List<java.lang.String> r0 = r10.f11762f
                r0.clear()
                java.util.List<java.lang.String> r0 = r10.f11762f
                r0.add(r2)
                goto L_0x0041
            L_0x0029:
                r6 = r12
                if (r6 >= r13) goto L_0x0044
                java.lang.String r12 = "/\\"
                int r12 = p231i.p232f0.C4951c.m17340a(r11, r6, r13, r12)
                if (r12 >= r13) goto L_0x0036
                r0 = 1
                goto L_0x0037
            L_0x0036:
                r0 = 0
            L_0x0037:
                r9 = 1
                r4 = r10
                r5 = r11
                r7 = r12
                r8 = r0
                r4.m17974a(r5, r6, r7, r8, r9)
                if (r0 == 0) goto L_0x0029
            L_0x0041:
                int r12 = r12 + 1
                goto L_0x0029
            L_0x0044:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.HttpUrl.C5017a.m17978d(java.lang.String, int, int):void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.f0.c.a(java.lang.String, int, int, char):int
         arg types: [java.lang.String, int, int, int]
         candidates:
          i.f0.c.a(java.lang.String, int, int, java.lang.String):int
          i.f0.c.a(java.lang.String, int, int, char):int */
        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C5018a mo27771a(HttpUrl sVar, String str) {
            int i;
            int a;
            int i2;
            HttpUrl sVar2 = sVar;
            String str2 = str;
            int b = C4951c.m17360b(str2, 0, str.length());
            int c = C4951c.m17365c(str2, b, str.length());
            if (m17979e(str2, b, c) != -1) {
                if (str.regionMatches(true, b, "https:", 0, 6)) {
                    this.f11757a = "https";
                    b += 6;
                } else if (!str.regionMatches(true, b, "http:", 0, 5)) {
                    return C5018a.UNSUPPORTED_SCHEME;
                } else {
                    this.f11757a = "http";
                    b += 5;
                }
            } else if (sVar2 == null) {
                return C5018a.MISSING_SCHEME;
            } else {
                this.f11757a = sVar2.f11749a;
            }
            int f = m17980f(str2, b, c);
            char c2 = '?';
            char c3 = '#';
            if (f >= 2 || sVar2 == null || !sVar2.f11749a.equals(this.f11757a)) {
                int i3 = b + f;
                boolean z = false;
                boolean z2 = false;
                while (true) {
                    a = C4951c.m17340a(str2, i3, c, "@/\\?#");
                    char charAt = a != c ? str2.charAt(a) : 65535;
                    if (charAt == 65535 || charAt == c3 || charAt == '/' || charAt == '\\' || charAt == c2) {
                        i = a;
                        int c4 = m17976c(str2, i3, i);
                        int i4 = c4 + 1;
                    } else {
                        if (charAt == '@') {
                            if (!z) {
                                int a2 = C4951c.m17339a(str2, i3, a, ':');
                                int i5 = a2;
                                String str3 = "%40";
                                i2 = a;
                                String a3 = HttpUrl.m17944a(str, i3, a2, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                if (z2) {
                                    a3 = this.f11758b + str3 + a3;
                                }
                                this.f11758b = a3;
                                if (i5 != i2) {
                                    this.f11759c = HttpUrl.m17944a(str, i5 + 1, i2, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                    z = true;
                                }
                                z2 = true;
                            } else {
                                i2 = a;
                                this.f11759c += "%40" + HttpUrl.m17944a(str, i3, i2, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                            }
                            i3 = i2 + 1;
                        }
                        c2 = '?';
                        c3 = '#';
                    }
                }
                i = a;
                int c42 = m17976c(str2, i3, i);
                int i42 = c42 + 1;
                if (i42 < i) {
                    this.f11760d = m17973a(str2, i3, c42);
                    this.f11761e = m17975b(str2, i42, i);
                    if (this.f11761e == -1) {
                        return C5018a.INVALID_PORT;
                    }
                } else {
                    this.f11760d = m17973a(str2, i3, c42);
                    this.f11761e = HttpUrl.m17954c(this.f11757a);
                }
                if (this.f11760d == null) {
                    return C5018a.INVALID_HOST;
                }
            } else {
                this.f11758b = sVar.mo27760f();
                this.f11759c = sVar.mo27755b();
                this.f11760d = sVar2.f11752d;
                this.f11761e = sVar2.f11753e;
                this.f11762f.clear();
                this.f11762f.addAll(sVar.mo27757d());
                if (b == c || str2.charAt(b) == '#') {
                    mo27773a(sVar.mo27758e());
                }
                i = b;
            }
            int a4 = C4951c.m17340a(str2, i, c, "?#");
            m17978d(str2, i, a4);
            if (a4 < c && str2.charAt(a4) == '?') {
                int a5 = C4951c.m17339a(str2, a4, c, '#');
                this.f11763g = HttpUrl.m17956e(HttpUrl.m17944a(str, a4 + 1, a5, " \"'<>#", true, false, true, true, null));
                a4 = a5;
            }
            if (a4 < c && str2.charAt(a4) == '#') {
                this.f11764h = HttpUrl.m17944a(str, 1 + a4, c, "", true, false, false, false, null);
            }
            return C5018a.SUCCESS;
        }

        /* renamed from: d */
        private void m17977d() {
            List<String> list = this.f11762f;
            if (!list.remove(list.size() - 1).isEmpty() || this.f11762f.isEmpty()) {
                this.f11762f.add("");
                return;
            }
            List<String> list2 = this.f11762f;
            list2.set(list2.size() - 1, "");
        }

        /* renamed from: c */
        private static int m17976c(String str, int i, int i2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt == ':') {
                    return i;
                }
                if (charAt == '[') {
                    do {
                        i++;
                        if (i >= i2) {
                            break;
                        }
                    } while (str.charAt(i) != ']');
                }
                i++;
            }
            return i2;
        }

        /* renamed from: a */
        private void m17974a(String str, int i, int i2, boolean z, boolean z2) {
            String a = HttpUrl.m17944a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true, null);
            if (!m17981f(a)) {
                if (m17982g(a)) {
                    m17977d();
                    return;
                }
                List<String> list = this.f11762f;
                if (list.get(list.size() - 1).isEmpty()) {
                    List<String> list2 = this.f11762f;
                    list2.set(list2.size() - 1, a);
                } else {
                    this.f11762f.add(a);
                }
                if (z) {
                    this.f11762f.add("");
                }
            }
        }

        /* renamed from: a */
        private static String m17973a(String str, int i, int i2) {
            return C4951c.m17344a(HttpUrl.m17945a(str, i, i2, false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.s.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      i.s.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
      i.s.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      i.s.a(java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.s.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      i.s.a(java.lang.String, boolean):java.lang.String
      i.s.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      i.s.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String> */
    HttpUrl(C5017a aVar) {
        this.f11749a = aVar.f11757a;
        this.f11750b = m17947a(aVar.f11758b, false);
        this.f11751c = m17947a(aVar.f11759c, false);
        this.f11752d = aVar.f11760d;
        this.f11753e = aVar.mo27775b();
        m17948a(aVar.f11762f, false);
        List<String> list = aVar.f11763g;
        String str = null;
        this.f11754f = list != null ? m17948a(list, true) : null;
        String str2 = aVar.f11764h;
        this.f11755g = str2 != null ? m17947a(str2, false) : str;
        this.f11756h = aVar.toString();
    }

    /* renamed from: a */
    static void m17951a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i += 2) {
            String str = list.get(i);
            String str2 = list.get(i + 1);
            if (i > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    /* renamed from: c */
    public static int m17954c(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") ? 443 : -1;
    }

    /* renamed from: b */
    public String mo27755b() {
        if (this.f11751c.isEmpty()) {
            return "";
        }
        int indexOf = this.f11756h.indexOf(64);
        return this.f11756h.substring(this.f11756h.indexOf(58, this.f11749a.length() + 3) + 1, indexOf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      i.f0.c.a(java.lang.String, int, int, java.lang.String):int
      i.f0.c.a(java.lang.String, int, int, char):int */
    /* renamed from: d */
    public List<String> mo27757d() {
        int indexOf = this.f11756h.indexOf(47, this.f11749a.length() + 3);
        String str = this.f11756h;
        int a = C4951c.m17340a(str, indexOf, str.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < a) {
            int i = indexOf + 1;
            int a2 = C4951c.m17339a(this.f11756h, i, a, '/');
            arrayList.add(this.f11756h.substring(i, a2));
            indexOf = a2;
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      i.f0.c.a(java.lang.String, int, int, java.lang.String):int
      i.f0.c.a(java.lang.String, int, int, char):int */
    /* renamed from: e */
    public String mo27758e() {
        if (this.f11754f == null) {
            return null;
        }
        int indexOf = this.f11756h.indexOf(63) + 1;
        String str = this.f11756h;
        return this.f11756h.substring(indexOf, C4951c.m17339a(str, indexOf, str.length(), '#'));
    }

    public boolean equals(Object obj) {
        return (obj instanceof HttpUrl) && ((HttpUrl) obj).f11756h.equals(this.f11756h);
    }

    /* renamed from: f */
    public String mo27760f() {
        if (this.f11750b.isEmpty()) {
            return "";
        }
        int length = this.f11749a.length() + 3;
        String str = this.f11756h;
        return this.f11756h.substring(length, C4951c.m17340a(str, length, str.length(), ":@"));
    }

    /* renamed from: g */
    public String mo27761g() {
        return this.f11752d;
    }

    /* renamed from: h */
    public boolean mo27762h() {
        return this.f11749a.equals("https");
    }

    public int hashCode() {
        return this.f11756h.hashCode();
    }

    /* renamed from: i */
    public C5017a mo27764i() {
        C5017a aVar = new C5017a();
        aVar.f11757a = this.f11749a;
        aVar.f11758b = mo27760f();
        aVar.f11759c = mo27755b();
        aVar.f11760d = this.f11752d;
        aVar.f11761e = this.f11753e != m17954c(this.f11749a) ? this.f11753e : -1;
        aVar.f11762f.clear();
        aVar.f11762f.addAll(mo27757d());
        aVar.mo27773a(mo27758e());
        aVar.f11764h = mo27753a();
        return aVar;
    }

    /* renamed from: j */
    public int mo27765j() {
        return this.f11753e;
    }

    /* renamed from: k */
    public String mo27766k() {
        if (this.f11754f == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        m17951a(sb, this.f11754f);
        return sb.toString();
    }

    /* renamed from: l */
    public String mo27767l() {
        C5017a a = mo27752a("/...");
        a.mo27780e("");
        a.mo27778c("");
        return a.mo27774a().toString();
    }

    /* renamed from: m */
    public String mo27768m() {
        return this.f11749a;
    }

    /* renamed from: n */
    public URI mo27769n() {
        C5017a i = mo27764i();
        i.mo27777c();
        String aVar = i.toString();
        try {
            return new URI(aVar);
        } catch (URISyntaxException e) {
            try {
                return URI.create(aVar.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception unused) {
                throw new RuntimeException(e);
            }
        }
    }

    public String toString() {
        return this.f11756h;
    }

    /* renamed from: c */
    public String mo27756c() {
        int indexOf = this.f11756h.indexOf(47, this.f11749a.length() + 3);
        String str = this.f11756h;
        return this.f11756h.substring(indexOf, C4951c.m17340a(str, indexOf, str.length(), "?#"));
    }

    /* renamed from: b */
    static void m17953b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append('/');
            sb.append(list.get(i));
        }
    }

    /* renamed from: e */
    static List<String> m17956e(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i <= str.length()) {
            int indexOf = str.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i = indexOf + 1;
        }
        return arrayList;
    }

    /* renamed from: d */
    public static HttpUrl m17955d(String str) {
        C5017a aVar = new C5017a();
        if (aVar.mo27771a(null, str) == C5017a.C5018a.SUCCESS) {
            return aVar.mo27774a();
        }
        return null;
    }

    /* renamed from: a */
    public String mo27753a() {
        if (this.f11755g == null) {
            return null;
        }
        return this.f11756h.substring(this.f11756h.indexOf(35) + 1);
    }

    /* renamed from: b */
    public HttpUrl mo27754b(String str) {
        C5017a a = mo27752a(str);
        if (a != null) {
            return a.mo27774a();
        }
        return null;
    }

    /* renamed from: a */
    public C5017a mo27752a(String str) {
        C5017a aVar = new C5017a();
        if (aVar.mo27771a(this, str) == C5017a.C5018a.SUCCESS) {
            return aVar;
        }
        return null;
    }

    /* renamed from: a */
    static String m17947a(String str, boolean z) {
        return m17945a(str, 0, str.length(), z);
    }

    /* renamed from: a */
    private List<String> m17948a(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            arrayList.add(str != null ? m17947a(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: a */
    static String m17945a(String str, int i, int i2, boolean z) {
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (charAt == '%' || (charAt == '+' && z)) {
                Buffer cVar = new Buffer();
                cVar.mo28034a(str, i, i3);
                m17950a(cVar, str, i3, i2, z);
                return cVar.mo28049d();
            }
        }
        return str.substring(i, i2);
    }

    /* renamed from: a */
    static void m17950a(Buffer cVar, String str, int i, int i2, boolean z) {
        int i3;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt == 37 && (i3 = i + 2) < i2) {
                int a = C4951c.m17338a(str.charAt(i + 1));
                int a2 = C4951c.m17338a(str.charAt(i3));
                if (!(a == -1 || a2 == -1)) {
                    cVar.writeByte((a << 4) + a2);
                    i = i3;
                    i += Character.charCount(codePointAt);
                }
            } else if (codePointAt == 43 && z) {
                cVar.writeByte(32);
                i += Character.charCount(codePointAt);
            }
            cVar.mo28045c(codePointAt);
            i += Character.charCount(codePointAt);
        }
    }

    /* renamed from: a */
    static boolean m17952a(String str, int i, int i2) {
        int i3 = i + 2;
        if (i3 >= i2 || str.charAt(i) != '%' || C4951c.m17338a(str.charAt(i + 1)) == -1 || C4951c.m17338a(str.charAt(i3)) == -1) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    static String m17944a(String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        int i3 = i2;
        int i4 = i;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt >= 32 && codePointAt != 127 && (codePointAt < 128 || !z4)) {
                if (str2.indexOf(codePointAt) == -1 && ((codePointAt != 37 || (z && (!z2 || m17952a(str, i4, i2)))) && (codePointAt != 43 || !z3))) {
                    i4 += Character.charCount(codePointAt);
                }
            }
            Buffer cVar = new Buffer();
            cVar.mo28034a(str, i, i4);
            m17949a(cVar, str, i4, i2, str2, z, z2, z3, z4, charset);
            return cVar.mo28049d();
        }
        return str.substring(i, i2);
    }

    /* renamed from: a */
    static void m17949a(Buffer cVar, String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        Buffer cVar2 = null;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    cVar.mo28033a(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !m17952a(str, i, i2)))))) {
                    if (cVar2 == null) {
                        cVar2 = new Buffer();
                    }
                    if (charset == null || charset.equals(C4951c.f11306d)) {
                        cVar2.mo28045c(codePointAt);
                    } else {
                        cVar2.mo28035a(str, i, Character.charCount(codePointAt) + i, charset);
                    }
                    while (!cVar2.mo28063j()) {
                        byte readByte = cVar2.readByte() & 255;
                        cVar.writeByte(37);
                        cVar.writeByte((int) f11748i[(readByte >> 4) & 15]);
                        cVar.writeByte((int) f11748i[readByte & 15]);
                    }
                } else {
                    cVar.mo28045c(codePointAt);
                }
            }
            i += Character.charCount(codePointAt);
        }
    }

    /* renamed from: a */
    static String m17946a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return m17944a(str, 0, str.length(), str2, z, z2, z3, z4, null);
    }
}
