package p231i;

import androidx.core.app.NotificationCompat;
import java.io.IOException;
import java.util.ArrayList;
import p231i.p232f0.NamedRunnable;
import p231i.p232f0.p233e.CacheInterceptor;
import p231i.p232f0.p234f.ConnectInterceptor;
import p231i.p232f0.p235g.BridgeInterceptor;
import p231i.p232f0.p235g.CallServerInterceptor;
import p231i.p232f0.p235g.RealInterceptorChain;
import p231i.p232f0.p235g.RetryAndFollowUpInterceptor;
import p231i.p232f0.p239k.Platform;

/* renamed from: i.x */
final class RealCall implements Call {

    /* renamed from: P */
    final OkHttpClient f11834P;

    /* renamed from: Q */
    final RetryAndFollowUpInterceptor f11835Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public EventListener f11836R;

    /* renamed from: S */
    final C5023y f11837S;

    /* renamed from: T */
    final boolean f11838T;

    /* renamed from: U */
    private boolean f11839U;

    /* renamed from: i.x$a */
    /* compiled from: RealCall */
    final class C5022a extends NamedRunnable {

        /* renamed from: Q */
        private final C4950f f11840Q;

        /* renamed from: R */
        final /* synthetic */ RealCall f11841R;

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x003d A[SYNTHETIC, Splitter:B:13:0x003d] */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005d A[Catch:{ all -> 0x0036 }] */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo27409b() {
            /*
                r5 = this;
                r0 = 1
                r1 = 0
                i.x r2 = r5.f11841R     // Catch:{ IOException -> 0x0038 }
                i.a0 r2 = r2.mo27812a()     // Catch:{ IOException -> 0x0038 }
                i.x r3 = r5.f11841R     // Catch:{ IOException -> 0x0038 }
                i.f0.g.j r3 = r3.f11835Q     // Catch:{ IOException -> 0x0038 }
                boolean r1 = r3.mo27502a()     // Catch:{ IOException -> 0x0038 }
                if (r1 == 0) goto L_0x0021
                i.f r1 = r5.f11840Q     // Catch:{ IOException -> 0x0034 }
                i.x r2 = r5.f11841R     // Catch:{ IOException -> 0x0034 }
                java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x0034 }
                java.lang.String r4 = "Canceled"
                r3.<init>(r4)     // Catch:{ IOException -> 0x0034 }
                r1.mo27398a(r2, r3)     // Catch:{ IOException -> 0x0034 }
                goto L_0x0028
            L_0x0021:
                i.f r1 = r5.f11840Q     // Catch:{ IOException -> 0x0034 }
                i.x r3 = r5.f11841R     // Catch:{ IOException -> 0x0034 }
                r1.mo27397a(r3, r2)     // Catch:{ IOException -> 0x0034 }
            L_0x0028:
                i.x r0 = r5.f11841R
                i.v r0 = r0.f11834P
                i.n r0 = r0.mo27792h()
                r0.mo27704a(r5)
                goto L_0x0070
            L_0x0034:
                r1 = move-exception
                goto L_0x003b
            L_0x0036:
                r0 = move-exception
                goto L_0x0071
            L_0x0038:
                r0 = move-exception
                r1 = r0
                r0 = 0
            L_0x003b:
                if (r0 == 0) goto L_0x005d
                i.f0.k.f r0 = p231i.p232f0.p239k.Platform.m17817d()     // Catch:{ all -> 0x0036 }
                r2 = 4
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0036 }
                r3.<init>()     // Catch:{ all -> 0x0036 }
                java.lang.String r4 = "Callback failure for "
                r3.append(r4)     // Catch:{ all -> 0x0036 }
                i.x r4 = r5.f11841R     // Catch:{ all -> 0x0036 }
                java.lang.String r4 = r4.mo27816d()     // Catch:{ all -> 0x0036 }
                r3.append(r4)     // Catch:{ all -> 0x0036 }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0036 }
                r0.mo27629a(r2, r3, r1)     // Catch:{ all -> 0x0036 }
                goto L_0x0028
            L_0x005d:
                i.x r0 = r5.f11841R     // Catch:{ all -> 0x0036 }
                i.p r0 = r0.f11836R     // Catch:{ all -> 0x0036 }
                i.x r2 = r5.f11841R     // Catch:{ all -> 0x0036 }
                r0.mo27715a(r2, r1)     // Catch:{ all -> 0x0036 }
                i.f r0 = r5.f11840Q     // Catch:{ all -> 0x0036 }
                i.x r2 = r5.f11841R     // Catch:{ all -> 0x0036 }
                r0.mo27398a(r2, r1)     // Catch:{ all -> 0x0036 }
                goto L_0x0028
            L_0x0070:
                return
            L_0x0071:
                i.x r1 = r5.f11841R
                i.v r1 = r1.f11834P
                i.n r1 = r1.mo27792h()
                r1.mo27704a(r5)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.RealCall.C5022a.mo27409b():void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public RealCall mo27817c() {
            return this.f11841R;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: d */
        public String mo27818d() {
            return this.f11841R.f11837S.mo27827g().mo27761g();
        }
    }

    private RealCall(OkHttpClient vVar, C5023y yVar, boolean z) {
        this.f11834P = vVar;
        this.f11837S = yVar;
        this.f11838T = z;
        this.f11835Q = new RetryAndFollowUpInterceptor(vVar, z);
    }

    /* renamed from: e */
    private void m18043e() {
        this.f11835Q.mo27501a(Platform.m17817d().mo27628a("response.body().close()"));
    }

    /* renamed from: b */
    public boolean mo27813b() {
        return this.f11835Q.mo27502a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public String mo27814c() {
        return this.f11837S.mo27827g().mo27767l();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public String mo27816d() {
        StringBuilder sb = new StringBuilder();
        sb.append(mo27813b() ? "canceled " : "");
        sb.append(this.f11838T ? "web socket" : NotificationCompat.CATEGORY_CALL);
        sb.append(" to ");
        sb.append(mo27814c());
        return sb.toString();
    }

    /* renamed from: s */
    public Response mo27395s() {
        synchronized (this) {
            if (!this.f11839U) {
                this.f11839U = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        m18043e();
        this.f11836R.mo27721b(this);
        try {
            this.f11834P.mo27792h().mo27705a(this);
            Response a = mo27812a();
            if (a != null) {
                this.f11834P.mo27792h().mo27707b(this);
                return a;
            }
            throw new IOException("Canceled");
        } catch (IOException e) {
            this.f11836R.mo27715a(this, e);
            throw e;
        } catch (Throwable th) {
            this.f11834P.mo27792h().mo27707b(this);
            throw th;
        }
    }

    /* renamed from: a */
    static RealCall m18042a(OkHttpClient vVar, C5023y yVar, boolean z) {
        RealCall xVar = new RealCall(vVar, yVar, z);
        xVar.f11836R = vVar.mo27794j().mo27729a(xVar);
        return xVar;
    }

    public RealCall clone() {
        return m18042a(this.f11834P, this.f11837S, this.f11838T);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Response mo27812a() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f11834P.mo27798n());
        arrayList.add(this.f11835Q);
        arrayList.add(new BridgeInterceptor(this.f11834P.mo27791g()));
        arrayList.add(new CacheInterceptor(this.f11834P.mo27799o()));
        arrayList.add(new ConnectInterceptor(this.f11834P));
        if (!this.f11838T) {
            arrayList.addAll(this.f11834P.mo27800p());
        }
        arrayList.add(new CallServerInterceptor(this.f11838T));
        return new RealInterceptorChain(arrayList, null, null, null, 0, this.f11837S, this, this.f11836R, this.f11834P.mo27788d(), this.f11834P.mo27806x(), this.f11834P.mo27784B()).mo27491a(this.f11837S);
    }
}
