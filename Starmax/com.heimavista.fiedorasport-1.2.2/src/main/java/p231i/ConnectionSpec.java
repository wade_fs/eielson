package p231i;

import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;
import p231i.p232f0.C4951c;

/* renamed from: i.k */
public final class ConnectionSpec {

    /* renamed from: e */
    private static final CipherSuite[] f11707e = {CipherSuite.f11692k, CipherSuite.f11694m, CipherSuite.f11693l, CipherSuite.f11695n, CipherSuite.f11697p, CipherSuite.f11696o, CipherSuite.f11690i, CipherSuite.f11691j, CipherSuite.f11688g, CipherSuite.f11689h, CipherSuite.f11686e, CipherSuite.f11687f, CipherSuite.f11685d};

    /* renamed from: f */
    public static final ConnectionSpec f11708f;

    /* renamed from: g */
    public static final ConnectionSpec f11709g = new C5008a(false).mo27693a();

    /* renamed from: a */
    final boolean f11710a;

    /* renamed from: b */
    final boolean f11711b;

    /* renamed from: c */
    final String[] f11712c;

    /* renamed from: d */
    final String[] f11713d;

    static {
        C5008a aVar = new C5008a(true);
        aVar.mo27691a(f11707e);
        aVar.mo27690a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0);
        aVar.mo27689a(true);
        f11708f = aVar.mo27693a();
        C5008a aVar2 = new C5008a(f11708f);
        aVar2.mo27690a(TlsVersion.TLS_1_0);
        aVar2.mo27689a(true);
        aVar2.mo27693a();
    }

    ConnectionSpec(C5008a aVar) {
        this.f11710a = aVar.f11714a;
        this.f11712c = aVar.f11715b;
        this.f11713d = aVar.f11716c;
        this.f11711b = aVar.f11717d;
    }

    /* renamed from: a */
    public List<CipherSuite> mo27680a() {
        String[] strArr = this.f11712c;
        if (strArr != null) {
            return CipherSuite.m17853a(strArr);
        }
        return null;
    }

    /* renamed from: b */
    public boolean mo27683b() {
        return this.f11710a;
    }

    /* renamed from: c */
    public boolean mo27684c() {
        return this.f11711b;
    }

    /* renamed from: d */
    public List<TlsVersion> mo27685d() {
        String[] strArr = this.f11713d;
        if (strArr != null) {
            return TlsVersion.m17321a(strArr);
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ConnectionSpec)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ConnectionSpec kVar = (ConnectionSpec) obj;
        boolean z = this.f11710a;
        if (z != kVar.f11710a) {
            return false;
        }
        return !z || (Arrays.equals(this.f11712c, kVar.f11712c) && Arrays.equals(this.f11713d, kVar.f11713d) && this.f11711b == kVar.f11711b);
    }

    public int hashCode() {
        if (this.f11710a) {
            return ((((527 + Arrays.hashCode(this.f11712c)) * 31) + Arrays.hashCode(this.f11713d)) * 31) + (this.f11711b ^ true ? 1 : 0);
        }
        return 17;
    }

    public String toString() {
        if (!this.f11710a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.f11712c != null ? mo27680a().toString() : str;
        if (this.f11713d != null) {
            str = mo27685d().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.f11711b + ")";
    }

    /* renamed from: i.k$a */
    /* compiled from: ConnectionSpec */
    public static final class C5008a {

        /* renamed from: a */
        boolean f11714a;

        /* renamed from: b */
        String[] f11715b;

        /* renamed from: c */
        String[] f11716c;

        /* renamed from: d */
        boolean f11717d;

        C5008a(boolean z) {
            this.f11714a = z;
        }

        /* renamed from: a */
        public C5008a mo27691a(CipherSuite... hVarArr) {
            if (this.f11714a) {
                String[] strArr = new String[hVarArr.length];
                for (int i = 0; i < hVarArr.length; i++) {
                    strArr[i] = hVarArr[i].f11698a;
                }
                mo27692a(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        /* renamed from: b */
        public C5008a mo27694b(String... strArr) {
            if (!this.f11714a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.f11716c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        public C5008a(ConnectionSpec kVar) {
            this.f11714a = kVar.f11710a;
            this.f11715b = kVar.f11712c;
            this.f11716c = kVar.f11713d;
            this.f11717d = kVar.f11711b;
        }

        /* renamed from: a */
        public C5008a mo27692a(String... strArr) {
            if (!this.f11714a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.f11715b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        /* renamed from: a */
        public C5008a mo27690a(TlsVersion... d0VarArr) {
            if (this.f11714a) {
                String[] strArr = new String[d0VarArr.length];
                for (int i = 0; i < d0VarArr.length; i++) {
                    strArr[i] = d0VarArr[i].f11300P;
                }
                mo27694b(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        /* renamed from: a */
        public C5008a mo27689a(boolean z) {
            if (this.f11714a) {
                this.f11717d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        /* renamed from: a */
        public ConnectionSpec mo27693a() {
            return new ConnectionSpec(this);
        }
    }

    /* renamed from: b */
    private ConnectionSpec m17862b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2;
        if (this.f11712c != null) {
            strArr = C4951c.m17358a(CipherSuite.f11683b, sSLSocket.getEnabledCipherSuites(), this.f11712c);
        } else {
            strArr = sSLSocket.getEnabledCipherSuites();
        }
        if (this.f11713d != null) {
            strArr2 = C4951c.m17358a(C4951c.f11308f, sSLSocket.getEnabledProtocols(), this.f11713d);
        } else {
            strArr2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int a = C4951c.m17341a(CipherSuite.f11683b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && a != -1) {
            strArr = C4951c.m17359a(strArr, supportedCipherSuites[a]);
        }
        C5008a aVar = new C5008a(this);
        aVar.mo27692a(strArr);
        aVar.mo27694b(strArr2);
        return aVar.mo27693a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27681a(SSLSocket sSLSocket, boolean z) {
        ConnectionSpec b = m17862b(sSLSocket, z);
        String[] strArr = b.f11713d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = b.f11712c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    /* renamed from: a */
    public boolean mo27682a(SSLSocket sSLSocket) {
        if (!this.f11710a) {
            return false;
        }
        String[] strArr = this.f11713d;
        if (strArr != null && !C4951c.m17363b(C4951c.f11308f, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.f11712c;
        if (strArr2 == null || C4951c.m17363b(CipherSuite.f11683b, strArr2, sSLSocket.getEnabledCipherSuites())) {
            return true;
        }
        return false;
    }
}
