package p231i;

import java.io.IOException;

/* renamed from: i.w */
public enum Protocol {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2"),
    QUIC("quic");
    

    /* renamed from: P */
    private final String f11833P;

    private Protocol(String str) {
        this.f11833P = str;
    }

    /* renamed from: a */
    public static Protocol m18040a(String str) {
        if (str.equals(HTTP_1_0.f11833P)) {
            return HTTP_1_0;
        }
        if (str.equals(HTTP_1_1.f11833P)) {
            return HTTP_1_1;
        }
        if (str.equals(HTTP_2.f11833P)) {
            return HTTP_2;
        }
        if (str.equals(SPDY_3.f11833P)) {
            return SPDY_3;
        }
        if (str.equals(QUIC.f11833P)) {
            return QUIC;
        }
        throw new IOException("Unexpected protocol: " + str);
    }

    public String toString() {
        return this.f11833P;
    }
}
