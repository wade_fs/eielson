package p231i;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import p231i.C5015r;
import p231i.Call;
import p231i.EventListener;
import p231i.Response;
import p231i.p232f0.C4951c;
import p231i.p232f0.Internal;
import p231i.p232f0.p233e.InternalCache;
import p231i.p232f0.p234f.RealConnection;
import p231i.p232f0.p234f.RouteDatabase;
import p231i.p232f0.p234f.StreamAllocation;
import p231i.p232f0.p239k.Platform;
import p231i.p232f0.p240l.CertificateChainCleaner;
import p231i.p232f0.p240l.OkHostnameVerifier;

/* renamed from: i.v */
public class OkHttpClient implements Cloneable, Call.C4949a, WebSocket {

    /* renamed from: q0 */
    static final List<Protocol> f11771q0 = C4951c.m17349a(Protocol.HTTP_2, Protocol.HTTP_1_1);

    /* renamed from: r0 */
    static final List<ConnectionSpec> f11772r0 = C4951c.m17349a(ConnectionSpec.f11708f, ConnectionSpec.f11709g);

    /* renamed from: P */
    final C5010n f11773P;

    /* renamed from: Q */
    final Proxy f11774Q;

    /* renamed from: R */
    final List<Protocol> f11775R;

    /* renamed from: S */
    final List<ConnectionSpec> f11776S;

    /* renamed from: T */
    final List<Interceptor> f11777T;

    /* renamed from: U */
    final List<Interceptor> f11778U;

    /* renamed from: V */
    final EventListener.C5014c f11779V;

    /* renamed from: W */
    final ProxySelector f11780W;

    /* renamed from: X */
    final CookieJar f11781X;

    /* renamed from: Y */
    final C4941c f11782Y;

    /* renamed from: Z */
    final InternalCache f11783Z;

    /* renamed from: a0 */
    final SocketFactory f11784a0;

    /* renamed from: b0 */
    final SSLSocketFactory f11785b0;

    /* renamed from: c0 */
    final CertificateChainCleaner f11786c0;

    /* renamed from: d0 */
    final HostnameVerifier f11787d0;

    /* renamed from: e0 */
    final CertificatePinner f11788e0;

    /* renamed from: f0 */
    final Authenticator f11789f0;

    /* renamed from: g0 */
    final Authenticator f11790g0;

    /* renamed from: h0 */
    final ConnectionPool f11791h0;

    /* renamed from: i0 */
    final Dns f11792i0;

    /* renamed from: j0 */
    final boolean f11793j0;

    /* renamed from: k0 */
    final boolean f11794k0;

    /* renamed from: l0 */
    final boolean f11795l0;

    /* renamed from: m0 */
    final int f11796m0;

    /* renamed from: n0 */
    final int f11797n0;

    /* renamed from: o0 */
    final int f11798o0;

    /* renamed from: p0 */
    final int f11799p0;

    /* renamed from: i.v$a */
    /* compiled from: OkHttpClient */
    class C5020a extends Internal {
        C5020a() {
        }

        /* renamed from: a */
        public void mo27404a(C5015r.C5016a aVar, String str) {
            aVar.mo27745a(str);
        }

        /* renamed from: b */
        public void mo27408b(ConnectionPool jVar, RealConnection cVar) {
            jVar.mo27678b(cVar);
        }

        /* renamed from: a */
        public void mo27405a(C5015r.C5016a aVar, String str, String str2) {
            aVar.mo27748b(str, str2);
        }

        /* renamed from: a */
        public boolean mo27407a(ConnectionPool jVar, RealConnection cVar) {
            return jVar.mo27677a(cVar);
        }

        /* renamed from: a */
        public RealConnection mo27400a(ConnectionPool jVar, Address aVar, StreamAllocation gVar, Route c0Var) {
            return jVar.mo27675a(aVar, gVar, c0Var);
        }

        /* renamed from: a */
        public boolean mo27406a(Address aVar, Address aVar2) {
            return aVar.mo27299a(aVar2);
        }

        /* renamed from: a */
        public Socket mo27402a(ConnectionPool jVar, Address aVar, StreamAllocation gVar) {
            return jVar.mo27676a(aVar, gVar);
        }

        /* renamed from: a */
        public RouteDatabase mo27401a(ConnectionPool jVar) {
            return jVar.f11704e;
        }

        /* renamed from: a */
        public int mo27399a(Response.C4938a aVar) {
            return aVar.f11225c;
        }

        /* renamed from: a */
        public void mo27403a(ConnectionSpec kVar, SSLSocket sSLSocket, boolean z) {
            kVar.mo27681a(sSLSocket, z);
        }
    }

    static {
        Internal.f11301a = new C5020a();
    }

    public OkHttpClient() {
        this(new C5021b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.security.GeneralSecurityException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: C */
    private X509TrustManager m17999C() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e) {
            throw C4951c.m17342a("No System TLS", (Exception) e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError
     arg types: [java.lang.String, java.security.GeneralSecurityException]
     candidates:
      i.f0.c.a(i.s, boolean):java.lang.String
      i.f0.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      i.f0.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      i.f0.c.a(java.lang.Object, java.lang.Object):boolean
      i.f0.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      i.f0.c.a(java.lang.String, java.lang.Exception):java.lang.AssertionError */
    /* renamed from: a */
    private SSLSocketFactory m18000a(X509TrustManager x509TrustManager) {
        try {
            SSLContext b = Platform.m17817d().mo27644b();
            b.init(null, new TrustManager[]{x509TrustManager}, null);
            return b.getSocketFactory();
        } catch (GeneralSecurityException e) {
            throw C4951c.m17342a("No System TLS", (Exception) e);
        }
    }

    /* renamed from: A */
    public SSLSocketFactory mo27783A() {
        return this.f11785b0;
    }

    /* renamed from: B */
    public int mo27784B() {
        return this.f11798o0;
    }

    /* renamed from: b */
    public C4941c mo27786b() {
        return this.f11782Y;
    }

    /* renamed from: c */
    public CertificatePinner mo27787c() {
        return this.f11788e0;
    }

    /* renamed from: d */
    public int mo27788d() {
        return this.f11796m0;
    }

    /* renamed from: e */
    public ConnectionPool mo27789e() {
        return this.f11791h0;
    }

    /* renamed from: f */
    public List<ConnectionSpec> mo27790f() {
        return this.f11776S;
    }

    /* renamed from: g */
    public CookieJar mo27791g() {
        return this.f11781X;
    }

    /* renamed from: h */
    public C5010n mo27792h() {
        return this.f11773P;
    }

    /* renamed from: i */
    public Dns mo27793i() {
        return this.f11792i0;
    }

    /* renamed from: j */
    public EventListener.C5014c mo27794j() {
        return this.f11779V;
    }

    /* renamed from: k */
    public boolean mo27795k() {
        return this.f11794k0;
    }

    /* renamed from: l */
    public boolean mo27796l() {
        return this.f11793j0;
    }

    /* renamed from: m */
    public HostnameVerifier mo27797m() {
        return this.f11787d0;
    }

    /* renamed from: n */
    public List<Interceptor> mo27798n() {
        return this.f11777T;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: o */
    public InternalCache mo27799o() {
        C4941c cVar = this.f11782Y;
        return cVar != null ? cVar.f11238P : this.f11783Z;
    }

    /* renamed from: p */
    public List<Interceptor> mo27800p() {
        return this.f11778U;
    }

    /* renamed from: q */
    public int mo27801q() {
        return this.f11799p0;
    }

    /* renamed from: t */
    public List<Protocol> mo27802t() {
        return this.f11775R;
    }

    /* renamed from: u */
    public Proxy mo27803u() {
        return this.f11774Q;
    }

    /* renamed from: v */
    public Authenticator mo27804v() {
        return this.f11789f0;
    }

    /* renamed from: w */
    public ProxySelector mo27805w() {
        return this.f11780W;
    }

    /* renamed from: x */
    public int mo27806x() {
        return this.f11797n0;
    }

    /* renamed from: y */
    public boolean mo27807y() {
        return this.f11795l0;
    }

    /* renamed from: z */
    public SocketFactory mo27808z() {
        return this.f11784a0;
    }

    /* renamed from: i.v$b */
    /* compiled from: OkHttpClient */
    public static final class C5021b {

        /* renamed from: A */
        int f11800A;

        /* renamed from: a */
        C5010n f11801a = new C5010n();

        /* renamed from: b */
        Proxy f11802b;

        /* renamed from: c */
        List<Protocol> f11803c = OkHttpClient.f11771q0;

        /* renamed from: d */
        List<ConnectionSpec> f11804d = OkHttpClient.f11772r0;

        /* renamed from: e */
        final List<Interceptor> f11805e = new ArrayList();

        /* renamed from: f */
        final List<Interceptor> f11806f = new ArrayList();

        /* renamed from: g */
        EventListener.C5014c f11807g = EventListener.m17900a(EventListener.f11740a);

        /* renamed from: h */
        ProxySelector f11808h = ProxySelector.getDefault();

        /* renamed from: i */
        CookieJar f11809i = CookieJar.f11731a;

        /* renamed from: j */
        C4941c f11810j;

        /* renamed from: k */
        InternalCache f11811k;

        /* renamed from: l */
        SocketFactory f11812l = SocketFactory.getDefault();

        /* renamed from: m */
        SSLSocketFactory f11813m;

        /* renamed from: n */
        CertificateChainCleaner f11814n;

        /* renamed from: o */
        HostnameVerifier f11815o = OkHostnameVerifier.f11674a;

        /* renamed from: p */
        CertificatePinner f11816p = CertificatePinner.f11675c;

        /* renamed from: q */
        Authenticator f11817q;

        /* renamed from: r */
        Authenticator f11818r;

        /* renamed from: s */
        ConnectionPool f11819s;

        /* renamed from: t */
        Dns f11820t;

        /* renamed from: u */
        boolean f11821u;

        /* renamed from: v */
        boolean f11822v;

        /* renamed from: w */
        boolean f11823w;

        /* renamed from: x */
        int f11824x;

        /* renamed from: y */
        int f11825y;

        /* renamed from: z */
        int f11826z;

        public C5021b() {
            Authenticator bVar = Authenticator.f11235a;
            this.f11817q = bVar;
            this.f11818r = bVar;
            this.f11819s = new ConnectionPool();
            this.f11820t = Dns.f11739a;
            this.f11821u = true;
            this.f11822v = true;
            this.f11823w = true;
            this.f11824x = 10000;
            this.f11825y = 10000;
            this.f11826z = 10000;
            this.f11800A = 0;
        }

        /* renamed from: a */
        public C5021b mo27809a(C4941c cVar) {
            this.f11810j = cVar;
            this.f11811k = null;
            return this;
        }

        /* renamed from: a */
        public OkHttpClient mo27810a() {
            return new OkHttpClient(this);
        }
    }

    OkHttpClient(C5021b bVar) {
        boolean z;
        this.f11773P = bVar.f11801a;
        this.f11774Q = bVar.f11802b;
        this.f11775R = bVar.f11803c;
        this.f11776S = bVar.f11804d;
        this.f11777T = C4951c.m17348a(bVar.f11805e);
        this.f11778U = C4951c.m17348a(bVar.f11806f);
        this.f11779V = bVar.f11807g;
        this.f11780W = bVar.f11808h;
        this.f11781X = bVar.f11809i;
        this.f11782Y = bVar.f11810j;
        this.f11783Z = bVar.f11811k;
        this.f11784a0 = bVar.f11812l;
        Iterator<ConnectionSpec> it = this.f11776S.iterator();
        loop0:
        while (true) {
            z = false;
            while (true) {
                if (!it.hasNext()) {
                    break loop0;
                }
                ConnectionSpec next = it.next();
                if (z || next.mo27683b()) {
                    z = true;
                }
            }
        }
        if (bVar.f11813m != null || !z) {
            this.f11785b0 = bVar.f11813m;
            this.f11786c0 = bVar.f11814n;
        } else {
            X509TrustManager C = m17999C();
            this.f11785b0 = m18000a(C);
            this.f11786c0 = CertificateChainCleaner.m17834a(C);
        }
        this.f11787d0 = bVar.f11815o;
        this.f11788e0 = bVar.f11816p.mo27660a(this.f11786c0);
        this.f11789f0 = bVar.f11817q;
        this.f11790g0 = bVar.f11818r;
        this.f11791h0 = bVar.f11819s;
        this.f11792i0 = bVar.f11820t;
        this.f11793j0 = bVar.f11821u;
        this.f11794k0 = bVar.f11822v;
        this.f11795l0 = bVar.f11823w;
        this.f11796m0 = bVar.f11824x;
        this.f11797n0 = bVar.f11825y;
        this.f11798o0 = bVar.f11826z;
        this.f11799p0 = bVar.f11800A;
        if (this.f11777T.contains(null)) {
            throw new IllegalStateException("Null interceptor: " + this.f11777T);
        } else if (this.f11778U.contains(null)) {
            throw new IllegalStateException("Null network interceptor: " + this.f11778U);
        }
    }

    /* renamed from: a */
    public Authenticator mo27785a() {
        return this.f11790g0;
    }

    /* renamed from: a */
    public Call mo27396a(C5023y yVar) {
        return RealCall.m18042a(this, yVar, false);
    }
}
