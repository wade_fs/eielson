package p231i;

import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p231i.p232f0.C4951c;
import p231i.p232f0.p234f.RealConnection;
import p231i.p232f0.p234f.RouteDatabase;
import p231i.p232f0.p234f.StreamAllocation;
import p231i.p232f0.p239k.Platform;

/* renamed from: i.j */
public final class ConnectionPool {

    /* renamed from: g */
    private static final Executor f11699g = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), C4951c.m17350a("OkHttp ConnectionPool", true));

    /* renamed from: a */
    private final int f11700a;

    /* renamed from: b */
    private final long f11701b;

    /* renamed from: c */
    private final Runnable f11702c;

    /* renamed from: d */
    private final Deque<RealConnection> f11703d;

    /* renamed from: e */
    final RouteDatabase f11704e;

    /* renamed from: f */
    boolean f11705f;

    /* renamed from: i.j$a */
    /* compiled from: ConnectionPool */
    class C5007a implements Runnable {
        C5007a() {
        }

        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
            L_0x0000:
                i.j r0 = p231i.ConnectionPool.this
                long r1 = java.lang.System.nanoTime()
                long r0 = r0.mo27674a(r1)
                r2 = -1
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 != 0) goto L_0x0011
                return
            L_0x0011:
                r2 = 0
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 <= 0) goto L_0x0000
                r2 = 1000000(0xf4240, double:4.940656E-318)
                long r4 = r0 / r2
                long r2 = r2 * r4
                long r0 = r0 - r2
                i.j r2 = p231i.ConnectionPool.this
                monitor-enter(r2)
                i.j r3 = p231i.ConnectionPool.this     // Catch:{ InterruptedException -> 0x002b }
                int r1 = (int) r0     // Catch:{ InterruptedException -> 0x002b }
                r3.wait(r4, r1)     // Catch:{ InterruptedException -> 0x002b }
                goto L_0x002b
            L_0x0029:
                r0 = move-exception
                goto L_0x002d
            L_0x002b:
                monitor-exit(r2)     // Catch:{ all -> 0x0029 }
                goto L_0x0000
            L_0x002d:
                monitor-exit(r2)     // Catch:{ all -> 0x0029 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p231i.ConnectionPool.C5007a.run():void");
        }
    }

    public ConnectionPool() {
        this(5, 5, TimeUnit.MINUTES);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public RealConnection mo27675a(Address aVar, StreamAllocation gVar, Route c0Var) {
        for (RealConnection cVar : this.f11703d) {
            if (cVar.mo27453a(aVar, c0Var)) {
                gVar.mo27474a(cVar, true);
                return cVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27678b(RealConnection cVar) {
        if (!this.f11705f) {
            this.f11705f = true;
            f11699g.execute(this.f11702c);
        }
        this.f11703d.add(cVar);
    }

    public ConnectionPool(int i, long j, TimeUnit timeUnit) {
        this.f11702c = new C5007a();
        this.f11703d = new ArrayDeque();
        this.f11704e = new RouteDatabase();
        this.f11700a = i;
        this.f11701b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Socket mo27676a(Address aVar, StreamAllocation gVar) {
        for (RealConnection cVar : this.f11703d) {
            if (cVar.mo27453a(aVar, null) && cVar.mo27456b() && cVar != gVar.mo27477b()) {
                return gVar.mo27473a(cVar);
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo27677a(RealConnection cVar) {
        if (cVar.f11386k || this.f11700a == 0) {
            this.f11703d.remove(cVar);
            return true;
        }
        notifyAll();
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public long mo27674a(long j) {
        synchronized (this) {
            long j2 = Long.MIN_VALUE;
            RealConnection cVar = null;
            int i = 0;
            int i2 = 0;
            for (RealConnection cVar2 : this.f11703d) {
                if (m17856a(cVar2, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - cVar2.f11390o;
                    if (j3 > j2) {
                        cVar = cVar2;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.f11701b) {
                if (i <= this.f11700a) {
                    if (i > 0) {
                        long j4 = this.f11701b - j2;
                        return j4;
                    } else if (i2 > 0) {
                        long j5 = this.f11701b;
                        return j5;
                    } else {
                        this.f11705f = false;
                        return -1;
                    }
                }
            }
            this.f11703d.remove(cVar);
            C4951c.m17353a(cVar.mo27458d());
            return 0;
        }
    }

    /* renamed from: a */
    private int m17856a(RealConnection cVar, long j) {
        List<Reference<StreamAllocation>> list = cVar.f11389n;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                Platform.m17817d().mo27630a("A connection to " + cVar.mo27457c().mo27371a().mo27311k() + " was leaked. Did you forget to close a response body?", ((StreamAllocation.C4965a) reference).f11418a);
                list.remove(i);
                cVar.f11386k = true;
                if (list.isEmpty()) {
                    cVar.f11390o = j - this.f11701b;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
