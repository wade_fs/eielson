package p231i;

import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import p231i.HttpUrl;
import p231i.p232f0.C4951c;

/* renamed from: i.a */
public final class Address {

    /* renamed from: a */
    final HttpUrl f11199a;

    /* renamed from: b */
    final Dns f11200b;

    /* renamed from: c */
    final SocketFactory f11201c;

    /* renamed from: d */
    final Authenticator f11202d;

    /* renamed from: e */
    final List<Protocol> f11203e;

    /* renamed from: f */
    final List<ConnectionSpec> f11204f;

    /* renamed from: g */
    final ProxySelector f11205g;

    /* renamed from: h */
    final Proxy f11206h;

    /* renamed from: i */
    final SSLSocketFactory f11207i;

    /* renamed from: j */
    final HostnameVerifier f11208j;

    /* renamed from: k */
    final CertificatePinner f11209k;

    public Address(String str, int i, Dns oVar, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, CertificatePinner gVar, Authenticator bVar, Proxy proxy, List<Protocol> list, List<ConnectionSpec> list2, ProxySelector proxySelector) {
        HttpUrl.C5017a aVar = new HttpUrl.C5017a();
        aVar.mo27779d(sSLSocketFactory != null ? "https" : "http");
        aVar.mo27776b(str);
        aVar.mo27772a(i);
        this.f11199a = aVar.mo27774a();
        if (oVar != null) {
            this.f11200b = oVar;
            if (socketFactory != null) {
                this.f11201c = socketFactory;
                if (bVar != null) {
                    this.f11202d = bVar;
                    if (list != null) {
                        this.f11203e = C4951c.m17348a(list);
                        if (list2 != null) {
                            this.f11204f = C4951c.m17348a(list2);
                            if (proxySelector != null) {
                                this.f11205g = proxySelector;
                                this.f11206h = proxy;
                                this.f11207i = sSLSocketFactory;
                                this.f11208j = hostnameVerifier;
                                this.f11209k = gVar;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    /* renamed from: a */
    public CertificatePinner mo27298a() {
        return this.f11209k;
    }

    /* renamed from: b */
    public List<ConnectionSpec> mo27300b() {
        return this.f11204f;
    }

    /* renamed from: c */
    public Dns mo27301c() {
        return this.f11200b;
    }

    /* renamed from: d */
    public HostnameVerifier mo27302d() {
        return this.f11208j;
    }

    /* renamed from: e */
    public List<Protocol> mo27303e() {
        return this.f11203e;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Address) {
            Address aVar = (Address) obj;
            return this.f11199a.equals(aVar.f11199a) && mo27299a(aVar);
        }
    }

    /* renamed from: f */
    public Proxy mo27305f() {
        return this.f11206h;
    }

    /* renamed from: g */
    public Authenticator mo27306g() {
        return this.f11202d;
    }

    /* renamed from: h */
    public ProxySelector mo27307h() {
        return this.f11205g;
    }

    public int hashCode() {
        int hashCode = (((((((((((527 + this.f11199a.hashCode()) * 31) + this.f11200b.hashCode()) * 31) + this.f11202d.hashCode()) * 31) + this.f11203e.hashCode()) * 31) + this.f11204f.hashCode()) * 31) + this.f11205g.hashCode()) * 31;
        Proxy proxy = this.f11206h;
        int i = 0;
        int hashCode2 = (hashCode + (proxy != null ? proxy.hashCode() : 0)) * 31;
        SSLSocketFactory sSLSocketFactory = this.f11207i;
        int hashCode3 = (hashCode2 + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.f11208j;
        int hashCode4 = (hashCode3 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        CertificatePinner gVar = this.f11209k;
        if (gVar != null) {
            i = gVar.hashCode();
        }
        return hashCode4 + i;
    }

    /* renamed from: i */
    public SocketFactory mo27309i() {
        return this.f11201c;
    }

    /* renamed from: j */
    public SSLSocketFactory mo27310j() {
        return this.f11207i;
    }

    /* renamed from: k */
    public HttpUrl mo27311k() {
        return this.f11199a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.f11199a.mo27761g());
        sb.append(":");
        sb.append(this.f11199a.mo27765j());
        if (this.f11206h != null) {
            sb.append(", proxy=");
            sb.append(this.f11206h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.f11205g);
        }
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo27299a(Address aVar) {
        return this.f11200b.equals(aVar.f11200b) && this.f11202d.equals(aVar.f11202d) && this.f11203e.equals(aVar.f11203e) && this.f11204f.equals(aVar.f11204f) && this.f11205g.equals(aVar.f11205g) && C4951c.m17356a(this.f11206h, aVar.f11206h) && C4951c.m17356a(this.f11207i, aVar.f11207i) && C4951c.m17356a(this.f11208j, aVar.f11208j) && C4951c.m17356a(this.f11209k, aVar.f11209k) && mo27311k().mo27765j() == aVar.mo27311k().mo27765j();
    }
}
