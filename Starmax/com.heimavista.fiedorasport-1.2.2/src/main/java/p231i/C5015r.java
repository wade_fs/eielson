package p231i;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import p231i.p232f0.C4951c;

/* renamed from: i.r */
/* compiled from: Headers */
public final class C5015r {

    /* renamed from: a */
    private final String[] f11746a;

    C5015r(C5016a aVar) {
        List<String> list = aVar.f11747a;
        this.f11746a = (String[]) list.toArray(new String[list.size()]);
    }

    /* renamed from: a */
    public String mo27738a(String str) {
        return m17929a(this.f11746a, str);
    }

    /* renamed from: b */
    public int mo27739b() {
        return this.f11746a.length / 2;
    }

    public boolean equals(Object obj) {
        return (obj instanceof C5015r) && Arrays.equals(((C5015r) obj).f11746a, this.f11746a);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f11746a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int b = mo27739b();
        for (int i = 0; i < b; i++) {
            sb.append(mo27737a(i));
            sb.append(": ");
            sb.append(mo27740b(i));
            sb.append("\n");
        }
        return sb.toString();
    }

    /* renamed from: i.r$a */
    /* compiled from: Headers */
    public static final class C5016a {

        /* renamed from: a */
        final List<String> f11747a = new ArrayList(20);

        /* renamed from: d */
        private void m17936d(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (!str.isEmpty()) {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= ' ' || charAt >= 127) {
                        throw new IllegalArgumentException(C4951c.m17345a("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 != null) {
                    int length2 = str2.length();
                    int i2 = 0;
                    while (i2 < length2) {
                        char charAt2 = str2.charAt(i2);
                        if ((charAt2 > 31 || charAt2 == 9) && charAt2 < 127) {
                            i2++;
                        } else {
                            throw new IllegalArgumentException(C4951c.m17345a("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                        }
                    }
                    return;
                }
                throw new NullPointerException("value for name " + str + " == null");
            } else {
                throw new IllegalArgumentException("name is empty");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C5016a mo27745a(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                mo27748b(str.substring(0, indexOf), str.substring(indexOf + 1));
                return this;
            } else if (str.startsWith(":")) {
                mo27748b("", str.substring(1));
                return this;
            } else {
                mo27748b("", str);
                return this;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public C5016a mo27748b(String str, String str2) {
            this.f11747a.add(str);
            this.f11747a.add(str2.trim());
            return this;
        }

        /* renamed from: c */
        public C5016a mo27750c(String str) {
            int i = 0;
            while (i < this.f11747a.size()) {
                if (str.equalsIgnoreCase(this.f11747a.get(i))) {
                    this.f11747a.remove(i);
                    this.f11747a.remove(i);
                    i -= 2;
                }
                i += 2;
            }
            return this;
        }

        /* renamed from: b */
        public String mo27749b(String str) {
            for (int size = this.f11747a.size() - 2; size >= 0; size -= 2) {
                if (str.equalsIgnoreCase(this.f11747a.get(size))) {
                    return this.f11747a.get(size + 1);
                }
            }
            return null;
        }

        /* renamed from: c */
        public C5016a mo27751c(String str, String str2) {
            m17936d(str, str2);
            mo27750c(str);
            mo27748b(str, str2);
            return this;
        }

        /* renamed from: a */
        public C5016a mo27746a(String str, String str2) {
            m17936d(str, str2);
            mo27748b(str, str2);
            return this;
        }

        /* renamed from: a */
        public C5015r mo27747a() {
            return new C5015r(this);
        }
    }

    /* renamed from: a */
    public String mo27737a(int i) {
        return this.f11746a[i * 2];
    }

    /* renamed from: b */
    public String mo27740b(int i) {
        return this.f11746a[(i * 2) + 1];
    }

    /* renamed from: a */
    public C5016a mo27736a() {
        C5016a aVar = new C5016a();
        Collections.addAll(aVar.f11747a, this.f11746a);
        return aVar;
    }

    /* renamed from: b */
    public List<String> mo27741b(String str) {
        int b = mo27739b();
        ArrayList arrayList = null;
        for (int i = 0; i < b; i++) {
            if (str.equalsIgnoreCase(mo27737a(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(mo27740b(i));
            }
        }
        if (arrayList != null) {
            return Collections.unmodifiableList(arrayList);
        }
        return Collections.emptyList();
    }

    /* renamed from: a */
    private static String m17929a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }
}
