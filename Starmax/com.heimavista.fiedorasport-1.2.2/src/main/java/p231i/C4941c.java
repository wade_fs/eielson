package p231i;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import p231i.C5015r;
import p231i.C5023y;
import p231i.Response;
import p231i.p232f0.C4951c;
import p231i.p232f0.p233e.C4957d;
import p231i.p232f0.p233e.CacheRequest;
import p231i.p232f0.p233e.CacheStrategy;
import p231i.p232f0.p233e.InternalCache;
import p231i.p232f0.p235g.HttpHeaders;
import p231i.p232f0.p235g.HttpMethod;
import p231i.p232f0.p235g.StatusLine;
import p231i.p232f0.p238j.FileSystem;
import p231i.p232f0.p239k.Platform;
import p244j.Buffer;
import p244j.BufferedSink;
import p244j.BufferedSource;
import p244j.ByteString;
import p244j.C5049s;
import p244j.ForwardingSink;
import p244j.ForwardingSource;
import p244j.Okio;
import p244j.Sink;

/* renamed from: i.c */
/* compiled from: Cache */
public final class C4941c implements Closeable, Flushable {

    /* renamed from: P */
    final InternalCache f11238P;

    /* renamed from: Q */
    final C4957d f11239Q;

    /* renamed from: R */
    int f11240R;

    /* renamed from: S */
    int f11241S;

    /* renamed from: T */
    private int f11242T;

    /* renamed from: U */
    private int f11243U;

    /* renamed from: V */
    private int f11244V;

    /* renamed from: i.c$a */
    /* compiled from: Cache */
    class C4942a implements InternalCache {
        C4942a() {
        }

        /* renamed from: a */
        public CacheRequest mo27358a(Response a0Var) {
            return C4941c.this.mo27351a(a0Var);
        }

        /* renamed from: b */
        public Response mo27363b(C5023y yVar) {
            return C4941c.this.mo27350a(yVar);
        }

        /* renamed from: a */
        public void mo27362a(C5023y yVar) {
            C4941c.this.mo27355b(yVar);
        }

        /* renamed from: a */
        public void mo27360a(Response a0Var, Response a0Var2) {
            C4941c.this.mo27353a(a0Var, a0Var2);
        }

        /* renamed from: a */
        public void mo27359a() {
            C4941c.this.mo27352a();
        }

        /* renamed from: a */
        public void mo27361a(CacheStrategy cVar) {
            C4941c.this.mo27354a(cVar);
        }
    }

    /* renamed from: i.c$b */
    /* compiled from: Cache */
    private final class C4943b implements CacheRequest {

        /* renamed from: a */
        private final C4957d.C4960c f11246a;

        /* renamed from: b */
        private Sink f11247b;

        /* renamed from: c */
        private Sink f11248c;

        /* renamed from: d */
        boolean f11249d;

        /* renamed from: i.c$b$a */
        /* compiled from: Cache */
        class C4944a extends ForwardingSink {

            /* renamed from: Q */
            final /* synthetic */ C4957d.C4960c f11251Q;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C4944a(Sink rVar, C4941c cVar, C4957d.C4960c cVar2) {
                super(rVar);
                this.f11251Q = cVar2;
            }

            public void close() {
                synchronized (C4941c.this) {
                    if (!C4943b.this.f11249d) {
                        C4943b.this.f11249d = true;
                        C4941c.this.f11240R++;
                        super.close();
                        this.f11251Q.mo27436b();
                    }
                }
            }
        }

        C4943b(C4957d.C4960c cVar) {
            this.f11246a = cVar;
            this.f11247b = cVar.mo27434a(1);
            this.f11248c = new C4944a(this.f11247b, C4941c.this, cVar);
        }

        /* renamed from: a */
        public void mo27364a() {
            synchronized (C4941c.this) {
                if (!this.f11249d) {
                    this.f11249d = true;
                    C4941c.this.f11241S++;
                    C4951c.m17352a(this.f11247b);
                    try {
                        this.f11246a.mo27435a();
                    } catch (IOException unused) {
                    }
                }
            }
        }

        /* renamed from: b */
        public Sink mo27365b() {
            return this.f11248c;
        }
    }

    /* renamed from: i.c$c */
    /* compiled from: Cache */
    private static class C4945c extends ResponseBody {

        /* renamed from: P */
        final C4957d.C4963e f11253P;

        /* renamed from: Q */
        private final BufferedSource f11254Q;

        /* renamed from: R */
        private final String f11255R;

        /* renamed from: i.c$c$a */
        /* compiled from: Cache */
        class C4946a extends ForwardingSource {

            /* renamed from: Q */
            final /* synthetic */ C4957d.C4963e f11256Q;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C4946a(C4945c cVar, C5049s sVar, C4957d.C4963e eVar) {
                super(sVar);
                this.f11256Q = eVar;
            }

            public void close() {
                this.f11256Q.close();
                super.close();
            }
        }

        C4945c(C4957d.C4963e eVar, String str, String str2) {
            this.f11253P = eVar;
            this.f11255R = str2;
            this.f11254Q = Okio.m18212a(new C4946a(this, eVar.mo27442a(1), eVar));
        }

        /* renamed from: a */
        public long mo27347a() {
            try {
                if (this.f11255R != null) {
                    return Long.parseLong(this.f11255R);
                }
                return -1;
            } catch (NumberFormatException unused) {
                return -1;
            }
        }

        /* renamed from: b */
        public BufferedSource mo27348b() {
            return this.f11254Q;
        }
    }

    public C4941c(File file, long j) {
        this(file, j, FileSystem.f11644a);
    }

    /* renamed from: a */
    public static String m17275a(HttpUrl sVar) {
        return ByteString.m18170d(sVar.toString()).mo28101i().mo28099h();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27355b(C5023y yVar) {
        this.f11239Q.mo27428d(m17275a(yVar.mo27827g()));
    }

    public void close() {
        this.f11239Q.close();
    }

    public void flush() {
        this.f11239Q.flush();
    }

    C4941c(File file, long j, FileSystem aVar) {
        this.f11238P = new C4942a();
        this.f11239Q = C4957d.m17387a(aVar, file, 201105, 2, j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Response mo27350a(C5023y yVar) {
        try {
            C4957d.C4963e c = this.f11239Q.mo27424c(m17275a(yVar.mo27827g()));
            if (c == null) {
                return null;
            }
            try {
                C4947d dVar = new C4947d(c.mo27442a(0));
                Response a = dVar.mo27368a(c);
                if (dVar.mo27370a(yVar, a)) {
                    return a;
                }
                C4951c.m17352a(a.mo27315a());
                return null;
            } catch (IOException unused) {
                C4951c.m17352a(c);
                return null;
            }
        } catch (IOException unused2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public CacheRequest mo27351a(Response a0Var) {
        C4957d.C4960c cVar;
        String e = a0Var.mo27313A().mo27825e();
        if (HttpMethod.m17509a(a0Var.mo27313A().mo27825e())) {
            try {
                mo27355b(a0Var.mo27313A());
            } catch (IOException unused) {
            }
            return null;
        } else if (!e.equals("GET") || HttpHeaders.m17506c(a0Var)) {
            return null;
        } else {
            C4947d dVar = new C4947d(a0Var);
            try {
                cVar = this.f11239Q.mo27422b(m17275a(a0Var.mo27313A().mo27827g()));
                if (cVar == null) {
                    return null;
                }
                try {
                    dVar.mo27369a(cVar);
                    return new C4943b(cVar);
                } catch (IOException unused2) {
                    m17276a(cVar);
                    return null;
                }
            } catch (IOException unused3) {
                cVar = null;
                m17276a(cVar);
                return null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27353a(Response a0Var, Response a0Var2) {
        C4957d.C4960c cVar;
        C4947d dVar = new C4947d(a0Var2);
        try {
            cVar = ((C4945c) a0Var.mo27315a()).f11253P.mo27441a();
            if (cVar != null) {
                try {
                    dVar.mo27369a(cVar);
                    cVar.mo27436b();
                } catch (IOException unused) {
                }
            }
        } catch (IOException unused2) {
            cVar = null;
            m17276a(cVar);
        }
    }

    /* renamed from: a */
    private void m17276a(C4957d.C4960c cVar) {
        if (cVar != null) {
            try {
                cVar.mo27435a();
            } catch (IOException unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo27354a(CacheStrategy cVar) {
        this.f11244V++;
        if (cVar.f11317a != null) {
            this.f11242T++;
        } else if (cVar.f11318b != null) {
            this.f11243U++;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo27352a() {
        this.f11243U++;
    }

    /* renamed from: a */
    static int m17274a(BufferedSource eVar) {
        try {
            long l = eVar.mo28065l();
            String m = eVar.mo28066m();
            if (l >= 0 && l <= 2147483647L && m.isEmpty()) {
                return (int) l;
            }
            throw new IOException("expected an int but was \"" + l + m + "\"");
        } catch (NumberFormatException e) {
            throw new IOException(e.getMessage());
        }
    }

    /* renamed from: i.c$d */
    /* compiled from: Cache */
    private static final class C4947d {

        /* renamed from: k */
        private static final String f11257k = (Platform.m17817d().mo27652a() + "-Sent-Millis");

        /* renamed from: l */
        private static final String f11258l = (Platform.m17817d().mo27652a() + "-Received-Millis");

        /* renamed from: a */
        private final String f11259a;

        /* renamed from: b */
        private final C5015r f11260b;

        /* renamed from: c */
        private final String f11261c;

        /* renamed from: d */
        private final Protocol f11262d;

        /* renamed from: e */
        private final int f11263e;

        /* renamed from: f */
        private final String f11264f;

        /* renamed from: g */
        private final C5015r f11265g;

        /* renamed from: h */
        private final Handshake f11266h;

        /* renamed from: i */
        private final long f11267i;

        /* renamed from: j */
        private final long f11268j;

        C4947d(C5049s sVar) {
            TlsVersion d0Var;
            try {
                BufferedSource a = Okio.m18212a(sVar);
                this.f11259a = a.mo28066m();
                this.f11261c = a.mo28066m();
                C5015r.C5016a aVar = new C5015r.C5016a();
                int a2 = C4941c.m17274a(a);
                for (int i = 0; i < a2; i++) {
                    aVar.mo27745a(a.mo28066m());
                }
                this.f11260b = aVar.mo27747a();
                StatusLine a3 = StatusLine.m17539a(a.mo28066m());
                this.f11262d = a3.f11443a;
                this.f11263e = a3.f11444b;
                this.f11264f = a3.f11445c;
                C5015r.C5016a aVar2 = new C5015r.C5016a();
                int a4 = C4941c.m17274a(a);
                for (int i2 = 0; i2 < a4; i2++) {
                    aVar2.mo27745a(a.mo28066m());
                }
                String b = aVar2.mo27749b(f11257k);
                String b2 = aVar2.mo27749b(f11258l);
                aVar2.mo27750c(f11257k);
                aVar2.mo27750c(f11258l);
                long j = 0;
                this.f11267i = b != null ? Long.parseLong(b) : 0;
                this.f11268j = b2 != null ? Long.parseLong(b2) : j;
                this.f11265g = aVar2.mo27747a();
                if (m17295a()) {
                    String m = a.mo28066m();
                    if (m.length() <= 0) {
                        CipherSuite a5 = CipherSuite.m17851a(a.mo28066m());
                        List<Certificate> a6 = m17293a(a);
                        List<Certificate> a7 = m17293a(a);
                        if (!a.mo28063j()) {
                            d0Var = TlsVersion.m17320a(a.mo28066m());
                        } else {
                            d0Var = TlsVersion.SSL_3_0;
                        }
                        this.f11266h = Handshake.m17923a(d0Var, a5, a6, a7);
                    } else {
                        throw new IOException("expected \"\" but was \"" + m + "\"");
                    }
                } else {
                    this.f11266h = null;
                }
            } finally {
                sVar.close();
            }
        }

        /* renamed from: a */
        public void mo27369a(C4957d.C4960c cVar) {
            BufferedSink a = Okio.m18211a(cVar.mo27434a(0));
            a.mo28033a(this.f11259a).writeByte(10);
            a.mo28033a(this.f11261c).writeByte(10);
            a.mo28055f((long) this.f11260b.mo27739b()).writeByte(10);
            int b = this.f11260b.mo27739b();
            for (int i = 0; i < b; i++) {
                a.mo28033a(this.f11260b.mo27737a(i)).mo28033a(": ").mo28033a(this.f11260b.mo27740b(i)).writeByte(10);
            }
            a.mo28033a(new StatusLine(this.f11262d, this.f11263e, this.f11264f).toString()).writeByte(10);
            a.mo28055f((long) (this.f11265g.mo27739b() + 2)).writeByte(10);
            int b2 = this.f11265g.mo27739b();
            for (int i2 = 0; i2 < b2; i2++) {
                a.mo28033a(this.f11265g.mo27737a(i2)).mo28033a(": ").mo28033a(this.f11265g.mo27740b(i2)).writeByte(10);
            }
            a.mo28033a(f11257k).mo28033a(": ").mo28055f(this.f11267i).writeByte(10);
            a.mo28033a(f11258l).mo28033a(": ").mo28055f(this.f11268j).writeByte(10);
            if (m17295a()) {
                a.writeByte(10);
                a.mo28033a(this.f11266h.mo27730a().mo27670a()).writeByte(10);
                m17294a(a, this.f11266h.mo27732c());
                m17294a(a, this.f11266h.mo27731b());
                a.mo28033a(this.f11266h.mo27733d().mo27394a()).writeByte(10);
            }
            a.close();
        }

        /* renamed from: a */
        private boolean m17295a() {
            return this.f11259a.startsWith("https://");
        }

        C4947d(Response a0Var) {
            this.f11259a = a0Var.mo27313A().mo27827g().toString();
            this.f11260b = HttpHeaders.m17508e(a0Var);
            this.f11261c = a0Var.mo27313A().mo27825e();
            this.f11262d = a0Var.mo27330y();
            this.f11263e = a0Var.mo27321d();
            this.f11264f = a0Var.mo27326u();
            this.f11265g = a0Var.mo27323g();
            this.f11266h = a0Var.mo27322e();
            this.f11267i = a0Var.mo27314B();
            this.f11268j = a0Var.mo27331z();
        }

        /* renamed from: a */
        private List<Certificate> m17293a(BufferedSource eVar) {
            int a = C4941c.m17274a(eVar);
            if (a == -1) {
                return Collections.emptyList();
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(a);
                for (int i = 0; i < a; i++) {
                    String m = eVar.mo28066m();
                    Buffer cVar = new Buffer();
                    cVar.mo28032a(ByteString.m18166a(m));
                    arrayList.add(instance.generateCertificate(cVar.mo28070q()));
                }
                return arrayList;
            } catch (CertificateException e) {
                throw new IOException(e.getMessage());
            }
        }

        /* renamed from: a */
        private void m17294a(BufferedSink dVar, List<Certificate> list) {
            try {
                dVar.mo28055f((long) list.size()).writeByte(10);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    dVar.mo28033a(ByteString.m18167a(list.get(i).getEncoded()).mo28092a()).writeByte(10);
                }
            } catch (CertificateEncodingException e) {
                throw new IOException(e.getMessage());
            }
        }

        /* renamed from: a */
        public boolean mo27370a(C5023y yVar, Response a0Var) {
            return this.f11259a.equals(yVar.mo27827g().toString()) && this.f11261c.equals(yVar.mo27825e()) && HttpHeaders.m17501a(a0Var, this.f11260b, yVar);
        }

        /* renamed from: a */
        public Response mo27368a(C4957d.C4963e eVar) {
            String a = this.f11265g.mo27738a("Content-Type");
            String a2 = this.f11265g.mo27738a("Content-Length");
            C5023y.C5024a aVar = new C5023y.C5024a();
            aVar.mo27836b(this.f11259a);
            aVar.mo27833a(this.f11261c, (RequestBody) null);
            aVar.mo27830a(this.f11260b);
            C5023y a3 = aVar.mo27835a();
            Response.C4938a aVar2 = new Response.C4938a();
            aVar2.mo27339a(a3);
            aVar2.mo27338a(this.f11262d);
            aVar2.mo27332a(this.f11263e);
            aVar2.mo27340a(this.f11264f);
            aVar2.mo27337a(this.f11265g);
            aVar2.mo27335a(new C4945c(eVar, a, a2));
            aVar2.mo27336a(this.f11266h);
            aVar2.mo27343b(this.f11267i);
            aVar2.mo27333a(this.f11268j);
            return aVar2.mo27342a();
        }
    }
}
