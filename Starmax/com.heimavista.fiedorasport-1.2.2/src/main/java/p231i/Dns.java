package p231i;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

/* renamed from: i.o */
public interface Dns {

    /* renamed from: a */
    public static final Dns f11739a = new C5011a();

    /* renamed from: i.o$a */
    /* compiled from: Dns */
    class C5011a implements Dns {
        C5011a() {
        }

        /* renamed from: a */
        public List<InetAddress> mo27708a(String str) {
            if (str != null) {
                try {
                    return Arrays.asList(InetAddress.getAllByName(str));
                } catch (NullPointerException e) {
                    UnknownHostException unknownHostException = new UnknownHostException("Broken system behaviour for dns lookup of " + str);
                    unknownHostException.initCause(e);
                    throw unknownHostException;
                }
            } else {
                throw new UnknownHostException("hostname == null");
            }
        }
    }

    /* renamed from: a */
    List<InetAddress> mo27708a(String str);
}
