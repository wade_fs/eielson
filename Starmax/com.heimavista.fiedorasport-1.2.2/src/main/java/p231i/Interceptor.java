package p231i;

/* renamed from: i.t */
public interface Interceptor {

    /* renamed from: i.t$a */
    /* compiled from: Interceptor */
    public interface C5019a {
        /* renamed from: a */
        int mo27490a();

        /* renamed from: a */
        Response mo27491a(C5023y yVar);

        /* renamed from: b */
        int mo27493b();

        /* renamed from: c */
        int mo27494c();

        /* renamed from: d */
        C5023y mo27495d();
    }

    /* renamed from: a */
    Response mo27414a(C5019a aVar);
}
