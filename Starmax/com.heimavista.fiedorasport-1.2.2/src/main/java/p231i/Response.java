package p231i;

import java.io.Closeable;
import p231i.C5015r;

/* renamed from: i.a0 */
public final class Response implements Closeable {

    /* renamed from: P */
    final C5023y f11210P;

    /* renamed from: Q */
    final Protocol f11211Q;

    /* renamed from: R */
    final int f11212R;

    /* renamed from: S */
    final String f11213S;

    /* renamed from: T */
    final Handshake f11214T;

    /* renamed from: U */
    final C5015r f11215U;

    /* renamed from: V */
    final ResponseBody f11216V;

    /* renamed from: W */
    final Response f11217W;

    /* renamed from: X */
    final Response f11218X;

    /* renamed from: Y */
    final Response f11219Y;

    /* renamed from: Z */
    final long f11220Z;

    /* renamed from: a0 */
    final long f11221a0;

    /* renamed from: b0 */
    private volatile CacheControl f11222b0;

    /* renamed from: i.a0$a */
    /* compiled from: Response */
    public static class C4938a {

        /* renamed from: a */
        C5023y f11223a;

        /* renamed from: b */
        Protocol f11224b;

        /* renamed from: c */
        int f11225c;

        /* renamed from: d */
        String f11226d;

        /* renamed from: e */
        Handshake f11227e;

        /* renamed from: f */
        C5015r.C5016a f11228f;

        /* renamed from: g */
        ResponseBody f11229g;

        /* renamed from: h */
        Response f11230h;

        /* renamed from: i */
        Response f11231i;

        /* renamed from: j */
        Response f11232j;

        /* renamed from: k */
        long f11233k;

        /* renamed from: l */
        long f11234l;

        public C4938a() {
            this.f11225c = -1;
            this.f11228f = new C5015r.C5016a();
        }

        /* renamed from: d */
        private void m17251d(Response a0Var) {
            if (a0Var.f11216V != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        /* renamed from: a */
        public C4938a mo27339a(C5023y yVar) {
            this.f11223a = yVar;
            return this;
        }

        /* renamed from: b */
        public C4938a mo27344b(Response a0Var) {
            if (a0Var != null) {
                m17250a("networkResponse", a0Var);
            }
            this.f11230h = a0Var;
            return this;
        }

        /* renamed from: c */
        public C4938a mo27345c(Response a0Var) {
            if (a0Var != null) {
                m17251d(a0Var);
            }
            this.f11232j = a0Var;
            return this;
        }

        /* renamed from: a */
        public C4938a mo27338a(Protocol wVar) {
            this.f11224b = wVar;
            return this;
        }

        /* renamed from: a */
        public C4938a mo27332a(int i) {
            this.f11225c = i;
            return this;
        }

        /* renamed from: b */
        public C4938a mo27343b(long j) {
            this.f11233k = j;
            return this;
        }

        C4938a(Response a0Var) {
            this.f11225c = -1;
            this.f11223a = a0Var.f11210P;
            this.f11224b = a0Var.f11211Q;
            this.f11225c = a0Var.f11212R;
            this.f11226d = a0Var.f11213S;
            this.f11227e = a0Var.f11214T;
            this.f11228f = a0Var.f11215U.mo27736a();
            this.f11229g = a0Var.f11216V;
            this.f11230h = a0Var.f11217W;
            this.f11231i = a0Var.f11218X;
            this.f11232j = a0Var.f11219Y;
            this.f11233k = a0Var.f11220Z;
            this.f11234l = a0Var.f11221a0;
        }

        /* renamed from: a */
        public C4938a mo27340a(String str) {
            this.f11226d = str;
            return this;
        }

        /* renamed from: a */
        public C4938a mo27336a(Handshake qVar) {
            this.f11227e = qVar;
            return this;
        }

        /* renamed from: a */
        public C4938a mo27341a(String str, String str2) {
            this.f11228f.mo27746a(str, str2);
            return this;
        }

        /* renamed from: a */
        public C4938a mo27337a(C5015r rVar) {
            this.f11228f = rVar.mo27736a();
            return this;
        }

        /* renamed from: a */
        public C4938a mo27335a(ResponseBody b0Var) {
            this.f11229g = b0Var;
            return this;
        }

        /* renamed from: a */
        public C4938a mo27334a(Response a0Var) {
            if (a0Var != null) {
                m17250a("cacheResponse", a0Var);
            }
            this.f11231i = a0Var;
            return this;
        }

        /* renamed from: a */
        private void m17250a(String str, Response a0Var) {
            if (a0Var.f11216V != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (a0Var.f11217W != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (a0Var.f11218X != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (a0Var.f11219Y != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        /* renamed from: a */
        public C4938a mo27333a(long j) {
            this.f11234l = j;
            return this;
        }

        /* renamed from: a */
        public Response mo27342a() {
            if (this.f11223a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f11224b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f11225c < 0) {
                throw new IllegalStateException("code < 0: " + this.f11225c);
            } else if (this.f11226d != null) {
                return new Response(this);
            } else {
                throw new IllegalStateException("message == null");
            }
        }
    }

    Response(C4938a aVar) {
        this.f11210P = aVar.f11223a;
        this.f11211Q = aVar.f11224b;
        this.f11212R = aVar.f11225c;
        this.f11213S = aVar.f11226d;
        this.f11214T = aVar.f11227e;
        this.f11215U = aVar.f11228f.mo27747a();
        this.f11216V = aVar.f11229g;
        this.f11217W = aVar.f11230h;
        this.f11218X = aVar.f11231i;
        this.f11219Y = aVar.f11232j;
        this.f11220Z = aVar.f11233k;
        this.f11221a0 = aVar.f11234l;
    }

    /* renamed from: A */
    public C5023y mo27313A() {
        return this.f11210P;
    }

    /* renamed from: B */
    public long mo27314B() {
        return this.f11220Z;
    }

    /* renamed from: a */
    public String mo27316a(String str, String str2) {
        String a = this.f11215U.mo27738a(str);
        return a != null ? a : str2;
    }

    /* renamed from: b */
    public String mo27318b(String str) {
        return mo27316a(str, null);
    }

    /* renamed from: c */
    public Response mo27319c() {
        return this.f11218X;
    }

    public void close() {
        ResponseBody b0Var = this.f11216V;
        if (b0Var != null) {
            b0Var.close();
            return;
        }
        throw new IllegalStateException("response is not eligible for a body and must not be closed");
    }

    /* renamed from: d */
    public int mo27321d() {
        return this.f11212R;
    }

    /* renamed from: e */
    public Handshake mo27322e() {
        return this.f11214T;
    }

    /* renamed from: g */
    public C5015r mo27323g() {
        return this.f11215U;
    }

    /* renamed from: t */
    public boolean mo27324t() {
        int i = this.f11212R;
        return i >= 200 && i < 300;
    }

    public String toString() {
        return "Response{protocol=" + this.f11211Q + ", code=" + this.f11212R + ", message=" + this.f11213S + ", url=" + this.f11210P.mo27827g() + '}';
    }

    /* renamed from: u */
    public String mo27326u() {
        return this.f11213S;
    }

    /* renamed from: v */
    public Response mo27327v() {
        return this.f11217W;
    }

    /* renamed from: w */
    public C4938a mo27328w() {
        return new C4938a(this);
    }

    /* renamed from: x */
    public Response mo27329x() {
        return this.f11219Y;
    }

    /* renamed from: y */
    public Protocol mo27330y() {
        return this.f11211Q;
    }

    /* renamed from: z */
    public long mo27331z() {
        return this.f11221a0;
    }

    /* renamed from: a */
    public ResponseBody mo27315a() {
        return this.f11216V;
    }

    /* renamed from: b */
    public CacheControl mo27317b() {
        CacheControl dVar = this.f11222b0;
        if (dVar != null) {
            return dVar;
        }
        CacheControl a = CacheControl.m17303a(this.f11215U);
        this.f11222b0 = a;
        return a;
    }
}
