package p231i;

import java.io.Closeable;
import p231i.p232f0.C4951c;
import p244j.Buffer;
import p244j.BufferedSource;

/* renamed from: i.b0 */
public abstract class ResponseBody implements Closeable {

    /* renamed from: i.b0$a */
    /* compiled from: ResponseBody */
    class C4940a extends ResponseBody {

        /* renamed from: P */
        final /* synthetic */ long f11236P;

        /* renamed from: Q */
        final /* synthetic */ BufferedSource f11237Q;

        C4940a(MediaType uVar, long j, BufferedSource eVar) {
            this.f11236P = j;
            this.f11237Q = eVar;
        }

        /* renamed from: a */
        public long mo27347a() {
            return this.f11236P;
        }

        /* renamed from: b */
        public BufferedSource mo27348b() {
            return this.f11237Q;
        }
    }

    /* renamed from: a */
    public static ResponseBody m17269a(MediaType uVar, byte[] bArr) {
        Buffer cVar = new Buffer();
        cVar.write(bArr);
        return m17268a(uVar, (long) bArr.length, cVar);
    }

    /* renamed from: a */
    public abstract long mo27347a();

    /* renamed from: b */
    public abstract BufferedSource mo27348b();

    public void close() {
        C4951c.m17352a(mo27348b());
    }

    /* renamed from: a */
    public static ResponseBody m17268a(MediaType uVar, long j, BufferedSource eVar) {
        if (eVar != null) {
            return new C4940a(uVar, j, eVar);
        }
        throw new NullPointerException("source == null");
    }
}
