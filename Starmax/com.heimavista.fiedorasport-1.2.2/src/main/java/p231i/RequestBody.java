package p231i;

import p231i.p232f0.C4951c;
import p244j.BufferedSink;

/* renamed from: i.z */
public abstract class RequestBody {

    /* renamed from: i.z$a */
    /* compiled from: RequestBody */
    class C5025a extends RequestBody {

        /* renamed from: a */
        final /* synthetic */ MediaType f11853a;

        /* renamed from: b */
        final /* synthetic */ int f11854b;

        /* renamed from: c */
        final /* synthetic */ byte[] f11855c;

        /* renamed from: d */
        final /* synthetic */ int f11856d;

        C5025a(MediaType uVar, int i, byte[] bArr, int i2) {
            this.f11853a = uVar;
            this.f11854b = i;
            this.f11855c = bArr;
            this.f11856d = i2;
        }

        /* renamed from: a */
        public long mo27837a() {
            return (long) this.f11854b;
        }

        /* renamed from: b */
        public MediaType mo27839b() {
            return this.f11853a;
        }

        /* renamed from: a */
        public void mo27838a(BufferedSink dVar) {
            dVar.write(this.f11855c, this.f11856d, this.f11854b);
        }
    }

    /* renamed from: a */
    public static RequestBody m18069a(MediaType uVar, byte[] bArr) {
        return m18070a(uVar, bArr, 0, bArr.length);
    }

    /* renamed from: a */
    public abstract long mo27837a();

    /* renamed from: a */
    public abstract void mo27838a(BufferedSink dVar);

    /* renamed from: b */
    public abstract MediaType mo27839b();

    /* renamed from: a */
    public static RequestBody m18070a(MediaType uVar, byte[] bArr, int i, int i2) {
        if (bArr != null) {
            C4951c.m17351a((long) bArr.length, (long) i, (long) i2);
            return new C5025a(uVar, i2, bArr, i);
        }
        throw new NullPointerException("content == null");
    }
}
