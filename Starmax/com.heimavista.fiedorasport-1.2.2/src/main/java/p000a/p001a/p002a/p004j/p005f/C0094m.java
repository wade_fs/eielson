package p000a.p001a.p002a.p004j.p005f;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import p000a.p001a.p002a.C0004e;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.m */
public final class C0094m extends C0008g<Time> {

    /* renamed from: b */
    public static final C0010h f150b = new C0095a();

    /* renamed from: a */
    private final DateFormat f151a = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a.a.a.j.f.m$a */
    static class C0095a implements C0010h {
        C0095a() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() == Time.class) {
                return new C0094m();
            }
            return null;
        }
    }

    /* renamed from: a */
    public synchronized Time mo17a(C0127a aVar) {
        if (aVar.mo107C() == C0129b.NULL) {
            aVar.mo105A();
            return null;
        }
        try {
            return new Time(this.f151a.parse(aVar.mo106B()).getTime());
        } catch (ParseException e) {
            throw new C0004e(e);
        }
    }

    /* renamed from: a */
    public synchronized void mo18a(C0130c cVar, Time time) {
        cVar.mo132c(time == null ? null : this.f151a.format(time));
    }
}
