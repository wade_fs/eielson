package p000a.p001a.p002a.p004j;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0124k;
import p000a.p001a.p002a.C0126m;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p003i.C0011a;
import p000a.p001a.p002a.p003i.C0014d;
import p000a.p001a.p002a.p003i.C0015e;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.j */
public final class C0120j implements C0010h, Cloneable {

    /* renamed from: V */
    public static final C0120j f179V = new C0120j();

    /* renamed from: P */
    private double f180P = -1.0d;

    /* renamed from: Q */
    private int f181Q = 136;

    /* renamed from: R */
    private boolean f182R = true;

    /* renamed from: S */
    private boolean f183S;

    /* renamed from: T */
    private List<C0124k> f184T = Collections.emptyList();

    /* renamed from: U */
    private List<C0124k> f185U = Collections.emptyList();

    /* renamed from: a.a.a.j.j$a */
    class C0121a extends C0008g<T> {

        /* renamed from: a */
        private C0008g<T> f186a;

        /* renamed from: b */
        final /* synthetic */ boolean f187b;

        /* renamed from: c */
        final /* synthetic */ boolean f188c;

        /* renamed from: d */
        final /* synthetic */ C0139q f189d;

        /* renamed from: e */
        final /* synthetic */ C0125a f190e;

        C0121a(boolean z, boolean z2, C0139q qVar, C0125a aVar) {
            this.f187b = z;
            this.f188c = z2;
            this.f189d = qVar;
            this.f190e = aVar;
        }

        /* renamed from: b */
        private C0008g<T> m325b() {
            C0008g<T> gVar = this.f186a;
            if (gVar != null) {
                return super;
            }
            C0008g<T> a = this.f189d.mo191a(C0120j.this, this.f190e);
            this.f186a = super;
            return super;
        }

        /* renamed from: a */
        public T mo17a(C0127a aVar) {
            if (!this.f187b) {
                return m325b().mo17a(aVar);
            }
            aVar.mo108D();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, T t) {
            if (this.f188c) {
                cVar.mo137t();
            } else {
                m325b().mo18a(cVar, t);
            }
        }
    }

    /* renamed from: a */
    private boolean m316a(C0014d dVar) {
        return dVar == null || dVar.value() <= this.f180P;
    }

    /* renamed from: a */
    private boolean m317a(C0014d dVar, C0015e eVar) {
        return m316a(dVar) && m318a(eVar);
    }

    /* renamed from: a */
    private boolean m318a(C0015e eVar) {
        return eVar == null || eVar.value() > this.f180P;
    }

    /* renamed from: a */
    private boolean m319a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    /* renamed from: b */
    private boolean m320b(Class<?> cls) {
        return cls.isMemberClass() && !m321c(cls);
    }

    /* renamed from: c */
    private boolean m321c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.j.a(java.lang.Class<?>, boolean):boolean
     arg types: [java.lang.Class<? super T>, int]
     candidates:
      a.a.a.j.j.a(a.a.a.i.d, a.a.a.i.e):boolean
      a.a.a.j.j.a(a.a.a.q, a.a.a.l.a):a.a.a.g<T>
      a.a.a.j.j.a(java.lang.reflect.Field, boolean):boolean
      a.a.a.h.a(a.a.a.q, a.a.a.l.a):a.a.a.g<T>
      a.a.a.j.j.a(java.lang.Class<?>, boolean):boolean */
    /* renamed from: a */
    public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
        Class<? super T> a = aVar.mo179a();
        boolean a2 = mo166a((Class<?>) a, true);
        boolean a3 = mo166a((Class<?>) a, false);
        if (a2 || a3) {
            return new C0121a(a3, a2, qVar, aVar);
        }
        return null;
    }

    /* renamed from: a */
    public boolean mo166a(Class<?> cls, boolean z) {
        if (this.f180P != -1.0d && !m317a((C0014d) cls.getAnnotation(C0014d.class), (C0015e) cls.getAnnotation(C0015e.class))) {
            return true;
        }
        if ((!this.f182R && m320b(cls)) || m319a(cls)) {
            return true;
        }
        for (C0124k kVar : z ? this.f184T : this.f185U) {
            if (kVar.mo178a(cls)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo167a(Field field, boolean z) {
        C0011a aVar;
        if ((this.f181Q & field.getModifiers()) != 0) {
            return true;
        }
        if ((this.f180P != -1.0d && !m317a((C0014d) field.getAnnotation(C0014d.class), (C0015e) field.getAnnotation(C0015e.class))) || field.isSynthetic()) {
            return true;
        }
        if (this.f183S && ((aVar = (C0011a) field.getAnnotation(C0011a.class)) == null || (!z ? !aVar.deserialize() : !aVar.serialize()))) {
            return true;
        }
        if ((!this.f182R && m320b(field.getType())) || m319a(field.getType())) {
            return true;
        }
        List<C0124k> list = z ? this.f184T : this.f185U;
        if (list.isEmpty()) {
            return false;
        }
        C0126m mVar = new C0126m(field);
        for (C0124k kVar : list) {
            if (kVar.mo177a(mVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public C0120j clone() {
        try {
            return (C0120j) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
