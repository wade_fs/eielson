package p000a.p001a.p002a;

import java.util.Map;
import java.util.Set;
import p000a.p001a.p002a.p004j.C0016a;

/* renamed from: a.a.a.y */
public final class C0154y extends C0151v {

    /* renamed from: a */
    private final C0016a<String, C0151v> f254a = new C0016a<>();

    /* renamed from: a */
    public void mo222a(String str, C0151v vVar) {
        if (vVar == null) {
            vVar = C0153x.f253a;
        }
        this.f254a.put(str, vVar);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof C0154y) && ((C0154y) obj).f254a.equals(this.f254a));
    }

    public int hashCode() {
        return this.f254a.hashCode();
    }

    /* renamed from: k */
    public Set<Map.Entry<String, C0151v>> mo225k() {
        return this.f254a.entrySet();
    }
}
