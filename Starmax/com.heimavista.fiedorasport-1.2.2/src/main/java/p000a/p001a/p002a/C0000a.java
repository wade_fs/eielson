package p000a.p001a.p002a;

/* renamed from: a.a.a.a */
public class C0000a extends RuntimeException {
    public C0000a(String str) {
        super(str);
    }

    public C0000a(String str, Throwable th) {
        super(str, th);
    }

    public C0000a(Throwable th) {
        super(th);
    }
}
