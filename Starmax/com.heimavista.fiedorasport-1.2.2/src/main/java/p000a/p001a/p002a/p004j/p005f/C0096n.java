package p000a.p001a.p002a.p004j.p005f;

import p000a.p001a.p002a.C0002c;
import p000a.p001a.p002a.C0003d;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.C0149t;
import p000a.p001a.p002a.C0150u;
import p000a.p001a.p002a.C0151v;
import p000a.p001a.p002a.p004j.C0026d;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.n */
public final class C0096n<T> extends C0008g<T> {

    /* renamed from: a */
    private final C0003d<T> f152a;

    /* renamed from: b */
    private final C0150u<T> f153b;

    /* renamed from: c */
    private final C0139q f154c;

    /* renamed from: d */
    private final C0125a<T> f155d;

    /* renamed from: e */
    private final C0010h f156e;

    /* renamed from: f */
    private final C0096n<T>.b f157f = new C0098b();

    /* renamed from: g */
    private C0008g<T> f158g;

    /* renamed from: a.a.a.j.f.n$b */
    private final class C0098b implements C0002c, C0149t {
        private C0098b(C0096n nVar) {
        }
    }

    public C0096n(C0003d<T> dVar, C0150u<T> uVar, C0139q qVar, C0125a<T> aVar, C0010h hVar) {
        this.f152a = dVar;
        this.f153b = uVar;
        this.f154c = qVar;
        this.f155d = aVar;
        this.f156e = hVar;
    }

    /* renamed from: b */
    private C0008g<T> m270b() {
        C0008g<T> gVar = this.f158g;
        if (gVar != null) {
            return super;
        }
        C0008g<T> a = this.f154c.mo191a(this.f156e, this.f155d);
        this.f158g = super;
        return super;
    }

    /* renamed from: a */
    public T mo17a(C0127a aVar) {
        if (this.f153b == null) {
            return m270b().mo17a(aVar);
        }
        C0151v a = C0026d.m39a(aVar);
        if (a.mo216h()) {
            return null;
        }
        return this.f153b.mo211a(a, this.f155d.mo180b(), this.f157f);
    }

    /* renamed from: a */
    public void mo18a(C0130c cVar, T t) {
        C0003d<T> dVar = this.f152a;
        if (dVar == null) {
            m270b().mo18a(cVar, t);
        } else if (t == null) {
            cVar.mo137t();
        } else {
            C0026d.m40a(dVar.mo14a(t, this.f155d.mo180b(), this.f157f), cVar);
        }
    }
}
