package p000a.p001a.p002a.p004j;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import p000a.p001a.p002a.C0147r;
import p000a.p001a.p002a.C0152w;
import p000a.p001a.p002a.p007l.C0125a;

/* renamed from: a.a.a.j.i */
public final class C0105i {

    /* renamed from: a */
    private final Map<Type, C0147r<?>> f169a;

    /* renamed from: a.a.a.j.i$a */
    class C0106a implements C0024b<T> {
        C0106a(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new ConcurrentHashMap();
        }
    }

    /* renamed from: a.a.a.j.i$b */
    class C0107b implements C0024b<T> {
        C0107b(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new TreeMap();
        }
    }

    /* renamed from: a.a.a.j.i$c */
    class C0108c implements C0024b<T> {
        C0108c(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new LinkedHashMap();
        }
    }

    /* renamed from: a.a.a.j.i$d */
    class C0109d implements C0024b<T> {
        C0109d(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new C0016a();
        }
    }

    /* renamed from: a.a.a.j.i$e */
    class C0110e implements C0024b<T> {

        /* renamed from: a */
        private final C0027e f170a = C0027e.m41a();

        /* renamed from: b */
        final /* synthetic */ Class f171b;

        /* renamed from: c */
        final /* synthetic */ Type f172c;

        C0110e(C0105i iVar, Class cls, Type type) {
            this.f171b = cls;
            this.f172c = type;
        }

        /* renamed from: a */
        public T mo66a() {
            try {
                return this.f170a.mo67a(this.f171b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.f172c + ". Register an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    /* renamed from: a.a.a.j.i$f */
    class C0111f implements C0024b<T> {

        /* renamed from: a */
        final /* synthetic */ C0147r f173a;

        /* renamed from: b */
        final /* synthetic */ Type f174b;

        C0111f(C0105i iVar, C0147r rVar, Type type) {
            this.f173a = rVar;
            this.f174b = type;
        }

        /* renamed from: a */
        public T mo66a() {
            return this.f173a.mo206a(this.f174b);
        }
    }

    /* renamed from: a.a.a.j.i$g */
    class C0112g implements C0024b<T> {

        /* renamed from: a */
        final /* synthetic */ C0147r f175a;

        /* renamed from: b */
        final /* synthetic */ Type f176b;

        C0112g(C0105i iVar, C0147r rVar, Type type) {
            this.f175a = rVar;
            this.f176b = type;
        }

        /* renamed from: a */
        public T mo66a() {
            return this.f175a.mo206a(this.f176b);
        }
    }

    /* renamed from: a.a.a.j.i$h */
    class C0113h implements C0024b<T> {

        /* renamed from: a */
        final /* synthetic */ Constructor f177a;

        C0113h(C0105i iVar, Constructor constructor) {
            this.f177a = constructor;
        }

        /* renamed from: a */
        public T mo66a() {
            try {
                return this.f177a.newInstance(null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.f177a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.f177a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    /* renamed from: a.a.a.j.i$i */
    class C0114i implements C0024b<T> {
        C0114i(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new TreeSet();
        }
    }

    /* renamed from: a.a.a.j.i$j */
    class C0115j implements C0024b<T> {

        /* renamed from: a */
        final /* synthetic */ Type f178a;

        C0115j(C0105i iVar, Type type) {
            this.f178a = type;
        }

        /* renamed from: a */
        public T mo66a() {
            Type type = this.f178a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return EnumSet.noneOf((Class) type2);
                }
                throw new C0152w("Invalid EnumSet type: " + this.f178a.toString());
            }
            throw new C0152w("Invalid EnumSet type: " + this.f178a.toString());
        }
    }

    /* renamed from: a.a.a.j.i$k */
    class C0116k implements C0024b<T> {
        C0116k(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new LinkedHashSet();
        }
    }

    /* renamed from: a.a.a.j.i$l */
    class C0117l implements C0024b<T> {
        C0117l(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new ArrayDeque();
        }
    }

    /* renamed from: a.a.a.j.i$m */
    class C0118m implements C0024b<T> {
        C0118m(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new ArrayList();
        }
    }

    /* renamed from: a.a.a.j.i$n */
    class C0119n implements C0024b<T> {
        C0119n(C0105i iVar) {
        }

        /* renamed from: a */
        public T mo66a() {
            return new ConcurrentSkipListMap();
        }
    }

    public C0105i(Map<Type, C0147r<?>> map) {
        this.f169a = map;
    }

    /* renamed from: a */
    private <T> C0024b<T> m298a(Class cls) {
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new C0113h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    /* renamed from: a */
    private <T> C0024b<T> m299a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new C0114i(this) : EnumSet.class.isAssignableFrom(cls) ? new C0115j(this, type) : Set.class.isAssignableFrom(cls) ? new C0116k(this) : Queue.class.isAssignableFrom(cls) ? new C0117l(this) : new C0118m(this);
        }
        if (!Map.class.isAssignableFrom(cls)) {
            return null;
        }
        if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
            return new C0119n(this);
        }
        if (ConcurrentMap.class.isAssignableFrom(cls)) {
            return new C0106a(this);
        }
        if (SortedMap.class.isAssignableFrom(cls)) {
            return new C0107b(this);
        }
        if (type instanceof ParameterizedType) {
            if (!String.class.isAssignableFrom(C0125a.m332a(((ParameterizedType) type).getActualTypeArguments()[0]).mo179a())) {
                return new C0108c(this);
            }
        }
        return new C0109d(this);
    }

    /* renamed from: b */
    private <T> C0024b<T> m300b(Type type, Class<? super T> cls) {
        return new C0110e(this, cls, type);
    }

    /* renamed from: a */
    public <T> C0024b<T> mo164a(C0125a aVar) {
        Type b = aVar.mo180b();
        Class a = aVar.mo179a();
        C0147r rVar = this.f169a.get(b);
        if (rVar != null) {
            return new C0111f(this, rVar, b);
        }
        C0147r rVar2 = this.f169a.get(a);
        if (rVar2 != null) {
            return new C0112g(this, rVar2, b);
        }
        C0024b<T> a2 = m298a(a);
        if (a2 != null) {
            return a2;
        }
        C0024b<T> a3 = m299a(b, a);
        return a3 != null ? a3 : m300b(b, a);
    }

    public String toString() {
        return this.f169a.toString();
    }
}
