package p000a.p001a.p002a;

/* renamed from: a.a.a.x */
public final class C0153x extends C0151v {

    /* renamed from: a */
    public static final C0153x f253a = new C0153x();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof C0153x);
    }

    public int hashCode() {
        return C0153x.class.hashCode();
    }
}
