package p000a.p001a.p002a.p003i;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.a.a.i.a */
public @interface C0011a {
    boolean deserialize() default true;

    boolean serialize() default true;
}
