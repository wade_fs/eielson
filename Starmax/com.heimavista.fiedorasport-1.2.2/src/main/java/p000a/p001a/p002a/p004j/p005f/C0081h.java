package p000a.p001a.p002a.p004j.p005f;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import p000a.p001a.p002a.C0001b;
import p000a.p001a.p002a.C0148s;
import p000a.p001a.p002a.C0151v;
import p000a.p001a.p002a.C0153x;
import p000a.p001a.p002a.C0154y;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.h */
public final class C0081h extends C0130c {

    /* renamed from: d0 */
    private static final Writer f119d0 = new C0082a();

    /* renamed from: e0 */
    private static final C0001b f120e0 = new C0001b("closed");

    /* renamed from: a0 */
    private final List<C0151v> f121a0 = new ArrayList();

    /* renamed from: b0 */
    private String f122b0;

    /* renamed from: c0 */
    private C0151v f123c0 = C0153x.f253a;

    /* renamed from: a.a.a.j.f.h$a */
    static class C0082a extends Writer {
        C0082a() {
        }

        public void close() {
            throw new AssertionError();
        }

        public void flush() {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    public C0081h() {
        super(f119d0);
    }

    /* renamed from: a */
    private void m222a(C0151v vVar) {
        if (this.f122b0 != null) {
            if (!vVar.mo216h() || mo188e()) {
                ((C0154y) m223v()).mo222a(this.f122b0, vVar);
            }
            this.f122b0 = null;
        } else if (this.f121a0.isEmpty()) {
            this.f123c0 = vVar;
        } else {
            C0151v v = m223v();
            if (v instanceof C0148s) {
                ((C0148s) v).mo207a(vVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: v */
    private C0151v m223v() {
        List<C0151v> list = this.f121a0;
        return list.get(list.size() - 1);
    }

    /* renamed from: a */
    public C0130c mo125a() {
        C0148s sVar = new C0148s();
        m222a(sVar);
        this.f121a0.add(sVar);
        return super;
    }

    /* renamed from: a */
    public C0130c mo126a(Boolean bool) {
        if (bool == null) {
            mo137t();
            return super;
        }
        m222a(new C0001b(bool));
        return super;
    }

    /* renamed from: a */
    public C0130c mo127a(Number number) {
        if (number == null) {
            mo137t();
            return super;
        }
        if (!mo189g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        m222a(new C0001b(number));
        return super;
    }

    /* renamed from: b */
    public C0130c mo128b() {
        C0154y yVar = new C0154y();
        m222a(yVar);
        this.f121a0.add(yVar);
        return super;
    }

    /* renamed from: b */
    public C0130c mo129b(String str) {
        if (this.f121a0.isEmpty() || this.f122b0 != null) {
            throw new IllegalStateException();
        } else if (m223v() instanceof C0154y) {
            this.f122b0 = str;
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: b */
    public C0130c mo130b(boolean z) {
        m222a(new C0001b(Boolean.valueOf(z)));
        return super;
    }

    /* renamed from: c */
    public C0130c mo131c() {
        if (this.f121a0.isEmpty() || this.f122b0 != null) {
            throw new IllegalStateException();
        } else if (m223v() instanceof C0148s) {
            List<C0151v> list = this.f121a0;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: c */
    public C0130c mo132c(String str) {
        if (str == null) {
            mo137t();
            return super;
        }
        m222a(new C0001b(str));
        return super;
    }

    public void close() {
        if (this.f121a0.isEmpty()) {
            this.f121a0.add(f120e0);
            return;
        }
        throw new IOException("Incomplete document");
    }

    /* renamed from: d */
    public C0130c mo134d() {
        if (this.f121a0.isEmpty() || this.f122b0 != null) {
            throw new IllegalStateException();
        } else if (m223v() instanceof C0154y) {
            List<C0151v> list = this.f121a0;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public void flush() {
    }

    /* renamed from: g */
    public C0130c mo136g(long j) {
        m222a(new C0001b(Long.valueOf(j)));
        return super;
    }

    /* renamed from: t */
    public C0130c mo137t() {
        m222a(C0153x.f253a);
        return super;
    }

    /* renamed from: u */
    public C0151v mo138u() {
        if (this.f121a0.isEmpty()) {
            return this.f123c0;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.f121a0);
    }
}
