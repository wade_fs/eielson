package p000a.p001a.p002a.p004j.p005f;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p004j.C0101h;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.c */
public final class C0072c<E> extends C0008g<Object> {

    /* renamed from: c */
    public static final C0010h f104c = new C0073a();

    /* renamed from: a */
    private final Class<E> f105a;

    /* renamed from: b */
    private final C0008g<E> f106b;

    /* renamed from: a.a.a.j.f.c$a */
    static class C0073a implements C0010h {
        C0073a() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            Type b = aVar.mo180b();
            if (!(b instanceof GenericArrayType) && (!(b instanceof Class) || !((Class) b).isArray())) {
                return null;
            }
            Type d = C0101h.m293d(b);
            return new C0072c(qVar, qVar.mo192a(C0125a.m332a(d)), C0101h.m294e(d));
        }
    }

    public C0072c(C0139q qVar, C0008g<E> gVar, Class<E> cls) {
        this.f106b = new C0099o(qVar, super, cls);
        this.f105a = cls;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<E>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    /* renamed from: a */
    public Object mo17a(C0127a aVar) {
        if (aVar.mo107C() == C0129b.NULL) {
            aVar.mo105A();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.mo110a();
        while (aVar.mo116t()) {
            arrayList.add(this.f106b.mo17a(aVar));
        }
        aVar.mo113d();
        Object newInstance = Array.newInstance((Class<?>) this.f105a, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }

    /* renamed from: a */
    public void mo18a(C0130c cVar, Object obj) {
        if (obj == null) {
            cVar.mo137t();
            return;
        }
        cVar.mo125a();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.f106b.mo18a(cVar, Array.get(obj, i));
        }
        cVar.mo131c();
    }
}
