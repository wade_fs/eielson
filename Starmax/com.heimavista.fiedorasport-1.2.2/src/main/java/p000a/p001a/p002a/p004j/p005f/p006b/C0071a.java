package p000a.p001a.p002a.p004j.p005f.p006b;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: a.a.a.j.f.b.a */
public class C0071a {

    /* renamed from: a */
    private static final TimeZone f103a = TimeZone.getTimeZone("UTC");

    /* renamed from: a */
    private static int m181a(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return i;
            }
            i++;
        }
        return str.length();
    }

    /* renamed from: a */
    private static int m182a(String str, int i, int i2) {
        int i3;
        int i4;
        if (i < 0 || i2 > str.length() || i > i2) {
            throw new NumberFormatException(str);
        }
        if (i < i2) {
            i4 = i + 1;
            int digit = Character.digit(str.charAt(i), 10);
            if (digit >= 0) {
                i3 = -digit;
            } else {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
        } else {
            i4 = i;
            i3 = 0;
        }
        while (i4 < i2) {
            int i5 = i4 + 1;
            int digit2 = Character.digit(str.charAt(i4), 10);
            if (digit2 >= 0) {
                i3 = (i3 * 10) - digit2;
                i4 = i5;
            } else {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
        }
        return -i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.f.b.a.a(java.lang.String, int, char):boolean
     arg types: [java.lang.String, int, int]
     candidates:
      a.a.a.j.f.b.a.a(java.lang.String, int, int):int
      a.a.a.j.f.b.a.a(java.lang.String, int, char):boolean */
    /* renamed from: a */
    public static Date m183a(String str, ParsePosition parsePosition) {
        String str2;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        TimeZone timeZone;
        int i7;
        char charAt;
        String str3 = str;
        ParsePosition parsePosition2 = parsePosition;
        try {
            int index = parsePosition.getIndex();
            int i8 = index + 4;
            int a = m182a(str3, index, i8);
            if (m184a(str3, i8, '-')) {
                i8++;
            }
            int i9 = i8 + 2;
            int a2 = m182a(str3, i8, i9);
            if (m184a(str3, i9, '-')) {
                i9++;
            }
            int i10 = i9 + 2;
            int a3 = m182a(str3, i9, i10);
            boolean a4 = m184a(str3, i10, 'T');
            if (a4 || str.length() > i10) {
                if (a4) {
                    int i11 = i10 + 1;
                    int i12 = i11 + 2;
                    i4 = m182a(str3, i11, i12);
                    if (m184a(str3, i12, ':')) {
                        i12++;
                    }
                    int i13 = i12 + 2;
                    i3 = m182a(str3, i12, i13);
                    if (m184a(str3, i13, ':')) {
                        i13++;
                    }
                    if (str.length() <= i13 || (charAt = str3.charAt(i13)) == 'Z' || charAt == '+' || charAt == '-') {
                        i7 = i13;
                        i2 = 0;
                    } else {
                        i7 = i13 + 2;
                        int a5 = m182a(str3, i13, i7);
                        i2 = (a5 <= 59 || a5 >= 63) ? a5 : 59;
                        if (m184a(str3, i7, (char) JwtParser.SEPARATOR_CHAR)) {
                            int i14 = i7 + 1;
                            i = m181a(str3, i14 + 1);
                            int min = Math.min(i, i14 + 3);
                            int a6 = m182a(str3, i14, min);
                            int i15 = min - i14;
                            i5 = i15 != 1 ? i15 != 2 ? a6 : a6 * 10 : a6 * 100;
                        }
                    }
                    i = i7;
                    i5 = 0;
                } else {
                    i = i10;
                    i5 = 0;
                    i4 = 0;
                    i3 = 0;
                    i2 = 0;
                }
                if (str.length() > i) {
                    char charAt2 = str3.charAt(i);
                    if (charAt2 == 'Z') {
                        timeZone = f103a;
                        i6 = i + 1;
                    } else {
                        if (charAt2 != '+') {
                            if (charAt2 != '-') {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Invalid time zone indicator '");
                                sb.append(charAt2);
                                sb.append("'");
                                throw new IndexOutOfBoundsException(sb.toString());
                            }
                        }
                        String substring = str3.substring(i);
                        if (substring.length() < 5) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(substring);
                            sb2.append("00");
                            substring = sb2.toString();
                        }
                        i6 = i + substring.length();
                        if (!"+0000".equals(substring)) {
                            if (!"+00:00".equals(substring)) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("GMT");
                                sb3.append(substring);
                                String sb4 = sb3.toString();
                                TimeZone timeZone2 = TimeZone.getTimeZone(sb4);
                                String id = timeZone2.getID();
                                if (!id.equals(sb4)) {
                                    if (!id.replace(":", "").equals(sb4)) {
                                        StringBuilder sb5 = new StringBuilder();
                                        sb5.append("Mismatching time zone indicator: ");
                                        sb5.append(sb4);
                                        sb5.append(" given, resolves to ");
                                        sb5.append(timeZone2.getID());
                                        throw new IndexOutOfBoundsException(sb5.toString());
                                    }
                                }
                                timeZone = timeZone2;
                            }
                        }
                        timeZone = f103a;
                    }
                    GregorianCalendar gregorianCalendar = new GregorianCalendar(timeZone);
                    gregorianCalendar.setLenient(false);
                    gregorianCalendar.set(1, a);
                    gregorianCalendar.set(2, a2 - 1);
                    gregorianCalendar.set(5, a3);
                    gregorianCalendar.set(11, i4);
                    gregorianCalendar.set(12, i3);
                    gregorianCalendar.set(13, i2);
                    gregorianCalendar.set(14, i5);
                    parsePosition2.setIndex(i6);
                    return gregorianCalendar.getTime();
                }
                throw new IllegalArgumentException("No time zone indicator");
            }
            GregorianCalendar gregorianCalendar2 = new GregorianCalendar(a, a2 - 1, a3);
            parsePosition2.setIndex(i10);
            return gregorianCalendar2.getTime();
        } catch (IllegalArgumentException | IndexOutOfBoundsException | NumberFormatException e) {
            if (str3 == null) {
                str2 = null;
            } else {
                str2 = '\"' + str3 + "'";
            }
            String message = e.getMessage();
            if (message == null || message.isEmpty()) {
                message = "(" + e.getClass().getName() + ")";
            }
            ParseException parseException = new ParseException("Failed to parse date [" + str2 + "]: " + message, parsePosition.getIndex());
            parseException.initCause(e);
            throw parseException;
        }
    }

    /* renamed from: a */
    private static boolean m184a(String str, int i, char c) {
        return i < str.length() && str.charAt(i) == c;
    }
}
