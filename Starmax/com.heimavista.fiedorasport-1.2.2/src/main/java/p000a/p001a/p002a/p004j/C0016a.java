package p000a.p001a.p002a.p004j;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: a.a.a.j.a */
public final class C0016a<K, V> extends AbstractMap<K, V> implements Serializable {

    /* renamed from: W */
    private static final Comparator<Comparable> f6W = new C0023e();

    /* renamed from: X */
    static final /* synthetic */ boolean f7X = (!C0016a.class.desiredAssertionStatus());

    /* renamed from: P */
    Comparator<? super K> f8P;

    /* renamed from: Q */
    C0022d<K, V> f9Q;

    /* renamed from: R */
    int f10R;

    /* renamed from: S */
    int f11S;

    /* renamed from: T */
    final C0022d<K, V> f12T;

    /* renamed from: U */
    private C0016a<K, V>.a f13U;

    /* renamed from: V */
    private C0016a<K, V>.b f14V;

    /* renamed from: a.a.a.j.a$a */
    class C0017a extends AbstractSet<Map.Entry<K, V>> {

        /* renamed from: a.a.a.j.a$a$a */
        class C0018a extends C0016a<K, V>.c<Map.Entry<K, V>> {
            C0018a(C0017a aVar) {
                super();
            }

            public Map.Entry<K, V> next() {
                return mo53b();
            }
        }

        C0017a() {
        }

        public void clear() {
            C0016a.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && C0016a.this.mo30a((Map.Entry) obj) != null;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new C0018a(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
         arg types: [a.a.a.j.a$d, int]
         candidates:
          a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
          a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
          a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
          a.a.a.j.a.a(a.a.a.j.a$d, boolean):void */
        public boolean remove(Object obj) {
            C0022d a;
            if (!(obj instanceof Map.Entry) || (a = C0016a.this.mo30a((Map.Entry<?, ?>) ((Map.Entry) obj))) == null) {
                return false;
            }
            C0016a.this.mo31a(a, true);
            return true;
        }

        public int size() {
            return C0016a.this.f10R;
        }
    }

    /* renamed from: a.a.a.j.a$b */
    final class C0019b extends AbstractSet<K> {

        /* renamed from: a.a.a.j.a$b$a */
        class C0020a extends C0016a<K, V>.c<K> {
            C0020a(C0019b bVar) {
                super();
            }

            public K next() {
                return mo53b().f26U;
            }
        }

        C0019b() {
        }

        public void clear() {
            C0016a.this.clear();
        }

        public boolean contains(Object obj) {
            return C0016a.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new C0020a(this);
        }

        public boolean remove(Object obj) {
            return C0016a.this.mo32b(obj) != null;
        }

        public int size() {
            return C0016a.this.f10R;
        }
    }

    /* renamed from: a.a.a.j.a$c */
    private abstract class C0021c<T> implements Iterator<T> {

        /* renamed from: P */
        C0022d<K, V> f17P;

        /* renamed from: Q */
        C0022d<K, V> f18Q = null;

        /* renamed from: R */
        int f19R;

        C0021c() {
            C0016a aVar = C0016a.this;
            this.f17P = aVar.f12T.f24S;
            this.f19R = aVar.f11S;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public final C0022d<K, V> mo53b() {
            C0022d<K, V> dVar = this.f17P;
            C0016a aVar = C0016a.this;
            if (dVar == aVar.f12T) {
                throw new NoSuchElementException();
            } else if (aVar.f11S == this.f19R) {
                this.f17P = dVar.f24S;
                this.f18Q = dVar;
                return dVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        public final boolean hasNext() {
            return this.f17P != C0016a.this.f12T;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
         arg types: [a.a.a.j.a$d<K, V>, int]
         candidates:
          a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
          a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
          a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
          a.a.a.j.a.a(a.a.a.j.a$d, boolean):void */
        public final void remove() {
            C0022d<K, V> dVar = this.f18Q;
            if (dVar != null) {
                C0016a.this.mo31a((C0022d) dVar, true);
                this.f18Q = null;
                this.f19R = C0016a.this.f11S;
                return;
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: a.a.a.j.a$d */
    static final class C0022d<K, V> implements Map.Entry<K, V> {

        /* renamed from: P */
        C0022d<K, V> f21P;

        /* renamed from: Q */
        C0022d<K, V> f22Q;

        /* renamed from: R */
        C0022d<K, V> f23R;

        /* renamed from: S */
        C0022d<K, V> f24S;

        /* renamed from: T */
        C0022d<K, V> f25T;

        /* renamed from: U */
        final K f26U;

        /* renamed from: V */
        V f27V;

        /* renamed from: W */
        int f28W;

        C0022d() {
            this.f26U = null;
            this.f25T = this;
            this.f24S = this;
        }

        C0022d(C0022d<K, V> dVar, K k, C0022d<K, V> dVar2, C0022d<K, V> dVar3) {
            this.f21P = dVar;
            this.f26U = k;
            this.f28W = 1;
            this.f24S = dVar2;
            this.f25T = dVar3;
            dVar3.f24S = this;
            dVar2.f25T = this;
        }

        /* renamed from: a */
        public C0022d<K, V> mo56a() {
            C0022d<K, V> dVar = this;
            for (C0022d<K, V> dVar2 = this.f22Q; dVar2 != null; dVar2 = dVar2.f22Q) {
                dVar = dVar2;
            }
            return dVar;
        }

        /* renamed from: b */
        public C0022d<K, V> mo57b() {
            C0022d<K, V> dVar = this;
            for (C0022d<K, V> dVar2 = this.f23R; dVar2 != null; dVar2 = dVar2.f23R) {
                dVar = dVar2;
            }
            return dVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x002d A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r0 = r4 instanceof java.util.Map.Entry
                r1 = 0
                if (r0 == 0) goto L_0x002e
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r0 = r3.f26U
                if (r0 != 0) goto L_0x0012
                java.lang.Object r0 = r4.getKey()
                if (r0 != 0) goto L_0x002e
                goto L_0x001c
            L_0x0012:
                java.lang.Object r2 = r4.getKey()
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x002e
            L_0x001c:
                V r0 = r3.f27V
                java.lang.Object r4 = r4.getValue()
                if (r0 != 0) goto L_0x0027
                if (r4 != 0) goto L_0x002e
                goto L_0x002d
            L_0x0027:
                boolean r4 = r0.equals(r4)
                if (r4 == 0) goto L_0x002e
            L_0x002d:
                r1 = 1
            L_0x002e:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.C0016a.C0022d.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.f26U;
        }

        public V getValue() {
            return this.f27V;
        }

        public int hashCode() {
            K k = this.f26U;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.f27V;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.f27V;
            this.f27V = v;
            return v2;
        }

        public String toString() {
            return ((Object) this.f26U) + "=" + ((Object) this.f27V);
        }
    }

    /* renamed from: a.a.a.j.a$e */
    static class C0023e implements Comparator<Comparable> {
        C0023e() {
        }

        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    }

    public C0016a() {
        this(f6W);
    }

    public C0016a(Comparator<? super K> comparator) {
        this.f10R = 0;
        this.f11S = 0;
        this.f12T = new C0022d<>();
        this.f8P = comparator == null ? f6W : comparator;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
     arg types: [a.a.a.j.a$d<K, V>, a.a.a.j.a$d<K, V>]
     candidates:
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void */
    /* renamed from: a */
    private void m21a(C0022d<K, V> dVar) {
        C0022d<K, V> dVar2 = dVar.f22Q;
        C0022d<K, V> dVar3 = dVar.f23R;
        C0022d<K, V> dVar4 = dVar3.f22Q;
        C0022d<K, V> dVar5 = dVar3.f23R;
        dVar.f23R = dVar4;
        if (dVar4 != null) {
            dVar4.f21P = dVar;
        }
        m22a((C0022d) dVar, (C0022d) dVar3);
        dVar3.f22Q = dVar;
        dVar.f21P = dVar3;
        int i = 0;
        dVar.f28W = Math.max(dVar2 != null ? dVar2.f28W : 0, dVar4 != null ? dVar4.f28W : 0) + 1;
        int i2 = dVar.f28W;
        if (dVar5 != null) {
            i = dVar5.f28W;
        }
        dVar3.f28W = Math.max(i2, i) + 1;
    }

    /* renamed from: a */
    private void m22a(C0022d<K, V> dVar, C0022d<K, V> dVar2) {
        C0022d<K, V> dVar3 = dVar.f21P;
        dVar.f21P = null;
        if (dVar2 != null) {
            dVar2.f21P = dVar3;
        }
        if (dVar3 == null) {
            this.f9Q = dVar2;
        } else if (dVar3.f22Q == dVar) {
            dVar3.f22Q = dVar2;
        } else if (f7X || dVar3.f23R == dVar) {
            dVar3.f23R = dVar2;
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    private boolean m23a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
     arg types: [a.a.a.j.a$d<K, V>, a.a.a.j.a$d<K, V>]
     candidates:
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void */
    /* renamed from: b */
    private void m24b(C0022d<K, V> dVar) {
        C0022d<K, V> dVar2 = dVar.f22Q;
        C0022d<K, V> dVar3 = dVar.f23R;
        C0022d<K, V> dVar4 = dVar2.f22Q;
        C0022d<K, V> dVar5 = dVar2.f23R;
        dVar.f22Q = dVar5;
        if (dVar5 != null) {
            dVar5.f21P = dVar;
        }
        m22a((C0022d) dVar, (C0022d) dVar2);
        dVar2.f23R = dVar;
        dVar.f21P = dVar2;
        int i = 0;
        dVar.f28W = Math.max(dVar3 != null ? dVar3.f28W : 0, dVar5 != null ? dVar5.f28W : 0) + 1;
        int i2 = dVar.f28W;
        if (dVar4 != null) {
            i = dVar4.f28W;
        }
        dVar2.f28W = Math.max(i2, i) + 1;
    }

    /* renamed from: b */
    private void m25b(C0022d<K, V> dVar, boolean z) {
        while (dVar != null) {
            C0022d<K, V> dVar2 = dVar.f22Q;
            C0022d<K, V> dVar3 = dVar.f23R;
            int i = 0;
            int i2 = dVar2 != null ? dVar2.f28W : 0;
            int i3 = dVar3 != null ? dVar3.f28W : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                C0022d<K, V> dVar4 = dVar3.f22Q;
                C0022d<K, V> dVar5 = dVar3.f23R;
                int i5 = dVar5 != null ? dVar5.f28W : 0;
                if (dVar4 != null) {
                    i = dVar4.f28W;
                }
                int i6 = i - i5;
                if (i6 != -1 && (i6 != 0 || z)) {
                    if (f7X || i6 == 1) {
                        m24b((C0022d) dVar3);
                    } else {
                        throw new AssertionError();
                    }
                }
                m21a((C0022d) dVar);
                if (z) {
                    return;
                }
            } else if (i4 == 2) {
                C0022d<K, V> dVar6 = dVar2.f22Q;
                C0022d<K, V> dVar7 = dVar2.f23R;
                int i7 = dVar7 != null ? dVar7.f28W : 0;
                if (dVar6 != null) {
                    i = dVar6.f28W;
                }
                int i8 = i - i7;
                if (i8 != 1 && (i8 != 0 || z)) {
                    if (f7X || i8 == -1) {
                        m21a((C0022d) dVar2);
                    } else {
                        throw new AssertionError();
                    }
                }
                m24b((C0022d) dVar);
                if (z) {
                    return;
                }
            } else if (i4 == 0) {
                dVar.f28W = i2 + 1;
                if (z) {
                    return;
                }
            } else if (f7X || i4 == -1 || i4 == 1) {
                dVar.f28W = Math.max(i2, i3) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            dVar = dVar.f21P;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V> */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C0022d<K, V> mo28a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return mo29a(obj, false);
        } catch (ClassCastException unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C0022d<K, V> mo29a(K k, boolean z) {
        int i;
        C0022d<K, V> dVar;
        Comparator<? super K> comparator = this.f8P;
        C0022d<K, V> dVar2 = this.f9Q;
        if (dVar2 != null) {
            Comparable comparable = comparator == f6W ? (Comparable) k : null;
            while (true) {
                K k2 = dVar2.f26U;
                i = comparable != null ? comparable.compareTo(k2) : comparator.compare(k, k2);
                if (i == 0) {
                    return dVar2;
                }
                C0022d<K, V> dVar3 = i < 0 ? dVar2.f22Q : dVar2.f23R;
                if (dVar3 == null) {
                    break;
                }
                dVar2 = dVar3;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        C0022d<K, V> dVar4 = this.f12T;
        if (dVar2 != null) {
            dVar = new C0022d<>(dVar2, k, dVar4, dVar4.f25T);
            if (i < 0) {
                dVar2.f22Q = dVar;
            } else {
                dVar2.f23R = dVar;
            }
            m25b(dVar2, true);
        } else if (comparator != f6W || (k instanceof Comparable)) {
            dVar = new C0022d<>(dVar2, k, dVar4, dVar4.f25T);
            this.f9Q = dVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.f10R++;
        this.f11S++;
        return dVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C0022d<K, V> mo30a(Map.Entry<?, ?> entry) {
        C0022d<K, V> a = mo28a(entry.getKey());
        if (a != null && m23a(a.f27V, entry.getValue())) {
            return a;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
     arg types: [a.a.a.j.a$d<K, V>, int]
     candidates:
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
     arg types: [a.a.a.j.a$d<K, V>, a.a.a.j.a$d<K, V>]
     candidates:
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
     arg types: [a.a.a.j.a$d<K, V>, ?[OBJECT, ARRAY]]
     candidates:
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo31a(C0022d<K, V> dVar, boolean z) {
        int i;
        if (z) {
            C0022d<K, V> dVar2 = dVar.f25T;
            dVar2.f24S = dVar.f24S;
            dVar.f24S.f25T = dVar2;
        }
        C0022d<K, V> dVar3 = dVar.f22Q;
        C0022d<K, V> dVar4 = dVar.f23R;
        C0022d<K, V> dVar5 = dVar.f21P;
        int i2 = 0;
        if (dVar3 == null || dVar4 == null) {
            if (dVar3 != null) {
                m22a((C0022d) dVar, (C0022d) dVar3);
                dVar.f22Q = null;
            } else if (dVar4 != null) {
                m22a((C0022d) dVar, (C0022d) dVar4);
                dVar.f23R = null;
            } else {
                m22a((C0022d) dVar, (C0022d) null);
            }
            m25b(dVar5, false);
            this.f10R--;
            this.f11S++;
            return;
        }
        C0022d<K, V> b = dVar3.f28W > dVar4.f28W ? dVar3.mo57b() : dVar4.mo56a();
        mo31a((C0022d) b, false);
        C0022d<K, V> dVar6 = dVar.f22Q;
        if (dVar6 != null) {
            i = dVar6.f28W;
            b.f22Q = dVar6;
            dVar6.f21P = b;
            dVar.f22Q = null;
        } else {
            i = 0;
        }
        C0022d<K, V> dVar7 = dVar.f23R;
        if (dVar7 != null) {
            i2 = dVar7.f28W;
            b.f23R = dVar7;
            dVar7.f21P = b;
            dVar.f23R = null;
        }
        b.f28W = Math.max(i, i2) + 1;
        m22a((C0022d) dVar, (C0022d) b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
     arg types: [a.a.a.j.a$d<K, V>, int]
     candidates:
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C0022d<K, V> mo32b(Object obj) {
        C0022d<K, V> a = mo28a(obj);
        if (a != null) {
            mo31a((C0022d) a, true);
        }
        return a;
    }

    public void clear() {
        this.f9Q = null;
        this.f10R = 0;
        this.f11S++;
        C0022d<K, V> dVar = this.f12T;
        dVar.f25T = dVar;
        dVar.f24S = dVar;
    }

    public boolean containsKey(Object obj) {
        return mo28a(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        C0016a<K, V>.a aVar = this.f13U;
        if (aVar != null) {
            return aVar;
        }
        C0016a<K, V>.a aVar2 = new C0017a();
        this.f13U = aVar2;
        return aVar2;
    }

    public V get(Object obj) {
        C0022d a = mo28a(obj);
        if (a != null) {
            return a.f27V;
        }
        return null;
    }

    public Set<K> keySet() {
        C0016a<K, V>.b bVar = this.f14V;
        if (bVar != null) {
            return bVar;
        }
        C0016a<K, V>.b bVar2 = new C0019b();
        this.f14V = bVar2;
        return bVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V>
     arg types: [K, int]
     candidates:
      a.a.a.j.a.a(a.a.a.j.a$d, a.a.a.j.a$d):void
      a.a.a.j.a.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.a.a(a.a.a.j.a$d, boolean):void
      a.a.a.j.a.a(java.lang.Object, boolean):a.a.a.j.a$d<K, V> */
    public V put(K k, V v) {
        if (k != null) {
            C0022d a = mo29a((Object) k, true);
            V v2 = a.f27V;
            a.f27V = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    public V remove(Object obj) {
        C0022d b = mo32b(obj);
        if (b != null) {
            return b.f27V;
        }
        return null;
    }

    public int size() {
        return this.f10R;
    }
}
