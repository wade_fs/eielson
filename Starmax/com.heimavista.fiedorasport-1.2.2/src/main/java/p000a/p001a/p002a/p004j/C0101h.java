package p000a.p001a.p002a.p004j;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/* renamed from: a.a.a.j.h */
public final class C0101h {

    /* renamed from: a */
    static final Type[] f162a = new Type[0];

    /* renamed from: a.a.a.j.h$a */
    private static final class C0102a implements GenericArrayType, Serializable {

        /* renamed from: P */
        private final Type f163P;

        public C0102a(Type type) {
            this.f163P = C0101h.m289b(type);
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && C0101h.m288a(this, (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.f163P;
        }

        public int hashCode() {
            return this.f163P.hashCode();
        }

        public String toString() {
            return C0101h.m297h(this.f163P) + "[]";
        }
    }

    /* renamed from: a.a.a.j.h$b */
    private static final class C0103b implements ParameterizedType, Serializable {

        /* renamed from: P */
        private final Type f164P;

        /* renamed from: Q */
        private final Type f165Q;

        /* renamed from: R */
        private final Type[] f166R;

        public C0103b(Type type, Type type2, Type... typeArr) {
            int i = 0;
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                boolean z = true;
                boolean z2 = Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null;
                if (type == null && !z2) {
                    z = false;
                }
                C0100g.m277a(z);
            }
            this.f164P = type == null ? null : C0101h.m289b(type);
            this.f165Q = C0101h.m289b(type2);
            this.f166R = (Type[]) typeArr.clone();
            while (true) {
                Type[] typeArr2 = this.f166R;
                if (i < typeArr2.length) {
                    C0100g.m276a(typeArr2[i]);
                    C0101h.m292c(this.f166R[i]);
                    Type[] typeArr3 = this.f166R;
                    typeArr3[i] = C0101h.m289b(typeArr3[i]);
                    i++;
                } else {
                    return;
                }
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && C0101h.m288a(this, (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.f166R.clone();
        }

        public Type getOwnerType() {
            return this.f164P;
        }

        public Type getRawType() {
            return this.f165Q;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.f166R) ^ this.f165Q.hashCode()) ^ C0101h.m278a((Object) this.f164P);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder((this.f166R.length + 1) * 30);
            sb.append(C0101h.m297h(this.f165Q));
            if (this.f166R.length == 0) {
                return sb.toString();
            }
            sb.append("<");
            sb.append(C0101h.m297h(this.f166R[0]));
            for (int i = 1; i < this.f166R.length; i++) {
                sb.append(", ");
                sb.append(C0101h.m297h(this.f166R[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }

    /* renamed from: a.a.a.j.h$c */
    private static final class C0104c implements WildcardType, Serializable {

        /* renamed from: P */
        private final Type f167P;

        /* renamed from: Q */
        private final Type f168Q;

        public C0104c(Type[] typeArr, Type[] typeArr2) {
            Class<Object> cls = Object.class;
            boolean z = true;
            C0100g.m277a(typeArr2.length <= 1);
            C0100g.m277a(typeArr.length == 1);
            if (typeArr2.length == 1) {
                C0100g.m276a(typeArr2[0]);
                C0101h.m292c(typeArr2[0]);
                C0100g.m277a(typeArr[0] != cls ? false : z);
                this.f168Q = C0101h.m289b(typeArr2[0]);
                this.f167P = cls;
                return;
            }
            C0100g.m276a(typeArr[0]);
            C0101h.m292c(typeArr[0]);
            this.f168Q = null;
            this.f167P = C0101h.m289b(typeArr[0]);
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && C0101h.m288a(this, (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            Type type = this.f168Q;
            if (type == null) {
                return C0101h.f162a;
            }
            return new Type[]{type};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.f167P};
        }

        public int hashCode() {
            Type type = this.f168Q;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.f167P.hashCode() + 31);
        }

        public String toString() {
            StringBuilder sb;
            Type type;
            if (this.f168Q != null) {
                sb = new StringBuilder();
                sb.append("? super ");
                type = this.f168Q;
            } else if (this.f167P == Object.class) {
                return "?";
            } else {
                sb = new StringBuilder();
                sb.append("? extends ");
                type = this.f167P;
            }
            sb.append(C0101h.m297h(type));
            return sb.toString();
        }
    }

    /* renamed from: a */
    static int m278a(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    /* renamed from: a */
    private static int m279a(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /* renamed from: a */
    private static Class<?> m280a(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    /* renamed from: a */
    public static GenericArrayType m281a(Type type) {
        return new C0102a(type);
    }

    /* renamed from: a */
    public static ParameterizedType m282a(Type type, Type type2, Type... typeArr) {
        return new C0103b(type, type2, typeArr);
    }

    /* renamed from: a */
    public static Type m283a(Type type, Class<?> cls) {
        Type b = m290b(type, cls, Collection.class);
        if (b instanceof WildcardType) {
            b = ((WildcardType) b).getUpperBounds()[0];
        }
        return b instanceof ParameterizedType ? ((ParameterizedType) b).getActualTypeArguments()[0] : Object.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super ?>, java.lang.Class<?>]
     candidates:
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.reflect.Type, java.lang.reflect.Type[]):java.lang.reflect.ParameterizedType
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.Type):java.lang.reflect.Type
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.TypeVariable<?>):java.lang.reflect.Type
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type */
    /* renamed from: a */
    static Type m284a(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return m284a(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return m284a(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:56:? */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.reflect.Type[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v13, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v14, resolved type: java.lang.reflect.WildcardType} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.reflect.Type m285a(java.lang.reflect.Type r8, java.lang.Class<?> r9, java.lang.reflect.Type r10) {
        /*
        L_0x0000:
            boolean r0 = r10 instanceof java.lang.reflect.TypeVariable
            if (r0 == 0) goto L_0x000f
            java.lang.reflect.TypeVariable r10 = (java.lang.reflect.TypeVariable) r10
            java.lang.reflect.Type r0 = m286a(r8, r9, r10)
            if (r0 != r10) goto L_0x000d
            return r0
        L_0x000d:
            r10 = r0
            goto L_0x0000
        L_0x000f:
            boolean r0 = r10 instanceof java.lang.Class
            if (r0 == 0) goto L_0x002c
            r0 = r10
            java.lang.Class r0 = (java.lang.Class) r0
            boolean r1 = r0.isArray()
            if (r1 == 0) goto L_0x002c
            java.lang.Class r10 = r0.getComponentType()
            java.lang.reflect.Type r8 = m285a(r8, r9, r10)
            if (r10 != r8) goto L_0x0027
            goto L_0x002b
        L_0x0027:
            java.lang.reflect.GenericArrayType r0 = m281a(r8)
        L_0x002b:
            return r0
        L_0x002c:
            boolean r0 = r10 instanceof java.lang.reflect.GenericArrayType
            if (r0 == 0) goto L_0x0042
            java.lang.reflect.GenericArrayType r10 = (java.lang.reflect.GenericArrayType) r10
            java.lang.reflect.Type r0 = r10.getGenericComponentType()
            java.lang.reflect.Type r8 = m285a(r8, r9, r0)
            if (r0 != r8) goto L_0x003d
            goto L_0x0041
        L_0x003d:
            java.lang.reflect.GenericArrayType r10 = m281a(r8)
        L_0x0041:
            return r10
        L_0x0042:
            boolean r0 = r10 instanceof java.lang.reflect.ParameterizedType
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0082
            java.lang.reflect.ParameterizedType r10 = (java.lang.reflect.ParameterizedType) r10
            java.lang.reflect.Type r0 = r10.getOwnerType()
            java.lang.reflect.Type r3 = m285a(r8, r9, r0)
            if (r3 == r0) goto L_0x0056
            r0 = 1
            goto L_0x0057
        L_0x0056:
            r0 = 0
        L_0x0057:
            java.lang.reflect.Type[] r4 = r10.getActualTypeArguments()
            int r5 = r4.length
        L_0x005c:
            if (r2 >= r5) goto L_0x0077
            r6 = r4[r2]
            java.lang.reflect.Type r6 = m285a(r8, r9, r6)
            r7 = r4[r2]
            if (r6 == r7) goto L_0x0074
            if (r0 != 0) goto L_0x0072
            java.lang.Object r0 = r4.clone()
            r4 = r0
            java.lang.reflect.Type[] r4 = (java.lang.reflect.Type[]) r4
            r0 = 1
        L_0x0072:
            r4[r2] = r6
        L_0x0074:
            int r2 = r2 + 1
            goto L_0x005c
        L_0x0077:
            if (r0 == 0) goto L_0x0081
            java.lang.reflect.Type r8 = r10.getRawType()
            java.lang.reflect.ParameterizedType r10 = m282a(r3, r8, r4)
        L_0x0081:
            return r10
        L_0x0082:
            boolean r0 = r10 instanceof java.lang.reflect.WildcardType
            if (r0 == 0) goto L_0x00b4
            java.lang.reflect.WildcardType r10 = (java.lang.reflect.WildcardType) r10
            java.lang.reflect.Type[] r0 = r10.getLowerBounds()
            java.lang.reflect.Type[] r3 = r10.getUpperBounds()
            int r4 = r0.length
            if (r4 != r1) goto L_0x00a2
            r1 = r0[r2]
            java.lang.reflect.Type r8 = m285a(r8, r9, r1)
            r9 = r0[r2]
            if (r8 == r9) goto L_0x00b4
            java.lang.reflect.WildcardType r8 = m296g(r8)
            return r8
        L_0x00a2:
            int r0 = r3.length
            if (r0 != r1) goto L_0x00b4
            r0 = r3[r2]
            java.lang.reflect.Type r8 = m285a(r8, r9, r0)     // Catch:{ all -> 0x00b5 }
            r9 = r3[r2]
            if (r8 == r9) goto L_0x00b4
            java.lang.reflect.WildcardType r8 = m295f(r8)
            return r8
        L_0x00b4:
            return r10
        L_0x00b5:
            r8 = move-exception
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.C0101h.m285a(java.lang.reflect.Type, java.lang.Class, java.lang.reflect.Type):java.lang.reflect.Type");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.h.a(java.lang.Object[], java.lang.Object):int
     arg types: [java.lang.reflect.TypeVariable[], java.lang.reflect.TypeVariable<?>]
     candidates:
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
      a.a.a.j.h.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      a.a.a.j.h.a(java.lang.Object[], java.lang.Object):int */
    /* renamed from: a */
    static Type m286a(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> a = m280a(typeVariable);
        if (a == null) {
            return typeVariable;
        }
        Type a2 = m284a(type, cls, a);
        if (!(a2 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) a2).getActualTypeArguments()[m279a((Object[]) a.getTypeParameters(), (Object) typeVariable)];
    }

    /* renamed from: a */
    static boolean m287a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: a */
    public static boolean m288a(Type type, Type type2) {
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            return m287a(parameterizedType.getOwnerType(), parameterizedType2.getOwnerType()) && parameterizedType.getRawType().equals(parameterizedType2.getRawType()) && Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return m288a(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            return Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) && Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds());
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            return typeVariable.getGenericDeclaration() == typeVariable2.getGenericDeclaration() && typeVariable.getName().equals(typeVariable2.getName());
        }
    }

    /* renamed from: b */
    public static Type m289b(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            return cls.isArray() ? new C0102a(m289b(cls.getComponentType())) : cls;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new C0103b(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new C0102a(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new C0104c(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    /* renamed from: b */
    static Type m290b(Type type, Class<?> cls, Class<?> cls2) {
        C0100g.m277a(cls2.isAssignableFrom(cls));
        return m285a(type, cls, m284a(type, cls, cls2));
    }

    /* renamed from: b */
    public static Type[] m291b(Type type, Class<?> cls) {
        Class<Object> cls2 = Object.class;
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type b = m290b(type, cls, Map.class);
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments();
        }
        return new Type[]{cls2, cls2};
    }

    /* renamed from: c */
    static void m292c(Type type) {
        C0100g.m277a(!(type instanceof Class) || !((Class) type).isPrimitive());
    }

    /* renamed from: d */
    public static Type m293d(Type type) {
        return type instanceof GenericArrayType ? ((GenericArrayType) type).getGenericComponentType() : ((Class) type).getComponentType();
    }

    /* renamed from: e */
    public static Class<?> m294e(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            C0100g.m277a(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(m294e(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return m294e(((WildcardType) type).getUpperBounds()[0]);
            }
            String name = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + name);
        }
    }

    /* renamed from: f */
    public static WildcardType m295f(Type type) {
        return new C0104c(new Type[]{type}, f162a);
    }

    /* renamed from: g */
    public static WildcardType m296g(Type type) {
        return new C0104c(new Type[]{Object.class}, new Type[]{type});
    }

    /* renamed from: h */
    public static String m297h(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }
}
