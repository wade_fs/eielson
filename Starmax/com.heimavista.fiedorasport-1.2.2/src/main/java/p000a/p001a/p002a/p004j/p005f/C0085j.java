package p000a.p001a.p002a.p004j.p005f;

import java.util.ArrayList;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p004j.C0016a;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.j */
public final class C0085j extends C0008g<Object> {

    /* renamed from: b */
    public static final C0010h f130b = new C0086a();

    /* renamed from: a */
    private final C0139q f131a;

    /* renamed from: a.a.a.j.f.j$a */
    static class C0086a implements C0010h {
        C0086a() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() == Object.class) {
                return new C0085j(qVar);
            }
            return null;
        }
    }

    /* renamed from: a.a.a.j.f.j$b */
    static /* synthetic */ class C0087b {

        /* renamed from: a */
        static final /* synthetic */ int[] f132a = new int[C0129b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                a.a.a.n.b[] r0 = p000a.p001a.p002a.p008n.C0129b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a = r0
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x0014 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BEGIN_ARRAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x001f }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BEGIN_OBJECT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x002a }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.STRING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x0035 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NUMBER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x0040 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BOOLEAN     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.f132a     // Catch:{ NoSuchFieldError -> 0x004b }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NULL     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0085j.C0087b.<clinit>():void");
        }
    }

    C0085j(C0139q qVar) {
        this.f131a = qVar;
    }

    /* renamed from: a */
    public Object mo17a(C0127a aVar) {
        switch (C0087b.f132a[aVar.mo107C().ordinal()]) {
            case 1:
                ArrayList arrayList = new ArrayList();
                aVar.mo110a();
                while (aVar.mo116t()) {
                    arrayList.add(mo17a(aVar));
                }
                aVar.mo113d();
                return arrayList;
            case 2:
                C0016a aVar2 = new C0016a();
                aVar.mo111b();
                while (aVar.mo116t()) {
                    aVar2.put(aVar.mo122z(), mo17a(aVar));
                }
                aVar.mo114e();
                return aVar2;
            case 3:
                return aVar.mo106B();
            case 4:
                return Double.valueOf(aVar.mo119w());
            case 5:
                return Boolean.valueOf(aVar.mo118v());
            case 6:
                aVar.mo105A();
                return null;
            default:
                throw new IllegalStateException();
        }
    }

    /* renamed from: a */
    public void mo18a(C0130c cVar, Object obj) {
        if (obj == null) {
            cVar.mo137t();
            return;
        }
        C0008g a = this.f131a.mo193a(obj.getClass());
        if (a instanceof C0085j) {
            cVar.mo128b();
            cVar.mo134d();
            return;
        }
        super.mo18a(cVar, obj);
    }
}
