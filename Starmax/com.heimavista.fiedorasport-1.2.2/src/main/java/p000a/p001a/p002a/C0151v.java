package p000a.p001a.p002a;

import java.io.IOException;
import java.io.StringWriter;
import p000a.p001a.p002a.p004j.C0026d;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.v */
public abstract class C0151v {
    /* renamed from: b */
    public C0148s mo212b() {
        if (mo215f()) {
            return (C0148s) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    /* renamed from: c */
    public C0154y mo213c() {
        if (mo217i()) {
            return (C0154y) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    /* renamed from: d */
    public C0001b mo214d() {
        if (mo218j()) {
            return (C0001b) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    /* renamed from: f */
    public boolean mo215f() {
        return this instanceof C0148s;
    }

    /* renamed from: h */
    public boolean mo216h() {
        return this instanceof C0153x;
    }

    /* renamed from: i */
    public boolean mo217i() {
        return this instanceof C0154y;
    }

    /* renamed from: j */
    public boolean mo218j() {
        return this instanceof C0001b;
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            C0130c cVar = new C0130c(stringWriter);
            cVar.mo187a(true);
            C0026d.m40a(this, cVar);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
