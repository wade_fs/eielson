package p000a.p001a.p002a.p004j.p005f;

import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.p004j.C0105i;

/* renamed from: a.a.a.j.f.f */
public final class C0078f implements C0010h {

    /* renamed from: P */
    private final C0105i f113P;

    public C0078f(C0105i iVar) {
        this.f113P = iVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v8, types: [a.a.a.g] */
    /* JADX WARN: Type inference failed for: r9v9, types: [a.a.a.g] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p000a.p001a.p002a.C0008g<?> mo104a(p000a.p001a.p002a.p004j.C0105i r9, p000a.p001a.p002a.C0139q r10, p000a.p001a.p002a.p007l.C0125a<?> r11, p000a.p001a.p002a.p003i.C0012b r12) {
        /*
            r8 = this;
            java.lang.Class r0 = r12.value()
            a.a.a.l.a r0 = p000a.p001a.p002a.p007l.C0125a.m331a(r0)
            a.a.a.j.b r9 = r9.mo164a(r0)
            java.lang.Object r9 = r9.mo66a()
            boolean r0 = r9 instanceof p000a.p001a.p002a.C0008g
            if (r0 == 0) goto L_0x0017
            a.a.a.g r9 = (p000a.p001a.p002a.C0008g) r9
            goto L_0x004d
        L_0x0017:
            boolean r0 = r9 instanceof p000a.p001a.p002a.C0010h
            if (r0 == 0) goto L_0x0022
            a.a.a.h r9 = (p000a.p001a.p002a.C0010h) r9
            a.a.a.g r9 = r9.mo19a(r10, r11)
            goto L_0x004d
        L_0x0022:
            boolean r0 = r9 instanceof p000a.p001a.p002a.C0003d
            if (r0 != 0) goto L_0x0033
            boolean r1 = r9 instanceof p000a.p001a.p002a.C0150u
            if (r1 == 0) goto L_0x002b
            goto L_0x0033
        L_0x002b:
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            java.lang.String r10 = "@JsonAdapter value must be TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer reference."
            r9.<init>(r10)
            throw r9
        L_0x0033:
            r1 = 0
            if (r0 == 0) goto L_0x003b
            r0 = r9
            a.a.a.d r0 = (p000a.p001a.p002a.C0003d) r0
            r3 = r0
            goto L_0x003c
        L_0x003b:
            r3 = r1
        L_0x003c:
            boolean r0 = r9 instanceof p000a.p001a.p002a.C0150u
            if (r0 == 0) goto L_0x0043
            r1 = r9
            a.a.a.u r1 = (p000a.p001a.p002a.C0150u) r1
        L_0x0043:
            r4 = r1
            a.a.a.j.f.n r9 = new a.a.a.j.f.n
            r7 = 0
            r2 = r9
            r5 = r10
            r6 = r11
            r2.<init>(r3, r4, r5, r6, r7)
        L_0x004d:
            if (r9 == 0) goto L_0x0059
            boolean r10 = r12.nullSafe()
            if (r10 == 0) goto L_0x0059
            a.a.a.g r9 = r9.mo15a()
        L_0x0059:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0078f.mo104a(a.a.a.j.i, a.a.a.q, a.a.a.l.a, a.a.a.i.b):a.a.a.g");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [a.a.a.l.a, a.a.a.l.a<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> p000a.p001a.p002a.C0008g<T> mo19a(p000a.p001a.p002a.C0139q r3, p000a.p001a.p002a.p007l.C0125a<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.mo179a()
            java.lang.Class<a.a.a.i.b> r1 = p000a.p001a.p002a.p003i.C0012b.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            a.a.a.i.b r0 = (p000a.p001a.p002a.p003i.C0012b) r0
            if (r0 != 0) goto L_0x0010
            r3 = 0
            return r3
        L_0x0010:
            a.a.a.j.i r1 = r2.f113P
            a.a.a.g r3 = r2.mo104a(r1, r3, r4, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0078f.mo19a(a.a.a.q, a.a.a.l.a):a.a.a.g");
    }
}
