package p000a.p001a.p002a.p004j.p005f;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import p000a.p001a.p002a.C0001b;
import p000a.p001a.p002a.C0004e;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.C0148s;
import p000a.p001a.p002a.C0151v;
import p000a.p001a.p002a.C0152w;
import p000a.p001a.p002a.C0153x;
import p000a.p001a.p002a.C0154y;
import p000a.p001a.p002a.p003i.C0013c;
import p000a.p001a.p002a.p004j.C0123l;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.a */
public final class C0032a {

    /* renamed from: A */
    public static final C0008g<String> f35A = new C0039d();

    /* renamed from: B */
    public static final C0008g<BigDecimal> f36B = new C0041e();

    /* renamed from: C */
    public static final C0008g<BigInteger> f37C = new C0043f();

    /* renamed from: D */
    public static final C0010h f38D = m49a(String.class, f35A);

    /* renamed from: E */
    public static final C0008g<StringBuilder> f39E = new C0045g();

    /* renamed from: F */
    public static final C0010h f40F = m49a(StringBuilder.class, f39E);

    /* renamed from: G */
    public static final C0008g<StringBuffer> f41G = new C0049i();

    /* renamed from: H */
    public static final C0010h f42H = m49a(StringBuffer.class, f41G);

    /* renamed from: I */
    public static final C0008g<URL> f43I = new C0051j();

    /* renamed from: J */
    public static final C0010h f44J = m49a(URL.class, f43I);

    /* renamed from: K */
    public static final C0008g<URI> f45K = new C0053k();

    /* renamed from: L */
    public static final C0010h f46L = m49a(URI.class, f45K);

    /* renamed from: M */
    public static final C0008g<InetAddress> f47M = new C0054l();

    /* renamed from: N */
    public static final C0010h f48N = m51b(InetAddress.class, f47M);

    /* renamed from: O */
    public static final C0008g<UUID> f49O = new C0055m();

    /* renamed from: P */
    public static final C0010h f50P = m49a(UUID.class, f49O);

    /* renamed from: Q */
    public static final C0008g<Currency> f51Q = new C0056n().mo15a();

    /* renamed from: R */
    public static final C0010h f52R = m49a(Currency.class, f51Q);

    /* renamed from: S */
    public static final C0010h f53S = new C0057o();

    /* renamed from: T */
    public static final C0008g<Calendar> f54T = new C0059p();

    /* renamed from: U */
    public static final C0010h f55U = m52b(Calendar.class, GregorianCalendar.class, f54T);

    /* renamed from: V */
    public static final C0008g<Locale> f56V = new C0060q();

    /* renamed from: W */
    public static final C0010h f57W = m49a(Locale.class, f56V);

    /* renamed from: X */
    public static final C0008g<C0151v> f58X = new C0061r();

    /* renamed from: Y */
    public static final C0010h f59Y = m51b(C0151v.class, f58X);

    /* renamed from: Z */
    public static final C0010h f60Z = new C0063t();

    /* renamed from: a */
    public static final C0008g<Class> f61a = new C0047h();

    /* renamed from: b */
    public static final C0010h f62b = m49a(Class.class, f61a);

    /* renamed from: c */
    public static final C0008g<BitSet> f63c = new C0062s();

    /* renamed from: d */
    public static final C0010h f64d = m49a(BitSet.class, f63c);

    /* renamed from: e */
    public static final C0008g<Boolean> f65e = new C0070z();

    /* renamed from: f */
    public static final C0008g<Boolean> f66f = new C0034a0();

    /* renamed from: g */
    public static final C0010h f67g = m50a(Boolean.TYPE, Boolean.class, f65e);

    /* renamed from: h */
    public static final C0008g<Number> f68h = new C0036b0();

    /* renamed from: i */
    public static final C0010h f69i = m50a(Byte.TYPE, Byte.class, f68h);

    /* renamed from: j */
    public static final C0008g<Number> f70j = new C0038c0();

    /* renamed from: k */
    public static final C0010h f71k = m50a(Short.TYPE, Short.class, f70j);

    /* renamed from: l */
    public static final C0008g<Number> f72l = new C0040d0();

    /* renamed from: m */
    public static final C0010h f73m = m50a(Integer.TYPE, Integer.class, f72l);

    /* renamed from: n */
    public static final C0008g<AtomicInteger> f74n = new C0042e0().mo15a();

    /* renamed from: o */
    public static final C0010h f75o = m49a(AtomicInteger.class, f74n);

    /* renamed from: p */
    public static final C0008g<AtomicBoolean> f76p = new C0044f0().mo15a();

    /* renamed from: q */
    public static final C0010h f77q = m49a(AtomicBoolean.class, f76p);

    /* renamed from: r */
    public static final C0008g<AtomicIntegerArray> f78r = new C0048h0().mo15a();

    /* renamed from: s */
    public static final C0010h f79s = m49a(AtomicIntegerArray.class, f78r);

    /* renamed from: t */
    public static final C0008g<Number> f80t = new C0050i0();

    /* renamed from: u */
    public static final C0008g<Number> f81u = new C0052j0();

    /* renamed from: v */
    public static final C0008g<Number> f82v = new C0033a();

    /* renamed from: w */
    public static final C0008g<Number> f83w = new C0035b();

    /* renamed from: x */
    public static final C0010h f84x = m49a(Number.class, f83w);

    /* renamed from: y */
    public static final C0008g<Character> f85y = new C0037c();

    /* renamed from: z */
    public static final C0010h f86z = m50a(Character.TYPE, Character.class, f85y);

    /* renamed from: a.a.a.j.f.a$a */
    static class C0033a extends C0008g<Number> {
        C0033a() {
        }

        /* renamed from: a */
        public Number m54a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Double.valueOf(aVar.mo119w());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$a0 */
    static class C0034a0 extends C0008g<Boolean> {
        C0034a0() {
        }

        /* renamed from: a */
        public Boolean m58a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Boolean.valueOf(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Boolean bool) {
            cVar.mo132c(bool == null ? "null" : bool.toString());
        }
    }

    /* renamed from: a.a.a.j.f.a$b */
    static class C0035b extends C0008g<Number> {
        C0035b() {
        }

        /* renamed from: a */
        public Number m62a(C0127a aVar) {
            C0129b C = aVar.mo107C();
            int i = C0069y.f102a[C.ordinal()];
            if (i == 1) {
                return new C0123l(aVar.mo106B());
            }
            if (i == 4) {
                aVar.mo105A();
                return null;
            }
            throw new C0004e("Expecting number, got: " + C);
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$b0 */
    static class C0036b0 extends C0008g<Number> {
        C0036b0() {
        }

        /* renamed from: a */
        public Number m66a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return Byte.valueOf((byte) aVar.mo120x());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$c */
    static class C0037c extends C0008g<Character> {
        C0037c() {
        }

        /* renamed from: a */
        public Character m70a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            String B = aVar.mo106B();
            if (B.length() == 1) {
                return Character.valueOf(B.charAt(0));
            }
            throw new C0004e("Expecting character, got: " + B);
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Character ch) {
            cVar.mo132c(ch == null ? null : String.valueOf(ch));
        }
    }

    /* renamed from: a.a.a.j.f.a$c0 */
    static class C0038c0 extends C0008g<Number> {
        C0038c0() {
        }

        /* renamed from: a */
        public Number m74a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return Short.valueOf((short) aVar.mo120x());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$d */
    static class C0039d extends C0008g<String> {
        C0039d() {
        }

        /* renamed from: a */
        public String mo17a(C0127a aVar) {
            C0129b C = aVar.mo107C();
            if (C != C0129b.NULL) {
                return C == C0129b.BOOLEAN ? Boolean.toString(aVar.mo118v()) : aVar.mo106B();
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, String str) {
            cVar.mo132c(str);
        }
    }

    /* renamed from: a.a.a.j.f.a$d0 */
    static class C0040d0 extends C0008g<Number> {
        C0040d0() {
        }

        /* renamed from: a */
        public Number m82a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return Integer.valueOf(aVar.mo120x());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$e */
    static class C0041e extends C0008g<BigDecimal> {
        C0041e() {
        }

        /* renamed from: a */
        public BigDecimal mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return new BigDecimal(aVar.mo106B());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, BigDecimal bigDecimal) {
            cVar.mo127a(bigDecimal);
        }
    }

    /* renamed from: a.a.a.j.f.a$e0 */
    static class C0042e0 extends C0008g<AtomicInteger> {
        C0042e0() {
        }

        /* renamed from: a */
        public AtomicInteger mo17a(C0127a aVar) {
            try {
                return new AtomicInteger(aVar.mo120x());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, AtomicInteger atomicInteger) {
            cVar.mo136g((long) atomicInteger.get());
        }
    }

    /* renamed from: a.a.a.j.f.a$f */
    static class C0043f extends C0008g<BigInteger> {
        C0043f() {
        }

        /* renamed from: a */
        public BigInteger mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return new BigInteger(aVar.mo106B());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, BigInteger bigInteger) {
            cVar.mo127a(bigInteger);
        }
    }

    /* renamed from: a.a.a.j.f.a$f0 */
    static class C0044f0 extends C0008g<AtomicBoolean> {
        C0044f0() {
        }

        /* renamed from: a */
        public AtomicBoolean mo17a(C0127a aVar) {
            return new AtomicBoolean(aVar.mo118v());
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, AtomicBoolean atomicBoolean) {
            cVar.mo130b(atomicBoolean.get());
        }
    }

    /* renamed from: a.a.a.j.f.a$g */
    static class C0045g extends C0008g<StringBuilder> {
        C0045g() {
        }

        /* renamed from: a */
        public StringBuilder mo17a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return new StringBuilder(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, StringBuilder sb) {
            cVar.mo132c(sb == null ? null : sb.toString());
        }
    }

    /* renamed from: a.a.a.j.f.a$g0 */
    private static final class C0046g0<T extends Enum<T>> extends C0008g<T> {

        /* renamed from: a */
        private final Map<String, T> f87a = new HashMap();

        /* renamed from: b */
        private final Map<T, String> f88b = new HashMap();

        public C0046g0(Class<T> cls) {
            try {
                Enum[] enumArr = (Enum[]) cls.getEnumConstants();
                for (Enum enumR : enumArr) {
                    String name = enumR.name();
                    C0013c cVar = (C0013c) cls.getField(name).getAnnotation(C0013c.class);
                    if (cVar != null) {
                        name = cVar.value();
                        for (String str : cVar.alternate()) {
                            this.f87a.put(str, enumR);
                        }
                    }
                    this.f87a.put(name, enumR);
                    this.f88b.put(enumR, name);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError(e);
            }
        }

        /* renamed from: a */
        public T m106a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return (Enum) this.f87a.get(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, T t) {
            cVar.mo132c(t == null ? null : this.f88b.get(t));
        }
    }

    /* renamed from: a.a.a.j.f.a$h */
    static class C0047h extends C0008g<Class> {
        C0047h() {
        }

        /* renamed from: a */
        public Class m110a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Class cls) {
            if (cls == null) {
                cVar.mo137t();
                return;
            }
            throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
        }
    }

    /* renamed from: a.a.a.j.f.a$h0 */
    static class C0048h0 extends C0008g<AtomicIntegerArray> {
        C0048h0() {
        }

        /* renamed from: a */
        public AtomicIntegerArray mo17a(C0127a aVar) {
            ArrayList arrayList = new ArrayList();
            aVar.mo110a();
            while (aVar.mo116t()) {
                try {
                    arrayList.add(Integer.valueOf(aVar.mo120x()));
                } catch (NumberFormatException e) {
                    throw new C0004e(e);
                }
            }
            aVar.mo113d();
            int size = arrayList.size();
            AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
            for (int i = 0; i < size; i++) {
                atomicIntegerArray.set(i, ((Integer) arrayList.get(i)).intValue());
            }
            return atomicIntegerArray;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, AtomicIntegerArray atomicIntegerArray) {
            cVar.mo125a();
            int length = atomicIntegerArray.length();
            for (int i = 0; i < length; i++) {
                cVar.mo136g((long) atomicIntegerArray.get(i));
            }
            cVar.mo131c();
        }
    }

    /* renamed from: a.a.a.j.f.a$i */
    static class C0049i extends C0008g<StringBuffer> {
        C0049i() {
        }

        /* renamed from: a */
        public StringBuffer mo17a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return new StringBuffer(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, StringBuffer stringBuffer) {
            cVar.mo132c(stringBuffer == null ? null : stringBuffer.toString());
        }
    }

    /* renamed from: a.a.a.j.f.a$i0 */
    static class C0050i0 extends C0008g<Number> {
        C0050i0() {
        }

        /* renamed from: a */
        public Number m122a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                return Long.valueOf(aVar.mo121y());
            } catch (NumberFormatException e) {
                throw new C0004e(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$j */
    static class C0051j extends C0008g<URL> {
        C0051j() {
        }

        /* renamed from: a */
        public URL mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            String B = aVar.mo106B();
            if ("null".equals(B)) {
                return null;
            }
            return new URL(B);
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, URL url) {
            cVar.mo132c(url == null ? null : url.toExternalForm());
        }
    }

    /* renamed from: a.a.a.j.f.a$j0 */
    static class C0052j0 extends C0008g<Number> {
        C0052j0() {
        }

        /* renamed from: a */
        public Number m130a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Float.valueOf((float) aVar.mo119w());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.j.f.a$k */
    static class C0053k extends C0008g<URI> {
        C0053k() {
        }

        /* renamed from: a */
        public URI mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            try {
                String B = aVar.mo106B();
                if ("null".equals(B)) {
                    return null;
                }
                return new URI(B);
            } catch (URISyntaxException e) {
                throw new C0152w(e);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, URI uri) {
            cVar.mo132c(uri == null ? null : uri.toASCIIString());
        }
    }

    /* renamed from: a.a.a.j.f.a$l */
    static class C0054l extends C0008g<InetAddress> {
        C0054l() {
        }

        /* renamed from: a */
        public InetAddress mo17a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return InetAddress.getByName(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, InetAddress inetAddress) {
            cVar.mo132c(inetAddress == null ? null : inetAddress.getHostAddress());
        }
    }

    /* renamed from: a.a.a.j.f.a$m */
    static class C0055m extends C0008g<UUID> {
        C0055m() {
        }

        /* renamed from: a */
        public UUID mo17a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return UUID.fromString(aVar.mo106B());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, UUID uuid) {
            cVar.mo132c(uuid == null ? null : uuid.toString());
        }
    }

    /* renamed from: a.a.a.j.f.a$n */
    static class C0056n extends C0008g<Currency> {
        C0056n() {
        }

        /* renamed from: a */
        public Currency mo17a(C0127a aVar) {
            return Currency.getInstance(aVar.mo106B());
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Currency currency) {
            cVar.mo132c(currency.getCurrencyCode());
        }
    }

    /* renamed from: a.a.a.j.f.a$o */
    static class C0057o implements C0010h {

        /* renamed from: a.a.a.j.f.a$o$a */
        class C0058a extends C0008g<Timestamp> {

            /* renamed from: a */
            final /* synthetic */ C0008g f89a;

            C0058a(C0057o oVar, C0008g gVar) {
                this.f89a = super;
            }

            /* renamed from: a */
            public Timestamp mo17a(C0127a aVar) {
                Date date = (Date) this.f89a.mo17a(aVar);
                if (date != null) {
                    return new Timestamp(date.getTime());
                }
                return null;
            }

            /* renamed from: a */
            public void mo18a(C0130c cVar, Timestamp timestamp) {
                this.f89a.mo18a(cVar, timestamp);
            }
        }

        C0057o() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() != Timestamp.class) {
                return null;
            }
            return new C0058a(this, qVar.mo193a(Date.class));
        }
    }

    /* renamed from: a.a.a.j.f.a$p */
    static class C0059p extends C0008g<Calendar> {
        C0059p() {
        }

        /* renamed from: a */
        public Calendar mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            aVar.mo111b();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (aVar.mo107C() != C0129b.END_OBJECT) {
                String z = aVar.mo122z();
                int x = aVar.mo120x();
                if ("year".equals(z)) {
                    i = x;
                } else if ("month".equals(z)) {
                    i2 = x;
                } else if ("dayOfMonth".equals(z)) {
                    i3 = x;
                } else if ("hourOfDay".equals(z)) {
                    i4 = x;
                } else if ("minute".equals(z)) {
                    i5 = x;
                } else if ("second".equals(z)) {
                    i6 = x;
                }
            }
            aVar.mo114e();
            return new GregorianCalendar(i, i2, i3, i4, i5, i6);
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Calendar calendar) {
            if (calendar == null) {
                cVar.mo137t();
                return;
            }
            cVar.mo128b();
            cVar.mo129b("year");
            cVar.mo136g((long) calendar.get(1));
            cVar.mo129b("month");
            cVar.mo136g((long) calendar.get(2));
            cVar.mo129b("dayOfMonth");
            cVar.mo136g((long) calendar.get(5));
            cVar.mo129b("hourOfDay");
            cVar.mo136g((long) calendar.get(11));
            cVar.mo129b("minute");
            cVar.mo136g((long) calendar.get(12));
            cVar.mo129b("second");
            cVar.mo136g((long) calendar.get(13));
            cVar.mo134d();
        }
    }

    /* renamed from: a.a.a.j.f.a$q */
    static class C0060q extends C0008g<Locale> {
        C0060q() {
        }

        /* renamed from: a */
        public Locale mo17a(C0127a aVar) {
            Locale locale;
            String str = null;
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(aVar.mo106B(), "_");
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            if (stringTokenizer.hasMoreElements()) {
                str = stringTokenizer.nextToken();
            }
            if (nextToken2 == null && str == null) {
                return new Locale(nextToken);
            }
            if (str == null) {
                return locale;
            }
            locale = new Locale(nextToken, nextToken2, str);
            return locale;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Locale locale) {
            cVar.mo132c(locale == null ? null : locale.toString());
        }
    }

    /* renamed from: a.a.a.j.f.a$r */
    static class C0061r extends C0008g<C0151v> {
        C0061r() {
        }

        /* renamed from: a */
        public C0151v m163a(C0127a aVar) {
            switch (C0069y.f102a[aVar.mo107C().ordinal()]) {
                case 1:
                    return new C0001b(new C0123l(aVar.mo106B()));
                case 2:
                    return new C0001b(Boolean.valueOf(aVar.mo118v()));
                case 3:
                    return new C0001b(aVar.mo106B());
                case 4:
                    aVar.mo105A();
                    return C0153x.f253a;
                case 5:
                    C0148s sVar = new C0148s();
                    aVar.mo110a();
                    while (aVar.mo116t()) {
                        sVar.mo207a(m163a(aVar));
                    }
                    aVar.mo113d();
                    return sVar;
                case 6:
                    C0154y yVar = new C0154y();
                    aVar.mo111b();
                    while (aVar.mo116t()) {
                        yVar.mo222a(aVar.mo122z(), m163a(aVar));
                    }
                    aVar.mo114e();
                    return yVar;
                default:
                    throw new IllegalArgumentException();
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, C0151v vVar) {
            if (vVar == null || vVar.mo216h()) {
                cVar.mo137t();
            } else if (vVar.mo218j()) {
                C0001b d = vVar.mo214d();
                if (d.mo12s()) {
                    cVar.mo127a(d.mo9p());
                } else if (d.mo11r()) {
                    cVar.mo130b(d.mo4k());
                } else {
                    cVar.mo132c(d.mo10q());
                }
            } else if (vVar.mo215f()) {
                cVar.mo125a();
                Iterator<C0151v> it = vVar.mo212b().iterator();
                while (it.hasNext()) {
                    mo18a(cVar, it.next());
                }
                cVar.mo131c();
            } else if (vVar.mo217i()) {
                cVar.mo128b();
                for (Map.Entry entry : vVar.mo213c().mo225k()) {
                    cVar.mo129b((String) entry.getKey());
                    mo18a(cVar, (C0151v) entry.getValue());
                }
                cVar.mo134d();
            } else {
                throw new IllegalArgumentException("Couldn't write " + vVar.getClass());
            }
        }
    }

    /* renamed from: a.a.a.j.f.a$s */
    static class C0062s extends C0008g<BitSet> {
        C0062s() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
            if (java.lang.Integer.parseInt(r1) != 0) goto L_0x0074;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0072, code lost:
            if (r8.mo120x() != 0) goto L_0x0074;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0076, code lost:
            r1 = false;
         */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0079  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x007c A[SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.BitSet mo17a(p000a.p001a.p002a.p008n.C0127a r8) {
            /*
                r7 = this;
                a.a.a.n.b r0 = r8.mo107C()
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NULL
                if (r0 != r1) goto L_0x000d
                r8.mo105A()
                r8 = 0
                return r8
            L_0x000d:
                java.util.BitSet r0 = new java.util.BitSet
                r0.<init>()
                r8.mo110a()
                a.a.a.n.b r1 = r8.mo107C()
                r2 = 0
                r3 = 0
            L_0x001b:
                a.a.a.n.b r4 = p000a.p001a.p002a.p008n.C0129b.END_ARRAY
                if (r1 == r4) goto L_0x0083
                int[] r4 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a
                int r5 = r1.ordinal()
                r4 = r4[r5]
                r5 = 1
                if (r4 == r5) goto L_0x006e
                r6 = 2
                if (r4 == r6) goto L_0x0069
                r6 = 3
                if (r4 != r6) goto L_0x0052
                java.lang.String r1 = r8.mo106B()
                int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x003b }
                if (r1 == 0) goto L_0x0076
                goto L_0x0074
            L_0x003b:
                a.a.a.e r8 = new a.a.a.e
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Error: Expecting: bitset number value (1, 0), Found: "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r8.<init>(r0)
                throw r8
            L_0x0052:
                a.a.a.e r8 = new a.a.a.e
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Invalid bitset value type: "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r8.<init>(r0)
                throw r8
            L_0x0069:
                boolean r1 = r8.mo118v()
                goto L_0x0077
            L_0x006e:
                int r1 = r8.mo120x()
                if (r1 == 0) goto L_0x0076
            L_0x0074:
                r1 = 1
                goto L_0x0077
            L_0x0076:
                r1 = 0
            L_0x0077:
                if (r1 == 0) goto L_0x007c
                r0.set(r3)
            L_0x007c:
                int r3 = r3 + 1
                a.a.a.n.b r1 = r8.mo107C()
                goto L_0x001b
            L_0x0083:
                r8.mo113d()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0032a.C0062s.mo17a(a.a.a.n.a):java.util.BitSet");
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, BitSet bitSet) {
            if (bitSet == null) {
                cVar.mo137t();
                return;
            }
            cVar.mo125a();
            for (int i = 0; i < bitSet.length(); i++) {
                cVar.mo136g(bitSet.get(i) ? 1 : 0);
            }
            cVar.mo131c();
        }
    }

    /* renamed from: a.a.a.j.f.a$t */
    static class C0063t implements C0010h {
        C0063t() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            Class<? super T> a = aVar.mo179a();
            if (!Enum.class.isAssignableFrom(a) || a == Enum.class) {
                return null;
            }
            if (!a.isEnum()) {
                a = a.getSuperclass();
            }
            return new C0046g0(a);
        }
    }

    /* renamed from: a.a.a.j.f.a$u */
    static class C0064u implements C0010h {

        /* renamed from: P */
        final /* synthetic */ Class f90P;

        /* renamed from: Q */
        final /* synthetic */ C0008g f91Q;

        C0064u(Class cls, C0008g gVar) {
            this.f90P = cls;
            this.f91Q = gVar;
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() == this.f90P) {
                return this.f91Q;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f90P.getName() + ",adapter=" + this.f91Q + "]";
        }
    }

    /* renamed from: a.a.a.j.f.a$v */
    static class C0065v implements C0010h {

        /* renamed from: P */
        final /* synthetic */ Class f92P;

        /* renamed from: Q */
        final /* synthetic */ Class f93Q;

        /* renamed from: R */
        final /* synthetic */ C0008g f94R;

        C0065v(Class cls, Class cls2, C0008g gVar) {
            this.f92P = cls;
            this.f93Q = cls2;
            this.f94R = gVar;
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            Class<? super T> a = aVar.mo179a();
            if (a == this.f92P || a == this.f93Q) {
                return this.f94R;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f93Q.getName() + "+" + this.f92P.getName() + ",adapter=" + this.f94R + "]";
        }
    }

    /* renamed from: a.a.a.j.f.a$w */
    static class C0066w implements C0010h {

        /* renamed from: P */
        final /* synthetic */ Class f95P;

        /* renamed from: Q */
        final /* synthetic */ Class f96Q;

        /* renamed from: R */
        final /* synthetic */ C0008g f97R;

        C0066w(Class cls, Class cls2, C0008g gVar) {
            this.f95P = cls;
            this.f96Q = cls2;
            this.f97R = gVar;
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            Class<? super T> a = aVar.mo179a();
            if (a == this.f95P || a == this.f96Q) {
                return this.f97R;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f95P.getName() + "+" + this.f96Q.getName() + ",adapter=" + this.f97R + "]";
        }
    }

    /* renamed from: a.a.a.j.f.a$x */
    static class C0067x implements C0010h {

        /* renamed from: P */
        final /* synthetic */ Class f98P;

        /* renamed from: Q */
        final /* synthetic */ C0008g f99Q;

        /* renamed from: a.a.a.j.f.a$x$a */
        class C0068a extends C0008g<T1> {

            /* renamed from: a */
            final /* synthetic */ Class f100a;

            C0068a(Class cls) {
                this.f100a = cls;
            }

            /* renamed from: a */
            public T1 mo17a(C0127a aVar) {
                T1 a = C0067x.this.f99Q.mo17a(aVar);
                if (a == null || this.f100a.isInstance(a)) {
                    return a;
                }
                throw new C0004e("Expected a " + this.f100a.getName() + " but was " + a.getClass().getName());
            }

            /* renamed from: a */
            public void mo18a(C0130c cVar, T1 t1) {
                C0067x.this.f99Q.mo18a(cVar, t1);
            }
        }

        C0067x(Class cls, C0008g gVar) {
            this.f98P = cls;
            this.f99Q = gVar;
        }

        /* renamed from: a */
        public <T2> C0008g<T2> mo19a(C0139q qVar, C0125a<T2> aVar) {
            Class<? super T2> a = aVar.mo179a();
            if (!this.f98P.isAssignableFrom(a)) {
                return null;
            }
            return new C0068a(a);
        }

        public String toString() {
            return "Factory[typeHierarchy=" + this.f98P.getName() + ",adapter=" + this.f99Q + "]";
        }
    }

    /* renamed from: a.a.a.j.f.a$y */
    static /* synthetic */ class C0069y {

        /* renamed from: a */
        static final /* synthetic */ int[] f102a = new int[C0129b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(3:19|20|22)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|22) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                a.a.a.n.b[] r0 = p000a.p001a.p002a.p008n.C0129b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a = r0
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x0014 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NUMBER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x001f }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BOOLEAN     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x002a }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.STRING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x0035 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NULL     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x0040 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BEGIN_ARRAY     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x004b }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.BEGIN_OBJECT     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x0056 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.END_DOCUMENT     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x0062 }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.NAME     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x006e }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.END_OBJECT     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.f102a     // Catch:{ NoSuchFieldError -> 0x007a }
                a.a.a.n.b r1 = p000a.p001a.p002a.p008n.C0129b.END_ARRAY     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0032a.C0069y.<clinit>():void");
        }
    }

    /* renamed from: a.a.a.j.f.a$z */
    static class C0070z extends C0008g<Boolean> {
        C0070z() {
        }

        /* renamed from: a */
        public Boolean m178a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return aVar.mo107C() == C0129b.STRING ? Boolean.valueOf(Boolean.parseBoolean(aVar.mo106B())) : Boolean.valueOf(aVar.mo118v());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Boolean bool) {
            cVar.mo126a(bool);
        }
    }

    /* renamed from: a */
    public static <TT> C0010h m49a(Class<TT> cls, C0008g<TT> gVar) {
        return new C0064u(cls, gVar);
    }

    /* renamed from: a */
    public static <TT> C0010h m50a(Class<TT> cls, Class<TT> cls2, C0008g<? super TT> gVar) {
        return new C0065v(cls, cls2, gVar);
    }

    /* renamed from: b */
    public static <T1> C0010h m51b(Class<T1> cls, C0008g<T1> gVar) {
        return new C0067x(cls, gVar);
    }

    /* renamed from: b */
    public static <TT> C0010h m52b(Class<TT> cls, Class<? extends TT> cls2, C0008g<? super TT> gVar) {
        return new C0066w(cls, cls2, gVar);
    }
}
