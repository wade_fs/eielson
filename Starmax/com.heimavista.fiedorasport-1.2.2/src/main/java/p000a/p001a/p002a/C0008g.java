package p000a.p001a.p002a;

import java.io.IOException;
import p000a.p001a.p002a.p004j.p005f.C0081h;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.g */
public abstract class C0008g<T> {

    /* renamed from: a.a.a.g$a */
    class C0009a extends C0008g<T> {
        C0009a() {
        }

        /* renamed from: a */
        public T mo17a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return C0008g.this.mo17a(aVar);
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, T t) {
            if (t == null) {
                cVar.mo137t();
            } else {
                C0008g.this.mo18a(cVar, t);
            }
        }
    }

    /* renamed from: a */
    public final C0008g<T> mo15a() {
        return new C0009a();
    }

    /* renamed from: a */
    public final C0151v mo16a(T t) {
        try {
            C0081h hVar = new C0081h();
            mo18a(hVar, t);
            return hVar.mo138u();
        } catch (IOException e) {
            throw new C0152w(e);
        }
    }

    /* renamed from: a */
    public abstract T mo17a(C0127a aVar);

    /* renamed from: a */
    public abstract void mo18a(C0130c cVar, T t);
}
