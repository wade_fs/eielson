package p000a.p001a.p002a.p004j.p005f;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p008n.C0127a;

/* renamed from: a.a.a.j.f.o */
final class C0099o<T> extends C0008g<T> {

    /* renamed from: a */
    private final C0139q f159a;

    /* renamed from: b */
    private final C0008g<T> f160b;

    /* renamed from: c */
    private final Type f161c;

    C0099o(C0139q qVar, C0008g<T> gVar, Type type) {
        this.f159a = qVar;
        this.f160b = super;
        this.f161c = type;
    }

    /* renamed from: a */
    private Type m273a(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    /* renamed from: a */
    public T mo17a(C0127a aVar) {
        return this.f160b.mo17a(aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if ((r1 instanceof p000a.p001a.p002a.p004j.p005f.C0088k.C0090b) == false) goto L_0x0023;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo18a(p000a.p001a.p002a.p008n.C0130c r4, T r5) {
        /*
            r3 = this;
            a.a.a.g<T> r0 = r3.f160b
            java.lang.reflect.Type r1 = r3.f161c
            java.lang.reflect.Type r1 = r3.m273a(r1, r5)
            java.lang.reflect.Type r2 = r3.f161c
            if (r1 == r2) goto L_0x0022
            a.a.a.q r0 = r3.f159a
            a.a.a.l.a r1 = p000a.p001a.p002a.p007l.C0125a.m332a(r1)
            a.a.a.g r0 = r0.mo192a(r1)
            boolean r1 = r0 instanceof p000a.p001a.p002a.p004j.p005f.C0088k.C0090b
            if (r1 != 0) goto L_0x001b
            goto L_0x0022
        L_0x001b:
            a.a.a.g<T> r1 = r3.f160b
            boolean r2 = r1 instanceof p000a.p001a.p002a.p004j.p005f.C0088k.C0090b
            if (r2 != 0) goto L_0x0022
            goto L_0x0023
        L_0x0022:
            r1 = r0
        L_0x0023:
            r1.mo18a(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0099o.mo18a(a.a.a.n.c, java.lang.Object):void");
    }
}
