package p000a.p001a.p002a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.a.a.s */
public final class C0148s extends C0151v implements Iterable<C0151v> {

    /* renamed from: P */
    private final List<C0151v> f252P = new ArrayList();

    /* renamed from: a */
    public void mo207a(C0151v vVar) {
        if (vVar == null) {
            vVar = C0153x.f253a;
        }
        this.f252P.add(vVar);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof C0148s) && ((C0148s) obj).f252P.equals(this.f252P));
    }

    public int hashCode() {
        return this.f252P.hashCode();
    }

    public Iterator<C0151v> iterator() {
        return this.f252P.iterator();
    }
}
