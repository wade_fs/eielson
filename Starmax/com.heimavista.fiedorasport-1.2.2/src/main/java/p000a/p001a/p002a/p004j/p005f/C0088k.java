package p000a.p001a.p002a.p004j.p005f;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import p000a.p001a.p002a.C0004e;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0138p;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p003i.C0012b;
import p000a.p001a.p002a.p003i.C0013c;
import p000a.p001a.p002a.p004j.C0024b;
import p000a.p001a.p002a.p004j.C0025c;
import p000a.p001a.p002a.p004j.C0101h;
import p000a.p001a.p002a.p004j.C0105i;
import p000a.p001a.p002a.p004j.C0120j;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.k */
public final class C0088k implements C0010h {

    /* renamed from: P */
    private final C0105i f133P;

    /* renamed from: Q */
    private final C0138p f134Q;

    /* renamed from: R */
    private final C0120j f135R;

    /* renamed from: S */
    private final C0078f f136S;

    /* renamed from: a.a.a.j.f.k$a */
    class C0089a extends C0091c {

        /* renamed from: d */
        final /* synthetic */ Field f137d;

        /* renamed from: e */
        final /* synthetic */ boolean f138e;

        /* renamed from: f */
        final /* synthetic */ C0008g f139f;

        /* renamed from: g */
        final /* synthetic */ C0139q f140g;

        /* renamed from: h */
        final /* synthetic */ C0125a f141h;

        /* renamed from: i */
        final /* synthetic */ boolean f142i;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C0089a(C0088k kVar, String str, boolean z, boolean z2, Field field, boolean z3, C0008g gVar, C0139q qVar, C0125a aVar, boolean z4) {
            super(str, z, z2);
            this.f137d = field;
            this.f138e = z3;
            this.f139f = gVar;
            this.f140g = qVar;
            this.f141h = aVar;
            this.f142i = z4;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo144a(C0127a aVar, Object obj) {
            Object a = this.f139f.mo17a(aVar);
            if (a != null || !this.f142i) {
                this.f137d.set(obj, a);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo145a(C0130c cVar, Object obj) {
            (this.f138e ? this.f139f : new C0099o(this.f140g, this.f139f, this.f141h.mo180b())).mo18a(cVar, this.f137d.get(obj));
        }

        /* renamed from: a */
        public boolean mo146a(Object obj) {
            return super.f146b && this.f137d.get(obj) != obj;
        }
    }

    /* renamed from: a.a.a.j.f.k$b */
    public static final class C0090b<T> extends C0008g<T> {

        /* renamed from: a */
        private final C0024b<T> f143a;

        /* renamed from: b */
        private final Map<String, C0091c> f144b;

        C0090b(C0024b<T> bVar, Map<String, C0091c> map) {
            this.f143a = bVar;
            this.f144b = map;
        }

        /* renamed from: a */
        public T mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            T a = this.f143a.mo66a();
            try {
                aVar.mo111b();
                while (aVar.mo116t()) {
                    C0091c cVar = this.f144b.get(aVar.mo122z());
                    if (cVar != null) {
                        if (cVar.f147c) {
                            cVar.mo144a(aVar, a);
                        }
                    }
                    aVar.mo108D();
                }
                aVar.mo114e();
                return a;
            } catch (IllegalStateException e) {
                throw new C0004e(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, T t) {
            if (t == null) {
                cVar.mo137t();
                return;
            }
            cVar.mo128b();
            try {
                for (C0091c cVar2 : this.f144b.values()) {
                    if (cVar2.mo146a(t)) {
                        cVar.mo129b(cVar2.f145a);
                        cVar2.mo145a(cVar, t);
                    }
                }
                cVar.mo134d();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* renamed from: a.a.a.j.f.k$c */
    static abstract class C0091c {

        /* renamed from: a */
        final String f145a;

        /* renamed from: b */
        final boolean f146b;

        /* renamed from: c */
        final boolean f147c;

        protected C0091c(String str, boolean z, boolean z2) {
            this.f145a = str;
            this.f146b = z;
            this.f147c = z2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract void mo144a(C0127a aVar, Object obj);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract void mo145a(C0130c cVar, Object obj);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract boolean mo146a(Object obj);
    }

    public C0088k(C0105i iVar, C0138p pVar, C0120j jVar, C0078f fVar) {
        this.f133P = iVar;
        this.f134Q = pVar;
        this.f135R = jVar;
        this.f136S = fVar;
    }

    /* renamed from: a */
    private C0091c m246a(C0139q qVar, Field field, String str, C0125a<?> aVar, boolean z, boolean z2) {
        C0125a<?> aVar2 = aVar;
        boolean a = C0025c.m38a((Type) aVar.mo179a());
        C0012b bVar = (C0012b) field.getAnnotation(C0012b.class);
        C0008g<?> a2 = bVar != null ? this.f136S.mo104a(this.f133P, qVar, aVar2, bVar) : null;
        boolean z3 = a2 != null;
        if (a2 == null) {
            a2 = qVar.mo192a(aVar2);
        }
        return new C0089a(this, str, z, z2, field, z3, a2, qVar, aVar, a);
    }

    /* renamed from: a */
    private List<String> m247a(Field field) {
        C0013c cVar = (C0013c) field.getAnnotation(C0013c.class);
        if (cVar == null) {
            return Collections.singletonList(this.f134Q.mo190a(field));
        }
        String value = cVar.value();
        String[] alternate = cVar.alternate();
        if (alternate.length == 0) {
            return Collections.singletonList(value);
        }
        ArrayList arrayList = new ArrayList(alternate.length + 1);
        arrayList.add(value);
        for (String str : alternate) {
            arrayList.add(str);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.f.k.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      a.a.a.j.f.k.a(a.a.a.q, a.a.a.l.a):a.a.a.g<T>
      a.a.a.h.a(a.a.a.q, a.a.a.l.a):a.a.a.g<T>
      a.a.a.j.f.k.a(java.lang.reflect.Field, boolean):boolean */
    /* renamed from: a */
    private Map<String, C0091c> m248a(C0139q qVar, C0125a<?> aVar, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b = aVar.mo180b();
        C0125a<?> aVar2 = aVar;
        Class<?> cls2 = cls;
        while (cls2 != Object.class) {
            Field[] declaredFields = cls2.getDeclaredFields();
            int length = declaredFields.length;
            boolean z = false;
            int i = 0;
            while (i < length) {
                Field field = declaredFields[i];
                boolean a = mo143a(field, true);
                boolean a2 = mo143a(field, z);
                if (a || a2) {
                    field.setAccessible(true);
                    Type a3 = C0101h.m285a(aVar2.mo180b(), cls2, field.getGenericType());
                    List<String> a4 = m247a(field);
                    C0091c cVar = null;
                    int i2 = 0;
                    while (i2 < a4.size()) {
                        String str = a4.get(i2);
                        boolean z2 = i2 != 0 ? false : a;
                        String str2 = str;
                        C0091c cVar2 = cVar;
                        int i3 = i2;
                        List<String> list = a4;
                        Field field2 = field;
                        cVar = cVar2 == null ? (C0091c) linkedHashMap.put(str2, m246a(qVar, field, str2, C0125a.m332a(a3), z2, a2)) : cVar2;
                        i2 = i3 + 1;
                        a = z2;
                        a4 = list;
                        field = field2;
                    }
                    C0091c cVar3 = cVar;
                    if (cVar3 != null) {
                        throw new IllegalArgumentException(b + " declares multiple JSON fields named " + cVar3.f145a);
                    }
                }
                i++;
                z = false;
            }
            aVar2 = C0125a.m332a(C0101h.m285a(aVar2.mo180b(), cls2, cls2.getGenericSuperclass()));
            cls2 = aVar2.mo179a();
        }
        return linkedHashMap;
    }

    /* renamed from: a */
    static boolean m249a(Field field, boolean z, C0120j jVar) {
        return !jVar.mo166a(field.getType(), z) && !jVar.mo167a(field, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.f.k.a(a.a.a.q, a.a.a.l.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, a.a.a.j.f.k$c>
     arg types: [a.a.a.q, a.a.a.l.a<T>, java.lang.Class<? super T>]
     candidates:
      a.a.a.j.f.k.a(java.lang.reflect.Field, boolean, a.a.a.j.j):boolean
      a.a.a.j.f.k.a(a.a.a.q, a.a.a.l.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, a.a.a.j.f.k$c> */
    /* renamed from: a */
    public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
        Class<? super T> a = aVar.mo179a();
        if (!Object.class.isAssignableFrom(a)) {
            return null;
        }
        return new C0090b(this.f133P.mo164a(aVar), m248a(qVar, (C0125a<?>) aVar, (Class<?>) a));
    }

    /* renamed from: a */
    public boolean mo143a(Field field, boolean z) {
        return m249a(field, z, this.f135R);
    }
}
