package p000a.p001a.p002a.p004j.p005f;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import p000a.p001a.p002a.C0001b;
import p000a.p001a.p002a.C0004e;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.C0151v;
import p000a.p001a.p002a.p004j.C0024b;
import p000a.p001a.p002a.p004j.C0026d;
import p000a.p001a.p002a.p004j.C0101h;
import p000a.p001a.p002a.p004j.C0105i;
import p000a.p001a.p002a.p004j.C0122k;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.i */
public final class C0083i implements C0010h {

    /* renamed from: P */
    private final C0105i f124P;

    /* renamed from: Q */
    final boolean f125Q;

    /* renamed from: a.a.a.j.f.i$a */
    private final class C0084a<K, V> extends C0008g<Map<K, V>> {

        /* renamed from: a */
        private final C0008g<K> f126a;

        /* renamed from: b */
        private final C0008g<V> f127b;

        /* renamed from: c */
        private final C0024b<? extends Map<K, V>> f128c;

        public C0084a(C0139q qVar, Type type, C0008g<K> gVar, Type type2, C0008g<V> gVar2, C0024b<? extends Map<K, V>> bVar) {
            this.f126a = new C0099o(qVar, super, type);
            this.f127b = new C0099o(qVar, super, type2);
            this.f128c = bVar;
        }

        /* renamed from: a */
        private String m238a(C0151v vVar) {
            if (vVar.mo218j()) {
                C0001b d = vVar.mo214d();
                if (d.mo12s()) {
                    return String.valueOf(d.mo9p());
                }
                if (d.mo11r()) {
                    return Boolean.toString(d.mo4k());
                }
                if (d.mo13u()) {
                    return d.mo10q();
                }
                throw new AssertionError();
            } else if (vVar.mo216h()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }

        /* renamed from: a */
        public Map<K, V> mo17a(C0127a aVar) {
            C0129b C = aVar.mo107C();
            if (C == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            Map<K, V> map = (Map) this.f128c.mo66a();
            if (C == C0129b.BEGIN_ARRAY) {
                aVar.mo110a();
                while (aVar.mo116t()) {
                    aVar.mo110a();
                    K a = this.f126a.mo17a(aVar);
                    if (map.put(a, this.f127b.mo17a(aVar)) == null) {
                        aVar.mo113d();
                    } else {
                        throw new C0004e("duplicate key: " + ((Object) a));
                    }
                }
                aVar.mo113d();
            } else {
                aVar.mo111b();
                while (aVar.mo116t()) {
                    C0122k.f192a.mo169a(aVar);
                    K a2 = this.f126a.mo17a(aVar);
                    if (map.put(a2, this.f127b.mo17a(aVar)) != null) {
                        throw new C0004e("duplicate key: " + ((Object) a2));
                    }
                }
                aVar.mo114e();
            }
            return map;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Map<K, V> map) {
            if (map == null) {
                cVar.mo137t();
            } else if (!C0083i.this.f125Q) {
                cVar.mo128b();
                for (Map.Entry entry : map.entrySet()) {
                    cVar.mo129b(String.valueOf(entry.getKey()));
                    this.f127b.mo18a(cVar, entry.getValue());
                }
                cVar.mo134d();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                int i = 0;
                boolean z = false;
                for (Map.Entry entry2 : map.entrySet()) {
                    C0151v a = this.f126a.mo16a(entry2.getKey());
                    arrayList.add(a);
                    arrayList2.add(entry2.getValue());
                    z |= a.mo215f() || a.mo217i();
                }
                if (z) {
                    cVar.mo125a();
                    while (i < arrayList.size()) {
                        cVar.mo125a();
                        C0026d.m40a((C0151v) arrayList.get(i), cVar);
                        this.f127b.mo18a(cVar, arrayList2.get(i));
                        cVar.mo131c();
                        i++;
                    }
                    cVar.mo131c();
                    return;
                }
                cVar.mo128b();
                while (i < arrayList.size()) {
                    cVar.mo129b(m238a((C0151v) arrayList.get(i)));
                    this.f127b.mo18a(cVar, arrayList2.get(i));
                    i++;
                }
                cVar.mo134d();
            }
        }
    }

    public C0083i(C0105i iVar, boolean z) {
        this.f124P = iVar;
        this.f125Q = z;
    }

    /* renamed from: a */
    private C0008g<?> m236a(C0139q qVar, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? C0032a.f66f : qVar.mo192a(C0125a.m332a(type));
    }

    /* renamed from: a */
    public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
        Type b = aVar.mo180b();
        if (!Map.class.isAssignableFrom(aVar.mo179a())) {
            return null;
        }
        Type[] b2 = C0101h.m291b(b, C0101h.m294e(b));
        return new C0084a(qVar, b2[0], m236a(qVar, b2[0]), b2[1], qVar.mo192a(C0125a.m332a(b2[1])), this.f124P.mo164a(aVar));
    }
}
