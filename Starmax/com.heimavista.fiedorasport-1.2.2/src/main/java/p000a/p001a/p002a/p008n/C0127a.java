package p000a.p001a.p002a.p008n;

import com.facebook.internal.ServerProtocol;
import com.tencent.bugly.Bugly;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import p000a.p001a.p002a.p004j.C0122k;
import p000a.p001a.p002a.p004j.p005f.C0079g;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: a.a.a.n.a */
public class C0127a implements Closeable {

    /* renamed from: e0 */
    private static final char[] f197e0 = ")]}'\n".toCharArray();

    /* renamed from: P */
    private final Reader f198P;

    /* renamed from: Q */
    private boolean f199Q = false;

    /* renamed from: R */
    private final char[] f200R = new char[1024];

    /* renamed from: S */
    private int f201S = 0;

    /* renamed from: T */
    private int f202T = 0;

    /* renamed from: U */
    private int f203U = 0;

    /* renamed from: V */
    private int f204V = 0;

    /* renamed from: W */
    int f205W = 0;

    /* renamed from: X */
    private long f206X;

    /* renamed from: Y */
    private int f207Y;

    /* renamed from: Z */
    private String f208Z;

    /* renamed from: a0 */
    private int[] f209a0 = new int[32];

    /* renamed from: b0 */
    private int f210b0 = 0;

    /* renamed from: c0 */
    private String[] f211c0;

    /* renamed from: d0 */
    private int[] f212d0;

    /* renamed from: a.a.a.n.a$a */
    static class C0128a extends C0122k {
        C0128a() {
        }

        /* renamed from: a */
        public void mo169a(C0127a aVar) {
            int i;
            if (aVar instanceof C0079g) {
                ((C0079g) aVar).mo109E();
                return;
            }
            int i2 = aVar.f205W;
            if (i2 == 0) {
                i2 = aVar.mo185c();
            }
            if (i2 == 13) {
                i = 9;
            } else if (i2 == 12) {
                i = 8;
            } else if (i2 == 14) {
                i = 10;
            } else {
                throw new IllegalStateException("Expected a name but was " + aVar.mo107C() + aVar.m338G());
            }
            aVar.f205W = i;
        }
    }

    static {
        C0122k.f192a = new C0128a();
    }

    public C0127a(Reader reader) {
        int[] iArr = this.f209a0;
        int i = this.f210b0;
        this.f210b0 = i + 1;
        iArr[i] = 6;
        this.f211c0 = new String[32];
        this.f212d0 = new int[32];
        if (reader != null) {
            this.f198P = reader;
            return;
        }
        throw new NullPointerException("in == null");
    }

    /* renamed from: E */
    private void mo109E() {
        if (!this.f199Q) {
            m352c("Use JsonReader.setLenient(true) to accept malformed JSON");
            throw null;
        }
    }

    /* renamed from: F */
    private void m337F() {
        m348b(true);
        this.f201S--;
        int i = this.f201S;
        char[] cArr = f197e0;
        if (i + cArr.length <= this.f202T || m347a(cArr.length)) {
            int i2 = 0;
            while (true) {
                char[] cArr2 = f197e0;
                if (i2 >= cArr2.length) {
                    this.f201S += cArr2.length;
                    return;
                } else if (this.f200R[this.f201S + i2] == cArr2[i2]) {
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: G */
    public String m338G() {
        return " at line " + (this.f203U + 1) + " column " + ((this.f201S - this.f204V) + 1) + " path " + mo115g();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0049, code lost:
        mo109E();
     */
    /* renamed from: H */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m339H() {
        /*
            r5 = this;
            r0 = 0
            r1 = 0
            r2 = r1
        L_0x0003:
            r1 = 0
        L_0x0004:
            int r3 = r5.f201S
            int r3 = r3 + r1
            int r4 = r5.f202T
            if (r3 >= r4) goto L_0x004d
            char[] r4 = r5.f200R
            char r3 = r4[r3]
            r4 = 9
            if (r3 == r4) goto L_0x005b
            r4 = 10
            if (r3 == r4) goto L_0x005b
            r4 = 12
            if (r3 == r4) goto L_0x005b
            r4 = 13
            if (r3 == r4) goto L_0x005b
            r4 = 32
            if (r3 == r4) goto L_0x005b
            r4 = 35
            if (r3 == r4) goto L_0x0049
            r4 = 44
            if (r3 == r4) goto L_0x005b
            r4 = 47
            if (r3 == r4) goto L_0x0049
            r4 = 61
            if (r3 == r4) goto L_0x0049
            r4 = 123(0x7b, float:1.72E-43)
            if (r3 == r4) goto L_0x005b
            r4 = 125(0x7d, float:1.75E-43)
            if (r3 == r4) goto L_0x005b
            r4 = 58
            if (r3 == r4) goto L_0x005b
            r4 = 59
            if (r3 == r4) goto L_0x0049
            switch(r3) {
                case 91: goto L_0x005b;
                case 92: goto L_0x0049;
                case 93: goto L_0x005b;
                default: goto L_0x0046;
            }
        L_0x0046:
            int r1 = r1 + 1
            goto L_0x0004
        L_0x0049:
            r5.mo109E()
            goto L_0x005b
        L_0x004d:
            char[] r3 = r5.f200R
            int r3 = r3.length
            if (r1 >= r3) goto L_0x005d
            int r3 = r1 + 1
            boolean r3 = r5.m347a(r3)
            if (r3 == 0) goto L_0x005b
            goto L_0x0004
        L_0x005b:
            r0 = r1
            goto L_0x0077
        L_0x005d:
            if (r2 != 0) goto L_0x0064
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
        L_0x0064:
            char[] r3 = r5.f200R
            int r4 = r5.f201S
            r2.append(r3, r4, r1)
            int r3 = r5.f201S
            int r3 = r3 + r1
            r5.f201S = r3
            r1 = 1
            boolean r1 = r5.m347a(r1)
            if (r1 != 0) goto L_0x0003
        L_0x0077:
            if (r2 != 0) goto L_0x0083
            java.lang.String r1 = new java.lang.String
            char[] r2 = r5.f200R
            int r3 = r5.f201S
            r1.<init>(r2, r3, r0)
            goto L_0x008e
        L_0x0083:
            char[] r1 = r5.f200R
            int r3 = r5.f201S
            r2.append(r1, r3, r0)
            java.lang.String r1 = r2.toString()
        L_0x008e:
            int r2 = r5.f201S
            int r2 = r2 + r0
            r5.f201S = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p008n.C0127a.m339H():java.lang.String");
    }

    /* renamed from: I */
    private int m340I() {
        String str;
        String str2;
        int i;
        char c = this.f200R[this.f201S];
        if (c == 't' || c == 'T') {
            i = 5;
            str2 = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
            str = "TRUE";
        } else if (c == 'f' || c == 'F') {
            i = 6;
            str2 = Bugly.SDK_IS_DEV;
            str = "FALSE";
        } else if (c != 'n' && c != 'N') {
            return 0;
        } else {
            i = 7;
            str2 = "null";
            str = "NULL";
        }
        int length = str2.length();
        for (int i2 = 1; i2 < length; i2++) {
            if (this.f201S + i2 >= this.f202T && !m347a(i2 + 1)) {
                return 0;
            }
            char c2 = this.f200R[this.f201S + i2];
            if (c2 != str2.charAt(i2) && c2 != str.charAt(i2)) {
                return 0;
            }
        }
        if ((this.f201S + length < this.f202T || m347a(length + 1)) && m346a(this.f200R[this.f201S + length])) {
            return 0;
        }
        this.f201S += length;
        this.f205W = i;
        return i;
    }

    /* renamed from: J */
    private int m341J() {
        int i;
        char c;
        char[] cArr = this.f200R;
        int i2 = this.f201S;
        int i3 = 0;
        int i4 = this.f202T;
        int i5 = 0;
        char c2 = 0;
        long j = 0;
        boolean z = false;
        boolean z2 = true;
        while (true) {
            if (i2 + i5 == i4) {
                if (i5 == cArr.length) {
                    return i3;
                }
                if (!m347a(i5 + 1)) {
                    break;
                }
                i2 = this.f201S;
                i4 = this.f202T;
            }
            c = cArr[i2 + i5];
            if (c == '+') {
                i3 = 0;
                if (c2 != 5) {
                    return 0;
                }
            } else if (c == 'E' || c == 'e') {
                i3 = 0;
                if (c2 != 2 && c2 != 4) {
                    return 0;
                }
                c2 = 5;
                i5++;
            } else {
                if (c == '-') {
                    i3 = 0;
                    if (c2 == 0) {
                        c2 = 1;
                        z = true;
                    } else if (c2 != 5) {
                        return 0;
                    }
                } else if (c == '.') {
                    i3 = 0;
                    if (c2 != 2) {
                        return 0;
                    }
                    c2 = 3;
                } else if (c >= '0' && c <= '9') {
                    if (c2 == 1 || c2 == 0) {
                        j = (long) (-(c - '0'));
                        i3 = 0;
                        c2 = 2;
                    } else {
                        if (c2 == 2) {
                            if (j == 0) {
                                return 0;
                            }
                            long j2 = (10 * j) - ((long) (c - '0'));
                            int i6 = (j > -922337203685477580L ? 1 : (j == -922337203685477580L ? 0 : -1));
                            boolean z3 = i6 > 0 || (i6 == 0 && j2 < j);
                            j = j2;
                            z2 = z3 & z2;
                        } else if (c2 == 3) {
                            i3 = 0;
                            c2 = 4;
                        } else if (c2 == 5 || c2 == 6) {
                            i3 = 0;
                            c2 = 7;
                        }
                        i3 = 0;
                    }
                }
                i5++;
            }
            c2 = 6;
            i5++;
        }
        if (m346a(c)) {
            return 0;
        }
        if (c2 == 2 && z2 && (j != Long.MIN_VALUE || z)) {
            if (!z) {
                j = -j;
            }
            this.f206X = j;
            this.f201S += i5;
            i = 15;
        } else if (c2 != 2 && c2 != 4 && c2 != 7) {
            return 0;
        } else {
            this.f207Y = i5;
            i = 16;
        }
        this.f205W = i;
        return i;
    }

    /* renamed from: K */
    private char m342K() {
        int i;
        int i2;
        if (this.f201S != this.f202T || m347a(1)) {
            char[] cArr = this.f200R;
            int i3 = this.f201S;
            this.f201S = i3 + 1;
            char c = cArr[i3];
            if (c == 10) {
                this.f203U++;
                this.f204V = this.f201S;
            } else if (!(c == '\"' || c == '\'' || c == '/' || c == '\\')) {
                if (c == 'b') {
                    return 8;
                }
                if (c == 'f') {
                    return 12;
                }
                if (c == 'n') {
                    return 10;
                }
                if (c == 'r') {
                    return 13;
                }
                if (c == 't') {
                    return 9;
                }
                if (c != 'u') {
                    m352c("Invalid escape sequence");
                    throw null;
                } else if (this.f201S + 4 <= this.f202T || m347a(4)) {
                    char c2 = 0;
                    int i4 = this.f201S;
                    int i5 = i4 + 4;
                    while (i4 < i5) {
                        char c3 = this.f200R[i4];
                        char c4 = (char) (c2 << 4);
                        if (c3 < '0' || c3 > '9') {
                            if (c3 >= 'a' && c3 <= 'f') {
                                i = c3 - 'a';
                            } else if (c3 < 'A' || c3 > 'F') {
                                throw new NumberFormatException("\\u" + new String(this.f200R, this.f201S, 4));
                            } else {
                                i = c3 - 'A';
                            }
                            i2 = i + 10;
                        } else {
                            i2 = c3 - '0';
                        }
                        c2 = (char) (c4 + i2);
                        i4++;
                    }
                    this.f201S += 4;
                    return c2;
                } else {
                    m352c("Unterminated escape sequence");
                    throw null;
                }
            }
            return c;
        }
        m352c("Unterminated escape sequence");
        throw null;
    }

    /* renamed from: L */
    private void m343L() {
        char c;
        do {
            if (this.f201S < this.f202T || m347a(1)) {
                char[] cArr = this.f200R;
                int i = this.f201S;
                this.f201S = i + 1;
                c = cArr[i];
                if (c == 10) {
                    this.f203U++;
                    this.f204V = this.f201S;
                    return;
                }
            } else {
                return;
            }
        } while (c != 13);
    }

    /* renamed from: M */
    private void m344M() {
        do {
            int i = 0;
            while (true) {
                int i2 = this.f201S + i;
                if (i2 < this.f202T) {
                    char c = this.f200R[i2];
                    if (!(c == 9 || c == 10 || c == 12 || c == 13 || c == ' ')) {
                        if (c != '#') {
                            if (c != ',') {
                                if (!(c == '/' || c == '=')) {
                                    if (!(c == '{' || c == '}' || c == ':')) {
                                        if (c != ';') {
                                            switch (c) {
                                                case '[':
                                                case ']':
                                                    break;
                                                case '\\':
                                                    break;
                                                default:
                                                    i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    this.f201S = i2;
                }
            }
            mo109E();
            this.f201S += i;
            return;
        } while (m347a(1));
    }

    /* renamed from: a */
    private boolean m346a(char c) {
        if (c == 9 || c == 10 || c == 12 || c == 13 || c == ' ') {
            return false;
        }
        if (c != '#') {
            if (c == ',') {
                return false;
            }
            if (!(c == '/' || c == '=')) {
                if (c == '{' || c == '}' || c == ':') {
                    return false;
                }
                if (c != ';') {
                    switch (c) {
                        case '[':
                        case ']':
                            return false;
                        case '\\':
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        mo109E();
        return false;
    }

    /* renamed from: a */
    private boolean m347a(int i) {
        int i2;
        char[] cArr = this.f200R;
        int i3 = this.f204V;
        int i4 = this.f201S;
        this.f204V = i3 - i4;
        int i5 = this.f202T;
        if (i5 != i4) {
            this.f202T = i5 - i4;
            System.arraycopy(cArr, i4, cArr, 0, this.f202T);
        } else {
            this.f202T = 0;
        }
        this.f201S = 0;
        do {
            Reader reader = this.f198P;
            int i6 = this.f202T;
            int read = reader.read(cArr, i6, cArr.length - i6);
            if (read == -1) {
                return false;
            }
            this.f202T += read;
            if (this.f203U == 0 && (i2 = this.f204V) == 0 && this.f202T > 0 && cArr[0] == 65279) {
                this.f201S++;
                this.f204V = i2 + 1;
                i++;
            }
        } while (this.f202T < i);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        if (r1 != '/') goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        r7.f201S = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0058, code lost:
        if (r4 != r2) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        r7.f201S--;
        r2 = m347a(2);
        r7.f201S++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0068, code lost:
        if (r2 != false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006b, code lost:
        mo109E();
        r2 = r7.f201S;
        r3 = r0[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0074, code lost:
        if (r3 == '*') goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0076, code lost:
        if (r3 == '/') goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0078, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0079, code lost:
        r7.f201S = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007e, code lost:
        r7.f201S = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0088, code lost:
        if (m351b("*/") == false) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008f, code lost:
        m352c("Unterminated comment");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0095, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0096, code lost:
        r7.f201S = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009a, code lost:
        if (r1 != '#') goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009c, code lost:
        mo109E();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a4, code lost:
        return r1;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m348b(boolean r8) {
        /*
            r7 = this;
            char[] r0 = r7.f200R
        L_0x0002:
            int r1 = r7.f201S
        L_0x0004:
            int r2 = r7.f202T
        L_0x0006:
            r3 = 1
            if (r1 != r2) goto L_0x0034
            r7.f201S = r1
            boolean r1 = r7.m347a(r3)
            if (r1 != 0) goto L_0x0030
            if (r8 != 0) goto L_0x0015
            r8 = -1
            return r8
        L_0x0015:
            java.io.EOFException r8 = new java.io.EOFException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "End of input"
            r0.append(r1)
            java.lang.String r1 = r7.m338G()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r8.<init>(r0)
            throw r8
        L_0x0030:
            int r1 = r7.f201S
            int r2 = r7.f202T
        L_0x0034:
            int r4 = r1 + 1
            char r1 = r0[r1]
            r5 = 10
            if (r1 != r5) goto L_0x0044
            int r1 = r7.f203U
            int r1 = r1 + r3
            r7.f203U = r1
            r7.f204V = r4
            goto L_0x00a5
        L_0x0044:
            r5 = 32
            if (r1 == r5) goto L_0x00a5
            r5 = 13
            if (r1 == r5) goto L_0x00a5
            r5 = 9
            if (r1 != r5) goto L_0x0051
            goto L_0x00a5
        L_0x0051:
            r5 = 47
            if (r1 != r5) goto L_0x0096
            r7.f201S = r4
            r6 = 2
            if (r4 != r2) goto L_0x006b
            int r2 = r7.f201S
            int r2 = r2 - r3
            r7.f201S = r2
            boolean r2 = r7.m347a(r6)
            int r4 = r7.f201S
            int r4 = r4 + r3
            r7.f201S = r4
            if (r2 != 0) goto L_0x006b
            return r1
        L_0x006b:
            r7.mo109E()
            int r2 = r7.f201S
            char r3 = r0[r2]
            r4 = 42
            if (r3 == r4) goto L_0x007e
            if (r3 == r5) goto L_0x0079
            return r1
        L_0x0079:
            int r2 = r2 + 1
            r7.f201S = r2
            goto L_0x009f
        L_0x007e:
            int r2 = r2 + 1
            r7.f201S = r2
            java.lang.String r1 = "*/"
            boolean r1 = r7.m351b(r1)
            if (r1 == 0) goto L_0x008f
            int r1 = r7.f201S
            int r1 = r1 + r6
            goto L_0x0004
        L_0x008f:
            java.lang.String r8 = "Unterminated comment"
            r7.m352c(r8)
            r8 = 0
            throw r8
        L_0x0096:
            r2 = 35
            r7.f201S = r4
            if (r1 != r2) goto L_0x00a4
            r7.mo109E()
        L_0x009f:
            r7.m343L()
            goto L_0x0002
        L_0x00a4:
            return r1
        L_0x00a5:
            r1 = r4
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p008n.C0127a.m348b(boolean):int");
    }

    /* renamed from: b */
    private String m349b(char c) {
        char[] cArr = this.f200R;
        StringBuilder sb = new StringBuilder();
        do {
            int i = this.f201S;
            int i2 = this.f202T;
            int i3 = i;
            while (i < i2) {
                int i4 = i + 1;
                char c2 = cArr[i];
                if (c2 == c) {
                    this.f201S = i4;
                    sb.append(cArr, i3, (i4 - i3) - 1);
                    return sb.toString();
                } else if (c2 == '\\') {
                    this.f201S = i4;
                    sb.append(cArr, i3, (i4 - i3) - 1);
                    sb.append(m342K());
                    i3 = this.f201S;
                    i2 = this.f202T;
                    i = i3;
                } else {
                    if (c2 == 10) {
                        this.f203U++;
                        this.f204V = i4;
                    }
                    i = i4;
                }
            }
            sb.append(cArr, i3, i - i3);
            this.f201S = i;
        } while (m347a(1));
        m352c("Unterminated string");
        throw null;
    }

    /* renamed from: b */
    private void m350b(int i) {
        int i2 = this.f210b0;
        int[] iArr = this.f209a0;
        if (i2 == iArr.length) {
            int i3 = i2 * 2;
            int[] iArr2 = new int[i3];
            int[] iArr3 = new int[i3];
            String[] strArr = new String[i3];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            System.arraycopy(this.f212d0, 0, iArr3, 0, this.f210b0);
            System.arraycopy(this.f211c0, 0, strArr, 0, this.f210b0);
            this.f209a0 = iArr2;
            this.f212d0 = iArr3;
            this.f211c0 = strArr;
        }
        int[] iArr4 = this.f209a0;
        int i4 = this.f210b0;
        this.f210b0 = i4 + 1;
        iArr4[i4] = i;
    }

    /* renamed from: b */
    private boolean m351b(String str) {
        while (true) {
            int i = 0;
            if (this.f201S + str.length() > this.f202T && !m347a(str.length())) {
                return false;
            }
            char[] cArr = this.f200R;
            int i2 = this.f201S;
            if (cArr[i2] == 10) {
                this.f203U++;
                this.f204V = i2 + 1;
            } else {
                while (i < str.length()) {
                    if (this.f200R[this.f201S + i] == str.charAt(i)) {
                        i++;
                    }
                }
                return true;
            }
            this.f201S++;
        }
    }

    /* renamed from: c */
    private IOException m352c(String str) {
        throw new C0131d(str + m338G());
    }

    /* renamed from: c */
    private void m353c(char c) {
        char[] cArr = this.f200R;
        while (true) {
            int i = this.f201S;
            int i2 = this.f202T;
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    char c2 = cArr[i];
                    if (c2 == c) {
                        this.f201S = i3;
                        return;
                    } else if (c2 == '\\') {
                        this.f201S = i3;
                        m342K();
                        break;
                    } else {
                        if (c2 == 10) {
                            this.f203U++;
                            this.f204V = i3;
                        }
                        i = i3;
                    }
                } else {
                    this.f201S = i;
                    if (!m347a(1)) {
                        m352c("Unterminated string");
                        throw null;
                    }
                }
            }
        }
    }

    /* renamed from: A */
    public void mo105A() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 7) {
            this.f205W = 0;
            int[] iArr = this.f212d0;
            int i2 = this.f210b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        throw new IllegalStateException("Expected null but was " + mo107C() + m338G());
    }

    /* renamed from: B */
    public String mo106B() {
        String str;
        char c;
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 10) {
            str = m339H();
        } else {
            if (i == 8) {
                c = '\'';
            } else if (i == 9) {
                c = '\"';
            } else if (i == 11) {
                str = this.f208Z;
                this.f208Z = null;
            } else if (i == 15) {
                str = Long.toString(this.f206X);
            } else if (i == 16) {
                str = new String(this.f200R, this.f201S, this.f207Y);
                this.f201S += this.f207Y;
            } else {
                throw new IllegalStateException("Expected a string but was " + mo107C() + m338G());
            }
            str = m349b(c);
        }
        this.f205W = 0;
        int[] iArr = this.f212d0;
        int i2 = this.f210b0 - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    /* renamed from: C */
    public C0129b mo107C() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        switch (i) {
            case 1:
                return C0129b.BEGIN_OBJECT;
            case 2:
                return C0129b.END_OBJECT;
            case 3:
                return C0129b.BEGIN_ARRAY;
            case 4:
                return C0129b.END_ARRAY;
            case 5:
            case 6:
                return C0129b.BOOLEAN;
            case 7:
                return C0129b.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return C0129b.STRING;
            case 12:
            case 13:
            case 14:
                return C0129b.NAME;
            case 15:
            case 16:
                return C0129b.NUMBER;
            case 17:
                return C0129b.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    /* renamed from: D */
    public void mo108D() {
        char c;
        int i = 0;
        do {
            int i2 = this.f205W;
            if (i2 == 0) {
                i2 = mo185c();
            }
            if (i2 == 3) {
                m350b(1);
            } else if (i2 == 1) {
                m350b(3);
            } else if (i2 == 4 || i2 == 2) {
                this.f210b0--;
                i--;
                this.f205W = 0;
            } else if (i2 == 14 || i2 == 10) {
                m344M();
                this.f205W = 0;
            } else {
                if (i2 == 8 || i2 == 12) {
                    c = '\'';
                } else if (i2 == 9 || i2 == 13) {
                    c = '\"';
                } else {
                    if (i2 == 16) {
                        this.f201S += this.f207Y;
                    }
                    this.f205W = 0;
                }
                m353c(c);
                this.f205W = 0;
            }
            i++;
            this.f205W = 0;
        } while (i != 0);
        int[] iArr = this.f212d0;
        int i3 = this.f210b0 - 1;
        iArr[i3] = iArr[i3] + 1;
        this.f211c0[i3] = "null";
    }

    /* renamed from: a */
    public void mo110a() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 3) {
            m350b(1);
            this.f212d0[this.f210b0 - 1] = 0;
            this.f205W = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_ARRAY but was " + mo107C() + m338G());
    }

    /* renamed from: a */
    public final void mo184a(boolean z) {
        this.f199Q = z;
    }

    /* renamed from: b */
    public void mo111b() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 1) {
            m350b(3);
            this.f205W = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_OBJECT but was " + mo107C() + m338G());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0109  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int mo185c() {
        /*
            r16 = this;
            r0 = r16
            int[] r1 = r0.f209a0
            int r2 = r0.f210b0
            r3 = 1
            int r2 = r2 - r3
            r4 = r1[r2]
            r5 = 93
            r6 = 39
            r7 = 34
            r8 = 8
            r9 = 59
            r10 = 44
            r11 = 3
            r12 = 7
            r13 = 4
            r14 = 0
            r15 = 2
            if (r4 != r3) goto L_0x0021
            r1[r2] = r15
            goto L_0x00a0
        L_0x0021:
            if (r4 != r15) goto L_0x003a
            int r1 = r0.m348b(r3)
            if (r1 == r10) goto L_0x00a0
            if (r1 == r9) goto L_0x0036
            if (r1 != r5) goto L_0x0030
            r0.f205W = r13
            return r13
        L_0x0030:
            java.lang.String r1 = "Unterminated array"
            r0.m352c(r1)
            throw r14
        L_0x0036:
            r16.mo109E()
            goto L_0x00a0
        L_0x003a:
            r15 = 5
            if (r4 == r11) goto L_0x0114
            if (r4 != r15) goto L_0x0041
            goto L_0x0114
        L_0x0041:
            if (r4 != r13) goto L_0x0074
            r1[r2] = r15
            int r1 = r0.m348b(r3)
            r2 = 58
            if (r1 == r2) goto L_0x00a0
            r2 = 61
            if (r1 != r2) goto L_0x006e
            r16.mo109E()
            int r1 = r0.f201S
            int r2 = r0.f202T
            if (r1 < r2) goto L_0x0060
            boolean r1 = r0.m347a(r3)
            if (r1 == 0) goto L_0x00a0
        L_0x0060:
            char[] r1 = r0.f200R
            int r2 = r0.f201S
            char r1 = r1[r2]
            r15 = 62
            if (r1 != r15) goto L_0x00a0
            int r2 = r2 + r3
            r0.f201S = r2
            goto L_0x00a0
        L_0x006e:
            java.lang.String r1 = "Expected ':'"
            r0.m352c(r1)
            throw r14
        L_0x0074:
            r1 = 6
            if (r4 != r1) goto L_0x0086
            boolean r1 = r0.f199Q
            if (r1 == 0) goto L_0x007e
            r16.m337F()
        L_0x007e:
            int[] r1 = r0.f209a0
            int r2 = r0.f210b0
            int r2 = r2 - r3
            r1[r2] = r12
            goto L_0x00a0
        L_0x0086:
            if (r4 != r12) goto L_0x009e
            r1 = 0
            int r1 = r0.m348b(r1)
            r2 = -1
            if (r1 != r2) goto L_0x0095
            r1 = 17
        L_0x0092:
            r0.f205W = r1
            return r1
        L_0x0095:
            r16.mo109E()
            int r1 = r0.f201S
            int r1 = r1 - r3
            r0.f201S = r1
            goto L_0x00a0
        L_0x009e:
            if (r4 == r8) goto L_0x010c
        L_0x00a0:
            int r1 = r0.m348b(r3)
            if (r1 == r7) goto L_0x0109
            if (r1 == r6) goto L_0x0103
            if (r1 == r10) goto L_0x00ec
            if (r1 == r9) goto L_0x00ec
            r2 = 91
            if (r1 == r2) goto L_0x00e9
            if (r1 == r5) goto L_0x00e4
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 == r2) goto L_0x00e1
            int r1 = r0.f201S
            int r1 = r1 - r3
            r0.f201S = r1
            int r1 = r16.m340I()
            if (r1 == 0) goto L_0x00c2
            return r1
        L_0x00c2:
            int r1 = r16.m341J()
            if (r1 == 0) goto L_0x00c9
            return r1
        L_0x00c9:
            char[] r1 = r0.f200R
            int r2 = r0.f201S
            char r1 = r1[r2]
            boolean r1 = r0.m346a(r1)
            if (r1 == 0) goto L_0x00db
            r16.mo109E()
            r1 = 10
            goto L_0x0092
        L_0x00db:
            java.lang.String r1 = "Expected value"
            r0.m352c(r1)
            throw r14
        L_0x00e1:
            r0.f205W = r3
            return r3
        L_0x00e4:
            if (r4 != r3) goto L_0x00ec
            r0.f205W = r13
            return r13
        L_0x00e9:
            r0.f205W = r11
            return r11
        L_0x00ec:
            if (r4 == r3) goto L_0x00f8
            r1 = 2
            if (r4 != r1) goto L_0x00f2
            goto L_0x00f8
        L_0x00f2:
            java.lang.String r1 = "Unexpected value"
            r0.m352c(r1)
            throw r14
        L_0x00f8:
            r16.mo109E()
            int r1 = r0.f201S
            int r1 = r1 - r3
            r0.f201S = r1
            r0.f205W = r12
            return r12
        L_0x0103:
            r16.mo109E()
            r0.f205W = r8
            return r8
        L_0x0109:
            r1 = 9
            goto L_0x0092
        L_0x010c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "JsonReader is closed"
            r1.<init>(r2)
            throw r1
        L_0x0114:
            int[] r1 = r0.f209a0
            int r2 = r0.f210b0
            int r2 = r2 - r3
            r1[r2] = r13
            if (r4 != r15) goto L_0x0135
            int r1 = r0.m348b(r3)
            if (r1 == r10) goto L_0x0135
            if (r1 == r9) goto L_0x0132
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 != r2) goto L_0x012c
        L_0x0129:
            r1 = 2
            goto L_0x0092
        L_0x012c:
            java.lang.String r1 = "Unterminated object"
            r0.m352c(r1)
            throw r14
        L_0x0132:
            r16.mo109E()
        L_0x0135:
            int r1 = r0.m348b(r3)
            if (r1 == r7) goto L_0x016a
            if (r1 == r6) goto L_0x0163
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 == r2) goto L_0x015a
            r16.mo109E()
            int r2 = r0.f201S
            int r2 = r2 - r3
            r0.f201S = r2
            char r1 = (char) r1
            boolean r1 = r0.m346a(r1)
            if (r1 == 0) goto L_0x0154
            r1 = 14
            goto L_0x0092
        L_0x0154:
            java.lang.String r1 = "Expected name"
            r0.m352c(r1)
            throw r14
        L_0x015a:
            if (r4 == r15) goto L_0x015d
            goto L_0x0129
        L_0x015d:
            java.lang.String r1 = "Expected name"
            r0.m352c(r1)
            throw r14
        L_0x0163:
            r16.mo109E()
            r1 = 12
            goto L_0x0092
        L_0x016a:
            r1 = 13
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p008n.C0127a.mo185c():int");
    }

    public void close() {
        this.f205W = 0;
        this.f209a0[0] = 8;
        this.f210b0 = 1;
        this.f198P.close();
    }

    /* renamed from: d */
    public void mo113d() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 4) {
            this.f210b0--;
            int[] iArr = this.f212d0;
            int i2 = this.f210b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            this.f205W = 0;
            return;
        }
        throw new IllegalStateException("Expected END_ARRAY but was " + mo107C() + m338G());
    }

    /* renamed from: e */
    public void mo114e() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 2) {
            this.f210b0--;
            String[] strArr = this.f211c0;
            int i2 = this.f210b0;
            strArr[i2] = null;
            int[] iArr = this.f212d0;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.f205W = 0;
            return;
        }
        throw new IllegalStateException("Expected END_OBJECT but was " + mo107C() + m338G());
    }

    /* renamed from: g */
    public String mo115g() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = this.f210b0;
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.f209a0[i2];
            if (i3 == 1 || i3 == 2) {
                sb.append('[');
                sb.append(this.f212d0[i2]);
                sb.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                sb.append((char) JwtParser.SEPARATOR_CHAR);
                String[] strArr = this.f211c0;
                if (strArr[i2] != null) {
                    sb.append(strArr[i2]);
                }
            }
        }
        return sb.toString();
    }

    /* renamed from: t */
    public boolean mo116t() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        return (i == 2 || i == 4) ? false : true;
    }

    public String toString() {
        return getClass().getSimpleName() + m338G();
    }

    /* renamed from: u */
    public final boolean mo186u() {
        return this.f199Q;
    }

    /* renamed from: v */
    public boolean mo118v() {
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 5) {
            this.f205W = 0;
            int[] iArr = this.f212d0;
            int i2 = this.f210b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.f205W = 0;
            int[] iArr2 = this.f212d0;
            int i3 = this.f210b0 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            throw new IllegalStateException("Expected a boolean but was " + mo107C() + m338G());
        }
    }

    /* renamed from: w */
    public double mo119w() {
        String str;
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 15) {
            this.f205W = 0;
            int[] iArr = this.f212d0;
            int i2 = this.f210b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return (double) this.f206X;
        }
        if (i == 16) {
            this.f208Z = new String(this.f200R, this.f201S, this.f207Y);
            this.f201S += this.f207Y;
        } else {
            if (i == 8 || i == 9) {
                str = m349b(i == 8 ? '\'' : '\"');
            } else if (i == 10) {
                str = m339H();
            } else if (i != 11) {
                throw new IllegalStateException("Expected a double but was " + mo107C() + m338G());
            }
            this.f208Z = str;
        }
        this.f205W = 11;
        double parseDouble = Double.parseDouble(this.f208Z);
        if (this.f199Q || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.f208Z = null;
            this.f205W = 0;
            int[] iArr2 = this.f212d0;
            int i3 = this.f210b0 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        }
        throw new C0131d("JSON forbids NaN and infinities: " + parseDouble + m338G());
    }

    /* renamed from: x */
    public int mo120x() {
        String b;
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 15) {
            long j = this.f206X;
            int i2 = (int) j;
            if (j == ((long) i2)) {
                this.f205W = 0;
                int[] iArr = this.f212d0;
                int i3 = this.f210b0 - 1;
                iArr[i3] = iArr[i3] + 1;
                return i2;
            }
            throw new NumberFormatException("Expected an int but was " + this.f206X + m338G());
        }
        if (i == 16) {
            this.f208Z = new String(this.f200R, this.f201S, this.f207Y);
            this.f201S += this.f207Y;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                b = m339H();
            } else {
                b = m349b(i == 8 ? '\'' : '\"');
            }
            this.f208Z = b;
            try {
                int parseInt = Integer.parseInt(this.f208Z);
                this.f205W = 0;
                int[] iArr2 = this.f212d0;
                int i4 = this.f210b0 - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        } else {
            throw new IllegalStateException("Expected an int but was " + mo107C() + m338G());
        }
        this.f205W = 11;
        double parseDouble = Double.parseDouble(this.f208Z);
        int i5 = (int) parseDouble;
        if (((double) i5) == parseDouble) {
            this.f208Z = null;
            this.f205W = 0;
            int[] iArr3 = this.f212d0;
            int i6 = this.f210b0 - 1;
            iArr3[i6] = iArr3[i6] + 1;
            return i5;
        }
        throw new NumberFormatException("Expected an int but was " + this.f208Z + m338G());
    }

    /* renamed from: y */
    public long mo121y() {
        String b;
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 15) {
            this.f205W = 0;
            int[] iArr = this.f212d0;
            int i2 = this.f210b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.f206X;
        }
        if (i == 16) {
            this.f208Z = new String(this.f200R, this.f201S, this.f207Y);
            this.f201S += this.f207Y;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                b = m339H();
            } else {
                b = m349b(i == 8 ? '\'' : '\"');
            }
            this.f208Z = b;
            try {
                long parseLong = Long.parseLong(this.f208Z);
                this.f205W = 0;
                int[] iArr2 = this.f212d0;
                int i3 = this.f210b0 - 1;
                iArr2[i3] = iArr2[i3] + 1;
                return parseLong;
            } catch (NumberFormatException unused) {
            }
        } else {
            throw new IllegalStateException("Expected a long but was " + mo107C() + m338G());
        }
        this.f205W = 11;
        double parseDouble = Double.parseDouble(this.f208Z);
        long j = (long) parseDouble;
        if (((double) j) == parseDouble) {
            this.f208Z = null;
            this.f205W = 0;
            int[] iArr3 = this.f212d0;
            int i4 = this.f210b0 - 1;
            iArr3[i4] = iArr3[i4] + 1;
            return j;
        }
        throw new NumberFormatException("Expected a long but was " + this.f208Z + m338G());
    }

    /* renamed from: z */
    public String mo122z() {
        String str;
        char c;
        int i = this.f205W;
        if (i == 0) {
            i = mo185c();
        }
        if (i == 14) {
            str = m339H();
        } else {
            if (i == 12) {
                c = '\'';
            } else if (i == 13) {
                c = '\"';
            } else {
                throw new IllegalStateException("Expected a name but was " + mo107C() + m338G());
            }
            str = m349b(c);
        }
        this.f205W = 0;
        this.f211c0[this.f210b0 - 1] = str;
        return str;
    }
}
