package p000a.p001a.p002a.p004j;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.a.a.j.c */
public final class C0025c {

    /* renamed from: a */
    private static final Map<Class<?>, Class<?>> f29a;

    static {
        HashMap hashMap = new HashMap(16);
        HashMap hashMap2 = new HashMap(16);
        m37a(hashMap, hashMap2, Boolean.TYPE, Boolean.class);
        m37a(hashMap, hashMap2, Byte.TYPE, Byte.class);
        m37a(hashMap, hashMap2, Character.TYPE, Character.class);
        m37a(hashMap, hashMap2, Double.TYPE, Double.class);
        m37a(hashMap, hashMap2, Float.TYPE, Float.class);
        m37a(hashMap, hashMap2, Integer.TYPE, Integer.class);
        m37a(hashMap, hashMap2, Long.TYPE, Long.class);
        m37a(hashMap, hashMap2, Short.TYPE, Short.class);
        m37a(hashMap, hashMap2, Void.TYPE, Void.class);
        f29a = Collections.unmodifiableMap(hashMap);
        Collections.unmodifiableMap(hashMap2);
    }

    /* renamed from: a */
    public static <T> Class<T> m36a(Class<T> cls) {
        Map<Class<?>, Class<?>> map = f29a;
        C0100g.m276a(cls);
        Class<T> cls2 = map.get(cls);
        return cls2 == null ? cls : cls2;
    }

    /* renamed from: a */
    private static void m37a(Map<Class<?>, Class<?>> map, Map<Class<?>, Class<?>> map2, Class<?> cls, Class<?> cls2) {
        map.put(cls, cls2);
        map2.put(cls2, cls);
    }

    /* renamed from: a */
    public static boolean m38a(Type type) {
        return f29a.containsKey(type);
    }
}
