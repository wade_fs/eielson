package p000a.p001a.p002a.p007l;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import p000a.p001a.p002a.p004j.C0100g;
import p000a.p001a.p002a.p004j.C0101h;

/* renamed from: a.a.a.l.a */
public class C0125a<T> {

    /* renamed from: a */
    final Class<? super T> f194a;

    /* renamed from: b */
    final Type f195b;

    /* renamed from: c */
    final int f196c;

    protected C0125a() {
        this.f195b = m333b(getClass());
        this.f194a = C0101h.m294e(this.f195b);
        this.f196c = this.f195b.hashCode();
    }

    C0125a(Type type) {
        C0100g.m276a(type);
        this.f195b = C0101h.m289b(type);
        this.f194a = C0101h.m294e(this.f195b);
        this.f196c = this.f195b.hashCode();
    }

    /* renamed from: a */
    public static <T> C0125a<T> m331a(Class<T> cls) {
        return new C0125a<>(cls);
    }

    /* renamed from: a */
    public static C0125a<?> m332a(Type type) {
        return new C0125a<>(type);
    }

    /* renamed from: b */
    static Type m333b(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return C0101h.m289b(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    /* renamed from: a */
    public final Class<? super T> mo179a() {
        return this.f194a;
    }

    /* renamed from: b */
    public final Type mo180b() {
        return this.f195b;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof C0125a) && C0101h.m288a(this.f195b, ((C0125a) obj).f195b);
    }

    public final int hashCode() {
        return this.f196c;
    }

    public final String toString() {
        return C0101h.m297h(this.f195b);
    }
}
