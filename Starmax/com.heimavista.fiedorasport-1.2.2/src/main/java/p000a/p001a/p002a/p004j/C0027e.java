package p000a.p001a.p002a.p004j;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: a.a.a.j.e */
public abstract class C0027e {

    /* renamed from: a.a.a.j.e$a */
    static class C0028a extends C0027e {

        /* renamed from: a */
        final /* synthetic */ Method f30a;

        /* renamed from: b */
        final /* synthetic */ Object f31b;

        C0028a(Method method, Object obj) {
            this.f30a = method;
            this.f31b = obj;
        }

        /* renamed from: a */
        public <T> T mo67a(Class<T> cls) {
            C0027e.m43c(cls);
            return this.f30a.invoke(this.f31b, cls);
        }
    }

    /* renamed from: a.a.a.j.e$b */
    static class C0029b extends C0027e {

        /* renamed from: a */
        final /* synthetic */ Method f32a;

        /* renamed from: b */
        final /* synthetic */ int f33b;

        C0029b(Method method, int i) {
            this.f32a = method;
            this.f33b = i;
        }

        /* renamed from: a */
        public <T> T mo67a(Class<T> cls) {
            C0027e.m43c(cls);
            return this.f32a.invoke(null, cls, Integer.valueOf(this.f33b));
        }
    }

    /* renamed from: a.a.a.j.e$c */
    static class C0030c extends C0027e {

        /* renamed from: a */
        final /* synthetic */ Method f34a;

        C0030c(Method method) {
            this.f34a = method;
        }

        /* renamed from: a */
        public <T> T mo67a(Class<T> cls) {
            C0027e.m43c(cls);
            return this.f34a.invoke(null, cls, Object.class);
        }
    }

    /* renamed from: a.a.a.j.e$d */
    static class C0031d extends C0027e {
        C0031d() {
        }

        /* renamed from: a */
        public <T> T mo67a(Class<T> cls) {
            throw new UnsupportedOperationException("Cannot allocate " + cls);
        }
    }

    /* renamed from: a */
    public static C0027e m41a() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new C0028a(cls.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception unused) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new C0029b(declaredMethod2, intValue);
            } catch (Exception unused2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new C0030c(declaredMethod3);
                } catch (Exception unused3) {
                    return new C0031d();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m43c(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + cls.getName());
        } else if (Modifier.isAbstract(modifiers)) {
            throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + cls.getName());
        }
    }

    /* renamed from: a */
    public abstract <T> T mo67a(Class<T> cls);
}
