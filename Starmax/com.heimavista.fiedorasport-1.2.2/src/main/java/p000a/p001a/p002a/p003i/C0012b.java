package p000a.p001a.p002a.p003i;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.a.a.i.b */
public @interface C0012b {
    boolean nullSafe() default true;

    Class<?> value();
}
