package p000a.p001a.p002a.p008n;

import com.facebook.internal.ServerProtocol;
import com.tencent.bugly.Bugly;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;

/* renamed from: a.a.a.n.c */
public class C0130c implements Closeable, Flushable {

    /* renamed from: Y */
    private static final String[] f224Y = new String[128];

    /* renamed from: Z */
    private static final String[] f225Z;

    /* renamed from: P */
    private final Writer f226P;

    /* renamed from: Q */
    private int[] f227Q = new int[32];

    /* renamed from: R */
    private int f228R = 0;

    /* renamed from: S */
    private String f229S;

    /* renamed from: T */
    private String f230T;

    /* renamed from: U */
    private boolean f231U;

    /* renamed from: V */
    private boolean f232V;

    /* renamed from: W */
    private String f233W;

    /* renamed from: X */
    private boolean f234X;

    static {
        for (int i = 0; i <= 31; i++) {
            f224Y[i] = String.format("\\u%04x", Integer.valueOf(i));
        }
        String[] strArr = f224Y;
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
        f225Z = (String[]) strArr.clone();
        String[] strArr2 = f225Z;
        strArr2[60] = "\\u003c";
        strArr2[62] = "\\u003e";
        strArr2[38] = "\\u0026";
        strArr2[61] = "\\u003d";
        strArr2[39] = "\\u0027";
    }

    public C0130c(Writer writer) {
        m375a(6);
        this.f230T = ":";
        this.f234X = true;
        if (writer != null) {
            this.f226P = writer;
            return;
        }
        throw new NullPointerException("out == null");
    }

    /* renamed from: a */
    private C0130c m373a(int i, int i2, String str) {
        int x = m381x();
        if (x != i2 && x != i) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.f233W == null) {
            this.f228R--;
            if (x == i2) {
                m380w();
            }
            this.f226P.write(str);
            return this;
        } else {
            throw new IllegalStateException("Dangling name: " + this.f233W);
        }
    }

    /* renamed from: a */
    private C0130c m374a(int i, String str) {
        m379v();
        m375a(i);
        this.f226P.write(str);
        return this;
    }

    /* renamed from: a */
    private void m375a(int i) {
        int i2 = this.f228R;
        int[] iArr = this.f227Q;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(i2 * 2)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.f227Q = iArr2;
        }
        int[] iArr3 = this.f227Q;
        int i3 = this.f228R;
        this.f228R = i3 + 1;
        iArr3[i3] = i;
    }

    /* renamed from: b */
    private void m376b(int i) {
        this.f227Q[this.f228R - 1] = i;
    }

    /* renamed from: d */
    private void m377d(String str) {
        String str2;
        String[] strArr = this.f232V ? f225Z : f224Y;
        this.f226P.write("\"");
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 128) {
                str2 = strArr[charAt];
                if (str2 == null) {
                }
            } else if (charAt == 8232) {
                str2 = "\\u2028";
            } else if (charAt == 8233) {
                str2 = "\\u2029";
            }
            if (i < i2) {
                this.f226P.write(str, i, i2 - i);
            }
            this.f226P.write(str2);
            i = i2 + 1;
        }
        if (i < length) {
            this.f226P.write(str, i, length - i);
        }
        this.f226P.write("\"");
    }

    /* renamed from: u */
    private void mo138u() {
        int x = m381x();
        if (x == 5) {
            this.f226P.write(44);
        } else if (x != 3) {
            throw new IllegalStateException("Nesting problem.");
        }
        m380w();
        m376b(4);
    }

    /* renamed from: v */
    private void m379v() {
        int x = m381x();
        if (x == 1) {
            m376b(2);
        } else if (x == 2) {
            this.f226P.append(',');
        } else if (x != 4) {
            if (x != 6) {
                if (x != 7) {
                    throw new IllegalStateException("Nesting problem.");
                } else if (!this.f231U) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
            }
            m376b(7);
            return;
        } else {
            this.f226P.append((CharSequence) this.f230T);
            m376b(5);
            return;
        }
        m380w();
    }

    /* renamed from: w */
    private void m380w() {
        if (this.f229S != null) {
            this.f226P.write("\n");
            int i = this.f228R;
            for (int i2 = 1; i2 < i; i2++) {
                this.f226P.write(this.f229S);
            }
        }
    }

    /* renamed from: x */
    private int m381x() {
        int i = this.f228R;
        if (i != 0) {
            return this.f227Q[i - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    /* renamed from: y */
    private void m382y() {
        if (this.f233W != null) {
            mo138u();
            m377d(this.f233W);
            this.f233W = null;
        }
    }

    /* renamed from: a */
    public C0130c mo125a() {
        m382y();
        m374a(1, "[");
        return this;
    }

    /* renamed from: a */
    public C0130c mo126a(Boolean bool) {
        if (bool == null) {
            return mo137t();
        }
        m382y();
        m379v();
        this.f226P.write(bool.booleanValue() ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : Bugly.SDK_IS_DEV);
        return this;
    }

    /* renamed from: a */
    public C0130c mo127a(Number number) {
        if (number == null) {
            return mo137t();
        }
        m382y();
        String obj = number.toString();
        if (this.f231U || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            m379v();
            this.f226P.append((CharSequence) obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    /* renamed from: a */
    public final void mo187a(boolean z) {
        this.f231U = z;
    }

    /* renamed from: b */
    public C0130c mo128b() {
        m382y();
        m374a(3, "{");
        return this;
    }

    /* renamed from: b */
    public C0130c mo129b(String str) {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (this.f233W != null) {
            throw new IllegalStateException();
        } else if (this.f228R != 0) {
            this.f233W = str;
            return this;
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }

    /* renamed from: b */
    public C0130c mo130b(boolean z) {
        m382y();
        m379v();
        this.f226P.write(z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : Bugly.SDK_IS_DEV);
        return this;
    }

    /* renamed from: c */
    public C0130c mo131c() {
        m373a(1, 2, "]");
        return this;
    }

    /* renamed from: c */
    public C0130c mo132c(String str) {
        if (str == null) {
            return mo137t();
        }
        m382y();
        m379v();
        m377d(str);
        return this;
    }

    public void close() {
        this.f226P.close();
        int i = this.f228R;
        if (i > 1 || (i == 1 && this.f227Q[i - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.f228R = 0;
    }

    /* renamed from: d */
    public C0130c mo134d() {
        m373a(3, 5, "}");
        return this;
    }

    /* renamed from: e */
    public final boolean mo188e() {
        return this.f234X;
    }

    public void flush() {
        if (this.f228R != 0) {
            this.f226P.flush();
            return;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    /* renamed from: g */
    public C0130c mo136g(long j) {
        m382y();
        m379v();
        this.f226P.write(Long.toString(j));
        return this;
    }

    /* renamed from: g */
    public boolean mo189g() {
        return this.f231U;
    }

    /* renamed from: t */
    public C0130c mo137t() {
        if (this.f233W != null) {
            if (this.f234X) {
                m382y();
            } else {
                this.f233W = null;
                return this;
            }
        }
        m379v();
        this.f226P.write("null");
        return this;
    }
}
