package p000a.p001a.p002a.p004j.p005f;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.e */
public final class C0076e extends C0008g<Date> {

    /* renamed from: c */
    public static final C0010h f110c = new C0077a();

    /* renamed from: a */
    private final DateFormat f111a = DateFormat.getDateTimeInstance(2, 2, Locale.US);

    /* renamed from: b */
    private final DateFormat f112b = DateFormat.getDateTimeInstance(2, 2);

    /* renamed from: a.a.a.j.f.e$a */
    static class C0077a implements C0010h {
        C0077a() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() == Date.class) {
                return new C0076e();
            }
            return null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:6|7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        return p000a.p001a.p002a.p004j.p005f.p006b.C0071a.m183a(r3, new java.text.ParsePosition(0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        throw new p000a.p001a.p002a.C0004e(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        return r2.f111a.parse(r3);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0013 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000b */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.Date m193a(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.text.DateFormat r0 = r2.f112b     // Catch:{ ParseException -> 0x000b }
            java.util.Date r3 = r0.parse(r3)     // Catch:{ ParseException -> 0x000b }
            monitor-exit(r2)
            return r3
        L_0x0009:
            r3 = move-exception
            goto L_0x0026
        L_0x000b:
            java.text.DateFormat r0 = r2.f111a     // Catch:{ ParseException -> 0x0013 }
            java.util.Date r3 = r0.parse(r3)     // Catch:{ ParseException -> 0x0013 }
            monitor-exit(r2)
            return r3
        L_0x0013:
            java.text.ParsePosition r0 = new java.text.ParsePosition     // Catch:{ ParseException -> 0x001f }
            r1 = 0
            r0.<init>(r1)     // Catch:{ ParseException -> 0x001f }
            java.util.Date r3 = p000a.p001a.p002a.p004j.p005f.p006b.C0071a.m183a(r3, r0)     // Catch:{ ParseException -> 0x001f }
            monitor-exit(r2)
            return r3
        L_0x001f:
            r0 = move-exception
            a.a.a.e r1 = new a.a.a.e     // Catch:{ all -> 0x0009 }
            r1.<init>(r3, r0)     // Catch:{ all -> 0x0009 }
            throw r1     // Catch:{ all -> 0x0009 }
        L_0x0026:
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.p005f.C0076e.m193a(java.lang.String):java.util.Date");
    }

    /* renamed from: a */
    public Date mo17a(C0127a aVar) {
        if (aVar.mo107C() != C0129b.NULL) {
            return m193a(aVar.mo106B());
        }
        aVar.mo105A();
        return null;
    }

    /* renamed from: a */
    public synchronized void mo18a(C0130c cVar, Date date) {
        if (date == null) {
            cVar.mo137t();
        } else {
            cVar.mo132c(this.f111a.format(date));
        }
    }
}
