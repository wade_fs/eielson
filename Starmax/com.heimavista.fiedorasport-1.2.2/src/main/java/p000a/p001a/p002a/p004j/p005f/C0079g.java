package p000a.p001a.p002a.p004j.p005f;

import java.io.Reader;
import java.util.Iterator;
import java.util.Map;
import p000a.p001a.p002a.C0001b;
import p000a.p001a.p002a.C0148s;
import p000a.p001a.p002a.C0153x;
import p000a.p001a.p002a.C0154y;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: a.a.a.j.f.g */
public final class C0079g extends C0127a {

    /* renamed from: j0 */
    private static final Object f114j0 = new Object();

    /* renamed from: f0 */
    private Object[] f115f0;

    /* renamed from: g0 */
    private int f116g0;

    /* renamed from: h0 */
    private String[] f117h0;

    /* renamed from: i0 */
    private int[] f118i0;

    /* renamed from: a.a.a.j.f.g$a */
    static class C0080a extends Reader {
        C0080a() {
        }

        public void close() {
            throw new AssertionError();
        }

        public int read(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    static {
        new C0080a();
    }

    /* renamed from: F */
    private String m201F() {
        return " at path " + mo115g();
    }

    /* renamed from: G */
    private Object m202G() {
        return this.f115f0[this.f116g0 - 1];
    }

    /* renamed from: H */
    private Object m203H() {
        Object[] objArr = this.f115f0;
        int i = this.f116g0 - 1;
        this.f116g0 = i;
        Object obj = objArr[i];
        objArr[this.f116g0] = null;
        return obj;
    }

    /* renamed from: a */
    private void m204a(C0129b bVar) {
        if (mo107C() != bVar) {
            throw new IllegalStateException("Expected " + bVar + " but was " + mo107C() + m201F());
        }
    }

    /* renamed from: a */
    private void m205a(Object obj) {
        int i = this.f116g0;
        Object[] objArr = this.f115f0;
        if (i == objArr.length) {
            int i2 = i * 2;
            Object[] objArr2 = new Object[i2];
            int[] iArr = new int[i2];
            String[] strArr = new String[i2];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.f118i0, 0, iArr, 0, this.f116g0);
            System.arraycopy(this.f117h0, 0, strArr, 0, this.f116g0);
            this.f115f0 = objArr2;
            this.f118i0 = iArr;
            this.f117h0 = strArr;
        }
        Object[] objArr3 = this.f115f0;
        int i3 = this.f116g0;
        this.f116g0 = i3 + 1;
        objArr3[i3] = obj;
    }

    /* renamed from: A */
    public void mo105A() {
        m204a(C0129b.NULL);
        m203H();
        int i = this.f116g0;
        if (i > 0) {
            int[] iArr = this.f118i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: B */
    public String mo106B() {
        C0129b C = mo107C();
        if (C == C0129b.STRING || C == C0129b.NUMBER) {
            String q = ((C0001b) m203H()).mo10q();
            int i = this.f116g0;
            if (i > 0) {
                int[] iArr = this.f118i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return q;
        }
        throw new IllegalStateException("Expected " + C0129b.STRING + " but was " + C + m201F());
    }

    /* renamed from: C */
    public C0129b mo107C() {
        if (this.f116g0 == 0) {
            return C0129b.END_DOCUMENT;
        }
        Object G = m202G();
        if (G instanceof Iterator) {
            boolean z = this.f115f0[this.f116g0 - 2] instanceof C0154y;
            Iterator it = (Iterator) G;
            if (!it.hasNext()) {
                return z ? C0129b.END_OBJECT : C0129b.END_ARRAY;
            }
            if (z) {
                return C0129b.NAME;
            }
            m205a(it.next());
            return mo107C();
        } else if (G instanceof C0154y) {
            return C0129b.BEGIN_OBJECT;
        } else {
            if (G instanceof C0148s) {
                return C0129b.BEGIN_ARRAY;
            }
            if (G instanceof C0001b) {
                C0001b bVar = (C0001b) G;
                if (bVar.mo13u()) {
                    return C0129b.STRING;
                }
                if (bVar.mo11r()) {
                    return C0129b.BOOLEAN;
                }
                if (bVar.mo12s()) {
                    return C0129b.NUMBER;
                }
                throw new AssertionError();
            } else if (G instanceof C0153x) {
                return C0129b.NULL;
            } else {
                if (G == f114j0) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    /* renamed from: D */
    public void mo108D() {
        if (mo107C() == C0129b.NAME) {
            mo122z();
            this.f117h0[this.f116g0 - 2] = "null";
        } else {
            m203H();
            this.f117h0[this.f116g0 - 1] = "null";
        }
        int[] iArr = this.f118i0;
        int i = this.f116g0 - 1;
        iArr[i] = iArr[i] + 1;
    }

    /* renamed from: E */
    public void mo109E() {
        m204a(C0129b.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m202G()).next();
        m205a(entry.getValue());
        m205a(new C0001b((String) entry.getKey()));
    }

    /* renamed from: a */
    public void mo110a() {
        m204a(C0129b.BEGIN_ARRAY);
        m205a(((C0148s) m202G()).iterator());
        this.f118i0[this.f116g0 - 1] = 0;
    }

    /* renamed from: b */
    public void mo111b() {
        m204a(C0129b.BEGIN_OBJECT);
        m205a(((C0154y) m202G()).mo225k().iterator());
    }

    public void close() {
        this.f115f0 = new Object[]{f114j0};
        this.f116g0 = 1;
    }

    /* renamed from: d */
    public void mo113d() {
        m204a(C0129b.END_ARRAY);
        m203H();
        m203H();
        int i = this.f116g0;
        if (i > 0) {
            int[] iArr = this.f118i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: e */
    public void mo114e() {
        m204a(C0129b.END_OBJECT);
        m203H();
        m203H();
        int i = this.f116g0;
        if (i > 0) {
            int[] iArr = this.f118i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: g */
    public String mo115g() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.f116g0) {
            Object[] objArr = this.f115f0;
            if (objArr[i] instanceof C0148s) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.f118i0[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof C0154y) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append((char) JwtParser.SEPARATOR_CHAR);
                    String[] strArr = this.f117h0;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    /* renamed from: t */
    public boolean mo116t() {
        C0129b C = mo107C();
        return (C == C0129b.END_OBJECT || C == C0129b.END_ARRAY) ? false : true;
    }

    public String toString() {
        return C0079g.class.getSimpleName();
    }

    /* renamed from: v */
    public boolean mo118v() {
        m204a(C0129b.BOOLEAN);
        boolean k = ((C0001b) m203H()).mo4k();
        int i = this.f116g0;
        if (i > 0) {
            int[] iArr = this.f118i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return k;
    }

    /* renamed from: w */
    public double mo119w() {
        C0129b C = mo107C();
        if (C == C0129b.NUMBER || C == C0129b.STRING) {
            double m = ((C0001b) m202G()).mo6m();
            if (mo186u() || (!Double.isNaN(m) && !Double.isInfinite(m))) {
                m203H();
                int i = this.f116g0;
                if (i > 0) {
                    int[] iArr = this.f118i0;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return m;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + m);
        }
        throw new IllegalStateException("Expected " + C0129b.NUMBER + " but was " + C + m201F());
    }

    /* renamed from: x */
    public int mo120x() {
        C0129b C = mo107C();
        if (C == C0129b.NUMBER || C == C0129b.STRING) {
            int n = ((C0001b) m202G()).mo7n();
            m203H();
            int i = this.f116g0;
            if (i > 0) {
                int[] iArr = this.f118i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return n;
        }
        throw new IllegalStateException("Expected " + C0129b.NUMBER + " but was " + C + m201F());
    }

    /* renamed from: y */
    public long mo121y() {
        C0129b C = mo107C();
        if (C == C0129b.NUMBER || C == C0129b.STRING) {
            long o = ((C0001b) m202G()).mo8o();
            m203H();
            int i = this.f116g0;
            if (i > 0) {
                int[] iArr = this.f118i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return o;
        }
        throw new IllegalStateException("Expected " + C0129b.NUMBER + " but was " + C + m201F());
    }

    /* renamed from: z */
    public String mo122z() {
        m204a(C0129b.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m202G()).next();
        String str = (String) entry.getKey();
        this.f117h0[this.f116g0 - 1] = str;
        m205a(entry.getValue());
        return str;
    }
}
