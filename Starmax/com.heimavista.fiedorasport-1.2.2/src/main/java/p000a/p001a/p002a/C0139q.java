package p000a.p001a.p002a;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;
import p000a.p001a.p002a.p004j.C0025c;
import p000a.p001a.p002a.p004j.C0105i;
import p000a.p001a.p002a.p004j.C0120j;
import p000a.p001a.p002a.p004j.p005f.C0032a;
import p000a.p001a.p002a.p004j.p005f.C0072c;
import p000a.p001a.p002a.p004j.p005f.C0074d;
import p000a.p001a.p002a.p004j.p005f.C0076e;
import p000a.p001a.p002a.p004j.p005f.C0078f;
import p000a.p001a.p002a.p004j.p005f.C0083i;
import p000a.p001a.p002a.p004j.p005f.C0085j;
import p000a.p001a.p002a.p004j.p005f.C0088k;
import p000a.p001a.p002a.p004j.p005f.C0092l;
import p000a.p001a.p002a.p004j.p005f.C0094m;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;
import p000a.p001a.p002a.p008n.C0131d;

/* renamed from: a.a.a.q */
public final class C0139q {

    /* renamed from: h */
    private static final C0125a<?> f241h = new C0140a();

    /* renamed from: a */
    private final ThreadLocal<Map<C0125a<?>, C0146g<?>>> f242a;

    /* renamed from: b */
    private final Map<C0125a<?>, C0008g<?>> f243b;

    /* renamed from: c */
    private final List<C0010h> f244c;

    /* renamed from: d */
    private final C0105i f245d;

    /* renamed from: e */
    private final boolean f246e;

    /* renamed from: f */
    private final boolean f247f;

    /* renamed from: g */
    private final C0078f f248g;

    /* renamed from: a.a.a.q$a */
    static class C0140a extends C0125a<Object> {
        C0140a() {
        }
    }

    /* renamed from: a.a.a.q$b */
    class C0141b extends C0008g<Number> {
        C0141b(C0139q qVar) {
        }

        /* renamed from: a */
        public Double m422a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Double.valueOf(aVar.mo119w());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            if (number == null) {
                cVar.mo137t();
                return;
            }
            C0139q.m409a(number.doubleValue());
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.q$c */
    class C0142c extends C0008g<Number> {
        C0142c(C0139q qVar) {
        }

        /* renamed from: a */
        public Float m426a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Float.valueOf((float) aVar.mo119w());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            if (number == null) {
                cVar.mo137t();
                return;
            }
            C0139q.m409a((double) number.floatValue());
            cVar.mo127a(number);
        }
    }

    /* renamed from: a.a.a.q$d */
    static class C0143d extends C0008g<Number> {
        C0143d() {
        }

        /* renamed from: a */
        public Number m430a(C0127a aVar) {
            if (aVar.mo107C() != C0129b.NULL) {
                return Long.valueOf(aVar.mo121y());
            }
            aVar.mo105A();
            return null;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Number number) {
            if (number == null) {
                cVar.mo137t();
            } else {
                cVar.mo132c(number.toString());
            }
        }
    }

    /* renamed from: a.a.a.q$e */
    static class C0144e extends C0008g<AtomicLong> {

        /* renamed from: a */
        final /* synthetic */ C0008g f249a;

        C0144e(C0008g gVar) {
            this.f249a = super;
        }

        /* renamed from: a */
        public AtomicLong mo17a(C0127a aVar) {
            return new AtomicLong(((Number) this.f249a.mo17a(aVar)).longValue());
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, AtomicLong atomicLong) {
            this.f249a.mo18a(cVar, Long.valueOf(atomicLong.get()));
        }
    }

    /* renamed from: a.a.a.q$f */
    static class C0145f extends C0008g<AtomicLongArray> {

        /* renamed from: a */
        final /* synthetic */ C0008g f250a;

        C0145f(C0008g gVar) {
            this.f250a = super;
        }

        /* renamed from: a */
        public AtomicLongArray mo17a(C0127a aVar) {
            ArrayList arrayList = new ArrayList();
            aVar.mo110a();
            while (aVar.mo116t()) {
                arrayList.add(Long.valueOf(((Number) this.f250a.mo17a(aVar)).longValue()));
            }
            aVar.mo113d();
            int size = arrayList.size();
            AtomicLongArray atomicLongArray = new AtomicLongArray(size);
            for (int i = 0; i < size; i++) {
                atomicLongArray.set(i, ((Long) arrayList.get(i)).longValue());
            }
            return atomicLongArray;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, AtomicLongArray atomicLongArray) {
            cVar.mo125a();
            int length = atomicLongArray.length();
            for (int i = 0; i < length; i++) {
                this.f250a.mo18a(cVar, Long.valueOf(atomicLongArray.get(i)));
            }
            cVar.mo131c();
        }
    }

    /* renamed from: a.a.a.q$g */
    static class C0146g<T> extends C0008g<T> {

        /* renamed from: a */
        private C0008g<T> f251a;

        C0146g() {
        }

        /* renamed from: a */
        public T mo17a(C0127a aVar) {
            C0008g<T> gVar = this.f251a;
            if (gVar != null) {
                return super.mo17a(aVar);
            }
            throw new IllegalStateException();
        }

        /* renamed from: a */
        public void mo205a(C0008g<T> gVar) {
            if (this.f251a == null) {
                this.f251a = super;
                return;
            }
            throw new AssertionError();
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, T t) {
            C0008g<T> gVar = this.f251a;
            if (gVar != null) {
                super.mo18a(cVar, t);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public C0139q() {
        this(C0120j.f179V, C0132o.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, false, C0005f.DEFAULT, Collections.emptyList());
    }

    C0139q(C0120j jVar, C0138p pVar, Map<Type, C0147r<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, C0005f fVar, List<C0010h> list) {
        this.f242a = new ThreadLocal<>();
        this.f243b = new ConcurrentHashMap();
        this.f245d = new C0105i(map);
        this.f246e = z;
        this.f247f = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(C0032a.f59Y);
        arrayList.add(C0085j.f130b);
        arrayList.add(jVar);
        arrayList.addAll(list);
        arrayList.add(C0032a.f38D);
        arrayList.add(C0032a.f73m);
        arrayList.add(C0032a.f67g);
        arrayList.add(C0032a.f69i);
        arrayList.add(C0032a.f71k);
        C0008g<Number> a = m406a(fVar);
        arrayList.add(C0032a.m50a(Long.TYPE, Long.class, a));
        arrayList.add(C0032a.m50a(Double.TYPE, Double.class, m408a(z7)));
        arrayList.add(C0032a.m50a(Float.TYPE, Float.class, m412b(z7)));
        arrayList.add(C0032a.f84x);
        arrayList.add(C0032a.f75o);
        arrayList.add(C0032a.f77q);
        arrayList.add(C0032a.m49a(AtomicLong.class, m407a(a)));
        arrayList.add(C0032a.m49a(AtomicLongArray.class, m411b(a)));
        arrayList.add(C0032a.f79s);
        arrayList.add(C0032a.f86z);
        arrayList.add(C0032a.f40F);
        arrayList.add(C0032a.f42H);
        arrayList.add(C0032a.m49a(BigDecimal.class, C0032a.f36B));
        arrayList.add(C0032a.m49a(BigInteger.class, C0032a.f37C));
        arrayList.add(C0032a.f44J);
        arrayList.add(C0032a.f46L);
        arrayList.add(C0032a.f50P);
        arrayList.add(C0032a.f52R);
        arrayList.add(C0032a.f57W);
        arrayList.add(C0032a.f48N);
        arrayList.add(C0032a.f64d);
        arrayList.add(C0076e.f110c);
        arrayList.add(C0032a.f55U);
        arrayList.add(C0094m.f150b);
        arrayList.add(C0092l.f148b);
        arrayList.add(C0032a.f53S);
        arrayList.add(C0072c.f104c);
        arrayList.add(C0032a.f62b);
        arrayList.add(new C0074d(this.f245d));
        arrayList.add(new C0083i(this.f245d, z2));
        this.f248g = new C0078f(this.f245d);
        arrayList.add(this.f248g);
        arrayList.add(C0032a.f60Z);
        arrayList.add(new C0088k(this.f245d, pVar, jVar, this.f248g));
        this.f244c = Collections.unmodifiableList(arrayList);
    }

    /* renamed from: a */
    private static C0008g<Number> m406a(C0005f fVar) {
        return fVar == C0005f.DEFAULT ? C0032a.f80t : new C0143d();
    }

    /* renamed from: a */
    private static C0008g<AtomicLong> m407a(C0008g<Number> gVar) {
        return new C0144e(gVar).mo15a();
    }

    /* renamed from: a */
    private C0008g<Number> m408a(boolean z) {
        return z ? C0032a.f82v : new C0141b(this);
    }

    /* renamed from: a */
    static void m409a(double d) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            throw new IllegalArgumentException(d + " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    /* renamed from: a */
    private static void m410a(Object obj, C0127a aVar) {
        if (obj != null) {
            try {
                if (aVar.mo107C() != C0129b.END_DOCUMENT) {
                    throw new C0152w("JSON document was not fully consumed.");
                }
            } catch (C0131d e) {
                throw new C0004e(e);
            } catch (IOException e2) {
                throw new C0152w(e2);
            }
        }
    }

    /* renamed from: b */
    private static C0008g<AtomicLongArray> m411b(C0008g<Number> gVar) {
        return new C0145f(gVar).mo15a();
    }

    /* renamed from: b */
    private C0008g<Number> m412b(boolean z) {
        return z ? C0032a.f81u : new C0142c(this);
    }

    /* renamed from: a */
    public <T> C0008g<T> mo191a(C0010h hVar, C0125a<T> aVar) {
        if (!this.f244c.contains(hVar)) {
            hVar = this.f248g;
        }
        boolean z = false;
        for (C0010h hVar2 : this.f244c) {
            if (z) {
                C0008g<T> a = hVar2.mo19a(this, aVar);
                if (a != null) {
                    return a;
                }
            } else if (hVar2 == hVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    /* renamed from: a */
    public <T> C0008g<T> mo192a(C0125a aVar) {
        C0008g<T> gVar = this.f243b.get(aVar == null ? f241h : aVar);
        if (gVar != null) {
            return gVar;
        }
        Map map = this.f242a.get();
        boolean z = false;
        if (map == null) {
            map = new HashMap();
            this.f242a.set(map);
            z = true;
        }
        C0146g gVar2 = (C0146g) map.get(aVar);
        if (gVar2 != null) {
            return gVar2;
        }
        try {
            C0146g gVar3 = new C0146g();
            map.put(aVar, gVar3);
            for (C0010h hVar : this.f244c) {
                C0008g<T> a = hVar.mo19a(this, aVar);
                if (a != null) {
                    gVar3.mo205a((C0008g) a);
                    this.f243b.put(aVar, a);
                    return a;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("GSON cannot handle ");
            sb.append(aVar);
            throw new IllegalArgumentException(sb.toString());
        } finally {
            map.remove(aVar);
            if (z) {
                this.f242a.remove();
            }
        }
    }

    /* renamed from: a */
    public <T> C0008g<T> mo193a(Class cls) {
        return mo192a(C0125a.m331a(cls));
    }

    /* renamed from: a */
    public C0127a mo194a(Reader reader) {
        C0127a aVar = new C0127a(reader);
        aVar.mo184a(this.f247f);
        return aVar;
    }

    /* renamed from: a */
    public <T> T mo195a(C0127a aVar, Type type) {
        boolean u = aVar.mo186u();
        aVar.mo184a(true);
        try {
            aVar.mo107C();
            T a = mo192a(C0125a.m332a(type)).mo17a(aVar);
            aVar.mo184a(u);
            return a;
        } catch (EOFException e) {
            if (1 != 0) {
                aVar.mo184a(u);
                return null;
            }
            throw new C0004e(e);
        } catch (IllegalStateException e2) {
            throw new C0004e(e2);
        } catch (IOException e3) {
            throw new C0004e(e3);
        } catch (Throwable th) {
            aVar.mo184a(u);
            throw th;
        }
    }

    /* renamed from: a */
    public <T> T mo196a(Reader reader, Type type) {
        C0127a a = mo194a(reader);
        T a2 = mo195a(a, type);
        m410a(a2, a);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.q.a(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      a.a.a.q.a(java.lang.Object, a.a.a.n.a):void
      a.a.a.q.a(a.a.a.h, a.a.a.l.a):a.a.a.g<T>
      a.a.a.q.a(a.a.a.n.a, java.lang.reflect.Type):T
      a.a.a.q.a(java.io.Reader, java.lang.reflect.Type):T
      a.a.a.q.a(java.lang.String, java.lang.Class):T
      a.a.a.q.a(java.lang.String, java.lang.reflect.Type):T */
    /* renamed from: a */
    public <T> T mo197a(String str, Class<T> cls) {
        return C0025c.m36a((Class) cls).cast(mo198a(str, (Type) cls));
    }

    /* renamed from: a */
    public <T> T mo198a(String str, Type type) {
        if (str == null) {
            return null;
        }
        return mo196a(new StringReader(str), type);
    }

    public String toString() {
        return "{serializeNulls:" + this.f246e + "factories:" + this.f244c + ",instanceCreators:" + this.f245d + "}";
    }
}
