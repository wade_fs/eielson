package p000a.p001a.p002a.p004j.p005f;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import p000a.p001a.p002a.C0004e;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.l */
public final class C0092l extends C0008g<Date> {

    /* renamed from: b */
    public static final C0010h f148b = new C0093a();

    /* renamed from: a */
    private final DateFormat f149a = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a.a.a.j.f.l$a */
    static class C0093a implements C0010h {
        C0093a() {
        }

        /* renamed from: a */
        public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
            if (aVar.mo179a() == Date.class) {
                return new C0092l();
            }
            return null;
        }
    }

    /* renamed from: a */
    public synchronized Date mo17a(C0127a aVar) {
        if (aVar.mo107C() == C0129b.NULL) {
            aVar.mo105A();
            return null;
        }
        try {
            return new Date(this.f149a.parse(aVar.mo106B()).getTime());
        } catch (ParseException e) {
            throw new C0004e(e);
        }
    }

    /* renamed from: a */
    public synchronized void mo18a(C0130c cVar, Date date) {
        cVar.mo132c(date == null ? null : this.f149a.format(date));
    }
}
