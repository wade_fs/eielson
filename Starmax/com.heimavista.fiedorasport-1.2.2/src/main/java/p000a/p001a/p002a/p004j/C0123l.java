package p000a.p001a.p002a.p004j;

import java.math.BigDecimal;

/* renamed from: a.a.a.j.l */
public final class C0123l extends Number {

    /* renamed from: P */
    private final String f193P;

    public C0123l(String str) {
        this.f193P = str;
    }

    public double doubleValue() {
        return Double.parseDouble(this.f193P);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0123l)) {
            return false;
        }
        String str = this.f193P;
        String str2 = ((C0123l) obj).f193P;
        return str == str2 || str.equals(str2);
    }

    public float floatValue() {
        return Float.parseFloat(this.f193P);
    }

    public int hashCode() {
        return this.f193P.hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        return (int) java.lang.Long.parseLong(r2.f193P);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return new java.math.BigDecimal(r2.f193P).intValue();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0007 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int intValue() {
        /*
            r2 = this;
            java.lang.String r0 = r2.f193P     // Catch:{ NumberFormatException -> 0x0007 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0007 }
            return r0
        L_0x0007:
            java.lang.String r0 = r2.f193P     // Catch:{ NumberFormatException -> 0x000f }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x000f }
            int r1 = (int) r0
            return r1
        L_0x000f:
            java.math.BigDecimal r0 = new java.math.BigDecimal
            java.lang.String r1 = r2.f193P
            r0.<init>(r1)
            int r0 = r0.intValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p000a.p001a.p002a.p004j.C0123l.intValue():int");
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f193P);
        } catch (NumberFormatException unused) {
            return new BigDecimal(this.f193P).longValue();
        }
    }

    public String toString() {
        return this.f193P;
    }
}
