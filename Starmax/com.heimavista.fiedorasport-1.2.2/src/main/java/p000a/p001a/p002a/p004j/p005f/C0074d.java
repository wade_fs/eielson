package p000a.p001a.p002a.p004j.p005f;

import java.lang.reflect.Type;
import java.util.Collection;
import p000a.p001a.p002a.C0008g;
import p000a.p001a.p002a.C0010h;
import p000a.p001a.p002a.C0139q;
import p000a.p001a.p002a.p004j.C0024b;
import p000a.p001a.p002a.p004j.C0101h;
import p000a.p001a.p002a.p004j.C0105i;
import p000a.p001a.p002a.p007l.C0125a;
import p000a.p001a.p002a.p008n.C0127a;
import p000a.p001a.p002a.p008n.C0129b;
import p000a.p001a.p002a.p008n.C0130c;

/* renamed from: a.a.a.j.f.d */
public final class C0074d implements C0010h {

    /* renamed from: P */
    private final C0105i f107P;

    /* renamed from: a.a.a.j.f.d$a */
    private static final class C0075a<E> extends C0008g<Collection<E>> {

        /* renamed from: a */
        private final C0008g<E> f108a;

        /* renamed from: b */
        private final C0024b<? extends Collection<E>> f109b;

        public C0075a(C0139q qVar, Type type, C0008g<E> gVar, C0024b<? extends Collection<E>> bVar) {
            this.f108a = new C0099o(qVar, super, type);
            this.f109b = bVar;
        }

        /* renamed from: a */
        public Collection<E> mo17a(C0127a aVar) {
            if (aVar.mo107C() == C0129b.NULL) {
                aVar.mo105A();
                return null;
            }
            Collection<E> collection = (Collection) this.f109b.mo66a();
            aVar.mo110a();
            while (aVar.mo116t()) {
                collection.add(this.f108a.mo17a(aVar));
            }
            aVar.mo113d();
            return collection;
        }

        /* renamed from: a */
        public void mo18a(C0130c cVar, Collection<E> collection) {
            if (collection == null) {
                cVar.mo137t();
                return;
            }
            cVar.mo125a();
            for (E e : collection) {
                this.f108a.mo18a(cVar, e);
            }
            cVar.mo131c();
        }
    }

    public C0074d(C0105i iVar) {
        this.f107P = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super T>]
     candidates:
      a.a.a.j.h.a(java.lang.Object[], java.lang.Object):int
      a.a.a.j.h.a(java.lang.Object, java.lang.Object):boolean
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      a.a.a.j.h.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type */
    /* renamed from: a */
    public <T> C0008g<T> mo19a(C0139q qVar, C0125a<T> aVar) {
        Type b = aVar.mo180b();
        Class<? super T> a = aVar.mo179a();
        if (!Collection.class.isAssignableFrom(a)) {
            return null;
        }
        Type a2 = C0101h.m283a(b, (Class<?>) a);
        return new C0075a(qVar, a2, qVar.mo192a(C0125a.m332a(a2)), this.f107P.mo164a(aVar));
    }
}
