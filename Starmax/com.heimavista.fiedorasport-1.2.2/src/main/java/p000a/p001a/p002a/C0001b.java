package p000a.p001a.p002a;

import java.math.BigInteger;
import p000a.p001a.p002a.p004j.C0100g;
import p000a.p001a.p002a.p004j.C0123l;

/* renamed from: a.a.a.b */
public final class C0001b extends C0151v {

    /* renamed from: b */
    private static final Class<?>[] f0b = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: a */
    private Object f1a;

    public C0001b(Boolean bool) {
        mo1a(bool);
    }

    public C0001b(Number number) {
        mo1a(number);
    }

    public C0001b(String str) {
        mo1a(str);
    }

    /* renamed from: a */
    private static boolean m0a(C0001b bVar) {
        Object obj = bVar.f1a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    /* renamed from: b */
    private static boolean m1b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> cls2 : f0b) {
            if (cls2.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo1a(Object obj) {
        if (obj instanceof Character) {
            obj = String.valueOf(((Character) obj).charValue());
        } else {
            C0100g.m277a((obj instanceof Number) || m1b(obj));
        }
        this.f1a = obj;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C0001b.class != obj.getClass()) {
            return false;
        }
        C0001b bVar = (C0001b) obj;
        if (this.f1a == null) {
            return bVar.f1a == null;
        }
        if (m0a(this) && m0a(bVar)) {
            return mo9p().longValue() == bVar.mo9p().longValue();
        }
        if (!(this.f1a instanceof Number) || !(bVar.f1a instanceof Number)) {
            return this.f1a.equals(bVar.f1a);
        }
        double doubleValue = mo9p().doubleValue();
        double doubleValue2 = bVar.mo9p().doubleValue();
        if (doubleValue != doubleValue2) {
            return Double.isNaN(doubleValue) && Double.isNaN(doubleValue2);
        }
        return true;
    }

    public int hashCode() {
        long doubleToLongBits;
        if (this.f1a == null) {
            return 31;
        }
        if (m0a(this)) {
            doubleToLongBits = mo9p().longValue();
        } else {
            Object obj = this.f1a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(mo9p().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    /* renamed from: k */
    public boolean mo4k() {
        return mo11r() ? mo5l().booleanValue() : Boolean.parseBoolean(mo10q());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public Boolean mo5l() {
        return (Boolean) this.f1a;
    }

    /* renamed from: m */
    public double mo6m() {
        return mo12s() ? mo9p().doubleValue() : Double.parseDouble(mo10q());
    }

    /* renamed from: n */
    public int mo7n() {
        return mo12s() ? mo9p().intValue() : Integer.parseInt(mo10q());
    }

    /* renamed from: o */
    public long mo8o() {
        return mo12s() ? mo9p().longValue() : Long.parseLong(mo10q());
    }

    /* renamed from: p */
    public Number mo9p() {
        Object obj = this.f1a;
        return obj instanceof String ? new C0123l((String) obj) : (Number) obj;
    }

    /* renamed from: q */
    public String mo10q() {
        return mo12s() ? mo9p().toString() : mo11r() ? mo5l().toString() : (String) this.f1a;
    }

    /* renamed from: r */
    public boolean mo11r() {
        return this.f1a instanceof Boolean;
    }

    /* renamed from: s */
    public boolean mo12s() {
        return this.f1a instanceof Number;
    }

    /* renamed from: u */
    public boolean mo13u() {
        return this.f1a instanceof String;
    }
}
