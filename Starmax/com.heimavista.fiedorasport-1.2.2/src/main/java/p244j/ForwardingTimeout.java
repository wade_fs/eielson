package p244j;

import java.util.concurrent.TimeUnit;

/* renamed from: j.i */
public class ForwardingTimeout extends Timeout {

    /* renamed from: e */
    private Timeout f11882e;

    public ForwardingTimeout(Timeout tVar) {
        if (tVar != null) {
            this.f11882e = super;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    /* renamed from: a */
    public final ForwardingTimeout mo28112a(Timeout tVar) {
        if (tVar != null) {
            this.f11882e = super;
            return this;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    /* renamed from: b */
    public Timeout mo28116b() {
        return this.f11882e.mo28116b();
    }

    /* renamed from: c */
    public long mo28117c() {
        return this.f11882e.mo28117c();
    }

    /* renamed from: d */
    public boolean mo28118d() {
        return this.f11882e.mo28118d();
    }

    /* renamed from: e */
    public void mo28119e() {
        this.f11882e.mo28119e();
    }

    /* renamed from: g */
    public final Timeout mo28120g() {
        return this.f11882e;
    }

    /* renamed from: a */
    public Timeout mo28115a(long j, TimeUnit timeUnit) {
        return this.f11882e.mo28115a(j, timeUnit);
    }

    /* renamed from: a */
    public Timeout mo28114a(long j) {
        return this.f11882e.mo28114a(j);
    }

    /* renamed from: a */
    public Timeout mo28113a() {
        return this.f11882e.mo28113a();
    }
}
