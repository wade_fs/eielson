package p244j;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: j.n */
final class RealBufferedSource implements BufferedSource {

    /* renamed from: P */
    public final Buffer f11901P = new Buffer();

    /* renamed from: Q */
    public final C5049s f11902Q;

    /* renamed from: R */
    boolean f11903R;

    RealBufferedSource(C5049s sVar) {
        if (sVar != null) {
            this.f11902Q = sVar;
            return;
        }
        throw new NullPointerException("source == null");
    }

    /* renamed from: a */
    public ByteString mo28037a(long j) {
        mo28052e(j);
        return this.f11901P.mo28037a(j);
    }

    /* renamed from: b */
    public long mo27415b(Buffer cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.f11903R) {
            Buffer cVar2 = this.f11901P;
            if (cVar2.f11873Q == 0 && this.f11902Q.mo27415b(cVar2, 8192) == -1) {
                return -1;
            }
            return this.f11901P.mo27415b(cVar, Math.min(j, this.f11901P.f11873Q));
        } else {
            throw new IllegalStateException("closed");
        }
    }

    /* renamed from: c */
    public boolean mo28129c(long j) {
        Buffer cVar;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.f11903R) {
            do {
                cVar = this.f11901P;
                if (cVar.f11873Q >= j) {
                    return true;
                }
            } while (this.f11902Q.mo27415b(cVar, 8192) != -1);
            return false;
        } else {
            throw new IllegalStateException("closed");
        }
    }

    public void close() {
        if (!this.f11903R) {
            this.f11903R = true;
            this.f11902Q.close();
            this.f11901P.mo28039a();
        }
    }

    /* renamed from: d */
    public byte[] mo28050d(long j) {
        mo28052e(j);
        return this.f11901P.mo28050d(j);
    }

    /* renamed from: e */
    public void mo28052e(long j) {
        if (!mo28129c(j)) {
            throw new EOFException();
        }
    }

    /* renamed from: f */
    public Buffer mo28054f() {
        return this.f11901P;
    }

    /* renamed from: h */
    public Timeout mo27416h() {
        return this.f11902Q.mo27416h();
    }

    /* renamed from: i */
    public byte[] mo28061i() {
        this.f11901P.mo28030a(this.f11902Q);
        return this.f11901P.mo28061i();
    }

    public boolean isOpen() {
        return !this.f11903R;
    }

    /* renamed from: j */
    public boolean mo28063j() {
        if (!this.f11903R) {
            return this.f11901P.mo28063j() && this.f11902Q.mo27415b(this.f11901P, 8192) == -1;
        }
        throw new IllegalStateException("closed");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    /* renamed from: l */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo28065l() {
        /*
            r6 = this;
            r0 = 1
            r6.mo28052e(r0)
            r0 = 0
            r1 = 0
        L_0x0007:
            int r2 = r1 + 1
            long r3 = (long) r2
            boolean r3 = r6.mo28129c(r3)
            if (r3 == 0) goto L_0x0040
            j.c r3 = r6.f11901P
            long r4 = (long) r1
            byte r3 = r3.mo28056g(r4)
            r4 = 48
            if (r3 < r4) goto L_0x001f
            r4 = 57
            if (r3 <= r4) goto L_0x0026
        L_0x001f:
            if (r1 != 0) goto L_0x0028
            r4 = 45
            if (r3 == r4) goto L_0x0026
            goto L_0x0028
        L_0x0026:
            r1 = r2
            goto L_0x0007
        L_0x0028:
            if (r1 == 0) goto L_0x002b
            goto L_0x0040
        L_0x002b:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            r2[r0] = r3
            java.lang.String r0 = "Expected leading [0-9] or '-' character but was %#x"
            java.lang.String r0 = java.lang.String.format(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x0040:
            j.c r0 = r6.f11901P
            long r0 = r0.mo28065l()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p244j.RealBufferedSource.mo28065l():long");
    }

    /* renamed from: m */
    public String mo28066m() {
        return mo28044b(Long.MAX_VALUE);
    }

    /* renamed from: n */
    public int mo28067n() {
        mo28052e(4);
        return this.f11901P.mo28067n();
    }

    /* renamed from: o */
    public short mo28068o() {
        mo28052e(2);
        return this.f11901P.mo28068o();
    }

    /* renamed from: p */
    public long mo28069p() {
        mo28052e(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!mo28129c((long) i2)) {
                break;
            }
            byte g = this.f11901P.mo28056g((long) i);
            if ((g >= 48 && g <= 57) || ((g >= 97 && g <= 102) || (g >= 65 && g <= 70))) {
                i = i2;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", Byte.valueOf(g)));
            }
        }
        return this.f11901P.mo28069p();
    }

    /* renamed from: q */
    public InputStream mo28070q() {
        return new C5048a();
    }

    public int read(ByteBuffer byteBuffer) {
        Buffer cVar = this.f11901P;
        if (cVar.f11873Q == 0 && this.f11902Q.mo27415b(cVar, 8192) == -1) {
            return -1;
        }
        return this.f11901P.read(byteBuffer);
    }

    public byte readByte() {
        mo28052e(1);
        return this.f11901P.readByte();
    }

    public void readFully(byte[] bArr) {
        try {
            mo28052e((long) bArr.length);
            this.f11901P.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (true) {
                Buffer cVar = this.f11901P;
                long j = cVar.f11873Q;
                if (j > 0) {
                    int a = cVar.mo28027a(bArr, i, (int) j);
                    if (a != -1) {
                        i += a;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw e;
                }
            }
        }
    }

    public int readInt() {
        mo28052e(4);
        return this.f11901P.readInt();
    }

    public short readShort() {
        mo28052e(2);
        return this.f11901P.readShort();
    }

    public void skip(long j) {
        if (!this.f11903R) {
            while (j > 0) {
                Buffer cVar = this.f11901P;
                if (cVar.f11873Q == 0 && this.f11902Q.mo27415b(cVar, 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.f11901P.mo28051e());
                this.f11901P.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed");
    }

    public String toString() {
        return "buffer(" + this.f11902Q + ")";
    }

    /* renamed from: a */
    public long mo28028a(byte b) {
        return mo28127a(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: a */
    public long mo28127a(byte b, long j, long j2) {
        if (this.f11903R) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", Long.valueOf(j), Long.valueOf(j2)));
        } else {
            while (j < j2) {
                long a = this.f11901P.mo28029a(b, j, j2);
                if (a == -1) {
                    Buffer cVar = this.f11901P;
                    long j3 = cVar.f11873Q;
                    if (j3 >= j2 || this.f11902Q.mo27415b(cVar, 8192) == -1) {
                        break;
                    }
                    j = Math.max(j, j3);
                } else {
                    return a;
                }
            }
            return -1;
        }
    }

    /* renamed from: j.n$a */
    /* compiled from: RealBufferedSource */
    class C5048a extends InputStream {
        C5048a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public int available() {
            RealBufferedSource nVar = RealBufferedSource.this;
            if (!nVar.f11903R) {
                return (int) Math.min(nVar.f11901P.f11873Q, 2147483647L);
            }
            throw new IOException("closed");
        }

        public void close() {
            RealBufferedSource.this.close();
        }

        public int read() {
            RealBufferedSource nVar = RealBufferedSource.this;
            if (!nVar.f11903R) {
                Buffer cVar = nVar.f11901P;
                if (cVar.f11873Q == 0 && nVar.f11902Q.mo27415b(cVar, 8192) == -1) {
                    return -1;
                }
                return RealBufferedSource.this.f11901P.readByte() & 255;
            }
            throw new IOException("closed");
        }

        public String toString() {
            return RealBufferedSource.this + ".inputStream()";
        }

        public int read(byte[] bArr, int i, int i2) {
            if (!RealBufferedSource.this.f11903R) {
                C5051u.m18301a((long) bArr.length, (long) i, (long) i2);
                RealBufferedSource nVar = RealBufferedSource.this;
                Buffer cVar = nVar.f11901P;
                if (cVar.f11873Q == 0 && nVar.f11902Q.mo27415b(cVar, 8192) == -1) {
                    return -1;
                }
                return RealBufferedSource.this.f11901P.mo28027a(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* renamed from: b */
    public String mo28044b(long j) {
        if (j >= 0) {
            long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
            long a = mo28127a((byte) 10, 0, j2);
            if (a != -1) {
                return this.f11901P.mo28060i(a);
            }
            if (j2 < Long.MAX_VALUE && mo28129c(j2) && this.f11901P.mo28056g(j2 - 1) == 13 && mo28129c(1 + j2) && this.f11901P.mo28056g(j2) == 10) {
                return this.f11901P.mo28060i(j2);
            }
            Buffer cVar = new Buffer();
            Buffer cVar2 = this.f11901P;
            cVar2.mo28031a(cVar, 0, Math.min(32L, cVar2.mo28051e()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.f11901P.mo28051e(), j) + " content=" + cVar.mo28047c().mo28099h() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    /* renamed from: a */
    public boolean mo28040a(long j, ByteString fVar) {
        return mo28128a(j, fVar, 0, fVar.mo28104l());
    }

    /* renamed from: a */
    public boolean mo28128a(long j, ByteString fVar, int i, int i2) {
        if (this.f11903R) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || fVar.mo28104l() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!mo28129c(1 + j2) || this.f11901P.mo28056g(j2) != fVar.mo28089a(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }
}
