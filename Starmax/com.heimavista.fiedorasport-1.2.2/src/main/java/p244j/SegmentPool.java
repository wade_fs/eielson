package p244j;

/* renamed from: j.p */
final class SegmentPool {

    /* renamed from: a */
    static Segment f11912a;

    /* renamed from: b */
    static long f11913b;

    private SegmentPool() {
    }

    /* renamed from: a */
    static Segment m18266a() {
        synchronized (SegmentPool.class) {
            if (f11912a == null) {
                return new Segment();
            }
            Segment oVar = f11912a;
            f11912a = oVar.f11910f;
            oVar.f11910f = null;
            f11913b -= 8192;
            return oVar;
        }
    }

    /* renamed from: a */
    static void m18267a(Segment oVar) {
        if (oVar.f11910f != null || oVar.f11911g != null) {
            throw new IllegalArgumentException();
        } else if (!oVar.f11908d) {
            synchronized (SegmentPool.class) {
                if (f11913b + 8192 <= 65536) {
                    f11913b += 8192;
                    oVar.f11910f = f11912a;
                    oVar.f11907c = 0;
                    oVar.f11906b = 0;
                    f11912a = oVar;
                }
            }
        }
    }
}
