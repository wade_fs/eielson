package p244j;

import java.nio.channels.WritableByteChannel;

/* renamed from: j.d */
public interface BufferedSink extends Sink, WritableByteChannel {
    /* renamed from: a */
    BufferedSink mo28033a(String str);

    /* renamed from: c */
    BufferedSink mo28046c(long j);

    /* renamed from: f */
    Buffer mo28054f();

    /* renamed from: f */
    BufferedSink mo28055f(long j);

    void flush();

    /* renamed from: k */
    BufferedSink mo28064k();

    BufferedSink write(byte[] bArr);

    BufferedSink write(byte[] bArr, int i, int i2);

    BufferedSink writeByte(int i);

    BufferedSink writeInt(int i);

    BufferedSink writeShort(int i);
}
