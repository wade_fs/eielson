package p244j;

import com.google.android.exoplayer2.C1750C;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;

/* renamed from: j.c */
public final class Buffer implements BufferedSource, BufferedSink, Cloneable, ByteChannel {

    /* renamed from: R */
    private static final byte[] f11871R = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: P */
    Segment f11872P;

    /* renamed from: Q */
    long f11873Q;

    /* renamed from: j.c$a */
    /* compiled from: Buffer */
    class C5043a extends InputStream {
        C5043a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public int available() {
            return (int) Math.min(Buffer.this.f11873Q, 2147483647L);
        }

        public void close() {
        }

        public int read() {
            Buffer cVar = Buffer.this;
            if (cVar.f11873Q > 0) {
                return cVar.readByte() & 255;
            }
            return -1;
        }

        public String toString() {
            return Buffer.this + ".inputStream()";
        }

        public int read(byte[] bArr, int i, int i2) {
            return Buffer.this.mo28027a(bArr, i, i2);
        }
    }

    /* renamed from: b */
    public long mo28042b() {
        long j = this.f11873Q;
        if (j == 0) {
            return 0;
        }
        Segment oVar = this.f11872P.f11911g;
        int i = oVar.f11907c;
        return (i >= 8192 || !oVar.f11909e) ? j : j - ((long) (i - oVar.f11906b));
    }

    public void close() {
    }

    /* renamed from: d */
    public String mo28049d() {
        try {
            return mo28038a(this.f11873Q, C5051u.f11920a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: e */
    public long mo28051e() {
        return this.f11873Q;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Buffer)) {
            return false;
        }
        Buffer cVar = (Buffer) obj;
        long j = this.f11873Q;
        if (j != cVar.f11873Q) {
            return false;
        }
        long j2 = 0;
        if (j == 0) {
            return true;
        }
        Segment oVar = this.f11872P;
        Segment oVar2 = cVar.f11872P;
        int i = oVar.f11906b;
        int i2 = oVar2.f11906b;
        while (j2 < this.f11873Q) {
            long min = (long) Math.min(oVar.f11907c - i, oVar2.f11907c - i2);
            int i3 = i2;
            int i4 = i;
            int i5 = 0;
            while (((long) i5) < min) {
                int i6 = i4 + 1;
                int i7 = i3 + 1;
                if (oVar.f11905a[i4] != oVar2.f11905a[i3]) {
                    return false;
                }
                i5++;
                i4 = i6;
                i3 = i7;
            }
            if (i4 == oVar.f11907c) {
                oVar = oVar.f11910f;
                i = oVar.f11906b;
            } else {
                i = i4;
            }
            if (i3 == oVar2.f11907c) {
                oVar2 = oVar2.f11910f;
                i2 = oVar2.f11906b;
            } else {
                i2 = i3;
            }
            j2 += min;
        }
        return true;
    }

    /* renamed from: f */
    public Buffer mo28054f() {
        return this;
    }

    public void flush() {
    }

    /* renamed from: g */
    public byte mo28056g(long j) {
        int i;
        C5051u.m18301a(this.f11873Q, j, 1);
        long j2 = this.f11873Q;
        if (j2 - j > j) {
            Segment oVar = this.f11872P;
            while (true) {
                int i2 = oVar.f11907c;
                int i3 = oVar.f11906b;
                long j3 = (long) (i2 - i3);
                if (j < j3) {
                    return oVar.f11905a[i3 + ((int) j)];
                }
                j -= j3;
                oVar = oVar.f11910f;
            }
        } else {
            long j4 = j - j2;
            Segment oVar2 = this.f11872P;
            do {
                oVar2 = oVar2.f11911g;
                int i4 = oVar2.f11907c;
                i = oVar2.f11906b;
                j4 += (long) (i4 - i);
            } while (j4 < 0);
            return oVar2.f11905a[i + ((int) j4)];
        }
    }

    /* renamed from: h */
    public String mo28058h(long j) {
        return mo28038a(j, C5051u.f11920a);
    }

    public int hashCode() {
        Segment oVar = this.f11872P;
        if (oVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = oVar.f11907c;
            for (int i3 = oVar.f11906b; i3 < i2; i3++) {
                i = (i * 31) + oVar.f11905a[i3];
            }
            oVar = oVar.f11910f;
        } while (oVar != this.f11872P);
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public String mo28060i(long j) {
        if (j > 0) {
            long j2 = j - 1;
            if (mo28056g(j2) == 13) {
                String h = mo28058h(j2);
                skip(2);
                return h;
            }
        }
        String h2 = mo28058h(j);
        skip(1);
        return h2;
    }

    public boolean isOpen() {
        return true;
    }

    /* renamed from: j */
    public boolean mo28063j() {
        return this.f11873Q == 0;
    }

    /* renamed from: k */
    public Buffer mo28064k() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (r5 != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        r1.readByte();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        throw new java.lang.NumberFormatException("Number too large: " + r1.mo28049d());
     */
    /* renamed from: l */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo28065l() {
        /*
            r17 = this;
            r0 = r17
            long r1 = r0.f11873Q
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00b5
            r5 = -7
            r7 = 0
            r8 = r5
            r5 = 0
            r6 = 0
        L_0x0010:
            j.o r10 = r0.f11872P
            byte[] r11 = r10.f11905a
            int r12 = r10.f11906b
            int r13 = r10.f11907c
        L_0x0018:
            if (r12 >= r13) goto L_0x0096
            byte r15 = r11[r12]
            r14 = 48
            if (r15 < r14) goto L_0x0068
            r1 = 57
            if (r15 > r1) goto L_0x0068
            int r14 = r14 - r15
            r1 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r16 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r16 < 0) goto L_0x003d
            if (r16 != 0) goto L_0x0036
            long r1 = (long) r14
            int r16 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r16 >= 0) goto L_0x0036
            goto L_0x003d
        L_0x0036:
            r1 = 10
            long r3 = r3 * r1
            long r1 = (long) r14
            long r3 = r3 + r1
            goto L_0x0072
        L_0x003d:
            j.c r1 = new j.c
            r1.<init>()
            r1.mo28055f(r3)
            r1.writeByte(r15)
            if (r5 != 0) goto L_0x004d
            r1.readByte()
        L_0x004d:
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Number too large: "
            r3.append(r4)
            java.lang.String r1 = r1.mo28049d()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x0068:
            r1 = 45
            if (r15 != r1) goto L_0x0077
            if (r7 != 0) goto L_0x0077
            r1 = 1
            long r8 = r8 - r1
            r5 = 1
        L_0x0072:
            int r12 = r12 + 1
            int r7 = r7 + 1
            goto L_0x0018
        L_0x0077:
            if (r7 == 0) goto L_0x007b
            r6 = 1
            goto L_0x0096
        L_0x007b:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was 0x"
            r2.append(r3)
            java.lang.String r3 = java.lang.Integer.toHexString(r15)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0096:
            if (r12 != r13) goto L_0x00a2
            j.o r1 = r10.mo28142b()
            r0.f11872P = r1
            p244j.SegmentPool.m18267a(r10)
            goto L_0x00a4
        L_0x00a2:
            r10.f11906b = r12
        L_0x00a4:
            if (r6 != 0) goto L_0x00aa
            j.o r1 = r0.f11872P
            if (r1 != 0) goto L_0x0010
        L_0x00aa:
            long r1 = r0.f11873Q
            long r6 = (long) r7
            long r1 = r1 - r6
            r0.f11873Q = r1
            if (r5 == 0) goto L_0x00b3
            goto L_0x00b4
        L_0x00b3:
            long r3 = -r3
        L_0x00b4:
            return r3
        L_0x00b5:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "size == 0"
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p244j.Buffer.mo28065l():long");
    }

    /* renamed from: m */
    public String mo28066m() {
        return mo28044b(Long.MAX_VALUE);
    }

    /* renamed from: n */
    public int mo28067n() {
        return C5051u.m18299a(readInt());
    }

    /* renamed from: o */
    public short mo28068o() {
        return C5051u.m18300a(readShort());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        if (r8 != r9) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
        r15.f11872P = r6.mo28142b();
        p244j.SegmentPool.m18267a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009b, code lost:
        r6.f11906b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009d, code lost:
        if (r0 != false) goto L_0x00a3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0074 A[SYNTHETIC] */
    /* renamed from: p */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo28069p() {
        /*
            r15 = this;
            long r0 = r15.f11873Q
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00aa
            r0 = 0
            r4 = r2
            r1 = 0
        L_0x000b:
            j.o r6 = r15.f11872P
            byte[] r7 = r6.f11905a
            int r8 = r6.f11906b
            int r9 = r6.f11907c
        L_0x0013:
            if (r8 >= r9) goto L_0x008f
            byte r10 = r7[r8]
            r11 = 48
            if (r10 < r11) goto L_0x0022
            r11 = 57
            if (r10 > r11) goto L_0x0022
            int r11 = r10 + -48
            goto L_0x003a
        L_0x0022:
            r11 = 97
            if (r10 < r11) goto L_0x002f
            r11 = 102(0x66, float:1.43E-43)
            if (r10 > r11) goto L_0x002f
            int r11 = r10 + -97
        L_0x002c:
            int r11 = r11 + 10
            goto L_0x003a
        L_0x002f:
            r11 = 65
            if (r10 < r11) goto L_0x0070
            r11 = 70
            if (r10 > r11) goto L_0x0070
            int r11 = r10 + -65
            goto L_0x002c
        L_0x003a:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L_0x004a
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r1 = r1 + 1
            goto L_0x0013
        L_0x004a:
            j.c r0 = new j.c
            r0.<init>()
            r0.mo28046c(r4)
            r0.writeByte(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Number too large: "
            r2.append(r3)
            java.lang.String r0 = r0.mo28049d()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x0070:
            if (r1 == 0) goto L_0x0074
            r0 = 1
            goto L_0x008f
        L_0x0074:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was 0x"
            r1.append(r2)
            java.lang.String r2 = java.lang.Integer.toHexString(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x008f:
            if (r8 != r9) goto L_0x009b
            j.o r7 = r6.mo28142b()
            r15.f11872P = r7
            p244j.SegmentPool.m18267a(r6)
            goto L_0x009d
        L_0x009b:
            r6.f11906b = r8
        L_0x009d:
            if (r0 != 0) goto L_0x00a3
            j.o r6 = r15.f11872P
            if (r6 != 0) goto L_0x000b
        L_0x00a3:
            long r2 = r15.f11873Q
            long r0 = (long) r1
            long r2 = r2 - r0
            r15.f11873Q = r2
            return r4
        L_0x00aa:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "size == 0"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p244j.Buffer.mo28069p():long");
    }

    /* renamed from: q */
    public InputStream mo28070q() {
        return new C5043a();
    }

    public int read(ByteBuffer byteBuffer) {
        Segment oVar = this.f11872P;
        if (oVar == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), oVar.f11907c - oVar.f11906b);
        byteBuffer.put(oVar.f11905a, oVar.f11906b, min);
        oVar.f11906b += min;
        this.f11873Q -= (long) min;
        if (oVar.f11906b == oVar.f11907c) {
            this.f11872P = oVar.mo28142b();
            SegmentPool.m18267a(oVar);
        }
        return min;
    }

    public byte readByte() {
        long j = this.f11873Q;
        if (j != 0) {
            Segment oVar = this.f11872P;
            int i = oVar.f11906b;
            int i2 = oVar.f11907c;
            int i3 = i + 1;
            byte b = oVar.f11905a[i];
            this.f11873Q = j - 1;
            if (i3 == i2) {
                this.f11872P = oVar.mo28142b();
                SegmentPool.m18267a(oVar);
            } else {
                oVar.f11906b = i3;
            }
            return b;
        }
        throw new IllegalStateException("size == 0");
    }

    public void readFully(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int a = mo28027a(bArr, i, bArr.length - i);
            if (a != -1) {
                i += a;
            } else {
                throw new EOFException();
            }
        }
    }

    public int readInt() {
        long j = this.f11873Q;
        if (j >= 4) {
            Segment oVar = this.f11872P;
            int i = oVar.f11906b;
            int i2 = oVar.f11907c;
            if (i2 - i < 4) {
                return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
            }
            byte[] bArr = oVar.f11905a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i5 = i4 + 1;
            byte b2 = b | ((bArr[i4] & 255) << 8);
            int i6 = i5 + 1;
            byte b3 = b2 | (bArr[i5] & 255);
            this.f11873Q = j - 4;
            if (i6 == i2) {
                this.f11872P = oVar.mo28142b();
                SegmentPool.m18267a(oVar);
            } else {
                oVar.f11906b = i6;
            }
            return b3;
        }
        throw new IllegalStateException("size < 4: " + this.f11873Q);
    }

    public short readShort() {
        long j = this.f11873Q;
        if (j >= 2) {
            Segment oVar = this.f11872P;
            int i = oVar.f11906b;
            int i2 = oVar.f11907c;
            if (i2 - i < 2) {
                return (short) (((readByte() & 255) << 8) | (readByte() & 255));
            }
            byte[] bArr = oVar.f11905a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
            this.f11873Q = j - 2;
            if (i4 == i2) {
                this.f11872P = oVar.mo28142b();
                SegmentPool.m18267a(oVar);
            } else {
                oVar.f11906b = i4;
            }
            return (short) b;
        }
        throw new IllegalStateException("size < 2: " + this.f11873Q);
    }

    public void skip(long j) {
        while (j > 0) {
            Segment oVar = this.f11872P;
            if (oVar != null) {
                int min = (int) Math.min(j, (long) (oVar.f11907c - oVar.f11906b));
                long j2 = (long) min;
                this.f11873Q -= j2;
                j -= j2;
                Segment oVar2 = this.f11872P;
                oVar2.f11906b += min;
                if (oVar2.f11906b == oVar2.f11907c) {
                    this.f11872P = oVar2.mo28142b();
                    SegmentPool.m18267a(oVar2);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    public String toString() {
        return mo28057g().toString();
    }

    /* renamed from: a */
    public Buffer mo28031a(Buffer cVar, long j, long j2) {
        if (cVar != null) {
            C5051u.m18301a(this.f11873Q, j, j2);
            if (j2 == 0) {
                return this;
            }
            cVar.f11873Q += j2;
            Segment oVar = this.f11872P;
            while (true) {
                int i = oVar.f11907c;
                int i2 = oVar.f11906b;
                if (j < ((long) (i - i2))) {
                    break;
                }
                j -= (long) (i - i2);
                oVar = oVar.f11910f;
            }
            while (j2 > 0) {
                Segment c = oVar.mo28143c();
                c.f11906b = (int) (((long) c.f11906b) + j);
                c.f11907c = Math.min(c.f11906b + ((int) j2), c.f11907c);
                Segment oVar2 = cVar.f11872P;
                if (oVar2 == null) {
                    c.f11911g = c;
                    c.f11910f = c;
                    cVar.f11872P = c;
                } else {
                    oVar2.f11911g.mo28139a(c);
                }
                j2 -= (long) (c.f11907c - c.f11906b);
                oVar = oVar.f11910f;
                j = 0;
            }
            return this;
        }
        throw new IllegalArgumentException("out == null");
    }

    /* renamed from: c */
    public ByteString mo28047c() {
        return new ByteString(mo28061i());
    }

    public Buffer clone() {
        Buffer cVar = new Buffer();
        if (this.f11873Q == 0) {
            return cVar;
        }
        cVar.f11872P = this.f11872P.mo28143c();
        Segment oVar = cVar.f11872P;
        oVar.f11911g = oVar;
        oVar.f11910f = oVar;
        Segment oVar2 = this.f11872P;
        while (true) {
            oVar2 = oVar2.f11910f;
            if (oVar2 != this.f11872P) {
                cVar.f11872P.f11911g.mo28139a(oVar2.mo28143c());
            } else {
                cVar.f11873Q = this.f11873Q;
                return cVar;
            }
        }
    }

    /* renamed from: e */
    public void mo28052e(long j) {
        if (this.f11873Q < j) {
            throw new EOFException();
        }
    }

    /* renamed from: f */
    public Buffer mo28055f(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            writeByte(48);
            return this;
        }
        boolean z = false;
        int i2 = 1;
        if (i < 0) {
            j = -j;
            if (j < 0) {
                mo28033a("-9223372036854775808");
                return this;
            }
            z = true;
        }
        if (j >= 100000000) {
            i2 = j < 1000000000000L ? j < 10000000000L ? j < C1750C.NANOS_PER_SECOND ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= 10000) {
            i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i2 = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i2 = 2;
        }
        if (z) {
            i2++;
        }
        Segment b = mo28043b(i2);
        byte[] bArr = b.f11905a;
        int i3 = b.f11907c + i2;
        while (j != 0) {
            i3--;
            bArr[i3] = f11871R[(int) (j % 10)];
            j /= 10;
        }
        if (z) {
            bArr[i3 - 1] = 45;
        }
        b.f11907c += i2;
        this.f11873Q += (long) i2;
        return this;
    }

    /* renamed from: h */
    public Timeout mo27416h() {
        return Timeout.f11916d;
    }

    public Buffer writeByte(int i) {
        Segment b = mo28043b(1);
        byte[] bArr = b.f11905a;
        int i2 = b.f11907c;
        b.f11907c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f11873Q++;
        return this;
    }

    public Buffer writeInt(int i) {
        Segment b = mo28043b(4);
        byte[] bArr = b.f11905a;
        int i2 = b.f11907c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        b.f11907c = i5 + 1;
        this.f11873Q += 4;
        return this;
    }

    public Buffer writeShort(int i) {
        Segment b = mo28043b(2);
        byte[] bArr = b.f11905a;
        int i2 = b.f11907c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        b.f11907c = i3 + 1;
        this.f11873Q += 2;
        return this;
    }

    /* renamed from: c */
    public Buffer mo28045c(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            writeByte((i >> 6) | PsExtractor.AUDIO_STREAM);
            writeByte((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                writeByte((i >> 12) | 224);
                writeByte(((i >> 6) & 63) | 128);
                writeByte((i & 63) | 128);
            } else {
                writeByte(63);
            }
        } else if (i <= 1114111) {
            writeByte((i >> 18) | PsExtractor.VIDEO_STREAM_MASK);
            writeByte(((i >> 12) & 63) | 128);
            writeByte(((i >> 6) & 63) | 128);
            writeByte((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    /* renamed from: d */
    public byte[] mo28050d(long j) {
        C5051u.m18301a(this.f11873Q, 0, j);
        if (j <= 2147483647L) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        }
        throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.write(byte[], int, int):j.c
     arg types: [byte[], int, int]
     candidates:
      j.c.write(byte[], int, int):j.d
      j.d.write(byte[], int, int):j.d
      j.c.write(byte[], int, int):j.c */
    public Buffer write(byte[] bArr) {
        if (bArr != null) {
            write(bArr, 0, bArr.length);
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* renamed from: b */
    public String mo28044b(long j) {
        if (j >= 0) {
            long j2 = Long.MAX_VALUE;
            if (j != Long.MAX_VALUE) {
                j2 = j + 1;
            }
            long a = mo28029a((byte) 10, 0, j2);
            if (a != -1) {
                return mo28060i(a);
            }
            if (j2 < mo28051e() && mo28056g(j2 - 1) == 13 && mo28056g(j2) == 10) {
                return mo28060i(j2);
            }
            Buffer cVar = new Buffer();
            mo28031a(cVar, 0, Math.min(32L, mo28051e()));
            throw new EOFException("\\n not found: limit=" + Math.min(mo28051e(), j) + " content=" + cVar.mo28047c().mo28099h() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    public Buffer write(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            long j = (long) i2;
            C5051u.m18301a((long) bArr.length, (long) i, j);
            int i3 = i2 + i;
            while (i < i3) {
                Segment b = mo28043b(1);
                int min = Math.min(i3 - i, 8192 - b.f11907c);
                System.arraycopy(bArr, i, b.f11905a, b.f11907c, min);
                i += min;
                b.f11907c += min;
            }
            this.f11873Q += j;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: i */
    public byte[] mo28061i() {
        try {
            return mo28050d(this.f11873Q);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: g */
    public ByteString mo28057g() {
        long j = this.f11873Q;
        if (j <= 2147483647L) {
            return mo28036a((int) j);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f11873Q);
    }

    public int write(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            int remaining = byteBuffer.remaining();
            int i = remaining;
            while (i > 0) {
                Segment b = mo28043b(1);
                int min = Math.min(i, 8192 - b.f11907c);
                byteBuffer.get(b.f11905a, b.f11907c, min);
                i -= min;
                b.f11907c += min;
            }
            this.f11873Q += (long) remaining;
            return remaining;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: a */
    public ByteString mo28037a(long j) {
        return new ByteString(mo28050d(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Segment mo28043b(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        }
        Segment oVar = this.f11872P;
        if (oVar == null) {
            this.f11872P = SegmentPool.m18266a();
            Segment oVar2 = this.f11872P;
            oVar2.f11911g = oVar2;
            oVar2.f11910f = oVar2;
            return oVar2;
        }
        Segment oVar3 = oVar.f11911g;
        if (oVar3.f11907c + i <= 8192 && oVar3.f11909e) {
            return oVar3;
        }
        Segment a = SegmentPool.m18266a();
        oVar3.mo28139a(a);
        return a;
    }

    /* renamed from: a */
    public String mo28038a(long j, Charset charset) {
        C5051u.m18301a(this.f11873Q, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            Segment oVar = this.f11872P;
            int i = oVar.f11906b;
            if (((long) i) + j > ((long) oVar.f11907c)) {
                return new String(mo28050d(j), charset);
            }
            String str = new String(oVar.f11905a, i, (int) j, charset);
            oVar.f11906b = (int) (((long) oVar.f11906b) + j);
            this.f11873Q -= j;
            if (oVar.f11906b == oVar.f11907c) {
                this.f11872P = oVar.mo28142b();
                SegmentPool.m18267a(oVar);
            }
            return str;
        }
    }

    /* renamed from: c */
    public Buffer mo28046c(long j) {
        if (j == 0) {
            writeByte(48);
            return this;
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        Segment b = mo28043b(numberOfTrailingZeros);
        byte[] bArr = b.f11905a;
        int i = b.f11907c;
        for (int i2 = (i + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f11871R[(int) (15 & j)];
            j >>>= 4;
        }
        b.f11907c += numberOfTrailingZeros;
        this.f11873Q += (long) numberOfTrailingZeros;
        return this;
    }

    /* renamed from: b */
    public long mo27415b(Buffer cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j >= 0) {
            long j2 = this.f11873Q;
            if (j2 == 0) {
                return -1;
            }
            if (j > j2) {
                j = j2;
            }
            cVar.mo27444a(this, j);
            return j;
        } else {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        }
    }

    /* renamed from: a */
    public int mo28027a(byte[] bArr, int i, int i2) {
        C5051u.m18301a((long) bArr.length, (long) i, (long) i2);
        Segment oVar = this.f11872P;
        if (oVar == null) {
            return -1;
        }
        int min = Math.min(i2, oVar.f11907c - oVar.f11906b);
        System.arraycopy(oVar.f11905a, oVar.f11906b, bArr, i, min);
        oVar.f11906b += min;
        this.f11873Q -= (long) min;
        if (oVar.f11906b == oVar.f11907c) {
            this.f11872P = oVar.mo28142b();
            SegmentPool.m18267a(oVar);
        }
        return min;
    }

    /* renamed from: a */
    public void mo28039a() {
        try {
            skip(this.f11873Q);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: a */
    public Buffer mo28032a(ByteString fVar) {
        if (fVar != null) {
            fVar.mo28093a(this);
            return this;
        }
        throw new IllegalArgumentException("byteString == null");
    }

    /* renamed from: a */
    public Buffer mo28033a(String str) {
        mo28034a(str, 0, str.length());
        return this;
    }

    /* renamed from: a */
    public Buffer mo28034a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 <= str.length()) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    Segment b = mo28043b(1);
                    byte[] bArr = b.f11905a;
                    int i3 = b.f11907c - i;
                    int min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        char charAt2 = str.charAt(i4);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i4 + i3] = (byte) charAt2;
                        i4++;
                    }
                    int i5 = b.f11907c;
                    int i6 = (i3 + i4) - i5;
                    b.f11907c = i5 + i6;
                    this.f11873Q += (long) i6;
                    i = i4;
                } else {
                    if (charAt < 2048) {
                        writeByte((charAt >> 6) | PsExtractor.AUDIO_STREAM);
                        writeByte((int) ((charAt & '?') | 128));
                    } else if (charAt < 55296 || charAt > 57343) {
                        writeByte((charAt >> 12) | 224);
                        writeByte(((charAt >> 6) & 63) | 128);
                        writeByte((int) ((charAt & '?') | 128));
                    } else {
                        int i7 = i + 1;
                        char charAt3 = i7 < i2 ? str.charAt(i7) : 0;
                        if (charAt > 56319 || charAt3 < 56320 || charAt3 > 57343) {
                            writeByte(63);
                            i = i7;
                        } else {
                            int i8 = (((charAt & 10239) << 10) | (9215 & charAt3)) + 0;
                            writeByte((i8 >> 18) | PsExtractor.VIDEO_STREAM_MASK);
                            writeByte(((i8 >> 12) & 63) | 128);
                            writeByte(((i8 >> 6) & 63) | 128);
                            writeByte((i8 & 63) | 128);
                            i += 2;
                        }
                    }
                    i++;
                }
            }
            return this;
        } else {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.write(byte[], int, int):j.c
     arg types: [byte[], int, int]
     candidates:
      j.c.write(byte[], int, int):j.d
      j.d.write(byte[], int, int):j.d
      j.c.write(byte[], int, int):j.c */
    /* renamed from: a */
    public Buffer mo28035a(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(C5051u.f11920a)) {
            mo28034a(str, i, i2);
            return this;
        } else {
            byte[] bytes = str.substring(i, i2).getBytes(charset);
            write(bytes, 0, bytes.length);
            return this;
        }
    }

    /* renamed from: a */
    public long mo28030a(C5049s sVar) {
        if (sVar != null) {
            long j = 0;
            while (true) {
                long b = sVar.mo27415b(this, 8192);
                if (b == -1) {
                    return j;
                }
                j += b;
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }

    /* renamed from: a */
    public void mo27444a(Buffer cVar, long j) {
        int i;
        if (cVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (cVar != this) {
            C5051u.m18301a(cVar.f11873Q, 0, j);
            while (j > 0) {
                Segment oVar = cVar.f11872P;
                if (j < ((long) (oVar.f11907c - oVar.f11906b))) {
                    Segment oVar2 = this.f11872P;
                    Segment oVar3 = oVar2 != null ? oVar2.f11911g : null;
                    if (oVar3 != null && oVar3.f11909e) {
                        long j2 = ((long) oVar3.f11907c) + j;
                        if (oVar3.f11908d) {
                            i = 0;
                        } else {
                            i = oVar3.f11906b;
                        }
                        if (j2 - ((long) i) <= 8192) {
                            cVar.f11872P.mo28141a(oVar3, (int) j);
                            cVar.f11873Q -= j;
                            this.f11873Q += j;
                            return;
                        }
                    }
                    cVar.f11872P = cVar.f11872P.mo28138a((int) j);
                }
                Segment oVar4 = cVar.f11872P;
                long j3 = (long) (oVar4.f11907c - oVar4.f11906b);
                cVar.f11872P = oVar4.mo28142b();
                Segment oVar5 = this.f11872P;
                if (oVar5 == null) {
                    this.f11872P = oVar4;
                    Segment oVar6 = this.f11872P;
                    oVar6.f11911g = oVar6;
                    oVar6.f11910f = oVar6;
                } else {
                    oVar5.f11911g.mo28139a(oVar4);
                    oVar4.mo28140a();
                }
                cVar.f11873Q -= j3;
                this.f11873Q += j3;
                j -= j3;
            }
        } else {
            throw new IllegalArgumentException("source == this");
        }
    }

    /* renamed from: a */
    public long mo28028a(byte b) {
        return mo28029a(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: a */
    public long mo28029a(byte b, long j, long j2) {
        Segment oVar;
        long j3 = 0;
        if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", Long.valueOf(this.f11873Q), Long.valueOf(j), Long.valueOf(j2)));
        }
        long j4 = this.f11873Q;
        if (j2 <= j4) {
            j4 = j2;
        }
        if (j == j4 || (oVar = this.f11872P) == null) {
            return -1;
        }
        long j5 = this.f11873Q;
        if (j5 - j >= j) {
            while (true) {
                j5 = j3;
                j3 = ((long) (oVar.f11907c - oVar.f11906b)) + j5;
                if (j3 >= j) {
                    break;
                }
                oVar = oVar.f11910f;
            }
        } else {
            while (j5 > j) {
                oVar = oVar.f11911g;
                j5 -= (long) (oVar.f11907c - oVar.f11906b);
            }
        }
        long j6 = j;
        while (j5 < j4) {
            byte[] bArr = oVar.f11905a;
            int min = (int) Math.min((long) oVar.f11907c, (((long) oVar.f11906b) + j4) - j5);
            for (int i = (int) ((((long) oVar.f11906b) + j6) - j5); i < min; i++) {
                if (bArr[i] == b) {
                    return ((long) (i - oVar.f11906b)) + j5;
                }
            }
            j6 = ((long) (oVar.f11907c - oVar.f11906b)) + j5;
            oVar = oVar.f11910f;
            j5 = j6;
        }
        return -1;
    }

    /* renamed from: a */
    public boolean mo28040a(long j, ByteString fVar) {
        return mo28041a(j, fVar, 0, fVar.mo28104l());
    }

    /* renamed from: a */
    public boolean mo28041a(long j, ByteString fVar, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.f11873Q - j < ((long) i2) || fVar.mo28104l() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (mo28056g(((long) i3) + j) != fVar.mo28089a(i + i3)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public ByteString mo28036a(int i) {
        if (i == 0) {
            return ByteString.f11876T;
        }
        return new SegmentedByteString(this, i);
    }
}
