package p244j;

import java.io.Closeable;

/* renamed from: j.s */
/* compiled from: Source */
public interface C5049s extends Closeable {
    /* renamed from: b */
    long mo27415b(Buffer cVar, long j);

    void close();

    /* renamed from: h */
    Timeout mo27416h();
}
