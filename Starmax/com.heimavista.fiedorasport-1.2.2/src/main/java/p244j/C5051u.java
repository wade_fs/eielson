package p244j;

import com.google.android.exoplayer2.C1750C;
import java.nio.charset.Charset;

/* renamed from: j.u */
/* compiled from: Util */
final class C5051u {

    /* renamed from: a */
    public static final Charset f11920a = Charset.forName(C1750C.UTF8_NAME);

    /* renamed from: a */
    public static int m18299a(int i) {
        return ((i & 255) << 24) | ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8);
    }

    /* renamed from: a */
    public static short m18300a(short s) {
        short s2 = s & 65535;
        return (short) (((s2 & 255) << 8) | ((65280 & s2) >>> 8));
    }

    /* renamed from: a */
    public static void m18301a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException(String.format("size=%s offset=%s byteCount=%s", Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3)));
        }
    }

    /* renamed from: b */
    private static <T extends Throwable> void m18304b(Throwable th) {
        throw th;
    }

    /* renamed from: a */
    public static void m18302a(Throwable th) {
        m18304b(th);
        throw null;
    }

    /* renamed from: a */
    public static boolean m18303a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }
}
