package p244j;

import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;

/* renamed from: j.e */
public interface BufferedSource extends C5049s, ReadableByteChannel {
    /* renamed from: a */
    long mo28028a(byte b);

    /* renamed from: a */
    ByteString mo28037a(long j);

    /* renamed from: a */
    boolean mo28040a(long j, ByteString fVar);

    /* renamed from: b */
    String mo28044b(long j);

    /* renamed from: d */
    byte[] mo28050d(long j);

    /* renamed from: e */
    void mo28052e(long j);

    /* renamed from: f */
    Buffer mo28054f();

    /* renamed from: i */
    byte[] mo28061i();

    /* renamed from: j */
    boolean mo28063j();

    /* renamed from: l */
    long mo28065l();

    /* renamed from: m */
    String mo28066m();

    /* renamed from: n */
    int mo28067n();

    /* renamed from: o */
    short mo28068o();

    /* renamed from: p */
    long mo28069p();

    /* renamed from: q */
    InputStream mo28070q();

    byte readByte();

    void readFully(byte[] bArr);

    int readInt();

    short readShort();

    void skip(long j);
}
