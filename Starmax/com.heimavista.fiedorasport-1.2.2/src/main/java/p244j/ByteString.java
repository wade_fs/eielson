package p244j;

import java.io.EOFException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/* renamed from: j.f */
public class ByteString implements Serializable, Comparable<ByteString> {

    /* renamed from: S */
    static final char[] f11875S = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: T */
    public static final ByteString f11876T = m18167a(new byte[0]);
    private static final long serialVersionUID = 1;

    /* renamed from: P */
    final byte[] f11877P;

    /* renamed from: Q */
    transient int f11878Q;

    /* renamed from: R */
    transient String f11879R;

    ByteString(byte[] bArr) {
        this.f11877P = bArr;
    }

    /* renamed from: a */
    public static ByteString m18167a(byte... bArr) {
        if (bArr != null) {
            return new ByteString((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    /* renamed from: b */
    public static ByteString m18168b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("hex == null");
        } else if (str.length() % 2 == 0) {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                int i2 = i * 2;
                bArr[i] = (byte) ((m18163a(str.charAt(i2)) << 4) + m18163a(str.charAt(i2 + 1)));
            }
            return m18167a(bArr);
        } else {
            throw new IllegalArgumentException("Unexpected hex string: " + str);
        }
    }

    /* renamed from: c */
    private ByteString m18169c(String str) {
        try {
            return m18167a(MessageDigest.getInstance(str).digest(this.f11877P));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: d */
    public static ByteString m18170d(String str) {
        if (str != null) {
            ByteString fVar = new ByteString(str.getBytes(C5051u.f11920a));
            fVar.f11879R = str;
            return fVar;
        }
        throw new IllegalArgumentException("s == null");
    }

    private void readObject(ObjectInputStream objectInputStream) {
        ByteString a = m18165a(objectInputStream, objectInputStream.readInt());
        try {
            Field declaredField = ByteString.class.getDeclaredField("P");
            declaredField.setAccessible(true);
            declaredField.set(this, a.f11877P);
        } catch (NoSuchFieldException unused) {
            throw new AssertionError();
        } catch (IllegalAccessException unused2) {
            throw new AssertionError();
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(this.f11877P.length);
        objectOutputStream.write(this.f11877P);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString fVar = (ByteString) obj;
            int l = fVar.mo28104l();
            byte[] bArr = this.f11877P;
            if (l != bArr.length || !fVar.mo28095a(0, bArr, 0, bArr.length)) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* renamed from: h */
    public String mo28099h() {
        byte[] bArr = this.f11877P;
        char[] cArr = new char[(bArr.length * 2)];
        int i = 0;
        for (byte b : bArr) {
            int i2 = i + 1;
            char[] cArr2 = f11875S;
            cArr[i] = cArr2[(b >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = cArr2[b & 15];
        }
        return new String(cArr);
    }

    public int hashCode() {
        int i = this.f11878Q;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.f11877P);
        this.f11878Q = hashCode;
        return hashCode;
    }

    /* renamed from: i */
    public ByteString mo28101i() {
        return m18169c("MD5");
    }

    /* renamed from: j */
    public ByteString mo28102j() {
        return m18169c("SHA-1");
    }

    /* renamed from: k */
    public ByteString mo28103k() {
        return m18169c("SHA-256");
    }

    /* renamed from: l */
    public int mo28104l() {
        return this.f11877P.length;
    }

    /* renamed from: m */
    public ByteString mo28105m() {
        int i = 0;
        while (true) {
            byte[] bArr = this.f11877P;
            if (i >= bArr.length) {
                return this;
            }
            byte b = bArr[i];
            if (b < 65 || b > 90) {
                i++;
            } else {
                byte[] bArr2 = (byte[]) bArr.clone();
                bArr2[i] = (byte) (b + 32);
                for (int i2 = i + 1; i2 < bArr2.length; i2++) {
                    byte b2 = bArr2[i2];
                    if (b2 >= 65 && b2 <= 90) {
                        bArr2[i2] = (byte) (b2 + 32);
                    }
                }
                return new ByteString(bArr2);
            }
        }
    }

    /* renamed from: n */
    public byte[] mo28106n() {
        return (byte[]) this.f11877P.clone();
    }

    /* renamed from: o */
    public String mo28107o() {
        String str = this.f11879R;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.f11877P, C5051u.f11920a);
        this.f11879R = str2;
        return str2;
    }

    public String toString() {
        if (this.f11877P.length == 0) {
            return "[size=0]";
        }
        String o = mo28107o();
        int a = m18164a(o, 64);
        if (a != -1) {
            String replace = o.substring(0, a).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r");
            if (a < o.length()) {
                return "[size=" + this.f11877P.length + " text=" + replace + "…]";
            }
            return "[text=" + replace + "]";
        } else if (this.f11877P.length <= 64) {
            return "[hex=" + mo28099h() + "]";
        } else {
            return "[size=" + this.f11877P.length + " hex=" + mo28091a(0, 64).mo28099h() + "…]";
        }
    }

    /* renamed from: a */
    public String mo28092a() {
        return Base64.m18093a(this.f11877P);
    }

    /* renamed from: a */
    public static ByteString m18166a(String str) {
        if (str != null) {
            byte[] a = Base64.m18095a(str);
            if (a != null) {
                return new ByteString(a);
            }
            return null;
        }
        throw new IllegalArgumentException("base64 == null");
    }

    /* renamed from: a */
    private static int m18163a(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        char c2 = 'a';
        if (c < 'a' || c > 'f') {
            c2 = 'A';
            if (c < 'A' || c > 'F') {
                throw new IllegalArgumentException("Unexpected hex digit: " + c);
            }
        }
        return (c - c2) + 10;
    }

    /* renamed from: a */
    public static ByteString m18165a(InputStream inputStream, int i) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (i >= 0) {
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read != -1) {
                    i2 += read;
                } else {
                    throw new EOFException();
                }
            }
            return new ByteString(bArr);
        } else {
            throw new IllegalArgumentException("byteCount < 0: " + i);
        }
    }

    /* renamed from: b */
    public final boolean mo28096b(ByteString fVar) {
        return mo28094a(0, fVar, 0, fVar.mo28104l());
    }

    /* renamed from: a */
    public ByteString mo28091a(int i, int i2) {
        if (i >= 0) {
            byte[] bArr = this.f11877P;
            if (i2 <= bArr.length) {
                int i3 = i2 - i;
                if (i3 < 0) {
                    throw new IllegalArgumentException("endIndex < beginIndex");
                } else if (i == 0 && i2 == bArr.length) {
                    return this;
                } else {
                    byte[] bArr2 = new byte[i3];
                    System.arraycopy(this.f11877P, i, bArr2, 0, i3);
                    return new ByteString(bArr2);
                }
            } else {
                throw new IllegalArgumentException("endIndex > length(" + this.f11877P.length + ")");
            }
        } else {
            throw new IllegalArgumentException("beginIndex < 0");
        }
    }

    /* renamed from: a */
    public byte mo28089a(int i) {
        return this.f11877P[i];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.write(byte[], int, int):j.c
     arg types: [byte[], int, int]
     candidates:
      j.c.write(byte[], int, int):j.d
      j.d.write(byte[], int, int):j.d
      j.c.write(byte[], int, int):j.c */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28093a(Buffer cVar) {
        byte[] bArr = this.f11877P;
        cVar.write(bArr, 0, bArr.length);
    }

    /* renamed from: a */
    public boolean mo28094a(int i, ByteString fVar, int i2, int i3) {
        return fVar.mo28095a(i2, this.f11877P, i, i3);
    }

    /* renamed from: a */
    public boolean mo28095a(int i, byte[] bArr, int i2, int i3) {
        if (i >= 0) {
            byte[] bArr2 = this.f11877P;
            return i <= bArr2.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && C5051u.m18303a(bArr2, i, bArr, i2, i3);
        }
    }

    /* renamed from: a */
    public int compareTo(ByteString fVar) {
        int l = mo28104l();
        int l2 = fVar.mo28104l();
        int min = Math.min(l, l2);
        int i = 0;
        while (i < min) {
            byte a = mo28089a(i) & 255;
            byte a2 = fVar.mo28089a(i) & 255;
            if (a == a2) {
                i++;
            } else if (a < a2) {
                return -1;
            } else {
                return 1;
            }
        }
        if (l == l2) {
            return 0;
        }
        if (l < l2) {
            return -1;
        }
        return 1;
    }

    /* renamed from: a */
    static int m18164a(String str, int i) {
        int length = str.length();
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (i3 == i) {
                return i2;
            }
            int codePointAt = str.codePointAt(i2);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i3++;
            i2 += Character.charCount(codePointAt);
        }
        return str.length();
    }
}
