package p244j;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* renamed from: j.t */
public class Timeout {

    /* renamed from: d */
    public static final Timeout f11916d = new C5050a();

    /* renamed from: a */
    private boolean f11917a;

    /* renamed from: b */
    private long f11918b;

    /* renamed from: c */
    private long f11919c;

    /* renamed from: j.t$a */
    /* compiled from: Timeout */
    class C5050a extends Timeout {
        C5050a() {
        }

        /* renamed from: a */
        public Timeout mo28114a(long j) {
            return super;
        }

        /* renamed from: a */
        public Timeout mo28115a(long j, TimeUnit timeUnit) {
            return super;
        }

        /* renamed from: e */
        public void mo28119e() {
        }
    }

    /* renamed from: a */
    public Timeout mo28115a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit != null) {
            this.f11919c = timeUnit.toNanos(j);
            return this;
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    /* renamed from: b */
    public Timeout mo28116b() {
        this.f11919c = 0;
        return this;
    }

    /* renamed from: c */
    public long mo28117c() {
        if (this.f11917a) {
            return this.f11918b;
        }
        throw new IllegalStateException("No deadline");
    }

    /* renamed from: d */
    public boolean mo28118d() {
        return this.f11917a;
    }

    /* renamed from: e */
    public void mo28119e() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f11917a && this.f11918b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    /* renamed from: f */
    public long mo28144f() {
        return this.f11919c;
    }

    /* renamed from: a */
    public Timeout mo28114a(long j) {
        this.f11917a = true;
        this.f11918b = j;
        return this;
    }

    /* renamed from: a */
    public Timeout mo28113a() {
        this.f11917a = false;
        return this;
    }
}
