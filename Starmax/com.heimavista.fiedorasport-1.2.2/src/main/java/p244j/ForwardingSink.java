package p244j;

/* renamed from: j.g */
public abstract class ForwardingSink implements Sink {

    /* renamed from: P */
    private final Sink f11880P;

    public ForwardingSink(Sink rVar) {
        if (rVar != null) {
            this.f11880P = rVar;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    /* renamed from: a */
    public void mo27444a(Buffer cVar, long j) {
        this.f11880P.mo27444a(cVar, j);
    }

    public void close() {
        this.f11880P.close();
    }

    public void flush() {
        this.f11880P.flush();
    }

    /* renamed from: h */
    public Timeout mo27513h() {
        return this.f11880P.mo27513h();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f11880P.toString() + ")";
    }
}
