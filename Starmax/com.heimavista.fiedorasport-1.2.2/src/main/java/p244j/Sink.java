package p244j;

import java.io.Closeable;
import java.io.Flushable;

/* renamed from: j.r */
public interface Sink extends Closeable, Flushable {
    /* renamed from: a */
    void mo27444a(Buffer cVar, long j);

    void close();

    void flush();

    /* renamed from: h */
    Timeout mo27513h();
}
