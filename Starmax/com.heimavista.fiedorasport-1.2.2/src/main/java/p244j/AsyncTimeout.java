package p244j;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* renamed from: j.a */
public class AsyncTimeout extends Timeout {

    /* renamed from: h */
    private static final long f11860h = TimeUnit.SECONDS.toMillis(60);

    /* renamed from: i */
    private static final long f11861i = TimeUnit.MILLISECONDS.toNanos(f11860h);

    /* renamed from: j */
    static AsyncTimeout f11862j;

    /* renamed from: e */
    private boolean f11863e;

    /* renamed from: f */
    private AsyncTimeout f11864f;

    /* renamed from: g */
    private long f11865g;

    /* renamed from: j.a$a */
    /* compiled from: AsyncTimeout */
    class C5040a implements Sink {

        /* renamed from: P */
        final /* synthetic */ Sink f11866P;

        C5040a(Sink rVar) {
            this.f11866P = rVar;
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            C5051u.m18301a(cVar.f11873Q, 0, j);
            while (true) {
                long j2 = 0;
                if (j > 0) {
                    Segment oVar = cVar.f11872P;
                    while (true) {
                        if (j2 >= 65536) {
                            break;
                        }
                        j2 += (long) (oVar.f11907c - oVar.f11906b);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        oVar = oVar.f11910f;
                    }
                    AsyncTimeout.this.mo28022g();
                    try {
                        this.f11866P.mo27444a(cVar, j2);
                        j -= j2;
                        AsyncTimeout.this.mo28021a(true);
                    } catch (IOException e) {
                        throw AsyncTimeout.this.mo28020a(e);
                    } catch (Throwable th) {
                        AsyncTimeout.this.mo28021a(false);
                        throw th;
                    }
                } else {
                    return;
                }
            }
        }

        public void close() {
            AsyncTimeout.this.mo28022g();
            try {
                this.f11866P.close();
                AsyncTimeout.this.mo28021a(true);
            } catch (IOException e) {
                throw AsyncTimeout.this.mo28020a(e);
            } catch (Throwable th) {
                AsyncTimeout.this.mo28021a(false);
                throw th;
            }
        }

        public void flush() {
            AsyncTimeout.this.mo28022g();
            try {
                this.f11866P.flush();
                AsyncTimeout.this.mo28021a(true);
            } catch (IOException e) {
                throw AsyncTimeout.this.mo28020a(e);
            } catch (Throwable th) {
                AsyncTimeout.this.mo28021a(false);
                throw th;
            }
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return AsyncTimeout.this;
        }

        public String toString() {
            return "AsyncTimeout.sink(" + this.f11866P + ")";
        }
    }

    /* renamed from: j.a$b */
    /* compiled from: AsyncTimeout */
    class C5041b implements C5049s {

        /* renamed from: P */
        final /* synthetic */ C5049s f11868P;

        C5041b(C5049s sVar) {
            this.f11868P = sVar;
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            AsyncTimeout.this.mo28022g();
            try {
                long b = this.f11868P.mo27415b(cVar, j);
                AsyncTimeout.this.mo28021a(true);
                return b;
            } catch (IOException e) {
                throw AsyncTimeout.this.mo28020a(e);
            } catch (Throwable th) {
                AsyncTimeout.this.mo28021a(false);
                throw th;
            }
        }

        public void close() {
            try {
                this.f11868P.close();
                AsyncTimeout.this.mo28021a(true);
            } catch (IOException e) {
                throw AsyncTimeout.this.mo28020a(e);
            } catch (Throwable th) {
                AsyncTimeout.this.mo28021a(false);
                throw th;
            }
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return AsyncTimeout.this;
        }

        public String toString() {
            return "AsyncTimeout.source(" + this.f11868P + ")";
        }
    }

    /* renamed from: j.a$c */
    /* compiled from: AsyncTimeout */
    private static final class C5042c extends Thread {
        C5042c() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1.mo27584i();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<j.a> r0 = p244j.AsyncTimeout.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0000 }
                j.a r1 = p244j.AsyncTimeout.m18080j()     // Catch:{ all -> 0x0019 }
                if (r1 != 0) goto L_0x000b
                monitor-exit(r0)     // Catch:{ all -> 0x0019 }
                goto L_0x0000
            L_0x000b:
                j.a r2 = p244j.AsyncTimeout.f11862j     // Catch:{ all -> 0x0019 }
                if (r1 != r2) goto L_0x0014
                r1 = 0
                p244j.AsyncTimeout.f11862j = r1     // Catch:{ all -> 0x0019 }
                monitor-exit(r0)     // Catch:{ all -> 0x0019 }
                return
            L_0x0014:
                monitor-exit(r0)     // Catch:{ all -> 0x0019 }
                r1.mo27584i()     // Catch:{ InterruptedException -> 0x0000 }
                goto L_0x0000
            L_0x0019:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0019 }
                throw r1     // Catch:{ InterruptedException -> 0x0000 }
            */
            throw new UnsupportedOperationException("Method not decompiled: p244j.AsyncTimeout.C5042c.run():void");
        }
    }

    /* renamed from: a */
    private static synchronized void m18077a(AsyncTimeout aVar, long j, boolean z) {
        Class<AsyncTimeout> cls = AsyncTimeout.class;
        synchronized (cls) {
            if (f11862j == null) {
                f11862j = new AsyncTimeout();
                new C5042c().start();
            }
            long nanoTime = System.nanoTime();
            int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            if (i != 0 && z) {
                aVar.f11865g = Math.min(j, super.mo28117c() - nanoTime) + nanoTime;
            } else if (i != 0) {
                aVar.f11865g = j + nanoTime;
            } else if (z) {
                aVar.f11865g = super.mo28117c();
            } else {
                throw new AssertionError();
            }
            long b = aVar.m18079b(nanoTime);
            AsyncTimeout aVar2 = f11862j;
            while (true) {
                if (aVar2.f11864f == null) {
                    break;
                } else if (b < aVar2.f11864f.m18079b(nanoTime)) {
                    break;
                } else {
                    aVar2 = aVar2.f11864f;
                }
            }
            aVar.f11864f = aVar2.f11864f;
            aVar2.f11864f = aVar;
            if (aVar2 == f11862j) {
                cls.notify();
            }
        }
    }

    /* renamed from: b */
    private long m18079b(long j) {
        return this.f11865g - j;
    }

    /* renamed from: j */
    static AsyncTimeout m18080j() {
        Class<AsyncTimeout> cls = AsyncTimeout.class;
        AsyncTimeout aVar = f11862j.f11864f;
        if (aVar == null) {
            long nanoTime = System.nanoTime();
            cls.wait(f11860h);
            if (f11862j.f11864f != null || System.nanoTime() - nanoTime < f11861i) {
                return null;
            }
            return f11862j;
        }
        long b = aVar.m18079b(System.nanoTime());
        if (b > 0) {
            long j = b / 1000000;
            cls.wait(j, (int) (b - (1000000 * j)));
            return null;
        }
        f11862j.f11864f = aVar.f11864f;
        aVar.f11864f = null;
        return aVar;
    }

    /* renamed from: g */
    public final void mo28022g() {
        if (!this.f11863e) {
            long f = mo28144f();
            boolean d = mo28118d();
            if (f != 0 || d) {
                this.f11863e = true;
                m18077a(this, f, d);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit");
    }

    /* renamed from: h */
    public final boolean mo28023h() {
        if (!this.f11863e) {
            return false;
        }
        this.f11863e = false;
        return m18078a(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo27584i() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public IOException mo27583b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* renamed from: a */
    private static synchronized boolean m18078a(AsyncTimeout aVar) {
        synchronized (AsyncTimeout.class) {
            for (AsyncTimeout aVar2 = f11862j; aVar2 != null; aVar2 = aVar2.f11864f) {
                if (aVar2.f11864f == aVar) {
                    aVar2.f11864f = aVar.f11864f;
                    aVar.f11864f = null;
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: a */
    public final Sink mo28018a(Sink rVar) {
        return new C5040a(rVar);
    }

    /* renamed from: a */
    public final C5049s mo28019a(C5049s sVar) {
        return new C5041b(sVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo28021a(boolean z) {
        if (mo28023h() && z) {
            throw mo27583b((IOException) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final IOException mo28020a(IOException iOException) {
        if (!mo28023h()) {
            return iOException;
        }
        return mo27583b(iOException);
    }
}
