package p244j;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

/* renamed from: j.j */
public final class GzipSource implements C5049s {

    /* renamed from: P */
    private int f11883P = 0;

    /* renamed from: Q */
    private final BufferedSource f11884Q;

    /* renamed from: R */
    private final Inflater f11885R;

    /* renamed from: S */
    private final InflaterSource f11886S;

    /* renamed from: T */
    private final CRC32 f11887T = new CRC32();

    public GzipSource(C5049s sVar) {
        if (sVar != null) {
            this.f11885R = new Inflater(true);
            this.f11884Q = Okio.m18212a(sVar);
            this.f11886S = new InflaterSource(this.f11884Q, this.f11885R);
            return;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: a */
    private void m18201a() {
        this.f11884Q.mo28052e(10);
        byte g = this.f11884Q.mo28054f().mo28056g(3);
        boolean z = ((g >> 1) & 1) == 1;
        if (z) {
            m18202a(this.f11884Q.mo28054f(), 0, 10);
        }
        m18203a("ID1ID2", 8075, this.f11884Q.readShort());
        this.f11884Q.skip(8);
        if (((g >> 2) & 1) == 1) {
            this.f11884Q.mo28052e(2);
            if (z) {
                m18202a(this.f11884Q.mo28054f(), 0, 2);
            }
            long o = (long) this.f11884Q.mo28054f().mo28068o();
            this.f11884Q.mo28052e(o);
            if (z) {
                m18202a(this.f11884Q.mo28054f(), 0, o);
            }
            this.f11884Q.skip(o);
        }
        if (((g >> 3) & 1) == 1) {
            long a = this.f11884Q.mo28028a((byte) 0);
            if (a != -1) {
                if (z) {
                    m18202a(this.f11884Q.mo28054f(), 0, a + 1);
                }
                this.f11884Q.skip(a + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((g >> 4) & 1) == 1) {
            long a2 = this.f11884Q.mo28028a((byte) 0);
            if (a2 != -1) {
                if (z) {
                    m18202a(this.f11884Q.mo28054f(), 0, a2 + 1);
                }
                this.f11884Q.skip(a2 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z) {
            m18203a("FHCRC", this.f11884Q.mo28068o(), (short) ((int) this.f11887T.getValue()));
            this.f11887T.reset();
        }
    }

    /* renamed from: b */
    public long mo27415b(Buffer cVar, long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (i == 0) {
            return 0;
        } else {
            if (this.f11883P == 0) {
                m18201a();
                this.f11883P = 1;
            }
            if (this.f11883P == 1) {
                long j2 = cVar.f11873Q;
                long b = this.f11886S.mo27415b(cVar, j);
                if (b != -1) {
                    m18202a(cVar, j2, b);
                    return b;
                }
                this.f11883P = 2;
            }
            if (this.f11883P == 2) {
                m18204b();
                this.f11883P = 3;
                if (!this.f11884Q.mo28063j()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    public void close() {
        this.f11886S.close();
    }

    /* renamed from: h */
    public Timeout mo27416h() {
        return this.f11884Q.mo27416h();
    }

    /* renamed from: b */
    private void m18204b() {
        m18203a("CRC", this.f11884Q.mo28067n(), (int) this.f11887T.getValue());
        m18203a("ISIZE", this.f11884Q.mo28067n(), (int) this.f11885R.getBytesWritten());
    }

    /* renamed from: a */
    private void m18202a(Buffer cVar, long j, long j2) {
        Segment oVar = cVar.f11872P;
        while (true) {
            int i = oVar.f11907c;
            int i2 = oVar.f11906b;
            if (j < ((long) (i - i2))) {
                break;
            }
            j -= (long) (i - i2);
            oVar = oVar.f11910f;
        }
        while (j2 > 0) {
            int i3 = (int) (((long) oVar.f11906b) + j);
            int min = (int) Math.min((long) (oVar.f11907c - i3), j2);
            this.f11887T.update(oVar.f11905a, i3, min);
            j2 -= (long) min;
            oVar = oVar.f11910f;
            j = 0;
        }
    }

    /* renamed from: a */
    private void m18203a(String str, int i, int i2) {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", str, Integer.valueOf(i2), Integer.valueOf(i)));
        }
    }
}
