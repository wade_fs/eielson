package p244j;

/* renamed from: j.o */
final class Segment {

    /* renamed from: a */
    final byte[] f11905a;

    /* renamed from: b */
    int f11906b;

    /* renamed from: c */
    int f11907c;

    /* renamed from: d */
    boolean f11908d;

    /* renamed from: e */
    boolean f11909e;

    /* renamed from: f */
    Segment f11910f;

    /* renamed from: g */
    Segment f11911g;

    Segment() {
        this.f11905a = new byte[8192];
        this.f11909e = true;
        this.f11908d = false;
    }

    /* renamed from: a */
    public Segment mo28139a(Segment oVar) {
        oVar.f11911g = this;
        oVar.f11910f = this.f11910f;
        this.f11910f.f11911g = oVar;
        this.f11910f = oVar;
        return oVar;
    }

    /* renamed from: b */
    public Segment mo28142b() {
        Segment oVar = this.f11910f;
        if (oVar == this) {
            oVar = null;
        }
        Segment oVar2 = this.f11911g;
        oVar2.f11910f = this.f11910f;
        this.f11910f.f11911g = oVar2;
        this.f11910f = null;
        this.f11911g = null;
        return oVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public Segment mo28143c() {
        this.f11908d = true;
        return new Segment(this.f11905a, this.f11906b, this.f11907c, true, false);
    }

    Segment(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        this.f11905a = bArr;
        this.f11906b = i;
        this.f11907c = i2;
        this.f11908d = z;
        this.f11909e = z2;
    }

    /* renamed from: a */
    public Segment mo28138a(int i) {
        Segment oVar;
        if (i <= 0 || i > this.f11907c - this.f11906b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            oVar = mo28143c();
        } else {
            oVar = SegmentPool.m18266a();
            System.arraycopy(this.f11905a, this.f11906b, oVar.f11905a, 0, i);
        }
        oVar.f11907c = oVar.f11906b + i;
        this.f11906b += i;
        this.f11911g.mo28139a(oVar);
        return oVar;
    }

    /* renamed from: a */
    public void mo28140a() {
        Segment oVar = this.f11911g;
        if (oVar == this) {
            throw new IllegalStateException();
        } else if (oVar.f11909e) {
            int i = this.f11907c - this.f11906b;
            if (i <= (8192 - oVar.f11907c) + (oVar.f11908d ? 0 : oVar.f11906b)) {
                mo28141a(this.f11911g, i);
                mo28142b();
                SegmentPool.m18267a(this);
            }
        }
    }

    /* renamed from: a */
    public void mo28141a(Segment oVar, int i) {
        if (oVar.f11909e) {
            int i2 = oVar.f11907c;
            if (i2 + i > 8192) {
                if (!oVar.f11908d) {
                    int i3 = oVar.f11906b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = oVar.f11905a;
                        System.arraycopy(bArr, i3, bArr, 0, i2 - i3);
                        oVar.f11907c -= oVar.f11906b;
                        oVar.f11906b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            System.arraycopy(this.f11905a, this.f11906b, oVar.f11905a, oVar.f11907c, i);
            oVar.f11907c += i;
            this.f11906b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
