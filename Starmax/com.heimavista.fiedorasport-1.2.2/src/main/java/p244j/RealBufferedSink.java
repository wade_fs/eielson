package p244j;

import java.nio.ByteBuffer;

/* renamed from: j.m */
final class RealBufferedSink implements BufferedSink {

    /* renamed from: P */
    public final Buffer f11898P = new Buffer();

    /* renamed from: Q */
    public final Sink f11899Q;

    /* renamed from: R */
    boolean f11900R;

    RealBufferedSink(Sink rVar) {
        if (rVar != null) {
            this.f11899Q = rVar;
            return;
        }
        throw new NullPointerException("sink == null");
    }

    /* renamed from: a */
    public void mo27444a(Buffer cVar, long j) {
        if (!this.f11900R) {
            this.f11898P.mo27444a(cVar, j);
            mo28064k();
            return;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: c */
    public BufferedSink mo28046c(long j) {
        if (!this.f11900R) {
            this.f11898P.mo28046c(j);
            return mo28064k();
        }
        throw new IllegalStateException("closed");
    }

    public void close() {
        if (!this.f11900R) {
            try {
                if (this.f11898P.f11873Q > 0) {
                    this.f11899Q.mo27444a(this.f11898P, this.f11898P.f11873Q);
                }
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.f11899Q.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            this.f11900R = true;
            if (th != null) {
                C5051u.m18302a(th);
                throw null;
            }
        }
    }

    /* renamed from: f */
    public Buffer mo28054f() {
        return this.f11898P;
    }

    public void flush() {
        if (!this.f11900R) {
            Buffer cVar = this.f11898P;
            long j = cVar.f11873Q;
            if (j > 0) {
                this.f11899Q.mo27444a(cVar, j);
            }
            this.f11899Q.flush();
            return;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: h */
    public Timeout mo27513h() {
        return this.f11899Q.mo27513h();
    }

    public boolean isOpen() {
        return !this.f11900R;
    }

    /* renamed from: k */
    public BufferedSink mo28064k() {
        if (!this.f11900R) {
            long b = this.f11898P.mo28042b();
            if (b > 0) {
                this.f11899Q.mo27444a(this.f11898P, b);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public String toString() {
        return "buffer(" + this.f11899Q + ")";
    }

    public BufferedSink write(byte[] bArr) {
        if (!this.f11900R) {
            this.f11898P.write(bArr);
            mo28064k();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public BufferedSink writeByte(int i) {
        if (!this.f11900R) {
            this.f11898P.writeByte(i);
            mo28064k();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public BufferedSink writeInt(int i) {
        if (!this.f11900R) {
            this.f11898P.writeInt(i);
            return mo28064k();
        }
        throw new IllegalStateException("closed");
    }

    public BufferedSink writeShort(int i) {
        if (!this.f11900R) {
            this.f11898P.writeShort(i);
            mo28064k();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: f */
    public BufferedSink mo28055f(long j) {
        if (!this.f11900R) {
            this.f11898P.mo28055f(j);
            mo28064k();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: a */
    public BufferedSink mo28033a(String str) {
        if (!this.f11900R) {
            this.f11898P.mo28033a(str);
            return mo28064k();
        }
        throw new IllegalStateException("closed");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.write(byte[], int, int):j.c
     arg types: [byte[], int, int]
     candidates:
      j.c.write(byte[], int, int):j.d
      j.d.write(byte[], int, int):j.d
      j.c.write(byte[], int, int):j.c */
    public BufferedSink write(byte[] bArr, int i, int i2) {
        if (!this.f11900R) {
            this.f11898P.write(bArr, i, i2);
            mo28064k();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public int write(ByteBuffer byteBuffer) {
        if (!this.f11900R) {
            int write = this.f11898P.write(byteBuffer);
            mo28064k();
            return write;
        }
        throw new IllegalStateException("closed");
    }
}
