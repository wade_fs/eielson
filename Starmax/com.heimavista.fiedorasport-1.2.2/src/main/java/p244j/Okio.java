package p244j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: j.l */
public final class Okio {

    /* renamed from: a */
    static final Logger f11892a = Logger.getLogger(Okio.class.getName());

    /* renamed from: j.l$a */
    /* compiled from: Okio */
    class C5044a implements Sink {

        /* renamed from: P */
        final /* synthetic */ Timeout f11893P;

        /* renamed from: Q */
        final /* synthetic */ OutputStream f11894Q;

        C5044a(Timeout tVar, OutputStream outputStream) {
            this.f11893P = tVar;
            this.f11894Q = outputStream;
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            C5051u.m18301a(cVar.f11873Q, 0, j);
            while (j > 0) {
                this.f11893P.mo28119e();
                Segment oVar = cVar.f11872P;
                int min = (int) Math.min(j, (long) (oVar.f11907c - oVar.f11906b));
                this.f11894Q.write(oVar.f11905a, oVar.f11906b, min);
                oVar.f11906b += min;
                long j2 = (long) min;
                j -= j2;
                cVar.f11873Q -= j2;
                if (oVar.f11906b == oVar.f11907c) {
                    cVar.f11872P = oVar.mo28142b();
                    SegmentPool.m18267a(oVar);
                }
            }
        }

        public void close() {
            this.f11894Q.close();
        }

        public void flush() {
            this.f11894Q.flush();
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return this.f11893P;
        }

        public String toString() {
            return "sink(" + this.f11894Q + ")";
        }
    }

    /* renamed from: j.l$b */
    /* compiled from: Okio */
    class C5045b implements C5049s {

        /* renamed from: P */
        final /* synthetic */ Timeout f11895P;

        /* renamed from: Q */
        final /* synthetic */ InputStream f11896Q;

        C5045b(Timeout tVar, InputStream inputStream) {
            this.f11895P = tVar;
            this.f11896Q = inputStream;
        }

        /* renamed from: b */
        public long mo27415b(Buffer cVar, long j) {
            int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (i == 0) {
                return 0;
            } else {
                try {
                    this.f11895P.mo28119e();
                    Segment b = cVar.mo28043b(1);
                    int read = this.f11896Q.read(b.f11905a, b.f11907c, (int) Math.min(j, (long) (8192 - b.f11907c)));
                    if (read == -1) {
                        return -1;
                    }
                    b.f11907c += read;
                    long j2 = (long) read;
                    cVar.f11873Q += j2;
                    return j2;
                } catch (AssertionError e) {
                    if (Okio.m18220a(e)) {
                        throw new IOException(e);
                    }
                    throw e;
                }
            }
        }

        public void close() {
            this.f11896Q.close();
        }

        /* renamed from: h */
        public Timeout mo27416h() {
            return this.f11895P;
        }

        public String toString() {
            return "source(" + this.f11896Q + ")";
        }
    }

    /* renamed from: j.l$c */
    /* compiled from: Okio */
    class C5046c implements Sink {
        C5046c() {
        }

        /* renamed from: a */
        public void mo27444a(Buffer cVar, long j) {
            cVar.skip(j);
        }

        public void close() {
        }

        public void flush() {
        }

        /* renamed from: h */
        public Timeout mo27513h() {
            return Timeout.f11916d;
        }
    }

    /* renamed from: j.l$d */
    /* compiled from: Okio */
    class C5047d extends AsyncTimeout {

        /* renamed from: k */
        final /* synthetic */ Socket f11897k;

        C5047d(Socket socket) {
            this.f11897k = socket;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public IOException mo27583b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.lang.AssertionError]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* access modifiers changed from: protected */
        /* renamed from: i */
        public void mo27584i() {
            try {
                this.f11897k.close();
            } catch (Exception e) {
                Logger logger = Okio.f11892a;
                Level level = Level.WARNING;
                logger.log(level, "Failed to close timed out socket " + this.f11897k, (Throwable) e);
            } catch (AssertionError e2) {
                if (Okio.m18220a(e2)) {
                    Logger logger2 = Okio.f11892a;
                    Level level2 = Level.WARNING;
                    logger2.log(level2, "Failed to close timed out socket " + this.f11897k, (Throwable) e2);
                    return;
                }
                throw e2;
            }
        }
    }

    private Okio() {
    }

    /* renamed from: a */
    public static BufferedSource m18212a(C5049s sVar) {
        return new RealBufferedSource(sVar);
    }

    /* renamed from: b */
    public static Sink m18221b(File file) {
        if (file != null) {
            return m18215a(new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: c */
    public static C5049s m18224c(File file) {
        if (file != null) {
            return m18218a(new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: a */
    public static BufferedSink m18211a(Sink rVar) {
        return new RealBufferedSink(rVar);
    }

    /* renamed from: a */
    public static Sink m18215a(OutputStream outputStream) {
        return m18216a(outputStream, new Timeout());
    }

    /* renamed from: b */
    public static C5049s m18222b(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getInputStream() != null) {
            AsyncTimeout c = m18223c(socket);
            return c.mo28019a(m18219a(socket.getInputStream(), c));
        } else {
            throw new IOException("socket's input stream == null");
        }
    }

    /* renamed from: c */
    private static AsyncTimeout m18223c(Socket socket) {
        return new C5047d(socket);
    }

    /* renamed from: a */
    private static Sink m18216a(OutputStream outputStream, Timeout tVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (tVar != null) {
            return new C5044a(tVar, outputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* renamed from: a */
    public static Sink m18217a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getOutputStream() != null) {
            AsyncTimeout c = m18223c(socket);
            return c.mo28018a(m18216a(socket.getOutputStream(), c));
        } else {
            throw new IOException("socket's output stream == null");
        }
    }

    /* renamed from: a */
    public static C5049s m18218a(InputStream inputStream) {
        return m18219a(inputStream, new Timeout());
    }

    /* renamed from: a */
    private static C5049s m18219a(InputStream inputStream, Timeout tVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (tVar != null) {
            return new C5045b(tVar, inputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* renamed from: a */
    public static Sink m18214a(File file) {
        if (file != null) {
            return m18215a(new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: a */
    public static Sink m18213a() {
        return new C5046c();
    }

    /* renamed from: a */
    static boolean m18220a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
