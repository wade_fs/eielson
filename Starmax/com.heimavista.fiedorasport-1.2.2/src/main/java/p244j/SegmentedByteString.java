package p244j;

import java.util.Arrays;

/* renamed from: j.q */
final class SegmentedByteString extends ByteString {

    /* renamed from: U */
    final transient byte[][] f11914U;

    /* renamed from: V */
    final transient int[] f11915V;

    SegmentedByteString(Buffer cVar, int i) {
        super(null);
        C5051u.m18301a(cVar.f11873Q, 0, (long) i);
        int i2 = 0;
        Segment oVar = cVar.f11872P;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            int i5 = oVar.f11907c;
            int i6 = oVar.f11906b;
            if (i5 != i6) {
                i3 += i5 - i6;
                i4++;
                oVar = oVar.f11910f;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        this.f11914U = new byte[i4][];
        this.f11915V = new int[(i4 * 2)];
        Segment oVar2 = cVar.f11872P;
        int i7 = 0;
        while (i2 < i) {
            this.f11914U[i7] = oVar2.f11905a;
            i2 += oVar2.f11907c - oVar2.f11906b;
            if (i2 > i) {
                i2 = i;
            }
            int[] iArr = this.f11915V;
            iArr[i7] = i2;
            iArr[this.f11914U.length + i7] = oVar2.f11906b;
            oVar2.f11908d = true;
            i7++;
            oVar2 = oVar2.f11910f;
        }
    }

    /* renamed from: b */
    private int m18268b(int i) {
        int binarySearch = Arrays.binarySearch(this.f11915V, 0, this.f11914U.length, i + 1);
        return binarySearch >= 0 ? binarySearch : ~binarySearch;
    }

    /* renamed from: p */
    private ByteString m18269p() {
        return new ByteString(mo28106n());
    }

    private Object writeReplace() {
        return m18269p();
    }

    /* renamed from: a */
    public String mo28092a() {
        return m18269p().mo28092a();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString fVar = (ByteString) obj;
            if (super.mo28104l() != mo28104l() || !mo28094a(0, super, 0, mo28104l())) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* renamed from: h */
    public String mo28099h() {
        return m18269p().mo28099h();
    }

    public int hashCode() {
        int i = super.f11878Q;
        if (i != 0) {
            return i;
        }
        int length = this.f11914U.length;
        int i2 = 0;
        int i3 = 1;
        int i4 = 0;
        while (i2 < length) {
            byte[] bArr = this.f11914U[i2];
            int[] iArr = this.f11915V;
            int i5 = iArr[length + i2];
            int i6 = iArr[i2];
            int i7 = (i6 - i4) + i5;
            while (i5 < i7) {
                i3 = (i3 * 31) + bArr[i5];
                i5++;
            }
            i2++;
            i4 = i6;
        }
        super.f11878Q = i3;
        return i3;
    }

    /* renamed from: i */
    public ByteString mo28101i() {
        return m18269p().mo28101i();
    }

    /* renamed from: j */
    public ByteString mo28102j() {
        return m18269p().mo28102j();
    }

    /* renamed from: k */
    public ByteString mo28103k() {
        return m18269p().mo28103k();
    }

    /* renamed from: l */
    public int mo28104l() {
        return this.f11915V[this.f11914U.length - 1];
    }

    /* renamed from: m */
    public ByteString mo28105m() {
        return m18269p().mo28105m();
    }

    /* renamed from: n */
    public byte[] mo28106n() {
        int[] iArr = this.f11915V;
        byte[][] bArr = this.f11914U;
        byte[] bArr2 = new byte[iArr[bArr.length - 1]];
        int length = bArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int[] iArr2 = this.f11915V;
            int i3 = iArr2[length + i];
            int i4 = iArr2[i];
            System.arraycopy(this.f11914U[i], i3, bArr2, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr2;
    }

    /* renamed from: o */
    public String mo28107o() {
        return m18269p().mo28107o();
    }

    public String toString() {
        return m18269p().toString();
    }

    /* renamed from: a */
    public ByteString mo28091a(int i, int i2) {
        return m18269p().mo28091a(i, i2);
    }

    /* renamed from: a */
    public byte mo28089a(int i) {
        int i2;
        C5051u.m18301a((long) this.f11915V[this.f11914U.length - 1], (long) i, 1);
        int b = m18268b(i);
        if (b == 0) {
            i2 = 0;
        } else {
            i2 = this.f11915V[b - 1];
        }
        int[] iArr = this.f11915V;
        byte[][] bArr = this.f11914U;
        return bArr[b][(i - i2) + iArr[bArr.length + b]];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28093a(Buffer cVar) {
        int length = this.f11914U.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int[] iArr = this.f11915V;
            int i3 = iArr[length + i];
            int i4 = iArr[i];
            Segment oVar = new Segment(this.f11914U[i], i3, (i3 + i4) - i2, true, false);
            Segment oVar2 = cVar.f11872P;
            if (oVar2 == null) {
                oVar.f11911g = oVar;
                oVar.f11910f = oVar;
                cVar.f11872P = oVar;
            } else {
                oVar2.f11911g.mo28139a(oVar);
            }
            i++;
            i2 = i4;
        }
        cVar.f11873Q += (long) i2;
    }

    /* renamed from: a */
    public boolean mo28094a(int i, ByteString fVar, int i2, int i3) {
        int i4;
        if (i < 0 || i > mo28104l() - i3) {
            return false;
        }
        int b = m18268b(i);
        while (i3 > 0) {
            if (b == 0) {
                i4 = 0;
            } else {
                i4 = this.f11915V[b - 1];
            }
            int min = Math.min(i3, ((this.f11915V[b] - i4) + i4) - i);
            int[] iArr = this.f11915V;
            byte[][] bArr = this.f11914U;
            if (!super.mo28095a(i2, bArr[b], (i - i4) + iArr[bArr.length + b], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b++;
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo28095a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        if (i < 0 || i > mo28104l() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int b = m18268b(i);
        while (i3 > 0) {
            if (b == 0) {
                i4 = 0;
            } else {
                i4 = this.f11915V[b - 1];
            }
            int min = Math.min(i3, ((this.f11915V[b] - i4) + i4) - i);
            int[] iArr = this.f11915V;
            byte[][] bArr2 = this.f11914U;
            if (!C5051u.m18303a(bArr2[b], (i - i4) + iArr[bArr2.length + b], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b++;
        }
        return true;
    }
}
