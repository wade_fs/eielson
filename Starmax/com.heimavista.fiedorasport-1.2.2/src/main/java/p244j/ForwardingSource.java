package p244j;

/* renamed from: j.h */
public abstract class ForwardingSource implements C5049s {

    /* renamed from: P */
    private final C5049s f11881P;

    public ForwardingSource(C5049s sVar) {
        if (sVar != null) {
            this.f11881P = sVar;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    /* renamed from: a */
    public final C5049s mo28110a() {
        return this.f11881P;
    }

    /* renamed from: b */
    public long mo27415b(Buffer cVar, long j) {
        return this.f11881P.mo27415b(cVar, j);
    }

    public void close() {
        this.f11881P.close();
    }

    /* renamed from: h */
    public Timeout mo27416h() {
        return this.f11881P.mo27416h();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f11881P.toString() + ")";
    }
}
