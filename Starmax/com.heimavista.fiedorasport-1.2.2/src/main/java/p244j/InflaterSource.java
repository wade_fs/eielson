package p244j;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/* renamed from: j.k */
public final class InflaterSource implements C5049s {

    /* renamed from: P */
    private final BufferedSource f11888P;

    /* renamed from: Q */
    private final Inflater f11889Q;

    /* renamed from: R */
    private int f11890R;

    /* renamed from: S */
    private boolean f11891S;

    InflaterSource(BufferedSource eVar, Inflater inflater) {
        if (eVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater != null) {
            this.f11888P = eVar;
            this.f11889Q = inflater;
        } else {
            throw new IllegalArgumentException("inflater == null");
        }
    }

    /* renamed from: a */
    public boolean mo28121a() {
        if (!this.f11889Q.needsInput()) {
            return false;
        }
        m18207b();
        if (this.f11889Q.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f11888P.mo28063j()) {
            return true;
        } else {
            Segment oVar = this.f11888P.mo28054f().f11872P;
            int i = oVar.f11907c;
            int i2 = oVar.f11906b;
            this.f11890R = i - i2;
            this.f11889Q.setInput(oVar.f11905a, i2, this.f11890R);
            return false;
        }
    }

    /* renamed from: b */
    public long mo27415b(Buffer cVar, long j) {
        Segment b;
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f11891S) {
            throw new IllegalStateException("closed");
        } else if (i == 0) {
            return 0;
        } else {
            while (true) {
                boolean a = mo28121a();
                try {
                    b = cVar.mo28043b(1);
                    int inflate = this.f11889Q.inflate(b.f11905a, b.f11907c, (int) Math.min(j, (long) (8192 - b.f11907c)));
                    if (inflate > 0) {
                        b.f11907c += inflate;
                        long j2 = (long) inflate;
                        cVar.f11873Q += j2;
                        return j2;
                    } else if (this.f11889Q.finished()) {
                        break;
                    } else if (this.f11889Q.needsDictionary()) {
                        break;
                    } else if (a) {
                        throw new EOFException("source exhausted prematurely");
                    }
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            }
            m18207b();
            if (b.f11906b != b.f11907c) {
                return -1;
            }
            cVar.f11872P = b.mo28142b();
            SegmentPool.m18267a(b);
            return -1;
        }
    }

    public void close() {
        if (!this.f11891S) {
            this.f11889Q.end();
            this.f11891S = true;
            this.f11888P.close();
        }
    }

    /* renamed from: h */
    public Timeout mo27416h() {
        return this.f11888P.mo27416h();
    }

    /* renamed from: b */
    private void m18207b() {
        int i = this.f11890R;
        if (i != 0) {
            int remaining = i - this.f11889Q.getRemaining();
            this.f11890R -= remaining;
            this.f11888P.skip((long) remaining);
        }
    }
}
