package p224f.p225b;

import p224f.Lazy;
import p226g.p227a.Provider;

/* renamed from: f.b.a */
public final class DoubleCheck<T> implements Provider<T>, Lazy<T> {

    /* renamed from: c */
    private static final Object f11190c = new Object();

    /* renamed from: a */
    private volatile Provider<T> f11191a;

    /* renamed from: b */
    private volatile Object f11192b = f11190c;

    private DoubleCheck(Provider<T> aVar) {
        this.f11191a = aVar;
    }

    /* renamed from: a */
    public static Object m17210a(Object obj, Object obj2) {
        if (!(obj != f11190c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    public T get() {
        T t = this.f11192b;
        if (t == f11190c) {
            synchronized (this) {
                t = this.f11192b;
                if (t == f11190c) {
                    t = this.f11191a.get();
                    m17210a(this.f11192b, t);
                    this.f11192b = t;
                    this.f11191a = null;
                }
            }
        }
        return t;
    }

    /* renamed from: a */
    public static <P extends Provider<T>, T> Provider<T> m17209a(P p) {
        C4936d.m17212a(p);
        if (p instanceof DoubleCheck) {
            return p;
        }
        return new DoubleCheck(p);
    }
}
