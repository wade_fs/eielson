package p224f.p225b;

import p224f.Lazy;

/* renamed from: f.b.c */
public final class InstanceFactory<T> implements Factory<T>, Lazy<T> {

    /* renamed from: a */
    private final T f11193a;

    static {
        new InstanceFactory(null);
    }

    private InstanceFactory(T t) {
        this.f11193a = t;
    }

    /* renamed from: a */
    public static <T> Factory<T> m17211a(T t) {
        C4936d.m17213a(t, "instance cannot be null");
        return new InstanceFactory(t);
    }

    public T get() {
        return this.f11193a;
    }
}
