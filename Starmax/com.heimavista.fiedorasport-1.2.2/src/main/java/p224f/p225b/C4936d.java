package p224f.p225b;

/* renamed from: f.b.d */
/* compiled from: Preconditions */
public final class C4936d {
    /* renamed from: a */
    public static <T> T m17212a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    /* renamed from: a */
    public static <T> T m17213a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: a */
    public static <T> void m17214a(Object obj, Class cls) {
        if (obj == null) {
            throw new IllegalStateException(cls.getCanonicalName() + " must be set");
        }
    }
}
