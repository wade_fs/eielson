package cat.ereza.customactivityoncrash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import cat.ereza.customactivityoncrash.activity.DefaultErrorActivity;
import cat.ereza.customactivityoncrash.p018b.CaocConfig;
import com.facebook.internal.AnalyticsEvents;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipFile;

/* renamed from: cat.ereza.customactivityoncrash.a */
public final class CustomActivityOnCrash {
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a */
    public static Application f452a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static CaocConfig f453b = new CaocConfig();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final Deque<String> f454c = new ArrayDeque(50);
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static WeakReference<Activity> f455d = new WeakReference<>(null);
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static long f456e = 0;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static boolean f457f = true;

    /* renamed from: cat.ereza.customactivityoncrash.a$a */
    /* compiled from: CustomActivityOnCrash */
    class C0795a implements Thread.UncaughtExceptionHandler {

        /* renamed from: a */
        final /* synthetic */ Thread.UncaughtExceptionHandler f458a;

        C0795a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.f458a = uncaughtExceptionHandler;
        }

        public void uncaughtException(@NonNull Thread thread, @NonNull Throwable th) {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler;
            if (CustomActivityOnCrash.f453b.mo9458j()) {
                Log.e("CustomActivityOnCrash", "App has crashed, executing CustomActivityOnCrash's UncaughtExceptionHandler", th);
                if (CustomActivityOnCrash.m733k(CustomActivityOnCrash.f452a)) {
                    Log.e("CustomActivityOnCrash", "App already crashed recently, not starting custom error activity because we could enter a restart loop. Are you sure that your app does not crash directly on init?", th);
                    Thread.UncaughtExceptionHandler uncaughtExceptionHandler2 = this.f458a;
                    if (uncaughtExceptionHandler2 != null) {
                        uncaughtExceptionHandler2.uncaughtException(thread, th);
                        return;
                    }
                } else {
                    CustomActivityOnCrash.m714b(CustomActivityOnCrash.f452a, new Date().getTime());
                    Class<? extends Activity> e = CustomActivityOnCrash.f453b.mo9453e();
                    if (e == null) {
                        e = CustomActivityOnCrash.m729i(CustomActivityOnCrash.f452a);
                    }
                    if (CustomActivityOnCrash.m715b(th, e)) {
                        Log.e("CustomActivityOnCrash", "Your application class or your error activity have crashed, the custom activity will not be launched!");
                        Thread.UncaughtExceptionHandler uncaughtExceptionHandler3 = this.f458a;
                        if (uncaughtExceptionHandler3 != null) {
                            uncaughtExceptionHandler3.uncaughtException(thread, th);
                            return;
                        }
                    } else if (CustomActivityOnCrash.f453b.mo9451a() == 1 || !CustomActivityOnCrash.f457f || CustomActivityOnCrash.f456e >= new Date().getTime() - 500) {
                        Intent intent = new Intent(CustomActivityOnCrash.f452a, e);
                        StringWriter stringWriter = new StringWriter();
                        th.printStackTrace(new PrintWriter(stringWriter));
                        String stringWriter2 = stringWriter.toString();
                        if (stringWriter2.length() > 131071) {
                            stringWriter2 = stringWriter2.substring(0, 131047) + " [stack trace too large]";
                        }
                        intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_STACK_TRACE", stringWriter2);
                        if (CustomActivityOnCrash.f453b.mo9462n()) {
                            StringBuilder sb = new StringBuilder();
                            while (!CustomActivityOnCrash.f454c.isEmpty()) {
                                sb.append((String) CustomActivityOnCrash.f454c.poll());
                            }
                            intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_ACTIVITY_LOG", sb.toString());
                        }
                        if (CustomActivityOnCrash.f453b.mo9461m() && CustomActivityOnCrash.f453b.mo9457i() == null) {
                            CustomActivityOnCrash.f453b.mo9452a(CustomActivityOnCrash.m731j(CustomActivityOnCrash.f452a));
                        }
                        intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_CONFIG", CustomActivityOnCrash.f453b);
                        intent.setFlags(268468224);
                        if (CustomActivityOnCrash.f453b.mo9455g() != null) {
                            CustomActivityOnCrash.f453b.mo9455g().mo9444b();
                        }
                        CustomActivityOnCrash.f452a.startActivity(intent);
                    } else if (CustomActivityOnCrash.f453b.mo9451a() == 2 && (uncaughtExceptionHandler = this.f458a) != null) {
                        uncaughtExceptionHandler.uncaughtException(thread, th);
                        return;
                    }
                }
                Activity activity = (Activity) CustomActivityOnCrash.f455d.get();
                if (activity != null) {
                    activity.finish();
                    CustomActivityOnCrash.f455d.clear();
                }
                CustomActivityOnCrash.m732j();
                return;
            }
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler4 = this.f458a;
            if (uncaughtExceptionHandler4 != null) {
                uncaughtExceptionHandler4.uncaughtException(thread, th);
            }
        }
    }

    /* renamed from: cat.ereza.customactivityoncrash.a$b */
    /* compiled from: CustomActivityOnCrash */
    class C0796b implements Application.ActivityLifecycleCallbacks {

        /* renamed from: P */
        int f459P = 0;

        /* renamed from: Q */
        final DateFormat f460Q = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        C0796b() {
        }

        public void onActivityCreated(@NonNull Activity activity, Bundle bundle) {
            if (activity.getClass() != CustomActivityOnCrash.f453b.mo9453e()) {
                WeakReference unused = CustomActivityOnCrash.f455d = new WeakReference(activity);
                long unused2 = CustomActivityOnCrash.f456e = new Date().getTime();
            }
            if (CustomActivityOnCrash.f453b.mo9462n()) {
                Deque g = CustomActivityOnCrash.f454c;
                g.add(this.f460Q.format(new Date()) + ": " + activity.getClass().getSimpleName() + " created\n");
            }
        }

        public void onActivityDestroyed(@NonNull Activity activity) {
            if (CustomActivityOnCrash.f453b.mo9462n()) {
                Deque g = CustomActivityOnCrash.f454c;
                g.add(this.f460Q.format(new Date()) + ": " + activity.getClass().getSimpleName() + " destroyed\n");
            }
        }

        public void onActivityPaused(@NonNull Activity activity) {
            if (CustomActivityOnCrash.f453b.mo9462n()) {
                Deque g = CustomActivityOnCrash.f454c;
                g.add(this.f460Q.format(new Date()) + ": " + activity.getClass().getSimpleName() + " paused\n");
            }
        }

        public void onActivityResumed(@NonNull Activity activity) {
            if (CustomActivityOnCrash.f453b.mo9462n()) {
                Deque g = CustomActivityOnCrash.f454c;
                g.add(this.f460Q.format(new Date()) + ": " + activity.getClass().getSimpleName() + " resumed\n");
            }
        }

        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {
        }

        public void onActivityStarted(@NonNull Activity activity) {
            boolean z = true;
            this.f459P++;
            if (this.f459P != 0) {
                z = false;
            }
            boolean unused = CustomActivityOnCrash.f457f = z;
        }

        public void onActivityStopped(@NonNull Activity activity) {
            boolean z = true;
            this.f459P--;
            if (this.f459P != 0) {
                z = false;
            }
            boolean unused = CustomActivityOnCrash.f457f = z;
        }
    }

    /* renamed from: cat.ereza.customactivityoncrash.a$c */
    /* compiled from: CustomActivityOnCrash */
    public interface C0797c extends Serializable {
        /* renamed from: b */
        void mo9444b();

        /* renamed from: c */
        void mo9445c();

        /* renamed from: d */
        void mo9446d();
    }

    @NonNull
    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: h */
    public static CaocConfig m727h() {
        return f453b;
    }

    @NonNull
    /* renamed from: i */
    private static String m730i() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        if (str2.startsWith(str)) {
            return m701a(str2);
        }
        return m701a(str) + " " + str2;
    }

    /* access modifiers changed from: private */
    @Nullable
    /* renamed from: j */
    public static Class<? extends Activity> m731j(@NonNull Context context) {
        Class<? extends Activity> g = m725g(context);
        return g == null ? m724f(context) : g;
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public static boolean m733k(@NonNull Context context) {
        long e = m721e(context);
        long time = new Date().getTime();
        return e <= time && time - e < ((long) f453b.mo9456h());
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: l */
    public static void m734l(@Nullable Context context) {
        if (context == null) {
            try {
                Log.e("CustomActivityOnCrash", "Install failed: context is null!");
            } catch (Throwable th) {
                Log.e("CustomActivityOnCrash", "An unknown error occurred while installing CustomActivityOnCrash, it may not have been properly initialized. Please report this as a bug if needed.", th);
            }
        } else {
            Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtExceptionHandler == null || !defaultUncaughtExceptionHandler.getClass().getName().startsWith("cat.ereza.customactivityoncrash")) {
                if (defaultUncaughtExceptionHandler != null && !defaultUncaughtExceptionHandler.getClass().getName().startsWith("com.android.internal.os")) {
                    Log.e("CustomActivityOnCrash", "IMPORTANT WARNING! You already have an UncaughtExceptionHandler, are you sure this is correct? If you use a custom UncaughtExceptionHandler, you must initialize it AFTER CustomActivityOnCrash! Installing anyway, but your original handler will not be called.");
                }
                f452a = (Application) context.getApplicationContext();
                Thread.setDefaultUncaughtExceptionHandler(new C0795a(defaultUncaughtExceptionHandler));
                f452a.registerActivityLifecycleCallbacks(new C0796b());
            } else {
                Log.e("CustomActivityOnCrash", "CustomActivityOnCrash was already installed, doing nothing!");
            }
            Log.i("CustomActivityOnCrash", "CustomActivityOnCrash has been installed.");
        }
    }

    @Nullable
    /* renamed from: d */
    private static Class<? extends Activity> m719d(@NonNull Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent().setAction("cat.ereza.customactivityoncrash.ERROR").setPackage(context.getPackageName()), 64);
        if (queryIntentActivities.size() <= 0) {
            return null;
        }
        try {
            return Class.forName(queryIntentActivities.get(0).activityInfo.name);
        } catch (ClassNotFoundException e) {
            Log.e("CustomActivityOnCrash", "Failed when resolving the error activity class via intent filter, stack trace follows!", e);
            return null;
        }
    }

    /* renamed from: e */
    private static long m721e(@NonNull Context context) {
        return context.getSharedPreferences("custom_activity_on_crash", 0).getLong("last_crash_timestamp", -1);
    }

    @Nullable
    /* renamed from: f */
    private static Class<? extends Activity> m724f(@NonNull Context context) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntentForPackage == null || launchIntentForPackage.getComponent() == null) {
            return null;
        }
        try {
            return Class.forName(launchIntentForPackage.getComponent().getClassName());
        } catch (ClassNotFoundException e) {
            Log.e("CustomActivityOnCrash", "Failed when resolving the restart activity class via getLaunchIntentForPackage, stack trace follows!", e);
            return null;
        }
    }

    @Nullable
    /* renamed from: g */
    private static Class<? extends Activity> m725g(@NonNull Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent().setAction("cat.ereza.customactivityoncrash.RESTART").setPackage(context.getPackageName()), 64);
        if (queryIntentActivities.size() <= 0) {
            return null;
        }
        try {
            return Class.forName(queryIntentActivities.get(0).activityInfo.name);
        } catch (ClassNotFoundException e) {
            Log.e("CustomActivityOnCrash", "Failed when resolving the restart activity class via intent filter, stack trace follows!", e);
            return null;
        }
    }

    @NonNull
    /* renamed from: h */
    private static String m728h(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
    }

    @Nullable
    /* renamed from: b */
    public static CaocConfig m711b(@NonNull Intent intent) {
        CaocConfig aVar = (CaocConfig) intent.getSerializableExtra("cat.ereza.customactivityoncrash.EXTRA_CONFIG");
        if (!(aVar == null || !aVar.mo9459k() || m717c(intent) == null)) {
            Log.e("CustomActivityOnCrash", "The previous app process crashed. This is the stack trace of the crash:\n" + m717c(intent));
        }
        return aVar;
    }

    @Nullable
    /* renamed from: c */
    public static String m717c(@NonNull Intent intent) {
        return intent.getStringExtra("cat.ereza.customactivityoncrash.EXTRA_STACK_TRACE");
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public static void m732j() {
        Process.killProcess(Process.myPid());
        System.exit(10);
    }

    /* access modifiers changed from: private */
    @NonNull
    /* renamed from: i */
    public static Class<? extends Activity> m729i(@NonNull Context context) {
        Class<? extends Activity> d = m719d(context);
        return d == null ? DefaultErrorActivity.class : d;
    }

    /* renamed from: b */
    public static void m713b(@NonNull Activity activity, @NonNull CaocConfig aVar) {
        m703a(activity, new Intent(activity, aVar.mo9457i()), aVar);
    }

    @Nullable
    /* renamed from: a */
    public static String m700a(@NonNull Intent intent) {
        return intent.getStringExtra("cat.ereza.customactivityoncrash.EXTRA_ACTIVITY_LOG");
    }

    @NonNull
    /* renamed from: a */
    public static String m698a(@NonNull Context context, @NonNull Intent intent) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String a = m699a(context, simpleDateFormat);
        String str = "" + "Build version: " + m728h(context) + " \n";
        if (a != null) {
            str = str + "Build date: " + a + " \n";
        }
        String str2 = ((((str + "Current date: " + simpleDateFormat.format(date) + " \n") + "Device: " + m730i() + " \n") + "OS version: Android " + Build.VERSION.RELEASE + " (SDK " + Build.VERSION.SDK_INT + ") \n \n") + "Stack trace:  \n") + m717c(intent);
        String a2 = m700a(intent);
        if (a2 == null) {
            return str2;
        }
        return (str2 + "\nUser actions: \n") + a2;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static boolean m715b(@NonNull Throwable th, @NonNull Class<? extends Activity> cls) {
        String str;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/self/cmdline"));
            str = bufferedReader.readLine().trim();
            bufferedReader.close();
        } catch (IOException unused) {
            str = null;
        }
        if (str != null && str.endsWith(":error_activity")) {
            return true;
        }
        do {
            StackTraceElement[] stackTrace = th.getStackTrace();
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (stackTraceElement.getClassName().equals("android.app.ActivityThread") && stackTraceElement.getMethodName().equals("handleBindApplication")) {
                    return true;
                }
            }
            th = th.getCause();
        } while (th != null);
        return false;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"ApplySharedPref"})
    /* renamed from: b */
    public static void m714b(@NonNull Context context, long j) {
        context.getSharedPreferences("custom_activity_on_crash", 0).edit().putLong("last_crash_timestamp", j).commit();
    }

    /* renamed from: a */
    public static void m703a(@NonNull Activity activity, @NonNull Intent intent, @NonNull CaocConfig aVar) {
        intent.addFlags(270565376);
        if (intent.getComponent() != null) {
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
        }
        if (aVar.mo9455g() != null) {
            aVar.mo9455g().mo9446d();
        }
        activity.finish();
        activity.startActivity(intent);
        m732j();
    }

    /* renamed from: a */
    public static void m704a(@NonNull Activity activity, @NonNull CaocConfig aVar) {
        if (aVar.mo9455g() != null) {
            aVar.mo9455g().mo9445c();
        }
        activity.finish();
        m732j();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: a */
    public static void m706a(@NonNull CaocConfig aVar) {
        f453b = aVar;
    }

    @Nullable
    /* renamed from: a */
    private static String m699a(@NonNull Context context, @NonNull DateFormat dateFormat) {
        long j;
        try {
            ZipFile zipFile = new ZipFile(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir);
            j = zipFile.getEntry("classes.dex").getTime();
            zipFile.close();
        } catch (Exception unused) {
            j = 0;
        }
        if (j > 312764400000L) {
            return dateFormat.format(new Date(j));
        }
        return null;
    }

    @NonNull
    /* renamed from: a */
    private static String m701a(@Nullable String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char charAt = str.charAt(0);
        if (Character.isUpperCase(charAt)) {
            return str;
        }
        return Character.toUpperCase(charAt) + str.substring(1);
    }
}
