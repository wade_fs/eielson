package cat.ereza.customactivityoncrash.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.R$dimen;
import cat.ereza.customactivityoncrash.R$id;
import cat.ereza.customactivityoncrash.R$layout;
import cat.ereza.customactivityoncrash.R$string;
import cat.ereza.customactivityoncrash.R$style;
import cat.ereza.customactivityoncrash.R$styleable;
import cat.ereza.customactivityoncrash.p018b.CaocConfig;

public final class DefaultErrorActivity extends AppCompatActivity {

    /* renamed from: cat.ereza.customactivityoncrash.activity.DefaultErrorActivity$a */
    class C0798a implements View.OnClickListener {

        /* renamed from: P */
        final /* synthetic */ CaocConfig f461P;

        C0798a(CaocConfig aVar) {
            this.f461P = aVar;
        }

        public void onClick(View view) {
            CustomActivityOnCrash.m713b(DefaultErrorActivity.this, this.f461P);
        }
    }

    /* renamed from: cat.ereza.customactivityoncrash.activity.DefaultErrorActivity$b */
    class C0799b implements View.OnClickListener {

        /* renamed from: P */
        final /* synthetic */ CaocConfig f463P;

        C0799b(CaocConfig aVar) {
            this.f463P = aVar;
        }

        public void onClick(View view) {
            CustomActivityOnCrash.m704a(DefaultErrorActivity.this, this.f463P);
        }
    }

    /* renamed from: cat.ereza.customactivityoncrash.activity.DefaultErrorActivity$c */
    class C0800c implements View.OnClickListener {

        /* renamed from: cat.ereza.customactivityoncrash.activity.DefaultErrorActivity$c$a */
        class C0801a implements DialogInterface.OnClickListener {
            C0801a() {
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                DefaultErrorActivity.this.m738a();
            }
        }

        C0800c() {
        }

        public void onClick(View view) {
            AlertDialog.Builder title = new AlertDialog.Builder(DefaultErrorActivity.this).setTitle(R$string.customactivityoncrash_error_activity_error_details_title);
            DefaultErrorActivity defaultErrorActivity = DefaultErrorActivity.this;
            TextView textView = (TextView) title.setMessage(CustomActivityOnCrash.m698a(defaultErrorActivity, defaultErrorActivity.getIntent())).setPositiveButton(R$string.customactivityoncrash_error_activity_error_details_close, (DialogInterface.OnClickListener) null).setNeutralButton(R$string.customactivityoncrash_error_activity_error_details_copy, new C0801a()).show().findViewById(16908299);
            if (textView != null) {
                textView.setTextSize(0, DefaultErrorActivity.this.getResources().getDimension(R$dimen.customactivityoncrash_error_activity_error_details_text_size));
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"PrivateResource"})
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        TypedArray obtainStyledAttributes = obtainStyledAttributes(R$styleable.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowActionBar)) {
            setTheme(R$style.Theme_AppCompat_Light_DarkActionBar);
        }
        obtainStyledAttributes.recycle();
        setContentView(R$layout.customactivityoncrash_default_error_activity);
        Button button = (Button) findViewById(R$id.customactivityoncrash_error_activity_restart_button);
        CaocConfig b = CustomActivityOnCrash.m711b(getIntent());
        if (b == null) {
            finish();
            return;
        }
        if (!b.mo9461m() || b.mo9457i() == null) {
            button.setOnClickListener(new C0799b(b));
        } else {
            button.setText(R$string.customactivityoncrash_error_activity_restart_app);
            button.setOnClickListener(new C0798a(b));
        }
        Button button2 = (Button) findViewById(R$id.customactivityoncrash_error_activity_more_info_button);
        if (b.mo9460l()) {
            button2.setOnClickListener(new C0800c());
        } else {
            button2.setVisibility(8);
        }
        Integer f = b.mo9454f();
        ImageView imageView = (ImageView) findViewById(R$id.customactivityoncrash_error_activity_image);
        if (f != null) {
            imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), f.intValue(), getTheme()));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m738a() {
        String a = CustomActivityOnCrash.m698a(this, getIntent());
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService("clipboard");
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(ClipData.newPlainText(getString(R$string.f451x949651f5), a));
            Toast.makeText(this, R$string.customactivityoncrash_error_activity_error_details_copied, 0).show();
        }
    }
}
