package cat.ereza.customactivityoncrash.p018b;

import android.app.Activity;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.vectordrawable.graphics.drawable.PathInterpolatorCompat;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import java.io.Serializable;

/* renamed from: cat.ereza.customactivityoncrash.b.a */
public class CaocConfig implements Serializable {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public int f467P = 1;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public boolean f468Q = true;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public boolean f469R = true;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public boolean f470S = true;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public boolean f471T = true;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public boolean f472U = false;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public int f473V = PathInterpolatorCompat.MAX_NUM_POINTS;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public Integer f474W = null;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public Class<? extends Activity> f475X = null;
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public Class<? extends Activity> f476Y = null;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public CustomActivityOnCrash.C0797c f477Z = null;

    /* renamed from: cat.ereza.customactivityoncrash.b.a$a */
    /* compiled from: CaocConfig */
    public static class C0802a {

        /* renamed from: a */
        private CaocConfig f478a;

        @NonNull
        /* renamed from: b */
        public static C0802a m774b() {
            C0802a aVar = new C0802a();
            CaocConfig h = CustomActivityOnCrash.m727h();
            CaocConfig aVar2 = new CaocConfig();
            int unused = aVar2.f467P = h.f467P;
            boolean unused2 = aVar2.f468Q = h.f468Q;
            boolean unused3 = aVar2.f469R = h.f469R;
            boolean unused4 = aVar2.f470S = h.f470S;
            boolean unused5 = aVar2.f471T = h.f471T;
            boolean unused6 = aVar2.f472U = h.f472U;
            int unused7 = aVar2.f473V = h.f473V;
            Integer unused8 = aVar2.f474W = h.f474W;
            Class unused9 = aVar2.f475X = h.f475X;
            Class unused10 = aVar2.f476Y = h.f476Y;
            CustomActivityOnCrash.C0797c unused11 = aVar2.f477Z = h.f477Z;
            aVar.f478a = aVar2;
            return aVar;
        }

        @NonNull
        /* renamed from: a */
        public C0802a mo9463a(int i) {
            int unused = this.f478a.f467P = i;
            return this;
        }

        @NonNull
        /* renamed from: c */
        public C0802a mo9470c(boolean z) {
            boolean unused = this.f478a.f469R = z;
            return this;
        }

        @NonNull
        /* renamed from: d */
        public C0802a mo9471d(boolean z) {
            boolean unused = this.f478a.f472U = z;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0802a mo9465a(boolean z) {
            boolean unused = this.f478a.f468Q = z;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0802a mo9464a(@Nullable Class<? extends Activity> cls) {
            Class unused = this.f478a.f475X = cls;
            return this;
        }

        /* renamed from: a */
        public void mo9466a() {
            CustomActivityOnCrash.m706a(this.f478a);
        }

        @NonNull
        /* renamed from: b */
        public C0802a mo9469b(boolean z) {
            boolean unused = this.f478a.f471T = z;
            return this;
        }

        @NonNull
        /* renamed from: b */
        public C0802a mo9467b(int i) {
            int unused = this.f478a.f473V = i;
            return this;
        }

        @NonNull
        /* renamed from: b */
        public C0802a mo9468b(@Nullable Class<? extends Activity> cls) {
            Class unused = this.f478a.f476Y = cls;
            return this;
        }
    }

    /* renamed from: l */
    public boolean mo9460l() {
        return this.f469R;
    }

    /* renamed from: m */
    public boolean mo9461m() {
        return this.f470S;
    }

    /* renamed from: n */
    public boolean mo9462n() {
        return this.f472U;
    }

    @DrawableRes
    @Nullable
    /* renamed from: f */
    public Integer mo9454f() {
        return this.f474W;
    }

    @Nullable
    /* renamed from: g */
    public CustomActivityOnCrash.C0797c mo9455g() {
        return this.f477Z;
    }

    /* renamed from: h */
    public int mo9456h() {
        return this.f473V;
    }

    @Nullable
    /* renamed from: i */
    public Class<? extends Activity> mo9457i() {
        return this.f476Y;
    }

    /* renamed from: j */
    public boolean mo9458j() {
        return this.f468Q;
    }

    /* renamed from: k */
    public boolean mo9459k() {
        return this.f471T;
    }

    @Nullable
    /* renamed from: e */
    public Class<? extends Activity> mo9453e() {
        return this.f475X;
    }

    /* renamed from: a */
    public int mo9451a() {
        return this.f467P;
    }

    /* renamed from: a */
    public void mo9452a(@Nullable Class<? extends Activity> cls) {
        this.f476Y = cls;
    }
}
