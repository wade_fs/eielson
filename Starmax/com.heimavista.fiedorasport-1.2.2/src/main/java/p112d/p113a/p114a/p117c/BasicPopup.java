package p112d.p113a.p114a.p117c;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.annotation.StyleRes;
import p112d.p113a.p114a.p118d.C3820c;
import p112d.p113a.p114a.p118d.C3821d;

/* renamed from: d.a.a.c.a */
public abstract class BasicPopup<V extends View> implements DialogInterface.OnKeyListener, DialogInterface.OnDismissListener {

    /* renamed from: P */
    protected Activity f6802P;

    /* renamed from: Q */
    protected int f6803Q;

    /* renamed from: R */
    private Dialog f6804R;

    /* renamed from: S */
    private FrameLayout f6805S;

    /* renamed from: T */
    private boolean f6806T = false;

    public BasicPopup(Activity activity) {
        this.f6802P = activity;
        DisplayMetrics a = C3821d.m11056a(activity);
        this.f6803Q = a.widthPixels;
        int i = a.heightPixels;
        mo23079h();
    }

    /* renamed from: h */
    private void mo23079h() {
        this.f6805S = new FrameLayout(this.f6802P);
        this.f6805S.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.f6805S.setFocusable(true);
        this.f6805S.setFocusableInTouchMode(true);
        this.f6804R = new Dialog(this.f6802P);
        this.f6804R.setCanceledOnTouchOutside(true);
        this.f6804R.setCancelable(true);
        this.f6804R.setOnKeyListener(this);
        this.f6804R.setOnDismissListener(this);
        Window window = this.f6804R.getWindow();
        if (window != null) {
            window.setGravity(80);
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.requestFeature(1);
            window.setContentView(this.f6805S);
        }
        mo23122a(this.f6803Q, -2);
    }

    /* renamed from: a */
    public void mo23123a(View view) {
        this.f6805S.removeAllViews();
        this.f6805S.addView(view);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo23124b() {
        this.f6804R.dismiss();
        C3820c.m11048a(this, "popup dismiss");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23125b(View view) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract V mo23126c();

    /* renamed from: d */
    public boolean mo23127d() {
        mo23120a();
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo23128e() {
    }

    /* renamed from: f */
    public final void mo23129f() {
        if (this.f6806T) {
            this.f6804R.show();
            mo23130g();
            return;
        }
        C3820c.m11048a(this, "do something before popup show");
        mo23128e();
        View c = mo23126c();
        mo23123a(c);
        mo23125b(c);
        this.f6806T = true;
        this.f6804R.show();
        mo23130g();
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo23130g() {
        C3820c.m11048a(this, "popup show");
    }

    public void onDismiss(DialogInterface dialogInterface) {
        mo23120a();
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || i != 4) {
            return false;
        }
        mo23127d();
        return false;
    }

    /* renamed from: a */
    public void mo23121a(@StyleRes int i) {
        Window window = this.f6804R.getWindow();
        if (window != null) {
            window.setWindowAnimations(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        if (r5 == 0) goto L_0x000c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo23122a(int r4, int r5) {
        /*
            r3 = this;
            r0 = -1
            if (r4 != r0) goto L_0x0005
            int r4 = r3.f6803Q
        L_0x0005:
            r0 = -2
            if (r4 != 0) goto L_0x000e
            if (r5 != 0) goto L_0x000e
            int r4 = r3.f6803Q
        L_0x000c:
            r5 = -2
            goto L_0x0016
        L_0x000e:
            if (r4 != 0) goto L_0x0013
            int r4 = r3.f6803Q
            goto L_0x0016
        L_0x0013:
            if (r5 != 0) goto L_0x0016
            goto L_0x000c
        L_0x0016:
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r1 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)
            r0[r1] = r2
            r1 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
            r0[r1] = r2
            java.lang.String r1 = "will set popup width/height to: %s/%s"
            java.lang.String r0 = java.lang.String.format(r1, r0)
            p112d.p113a.p114a.p118d.C3820c.m11048a(r3, r0)
            android.widget.FrameLayout r0 = r3.f6805S
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            if (r0 != 0) goto L_0x003f
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams
            r0.<init>(r4, r5)
            goto L_0x0043
        L_0x003f:
            r0.width = r4
            r0.height = r5
        L_0x0043:
            android.widget.FrameLayout r4 = r3.f6805S
            r4.setLayoutParams(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p112d.p113a.p114a.p117c.BasicPopup.mo23122a(int, int):void");
    }

    /* renamed from: a */
    public void mo23120a() {
        mo23124b();
    }
}
