package p112d.p113a.p114a.p118d;

import androidx.annotation.NonNull;
import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import java.util.Arrays;
import java.util.List;

/* renamed from: d.a.a.d.b */
public class DateUtils extends android.text.format.DateUtils {
    /* renamed from: a */
    public static int m11041a(int i, int i2) {
        List asList = Arrays.asList(AppEventsConstants.EVENT_PARAM_VALUE_YES, ExifInterface.GPS_MEASUREMENT_3D, "5", "7", "8", "10", "12");
        List asList2 = Arrays.asList("4", "6", "9", "11");
        if (asList.contains(String.valueOf(i2))) {
            return 31;
        }
        if (asList2.contains(String.valueOf(i2))) {
            return 30;
        }
        if (i <= 0) {
            return 29;
        }
        if ((i % 4 != 0 || i % 100 == 0) && i % 400 != 0) {
            return 28;
        }
        return 29;
    }

    @NonNull
    /* renamed from: a */
    public static String m11043a(int i) {
        String str;
        StringBuilder sb;
        if (i < 10) {
            sb = new StringBuilder();
            str = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        } else {
            sb = new StringBuilder();
            str = "";
        }
        sb.append(str);
        sb.append(i);
        return sb.toString();
    }

    /* renamed from: a */
    public static int m11042a(@NonNull String str) {
        try {
            if (str.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                str = str.substring(1);
            }
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            C3820c.m11052b(e);
            return 0;
        }
    }
}
