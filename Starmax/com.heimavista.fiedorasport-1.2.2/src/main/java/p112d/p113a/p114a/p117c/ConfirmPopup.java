package p112d.p113a.p114a.p117c;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.view.ViewCompat;
import p112d.p113a.p114a.p118d.C3819a;

/* renamed from: d.a.a.c.b */
public abstract class ConfirmPopup<V extends View> extends BasicPopup<View> {

    /* renamed from: U */
    protected boolean f6807U = true;

    /* renamed from: V */
    protected int f6808V = -13388315;

    /* renamed from: W */
    protected int f6809W = 1;

    /* renamed from: X */
    protected int f6810X = -1;

    /* renamed from: Y */
    protected int f6811Y = 40;

    /* renamed from: Z */
    protected int f6812Z = 15;

    /* renamed from: a0 */
    protected int f6813a0 = 0;

    /* renamed from: b0 */
    protected int f6814b0 = 0;

    /* renamed from: c0 */
    protected boolean f6815c0 = true;

    /* renamed from: d0 */
    protected CharSequence f6816d0 = "";

    /* renamed from: e0 */
    protected CharSequence f6817e0 = "";

    /* renamed from: f0 */
    protected CharSequence f6818f0 = "";

    /* renamed from: g0 */
    protected int f6819g0 = -13388315;

    /* renamed from: h0 */
    protected int f6820h0 = -13388315;

    /* renamed from: i0 */
    protected int f6821i0 = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: j0 */
    protected int f6822j0 = -16611122;

    /* renamed from: k0 */
    protected int f6823k0 = 0;

    /* renamed from: l0 */
    protected int f6824l0 = 0;

    /* renamed from: m0 */
    protected int f6825m0 = 0;

    /* renamed from: n0 */
    protected int f6826n0 = -1;

    /* renamed from: o0 */
    protected TextView f6827o0;

    /* renamed from: p0 */
    protected TextView f6828p0;

    /* renamed from: q0 */
    protected View f6829q0;

    /* renamed from: r0 */
    protected View f6830r0;

    /* renamed from: s0 */
    protected View f6831s0;

    /* renamed from: t0 */
    protected View f6832t0;

    /* renamed from: d.a.a.c.b$a */
    /* compiled from: ConfirmPopup */
    class C3817a implements View.OnClickListener {
        C3817a() {
        }

        public void onClick(View view) {
            ConfirmPopup.this.mo23120a();
            ConfirmPopup.this.mo23142k();
        }
    }

    /* renamed from: d.a.a.c.b$b */
    /* compiled from: ConfirmPopup */
    class C3818b implements View.OnClickListener {
        C3818b() {
        }

        public void onClick(View view) {
            ConfirmPopup.this.mo23120a();
            ConfirmPopup.this.mo23080l();
        }
    }

    public ConfirmPopup(Activity activity) {
        super(activity);
        this.f6816d0 = activity.getString(17039360);
        this.f6817e0 = activity.getString(17039370);
    }

    /* renamed from: a */
    public void mo23134a(boolean z) {
        this.f6807U = z;
    }

    /* renamed from: b */
    public void mo23135b(@StringRes int i) {
        mo23133a(super.f6802P.getString(i));
    }

    /* renamed from: c */
    public void mo23137c(@ColorInt int i) {
        TextView textView = this.f6827o0;
        if (textView != null) {
            textView.setTextColor(i);
        } else {
            this.f6819g0 = i;
        }
    }

    /* renamed from: d */
    public void mo23138d(@StringRes int i) {
        mo23136b(super.f6802P.getString(i));
    }

    /* renamed from: e */
    public void mo23139e(@ColorInt int i) {
        TextView textView = this.f6828p0;
        if (textView != null) {
            textView.setTextColor(i);
        } else {
            this.f6820h0 = i;
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: h */
    public abstract V mo23079h();

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: i */
    public View mo23140i() {
        View view = this.f6832t0;
        if (view != null) {
            return view;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: j */
    public View mo23141j() {
        View view = this.f6830r0;
        if (view != null) {
            return view;
        }
        RelativeLayout relativeLayout = new RelativeLayout(super.f6802P);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, C3819a.m11038a(super.f6802P, (float) this.f6811Y)));
        relativeLayout.setBackgroundColor(this.f6810X);
        relativeLayout.setGravity(16);
        this.f6827o0 = new TextView(super.f6802P);
        this.f6827o0.setVisibility(this.f6815c0 ? 0 : 8);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams.addRule(9, -1);
        layoutParams.addRule(15, -1);
        this.f6827o0.setLayoutParams(layoutParams);
        this.f6827o0.setBackgroundColor(0);
        this.f6827o0.setGravity(17);
        int a = C3819a.m11038a(super.f6802P, (float) this.f6812Z);
        this.f6827o0.setPadding(a, 0, a, 0);
        if (!TextUtils.isEmpty(this.f6816d0)) {
            this.f6827o0.setText(this.f6816d0);
        }
        this.f6827o0.setTextColor(C3819a.m11039a(this.f6819g0, this.f6822j0));
        int i = this.f6823k0;
        if (i != 0) {
            this.f6827o0.setTextSize((float) i);
        }
        this.f6827o0.setOnClickListener(new C3817a());
        relativeLayout.addView(this.f6827o0);
        if (this.f6829q0 == null) {
            TextView textView = new TextView(super.f6802P);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            int a2 = C3819a.m11038a(super.f6802P, (float) this.f6812Z);
            layoutParams2.leftMargin = a2;
            layoutParams2.rightMargin = a2;
            layoutParams2.addRule(14, -1);
            layoutParams2.addRule(15, -1);
            textView.setLayoutParams(layoutParams2);
            textView.setGravity(17);
            if (!TextUtils.isEmpty(this.f6818f0)) {
                textView.setText(this.f6818f0);
            }
            textView.setTextColor(this.f6821i0);
            int i2 = this.f6825m0;
            if (i2 != 0) {
                textView.setTextSize((float) i2);
            }
            this.f6829q0 = textView;
        }
        relativeLayout.addView(this.f6829q0);
        this.f6828p0 = new TextView(super.f6802P);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams3.addRule(11, -1);
        layoutParams3.addRule(15, -1);
        this.f6828p0.setLayoutParams(layoutParams3);
        this.f6828p0.setBackgroundColor(0);
        this.f6828p0.setGravity(17);
        this.f6828p0.setPadding(a, 0, a, 0);
        if (!TextUtils.isEmpty(this.f6817e0)) {
            this.f6828p0.setText(this.f6817e0);
        }
        this.f6828p0.setTextColor(C3819a.m11039a(this.f6820h0, this.f6822j0));
        int i3 = this.f6824l0;
        if (i3 != 0) {
            this.f6828p0.setTextSize((float) i3);
        }
        this.f6828p0.setOnClickListener(new C3818b());
        relativeLayout.addView(this.f6828p0);
        return relativeLayout;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public void mo23142k() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public abstract void mo23080l();

    /* renamed from: a */
    public void mo23133a(CharSequence charSequence) {
        TextView textView = this.f6827o0;
        if (textView != null) {
            textView.setText(charSequence);
        } else {
            this.f6816d0 = charSequence;
        }
    }

    /* renamed from: b */
    public void mo23136b(CharSequence charSequence) {
        TextView textView = this.f6828p0;
        if (textView != null) {
            textView.setText(charSequence);
        } else {
            this.f6817e0 = charSequence;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public final View mo23126c() {
        LinearLayout linearLayout = new LinearLayout(super.f6802P);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setBackgroundColor(this.f6826n0);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.setClipToPadding(false);
        View j = mo23141j();
        if (j != null) {
            linearLayout.addView(j);
        }
        if (this.f6807U) {
            View view = new View(super.f6802P);
            view.setLayoutParams(new LinearLayout.LayoutParams(-1, this.f6809W));
            view.setBackgroundColor(this.f6808V);
            linearLayout.addView(view);
        }
        if (this.f6831s0 == null) {
            this.f6831s0 = mo23079h();
        }
        int i = this.f6813a0;
        int a = i > 0 ? C3819a.m11038a(super.f6802P, (float) i) : 0;
        int i2 = this.f6814b0;
        int a2 = i2 > 0 ? C3819a.m11038a(super.f6802P, (float) i2) : 0;
        this.f6831s0.setPadding(a, a2, a, a2);
        ViewGroup viewGroup = (ViewGroup) this.f6831s0.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this.f6831s0);
        }
        linearLayout.addView(this.f6831s0, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        View i3 = mo23140i();
        if (i3 != null) {
            linearLayout.addView(i3);
        }
        return linearLayout;
    }
}
