package p112d.p113a.p114a.p116b;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.facebook.appevents.AppEventsConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import p019cn.qqtheme.framework.widget.WheelView;
import p112d.p113a.p114a.p118d.C3820c;
import p112d.p113a.p114a.p118d.DateUtils;

/* renamed from: d.a.a.b.b */
public class DateTimePicker extends WheelPicker {
    /* access modifiers changed from: private */

    /* renamed from: G0 */
    public ArrayList<String> f6740G0 = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: H0 */
    public ArrayList<String> f6741H0 = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: I0 */
    public ArrayList<String> f6742I0 = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: J0 */
    public ArrayList<String> f6743J0 = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: K0 */
    public ArrayList<String> f6744K0 = new ArrayList<>();

    /* renamed from: L0 */
    private String f6745L0 = "年";

    /* renamed from: M0 */
    private String f6746M0 = "月";

    /* renamed from: N0 */
    private String f6747N0 = "日";

    /* renamed from: O0 */
    private String f6748O0 = "时";

    /* renamed from: P0 */
    private String f6749P0 = "分";
    /* access modifiers changed from: private */

    /* renamed from: Q0 */
    public int f6750Q0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: R0 */
    public int f6751R0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: S0 */
    public int f6752S0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: T0 */
    public String f6753T0 = "";
    /* access modifiers changed from: private */

    /* renamed from: U0 */
    public String f6754U0 = "";
    /* access modifiers changed from: private */

    /* renamed from: V0 */
    public C3808j f6755V0;

    /* renamed from: W0 */
    private C3805g f6756W0;
    /* access modifiers changed from: private */

    /* renamed from: X0 */
    public int f6757X0 = 0;

    /* renamed from: Y0 */
    private int f6758Y0 = 3;

    /* renamed from: Z0 */
    private int f6759Z0 = 2010;

    /* renamed from: a1 */
    private int f6760a1 = 1;

    /* renamed from: b1 */
    private int f6761b1 = 1;

    /* renamed from: c1 */
    private int f6762c1 = 2020;

    /* renamed from: d1 */
    private int f6763d1 = 12;

    /* renamed from: e1 */
    private int f6764e1 = 31;

    /* renamed from: f1 */
    private int f6765f1;

    /* renamed from: g1 */
    private int f6766g1 = 0;

    /* renamed from: h1 */
    private int f6767h1;

    /* renamed from: i1 */
    private int f6768i1 = 59;

    /* renamed from: j1 */
    private int f6769j1 = 16;
    /* access modifiers changed from: private */

    /* renamed from: k1 */
    public boolean f6770k1 = true;

    /* renamed from: d.a.a.b.b$a */
    /* compiled from: DateTimePicker */
    class C3799a implements WheelView.C0843f {

        /* renamed from: a */
        final /* synthetic */ WheelView f6771a;

        /* renamed from: b */
        final /* synthetic */ WheelView f6772b;

        C3799a(WheelView wheelView, WheelView wheelView2) {
            this.f6771a = wheelView;
            this.f6772b = wheelView2;
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            int unused = DateTimePicker.this.f6750Q0 = i;
            String str = (String) DateTimePicker.this.f6740G0.get(DateTimePicker.this.f6750Q0);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23092e(DateTimePicker.this.f6750Q0, str);
            }
            C3820c.m11048a(this, "change months after year wheeled");
            if (DateTimePicker.this.f6770k1) {
                int unused2 = DateTimePicker.this.f6751R0 = 0;
                int unused3 = DateTimePicker.this.f6752S0 = 0;
            }
            int a = DateUtils.m11042a(str);
            DateTimePicker.this.m10942k(a);
            this.f6771a.mo9642a(DateTimePicker.this.f6741H0, DateTimePicker.this.f6751R0);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23091d(DateTimePicker.this.f6751R0, (String) DateTimePicker.this.f6741H0.get(DateTimePicker.this.f6751R0));
            }
            DateTimePicker bVar = DateTimePicker.this;
            bVar.mo23063f(a, DateUtils.m11042a((String) bVar.f6741H0.get(DateTimePicker.this.f6751R0)));
            this.f6772b.mo9642a(DateTimePicker.this.f6742I0, DateTimePicker.this.f6752S0);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23089b(DateTimePicker.this.f6752S0, (String) DateTimePicker.this.f6742I0.get(DateTimePicker.this.f6752S0));
            }
        }
    }

    /* renamed from: d.a.a.b.b$b */
    /* compiled from: DateTimePicker */
    class C3800b implements WheelView.C0843f {

        /* renamed from: a */
        final /* synthetic */ WheelView f6774a;

        C3800b(WheelView wheelView) {
            this.f6774a = wheelView;
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            int i2;
            int unused = DateTimePicker.this.f6751R0 = i;
            String str = (String) DateTimePicker.this.f6741H0.get(DateTimePicker.this.f6751R0);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23091d(DateTimePicker.this.f6751R0, str);
            }
            if (DateTimePicker.this.f6757X0 == 0 || DateTimePicker.this.f6757X0 == 2) {
                C3820c.m11048a(this, "change days after month wheeled");
                if (DateTimePicker.this.f6770k1) {
                    int unused2 = DateTimePicker.this.f6752S0 = 0;
                }
                if (DateTimePicker.this.f6757X0 == 0) {
                    i2 = DateUtils.m11042a(DateTimePicker.this.mo23085s());
                } else {
                    i2 = Calendar.getInstance(Locale.CHINA).get(1);
                }
                DateTimePicker.this.mo23063f(i2, DateUtils.m11042a(str));
                this.f6774a.mo9642a(DateTimePicker.this.f6742I0, DateTimePicker.this.f6752S0);
                if (DateTimePicker.this.f6755V0 != null) {
                    DateTimePicker.this.f6755V0.mo23089b(DateTimePicker.this.f6752S0, (String) DateTimePicker.this.f6742I0.get(DateTimePicker.this.f6752S0));
                }
            }
        }
    }

    /* renamed from: d.a.a.b.b$c */
    /* compiled from: DateTimePicker */
    class C3801c implements WheelView.C0843f {
        C3801c() {
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            int unused = DateTimePicker.this.f6752S0 = i;
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23089b(DateTimePicker.this.f6752S0, (String) DateTimePicker.this.f6742I0.get(DateTimePicker.this.f6752S0));
            }
        }
    }

    /* renamed from: d.a.a.b.b$d */
    /* compiled from: DateTimePicker */
    class C3802d implements WheelView.C0843f {

        /* renamed from: a */
        final /* synthetic */ WheelView f6777a;

        C3802d(WheelView wheelView) {
            this.f6777a = wheelView;
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            DateTimePicker bVar = DateTimePicker.this;
            String unused = bVar.f6753T0 = (String) bVar.f6743J0.get(i);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23088a(i, DateTimePicker.this.f6753T0);
            }
            C3820c.m11048a(this, "change minutes after hour wheeled");
            DateTimePicker bVar2 = DateTimePicker.this;
            bVar2.m10940j(DateUtils.m11042a(bVar2.f6753T0));
            this.f6777a.mo9643a(DateTimePicker.this.f6744K0, DateTimePicker.this.f6754U0);
        }
    }

    /* renamed from: d.a.a.b.b$e */
    /* compiled from: DateTimePicker */
    class C3803e implements WheelView.C0843f {
        C3803e() {
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            DateTimePicker bVar = DateTimePicker.this;
            String unused = bVar.f6754U0 = (String) bVar.f6744K0.get(i);
            if (DateTimePicker.this.f6755V0 != null) {
                DateTimePicker.this.f6755V0.mo23090c(i, DateTimePicker.this.f6754U0);
            }
        }
    }

    /* renamed from: d.a.a.b.b$f */
    /* compiled from: DateTimePicker */
    class C3804f implements Comparator<Object> {
        C3804f(DateTimePicker bVar) {
        }

        public int compare(Object obj, Object obj2) {
            String obj3 = obj.toString();
            String obj4 = obj2.toString();
            if (obj3.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                obj3 = obj3.substring(1);
            }
            if (obj4.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                obj4 = obj4.substring(1);
            }
            try {
                return Integer.parseInt(obj3) - Integer.parseInt(obj4);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    /* renamed from: d.a.a.b.b$g */
    /* compiled from: DateTimePicker */
    protected interface C3805g {
    }

    /* renamed from: d.a.a.b.b$h */
    /* compiled from: DateTimePicker */
    public interface C3806h extends C3805g {
        /* renamed from: a */
        void mo23068a(String str, String str2, String str3, String str4);
    }

    /* renamed from: d.a.a.b.b$i */
    /* compiled from: DateTimePicker */
    public interface C3807i extends C3805g {
        /* renamed from: a */
        void mo23087a(String str, String str2);
    }

    /* renamed from: d.a.a.b.b$j */
    /* compiled from: DateTimePicker */
    public interface C3808j {
        /* renamed from: a */
        void mo23088a(int i, String str);

        /* renamed from: b */
        void mo23089b(int i, String str);

        /* renamed from: c */
        void mo23090c(int i, String str);

        /* renamed from: d */
        void mo23091d(int i, String str);

        /* renamed from: e */
        void mo23092e(int i, String str);
    }

    /* renamed from: d.a.a.b.b$k */
    /* compiled from: DateTimePicker */
    public interface C3809k extends C3805g {
        /* renamed from: a */
        void mo23066a(String str, String str2, String str3, String str4, String str5);
    }

    /* renamed from: d.a.a.b.b$l */
    /* compiled from: DateTimePicker */
    public interface C3810l extends C3805g {
        /* renamed from: a */
        void mo23067a(String str, String str2, String str3, String str4);
    }

    public DateTimePicker(Activity activity, int i, int i2) {
        super(activity);
        if (i == -1 && i2 == -1) {
            throw new IllegalArgumentException("The modes are NONE at the same time");
        }
        if (i == 0 && i2 != -1) {
            int i3 = this.f6803Q;
            if (i3 < 720) {
                this.f6769j1 = 14;
            } else if (i3 < 480) {
                this.f6769j1 = 12;
            }
        }
        this.f6757X0 = i;
        if (i2 == 4) {
            this.f6765f1 = 1;
            this.f6767h1 = 12;
        } else {
            this.f6765f1 = 0;
            this.f6767h1 = 23;
        }
        this.f6758Y0 = i2;
    }

    /* renamed from: t */
    private void m10945t() {
        int i;
        this.f6743J0.clear();
        if (!this.f6770k1) {
            i = this.f6758Y0 == 3 ? Calendar.getInstance().get(11) : Calendar.getInstance().get(10);
        } else {
            i = 0;
        }
        for (int i2 = this.f6765f1; i2 <= this.f6767h1; i2++) {
            String a = DateUtils.m11043a(i2);
            if (!this.f6770k1 && i2 == i) {
                this.f6753T0 = a;
            }
            this.f6743J0.add(a);
        }
        if (this.f6743J0.indexOf(this.f6753T0) == -1) {
            this.f6753T0 = this.f6743J0.get(0);
        }
        if (!this.f6770k1) {
            this.f6754U0 = DateUtils.m11043a(Calendar.getInstance().get(12));
        }
    }

    /* renamed from: u */
    private void m10946u() {
        this.f6740G0.clear();
        int i = this.f6759Z0;
        int i2 = this.f6762c1;
        if (i == i2) {
            this.f6740G0.add(String.valueOf(i));
        } else if (i < i2) {
            while (i <= this.f6762c1) {
                this.f6740G0.add(String.valueOf(i));
                i++;
            }
        } else {
            while (i >= this.f6762c1) {
                this.f6740G0.add(String.valueOf(i));
                i--;
            }
        }
        if (!this.f6770k1) {
            int i3 = this.f6757X0;
            if (i3 == 0 || i3 == 1) {
                int indexOf = this.f6740G0.indexOf(DateUtils.m11043a(Calendar.getInstance().get(1)));
                if (indexOf == -1) {
                    this.f6750Q0 = 0;
                } else {
                    this.f6750Q0 = indexOf;
                }
            }
        }
    }

    /* renamed from: o */
    public String mo23081o() {
        int i = this.f6757X0;
        if (i != 0 && i != 2) {
            return "";
        }
        if (this.f6742I0.size() <= this.f6752S0) {
            this.f6752S0 = this.f6742I0.size() - 1;
        }
        return this.f6742I0.get(this.f6752S0);
    }

    /* renamed from: p */
    public String mo23082p() {
        return this.f6758Y0 != -1 ? this.f6753T0 : "";
    }

    /* renamed from: q */
    public String mo23083q() {
        return this.f6758Y0 != -1 ? this.f6754U0 : "";
    }

    /* renamed from: r */
    public String mo23084r() {
        if (this.f6757X0 == -1) {
            return "";
        }
        if (this.f6741H0.size() <= this.f6751R0) {
            this.f6751R0 = this.f6741H0.size() - 1;
        }
        return this.f6741H0.get(this.f6751R0);
    }

    /* renamed from: s */
    public String mo23085s() {
        int i = this.f6757X0;
        if (i != 0 && i != 1) {
            return "";
        }
        if (this.f6740G0.size() <= this.f6750Q0) {
            this.f6750Q0 = this.f6740G0.size() - 1;
        }
        return this.f6740G0.get(this.f6750Q0);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void mo23063f(int i, int i2) {
        String str;
        int a = DateUtils.m11041a(i, i2);
        if (!this.f6770k1) {
            if (this.f6752S0 >= a) {
                this.f6752S0 = a - 1;
            }
            int size = this.f6742I0.size();
            int i3 = this.f6752S0;
            if (size > i3) {
                str = this.f6742I0.get(i3);
            } else {
                str = DateUtils.m11043a(Calendar.getInstance().get(5));
            }
            C3820c.m11048a(this, "maxDays=" + a + ", preSelectDay=" + str);
        } else {
            str = "";
        }
        this.f6742I0.clear();
        if (i == this.f6759Z0 && i2 == this.f6760a1 && i == this.f6762c1 && i2 == this.f6763d1) {
            for (int i4 = this.f6761b1; i4 <= this.f6764e1; i4++) {
                this.f6742I0.add(DateUtils.m11043a(i4));
            }
        } else if (i == this.f6759Z0 && i2 == this.f6760a1) {
            for (int i5 = this.f6761b1; i5 <= a; i5++) {
                this.f6742I0.add(DateUtils.m11043a(i5));
            }
        } else {
            int i6 = 1;
            if (i == this.f6762c1 && i2 == this.f6763d1) {
                while (i6 <= this.f6764e1) {
                    this.f6742I0.add(DateUtils.m11043a(i6));
                    i6++;
                }
            } else {
                while (i6 <= a) {
                    this.f6742I0.add(DateUtils.m11043a(i6));
                    i6++;
                }
            }
        }
        if (!this.f6770k1) {
            int indexOf = this.f6742I0.indexOf(str);
            if (indexOf == -1) {
                indexOf = 0;
            }
            this.f6752S0 = indexOf;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m10940j(int i) {
        this.f6744K0.clear();
        int i2 = this.f6765f1;
        int i3 = this.f6767h1;
        if (i2 == i3) {
            int i4 = this.f6766g1;
            int i5 = this.f6768i1;
            if (i4 > i5) {
                this.f6766g1 = i5;
                this.f6768i1 = i4;
            }
            for (int i6 = this.f6766g1; i6 <= this.f6768i1; i6++) {
                this.f6744K0.add(DateUtils.m11043a(i6));
            }
        } else if (i == i2) {
            for (int i7 = this.f6766g1; i7 <= 59; i7++) {
                this.f6744K0.add(DateUtils.m11043a(i7));
            }
        } else if (i == i3) {
            for (int i8 = 0; i8 <= this.f6768i1; i8++) {
                this.f6744K0.add(DateUtils.m11043a(i8));
            }
        } else {
            for (int i9 = 0; i9 <= 59; i9++) {
                this.f6744K0.add(DateUtils.m11043a(i9));
            }
        }
        if (this.f6744K0.indexOf(this.f6754U0) == -1) {
            this.f6754U0 = this.f6744K0.get(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m10942k(int i) {
        String str;
        int i2;
        int i3 = 1;
        if (!this.f6770k1) {
            int size = this.f6741H0.size();
            int i4 = this.f6751R0;
            if (size > i4) {
                str = this.f6741H0.get(i4);
            } else {
                str = DateUtils.m11043a(Calendar.getInstance().get(2) + 1);
            }
            C3820c.m11048a(this, "preSelectMonth=" + str);
        } else {
            str = "";
        }
        this.f6741H0.clear();
        int i5 = this.f6760a1;
        if (i5 < 1 || (i2 = this.f6763d1) < 1 || i5 > 12 || i2 > 12) {
            throw new IllegalArgumentException("Month out of range [1-12]");
        }
        int i6 = this.f6759Z0;
        int i7 = this.f6762c1;
        if (i6 == i7) {
            if (i5 > i2) {
                while (i2 >= this.f6760a1) {
                    this.f6741H0.add(DateUtils.m11043a(i2));
                    i2--;
                }
            } else {
                while (i5 <= this.f6763d1) {
                    this.f6741H0.add(DateUtils.m11043a(i5));
                    i5++;
                }
            }
        } else if (i == i6) {
            while (i5 <= 12) {
                this.f6741H0.add(DateUtils.m11043a(i5));
                i5++;
            }
        } else if (i == i7) {
            while (i3 <= this.f6763d1) {
                this.f6741H0.add(DateUtils.m11043a(i3));
                i3++;
            }
        } else {
            while (i3 <= 12) {
                this.f6741H0.add(DateUtils.m11043a(i3));
                i3++;
            }
        }
        if (!this.f6770k1) {
            int indexOf = this.f6741H0.indexOf(str);
            if (indexOf == -1) {
                indexOf = 0;
            }
            this.f6751R0 = indexOf;
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: h */
    public View mo23079h() {
        int i;
        int i2 = this.f6757X0;
        if ((i2 == 0 || i2 == 1) && this.f6740G0.size() == 0) {
            C3820c.m11048a(this, "init years before make view");
            m10946u();
        }
        if (this.f6757X0 != -1 && this.f6741H0.size() == 0) {
            C3820c.m11048a(this, "init months before make view");
            m10942k(DateUtils.m11042a(mo23085s()));
        }
        int i3 = this.f6757X0;
        if ((i3 == 0 || i3 == 2) && this.f6742I0.size() == 0) {
            C3820c.m11048a(this, "init days before make view");
            if (this.f6757X0 == 0) {
                i = DateUtils.m11042a(mo23085s());
            } else {
                i = Calendar.getInstance(Locale.CHINA).get(1);
            }
            mo23063f(i, DateUtils.m11042a(mo23084r()));
        }
        if (this.f6758Y0 != -1 && this.f6743J0.size() == 0) {
            C3820c.m11048a(this, "init hours before make view");
            m10945t();
        }
        if (this.f6758Y0 != -1 && this.f6744K0.size() == 0) {
            C3820c.m11048a(this, "init minutes before make view");
            m10940j(DateUtils.m11042a(this.f6753T0));
        }
        LinearLayout linearLayout = new LinearLayout(this.f6802P);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        WheelView n = mo23119n();
        WheelView n2 = mo23119n();
        WheelView n3 = mo23119n();
        WheelView n4 = mo23119n();
        WheelView n5 = mo23119n();
        int i4 = this.f6757X0;
        if (i4 == 0 || i4 == 1) {
            n.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            n.mo9642a(this.f6740G0, this.f6750Q0);
            n.setOnItemSelectListener(new C3799a(n2, n3));
            linearLayout.addView(n);
            if (!TextUtils.isEmpty(this.f6745L0)) {
                TextView m = mo23118m();
                m.setTextSize((float) this.f6769j1);
                m.setText(this.f6745L0);
                linearLayout.addView(m);
            }
        }
        if (this.f6757X0 != -1) {
            n2.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            n2.mo9642a(this.f6741H0, this.f6751R0);
            n2.setOnItemSelectListener(new C3800b(n3));
            linearLayout.addView(n2);
            if (!TextUtils.isEmpty(this.f6746M0)) {
                TextView m2 = mo23118m();
                m2.setTextSize((float) this.f6769j1);
                m2.setText(this.f6746M0);
                linearLayout.addView(m2);
            }
        }
        int i5 = this.f6757X0;
        if (i5 == 0 || i5 == 2) {
            n3.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            n3.mo9642a(this.f6742I0, this.f6752S0);
            n3.setOnItemSelectListener(new C3801c());
            linearLayout.addView(n3);
            if (!TextUtils.isEmpty(this.f6747N0)) {
                TextView m3 = mo23118m();
                m3.setTextSize((float) this.f6769j1);
                m3.setText(this.f6747N0);
                linearLayout.addView(m3);
            }
        }
        if (this.f6758Y0 != -1) {
            n4.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            n4.mo9643a(this.f6743J0, this.f6753T0);
            n4.setOnItemSelectListener(new C3802d(n5));
            linearLayout.addView(n4);
            if (!TextUtils.isEmpty(this.f6748O0)) {
                TextView m4 = mo23118m();
                m4.setTextSize((float) this.f6769j1);
                m4.setText(this.f6748O0);
                linearLayout.addView(m4);
            }
            n5.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            n5.mo9643a(this.f6744K0, this.f6754U0);
            n5.setOnItemSelectListener(new C3803e());
            linearLayout.addView(n5);
            if (!TextUtils.isEmpty(this.f6749P0)) {
                TextView m5 = mo23118m();
                m5.setTextSize((float) this.f6769j1);
                m5.setText(this.f6749P0);
                linearLayout.addView(m5);
            }
        }
        return linearLayout;
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public void mo23080l() {
        if (this.f6756W0 != null) {
            String s = mo23085s();
            String r = mo23084r();
            String o = mo23081o();
            String p = mo23082p();
            String q = mo23083q();
            int i = this.f6757X0;
            if (i == -1) {
                ((C3807i) this.f6756W0).mo23087a(p, q);
            } else if (i == 0) {
                ((C3809k) this.f6756W0).mo23066a(s, r, o, p, q);
            } else if (i == 1) {
                ((C3810l) this.f6756W0).mo23067a(s, r, p, q);
            } else if (i == 2) {
                ((C3806h) this.f6756W0).mo23068a(r, o, p, q);
            }
        }
    }

    /* renamed from: c */
    public void mo23076c(int i, int i2) {
        int i3 = this.f6757X0;
        if (i3 == -1) {
            throw new IllegalArgumentException("Date mode invalid");
        } else if (i3 != 0) {
            if (i3 == 1) {
                this.f6759Z0 = i;
                this.f6760a1 = i2;
            } else if (i3 == 2) {
                int i4 = Calendar.getInstance(Locale.CHINA).get(1);
                this.f6762c1 = i4;
                this.f6759Z0 = i4;
                this.f6760a1 = i;
                this.f6761b1 = i2;
            }
            m10946u();
        } else {
            throw new IllegalArgumentException("Not support year/month/day mode");
        }
    }

    /* renamed from: d */
    public void mo23077d(int i, int i2) {
        if (this.f6758Y0 != -1) {
            boolean z = false;
            if (i < 0 || i2 < 0 || i2 > 59) {
                z = true;
            }
            if (this.f6758Y0 == 4 && (i == 0 || i > 12)) {
                z = true;
            }
            if (this.f6758Y0 == 3 && i >= 24) {
                z = true;
            }
            if (!z) {
                this.f6767h1 = i;
                this.f6768i1 = i2;
                m10945t();
                return;
            }
            throw new IllegalArgumentException("Time out of range");
        }
        throw new IllegalArgumentException("Time mode invalid");
    }

    /* renamed from: e */
    public void mo23078e(int i, int i2) {
        if (this.f6758Y0 != -1) {
            boolean z = false;
            if (i < 0 || i2 < 0 || i2 > 59) {
                z = true;
            }
            if (this.f6758Y0 == 4 && (i == 0 || i > 12)) {
                z = true;
            }
            if (this.f6758Y0 == 3 && i >= 24) {
                z = true;
            }
            if (!z) {
                this.f6765f1 = i;
                this.f6766g1 = i2;
                m10945t();
                return;
            }
            throw new IllegalArgumentException("Time out of range");
        }
        throw new IllegalArgumentException("Time mode invalid");
    }

    /* renamed from: b */
    public void mo23075b(int i, int i2) {
        int i3 = this.f6757X0;
        if (i3 == -1) {
            throw new IllegalArgumentException("Date mode invalid");
        } else if (i3 != 0) {
            if (i3 == 1) {
                this.f6762c1 = i;
                this.f6763d1 = i2;
            } else if (i3 == 2) {
                this.f6763d1 = i;
                this.f6764e1 = i2;
            }
            m10946u();
        } else {
            throw new IllegalArgumentException("Not support year/month/day mode");
        }
    }

    /* renamed from: a */
    public void mo23074a(String str, String str2, String str3, String str4, String str5) {
        this.f6745L0 = str;
        this.f6746M0 = str2;
        this.f6747N0 = str3;
        this.f6748O0 = str4;
        this.f6749P0 = str5;
    }

    /* renamed from: a */
    public void mo23072a(int i, int i2, int i3, int i4) {
        int i5 = this.f6757X0;
        if (i5 != 0) {
            if (i5 == 2) {
                C3820c.m11048a(this, "change months and days while set selected");
                int i6 = Calendar.getInstance(Locale.CHINA).get(1);
                this.f6762c1 = i6;
                this.f6759Z0 = i6;
                m10942k(i6);
                mo23063f(i6, i);
                this.f6751R0 = m10922a(this.f6741H0, i);
                this.f6752S0 = m10922a(this.f6742I0, i2);
            } else if (i5 == 1) {
                C3820c.m11048a(this, "change months while set selected");
                m10942k(i);
                this.f6750Q0 = m10922a(this.f6740G0, i);
                this.f6751R0 = m10922a(this.f6741H0, i2);
            }
            if (this.f6758Y0 != -1) {
                this.f6753T0 = DateUtils.m11043a(i3);
                this.f6754U0 = DateUtils.m11043a(i4);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Date mode invalid");
    }

    /* renamed from: a */
    public void mo23073a(C3805g gVar) {
        this.f6756W0 = gVar;
    }

    /* renamed from: a */
    private int m10922a(ArrayList<String> arrayList, int i) {
        int binarySearch = Collections.binarySearch(arrayList, Integer.valueOf(i), new C3804f(this));
        if (binarySearch >= 0) {
            return binarySearch;
        }
        throw new IllegalArgumentException("Item[" + i + "] out of range");
    }
}
