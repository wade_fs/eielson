package p112d.p113a.p114a.p116b;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import p019cn.qqtheme.framework.widget.WheelView;
import p112d.p113a.p114a.p117c.ConfirmPopup;

/* renamed from: d.a.a.b.f */
public abstract class WheelPicker extends ConfirmPopup<View> {

    /* renamed from: A0 */
    protected int f6790A0 = -16611122;

    /* renamed from: B0 */
    protected int f6791B0 = 3;

    /* renamed from: C0 */
    protected boolean f6792C0 = true;

    /* renamed from: D0 */
    protected boolean f6793D0 = true;

    /* renamed from: E0 */
    protected boolean f6794E0 = true;

    /* renamed from: F0 */
    protected WheelView.C0840c f6795F0 = new WheelView.C0840c();

    /* renamed from: u0 */
    protected float f6796u0 = 2.0f;

    /* renamed from: v0 */
    protected int f6797v0 = -1;

    /* renamed from: w0 */
    protected int f6798w0 = 16;

    /* renamed from: x0 */
    protected Typeface f6799x0 = Typeface.DEFAULT;

    /* renamed from: y0 */
    protected int f6800y0 = -4473925;

    /* renamed from: z0 */
    protected int f6801z0 = -16611122;

    public WheelPicker(Activity activity) {
        super(activity);
    }

    /* renamed from: b */
    public void mo23112b(boolean z) {
        this.f6792C0 = z;
    }

    /* renamed from: c */
    public void mo23113c(boolean z) {
        this.f6793D0 = z;
    }

    /* renamed from: f */
    public void mo23114f(@ColorInt int i) {
        if (this.f6795F0 == null) {
            this.f6795F0 = new WheelView.C0840c();
        }
        this.f6795F0.mo9675b(true);
        this.f6795F0.mo9673a(i);
    }

    /* renamed from: g */
    public void mo23115g(int i) {
        this.f6790A0 = i;
    }

    /* renamed from: h */
    public void mo23116h(@IntRange(from = 1, mo464to = 5) int i) {
        this.f6791B0 = i;
    }

    /* renamed from: i */
    public void mo23117i(int i) {
        this.f6797v0 = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public TextView mo23118m() {
        TextView textView = new TextView(this.f6802P);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView.setTextColor(this.f6790A0);
        textView.setTextSize((float) this.f6798w0);
        return textView;
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public WheelView mo23119n() {
        WheelView wheelView = new WheelView(this.f6802P);
        wheelView.setLineSpaceMultiplier(this.f6796u0);
        wheelView.setTextPadding(this.f6797v0);
        wheelView.setTextSize((float) this.f6798w0);
        wheelView.setTypeface(this.f6799x0);
        wheelView.mo9640a(this.f6800y0, this.f6801z0);
        wheelView.setDividerConfig(this.f6795F0);
        wheelView.setOffset(this.f6791B0);
        wheelView.setCycleDisable(this.f6792C0);
        wheelView.setUseWeight(this.f6793D0);
        wheelView.setTextSizeAutoFit(this.f6794E0);
        return wheelView;
    }
}
