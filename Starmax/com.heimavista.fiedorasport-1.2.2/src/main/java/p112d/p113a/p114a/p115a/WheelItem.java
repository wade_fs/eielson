package p112d.p113a.p114a.p115a;

import java.io.Serializable;

/* renamed from: d.a.a.a.a */
public interface WheelItem extends Serializable {
    String getName();
}
