package p112d.p113a.p114a.p118d;

import android.content.Context;
import android.content.res.ColorStateList;
import androidx.annotation.ColorInt;

/* renamed from: d.a.a.d.a */
/* compiled from: ConvertUtils */
public class C3819a {
    /* renamed from: a */
    public static int m11038a(Context context, float f) {
        int i = (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
        C3820c.m11053c(f + " dp == " + i + " px");
        return i;
    }

    /* renamed from: a */
    public static ColorStateList m11040a(@ColorInt int i, @ColorInt int i2, @ColorInt int i3, @ColorInt int i4) {
        int[] iArr = {i2, i3, i, i3, i4, i};
        return new ColorStateList(new int[][]{new int[]{16842919, 16842910}, new int[]{16842910, 16842908}, new int[]{16842910}, new int[]{16842908}, new int[]{16842909}, new int[0]}, iArr);
    }

    /* renamed from: a */
    public static ColorStateList m11039a(@ColorInt int i, @ColorInt int i2) {
        return m11040a(i, i2, i2, i);
    }
}
