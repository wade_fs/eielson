package p112d.p113a.p114a.p116b;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import p019cn.qqtheme.framework.widget.WheelView;
import p112d.p113a.p114a.p118d.C3819a;

/* renamed from: d.a.a.b.d */
public class SinglePicker<T> extends WheelPicker {
    /* access modifiers changed from: private */

    /* renamed from: G0 */
    public List<T> f6780G0;

    /* renamed from: H0 */
    private List<String> f6781H0;

    /* renamed from: I0 */
    private WheelView f6782I0;
    /* access modifiers changed from: private */

    /* renamed from: J0 */
    public C3814c<T> f6783J0;

    /* renamed from: K0 */
    private C3813b<T> f6784K0;
    /* access modifiers changed from: private */

    /* renamed from: L0 */
    public int f6785L0;

    /* renamed from: M0 */
    private String f6786M0;

    /* renamed from: N0 */
    private int f6787N0;

    /* renamed from: d.a.a.b.d$a */
    /* compiled from: SinglePicker */
    class C3812a implements WheelView.C0843f {
        C3812a() {
        }

        /* renamed from: a */
        public void mo9679a(int i) {
            int unused = SinglePicker.this.f6785L0 = i;
            if (SinglePicker.this.f6783J0 != null) {
                SinglePicker.this.f6783J0.mo23106a(SinglePicker.this.f6785L0, SinglePicker.this.f6780G0.get(i));
            }
        }
    }

    /* renamed from: d.a.a.b.d$b */
    /* compiled from: SinglePicker */
    public interface C3813b<T> {
        /* renamed from: a */
        void mo23097a(int i, Object obj);
    }

    /* renamed from: d.a.a.b.d$c */
    /* compiled from: SinglePicker */
    public interface C3814c<T> {
        /* renamed from: a */
        void mo23106a(int i, T t);
    }

    public SinglePicker(Activity activity, Object[] objArr) {
        this(activity, Arrays.asList(objArr));
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: h */
    public View mo23079h() {
        if (this.f6780G0.size() != 0) {
            LinearLayout linearLayout = new LinearLayout(this.f6802P);
            linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            linearLayout.setOrientation(0);
            linearLayout.setGravity(17);
            this.f6782I0 = mo23119n();
            linearLayout.addView(this.f6782I0);
            if (TextUtils.isEmpty(this.f6786M0)) {
                this.f6782I0.setLayoutParams(new LinearLayout.LayoutParams(this.f6803Q, -2));
            } else {
                this.f6782I0.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                TextView m = mo23118m();
                m.setText(this.f6786M0);
                linearLayout.addView(m);
            }
            this.f6782I0.mo9642a(this.f6781H0, this.f6785L0);
            this.f6782I0.setOnItemSelectListener(new C3812a());
            if (this.f6787N0 != -99) {
                ViewGroup.LayoutParams layoutParams = this.f6782I0.getLayoutParams();
                layoutParams.width = C3819a.m11038a(this.f6802P, (float) this.f6787N0);
                this.f6782I0.setLayoutParams(layoutParams);
            }
            return linearLayout;
        }
        throw new IllegalArgumentException("Items can't be empty");
    }

    /* renamed from: j */
    public void mo23104j(int i) {
        if (i >= 0 && i < this.f6780G0.size()) {
            this.f6785L0 = i;
        }
    }

    /* renamed from: l */
    public void mo23080l() {
        C3813b<T> bVar = this.f6784K0;
        if (bVar != null) {
            bVar.mo23097a(this.f6785L0, mo23105o());
        }
    }

    /* renamed from: o */
    public T mo23105o() {
        return this.f6780G0.get(this.f6785L0);
    }

    public SinglePicker(Activity activity, List list) {
        super(activity);
        this.f6780G0 = new ArrayList();
        this.f6781H0 = new ArrayList();
        this.f6785L0 = 0;
        this.f6786M0 = "";
        this.f6787N0 = -99;
        mo23102a(list);
    }

    /* renamed from: c */
    private String m10984c(T t) {
        if ((t instanceof Float) || (t instanceof Double)) {
            return new DecimalFormat("0.00").format(t);
        }
        return t.toString();
    }

    /* renamed from: b */
    public void mo23103b(@NonNull Object obj) {
        mo23104j(this.f6781H0.indexOf(m10984c(obj)));
    }

    /* renamed from: a */
    public void mo23100a(Object obj) {
        this.f6780G0.add(obj);
        this.f6781H0.add(m10984c(obj));
    }

    /* renamed from: a */
    public void mo23102a(List list) {
        if (list != null && list.size() != 0) {
            this.f6780G0 = list;
            this.f6781H0.clear();
            for (Object obj : list) {
                this.f6781H0.add(m10984c(obj));
            }
            WheelView wheelView = this.f6782I0;
            if (wheelView != null) {
                wheelView.mo9642a(this.f6781H0, this.f6785L0);
            }
        }
    }

    /* renamed from: a */
    public void mo23101a(String str) {
        this.f6786M0 = str;
    }

    /* renamed from: a */
    public void mo23099a(C3813b bVar) {
        this.f6784K0 = bVar;
    }
}
