package p112d.p113a.p114a.p116b;

import android.app.Activity;
import p112d.p113a.p114a.p116b.DateTimePicker;

/* renamed from: d.a.a.b.e */
public class TimePicker extends DateTimePicker {

    /* renamed from: d.a.a.b.e$a */
    /* compiled from: TimePicker */
    class C3815a implements DateTimePicker.C3807i {

        /* renamed from: a */
        final /* synthetic */ C3816b f6789a;

        C3815a(TimePicker eVar, C3816b bVar) {
            this.f6789a = bVar;
        }

        /* renamed from: a */
        public void mo23087a(String str, String str2) {
            this.f6789a.mo23111a(str, str2);
        }
    }

    /* renamed from: d.a.a.b.e$b */
    /* compiled from: TimePicker */
    public interface C3816b {
        /* renamed from: a */
        void mo23111a(String str, String str2);
    }

    public TimePicker(Activity activity, int i) {
        super(activity, -1, i);
    }

    /* renamed from: a */
    public void mo23108a(String str, String str2) {
        super.mo23074a("", "", "", str, str2);
    }

    /* renamed from: f */
    public void mo23063f(int i, int i2) {
        super.mo23077d(i, i2);
    }

    /* renamed from: g */
    public void mo23109g(int i, int i2) {
        super.mo23078e(i, i2);
    }

    /* renamed from: h */
    public void mo23110h(int i, int i2) {
        super.mo23072a(0, 0, i, i2);
    }

    /* renamed from: a */
    public void mo23107a(C3816b bVar) {
        if (bVar != null) {
            super.mo23073a(new C3815a(this, bVar));
        }
    }
}
