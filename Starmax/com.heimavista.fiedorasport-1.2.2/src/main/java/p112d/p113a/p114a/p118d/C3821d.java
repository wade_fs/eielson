package p112d.p113a.p114a.p118d;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/* renamed from: d.a.a.d.d */
/* compiled from: ScreenUtils */
public final class C3821d {

    /* renamed from: a */
    private static DisplayMetrics f6837a;

    /* renamed from: a */
    public static DisplayMetrics m11056a(Context context) {
        DisplayMetrics displayMetrics = f6837a;
        if (displayMetrics != null) {
            return displayMetrics;
        }
        DisplayMetrics displayMetrics2 = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics2);
        C3820c.m11053c("screen width=" + displayMetrics2.widthPixels + "px, screen height=" + displayMetrics2.heightPixels + "px, densityDpi=" + displayMetrics2.densityDpi + ", density=" + displayMetrics2.density);
        return displayMetrics2;
    }
}
