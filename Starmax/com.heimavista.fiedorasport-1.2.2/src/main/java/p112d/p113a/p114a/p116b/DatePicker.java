package p112d.p113a.p114a.p116b;

import android.app.Activity;
import p112d.p113a.p114a.p116b.DateTimePicker;

/* renamed from: d.a.a.b.a */
public class DatePicker extends DateTimePicker {

    /* renamed from: d.a.a.b.a$a */
    /* compiled from: DatePicker */
    class C3792a implements DateTimePicker.C3809k {

        /* renamed from: a */
        final /* synthetic */ C3795d f6737a;

        C3792a(DatePicker aVar, C3795d dVar) {
            this.f6737a = dVar;
        }

        /* renamed from: a */
        public void mo23066a(String str, String str2, String str3, String str4, String str5) {
            ((C3797f) this.f6737a).mo23070a(str, str2, str3);
        }
    }

    /* renamed from: d.a.a.b.a$b */
    /* compiled from: DatePicker */
    class C3793b implements DateTimePicker.C3810l {

        /* renamed from: a */
        final /* synthetic */ C3795d f6738a;

        C3793b(DatePicker aVar, C3795d dVar) {
            this.f6738a = dVar;
        }

        /* renamed from: a */
        public void mo23067a(String str, String str2, String str3, String str4) {
            ((C3798g) this.f6738a).mo23071a(str, str2);
        }
    }

    /* renamed from: d.a.a.b.a$c */
    /* compiled from: DatePicker */
    class C3794c implements DateTimePicker.C3806h {

        /* renamed from: a */
        final /* synthetic */ C3795d f6739a;

        C3794c(DatePicker aVar, C3795d dVar) {
            this.f6739a = dVar;
        }

        /* renamed from: a */
        public void mo23068a(String str, String str2, String str3, String str4) {
            ((C3796e) this.f6739a).mo23069a(str, str2);
        }
    }

    /* renamed from: d.a.a.b.a$d */
    /* compiled from: DatePicker */
    protected interface C3795d {
    }

    /* renamed from: d.a.a.b.a$e */
    /* compiled from: DatePicker */
    public interface C3796e extends C3795d {
        /* renamed from: a */
        void mo23069a(String str, String str2);
    }

    /* renamed from: d.a.a.b.a$f */
    /* compiled from: DatePicker */
    public interface C3797f extends C3795d {
        /* renamed from: a */
        void mo23070a(String str, String str2, String str3);
    }

    /* renamed from: d.a.a.b.a$g */
    /* compiled from: DatePicker */
    public interface C3798g extends C3795d {
        /* renamed from: a */
        void mo23071a(String str, String str2);
    }

    public DatePicker(Activity activity, int i) {
        super(activity, i, -1);
    }

    /* renamed from: a */
    public void mo23062a(String str, String str2, String str3) {
        super.mo23074a(str, str2, str3, "", "");
    }

    /* renamed from: f */
    public void mo23063f(int i, int i2) {
        super.mo23075b(i, i2);
    }

    /* renamed from: g */
    public void mo23064g(int i, int i2) {
        super.mo23076c(i, i2);
    }

    /* renamed from: h */
    public void mo23065h(int i, int i2) {
        super.mo23072a(i, i2, 0, 0);
    }

    /* renamed from: a */
    public void mo23061a(C3795d dVar) {
        if (dVar != null) {
            if (dVar instanceof C3797f) {
                super.mo23073a(new C3792a(this, dVar));
            } else if (dVar instanceof C3798g) {
                super.mo23073a(new C3793b(this, dVar));
            } else if (dVar instanceof C3796e) {
                super.mo23073a(new C3794c(this, dVar));
            }
        }
    }
}
