package p112d.p113a.p114a.p118d;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: d.a.a.d.c */
/* compiled from: LogUtils */
public final class C3820c {

    /* renamed from: a */
    private static boolean f6835a = false;

    /* renamed from: b */
    private static String f6836b = "liyujiang";

    /* renamed from: a */
    public static void m11048a(Object obj, String str) {
        m11051b(obj.getClass().getSimpleName(), str);
    }

    /* renamed from: b */
    public static void m11051b(String str, String str2) {
        if (f6835a) {
            StringBuilder sb = new StringBuilder();
            sb.append(f6836b);
            sb.append((str == null || str.trim().length() == 0) ? "" : "-");
            sb.append(str);
            String sb2 = sb.toString();
            Log.v(sb2, str2 + m11045a());
        }
    }

    /* renamed from: c */
    public static void m11053c(String str) {
        m11051b("", str);
    }

    /* renamed from: d */
    public static void m11055d(String str) {
        m11054c("", str);
    }

    /* renamed from: a */
    public static void m11049a(String str, String str2) {
        if (f6835a) {
            StringBuilder sb = new StringBuilder();
            sb.append(f6836b);
            sb.append((str == null || str.trim().length() == 0) ? "" : "-");
            sb.append(str);
            String sb2 = sb.toString();
            Log.d(sb2, str2 + m11045a());
        }
    }

    /* renamed from: c */
    public static void m11054c(String str, String str2) {
        if (f6835a) {
            StringBuilder sb = new StringBuilder();
            sb.append(f6836b);
            sb.append((str == null || str.trim().length() == 0) ? "" : "-");
            sb.append(str);
            String sb2 = sb.toString();
            Log.w(sb2, str2 + m11045a());
        }
    }

    /* renamed from: b */
    public static void m11050b(String str) {
        m11049a("", str);
    }

    /* renamed from: a */
    public static String m11047a(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        String stringWriter2 = stringWriter.toString();
        if (stringWriter2.length() > 131071) {
            stringWriter2 = stringWriter2.substring(0, 131047) + " [stack trace too large]";
        }
        printWriter.close();
        return stringWriter2;
    }

    /* renamed from: b */
    public static void m11052b(Throwable th) {
        m11055d(m11047a(th));
    }

    /* renamed from: a */
    private static String m11045a() {
        int i = 2;
        try {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            int a = m11044a(stackTrace);
            if (a + 2 > stackTrace.length) {
                i = (stackTrace.length - a) - 1;
            }
            StringBuilder sb = new StringBuilder();
            String str = "    ";
            while (i > 0) {
                int i2 = i + a;
                if (i2 < stackTrace.length) {
                    sb.append("\n");
                    sb.append(str);
                    sb.append(m11046a(stackTrace[i2].getClassName()));
                    sb.append(".");
                    sb.append(stackTrace[i2].getMethodName());
                    sb.append(" ");
                    sb.append("(");
                    sb.append(stackTrace[i2].getFileName());
                    sb.append(":");
                    sb.append(stackTrace[i2].getLineNumber());
                    sb.append(")");
                    str = str + "    ";
                }
                i--;
            }
            return sb.toString();
        } catch (Exception e) {
            Log.w(f6836b, e);
            return "";
        }
    }

    /* renamed from: a */
    private static int m11044a(StackTraceElement[] stackTraceElementArr) {
        for (int i = 3; i < stackTraceElementArr.length; i++) {
            if (!stackTraceElementArr[i].getClassName().equals(C3820c.class.getName())) {
                return i - 1;
            }
        }
        return -1;
    }

    /* renamed from: a */
    private static String m11046a(String str) {
        return str.substring(str.lastIndexOf(".") + 1);
    }
}
