package p112d.p113a.p114a.p116b;

import android.app.Activity;
import p112d.p113a.p114a.p116b.SinglePicker;

/* renamed from: d.a.a.b.c */
public class NumberPicker extends SinglePicker<Number> {

    /* renamed from: d.a.a.b.c$a */
    /* compiled from: NumberPicker */
    public static abstract class C3811a implements SinglePicker.C3813b<Number> {
        /* renamed from: b */
        public abstract void mo23098b(int i, Number number);

        /* renamed from: a */
        public final void mo23097a(int i, Number number) {
            mo23098b(i, number);
        }
    }

    public NumberPicker(Activity activity) {
        super(activity, new Number[0]);
    }

    /* renamed from: a */
    public void mo23093a(int i, int i2, int i3) {
        while (i <= i2) {
            mo23100a(Integer.valueOf(i));
            i += i3;
        }
    }

    /* renamed from: k */
    public void mo23095k(int i) {
        super.mo23103b(Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo23094a(C3811a aVar) {
        super.mo23099a((SinglePicker.C3813b) aVar);
    }
}
