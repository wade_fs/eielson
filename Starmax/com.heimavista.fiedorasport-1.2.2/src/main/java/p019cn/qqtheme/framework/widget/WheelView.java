package p019cn.qqtheme.framework.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.core.view.GravityCompat;
import androidx.vectordrawable.graphics.drawable.PathInterpolatorCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import p112d.p113a.p114a.p115a.WheelItem;
import p112d.p113a.p114a.p118d.C3819a;
import p112d.p113a.p114a.p118d.C3820c;

/* renamed from: cn.qqtheme.framework.widget.WheelView */
public class WheelView extends View {

    /* renamed from: A0 */
    private int f479A0;

    /* renamed from: B0 */
    private float f480B0;

    /* renamed from: C0 */
    private long f481C0;

    /* renamed from: D0 */
    private int f482D0;

    /* renamed from: E0 */
    private int f483E0;

    /* renamed from: F0 */
    private int f484F0;

    /* renamed from: G0 */
    private int f485G0;

    /* renamed from: H0 */
    private float f486H0;

    /* renamed from: I0 */
    private boolean f487I0;

    /* renamed from: J0 */
    private boolean f488J0;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public C0842e f489P;

    /* renamed from: Q */
    private GestureDetector f490Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public C0843f f491R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public C0844g f492S;

    /* renamed from: T */
    private boolean f493T;

    /* renamed from: U */
    private ScheduledFuture<?> f494U;

    /* renamed from: V */
    private Paint f495V;

    /* renamed from: W */
    private Paint f496W;

    /* renamed from: a0 */
    private Paint f497a0;

    /* renamed from: b0 */
    private Paint f498b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public List<WheelItem> f499c0;

    /* renamed from: d0 */
    private String f500d0;

    /* renamed from: e0 */
    private int f501e0;

    /* renamed from: f0 */
    private int f502f0;

    /* renamed from: g0 */
    private int f503g0;

    /* renamed from: h0 */
    private int f504h0;
    /* access modifiers changed from: private */

    /* renamed from: i0 */
    public float f505i0;

    /* renamed from: j0 */
    private Typeface f506j0;

    /* renamed from: k0 */
    private int f507k0;

    /* renamed from: l0 */
    private int f508l0;

    /* renamed from: m0 */
    private C0840c f509m0;

    /* renamed from: n0 */
    private float f510n0;

    /* renamed from: o0 */
    private int f511o0;
    /* access modifiers changed from: private */

    /* renamed from: p0 */
    public boolean f512p0;

    /* renamed from: q0 */
    private float f513q0;

    /* renamed from: r0 */
    private float f514r0;
    /* access modifiers changed from: private */

    /* renamed from: s0 */
    public float f515s0;
    /* access modifiers changed from: private */

    /* renamed from: t0 */
    public int f516t0;
    /* access modifiers changed from: private */

    /* renamed from: u0 */
    public int f517u0;

    /* renamed from: v0 */
    private int f518v0;

    /* renamed from: w0 */
    private int f519w0;

    /* renamed from: x0 */
    private int f520x0;

    /* renamed from: y0 */
    private int f521y0;

    /* renamed from: z0 */
    private int f522z0;

    /* renamed from: cn.qqtheme.framework.widget.WheelView$a */
    class C0838a extends GestureDetector.SimpleOnGestureListener {
        C0838a() {
        }

        public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            WheelView.this.m789a(f2);
            return true;
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$b */
    class C0839b implements Runnable {
        C0839b() {
        }

        public void run() {
            if (WheelView.this.f491R != null) {
                WheelView.this.f491R.mo9679a(WheelView.this.f517u0);
            }
            if (WheelView.this.f492S != null) {
                WheelView.this.f492S.mo9680a(true, WheelView.this.f517u0, ((WheelItem) WheelView.this.f499c0.get(WheelView.this.f517u0)).getName());
            }
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$d */
    private static class C0841d extends TimerTask {

        /* renamed from: P */
        float f533P = 2.14748365E9f;

        /* renamed from: Q */
        final float f534Q;

        /* renamed from: R */
        final WheelView f535R;

        C0841d(WheelView wheelView, float f) {
            this.f535R = wheelView;
            this.f534Q = f;
        }

        public final void run() {
            if (this.f533P == 2.14748365E9f) {
                if (Math.abs(this.f534Q) <= 2000.0f) {
                    this.f533P = this.f534Q;
                } else if (this.f534Q > 0.0f) {
                    this.f533P = 2000.0f;
                } else {
                    this.f533P = -2000.0f;
                }
            }
            if (Math.abs(this.f533P) < 0.0f || Math.abs(this.f533P) > 20.0f) {
                WheelView wheelView = this.f535R;
                float f = (float) ((int) ((this.f533P * 10.0f) / 1000.0f));
                float unused = wheelView.f515s0 = wheelView.f515s0 - f;
                if (!this.f535R.f512p0) {
                    float c = this.f535R.f505i0;
                    float f2 = ((float) (-this.f535R.f516t0)) * c;
                    float itemCount = ((float) ((this.f535R.getItemCount() - 1) - this.f535R.f516t0)) * c;
                    double d = ((double) c) * 0.25d;
                    if (((double) this.f535R.f515s0) - d < ((double) f2)) {
                        f2 = this.f535R.f515s0 + f;
                    } else if (((double) this.f535R.f515s0) + d > ((double) itemCount)) {
                        itemCount = this.f535R.f515s0 + f;
                    }
                    if (this.f535R.f515s0 <= f2) {
                        this.f533P = 40.0f;
                        float unused2 = this.f535R.f515s0 = (float) ((int) f2);
                    } else if (this.f535R.f515s0 >= itemCount) {
                        float unused3 = this.f535R.f515s0 = (float) ((int) itemCount);
                        this.f533P = -40.0f;
                    }
                }
                float f3 = this.f533P;
                if (f3 < 0.0f) {
                    this.f533P = f3 + 20.0f;
                } else {
                    this.f533P = f3 - 20.0f;
                }
                this.f535R.f489P.sendEmptyMessage(1000);
                return;
            }
            this.f535R.m788a();
            this.f535R.f489P.sendEmptyMessage(2000);
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$e */
    private static class C0842e extends Handler {

        /* renamed from: a */
        final WheelView f536a;

        C0842e(WheelView wheelView) {
            this.f536a = wheelView;
        }

        public final void handleMessage(Message message) {
            int i = message.what;
            if (i == 1000) {
                this.f536a.invalidate();
            } else if (i == 2000) {
                this.f536a.m796b(2);
            } else if (i == 3000) {
                this.f536a.m803d();
            }
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$f */
    public interface C0843f {
        /* renamed from: a */
        void mo9679a(int i);
    }

    @Deprecated
    /* renamed from: cn.qqtheme.framework.widget.WheelView$g */
    public interface C0844g {
        /* renamed from: a */
        void mo9680a(boolean z, int i, String str);
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$h */
    private static class C0845h extends TimerTask {

        /* renamed from: P */
        int f537P = Integer.MAX_VALUE;

        /* renamed from: Q */
        int f538Q = 0;

        /* renamed from: R */
        int f539R;

        /* renamed from: S */
        final WheelView f540S;

        C0845h(WheelView wheelView, int i) {
            this.f540S = wheelView;
            this.f539R = i;
        }

        public void run() {
            if (this.f537P == Integer.MAX_VALUE) {
                this.f537P = this.f539R;
            }
            int i = this.f537P;
            this.f538Q = (int) (((float) i) * 0.1f);
            if (this.f538Q == 0) {
                if (i < 0) {
                    this.f538Q = -1;
                } else {
                    this.f538Q = 1;
                }
            }
            if (Math.abs(this.f537P) <= 1) {
                this.f540S.m788a();
                this.f540S.f489P.sendEmptyMessage(PathInterpolatorCompat.MAX_NUM_POINTS);
                return;
            }
            WheelView wheelView = this.f540S;
            float unused = wheelView.f515s0 = wheelView.f515s0 + ((float) this.f538Q);
            if (!this.f540S.f512p0) {
                float c = this.f540S.f505i0;
                float f = ((float) (-this.f540S.f516t0)) * c;
                float itemCount = ((float) ((this.f540S.getItemCount() - 1) - this.f540S.f516t0)) * c;
                if (this.f540S.f515s0 <= f || this.f540S.f515s0 >= itemCount) {
                    WheelView wheelView2 = this.f540S;
                    float unused2 = wheelView2.f515s0 = wheelView2.f515s0 - ((float) this.f538Q);
                    this.f540S.m788a();
                    this.f540S.f489P.sendEmptyMessage(PathInterpolatorCompat.MAX_NUM_POINTS);
                    return;
                }
            }
            this.f540S.f489P.sendEmptyMessage(1000);
            this.f537P -= this.f538Q;
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$i */
    private static class C0846i implements WheelItem {

        /* renamed from: P */
        private String f541P;

        /* synthetic */ C0846i(String str, C0838a aVar) {
            this(str);
        }

        public String getName() {
            return this.f541P;
        }

        private C0846i(String str) {
            this.f541P = str;
        }
    }

    public WheelView(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    public int getItemCount() {
        List<WheelItem> list = this.f499c0;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public final int getSelectedIndex() {
        return this.f517u0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String str;
        Canvas canvas2 = canvas;
        List<WheelItem> list = this.f499c0;
        if (list != null && list.size() != 0) {
            String[] strArr = new String[this.f519w0];
            this.f518v0 = this.f516t0 + (((int) (this.f515s0 / this.f505i0)) % this.f499c0.size());
            if (!this.f512p0) {
                if (this.f518v0 < 0) {
                    this.f518v0 = 0;
                }
                if (this.f518v0 > this.f499c0.size() - 1) {
                    this.f518v0 = this.f499c0.size() - 1;
                }
            } else {
                if (this.f518v0 < 0) {
                    this.f518v0 = this.f499c0.size() + this.f518v0;
                }
                if (this.f518v0 > this.f499c0.size() - 1) {
                    this.f518v0 -= this.f499c0.size();
                }
            }
            float f = this.f515s0 % this.f505i0;
            int i = 0;
            while (true) {
                int i2 = this.f519w0;
                if (i >= i2) {
                    break;
                }
                int i3 = this.f518v0 - ((i2 / 2) - i);
                if (this.f512p0) {
                    strArr[i] = this.f499c0.get(m785a(i3)).getName();
                } else if (i3 < 0) {
                    strArr[i] = "";
                } else if (i3 > this.f499c0.size() - 1) {
                    strArr[i] = "";
                } else {
                    strArr[i] = this.f499c0.get(i3).getName();
                }
                i++;
            }
            C0840c cVar = this.f509m0;
            if (cVar.f525a) {
                float f2 = cVar.f531g;
                int i4 = this.f521y0;
                float f3 = this.f513q0;
                float f4 = 1.0f - f2;
                canvas.drawLine(((float) i4) * f2, f3, ((float) i4) * f4, f3, this.f497a0);
                int i5 = this.f521y0;
                float f5 = this.f514r0;
                canvas.drawLine(((float) i5) * f2, f5, ((float) i5) * f4, f5, this.f497a0);
            }
            C0840c cVar2 = this.f509m0;
            if (cVar2.f526b) {
                this.f498b0.setColor(cVar2.f528d);
                this.f498b0.setAlpha(this.f509m0.f529e);
                canvas.drawRect(0.0f, this.f513q0, (float) this.f521y0, this.f514r0, this.f498b0);
            }
            for (int i6 = 0; i6 < this.f519w0; i6++) {
                canvas.save();
                double d = (double) (((this.f505i0 * ((float) i6)) - f) / ((float) this.f522z0));
                float f6 = (float) (90.0d - ((d / 3.141592653589793d) * 180.0d));
                if (f6 >= 90.0f || f6 <= -90.0f) {
                    canvas.restore();
                } else {
                    String a = m787a((Object) strArr[i6]);
                    if (this.f493T || TextUtils.isEmpty(this.f500d0) || TextUtils.isEmpty(a)) {
                        str = a;
                    } else {
                        str = a + this.f500d0;
                    }
                    if (this.f488J0) {
                        m801c(str);
                        this.f483E0 = 17;
                    } else {
                        this.f483E0 = GravityCompat.START;
                    }
                    m793a(str);
                    m797b(str);
                    String str2 = a;
                    float cos = (float) ((((double) this.f522z0) - (Math.cos(d) * ((double) this.f522z0))) - ((Math.sin(d) * ((double) this.f502f0)) / 2.0d));
                    canvas2.translate(0.0f, cos);
                    float f7 = this.f513q0;
                    if (cos > f7 || ((float) this.f502f0) + cos < f7) {
                        float f8 = this.f514r0;
                        if (cos > f8 || ((float) this.f502f0) + cos < f8) {
                            if (cos >= this.f513q0) {
                                int i7 = this.f502f0;
                                if (((float) i7) + cos <= this.f514r0) {
                                    canvas2.clipRect(0, 0, this.f521y0, i7);
                                    float f9 = ((float) this.f502f0) - this.f486H0;
                                    Iterator<WheelItem> it = this.f499c0.iterator();
                                    int i8 = 0;
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        String str3 = str2;
                                        if (it.next().getName().equals(str3)) {
                                            this.f517u0 = i8;
                                            break;
                                        } else {
                                            i8++;
                                            str2 = str3;
                                        }
                                    }
                                    if (this.f493T && !TextUtils.isEmpty(this.f500d0)) {
                                        str = str + this.f500d0;
                                    }
                                    canvas2.drawText(str, (float) this.f484F0, f9, this.f496W);
                                    canvas.restore();
                                    this.f496W.setTextSize((float) this.f504h0);
                                }
                            }
                            canvas.save();
                            canvas2.clipRect(0.0f, 0.0f, (float) this.f521y0, this.f505i0);
                            canvas2.scale(1.0f, ((float) Math.sin(d)) * 0.8f);
                            float pow = (float) Math.pow((double) (Math.abs(f6) / 90.0f), 2.2d);
                            int i9 = this.f503g0;
                            if (i9 != 0) {
                                Paint paint = this.f495V;
                                int i10 = -1;
                                int i11 = i9 > 0 ? 1 : -1;
                                if (f6 <= 0.0f) {
                                    i10 = 1;
                                }
                                paint.setTextSkewX(((float) (i11 * i10)) * 0.5f * pow);
                                this.f495V.setAlpha((int) ((1.0f - pow) * 255.0f));
                            }
                            canvas2.drawText(str, ((float) this.f485G0) + (((float) this.f503g0) * pow), (float) this.f502f0, this.f495V);
                            canvas.restore();
                            canvas.restore();
                            this.f496W.setTextSize((float) this.f504h0);
                        } else {
                            canvas.save();
                            canvas2.clipRect(0.0f, 0.0f, (float) this.f521y0, this.f514r0 - cos);
                            canvas2.scale(1.0f, ((float) Math.sin(d)) * 1.0f);
                            canvas2.drawText(str, (float) this.f484F0, ((float) this.f502f0) - this.f486H0, this.f496W);
                            canvas.restore();
                            canvas.save();
                            canvas2.clipRect(0.0f, this.f514r0 - cos, (float) this.f521y0, (float) ((int) this.f505i0));
                            canvas2.scale(1.0f, ((float) Math.sin(d)) * 0.8f);
                            canvas2.drawText(str, (float) this.f485G0, (float) this.f502f0, this.f495V);
                            canvas.restore();
                        }
                    } else {
                        canvas.save();
                        canvas2.clipRect(0.0f, 0.0f, (float) this.f521y0, this.f513q0 - cos);
                        canvas2.scale(1.0f, ((float) Math.sin(d)) * 0.8f);
                        canvas2.drawText(str, (float) this.f485G0, (float) this.f502f0, this.f495V);
                        canvas.restore();
                        canvas.save();
                        canvas2.clipRect(0.0f, this.f513q0 - cos, (float) this.f521y0, (float) ((int) this.f505i0));
                        canvas2.scale(1.0f, ((float) Math.sin(d)) * 1.0f);
                        canvas2.drawText(str, (float) this.f484F0, ((float) this.f502f0) - this.f486H0, this.f496W);
                        canvas.restore();
                    }
                    canvas.restore();
                    this.f496W.setTextSize((float) this.f504h0);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.f482D0 = i;
        m809g();
        setMeasuredDimension(this.f521y0, this.f520x0);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = this.f490Q.onTouchEvent(motionEvent);
        ViewParent parent = getParent();
        int action = motionEvent.getAction();
        if (action == 0) {
            this.f481C0 = System.currentTimeMillis();
            m788a();
            this.f480B0 = motionEvent.getRawY();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        } else if (action != 2) {
            if (!onTouchEvent) {
                float y = motionEvent.getY();
                int i = this.f522z0;
                float f = this.f505i0;
                this.f479A0 = (int) ((((float) (((int) (((Math.acos((double) ((((float) i) - y) / ((float) i))) * ((double) this.f522z0)) + ((double) (f / 2.0f))) / ((double) f))) - (this.f519w0 / 2))) * f) - (((this.f515s0 % f) + f) % f));
                if (System.currentTimeMillis() - this.f481C0 > 120) {
                    m796b(3);
                } else {
                    m796b(1);
                }
            }
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(false);
            }
        } else {
            float rawY = this.f480B0 - motionEvent.getRawY();
            this.f480B0 = motionEvent.getRawY();
            this.f515s0 += rawY;
            if (!this.f512p0) {
                float f2 = ((float) (-this.f516t0)) * this.f505i0;
                float f3 = this.f505i0;
                float size = ((float) ((this.f499c0.size() - 1) - this.f516t0)) * f3;
                float f4 = this.f515s0;
                if (((double) f4) - (((double) f3) * 0.25d) < ((double) f2)) {
                    f2 = f4 - rawY;
                } else if (((double) f4) + (((double) f3) * 0.25d) > ((double) size)) {
                    size = f4 - rawY;
                }
                float f5 = this.f515s0;
                if (f5 < f2) {
                    this.f515s0 = (float) ((int) f2);
                } else if (f5 > size) {
                    this.f515s0 = (float) ((int) size);
                }
            }
        }
        if (motionEvent.getAction() != 0) {
            invalidate();
        }
        return true;
    }

    public final void setCycleDisable(boolean z) {
        this.f512p0 = !z;
    }

    public void setDividerColor(@ColorInt int i) {
        this.f509m0.mo9673a(i);
        this.f497a0.setColor(i);
    }

    public void setDividerConfig(C0840c cVar) {
        if (cVar == null) {
            this.f509m0.mo9675b(false);
            this.f509m0.mo9674a(false);
            return;
        }
        this.f509m0 = cVar;
        this.f497a0.setColor(cVar.f527c);
        this.f497a0.setStrokeWidth(cVar.f532h);
        this.f497a0.setAlpha(cVar.f530f);
        this.f498b0.setColor(cVar.f528d);
        this.f498b0.setAlpha(cVar.f529e);
    }

    public final void setGravity(int i) {
        this.f483E0 = i;
    }

    public final void setItems(List<?> list) {
        this.f499c0.clear();
        for (Object obj : list) {
            if (obj instanceof WheelItem) {
                this.f499c0.add((WheelItem) obj);
            } else if ((obj instanceof CharSequence) || (obj instanceof Number)) {
                this.f499c0.add(new C0846i(obj.toString(), null));
            } else {
                throw new IllegalArgumentException("please implements " + WheelItem.class.getName());
            }
        }
        m809g();
        invalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.qqtheme.framework.widget.WheelView.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      cn.qqtheme.framework.widget.WheelView.a(android.graphics.Paint, java.lang.String):int
      cn.qqtheme.framework.widget.WheelView.a(cn.qqtheme.framework.widget.WheelView, float):void
      cn.qqtheme.framework.widget.WheelView.a(cn.qqtheme.framework.widget.WheelView, int):void
      cn.qqtheme.framework.widget.WheelView.a(int, int):void
      cn.qqtheme.framework.widget.WheelView.a(java.util.List<?>, int):void
      cn.qqtheme.framework.widget.WheelView.a(java.util.List<java.lang.String>, java.lang.String):void
      cn.qqtheme.framework.widget.WheelView.a(java.lang.String, boolean):void */
    public final void setLabel(String str) {
        mo9641a(str, true);
    }

    @Deprecated
    public void setLineConfig(C0840c cVar) {
        setDividerConfig(cVar);
    }

    public final void setLineSpaceMultiplier(@FloatRange(from = 2.0d, mo446to = 4.0d) float f) {
        this.f510n0 = f;
        m805e();
    }

    public final void setOffset(@IntRange(from = 1, mo464to = 5) int i) {
        if (i < 1 || i > 5) {
            throw new IllegalArgumentException("must between 1 and 5");
        }
        int i2 = (i * 2) + 1;
        if (i % 2 != 0) {
            i--;
        }
        setVisibleItemCount(i2 + i);
    }

    public final void setOnItemSelectListener(C0843f fVar) {
        this.f491R = fVar;
    }

    @Deprecated
    public final void setOnWheelListener(C0844g gVar) {
        this.f492S = gVar;
    }

    @Deprecated
    public void setPadding(int i) {
        setTextPadding(i);
    }

    public final void setSelectedIndex(int i) {
        List<WheelItem> list = this.f499c0;
        if (list != null && !list.isEmpty()) {
            int size = this.f499c0.size();
            if (i == 0 || (i > 0 && i < size && i != this.f517u0)) {
                this.f516t0 = i;
                this.f515s0 = 0.0f;
                this.f479A0 = 0;
                invalidate();
            }
        }
    }

    public void setTextColor(@ColorInt int i) {
        this.f507k0 = i;
        this.f508l0 = i;
        this.f495V.setColor(i);
        this.f496W.setColor(i);
    }

    public void setTextPadding(int i) {
        this.f511o0 = C3819a.m11038a(getContext(), (float) i);
    }

    public final void setTextSize(float f) {
        if (f > 0.0f) {
            this.f504h0 = (int) (getContext().getResources().getDisplayMetrics().density * f);
            this.f495V.setTextSize((float) this.f504h0);
            this.f496W.setTextSize((float) this.f504h0);
        }
    }

    public void setTextSizeAutoFit(boolean z) {
        this.f488J0 = z;
    }

    public void setTextSkewXOffset(int i) {
        this.f503g0 = i;
        if (i != 0) {
            this.f496W.setTextScaleX(1.0f);
        }
    }

    public final void setTypeface(Typeface typeface) {
        this.f506j0 = typeface;
        this.f495V.setTypeface(this.f506j0);
        this.f496W.setTypeface(this.f506j0);
    }

    public void setUseWeight(boolean z) {
        this.f487I0 = z;
    }

    public final void setVisibleItemCount(int i) {
        if (i % 2 == 0) {
            throw new IllegalArgumentException("must be odd");
        } else if (i != this.f519w0) {
            this.f519w0 = i;
        }
    }

    public WheelView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f493T = true;
        this.f499c0 = new ArrayList();
        this.f503g0 = 0;
        this.f504h0 = 16;
        this.f506j0 = Typeface.DEFAULT;
        this.f507k0 = -4473925;
        this.f508l0 = -16611122;
        this.f509m0 = new C0840c();
        this.f510n0 = 2.0f;
        this.f511o0 = -1;
        this.f512p0 = true;
        this.f515s0 = 0.0f;
        this.f516t0 = -1;
        this.f519w0 = 7;
        this.f479A0 = 0;
        this.f480B0 = 0.0f;
        this.f481C0 = 0;
        this.f483E0 = 17;
        this.f484F0 = 0;
        this.f485G0 = 0;
        this.f487I0 = false;
        this.f488J0 = true;
        float f = getResources().getDisplayMetrics().density;
        if (f < 1.0f) {
            this.f486H0 = 2.4f;
        } else {
            int i = (1.0f > f ? 1 : (1.0f == f ? 0 : -1));
            if (i <= 0 && f < 2.0f) {
                this.f486H0 = 3.6f;
            } else if (i <= 0 && f < 2.0f) {
                this.f486H0 = 4.5f;
            } else if (2.0f <= f && f < 3.0f) {
                this.f486H0 = 6.0f;
            } else if (f >= 3.0f) {
                this.f486H0 = f * 2.5f;
            }
        }
        m805e();
        m790a(context);
    }

    /* renamed from: c */
    private void m800c() {
        this.f495V = new Paint();
        this.f495V.setAntiAlias(true);
        this.f495V.setColor(this.f507k0);
        this.f495V.setTypeface(this.f506j0);
        this.f495V.setTextSize((float) this.f504h0);
        this.f496W = new Paint();
        this.f496W.setAntiAlias(true);
        this.f496W.setColor(this.f508l0);
        this.f496W.setTextScaleX(1.0f);
        this.f496W.setTypeface(this.f506j0);
        this.f496W.setTextSize((float) this.f504h0);
        this.f497a0 = new Paint();
        this.f497a0.setAntiAlias(true);
        this.f497a0.setColor(this.f509m0.f527c);
        this.f497a0.setStrokeWidth(this.f509m0.f532h);
        this.f497a0.setAlpha(this.f509m0.f530f);
        this.f498b0 = new Paint();
        this.f498b0.setAntiAlias(true);
        this.f498b0.setColor(this.f509m0.f528d);
        this.f498b0.setAlpha(this.f509m0.f529e);
        setLayerType(1, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m803d() {
        if (this.f491R != null || this.f492S != null) {
            postDelayed(new C0839b(), 200);
        }
    }

    /* renamed from: e */
    private void m805e() {
        float f = this.f510n0;
        if (f < 1.5f) {
            this.f510n0 = 1.5f;
        } else if (f > 4.0f) {
            this.f510n0 = 4.0f;
        }
    }

    /* renamed from: f */
    private void m807f() {
        Rect rect = new Rect();
        for (int i = 0; i < this.f499c0.size(); i++) {
            String a = m787a(this.f499c0.get(i));
            this.f496W.getTextBounds(a, 0, a.length(), rect);
            int width = rect.width();
            if (width > this.f501e0) {
                this.f501e0 = width;
            }
            this.f496W.getTextBounds("测试", 0, 2, rect);
            this.f502f0 = rect.height() + 2;
        }
        this.f505i0 = this.f510n0 * ((float) this.f502f0);
    }

    /* renamed from: g */
    private void m809g() {
        int i;
        if (this.f499c0 != null) {
            m807f();
            int i2 = (int) (this.f505i0 * ((float) (this.f519w0 - 1)));
            this.f520x0 = (int) (((double) (i2 * 2)) / 3.141592653589793d);
            this.f522z0 = (int) (((double) i2) / 3.141592653589793d);
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (this.f487I0) {
                this.f521y0 = View.MeasureSpec.getSize(this.f482D0);
            } else if (layoutParams == null || (i = layoutParams.width) <= 0) {
                this.f521y0 = this.f501e0;
                if (this.f511o0 < 0) {
                    this.f511o0 = C3819a.m11038a(getContext(), 13.0f);
                }
                this.f521y0 += this.f511o0 * 2;
                if (!TextUtils.isEmpty(this.f500d0)) {
                    this.f521y0 += m786a(this.f496W, this.f500d0);
                }
            } else {
                this.f521y0 = i;
            }
            C3820c.m11050b("measuredWidth=" + this.f521y0 + ",measuredHeight=" + this.f520x0);
            int i3 = this.f520x0;
            float f = this.f505i0;
            this.f513q0 = (((float) i3) - f) / 2.0f;
            this.f514r0 = (((float) i3) + f) / 2.0f;
            if (this.f516t0 == -1) {
                if (this.f512p0) {
                    this.f516t0 = (this.f499c0.size() + 1) / 2;
                } else {
                    this.f516t0 = 0;
                }
            }
            this.f518v0 = this.f516t0;
        }
    }

    /* renamed from: b */
    private void m795b() {
        if (isInEditMode()) {
            setItems(new String[]{"李玉江", "男", "贵州", "穿青人"});
        }
    }

    /* renamed from: cn.qqtheme.framework.widget.WheelView$c */
    public static class C0840c {

        /* renamed from: a */
        protected boolean f525a = true;

        /* renamed from: b */
        protected boolean f526b = false;

        /* renamed from: c */
        protected int f527c = -8139290;

        /* renamed from: d */
        protected int f528d = -4473925;

        /* renamed from: e */
        protected int f529e = 100;

        /* renamed from: f */
        protected int f530f = 220;

        /* renamed from: g */
        protected float f531g = 0.1f;

        /* renamed from: h */
        protected float f532h = 2.0f;

        /* renamed from: a */
        public C0840c mo9674a(boolean z) {
            this.f526b = z;
            if (z && this.f527c == -8139290) {
                this.f527c = this.f528d;
                this.f530f = 255;
            }
            return this;
        }

        /* renamed from: b */
        public C0840c mo9675b(boolean z) {
            this.f525a = z;
            return this;
        }

        public String toString() {
            return "visible=" + this.f525a + ",color=" + this.f527c + ",alpha=" + this.f530f + ",thick=" + this.f532h;
        }

        /* renamed from: a */
        public C0840c mo9673a(@ColorInt int i) {
            this.f527c = i;
            return this;
        }
    }

    /* renamed from: a */
    public final void mo9642a(List<?> list, int i) {
        setItems(list);
        setSelectedIndex(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m796b(int i) {
        m788a();
        if (i == 2 || i == 3) {
            float f = this.f515s0;
            float f2 = this.f505i0;
            this.f479A0 = (int) (((f % f2) + f2) % f2);
            int i2 = this.f479A0;
            if (((float) i2) > f2 / 2.0f) {
                this.f479A0 = (int) (f2 - ((float) i2));
            } else {
                this.f479A0 = -i2;
            }
        }
        this.f494U = Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(new C0845h(this, this.f479A0), 0, 10, TimeUnit.MILLISECONDS);
    }

    /* renamed from: a */
    public final void mo9643a(List<String> list, String str) {
        int indexOf = list.indexOf(str);
        if (indexOf == -1) {
            indexOf = 0;
        }
        mo9642a(list, indexOf);
    }

    /* renamed from: a */
    public final void mo9641a(String str, boolean z) {
        this.f500d0 = str;
        this.f493T = z;
    }

    /* renamed from: a */
    public void mo9640a(@ColorInt int i, @ColorInt int i2) {
        this.f507k0 = i;
        this.f508l0 = i2;
        this.f495V.setColor(i);
        this.f496W.setColor(i2);
    }

    public final void setItems(String[] strArr) {
        setItems(Arrays.asList(strArr));
    }

    /* renamed from: b */
    private void m797b(String str) {
        Rect rect = new Rect();
        this.f495V.getTextBounds(str, 0, str.length(), rect);
        int i = this.f483E0;
        if (i == 3) {
            this.f485G0 = C3819a.m11038a(getContext(), 8.0f);
        } else if (i == 5) {
            this.f485G0 = (this.f521y0 - rect.width()) - ((int) this.f486H0);
        } else if (i == 17) {
            this.f485G0 = (int) (((double) (this.f521y0 - rect.width())) * 0.5d);
        }
    }

    /* renamed from: a */
    private void m790a(Context context) {
        this.f489P = new C0842e(this);
        this.f490Q = new GestureDetector(context, new C0838a());
        this.f490Q.setIsLongpressEnabled(false);
        m800c();
        m795b();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m789a(float f) {
        m788a();
        this.f494U = Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(new C0841d(this, f), 0, 5, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m788a() {
        ScheduledFuture<?> scheduledFuture = this.f494U;
        if (scheduledFuture != null && !scheduledFuture.isCancelled()) {
            this.f494U.cancel(true);
            this.f494U = null;
        }
    }

    /* renamed from: c */
    private void m801c(String str) {
        Rect rect = new Rect();
        this.f496W.getTextBounds(str, 0, str.length(), rect);
        int i = this.f504h0;
        for (int width = rect.width(); width > this.f521y0; width = rect.width()) {
            i--;
            this.f496W.setTextSize((float) i);
            this.f496W.getTextBounds(str, 0, str.length(), rect);
        }
        this.f495V.setTextSize((float) i);
    }

    /* renamed from: a */
    private int m785a(int i) {
        if (i < 0) {
            return m785a(i + this.f499c0.size());
        }
        return i > this.f499c0.size() + -1 ? m785a(i - this.f499c0.size()) : i;
    }

    /* renamed from: a */
    private String m787a(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof WheelItem) {
            return ((WheelItem) obj).getName();
        }
        if (!(obj instanceof Integer)) {
            return obj.toString();
        }
        return String.format(Locale.getDefault(), "%02d", Integer.valueOf(((Integer) obj).intValue()));
    }

    /* renamed from: a */
    private void m793a(String str) {
        Rect rect = new Rect();
        this.f496W.getTextBounds(str, 0, str.length(), rect);
        int i = this.f483E0;
        if (i == 3) {
            this.f484F0 = C3819a.m11038a(getContext(), 8.0f);
        } else if (i == 5) {
            this.f484F0 = (this.f521y0 - rect.width()) - ((int) this.f486H0);
        } else if (i == 17) {
            this.f484F0 = (int) (((double) (this.f521y0 - rect.width())) * 0.5d);
        }
    }

    /* renamed from: a */
    private int m786a(Paint paint, String str) {
        if (str == null || str.length() <= 0) {
            return 0;
        }
        int length = str.length();
        float[] fArr = new float[length];
        paint.getTextWidths(str, fArr);
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += (int) Math.ceil((double) fArr[i2]);
        }
        return i;
    }
}
