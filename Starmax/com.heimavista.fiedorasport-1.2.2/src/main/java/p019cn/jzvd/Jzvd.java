package p019cn.jzvd;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.AudioManager;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.util.MimeTypes;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/* renamed from: cn.jzvd.Jzvd */
public abstract class Jzvd extends FrameLayout implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, View.OnTouchListener {
    public static LinkedList<ViewGroup> CONTAINER_LIST = new LinkedList<>();
    public static Jzvd CURRENT_JZVD = null;
    public static int FULLSCREEN_ORIENTATION = 6;
    public static int NORMAL_ORIENTATION = 1;
    public static int ON_PLAY_PAUSE_TMP_STATE = 0;
    public static boolean SAVE_PROGRESS = false;
    public static final int SCREEN_FULLSCREEN = 1;
    public static final int SCREEN_NORMAL = 0;
    public static final int SCREEN_TINY = 2;
    public static final int STATE_AUTO_COMPLETE = 6;
    public static final int STATE_ERROR = 7;
    public static final int STATE_IDLE = -1;
    public static final int STATE_NORMAL = 0;
    public static final int STATE_PAUSE = 5;
    public static final int STATE_PLAYING = 4;
    public static final int STATE_PREPARED = 3;
    public static final int STATE_PREPARING = 1;
    public static final int STATE_PREPARING_CHANGING_URL = 2;
    public static final String TAG = "JZVD";
    public static final int THRESHOLD = 80;
    public static boolean TOOL_BAR_EXIST = true;
    public static int VIDEO_IMAGE_DISPLAY_TYPE = 0;
    public static final int VIDEO_IMAGE_DISPLAY_TYPE_ADAPTER = 0;
    public static final int VIDEO_IMAGE_DISPLAY_TYPE_FILL_PARENT = 1;
    public static final int VIDEO_IMAGE_DISPLAY_TYPE_FILL_SCROP = 2;
    public static final int VIDEO_IMAGE_DISPLAY_TYPE_ORIGINAL = 3;
    public static boolean WIFI_TIP_DIALOG_SHOWED = false;
    public static long lastAutoFullscreenTime = 0;
    public static AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        /* class p019cn.jzvd.Jzvd.C08161 */

        public void onAudioFocusChange(int i) {
            if (i == -2) {
                try {
                    Jzvd jzvd = Jzvd.CURRENT_JZVD;
                    if (!(jzvd == null || jzvd.state != 4 || jzvd.startButton == null)) {
                        jzvd.startButton.performClick();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                Log.d("JZVD", "AUDIOFOCUS_LOSS_TRANSIENT [" + hashCode() + "]");
            } else if (i == -1) {
                Jzvd.releaseAllVideos();
                Log.d("JZVD", "AUDIOFOCUS_LOSS [" + hashCode() + "]");
            }
        }
    };
    protected Timer UPDATE_PROGRESS_TIMER;
    public ViewGroup bottomContainer;
    public TextView currentTimeTextView;
    public ImageView fullscreenButton;
    protected long gobakFullscreenTime = 0;
    public int heightRatio = 0;
    public JZDataSource jzDataSource;
    protected AudioManager mAudioManager;
    protected boolean mChangeBrightness;
    protected boolean mChangePosition;
    protected boolean mChangeVolume;
    protected float mDownX;
    protected float mDownY;
    protected float mGestureDownBrightness;
    protected long mGestureDownPosition;
    protected int mGestureDownVolume;
    protected ProgressTimerTask mProgressTimerTask;
    protected int mScreenHeight;
    protected int mScreenWidth;
    protected long mSeekTimePosition;
    protected boolean mTouchingProgressBar;
    public JZMediaInterface mediaInterface;
    public Class mediaInterfaceClass;
    public int positionInList = -1;
    public boolean preloading = false;
    public SeekBar progressBar;
    public int screen = -1;
    public long seekToInAdvance = 0;
    public int seekToManulPosition = -1;
    public ImageView startButton;
    public int state = -1;
    public JZTextureView textureView;
    public ViewGroup textureViewContainer;
    public ViewGroup topContainer;
    public TextView totalTimeTextView;
    public int videoRotation = 0;
    public int widthRatio = 0;

    /* renamed from: cn.jzvd.Jzvd$JZAutoFullscreenListener */
    public static class JZAutoFullscreenListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            float[] fArr = sensorEvent.values;
            float f = fArr[0];
            float f2 = fArr[1];
            float f3 = fArr[2];
            if ((f < -12.0f || f > 12.0f) && System.currentTimeMillis() - Jzvd.lastAutoFullscreenTime > AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS) {
                Jzvd jzvd = Jzvd.CURRENT_JZVD;
                if (jzvd != null) {
                    jzvd.autoFullscreen(f);
                }
                Jzvd.lastAutoFullscreenTime = System.currentTimeMillis();
            }
        }
    }

    /* renamed from: cn.jzvd.Jzvd$ProgressTimerTask */
    public class ProgressTimerTask extends TimerTask {
        public ProgressTimerTask() {
        }

        public void run() {
            int i = Jzvd.this.state;
            if (i == 4 || i == 5) {
                Jzvd.this.post(new Runnable() {
                    /* class p019cn.jzvd.Jzvd.ProgressTimerTask.C08171 */

                    public void run() {
                        long currentPositionWhenPlaying = Jzvd.this.getCurrentPositionWhenPlaying();
                        long duration = Jzvd.this.getDuration();
                        Jzvd.this.onProgress((int) ((100 * currentPositionWhenPlaying) / (duration == 0 ? 1 : duration)), currentPositionWhenPlaying, duration);
                    }
                });
            }
        }
    }

    public Jzvd(Context context) {
        super(context);
        init(context);
    }

    public static boolean backPress() {
        Jzvd jzvd;
        Jzvd jzvd2;
        Log.i("JZVD", "backPress");
        if (CONTAINER_LIST.size() != 0 && (jzvd2 = CURRENT_JZVD) != null) {
            jzvd2.gotoScreenNormal();
            return true;
        } else if (CONTAINER_LIST.size() != 0 || (jzvd = CURRENT_JZVD) == null || jzvd.screen == 0) {
            return false;
        } else {
            jzvd.clearFloatScreen();
            return true;
        }
    }

    public static void clearSavedProgress(Context context, String str) {
        JZUtils.clearSavedProgress(context, str);
    }

    public static void goOnPlayOnPause() {
        Jzvd jzvd = CURRENT_JZVD;
        if (jzvd != null) {
            int i = jzvd.state;
            if (i == 6 || i == 0 || i == 7) {
                releaseAllVideos();
                return;
            }
            ON_PLAY_PAUSE_TMP_STATE = i;
            jzvd.onStatePause();
            CURRENT_JZVD.mediaInterface.pause();
        }
    }

    public static void goOnPlayOnResume() {
        Jzvd jzvd = CURRENT_JZVD;
        if (jzvd != null && jzvd.state == 5) {
            if (ON_PLAY_PAUSE_TMP_STATE == 5) {
                jzvd.onStatePause();
                CURRENT_JZVD.mediaInterface.pause();
            } else {
                jzvd.onStatePlaying();
                CURRENT_JZVD.mediaInterface.start();
            }
            ON_PLAY_PAUSE_TMP_STATE = 0;
        }
    }

    public static void releaseAllVideos() {
        Log.d("JZVD", "releaseAllVideos");
        Jzvd jzvd = CURRENT_JZVD;
        if (jzvd != null) {
            jzvd.reset();
            CURRENT_JZVD = null;
        }
    }

    public static void setCurrentJzvd(Jzvd jzvd) {
        Jzvd jzvd2 = CURRENT_JZVD;
        if (jzvd2 != null) {
            jzvd2.reset();
        }
        CURRENT_JZVD = jzvd;
    }

    public static void setTextureViewRotation(int i) {
        JZTextureView jZTextureView;
        Jzvd jzvd = CURRENT_JZVD;
        if (jzvd != null && (jZTextureView = jzvd.textureView) != null) {
            jZTextureView.setRotation((float) i);
        }
    }

    public static void setVideoImageDisplayType(int i) {
        JZTextureView jZTextureView;
        VIDEO_IMAGE_DISPLAY_TYPE = i;
        Jzvd jzvd = CURRENT_JZVD;
        if (jzvd != null && (jZTextureView = jzvd.textureView) != null) {
            jZTextureView.requestLayout();
        }
    }

    public static void startFullscreenDirectly(Context context, Class cls, String str, String str2) {
        startFullscreenDirectly(context, cls, new JZDataSource(str, str2));
    }

    public void addTextureView() {
        Log.d("JZVD", "addTextureView [" + hashCode() + "] ");
        JZTextureView jZTextureView = this.textureView;
        if (jZTextureView != null) {
            this.textureViewContainer.removeView(jZTextureView);
        }
        this.textureView = new JZTextureView(getContext().getApplicationContext());
        this.textureView.setSurfaceTextureListener(this.mediaInterface);
        this.textureViewContainer.addView(this.textureView, new FrameLayout.LayoutParams(-1, -1, 17));
    }

    public void autoFullscreen(float f) {
        int i;
        if (CURRENT_JZVD != null) {
            int i2 = this.state;
            if ((i2 == 4 || i2 == 5) && (i = this.screen) != 1 && i != 2) {
                if (f > 0.0f) {
                    JZUtils.setRequestedOrientation(getContext(), 0);
                } else {
                    JZUtils.setRequestedOrientation(getContext(), 8);
                }
                gotoScreenFullscreen();
            }
        }
    }

    public void autoQuitFullscreen() {
        if (System.currentTimeMillis() - lastAutoFullscreenTime > AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS && this.state == 4 && this.screen == 1) {
            lastAutoFullscreenTime = System.currentTimeMillis();
            backPress();
        }
    }

    public void cancelProgressTimer() {
        Timer timer = this.UPDATE_PROGRESS_TIMER;
        if (timer != null) {
            timer.cancel();
        }
        ProgressTimerTask progressTimerTask = this.mProgressTimerTask;
        if (progressTimerTask != null) {
            progressTimerTask.cancel();
        }
    }

    public void changeUrl(String str, String str2, long j) {
        changeUrl(new JZDataSource(str, str2), j);
    }

    public void clearFloatScreen() {
        JZUtils.showStatusBar(getContext());
        JZUtils.setRequestedOrientation(getContext(), NORMAL_ORIENTATION);
        JZUtils.showSystemUI(getContext());
        ((ViewGroup) JZUtils.scanForActivity(getContext()).getWindow().getDecorView()).removeView(this);
        JZMediaInterface jZMediaInterface = this.mediaInterface;
        if (jZMediaInterface != null) {
            jZMediaInterface.release();
        }
        CURRENT_JZVD = null;
    }

    public void cloneAJzvd(ViewGroup viewGroup) {
        try {
            Jzvd jzvd = (Jzvd) getClass().getConstructor(Context.class).newInstance(getContext());
            super.setId(getId());
            viewGroup.addView(jzvd);
            jzvd.setUp(this.jzDataSource.cloneMe(), 0, this.mediaInterfaceClass);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
        }
    }

    public void dismissBrightnessDialog() {
    }

    public void dismissProgressDialog() {
    }

    public void dismissVolumeDialog() {
    }

    public Context getApplicationContext() {
        Context applicationContext;
        Context context = getContext();
        return (context == null || (applicationContext = context.getApplicationContext()) == null) ? context : applicationContext;
    }

    public long getCurrentPositionWhenPlaying() {
        int i = this.state;
        if (i != 4 && i != 5) {
            return 0;
        }
        try {
            return this.mediaInterface.getCurrentPosition();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long getDuration() {
        try {
            return this.mediaInterface.getDuration();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public abstract int getLayoutId();

    public void gotoScreenFullscreen() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        viewGroup.removeView(this);
        cloneAJzvd(viewGroup);
        CONTAINER_LIST.add(viewGroup);
        ((ViewGroup) JZUtils.scanForActivity(getContext()).getWindow().getDecorView()).addView(this, new FrameLayout.LayoutParams(-1, -1));
        setScreenFullscreen();
        JZUtils.hideStatusBar(getContext());
        JZUtils.setRequestedOrientation(getContext(), FULLSCREEN_ORIENTATION);
        JZUtils.hideSystemUI(getContext());
    }

    public void gotoScreenNormal() {
        this.gobakFullscreenTime = System.currentTimeMillis();
        ((ViewGroup) JZUtils.scanForActivity(getContext()).getWindow().getDecorView()).removeView(this);
        CONTAINER_LIST.getLast().removeAllViews();
        CONTAINER_LIST.getLast().addView(this, new FrameLayout.LayoutParams(-1, -1));
        CONTAINER_LIST.pop();
        setScreenNormal();
        JZUtils.showStatusBar(getContext());
        JZUtils.setRequestedOrientation(getContext(), NORMAL_ORIENTATION);
        JZUtils.showSystemUI(getContext());
    }

    public void init(Context context) {
        View.inflate(context, getLayoutId(), this);
        this.startButton = (ImageView) findViewById(C0824R.C0827id.start);
        this.fullscreenButton = (ImageView) findViewById(C0824R.C0827id.fullscreen);
        this.progressBar = (SeekBar) findViewById(C0824R.C0827id.bottom_seek_progress);
        this.currentTimeTextView = (TextView) findViewById(C0824R.C0827id.current);
        this.totalTimeTextView = (TextView) findViewById(C0824R.C0827id.total);
        this.bottomContainer = (ViewGroup) findViewById(C0824R.C0827id.layout_bottom);
        this.textureViewContainer = (ViewGroup) findViewById(C0824R.C0827id.surface_container);
        this.topContainer = (ViewGroup) findViewById(C0824R.C0827id.layout_top);
        ImageView imageView = this.startButton;
        if (imageView != null) {
            imageView.setOnClickListener(this);
        }
        this.fullscreenButton.setOnClickListener(this);
        this.progressBar.setOnSeekBarChangeListener(this);
        this.bottomContainer.setOnClickListener(this);
        this.textureViewContainer.setOnClickListener(this);
        this.textureViewContainer.setOnTouchListener(this);
        this.mScreenWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        this.mScreenHeight = getContext().getResources().getDisplayMetrics().heightPixels;
        this.state = -1;
    }

    public void onAutoCompletion() {
        Runtime.getRuntime().gc();
        Log.i("JZVD", "onAutoCompletion  [" + hashCode() + "] ");
        cancelProgressTimer();
        dismissBrightnessDialog();
        dismissProgressDialog();
        dismissVolumeDialog();
        onStateAutoComplete();
        this.mediaInterface.release();
        JZUtils.scanForActivity(getContext()).getWindow().clearFlags(128);
        JZUtils.saveProgress(getContext(), this.jzDataSource.getCurrentUrl(), 0);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == C0824R.C0827id.start) {
            Log.i("JZVD", "onClick start [" + hashCode() + "] ");
            JZDataSource jZDataSource = this.jzDataSource;
            if (jZDataSource == null || jZDataSource.urlsMap.isEmpty() || this.jzDataSource.getCurrentUrl() == null) {
                Toast.makeText(getContext(), getResources().getString(C0824R.string.no_url), 0).show();
                return;
            }
            int i = this.state;
            if (i == 0) {
                if (this.jzDataSource.getCurrentUrl().toString().startsWith("file") || this.jzDataSource.getCurrentUrl().toString().startsWith("/") || JZUtils.isWifiConnected(getContext()) || WIFI_TIP_DIALOG_SHOWED) {
                    startVideo();
                } else {
                    showWifiDialog();
                }
            } else if (i == 4) {
                Log.d("JZVD", "pauseVideo [" + hashCode() + "] ");
                this.mediaInterface.pause();
                onStatePause();
            } else if (i == 5) {
                this.mediaInterface.start();
                onStatePlaying();
            } else if (i == 6) {
                startVideo();
            }
        } else if (id == C0824R.C0827id.fullscreen) {
            Log.i("JZVD", "onClick fullscreen [" + hashCode() + "] ");
            if (this.state != 6) {
                if (this.screen == 1) {
                    backPress();
                    return;
                }
                Log.d("JZVD", "toFullscreenActivity [" + hashCode() + "] ");
                gotoScreenFullscreen();
            }
        }
    }

    public void onError(int i, int i2) {
        Log.e("JZVD", "onError " + i + " - " + i2 + " [" + hashCode() + "] ");
        if (i != 38 && i2 != -38 && i != -38 && i2 != 38 && i2 != -19) {
            onStateError();
            this.mediaInterface.release();
        }
    }

    public void onInfo(int i, int i2) {
        Log.d("JZVD", "onInfo what - " + i + " extra - " + i2);
        if (i == 3) {
            int i3 = this.state;
            if (i3 == 3 || i3 == 2) {
                onStatePlaying();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = this.screen;
        if (i3 == 1 || i3 == 2) {
            super.onMeasure(i, i2);
        } else if (this.widthRatio == 0 || this.heightRatio == 0) {
            super.onMeasure(i, i2);
        } else {
            int size = View.MeasureSpec.getSize(i);
            int i4 = (int) ((((float) size) * ((float) this.heightRatio)) / ((float) this.widthRatio));
            setMeasuredDimension(size, i4);
            getChildAt(0).measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
        }
    }

    public void onPrepared() {
        Log.i("JZVD", "onPrepared  [" + hashCode() + "] ");
        this.state = 3;
        if (!this.preloading) {
            this.mediaInterface.start();
            this.preloading = false;
        }
        if (this.jzDataSource.getCurrentUrl().toString().toLowerCase().contains("mp3") || this.jzDataSource.getCurrentUrl().toString().toLowerCase().contains("wav")) {
            onStatePlaying();
        }
    }

    public void onProgress(int i, long j, long j2) {
        if (!this.mTouchingProgressBar) {
            int i2 = this.seekToManulPosition;
            if (i2 != -1) {
                if (i2 <= i) {
                    this.seekToManulPosition = -1;
                } else {
                    return;
                }
            } else if (i != 0) {
                this.progressBar.setProgress(i);
            }
        }
        if (j != 0) {
            this.currentTimeTextView.setText(JZUtils.stringForTime(j));
        }
        this.totalTimeTextView.setText(JZUtils.stringForTime(j2));
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (z) {
            this.currentTimeTextView.setText(JZUtils.stringForTime((((long) i) * getDuration()) / 100));
        }
    }

    public void onSeekComplete() {
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.i("JZVD", "bottomProgress onStartTrackingTouch [" + hashCode() + "] ");
        cancelProgressTimer();
        for (ViewParent parent = getParent(); parent != null; parent = parent.getParent()) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    public void onStateAutoComplete() {
        Log.i("JZVD", "onStateAutoComplete  [" + hashCode() + "] ");
        this.state = 6;
        cancelProgressTimer();
        this.progressBar.setProgress(100);
        this.currentTimeTextView.setText(this.totalTimeTextView.getText());
    }

    public void onStateError() {
        Log.i("JZVD", "onStateError  [" + hashCode() + "] ");
        this.state = 7;
        cancelProgressTimer();
    }

    public void onStateNormal() {
        Log.i("JZVD", "onStateNormal  [" + hashCode() + "] ");
        this.state = 0;
        cancelProgressTimer();
        JZMediaInterface jZMediaInterface = this.mediaInterface;
        if (jZMediaInterface != null) {
            jZMediaInterface.release();
        }
    }

    public void onStatePause() {
        Log.i("JZVD", "onStatePause  [" + hashCode() + "] ");
        this.state = 5;
        startProgressTimer();
    }

    public void onStatePlaying() {
        Log.i("JZVD", "onStatePlaying  [" + hashCode() + "] ");
        if (this.state == 3) {
            long j = this.seekToInAdvance;
            if (j != 0) {
                this.mediaInterface.seekTo(j);
                this.seekToInAdvance = 0;
            } else {
                long savedProgress = JZUtils.getSavedProgress(getContext(), this.jzDataSource.getCurrentUrl());
                if (savedProgress != 0) {
                    this.mediaInterface.seekTo(savedProgress);
                }
            }
        }
        this.state = 4;
        startProgressTimer();
    }

    public void onStatePreparing() {
        Log.i("JZVD", "onStatePreparing  [" + hashCode() + "] ");
        this.state = 1;
        resetProgressAndTime();
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.i("JZVD", "bottomProgress onStopTrackingTouch [" + hashCode() + "] ");
        startProgressTimer();
        for (ViewParent parent = getParent(); parent != null; parent = parent.getParent()) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
        int i = this.state;
        if (i == 4 || i == 5) {
            long progress = (((long) seekBar.getProgress()) * getDuration()) / 100;
            this.seekToManulPosition = seekBar.getProgress();
            this.mediaInterface.seekTo(progress);
            Log.i("JZVD", "seekTo " + progress + " [" + hashCode() + "] ");
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i;
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        if (view.getId() == C0824R.C0827id.surface_container) {
            int action = motionEvent.getAction();
            if (action == 0) {
                Log.i("JZVD", "onTouch surfaceContainer actionDown [" + hashCode() + "] ");
                this.mTouchingProgressBar = true;
                this.mDownX = x;
                this.mDownY = y;
                this.mChangeVolume = false;
                this.mChangePosition = false;
                this.mChangeBrightness = false;
            } else if (action == 1) {
                Log.i("JZVD", "onTouch surfaceContainer actionUp [" + hashCode() + "] ");
                this.mTouchingProgressBar = false;
                dismissProgressDialog();
                dismissVolumeDialog();
                dismissBrightnessDialog();
                if (this.mChangePosition) {
                    this.mediaInterface.seekTo(this.mSeekTimePosition);
                    long duration = getDuration();
                    long j = this.mSeekTimePosition * 100;
                    if (duration == 0) {
                        duration = 1;
                    }
                    this.progressBar.setProgress((int) (j / duration));
                }
                startProgressTimer();
            } else if (action == 2) {
                Log.i("JZVD", "onTouch surfaceContainer actionMove [" + hashCode() + "] ");
                float f = x - this.mDownX;
                float f2 = y - this.mDownY;
                float abs = Math.abs(f);
                float abs2 = Math.abs(f2);
                if (this.screen == 1 && !this.mChangePosition && !this.mChangeVolume && !this.mChangeBrightness && (abs > 80.0f || abs2 > 80.0f)) {
                    cancelProgressTimer();
                    if (i >= 0) {
                        if (this.state != 7) {
                            this.mChangePosition = true;
                            this.mGestureDownPosition = getCurrentPositionWhenPlaying();
                        }
                    } else if (this.mDownX < ((float) this.mScreenWidth) * 0.5f) {
                        this.mChangeBrightness = true;
                        float f3 = JZUtils.getWindow(getContext()).getAttributes().screenBrightness;
                        if (f3 < 0.0f) {
                            try {
                                this.mGestureDownBrightness = (float) Settings.System.getInt(getContext().getContentResolver(), "screen_brightness");
                                Log.i("JZVD", "current system brightness: " + this.mGestureDownBrightness);
                            } catch (Settings.SettingNotFoundException e) {
                                e.printStackTrace();
                            }
                        } else {
                            this.mGestureDownBrightness = f3 * 255.0f;
                            Log.i("JZVD", "current activity brightness: " + this.mGestureDownBrightness);
                        }
                    } else {
                        this.mChangeVolume = true;
                        this.mGestureDownVolume = this.mAudioManager.getStreamVolume(3);
                    }
                }
                if (this.mChangePosition) {
                    long duration2 = getDuration();
                    this.mSeekTimePosition = (long) ((int) (((float) this.mGestureDownPosition) + ((((float) duration2) * f) / ((float) this.mScreenWidth))));
                    if (this.mSeekTimePosition > duration2) {
                        this.mSeekTimePosition = duration2;
                    }
                    showProgressDialog(f, JZUtils.stringForTime(this.mSeekTimePosition), this.mSeekTimePosition, JZUtils.stringForTime(duration2), duration2);
                }
                if (this.mChangeVolume) {
                    f2 = -f2;
                    int streamMaxVolume = this.mAudioManager.getStreamMaxVolume(3);
                    this.mAudioManager.setStreamVolume(3, this.mGestureDownVolume + ((int) (((((float) streamMaxVolume) * f2) * 3.0f) / ((float) this.mScreenHeight))), 0);
                    showVolumeDialog(-f2, (int) (((float) ((this.mGestureDownVolume * 100) / streamMaxVolume)) + (((f2 * 3.0f) * 100.0f) / ((float) this.mScreenHeight))));
                }
                if (this.mChangeBrightness) {
                    float f4 = -f2;
                    WindowManager.LayoutParams attributes = JZUtils.getWindow(getContext()).getAttributes();
                    float f5 = this.mGestureDownBrightness;
                    float f6 = (float) ((int) (((f4 * 255.0f) * 3.0f) / ((float) this.mScreenHeight)));
                    if ((f5 + f6) / 255.0f >= 1.0f) {
                        attributes.screenBrightness = 1.0f;
                    } else if ((f5 + f6) / 255.0f <= 0.0f) {
                        attributes.screenBrightness = 0.01f;
                    } else {
                        attributes.screenBrightness = (f5 + f6) / 255.0f;
                    }
                    JZUtils.getWindow(getContext()).setAttributes(attributes);
                    showBrightnessDialog((int) (((this.mGestureDownBrightness * 100.0f) / 255.0f) + (((f4 * 3.0f) * 100.0f) / ((float) this.mScreenHeight))));
                }
            }
        }
        return false;
    }

    public void onVideoSizeChanged(int i, int i2) {
        Log.i("JZVD", "onVideoSizeChanged  [" + hashCode() + "] ");
        JZTextureView jZTextureView = this.textureView;
        if (jZTextureView != null) {
            int i3 = this.videoRotation;
            if (i3 != 0) {
                jZTextureView.setRotation((float) i3);
            }
            this.textureView.setVideoSize(i, i2);
        }
    }

    public void reset() {
        Log.i("JZVD", "reset  [" + hashCode() + "] ");
        int i = this.state;
        if (i == 4 || i == 5) {
            JZUtils.saveProgress(getContext(), this.jzDataSource.getCurrentUrl(), getCurrentPositionWhenPlaying());
        }
        cancelProgressTimer();
        dismissBrightnessDialog();
        dismissProgressDialog();
        dismissVolumeDialog();
        onStateNormal();
        this.textureViewContainer.removeAllViews();
        JZMediaInterface.SAVED_SURFACE = null;
        ((AudioManager) getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO)).abandonAudioFocus(onAudioFocusChangeListener);
        JZUtils.scanForActivity(getContext()).getWindow().clearFlags(128);
        JZMediaInterface jZMediaInterface = this.mediaInterface;
        if (jZMediaInterface != null) {
            jZMediaInterface.release();
        }
    }

    public void resetProgressAndTime() {
        this.progressBar.setProgress(0);
        this.progressBar.setSecondaryProgress(0);
        this.currentTimeTextView.setText(JZUtils.stringForTime(0));
        this.totalTimeTextView.setText(JZUtils.stringForTime(0));
    }

    public void setBufferProgress(int i) {
        if (i != 0) {
            this.progressBar.setSecondaryProgress(i);
        }
    }

    public void setMediaInterface(Class cls) {
        reset();
        this.mediaInterfaceClass = cls;
    }

    public void setScreen(int i) {
        if (i == 0) {
            setScreenNormal();
        } else if (i == 1) {
            setScreenFullscreen();
        } else if (i == 2) {
            setScreenTiny();
        }
    }

    public void setScreenFullscreen() {
        this.screen = 1;
    }

    public void setScreenNormal() {
        this.screen = 0;
    }

    public void setScreenTiny() {
        this.screen = 2;
    }

    public void setState(int i) {
        setState(i, 0, 0);
    }

    public void setUp(String str, String str2) {
        setUp(new JZDataSource(str, str2), 0);
    }

    public void showBrightnessDialog(int i) {
    }

    public void showProgressDialog(float f, String str, long j, String str2, long j2) {
    }

    public void showVolumeDialog(float f, int i) {
    }

    public void showWifiDialog() {
    }

    public void startPreloading() {
        this.preloading = true;
        startVideo();
    }

    public void startProgressTimer() {
        Log.i("JZVD", "startProgressTimer:  [" + hashCode() + "] ");
        cancelProgressTimer();
        this.UPDATE_PROGRESS_TIMER = new Timer();
        this.mProgressTimerTask = new ProgressTimerTask();
        this.UPDATE_PROGRESS_TIMER.schedule(this.mProgressTimerTask, 0, 300);
    }

    public void startVideo() {
        Log.d("JZVD", "startVideo [" + hashCode() + "] ");
        setCurrentJzvd(this);
        try {
            this.mediaInterface = (JZMediaInterface) this.mediaInterfaceClass.getConstructor(Jzvd.class).newInstance(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InstantiationException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        }
        addTextureView();
        this.mAudioManager = (AudioManager) getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        this.mAudioManager.requestAudioFocus(onAudioFocusChangeListener, 3, 2);
        JZUtils.scanForActivity(getContext()).getWindow().addFlags(128);
        onStatePreparing();
    }

    public void startVideoAfterPreloading() {
        if (this.state == 3) {
            this.mediaInterface.start();
            return;
        }
        this.preloading = false;
        startVideo();
    }

    public static void startFullscreenDirectly(Context context, Class cls, JZDataSource jZDataSource) {
        JZUtils.hideStatusBar(context);
        JZUtils.setRequestedOrientation(context, FULLSCREEN_ORIENTATION);
        JZUtils.hideSystemUI(context);
        ViewGroup viewGroup = (ViewGroup) JZUtils.scanForActivity(context).getWindow().getDecorView();
        try {
            Jzvd jzvd = (Jzvd) cls.getConstructor(Context.class).newInstance(context);
            viewGroup.addView(jzvd, new FrameLayout.LayoutParams(-1, -1));
            jzvd.setUp(jZDataSource, 1);
            jzvd.startVideo();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void changeUrl(int i, long j) {
        this.state = 2;
        this.seekToInAdvance = j;
        this.jzDataSource.currentUrlIndex = i;
        this.mediaInterface.setSurface(null);
        this.mediaInterface.release();
        this.mediaInterface.prepare();
    }

    public void setState(int i, int i2, int i3) {
        if (i == 0) {
            onStateNormal();
        } else if (i == 1) {
            onStatePreparing();
        } else if (i == 2) {
            changeUrl(i2, (long) i3);
        } else if (i == 4) {
            onStatePlaying();
        } else if (i == 5) {
            onStatePause();
        } else if (i == 6) {
            onStateAutoComplete();
        } else if (i == 7) {
            onStateError();
        }
    }

    public void setUp(String str, String str2, int i) {
        setUp(new JZDataSource(str, str2), i);
    }

    public void setUp(JZDataSource jZDataSource, int i) {
        setUp(jZDataSource, i, JZMediaSystem.class);
    }

    public void setUp(String str, String str2, int i, Class cls) {
        setUp(new JZDataSource(str, str2), i, cls);
    }

    public void setUp(JZDataSource jZDataSource, int i, Class cls) {
        if (System.currentTimeMillis() - this.gobakFullscreenTime >= 200) {
            this.jzDataSource = jZDataSource;
            this.screen = i;
            onStateNormal();
            this.mediaInterfaceClass = cls;
        }
    }

    public void changeUrl(JZDataSource jZDataSource, long j) {
        this.state = 2;
        this.seekToInAdvance = j;
        this.jzDataSource = jZDataSource;
        this.mediaInterface.setSurface(null);
        this.mediaInterface.release();
        this.mediaInterface.prepare();
    }

    public Jzvd(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }
}
