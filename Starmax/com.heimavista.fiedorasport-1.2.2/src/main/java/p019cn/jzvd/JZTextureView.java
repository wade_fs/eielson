package p019cn.jzvd;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

/* renamed from: cn.jzvd.JZTextureView */
public class JZTextureView extends TextureView {
    protected static final String TAG = "JZResizeTextureView";
    public int currentVideoHeight;
    public int currentVideoWidth;

    public JZTextureView(Context context) {
        super(context);
        this.currentVideoWidth = 0;
        this.currentVideoHeight = 0;
        this.currentVideoWidth = 0;
        this.currentVideoHeight = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        Log.i(TAG, "onMeasure  [" + hashCode() + "] ");
        int rotation = (int) getRotation();
        int i9 = this.currentVideoWidth;
        int i10 = this.currentVideoHeight;
        int measuredHeight = ((View) getParent()).getMeasuredHeight();
        int measuredWidth = ((View) getParent()).getMeasuredWidth();
        if (!(measuredWidth == 0 || measuredHeight == 0 || i9 == 0 || i10 == 0 || Jzvd.VIDEO_IMAGE_DISPLAY_TYPE != 1)) {
            if (rotation == 90 || rotation == 270) {
                int i11 = measuredWidth;
                measuredWidth = measuredHeight;
                measuredHeight = i11;
            }
            i10 = (i9 * measuredHeight) / measuredWidth;
        }
        if (rotation == 90 || rotation == 270) {
            i3 = i;
            i4 = i2;
        } else {
            i4 = i;
            i3 = i2;
        }
        int defaultSize = TextureView.getDefaultSize(i9, i4);
        int defaultSize2 = TextureView.getDefaultSize(i10, i3);
        if (i9 <= 0 || i10 <= 0) {
            i5 = defaultSize2;
            i6 = defaultSize;
        } else {
            int mode = View.MeasureSpec.getMode(i4);
            i6 = View.MeasureSpec.getSize(i4);
            int mode2 = View.MeasureSpec.getMode(i3);
            i5 = View.MeasureSpec.getSize(i3);
            Log.i(TAG, "widthMeasureSpec  [" + View.MeasureSpec.toString(i4) + "]");
            Log.i(TAG, "heightMeasureSpec [" + View.MeasureSpec.toString(i3) + "]");
            if (mode == 1073741824 && mode2 == 1073741824) {
                int i12 = i9 * i5;
                int i13 = i6 * i10;
                if (i12 < i13) {
                    i7 = i12 / i10;
                } else if (i12 > i13) {
                    i5 = i13 / i9;
                }
            } else {
                if (mode == 1073741824) {
                    i8 = (i6 * i10) / i9;
                    if (mode2 == Integer.MIN_VALUE && i8 > i5) {
                        i7 = (i5 * i9) / i10;
                    }
                } else if (mode2 == 1073741824) {
                    i7 = (i5 * i9) / i10;
                    if (mode == Integer.MIN_VALUE && i7 > i6) {
                        i8 = (i6 * i10) / i9;
                    }
                } else {
                    if (mode2 != Integer.MIN_VALUE || i10 <= i5) {
                        i7 = i9;
                        i5 = i10;
                    } else {
                        i7 = (i5 * i9) / i10;
                    }
                    if (mode == Integer.MIN_VALUE && i7 > i6) {
                        i8 = (i6 * i10) / i9;
                    }
                }
                i5 = i8;
            }
            i6 = i7;
        }
        if (!(measuredWidth == 0 || measuredHeight == 0 || i9 == 0 || i10 == 0)) {
            int i14 = Jzvd.VIDEO_IMAGE_DISPLAY_TYPE;
            if (i14 != 3) {
                if (i14 == 2) {
                    if (rotation == 90 || rotation == 270) {
                        int i15 = measuredWidth;
                        measuredWidth = measuredHeight;
                        measuredHeight = i15;
                    }
                    double d = ((double) i10) / ((double) i9);
                    double d2 = (double) measuredHeight;
                    double d3 = (double) measuredWidth;
                    double d4 = d2 / d3;
                    if (d > d4) {
                        i10 = (int) ((d3 / ((double) i6)) * ((double) i5));
                        i9 = measuredWidth;
                    } else if (d < d4) {
                        i9 = (int) ((d2 / ((double) i5)) * ((double) i6));
                        i10 = measuredHeight;
                    }
                }
            }
            setMeasuredDimension(i9, i10);
        }
        i9 = i6;
        i10 = i5;
        setMeasuredDimension(i9, i10);
    }

    public void setRotation(float f) {
        if (f != getRotation()) {
            super.setRotation(f);
            requestLayout();
        }
    }

    public void setVideoSize(int i, int i2) {
        if (this.currentVideoWidth != i || this.currentVideoHeight != i2) {
            this.currentVideoWidth = i;
            this.currentVideoHeight = i2;
            requestLayout();
        }
    }

    public JZTextureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.currentVideoWidth = 0;
        this.currentVideoHeight = 0;
        this.currentVideoWidth = 0;
        this.currentVideoHeight = 0;
    }
}
