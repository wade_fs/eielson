package p019cn.jzvd;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/* renamed from: cn.jzvd.JzvdStd */
public class JzvdStd extends Jzvd {
    protected static Timer DISMISS_CONTROL_VIEW_TIMER = null;
    public static int LAST_GET_BATTERYLEVEL_PERCENT = 70;
    public static long LAST_GET_BATTERYLEVEL_TIME;
    public ImageView backButton;
    /* access modifiers changed from: private */
    public BroadcastReceiver battertReceiver = new BroadcastReceiver() {
        /* class p019cn.jzvd.JzvdStd.C08236 */

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                JzvdStd.LAST_GET_BATTERYLEVEL_PERCENT = (intent.getIntExtra("level", 0) * 100) / intent.getIntExtra("scale", 100);
                JzvdStd.this.setBatteryLevel();
                try {
                    JzvdStd.this.getContext().unregisterReceiver(JzvdStd.this.battertReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    public ImageView batteryLevel;
    public LinearLayout batteryTimeLayout;
    public ProgressBar bottomProgressBar;
    public TextView clarity;
    public PopupWindow clarityPopWindow;
    public ProgressBar loadingProgressBar;
    protected Dialog mBrightnessDialog;
    protected ProgressBar mDialogBrightnessProgressBar;
    protected TextView mDialogBrightnessTextView;
    protected ImageView mDialogIcon;
    protected ProgressBar mDialogProgressBar;
    protected TextView mDialogSeekTime;
    protected TextView mDialogTotalTime;
    protected ImageView mDialogVolumeImageView;
    protected ProgressBar mDialogVolumeProgressBar;
    protected TextView mDialogVolumeTextView;
    protected DismissControlViewTimerTask mDismissControlViewTimerTask;
    protected Dialog mProgressDialog;
    public TextView mRetryBtn;
    public LinearLayout mRetryLayout;
    protected Dialog mVolumeDialog;
    public TextView replayTextView;
    public ImageView thumbImageView;
    public ImageView tinyBackImageView;
    public TextView titleTextView;
    public TextView videoCurrentTime;

    /* renamed from: cn.jzvd.JzvdStd$DismissControlViewTimerTask */
    public class DismissControlViewTimerTask extends TimerTask {
        public DismissControlViewTimerTask() {
        }

        public void run() {
            JzvdStd.this.dissmissControlView();
        }
    }

    public JzvdStd(Context context) {
        super(context);
    }

    public void cancelDismissControlViewTimer() {
        Timer timer = DISMISS_CONTROL_VIEW_TIMER;
        if (timer != null) {
            timer.cancel();
        }
        DismissControlViewTimerTask dismissControlViewTimerTask = this.mDismissControlViewTimerTask;
        if (dismissControlViewTimerTask != null) {
            dismissControlViewTimerTask.cancel();
        }
    }

    public void changeStartButtonSize(int i) {
        ImageView imageView = super.startButton;
        if (imageView != null) {
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            layoutParams.height = i;
            layoutParams.width = i;
        }
        ProgressBar progressBar = this.loadingProgressBar;
        if (progressBar != null) {
            ViewGroup.LayoutParams layoutParams2 = progressBar.getLayoutParams();
            layoutParams2.height = i;
            layoutParams2.width = i;
        }
    }

    public void changeUiToComplete() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(0, 4, 0, 4, 0, 4, 4);
            updateStartImage();
        } else if (i == 1) {
            setAllControlsVisiblity(0, 4, 0, 4, 0, 4, 4);
            updateStartImage();
        }
    }

    public void changeUiToError() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(4, 4, 0, 4, 4, 4, 0);
            updateStartImage();
        } else if (i == 1) {
            setAllControlsVisiblity(0, 4, 0, 4, 4, 4, 0);
            updateStartImage();
        }
    }

    public void changeUiToNormal() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(0, 4, 0, 4, 0, 4, 4);
            updateStartImage();
        } else if (i == 1) {
            setAllControlsVisiblity(0, 4, 0, 4, 0, 4, 4);
            updateStartImage();
        }
    }

    public void changeUiToPauseClear() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(4, 4, 4, 4, 4, 0, 4);
        } else if (i == 1) {
            setAllControlsVisiblity(4, 4, 4, 4, 4, 0, 4);
        }
    }

    public void changeUiToPauseShow() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(0, 0, 0, 4, 4, 4, 4);
            updateStartImage();
        } else if (i == 1) {
            setAllControlsVisiblity(0, 0, 0, 4, 4, 4, 4);
            updateStartImage();
        }
    }

    public void changeUiToPlayingClear() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(4, 4, 4, 4, 4, 0, 4);
        } else if (i == 1) {
            setAllControlsVisiblity(4, 4, 4, 4, 4, 0, 4);
        }
    }

    public void changeUiToPlayingShow() {
        int i = super.screen;
        if (i == 0) {
            setAllControlsVisiblity(0, 0, 0, 4, 4, 4, 4);
            updateStartImage();
        } else if (i == 1) {
            setAllControlsVisiblity(0, 0, 0, 4, 4, 4, 4);
            updateStartImage();
        }
    }

    public void changeUiToPreparing() {
        int i = super.screen;
        if (i == 0 || i == 1) {
            setAllControlsVisiblity(4, 4, 4, 0, 0, 4, 4);
            updateStartImage();
        }
    }

    public void changeUrl(int i, long j) {
        super.changeUrl(i, j);
        ImageView imageView = super.startButton;
        if (imageView != null) {
            imageView.setVisibility(4);
        }
        this.replayTextView.setVisibility(8);
        this.mRetryLayout.setVisibility(8);
    }

    public Dialog createDialogWithView(View view) {
        Dialog dialog = new Dialog(getContext(), C0824R.C0829style.jz_style_dialog_progress);
        dialog.setContentView(view);
        Window window = dialog.getWindow();
        window.addFlags(8);
        window.addFlags(32);
        window.addFlags(16);
        window.setLayout(-2, -2);
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        window.setAttributes(attributes);
        return dialog;
    }

    public void dismissBrightnessDialog() {
        super.dismissBrightnessDialog();
        Dialog dialog = this.mBrightnessDialog;
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void dismissProgressDialog() {
        super.dismissProgressDialog();
        Dialog dialog = this.mProgressDialog;
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void dismissVolumeDialog() {
        super.dismissVolumeDialog();
        Dialog dialog = this.mVolumeDialog;
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void dissmissControlView() {
        int i = super.state;
        if (i != 0 && i != 7 && i != 6) {
            post(new Runnable() {
                /* class p019cn.jzvd.JzvdStd.C08225 */

                public void run() {
                    JzvdStd.this.bottomContainer.setVisibility(4);
                    JzvdStd.this.topContainer.setVisibility(4);
                    ImageView imageView = JzvdStd.this.startButton;
                    if (imageView != null) {
                        imageView.setVisibility(4);
                    }
                    PopupWindow popupWindow = JzvdStd.this.clarityPopWindow;
                    if (popupWindow != null) {
                        popupWindow.dismiss();
                    }
                    JzvdStd jzvdStd = JzvdStd.this;
                    ProgressBar progressBar = jzvdStd.bottomProgressBar;
                    if (progressBar != null && jzvdStd.screen != 2) {
                        progressBar.setVisibility(0);
                    }
                }
            });
        }
    }

    public int getLayoutId() {
        return C0824R.C0828layout.jz_layout_std;
    }

    public void init(Context context) {
        super.init(context);
        this.batteryTimeLayout = (LinearLayout) findViewById(C0824R.C0827id.battery_time_layout);
        this.bottomProgressBar = (ProgressBar) findViewById(C0824R.C0827id.bottom_progress);
        this.titleTextView = (TextView) findViewById(C0824R.C0827id.title);
        this.backButton = (ImageView) findViewById(C0824R.C0827id.back);
        this.thumbImageView = (ImageView) findViewById(C0824R.C0827id.thumb);
        this.loadingProgressBar = (ProgressBar) findViewById(C0824R.C0827id.loading);
        this.tinyBackImageView = (ImageView) findViewById(C0824R.C0827id.back_tiny);
        this.batteryLevel = (ImageView) findViewById(C0824R.C0827id.battery_level);
        this.videoCurrentTime = (TextView) findViewById(C0824R.C0827id.video_current_time);
        this.replayTextView = (TextView) findViewById(C0824R.C0827id.replay_text);
        this.clarity = (TextView) findViewById(C0824R.C0827id.clarity);
        this.mRetryBtn = (TextView) findViewById(C0824R.C0827id.retry_btn);
        this.mRetryLayout = (LinearLayout) findViewById(C0824R.C0827id.retry_layout);
        this.thumbImageView.setOnClickListener(this);
        this.backButton.setOnClickListener(this);
        this.tinyBackImageView.setOnClickListener(this);
        this.clarity.setOnClickListener(this);
        this.mRetryBtn.setOnClickListener(this);
    }

    public void onAutoCompletion() {
        super.onAutoCompletion();
        cancelDismissControlViewTimer();
    }

    public void onCLickUiToggleToClear() {
        int i = super.state;
        if (i == 1) {
            if (super.bottomContainer.getVisibility() == 0) {
                changeUiToPreparing();
            }
        } else if (i == 4) {
            if (super.bottomContainer.getVisibility() == 0) {
                changeUiToPlayingClear();
            }
        } else if (i == 5) {
            if (super.bottomContainer.getVisibility() == 0) {
                changeUiToPauseClear();
            }
        } else if (i == 6 && super.bottomContainer.getVisibility() == 0) {
            changeUiToComplete();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.widget.LinearLayout, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == C0824R.C0827id.thumb) {
            JZDataSource jZDataSource = super.jzDataSource;
            if (jZDataSource == null || jZDataSource.urlsMap.isEmpty() || super.jzDataSource.getCurrentUrl() == null) {
                Toast.makeText(getContext(), getResources().getString(C0824R.string.no_url), 0).show();
                return;
            }
            int i = super.state;
            if (i == 0) {
                if (super.jzDataSource.getCurrentUrl().toString().startsWith("file") || super.jzDataSource.getCurrentUrl().toString().startsWith("/") || JZUtils.isWifiConnected(getContext()) || Jzvd.WIFI_TIP_DIALOG_SHOWED) {
                    startVideo();
                } else {
                    showWifiDialog();
                }
            } else if (i == 6) {
                onClickUiToggle();
            }
        } else if (id == C0824R.C0827id.surface_container) {
            startDismissControlViewTimer();
        } else if (id == C0824R.C0827id.back) {
            Jzvd.backPress();
        } else if (id == C0824R.C0827id.back_tiny) {
            clearFloatScreen();
        } else if (id == C0824R.C0827id.clarity) {
            final LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(C0824R.C0828layout.jz_layout_clarity, (ViewGroup) null);
            C08181 r0 = new View.OnClickListener() {
                /* class p019cn.jzvd.JzvdStd.C08181 */

                public void onClick(View view) {
                    int intValue = ((Integer) view.getTag()).intValue();
                    JzvdStd jzvdStd = JzvdStd.this;
                    jzvdStd.changeUrl(intValue, jzvdStd.getCurrentPositionWhenPlaying());
                    JzvdStd jzvdStd2 = JzvdStd.this;
                    jzvdStd2.clarity.setText(jzvdStd2.jzDataSource.getCurrentKey().toString());
                    for (int i = 0; i < linearLayout.getChildCount(); i++) {
                        if (i == JzvdStd.this.jzDataSource.currentUrlIndex) {
                            ((TextView) linearLayout.getChildAt(i)).setTextColor(Color.parseColor("#fff85959"));
                        } else {
                            ((TextView) linearLayout.getChildAt(i)).setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    PopupWindow popupWindow = JzvdStd.this.clarityPopWindow;
                    if (popupWindow != null) {
                        popupWindow.dismiss();
                    }
                }
            };
            for (int i2 = 0; i2 < super.jzDataSource.urlsMap.size(); i2++) {
                String keyFromDataSource = super.jzDataSource.getKeyFromDataSource(i2);
                TextView textView = (TextView) View.inflate(getContext(), C0824R.C0828layout.jz_layout_clarity_item, null);
                textView.setText(keyFromDataSource);
                textView.setTag(Integer.valueOf(i2));
                linearLayout.addView(textView, i2);
                textView.setOnClickListener(r0);
                if (i2 == super.jzDataSource.currentUrlIndex) {
                    textView.setTextColor(Color.parseColor("#fff85959"));
                }
            }
            this.clarityPopWindow = new PopupWindow((View) linearLayout, -2, -2, true);
            this.clarityPopWindow.setContentView(linearLayout);
            this.clarityPopWindow.showAsDropDown(this.clarity);
            linearLayout.measure(0, 0);
            this.clarityPopWindow.update(this.clarity, -(this.clarity.getMeasuredWidth() / 3), -(this.clarity.getMeasuredHeight() / 3), Math.round((float) (linearLayout.getMeasuredWidth() * 2)), linearLayout.getMeasuredHeight());
        } else if (id != C0824R.C0827id.retry_btn) {
        } else {
            if (super.jzDataSource.urlsMap.isEmpty() || super.jzDataSource.getCurrentUrl() == null) {
                Toast.makeText(getContext(), getResources().getString(C0824R.string.no_url), 0).show();
            } else if (super.jzDataSource.getCurrentUrl().toString().startsWith("file") || super.jzDataSource.getCurrentUrl().toString().startsWith("/") || JZUtils.isWifiConnected(getContext()) || Jzvd.WIFI_TIP_DIALOG_SHOWED) {
                addTextureView();
                onStatePreparing();
            } else {
                showWifiDialog();
            }
        }
    }

    public void onClickUiToggle() {
        if (super.bottomContainer.getVisibility() != 0) {
            setSystemTimeAndBattery();
            this.clarity.setText(super.jzDataSource.getCurrentKey().toString());
        }
        int i = super.state;
        if (i == 1) {
            changeUiToPreparing();
            if (super.bottomContainer.getVisibility() != 0) {
                setSystemTimeAndBattery();
            }
        } else if (i == 4) {
            if (super.bottomContainer.getVisibility() == 0) {
                changeUiToPlayingClear();
            } else {
                changeUiToPlayingShow();
            }
        } else if (i != 5) {
        } else {
            if (super.bottomContainer.getVisibility() == 0) {
                changeUiToPauseClear();
            } else {
                changeUiToPauseShow();
            }
        }
    }

    public void onProgress(int i, long j, long j2) {
        super.onProgress(i, j, j2);
        ProgressBar progressBar = this.bottomProgressBar;
        if (progressBar != null && i != 0) {
            progressBar.setProgress(i);
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        super.onStartTrackingTouch(seekBar);
        cancelDismissControlViewTimer();
    }

    public void onStateAutoComplete() {
        super.onStateAutoComplete();
        changeUiToComplete();
        cancelDismissControlViewTimer();
        ProgressBar progressBar = this.bottomProgressBar;
        if (progressBar != null) {
            progressBar.setProgress(100);
        }
    }

    public void onStateError() {
        super.onStateError();
        changeUiToError();
    }

    public void onStateNormal() {
        super.onStateNormal();
        changeUiToNormal();
    }

    public void onStatePause() {
        super.onStatePause();
        changeUiToPauseShow();
        cancelDismissControlViewTimer();
    }

    public void onStatePlaying() {
        super.onStatePlaying();
        changeUiToPlayingClear();
    }

    public void onStatePreparing() {
        super.onStatePreparing();
        changeUiToPreparing();
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        super.onStopTrackingTouch(seekBar);
        startDismissControlViewTimer();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id = view.getId();
        if (id == C0824R.C0827id.surface_container) {
            int action = motionEvent.getAction();
            if (action != 0) {
                if (action == 1) {
                    startDismissControlViewTimer();
                    if (this.bottomProgressBar != null && super.mChangePosition) {
                        long duration = getDuration();
                        long j = super.mSeekTimePosition * 100;
                        if (duration == 0) {
                            duration = 1;
                        }
                        this.bottomProgressBar.setProgress((int) (j / duration));
                    }
                    if (!super.mChangePosition && !super.mChangeVolume) {
                        onClickUiToggle();
                    }
                }
            }
        } else if (id == C0824R.C0827id.bottom_seek_progress) {
            int action2 = motionEvent.getAction();
            if (action2 == 0) {
                cancelDismissControlViewTimer();
            } else if (action2 == 1) {
                startDismissControlViewTimer();
            }
        }
        return super.onTouch(view, motionEvent);
    }

    public void reset() {
        super.reset();
        cancelDismissControlViewTimer();
        PopupWindow popupWindow = this.clarityPopWindow;
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    public void resetProgressAndTime() {
        super.resetProgressAndTime();
        ProgressBar progressBar = this.bottomProgressBar;
        if (progressBar != null) {
            progressBar.setProgress(0);
            this.bottomProgressBar.setSecondaryProgress(0);
        }
    }

    public void setAllControlsVisiblity(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        super.topContainer.setVisibility(i);
        super.bottomContainer.setVisibility(i2);
        ImageView imageView = super.startButton;
        if (imageView != null) {
            imageView.setVisibility(i3);
        }
        ProgressBar progressBar = this.loadingProgressBar;
        if (progressBar != null) {
            progressBar.setVisibility(i4);
        }
        this.thumbImageView.setVisibility(i5);
        ProgressBar progressBar2 = this.bottomProgressBar;
        if (progressBar2 != null) {
            progressBar2.setVisibility(i6);
        }
        this.mRetryLayout.setVisibility(i7);
    }

    public void setBatteryLevel() {
        int i = LAST_GET_BATTERYLEVEL_PERCENT;
        if (i < 15) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_10);
        } else if (i >= 15 && i < 40) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_30);
        } else if (i >= 40 && i < 60) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_50);
        } else if (i >= 60 && i < 80) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_70);
        } else if (i >= 80 && i < 95) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_90);
        } else if (i >= 95 && i <= 100) {
            this.batteryLevel.setBackgroundResource(C0824R.C0826drawable.jz_battery_level_100);
        }
    }

    public void setBufferProgress(int i) {
        super.setBufferProgress(i);
        ProgressBar progressBar = this.bottomProgressBar;
        if (progressBar != null && i != 0) {
            progressBar.setSecondaryProgress(i);
        }
    }

    public void setScreenFullscreen() {
        super.setScreenFullscreen();
        super.fullscreenButton.setImageResource(C0824R.C0826drawable.jz_shrink);
        this.backButton.setVisibility(0);
        this.tinyBackImageView.setVisibility(4);
        this.batteryTimeLayout.setVisibility(0);
        if (super.jzDataSource.urlsMap.size() == 1) {
            this.clarity.setVisibility(8);
        } else {
            this.clarity.setText(super.jzDataSource.getCurrentKey().toString());
            this.clarity.setVisibility(0);
        }
        changeStartButtonSize((int) getResources().getDimension(C0824R.dimen.jz_start_button_w_h_fullscreen));
        setSystemTimeAndBattery();
    }

    public void setScreenNormal() {
        super.setScreenNormal();
        super.fullscreenButton.setImageResource(C0824R.C0826drawable.jz_enlarge);
        this.backButton.setVisibility(8);
        this.tinyBackImageView.setVisibility(4);
        changeStartButtonSize((int) getResources().getDimension(C0824R.dimen.jz_start_button_w_h_normal));
        this.batteryTimeLayout.setVisibility(8);
        this.clarity.setVisibility(8);
    }

    public void setScreenTiny() {
        super.setScreenTiny();
        this.tinyBackImageView.setVisibility(0);
        setAllControlsVisiblity(4, 4, 4, 4, 4, 4, 4);
        this.batteryTimeLayout.setVisibility(8);
        this.clarity.setVisibility(8);
    }

    public void setSystemTimeAndBattery() {
        this.videoCurrentTime.setText(new SimpleDateFormat("HH:mm").format(new Date()));
        if (System.currentTimeMillis() - LAST_GET_BATTERYLEVEL_TIME > 30000) {
            LAST_GET_BATTERYLEVEL_TIME = System.currentTimeMillis();
            getContext().registerReceiver(this.battertReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            return;
        }
        setBatteryLevel();
    }

    public void setUp(JZDataSource jZDataSource, int i, Class cls) {
        super.setUp(jZDataSource, i, cls);
        this.titleTextView.setText(jZDataSource.title);
        setScreen(i);
    }

    public void showBrightnessDialog(int i) {
        super.showBrightnessDialog(i);
        if (this.mBrightnessDialog == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(C0824R.C0828layout.jz_dialog_brightness, (ViewGroup) null);
            this.mDialogBrightnessTextView = (TextView) inflate.findViewById(C0824R.C0827id.tv_brightness);
            this.mDialogBrightnessProgressBar = (ProgressBar) inflate.findViewById(C0824R.C0827id.brightness_progressbar);
            this.mBrightnessDialog = createDialogWithView(inflate);
        }
        if (!this.mBrightnessDialog.isShowing()) {
            this.mBrightnessDialog.show();
        }
        if (i > 100) {
            i = 100;
        } else if (i < 0) {
            i = 0;
        }
        TextView textView = this.mDialogBrightnessTextView;
        textView.setText(i + "%");
        this.mDialogBrightnessProgressBar.setProgress(i);
        onCLickUiToggleToClear();
    }

    public void showProgressDialog(float f, String str, long j, String str2, long j2) {
        super.showProgressDialog(f, str, j, str2, j2);
        if (this.mProgressDialog == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(C0824R.C0828layout.jz_dialog_progress, (ViewGroup) null);
            this.mDialogProgressBar = (ProgressBar) inflate.findViewById(C0824R.C0827id.duration_progressbar);
            this.mDialogSeekTime = (TextView) inflate.findViewById(C0824R.C0827id.tv_current);
            this.mDialogTotalTime = (TextView) inflate.findViewById(C0824R.C0827id.tv_duration);
            this.mDialogIcon = (ImageView) inflate.findViewById(C0824R.C0827id.duration_image_tip);
            this.mProgressDialog = createDialogWithView(inflate);
        }
        if (!this.mProgressDialog.isShowing()) {
            this.mProgressDialog.show();
        }
        this.mDialogSeekTime.setText(str);
        TextView textView = this.mDialogTotalTime;
        textView.setText(" / " + str2);
        this.mDialogProgressBar.setProgress(j2 <= 0 ? 0 : (int) ((j * 100) / j2));
        if (f > 0.0f) {
            this.mDialogIcon.setBackgroundResource(C0824R.C0826drawable.jz_forward_icon);
        } else {
            this.mDialogIcon.setBackgroundResource(C0824R.C0826drawable.jz_backward_icon);
        }
        onCLickUiToggleToClear();
    }

    public void showVolumeDialog(float f, int i) {
        super.showVolumeDialog(f, i);
        if (this.mVolumeDialog == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(C0824R.C0828layout.jz_dialog_volume, (ViewGroup) null);
            this.mDialogVolumeImageView = (ImageView) inflate.findViewById(C0824R.C0827id.volume_image_tip);
            this.mDialogVolumeTextView = (TextView) inflate.findViewById(C0824R.C0827id.tv_volume);
            this.mDialogVolumeProgressBar = (ProgressBar) inflate.findViewById(C0824R.C0827id.volume_progressbar);
            this.mVolumeDialog = createDialogWithView(inflate);
        }
        if (!this.mVolumeDialog.isShowing()) {
            this.mVolumeDialog.show();
        }
        if (i <= 0) {
            this.mDialogVolumeImageView.setBackgroundResource(C0824R.C0826drawable.jz_close_volume);
        } else {
            this.mDialogVolumeImageView.setBackgroundResource(C0824R.C0826drawable.jz_add_volume);
        }
        if (i > 100) {
            i = 100;
        } else if (i < 0) {
            i = 0;
        }
        TextView textView = this.mDialogVolumeTextView;
        textView.setText(i + "%");
        this.mDialogVolumeProgressBar.setProgress(i);
        onCLickUiToggleToClear();
    }

    public void showWifiDialog() {
        super.showWifiDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(C0824R.string.tips_not_wifi));
        builder.setPositiveButton(getResources().getString(C0824R.string.tips_not_wifi_confirm), new DialogInterface.OnClickListener() {
            /* class p019cn.jzvd.JzvdStd.C08192 */

            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                JzvdStd.this.startVideo();
                Jzvd.WIFI_TIP_DIALOG_SHOWED = true;
            }
        });
        builder.setNegativeButton(getResources().getString(C0824R.string.tips_not_wifi_cancel), new DialogInterface.OnClickListener() {
            /* class p019cn.jzvd.JzvdStd.C08203 */

            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                JzvdStd.this.clearFloatScreen();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            /* class p019cn.jzvd.JzvdStd.C08214 */

            public void onCancel(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void startDismissControlViewTimer() {
        cancelDismissControlViewTimer();
        DISMISS_CONTROL_VIEW_TIMER = new Timer();
        this.mDismissControlViewTimerTask = new DismissControlViewTimerTask();
        DISMISS_CONTROL_VIEW_TIMER.schedule(this.mDismissControlViewTimerTask, 2500);
    }

    public void updateStartImage() {
        int i = super.state;
        if (i == 4) {
            ImageView imageView = super.startButton;
            if (imageView != null) {
                imageView.setVisibility(0);
                super.startButton.setImageResource(C0824R.C0826drawable.jz_click_pause_selector);
            }
            this.replayTextView.setVisibility(8);
        } else if (i == 7) {
            ImageView imageView2 = super.startButton;
            if (imageView2 != null) {
                imageView2.setVisibility(4);
            }
            this.replayTextView.setVisibility(8);
        } else if (i == 6) {
            ImageView imageView3 = super.startButton;
            if (imageView3 != null) {
                imageView3.setVisibility(0);
                super.startButton.setImageResource(C0824R.C0826drawable.jz_click_replay_selector);
            }
            this.replayTextView.setVisibility(0);
        } else {
            ImageView imageView4 = super.startButton;
            if (imageView4 != null) {
                imageView4.setImageResource(C0824R.C0826drawable.jz_click_play_selector);
            }
            this.replayTextView.setVisibility(8);
        }
    }

    public JzvdStd(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void changeUrl(JZDataSource jZDataSource, long j) {
        super.changeUrl(jZDataSource, j);
        this.titleTextView.setText(jZDataSource.title);
        ImageView imageView = super.startButton;
        if (imageView != null) {
            imageView.setVisibility(4);
        }
        this.replayTextView.setVisibility(8);
        this.mRetryLayout.setVisibility(8);
    }
}
