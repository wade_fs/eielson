package p019cn.jzvd;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Surface;
import androidx.annotation.RequiresApi;
import java.util.Map;

/* renamed from: cn.jzvd.JZMediaSystem */
public class JZMediaSystem extends JZMediaInterface implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnVideoSizeChangedListener {
    public MediaPlayer mediaPlayer;

    public JZMediaSystem(Jzvd jzvd) {
        super(jzvd);
    }

    public long getCurrentPosition() {
        MediaPlayer mediaPlayer2 = this.mediaPlayer;
        if (mediaPlayer2 != null) {
            return (long) mediaPlayer2.getCurrentPosition();
        }
        return 0;
    }

    public long getDuration() {
        MediaPlayer mediaPlayer2 = this.mediaPlayer;
        if (mediaPlayer2 != null) {
            return (long) mediaPlayer2.getDuration();
        }
        return 0;
    }

    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer2, final int i) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08159 */

            public void run() {
                JZMediaSystem.this.jzvd.setBufferProgress(i);
            }
        });
    }

    public void onCompletion(MediaPlayer mediaPlayer2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08148 */

            public void run() {
                JZMediaSystem.this.jzvd.onAutoCompletion();
            }
        });
    }

    public boolean onError(MediaPlayer mediaPlayer2, final int i, final int i2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C080511 */

            public void run() {
                JZMediaSystem.this.jzvd.onError(i, i2);
            }
        });
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer2, final int i, final int i2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C080612 */

            public void run() {
                JZMediaSystem.this.jzvd.onInfo(i, i2);
            }
        });
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08137 */

            public void run() {
                JZMediaSystem.this.jzvd.onPrepared();
            }
        });
    }

    public void onSeekComplete(MediaPlayer mediaPlayer2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C080410 */

            public void run() {
                JZMediaSystem.this.jzvd.onSeekComplete();
            }
        });
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        SurfaceTexture surfaceTexture2 = JZMediaInterface.SAVED_SURFACE;
        if (surfaceTexture2 == null) {
            JZMediaInterface.SAVED_SURFACE = surfaceTexture;
            prepare();
            return;
        }
        super.jzvd.textureView.setSurfaceTexture(surfaceTexture2);
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer2, final int i, final int i2) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C080713 */

            public void run() {
                JZMediaSystem.this.jzvd.onVideoSizeChanged(i, i2);
            }
        });
    }

    public void pause() {
        super.mMediaHandler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08093 */

            public void run() {
                JZMediaSystem.this.mediaPlayer.pause();
            }
        });
    }

    public void prepare() {
        release();
        super.mMediaHandlerThread = new HandlerThread("JZVD");
        super.mMediaHandlerThread.start();
        super.mMediaHandler = new Handler(super.mMediaHandlerThread.getLooper());
        super.handler = new Handler();
        super.mMediaHandler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08031 */

            public void run() {
                try {
                    JZMediaSystem.this.mediaPlayer = new MediaPlayer();
                    JZMediaSystem.this.mediaPlayer.setAudioStreamType(3);
                    JZMediaSystem.this.mediaPlayer.setLooping(JZMediaSystem.this.jzvd.jzDataSource.looping);
                    JZMediaSystem.this.mediaPlayer.setOnPreparedListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setOnCompletionListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setOnBufferingUpdateListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setScreenOnWhilePlaying(true);
                    JZMediaSystem.this.mediaPlayer.setOnSeekCompleteListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setOnErrorListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setOnInfoListener(JZMediaSystem.this);
                    JZMediaSystem.this.mediaPlayer.setOnVideoSizeChangedListener(JZMediaSystem.this);
                    MediaPlayer.class.getDeclaredMethod("setDataSource", String.class, Map.class).invoke(JZMediaSystem.this.mediaPlayer, JZMediaSystem.this.jzvd.jzDataSource.getCurrentUrl().toString(), JZMediaSystem.this.jzvd.jzDataSource.headerMap);
                    JZMediaSystem.this.mediaPlayer.prepareAsync();
                    JZMediaSystem.this.mediaPlayer.setSurface(new Surface(JZMediaInterface.SAVED_SURFACE));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r4.mMediaHandlerThread;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0008, code lost:
        r2 = r4.mediaPlayer;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void release() {
        /*
            r4 = this;
            android.os.Handler r0 = r4.mMediaHandler
            if (r0 == 0) goto L_0x0017
            android.os.HandlerThread r1 = r4.mMediaHandlerThread
            if (r1 == 0) goto L_0x0017
            android.media.MediaPlayer r2 = r4.mediaPlayer
            if (r2 == 0) goto L_0x0017
            cn.jzvd.JZMediaSystem$5 r3 = new cn.jzvd.JZMediaSystem$5
            r3.<init>(r2, r1)
            r0.post(r3)
            r0 = 0
            r4.mediaPlayer = r0
        L_0x0017:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p019cn.jzvd.JZMediaSystem.release():void");
    }

    public void seekTo(final long j) {
        super.mMediaHandler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08104 */

            public void run() {
                try {
                    JZMediaSystem.this.mediaPlayer.seekTo((int) j);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = 23)
    public void setSpeed(float f) {
        PlaybackParams playbackParams = this.mediaPlayer.getPlaybackParams();
        playbackParams.setSpeed(f);
        this.mediaPlayer.setPlaybackParams(playbackParams);
    }

    public void setSurface(Surface surface) {
        this.mediaPlayer.setSurface(surface);
    }

    public void setVolume(final float f, final float f2) {
        Handler handler = super.mMediaHandler;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class p019cn.jzvd.JZMediaSystem.C08126 */

                public void run() {
                    MediaPlayer mediaPlayer = JZMediaSystem.this.mediaPlayer;
                    if (mediaPlayer != null) {
                        mediaPlayer.setVolume(f, f2);
                    }
                }
            });
        }
    }

    public void start() {
        super.mMediaHandler.post(new Runnable() {
            /* class p019cn.jzvd.JZMediaSystem.C08082 */

            public void run() {
                JZMediaSystem.this.mediaPlayer.start();
                JZMediaSystem jZMediaSystem = JZMediaSystem.this;
                if (jZMediaSystem.jzvd.jzDataSource.mute) {
                    jZMediaSystem.mediaPlayer.setVolume(0.0f, 0.0f);
                }
            }
        });
    }
}
