package p019cn.jzvd.media;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.C1949h;
import com.google.android.exoplayer2.video.VideoListener;
import p019cn.jzvd.JZMediaInterface;
import p019cn.jzvd.Jzvd;

/* renamed from: cn.jzvd.media.JZMediaExo */
public class JZMediaExo extends JZMediaInterface implements Player.EventListener, VideoListener {
    /* access modifiers changed from: private */
    public String TAG = "JZMediaExo";
    /* access modifiers changed from: private */
    public Runnable callback;
    private long previousSeek = 0;
    /* access modifiers changed from: private */
    public SimpleExoPlayer simpleExoPlayer;

    /* renamed from: cn.jzvd.media.JZMediaExo$onBufferingUpdate */
    private class onBufferingUpdate implements Runnable {
        private onBufferingUpdate() {
        }

        public void run() {
            if (JZMediaExo.this.simpleExoPlayer != null) {
                final int bufferedPercentage = JZMediaExo.this.simpleExoPlayer.getBufferedPercentage();
                JZMediaExo.this.handler.post(new Runnable() {
                    /* class p019cn.jzvd.media.JZMediaExo.onBufferingUpdate.C08371 */

                    public void run() {
                        JZMediaExo.this.jzvd.setBufferProgress(bufferedPercentage);
                    }
                });
                if (bufferedPercentage < 100) {
                    JZMediaExo jZMediaExo = JZMediaExo.this;
                    jZMediaExo.handler.postDelayed(jZMediaExo.callback, 300);
                    return;
                }
                JZMediaExo jZMediaExo2 = JZMediaExo.this;
                jZMediaExo2.handler.removeCallbacks(jZMediaExo2.callback);
            }
        }
    }

    public JZMediaExo(Jzvd jzvd) {
        super(jzvd);
    }

    /* access modifiers changed from: private */
    public String getAppName(Context context) {
        try {
            return context.getResources().getString(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.labelRes);
        } catch (Exception e) {
            e.printStackTrace();
            return "hv";
        }
    }

    public long getCurrentPosition() {
        SimpleExoPlayer simpleExoPlayer2 = this.simpleExoPlayer;
        if (simpleExoPlayer2 != null) {
            return simpleExoPlayer2.getCurrentPosition();
        }
        return 0;
    }

    public long getDuration() {
        SimpleExoPlayer simpleExoPlayer2 = this.simpleExoPlayer;
        if (simpleExoPlayer2 != null) {
            return simpleExoPlayer2.getDuration();
        }
        return 0;
    }

    public boolean isPlaying() {
        return this.simpleExoPlayer.getPlayWhenReady();
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        String str = this.TAG;
        Log.e(str, "onPlayerError" + exoPlaybackException.toString());
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.media.JZMediaExo.C08356 */

            public void run() {
                JZMediaExo.this.jzvd.onError(1000, 1000);
            }
        });
    }

    public void onPlayerStateChanged(final boolean z, final int i) {
        String str = this.TAG;
        Log.e(str, "onPlayerStateChanged" + i + "/ready=" + String.valueOf(z));
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.media.JZMediaExo.C08345 */

            public void run() {
                int i = i;
                if (i == 1) {
                    return;
                }
                if (i == 2) {
                    JZMediaExo jZMediaExo = JZMediaExo.this;
                    jZMediaExo.handler.post(jZMediaExo.callback);
                } else if (i == 3) {
                    JZMediaExo.this.jzvd.onPrepared();
                    if (z) {
                        JZMediaExo.this.jzvd.onStatePlaying();
                    }
                } else if (i == 4) {
                    JZMediaExo.this.jzvd.onAutoCompletion();
                }
            }
        });
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onRenderedFirstFrame() {
        Log.e(this.TAG, "onRenderedFirstFrame");
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onSeekProcessed() {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.media.JZMediaExo.C08367 */

            public void run() {
                JZMediaExo.this.jzvd.onSeekComplete();
            }
        });
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
        C1949h.$default$onSurfaceSizeChanged(this, i, i2);
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        SurfaceTexture surfaceTexture2 = JZMediaInterface.SAVED_SURFACE;
        if (surfaceTexture2 == null) {
            JZMediaInterface.SAVED_SURFACE = surfaceTexture;
            prepare();
            return;
        }
        super.jzvd.textureView.setSurfaceTexture(surfaceTexture2);
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj, int i) {
        Log.e(this.TAG, "onTimelineChanged");
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public void onVideoSizeChanged(final int i, final int i2, int i3, float f) {
        super.handler.post(new Runnable() {
            /* class p019cn.jzvd.media.JZMediaExo.C08312 */

            public void run() {
                JZMediaExo.this.jzvd.onVideoSizeChanged(i, i2);
            }
        });
    }

    public void pause() {
        this.simpleExoPlayer.setPlayWhenReady(false);
    }

    public void prepare() {
        Log.e(this.TAG, "prepare");
        final Context context = super.jzvd.getContext();
        release();
        super.mMediaHandlerThread = new HandlerThread("JZVD");
        super.mMediaHandlerThread.start();
        super.mMediaHandler = new Handler(Looper.getMainLooper());
        super.handler = new Handler();
        super.mMediaHandler.post(new Runnable() {
            /* class p019cn.jzvd.media.JZMediaExo.C08301 */

            public void run() {
                MediaSource mediaSource;
                DefaultTrackSelector defaultTrackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter()));
                DefaultLoadControl defaultLoadControl = new DefaultLoadControl(new DefaultAllocator(true, 65536), 360000, 600000, 1000, 5000, -1, false);
                SimpleExoPlayer unused = JZMediaExo.this.simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context, new DefaultRenderersFactory(context), defaultTrackSelector, defaultLoadControl);
                Context context = context;
                DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, JZMediaExo.this.getAppName(context)));
                String obj = JZMediaExo.this.jzvd.jzDataSource.getCurrentUrl().toString();
                if (obj.contains(".m3u8")) {
                    mediaSource = new HlsMediaSource.Factory(defaultDataSourceFactory).createMediaSource(Uri.parse(obj), JZMediaExo.this.handler, null);
                } else {
                    mediaSource = new ExtractorMediaSource.Factory(defaultDataSourceFactory).createMediaSource(Uri.parse(obj));
                }
                JZMediaExo.this.simpleExoPlayer.addVideoListener(JZMediaExo.this);
                String access$200 = JZMediaExo.this.TAG;
                Log.e(access$200, "URL Link = " + obj);
                JZMediaExo.this.simpleExoPlayer.addListener(JZMediaExo.this);
                JZMediaExo jZMediaExo = JZMediaExo.this;
                if (jZMediaExo.jzvd.jzDataSource.looping) {
                    jZMediaExo.simpleExoPlayer.setRepeatMode(1);
                } else {
                    jZMediaExo.simpleExoPlayer.setRepeatMode(0);
                }
                JZMediaExo jZMediaExo2 = JZMediaExo.this;
                if (jZMediaExo2.jzvd.jzDataSource.mute) {
                    jZMediaExo2.simpleExoPlayer.setVolume(0.0f);
                }
                JZMediaExo.this.simpleExoPlayer.prepare(mediaSource);
                JZMediaExo.this.simpleExoPlayer.setPlayWhenReady(true);
                JZMediaExo jZMediaExo3 = JZMediaExo.this;
                Runnable unused2 = jZMediaExo3.callback = new onBufferingUpdate();
                JZMediaExo.this.simpleExoPlayer.setVideoSurface(new Surface(JZMediaExo.this.jzvd.textureView.getSurfaceTexture()));
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r4.mMediaHandlerThread;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0008, code lost:
        r2 = r4.simpleExoPlayer;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void release() {
        /*
            r4 = this;
            android.os.Handler r0 = r4.mMediaHandler
            if (r0 == 0) goto L_0x0017
            android.os.HandlerThread r1 = r4.mMediaHandlerThread
            if (r1 == 0) goto L_0x0017
            com.google.android.exoplayer2.SimpleExoPlayer r2 = r4.simpleExoPlayer
            if (r2 == 0) goto L_0x0017
            cn.jzvd.media.JZMediaExo$3 r3 = new cn.jzvd.media.JZMediaExo$3
            r3.<init>(r2, r1)
            r0.post(r3)
            r0 = 0
            r4.simpleExoPlayer = r0
        L_0x0017:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p019cn.jzvd.media.JZMediaExo.release():void");
    }

    public void seekTo(long j) {
        if (j != this.previousSeek) {
            this.simpleExoPlayer.seekTo(j);
            this.previousSeek = j;
            super.jzvd.seekToInAdvance = j;
        }
    }

    public void setSpeed(float f) {
        this.simpleExoPlayer.setPlaybackParameters(new PlaybackParameters(f, 1.0f));
    }

    public void setSurface(Surface surface) {
        this.simpleExoPlayer.setVideoSurface(surface);
    }

    public void setVolume(final float f, final float f2) {
        Handler handler = super.mMediaHandler;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class p019cn.jzvd.media.JZMediaExo.C08334 */

                public void run() {
                    if (JZMediaExo.this.simpleExoPlayer != null) {
                        JZMediaExo.this.simpleExoPlayer.setVolume(f);
                        JZMediaExo.this.simpleExoPlayer.setVolume(f2);
                    }
                }
            });
        }
    }

    public void start() {
        this.simpleExoPlayer.setPlayWhenReady(true);
    }
}
