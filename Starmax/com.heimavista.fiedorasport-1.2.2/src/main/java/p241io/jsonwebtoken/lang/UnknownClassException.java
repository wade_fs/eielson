package p241io.jsonwebtoken.lang;

/* renamed from: io.jsonwebtoken.lang.UnknownClassException */
public class UnknownClassException extends RuntimeException {
    public UnknownClassException(String str) {
        super(str);
    }
}
