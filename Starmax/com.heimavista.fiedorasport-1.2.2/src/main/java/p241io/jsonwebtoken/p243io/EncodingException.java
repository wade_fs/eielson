package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.EncodingException */
public class EncodingException extends CodecException {
    public EncodingException(String str, Throwable th) {
        super(str, th);
    }
}
