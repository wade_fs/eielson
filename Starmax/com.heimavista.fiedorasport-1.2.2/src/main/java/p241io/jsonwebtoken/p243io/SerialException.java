package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.SerialException */
public class SerialException extends IOException {
    public SerialException(String str) {
        super(str);
    }

    public SerialException(String str, Throwable th) {
        super(str, th);
    }
}
