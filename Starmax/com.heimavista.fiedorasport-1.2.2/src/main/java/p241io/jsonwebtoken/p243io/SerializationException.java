package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.SerializationException */
public class SerializationException extends SerialException {
    public SerializationException(String str) {
        super(str);
    }

    public SerializationException(String str, Throwable th) {
        super(str, th);
    }
}
