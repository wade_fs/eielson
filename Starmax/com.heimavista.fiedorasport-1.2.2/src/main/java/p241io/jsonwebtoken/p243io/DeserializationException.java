package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.DeserializationException */
public class DeserializationException extends SerialException {
    public DeserializationException(String str) {
        super(str);
    }

    public DeserializationException(String str, Throwable th) {
        super(str, th);
    }
}
