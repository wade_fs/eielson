package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Base64UrlDecoder */
class Base64UrlDecoder extends Base64Decoder {
    Base64UrlDecoder() {
        super(Base64.URL_SAFE);
    }
}
