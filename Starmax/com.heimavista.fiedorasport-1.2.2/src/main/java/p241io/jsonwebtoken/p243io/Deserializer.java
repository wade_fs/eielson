package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Deserializer */
public interface Deserializer<T> {
    T deserialize(byte[] bArr);
}
