package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.DecodingException */
public class DecodingException extends CodecException {
    public DecodingException(String str) {
        super(str);
    }

    public DecodingException(String str, Throwable th) {
        super(str, th);
    }
}
