package p241io.jsonwebtoken.p243io;

import p241io.jsonwebtoken.lang.Assert;

/* renamed from: io.jsonwebtoken.io.Base64Decoder */
class Base64Decoder extends Base64Support implements Decoder<String, byte[]> {
    Base64Decoder() {
        super(Base64.DEFAULT);
    }

    Base64Decoder(Base64 base64) {
        super(base64);
    }

    public byte[] decode(String str) {
        Assert.notNull(str, "String argument cannot be null");
        return super.base64.decodeFast(str.toCharArray());
    }
}
