package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Base64UrlEncoder */
class Base64UrlEncoder extends Base64Encoder {
    Base64UrlEncoder() {
        super(Base64.URL_SAFE);
    }
}
