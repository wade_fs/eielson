package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Serializer */
public interface Serializer<T> {
    byte[] serialize(T t);
}
