package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Decoder */
public interface Decoder<T, R> {
    R decode(T t);
}
