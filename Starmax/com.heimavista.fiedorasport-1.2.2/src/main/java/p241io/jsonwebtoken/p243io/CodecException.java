package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.CodecException */
public class CodecException extends IOException {
    public CodecException(String str) {
        super(str);
    }

    public CodecException(String str, Throwable th) {
        super(str, th);
    }
}
