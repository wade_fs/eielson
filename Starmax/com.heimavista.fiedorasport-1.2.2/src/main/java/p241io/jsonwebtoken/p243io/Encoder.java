package p241io.jsonwebtoken.p243io;

/* renamed from: io.jsonwebtoken.io.Encoder */
public interface Encoder<T, R> {
    R encode(T t);
}
