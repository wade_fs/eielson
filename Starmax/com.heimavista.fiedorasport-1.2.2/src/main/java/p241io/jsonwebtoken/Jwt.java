package p241io.jsonwebtoken;

import p241io.jsonwebtoken.Header;

/* renamed from: io.jsonwebtoken.Jwt */
public interface Jwt<H extends Header, B> {
    B getBody();

    H getHeader();
}
