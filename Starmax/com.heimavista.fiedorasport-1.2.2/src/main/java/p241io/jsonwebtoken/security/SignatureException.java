package p241io.jsonwebtoken.security;

/* renamed from: io.jsonwebtoken.security.SignatureException */
public class SignatureException extends p241io.jsonwebtoken.SignatureException {
    public SignatureException(String str) {
        super(str);
    }

    public SignatureException(String str, Throwable th) {
        super(str, th);
    }
}
