package p241io.jsonwebtoken;

import java.security.Key;
import java.security.PrivateKey;
import java.security.interfaces.ECKey;
import java.security.interfaces.RSAKey;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.crypto.SecretKey;
import p241io.jsonwebtoken.security.InvalidKeyException;
import p241io.jsonwebtoken.security.Keys;
import p241io.jsonwebtoken.security.SignatureException;
import p241io.jsonwebtoken.security.WeakKeyException;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: io.jsonwebtoken.SignatureAlgorithm */
public final class SignatureAlgorithm extends Enum<SignatureAlgorithm> {
    private static final /* synthetic */ SignatureAlgorithm[] $VALUES;
    public static final SignatureAlgorithm ES256 = new SignatureAlgorithm("ES256", 7, "ES256", "ECDSA using P-256 and SHA-256", "ECDSA", "SHA256withECDSA", true, 256, 256);
    public static final SignatureAlgorithm ES384 = new SignatureAlgorithm("ES384", 8, "ES384", "ECDSA using P-384 and SHA-384", "ECDSA", "SHA384withECDSA", true, 384, 384);
    public static final SignatureAlgorithm ES512 = new SignatureAlgorithm("ES512", 9, "ES512", "ECDSA using P-521 and SHA-512", "ECDSA", "SHA512withECDSA", true, 512, 521);
    public static final SignatureAlgorithm HS256 = new SignatureAlgorithm("HS256", 1, "HS256", "HMAC using SHA-256", "HMAC", "HmacSHA256", true, 256, 256);
    public static final SignatureAlgorithm HS384 = new SignatureAlgorithm("HS384", 2, "HS384", "HMAC using SHA-384", "HMAC", "HmacSHA384", true, 384, 384);
    public static final SignatureAlgorithm HS512 = new SignatureAlgorithm("HS512", 3, "HS512", "HMAC using SHA-512", "HMAC", "HmacSHA512", true, 512, 512);
    public static final SignatureAlgorithm NONE = new SignatureAlgorithm("NONE", 0, "none", "No digital signature or MAC performed", "None", null, false, 0, 0);
    private static final List<SignatureAlgorithm> PREFERRED_EC_ALGS = Collections.unmodifiableList(Arrays.asList(ES512, ES384, ES256));
    private static final List<SignatureAlgorithm> PREFERRED_HMAC_ALGS;
    public static final SignatureAlgorithm PS256 = new SignatureAlgorithm("PS256", 10, "PS256", "RSASSA-PSS using SHA-256 and MGF1 with SHA-256", "RSA", "SHA256withRSAandMGF1", false, 256, 2048);
    public static final SignatureAlgorithm PS384 = new SignatureAlgorithm("PS384", 11, "PS384", "RSASSA-PSS using SHA-384 and MGF1 with SHA-384", "RSA", "SHA384withRSAandMGF1", false, 384, 2048);
    public static final SignatureAlgorithm PS512 = new SignatureAlgorithm("PS512", 12, "PS512", "RSASSA-PSS using SHA-512 and MGF1 with SHA-512", "RSA", "SHA512withRSAandMGF1", false, 512, 2048);
    public static final SignatureAlgorithm RS256 = new SignatureAlgorithm("RS256", 4, "RS256", "RSASSA-PKCS-v1_5 using SHA-256", "RSA", "SHA256withRSA", true, 256, 2048);
    public static final SignatureAlgorithm RS384 = new SignatureAlgorithm("RS384", 5, "RS384", "RSASSA-PKCS-v1_5 using SHA-384", "RSA", "SHA384withRSA", true, 384, 2048);
    public static final SignatureAlgorithm RS512 = new SignatureAlgorithm("RS512", 6, "RS512", "RSASSA-PKCS-v1_5 using SHA-512", "RSA", "SHA512withRSA", true, 512, 2048);
    private final String description;
    private final int digestLength;
    private final String familyName;
    private final String jcaName;
    private final boolean jdkStandard;
    private final int minKeyLength;
    private final String value;

    static {
        SignatureAlgorithm signatureAlgorithm = HS256;
        SignatureAlgorithm signatureAlgorithm2 = HS384;
        SignatureAlgorithm signatureAlgorithm3 = HS512;
        $VALUES = new SignatureAlgorithm[]{NONE, signatureAlgorithm, signatureAlgorithm2, signatureAlgorithm3, RS256, RS384, RS512, ES256, ES384, ES512, PS256, PS384, PS512};
        PREFERRED_HMAC_ALGS = Collections.unmodifiableList(Arrays.asList(signatureAlgorithm3, signatureAlgorithm2, signatureAlgorithm));
    }

    private SignatureAlgorithm(String str, int i, String str2, String str3, String str4, String str5, boolean z, int i2, int i3) {
        this.value = str2;
        this.description = str3;
        this.familyName = str4;
        this.jcaName = str5;
        this.jdkStandard = z;
        this.digestLength = i2;
        this.minKeyLength = i3;
    }

    private void assertValid(Key key, boolean z) {
        Class<Keys> cls = Keys.class;
        if (this == NONE) {
            throw new InvalidKeyException("The 'NONE' signature algorithm does not support cryptographic keys.");
        } else if (isHmac()) {
            if (key instanceof SecretKey) {
                SecretKey secretKey = (SecretKey) key;
                byte[] encoded = secretKey.getEncoded();
                if (encoded != null) {
                    String algorithm = secretKey.getAlgorithm();
                    if (algorithm == null) {
                        throw new InvalidKeyException("The " + keyType(z) + " key's algorithm cannot be null.");
                    } else if (HS256.jcaName.equalsIgnoreCase(algorithm) || HS384.jcaName.equalsIgnoreCase(algorithm) || HS512.jcaName.equalsIgnoreCase(algorithm)) {
                        int length = encoded.length * 8;
                        if (length < this.minKeyLength) {
                            throw new WeakKeyException("The " + keyType(z) + " key's size is " + length + " bits which " + "is not secure enough for the " + name() + " algorithm.  The JWT " + "JWA Specification (RFC 7518, Section 3.2) states that keys used with " + name() + " MUST have a " + "size >= " + this.minKeyLength + " bits (the key size must be greater than or equal to the hash " + "output size).  Consider using the " + cls.getName() + " class's " + "'secretKeyFor(SignatureAlgorithm." + name() + ")' method to create a key guaranteed to be " + "secure enough for " + name() + ".  See " + "https://tools.ietf.org/html/rfc7518#section-3.2 for more information.");
                        }
                    } else {
                        throw new InvalidKeyException("The " + keyType(z) + " key's algorithm '" + algorithm + "' does not equal a valid HmacSHA* algorithm name and cannot be used with " + name() + ".");
                    }
                } else {
                    throw new InvalidKeyException("The " + keyType(z) + " key's encoded bytes cannot be null.");
                }
            } else {
                throw new InvalidKeyException(this.familyName + " " + keyType(z) + " keys must be SecretKey instances.");
            }
        } else if (z && !(key instanceof PrivateKey)) {
            throw new InvalidKeyException(this.familyName + " signing keys must be PrivateKey instances.");
        } else if (isEllipticCurve()) {
            if (key instanceof ECKey) {
                int bitLength = ((ECKey) key).getParams().getOrder().bitLength();
                if (bitLength < this.minKeyLength) {
                    throw new WeakKeyException("The " + keyType(z) + " key's size (ECParameterSpec order) is " + bitLength + " bits which is not secure enough for the " + name() + " algorithm.  The JWT " + "JWA Specification (RFC 7518, Section 3.4) states that keys used with " + name() + " MUST have a size >= " + this.minKeyLength + " bits.  Consider using the " + cls.getName() + " class's " + "'keyPairFor(SignatureAlgorithm." + name() + ")' method to create a key pair guaranteed " + "to be secure enough for " + name() + ".  See " + "https://tools.ietf.org/html/rfc7518#section-3.4 for more information.");
                }
                return;
            }
            throw new InvalidKeyException(this.familyName + " " + keyType(z) + " keys must be ECKey instances.");
        } else if (key instanceof RSAKey) {
            int bitLength2 = ((RSAKey) key).getModulus().bitLength();
            if (bitLength2 < this.minKeyLength) {
                String str = name().startsWith("P") ? "3.5" : "3.3";
                throw new WeakKeyException("The " + keyType(z) + " key's size is " + bitLength2 + " bits which is not secure " + "enough for the " + name() + " algorithm.  The JWT JWA Specification (RFC 7518, Section " + str + ") states that keys used with " + name() + " MUST have a size >= " + this.minKeyLength + " bits.  Consider using the " + cls.getName() + " class's " + "'keyPairFor(SignatureAlgorithm." + name() + ")' method to create a key pair guaranteed " + "to be secure enough for " + name() + ".  See " + "https://tools.ietf.org/html/rfc7518#section-" + str + " for more information.");
            }
        } else {
            throw new InvalidKeyException(this.familyName + " " + keyType(z) + " keys must be RSAKey instances.");
        }
    }

    public static SignatureAlgorithm forName(String str) {
        SignatureAlgorithm[] values = values();
        for (SignatureAlgorithm signatureAlgorithm : values) {
            if (signatureAlgorithm.getValue().equalsIgnoreCase(str)) {
                return signatureAlgorithm;
            }
        }
        throw new SignatureException("Unsupported signature algorithm '" + str + "'");
    }

    public static SignatureAlgorithm forSigningKey(Key key) {
        if (key != null) {
            boolean z = key instanceof SecretKey;
            if (!z && (!(key instanceof PrivateKey) || (!(key instanceof ECKey) && !(key instanceof RSAKey)))) {
                throw new InvalidKeyException("JWT standard signing algorithms require either 1) a SecretKey for HMAC-SHA algorithms or 2) a private RSAKey for RSA algorithms or 3) a private ECKey for Elliptic Curve algorithms.  The specified key is of type " + key.getClass().getName());
            } else if (z) {
                int length = p241io.jsonwebtoken.lang.Arrays.length(((SecretKey) key).getEncoded()) * 8;
                for (SignatureAlgorithm signatureAlgorithm : PREFERRED_HMAC_ALGS) {
                    if (length >= signatureAlgorithm.minKeyLength) {
                        return signatureAlgorithm;
                    }
                }
                throw new WeakKeyException("The specified SecretKey is not strong enough to be used with JWT HMAC signature algorithms.  The JWT specification requires HMAC keys to be >= 256 bits long.  The specified key is " + length + " bits.  See https://tools.ietf.org/html/rfc7518#section-3.2 for more " + "information.");
            } else if (key instanceof RSAKey) {
                int bitLength = ((RSAKey) key).getModulus().bitLength();
                if (bitLength >= 4096) {
                    RS512.assertValidSigningKey(key);
                    return RS512;
                } else if (bitLength >= 3072) {
                    RS384.assertValidSigningKey(key);
                    return RS384;
                } else {
                    SignatureAlgorithm signatureAlgorithm2 = RS256;
                    if (bitLength >= signatureAlgorithm2.minKeyLength) {
                        signatureAlgorithm2.assertValidSigningKey(key);
                        return RS256;
                    }
                    throw new WeakKeyException("The specified RSA signing key is not strong enough to be used with JWT RSA signature algorithms.  The JWT specification requires RSA keys to be >= 2048 bits long.  The specified RSA key is " + bitLength + " bits.  See https://tools.ietf.org/html/rfc7518#section-3.3 for more " + "information.");
                }
            } else {
                int bitLength2 = ((ECKey) key).getParams().getOrder().bitLength();
                for (SignatureAlgorithm signatureAlgorithm3 : PREFERRED_EC_ALGS) {
                    if (bitLength2 >= signatureAlgorithm3.minKeyLength) {
                        signatureAlgorithm3.assertValidSigningKey(key);
                        return signatureAlgorithm3;
                    }
                }
                throw new WeakKeyException("The specified Elliptic Curve signing key is not strong enough to be used with JWT ECDSA signature algorithms.  The JWT specification requires ECDSA keys to be >= 256 bits long.  The specified ECDSA key is " + bitLength2 + " bits.  See " + "https://tools.ietf.org/html/rfc7518#section-3.4 for more information.");
            }
        } else {
            throw new InvalidKeyException("Key argument cannot be null.");
        }
    }

    private static String keyType(boolean z) {
        return z ? "signing" : "verification";
    }

    public static SignatureAlgorithm valueOf(String str) {
        return (SignatureAlgorithm) Enum.valueOf(SignatureAlgorithm.class, str);
    }

    public static SignatureAlgorithm[] values() {
        return (SignatureAlgorithm[]) $VALUES.clone();
    }

    public void assertValidSigningKey(Key key) {
        assertValid(key, true);
    }

    public void assertValidVerificationKey(Key key) {
        assertValid(key, false);
    }

    public String getDescription() {
        return this.description;
    }

    public String getFamilyName() {
        return this.familyName;
    }

    public String getJcaName() {
        return this.jcaName;
    }

    public int getMinKeyLength() {
        return this.minKeyLength;
    }

    public String getValue() {
        return this.value;
    }

    public boolean isEllipticCurve() {
        return this.familyName.equals("ECDSA");
    }

    public boolean isHmac() {
        return this.familyName.equals("HMAC");
    }

    public boolean isJdkStandard() {
        return this.jdkStandard;
    }

    public boolean isRsa() {
        return this.familyName.equals("RSA");
    }
}
