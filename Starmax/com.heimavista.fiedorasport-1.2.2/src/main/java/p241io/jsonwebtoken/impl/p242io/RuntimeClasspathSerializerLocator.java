package p241io.jsonwebtoken.impl.p242io;

import java.util.concurrent.atomic.AtomicReference;
import p241io.jsonwebtoken.lang.Assert;
import p241io.jsonwebtoken.lang.Classes;
import p241io.jsonwebtoken.p243io.Serializer;

/* renamed from: io.jsonwebtoken.impl.io.RuntimeClasspathSerializerLocator */
public class RuntimeClasspathSerializerLocator implements InstanceLocator<Serializer> {
    private static final AtomicReference<Serializer<Object>> SERIALIZER = new AtomicReference<>();

    /* access modifiers changed from: protected */
    public boolean compareAndSet(Serializer<Object> serializer) {
        return SERIALIZER.compareAndSet(null, serializer);
    }

    /* access modifiers changed from: protected */
    public boolean isAvailable(String str) {
        return Classes.isAvailable(str);
    }

    /* access modifiers changed from: protected */
    public Serializer<Object> locate() {
        if (isAvailable("com.fasterxml.jackson.databind.ObjectMapper")) {
            return (Serializer) Classes.newInstance("io.jsonwebtoken.io.JacksonSerializer");
        }
        if (isAvailable("org.json.JSONObject")) {
            return (Serializer) Classes.newInstance("io.jsonwebtoken.io.OrgJsonSerializer");
        }
        throw new IllegalStateException("Unable to discover any JSON Serializer implementations on the classpath.");
    }

    public Serializer<Object> getInstance() {
        Serializer<Object> serializer = SERIALIZER.get();
        boolean z = true;
        if (serializer == null) {
            serializer = locate();
            Assert.state(serializer != null, "locate() cannot return null.");
            if (!compareAndSet(serializer)) {
                serializer = SERIALIZER.get();
            }
        }
        if (serializer == null) {
            z = false;
        }
        Assert.state(z, "serializer cannot be null.");
        return serializer;
    }
}
