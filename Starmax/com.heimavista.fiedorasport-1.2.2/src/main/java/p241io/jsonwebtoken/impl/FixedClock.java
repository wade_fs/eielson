package p241io.jsonwebtoken.impl;

import java.util.Date;
import p241io.jsonwebtoken.Clock;

/* renamed from: io.jsonwebtoken.impl.FixedClock */
public class FixedClock implements Clock {
    private final Date now;

    public FixedClock() {
        this(new Date());
    }

    public Date now() {
        return this.now;
    }

    public FixedClock(Date date) {
        this.now = date;
    }
}
