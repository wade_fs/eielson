package p241io.jsonwebtoken.impl;

import p241io.jsonwebtoken.Header;
import p241io.jsonwebtoken.Jwt;

/* renamed from: io.jsonwebtoken.impl.DefaultJwt */
public class DefaultJwt<B> implements Jwt<Header, B> {
    private final B body;
    private final Header header;

    public DefaultJwt(Header header2, B b) {
        this.header = header2;
        this.body = b;
    }

    public B getBody() {
        return this.body;
    }

    public Header getHeader() {
        return this.header;
    }

    public String toString() {
        return "header=" + this.header + ",body=" + ((Object) this.body);
    }
}
