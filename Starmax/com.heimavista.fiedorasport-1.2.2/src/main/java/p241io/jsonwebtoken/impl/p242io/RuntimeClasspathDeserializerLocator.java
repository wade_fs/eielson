package p241io.jsonwebtoken.impl.p242io;

import java.util.concurrent.atomic.AtomicReference;
import p241io.jsonwebtoken.lang.Assert;
import p241io.jsonwebtoken.lang.Classes;
import p241io.jsonwebtoken.p243io.Deserializer;

/* renamed from: io.jsonwebtoken.impl.io.RuntimeClasspathDeserializerLocator */
public class RuntimeClasspathDeserializerLocator<T> implements InstanceLocator<Deserializer<T>> {
    private static final AtomicReference<Deserializer> DESERIALIZER = new AtomicReference<>();

    /* access modifiers changed from: protected */
    public boolean compareAndSet(Deserializer<T> deserializer) {
        return DESERIALIZER.compareAndSet(null, deserializer);
    }

    /* access modifiers changed from: protected */
    public boolean isAvailable(String str) {
        return Classes.isAvailable(str);
    }

    /* access modifiers changed from: protected */
    public Deserializer<T> locate() {
        if (isAvailable("com.fasterxml.jackson.databind.ObjectMapper")) {
            return (Deserializer) Classes.newInstance("io.jsonwebtoken.io.JacksonDeserializer");
        }
        if (isAvailable("org.json.JSONObject")) {
            return (Deserializer) Classes.newInstance("io.jsonwebtoken.io.OrgJsonDeserializer");
        }
        throw new IllegalStateException("Unable to discover any JSON Deserializer implementations on the classpath.");
    }

    public Deserializer<T> getInstance() {
        Deserializer<T> deserializer = DESERIALIZER.get();
        boolean z = true;
        if (deserializer == null) {
            deserializer = locate();
            Assert.state(deserializer != null, "locate() cannot return null.");
            if (!compareAndSet(deserializer)) {
                deserializer = DESERIALIZER.get();
            }
        }
        if (deserializer == null) {
            z = false;
        }
        Assert.state(z, "deserializer cannot be null.");
        return deserializer;
    }
}
