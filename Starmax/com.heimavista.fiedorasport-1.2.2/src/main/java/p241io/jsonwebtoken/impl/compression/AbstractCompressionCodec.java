package p241io.jsonwebtoken.impl.compression;

import java.io.IOException;
import p241io.jsonwebtoken.CompressionCodec;
import p241io.jsonwebtoken.CompressionException;
import p241io.jsonwebtoken.lang.Assert;

/* renamed from: io.jsonwebtoken.impl.compression.AbstractCompressionCodec */
public abstract class AbstractCompressionCodec implements CompressionCodec {
    public final byte[] compress(byte[] bArr) {
        Assert.notNull(bArr, "payload cannot be null.");
        try {
            return doCompress(bArr);
        } catch (IOException e) {
            throw new CompressionException("Unable to compress payload.", e);
        }
    }

    public final byte[] decompress(byte[] bArr) {
        Assert.notNull(bArr, "compressed bytes cannot be null.");
        try {
            return doDecompress(bArr);
        } catch (IOException e) {
            throw new CompressionException("Unable to decompress bytes.", e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract byte[] doCompress(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract byte[] doDecompress(byte[] bArr);
}
