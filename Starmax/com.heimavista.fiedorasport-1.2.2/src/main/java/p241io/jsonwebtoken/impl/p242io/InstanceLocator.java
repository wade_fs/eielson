package p241io.jsonwebtoken.impl.p242io;

/* renamed from: io.jsonwebtoken.impl.io.InstanceLocator */
public interface InstanceLocator<T> {
    T getInstance();
}
