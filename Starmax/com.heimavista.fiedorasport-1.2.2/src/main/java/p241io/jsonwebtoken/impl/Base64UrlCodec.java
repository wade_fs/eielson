package p241io.jsonwebtoken.impl;

import p241io.jsonwebtoken.p243io.Decoders;
import p241io.jsonwebtoken.p243io.Encoders;

@Deprecated
/* renamed from: io.jsonwebtoken.impl.Base64UrlCodec */
public class Base64UrlCodec extends AbstractTextCodec {
    public byte[] decode(String str) {
        return Decoders.BASE64URL.decode(str);
    }

    public String encode(byte[] bArr) {
        return Encoders.BASE64URL.encode(bArr);
    }
}
