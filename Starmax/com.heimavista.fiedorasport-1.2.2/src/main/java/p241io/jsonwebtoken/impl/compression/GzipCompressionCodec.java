package p241io.jsonwebtoken.impl.compression;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import p241io.jsonwebtoken.CompressionCodec;
import p241io.jsonwebtoken.lang.Objects;

/* renamed from: io.jsonwebtoken.impl.compression.GzipCompressionCodec */
public class GzipCompressionCodec extends AbstractCompressionCodec implements CompressionCodec {
    private static final String GZIP = "GZIP";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.zip.GZIPOutputStream.<init>(java.io.OutputStream, boolean):void throws java.io.IOException}
     arg types: [java.io.ByteArrayOutputStream, int]
     candidates:
      ClspMth{java.util.zip.GZIPOutputStream.<init>(java.io.OutputStream, int):void throws java.io.IOException}
      ClspMth{java.util.zip.GZIPOutputStream.<init>(java.io.OutputStream, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: protected */
    public byte[] doCompress(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream((OutputStream) byteArrayOutputStream, true);
        try {
            gZIPOutputStream.write(bArr, 0, bArr.length);
            gZIPOutputStream.finish();
            return byteArrayOutputStream.toByteArray();
        } finally {
            Objects.nullSafeClose(gZIPOutputStream, byteArrayOutputStream);
        }
    }

    /* access modifiers changed from: protected */
    public byte[] doDecompress(byte[] bArr) {
        GZIPInputStream gZIPInputStream;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        byte[] bArr2 = new byte[512];
        ByteArrayOutputStream byteArrayOutputStream2 = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            } catch (Throwable th) {
                th = th;
                gZIPInputStream = null;
                Objects.nullSafeClose(byteArrayInputStream, gZIPInputStream, byteArrayOutputStream2);
                throw th;
            }
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
            } catch (Throwable th2) {
                th = th2;
                Objects.nullSafeClose(byteArrayInputStream, gZIPInputStream, byteArrayOutputStream2);
                throw th;
            }
            try {
                for (int read = gZIPInputStream.read(bArr2); read != -1; read = gZIPInputStream.read(bArr2)) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Objects.nullSafeClose(byteArrayInputStream, gZIPInputStream, byteArrayOutputStream);
                return byteArray;
            } catch (Throwable th3) {
                th = th3;
                byteArrayOutputStream2 = byteArrayOutputStream;
                Objects.nullSafeClose(byteArrayInputStream, gZIPInputStream, byteArrayOutputStream2);
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            gZIPInputStream = null;
            byteArrayInputStream = null;
            Objects.nullSafeClose(byteArrayInputStream, gZIPInputStream, byteArrayOutputStream2);
            throw th;
        }
    }

    public String getAlgorithmName() {
        return GZIP;
    }
}
