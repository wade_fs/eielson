package p241io.jsonwebtoken.impl.compression;

import p241io.jsonwebtoken.CompressionCodec;

@Deprecated
/* renamed from: io.jsonwebtoken.impl.compression.CompressionCodecs */
public final class CompressionCodecs {
    @Deprecated
    public static final CompressionCodec DEFLATE = p241io.jsonwebtoken.CompressionCodecs.DEFLATE;
    @Deprecated
    public static final CompressionCodec GZIP = p241io.jsonwebtoken.CompressionCodecs.GZIP;

    /* renamed from: I */
    private static final CompressionCodecs f11858I = new CompressionCodecs();

    private CompressionCodecs() {
    }
}
