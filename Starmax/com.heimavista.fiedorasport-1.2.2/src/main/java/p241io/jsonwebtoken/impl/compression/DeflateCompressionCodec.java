package p241io.jsonwebtoken.impl.compression;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;
import p241io.jsonwebtoken.lang.Objects;

/* renamed from: io.jsonwebtoken.impl.compression.DeflateCompressionCodec */
public class DeflateCompressionCodec extends AbstractCompressionCodec {
    private static final String DEFLATE = "DEF";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.zip.DeflaterOutputStream.<init>(java.io.OutputStream, java.util.zip.Deflater, boolean):void}
     arg types: [java.io.ByteArrayOutputStream, java.util.zip.Deflater, int]
     candidates:
      ClspMth{java.util.zip.DeflaterOutputStream.<init>(java.io.OutputStream, java.util.zip.Deflater, int):void}
      ClspMth{java.util.zip.DeflaterOutputStream.<init>(java.io.OutputStream, java.util.zip.Deflater, boolean):void} */
    public byte[] doCompress(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream;
        Deflater deflater = new Deflater(9);
        DeflaterOutputStream deflaterOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                DeflaterOutputStream deflaterOutputStream2 = new DeflaterOutputStream((OutputStream) byteArrayOutputStream, deflater, true);
                try {
                    deflaterOutputStream2.write(bArr, 0, bArr.length);
                    deflaterOutputStream2.flush();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Objects.nullSafeClose(byteArrayOutputStream, deflaterOutputStream2);
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    deflaterOutputStream = deflaterOutputStream2;
                    Objects.nullSafeClose(byteArrayOutputStream, deflaterOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                Objects.nullSafeClose(byteArrayOutputStream, deflaterOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            byteArrayOutputStream = null;
            Objects.nullSafeClose(byteArrayOutputStream, deflaterOutputStream);
            throw th;
        }
    }

    public byte[] doDecompress(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream;
        InflaterOutputStream inflaterOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                InflaterOutputStream inflaterOutputStream2 = new InflaterOutputStream(byteArrayOutputStream);
                try {
                    inflaterOutputStream2.write(bArr);
                    inflaterOutputStream2.flush();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Objects.nullSafeClose(byteArrayOutputStream, inflaterOutputStream2);
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    inflaterOutputStream = inflaterOutputStream2;
                    Objects.nullSafeClose(byteArrayOutputStream, inflaterOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                Objects.nullSafeClose(byteArrayOutputStream, inflaterOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            byteArrayOutputStream = null;
            Objects.nullSafeClose(byteArrayOutputStream, inflaterOutputStream);
            throw th;
        }
    }

    public String getAlgorithmName() {
        return DEFLATE;
    }
}
