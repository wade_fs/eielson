package p241io.jsonwebtoken.impl;

import com.google.android.exoplayer2.C1750C;
import java.nio.charset.Charset;
import p241io.jsonwebtoken.lang.Assert;

@Deprecated
/* renamed from: io.jsonwebtoken.impl.AbstractTextCodec */
public abstract class AbstractTextCodec implements TextCodec {
    protected static final Charset US_ASCII = Charset.forName(C1750C.ASCII_NAME);
    protected static final Charset UTF8 = Charset.forName(C1750C.UTF8_NAME);

    public String decodeToString(String str) {
        return new String(decode(str), UTF8);
    }

    public String encode(String str) {
        Assert.hasText(str, "String argument to encode cannot be null or empty.");
        return encode(str.getBytes(UTF8));
    }
}
