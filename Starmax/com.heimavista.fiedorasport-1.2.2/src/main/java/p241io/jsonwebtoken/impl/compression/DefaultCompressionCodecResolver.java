package p241io.jsonwebtoken.impl.compression;

import p241io.jsonwebtoken.CompressionCodec;
import p241io.jsonwebtoken.CompressionCodecResolver;
import p241io.jsonwebtoken.CompressionCodecs;
import p241io.jsonwebtoken.CompressionException;
import p241io.jsonwebtoken.Header;
import p241io.jsonwebtoken.lang.Assert;
import p241io.jsonwebtoken.lang.Strings;

/* renamed from: io.jsonwebtoken.impl.compression.DefaultCompressionCodecResolver */
public class DefaultCompressionCodecResolver implements CompressionCodecResolver {
    private String getAlgorithmFromHeader(Header header) {
        Assert.notNull(header, "header cannot be null.");
        return header.getCompressionAlgorithm();
    }

    public CompressionCodec resolveCompressionCodec(Header header) {
        String algorithmFromHeader = getAlgorithmFromHeader(header);
        if (!Strings.hasText(algorithmFromHeader)) {
            return null;
        }
        if (CompressionCodecs.DEFLATE.getAlgorithmName().equalsIgnoreCase(algorithmFromHeader)) {
            return CompressionCodecs.DEFLATE;
        }
        if (CompressionCodecs.GZIP.getAlgorithmName().equalsIgnoreCase(algorithmFromHeader)) {
            return CompressionCodecs.GZIP;
        }
        throw new CompressionException("Unsupported compression algorithm '" + algorithmFromHeader + "'");
    }
}
