package p241io.jsonwebtoken.impl;

import p241io.jsonwebtoken.p243io.Decoders;
import p241io.jsonwebtoken.p243io.Encoders;

@Deprecated
/* renamed from: io.jsonwebtoken.impl.AndroidBase64Codec */
public class AndroidBase64Codec extends AbstractTextCodec {
    public byte[] decode(String str) {
        return Decoders.BASE64.decode(str);
    }

    public String encode(byte[] bArr) {
        return Encoders.BASE64.encode(bArr);
    }
}
