package p241io.jsonwebtoken.impl.crypto;

/* renamed from: io.jsonwebtoken.impl.crypto.Signer */
public interface Signer {
    byte[] sign(byte[] bArr);
}
