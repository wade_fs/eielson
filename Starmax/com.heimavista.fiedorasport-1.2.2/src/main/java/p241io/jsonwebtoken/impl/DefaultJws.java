package p241io.jsonwebtoken.impl;

import p241io.jsonwebtoken.Jws;
import p241io.jsonwebtoken.JwsHeader;

/* renamed from: io.jsonwebtoken.impl.DefaultJws */
public class DefaultJws<B> implements Jws<B> {
    private final B body;
    private final JwsHeader header;
    private final String signature;

    public DefaultJws(JwsHeader jwsHeader, B b, String str) {
        this.header = jwsHeader;
        this.body = b;
        this.signature = str;
    }

    public B getBody() {
        return this.body;
    }

    public String getSignature() {
        return this.signature;
    }

    public String toString() {
        return "header=" + this.header + ",body=" + ((Object) this.body) + ",signature=" + this.signature;
    }

    public JwsHeader getHeader() {
        return this.header;
    }
}
