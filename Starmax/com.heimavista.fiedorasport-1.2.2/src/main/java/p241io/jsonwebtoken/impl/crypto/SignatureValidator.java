package p241io.jsonwebtoken.impl.crypto;

/* renamed from: io.jsonwebtoken.impl.crypto.SignatureValidator */
public interface SignatureValidator {
    boolean isValid(byte[] bArr, byte[] bArr2);
}
