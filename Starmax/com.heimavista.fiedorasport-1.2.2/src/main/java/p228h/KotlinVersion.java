package p228h;

import p228h.p229b.p230a.Intrinsics;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: h.a */
public final class KotlinVersion implements Comparable<KotlinVersion> {

    /* renamed from: T */
    public static final KotlinVersion f11194T = new KotlinVersion(1, 3, 50);

    /* renamed from: P */
    private final int f11195P = m17215a(this.f11196Q, this.f11197R, this.f11198S);

    /* renamed from: Q */
    private final int f11196Q;

    /* renamed from: R */
    private final int f11197R;

    /* renamed from: S */
    private final int f11198S;

    /* renamed from: h.a$a */
    /* compiled from: KotlinVersion.kt */
    public static final class C4937a {
        private C4937a() {
        }

        public /* synthetic */ C4937a(DefaultConstructorMarker aVar) {
            this();
        }
    }

    static {
        new C4937a(null);
    }

    public KotlinVersion(int i, int i2, int i3) {
        this.f11196Q = i;
        this.f11197R = i2;
        this.f11198S = i3;
    }

    /* renamed from: a */
    private final int m17215a(int i, int i2, int i3) {
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            return (i << 16) + (i2 << 8) + i3;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + ((char) JwtParser.SEPARATOR_CHAR) + i2 + ((char) JwtParser.SEPARATOR_CHAR) + i3).toString());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof KotlinVersion)) {
            obj = null;
        }
        KotlinVersion aVar = (KotlinVersion) obj;
        if (aVar == null || this.f11195P != aVar.f11195P) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.f11195P;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f11196Q);
        sb.append((char) JwtParser.SEPARATOR_CHAR);
        sb.append(this.f11197R);
        sb.append((char) JwtParser.SEPARATOR_CHAR);
        sb.append(this.f11198S);
        return sb.toString();
    }

    /* renamed from: a */
    public int compareTo(KotlinVersion aVar) {
        Intrinsics.m17219a(aVar, "other");
        return this.f11195P - aVar.f11195P;
    }
}
