package p228h.p229b.p230a;

import java.util.Arrays;

/* renamed from: h.b.a.b */
public class Intrinsics {
    private Intrinsics() {
    }

    /* renamed from: a */
    public static void m17219a(Object obj, String str) {
        if (obj == null) {
            m17220a(str);
            throw null;
        }
    }

    /* renamed from: a */
    private static void m17220a(String str) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
        String className = stackTraceElement.getClassName();
        String methodName = stackTraceElement.getMethodName();
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Parameter specified as non-null is null: method " + className + "." + methodName + ", parameter " + str);
        m17217a(illegalArgumentException);
        throw illegalArgumentException;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.b.a.b.a(java.lang.Throwable, java.lang.String):T
     arg types: [T, java.lang.String]
     candidates:
      h.b.a.b.a(java.lang.Object, java.lang.String):void
      h.b.a.b.a(java.lang.Throwable, java.lang.String):T */
    /* renamed from: a */
    private static <T extends Throwable> T m17217a(T t) {
        m17218a((Throwable) t, Intrinsics.class.getName());
        return t;
    }

    /* renamed from: a */
    static <T extends Throwable> T m17218a(Throwable th, String str) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        th.setStackTrace((StackTraceElement[]) Arrays.copyOfRange(stackTrace, i + 1, length));
        return th;
    }
}
