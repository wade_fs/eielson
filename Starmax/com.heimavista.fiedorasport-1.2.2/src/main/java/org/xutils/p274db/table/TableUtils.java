package org.xutils.p274db.table;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.p274db.annotation.Column;
import org.xutils.p274db.converter.ColumnConverterFactory;

/* renamed from: org.xutils.db.table.a */
final class TableUtils {
    /* renamed from: a */
    static synchronized LinkedHashMap<String, ColumnEntity> m20159a(Class<?> cls) {
        LinkedHashMap<String, ColumnEntity> linkedHashMap;
        synchronized (TableUtils.class) {
            linkedHashMap = new LinkedHashMap<>();
            m20160a(cls, linkedHashMap);
        }
        return linkedHashMap;
    }

    /* renamed from: a */
    private static void m20160a(Class<?> cls, HashMap<String, ColumnEntity> hashMap) {
        if (!Object.class.equals(cls)) {
            try {
                Field[] declaredFields = cls.getDeclaredFields();
                for (Field field : declaredFields) {
                    int modifiers = field.getModifiers();
                    if (!Modifier.isStatic(modifiers)) {
                        if (!Modifier.isTransient(modifiers)) {
                            Column column = (Column) field.getAnnotation(Column.class);
                            if (column != null && ColumnConverterFactory.isSupportColumnConverter(field.getType())) {
                                ColumnEntity columnEntity = new ColumnEntity(cls, field, column);
                                if (!hashMap.containsKey(columnEntity.getName())) {
                                    hashMap.put(columnEntity.getName(), columnEntity);
                                }
                            }
                        }
                    }
                }
                m20160a(cls.getSuperclass(), hashMap);
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
    }
}
