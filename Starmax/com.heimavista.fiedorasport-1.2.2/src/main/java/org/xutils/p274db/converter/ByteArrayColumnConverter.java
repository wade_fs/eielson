package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.ByteArrayColumnConverter */
public class ByteArrayColumnConverter implements ColumnConverter<byte[]> {
    public Object fieldValue2DbValue(byte[] bArr) {
        return bArr;
    }

    public ColumnDbType getColumnDbType() {
        return ColumnDbType.BLOB;
    }

    public byte[] getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return cursor.getBlob(i);
    }
}
