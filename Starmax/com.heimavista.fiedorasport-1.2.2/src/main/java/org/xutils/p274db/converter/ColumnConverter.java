package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.ColumnConverter */
public interface ColumnConverter<T> {
    Object fieldValue2DbValue(Object obj);

    ColumnDbType getColumnDbType();

    T getFieldValue(Cursor cursor, int i);
}
