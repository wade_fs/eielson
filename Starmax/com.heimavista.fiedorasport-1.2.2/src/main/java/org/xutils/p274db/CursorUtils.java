package org.xutils.p274db;

import android.database.Cursor;
import java.util.LinkedHashMap;
import org.xutils.p274db.table.ColumnEntity;
import org.xutils.p274db.table.DbModel;
import org.xutils.p274db.table.TableEntity;

/* renamed from: org.xutils.db.a */
final class CursorUtils {
    /* renamed from: a */
    public static <T> T m20145a(TableEntity<T> tableEntity, Cursor cursor) {
        T createEntity = tableEntity.createEntity();
        LinkedHashMap<String, ColumnEntity> columnMap = tableEntity.getColumnMap();
        int columnCount = cursor.getColumnCount();
        for (int i = 0; i < columnCount; i++) {
            ColumnEntity columnEntity = columnMap.get(cursor.getColumnName(i));
            if (columnEntity != null) {
                columnEntity.setValueFromCursor(createEntity, cursor, i);
            }
        }
        return createEntity;
    }

    /* renamed from: a */
    public static DbModel m20146a(Cursor cursor) {
        DbModel dbModel = new DbModel();
        int columnCount = cursor.getColumnCount();
        for (int i = 0; i < columnCount; i++) {
            dbModel.add(cursor.getColumnName(i), cursor.getString(i));
        }
        return dbModel;
    }
}
