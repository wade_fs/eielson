package org.xutils.p274db.table;

import android.database.Cursor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.xutils.common.util.LogUtil;
import org.xutils.p274db.annotation.Column;
import org.xutils.p274db.converter.ColumnConverter;
import org.xutils.p274db.converter.ColumnConverterFactory;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.table.ColumnEntity */
public final class ColumnEntity {

    /* renamed from: a */
    private final String f12513a;

    /* renamed from: b */
    private final boolean f12514b;

    /* renamed from: c */
    private final boolean f12515c;
    protected final ColumnConverter columnConverter;
    protected final Field columnField;
    protected final Method getMethod;
    protected final String name;
    protected final Method setMethod;

    ColumnEntity(Class<?> cls, Field field, Column column) {
        field.setAccessible(true);
        this.columnField = field;
        this.name = column.name();
        this.f12513a = column.property();
        this.f12514b = column.isId();
        Class<?> type = field.getType();
        this.f12515c = this.f12514b && column.autoGen() && ColumnUtils.isAutoIdType(type);
        this.columnConverter = ColumnConverterFactory.getColumnConverter(type);
        this.getMethod = ColumnUtils.m20155a(cls, field);
        Method method = this.getMethod;
        if (method != null && !method.isAccessible()) {
            this.getMethod.setAccessible(true);
        }
        this.setMethod = ColumnUtils.m20156b(cls, field);
        Method method2 = this.setMethod;
        if (method2 != null && !method2.isAccessible()) {
            this.setMethod.setAccessible(true);
        }
    }

    public ColumnConverter getColumnConverter() {
        return this.columnConverter;
    }

    public ColumnDbType getColumnDbType() {
        return this.columnConverter.getColumnDbType();
    }

    public Field getColumnField() {
        return this.columnField;
    }

    public Object getColumnValue(Object obj) {
        Object fieldValue = getFieldValue(obj);
        if (!this.f12515c || (!fieldValue.equals(0L) && !fieldValue.equals(0))) {
            return this.columnConverter.fieldValue2DbValue(fieldValue);
        }
        return null;
    }

    public Object getFieldValue(Object obj) {
        if (obj != null) {
            Method method = this.getMethod;
            if (method != null) {
                try {
                    return method.invoke(obj, new Object[0]);
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            } else {
                try {
                    return this.columnField.get(obj);
                } catch (Throwable th2) {
                    LogUtil.m20120e(th2.getMessage(), th2);
                }
            }
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public String getProperty() {
        return this.f12513a;
    }

    public boolean isAutoId() {
        return this.f12515c;
    }

    public boolean isId() {
        return this.f12514b;
    }

    public void setAutoIdValue(Object obj, long j) {
        Object valueOf = Long.valueOf(j);
        if (ColumnUtils.isInteger(this.columnField.getType())) {
            valueOf = Integer.valueOf((int) j);
        }
        Method method = this.setMethod;
        if (method != null) {
            try {
                method.invoke(obj, valueOf);
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        } else {
            try {
                this.columnField.set(obj, valueOf);
            } catch (Throwable th2) {
                LogUtil.m20120e(th2.getMessage(), th2);
            }
        }
    }

    public void setValueFromCursor(Object obj, Cursor cursor, int i) {
        Object fieldValue = this.columnConverter.getFieldValue(cursor, i);
        if (fieldValue != null) {
            Method method = this.setMethod;
            if (method != null) {
                try {
                    method.invoke(obj, fieldValue);
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            } else {
                try {
                    this.columnField.set(obj, fieldValue);
                } catch (Throwable th2) {
                    LogUtil.m20120e(th2.getMessage(), th2);
                }
            }
        }
    }

    public String toString() {
        return this.name;
    }
}
