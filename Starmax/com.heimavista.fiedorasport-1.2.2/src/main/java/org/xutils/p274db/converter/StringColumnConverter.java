package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.StringColumnConverter */
public class StringColumnConverter implements ColumnConverter<String> {
    public Object fieldValue2DbValue(String str) {
        return str;
    }

    public ColumnDbType getColumnDbType() {
        return ColumnDbType.TEXT;
    }

    public String getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return cursor.getString(i);
    }
}
