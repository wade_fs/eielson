package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.CharColumnConverter */
public class CharColumnConverter implements ColumnConverter<Character> {
    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }

    public Object fieldValue2DbValue(Character ch) {
        if (ch == null) {
            return null;
        }
        return Integer.valueOf(ch.charValue());
    }

    public Character getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return Character.valueOf((char) cursor.getInt(i));
    }
}
