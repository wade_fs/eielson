package org.xutils.p274db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.xutils.C5217x;
import org.xutils.DbManager;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.p274db.sqlite.SqlInfo;
import org.xutils.p274db.sqlite.SqlInfoBuilder;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p274db.table.ColumnEntity;
import org.xutils.p274db.table.DbBase;
import org.xutils.p274db.table.DbModel;
import org.xutils.p274db.table.TableEntity;
import org.xutils.p275ex.DbException;

/* renamed from: org.xutils.db.DbManagerImpl */
public final class DbManagerImpl extends DbBase {

    /* renamed from: T */
    private static final HashMap<DbManager.DaoConfig, DbManagerImpl> f12489T = new HashMap<>();

    /* renamed from: Q */
    private SQLiteDatabase f12490Q;

    /* renamed from: R */
    private DbManager.DaoConfig f12491R;

    /* renamed from: S */
    private boolean f12492S;

    private DbManagerImpl(DbManager.DaoConfig daoConfig) {
        if (daoConfig != null) {
            this.f12491R = daoConfig;
            this.f12492S = daoConfig.isAllowTransaction();
            this.f12490Q = m20134a(daoConfig);
            DbManager.DbOpenListener dbOpenListener = daoConfig.getDbOpenListener();
            if (dbOpenListener != null) {
                dbOpenListener.onDbOpened(this);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("daoConfig may not be null");
    }

    /* renamed from: a */
    private SQLiteDatabase m20134a(DbManager.DaoConfig daoConfig) {
        File dbDir = daoConfig.getDbDir();
        if (dbDir == null || (!dbDir.exists() && !dbDir.mkdirs())) {
            return C5217x.app().openOrCreateDatabase(daoConfig.getDbName(), 0, null);
        }
        return SQLiteDatabase.openOrCreateDatabase(new File(dbDir, daoConfig.getDbName()), (SQLiteDatabase.CursorFactory) null);
    }

    /* renamed from: b */
    private void m20138b(TableEntity<?> tableEntity, Object obj) {
        ColumnEntity id = tableEntity.getId();
        if (!id.isAutoId()) {
            execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo(tableEntity, obj));
        } else if (id.getColumnValue(obj) != null) {
            execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo(tableEntity, obj, new String[0]));
        } else {
            m20136a(tableEntity, obj);
        }
    }

    /* renamed from: c */
    private void m20139c() {
        if (this.f12492S) {
            this.f12490Q.setTransactionSuccessful();
        }
    }

    public static synchronized DbManager getInstance(DbManager.DaoConfig daoConfig) {
        DbManagerImpl dbManagerImpl;
        synchronized (DbManagerImpl.class) {
            if (daoConfig == null) {
                daoConfig = new DbManager.DaoConfig();
            }
            dbManagerImpl = f12489T.get(daoConfig);
            if (dbManagerImpl == null) {
                dbManagerImpl = new DbManagerImpl(daoConfig);
                f12489T.put(daoConfig, dbManagerImpl);
            } else {
                dbManagerImpl.f12491R = daoConfig;
            }
            SQLiteDatabase sQLiteDatabase = dbManagerImpl.f12490Q;
            int version = sQLiteDatabase.getVersion();
            int dbVersion = daoConfig.getDbVersion();
            if (version != dbVersion) {
                if (version != 0) {
                    DbManager.DbUpgradeListener dbUpgradeListener = daoConfig.getDbUpgradeListener();
                    if (dbUpgradeListener != null) {
                        dbUpgradeListener.onUpgrade(dbManagerImpl, version, dbVersion);
                    } else {
                        try {
                            super.dropDb();
                        } catch (DbException e) {
                            LogUtil.m20120e(e.getMessage(), e);
                        }
                    }
                }
                sQLiteDatabase.setVersion(dbVersion);
            }
        }
        return dbManagerImpl;
    }

    public void close() {
        if (f12489T.containsKey(this.f12491R)) {
            f12489T.remove(this.f12491R);
            this.f12490Q.close();
        }
    }

    public void delete(Object obj) {
        try {
            m20135a();
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (!list.isEmpty()) {
                    TableEntity table = getTable(list.get(0).getClass());
                    if (!table.tableIsExist()) {
                        m20137b();
                        return;
                    }
                    for (Object obj2 : list) {
                        execNonQuery(SqlInfoBuilder.buildDeleteSqlInfo(table, obj2));
                    }
                } else {
                    return;
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                if (!table2.tableIsExist()) {
                    m20137b();
                    return;
                }
                execNonQuery(SqlInfoBuilder.buildDeleteSqlInfo(table2, obj));
            }
            m20139c();
            m20137b();
        } finally {
            m20137b();
        }
    }

    public void deleteById(Class<?> cls, Object obj) {
        TableEntity table = getTable(cls);
        if (table.tableIsExist()) {
            try {
                m20135a();
                execNonQuery(SqlInfoBuilder.buildDeleteSqlInfoById(table, obj));
                m20139c();
            } finally {
                m20137b();
            }
        }
    }

    public void execNonQuery(SqlInfo sqlInfo) {
        SQLiteStatement sQLiteStatement = null;
        try {
            sQLiteStatement = sqlInfo.buildStatement(this.f12490Q);
            sQLiteStatement.execute();
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.releaseReference();
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            }
        } catch (Throwable th2) {
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.releaseReference();
                } catch (Throwable th3) {
                    LogUtil.m20120e(th3.getMessage(), th3);
                }
            }
            throw th2;
        }
    }

    public Cursor execQuery(SqlInfo sqlInfo) {
        try {
            return this.f12490Q.rawQuery(sqlInfo.getSql(), sqlInfo.getBindArgsAsStrArray());
        } catch (Throwable th) {
            throw new DbException(th);
        }
    }

    public int executeUpdateDelete(SqlInfo sqlInfo) {
        SQLiteStatement sQLiteStatement = null;
        try {
            SQLiteStatement buildStatement = sqlInfo.buildStatement(this.f12490Q);
            int executeUpdateDelete = buildStatement.executeUpdateDelete();
            if (buildStatement != null) {
                try {
                    buildStatement.releaseReference();
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            }
            return executeUpdateDelete;
        } catch (Throwable th2) {
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.releaseReference();
                } catch (Throwable th3) {
                    LogUtil.m20120e(th3.getMessage(), th3);
                }
            }
            throw th2;
        }
    }

    public <T> List<T> findAll(Class<T> cls) {
        return selector(cls).findAll();
    }

    public <T> T findById(Class<T> cls, Object obj) {
        Cursor execQuery;
        TableEntity<T> table = getTable(cls);
        if (table.tableIsExist() && (execQuery = execQuery(Selector.m20142a(table).where(table.getId().getName(), "=", obj).limit(1).toString())) != null) {
            try {
                if (execQuery.moveToNext()) {
                    T a = CursorUtils.m20145a(table, execQuery);
                    IOUtil.closeQuietly(execQuery);
                    return a;
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return null;
    }

    public List<DbModel> findDbModelAll(SqlInfo sqlInfo) {
        ArrayList arrayList = new ArrayList();
        Cursor execQuery = execQuery(sqlInfo);
        if (execQuery != null) {
            while (execQuery.moveToNext()) {
                try {
                    arrayList.add(CursorUtils.m20146a(execQuery));
                } catch (Throwable th) {
                    IOUtil.closeQuietly(execQuery);
                    throw th;
                }
            }
            IOUtil.closeQuietly(execQuery);
        }
        return arrayList;
    }

    public DbModel findDbModelFirst(SqlInfo sqlInfo) {
        Cursor execQuery = execQuery(sqlInfo);
        if (execQuery == null) {
            return null;
        }
        try {
            if (execQuery.moveToNext()) {
                DbModel a = CursorUtils.m20146a(execQuery);
                IOUtil.closeQuietly(execQuery);
                return a;
            }
            IOUtil.closeQuietly(execQuery);
            return null;
        } catch (Throwable th) {
            IOUtil.closeQuietly(execQuery);
            throw th;
        }
    }

    public <T> T findFirst(Class<T> cls) {
        return selector(cls).findFirst();
    }

    public DbManager.DaoConfig getDaoConfig() {
        return this.f12491R;
    }

    public SQLiteDatabase getDatabase() {
        return this.f12490Q;
    }

    public void replace(Object obj) {
        try {
            m20135a();
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (!list.isEmpty()) {
                    TableEntity table = getTable(list.get(0).getClass());
                    createTableIfNotExist(table);
                    for (Object obj2 : list) {
                        execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo(table, obj2));
                    }
                } else {
                    return;
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                createTableIfNotExist(table2);
                execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo(table2, obj));
            }
            m20139c();
            m20137b();
        } finally {
            m20137b();
        }
    }

    public void save(Object obj) {
        try {
            m20135a();
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (!list.isEmpty()) {
                    TableEntity table = getTable(list.get(0).getClass());
                    createTableIfNotExist(table);
                    for (Object obj2 : list) {
                        execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(table, obj2));
                    }
                } else {
                    return;
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                createTableIfNotExist(table2);
                execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(table2, obj));
            }
            m20139c();
            m20137b();
        } finally {
            m20137b();
        }
    }

    public boolean saveBindingId(Object obj) {
        try {
            m20135a();
            boolean z = false;
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (list.isEmpty()) {
                    return false;
                }
                TableEntity table = getTable(list.get(0).getClass());
                createTableIfNotExist(table);
                for (Object obj2 : list) {
                    if (!m20136a(table, obj2)) {
                        throw new DbException("saveBindingId error, transaction will not commit!");
                    }
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                createTableIfNotExist(table2);
                z = m20136a(table2, obj);
            }
            m20139c();
            m20137b();
            return z;
        } finally {
            m20137b();
        }
    }

    public void saveOrUpdate(Object obj) {
        try {
            m20135a();
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (!list.isEmpty()) {
                    TableEntity table = getTable(list.get(0).getClass());
                    createTableIfNotExist(table);
                    for (Object obj2 : list) {
                        m20138b(table, obj2);
                    }
                } else {
                    return;
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                createTableIfNotExist(table2);
                m20138b(table2, obj);
            }
            m20139c();
            m20137b();
        } finally {
            m20137b();
        }
    }

    public <T> Selector<T> selector(Class<T> cls) {
        return Selector.m20142a(getTable(cls));
    }

    public void update(Object obj, String... strArr) {
        try {
            m20135a();
            if (obj instanceof List) {
                List<Object> list = (List) obj;
                if (!list.isEmpty()) {
                    TableEntity table = getTable(list.get(0).getClass());
                    if (!table.tableIsExist()) {
                        m20137b();
                        return;
                    }
                    for (Object obj2 : list) {
                        execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo(table, obj2, strArr));
                    }
                } else {
                    return;
                }
            } else {
                TableEntity table2 = getTable(obj.getClass());
                if (!table2.tableIsExist()) {
                    m20137b();
                    return;
                }
                execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo(table2, obj, strArr));
            }
            m20139c();
            m20137b();
        } finally {
            m20137b();
        }
    }

    public Cursor execQuery(String str) {
        try {
            return this.f12490Q.rawQuery(str, null);
        } catch (Throwable th) {
            throw new DbException(th);
        }
    }

    /* renamed from: a */
    private boolean m20136a(TableEntity<?> tableEntity, Object obj) {
        ColumnEntity id = tableEntity.getId();
        if (id.isAutoId()) {
            execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(tableEntity, obj));
            long a = m20133a(tableEntity.getName());
            if (a == -1) {
                return false;
            }
            id.setAutoIdValue(obj, a);
            return true;
        }
        execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(tableEntity, obj));
        return true;
    }

    /* renamed from: b */
    private void m20137b() {
        if (this.f12492S) {
            this.f12490Q.endTransaction();
        }
    }

    public void execNonQuery(String str) {
        try {
            this.f12490Q.execSQL(str);
        } catch (Throwable th) {
            throw new DbException(th);
        }
    }

    public int executeUpdateDelete(String str) {
        SQLiteStatement sQLiteStatement = null;
        try {
            SQLiteStatement compileStatement = this.f12490Q.compileStatement(str);
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            if (compileStatement != null) {
                try {
                    compileStatement.releaseReference();
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            }
            return executeUpdateDelete;
        } catch (Throwable th2) {
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.releaseReference();
                } catch (Throwable th3) {
                    LogUtil.m20120e(th3.getMessage(), th3);
                }
            }
            throw th2;
        }
    }

    /* renamed from: a */
    private long m20133a(String str) {
        Cursor execQuery = execQuery("SELECT seq FROM sqlite_sequence WHERE name='" + str + "' LIMIT 1");
        long j = -1;
        if (execQuery != null) {
            try {
                if (execQuery.moveToNext()) {
                    j = execQuery.getLong(0);
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return j;
    }

    public void delete(Class<?> cls) {
        delete(cls, null);
    }

    public int update(Class<?> cls, WhereBuilder whereBuilder, KeyValue... keyValueArr) {
        TableEntity table = getTable(cls);
        if (!table.tableIsExist()) {
            return 0;
        }
        try {
            m20135a();
            int executeUpdateDelete = executeUpdateDelete(SqlInfoBuilder.buildUpdateSqlInfo(table, whereBuilder, keyValueArr));
            m20139c();
            return executeUpdateDelete;
        } finally {
            m20137b();
        }
    }

    /* renamed from: a */
    private void m20135a() {
        if (!this.f12492S) {
            return;
        }
        if (Build.VERSION.SDK_INT < 16 || !this.f12490Q.isWriteAheadLoggingEnabled()) {
            this.f12490Q.beginTransaction();
        } else {
            this.f12490Q.beginTransactionNonExclusive();
        }
    }

    public int delete(Class<?> cls, WhereBuilder whereBuilder) {
        TableEntity table = getTable(cls);
        if (!table.tableIsExist()) {
            return 0;
        }
        try {
            m20135a();
            int executeUpdateDelete = executeUpdateDelete(SqlInfoBuilder.buildDeleteSqlInfo((TableEntity<?>) table, whereBuilder));
            m20139c();
            return executeUpdateDelete;
        } finally {
            m20137b();
        }
    }
}
