package org.xutils.p274db.sqlite;

import java.util.ArrayList;
import java.util.List;

/* renamed from: org.xutils.db.sqlite.WhereBuilder */
public class WhereBuilder {

    /* renamed from: a */
    private final List<String> f12512a = new ArrayList();

    private WhereBuilder() {
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:89:0x0144 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:85:0x00ac */
    /* JADX WARN: Type inference failed for: r2v5, types: [java.lang.Iterable] */
    /* JADX WARN: Type inference failed for: r2v9, types: [java.lang.Iterable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m20148a(java.lang.String r8, java.lang.String r9, java.lang.String r10, java.lang.Object r11) {
        /*
            r7 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.util.List<java.lang.String> r1 = r7.f12512a
            int r1 = r1.size()
            java.lang.String r2 = " "
            if (r1 <= 0) goto L_0x0012
            r0.append(r2)
        L_0x0012:
            boolean r1 = android.text.TextUtils.isEmpty(r8)
            if (r1 != 0) goto L_0x001e
            r0.append(r8)
            r0.append(r2)
        L_0x001e:
            java.lang.String r8 = "\""
            r0.append(r8)
            r0.append(r9)
            r0.append(r8)
            java.lang.String r8 = "!="
            boolean r8 = r8.equals(r10)
            java.lang.String r9 = "="
            java.lang.String r1 = "<>"
            if (r8 == 0) goto L_0x0037
            r10 = r1
            goto L_0x0040
        L_0x0037:
            java.lang.String r8 = "=="
            boolean r8 = r8.equals(r10)
            if (r8 == 0) goto L_0x0040
            r10 = r9
        L_0x0040:
            if (r11 != 0) goto L_0x0069
            boolean r8 = r9.equals(r10)
            if (r8 == 0) goto L_0x004f
            java.lang.String r8 = " IS NULL"
            r0.append(r8)
            goto L_0x01f7
        L_0x004f:
            boolean r8 = r1.equals(r10)
            if (r8 == 0) goto L_0x005c
            java.lang.String r8 = " IS NOT NULL"
            r0.append(r8)
            goto L_0x01f7
        L_0x005c:
            r0.append(r2)
            r0.append(r10)
            java.lang.String r8 = " NULL"
            r0.append(r8)
            goto L_0x01f7
        L_0x0069:
            r0.append(r2)
            r0.append(r10)
            r0.append(r2)
            java.lang.String r8 = "IN"
            boolean r8 = r8.equalsIgnoreCase(r10)
            java.lang.String r9 = "value must be an Array or an Iterable."
            r1 = 0
            r2 = 0
            java.lang.String r3 = "''"
            r4 = -1
            r5 = 39
            java.lang.String r6 = "'"
            if (r8 == 0) goto L_0x0115
            boolean r8 = r11 instanceof java.lang.Iterable
            if (r8 == 0) goto L_0x008d
            r2 = r11
            java.lang.Iterable r2 = (java.lang.Iterable) r2
            goto L_0x00ac
        L_0x008d:
            java.lang.Class r8 = r11.getClass()
            boolean r8 = r8.isArray()
            if (r8 == 0) goto L_0x00ac
            int r8 = java.lang.reflect.Array.getLength(r11)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r8)
        L_0x00a0:
            if (r1 >= r8) goto L_0x00ac
            java.lang.Object r10 = java.lang.reflect.Array.get(r11, r1)
            r2.add(r10)
            int r1 = r1 + 1
            goto L_0x00a0
        L_0x00ac:
            if (r2 == 0) goto L_0x010f
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "("
            r8.<init>(r9)
            java.util.Iterator r9 = r2.iterator()
        L_0x00b9:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x00f8
            java.lang.Object r10 = r9.next()
            java.lang.Object r10 = org.xutils.p274db.table.ColumnUtils.convert2DbValueIfNeeded(r10)
            org.xutils.db.sqlite.ColumnDbType r11 = org.xutils.p274db.sqlite.ColumnDbType.TEXT
            java.lang.Class r1 = r10.getClass()
            org.xutils.db.sqlite.ColumnDbType r1 = org.xutils.p274db.converter.ColumnConverterFactory.getDbColumnType(r1)
            boolean r11 = r11.equals(r1)
            if (r11 == 0) goto L_0x00ef
            java.lang.String r10 = r10.toString()
            int r11 = r10.indexOf(r5)
            if (r11 == r4) goto L_0x00e5
            java.lang.String r10 = r10.replace(r6, r3)
        L_0x00e5:
            r8.append(r6)
            r8.append(r10)
            r8.append(r6)
            goto L_0x00f2
        L_0x00ef:
            r8.append(r10)
        L_0x00f2:
            java.lang.String r10 = ","
            r8.append(r10)
            goto L_0x00b9
        L_0x00f8:
            int r9 = r8.length()
            int r9 = r9 + -1
            r8.deleteCharAt(r9)
            java.lang.String r9 = ")"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r0.append(r8)
            goto L_0x01f7
        L_0x010f:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            r8.<init>(r9)
            throw r8
        L_0x0115:
            java.lang.String r8 = "BETWEEN"
            boolean r8 = r8.equalsIgnoreCase(r10)
            if (r8 == 0) goto L_0x01c8
            boolean r8 = r11 instanceof java.lang.Iterable
            if (r8 == 0) goto L_0x0125
            r2 = r11
            java.lang.Iterable r2 = (java.lang.Iterable) r2
            goto L_0x0144
        L_0x0125:
            java.lang.Class r8 = r11.getClass()
            boolean r8 = r8.isArray()
            if (r8 == 0) goto L_0x0144
            int r8 = java.lang.reflect.Array.getLength(r11)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r8)
        L_0x0138:
            if (r1 >= r8) goto L_0x0144
            java.lang.Object r10 = java.lang.reflect.Array.get(r11, r1)
            r2.add(r10)
            int r1 = r1 + 1
            goto L_0x0138
        L_0x0144:
            if (r2 == 0) goto L_0x01c2
            java.util.Iterator r8 = r2.iterator()
            boolean r9 = r8.hasNext()
            java.lang.String r10 = "value must have tow items."
            if (r9 == 0) goto L_0x01bc
            java.lang.Object r9 = r8.next()
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x01b6
            java.lang.Object r8 = r8.next()
            java.lang.Object r9 = org.xutils.p274db.table.ColumnUtils.convert2DbValueIfNeeded(r9)
            java.lang.Object r8 = org.xutils.p274db.table.ColumnUtils.convert2DbValueIfNeeded(r8)
            org.xutils.db.sqlite.ColumnDbType r10 = org.xutils.p274db.sqlite.ColumnDbType.TEXT
            java.lang.Class r11 = r9.getClass()
            org.xutils.db.sqlite.ColumnDbType r11 = org.xutils.p274db.converter.ColumnConverterFactory.getDbColumnType(r11)
            boolean r10 = r10.equals(r11)
            java.lang.String r11 = " AND "
            if (r10 == 0) goto L_0x01ac
            java.lang.String r9 = r9.toString()
            int r10 = r9.indexOf(r5)
            if (r10 == r4) goto L_0x0188
            java.lang.String r9 = r9.replace(r6, r3)
        L_0x0188:
            java.lang.String r8 = r8.toString()
            int r10 = r8.indexOf(r5)
            if (r10 == r4) goto L_0x0196
            java.lang.String r8 = r8.replace(r6, r3)
        L_0x0196:
            r0.append(r6)
            r0.append(r9)
            r0.append(r6)
            r0.append(r11)
            r0.append(r6)
            r0.append(r8)
            r0.append(r6)
            goto L_0x01f7
        L_0x01ac:
            r0.append(r9)
            r0.append(r11)
            r0.append(r8)
            goto L_0x01f7
        L_0x01b6:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            r8.<init>(r10)
            throw r8
        L_0x01bc:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            r8.<init>(r10)
            throw r8
        L_0x01c2:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            r8.<init>(r9)
            throw r8
        L_0x01c8:
            java.lang.Object r8 = org.xutils.p274db.table.ColumnUtils.convert2DbValueIfNeeded(r11)
            org.xutils.db.sqlite.ColumnDbType r9 = org.xutils.p274db.sqlite.ColumnDbType.TEXT
            java.lang.Class r10 = r8.getClass()
            org.xutils.db.sqlite.ColumnDbType r10 = org.xutils.p274db.converter.ColumnConverterFactory.getDbColumnType(r10)
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x01f4
            java.lang.String r8 = r8.toString()
            int r9 = r8.indexOf(r5)
            if (r9 == r4) goto L_0x01ea
            java.lang.String r8 = r8.replace(r6, r3)
        L_0x01ea:
            r0.append(r6)
            r0.append(r8)
            r0.append(r6)
            goto L_0x01f7
        L_0x01f4:
            r0.append(r8)
        L_0x01f7:
            java.util.List<java.lang.String> r8 = r7.f12512a
            java.lang.String r9 = r0.toString()
            r8.add(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.p274db.sqlite.WhereBuilder.m20148a(java.lang.String, java.lang.String, java.lang.String, java.lang.Object):void");
    }

    /* renamed from: b */
    public static WhereBuilder m20149b() {
        return new WhereBuilder();
    }

    public WhereBuilder and(String str, String str2, Object obj) {
        m20148a(this.f12512a.size() == 0 ? null : "AND", str, str2, obj);
        return this;
    }

    public WhereBuilder expr(String str) {
        List<String> list = this.f12512a;
        list.add(" " + str);
        return this;
    }

    public int getWhereItemSize() {
        return this.f12512a.size();
    }

    /* renamed from: or */
    public WhereBuilder mo28689or(String str, String str2, Object obj) {
        m20148a(this.f12512a.size() == 0 ? null : "OR", str, str2, obj);
        return this;
    }

    public String toString() {
        if (this.f12512a.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String str : this.f12512a) {
            sb.append(str);
        }
        return sb.toString();
    }

    /* renamed from: b */
    public static WhereBuilder m20150b(String str, String str2, Object obj) {
        WhereBuilder whereBuilder = new WhereBuilder();
        whereBuilder.m20148a(null, str, str2, obj);
        return whereBuilder;
    }

    public WhereBuilder and(WhereBuilder whereBuilder) {
        String str = this.f12512a.size() == 0 ? " " : "AND ";
        return expr(str + "(" + whereBuilder.toString() + ")");
    }

    /* renamed from: or */
    public WhereBuilder mo28690or(WhereBuilder whereBuilder) {
        String str = this.f12512a.size() == 0 ? " " : "OR ";
        return expr(str + "(" + whereBuilder.toString() + ")");
    }
}
