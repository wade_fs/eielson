package org.xutils.p274db.table;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import org.xutils.common.util.LogUtil;
import org.xutils.p274db.converter.ColumnConverterFactory;

/* renamed from: org.xutils.db.table.ColumnUtils */
public final class ColumnUtils {

    /* renamed from: a */
    private static final HashSet<Class<?>> f12516a = new HashSet<>(2);

    /* renamed from: b */
    private static final HashSet<Class<?>> f12517b = new HashSet<>(2);

    /* renamed from: c */
    private static final HashSet<Class<?>> f12518c = new HashSet<>(4);

    static {
        f12516a.add(Boolean.TYPE);
        f12516a.add(Boolean.class);
        f12517b.add(Integer.TYPE);
        f12517b.add(Integer.class);
        f12518c.addAll(f12517b);
        f12518c.add(Long.TYPE);
        f12518c.add(Long.class);
    }

    private ColumnUtils() {
    }

    /* renamed from: a */
    static Method m20155a(Class<?> cls, Field field) {
        Method method = null;
        if (Object.class.equals(cls)) {
            return null;
        }
        String name = field.getName();
        if (isBoolean(field.getType())) {
            method = m20153a(cls, name);
        }
        if (method == null) {
            String str = "get" + name.substring(0, 1).toUpperCase();
            if (name.length() > 1) {
                str = str + name.substring(1);
            }
            try {
                method = cls.getDeclaredMethod(str, new Class[0]);
            } catch (NoSuchMethodException unused) {
                LogUtil.m20117d(cls.getName() + "#" + str + " not exist");
            }
        }
        return method == null ? m20155a(cls.getSuperclass(), field) : method;
    }

    /* renamed from: b */
    static Method m20156b(Class<?> cls, Field field) {
        Method method = null;
        if (Object.class.equals(cls)) {
            return null;
        }
        String name = field.getName();
        Class<?> type = field.getType();
        if (isBoolean(type)) {
            method = m20154a(cls, name, type);
        }
        if (method == null) {
            String str = "set" + name.substring(0, 1).toUpperCase();
            if (name.length() > 1) {
                str = str + name.substring(1);
            }
            try {
                method = cls.getDeclaredMethod(str, type);
            } catch (NoSuchMethodException unused) {
                LogUtil.m20117d(cls.getName() + "#" + str + " not exist");
            }
        }
        return method == null ? m20156b(cls.getSuperclass(), field) : method;
    }

    public static Object convert2DbValueIfNeeded(Object obj) {
        return obj != null ? ColumnConverterFactory.getColumnConverter(obj.getClass()).fieldValue2DbValue(obj) : obj;
    }

    public static boolean isAutoIdType(Class<?> cls) {
        return f12518c.contains(cls);
    }

    public static boolean isBoolean(Class<?> cls) {
        return f12516a.contains(cls);
    }

    public static boolean isInteger(Class<?> cls) {
        return f12517b.contains(cls);
    }

    /* renamed from: a */
    private static Method m20153a(Class<?> cls, String str) {
        if (!str.startsWith("is")) {
            String str2 = "is" + str.substring(0, 1).toUpperCase();
            if (str.length() > 1) {
                str = str2 + str.substring(1);
            } else {
                str = str2;
            }
        }
        try {
            return cls.getDeclaredMethod(str, new Class[0]);
        } catch (NoSuchMethodException unused) {
            LogUtil.m20117d(cls.getName() + "#" + str + " not exist");
            return null;
        }
    }

    /* renamed from: a */
    private static Method m20154a(Class<?> cls, String str, Class<?> cls2) {
        String str2;
        if (!str.startsWith("is") || str.length() <= 2) {
            str2 = "set" + str.substring(0, 1).toUpperCase();
            if (str.length() > 1) {
                str2 = str2 + str.substring(1);
            }
        } else {
            str2 = "set" + str.substring(2, 3).toUpperCase();
            if (str.length() > 3) {
                str2 = str2 + str.substring(3);
            }
        }
        try {
            return cls.getDeclaredMethod(str2, cls2);
        } catch (NoSuchMethodException unused) {
            LogUtil.m20117d(cls.getName() + "#" + str2 + " not exist");
            return null;
        }
    }
}
