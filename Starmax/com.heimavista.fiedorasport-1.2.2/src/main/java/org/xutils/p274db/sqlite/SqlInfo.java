package org.xutils.p274db.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import java.util.ArrayList;
import java.util.List;
import org.xutils.common.util.KeyValue;
import org.xutils.p274db.converter.ColumnConverterFactory;
import org.xutils.p274db.table.ColumnUtils;

/* renamed from: org.xutils.db.sqlite.SqlInfo */
public final class SqlInfo {

    /* renamed from: a */
    private String f12507a;

    /* renamed from: b */
    private List<KeyValue> f12508b;

    /* renamed from: org.xutils.db.sqlite.SqlInfo$a */
    static /* synthetic */ class C5194a {

        /* renamed from: a */
        static final /* synthetic */ int[] f12509a = new int[ColumnDbType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                org.xutils.db.sqlite.ColumnDbType[] r0 = org.xutils.p274db.sqlite.ColumnDbType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.xutils.p274db.sqlite.SqlInfo.C5194a.f12509a = r0
                int[] r0 = org.xutils.p274db.sqlite.SqlInfo.C5194a.f12509a     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.xutils.db.sqlite.ColumnDbType r1 = org.xutils.p274db.sqlite.ColumnDbType.INTEGER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.xutils.p274db.sqlite.SqlInfo.C5194a.f12509a     // Catch:{ NoSuchFieldError -> 0x001f }
                org.xutils.db.sqlite.ColumnDbType r1 = org.xutils.p274db.sqlite.ColumnDbType.REAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.xutils.p274db.sqlite.SqlInfo.C5194a.f12509a     // Catch:{ NoSuchFieldError -> 0x002a }
                org.xutils.db.sqlite.ColumnDbType r1 = org.xutils.p274db.sqlite.ColumnDbType.TEXT     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = org.xutils.p274db.sqlite.SqlInfo.C5194a.f12509a     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.xutils.db.sqlite.ColumnDbType r1 = org.xutils.p274db.sqlite.ColumnDbType.BLOB     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.xutils.p274db.sqlite.SqlInfo.C5194a.<clinit>():void");
        }
    }

    public SqlInfo() {
    }

    public void addBindArg(KeyValue keyValue) {
        if (this.f12508b == null) {
            this.f12508b = new ArrayList();
        }
        this.f12508b.add(keyValue);
    }

    public void addBindArgs(List<KeyValue> list) {
        List<KeyValue> list2 = this.f12508b;
        if (list2 == null) {
            this.f12508b = list;
        } else {
            list2.addAll(list);
        }
    }

    public SQLiteStatement buildStatement(SQLiteDatabase sQLiteDatabase) {
        SQLiteStatement compileStatement = sQLiteDatabase.compileStatement(this.f12507a);
        if (this.f12508b != null) {
            for (int i = 1; i < this.f12508b.size() + 1; i++) {
                Object convert2DbValueIfNeeded = ColumnUtils.convert2DbValueIfNeeded(this.f12508b.get(i - 1).value);
                if (convert2DbValueIfNeeded == null) {
                    compileStatement.bindNull(i);
                } else {
                    int i2 = C5194a.f12509a[ColumnConverterFactory.getColumnConverter(convert2DbValueIfNeeded.getClass()).getColumnDbType().ordinal()];
                    if (i2 == 1) {
                        compileStatement.bindLong(i, ((Number) convert2DbValueIfNeeded).longValue());
                    } else if (i2 == 2) {
                        compileStatement.bindDouble(i, ((Number) convert2DbValueIfNeeded).doubleValue());
                    } else if (i2 == 3) {
                        compileStatement.bindString(i, convert2DbValueIfNeeded.toString());
                    } else if (i2 != 4) {
                        compileStatement.bindNull(i);
                    } else {
                        compileStatement.bindBlob(i, (byte[]) convert2DbValueIfNeeded);
                    }
                }
            }
        }
        return compileStatement;
    }

    public Object[] getBindArgs() {
        List<KeyValue> list = this.f12508b;
        if (list == null) {
            return null;
        }
        Object[] objArr = new Object[list.size()];
        for (int i = 0; i < this.f12508b.size(); i++) {
            objArr[i] = ColumnUtils.convert2DbValueIfNeeded(this.f12508b.get(i).value);
        }
        return objArr;
    }

    public String[] getBindArgsAsStrArray() {
        String str;
        List<KeyValue> list = this.f12508b;
        if (list == null) {
            return null;
        }
        String[] strArr = new String[list.size()];
        for (int i = 0; i < this.f12508b.size(); i++) {
            Object convert2DbValueIfNeeded = ColumnUtils.convert2DbValueIfNeeded(this.f12508b.get(i).value);
            if (convert2DbValueIfNeeded == null) {
                str = null;
            } else {
                str = convert2DbValueIfNeeded.toString();
            }
            strArr[i] = str;
        }
        return strArr;
    }

    public String getSql() {
        return this.f12507a;
    }

    public void setSql(String str) {
        this.f12507a = str;
    }

    public SqlInfo(String str) {
        this.f12507a = str;
    }
}
