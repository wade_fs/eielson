package org.xutils.p274db.table;

import android.database.Cursor;
import java.lang.reflect.Constructor;
import java.util.LinkedHashMap;
import org.xutils.DbManager;
import org.xutils.common.util.IOUtil;
import org.xutils.p274db.annotation.Table;

/* renamed from: org.xutils.db.table.TableEntity */
public final class TableEntity<T> {

    /* renamed from: a */
    private final DbManager f12521a;

    /* renamed from: b */
    private final String f12522b;

    /* renamed from: c */
    private final String f12523c;

    /* renamed from: d */
    private ColumnEntity f12524d;

    /* renamed from: e */
    private Class<T> f12525e;

    /* renamed from: f */
    private Constructor<T> f12526f;

    /* renamed from: g */
    private volatile boolean f12527g;

    /* renamed from: h */
    private final LinkedHashMap<String, ColumnEntity> f12528h;

    TableEntity(DbManager dbManager, Class<T> cls) {
        this.f12521a = dbManager;
        this.f12525e = cls;
        this.f12526f = cls.getConstructor(new Class[0]);
        this.f12526f.setAccessible(true);
        Table table = (Table) cls.getAnnotation(Table.class);
        this.f12522b = table.name();
        this.f12523c = table.onCreated();
        this.f12528h = TableUtils.m20159a(cls);
        for (ColumnEntity columnEntity : this.f12528h.values()) {
            if (columnEntity.isId()) {
                this.f12524d = columnEntity;
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo28718a() {
        return this.f12527g;
    }

    public T createEntity() {
        return this.f12526f.newInstance(new Object[0]);
    }

    public LinkedHashMap<String, ColumnEntity> getColumnMap() {
        return this.f12528h;
    }

    public DbManager getDb() {
        return this.f12521a;
    }

    public Class<T> getEntityType() {
        return this.f12525e;
    }

    public ColumnEntity getId() {
        return this.f12524d;
    }

    public String getName() {
        return this.f12522b;
    }

    public String getOnCreated() {
        return this.f12523c;
    }

    public boolean tableIsExist() {
        if (mo28718a()) {
            return true;
        }
        DbManager dbManager = this.f12521a;
        Cursor execQuery = dbManager.execQuery("SELECT COUNT(*) AS c FROM sqlite_master WHERE type='table' AND name='" + this.f12522b + "'");
        if (execQuery != null) {
            try {
                if (!execQuery.moveToNext() || execQuery.getInt(0) <= 0) {
                    IOUtil.closeQuietly(execQuery);
                } else {
                    mo28717a(true);
                    IOUtil.closeQuietly(execQuery);
                    return true;
                }
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return false;
    }

    public String toString() {
        return this.f12522b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28717a(boolean z) {
        this.f12527g = z;
    }
}
