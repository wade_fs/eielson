package org.xutils.p274db.converter;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.ColumnConverterFactory */
public final class ColumnConverterFactory {

    /* renamed from: a */
    private static final ConcurrentHashMap<String, ColumnConverter> f12504a = new ConcurrentHashMap<>();

    static {
        BooleanColumnConverter booleanColumnConverter = new BooleanColumnConverter();
        f12504a.put(Boolean.TYPE.getName(), booleanColumnConverter);
        f12504a.put(Boolean.class.getName(), booleanColumnConverter);
        f12504a.put(byte[].class.getName(), new ByteArrayColumnConverter());
        ByteColumnConverter byteColumnConverter = new ByteColumnConverter();
        f12504a.put(Byte.TYPE.getName(), byteColumnConverter);
        f12504a.put(Byte.class.getName(), byteColumnConverter);
        CharColumnConverter charColumnConverter = new CharColumnConverter();
        f12504a.put(Character.TYPE.getName(), charColumnConverter);
        f12504a.put(Character.class.getName(), charColumnConverter);
        f12504a.put(Date.class.getName(), new DateColumnConverter());
        DoubleColumnConverter doubleColumnConverter = new DoubleColumnConverter();
        f12504a.put(Double.TYPE.getName(), doubleColumnConverter);
        f12504a.put(Double.class.getName(), doubleColumnConverter);
        FloatColumnConverter floatColumnConverter = new FloatColumnConverter();
        f12504a.put(Float.TYPE.getName(), floatColumnConverter);
        f12504a.put(Float.class.getName(), floatColumnConverter);
        IntegerColumnConverter integerColumnConverter = new IntegerColumnConverter();
        f12504a.put(Integer.TYPE.getName(), integerColumnConverter);
        f12504a.put(Integer.class.getName(), integerColumnConverter);
        LongColumnConverter longColumnConverter = new LongColumnConverter();
        f12504a.put(Long.TYPE.getName(), longColumnConverter);
        f12504a.put(Long.class.getName(), longColumnConverter);
        ShortColumnConverter shortColumnConverter = new ShortColumnConverter();
        f12504a.put(Short.TYPE.getName(), shortColumnConverter);
        f12504a.put(Short.class.getName(), shortColumnConverter);
        f12504a.put(java.sql.Date.class.getName(), new SqlDateColumnConverter());
        f12504a.put(String.class.getName(), new StringColumnConverter());
    }

    private ColumnConverterFactory() {
    }

    public static ColumnConverter getColumnConverter(Class cls) {
        ColumnConverter columnConverter;
        if (f12504a.containsKey(cls.getName())) {
            columnConverter = f12504a.get(cls.getName());
        } else {
            if (ColumnConverter.class.isAssignableFrom(cls)) {
                try {
                    columnConverter = (ColumnConverter) cls.newInstance();
                    if (columnConverter != null) {
                        f12504a.put(cls.getName(), columnConverter);
                    }
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            }
            columnConverter = null;
        }
        if (columnConverter != null) {
            return columnConverter;
        }
        throw new RuntimeException("Database Column Not Support: " + cls.getName() + ", please impl ColumnConverter or use ColumnConverterFactory#registerColumnConverter(...)");
    }

    public static ColumnDbType getDbColumnType(Class cls) {
        return getColumnConverter(cls).getColumnDbType();
    }

    public static boolean isSupportColumnConverter(Class cls) {
        if (f12504a.containsKey(cls.getName())) {
            return true;
        }
        if (ColumnConverter.class.isAssignableFrom(cls)) {
            try {
                ColumnConverter columnConverter = (ColumnConverter) cls.newInstance();
                if (columnConverter != null) {
                    f12504a.put(cls.getName(), columnConverter);
                }
                if (columnConverter == null) {
                    return true;
                }
                return false;
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    public static void registerColumnConverter(Class cls, ColumnConverter columnConverter) {
        f12504a.put(cls.getName(), columnConverter);
    }
}
