package org.xutils.p274db;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import org.xutils.common.util.IOUtil;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p274db.table.DbModel;
import org.xutils.p274db.table.TableEntity;

/* renamed from: org.xutils.db.Selector */
public final class Selector<T> {

    /* renamed from: a */
    private final TableEntity<T> f12497a;

    /* renamed from: b */
    private WhereBuilder f12498b;

    /* renamed from: c */
    private List<OrderBy> f12499c;

    /* renamed from: d */
    private int f12500d = 0;

    /* renamed from: e */
    private int f12501e = 0;

    private Selector(TableEntity<T> tableEntity) {
        this.f12497a = tableEntity;
    }

    /* renamed from: a */
    static <T> Selector<T> m20142a(TableEntity<T> tableEntity) {
        return new Selector<>(tableEntity);
    }

    public Selector<T> and(String str, String str2, Object obj) {
        this.f12498b.and(str, str2, obj);
        return this;
    }

    public long count() {
        if (!this.f12497a.tableIsExist()) {
            return 0;
        }
        DbModel findFirst = select("count(\"" + this.f12497a.getId().getName() + "\") as count").findFirst();
        if (findFirst != null) {
            return findFirst.getLong("count");
        }
        return 0;
    }

    public Selector<T> expr(String str) {
        if (this.f12498b == null) {
            this.f12498b = WhereBuilder.m20149b();
        }
        this.f12498b.expr(str);
        return this;
    }

    public List<T> findAll() {
        ArrayList arrayList = null;
        if (!this.f12497a.tableIsExist()) {
            return null;
        }
        Cursor execQuery = this.f12497a.getDb().execQuery(toString());
        if (execQuery != null) {
            try {
                arrayList = new ArrayList();
                while (execQuery.moveToNext()) {
                    arrayList.add(CursorUtils.m20145a(this.f12497a, execQuery));
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return arrayList;
    }

    public T findFirst() {
        if (!this.f12497a.tableIsExist()) {
            return null;
        }
        limit(1);
        Cursor execQuery = this.f12497a.getDb().execQuery(toString());
        if (execQuery != null) {
            try {
                if (execQuery.moveToNext()) {
                    T a = CursorUtils.m20145a(this.f12497a, execQuery);
                    IOUtil.closeQuietly(execQuery);
                    return a;
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return null;
    }

    public int getLimit() {
        return this.f12500d;
    }

    public int getOffset() {
        return this.f12501e;
    }

    public List<OrderBy> getOrderByList() {
        return this.f12499c;
    }

    public TableEntity<T> getTable() {
        return this.f12497a;
    }

    public WhereBuilder getWhereBuilder() {
        return this.f12498b;
    }

    public DbModelSelector groupBy(String str) {
        return new DbModelSelector(this, str);
    }

    public Selector<T> limit(int i) {
        this.f12500d = i;
        return this;
    }

    public Selector<T> offset(int i) {
        this.f12501e = i;
        return this;
    }

    /* renamed from: or */
    public Selector<T> mo28647or(String str, String str2, Object obj) {
        this.f12498b.mo28689or(str, str2, obj);
        return this;
    }

    public Selector<T> orderBy(String str) {
        if (this.f12499c == null) {
            this.f12499c = new ArrayList(5);
        }
        this.f12499c.add(new OrderBy(str));
        return this;
    }

    public DbModelSelector select(String... strArr) {
        return new DbModelSelector(this, strArr);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("*");
        sb.append(" FROM ");
        sb.append("\"");
        sb.append(this.f12497a.getName());
        sb.append("\"");
        WhereBuilder whereBuilder = this.f12498b;
        if (whereBuilder != null && whereBuilder.getWhereItemSize() > 0) {
            sb.append(" WHERE ");
            sb.append(this.f12498b.toString());
        }
        List<OrderBy> list = this.f12499c;
        if (list != null && list.size() > 0) {
            sb.append(" ORDER BY ");
            for (OrderBy orderBy : this.f12499c) {
                sb.append(orderBy.toString());
                sb.append(',');
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        if (this.f12500d > 0) {
            sb.append(" LIMIT ");
            sb.append(this.f12500d);
            sb.append(" OFFSET ");
            sb.append(this.f12501e);
        }
        return sb.toString();
    }

    public Selector<T> where(WhereBuilder whereBuilder) {
        this.f12498b = whereBuilder;
        return this;
    }

    /* renamed from: org.xutils.db.Selector$OrderBy */
    public static class OrderBy {

        /* renamed from: a */
        private String f12502a;

        /* renamed from: b */
        private boolean f12503b;

        public OrderBy(String str) {
            this.f12502a = str;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("\"");
            sb.append(this.f12502a);
            sb.append("\"");
            sb.append(this.f12503b ? " DESC" : " ASC");
            return sb.toString();
        }

        public OrderBy(String str, boolean z) {
            this.f12502a = str;
            this.f12503b = z;
        }
    }

    public Selector<T> and(WhereBuilder whereBuilder) {
        this.f12498b.and(whereBuilder);
        return this;
    }

    /* renamed from: or */
    public Selector mo28648or(WhereBuilder whereBuilder) {
        this.f12498b.mo28690or(whereBuilder);
        return this;
    }

    public Selector<T> where(String str, String str2, Object obj) {
        this.f12498b = WhereBuilder.m20150b(str, str2, obj);
        return this;
    }

    public Selector<T> orderBy(String str, boolean z) {
        if (this.f12499c == null) {
            this.f12499c = new ArrayList(5);
        }
        this.f12499c.add(new OrderBy(str, z));
        return this;
    }
}
