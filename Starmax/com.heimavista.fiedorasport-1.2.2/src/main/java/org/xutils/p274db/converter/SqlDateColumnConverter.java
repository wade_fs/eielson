package org.xutils.p274db.converter;

import android.database.Cursor;
import java.sql.Date;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.SqlDateColumnConverter */
public class SqlDateColumnConverter implements ColumnConverter<Date> {
    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }

    public Object fieldValue2DbValue(Date date) {
        if (date == null) {
            return null;
        }
        return Long.valueOf(date.getTime());
    }

    public Date getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return new Date(cursor.getLong(i));
    }
}
