package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.DoubleColumnConverter */
public class DoubleColumnConverter implements ColumnConverter<Double> {
    public Object fieldValue2DbValue(Double d) {
        return d;
    }

    public ColumnDbType getColumnDbType() {
        return ColumnDbType.REAL;
    }

    public Double getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return Double.valueOf(cursor.getDouble(i));
    }
}
