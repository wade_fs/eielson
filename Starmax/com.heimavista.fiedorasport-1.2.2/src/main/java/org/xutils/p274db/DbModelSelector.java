package org.xutils.p274db;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import org.xutils.common.util.IOUtil;
import org.xutils.p274db.Selector;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p274db.table.DbModel;
import org.xutils.p274db.table.TableEntity;

/* renamed from: org.xutils.db.DbModelSelector */
public final class DbModelSelector {

    /* renamed from: a */
    private String[] f12493a;

    /* renamed from: b */
    private String f12494b;

    /* renamed from: c */
    private WhereBuilder f12495c;

    /* renamed from: d */
    private Selector<?> f12496d;

    protected DbModelSelector(Selector<?> selector, String str) {
        this.f12496d = selector;
        this.f12494b = str;
    }

    public DbModelSelector and(String str, String str2, Object obj) {
        this.f12496d.and(str, str2, obj);
        return this;
    }

    public DbModelSelector expr(String str) {
        this.f12496d.expr(str);
        return this;
    }

    public List<DbModel> findAll() {
        TableEntity<?> table = this.f12496d.getTable();
        ArrayList arrayList = null;
        if (!table.tableIsExist()) {
            return null;
        }
        Cursor execQuery = table.getDb().execQuery(toString());
        if (execQuery != null) {
            try {
                arrayList = new ArrayList();
                while (execQuery.moveToNext()) {
                    arrayList.add(CursorUtils.m20146a(execQuery));
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return arrayList;
    }

    public DbModel findFirst() {
        TableEntity<?> table = this.f12496d.getTable();
        if (!table.tableIsExist()) {
            return null;
        }
        limit(1);
        Cursor execQuery = table.getDb().execQuery(toString());
        if (execQuery != null) {
            try {
                if (execQuery.moveToNext()) {
                    DbModel a = CursorUtils.m20146a(execQuery);
                    IOUtil.closeQuietly(execQuery);
                    return a;
                }
                IOUtil.closeQuietly(execQuery);
            } catch (Throwable th) {
                IOUtil.closeQuietly(execQuery);
                throw th;
            }
        }
        return null;
    }

    public TableEntity<?> getTable() {
        return this.f12496d.getTable();
    }

    public DbModelSelector groupBy(String str) {
        this.f12494b = str;
        return this;
    }

    public DbModelSelector having(WhereBuilder whereBuilder) {
        this.f12495c = whereBuilder;
        return this;
    }

    public DbModelSelector limit(int i) {
        this.f12496d.limit(i);
        return this;
    }

    public DbModelSelector offset(int i) {
        this.f12496d.offset(i);
        return this;
    }

    /* renamed from: or */
    public DbModelSelector mo28625or(String str, String str2, Object obj) {
        this.f12496d.mo28647or(str, str2, obj);
        return this;
    }

    public DbModelSelector orderBy(String str) {
        this.f12496d.orderBy(str);
        return this;
    }

    public DbModelSelector select(String... strArr) {
        this.f12493a = strArr;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        String[] strArr = this.f12493a;
        if (strArr != null && strArr.length > 0) {
            for (String str : strArr) {
                sb.append(str);
                sb.append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
        } else if (!TextUtils.isEmpty(this.f12494b)) {
            sb.append(this.f12494b);
        } else {
            sb.append("*");
        }
        sb.append(" FROM ");
        sb.append("\"");
        sb.append(this.f12496d.getTable().getName());
        sb.append("\"");
        WhereBuilder whereBuilder = this.f12496d.getWhereBuilder();
        if (whereBuilder != null && whereBuilder.getWhereItemSize() > 0) {
            sb.append(" WHERE ");
            sb.append(whereBuilder.toString());
        }
        if (!TextUtils.isEmpty(this.f12494b)) {
            sb.append(" GROUP BY ");
            sb.append("\"");
            sb.append(this.f12494b);
            sb.append("\"");
            WhereBuilder whereBuilder2 = this.f12495c;
            if (whereBuilder2 != null && whereBuilder2.getWhereItemSize() > 0) {
                sb.append(" HAVING ");
                sb.append(this.f12495c.toString());
            }
        }
        List<Selector.OrderBy> orderByList = this.f12496d.getOrderByList();
        if (orderByList != null && orderByList.size() > 0) {
            for (int i = 0; i < orderByList.size(); i++) {
                sb.append(" ORDER BY ");
                sb.append(orderByList.get(i).toString());
                sb.append(',');
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        if (this.f12496d.getLimit() > 0) {
            sb.append(" LIMIT ");
            sb.append(this.f12496d.getLimit());
            sb.append(" OFFSET ");
            sb.append(this.f12496d.getOffset());
        }
        return sb.toString();
    }

    public DbModelSelector where(WhereBuilder whereBuilder) {
        this.f12496d.where(whereBuilder);
        return this;
    }

    public DbModelSelector and(WhereBuilder whereBuilder) {
        this.f12496d.and(whereBuilder);
        return this;
    }

    /* renamed from: or */
    public DbModelSelector mo28626or(WhereBuilder whereBuilder) {
        this.f12496d.mo28648or(whereBuilder);
        return this;
    }

    public DbModelSelector orderBy(String str, boolean z) {
        this.f12496d.orderBy(str, z);
        return this;
    }

    public DbModelSelector where(String str, String str2, Object obj) {
        this.f12496d.where(str, str2, obj);
        return this;
    }

    protected DbModelSelector(Selector<?> selector, String[] strArr) {
        this.f12496d = selector;
        this.f12493a = strArr;
    }
}
