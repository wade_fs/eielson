package org.xutils.p274db.sqlite;

/* renamed from: org.xutils.db.sqlite.ColumnDbType */
public enum ColumnDbType {
    INTEGER("INTEGER"),
    REAL("REAL"),
    TEXT("TEXT"),
    BLOB("BLOB");
    

    /* renamed from: P */
    private String f12506P;

    private ColumnDbType(String str) {
        this.f12506P = str;
    }

    public String toString() {
        return this.f12506P;
    }
}
