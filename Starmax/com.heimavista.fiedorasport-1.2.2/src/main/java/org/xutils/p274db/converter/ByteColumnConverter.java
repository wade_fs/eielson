package org.xutils.p274db.converter;

import android.database.Cursor;
import org.xutils.p274db.sqlite.ColumnDbType;

/* renamed from: org.xutils.db.converter.ByteColumnConverter */
public class ByteColumnConverter implements ColumnConverter<Byte> {
    public Object fieldValue2DbValue(Byte b) {
        return b;
    }

    public ColumnDbType getColumnDbType() {
        return ColumnDbType.INTEGER;
    }

    public Byte getFieldValue(Cursor cursor, int i) {
        if (cursor.isNull(i)) {
            return null;
        }
        return Byte.valueOf((byte) cursor.getInt(i));
    }
}
