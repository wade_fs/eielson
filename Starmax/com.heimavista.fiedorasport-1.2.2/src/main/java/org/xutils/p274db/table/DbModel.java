package org.xutils.p274db.table;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.util.Date;
import java.util.HashMap;

/* renamed from: org.xutils.db.table.DbModel */
public final class DbModel {

    /* renamed from: a */
    private HashMap<String, String> f12520a = new HashMap<>();

    public void add(String str, String str2) {
        this.f12520a.put(str, str2);
    }

    public boolean getBoolean(String str) {
        String str2 = this.f12520a.get(str);
        if (str2 != null) {
            return str2.length() == 1 ? AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(str2) : Boolean.valueOf(str2).booleanValue();
        }
        return false;
    }

    public HashMap<String, String> getDataMap() {
        return this.f12520a;
    }

    public Date getDate(String str) {
        return new Date(Long.valueOf(this.f12520a.get(str)).longValue());
    }

    public double getDouble(String str) {
        return Double.valueOf(this.f12520a.get(str)).doubleValue();
    }

    public float getFloat(String str) {
        return Float.valueOf(this.f12520a.get(str)).floatValue();
    }

    public int getInt(String str) {
        return Integer.valueOf(this.f12520a.get(str)).intValue();
    }

    public long getLong(String str) {
        return Long.valueOf(this.f12520a.get(str)).longValue();
    }

    public java.sql.Date getSqlDate(String str) {
        return new java.sql.Date(Long.valueOf(this.f12520a.get(str)).longValue());
    }

    public String getString(String str) {
        return this.f12520a.get(str);
    }

    public boolean isEmpty(String str) {
        return TextUtils.isEmpty(this.f12520a.get(str));
    }
}
