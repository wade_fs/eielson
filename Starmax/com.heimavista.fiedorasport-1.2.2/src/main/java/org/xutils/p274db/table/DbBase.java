package org.xutils.p274db.table;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.HashMap;
import org.xutils.DbManager;
import org.xutils.common.util.IOUtil;
import org.xutils.p274db.sqlite.SqlInfoBuilder;
import org.xutils.p275ex.DbException;

/* renamed from: org.xutils.db.table.DbBase */
public abstract class DbBase implements DbManager {

    /* renamed from: P */
    private final HashMap<Class<?>, TableEntity<?>> f12519P = new HashMap<>();

    public void addColumn(Class<?> cls, String str) {
        TableEntity table = getTable(cls);
        ColumnEntity columnEntity = table.getColumnMap().get(str);
        if (columnEntity != null) {
            execNonQuery("ALTER TABLE " + "\"" + table.getName() + "\"" + " ADD COLUMN " + "\"" + columnEntity.getName() + "\"" + " " + columnEntity.getColumnDbType() + " " + columnEntity.getProperty());
        }
    }

    /* access modifiers changed from: protected */
    public void createTableIfNotExist(TableEntity<?> tableEntity) {
        if (!tableEntity.tableIsExist()) {
            synchronized (tableEntity.getClass()) {
                if (!tableEntity.tableIsExist()) {
                    execNonQuery(SqlInfoBuilder.buildCreateTableSqlInfo(tableEntity));
                    String onCreated = tableEntity.getOnCreated();
                    if (!TextUtils.isEmpty(onCreated)) {
                        execNonQuery(onCreated);
                    }
                    tableEntity.mo28717a(true);
                    DbManager.TableCreateListener tableCreateListener = getDaoConfig().getTableCreateListener();
                    if (tableCreateListener != null) {
                        tableCreateListener.onTableCreated(this, tableEntity);
                    }
                }
            }
        }
    }

    public void dropDb() {
        Cursor execQuery = execQuery("SELECT name FROM sqlite_master WHERE type='table' AND name<>'sqlite_sequence'");
        if (execQuery != null) {
            while (execQuery.moveToNext()) {
                try {
                    String string = execQuery.getString(0);
                    execNonQuery("DROP TABLE " + string);
                } catch (Throwable th) {
                    try {
                        throw new DbException(th);
                    } catch (Throwable th2) {
                        IOUtil.closeQuietly(execQuery);
                        throw th2;
                    }
                }
            }
            synchronized (this.f12519P) {
                for (TableEntity<?> tableEntity : this.f12519P.values()) {
                    tableEntity.mo28717a(false);
                }
                this.f12519P.clear();
            }
            IOUtil.closeQuietly(execQuery);
        }
    }

    public void dropTable(Class<?> cls) {
        TableEntity table = getTable(cls);
        if (table.tableIsExist()) {
            execNonQuery("DROP TABLE \"" + table.getName() + "\"");
            table.mo28717a(false);
            removeTable(cls);
        }
    }

    public <T> TableEntity<T> getTable(Class<T> cls) {
        TableEntity<T> tableEntity;
        synchronized (this.f12519P) {
            tableEntity = this.f12519P.get(cls);
            if (tableEntity == null) {
                try {
                    tableEntity = new TableEntity<>(this, cls);
                    this.f12519P.put(cls, tableEntity);
                } catch (Throwable th) {
                    throw new DbException(th);
                }
            }
        }
        return tableEntity;
    }

    /* access modifiers changed from: protected */
    public void removeTable(Class<?> cls) {
        synchronized (this.f12519P) {
            this.f12519P.remove(cls);
        }
    }
}
