package org.xutils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.util.List;
import org.xutils.common.util.KeyValue;
import org.xutils.p274db.Selector;
import org.xutils.p274db.sqlite.SqlInfo;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p274db.table.DbModel;
import org.xutils.p274db.table.TableEntity;

public interface DbManager extends Closeable {

    public static class DaoConfig {

        /* renamed from: a */
        private File f12403a;

        /* renamed from: b */
        private String f12404b = "xUtils.db";

        /* renamed from: c */
        private int f12405c = 1;

        /* renamed from: d */
        private boolean f12406d = true;

        /* renamed from: e */
        private DbUpgradeListener f12407e;

        /* renamed from: f */
        private TableCreateListener f12408f;

        /* renamed from: g */
        private DbOpenListener f12409g;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || DaoConfig.class != obj.getClass()) {
                return false;
            }
            DaoConfig daoConfig = (DaoConfig) obj;
            if (!this.f12404b.equals(daoConfig.f12404b)) {
                return false;
            }
            File file = this.f12403a;
            File file2 = daoConfig.f12403a;
            if (file != null) {
                return file.equals(file2);
            }
            if (file2 == null) {
                return true;
            }
            return false;
        }

        public File getDbDir() {
            return this.f12403a;
        }

        public String getDbName() {
            return this.f12404b;
        }

        public DbOpenListener getDbOpenListener() {
            return this.f12409g;
        }

        public DbUpgradeListener getDbUpgradeListener() {
            return this.f12407e;
        }

        public int getDbVersion() {
            return this.f12405c;
        }

        public TableCreateListener getTableCreateListener() {
            return this.f12408f;
        }

        public int hashCode() {
            int hashCode = this.f12404b.hashCode() * 31;
            File file = this.f12403a;
            return hashCode + (file != null ? file.hashCode() : 0);
        }

        public boolean isAllowTransaction() {
            return this.f12406d;
        }

        public DaoConfig setAllowTransaction(boolean z) {
            this.f12406d = z;
            return this;
        }

        public DaoConfig setDbDir(File file) {
            this.f12403a = file;
            return this;
        }

        public DaoConfig setDbName(String str) {
            if (!TextUtils.isEmpty(str)) {
                this.f12404b = str;
            }
            return this;
        }

        public DaoConfig setDbOpenListener(DbOpenListener dbOpenListener) {
            this.f12409g = dbOpenListener;
            return this;
        }

        public DaoConfig setDbUpgradeListener(DbUpgradeListener dbUpgradeListener) {
            this.f12407e = dbUpgradeListener;
            return this;
        }

        public DaoConfig setDbVersion(int i) {
            this.f12405c = i;
            return this;
        }

        public DaoConfig setTableCreateListener(TableCreateListener tableCreateListener) {
            this.f12408f = tableCreateListener;
            return this;
        }

        public String toString() {
            return String.valueOf(this.f12403a) + "/" + this.f12404b;
        }
    }

    public interface DbOpenListener {
        void onDbOpened(DbManager dbManager);
    }

    public interface DbUpgradeListener {
        void onUpgrade(DbManager dbManager, int i, int i2);
    }

    public interface TableCreateListener {
        void onTableCreated(DbManager dbManager, TableEntity<?> tableEntity);
    }

    void addColumn(Class<?> cls, String str);

    void close();

    int delete(Class<?> cls, WhereBuilder whereBuilder);

    void delete(Class<?> cls);

    void delete(Object obj);

    void deleteById(Class<?> cls, Object obj);

    void dropDb();

    void dropTable(Class<?> cls);

    void execNonQuery(String str);

    void execNonQuery(SqlInfo sqlInfo);

    Cursor execQuery(String str);

    Cursor execQuery(SqlInfo sqlInfo);

    int executeUpdateDelete(String str);

    int executeUpdateDelete(SqlInfo sqlInfo);

    <T> List<T> findAll(Class<T> cls);

    <T> T findById(Class<T> cls, Object obj);

    List<DbModel> findDbModelAll(SqlInfo sqlInfo);

    DbModel findDbModelFirst(SqlInfo sqlInfo);

    <T> T findFirst(Class<T> cls);

    DaoConfig getDaoConfig();

    SQLiteDatabase getDatabase();

    <T> TableEntity<T> getTable(Class<T> cls);

    void replace(Object obj);

    void save(Object obj);

    boolean saveBindingId(Object obj);

    void saveOrUpdate(Object obj);

    <T> Selector<T> selector(Class<T> cls);

    int update(Class<?> cls, WhereBuilder whereBuilder, KeyValue... keyValueArr);

    void update(Object obj, String... strArr);
}
