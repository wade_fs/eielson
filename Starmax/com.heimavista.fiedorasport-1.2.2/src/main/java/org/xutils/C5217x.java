package org.xutils;

import android.app.Application;
import android.content.Context;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import org.xutils.DbManager;
import org.xutils.common.TaskController;
import org.xutils.common.task.TaskControllerImpl;
import org.xutils.http.HttpManagerImpl;
import org.xutils.image.ImageManagerImpl;
import org.xutils.p274db.DbManagerImpl;
import org.xutils.view.ViewInjectorImpl;

/* renamed from: org.xutils.x */
public final class C5217x {

    /* renamed from: org.xutils.x$Ext */
    public static class Ext {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static boolean f12766a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public static Application f12767b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public static TaskController f12768c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public static HttpManager f12769d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public static ImageManager f12770e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public static ViewInjector f12771f;

        private Ext() {
        }

        public static void init(Application application) {
            TaskControllerImpl.registerInstance();
            if (f12767b == null) {
                f12767b = application;
            }
        }

        public static void setDebug(boolean z) {
            f12766a = z;
        }

        public static void setDefaultHostnameVerifier(HostnameVerifier hostnameVerifier) {
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        }

        public static void setHttpManager(HttpManager httpManager) {
            f12769d = httpManager;
        }

        public static void setImageManager(ImageManager imageManager) {
            f12770e = imageManager;
        }

        public static void setTaskController(TaskController taskController) {
            if (f12768c == null) {
                f12768c = taskController;
            }
        }

        public static void setViewInjector(ViewInjector viewInjector) {
            f12771f = viewInjector;
        }
    }

    /* renamed from: org.xutils.x$a */
    private static class C5218a extends Application {
        public C5218a(Context context) {
            attachBaseContext(context);
        }
    }

    private C5217x() {
    }

    public static Application app() {
        if (Ext.f12767b == null) {
            try {
                Application unused = Ext.f12767b = new C5218a((Context) Class.forName("com.android.layoutlib.bridge.impl.RenderAction").getDeclaredMethod("getCurrentContext", new Class[0]).invoke(null, new Object[0]));
            } catch (Throwable unused2) {
                throw new RuntimeException("please invoke x.Ext.init(app) on Application#onCreate() and register your Application in manifest.");
            }
        }
        return Ext.f12767b;
    }

    public static DbManager getDb(DbManager.DaoConfig daoConfig) {
        return DbManagerImpl.getInstance(daoConfig);
    }

    public static HttpManager http() {
        if (Ext.f12769d == null) {
            HttpManagerImpl.registerInstance();
        }
        return Ext.f12769d;
    }

    public static ImageManager image() {
        if (Ext.f12770e == null) {
            ImageManagerImpl.registerInstance();
        }
        return Ext.f12770e;
    }

    public static boolean isDebug() {
        return Ext.f12766a;
    }

    public static TaskController task() {
        return Ext.f12768c;
    }

    public static ViewInjector view() {
        if (Ext.f12771f == null) {
            ViewInjectorImpl.registerInstance();
        }
        return Ext.f12771f;
    }
}
