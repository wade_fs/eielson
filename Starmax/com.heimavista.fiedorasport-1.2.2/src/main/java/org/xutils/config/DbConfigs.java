package org.xutils.config;

import org.xutils.DbManager;
import org.xutils.common.util.LogUtil;
import org.xutils.p275ex.DbException;

public enum DbConfigs {
    HTTP(new DbManager.DaoConfig().setDbName("xUtils_http_cache.db").setDbVersion(1).setDbOpenListener(new C5191b()).setDbUpgradeListener(new C5190a())),
    COOKIE(new DbManager.DaoConfig().setDbName("xUtils_http_cookie.db").setDbVersion(1).setDbOpenListener(new C5193d()).setDbUpgradeListener(new C5192c()));
    

    /* renamed from: P */
    private DbManager.DaoConfig f12488P;

    /* renamed from: org.xutils.config.DbConfigs$a */
    static class C5190a implements DbManager.DbUpgradeListener {
        C5190a() {
        }

        public void onUpgrade(DbManager dbManager, int i, int i2) {
            try {
                dbManager.dropDb();
            } catch (DbException e) {
                LogUtil.m20120e(e.getMessage(), e);
            }
        }
    }

    /* renamed from: org.xutils.config.DbConfigs$b */
    static class C5191b implements DbManager.DbOpenListener {
        C5191b() {
        }

        public void onDbOpened(DbManager dbManager) {
            dbManager.getDatabase().enableWriteAheadLogging();
        }
    }

    /* renamed from: org.xutils.config.DbConfigs$c */
    static class C5192c implements DbManager.DbUpgradeListener {
        C5192c() {
        }

        public void onUpgrade(DbManager dbManager, int i, int i2) {
            try {
                dbManager.dropDb();
            } catch (DbException e) {
                LogUtil.m20120e(e.getMessage(), e);
            }
        }
    }

    /* renamed from: org.xutils.config.DbConfigs$d */
    static class C5193d implements DbManager.DbOpenListener {
        C5193d() {
        }

        public void onDbOpened(DbManager dbManager) {
            dbManager.getDatabase().enableWriteAheadLogging();
        }
    }

    private DbConfigs(DbManager.DaoConfig daoConfig) {
        this.f12488P = daoConfig;
    }

    public DbManager.DaoConfig getConfig() {
        return this.f12488P;
    }
}
