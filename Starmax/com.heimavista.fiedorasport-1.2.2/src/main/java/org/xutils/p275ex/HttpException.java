package org.xutils.p275ex;

import android.text.TextUtils;

/* renamed from: org.xutils.ex.HttpException */
public class HttpException extends BaseException {
    private static final long serialVersionUID = 1;

    /* renamed from: P */
    private int f12529P;

    /* renamed from: Q */
    private String f12530Q;

    /* renamed from: R */
    private String f12531R;

    /* renamed from: S */
    private String f12532S;

    public HttpException(int i, String str) {
        super(str);
        this.f12529P = i;
    }

    public int getCode() {
        return this.f12529P;
    }

    public String getErrorCode() {
        String str = this.f12530Q;
        return str == null ? String.valueOf(this.f12529P) : str;
    }

    public String getMessage() {
        if (!TextUtils.isEmpty(this.f12531R)) {
            return this.f12531R;
        }
        return super.getMessage();
    }

    public String getResult() {
        return this.f12532S;
    }

    public void setCode(int i) {
        this.f12529P = i;
    }

    public void setErrorCode(String str) {
        this.f12530Q = str;
    }

    public void setMessage(String str) {
        this.f12531R = str;
    }

    public void setResult(String str) {
        this.f12532S = str;
    }

    public String toString() {
        return "errorCode: " + getErrorCode() + ", msg: " + getMessage() + ", result: " + this.f12532S;
    }
}
