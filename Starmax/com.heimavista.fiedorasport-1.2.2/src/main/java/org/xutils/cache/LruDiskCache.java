package org.xutils.cache;

import android.text.TextUtils;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import org.xutils.C5217x;
import org.xutils.DbManager;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.FileUtil;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.common.util.MD5;
import org.xutils.common.util.ProcessLock;
import org.xutils.config.DbConfigs;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p275ex.DbException;
import org.xutils.p275ex.FileLockedException;

public final class LruDiskCache {

    /* renamed from: g */
    private static final HashMap<String, LruDiskCache> f12421g = new HashMap<>(5);
    /* access modifiers changed from: private */

    /* renamed from: a */
    public boolean f12422a = false;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final DbManager f12423b = C5217x.getDb(DbConfigs.HTTP.getConfig());
    /* access modifiers changed from: private */

    /* renamed from: c */
    public File f12424c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public long f12425d = 104857600;

    /* renamed from: e */
    private final Executor f12426e = new PriorityExecutor(1, true);
    /* access modifiers changed from: private */

    /* renamed from: f */
    public long f12427f = 0;

    /* renamed from: org.xutils.cache.LruDiskCache$a */
    class C5174a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ DiskCacheEntity f12428P;

        C5174a(DiskCacheEntity diskCacheEntity) {
            this.f12428P = diskCacheEntity;
        }

        public void run() {
            DiskCacheEntity diskCacheEntity = this.f12428P;
            diskCacheEntity.setHits(diskCacheEntity.getHits() + 1);
            this.f12428P.setLastAccess(System.currentTimeMillis());
            try {
                LruDiskCache.this.f12423b.update(this.f12428P, "hits", "lastAccess");
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
    }

    /* renamed from: org.xutils.cache.LruDiskCache$b */
    class C5175b implements Runnable {
        C5175b() {
        }

        public void run() {
            List<DiskCacheEntity> findAll;
            Class<DiskCacheEntity> cls = DiskCacheEntity.class;
            if (LruDiskCache.this.f12422a) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - LruDiskCache.this.f12427f >= 1000) {
                    long unused = LruDiskCache.this.f12427f = currentTimeMillis;
                    LruDiskCache.this.m20094a();
                    try {
                        int count = (int) LruDiskCache.this.f12423b.selector(cls).count();
                        if (count > 5010 && (findAll = LruDiskCache.this.f12423b.selector(cls).orderBy("lastAccess").orderBy("hits").limit(count - 5000).offset(0).findAll()) != null && findAll.size() > 0) {
                            for (DiskCacheEntity diskCacheEntity : findAll) {
                                try {
                                    LruDiskCache.this.f12423b.delete(diskCacheEntity);
                                    String path = diskCacheEntity.getPath();
                                    if (!TextUtils.isEmpty(path)) {
                                        boolean unused2 = LruDiskCache.this.m20095a(path);
                                        LruDiskCache lruDiskCache = LruDiskCache.this;
                                        boolean unused3 = lruDiskCache.m20095a(path + ".tmp");
                                    }
                                } catch (DbException e) {
                                    LogUtil.m20120e(e.getMessage(), e);
                                }
                            }
                        }
                    } catch (DbException e2) {
                        LogUtil.m20120e(e2.getMessage(), e2);
                    }
                    while (FileUtil.getFileOrDirSize(LruDiskCache.this.f12424c) > LruDiskCache.this.f12425d) {
                        try {
                            List<DiskCacheEntity> findAll2 = LruDiskCache.this.f12423b.selector(cls).orderBy("lastAccess").orderBy("hits").limit(10).offset(0).findAll();
                            if (findAll2 != null && findAll2.size() > 0) {
                                for (DiskCacheEntity diskCacheEntity2 : findAll2) {
                                    try {
                                        LruDiskCache.this.f12423b.delete(diskCacheEntity2);
                                        String path2 = diskCacheEntity2.getPath();
                                        if (!TextUtils.isEmpty(path2)) {
                                            boolean unused4 = LruDiskCache.this.m20095a(path2);
                                            LruDiskCache lruDiskCache2 = LruDiskCache.this;
                                            boolean unused5 = lruDiskCache2.m20095a(path2 + ".tmp");
                                        }
                                    } catch (DbException e3) {
                                        LogUtil.m20120e(e3.getMessage(), e3);
                                    }
                                }
                            }
                        } catch (DbException e4) {
                            LogUtil.m20120e(e4.getMessage(), e4);
                            return;
                        }
                    }
                }
            }
        }
    }

    /* renamed from: org.xutils.cache.LruDiskCache$c */
    class C5176c implements Runnable {
        C5176c() {
        }

        public void run() {
            if (LruDiskCache.this.f12422a) {
                try {
                    File[] listFiles = LruDiskCache.this.f12424c.listFiles();
                    if (listFiles != null) {
                        for (File file : listFiles) {
                            if (LruDiskCache.this.f12423b.selector(DiskCacheEntity.class).where("path", "=", file.getAbsolutePath()).count() < 1) {
                                IOUtil.deleteFileOrDir(file);
                            }
                        }
                    }
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
            }
        }
    }

    private LruDiskCache(String str) {
        this.f12424c = FileUtil.getCacheDir(str);
        File file = this.f12424c;
        if (file != null && (file.exists() || this.f12424c.mkdirs())) {
            this.f12422a = true;
        }
        m20097b();
    }

    public static synchronized LruDiskCache getDiskCache(String str) {
        LruDiskCache lruDiskCache;
        synchronized (LruDiskCache.class) {
            if (TextUtils.isEmpty(str)) {
                str = "xUtils_cache";
            }
            lruDiskCache = f12421g.get(str);
            if (lruDiskCache == null) {
                lruDiskCache = new LruDiskCache(str);
                f12421g.put(str, lruDiskCache);
            }
        }
        return lruDiskCache;
    }

    public void clearCacheFiles() {
        IOUtil.deleteFileOrDir(this.f12424c);
    }

    public DiskCacheFile createDiskCacheFile(DiskCacheEntity diskCacheEntity) {
        if (!this.f12422a || diskCacheEntity == null) {
            return null;
        }
        diskCacheEntity.setPath(new File(this.f12424c, MD5.md5(diskCacheEntity.getKey())).getAbsolutePath());
        String str = diskCacheEntity.getPath() + ".tmp";
        ProcessLock tryLock = ProcessLock.tryLock(str, true);
        if (tryLock == null || !tryLock.isValid()) {
            throw new FileLockedException(diskCacheEntity.getPath());
        }
        DiskCacheFile diskCacheFile = new DiskCacheFile(diskCacheEntity, str, tryLock);
        if (!diskCacheFile.getParentFile().exists()) {
            diskCacheFile.mkdirs();
        }
        return diskCacheFile;
    }

    public DiskCacheEntity get(String str) {
        DiskCacheEntity diskCacheEntity;
        if (!this.f12422a || TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            diskCacheEntity = (DiskCacheEntity) this.f12423b.selector(DiskCacheEntity.class).where("key", "=", str).findFirst();
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            diskCacheEntity = null;
        }
        if (diskCacheEntity != null) {
            if (diskCacheEntity.getExpires() < System.currentTimeMillis()) {
                return null;
            }
            this.f12426e.execute(new C5174a(diskCacheEntity));
        }
        return diskCacheEntity;
    }

    public DiskCacheFile getDiskCacheFile(String str) {
        DiskCacheEntity diskCacheEntity;
        ProcessLock tryLock;
        if (!this.f12422a || TextUtils.isEmpty(str) || (diskCacheEntity = get(str)) == null || !new File(diskCacheEntity.getPath()).exists() || (tryLock = ProcessLock.tryLock(diskCacheEntity.getPath(), false, 3000)) == null || !tryLock.isValid()) {
            return null;
        }
        DiskCacheFile diskCacheFile = new DiskCacheFile(diskCacheEntity, diskCacheEntity.getPath(), tryLock);
        if (diskCacheFile.exists()) {
            return diskCacheFile;
        }
        try {
            this.f12423b.delete(diskCacheEntity);
            return null;
        } catch (DbException e) {
            LogUtil.m20120e(e.getMessage(), e);
            return null;
        }
    }

    public void put(DiskCacheEntity diskCacheEntity) {
        if (this.f12422a && diskCacheEntity != null && !TextUtils.isEmpty(diskCacheEntity.getTextContent()) && diskCacheEntity.getExpires() >= System.currentTimeMillis()) {
            try {
                this.f12423b.replace(diskCacheEntity);
            } catch (DbException e) {
                LogUtil.m20120e(e.getMessage(), e);
            }
            m20100c();
        }
    }

    public LruDiskCache setMaxSize(long j) {
        if (j > 0) {
            long diskAvailableSize = FileUtil.getDiskAvailableSize();
            if (diskAvailableSize > j) {
                this.f12425d = j;
            } else {
                this.f12425d = diskAvailableSize;
            }
        }
        return this;
    }

    /* renamed from: b */
    private void m20097b() {
        this.f12426e.execute(new C5176c());
    }

    /* renamed from: c */
    private void m20100c() {
        this.f12426e.execute(new C5175b());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004c, code lost:
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0081, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081 A[ExcHandler: InterruptedException (e java.lang.InterruptedException), Splitter:B:18:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00c0  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.xutils.cache.DiskCacheFile mo28523a(org.xutils.cache.DiskCacheFile r7) {
        /*
            r6 = this;
            r0 = 0
            if (r7 == 0) goto L_0x0011
            long r1 = r7.length()
            r3 = 1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0011
            org.xutils.common.util.IOUtil.closeQuietly(r7)
            return r0
        L_0x0011:
            boolean r1 = r6.f12422a
            if (r1 == 0) goto L_0x00c8
            if (r7 != 0) goto L_0x0019
            goto L_0x00c8
        L_0x0019:
            org.xutils.cache.DiskCacheEntity r1 = r7.f12411P
            java.lang.String r2 = r7.getName()
            java.lang.String r3 = ".tmp"
            boolean r2 = r2.endsWith(r3)
            if (r2 == 0) goto L_0x00c7
            java.lang.String r2 = r1.getPath()     // Catch:{ InterruptedException -> 0x0093, all -> 0x008f }
            r3 = 1
            r4 = 3000(0xbb8, double:1.482E-320)
            org.xutils.common.util.ProcessLock r3 = org.xutils.common.util.ProcessLock.tryLock(r2, r3, r4)     // Catch:{ InterruptedException -> 0x0093, all -> 0x008f }
            if (r3 == 0) goto L_0x0083
            boolean r4 = r3.isValid()     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
            if (r4 == 0) goto L_0x0083
            org.xutils.cache.DiskCacheFile r4 = new org.xutils.cache.DiskCacheFile     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
            r4.<init>(r1, r2, r3)     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
            boolean r2 = r7.renameTo(r4)     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            if (r2 == 0) goto L_0x0064
            org.xutils.DbManager r0 = r6.f12423b     // Catch:{ DbException -> 0x0050 }
            r0.replace(r1)     // Catch:{ DbException -> 0x0050 }
            goto L_0x0058
        L_0x004b:
            r0 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x00b4
        L_0x0050:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()     // Catch:{ InterruptedException -> 0x0081, all -> 0x004b }
            org.xutils.common.util.LogUtil.m20120e(r1, r0)     // Catch:{ InterruptedException -> 0x0081, all -> 0x004b }
        L_0x0058:
            r6.m20100c()     // Catch:{ InterruptedException -> 0x0081, all -> 0x004b }
            org.xutils.common.util.IOUtil.closeQuietly(r7)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r7)
            r7 = r4
            goto L_0x00c7
        L_0x0064:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            r2.<init>()     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            java.lang.String r5 = "rename:"
            r2.append(r5)     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            java.lang.String r5 = r7.getAbsolutePath()     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            r2.append(r5)     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            java.lang.String r2 = r2.toString()     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            r1.<init>(r2)     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
            throw r1     // Catch:{ InterruptedException -> 0x0081, all -> 0x007f }
        L_0x007f:
            r1 = move-exception
            goto L_0x00b4
        L_0x0081:
            r0 = move-exception
            goto L_0x0097
        L_0x0083:
            org.xutils.ex.FileLockedException r1 = new org.xutils.ex.FileLockedException     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
            r1.<init>(r2)     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
            throw r1     // Catch:{ InterruptedException -> 0x008c, all -> 0x0089 }
        L_0x0089:
            r1 = move-exception
            r4 = r0
            goto L_0x00b4
        L_0x008c:
            r1 = move-exception
            r4 = r0
            goto L_0x0096
        L_0x008f:
            r1 = move-exception
            r3 = r0
            r4 = r3
            goto L_0x00b4
        L_0x0093:
            r1 = move-exception
            r3 = r0
            r4 = r3
        L_0x0096:
            r0 = r1
        L_0x0097:
            java.lang.String r1 = r0.getMessage()     // Catch:{ all -> 0x00b1 }
            org.xutils.common.util.LogUtil.m20120e(r1, r0)     // Catch:{ all -> 0x00b1 }
            if (r7 != 0) goto L_0x00aa
            org.xutils.common.util.IOUtil.closeQuietly(r4)
            org.xutils.common.util.IOUtil.closeQuietly(r3)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r4)
            goto L_0x00c7
        L_0x00aa:
            org.xutils.common.util.IOUtil.closeQuietly(r7)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r7)
            goto L_0x00c7
        L_0x00b1:
            r0 = move-exception
            r1 = r0
            r0 = r7
        L_0x00b4:
            if (r0 != 0) goto L_0x00c0
            org.xutils.common.util.IOUtil.closeQuietly(r4)
            org.xutils.common.util.IOUtil.closeQuietly(r3)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r4)
            goto L_0x00c6
        L_0x00c0:
            org.xutils.common.util.IOUtil.closeQuietly(r7)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r7)
        L_0x00c6:
            throw r1
        L_0x00c7:
            return r7
        L_0x00c8:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.cache.LruDiskCache.mo28523a(org.xutils.cache.DiskCacheFile):org.xutils.cache.DiskCacheFile");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m20094a() {
        Class<DiskCacheEntity> cls = DiskCacheEntity.class;
        try {
            WhereBuilder b = WhereBuilder.m20150b("expires", "<", Long.valueOf(System.currentTimeMillis()));
            List<DiskCacheEntity> findAll = this.f12423b.selector(cls).where(b).findAll();
            this.f12423b.delete(cls, b);
            if (findAll != null && findAll.size() > 0) {
                for (DiskCacheEntity diskCacheEntity : findAll) {
                    String path = diskCacheEntity.getPath();
                    if (!TextUtils.isEmpty(path)) {
                        m20095a(path);
                    }
                }
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m20095a(String str) {
        ProcessLock processLock;
        try {
            processLock = ProcessLock.tryLock(str, true);
            if (processLock != null) {
                try {
                    if (processLock.isValid()) {
                        boolean deleteFileOrDir = IOUtil.deleteFileOrDir(new File(str));
                        IOUtil.closeQuietly(processLock);
                        return deleteFileOrDir;
                    }
                } catch (Throwable th) {
                    th = th;
                    IOUtil.closeQuietly(processLock);
                    throw th;
                }
            }
            IOUtil.closeQuietly(processLock);
            return false;
        } catch (Throwable th2) {
            th = th2;
            processLock = null;
            IOUtil.closeQuietly(processLock);
            throw th;
        }
    }
}
