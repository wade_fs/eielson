package org.xutils.cache;

import java.io.Closeable;
import java.io.File;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.ProcessLock;

public final class DiskCacheFile extends File implements Closeable {

    /* renamed from: P */
    DiskCacheEntity f12411P;

    /* renamed from: Q */
    ProcessLock f12412Q;

    DiskCacheFile(DiskCacheEntity diskCacheEntity, String str, ProcessLock processLock) {
        super(str);
        this.f12411P = diskCacheEntity;
        this.f12412Q = processLock;
    }

    public void close() {
        IOUtil.closeQuietly(this.f12412Q);
    }

    public DiskCacheFile commit() {
        return getDiskCache().mo28523a(this);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        close();
    }

    public DiskCacheEntity getCacheEntity() {
        return this.f12411P;
    }

    public LruDiskCache getDiskCache() {
        return LruDiskCache.getDiskCache(getParentFile().getName());
    }
}
