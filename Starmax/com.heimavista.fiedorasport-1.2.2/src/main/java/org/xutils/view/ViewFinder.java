package org.xutils.view;

import android.app.Activity;
import android.view.View;

/* renamed from: org.xutils.view.a */
final class ViewFinder {

    /* renamed from: a */
    private View f12762a;

    /* renamed from: b */
    private Activity f12763b;

    public ViewFinder(View view) {
        this.f12762a = view;
    }

    /* renamed from: a */
    public View mo29029a(int i) {
        View view = this.f12762a;
        if (view != null) {
            return view.findViewById(i);
        }
        Activity activity = this.f12763b;
        if (activity != null) {
            return activity.findViewById(i);
        }
        return null;
    }

    public ViewFinder(Activity activity) {
        this.f12763b = activity;
    }

    /* renamed from: a */
    public View mo29031a(ViewInfo bVar) {
        return mo29030a(bVar.f12764a, bVar.f12765b);
    }

    /* renamed from: a */
    public View mo29030a(int i, int i2) {
        View a = i2 > 0 ? mo29029a(i2) : null;
        if (a != null) {
            return a.findViewById(i);
        }
        return mo29029a(i);
    }
}
