package org.xutils.view;

/* renamed from: org.xutils.view.b */
final class ViewInfo {

    /* renamed from: a */
    public int f12764a;

    /* renamed from: b */
    public int f12765b;

    ViewInfo() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ViewInfo.class != obj.getClass()) {
            return false;
        }
        ViewInfo bVar = (ViewInfo) obj;
        if (this.f12764a == bVar.f12764a && this.f12765b == bVar.f12765b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.f12764a * 31) + this.f12765b;
    }
}
