package org.xutils.view;

import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import org.xutils.C5217x;
import org.xutils.ViewInjector;
import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

public final class ViewInjectorImpl implements ViewInjector {

    /* renamed from: a */
    private static final HashSet<Class<?>> f12759a = new HashSet<>();

    /* renamed from: b */
    private static final Object f12760b = new Object();

    /* renamed from: c */
    private static volatile ViewInjectorImpl f12761c;

    static {
        f12759a.add(Object.class);
        f12759a.add(Activity.class);
        f12759a.add(Fragment.class);
        try {
            f12759a.add(Class.forName("androidx.fragment.app.Fragment"));
            f12759a.add(Class.forName("androidx.fragment.app.FragmentActivity"));
        } catch (Throwable unused) {
        }
    }

    private ViewInjectorImpl() {
    }

    /* renamed from: a */
    private static ContentView m20247a(Class<?> cls) {
        if (cls == null || f12759a.contains(cls)) {
            return null;
        }
        ContentView contentView = (ContentView) cls.getAnnotation(ContentView.class);
        return contentView == null ? m20247a(cls.getSuperclass()) : contentView;
    }

    public static void registerInstance() {
        if (f12761c == null) {
            synchronized (f12760b) {
                if (f12761c == null) {
                    f12761c = new ViewInjectorImpl();
                }
            }
        }
        C5217x.Ext.setViewInjector(f12761c);
    }

    public void inject(View view) {
        m20248a(view, view.getClass(), new ViewFinder(view));
    }

    public void inject(Activity activity) {
        int value;
        Class<?> cls = activity.getClass();
        try {
            ContentView a = m20247a(cls);
            if (a != null && (value = a.value()) > 0) {
                activity.setContentView(value);
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        m20248a(activity, cls, new ViewFinder(activity));
    }

    /* renamed from: a */
    private static void m20248a(Object obj, Class<?> cls, ViewFinder aVar) {
        Event event;
        int i;
        ViewInject viewInject;
        if (cls != null && !f12759a.contains(cls)) {
            m20248a(obj, cls.getSuperclass(), aVar);
            Field[] declaredFields = cls.getDeclaredFields();
            if (declaredFields != null && declaredFields.length > 0) {
                for (Field field : declaredFields) {
                    Class<?> type = field.getType();
                    if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers()) && !type.isPrimitive() && !type.isArray() && (viewInject = (ViewInject) field.getAnnotation(ViewInject.class)) != null) {
                        try {
                            View a = aVar.mo29030a(viewInject.value(), viewInject.parentId());
                            if (a != null) {
                                field.setAccessible(true);
                                field.set(obj, a);
                            } else {
                                throw new RuntimeException("Invalid @ViewInject for " + cls.getSimpleName() + "." + field.getName());
                            }
                        } catch (Throwable th) {
                            LogUtil.m20120e(th.getMessage(), th);
                        }
                    }
                }
            }
            Method[] declaredMethods = cls.getDeclaredMethods();
            if (declaredMethods != null && declaredMethods.length > 0) {
                for (Method method : declaredMethods) {
                    if (!Modifier.isStatic(method.getModifiers()) && Modifier.isPrivate(method.getModifiers()) && (event = (Event) method.getAnnotation(Event.class)) != null) {
                        try {
                            int[] value = event.value();
                            int[] parentId = event.parentId();
                            if (parentId == null) {
                                i = 0;
                            } else {
                                i = parentId.length;
                            }
                            int i2 = 0;
                            while (i2 < value.length) {
                                int i3 = value[i2];
                                if (i3 > 0) {
                                    ViewInfo bVar = new ViewInfo();
                                    bVar.f12764a = i3;
                                    bVar.f12765b = i > i2 ? parentId[i2] : 0;
                                    method.setAccessible(true);
                                    EventListenerManager.m20246a(aVar, bVar, event, obj, method);
                                }
                                i2++;
                            }
                        } catch (Throwable th2) {
                            LogUtil.m20120e(th2.getMessage(), th2);
                        }
                    }
                }
            }
        }
    }

    public void inject(Object obj, View view) {
        m20248a(obj, obj.getClass(), new ViewFinder(view));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View inject(Object obj, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        int value;
        Class<?> cls = obj.getClass();
        View view = null;
        try {
            ContentView a = m20247a(cls);
            if (a != null && (value = a.value()) > 0) {
                view = layoutInflater.inflate(value, viewGroup, false);
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        m20248a(obj, cls, new ViewFinder(view));
        return view;
    }
}
