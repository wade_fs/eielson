package org.xutils.view;

import android.text.TextUtils;
import android.view.View;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.xutils.common.util.DoubleKeyValueMap;
import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.Event;

final class EventListenerManager {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final HashSet<String> f12754a = new HashSet<>(2);

    /* renamed from: b */
    private static final DoubleKeyValueMap<ViewInfo, Class<?>, Object> f12755b = new DoubleKeyValueMap<>();

    public static class DynamicHandler implements InvocationHandler {

        /* renamed from: c */
        private static long f12756c;

        /* renamed from: a */
        private WeakReference<Object> f12757a;

        /* renamed from: b */
        private final HashMap<String, Method> f12758b = new HashMap<>(1);

        public DynamicHandler(Object obj) {
            this.f12757a = new WeakReference<>(obj);
        }

        public void addMethod(String str, Method method) {
            this.f12758b.put(str, method);
        }

        public Object getHandler() {
            return this.f12757a.get();
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            Object obj2 = this.f12757a.get();
            if (obj2 != null) {
                String name = method.getName();
                if ("toString".equals(name)) {
                    return DynamicHandler.class.getSimpleName();
                }
                Method method2 = this.f12758b.get(name);
                if (method2 == null && this.f12758b.size() == 1) {
                    Iterator<Map.Entry<String, Method>> it = this.f12758b.entrySet().iterator();
                    if (it.hasNext()) {
                        Map.Entry next = it.next();
                        if (TextUtils.isEmpty((CharSequence) next.getKey())) {
                            method2 = (Method) next.getValue();
                        }
                    }
                }
                if (method2 != null) {
                    if (EventListenerManager.f12754a.contains(name)) {
                        long currentTimeMillis = System.currentTimeMillis() - f12756c;
                        if (currentTimeMillis < 300) {
                            LogUtil.m20117d("onClick cancelled: " + currentTimeMillis);
                            return null;
                        }
                        f12756c = System.currentTimeMillis();
                    }
                    try {
                        return method2.invoke(obj2, objArr);
                    } catch (Throwable th) {
                        throw new RuntimeException("invoke method error:" + obj2.getClass().getName() + "#" + method2.getName(), th);
                    }
                } else {
                    LogUtil.m20125w("method not impl: " + name + "(" + obj2.getClass().getSimpleName() + ")");
                }
            }
            return null;
        }
    }

    static {
        f12754a.add("onClick");
        f12754a.add("onItemClick");
    }

    /* renamed from: a */
    public static void m20246a(ViewFinder aVar, ViewInfo bVar, Event event, Object obj, Method method) {
        boolean z;
        try {
            View a = aVar.mo29031a(bVar);
            if (a != null) {
                Class<?> type = event.type();
                String str = event.setter();
                if (TextUtils.isEmpty(str)) {
                    str = "set" + type.getSimpleName();
                }
                String method2 = event.method();
                Object obj2 = f12755b.get(bVar, type);
                if (obj2 != null) {
                    DynamicHandler dynamicHandler = (DynamicHandler) Proxy.getInvocationHandler(obj2);
                    z = obj.equals(dynamicHandler.getHandler());
                    if (z) {
                        dynamicHandler.addMethod(method2, method);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    DynamicHandler dynamicHandler2 = new DynamicHandler(obj);
                    dynamicHandler2.addMethod(method2, method);
                    obj2 = Proxy.newProxyInstance(type.getClassLoader(), new Class[]{type}, dynamicHandler2);
                    f12755b.put(bVar, type, obj2);
                }
                a.getClass().getMethod(str, type).invoke(a, obj2);
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }
}
