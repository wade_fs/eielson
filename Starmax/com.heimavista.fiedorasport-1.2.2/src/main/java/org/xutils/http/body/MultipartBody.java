package org.xutils.http.body;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.xutils.common.Callback;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.KeyValue;
import org.xutils.http.ProgressHandler;

public class MultipartBody implements ProgressBody {

    /* renamed from: h */
    private static byte[] f12617h = "--------7da3d81520810".getBytes();

    /* renamed from: i */
    private static byte[] f12618i = "\r\n".getBytes();

    /* renamed from: j */
    private static byte[] f12619j = "--".getBytes();

    /* renamed from: a */
    private byte[] f12620a;

    /* renamed from: b */
    private String f12621b;

    /* renamed from: c */
    private String f12622c = C1750C.UTF8_NAME;

    /* renamed from: d */
    private List<KeyValue> f12623d;

    /* renamed from: e */
    private long f12624e = 0;

    /* renamed from: f */
    private long f12625f = 0;

    /* renamed from: g */
    private ProgressHandler f12626g;

    public MultipartBody(List<KeyValue> list, String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f12622c = str;
        }
        this.f12623d = list;
        m20183a();
        C5203a aVar = new C5203a(this);
        try {
            writeTo(aVar);
            this.f12624e = aVar.f12627P.get();
        } catch (IOException unused) {
            this.f12624e = -1;
        }
    }

    /* renamed from: a */
    private void m20183a() {
        String hexString = Double.toHexString(Math.random() * 65535.0d);
        this.f12620a = hexString.getBytes();
        this.f12621b = "multipart/form-data; boundary=" + new String(f12617h) + hexString;
    }

    public long getContentLength() {
        return this.f12624e;
    }

    public String getContentType() {
        return this.f12621b;
    }

    public void setContentType(String str) {
        int indexOf = this.f12621b.indexOf(";");
        this.f12621b = "multipart/" + str + this.f12621b.substring(indexOf);
    }

    public void setProgressHandler(ProgressHandler progressHandler) {
        this.f12626g = progressHandler;
    }

    public void writeTo(OutputStream outputStream) {
        ProgressHandler progressHandler = this.f12626g;
        if (progressHandler == null || progressHandler.updateProgress(this.f12624e, this.f12625f, true)) {
            for (KeyValue keyValue : this.f12623d) {
                String str = keyValue.key;
                Object obj = keyValue.value;
                if (!TextUtils.isEmpty(str) && obj != null) {
                    m20186a(outputStream, str, obj);
                }
            }
            byte[] bArr = f12619j;
            m20187a(outputStream, bArr, f12617h, this.f12620a, bArr);
            outputStream.flush();
            ProgressHandler progressHandler2 = this.f12626g;
            if (progressHandler2 != null) {
                long j = this.f12624e;
                progressHandler2.updateProgress(j, j, true);
                return;
            }
            return;
        }
        throw new Callback.CancelledException("upload stopped!");
    }

    /* renamed from: org.xutils.http.body.MultipartBody$a */
    private class C5203a extends OutputStream {

        /* renamed from: P */
        final AtomicLong f12627P = new AtomicLong(0);

        public C5203a(MultipartBody multipartBody) {
        }

        /* renamed from: a */
        public void mo28857a(File file) {
            if (this.f12627P.get() != -1) {
                this.f12627P.addAndGet(file.length());
            }
        }

        public void write(int i) {
            if (this.f12627P.get() != -1) {
                this.f12627P.incrementAndGet();
            }
        }

        /* renamed from: a */
        public void mo28858a(InputStream inputStream) {
            if (this.f12627P.get() != -1) {
                long inputStreamLength = InputStreamBody.getInputStreamLength(inputStream);
                if (inputStreamLength > 0) {
                    this.f12627P.addAndGet(inputStreamLength);
                } else {
                    this.f12627P.set(-1);
                }
            }
        }

        public void write(byte[] bArr) {
            if (this.f12627P.get() != -1) {
                this.f12627P.addAndGet((long) bArr.length);
            }
        }

        public void write(byte[] bArr, int i, int i2) {
            if (this.f12627P.get() != -1) {
                this.f12627P.addAndGet((long) i2);
            }
        }
    }

    /* renamed from: a */
    private void m20186a(OutputStream outputStream, String str, Object obj) {
        String str2;
        String str3;
        byte[] bArr;
        m20187a(outputStream, f12619j, f12617h, this.f12620a);
        if (obj instanceof BodyItemWrapper) {
            BodyItemWrapper bodyItemWrapper = (BodyItemWrapper) obj;
            Object value = bodyItemWrapper.getValue();
            str2 = bodyItemWrapper.getFileName();
            Object obj2 = value;
            str3 = bodyItemWrapper.getContentType();
            obj = obj2;
        } else {
            str2 = "";
            str3 = null;
        }
        if (obj instanceof File) {
            File file = (File) obj;
            if (TextUtils.isEmpty(str2)) {
                str2 = file.getName();
            }
            if (TextUtils.isEmpty(str3)) {
                str3 = FileBody.getFileContentType(file);
            }
            m20187a(outputStream, m20189a(str, str2, this.f12622c));
            m20187a(outputStream, m20188a(obj, str3, this.f12622c));
            m20187a(outputStream, new byte[0][]);
            m20184a(outputStream, file);
            m20187a(outputStream, new byte[0][]);
            return;
        }
        m20187a(outputStream, m20189a(str, str2, this.f12622c));
        m20187a(outputStream, m20188a(obj, str3, this.f12622c));
        m20187a(outputStream, new byte[0][]);
        if (obj instanceof InputStream) {
            m20185a(outputStream, (InputStream) obj);
            m20187a(outputStream, new byte[0][]);
            return;
        }
        if (obj instanceof byte[]) {
            bArr = (byte[]) obj;
        } else {
            bArr = String.valueOf(obj).getBytes(this.f12622c);
        }
        m20187a(outputStream, bArr);
        this.f12625f += (long) bArr.length;
        ProgressHandler progressHandler = this.f12626g;
        if (progressHandler != null && !progressHandler.updateProgress(this.f12624e, this.f12625f, false)) {
            throw new Callback.CancelledException("upload stopped!");
        }
    }

    /* renamed from: a */
    private void m20187a(OutputStream outputStream, byte[]... bArr) {
        if (bArr != null) {
            for (byte[] bArr2 : bArr) {
                outputStream.write(bArr2);
            }
        }
        outputStream.write(f12618i);
    }

    /* renamed from: a */
    private void m20184a(OutputStream outputStream, File file) {
        if (outputStream instanceof C5203a) {
            ((C5203a) outputStream).mo28857a(file);
        } else {
            m20185a(outputStream, new FileInputStream(file));
        }
    }

    /* renamed from: a */
    private void m20185a(OutputStream outputStream, InputStream inputStream) {
        if (outputStream instanceof C5203a) {
            ((C5203a) outputStream).mo28858a(inputStream);
            return;
        }
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read >= 0) {
                    outputStream.write(bArr, 0, read);
                    this.f12625f += (long) read;
                    if (this.f12626g != null) {
                        if (!this.f12626g.updateProgress(this.f12624e, this.f12625f, false)) {
                            throw new Callback.CancelledException("upload stopped!");
                        }
                    }
                } else {
                    return;
                }
            }
        } finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /* renamed from: a */
    private static byte[] m20189a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("Content-Disposition: form-data");
        sb.append("; name=\"");
        sb.append(str.replace("\"", "\\\""));
        sb.append("\"");
        if (!TextUtils.isEmpty(str2)) {
            sb.append("; filename=\"");
            sb.append(str2.replace("\"", "\\\""));
            sb.append("\"");
        }
        return sb.toString().getBytes(str3);
    }

    /* renamed from: a */
    private static byte[] m20188a(Object obj, String str, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder("Content-Type: ");
        if (!TextUtils.isEmpty(str)) {
            str3 = str.replaceFirst("\\/jpg$", "/jpeg");
        } else if (obj instanceof String) {
            str3 = "text/plain; charset=" + str2;
        } else {
            str3 = "application/octet-stream";
        }
        sb.append(str3);
        return sb.toString().getBytes(str2);
    }
}
