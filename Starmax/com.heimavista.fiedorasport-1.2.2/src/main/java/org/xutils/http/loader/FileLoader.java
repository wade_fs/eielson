package org.xutils.http.loader;

import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Date;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.DiskCacheFile;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.common.util.ProcessLock;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;
import org.xutils.p275ex.FileLockedException;
import org.xutils.p275ex.HttpException;

public class FileLoader extends Loader<File> {

    /* renamed from: a */
    private String f12639a;

    /* renamed from: b */
    private String f12640b;

    /* renamed from: c */
    private boolean f12641c;

    /* renamed from: d */
    private boolean f12642d;

    /* renamed from: e */
    private long f12643e;

    /* renamed from: f */
    private String f12644f;

    /* renamed from: g */
    private DiskCacheFile f12645g;

    /* renamed from: a */
    private File m20197a(File file) {
        if (this.f12642d && file.exists() && !TextUtils.isEmpty(this.f12644f)) {
            File file2 = new File(file.getParent(), this.f12644f);
            while (file2.exists()) {
                String parent = file.getParent();
                file2 = new File(parent, System.currentTimeMillis() + this.f12644f);
            }
            return file.renameTo(file2) ? file2 : file;
        } else if (this.f12640b.equals(this.f12639a)) {
            return file;
        } else {
            File file3 = new File(this.f12640b);
            return file.renameTo(file3) ? file3 : file;
        }
    }

    /* renamed from: b */
    private void m20199b(UriRequest uriRequest) {
        DiskCacheEntity diskCacheEntity = new DiskCacheEntity();
        diskCacheEntity.setKey(uriRequest.getCacheKey());
        this.f12645g = LruDiskCache.getDiskCache(super.params.getCacheDirName()).createDiskCacheFile(diskCacheEntity);
        DiskCacheFile diskCacheFile = this.f12645g;
        if (diskCacheFile != null) {
            this.f12640b = diskCacheFile.getAbsolutePath();
            this.f12639a = this.f12640b;
            this.f12642d = false;
            return;
        }
        throw new IOException("create cache file error:" + uriRequest.getCacheKey());
    }

    /* renamed from: c */
    private static boolean m20200c(UriRequest uriRequest) {
        if (uriRequest == null) {
            return false;
        }
        String responseHeader = uriRequest.getResponseHeader("Accept-Ranges");
        if (responseHeader != null) {
            return responseHeader.contains("bytes");
        }
        String responseHeader2 = uriRequest.getResponseHeader("Content-Range");
        if (responseHeader2 == null || !responseHeader2.contains("bytes")) {
            return false;
        }
        return true;
    }

    public Loader<File> newInstance() {
        return new FileLoader();
    }

    public void save2Cache(UriRequest uriRequest) {
    }

    public void setParams(RequestParams requestParams) {
        if (requestParams != null) {
            super.params = requestParams;
            this.f12641c = requestParams.isAutoResume();
            this.f12642d = requestParams.isAutoRename();
        }
    }

    public File loadFromCache(DiskCacheEntity diskCacheEntity) {
        return LruDiskCache.getDiskCache(super.params.getCacheDirName()).getDiskCacheFile(diskCacheEntity.getKey());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public File load(InputStream inputStream) {
        BufferedInputStream bufferedInputStream;
        FileOutputStream fileOutputStream;
        BufferedOutputStream bufferedOutputStream;
        BufferedOutputStream bufferedOutputStream2;
        FileInputStream fileInputStream;
        InputStream inputStream2 = inputStream;
        BufferedOutputStream bufferedOutputStream3 = null;
        try {
            File file = new File(this.f12639a);
            if (file.isDirectory()) {
                IOUtil.deleteFileOrDir(file);
            }
            if (!file.exists()) {
                File parentFile = file.getParentFile();
                if (!parentFile.exists()) {
                    if (!parentFile.mkdirs()) {
                        throw new IOException("can not create dir: " + parentFile.getAbsolutePath());
                    }
                }
            }
            long length = file.length();
            if (this.f12641c && length > 0) {
                long j = length - 512;
                if (j > 0) {
                    try {
                        fileInputStream = new FileInputStream(file);
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = null;
                        IOUtil.closeQuietly(fileInputStream);
                        throw th;
                    }
                    try {
                        if (Arrays.equals(IOUtil.readBytes(inputStream2, 0, 512), IOUtil.readBytes(fileInputStream, j, 512))) {
                            this.f12643e -= 512;
                            IOUtil.closeQuietly(fileInputStream);
                        } else {
                            IOUtil.closeQuietly(fileInputStream);
                            IOUtil.deleteFileOrDir(file);
                            throw new RuntimeException("need retry");
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtil.closeQuietly(fileInputStream);
                        throw th;
                    }
                } else {
                    IOUtil.deleteFileOrDir(file);
                    throw new RuntimeException("need retry");
                }
            }
            if (this.f12641c) {
                fileOutputStream = new FileOutputStream(file, true);
            } else {
                fileOutputStream = new FileOutputStream(file);
                length = 0;
            }
            long j2 = this.f12643e + length;
            bufferedInputStream = new BufferedInputStream(inputStream2);
            try {
                bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            } catch (Throwable th3) {
                th = th3;
                IOUtil.closeQuietly(bufferedInputStream);
                IOUtil.closeQuietly(bufferedOutputStream3);
                throw th;
            }
            try {
                if (super.progressHandler != null) {
                    bufferedOutputStream2 = bufferedOutputStream;
                    try {
                        if (!super.progressHandler.updateProgress(j2, length, true)) {
                            throw new Callback.CancelledException("download stopped!");
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        bufferedOutputStream3 = bufferedOutputStream2;
                        IOUtil.closeQuietly(bufferedInputStream);
                        IOUtil.closeQuietly(bufferedOutputStream3);
                        throw th;
                    }
                } else {
                    bufferedOutputStream2 = bufferedOutputStream;
                }
                byte[] bArr = new byte[4096];
                while (true) {
                    long j3 = length;
                    int read = bufferedInputStream.read(bArr);
                    if (read == -1) {
                        bufferedOutputStream2.flush();
                        if (this.f12645g != null) {
                            file = this.f12645g.commit();
                        }
                        if (super.progressHandler != null) {
                            super.progressHandler.updateProgress(j2, j3, true);
                        }
                        IOUtil.closeQuietly(bufferedInputStream);
                        IOUtil.closeQuietly(bufferedOutputStream2);
                        return m20197a(file);
                    } else if (file.getParentFile().exists()) {
                        bufferedOutputStream2.write(bArr, 0, read);
                        length = ((long) read) + j3;
                        if (super.progressHandler != null) {
                            if (!super.progressHandler.updateProgress(j2, length, false)) {
                                bufferedOutputStream2.flush();
                                throw new Callback.CancelledException("download stopped!");
                            }
                        }
                    } else {
                        file.getParentFile().mkdirs();
                        throw new IOException("parent be deleted!");
                    }
                }
            } catch (Throwable th5) {
                th = th5;
                bufferedOutputStream2 = bufferedOutputStream;
                bufferedOutputStream3 = bufferedOutputStream2;
                IOUtil.closeQuietly(bufferedInputStream);
                IOUtil.closeQuietly(bufferedOutputStream3);
                throw th;
            }
        } catch (Throwable th6) {
            th = th6;
            bufferedInputStream = null;
        }
    }

    /* renamed from: a */
    private static String m20198a(UriRequest uriRequest) {
        int indexOf;
        if (uriRequest == null) {
            return null;
        }
        String responseHeader = uriRequest.getResponseHeader("Content-Disposition");
        if (!TextUtils.isEmpty(responseHeader) && (indexOf = responseHeader.indexOf("filename=")) > 0) {
            int i = indexOf + 9;
            int indexOf2 = responseHeader.indexOf(";", i);
            if (indexOf2 < 0) {
                indexOf2 = responseHeader.length();
            }
            if (indexOf2 > i) {
                try {
                    String decode = URLDecoder.decode(responseHeader.substring(i, indexOf2), uriRequest.getParams().getCharset());
                    return (!decode.startsWith("\"") || !decode.endsWith("\"")) ? decode : decode.substring(1, decode.length() - 1);
                } catch (UnsupportedEncodingException e) {
                    LogUtil.m20120e(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    public File load(UriRequest uriRequest) {
        File file;
        File file2;
        ProcessLock processLock = null;
        try {
            this.f12640b = super.params.getSaveFilePath();
            this.f12645g = null;
            if (TextUtils.isEmpty(this.f12640b)) {
                if (super.progressHandler != null) {
                    if (!super.progressHandler.updateProgress(0, 0, false)) {
                        throw new Callback.CancelledException("download stopped!");
                    }
                }
                m20199b(uriRequest);
            } else {
                this.f12639a = this.f12640b + ".tmp";
            }
            if (super.progressHandler != null) {
                if (!super.progressHandler.updateProgress(0, 0, false)) {
                    throw new Callback.CancelledException("download stopped!");
                }
            }
            processLock = ProcessLock.tryLock(this.f12640b + "_lock", true);
            if (processLock == null || !processLock.isValid()) {
                throw new FileLockedException("download exists: " + this.f12640b);
            }
            super.params = uriRequest.getParams();
            long j = 0;
            if (this.f12641c) {
                File file3 = new File(this.f12639a);
                long length = file3.length();
                if (length <= 512) {
                    IOUtil.deleteFileOrDir(file3);
                } else {
                    j = length - 512;
                }
            }
            RequestParams requestParams = super.params;
            requestParams.setHeader("RANGE", "bytes=" + j + "-");
            if (super.progressHandler != null) {
                if (!super.progressHandler.updateProgress(0, 0, false)) {
                    throw new Callback.CancelledException("download stopped!");
                }
            }
            uriRequest.sendRequest();
            this.f12643e = uriRequest.getContentLength();
            if (this.f12642d) {
                this.f12644f = m20198a(uriRequest);
            }
            if (this.f12641c) {
                this.f12641c = m20200c(uriRequest);
            }
            if (super.progressHandler != null) {
                if (!super.progressHandler.updateProgress(0, 0, false)) {
                    throw new Callback.CancelledException("download stopped!");
                }
            }
            if (this.f12645g != null) {
                DiskCacheEntity cacheEntity = this.f12645g.getCacheEntity();
                cacheEntity.setLastAccess(System.currentTimeMillis());
                cacheEntity.setEtag(uriRequest.getETag());
                cacheEntity.setExpires(uriRequest.getExpiration());
                cacheEntity.setLastModify(new Date(uriRequest.getLastModified()));
            }
            file = load(uriRequest.getInputStream());
            IOUtil.closeQuietly(processLock);
            IOUtil.closeQuietly(this.f12645g);
            return file;
        } catch (HttpException e) {
            if (e.getCode() == 416) {
                if (this.f12645g != null) {
                    file2 = this.f12645g.commit();
                } else {
                    file2 = new File(this.f12639a);
                }
                if (file2 == null || !file2.exists()) {
                    IOUtil.deleteFileOrDir(file2);
                    throw new IllegalStateException("cache file not found" + uriRequest.getCacheKey());
                }
                if (this.f12642d) {
                    this.f12644f = m20198a(uriRequest);
                }
                file = m20197a(file2);
            } else {
                throw e;
            }
        } catch (Throwable th) {
            IOUtil.closeQuietly((Closeable) null);
            IOUtil.closeQuietly(this.f12645g);
            throw th;
        }
    }
}
