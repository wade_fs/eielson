package org.xutils.http.cookie;

import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import org.xutils.C5217x;
import org.xutils.DbManager;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.LogUtil;
import org.xutils.config.DbConfigs;
import org.xutils.p274db.Selector;
import org.xutils.p274db.sqlite.WhereBuilder;
import org.xutils.p274db.table.DbModel;

public enum DbCookieStore implements CookieStore {
    INSTANCE;
    
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final DbManager f12634P = C5217x.getDb(DbConfigs.COOKIE.getConfig());

    /* renamed from: Q */
    private final Executor f12635Q = new PriorityExecutor(1, true);
    /* access modifiers changed from: private */

    /* renamed from: R */
    public long f12636R = 0;

    /* renamed from: org.xutils.http.cookie.DbCookieStore$a */
    class C5204a implements Runnable {
        C5204a() {
        }

        public void run() {
            List findAll;
            Class<CookieEntity> cls = CookieEntity.class;
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - DbCookieStore.this.f12636R >= 1000) {
                long unused = DbCookieStore.this.f12636R = currentTimeMillis;
                try {
                    DbCookieStore.this.f12634P.delete(cls, WhereBuilder.m20150b("expiry", "<", Long.valueOf(System.currentTimeMillis())).and("expiry", "!=", -1L));
                } catch (Throwable th) {
                    LogUtil.m20120e(th.getMessage(), th);
                }
                try {
                    int count = (int) DbCookieStore.this.f12634P.selector(cls).count();
                    if (count > 5010 && (findAll = DbCookieStore.this.f12634P.selector(cls).where("expiry", "!=", -1L).orderBy("expiry", false).limit(count - 5000).findAll()) != null) {
                        DbCookieStore.this.f12634P.delete(findAll);
                    }
                } catch (Throwable th2) {
                    LogUtil.m20120e(th2.getMessage(), th2);
                }
            }
        }
    }

    public void add(URI uri, HttpCookie httpCookie) {
        if (httpCookie != null) {
            try {
                this.f12634P.replace(new CookieEntity(m20194a(uri), httpCookie));
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
            m20195a();
        }
    }

    public List<HttpCookie> get(URI uri) {
        if (uri != null) {
            URI a = m20194a(uri);
            ArrayList arrayList = new ArrayList();
            try {
                Selector selector = this.f12634P.selector(CookieEntity.class);
                WhereBuilder b = WhereBuilder.m20149b();
                String host = a.getHost();
                if (!TextUtils.isEmpty(host)) {
                    WhereBuilder b2 = WhereBuilder.m20150b("domain", "=", host);
                    WhereBuilder or = b2.mo28689or("domain", "=", "." + host);
                    int indexOf = host.indexOf(".");
                    int lastIndexOf = host.lastIndexOf(".");
                    if (indexOf > 0 && lastIndexOf > indexOf) {
                        String substring = host.substring(indexOf, host.length());
                        if (!TextUtils.isEmpty(substring)) {
                            or.mo28689or("domain", "=", substring);
                        }
                    }
                    b.and(or);
                }
                String path = a.getPath();
                if (!TextUtils.isEmpty(path)) {
                    WhereBuilder or2 = WhereBuilder.m20150b("path", "=", path).mo28689or("path", "=", "/").mo28689or("path", "=", null);
                    int lastIndexOf2 = path.lastIndexOf("/");
                    while (lastIndexOf2 > 0) {
                        path = path.substring(0, lastIndexOf2);
                        or2.mo28689or("path", "=", path);
                        lastIndexOf2 = path.lastIndexOf("/");
                    }
                    b.and(or2);
                }
                b.mo28689or(ShareConstants.MEDIA_URI, "=", a.toString());
                List<CookieEntity> findAll = selector.where(b).findAll();
                if (findAll != null) {
                    for (CookieEntity aVar : findAll) {
                        if (!aVar.isExpired()) {
                            arrayList.add(aVar.toHttpCookie());
                        }
                    }
                }
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
            return arrayList;
        }
        throw new NullPointerException("uri is null");
    }

    public List<HttpCookie> getCookies() {
        ArrayList arrayList = new ArrayList();
        try {
            List<CookieEntity> findAll = this.f12634P.findAll(CookieEntity.class);
            if (findAll != null) {
                for (CookieEntity aVar : findAll) {
                    if (!aVar.isExpired()) {
                        arrayList.add(aVar.toHttpCookie());
                    }
                }
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        return arrayList;
    }

    public List<URI> getURIs() {
        String string;
        Class<CookieEntity> cls = CookieEntity.class;
        ArrayList arrayList = new ArrayList();
        try {
            List<DbModel> findAll = this.f12634P.selector(cls).select(ShareConstants.MEDIA_URI).findAll();
            if (findAll != null) {
                for (DbModel dbModel : findAll) {
                    string = dbModel.getString(ShareConstants.MEDIA_URI);
                    if (!TextUtils.isEmpty(string)) {
                        arrayList.add(new URI(string));
                    }
                }
            }
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        return arrayList;
    }

    public boolean remove(URI uri, HttpCookie httpCookie) {
        if (httpCookie == null) {
            return true;
        }
        try {
            WhereBuilder b = WhereBuilder.m20150b("name", "=", httpCookie.getName());
            String domain = httpCookie.getDomain();
            if (!TextUtils.isEmpty(domain)) {
                b.and("domain", "=", domain);
            }
            String path = httpCookie.getPath();
            if (!TextUtils.isEmpty(path)) {
                if (path.length() > 1 && path.endsWith("/")) {
                    path = path.substring(0, path.length() - 1);
                }
                b.and("path", "=", path);
            }
            this.f12634P.delete(CookieEntity.class, b);
            return true;
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            return false;
        }
    }

    public boolean removeAll() {
        try {
            this.f12634P.delete((Class<?>) CookieEntity.class);
            return true;
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            return true;
        }
    }

    /* renamed from: a */
    private void m20195a() {
        this.f12635Q.execute(new C5204a());
    }

    /* renamed from: a */
    private URI m20194a(URI uri) {
        try {
            return new URI("http", uri.getHost(), uri.getPath(), null, null);
        } catch (Throwable unused) {
            return uri;
        }
    }
}
