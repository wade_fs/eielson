package org.xutils.http.body;

import android.text.TextUtils;

public final class BodyItemWrapper {

    /* renamed from: a */
    private final Object f12607a;

    /* renamed from: b */
    private final String f12608b;

    /* renamed from: c */
    private final String f12609c;

    public BodyItemWrapper(Object obj, String str) {
        this(obj, str, null);
    }

    public String getContentType() {
        return this.f12609c;
    }

    public String getFileName() {
        return this.f12608b;
    }

    public Object getValue() {
        return this.f12607a;
    }

    public BodyItemWrapper(Object obj, String str, String str2) {
        this.f12607a = obj;
        if (TextUtils.isEmpty(str)) {
            this.f12609c = "application/octet-stream";
        } else {
            this.f12609c = str;
        }
        this.f12608b = str2;
    }
}
