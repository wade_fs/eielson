package org.xutils.http.body;

import android.net.Uri;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import org.xutils.common.util.LogUtil;

public class FileBody extends InputStreamBody {

    /* renamed from: f */
    private File f12610f;

    /* renamed from: g */
    private String f12611g;

    public FileBody(File file) {
        this(file, null);
    }

    public static String getFileContentType(File file) {
        String str;
        try {
            str = HttpURLConnection.guessContentTypeFromName(Uri.encode(file.getName(), "-![.:/,?&=]"));
        } catch (Exception e) {
            LogUtil.m20119e(e.toString());
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            return "application/octet-stream";
        }
        return str.replaceFirst("\\/jpg$", "/jpeg");
    }

    public String getContentType() {
        if (TextUtils.isEmpty(this.f12611g)) {
            this.f12611g = getFileContentType(this.f12610f);
        }
        return this.f12611g;
    }

    public void setContentType(String str) {
        this.f12611g = str;
    }

    public FileBody(File file, String str) {
        super(new FileInputStream(file));
        this.f12610f = file;
        this.f12611g = str;
    }
}
