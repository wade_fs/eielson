package org.xutils.http.loader;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.ParameterizedTypeUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpResponse;
import org.xutils.http.app.InputStreamResponseParser;
import org.xutils.http.app.ResponseParser;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.f */
class ObjectLoader extends Loader<Object> {

    /* renamed from: a */
    private String f12651a = C1750C.UTF8_NAME;

    /* renamed from: b */
    private String f12652b = null;

    /* renamed from: c */
    private final Type f12653c;

    /* renamed from: d */
    private final Class<?> f12654d;

    /* renamed from: e */
    private final ResponseParser f12655e;

    public ObjectLoader(Type type) {
        Class cls;
        this.f12653c = type;
        if (type instanceof ParameterizedType) {
            this.f12654d = (Class) ((ParameterizedType) type).getRawType();
        } else if (!(type instanceof TypeVariable)) {
            this.f12654d = (Class) type;
        } else {
            throw new IllegalArgumentException("not support callback type " + type.toString());
        }
        if (List.class.equals(this.f12654d)) {
            Type parameterizedType = ParameterizedTypeUtil.getParameterizedType(this.f12653c, List.class, 0);
            if (parameterizedType instanceof ParameterizedType) {
                cls = (Class) ((ParameterizedType) parameterizedType).getRawType();
            } else if (!(parameterizedType instanceof TypeVariable)) {
                cls = (Class) parameterizedType;
            } else {
                throw new IllegalArgumentException("not support callback type " + parameterizedType.toString());
            }
            HttpResponse httpResponse = (HttpResponse) cls.getAnnotation(HttpResponse.class);
            if (httpResponse != null) {
                try {
                    this.f12655e = (ResponseParser) httpResponse.parser().newInstance();
                } catch (Throwable th) {
                    throw new RuntimeException("create parser error", th);
                }
            } else {
                throw new IllegalArgumentException("not found @HttpResponse from " + parameterizedType);
            }
        } else {
            HttpResponse httpResponse2 = (HttpResponse) this.f12654d.getAnnotation(HttpResponse.class);
            if (httpResponse2 != null) {
                try {
                    this.f12655e = (ResponseParser) httpResponse2.parser().newInstance();
                } catch (Throwable th2) {
                    throw new RuntimeException("create parser error", th2);
                }
            } else {
                throw new IllegalArgumentException("not found @HttpResponse from " + this.f12653c);
            }
        }
    }

    public Object load(InputStream inputStream) {
        ResponseParser responseParser = this.f12655e;
        if (responseParser instanceof InputStreamResponseParser) {
            return ((InputStreamResponseParser) responseParser).parse(this.f12653c, this.f12654d, inputStream);
        }
        this.f12652b = IOUtil.readStr(inputStream, this.f12651a);
        return this.f12655e.parse(this.f12653c, this.f12654d, this.f12652b);
    }

    public Object loadFromCache(DiskCacheEntity diskCacheEntity) {
        if (diskCacheEntity == null) {
            return null;
        }
        String textContent = diskCacheEntity.getTextContent();
        if (!TextUtils.isEmpty(textContent)) {
            return this.f12655e.parse(this.f12653c, this.f12654d, textContent);
        }
        return null;
    }

    public Loader<Object> newInstance() {
        throw new IllegalAccessError("use constructor create ObjectLoader.");
    }

    public void save2Cache(UriRequest uriRequest) {
        saveStringCache(uriRequest, this.f12652b);
    }

    public void setParams(RequestParams requestParams) {
        if (requestParams != null) {
            String charset = requestParams.getCharset();
            if (!TextUtils.isEmpty(charset)) {
                this.f12651a = charset;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public Object load(UriRequest uriRequest) {
        try {
            uriRequest.sendRequest();
            this.f12655e.checkResponse(uriRequest);
            return load(uriRequest.getInputStream());
        } catch (Throwable th) {
            this.f12655e.checkResponse(uriRequest);
            throw th;
        }
    }
}
