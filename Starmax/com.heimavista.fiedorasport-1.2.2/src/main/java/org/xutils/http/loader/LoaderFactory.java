package org.xutils.http.loader;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

public final class LoaderFactory {

    /* renamed from: a */
    private static final HashMap<Type, Loader> f12646a = new HashMap<>();

    static {
        f12646a.put(JSONObject.class, new JSONObjectLoader());
        f12646a.put(JSONArray.class, new JSONArrayLoader());
        f12646a.put(String.class, new C5206g());
        f12646a.put(File.class, new FileLoader());
        f12646a.put(byte[].class, new C5205b());
        BooleanLoader aVar = new BooleanLoader();
        f12646a.put(Boolean.TYPE, aVar);
        f12646a.put(Boolean.class, aVar);
        IntegerLoader cVar = new IntegerLoader();
        f12646a.put(Integer.TYPE, cVar);
        f12646a.put(Integer.class, cVar);
    }

    private LoaderFactory() {
    }

    public static Loader<?> getLoader(Type type, RequestParams requestParams) {
        Loader<?> loader;
        Loader loader2 = f12646a.get(type);
        if (loader2 == null) {
            loader = new ObjectLoader(type);
        } else {
            loader = loader2.newInstance();
        }
        loader.setParams(requestParams);
        return loader;
    }

    public static <T> void registerLoader(Type type, Loader<T> loader) {
        f12646a.put(type, loader);
    }
}
