package org.xutils.http;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.http.body.BodyItemWrapper;
import org.xutils.http.body.FileBody;
import org.xutils.http.body.InputStreamBody;
import org.xutils.http.body.MultipartBody;
import org.xutils.http.body.RequestBody;
import org.xutils.http.body.StringBody;
import org.xutils.http.body.UrlEncodedParamsBody;

abstract class BaseParams {

    /* renamed from: a */
    private String f12533a = C1750C.UTF8_NAME;

    /* renamed from: b */
    private HttpMethod f12534b;

    /* renamed from: c */
    private String f12535c;

    /* renamed from: d */
    private boolean f12536d = false;

    /* renamed from: e */
    private boolean f12537e = false;

    /* renamed from: f */
    private RequestBody f12538f;

    /* renamed from: g */
    private final List<Header> f12539g = new ArrayList();

    /* renamed from: h */
    private final List<KeyValue> f12540h = new ArrayList();

    /* renamed from: i */
    private final List<KeyValue> f12541i = new ArrayList();

    /* renamed from: j */
    private final List<KeyValue> f12542j = new ArrayList();

    public static final class ArrayItem extends KeyValue {
        public ArrayItem(String str, Object obj) {
            super(str, obj);
        }
    }

    public static final class Header extends KeyValue {
        public final boolean setHeader;

        public Header(String str, String str2, boolean z) {
            super(str, str2);
            this.setHeader = z;
        }
    }

    BaseParams() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0085, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void mo28774a() {
        /*
            r2 = this;
            monitor-enter(r2)
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ all -> 0x0086 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r2)
            return
        L_0x000b:
            org.xutils.http.HttpMethod r0 = r2.f12534b     // Catch:{ all -> 0x0086 }
            boolean r0 = org.xutils.http.HttpMethod.permitsRequestBody(r0)     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x001f
            java.lang.String r0 = r2.f12535c     // Catch:{ all -> 0x0086 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x001f
            org.xutils.http.body.RequestBody r0 = r2.f12538f     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x002b
        L_0x001f:
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12540h     // Catch:{ all -> 0x0086 }
            java.util.List<org.xutils.common.util.KeyValue> r1 = r2.f12541i     // Catch:{ all -> 0x0086 }
            r0.addAll(r1)     // Catch:{ all -> 0x0086 }
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ all -> 0x0086 }
            r0.clear()     // Catch:{ all -> 0x0086 }
        L_0x002b:
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ all -> 0x0086 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0086 }
            if (r0 != 0) goto L_0x004b
            boolean r0 = r2.f12536d     // Catch:{ all -> 0x0086 }
            if (r0 != 0) goto L_0x003f
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12542j     // Catch:{ all -> 0x0086 }
            int r0 = r0.size()     // Catch:{ all -> 0x0086 }
            if (r0 <= 0) goto L_0x004b
        L_0x003f:
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12542j     // Catch:{ all -> 0x0086 }
            java.util.List<org.xutils.common.util.KeyValue> r1 = r2.f12541i     // Catch:{ all -> 0x0086 }
            r0.addAll(r1)     // Catch:{ all -> 0x0086 }
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ all -> 0x0086 }
            r0.clear()     // Catch:{ all -> 0x0086 }
        L_0x004b:
            boolean r0 = r2.f12537e     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x0084
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ all -> 0x0086 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0086 }
            if (r0 != 0) goto L_0x0084
            java.lang.String r0 = r2.f12535c     // Catch:{ JSONException -> 0x007d }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x007d }
            if (r0 != 0) goto L_0x0067
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x007d }
            java.lang.String r1 = r2.f12535c     // Catch:{ JSONException -> 0x007d }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x007d }
            goto L_0x006c
        L_0x0067:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x007d }
            r0.<init>()     // Catch:{ JSONException -> 0x007d }
        L_0x006c:
            java.util.List<org.xutils.common.util.KeyValue> r1 = r2.f12541i     // Catch:{ JSONException -> 0x007d }
            r2.m20162a(r0, r1)     // Catch:{ JSONException -> 0x007d }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x007d }
            r2.f12535c = r0     // Catch:{ JSONException -> 0x007d }
            java.util.List<org.xutils.common.util.KeyValue> r0 = r2.f12541i     // Catch:{ JSONException -> 0x007d }
            r0.clear()     // Catch:{ JSONException -> 0x007d }
            goto L_0x0084
        L_0x007d:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0086 }
            r1.<init>(r0)     // Catch:{ all -> 0x0086 }
            throw r1     // Catch:{ all -> 0x0086 }
        L_0x0084:
            monitor-exit(r2)
            return
        L_0x0086:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.http.BaseParams.mo28774a():void");
    }

    public void addBodyParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.f12541i.add(new KeyValue(str, str2));
        } else {
            this.f12535c = str2;
        }
    }

    public void addHeader(String str, String str2) {
        this.f12539g.add(new Header(str, str2, false));
    }

    public void addParameter(String str, Object obj) {
        if (obj != null) {
            HttpMethod httpMethod = this.f12534b;
            int i = 0;
            if (httpMethod == null || HttpMethod.permitsRequestBody(httpMethod)) {
                if (TextUtils.isEmpty(str)) {
                    this.f12535c = obj.toString();
                } else if ((obj instanceof File) || (obj instanceof InputStream) || (obj instanceof byte[])) {
                    this.f12542j.add(new KeyValue(str, obj));
                } else if (obj instanceof Iterable) {
                    for (Object obj2 : (Iterable) obj) {
                        this.f12541i.add(new ArrayItem(str, obj2));
                    }
                } else if (obj instanceof JSONArray) {
                    JSONArray jSONArray = (JSONArray) obj;
                    int length = jSONArray.length();
                    while (i < length) {
                        this.f12541i.add(new ArrayItem(str, jSONArray.opt(i)));
                        i++;
                    }
                } else if (obj.getClass().isArray()) {
                    int length2 = Array.getLength(obj);
                    while (i < length2) {
                        this.f12541i.add(new ArrayItem(str, Array.get(obj, i)));
                        i++;
                    }
                } else {
                    this.f12541i.add(new KeyValue(str, obj));
                }
            } else if (TextUtils.isEmpty(str)) {
            } else {
                if (obj instanceof Iterable) {
                    for (Object obj3 : (Iterable) obj) {
                        this.f12540h.add(new ArrayItem(str, obj3));
                    }
                } else if (obj.getClass().isArray()) {
                    int length3 = Array.getLength(obj);
                    while (i < length3) {
                        this.f12540h.add(new ArrayItem(str, Array.get(obj, i)));
                        i++;
                    }
                } else {
                    this.f12540h.add(new KeyValue(str, obj));
                }
            }
        }
    }

    public void addQueryStringParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.f12540h.add(new KeyValue(str, str2));
        }
    }

    public void clearParams() {
        this.f12540h.clear();
        this.f12541i.clear();
        this.f12542j.clear();
        this.f12535c = null;
        this.f12538f = null;
    }

    public String getBodyContent() {
        mo28774a();
        return this.f12535c;
    }

    public List<KeyValue> getBodyParams() {
        mo28774a();
        return new ArrayList(this.f12541i);
    }

    public String getCharset() {
        return this.f12533a;
    }

    public List<KeyValue> getFileParams() {
        mo28774a();
        return new ArrayList(this.f12542j);
    }

    public List<Header> getHeaders() {
        return new ArrayList(this.f12539g);
    }

    public HttpMethod getMethod() {
        return this.f12534b;
    }

    public List<KeyValue> getParams(String str) {
        ArrayList arrayList = new ArrayList();
        for (KeyValue keyValue : this.f12540h) {
            if (str == null && keyValue.key == null) {
                arrayList.add(keyValue);
            } else if (str != null && str.equals(keyValue.key)) {
                arrayList.add(keyValue);
            }
        }
        for (KeyValue keyValue2 : this.f12541i) {
            if (str == null && keyValue2.key == null) {
                arrayList.add(keyValue2);
            } else if (str != null && str.equals(keyValue2.key)) {
                arrayList.add(keyValue2);
            }
        }
        for (KeyValue keyValue3 : this.f12542j) {
            if (str == null && keyValue3.key == null) {
                arrayList.add(keyValue3);
            } else if (str != null && str.equals(keyValue3.key)) {
                arrayList.add(keyValue3);
            }
        }
        return arrayList;
    }

    public List<KeyValue> getQueryStringParams() {
        mo28774a();
        return new ArrayList(this.f12540h);
    }

    public RequestBody getRequestBody() {
        String str;
        mo28774a();
        RequestBody requestBody = this.f12538f;
        if (requestBody != null) {
            return requestBody;
        }
        if (!TextUtils.isEmpty(this.f12535c)) {
            return new StringBody(this.f12535c, this.f12533a);
        }
        if (this.f12536d || this.f12542j.size() > 0) {
            if (this.f12536d || this.f12542j.size() != 1) {
                this.f12536d = true;
                return new MultipartBody(this.f12542j, this.f12533a);
            }
            Iterator<KeyValue> it = this.f12542j.iterator();
            if (!it.hasNext()) {
                return null;
            }
            Object obj = it.next().value;
            if (obj instanceof BodyItemWrapper) {
                BodyItemWrapper bodyItemWrapper = (BodyItemWrapper) obj;
                Object value = bodyItemWrapper.getValue();
                str = bodyItemWrapper.getContentType();
                obj = value;
            } else {
                str = null;
            }
            if (obj instanceof File) {
                return new FileBody((File) obj, str);
            }
            if (obj instanceof InputStream) {
                return new InputStreamBody((InputStream) obj, str);
            }
            if (obj instanceof byte[]) {
                return new InputStreamBody(new ByteArrayInputStream((byte[]) obj), str);
            }
            if (obj instanceof String) {
                StringBody stringBody = new StringBody((String) obj, this.f12533a);
                stringBody.setContentType(str);
                return stringBody;
            }
            LogUtil.m20125w("Some params will be ignored for: " + toString());
            return null;
        } else if (this.f12541i.size() > 0) {
            return new UrlEncodedParamsBody(this.f12541i, this.f12533a);
        } else {
            return null;
        }
    }

    public String getStringParameter(String str) {
        for (KeyValue keyValue : this.f12540h) {
            if (str == null && keyValue.key == null) {
                return keyValue.getValueStr();
            }
            if (str != null && str.equals(keyValue.key)) {
                return keyValue.getValueStr();
            }
        }
        for (KeyValue keyValue2 : this.f12541i) {
            if (str == null && keyValue2.key == null) {
                return keyValue2.getValueStr();
            }
            if (str != null && str.equals(keyValue2.key)) {
                return keyValue2.getValueStr();
            }
        }
        return null;
    }

    public List<KeyValue> getStringParams() {
        ArrayList arrayList = new ArrayList(this.f12540h.size() + this.f12541i.size());
        arrayList.addAll(this.f12540h);
        arrayList.addAll(this.f12541i);
        return arrayList;
    }

    public boolean isAsJsonContent() {
        return this.f12537e;
    }

    public boolean isMultipart() {
        return this.f12536d;
    }

    public void removeParameter(String str) {
        if (!TextUtils.isEmpty(str)) {
            Iterator<KeyValue> it = this.f12540h.iterator();
            while (it.hasNext()) {
                if (str.equals(it.next().key)) {
                    it.remove();
                }
            }
            Iterator<KeyValue> it2 = this.f12541i.iterator();
            while (it2.hasNext()) {
                if (str.equals(it2.next().key)) {
                    it2.remove();
                }
            }
            Iterator<KeyValue> it3 = this.f12542j.iterator();
            while (it3.hasNext()) {
                if (str.equals(it3.next().key)) {
                    it3.remove();
                }
            }
            return;
        }
        this.f12535c = null;
    }

    public void setAsJsonContent(boolean z) {
        this.f12537e = z;
    }

    public void setBodyContent(String str) {
        this.f12535c = str;
    }

    public void setCharset(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f12533a = str;
        }
    }

    public void setHeader(String str, String str2) {
        Header header = new Header(str, str2, true);
        Iterator<Header> it = this.f12539g.iterator();
        while (it.hasNext()) {
            if (str.equals(it.next().key)) {
                it.remove();
            }
        }
        this.f12539g.add(header);
    }

    public void setMethod(HttpMethod httpMethod) {
        this.f12534b = httpMethod;
    }

    public void setMultipart(boolean z) {
        this.f12536d = z;
    }

    public void setRequestBody(RequestBody requestBody) {
        this.f12538f = requestBody;
    }

    public String toJSONString() {
        JSONObject jSONObject;
        ArrayList arrayList = new ArrayList(this.f12540h.size() + this.f12541i.size());
        arrayList.addAll(this.f12540h);
        arrayList.addAll(this.f12541i);
        try {
            if (!TextUtils.isEmpty(this.f12535c)) {
                jSONObject = new JSONObject(this.f12535c);
            } else {
                jSONObject = new JSONObject();
            }
            m20162a(jSONObject, arrayList);
            return jSONObject.toString();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public String toString() {
        mo28774a();
        StringBuilder sb = new StringBuilder();
        if (!this.f12540h.isEmpty()) {
            for (KeyValue keyValue : this.f12540h) {
                sb.append(keyValue.key);
                sb.append("=");
                sb.append(keyValue.value);
                sb.append("&");
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        if (HttpMethod.permitsRequestBody(this.f12534b)) {
            sb.append("<");
            if (!TextUtils.isEmpty(this.f12535c)) {
                sb.append(this.f12535c);
            } else if (!this.f12541i.isEmpty()) {
                for (KeyValue keyValue2 : this.f12541i) {
                    sb.append(keyValue2.key);
                    sb.append("=");
                    sb.append(keyValue2.value);
                    sb.append("&");
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append(">");
        }
        return sb.toString();
    }

    public void addBodyParameter(String str, File file) {
        addBodyParameter(str, file, null, null);
    }

    public void addBodyParameter(String str, Object obj, String str2) {
        addBodyParameter(str, obj, str2, null);
    }

    public void addBodyParameter(String str, Object obj, String str2, String str3) {
        if (!TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str3)) {
            this.f12542j.add(new KeyValue(str, new BodyItemWrapper(obj, str2, str3)));
        } else {
            this.f12542j.add(new KeyValue(str, obj));
        }
    }

    /* renamed from: a */
    private void m20162a(JSONObject jSONObject, List<KeyValue> list) {
        JSONArray jSONArray;
        HashSet hashSet = new HashSet(list.size());
        LinkedHashMap linkedHashMap = new LinkedHashMap(list.size());
        for (int i = 0; i < list.size(); i++) {
            KeyValue keyValue = list.get(i);
            String str = keyValue.key;
            if (!TextUtils.isEmpty(str)) {
                if (linkedHashMap.containsKey(str)) {
                    jSONArray = (JSONArray) linkedHashMap.get(str);
                } else {
                    jSONArray = new JSONArray();
                    linkedHashMap.put(str, jSONArray);
                }
                jSONArray.put(RequestParamsHelper.m20181a(keyValue.value));
                if (keyValue instanceof ArrayItem) {
                    hashSet.add(str);
                }
            }
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            String str2 = (String) entry.getKey();
            JSONArray jSONArray2 = (JSONArray) entry.getValue();
            if (jSONArray2.length() > 1 || hashSet.contains(str2)) {
                jSONObject.put(str2, jSONArray2);
            } else {
                jSONObject.put(str2, jSONArray2.get(0));
            }
        }
    }
}
