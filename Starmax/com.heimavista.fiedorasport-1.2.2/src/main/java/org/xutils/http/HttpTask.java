package org.xutils.http;

import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.common.task.AbsTask;
import org.xutils.common.task.Priority;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.ParameterizedTypeUtil;
import org.xutils.http.app.RequestInterceptListener;
import org.xutils.http.app.RequestTracker;
import org.xutils.http.request.UriRequest;
import org.xutils.http.request.UriRequestFactory;

public class HttpTask<ResultType> extends AbsTask<ResultType> implements ProgressHandler {
    /* access modifiers changed from: private */

    /* renamed from: w */
    public static final AtomicInteger f12548w = new AtomicInteger(0);

    /* renamed from: x */
    private static final HashMap<String, WeakReference<HttpTask<?>>> f12549x = new HashMap<>(1);

    /* renamed from: y */
    private static final PriorityExecutor f12550y = new PriorityExecutor(5, true);

    /* renamed from: z */
    private static final PriorityExecutor f12551z = new PriorityExecutor(5, true);
    /* access modifiers changed from: private */

    /* renamed from: f */
    public RequestParams f12552f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public UriRequest f12553g;

    /* renamed from: h */
    private HttpTask<ResultType>.c f12554h;

    /* renamed from: i */
    private final Executor f12555i;

    /* renamed from: j */
    private volatile boolean f12556j = false;

    /* renamed from: k */
    private final Callback.CommonCallback<ResultType> f12557k;

    /* renamed from: l */
    private Object f12558l = null;

    /* renamed from: m */
    private volatile Boolean f12559m = null;

    /* renamed from: n */
    private final Object f12560n = new Object();

    /* renamed from: o */
    private Callback.CacheCallback<ResultType> f12561o;

    /* renamed from: p */
    private Callback.PrepareCallback f12562p;

    /* renamed from: q */
    private Callback.ProgressCallback f12563q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public RequestInterceptListener f12564r;

    /* renamed from: s */
    private RequestTracker f12565s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public Type f12566t;

    /* renamed from: u */
    private long f12567u;

    /* renamed from: v */
    private long f12568v = 300;

    /* renamed from: org.xutils.http.HttpTask$a */
    class C5196a implements Runnable {
        C5196a() {
        }

        public void run() {
            HttpTask.this.m20172d();
        }
    }

    /* renamed from: org.xutils.http.HttpTask$b */
    class C5197b implements Runnable {
        C5197b() {
        }

        public void run() {
            HttpTask.this.m20172d();
        }
    }

    /* renamed from: org.xutils.http.HttpTask$c */
    private final class C5198c {

        /* renamed from: a */
        Object f12571a;

        /* renamed from: b */
        Throwable f12572b;

        /* synthetic */ C5198c(HttpTask httpTask, C5196a aVar) {
            this();
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(6:11|12|98|97|6|7) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
            r0 = true;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0010 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x010d */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x0010 A[LOOP:0: B:6:0x0010->B:97:0x0010, LOOP_START, SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo28773a() {
            /*
                r5 = this;
                r0 = 0
                java.lang.Class<java.io.File> r1 = java.io.File.class
                org.xutils.http.HttpTask r2 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x00ac }
                java.lang.reflect.Type r2 = r2.f12566t     // Catch:{ all -> 0x00ac }
                if (r1 != r2) goto L_0x003a
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x00ac }
                monitor-enter(r1)     // Catch:{ all -> 0x00ac }
            L_0x0010:
                java.util.concurrent.atomic.AtomicInteger r2 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x0037 }
                int r2 = r2.get()     // Catch:{ all -> 0x0037 }
                r3 = 3
                if (r2 < r3) goto L_0x002e
                org.xutils.http.HttpTask r2 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x0037 }
                boolean r2 = r2.isCancelled()     // Catch:{ all -> 0x0037 }
                if (r2 != 0) goto L_0x002e
                java.util.concurrent.atomic.AtomicInteger r2 = org.xutils.http.HttpTask.f12548w     // Catch:{ InterruptedException -> 0x002d, all -> 0x0010 }
                r3 = 10
                r2.wait(r3)     // Catch:{ InterruptedException -> 0x002d, all -> 0x0010 }
                goto L_0x0010
            L_0x002d:
                r0 = 1
            L_0x002e:
                monitor-exit(r1)     // Catch:{ all -> 0x0037 }
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x00ac }
                r1.incrementAndGet()     // Catch:{ all -> 0x00ac }
                goto L_0x003a
            L_0x0037:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0037 }
                throw r0     // Catch:{ all -> 0x00ac }
            L_0x003a:
                if (r0 != 0) goto L_0x008e
                org.xutils.http.HttpTask r1 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x00ac }
                boolean r1 = r1.isCancelled()     // Catch:{ all -> 0x00ac }
                if (r1 == 0) goto L_0x0045
                goto L_0x008e
            L_0x0045:
                org.xutils.http.HttpTask r0 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x0061 }
                org.xutils.http.request.UriRequest r0 = r0.f12553g     // Catch:{ all -> 0x0061 }
                org.xutils.http.HttpTask r1 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x0061 }
                org.xutils.http.app.RequestInterceptListener r1 = r1.f12564r     // Catch:{ all -> 0x0061 }
                r0.setRequestInterceptListener(r1)     // Catch:{ all -> 0x0061 }
                org.xutils.http.HttpTask r0 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x0061 }
                org.xutils.http.request.UriRequest r0 = r0.f12553g     // Catch:{ all -> 0x0061 }
                java.lang.Object r0 = r0.loadResult()     // Catch:{ all -> 0x0061 }
                r5.f12571a = r0     // Catch:{ all -> 0x0061 }
                goto L_0x0064
            L_0x0061:
                r0 = move-exception
                r5.f12572b = r0     // Catch:{ all -> 0x00ac }
            L_0x0064:
                java.lang.Throwable r0 = r5.f12572b     // Catch:{ all -> 0x00ac }
                if (r0 != 0) goto L_0x008b
                java.lang.Class<java.io.File> r0 = java.io.File.class
                org.xutils.http.HttpTask r1 = org.xutils.http.HttpTask.this
                java.lang.reflect.Type r1 = r1.f12566t
                if (r0 != r1) goto L_0x0131
                java.util.concurrent.atomic.AtomicInteger r0 = org.xutils.http.HttpTask.f12548w
                monitor-enter(r0)
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x0088 }
                r1.decrementAndGet()     // Catch:{ all -> 0x0088 }
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x0088 }
                r1.notifyAll()     // Catch:{ all -> 0x0088 }
                monitor-exit(r0)     // Catch:{ all -> 0x0088 }
                goto L_0x0131
            L_0x0088:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0088 }
                throw r1
            L_0x008b:
                java.lang.Throwable r0 = r5.f12572b     // Catch:{ all -> 0x00ac }
                throw r0     // Catch:{ all -> 0x00ac }
            L_0x008e:
                org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException     // Catch:{ all -> 0x00ac }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ac }
                r2.<init>()     // Catch:{ all -> 0x00ac }
                java.lang.String r3 = "cancelled before request"
                r2.append(r3)     // Catch:{ all -> 0x00ac }
                if (r0 == 0) goto L_0x009f
                java.lang.String r0 = "(interrupted)"
                goto L_0x00a1
            L_0x009f:
                java.lang.String r0 = ""
            L_0x00a1:
                r2.append(r0)     // Catch:{ all -> 0x00ac }
                java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00ac }
                r1.<init>(r0)     // Catch:{ all -> 0x00ac }
                throw r1     // Catch:{ all -> 0x00ac }
            L_0x00ac:
                r0 = move-exception
                r5.f12572b = r0     // Catch:{ all -> 0x0132 }
                boolean r1 = r0 instanceof org.xutils.p275ex.HttpException     // Catch:{ all -> 0x0132 }
                if (r1 == 0) goto L_0x010f
                r1 = r0
                org.xutils.ex.HttpException r1 = (org.xutils.p275ex.HttpException) r1     // Catch:{ all -> 0x0132 }
                int r2 = r1.getCode()     // Catch:{ all -> 0x0132 }
                r3 = 301(0x12d, float:4.22E-43)
                if (r2 == r3) goto L_0x00c2
                r3 = 302(0x12e, float:4.23E-43)
                if (r2 != r3) goto L_0x010f
            L_0x00c2:
                org.xutils.http.HttpTask r3 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x0132 }
                org.xutils.http.RequestParams r3 = r3.f12552f     // Catch:{ all -> 0x0132 }
                org.xutils.http.app.RedirectHandler r3 = r3.getRedirectHandler()     // Catch:{ all -> 0x0132 }
                if (r3 == 0) goto L_0x010f
                org.xutils.http.HttpTask r4 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x010d }
                org.xutils.http.request.UriRequest r4 = r4.f12553g     // Catch:{ all -> 0x010d }
                org.xutils.http.RequestParams r3 = r3.getRedirectParams(r4)     // Catch:{ all -> 0x010d }
                if (r3 == 0) goto L_0x010f
                org.xutils.http.HttpMethod r4 = r3.getMethod()     // Catch:{ all -> 0x010d }
                if (r4 != 0) goto L_0x00ed
                org.xutils.http.HttpTask r4 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x010d }
                org.xutils.http.RequestParams r4 = r4.f12552f     // Catch:{ all -> 0x010d }
                org.xutils.http.HttpMethod r4 = r4.getMethod()     // Catch:{ all -> 0x010d }
                r3.setMethod(r4)     // Catch:{ all -> 0x010d }
            L_0x00ed:
                org.xutils.http.HttpTask r4 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x010d }
                org.xutils.http.RequestParams unused = r4.f12552f = r3     // Catch:{ all -> 0x010d }
                org.xutils.http.HttpTask r3 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x010d }
                org.xutils.http.HttpTask r4 = org.xutils.http.HttpTask.this     // Catch:{ all -> 0x010d }
                org.xutils.http.request.UriRequest r4 = r4.m20174e()     // Catch:{ all -> 0x010d }
                org.xutils.http.request.UriRequest unused = r3.f12553g = r4     // Catch:{ all -> 0x010d }
                org.xutils.ex.HttpRedirectException r3 = new org.xutils.ex.HttpRedirectException     // Catch:{ all -> 0x010d }
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x010d }
                java.lang.String r1 = r1.getResult()     // Catch:{ all -> 0x010d }
                r3.<init>(r2, r4, r1)     // Catch:{ all -> 0x010d }
                r5.f12572b = r3     // Catch:{ all -> 0x010d }
                goto L_0x010f
            L_0x010d:
                r5.f12572b = r0     // Catch:{ all -> 0x0132 }
            L_0x010f:
                java.lang.Class<java.io.File> r0 = java.io.File.class
                org.xutils.http.HttpTask r1 = org.xutils.http.HttpTask.this
                java.lang.reflect.Type r1 = r1.f12566t
                if (r0 != r1) goto L_0x0131
                java.util.concurrent.atomic.AtomicInteger r0 = org.xutils.http.HttpTask.f12548w
                monitor-enter(r0)
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x012e }
                r1.decrementAndGet()     // Catch:{ all -> 0x012e }
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x012e }
                r1.notifyAll()     // Catch:{ all -> 0x012e }
                monitor-exit(r0)     // Catch:{ all -> 0x012e }
                goto L_0x0131
            L_0x012e:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x012e }
                throw r1
            L_0x0131:
                return
            L_0x0132:
                r0 = move-exception
                java.lang.Class<java.io.File> r1 = java.io.File.class
                org.xutils.http.HttpTask r2 = org.xutils.http.HttpTask.this
                java.lang.reflect.Type r2 = r2.f12566t
                if (r1 != r2) goto L_0x0155
                java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.http.HttpTask.f12548w
                monitor-enter(r1)
                java.util.concurrent.atomic.AtomicInteger r2 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x0152 }
                r2.decrementAndGet()     // Catch:{ all -> 0x0152 }
                java.util.concurrent.atomic.AtomicInteger r2 = org.xutils.http.HttpTask.f12548w     // Catch:{ all -> 0x0152 }
                r2.notifyAll()     // Catch:{ all -> 0x0152 }
                monitor-exit(r1)     // Catch:{ all -> 0x0152 }
                goto L_0x0155
            L_0x0152:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0152 }
                throw r0
            L_0x0155:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: org.xutils.http.HttpTask.C5198c.mo28773a():void");
        }

        private C5198c() {
        }
    }

    public HttpTask(RequestParams requestParams, Callback.Cancelable cancelable, Callback.CommonCallback<ResultType> commonCallback) {
        super(cancelable);
        this.f12552f = requestParams;
        this.f12557k = commonCallback;
        if (commonCallback instanceof Callback.CacheCallback) {
            this.f12561o = (Callback.CacheCallback) commonCallback;
        }
        if (commonCallback instanceof Callback.PrepareCallback) {
            this.f12562p = (Callback.PrepareCallback) commonCallback;
        }
        if (commonCallback instanceof Callback.ProgressCallback) {
            this.f12563q = (Callback.ProgressCallback) commonCallback;
        }
        if (commonCallback instanceof RequestInterceptListener) {
            this.f12564r = (RequestInterceptListener) commonCallback;
        }
        RequestTracker requestTracker = requestParams.getRequestTracker();
        if (requestTracker == null) {
            if (commonCallback instanceof RequestTracker) {
                requestTracker = (RequestTracker) commonCallback;
            } else {
                requestTracker = UriRequestFactory.getDefaultTracker();
            }
        }
        if (requestTracker != null) {
            this.f12565s = new RequestTrackerWrapper(requestTracker);
        }
        if (requestParams.getExecutor() != null) {
            this.f12555i = requestParams.getExecutor();
        } else if (this.f12561o != null) {
            this.f12555i = f12551z;
        } else {
            this.f12555i = f12550y;
        }
    }

    /* access modifiers changed from: protected */
    public void cancelWorks() {
        C5217x.task().run(new C5197b());
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:40|41|42|43|44|38|37) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00a6 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x00b0 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a6 A[LOOP:0: B:37:0x00a6->B:147:0x00a6, LOOP_START, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00ff A[SYNTHETIC, Splitter:B:70:0x00ff] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ResultType doBackground() {
        /*
            r9 = this;
            boolean r0 = r9.isCancelled()
            if (r0 != 0) goto L_0x0217
            r9.m20176f()
            org.xutils.http.request.UriRequest r0 = r9.m20174e()
            r9.f12553g = r0
            r9.m20168b()
            org.xutils.http.RequestParams r0 = r9.f12552f
            org.xutils.http.app.HttpRetryHandler r0 = r0.getHttpRetryHandler()
            if (r0 != 0) goto L_0x001f
            org.xutils.http.app.HttpRetryHandler r0 = new org.xutils.http.app.HttpRetryHandler
            r0.<init>()
        L_0x001f:
            org.xutils.http.RequestParams r1 = r9.f12552f
            int r1 = r1.getMaxRetryCount()
            r0.setMaxRetryCount(r1)
            boolean r1 = r9.isCancelled()
            if (r1 != 0) goto L_0x020f
            org.xutils.common.Callback$CacheCallback<ResultType> r1 = r9.f12561o
            r2 = 0
            r3 = 1
            r4 = 0
            if (r1 == 0) goto L_0x00d8
            org.xutils.http.RequestParams r1 = r9.f12552f
            org.xutils.http.HttpMethod r1 = r1.getMethod()
            boolean r1 = org.xutils.http.HttpMethod.permitsCache(r1)
            if (r1 == 0) goto L_0x00d8
            r9.m20170c()     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            r1.<init>()     // Catch:{ all -> 0x0067 }
            java.lang.String r5 = "load cache: "
            r1.append(r5)     // Catch:{ all -> 0x0067 }
            org.xutils.http.request.UriRequest r5 = r9.f12553g     // Catch:{ all -> 0x0067 }
            java.lang.String r5 = r5.getRequestUri()     // Catch:{ all -> 0x0067 }
            r1.append(r5)     // Catch:{ all -> 0x0067 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0067 }
            org.xutils.common.util.LogUtil.m20117d(r1)     // Catch:{ all -> 0x0067 }
            org.xutils.http.request.UriRequest r1 = r9.f12553g     // Catch:{ all -> 0x0067 }
            java.lang.Object r1 = r1.loadResultFromCache()     // Catch:{ all -> 0x0067 }
            r9.f12558l = r1     // Catch:{ all -> 0x0067 }
            goto L_0x006d
        L_0x0067:
            r1 = move-exception
            java.lang.String r5 = "load disk cache error"
            org.xutils.common.util.LogUtil.m20126w(r5, r1)
        L_0x006d:
            boolean r1 = r9.isCancelled()
            if (r1 != 0) goto L_0x00cd
            java.lang.Object r1 = r9.f12558l
            if (r1 == 0) goto L_0x00d8
            org.xutils.common.Callback$PrepareCallback r5 = r9.f12562p
            if (r5 == 0) goto L_0x0093
            java.lang.Object r1 = r5.prepare(r1)     // Catch:{ all -> 0x0083 }
            r9.m20170c()
            goto L_0x0093
        L_0x0083:
            r1 = move-exception
            java.lang.String r5 = "prepare disk cache error"
            org.xutils.common.util.LogUtil.m20126w(r5, r1)     // Catch:{ all -> 0x008e }
            r9.m20170c()
            r1 = r4
            goto L_0x0093
        L_0x008e:
            r0 = move-exception
            r9.m20170c()
            throw r0
        L_0x0093:
            boolean r5 = r9.isCancelled()
            if (r5 != 0) goto L_0x00c5
            if (r1 == 0) goto L_0x00d9
            r5 = 2
            java.lang.Object[] r6 = new java.lang.Object[r3]
            r6[r2] = r1
            r9.update(r5, r6)
            java.lang.Object r5 = r9.f12560n
            monitor-enter(r5)
        L_0x00a6:
            java.lang.Boolean r6 = r9.f12559m     // Catch:{ all -> 0x00c2 }
            if (r6 != 0) goto L_0x00b8
            java.lang.Object r6 = r9.f12560n     // Catch:{ InterruptedException -> 0x00b0, all -> 0x00a6 }
            r6.wait()     // Catch:{ InterruptedException -> 0x00b0, all -> 0x00a6 }
            goto L_0x00a6
        L_0x00b0:
            org.xutils.common.Callback$CancelledException r0 = new org.xutils.common.Callback$CancelledException     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = "cancelled before request"
            r0.<init>(r1)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00b8:
            monitor-exit(r5)     // Catch:{ all -> 0x00c2 }
            java.lang.Boolean r5 = r9.f12559m
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x00d9
            return r4
        L_0x00c2:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00c2 }
            throw r0
        L_0x00c5:
            org.xutils.common.Callback$CancelledException r0 = new org.xutils.common.Callback$CancelledException
            java.lang.String r1 = "cancelled before request"
            r0.<init>(r1)
            throw r0
        L_0x00cd:
            r9.m20170c()
            org.xutils.common.Callback$CancelledException r0 = new org.xutils.common.Callback$CancelledException
            java.lang.String r1 = "cancelled before request"
            r0.<init>(r1)
            throw r0
        L_0x00d8:
            r1 = r4
        L_0x00d9:
            java.lang.Boolean r5 = r9.f12559m
            if (r5 != 0) goto L_0x00e3
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r2)
            r9.f12559m = r5
        L_0x00e3:
            if (r1 != 0) goto L_0x00ea
            org.xutils.http.request.UriRequest r1 = r9.f12553g
            r1.clearCacheHeader()
        L_0x00ea:
            org.xutils.common.Callback$CommonCallback<ResultType> r1 = r9.f12557k
            boolean r5 = r1 instanceof org.xutils.common.Callback.ProxyCacheCallback
            if (r5 == 0) goto L_0x00f9
            org.xutils.common.Callback$ProxyCacheCallback r1 = (org.xutils.common.Callback.ProxyCacheCallback) r1
            boolean r1 = r1.onlyCache()
            if (r1 == 0) goto L_0x00f9
            return r4
        L_0x00f9:
            r5 = r4
            r6 = r5
            r1 = 1
            r7 = 0
        L_0x00fd:
            if (r1 == 0) goto L_0x01fe
            boolean r1 = r9.isCancelled()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 != 0) goto L_0x01a9
            org.xutils.http.request.UriRequest r1 = r9.f12553g     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            r1.close()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            r9.m20170c()     // Catch:{ all -> 0x0196 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0196 }
            r1.<init>()     // Catch:{ all -> 0x0196 }
            java.lang.String r8 = "load: "
            r1.append(r8)     // Catch:{ all -> 0x0196 }
            org.xutils.http.request.UriRequest r8 = r9.f12553g     // Catch:{ all -> 0x0196 }
            java.lang.String r8 = r8.getRequestUri()     // Catch:{ all -> 0x0196 }
            r1.append(r8)     // Catch:{ all -> 0x0196 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0196 }
            org.xutils.common.util.LogUtil.m20117d(r1)     // Catch:{ all -> 0x0196 }
            org.xutils.http.HttpTask$c r1 = new org.xutils.http.HttpTask$c     // Catch:{ all -> 0x0196 }
            r1.<init>(r9, r4)     // Catch:{ all -> 0x0196 }
            r9.f12554h = r1     // Catch:{ all -> 0x0196 }
            org.xutils.http.HttpTask<ResultType>$c r1 = r9.f12554h     // Catch:{ all -> 0x0196 }
            r1.mo28773a()     // Catch:{ all -> 0x0196 }
            org.xutils.http.HttpTask<ResultType>$c r1 = r9.f12554h     // Catch:{ all -> 0x0196 }
            java.lang.Throwable r1 = r1.f12572b     // Catch:{ all -> 0x0196 }
            if (r1 != 0) goto L_0x0191
            org.xutils.http.HttpTask<ResultType>$c r1 = r9.f12554h     // Catch:{ all -> 0x0196 }
            java.lang.Object r1 = r1.f12571a     // Catch:{ all -> 0x0196 }
            r9.f12558l = r1     // Catch:{ all -> 0x0196 }
            org.xutils.common.Callback$PrepareCallback r1 = r9.f12562p     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 == 0) goto L_0x0168
            boolean r1 = r9.isCancelled()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 != 0) goto L_0x0160
            org.xutils.common.Callback$PrepareCallback r1 = r9.f12562p     // Catch:{ all -> 0x015b }
            java.lang.Object r8 = r9.f12558l     // Catch:{ all -> 0x015b }
            java.lang.Object r1 = r1.prepare(r8)     // Catch:{ all -> 0x015b }
            r9.m20170c()     // Catch:{ HttpRedirectException -> 0x0158, all -> 0x0155 }
            goto L_0x016a
        L_0x0155:
            r5 = move-exception
            r6 = r1
            goto L_0x01b2
        L_0x0158:
            r6 = r1
            goto L_0x01e1
        L_0x015b:
            r1 = move-exception
            r9.m20170c()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x0160:
            org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            java.lang.String r8 = "cancelled before request"
            r1.<init>(r8)     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x0168:
            java.lang.Object r1 = r9.f12558l     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x016a:
            r6 = r1
            org.xutils.common.Callback$CacheCallback<ResultType> r1 = r9.f12561o     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 == 0) goto L_0x0180
            org.xutils.http.RequestParams r1 = r9.f12552f     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            org.xutils.http.HttpMethod r1 = r1.getMethod()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            boolean r1 = org.xutils.http.HttpMethod.permitsCache(r1)     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 == 0) goto L_0x0180
            org.xutils.http.request.UriRequest r1 = r9.f12553g     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            r1.save2Cache()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x0180:
            boolean r1 = r9.isCancelled()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r1 != 0) goto L_0x0189
            r1 = 0
            goto L_0x00fd
        L_0x0189:
            org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            java.lang.String r8 = "cancelled after request"
            r1.<init>(r8)     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x0191:
            org.xutils.http.HttpTask<ResultType>$c r1 = r9.f12554h     // Catch:{ all -> 0x0196 }
            java.lang.Throwable r1 = r1.f12572b     // Catch:{ all -> 0x0196 }
            throw r1     // Catch:{ all -> 0x0196 }
        L_0x0196:
            r1 = move-exception
            r9.m20170c()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            boolean r8 = r9.isCancelled()     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            if (r8 == 0) goto L_0x01a8
            org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            java.lang.String r8 = "cancelled during request"
            r1.<init>(r8)     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x01a8:
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x01a9:
            org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            java.lang.String r8 = "cancelled before request"
            r1.<init>(r8)     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
            throw r1     // Catch:{ HttpRedirectException -> 0x01e1, all -> 0x01b1 }
        L_0x01b1:
            r5 = move-exception
        L_0x01b2:
            org.xutils.http.request.UriRequest r1 = r9.f12553g
            int r1 = r1.getResponseCode()
            r8 = 204(0xcc, float:2.86E-43)
            if (r1 == r8) goto L_0x01e0
            r8 = 205(0xcd, float:2.87E-43)
            if (r1 == r8) goto L_0x01e0
            r8 = 304(0x130, float:4.26E-43)
            if (r1 == r8) goto L_0x01e0
            boolean r1 = r9.isCancelled()
            if (r1 == 0) goto L_0x01d6
            boolean r1 = r5 instanceof org.xutils.common.Callback.CancelledException
            if (r1 != 0) goto L_0x01d6
            org.xutils.common.Callback$CancelledException r1 = new org.xutils.common.Callback$CancelledException
            java.lang.String r5 = "canceled by user"
            r1.<init>(r5)
            r5 = r1
        L_0x01d6:
            org.xutils.http.request.UriRequest r1 = r9.f12553g
            int r7 = r7 + 1
            boolean r1 = r0.canRetry(r1, r5, r7)
            goto L_0x00fd
        L_0x01e0:
            return r4
        L_0x01e1:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r8 = "Http Redirect:"
            r1.append(r8)
            org.xutils.http.RequestParams r8 = r9.f12552f
            java.lang.String r8 = r8.getUri()
            r1.append(r8)
            java.lang.String r1 = r1.toString()
            org.xutils.common.util.LogUtil.m20125w(r1)
            r1 = 1
            goto L_0x00fd
        L_0x01fe:
            if (r5 == 0) goto L_0x020e
            if (r6 != 0) goto L_0x020e
            java.lang.Boolean r0 = r9.f12559m
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x020b
            goto L_0x020e
        L_0x020b:
            r9.f12556j = r3
            throw r5
        L_0x020e:
            return r6
        L_0x020f:
            org.xutils.common.Callback$CancelledException r0 = new org.xutils.common.Callback$CancelledException
            java.lang.String r1 = "cancelled before request"
            r0.<init>(r1)
            throw r0
        L_0x0217:
            org.xutils.common.Callback$CancelledException r0 = new org.xutils.common.Callback$CancelledException
            java.lang.String r1 = "cancelled before request"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.http.HttpTask.doBackground():java.lang.Object");
    }

    public Executor getExecutor() {
        return this.f12555i;
    }

    public Priority getPriority() {
        return this.f12552f.getPriority();
    }

    /* access modifiers changed from: protected */
    public boolean isCancelFast() {
        return this.f12552f.isCancelFast();
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Callback.CancelledException cancelledException) {
        RequestTracker requestTracker = this.f12565s;
        if (requestTracker != null) {
            requestTracker.onCancelled(this.f12553g);
        }
        this.f12557k.onCancelled(cancelledException);
    }

    /* access modifiers changed from: protected */
    public void onError(Throwable th, boolean z) {
        RequestTracker requestTracker = this.f12565s;
        if (requestTracker != null) {
            requestTracker.onError(this.f12553g, th, z);
        }
        this.f12557k.onError(th, z);
    }

    /* access modifiers changed from: protected */
    public void onFinished() {
        RequestTracker requestTracker = this.f12565s;
        if (requestTracker != null) {
            requestTracker.onFinished(this.f12553g);
        }
        C5217x.task().run(new C5196a());
        this.f12557k.onFinished();
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
        RequestTracker requestTracker = this.f12565s;
        if (requestTracker != null) {
            requestTracker.onStart(this.f12552f);
        }
        Callback.ProgressCallback progressCallback = this.f12563q;
        if (progressCallback != null) {
            progressCallback.onStarted();
        }
    }

    /* access modifiers changed from: protected */
    public void onSuccess(ResultType resulttype) {
        if (!this.f12556j) {
            RequestTracker requestTracker = this.f12565s;
            if (requestTracker != null) {
                requestTracker.onSuccess(this.f12553g, resulttype);
            }
            this.f12557k.onSuccess(resulttype);
        }
    }

    /* access modifiers changed from: protected */
    public void onUpdate(int i, Object... objArr) {
        Object obj;
        Callback.ProgressCallback progressCallback;
        if (i == 1) {
            RequestTracker requestTracker = this.f12565s;
            if (requestTracker != null) {
                requestTracker.onRequestCreated((UriRequest) objArr[0]);
            }
        } else if (i == 2) {
            synchronized (this.f12560n) {
                try {
                    Object obj2 = objArr[0];
                    if (this.f12565s != null) {
                        this.f12565s.onCache(this.f12553g, obj2);
                    }
                    this.f12559m = Boolean.valueOf(this.f12561o.onCache(obj2));
                    obj = this.f12560n;
                } catch (Throwable th) {
                    this.f12560n.notifyAll();
                    throw th;
                }
                obj.notifyAll();
            }
        } else if (i == 3 && (progressCallback = this.f12563q) != null && objArr.length == 3) {
            try {
                progressCallback.onLoading(((Number) objArr[0]).longValue(), ((Number) objArr[1]).longValue(), ((Boolean) objArr[2]).booleanValue());
            } catch (Throwable th2) {
                this.f12557k.onError(th2, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onWaiting() {
        RequestTracker requestTracker = this.f12565s;
        if (requestTracker != null) {
            requestTracker.onWaiting(this.f12552f);
        }
        Callback.ProgressCallback progressCallback = this.f12563q;
        if (progressCallback != null) {
            progressCallback.onWaiting();
        }
    }

    public String toString() {
        return this.f12552f.toString();
    }

    public boolean updateProgress(long j, long j2, boolean z) {
        if (isCancelled() || isFinished()) {
            return false;
        }
        if (!(this.f12563q == null || this.f12553g == null || j <= 0)) {
            if (j < j2) {
                j = j2;
            }
            if (z) {
                this.f12567u = System.currentTimeMillis();
                update(3, Long.valueOf(j), Long.valueOf(j2), Boolean.valueOf(this.f12553g.isLoading()));
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.f12567u >= this.f12568v) {
                    this.f12567u = currentTimeMillis;
                    update(3, Long.valueOf(j), Long.valueOf(j2), Boolean.valueOf(this.f12553g.isLoading()));
                }
            }
        }
        if (isCancelled() || isFinished()) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    private void m20168b() {
        if (File.class == this.f12566t) {
            synchronized (f12549x) {
                String saveFilePath = this.f12552f.getSaveFilePath();
                if (!TextUtils.isEmpty(saveFilePath)) {
                    WeakReference weakReference = f12549x.get(saveFilePath);
                    if (weakReference != null) {
                        HttpTask httpTask = (HttpTask) weakReference.get();
                        if (httpTask != null) {
                            super.cancel();
                            httpTask.m20172d();
                        }
                        f12549x.remove(saveFilePath);
                    }
                    f12549x.put(saveFilePath, new WeakReference(this));
                }
                if (f12549x.size() > 3) {
                    Iterator<Map.Entry<String, WeakReference<HttpTask<?>>>> it = f12549x.entrySet().iterator();
                    while (it.hasNext()) {
                        WeakReference weakReference2 = (WeakReference) it.next().getValue();
                        if (weakReference2 == null || weakReference2.get() == null) {
                            it.remove();
                        }
                    }
                }
            }
        }
    }

    /* renamed from: c */
    private void m20170c() {
        Object obj = this.f12558l;
        if (obj instanceof Closeable) {
            IOUtil.closeQuietly((Closeable) obj);
        }
        this.f12558l = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m20172d() {
        m20170c();
        IOUtil.closeQuietly(this.f12553g);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public UriRequest m20174e() {
        this.f12552f.mo28774a();
        UriRequest uriRequest = UriRequestFactory.getUriRequest(this.f12552f, this.f12566t);
        uriRequest.setCallingClassLoader(this.f12557k.getClass().getClassLoader());
        uriRequest.setProgressHandler(this);
        this.f12568v = (long) this.f12552f.getLoadingUpdateMaxTimeSpan();
        update(1, uriRequest);
        return uriRequest;
    }

    /* renamed from: f */
    private void m20176f() {
        Class<?> cls = this.f12557k.getClass();
        Callback.CommonCallback<ResultType> commonCallback = this.f12557k;
        if (commonCallback instanceof Callback.TypedCallback) {
            this.f12566t = ((Callback.TypedCallback) commonCallback).getLoadType();
        } else if (commonCallback instanceof Callback.PrepareCallback) {
            this.f12566t = ParameterizedTypeUtil.getParameterizedType(cls, Callback.PrepareCallback.class, 0);
        } else {
            this.f12566t = ParameterizedTypeUtil.getParameterizedType(cls, Callback.CommonCallback.class, 0);
        }
    }
}
