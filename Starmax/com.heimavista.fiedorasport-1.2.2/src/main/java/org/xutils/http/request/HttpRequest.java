package org.xutils.http.request;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.ItemTouchHelper;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.http.BaseParams;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.http.app.RequestInterceptListener;
import org.xutils.http.body.ProgressBody;
import org.xutils.http.body.RequestBody;
import org.xutils.http.cookie.DbCookieStore;
import org.xutils.p275ex.HttpException;

public class HttpRequest extends UriRequest {

    /* renamed from: U */
    private static final CookieManager f12660U = new CookieManager(DbCookieStore.INSTANCE, CookiePolicy.ACCEPT_ALL);

    /* renamed from: P */
    private String f12661P = null;

    /* renamed from: Q */
    private boolean f12662Q = false;

    /* renamed from: R */
    private InputStream f12663R = null;

    /* renamed from: S */
    private HttpURLConnection f12664S = null;

    /* renamed from: T */
    private int f12665T = 0;

    HttpRequest(RequestParams requestParams, Type type) {
        super(requestParams, type);
    }

    /* renamed from: a */
    private static String m20201a(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM y HH:mm:ss 'GMT'", Locale.US);
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        simpleDateFormat.setTimeZone(timeZone);
        new GregorianCalendar(timeZone).setTimeInMillis(date.getTime());
        return simpleDateFormat.format(date);
    }

    /* access modifiers changed from: protected */
    public String buildQueryUrl(RequestParams requestParams) {
        String uri = requestParams.getUri();
        StringBuilder sb = new StringBuilder(uri);
        if (!uri.contains("?")) {
            sb.append("?");
        } else if (!uri.endsWith("?")) {
            sb.append("&");
        }
        List<KeyValue> queryStringParams = requestParams.getQueryStringParams();
        if (queryStringParams != null) {
            for (KeyValue keyValue : queryStringParams) {
                String str = keyValue.key;
                String valueStr = keyValue.getValueStr();
                if (!TextUtils.isEmpty(str) && valueStr != null) {
                    sb.append(URLEncoder.encode(str, requestParams.getCharset()).replaceAll("\\+", "%20"));
                    sb.append("=");
                    sb.append(URLEncoder.encode(valueStr, requestParams.getCharset()).replaceAll("\\+", "%20"));
                    sb.append("&");
                }
            }
        }
        if (sb.charAt(sb.length() - 1) == '&') {
            sb.deleteCharAt(sb.length() - 1);
        }
        if (sb.charAt(sb.length() - 1) == '?') {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public void clearCacheHeader() {
        super.params.setHeader("If-Modified-Since", null);
        super.params.setHeader("If-None-Match", null);
    }

    public void close() {
        InputStream inputStream = this.f12663R;
        if (inputStream != null) {
            IOUtil.closeQuietly(inputStream);
            this.f12663R = null;
        }
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    public String getCacheKey() {
        if (this.f12661P == null) {
            this.f12661P = super.params.getCacheKey();
            if (TextUtils.isEmpty(this.f12661P)) {
                this.f12661P = super.params.toString();
            }
        }
        return this.f12661P;
    }

    public long getContentLength() {
        int i;
        HttpURLConnection httpURLConnection = this.f12664S;
        long j = 0;
        if (httpURLConnection != null) {
            try {
                j = (long) httpURLConnection.getContentLength();
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
            if (j >= 1) {
                return j;
            }
            try {
                i = getInputStream().available();
            } catch (Throwable unused) {
                return j;
            }
        } else {
            i = getInputStream().available();
        }
        return (long) i;
    }

    public String getETag() {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection == null) {
            return null;
        }
        return httpURLConnection.getHeaderField("ETag");
    }

    public long getExpiration() {
        HttpURLConnection httpURLConnection = this.f12664S;
        long j = -1;
        if (httpURLConnection == null) {
            return -1;
        }
        String headerField = httpURLConnection.getHeaderField("Cache-Control");
        if (!TextUtils.isEmpty(headerField)) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField, ",");
            while (true) {
                if (!stringTokenizer.hasMoreTokens()) {
                    break;
                }
                String lowerCase = stringTokenizer.nextToken().trim().toLowerCase();
                if (lowerCase.startsWith("max-age")) {
                    int indexOf = lowerCase.indexOf(61);
                    if (indexOf > 0) {
                        try {
                            long parseLong = Long.parseLong(lowerCase.substring(indexOf + 1).trim());
                            if (parseLong > 0) {
                                j = System.currentTimeMillis() + (parseLong * 1000);
                            }
                        } catch (Throwable th) {
                            LogUtil.m20120e(th.getMessage(), th);
                        }
                    }
                }
            }
        }
        if (j <= 0) {
            j = this.f12664S.getExpiration();
        }
        if (j <= 0 && super.params.getCacheMaxAge() > 0) {
            j = System.currentTimeMillis() + super.params.getCacheMaxAge();
        }
        if (j <= 0) {
            return Long.MAX_VALUE;
        }
        return j;
    }

    public long getHeaderFieldDate(String str, long j) {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection == null) {
            return j;
        }
        return httpURLConnection.getHeaderFieldDate(str, j);
    }

    public InputStream getInputStream() {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection != null && this.f12663R == null) {
            this.f12663R = httpURLConnection.getResponseCode() >= 400 ? this.f12664S.getErrorStream() : this.f12664S.getInputStream();
        }
        return this.f12663R;
    }

    public long getLastModified() {
        return getHeaderFieldDate("Last-Modified", System.currentTimeMillis());
    }

    public String getRequestUri() {
        URL url;
        String str = super.queryUrl;
        HttpURLConnection httpURLConnection = this.f12664S;
        return (httpURLConnection == null || (url = httpURLConnection.getURL()) == null) ? str : url.toString();
    }

    public int getResponseCode() {
        if (this.f12664S != null) {
            return this.f12665T;
        }
        if (getInputStream() != null) {
            return ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        }
        return 404;
    }

    public String getResponseHeader(String str) {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection == null) {
            return null;
        }
        return httpURLConnection.getHeaderField(str);
    }

    public Map<String, List<String>> getResponseHeaders() {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection == null) {
            return null;
        }
        return httpURLConnection.getHeaderFields();
    }

    public String getResponseMessage() {
        HttpURLConnection httpURLConnection = this.f12664S;
        if (httpURLConnection != null) {
            return URLDecoder.decode(httpURLConnection.getResponseMessage(), super.params.getCharset());
        }
        return null;
    }

    public boolean isLoading() {
        return this.f12662Q;
    }

    public Object loadResult() {
        this.f12662Q = true;
        return super.loadResult();
    }

    public Object loadResultFromCache() {
        this.f12662Q = true;
        DiskCacheEntity diskCacheEntity = LruDiskCache.getDiskCache(super.params.getCacheDirName()).setMaxSize(super.params.getCacheSize()).get(getCacheKey());
        if (diskCacheEntity == null) {
            return null;
        }
        if (HttpMethod.permitsCache(super.params.getMethod())) {
            Date lastModify = diskCacheEntity.getLastModify();
            if (lastModify.getTime() > 0) {
                super.params.setHeader("If-Modified-Since", m20201a(lastModify));
            }
            String etag = diskCacheEntity.getEtag();
            if (!TextUtils.isEmpty(etag)) {
                super.params.setHeader("If-None-Match", etag);
            }
        }
        return super.loader.loadFromCache(diskCacheEntity);
    }

    @TargetApi(19)
    public void sendRequest() {
        RequestBody requestBody;
        this.f12662Q = false;
        this.f12665T = 0;
        URL url = new URL(super.queryUrl);
        Proxy proxy = super.params.getProxy();
        if (proxy != null) {
            this.f12664S = (HttpURLConnection) url.openConnection(proxy);
        } else {
            this.f12664S = (HttpURLConnection) url.openConnection();
        }
        if (Build.VERSION.SDK_INT < 19) {
            this.f12664S.setRequestProperty("Connection", "close");
        }
        this.f12664S.setReadTimeout(super.params.getReadTimeout());
        this.f12664S.setConnectTimeout(super.params.getConnectTimeout());
        this.f12664S.setInstanceFollowRedirects(super.params.getRedirectHandler() == null);
        if (this.f12664S instanceof HttpsURLConnection) {
            SSLSocketFactory sslSocketFactory = super.params.getSslSocketFactory();
            if (sslSocketFactory != null) {
                ((HttpsURLConnection) this.f12664S).setSSLSocketFactory(sslSocketFactory);
            }
            HostnameVerifier hostnameVerifier = super.params.getHostnameVerifier();
            if (hostnameVerifier != null) {
                ((HttpsURLConnection) this.f12664S).setHostnameVerifier(hostnameVerifier);
            }
        }
        if (super.params.isUseCookie()) {
            try {
                List list = f12660U.get(url.toURI(), new HashMap(0)).get("Cookie");
                if (list != null) {
                    this.f12664S.setRequestProperty("Cookie", TextUtils.join(";", list));
                }
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
        List<BaseParams.Header> headers = super.params.getHeaders();
        if (headers != null) {
            for (BaseParams.Header header : headers) {
                String str = header.key;
                String valueStr = header.getValueStr();
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(valueStr)) {
                    if (header.setHeader) {
                        this.f12664S.setRequestProperty(str, valueStr);
                    } else {
                        this.f12664S.addRequestProperty(str, valueStr);
                    }
                }
            }
        }
        RequestInterceptListener requestInterceptListener = super.requestInterceptListener;
        if (requestInterceptListener != null) {
            requestInterceptListener.beforeRequest(super);
        }
        HttpMethod method = super.params.getMethod();
        try {
            this.f12664S.setRequestMethod(method.toString());
        } catch (ProtocolException e) {
            Field declaredField = HttpURLConnection.class.getDeclaredField("method");
            declaredField.setAccessible(true);
            declaredField.set(this.f12664S, method.toString());
        }
        if (HttpMethod.permitsRequestBody(method) && (requestBody = super.params.getRequestBody()) != null) {
            if (requestBody instanceof ProgressBody) {
                ((ProgressBody) requestBody).setProgressHandler(super.progressHandler);
            }
            String contentType = requestBody.getContentType();
            if (!TextUtils.isEmpty(contentType)) {
                this.f12664S.setRequestProperty("Content-Type", contentType);
            }
            long contentLength = requestBody.getContentLength();
            if (contentLength < 0) {
                this.f12664S.setChunkedStreamingMode(262144);
            } else if (contentLength < 2147483647L) {
                this.f12664S.setFixedLengthStreamingMode((int) contentLength);
            } else if (Build.VERSION.SDK_INT >= 19) {
                this.f12664S.setFixedLengthStreamingMode(contentLength);
            } else {
                this.f12664S.setChunkedStreamingMode(262144);
            }
            this.f12664S.setRequestProperty("Content-Length", String.valueOf(contentLength));
            this.f12664S.setDoOutput(true);
            requestBody.writeTo(this.f12664S.getOutputStream());
        }
        if (super.params.isUseCookie()) {
            try {
                Map<String, List<String>> headerFields = this.f12664S.getHeaderFields();
                if (headerFields != null) {
                    f12660U.put(url.toURI(), headerFields);
                }
            } catch (Throwable th2) {
                LogUtil.m20120e(th2.getMessage(), th2);
            }
        }
        this.f12665T = this.f12664S.getResponseCode();
        RequestInterceptListener requestInterceptListener2 = super.requestInterceptListener;
        if (requestInterceptListener2 != null) {
            requestInterceptListener2.afterRequest(super);
        }
        int i = this.f12665T;
        if (i == 204 || i == 205) {
            throw new HttpException(this.f12665T, getResponseMessage());
        } else if (i < 300) {
            this.f12662Q = true;
            return;
        } else {
            HttpException httpException = new HttpException(i, getResponseMessage());
            try {
                httpException.setResult(IOUtil.readStr(getInputStream(), super.params.getCharset()));
            } catch (Throwable unused) {
            }
            LogUtil.m20119e(httpException.toString() + ", url: " + super.queryUrl);
            throw httpException;
        }
    }
}
