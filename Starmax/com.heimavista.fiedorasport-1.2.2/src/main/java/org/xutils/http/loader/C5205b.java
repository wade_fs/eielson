package org.xutils.http.loader;

import java.io.InputStream;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.b */
/* compiled from: ByteArrayLoader */
class C5205b extends Loader<byte[]> {
    C5205b() {
    }

    public byte[] loadFromCache(DiskCacheEntity diskCacheEntity) {
        return null;
    }

    public Loader<byte[]> newInstance() {
        return new C5205b();
    }

    public void save2Cache(UriRequest uriRequest) {
    }

    public byte[] load(InputStream inputStream) {
        return IOUtil.readBytes(inputStream);
    }

    public byte[] load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return load(uriRequest.getInputStream());
    }
}
