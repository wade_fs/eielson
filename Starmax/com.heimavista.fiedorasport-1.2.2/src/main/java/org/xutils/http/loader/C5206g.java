package org.xutils.http.loader;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.InputStream;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.g */
/* compiled from: StringLoader */
class C5206g extends Loader<String> {

    /* renamed from: a */
    private String f12656a = C1750C.UTF8_NAME;

    /* renamed from: b */
    private String f12657b = null;

    C5206g() {
    }

    public Loader<String> newInstance() {
        return new C5206g();
    }

    public void save2Cache(UriRequest uriRequest) {
        saveStringCache(uriRequest, this.f12657b);
    }

    public void setParams(RequestParams requestParams) {
        if (requestParams != null) {
            String charset = requestParams.getCharset();
            if (!TextUtils.isEmpty(charset)) {
                this.f12656a = charset;
            }
        }
    }

    public String loadFromCache(DiskCacheEntity diskCacheEntity) {
        if (diskCacheEntity != null) {
            return diskCacheEntity.getTextContent();
        }
        return null;
    }

    public String load(InputStream inputStream) {
        this.f12657b = IOUtil.readStr(inputStream, this.f12656a);
        return this.f12657b;
    }

    public String load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return load(uriRequest.getInputStream());
    }
}
