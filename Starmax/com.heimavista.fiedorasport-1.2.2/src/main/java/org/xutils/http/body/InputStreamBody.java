package org.xutils.http.body;

import android.text.TextUtils;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.xutils.common.Callback;
import org.xutils.common.util.IOUtil;
import org.xutils.http.ProgressHandler;

public class InputStreamBody implements ProgressBody {

    /* renamed from: a */
    private InputStream f12612a;

    /* renamed from: b */
    private String f12613b;

    /* renamed from: c */
    private final long f12614c;

    /* renamed from: d */
    private long f12615d;

    /* renamed from: e */
    private ProgressHandler f12616e;

    public InputStreamBody(InputStream inputStream) {
        this(inputStream, null);
    }

    public static long getInputStreamLength(InputStream inputStream) {
        try {
            if ((inputStream instanceof FileInputStream) || (inputStream instanceof ByteArrayInputStream)) {
                return (long) inputStream.available();
            }
            return -1;
        } catch (Throwable unused) {
            return -1;
        }
    }

    public long getContentLength() {
        return this.f12614c;
    }

    public String getContentType() {
        return TextUtils.isEmpty(this.f12613b) ? "application/octet-stream" : this.f12613b;
    }

    public void setContentType(String str) {
        this.f12613b = str;
    }

    public void setProgressHandler(ProgressHandler progressHandler) {
        this.f12616e = progressHandler;
    }

    public void writeTo(OutputStream outputStream) {
        ProgressHandler progressHandler = this.f12616e;
        if (progressHandler == null || progressHandler.updateProgress(this.f12614c, this.f12615d, true)) {
            byte[] bArr = new byte[1024];
            while (true) {
                try {
                    int read = this.f12612a.read(bArr);
                    if (read != -1) {
                        outputStream.write(bArr, 0, read);
                        this.f12615d += (long) read;
                        if (this.f12616e != null) {
                            if (!this.f12616e.updateProgress(this.f12614c, this.f12615d, false)) {
                                throw new Callback.CancelledException("upload stopped!");
                            }
                        }
                    } else {
                        outputStream.flush();
                        if (this.f12616e != null) {
                            this.f12616e.updateProgress(this.f12614c, this.f12614c, true);
                        }
                        return;
                    }
                } finally {
                    IOUtil.closeQuietly(this.f12612a);
                }
            }
        } else {
            throw new Callback.CancelledException("upload stopped!");
        }
    }

    public InputStreamBody(InputStream inputStream, String str) {
        this.f12615d = 0;
        this.f12612a = inputStream;
        this.f12613b = str;
        this.f12614c = getInputStreamLength(inputStream);
    }
}
