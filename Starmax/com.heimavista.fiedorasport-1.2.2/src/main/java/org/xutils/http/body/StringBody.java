package org.xutils.http.body;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.OutputStream;

public class StringBody implements RequestBody {

    /* renamed from: a */
    private byte[] f12628a;

    /* renamed from: b */
    private String f12629b;

    /* renamed from: c */
    private String f12630c = C1750C.UTF8_NAME;

    public StringBody(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.f12630c = str2;
        }
        this.f12628a = str.getBytes(this.f12630c);
    }

    public long getContentLength() {
        return (long) this.f12628a.length;
    }

    public String getContentType() {
        if (!TextUtils.isEmpty(this.f12629b)) {
            return this.f12629b;
        }
        return "application/json;charset=" + this.f12630c;
    }

    public void setContentType(String str) {
        this.f12629b = str;
    }

    public void writeTo(OutputStream outputStream) {
        outputStream.write(this.f12628a);
        outputStream.flush();
    }
}
