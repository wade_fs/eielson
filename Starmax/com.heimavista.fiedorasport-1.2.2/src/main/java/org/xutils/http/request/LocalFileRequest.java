package org.xutils.http.request;

import androidx.recyclerview.widget.ItemTouchHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.loader.FileLoader;
import org.xutils.http.loader.Loader;

public class LocalFileRequest extends UriRequest {

    /* renamed from: P */
    private InputStream f12666P;

    LocalFileRequest(RequestParams requestParams, Type type) {
        super(requestParams, type);
    }

    /* renamed from: a */
    private File m20202a() {
        String str;
        if (super.queryUrl.startsWith("file:")) {
            str = super.queryUrl.substring(5);
        } else {
            str = super.queryUrl;
        }
        return new File(str);
    }

    public void clearCacheHeader() {
    }

    public void close() {
        IOUtil.closeQuietly(this.f12666P);
        this.f12666P = null;
    }

    public String getCacheKey() {
        return null;
    }

    public long getContentLength() {
        return m20202a().length();
    }

    public String getETag() {
        return null;
    }

    public long getExpiration() {
        return -1;
    }

    public long getHeaderFieldDate(String str, long j) {
        return j;
    }

    public InputStream getInputStream() {
        if (this.f12666P == null) {
            this.f12666P = new FileInputStream(m20202a());
        }
        return this.f12666P;
    }

    public long getLastModified() {
        return m20202a().lastModified();
    }

    public int getResponseCode() {
        if (m20202a().exists()) {
            return ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        }
        return 404;
    }

    public String getResponseHeader(String str) {
        return null;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return null;
    }

    public String getResponseMessage() {
        return null;
    }

    public boolean isLoading() {
        return true;
    }

    public Object loadResult() {
        Loader<?> loader = super.loader;
        if (loader instanceof FileLoader) {
            return m20202a();
        }
        return loader.load(super);
    }

    public Object loadResultFromCache() {
        return null;
    }

    public void save2Cache() {
    }

    public void sendRequest() {
    }
}
