package org.xutils.http.loader;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.InputStream;
import org.json.JSONArray;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.d */
class JSONArrayLoader extends Loader<JSONArray> {

    /* renamed from: a */
    private String f12647a = C1750C.UTF8_NAME;

    /* renamed from: b */
    private String f12648b = null;

    JSONArrayLoader() {
    }

    public Loader<JSONArray> newInstance() {
        return new JSONArrayLoader();
    }

    public void save2Cache(UriRequest uriRequest) {
        saveStringCache(uriRequest, this.f12648b);
    }

    public void setParams(RequestParams requestParams) {
        if (requestParams != null) {
            String charset = requestParams.getCharset();
            if (!TextUtils.isEmpty(charset)) {
                this.f12647a = charset;
            }
        }
    }

    public JSONArray loadFromCache(DiskCacheEntity diskCacheEntity) {
        if (diskCacheEntity == null) {
            return null;
        }
        String textContent = diskCacheEntity.getTextContent();
        if (!TextUtils.isEmpty(textContent)) {
            return new JSONArray(textContent);
        }
        return null;
    }

    public JSONArray load(InputStream inputStream) {
        this.f12648b = IOUtil.readStr(inputStream, this.f12647a);
        return new JSONArray(this.f12648b);
    }

    public JSONArray load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return load(uriRequest.getInputStream());
    }
}
