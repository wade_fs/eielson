package org.xutils.http;

import java.lang.reflect.Type;
import org.xutils.C5217x;
import org.xutils.HttpManager;
import org.xutils.common.Callback;

public final class HttpManagerImpl implements HttpManager {

    /* renamed from: a */
    private static final Object f12543a = new Object();

    /* renamed from: b */
    private static volatile HttpManagerImpl f12544b;

    /* renamed from: org.xutils.http.HttpManagerImpl$a */
    private class C5195a<T> implements Callback.TypedCallback<T> {

        /* renamed from: a */
        private final Class<T> f12545a;

        public C5195a(HttpManagerImpl httpManagerImpl, Class<T> cls) {
            this.f12545a = cls;
        }

        public Type getLoadType() {
            return this.f12545a;
        }

        public void onCancelled(Callback.CancelledException cancelledException) {
        }

        public void onError(Throwable th, boolean z) {
        }

        public void onFinished() {
        }

        public void onSuccess(T t) {
        }
    }

    private HttpManagerImpl() {
    }

    public static void registerInstance() {
        if (f12544b == null) {
            synchronized (f12543a) {
                if (f12544b == null) {
                    f12544b = new HttpManagerImpl();
                }
            }
        }
        C5217x.Ext.setHttpManager(f12544b);
    }

    public <T> Callback.Cancelable get(RequestParams requestParams, Callback.CommonCallback<T> commonCallback) {
        return request(HttpMethod.GET, requestParams, commonCallback);
    }

    public <T> T getSync(RequestParams requestParams, Class<T> cls) {
        return requestSync(HttpMethod.GET, requestParams, cls);
    }

    public <T> Callback.Cancelable post(RequestParams requestParams, Callback.CommonCallback<T> commonCallback) {
        return request(HttpMethod.POST, requestParams, commonCallback);
    }

    public <T> T postSync(RequestParams requestParams, Class<T> cls) {
        return requestSync(HttpMethod.POST, requestParams, cls);
    }

    public <T> Callback.Cancelable request(HttpMethod httpMethod, RequestParams requestParams, Callback.CommonCallback<T> commonCallback) {
        requestParams.setMethod(httpMethod);
        return C5217x.task().start(new HttpTask(requestParams, commonCallback instanceof Callback.Cancelable ? (Callback.Cancelable) commonCallback : null, commonCallback));
    }

    public <T> T requestSync(HttpMethod httpMethod, RequestParams requestParams, Class<T> cls) {
        return requestSync(httpMethod, requestParams, new C5195a(this, cls));
    }

    public <T> T requestSync(HttpMethod httpMethod, RequestParams requestParams, Callback.TypedCallback<T> typedCallback) {
        requestParams.setMethod(httpMethod);
        return C5217x.task().startSync(new HttpTask(requestParams, null, typedCallback));
    }
}
