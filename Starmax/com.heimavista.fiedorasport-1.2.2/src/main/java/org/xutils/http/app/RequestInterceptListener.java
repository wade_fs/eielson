package org.xutils.http.app;

import org.xutils.http.request.UriRequest;

public interface RequestInterceptListener {
    void afterRequest(UriRequest uriRequest);

    void beforeRequest(UriRequest uriRequest);
}
