package org.xutils.http.request;

import java.io.Closeable;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import org.xutils.C5217x;
import org.xutils.common.util.LogUtil;
import org.xutils.http.ProgressHandler;
import org.xutils.http.RequestParams;
import org.xutils.http.app.RequestInterceptListener;
import org.xutils.http.loader.Loader;
import org.xutils.http.loader.LoaderFactory;

public abstract class UriRequest implements Closeable {
    protected ClassLoader callingClassLoader = null;
    protected final Loader<?> loader;
    protected final RequestParams params;
    protected ProgressHandler progressHandler = null;
    protected final String queryUrl;
    protected RequestInterceptListener requestInterceptListener = null;

    /* renamed from: org.xutils.http.request.UriRequest$a */
    class C5207a implements Runnable {
        C5207a() {
        }

        public void run() {
            try {
                UriRequest.this.loader.save2Cache(UriRequest.this);
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
    }

    UriRequest(RequestParams requestParams, Type type) {
        this.params = requestParams;
        this.queryUrl = buildQueryUrl(requestParams);
        this.loader = LoaderFactory.getLoader(type, requestParams);
    }

    /* access modifiers changed from: protected */
    public String buildQueryUrl(RequestParams requestParams) {
        return requestParams.getUri();
    }

    public abstract void clearCacheHeader();

    public abstract void close();

    public abstract String getCacheKey();

    public abstract long getContentLength();

    public abstract String getETag();

    public abstract long getExpiration();

    public abstract long getHeaderFieldDate(String str, long j);

    public abstract InputStream getInputStream();

    public abstract long getLastModified();

    public RequestParams getParams() {
        return this.params;
    }

    public String getRequestUri() {
        return this.queryUrl;
    }

    public abstract int getResponseCode();

    public abstract String getResponseHeader(String str);

    public abstract Map<String, List<String>> getResponseHeaders();

    public abstract String getResponseMessage();

    public abstract boolean isLoading();

    public Object loadResult() {
        return this.loader.load(this);
    }

    public abstract Object loadResultFromCache();

    public void save2Cache() {
        C5217x.task().run(new C5207a());
    }

    public abstract void sendRequest();

    public void setCallingClassLoader(ClassLoader classLoader) {
        this.callingClassLoader = classLoader;
    }

    public void setProgressHandler(ProgressHandler progressHandler2) {
        this.progressHandler = progressHandler2;
        this.loader.setProgressHandler(progressHandler2);
    }

    public void setRequestInterceptListener(RequestInterceptListener requestInterceptListener2) {
        this.requestInterceptListener = requestInterceptListener2;
    }

    public String toString() {
        return getRequestUri();
    }
}
