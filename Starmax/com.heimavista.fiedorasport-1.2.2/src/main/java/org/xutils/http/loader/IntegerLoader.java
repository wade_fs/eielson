package org.xutils.http.loader;

import java.io.InputStream;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.c */
class IntegerLoader extends Loader<Integer> {
    IntegerLoader() {
    }

    public Integer loadFromCache(DiskCacheEntity diskCacheEntity) {
        return null;
    }

    public Loader<Integer> newInstance() {
        return new IntegerLoader();
    }

    public void save2Cache(UriRequest uriRequest) {
    }

    public Integer load(InputStream inputStream) {
        return 100;
    }

    public Integer load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return Integer.valueOf(uriRequest.getResponseCode());
    }
}
