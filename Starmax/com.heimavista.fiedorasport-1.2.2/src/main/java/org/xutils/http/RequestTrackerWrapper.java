package org.xutils.http;

import org.xutils.common.util.LogUtil;
import org.xutils.http.app.RequestTracker;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.b */
final class RequestTrackerWrapper implements RequestTracker {

    /* renamed from: a */
    private final RequestTracker f12606a;

    public RequestTrackerWrapper(RequestTracker requestTracker) {
        this.f12606a = requestTracker;
    }

    public void onCache(UriRequest uriRequest, Object obj) {
        try {
            this.f12606a.onCache(uriRequest, obj);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onCancelled(UriRequest uriRequest) {
        try {
            this.f12606a.onCancelled(uriRequest);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onError(UriRequest uriRequest, Throwable th, boolean z) {
        try {
            this.f12606a.onError(uriRequest, th, z);
        } catch (Throwable th2) {
            LogUtil.m20120e(th2.getMessage(), th2);
        }
    }

    public void onFinished(UriRequest uriRequest) {
        try {
            this.f12606a.onFinished(uriRequest);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onRequestCreated(UriRequest uriRequest) {
        try {
            this.f12606a.onRequestCreated(uriRequest);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onStart(RequestParams requestParams) {
        try {
            this.f12606a.onStart(requestParams);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onSuccess(UriRequest uriRequest, Object obj) {
        try {
            this.f12606a.onSuccess(uriRequest, obj);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public void onWaiting(RequestParams requestParams) {
        try {
            this.f12606a.onWaiting(requestParams);
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }
}
