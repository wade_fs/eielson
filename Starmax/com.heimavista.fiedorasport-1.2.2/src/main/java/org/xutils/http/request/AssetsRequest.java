package org.xutils.http.request;

import androidx.recyclerview.widget.ItemTouchHelper;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.xutils.C5217x;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;

public class AssetsRequest extends UriRequest {

    /* renamed from: P */
    private long f12658P = 0;

    /* renamed from: Q */
    private InputStream f12659Q;

    public AssetsRequest(RequestParams requestParams, Type type) {
        super(requestParams, type);
    }

    public void clearCacheHeader() {
    }

    public void close() {
        IOUtil.closeQuietly(this.f12659Q);
        this.f12659Q = null;
    }

    /* access modifiers changed from: protected */
    public long getAssetsLastModified() {
        return new File(C5217x.app().getApplicationInfo().sourceDir).lastModified();
    }

    public String getCacheKey() {
        return super.queryUrl;
    }

    public long getContentLength() {
        try {
            getInputStream();
            return this.f12658P;
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            return 0;
        }
    }

    public String getETag() {
        return null;
    }

    public long getExpiration() {
        return Long.MAX_VALUE;
    }

    public long getHeaderFieldDate(String str, long j) {
        return j;
    }

    public InputStream getInputStream() {
        if (this.f12659Q == null && super.callingClassLoader != null) {
            this.f12659Q = super.callingClassLoader.getResourceAsStream("assets/" + super.queryUrl.substring(9));
            this.f12658P = (long) this.f12659Q.available();
        }
        return this.f12659Q;
    }

    public long getLastModified() {
        return getAssetsLastModified();
    }

    public int getResponseCode() {
        if (getInputStream() != null) {
            return ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        }
        return 404;
    }

    public String getResponseHeader(String str) {
        return null;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return null;
    }

    public String getResponseMessage() {
        return null;
    }

    public boolean isLoading() {
        return true;
    }

    public Object loadResult() {
        return super.loader.load(super);
    }

    public Object loadResultFromCache() {
        Date lastModify;
        DiskCacheEntity diskCacheEntity = LruDiskCache.getDiskCache(super.params.getCacheDirName()).setMaxSize(super.params.getCacheSize()).get(getCacheKey());
        if (diskCacheEntity == null || (lastModify = diskCacheEntity.getLastModify()) == null || lastModify.getTime() < getAssetsLastModified()) {
            return null;
        }
        return super.loader.loadFromCache(diskCacheEntity);
    }

    public void sendRequest() {
    }
}
