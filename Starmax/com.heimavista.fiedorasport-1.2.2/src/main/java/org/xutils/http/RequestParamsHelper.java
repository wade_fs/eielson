package org.xutils.http;

import android.os.Parcelable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.LogUtil;

/* renamed from: org.xutils.http.a */
final class RequestParamsHelper {

    /* renamed from: a */
    private static final ClassLoader f12603a = String.class.getClassLoader();

    /* renamed from: org.xutils.http.a$a */
    /* compiled from: RequestParamsHelper */
    static class C5200a implements C5201b {

        /* renamed from: a */
        final /* synthetic */ JSONObject f12604a;

        C5200a(JSONObject jSONObject) {
            this.f12604a = jSONObject;
        }

        public void onParseKV(String str, Object obj) {
            try {
                this.f12604a.put(str, RequestParamsHelper.m20181a(obj));
            } catch (JSONException e) {
                throw new IllegalArgumentException("parse RequestParams to json failed", e);
            }
        }
    }

    /* renamed from: org.xutils.http.a$b */
    /* compiled from: RequestParamsHelper */
    interface C5201b {
        void onParseKV(String str, Object obj);
    }

    /* renamed from: a */
    static void m20182a(Object obj, Class<?> cls, C5201b bVar) {
        ClassLoader classLoader;
        if (obj != null && cls != null && cls != RequestParams.class && cls != Object.class && (classLoader = cls.getClassLoader()) != null && classLoader != f12603a) {
            Field[] declaredFields = cls.getDeclaredFields();
            if (declaredFields != null && declaredFields.length > 0) {
                for (Field field : declaredFields) {
                    if (!Modifier.isTransient(field.getModifiers()) && field.getType() != Parcelable.Creator.class) {
                        field.setAccessible(true);
                        try {
                            String name = field.getName();
                            Object obj2 = field.get(obj);
                            if (obj2 != null) {
                                bVar.onParseKV(name, obj2);
                            }
                        } catch (IllegalAccessException e) {
                            LogUtil.m20120e(e.getMessage(), e);
                        }
                    }
                }
            }
            m20182a(obj, cls.getSuperclass(), bVar);
        }
    }

    /* renamed from: a */
    static Object m20181a(Object obj) {
        if (obj == null) {
            return null;
        }
        Class<?> cls = obj.getClass();
        if (cls.isArray()) {
            JSONArray jSONArray = new JSONArray();
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                jSONArray.put(m20181a(Array.get(obj, i)));
            }
            return jSONArray;
        } else if (obj instanceof Iterable) {
            JSONArray jSONArray2 = new JSONArray();
            for (Object obj2 : (Iterable) obj) {
                jSONArray2.put(m20181a(obj2));
            }
            return jSONArray2;
        } else if (obj instanceof Map) {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (!(key == null || value == null)) {
                    jSONObject.put(String.valueOf(key), m20181a(value));
                }
            }
            return jSONObject;
        } else {
            ClassLoader classLoader = cls.getClassLoader();
            if (classLoader == null || classLoader == f12603a) {
                return obj;
            }
            JSONObject jSONObject2 = new JSONObject();
            m20182a(obj, cls, new C5200a(jSONObject2));
            return jSONObject2;
        }
    }
}
