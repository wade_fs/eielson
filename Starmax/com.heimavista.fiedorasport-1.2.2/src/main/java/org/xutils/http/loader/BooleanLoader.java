package org.xutils.http.loader;

import java.io.InputStream;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.a */
class BooleanLoader extends Loader<Boolean> {
    BooleanLoader() {
    }

    public Boolean loadFromCache(DiskCacheEntity diskCacheEntity) {
        return null;
    }

    public Loader<Boolean> newInstance() {
        return new BooleanLoader();
    }

    public void save2Cache(UriRequest uriRequest) {
    }

    public Boolean load(InputStream inputStream) {
        return false;
    }

    public Boolean load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return Boolean.valueOf(uriRequest.getResponseCode() < 300);
    }
}
