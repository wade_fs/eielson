package org.xutils.http.body;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import org.xutils.common.util.KeyValue;

public class UrlEncodedParamsBody implements RequestBody {

    /* renamed from: a */
    private byte[] f12631a;

    /* renamed from: b */
    private String f12632b = C1750C.UTF8_NAME;

    public UrlEncodedParamsBody(List<KeyValue> list, String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f12632b = str;
        }
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (KeyValue keyValue : list) {
                String str2 = keyValue.key;
                String valueStr = keyValue.getValueStr();
                if (!TextUtils.isEmpty(str2) && valueStr != null) {
                    if (sb.length() > 0) {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(str2, this.f12632b).replaceAll("\\+", "%20"));
                    sb.append("=");
                    sb.append(URLEncoder.encode(valueStr, this.f12632b).replaceAll("\\+", "%20"));
                }
            }
        }
        this.f12631a = sb.toString().getBytes(this.f12632b);
    }

    public long getContentLength() {
        return (long) this.f12631a.length;
    }

    public String getContentType() {
        return "application/x-www-form-urlencoded;charset=" + this.f12632b;
    }

    public void setContentType(String str) {
    }

    public void writeTo(OutputStream outputStream) {
        outputStream.write(this.f12631a);
        outputStream.flush();
    }
}
