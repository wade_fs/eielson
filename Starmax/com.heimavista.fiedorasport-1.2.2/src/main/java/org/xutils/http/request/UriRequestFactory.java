package org.xutils.http.request;

import android.text.TextUtils;
import java.lang.reflect.Type;
import java.util.HashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.app.RequestTracker;

public final class UriRequestFactory {

    /* renamed from: a */
    private static Class<? extends RequestTracker> f12668a;

    /* renamed from: b */
    private static final HashMap<String, Class<? extends UriRequest>> f12669b = new HashMap<>();

    private UriRequestFactory() {
    }

    public static RequestTracker getDefaultTracker() {
        try {
            if (f12668a == null) {
                return null;
            }
            return (RequestTracker) f12668a.newInstance();
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            return null;
        }
    }

    public static UriRequest getUriRequest(RequestParams requestParams, Type type) {
        String str;
        String uri = requestParams.getUri();
        int indexOf = uri.indexOf(":");
        if (indexOf > 0) {
            str = uri.substring(0, indexOf);
        } else if (uri.startsWith("/")) {
            str = "file";
        } else {
            str = null;
        }
        if (!TextUtils.isEmpty(str)) {
            Class cls = f12669b.get(str);
            if (cls != null) {
                return (UriRequest) cls.getConstructor(RequestParams.class, Class.class).newInstance(requestParams, type);
            } else if (str.startsWith("http")) {
                return new HttpRequest(requestParams, type);
            } else {
                if (str.equals("assets")) {
                    return new AssetsRequest(requestParams, type);
                }
                if (str.equals("file")) {
                    return new LocalFileRequest(requestParams, type);
                }
                throw new IllegalArgumentException("The url not be support: " + uri);
            }
        } else {
            throw new IllegalArgumentException("The url not be support: " + uri);
        }
    }

    public static void registerDefaultTrackerClass(Class<? extends RequestTracker> cls) {
        f12668a = cls;
    }

    public static void registerRequestClass(String str, Class<? extends UriRequest> cls) {
        f12669b.put(str, cls);
    }
}
