package org.xutils.http.app;

import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpRequest;

public class DefaultParamsBuilder implements ParamsBuilder {

    /* renamed from: a */
    private static SSLSocketFactory f12605a;

    /* renamed from: org.xutils.http.app.DefaultParamsBuilder$a */
    static class C5202a implements X509TrustManager {
        C5202a() {
        }

        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    public static SSLSocketFactory getTrustAllSSLSocketFactory() {
        if (f12605a == null) {
            synchronized (DefaultParamsBuilder.class) {
                if (f12605a == null) {
                    TrustManager[] trustManagerArr = {new C5202a()};
                    try {
                        SSLContext instance = SSLContext.getInstance("TLS");
                        instance.init(null, trustManagerArr, null);
                        f12605a = instance.getSocketFactory();
                    } catch (Throwable th) {
                        LogUtil.m20120e(th.getMessage(), th);
                    }
                }
            }
        }
        return f12605a;
    }

    public String buildCacheKey(RequestParams requestParams, String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return null;
        }
        String str = requestParams.getUri() + "?";
        for (String str2 : strArr) {
            String stringParameter = requestParams.getStringParameter(str2);
            if (stringParameter != null) {
                str = str + str2 + "=" + stringParameter + "&";
            }
        }
        return str;
    }

    public void buildParams(RequestParams requestParams) {
    }

    public void buildSign(RequestParams requestParams, String[] strArr) {
    }

    public String buildUri(RequestParams requestParams, HttpRequest httpRequest) {
        return httpRequest.host() + "/" + httpRequest.path();
    }

    public SSLSocketFactory getSSLSocketFactory() {
        return getTrustAllSSLSocketFactory();
    }
}
