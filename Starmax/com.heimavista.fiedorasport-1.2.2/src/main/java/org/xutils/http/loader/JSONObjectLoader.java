package org.xutils.http.loader;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.InputStream;
import org.json.JSONObject;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

/* renamed from: org.xutils.http.loader.e */
class JSONObjectLoader extends Loader<JSONObject> {

    /* renamed from: a */
    private String f12649a = C1750C.UTF8_NAME;

    /* renamed from: b */
    private String f12650b = null;

    JSONObjectLoader() {
    }

    public Loader<JSONObject> newInstance() {
        return new JSONObjectLoader();
    }

    public void save2Cache(UriRequest uriRequest) {
        saveStringCache(uriRequest, this.f12650b);
    }

    public void setParams(RequestParams requestParams) {
        if (requestParams != null) {
            String charset = requestParams.getCharset();
            if (!TextUtils.isEmpty(charset)) {
                this.f12649a = charset;
            }
        }
    }

    public JSONObject loadFromCache(DiskCacheEntity diskCacheEntity) {
        if (diskCacheEntity == null) {
            return null;
        }
        String textContent = diskCacheEntity.getTextContent();
        if (!TextUtils.isEmpty(textContent)) {
            return new JSONObject(textContent);
        }
        return null;
    }

    public JSONObject load(InputStream inputStream) {
        this.f12650b = IOUtil.readStr(inputStream, this.f12649a);
        return new JSONObject(this.f12650b);
    }

    public JSONObject load(UriRequest uriRequest) {
        uriRequest.sendRequest();
        return load(uriRequest.getInputStream());
    }
}
