package org.xutils.http;

import android.text.TextUtils;
import java.io.File;
import java.net.Proxy;
import java.util.List;
import java.util.concurrent.Executor;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import org.xutils.common.task.Priority;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParamsHelper;
import org.xutils.http.annotation.HttpRequest;
import org.xutils.http.app.DefaultParamsBuilder;
import org.xutils.http.app.HttpRetryHandler;
import org.xutils.http.app.ParamsBuilder;
import org.xutils.http.app.RedirectHandler;
import org.xutils.http.app.RequestTracker;
import org.xutils.http.body.RequestBody;

public class RequestParams extends BaseParams {

    /* renamed from: A */
    private int f12574A;

    /* renamed from: B */
    private int f12575B;

    /* renamed from: C */
    private boolean f12576C;

    /* renamed from: D */
    private boolean f12577D;

    /* renamed from: E */
    private int f12578E;

    /* renamed from: F */
    private String f12579F;

    /* renamed from: G */
    private boolean f12580G;

    /* renamed from: H */
    private int f12581H;

    /* renamed from: I */
    private HttpRetryHandler f12582I;

    /* renamed from: J */
    private RedirectHandler f12583J;

    /* renamed from: K */
    private RequestTracker f12584K;

    /* renamed from: L */
    private boolean f12585L;

    /* renamed from: k */
    private HttpRequest f12586k;

    /* renamed from: l */
    private String f12587l;

    /* renamed from: m */
    private final String[] f12588m;

    /* renamed from: n */
    private final String[] f12589n;

    /* renamed from: o */
    private ParamsBuilder f12590o;

    /* renamed from: p */
    private String f12591p;

    /* renamed from: q */
    private String f12592q;

    /* renamed from: r */
    private SSLSocketFactory f12593r;

    /* renamed from: s */
    private Proxy f12594s;

    /* renamed from: t */
    private HostnameVerifier f12595t;

    /* renamed from: u */
    private boolean f12596u;

    /* renamed from: v */
    private String f12597v;

    /* renamed from: w */
    private long f12598w;

    /* renamed from: x */
    private long f12599x;

    /* renamed from: y */
    private Executor f12600y;

    /* renamed from: z */
    private Priority f12601z;

    /* renamed from: org.xutils.http.RequestParams$a */
    class C5199a implements RequestParamsHelper.C5201b {
        C5199a() {
        }

        public void onParseKV(String str, Object obj) {
            RequestParams.this.addParameter(str, obj);
        }
    }

    public RequestParams() {
        this(null, null, null, null);
    }

    /* renamed from: b */
    private HttpRequest m20178b() {
        Class<RequestParams> cls = RequestParams.class;
        if (this.f12586k == null && !this.f12585L) {
            this.f12585L = true;
            if (cls != cls) {
                this.f12586k = (HttpRequest) cls.getAnnotation(HttpRequest.class);
            }
        }
        return this.f12586k;
    }

    /* renamed from: c */
    private void m20179c() {
        RequestParamsHelper.m20182a(this, RequestParams.class, new C5199a());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28774a() {
        if (TextUtils.isEmpty(this.f12591p)) {
            if (!TextUtils.isEmpty(this.f12587l) || m20178b() != null) {
                m20179c();
                this.f12591p = this.f12587l;
                HttpRequest b = m20178b();
                if (b != null) {
                    this.f12590o = (ParamsBuilder) b.builder().newInstance();
                    this.f12591p = this.f12590o.buildUri(this, b);
                    this.f12590o.buildParams(this);
                    this.f12590o.buildSign(this, b.signs());
                    if (this.f12593r == null) {
                        this.f12593r = this.f12590o.getSSLSocketFactory();
                        return;
                    }
                    return;
                }
                ParamsBuilder paramsBuilder = this.f12590o;
                if (paramsBuilder != null) {
                    paramsBuilder.buildParams(this);
                    this.f12590o.buildSign(this, this.f12588m);
                    if (this.f12593r == null) {
                        this.f12593r = this.f12590o.getSSLSocketFactory();
                        return;
                    }
                    return;
                }
                return;
            }
            throw new IllegalStateException("uri is empty && @HttpRequest == null");
        }
    }

    public /* bridge */ /* synthetic */ void addBodyParameter(String str, File file) {
        super.addBodyParameter(str, file);
    }

    public /* bridge */ /* synthetic */ void addHeader(String str, String str2) {
        super.addHeader(str, str2);
    }

    public /* bridge */ /* synthetic */ void addParameter(String str, Object obj) {
        super.addParameter(str, obj);
    }

    public /* bridge */ /* synthetic */ void addQueryStringParameter(String str, String str2) {
        super.addQueryStringParameter(str, str2);
    }

    public /* bridge */ /* synthetic */ void clearParams() {
        super.clearParams();
    }

    public /* bridge */ /* synthetic */ String getBodyContent() {
        return super.getBodyContent();
    }

    public /* bridge */ /* synthetic */ List getBodyParams() {
        return super.getBodyParams();
    }

    public String getCacheDirName() {
        return this.f12597v;
    }

    public String getCacheKey() {
        if (TextUtils.isEmpty(this.f12592q) && this.f12590o != null) {
            HttpRequest b = m20178b();
            if (b != null) {
                this.f12592q = this.f12590o.buildCacheKey(this, b.cacheKeys());
            } else {
                this.f12592q = this.f12590o.buildCacheKey(this, this.f12589n);
            }
        }
        return this.f12592q;
    }

    public long getCacheMaxAge() {
        return this.f12599x;
    }

    public long getCacheSize() {
        return this.f12598w;
    }

    public /* bridge */ /* synthetic */ String getCharset() {
        return super.getCharset();
    }

    public int getConnectTimeout() {
        return this.f12574A;
    }

    public Executor getExecutor() {
        return this.f12600y;
    }

    public /* bridge */ /* synthetic */ List getFileParams() {
        return super.getFileParams();
    }

    public /* bridge */ /* synthetic */ List getHeaders() {
        return super.getHeaders();
    }

    public HostnameVerifier getHostnameVerifier() {
        return this.f12595t;
    }

    public HttpRetryHandler getHttpRetryHandler() {
        return this.f12582I;
    }

    public int getLoadingUpdateMaxTimeSpan() {
        return this.f12581H;
    }

    public int getMaxRetryCount() {
        return this.f12578E;
    }

    public /* bridge */ /* synthetic */ HttpMethod getMethod() {
        return super.getMethod();
    }

    public /* bridge */ /* synthetic */ List getParams(String str) {
        return super.getParams(str);
    }

    public Priority getPriority() {
        return this.f12601z;
    }

    public Proxy getProxy() {
        return this.f12594s;
    }

    public /* bridge */ /* synthetic */ List getQueryStringParams() {
        return super.getQueryStringParams();
    }

    public int getReadTimeout() {
        return this.f12575B;
    }

    public RedirectHandler getRedirectHandler() {
        return this.f12583J;
    }

    public /* bridge */ /* synthetic */ RequestBody getRequestBody() {
        return super.getRequestBody();
    }

    public RequestTracker getRequestTracker() {
        return this.f12584K;
    }

    public String getSaveFilePath() {
        return this.f12579F;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return this.f12593r;
    }

    public /* bridge */ /* synthetic */ String getStringParameter(String str) {
        return super.getStringParameter(str);
    }

    public /* bridge */ /* synthetic */ List getStringParams() {
        return super.getStringParams();
    }

    public String getUri() {
        return TextUtils.isEmpty(this.f12591p) ? this.f12587l : this.f12591p;
    }

    public /* bridge */ /* synthetic */ boolean isAsJsonContent() {
        return super.isAsJsonContent();
    }

    public boolean isAutoRename() {
        return this.f12577D;
    }

    public boolean isAutoResume() {
        return this.f12576C;
    }

    public boolean isCancelFast() {
        return this.f12580G;
    }

    public /* bridge */ /* synthetic */ boolean isMultipart() {
        return super.isMultipart();
    }

    public boolean isUseCookie() {
        return this.f12596u;
    }

    public /* bridge */ /* synthetic */ void removeParameter(String str) {
        super.removeParameter(str);
    }

    public /* bridge */ /* synthetic */ void setAsJsonContent(boolean z) {
        super.setAsJsonContent(z);
    }

    public void setAutoRename(boolean z) {
        this.f12577D = z;
    }

    public void setAutoResume(boolean z) {
        this.f12576C = z;
    }

    public /* bridge */ /* synthetic */ void setBodyContent(String str) {
        super.setBodyContent(str);
    }

    public void setCacheDirName(String str) {
        this.f12597v = str;
    }

    public void setCacheMaxAge(long j) {
        this.f12599x = j;
    }

    public void setCacheSize(long j) {
        this.f12598w = j;
    }

    public void setCancelFast(boolean z) {
        this.f12580G = z;
    }

    public /* bridge */ /* synthetic */ void setCharset(String str) {
        super.setCharset(str);
    }

    public void setConnectTimeout(int i) {
        if (i > 0) {
            this.f12574A = i;
        }
    }

    public void setExecutor(Executor executor) {
        this.f12600y = executor;
    }

    public /* bridge */ /* synthetic */ void setHeader(String str, String str2) {
        super.setHeader(str, str2);
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
        this.f12595t = hostnameVerifier;
    }

    public void setHttpRetryHandler(HttpRetryHandler httpRetryHandler) {
        this.f12582I = httpRetryHandler;
    }

    public void setLoadingUpdateMaxTimeSpan(int i) {
        this.f12581H = i;
    }

    public void setMaxRetryCount(int i) {
        this.f12578E = i;
    }

    public /* bridge */ /* synthetic */ void setMethod(HttpMethod httpMethod) {
        super.setMethod(httpMethod);
    }

    public /* bridge */ /* synthetic */ void setMultipart(boolean z) {
        super.setMultipart(z);
    }

    public void setPriority(Priority priority) {
        this.f12601z = priority;
    }

    public void setProxy(Proxy proxy) {
        this.f12594s = proxy;
    }

    public void setReadTimeout(int i) {
        if (i > 0) {
            this.f12575B = i;
        }
    }

    public void setRedirectHandler(RedirectHandler redirectHandler) {
        this.f12583J = redirectHandler;
    }

    public /* bridge */ /* synthetic */ void setRequestBody(RequestBody requestBody) {
        super.setRequestBody(requestBody);
    }

    public void setRequestTracker(RequestTracker requestTracker) {
        this.f12584K = requestTracker;
    }

    public void setSaveFilePath(String str) {
        this.f12579F = str;
    }

    public void setSslSocketFactory(SSLSocketFactory sSLSocketFactory) {
        this.f12593r = sSLSocketFactory;
    }

    public void setUri(String str) {
        if (TextUtils.isEmpty(this.f12591p)) {
            this.f12587l = str;
        } else {
            this.f12591p = str;
        }
    }

    public void setUseCookie(boolean z) {
        this.f12596u = z;
    }

    public /* bridge */ /* synthetic */ String toJSONString() {
        return super.toJSONString();
    }

    public String toString() {
        try {
            mo28774a();
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        String uri = getUri();
        if (TextUtils.isEmpty(uri)) {
            return super.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(uri);
        String str = "?";
        if (uri.contains(str)) {
            str = "&";
        }
        sb.append(str);
        sb.append(super.toString());
        return sb.toString();
    }

    public RequestParams(String str) {
        this(str, null, null, null);
    }

    public /* bridge */ /* synthetic */ void addBodyParameter(String str, Object obj, String str2) {
        super.addBodyParameter(str, obj, str2);
    }

    public RequestParams(String str, ParamsBuilder paramsBuilder, String[] strArr, String[] strArr2) {
        this.f12596u = true;
        this.f12601z = Priority.DEFAULT;
        this.f12574A = 15000;
        this.f12575B = 15000;
        this.f12576C = true;
        this.f12577D = false;
        this.f12578E = 2;
        this.f12580G = false;
        this.f12581H = 300;
        this.f12585L = false;
        if (str != null && paramsBuilder == null) {
            paramsBuilder = new DefaultParamsBuilder();
        }
        this.f12587l = str;
        this.f12588m = strArr;
        this.f12589n = strArr2;
        this.f12590o = paramsBuilder;
    }

    public /* bridge */ /* synthetic */ void addBodyParameter(String str, Object obj, String str2, String str3) {
        super.addBodyParameter(str, obj, str2, str3);
    }

    public /* bridge */ /* synthetic */ void addBodyParameter(String str, String str2) {
        super.addBodyParameter(str, str2);
    }
}
