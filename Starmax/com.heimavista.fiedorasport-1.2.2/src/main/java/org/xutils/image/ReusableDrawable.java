package org.xutils.image;

/* renamed from: org.xutils.image.d */
interface ReusableDrawable {
    MemCacheKey getMemCacheKey();

    void setMemCacheKey(MemCacheKey bVar);
}
