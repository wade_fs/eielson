package org.xutils.image;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import java.lang.reflect.Field;
import org.xutils.common.util.DensityUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;

public class ImageOptions {
    public static final ImageOptions DEFAULT = new ImageOptions();

    /* renamed from: a */
    private int f12704a = 0;

    /* renamed from: b */
    private int f12705b = 0;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f12706c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f12707d = 0;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f12708e = false;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f12709f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f12710g = false;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public boolean f12711h = false;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public boolean f12712i = false;

    /* renamed from: j */
    private boolean f12713j = true;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public Bitmap.Config f12714k = Bitmap.Config.RGB_565;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f12715l = true;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public int f12716m = 0;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f12717n = 0;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public Drawable f12718o = null;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public Drawable f12719p = null;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public boolean f12720q = true;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public ImageView.ScaleType f12721r = ImageView.ScaleType.CENTER_INSIDE;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public ImageView.ScaleType f12722s = ImageView.ScaleType.CENTER_CROP;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public boolean f12723t = false;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public Animation f12724u = null;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f12725v = true;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public ParamsBuilder f12726w;

    public static class Builder {
        protected ImageOptions options;

        public Builder() {
            newImageOptions();
        }

        public ImageOptions build() {
            return this.options;
        }

        /* access modifiers changed from: protected */
        public void newImageOptions() {
            this.options = new ImageOptions();
        }

        public Builder setAnimation(Animation animation) {
            Animation unused = this.options.f12724u = animation;
            return this;
        }

        public Builder setAutoRotate(boolean z) {
            boolean unused = this.options.f12712i = z;
            return this;
        }

        public Builder setCircular(boolean z) {
            boolean unused = this.options.f12711h = z;
            return this;
        }

        public Builder setConfig(Bitmap.Config config) {
            Bitmap.Config unused = this.options.f12714k = config;
            return this;
        }

        public Builder setCrop(boolean z) {
            boolean unused = this.options.f12708e = z;
            return this;
        }

        public Builder setFadeIn(boolean z) {
            boolean unused = this.options.f12723t = z;
            return this;
        }

        public Builder setFailureDrawable(Drawable drawable) {
            Drawable unused = this.options.f12719p = drawable;
            return this;
        }

        public Builder setFailureDrawableId(int i) {
            int unused = this.options.f12717n = i;
            return this;
        }

        public Builder setForceLoadingDrawable(boolean z) {
            boolean unused = this.options.f12720q = z;
            return this;
        }

        public Builder setIgnoreGif(boolean z) {
            boolean unused = this.options.f12715l = z;
            return this;
        }

        public Builder setImageScaleType(ImageView.ScaleType scaleType) {
            ImageView.ScaleType unused = this.options.f12722s = scaleType;
            return this;
        }

        public Builder setLoadingDrawable(Drawable drawable) {
            Drawable unused = this.options.f12718o = drawable;
            return this;
        }

        public Builder setLoadingDrawableId(int i) {
            int unused = this.options.f12716m = i;
            return this;
        }

        public Builder setParamsBuilder(ParamsBuilder paramsBuilder) {
            ParamsBuilder unused = this.options.f12726w = paramsBuilder;
            return this;
        }

        public Builder setPlaceholderScaleType(ImageView.ScaleType scaleType) {
            ImageView.ScaleType unused = this.options.f12721r = scaleType;
            return this;
        }

        public Builder setRadius(int i) {
            int unused = this.options.f12709f = i;
            return this;
        }

        public Builder setSize(int i, int i2) {
            int unused = this.options.f12706c = i;
            int unused2 = this.options.f12707d = i2;
            return this;
        }

        public Builder setSquare(boolean z) {
            boolean unused = this.options.f12710g = z;
            return this;
        }

        public Builder setUseMemCache(boolean z) {
            boolean unused = this.options.f12725v = z;
            return this;
        }
    }

    public interface ParamsBuilder {
        RequestParams buildParams(RequestParams requestParams, ImageOptions imageOptions);
    }

    protected ImageOptions() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ImageOptions.class != obj.getClass()) {
            return false;
        }
        ImageOptions imageOptions = (ImageOptions) obj;
        if (this.f12704a == imageOptions.f12704a && this.f12705b == imageOptions.f12705b && this.f12706c == imageOptions.f12706c && this.f12707d == imageOptions.f12707d && this.f12708e == imageOptions.f12708e && this.f12709f == imageOptions.f12709f && this.f12710g == imageOptions.f12710g && this.f12711h == imageOptions.f12711h && this.f12712i == imageOptions.f12712i && this.f12713j == imageOptions.f12713j && this.f12714k == imageOptions.f12714k) {
            return true;
        }
        return false;
    }

    public Animation getAnimation() {
        return this.f12724u;
    }

    public Bitmap.Config getConfig() {
        return this.f12714k;
    }

    public Drawable getFailureDrawable(ImageView imageView) {
        if (this.f12719p == null && this.f12717n > 0 && imageView != null) {
            try {
                this.f12719p = imageView.getResources().getDrawable(this.f12717n);
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
        return this.f12719p;
    }

    public int getHeight() {
        return this.f12707d;
    }

    public ImageView.ScaleType getImageScaleType() {
        return this.f12722s;
    }

    public Drawable getLoadingDrawable(ImageView imageView) {
        if (this.f12718o == null && this.f12716m > 0 && imageView != null) {
            try {
                this.f12718o = imageView.getResources().getDrawable(this.f12716m);
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
            }
        }
        return this.f12718o;
    }

    public int getMaxHeight() {
        return this.f12705b;
    }

    public int getMaxWidth() {
        return this.f12704a;
    }

    public ParamsBuilder getParamsBuilder() {
        return this.f12726w;
    }

    public ImageView.ScaleType getPlaceholderScaleType() {
        return this.f12721r;
    }

    public int getRadius() {
        return this.f12709f;
    }

    public int getWidth() {
        return this.f12706c;
    }

    public int hashCode() {
        int i = ((((((((((((((((((this.f12704a * 31) + this.f12705b) * 31) + this.f12706c) * 31) + this.f12707d) * 31) + (this.f12708e ? 1 : 0)) * 31) + this.f12709f) * 31) + (this.f12710g ? 1 : 0)) * 31) + (this.f12711h ? 1 : 0)) * 31) + (this.f12712i ? 1 : 0)) * 31) + (this.f12713j ? 1 : 0)) * 31;
        Bitmap.Config config = this.f12714k;
        return i + (config != null ? config.hashCode() : 0);
    }

    public boolean isAutoRotate() {
        return this.f12712i;
    }

    public boolean isCircular() {
        return this.f12711h;
    }

    public boolean isCompress() {
        return this.f12713j;
    }

    public boolean isCrop() {
        return this.f12708e;
    }

    public boolean isFadeIn() {
        return this.f12723t;
    }

    public boolean isForceLoadingDrawable() {
        return this.f12720q;
    }

    public boolean isIgnoreGif() {
        return this.f12715l;
    }

    public boolean isSquare() {
        return this.f12710g;
    }

    public boolean isUseMemCache() {
        return this.f12725v;
    }

    public String toString() {
        return "_" + this.f12704a + "_" + this.f12705b + "_" + this.f12706c + "_" + this.f12707d + "_" + this.f12709f + "_" + this.f12714k + "_" + (this.f12708e ? 1 : 0) + (this.f12710g ? 1 : 0) + (this.f12711h ? 1 : 0) + (this.f12712i ? 1 : 0) + (this.f12713j ? 1 : 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo28962a(ImageView imageView) {
        int i;
        int i2 = this.f12706c;
        if (i2 <= 0 || (i = this.f12707d) <= 0) {
            int screenWidth = DensityUtil.getScreenWidth();
            int screenHeight = DensityUtil.getScreenHeight();
            if (this.f12706c < 0) {
                this.f12704a = (screenWidth * 3) / 2;
                this.f12713j = false;
            }
            if (this.f12707d < 0) {
                this.f12705b = (screenHeight * 3) / 2;
                this.f12713j = false;
            }
            if (imageView != null || this.f12704a > 0 || this.f12705b > 0) {
                int i3 = this.f12704a;
                int i4 = this.f12705b;
                if (imageView != null) {
                    ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                    if (layoutParams != null) {
                        if (i3 <= 0) {
                            int i5 = layoutParams.width;
                            if (i5 > 0) {
                                if (this.f12706c <= 0) {
                                    this.f12706c = i5;
                                }
                                i3 = i5;
                            } else if (i5 != -2) {
                                i3 = imageView.getWidth();
                            }
                        }
                        if (i4 <= 0) {
                            int i6 = layoutParams.height;
                            if (i6 > 0) {
                                if (this.f12707d <= 0) {
                                    this.f12707d = i6;
                                }
                                i4 = i6;
                            } else if (i6 != -2) {
                                i4 = imageView.getHeight();
                            }
                        }
                    }
                    if (i3 <= 0) {
                        i3 = m20208a(imageView, "mMaxWidth");
                    }
                    if (i4 <= 0) {
                        i4 = m20208a(imageView, "mMaxHeight");
                    }
                }
                if (i3 > 0) {
                    screenWidth = i3;
                }
                if (i4 > 0) {
                    screenHeight = i4;
                }
                this.f12704a = screenWidth;
                this.f12705b = screenHeight;
                return;
            }
            this.f12704a = screenWidth;
            this.f12705b = screenHeight;
            return;
        }
        this.f12704a = i2;
        this.f12705b = i;
    }

    /* renamed from: a */
    private static int m20208a(ImageView imageView, String str) {
        try {
            Field declaredField = ImageView.class.getDeclaredField(str);
            declaredField.setAccessible(true);
            int intValue = ((Integer) declaredField.get(imageView)).intValue();
            if (intValue <= 0 || intValue >= Integer.MAX_VALUE) {
                return 0;
            }
            return intValue;
        } catch (Throwable unused) {
            return 0;
        }
    }
}
