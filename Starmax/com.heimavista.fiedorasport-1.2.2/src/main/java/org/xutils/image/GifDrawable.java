package org.xutils.image;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Movie;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import org.xutils.common.util.LogUtil;

public class GifDrawable extends Drawable implements Runnable, Animatable {

    /* renamed from: P */
    private int f12672P;

    /* renamed from: Q */
    private int f12673Q = 300;

    /* renamed from: R */
    private volatile boolean f12674R;

    /* renamed from: S */
    private final Movie f12675S;

    /* renamed from: T */
    private final int f12676T;

    /* renamed from: U */
    private final long f12677U = SystemClock.uptimeMillis();

    public GifDrawable(Movie movie, int i) {
        this.f12675S = movie;
        this.f12672P = i;
        this.f12676T = movie.duration();
    }

    public void draw(Canvas canvas) {
        try {
            this.f12675S.setTime(this.f12676T > 0 ? ((int) (SystemClock.uptimeMillis() - this.f12677U)) % this.f12676T : 0);
            this.f12675S.draw(canvas, 0.0f, 0.0f);
            start();
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
    }

    public int getByteCount() {
        if (this.f12672P == 0) {
            this.f12672P = this.f12675S.width() * this.f12675S.height() * 3 * 5;
        }
        return this.f12672P;
    }

    public int getDuration() {
        return this.f12676T;
    }

    public int getIntrinsicHeight() {
        return this.f12675S.height();
    }

    public int getIntrinsicWidth() {
        return this.f12675S.width();
    }

    public Movie getMovie() {
        return this.f12675S;
    }

    public int getOpacity() {
        return this.f12675S.isOpaque() ? -1 : -3;
    }

    public int getRate() {
        return this.f12673Q;
    }

    public boolean isRunning() {
        return this.f12674R && this.f12676T > 0;
    }

    public void run() {
        if (this.f12676T > 0) {
            invalidateSelf();
            scheduleSelf(this, SystemClock.uptimeMillis() + ((long) this.f12673Q));
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void setRate(int i) {
        this.f12673Q = i;
    }

    public void start() {
        if (!isRunning()) {
            this.f12674R = true;
            run();
        }
    }

    public void stop() {
        if (isRunning()) {
            unscheduleSelf(this);
        }
    }
}
