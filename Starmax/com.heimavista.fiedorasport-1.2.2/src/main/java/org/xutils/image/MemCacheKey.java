package org.xutils.image;

/* renamed from: org.xutils.image.b */
final class MemCacheKey {

    /* renamed from: a */
    public final String f12751a;

    /* renamed from: b */
    public final ImageOptions f12752b;

    public MemCacheKey(String str, ImageOptions imageOptions) {
        this.f12751a = str;
        this.f12752b = imageOptions;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MemCacheKey.class != obj.getClass()) {
            return false;
        }
        MemCacheKey bVar = (MemCacheKey) obj;
        if (!this.f12751a.equals(bVar.f12751a)) {
            return false;
        }
        return this.f12752b.equals(bVar.f12752b);
    }

    public int hashCode() {
        return (this.f12751a.hashCode() * 31) + this.f12752b.hashCode();
    }

    public String toString() {
        return this.f12751a + this.f12752b.toString();
    }
}
