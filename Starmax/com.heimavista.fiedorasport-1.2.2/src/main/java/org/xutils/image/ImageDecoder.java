package org.xutils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.os.Build;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.cache.DiskCacheFile;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;

public final class ImageDecoder {

    /* renamed from: a */
    private static final int f12679a;

    /* renamed from: b */
    private static final AtomicInteger f12680b = new AtomicInteger(0);

    /* renamed from: c */
    private static final Object f12681c = new Object();

    /* renamed from: d */
    private static final Object f12682d = new Object();

    /* renamed from: e */
    private static final byte[] f12683e = {71, 73, 70};

    /* renamed from: f */
    private static final Executor f12684f = new PriorityExecutor(1, true);

    /* renamed from: g */
    private static final LruDiskCache f12685g = LruDiskCache.getDiskCache("xUtils_img_thumb");

    /* renamed from: h */
    private static final boolean f12686h;

    /* renamed from: org.xutils.image.ImageDecoder$a */
    static class C5208a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ File f12687P;

        /* renamed from: Q */
        final /* synthetic */ ImageOptions f12688Q;

        /* renamed from: R */
        final /* synthetic */ Bitmap f12689R;

        C5208a(File file, ImageOptions imageOptions, Bitmap bitmap) {
            this.f12687P = file;
            this.f12688Q = imageOptions;
            this.f12689R = bitmap;
        }

        public void run() {
            ImageDecoder.m20207b(this.f12687P, this.f12688Q, this.f12689R);
        }
    }

    static {
        boolean z = false;
        int i = 1;
        if (Build.VERSION.SDK_INT > 16) {
            z = true;
        }
        f12686h = z;
        if (Runtime.getRuntime().availableProcessors() > 4) {
            i = 2;
        }
        f12679a = i;
    }

    private ImageDecoder() {
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: org.xutils.cache.DiskCacheFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: org.xutils.cache.DiskCacheFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: org.xutils.cache.DiskCacheFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: org.xutils.cache.DiskCacheFile} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m20207b(java.io.File r4, org.xutils.image.ImageOptions r5, android.graphics.Bitmap r6) {
        /*
            org.xutils.cache.DiskCacheEntity r0 = new org.xutils.cache.DiskCacheEntity
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r4.getAbsolutePath()
            r1.append(r2)
            java.lang.String r2 = "@"
            r1.append(r2)
            long r2 = r4.lastModified()
            r1.append(r2)
            java.lang.String r4 = r5.toString()
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            r0.setKey(r4)
            r4 = 0
            org.xutils.cache.LruDiskCache r5 = org.xutils.image.ImageDecoder.f12685g     // Catch:{ all -> 0x005d }
            org.xutils.cache.DiskCacheFile r5 = r5.createDiskCacheFile(r0)     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x0056
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ all -> 0x0052 }
            r0.<init>(r5)     // Catch:{ all -> 0x0052 }
            boolean r4 = org.xutils.image.ImageDecoder.f12686h     // Catch:{ all -> 0x0050 }
            if (r4 == 0) goto L_0x0040
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.WEBP     // Catch:{ all -> 0x0050 }
            goto L_0x0042
        L_0x0040:
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ all -> 0x0050 }
        L_0x0042:
            r1 = 80
            r6.compress(r4, r1, r0)     // Catch:{ all -> 0x0050 }
            r0.flush()     // Catch:{ all -> 0x0050 }
            org.xutils.cache.DiskCacheFile r5 = r5.commit()     // Catch:{ all -> 0x0050 }
            r4 = r0
            goto L_0x0056
        L_0x0050:
            r4 = move-exception
            goto L_0x0061
        L_0x0052:
            r6 = move-exception
            r0 = r4
            r4 = r6
            goto L_0x0061
        L_0x0056:
            org.xutils.common.util.IOUtil.closeQuietly(r5)
            org.xutils.common.util.IOUtil.closeQuietly(r4)
            goto L_0x0071
        L_0x005d:
            r5 = move-exception
            r0 = r4
            r4 = r5
            r5 = r0
        L_0x0061:
            org.xutils.common.util.IOUtil.deleteFileOrDir(r5)     // Catch:{ all -> 0x0072 }
            java.lang.String r6 = r4.getMessage()     // Catch:{ all -> 0x0072 }
            org.xutils.common.util.LogUtil.m20126w(r6, r4)     // Catch:{ all -> 0x0072 }
            org.xutils.common.util.IOUtil.closeQuietly(r5)
            org.xutils.common.util.IOUtil.closeQuietly(r0)
        L_0x0071:
            return
        L_0x0072:
            r4 = move-exception
            org.xutils.common.util.IOUtil.closeQuietly(r5)
            org.xutils.common.util.IOUtil.closeQuietly(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.image.ImageDecoder.m20207b(java.io.File, org.xutils.image.ImageOptions, android.graphics.Bitmap):void");
    }

    public static int calculateSampleSize(int i, int i2, int i3, int i4) {
        int i5;
        int i6 = 1;
        if (i > i3 || i2 > i4) {
            if (i > i2) {
                i5 = Math.round(((float) i2) / ((float) i4));
            } else {
                i5 = Math.round(((float) i) / ((float) i3));
            }
            if (i5 >= 1) {
                i6 = i5;
            }
            float f = (float) (i * i2);
            while (f / ((float) (i6 * i6)) > ((float) (i3 * i4 * 2))) {
                i6++;
            }
        }
        return i6;
    }

    public static Bitmap cut2Circular(Bitmap bitmap, boolean z) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int min = Math.min(width, height);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Bitmap createBitmap = Bitmap.createBitmap(min, min, Bitmap.Config.ARGB_8888);
        if (createBitmap == null) {
            return bitmap;
        }
        Canvas canvas = new Canvas(createBitmap);
        float f = (float) (min / 2);
        canvas.drawCircle(f, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, (float) ((min - width) / 2), (float) ((min - height) / 2), paint);
        if (z) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static Bitmap cut2RoundCorner(Bitmap bitmap, int i, boolean z, boolean z2) {
        int i2;
        int i3;
        if (i <= 0) {
            return bitmap;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (z) {
            i2 = Math.min(width, height);
            i3 = i2;
        } else {
            i2 = width;
            i3 = height;
        }
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        if (createBitmap == null) {
            return bitmap;
        }
        Canvas canvas = new Canvas(createBitmap);
        float f = (float) i;
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) i2, (float) i3), f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, (float) ((i2 - width) / 2), (float) ((i3 - height) / 2), paint);
        if (z2) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap cut2ScaleSize(Bitmap bitmap, int i, int i2, boolean z) {
        int i3;
        int i4;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width == i && height == i2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        float f = (float) i;
        float f2 = (float) width;
        float f3 = f / f2;
        float f4 = (float) i2;
        float f5 = (float) height;
        float f6 = f4 / f5;
        if (f3 > f6) {
            float f7 = f4 / f3;
            height = (int) ((f5 + f7) / 2.0f);
            i4 = (int) ((f5 - f7) / 2.0f);
            i3 = 0;
        } else {
            float f8 = f / f6;
            i3 = (int) ((f2 - f8) / 2.0f);
            width = (int) ((f2 + f8) / 2.0f);
            f3 = f6;
            i4 = 0;
        }
        matrix.setScale(f3, f3);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, i3, i4, width - i3, height - i4, matrix, true);
        if (createBitmap == null) {
            return bitmap;
        }
        if (z && createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static Bitmap cut2Square(Bitmap bitmap, boolean z) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width == height) {
            return bitmap;
        }
        int min = Math.min(width, height);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, (width - min) / 2, (height - min) / 2, min, min);
        if (createBitmap == null) {
            return bitmap;
        }
        if (z && createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static Bitmap decodeBitmap(File file, ImageOptions imageOptions, Callback.Cancelable cancelable) {
        if (file == null || !file.exists() || file.length() < 1) {
            return null;
        }
        if (imageOptions == null) {
            imageOptions = ImageOptions.DEFAULT;
        }
        if (imageOptions.getMaxWidth() <= 0 || imageOptions.getMaxHeight() <= 0) {
            imageOptions.mo28962a(null);
        }
        if (cancelable != null) {
            try {
                if (cancelable.isCancelled()) {
                    throw new Callback.CancelledException("cancelled during decode image");
                }
            } catch (IOException e) {
                throw e;
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
                return null;
            }
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int i = 0;
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = imageOptions.getConfig();
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        int width = imageOptions.getWidth();
        int height = imageOptions.getHeight();
        if (imageOptions.isAutoRotate()) {
            i = getRotateAngle(file.getAbsolutePath());
            if ((i / 90) % 2 == 1) {
                i2 = options.outHeight;
                i3 = options.outWidth;
            }
        }
        if (!imageOptions.isCrop() && width > 0 && height > 0) {
            if ((i / 90) % 2 == 1) {
                options.outWidth = height;
                options.outHeight = width;
            } else {
                options.outWidth = width;
                options.outHeight = height;
            }
        }
        options.inSampleSize = calculateSampleSize(i2, i3, imageOptions.getMaxWidth(), imageOptions.getMaxHeight());
        if (cancelable != null) {
            if (cancelable.isCancelled()) {
                throw new Callback.CancelledException("cancelled during decode image");
            }
        }
        Bitmap decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        if (decodeFile != null) {
            if (cancelable != null) {
                if (cancelable.isCancelled()) {
                    throw new Callback.CancelledException("cancelled during decode image");
                }
            }
            if (i != 0) {
                decodeFile = rotate(decodeFile, i, true);
            }
            if (cancelable != null) {
                if (cancelable.isCancelled()) {
                    throw new Callback.CancelledException("cancelled during decode image");
                }
            }
            if (imageOptions.isCrop() && width > 0 && height > 0) {
                decodeFile = cut2ScaleSize(decodeFile, width, height, true);
            }
            if (decodeFile != null) {
                if (cancelable != null) {
                    if (cancelable.isCancelled()) {
                        throw new Callback.CancelledException("cancelled during decode image");
                    }
                }
                if (imageOptions.isCircular()) {
                    decodeFile = cut2Circular(decodeFile, true);
                } else if (imageOptions.getRadius() > 0) {
                    decodeFile = cut2RoundCorner(decodeFile, imageOptions.getRadius(), imageOptions.isSquare(), true);
                } else if (imageOptions.isSquare()) {
                    decodeFile = cut2Square(decodeFile, true);
                }
                if (decodeFile != null) {
                    return decodeFile;
                }
                throw new IOException("decode image error");
            }
            throw new IOException("decode image error");
        }
        throw new IOException("decode image error");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x0059=Splitter:B:32:0x0059, B:26:0x004c=Splitter:B:26:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Movie decodeGif(java.io.File r5, org.xutils.image.ImageOptions r6, org.xutils.common.Callback.Cancelable r7) {
        /*
            r6 = 0
            if (r5 == 0) goto L_0x005f
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x005f
            long r0 = r5.length()
            r2 = 1
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0014
            goto L_0x005f
        L_0x0014:
            if (r7 == 0) goto L_0x0025
            boolean r7 = r7.isCancelled()     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            if (r7 != 0) goto L_0x001d
            goto L_0x0025
        L_0x001d:
            org.xutils.common.Callback$CancelledException r5 = new org.xutils.common.Callback$CancelledException     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            java.lang.String r7 = "cancelled during decode image"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            throw r5     // Catch:{ IOException -> 0x0057, all -> 0x004a }
        L_0x0025:
            r7 = 16384(0x4000, float:2.2959E-41)
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            r1.<init>(r5)     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            r0.<init>(r1, r7)     // Catch:{ IOException -> 0x0057, all -> 0x004a }
            r0.mark(r7)     // Catch:{ IOException -> 0x0048, all -> 0x0046 }
            android.graphics.Movie r5 = android.graphics.Movie.decodeStream(r0)     // Catch:{ IOException -> 0x0048, all -> 0x0046 }
            if (r5 == 0) goto L_0x003e
            org.xutils.common.util.IOUtil.closeQuietly(r0)
            return r5
        L_0x003e:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ IOException -> 0x0048, all -> 0x0046 }
            java.lang.String r7 = "decode image error"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0048, all -> 0x0046 }
            throw r5     // Catch:{ IOException -> 0x0048, all -> 0x0046 }
        L_0x0046:
            r5 = move-exception
            goto L_0x004c
        L_0x0048:
            r5 = move-exception
            goto L_0x0059
        L_0x004a:
            r5 = move-exception
            r0 = r6
        L_0x004c:
            java.lang.String r7 = r5.getMessage()     // Catch:{ all -> 0x005a }
            org.xutils.common.util.LogUtil.m20120e(r7, r5)     // Catch:{ all -> 0x005a }
            org.xutils.common.util.IOUtil.closeQuietly(r0)
            return r6
        L_0x0057:
            r5 = move-exception
            r0 = r6
        L_0x0059:
            throw r5     // Catch:{ all -> 0x005a }
        L_0x005a:
            r5 = move-exception
            org.xutils.common.util.IOUtil.closeQuietly(r0)
            throw r5
        L_0x005f:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.image.ImageDecoder.decodeGif(java.io.File, org.xutils.image.ImageOptions, org.xutils.common.Callback$Cancelable):android.graphics.Movie");
    }

    public static int getRotateAngle(String str) {
        try {
            int attributeInt = new ExifInterface(str).getAttributeInt(androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION, 0);
            if (attributeInt == 3) {
                return 180;
            }
            if (attributeInt == 6) {
                return 90;
            }
            if (attributeInt != 8) {
                return 0;
            }
            return 270;
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
            return 0;
        }
    }

    /* JADX INFO: finally extract failed */
    public static boolean isGif(File file) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                boolean equals = Arrays.equals(f12683e, IOUtil.readBytes(fileInputStream, 0, 3));
                IOUtil.closeQuietly(fileInputStream);
                return equals;
            } catch (Throwable th) {
                th = th;
                try {
                    LogUtil.m20120e(th.getMessage(), th);
                    IOUtil.closeQuietly(fileInputStream);
                    return false;
                } catch (Throwable th2) {
                    IOUtil.closeQuietly(fileInputStream);
                    throw th2;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            LogUtil.m20120e(th.getMessage(), th);
            IOUtil.closeQuietly(fileInputStream);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap rotate(android.graphics.Bitmap r7, int r8, boolean r9) {
        /*
            if (r8 == 0) goto L_0x0024
            android.graphics.Matrix r5 = new android.graphics.Matrix
            r5.<init>()
            float r8 = (float) r8
            r5.setRotate(r8)
            r1 = 0
            r2 = 0
            int r3 = r7.getWidth()     // Catch:{ all -> 0x001c }
            int r4 = r7.getHeight()     // Catch:{ all -> 0x001c }
            r6 = 1
            r0 = r7
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x001c }
            goto L_0x0025
        L_0x001c:
            r8 = move-exception
            java.lang.String r0 = r8.getMessage()
            org.xutils.common.util.LogUtil.m20120e(r0, r8)
        L_0x0024:
            r8 = 0
        L_0x0025:
            if (r8 == 0) goto L_0x002f
            if (r9 == 0) goto L_0x002e
            if (r8 == r7) goto L_0x002e
            r7.recycle()
        L_0x002e:
            r7 = r8
        L_0x002f:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.image.ImageDecoder.rotate(android.graphics.Bitmap, int, boolean):android.graphics.Bitmap");
    }

    /* renamed from: a */
    static void m20205a() {
        f12685g.clearCacheFiles();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:35|36|37|38|39|30|29) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x004e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0066 */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004e A[LOOP:1: B:29:0x004e->B:89:0x004e, LOOP_START, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.graphics.drawable.Drawable m20204a(java.io.File r6, org.xutils.image.ImageOptions r7, org.xutils.common.Callback.Cancelable r8) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x00e0
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x00e0
            long r1 = r6.length()
            r3 = 1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0015
            goto L_0x00e0
        L_0x0015:
            if (r8 == 0) goto L_0x0026
            boolean r1 = r8.isCancelled()
            if (r1 != 0) goto L_0x001e
            goto L_0x0026
        L_0x001e:
            org.xutils.common.Callback$CancelledException r6 = new org.xutils.common.Callback$CancelledException
            java.lang.String r7 = "cancelled during decode image"
            r6.<init>(r7)
            throw r6
        L_0x0026:
            boolean r1 = r7.isIgnoreGif()
            if (r1 != 0) goto L_0x004b
            boolean r1 = isGif(r6)
            if (r1 == 0) goto L_0x004b
            java.lang.Object r1 = org.xutils.image.ImageDecoder.f12682d
            monitor-enter(r1)
            android.graphics.Movie r7 = decodeGif(r6, r7, r8)     // Catch:{ all -> 0x0048 }
            monitor-exit(r1)     // Catch:{ all -> 0x0048 }
            if (r7 == 0) goto L_0x00c6
            org.xutils.image.GifDrawable r0 = new org.xutils.image.GifDrawable
            long r1 = r6.length()
            int r6 = (int) r1
            r0.<init>(r7, r6)
            goto L_0x00c6
        L_0x0048:
            r6 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0048 }
            throw r6
        L_0x004b:
            java.lang.Object r1 = org.xutils.image.ImageDecoder.f12681c     // Catch:{ all -> 0x00cd }
            monitor-enter(r1)     // Catch:{ all -> 0x00cd }
        L_0x004e:
            java.util.concurrent.atomic.AtomicInteger r2 = org.xutils.image.ImageDecoder.f12680b     // Catch:{ all -> 0x00ca }
            int r2 = r2.get()     // Catch:{ all -> 0x00ca }
            int r3 = org.xutils.image.ImageDecoder.f12679a     // Catch:{ all -> 0x00ca }
            if (r2 < r3) goto L_0x006e
            if (r8 == 0) goto L_0x0060
            boolean r2 = r8.isCancelled()     // Catch:{ all -> 0x00ca }
            if (r2 != 0) goto L_0x006e
        L_0x0060:
            java.lang.Object r2 = org.xutils.image.ImageDecoder.f12681c     // Catch:{ InterruptedException -> 0x0066, all -> 0x004e }
            r2.wait()     // Catch:{ InterruptedException -> 0x0066, all -> 0x004e }
            goto L_0x004e
        L_0x0066:
            org.xutils.common.Callback$CancelledException r6 = new org.xutils.common.Callback$CancelledException     // Catch:{ all -> 0x00ca }
            java.lang.String r7 = "cancelled during decode image"
            r6.<init>(r7)     // Catch:{ all -> 0x00ca }
            throw r6     // Catch:{ all -> 0x00ca }
        L_0x006e:
            monitor-exit(r1)     // Catch:{ all -> 0x00ca }
            if (r8 == 0) goto L_0x0080
            boolean r1 = r8.isCancelled()     // Catch:{ all -> 0x00cd }
            if (r1 != 0) goto L_0x0078
            goto L_0x0080
        L_0x0078:
            org.xutils.common.Callback$CancelledException r6 = new org.xutils.common.Callback$CancelledException     // Catch:{ all -> 0x00cd }
            java.lang.String r7 = "cancelled during decode image"
            r6.<init>(r7)     // Catch:{ all -> 0x00cd }
            throw r6     // Catch:{ all -> 0x00cd }
        L_0x0080:
            java.util.concurrent.atomic.AtomicInteger r1 = org.xutils.image.ImageDecoder.f12680b     // Catch:{ all -> 0x00cd }
            r1.incrementAndGet()     // Catch:{ all -> 0x00cd }
            boolean r1 = r7.isCompress()     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x0090
            android.graphics.Bitmap r1 = m20203a(r6, r7)     // Catch:{ all -> 0x00cd }
            goto L_0x0091
        L_0x0090:
            r1 = r0
        L_0x0091:
            if (r1 != 0) goto L_0x00a9
            android.graphics.Bitmap r1 = decodeBitmap(r6, r7, r8)     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x00a9
            boolean r8 = r7.isCompress()     // Catch:{ all -> 0x00cd }
            if (r8 == 0) goto L_0x00a9
            java.util.concurrent.Executor r8 = org.xutils.image.ImageDecoder.f12684f     // Catch:{ all -> 0x00cd }
            org.xutils.image.ImageDecoder$a r2 = new org.xutils.image.ImageDecoder$a     // Catch:{ all -> 0x00cd }
            r2.<init>(r6, r7, r1)     // Catch:{ all -> 0x00cd }
            r8.execute(r2)     // Catch:{ all -> 0x00cd }
        L_0x00a9:
            java.util.concurrent.atomic.AtomicInteger r6 = org.xutils.image.ImageDecoder.f12680b
            r6.decrementAndGet()
            java.lang.Object r6 = org.xutils.image.ImageDecoder.f12681c
            monitor-enter(r6)
            java.lang.Object r7 = org.xutils.image.ImageDecoder.f12681c     // Catch:{ all -> 0x00c7 }
            r7.notifyAll()     // Catch:{ all -> 0x00c7 }
            monitor-exit(r6)     // Catch:{ all -> 0x00c7 }
            if (r1 == 0) goto L_0x00c6
            org.xutils.image.c r0 = new org.xutils.image.c
            android.app.Application r6 = org.xutils.C5217x.app()
            android.content.res.Resources r6 = r6.getResources()
            r0.<init>(r6, r1)
        L_0x00c6:
            return r0
        L_0x00c7:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00c7 }
            throw r7
        L_0x00ca:
            r6 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ca }
            throw r6     // Catch:{ all -> 0x00cd }
        L_0x00cd:
            r6 = move-exception
            java.util.concurrent.atomic.AtomicInteger r7 = org.xutils.image.ImageDecoder.f12680b
            r7.decrementAndGet()
            java.lang.Object r7 = org.xutils.image.ImageDecoder.f12681c
            monitor-enter(r7)
            java.lang.Object r8 = org.xutils.image.ImageDecoder.f12681c     // Catch:{ all -> 0x00dd }
            r8.notifyAll()     // Catch:{ all -> 0x00dd }
            monitor-exit(r7)     // Catch:{ all -> 0x00dd }
            throw r6
        L_0x00dd:
            r6 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00dd }
            throw r6
        L_0x00e0:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.image.ImageDecoder.m20204a(java.io.File, org.xutils.image.ImageOptions, org.xutils.common.Callback$Cancelable):android.graphics.drawable.Drawable");
    }

    /* renamed from: a */
    private static Bitmap m20203a(File file, ImageOptions imageOptions) {
        DiskCacheFile diskCacheFile;
        try {
            diskCacheFile = f12685g.getDiskCacheFile(file.getAbsolutePath() + "@" + file.lastModified() + imageOptions.toString());
            if (diskCacheFile != null) {
                try {
                    if (diskCacheFile.exists()) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = false;
                        options.inPurgeable = true;
                        options.inInputShareable = true;
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        Bitmap decodeFile = BitmapFactory.decodeFile(diskCacheFile.getAbsolutePath(), options);
                        IOUtil.closeQuietly(diskCacheFile);
                        return decodeFile;
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        LogUtil.m20126w(th.getMessage(), th);
                        IOUtil.closeQuietly(diskCacheFile);
                        return null;
                    } catch (Throwable th2) {
                        IOUtil.closeQuietly(diskCacheFile);
                        throw th2;
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            diskCacheFile = null;
            LogUtil.m20126w(th.getMessage(), th);
            IOUtil.closeQuietly(diskCacheFile);
            return null;
        }
        IOUtil.closeQuietly(diskCacheFile);
        return null;
    }
}
