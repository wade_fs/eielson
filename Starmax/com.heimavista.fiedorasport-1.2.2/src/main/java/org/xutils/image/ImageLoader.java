package org.xutils.image;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.widget.ImageView;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import org.xutils.C5217x;
import org.xutils.cache.LruCache;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback;
import org.xutils.common.task.Priority;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;
import org.xutils.p275ex.FileLockedException;

/* renamed from: org.xutils.image.a */
final class ImageLoader implements Callback.PrepareCallback<File, Drawable>, Callback.CacheCallback<Drawable>, Callback.ProgressCallback<Drawable>, Callback.TypedCallback<Drawable>, Callback.Cancelable {

    /* renamed from: m */
    private static final AtomicLong f12727m = new AtomicLong(0);

    /* renamed from: n */
    private static final Executor f12728n = new PriorityExecutor(10, false);

    /* renamed from: o */
    private static final LruCache<MemCacheKey, Drawable> f12729o = new C5213a(4194304);

    /* renamed from: p */
    private static final HashMap<String, C5216d> f12730p = new HashMap<>();

    /* renamed from: q */
    private static final Type f12731q = File.class;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public MemCacheKey f12732a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ImageOptions f12733b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public WeakReference<ImageView> f12734c;

    /* renamed from: d */
    private final long f12735d = f12727m.incrementAndGet();

    /* renamed from: e */
    private volatile boolean f12736e = false;

    /* renamed from: f */
    private volatile boolean f12737f = false;

    /* renamed from: g */
    private Callback.Cancelable f12738g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Callback.CommonCallback<Drawable> f12739h;

    /* renamed from: i */
    private Callback.PrepareCallback<File, Drawable> f12740i;

    /* renamed from: j */
    private Callback.CacheCallback<Drawable> f12741j;

    /* renamed from: k */
    private Callback.ProgressCallback<Drawable> f12742k;

    /* renamed from: l */
    private boolean f12743l = false;

    /* renamed from: org.xutils.image.a$a */
    /* compiled from: ImageLoader */
    static class C5213a extends LruCache<MemCacheKey, Drawable> {

        /* renamed from: i */
        private boolean f12744i = false;

        C5213a(int i) {
            super(i);
        }

        public void trimToSize(int i) {
            if (i < 0) {
                this.f12744i = true;
            }
            super.trimToSize(i);
            this.f12744i = false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.xutils.cache.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void
         arg types: [boolean, org.xutils.image.b, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable]
         candidates:
          org.xutils.image.a.a.entryRemoved(boolean, org.xutils.image.b, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable):void
          org.xutils.cache.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void */
        /* access modifiers changed from: protected */
        public void entryRemoved(boolean z, MemCacheKey bVar, Drawable drawable, Drawable drawable2) {
            super.entryRemoved(z, (Object) bVar, (Object) drawable, (Object) drawable2);
            if (z && this.f12744i && (drawable instanceof ReusableDrawable)) {
                ((ReusableDrawable) drawable).setMemCacheKey(null);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.xutils.cache.LruCache.sizeOf(java.lang.Object, java.lang.Object):int
         arg types: [org.xutils.image.b, android.graphics.drawable.Drawable]
         candidates:
          org.xutils.image.a.a.sizeOf(org.xutils.image.b, android.graphics.drawable.Drawable):int
          org.xutils.cache.LruCache.sizeOf(java.lang.Object, java.lang.Object):int */
        /* access modifiers changed from: protected */
        public int sizeOf(MemCacheKey bVar, Drawable drawable) {
            if (drawable instanceof BitmapDrawable) {
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                if (bitmap == null) {
                    return 0;
                }
                return bitmap.getByteCount();
            } else if (drawable instanceof GifDrawable) {
                return ((GifDrawable) drawable).getByteCount();
            } else {
                return super.sizeOf((Object) bVar, (Object) drawable);
            }
        }
    }

    /* renamed from: org.xutils.image.a$b */
    /* compiled from: ImageLoader */
    class C5214b implements Runnable {
        C5214b() {
        }

        public void run() {
            ImageLoader.m20231a((ImageView) ImageLoader.this.f12734c.get(), ImageLoader.this.f12732a.f12751a, ImageLoader.this.f12733b, ImageLoader.this.f12739h);
        }
    }

    /* renamed from: org.xutils.image.a$c */
    /* compiled from: ImageLoader */
    static class C5215c implements Runnable {

        /* renamed from: P */
        final /* synthetic */ Callback.CommonCallback f12746P;

        /* renamed from: Q */
        final /* synthetic */ ImageView f12747Q;

        /* renamed from: R */
        final /* synthetic */ ImageOptions f12748R;

        /* renamed from: S */
        final /* synthetic */ String f12749S;

        C5215c(Callback.CommonCallback commonCallback, ImageView imageView, ImageOptions imageOptions, String str) {
            this.f12746P = commonCallback;
            this.f12747Q = imageView;
            this.f12748R = imageOptions;
            this.f12749S = str;
        }

        public void run() {
            try {
                if (this.f12746P instanceof Callback.ProgressCallback) {
                    ((Callback.ProgressCallback) this.f12746P).onWaiting();
                }
                if (!(this.f12747Q == null || this.f12748R == null)) {
                    this.f12747Q.setScaleType(this.f12748R.getPlaceholderScaleType());
                    this.f12747Q.setImageDrawable(this.f12748R.getFailureDrawable(this.f12747Q));
                }
                if (this.f12746P != null) {
                    this.f12746P.onError(new IllegalArgumentException(this.f12749S), false);
                }
                Callback.CommonCallback commonCallback = this.f12746P;
                if (commonCallback != null) {
                    commonCallback.onFinished();
                    return;
                }
                return;
            } catch (Throwable th) {
                LogUtil.m20120e(th.getMessage(), th);
                return;
            }
            Callback.CommonCallback commonCallback2 = this.f12746P;
            if (commonCallback2 != null) {
                commonCallback2.onFinished();
            }
        }
    }

    @SuppressLint({"ViewConstructor"})
    /* renamed from: org.xutils.image.a$d */
    /* compiled from: ImageLoader */
    private static final class C5216d extends ImageView {

        /* renamed from: P */
        private Drawable f12750P;

        public C5216d() {
            super(C5217x.app());
        }

        public Drawable getDrawable() {
            return this.f12750P;
        }

        public void setImageDrawable(Drawable drawable) {
            this.f12750P = drawable;
        }

        public void setLayerType(int i, Paint paint) {
        }

        public void setScaleType(ImageView.ScaleType scaleType) {
        }

        public void startAnimation(Animation animation) {
        }
    }

    static {
        int memoryClass = (((ActivityManager) C5217x.app().getSystemService("activity")).getMemoryClass() * 1048576) / 8;
        if (memoryClass < 4194304) {
            memoryClass = 4194304;
        }
        f12729o.resize(memoryClass);
    }

    private ImageLoader() {
    }

    public void cancel() {
        this.f12736e = true;
        this.f12737f = true;
        Callback.Cancelable cancelable = this.f12738g;
        if (cancelable != null) {
            cancelable.cancel();
        }
    }

    public Type getLoadType() {
        return f12731q;
    }

    public boolean isCancelled() {
        return this.f12737f || !m20238a(false);
    }

    public void onCancelled(Callback.CancelledException cancelledException) {
        Callback.CommonCallback<Drawable> commonCallback;
        this.f12736e = true;
        if (m20238a(false) && (commonCallback = this.f12739h) != null) {
            commonCallback.onCancelled(cancelledException);
        }
    }

    public void onError(Throwable th, boolean z) {
        this.f12736e = true;
        if (m20238a(false)) {
            if (th instanceof FileLockedException) {
                LogUtil.m20117d("ImageFileLocked: " + this.f12732a.f12751a);
                C5217x.task().postDelayed(new C5214b(), 10);
                return;
            }
            LogUtil.m20120e(this.f12732a.f12751a, th);
            m20243c();
            Callback.CommonCallback<Drawable> commonCallback = this.f12739h;
            if (commonCallback != null) {
                commonCallback.onError(th, z);
            }
        }
    }

    public void onFinished() {
        Callback.CommonCallback<Drawable> commonCallback;
        this.f12736e = true;
        if (this.f12734c.get() instanceof C5216d) {
            synchronized (f12730p) {
                f12730p.remove(this.f12732a.f12751a);
            }
        }
        if (m20238a(false) && (commonCallback = this.f12739h) != null) {
            commonCallback.onFinished();
        }
    }

    public void onLoading(long j, long j2, boolean z) {
        Callback.ProgressCallback<Drawable> progressCallback;
        if (m20238a(true) && (progressCallback = this.f12742k) != null) {
            progressCallback.onLoading(j, j2, z);
        }
    }

    public void onStarted() {
        Callback.ProgressCallback<Drawable> progressCallback;
        if (m20238a(true) && (progressCallback = this.f12742k) != null) {
            progressCallback.onStarted();
        }
    }

    public void onWaiting() {
        Callback.ProgressCallback<Drawable> progressCallback = this.f12742k;
        if (progressCallback != null) {
            progressCallback.onWaiting();
        }
    }

    /* renamed from: a */
    static void m20235a() {
        LruDiskCache.getDiskCache("xUtils_img").clearCacheFiles();
    }

    /* renamed from: b */
    static void m20241b() {
        f12729o.evictAll();
    }

    /* renamed from: c */
    private void m20243c() {
        ImageView imageView = this.f12734c.get();
        if (imageView != null) {
            Drawable failureDrawable = this.f12733b.getFailureDrawable(imageView);
            imageView.setScaleType(this.f12733b.getPlaceholderScaleType());
            imageView.setImageDrawable(failureDrawable);
        }
    }

    public boolean onCache(Drawable drawable) {
        if (!m20238a(true) || drawable == null) {
            return false;
        }
        this.f12743l = true;
        m20236a(drawable);
        Callback.CacheCallback<Drawable> cacheCallback = this.f12741j;
        if (cacheCallback != null) {
            return cacheCallback.onCache(drawable);
        }
        Callback.CommonCallback<Drawable> commonCallback = this.f12739h;
        if (commonCallback != null) {
            commonCallback.onSuccess(drawable);
        }
        return true;
    }

    public void onSuccess(Drawable drawable) {
        if (m20238a(!this.f12743l) && drawable != null) {
            m20236a(drawable);
            Callback.CommonCallback<Drawable> commonCallback = this.f12739h;
            if (commonCallback != null) {
                commonCallback.onSuccess(drawable);
            }
        }
    }

    public Drawable prepare(File file) {
        if (!m20238a(true)) {
            return null;
        }
        try {
            Drawable prepare = this.f12740i != null ? this.f12740i.prepare(file) : null;
            if (prepare == null) {
                prepare = ImageDecoder.m20204a(file, this.f12733b, this);
            }
            if (prepare != null && (prepare instanceof ReusableDrawable)) {
                ((ReusableDrawable) prepare).setMemCacheKey(this.f12732a);
                f12729o.put(this.f12732a, prepare);
            }
            return prepare;
        } catch (IOException e) {
            IOUtil.deleteFileOrDir(file);
            LogUtil.m20126w(e.getMessage(), e);
            return null;
        }
    }

    /* renamed from: a */
    static Callback.Cancelable m20233a(String str, ImageOptions imageOptions, Callback.CommonCallback<Drawable> commonCallback) {
        C5216d dVar;
        if (TextUtils.isEmpty(str)) {
            m20237a((ImageView) null, imageOptions, "url is null", commonCallback);
            return null;
        }
        synchronized (f12730p) {
            dVar = f12730p.get(str);
            if (dVar == null) {
                dVar = new C5216d();
            }
        }
        return m20231a(dVar, str, imageOptions, commonCallback);
    }

    /* renamed from: b */
    private Callback.Cancelable m20239b(ImageView imageView, String str, ImageOptions imageOptions, Callback.CommonCallback<Drawable> commonCallback) {
        this.f12734c = new WeakReference<>(imageView);
        this.f12733b = imageOptions;
        this.f12732a = new MemCacheKey(str, imageOptions);
        this.f12739h = commonCallback;
        if (commonCallback instanceof Callback.ProgressCallback) {
            this.f12742k = (Callback.ProgressCallback) commonCallback;
        }
        if (commonCallback instanceof Callback.PrepareCallback) {
            this.f12740i = (Callback.PrepareCallback) commonCallback;
        }
        if (commonCallback instanceof Callback.CacheCallback) {
            this.f12741j = (Callback.CacheCallback) commonCallback;
        }
        if (imageOptions.isForceLoadingDrawable()) {
            Drawable loadingDrawable = imageOptions.getLoadingDrawable(imageView);
            imageView.setScaleType(imageOptions.getPlaceholderScaleType());
            imageView.setImageDrawable(new AsyncDrawable(this, loadingDrawable));
        } else {
            imageView.setImageDrawable(new AsyncDrawable(this, imageView.getDrawable()));
        }
        RequestParams a = m20234a(str, imageOptions);
        if (imageView instanceof C5216d) {
            synchronized (f12730p) {
                f12730p.put(str, (C5216d) imageView);
            }
        }
        Callback.Cancelable cancelable = C5217x.http().get(a, this);
        this.f12738g = cancelable;
        return cancelable;
    }

    /* renamed from: a */
    static Callback.Cancelable m20232a(String str, ImageOptions imageOptions, Callback.CacheCallback<File> cacheCallback) {
        if (TextUtils.isEmpty(str)) {
            m20237a((ImageView) null, imageOptions, "url is null", cacheCallback);
            return null;
        }
        return C5217x.http().get(m20234a(str, imageOptions), cacheCallback);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        if (r2.isRecycled() != false) goto L_0x007b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x00ff  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static org.xutils.common.Callback.Cancelable m20231a(android.widget.ImageView r5, java.lang.String r6, org.xutils.image.ImageOptions r7, org.xutils.common.Callback.CommonCallback<android.graphics.drawable.Drawable> r8) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0009
            java.lang.String r5 = "view is null"
            m20237a(r0, r7, r5, r8)
            return r0
        L_0x0009:
            boolean r1 = android.text.TextUtils.isEmpty(r6)
            if (r1 == 0) goto L_0x0015
            java.lang.String r6 = "url is null"
            m20237a(r5, r7, r6, r8)
            return r0
        L_0x0015:
            if (r7 != 0) goto L_0x0019
            org.xutils.image.ImageOptions r7 = org.xutils.image.ImageOptions.DEFAULT
        L_0x0019:
            r7.mo28962a(r5)
            org.xutils.image.b r1 = new org.xutils.image.b
            r1.<init>(r6, r7)
            android.graphics.drawable.Drawable r2 = r5.getDrawable()
            boolean r3 = r2 instanceof org.xutils.image.AsyncDrawable
            if (r3 == 0) goto L_0x0042
            org.xutils.image.AsyncDrawable r2 = (org.xutils.image.AsyncDrawable) r2
            org.xutils.image.a r2 = r2.getImageLoader()
            if (r2 == 0) goto L_0x005a
            boolean r3 = r2.f12736e
            if (r3 != 0) goto L_0x005a
            org.xutils.image.b r3 = r2.f12732a
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x003e
            return r0
        L_0x003e:
            r2.cancel()
            goto L_0x005a
        L_0x0042:
            boolean r3 = r2 instanceof org.xutils.image.ReusableDrawable
            if (r3 == 0) goto L_0x005a
            r3 = r2
            org.xutils.image.d r3 = (org.xutils.image.ReusableDrawable) r3
            org.xutils.image.b r3 = r3.getMemCacheKey()
            if (r3 == 0) goto L_0x005a
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x005a
            org.xutils.cache.LruCache<org.xutils.image.b, android.graphics.drawable.Drawable> r3 = org.xutils.image.ImageLoader.f12729o
            r3.put(r1, r2)
        L_0x005a:
            boolean r2 = r7.isUseMemCache()
            if (r2 == 0) goto L_0x007b
            org.xutils.cache.LruCache<org.xutils.image.b, android.graphics.drawable.Drawable> r2 = org.xutils.image.ImageLoader.f12729o
            java.lang.Object r1 = r2.get(r1)
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            boolean r2 = r1 instanceof android.graphics.drawable.BitmapDrawable
            if (r2 == 0) goto L_0x007c
            r2 = r1
            android.graphics.drawable.BitmapDrawable r2 = (android.graphics.drawable.BitmapDrawable) r2
            android.graphics.Bitmap r2 = r2.getBitmap()
            if (r2 == 0) goto L_0x007b
            boolean r2 = r2.isRecycled()
            if (r2 == 0) goto L_0x007c
        L_0x007b:
            r1 = r0
        L_0x007c:
            if (r1 == 0) goto L_0x00ff
            r2 = 0
            boolean r3 = r8 instanceof org.xutils.common.Callback.ProgressCallback     // Catch:{ all -> 0x00d7 }
            if (r3 == 0) goto L_0x0089
            r3 = r8
            org.xutils.common.Callback$ProgressCallback r3 = (org.xutils.common.Callback.ProgressCallback) r3     // Catch:{ all -> 0x00d7 }
            r3.onWaiting()     // Catch:{ all -> 0x00d7 }
        L_0x0089:
            android.widget.ImageView$ScaleType r3 = r7.getImageScaleType()     // Catch:{ all -> 0x00d7 }
            r5.setScaleType(r3)     // Catch:{ all -> 0x00d7 }
            r5.setImageDrawable(r1)     // Catch:{ all -> 0x00d7 }
            r3 = 1
            boolean r4 = r8 instanceof org.xutils.common.Callback.CacheCallback     // Catch:{ all -> 0x00d5 }
            if (r4 == 0) goto L_0x00be
            r4 = r8
            org.xutils.common.Callback$CacheCallback r4 = (org.xutils.common.Callback.CacheCallback) r4     // Catch:{ all -> 0x00d5 }
            boolean r1 = r4.onCache(r1)     // Catch:{ all -> 0x00d5 }
            if (r1 != 0) goto L_0x00c4
            org.xutils.image.a r0 = new org.xutils.image.a     // Catch:{ all -> 0x00bb }
            r0.<init>()     // Catch:{ all -> 0x00bb }
            org.xutils.common.Callback$Cancelable r5 = r0.m20239b(r5, r6, r7, r8)     // Catch:{ all -> 0x00bb }
            if (r1 == 0) goto L_0x00ba
            if (r8 == 0) goto L_0x00ba
            r8.onFinished()     // Catch:{ all -> 0x00b2 }
            goto L_0x00ba
        L_0x00b2:
            r6 = move-exception
            java.lang.String r7 = r6.getMessage()
            org.xutils.common.util.LogUtil.m20120e(r7, r6)
        L_0x00ba:
            return r5
        L_0x00bb:
            r0 = move-exception
            r3 = r1
            goto L_0x00d9
        L_0x00be:
            if (r8 == 0) goto L_0x00c3
            r8.onSuccess(r1)     // Catch:{ all -> 0x00d5 }
        L_0x00c3:
            r1 = 1
        L_0x00c4:
            if (r1 == 0) goto L_0x00d4
            if (r8 == 0) goto L_0x00d4
            r8.onFinished()     // Catch:{ all -> 0x00cc }
            goto L_0x00d4
        L_0x00cc:
            r5 = move-exception
            java.lang.String r6 = r5.getMessage()
            org.xutils.common.util.LogUtil.m20120e(r6, r5)
        L_0x00d4:
            return r0
        L_0x00d5:
            r0 = move-exception
            goto L_0x00d9
        L_0x00d7:
            r0 = move-exception
            r3 = 0
        L_0x00d9:
            java.lang.String r1 = r0.getMessage()     // Catch:{ all -> 0x00ec }
            org.xutils.common.util.LogUtil.m20120e(r1, r0)     // Catch:{ all -> 0x00ec }
            org.xutils.image.a r0 = new org.xutils.image.a     // Catch:{ all -> 0x00ea }
            r0.<init>()     // Catch:{ all -> 0x00ea }
            org.xutils.common.Callback$Cancelable r5 = r0.m20239b(r5, r6, r7, r8)     // Catch:{ all -> 0x00ea }
            return r5
        L_0x00ea:
            r5 = move-exception
            goto L_0x00ee
        L_0x00ec:
            r5 = move-exception
            r2 = r3
        L_0x00ee:
            if (r2 == 0) goto L_0x00fe
            if (r8 == 0) goto L_0x00fe
            r8.onFinished()     // Catch:{ all -> 0x00f6 }
            goto L_0x00fe
        L_0x00f6:
            r6 = move-exception
            java.lang.String r7 = r6.getMessage()
            org.xutils.common.util.LogUtil.m20120e(r7, r6)
        L_0x00fe:
            throw r5
        L_0x00ff:
            org.xutils.image.a r0 = new org.xutils.image.a
            r0.<init>()
            org.xutils.common.Callback$Cancelable r5 = r0.m20239b(r5, r6, r7, r8)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.image.ImageLoader.m20231a(android.widget.ImageView, java.lang.String, org.xutils.image.ImageOptions, org.xutils.common.Callback$CommonCallback):org.xutils.common.Callback$Cancelable");
    }

    /* renamed from: a */
    private static RequestParams m20234a(String str, ImageOptions imageOptions) {
        ImageOptions.ParamsBuilder paramsBuilder;
        RequestParams requestParams = new RequestParams(str);
        requestParams.setCacheDirName("xUtils_img");
        requestParams.setConnectTimeout(8000);
        requestParams.setPriority(Priority.BG_LOW);
        requestParams.setExecutor(f12728n);
        requestParams.setCancelFast(true);
        requestParams.setUseCookie(false);
        return (imageOptions == null || (paramsBuilder = imageOptions.getParamsBuilder()) == null) ? requestParams : paramsBuilder.buildParams(requestParams, imageOptions);
    }

    /* renamed from: a */
    private boolean m20238a(boolean z) {
        ImageView imageView = this.f12734c.get();
        if (imageView == null) {
            return false;
        }
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof AsyncDrawable) {
            ImageLoader imageLoader = ((AsyncDrawable) drawable).getImageLoader();
            if (imageLoader != null) {
                if (imageLoader == this) {
                    if (imageView.getVisibility() == 0) {
                        return true;
                    }
                    imageLoader.cancel();
                    return false;
                } else if (this.f12735d > imageLoader.f12735d) {
                    imageLoader.cancel();
                    return true;
                } else {
                    cancel();
                    return false;
                }
            }
        } else if (z) {
            cancel();
            return false;
        }
        return true;
    }

    /* renamed from: a */
    private void m20236a(Drawable drawable) {
        ImageView imageView = this.f12734c.get();
        if (imageView != null) {
            imageView.setScaleType(this.f12733b.getImageScaleType());
            if (drawable instanceof GifDrawable) {
                if (imageView.getScaleType() == ImageView.ScaleType.CENTER) {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                imageView.setLayerType(1, null);
            }
            if (this.f12733b.getAnimation() != null) {
                ImageAnimationHelper.animationDisplay(imageView, drawable, this.f12733b.getAnimation());
            } else if (this.f12733b.isFadeIn()) {
                ImageAnimationHelper.fadeInDisplay(imageView, drawable);
            } else {
                imageView.setImageDrawable(drawable);
            }
        }
    }

    /* renamed from: a */
    private static void m20237a(ImageView imageView, ImageOptions imageOptions, String str, Callback.CommonCallback<?> commonCallback) {
        C5217x.task().autoPost(new C5215c(commonCallback, imageView, imageOptions, str));
    }
}
