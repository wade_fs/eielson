package org.xutils.image;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

public final class AsyncDrawable extends Drawable {

    /* renamed from: a */
    private final WeakReference<ImageLoader> f12670a;

    /* renamed from: b */
    private Drawable f12671b;

    public AsyncDrawable(ImageLoader aVar, Drawable drawable) {
        if (aVar != null) {
            this.f12671b = super;
            while (true) {
                Drawable drawable2 = this.f12671b;
                if (drawable2 instanceof AsyncDrawable) {
                    this.f12671b = ((AsyncDrawable) drawable2).f12671b;
                } else {
                    this.f12670a = new WeakReference<>(aVar);
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("imageLoader may not be null");
        }
    }

    public void clearColorFilter() {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.clearColorFilter();
        }
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        ImageLoader imageLoader = getImageLoader();
        if (imageLoader != null) {
            imageLoader.cancel();
        }
    }

    public Drawable getBaseDrawable() {
        return this.f12671b;
    }

    public int getChangingConfigurations() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return 0;
        }
        return super.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return null;
        }
        return super.getConstantState();
    }

    public Drawable getCurrent() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return null;
        }
        return super.getCurrent();
    }

    public ImageLoader getImageLoader() {
        return this.f12670a.get();
    }

    public int getIntrinsicHeight() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return 0;
        }
        return super.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return 0;
        }
        return super.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return 0;
        }
        return super.getMinimumHeight();
    }

    public int getMinimumWidth() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return 0;
        }
        return super.getMinimumWidth();
    }

    public int getOpacity() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return -3;
        }
        return super.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        Drawable drawable = this.f12671b;
        return drawable != null && super.getPadding(rect);
    }

    public int[] getState() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return null;
        }
        return super.getState();
    }

    public Region getTransparentRegion() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return null;
        }
        return super.getTransparentRegion();
    }

    public void invalidateSelf() {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.invalidateSelf();
        }
    }

    public boolean isStateful() {
        Drawable drawable = this.f12671b;
        return drawable != null && super.isStateful();
    }

    public Drawable mutate() {
        Drawable drawable = this.f12671b;
        if (drawable == null) {
            return null;
        }
        return super.mutate();
    }

    public void scheduleSelf(Runnable runnable, long j) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.scheduleSelf(runnable, j);
        }
    }

    public void setAlpha(int i) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setAlpha(i);
        }
    }

    public void setBaseDrawable(Drawable drawable) {
        this.f12671b = super;
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setBounds(i, i2, i3, i4);
        }
    }

    public void setChangingConfigurations(int i) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setChangingConfigurations(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setColorFilter(colorFilter);
        }
    }

    public void setDither(boolean z) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setDither(z);
        }
    }

    public void setFilterBitmap(boolean z) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setFilterBitmap(z);
        }
    }

    public boolean setState(int[] iArr) {
        Drawable drawable = this.f12671b;
        return drawable != null && super.setState(iArr);
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.f12671b;
        return drawable != null && super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.unscheduleSelf(runnable);
        }
    }

    public void setBounds(Rect rect) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setBounds(rect);
        }
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        Drawable drawable = this.f12671b;
        if (drawable != null) {
            super.setColorFilter(i, mode);
        }
    }
}
