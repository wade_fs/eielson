package org.xutils.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/* renamed from: org.xutils.image.c */
final class ReusableBitmapDrawable extends BitmapDrawable implements ReusableDrawable {

    /* renamed from: P */
    private MemCacheKey f12753P;

    public ReusableBitmapDrawable(Resources resources, Bitmap bitmap) {
        super(resources, bitmap);
    }

    public MemCacheKey getMemCacheKey() {
        return this.f12753P;
    }

    public void setMemCacheKey(MemCacheKey bVar) {
        this.f12753P = bVar;
    }
}
