package org.xutils.image;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.io.File;
import org.xutils.C5217x;
import org.xutils.ImageManager;
import org.xutils.common.Callback;

public final class ImageManagerImpl implements ImageManager {

    /* renamed from: a */
    private static final Object f12690a = new Object();

    /* renamed from: b */
    private static volatile ImageManagerImpl f12691b;

    /* renamed from: org.xutils.image.ImageManagerImpl$a */
    class C5209a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ ImageView f12692P;

        /* renamed from: Q */
        final /* synthetic */ String f12693Q;

        C5209a(ImageManagerImpl imageManagerImpl, ImageView imageView, String str) {
            this.f12692P = imageView;
            this.f12693Q = str;
        }

        public void run() {
            ImageLoader.m20231a(this.f12692P, this.f12693Q, (ImageOptions) null, (Callback.CommonCallback<Drawable>) null);
        }
    }

    /* renamed from: org.xutils.image.ImageManagerImpl$b */
    class C5210b implements Runnable {

        /* renamed from: P */
        final /* synthetic */ ImageView f12694P;

        /* renamed from: Q */
        final /* synthetic */ String f12695Q;

        /* renamed from: R */
        final /* synthetic */ ImageOptions f12696R;

        C5210b(ImageManagerImpl imageManagerImpl, ImageView imageView, String str, ImageOptions imageOptions) {
            this.f12694P = imageView;
            this.f12695Q = str;
            this.f12696R = imageOptions;
        }

        public void run() {
            ImageLoader.m20231a(this.f12694P, this.f12695Q, this.f12696R, (Callback.CommonCallback<Drawable>) null);
        }
    }

    /* renamed from: org.xutils.image.ImageManagerImpl$c */
    class C5211c implements Runnable {

        /* renamed from: P */
        final /* synthetic */ ImageView f12697P;

        /* renamed from: Q */
        final /* synthetic */ String f12698Q;

        /* renamed from: R */
        final /* synthetic */ Callback.CommonCallback f12699R;

        C5211c(ImageManagerImpl imageManagerImpl, ImageView imageView, String str, Callback.CommonCallback commonCallback) {
            this.f12697P = imageView;
            this.f12698Q = str;
            this.f12699R = commonCallback;
        }

        public void run() {
            ImageLoader.m20231a(this.f12697P, this.f12698Q, (ImageOptions) null, this.f12699R);
        }
    }

    /* renamed from: org.xutils.image.ImageManagerImpl$d */
    class C5212d implements Runnable {

        /* renamed from: P */
        final /* synthetic */ ImageView f12700P;

        /* renamed from: Q */
        final /* synthetic */ String f12701Q;

        /* renamed from: R */
        final /* synthetic */ ImageOptions f12702R;

        /* renamed from: S */
        final /* synthetic */ Callback.CommonCallback f12703S;

        C5212d(ImageManagerImpl imageManagerImpl, ImageView imageView, String str, ImageOptions imageOptions, Callback.CommonCallback commonCallback) {
            this.f12700P = imageView;
            this.f12701Q = str;
            this.f12702R = imageOptions;
            this.f12703S = commonCallback;
        }

        public void run() {
            ImageLoader.m20231a(this.f12700P, this.f12701Q, this.f12702R, this.f12703S);
        }
    }

    private ImageManagerImpl() {
    }

    public static void registerInstance() {
        if (f12691b == null) {
            synchronized (f12690a) {
                if (f12691b == null) {
                    f12691b = new ImageManagerImpl();
                }
            }
        }
        C5217x.Ext.setImageManager(f12691b);
    }

    public void bind(ImageView imageView, String str) {
        C5217x.task().autoPost(new C5209a(this, imageView, str));
    }

    public void clearCacheFiles() {
        ImageLoader.m20235a();
        ImageDecoder.m20205a();
    }

    public void clearMemCache() {
        ImageLoader.m20241b();
    }

    public Callback.Cancelable loadDrawable(String str, ImageOptions imageOptions, Callback.CommonCallback<Drawable> commonCallback) {
        return ImageLoader.m20233a(str, imageOptions, commonCallback);
    }

    public Callback.Cancelable loadFile(String str, ImageOptions imageOptions, Callback.CacheCallback<File> cacheCallback) {
        return ImageLoader.m20232a(str, imageOptions, cacheCallback);
    }

    public void bind(ImageView imageView, String str, ImageOptions imageOptions) {
        C5217x.task().autoPost(new C5210b(this, imageView, str, imageOptions));
    }

    public void bind(ImageView imageView, String str, Callback.CommonCallback<Drawable> commonCallback) {
        C5217x.task().autoPost(new C5211c(this, imageView, str, commonCallback));
    }

    public void bind(ImageView imageView, String str, ImageOptions imageOptions, Callback.CommonCallback<Drawable> commonCallback) {
        C5217x.task().autoPost(new C5212d(this, imageView, str, imageOptions, commonCallback));
    }
}
