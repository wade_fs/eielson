package org.xutils.common.util;

import android.text.TextUtils;
import android.util.Log;
import org.xutils.C5217x;

public class LogUtil {
    public static String customTagPrefix = "x_log";

    private LogUtil() {
    }

    /* renamed from: a */
    private static String m20116a() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[2];
        String className = stackTraceElement.getClassName();
        String format = String.format("%s.%s(L:%d)", className.substring(className.lastIndexOf(".") + 1), stackTraceElement.getMethodName(), Integer.valueOf(stackTraceElement.getLineNumber()));
        if (TextUtils.isEmpty(customTagPrefix)) {
            return format;
        }
        return customTagPrefix + ":" + format;
    }

    /* renamed from: d */
    public static void m20117d(String str) {
        if (C5217x.isDebug()) {
            Log.d(m20116a(), str);
        }
    }

    /* renamed from: e */
    public static void m20119e(String str) {
        if (C5217x.isDebug()) {
            Log.e(m20116a(), str);
        }
    }

    /* renamed from: i */
    public static void m20121i(String str) {
        if (C5217x.isDebug()) {
            Log.i(m20116a(), str);
        }
    }

    /* renamed from: v */
    public static void m20123v(String str) {
        if (C5217x.isDebug()) {
            Log.v(m20116a(), str);
        }
    }

    /* renamed from: w */
    public static void m20125w(String str) {
        if (C5217x.isDebug()) {
            Log.w(m20116a(), str);
        }
    }

    public static void wtf(String str) {
        if (C5217x.isDebug()) {
            Log.wtf(m20116a(), str);
        }
    }

    /* renamed from: d */
    public static void m20118d(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.d(m20116a(), str, th);
        }
    }

    /* renamed from: e */
    public static void m20120e(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.e(m20116a(), str, th);
        }
    }

    /* renamed from: i */
    public static void m20122i(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.i(m20116a(), str, th);
        }
    }

    /* renamed from: v */
    public static void m20124v(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.v(m20116a(), str, th);
        }
    }

    /* renamed from: w */
    public static void m20126w(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.w(m20116a(), str, th);
        }
    }

    public static void wtf(String str, Throwable th) {
        if (C5217x.isDebug()) {
            Log.wtf(m20116a(), str, th);
        }
    }

    /* renamed from: w */
    public static void m20127w(Throwable th) {
        if (C5217x.isDebug()) {
            Log.w(m20116a(), th);
        }
    }

    public static void wtf(Throwable th) {
        if (C5217x.isDebug()) {
            Log.wtf(m20116a(), th);
        }
    }
}
