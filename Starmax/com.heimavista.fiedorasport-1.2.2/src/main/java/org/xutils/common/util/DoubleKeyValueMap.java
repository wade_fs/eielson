package org.xutils.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DoubleKeyValueMap<K1, K2, V> {

    /* renamed from: a */
    private final ConcurrentHashMap<K1, ConcurrentHashMap<K2, V>> f12478a = new ConcurrentHashMap<>();

    public void clear() {
        if (this.f12478a.size() > 0) {
            for (ConcurrentHashMap<K2, V> concurrentHashMap : this.f12478a.values()) {
                concurrentHashMap.clear();
            }
            this.f12478a.clear();
        }
    }

    public boolean containsKey(K1 k1, K2 k2) {
        if (this.f12478a.containsKey(k1)) {
            return this.f12478a.get(k1).containsKey(k2);
        }
        return false;
    }

    public ConcurrentHashMap<K2, V> get(K1 k1) {
        return this.f12478a.get(k1);
    }

    public Collection<V> getAllValues(K1 k1) {
        ConcurrentHashMap concurrentHashMap = this.f12478a.get(k1);
        if (concurrentHashMap == null) {
            return null;
        }
        return concurrentHashMap.values();
    }

    public Set<K1> getFirstKeys() {
        return this.f12478a.keySet();
    }

    public void put(K1 k1, K2 k2, V v) {
        if (k1 != null && k2 != null && v != null) {
            if (this.f12478a.containsKey(k1)) {
                ConcurrentHashMap concurrentHashMap = this.f12478a.get(k1);
                if (concurrentHashMap != null) {
                    concurrentHashMap.put(k2, v);
                    return;
                }
                ConcurrentHashMap concurrentHashMap2 = new ConcurrentHashMap();
                concurrentHashMap2.put(k2, v);
                this.f12478a.put(k1, concurrentHashMap2);
                return;
            }
            ConcurrentHashMap concurrentHashMap3 = new ConcurrentHashMap();
            concurrentHashMap3.put(k2, v);
            this.f12478a.put(k1, concurrentHashMap3);
        }
    }

    public void remove(K1 k1) {
        this.f12478a.remove(k1);
    }

    public int size() {
        int i = 0;
        if (this.f12478a.size() == 0) {
            return 0;
        }
        for (ConcurrentHashMap<K2, V> concurrentHashMap : this.f12478a.values()) {
            i += concurrentHashMap.size();
        }
        return i;
    }

    public V get(K1 k1, K2 k2) {
        ConcurrentHashMap concurrentHashMap = this.f12478a.get(k1);
        if (concurrentHashMap == null) {
            return null;
        }
        return concurrentHashMap.get(k2);
    }

    public void remove(K1 k1, K2 k2) {
        ConcurrentHashMap concurrentHashMap = this.f12478a.get(k1);
        if (concurrentHashMap != null) {
            concurrentHashMap.remove(k2);
        }
        if (concurrentHashMap == null || concurrentHashMap.isEmpty()) {
            this.f12478a.remove(k1);
        }
    }

    public boolean containsKey(K1 k1) {
        return this.f12478a.containsKey(k1);
    }

    public Collection<V> getAllValues() {
        Set<K1> keySet = this.f12478a.keySet();
        if (keySet == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (K1 k1 : keySet) {
            Collection values = this.f12478a.get(k1).values();
            if (values != null) {
                arrayList.addAll(values);
            }
        }
        return arrayList;
    }
}
