package org.xutils.common.util;

import com.google.android.exoplayer2.C1750C;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5 {

    /* renamed from: a */
    private static final char[] f12479a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    private MD5() {
    }

    public static String md5(File file) {
        Closeable closeable;
        FileInputStream fileInputStream = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                FileChannel channel = fileInputStream2.getChannel();
                instance.update(channel.map(FileChannel.MapMode.READ_ONLY, 0, file.length()));
                byte[] digest = instance.digest();
                IOUtil.closeQuietly(fileInputStream2);
                IOUtil.closeQuietly(channel);
                return toHexString(digest);
            } catch (NoSuchAlgorithmException e) {
                e = e;
                closeable = null;
                fileInputStream = fileInputStream2;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    th = th;
                    IOUtil.closeQuietly(fileInputStream);
                    IOUtil.closeQuietly(closeable);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                closeable = null;
                fileInputStream = fileInputStream2;
                IOUtil.closeQuietly(fileInputStream);
                IOUtil.closeQuietly(closeable);
                throw th;
            }
        } catch (NoSuchAlgorithmException e2) {
            e = e2;
            closeable = null;
            throw new RuntimeException(e);
        } catch (Throwable th3) {
            th = th3;
            closeable = null;
            IOUtil.closeQuietly(fileInputStream);
            IOUtil.closeQuietly(closeable);
            throw th;
        }
    }

    public static String toHexString(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            sb.append(f12479a[(b >> 4) & 15]);
            sb.append(f12479a[b & 15]);
        }
        return sb.toString();
    }

    public static String md5(String str) {
        try {
            return toHexString(MessageDigest.getInstance("MD5").digest(str.getBytes(C1750C.UTF8_NAME)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }
}
