package org.xutils.common.util;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.io.Closeable;
import java.io.File;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DecimalFormat;
import java.util.concurrent.ConcurrentHashMap;
import org.xutils.C5217x;

public final class ProcessLock implements Closeable {

    /* renamed from: U */
    private static final DoubleKeyValueMap<String, Integer, ProcessLock> f12480U = new DoubleKeyValueMap<>();

    /* renamed from: V */
    private static final DecimalFormat f12481V = new DecimalFormat("0.##################");

    /* renamed from: P */
    private final String f12482P;

    /* renamed from: Q */
    private final FileLock f12483Q;

    /* renamed from: R */
    private final File f12484R;

    /* renamed from: S */
    private final Closeable f12485S;

    /* renamed from: T */
    private final boolean f12486T;

    static {
        IOUtil.deleteFileOrDir(C5217x.app().getDir("process_lock", 0));
    }

    private ProcessLock(String str, File file, FileLock fileLock, Closeable closeable, boolean z) {
        this.f12482P = str;
        this.f12483Q = fileLock;
        this.f12484R = file;
        this.f12485S = closeable;
        this.f12486T = z;
    }

    /* renamed from: a */
    private static boolean m20131a(FileLock fileLock) {
        return fileLock != null && fileLock.isValid();
    }

    /* renamed from: b */
    private static String m20132b(String str) {
        if (TextUtils.isEmpty(str)) {
            return AppEventsConstants.EVENT_PARAM_VALUE_NO;
        }
        double d = 0.0d;
        byte[] bytes = str.getBytes();
        for (int i = 0; i < str.length(); i++) {
            d = ((d * 255.0d) + ((double) bytes[i])) * 0.005d;
        }
        return f12481V.format(d);
    }

    public static ProcessLock tryLock(String str, boolean z) {
        return m20129a(str, m20132b(str), z);
    }

    public void close() {
        release();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        release();
    }

    public boolean isValid() {
        return m20131a(this.f12483Q);
    }

    public void release() {
        m20130a(this.f12482P, this.f12483Q, this.f12484R, this.f12485S);
    }

    public String toString() {
        return this.f12482P + ": " + this.f12484R.getName();
    }

    /* renamed from: a */
    private static void m20130a(String str, FileLock fileLock, File file, Closeable closeable) {
        FileChannel channel;
        synchronized (f12480U) {
            if (fileLock != null) {
                try {
                    f12480U.remove(str, Integer.valueOf(fileLock.hashCode()));
                    ConcurrentHashMap<Integer, ProcessLock> concurrentHashMap = f12480U.get(str);
                    if (concurrentHashMap == null || concurrentHashMap.isEmpty()) {
                        IOUtil.deleteFileOrDir(file);
                    }
                    if (fileLock.channel().isOpen()) {
                        fileLock.release();
                    }
                    channel = fileLock.channel();
                } catch (Throwable th) {
                    IOUtil.closeQuietly(fileLock.channel());
                    throw th;
                }
                IOUtil.closeQuietly(channel);
            }
            IOUtil.closeQuietly(closeable);
        }
    }

    public static ProcessLock tryLock(String str, boolean z, long j) {
        long currentTimeMillis = System.currentTimeMillis() + j;
        String b = m20132b(str);
        ProcessLock processLock = null;
        while (System.currentTimeMillis() < currentTimeMillis && (processLock = m20129a(str, b, z)) == null) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw e;
            } catch (Throwable unused) {
            }
        }
        return processLock;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v5, resolved type: java.io.FileInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v7, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v10, resolved type: java.io.FileInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v11, resolved type: java.io.FileInputStream} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.xutils.common.util.ProcessLock m20129a(java.lang.String r12, java.lang.String r13, boolean r14) {
        /*
            org.xutils.common.util.DoubleKeyValueMap<java.lang.String, java.lang.Integer, org.xutils.common.util.ProcessLock> r0 = org.xutils.common.util.ProcessLock.f12480U
            monitor-enter(r0)
            org.xutils.common.util.DoubleKeyValueMap<java.lang.String, java.lang.Integer, org.xutils.common.util.ProcessLock> r1 = org.xutils.common.util.ProcessLock.f12480U     // Catch:{ all -> 0x00f7 }
            java.util.concurrent.ConcurrentHashMap r1 = r1.get(r12)     // Catch:{ all -> 0x00f7 }
            r2 = 0
            if (r1 == 0) goto L_0x0046
            boolean r3 = r1.isEmpty()     // Catch:{ all -> 0x00f7 }
            if (r3 != 0) goto L_0x0046
            java.util.Set r1 = r1.entrySet()     // Catch:{ all -> 0x00f7 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00f7 }
        L_0x001a:
            boolean r3 = r1.hasNext()     // Catch:{ all -> 0x00f7 }
            if (r3 == 0) goto L_0x0046
            java.lang.Object r3 = r1.next()     // Catch:{ all -> 0x00f7 }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ all -> 0x00f7 }
            java.lang.Object r3 = r3.getValue()     // Catch:{ all -> 0x00f7 }
            org.xutils.common.util.ProcessLock r3 = (org.xutils.common.util.ProcessLock) r3     // Catch:{ all -> 0x00f7 }
            if (r3 == 0) goto L_0x0042
            boolean r4 = r3.isValid()     // Catch:{ all -> 0x00f7 }
            if (r4 != 0) goto L_0x0038
            r1.remove()     // Catch:{ all -> 0x00f7 }
            goto L_0x001a
        L_0x0038:
            if (r14 == 0) goto L_0x003c
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            return r2
        L_0x003c:
            boolean r3 = r3.f12486T     // Catch:{ all -> 0x00f7 }
            if (r3 == 0) goto L_0x001a
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            return r2
        L_0x0042:
            r1.remove()     // Catch:{ all -> 0x00f7 }
            goto L_0x001a
        L_0x0046:
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x00cb }
            android.app.Application r1 = org.xutils.C5217x.app()     // Catch:{ all -> 0x00cb }
            java.lang.String r3 = "process_lock"
            r4 = 0
            java.io.File r1 = r1.getDir(r3, r4)     // Catch:{ all -> 0x00cb }
            r5.<init>(r1, r13)     // Catch:{ all -> 0x00cb }
            boolean r13 = r5.exists()     // Catch:{ all -> 0x00cb }
            if (r13 != 0) goto L_0x0062
            boolean r13 = r5.createNewFile()     // Catch:{ all -> 0x00cb }
            if (r13 == 0) goto L_0x00f5
        L_0x0062:
            if (r14 == 0) goto L_0x006e
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ all -> 0x00cb }
            r13.<init>(r5, r4)     // Catch:{ all -> 0x00cb }
            java.nio.channels.FileChannel r1 = r13.getChannel()     // Catch:{ all -> 0x00cb }
            goto L_0x0077
        L_0x006e:
            java.io.FileInputStream r13 = new java.io.FileInputStream     // Catch:{ all -> 0x00cb }
            r13.<init>(r5)     // Catch:{ all -> 0x00cb }
            java.nio.channels.FileChannel r1 = r13.getChannel()     // Catch:{ all -> 0x00cb }
        L_0x0077:
            if (r1 == 0) goto L_0x00ae
            r7 = 0
            r9 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r14 != 0) goto L_0x0085
            r4 = 1
            r11 = 1
            goto L_0x0086
        L_0x0085:
            r11 = 0
        L_0x0086:
            r6 = r1
            java.nio.channels.FileLock r9 = r6.tryLock(r7, r9, r11)     // Catch:{ all -> 0x00c9 }
            boolean r3 = m20131a(r9)     // Catch:{ all -> 0x00c9 }
            if (r3 == 0) goto L_0x00aa
            org.xutils.common.util.ProcessLock r10 = new org.xutils.common.util.ProcessLock     // Catch:{ all -> 0x00c9 }
            r3 = r10
            r4 = r12
            r6 = r9
            r7 = r13
            r8 = r14
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x00c9 }
            org.xutils.common.util.DoubleKeyValueMap<java.lang.String, java.lang.Integer, org.xutils.common.util.ProcessLock> r14 = org.xutils.common.util.ProcessLock.f12480U     // Catch:{ all -> 0x00c9 }
            int r3 = r9.hashCode()     // Catch:{ all -> 0x00c9 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00c9 }
            r14.put(r12, r3, r10)     // Catch:{ all -> 0x00c9 }
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            return r10
        L_0x00aa:
            m20130a(r12, r9, r5, r13)     // Catch:{ all -> 0x00c9 }
            goto L_0x00f5
        L_0x00ae:
            java.io.IOException r14 = new java.io.IOException     // Catch:{ all -> 0x00c9 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c9 }
            r3.<init>()     // Catch:{ all -> 0x00c9 }
            java.lang.String r4 = "can not get file channel:"
            r3.append(r4)     // Catch:{ all -> 0x00c9 }
            java.lang.String r4 = r5.getAbsolutePath()     // Catch:{ all -> 0x00c9 }
            r3.append(r4)     // Catch:{ all -> 0x00c9 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c9 }
            r14.<init>(r3)     // Catch:{ all -> 0x00c9 }
            throw r14     // Catch:{ all -> 0x00c9 }
        L_0x00c9:
            r14 = move-exception
            goto L_0x00cf
        L_0x00cb:
            r13 = move-exception
            r14 = r13
            r13 = r2
            r1 = r13
        L_0x00cf:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f7 }
            r3.<init>()     // Catch:{ all -> 0x00f7 }
            java.lang.String r4 = "tryLock: "
            r3.append(r4)     // Catch:{ all -> 0x00f7 }
            r3.append(r12)     // Catch:{ all -> 0x00f7 }
            java.lang.String r12 = ", "
            r3.append(r12)     // Catch:{ all -> 0x00f7 }
            java.lang.String r12 = r14.getMessage()     // Catch:{ all -> 0x00f7 }
            r3.append(r12)     // Catch:{ all -> 0x00f7 }
            java.lang.String r12 = r3.toString()     // Catch:{ all -> 0x00f7 }
            org.xutils.common.util.LogUtil.m20117d(r12)     // Catch:{ all -> 0x00f7 }
            org.xutils.common.util.IOUtil.closeQuietly(r13)     // Catch:{ all -> 0x00f7 }
            org.xutils.common.util.IOUtil.closeQuietly(r1)     // Catch:{ all -> 0x00f7 }
        L_0x00f5:
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            return r2
        L_0x00f7:
            r12 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.common.util.ProcessLock.m20129a(java.lang.String, java.lang.String, boolean):org.xutils.common.util.ProcessLock");
    }
}
