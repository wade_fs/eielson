package org.xutils.common.util;

import org.xutils.C5217x;

public final class DensityUtil {

    /* renamed from: a */
    private static float f12475a = -1.0f;

    /* renamed from: b */
    private static int f12476b = -1;

    /* renamed from: c */
    private static int f12477c = -1;

    private DensityUtil() {
    }

    public static int dip2px(float f) {
        return (int) ((f * getDensity()) + 0.5f);
    }

    public static float getDensity() {
        if (f12475a <= 0.0f) {
            f12475a = C5217x.app().getResources().getDisplayMetrics().density;
        }
        return f12475a;
    }

    public static int getScreenHeight() {
        if (f12477c <= 0) {
            f12477c = C5217x.app().getResources().getDisplayMetrics().heightPixels;
        }
        return f12477c;
    }

    public static int getScreenWidth() {
        if (f12476b <= 0) {
            f12476b = C5217x.app().getResources().getDisplayMetrics().widthPixels;
        }
        return f12476b;
    }

    public static int px2dip(float f) {
        return (int) ((f / getDensity()) + 0.5f);
    }
}
