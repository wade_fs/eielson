package org.xutils.common.task;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.Executor;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.common.task.AbsTask;
import org.xutils.common.util.LogUtil;

/* renamed from: org.xutils.common.task.b */
class TaskProxy<ResultType> extends AbsTask<ResultType> {

    /* renamed from: j */
    static final C5189c f12466j = new C5189c(null);

    /* renamed from: k */
    static final PriorityExecutor f12467k = new PriorityExecutor(true);
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final AbsTask<ResultType> f12468f;

    /* renamed from: g */
    private final Executor f12469g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public volatile boolean f12470h = false;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public volatile boolean f12471i = false;

    /* renamed from: org.xutils.common.task.b$a */
    /* compiled from: TaskProxy */
    class C5187a implements Runnable {
        C5187a() {
        }

        public void run() {
            try {
                if (!TaskProxy.this.f12470h) {
                    if (!TaskProxy.this.isCancelled()) {
                        TaskProxy.this.onStarted();
                        if (!TaskProxy.this.isCancelled()) {
                            TaskProxy.this.f12468f.mo28555a(TaskProxy.this.f12468f.doBackground());
                            TaskProxy.this.mo28555a(TaskProxy.this.f12468f.getResult());
                            if (!TaskProxy.this.isCancelled()) {
                                TaskProxy.this.onSuccess(TaskProxy.this.f12468f.getResult());
                                TaskProxy.this.onFinished();
                                return;
                            }
                            throw new Callback.CancelledException("");
                        }
                        throw new Callback.CancelledException("");
                    }
                }
                throw new Callback.CancelledException("");
            } catch (Callback.CancelledException e) {
                TaskProxy.this.onCancelled(e);
            } catch (Throwable th) {
                TaskProxy.this.onFinished();
                throw th;
            }
        }
    }

    /* renamed from: org.xutils.common.task.b$b */
    /* compiled from: TaskProxy */
    private static class C5188b {

        /* renamed from: a */
        final TaskProxy f12473a;

        /* renamed from: b */
        final Object[] f12474b;

        public C5188b(TaskProxy bVar, Object... objArr) {
            this.f12473a = bVar;
            this.f12474b = objArr;
        }
    }

    /* renamed from: org.xutils.common.task.b$c */
    /* compiled from: TaskProxy */
    static final class C5189c extends Handler {
        /* synthetic */ C5189c(C5187a aVar) {
            this();
        }

        public void handleMessage(Message message) {
            Object[] objArr;
            Object obj = message.obj;
            if (obj != null) {
                TaskProxy bVar = null;
                if (obj instanceof TaskProxy) {
                    bVar = (TaskProxy) obj;
                    objArr = null;
                } else if (obj instanceof C5188b) {
                    C5188b bVar2 = (C5188b) obj;
                    bVar = bVar2.f12473a;
                    objArr = bVar2.f12474b;
                } else {
                    objArr = null;
                }
                if (bVar != null) {
                    try {
                        switch (message.what) {
                            case 1000000001:
                                bVar.f12468f.onWaiting();
                                return;
                            case 1000000002:
                                bVar.f12468f.onStarted();
                                return;
                            case 1000000003:
                                bVar.f12468f.onSuccess(bVar.getResult());
                                return;
                            case 1000000004:
                                Throwable th = (Throwable) objArr[0];
                                LogUtil.m20118d(th.getMessage(), th);
                                bVar.f12468f.onError(th, false);
                                return;
                            case 1000000005:
                                bVar.f12468f.onUpdate(message.arg1, objArr);
                                return;
                            case 1000000006:
                                if (!bVar.f12470h) {
                                    boolean unused = bVar.f12470h = true;
                                    bVar.f12468f.onCancelled((Callback.CancelledException) objArr[0]);
                                    return;
                                }
                                return;
                            case 1000000007:
                                if (!bVar.f12471i) {
                                    boolean unused2 = bVar.f12471i = true;
                                    bVar.f12468f.onFinished();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    } catch (Throwable th2) {
                        bVar.mo28556a(AbsTask.State.ERROR);
                        if (message.what != 1000000004) {
                            bVar.f12468f.onError(th2, true);
                        } else if (C5217x.isDebug()) {
                            throw new RuntimeException(th2);
                        }
                    }
                } else {
                    throw new RuntimeException("msg.obj not instanceof TaskProxy");
                }
            } else {
                throw new IllegalArgumentException("msg must not be null");
            }
        }

        private C5189c() {
            super(Looper.getMainLooper());
        }
    }

    TaskProxy(AbsTask<ResultType> absTask) {
        super(absTask);
        this.f12468f = super;
        this.f12468f.mo28557a(this);
        mo28557a((TaskProxy) null);
        Executor executor = super.getExecutor();
        this.f12469g = executor == null ? f12467k : executor;
    }

    /* access modifiers changed from: protected */
    public final ResultType doBackground() {
        onWaiting();
        this.f12469g.execute(new PriorityRunnable(this.f12468f.getPriority(), new C5187a()));
        return null;
    }

    public final Executor getExecutor() {
        return this.f12469g;
    }

    public final Priority getPriority() {
        return this.f12468f.getPriority();
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Callback.CancelledException cancelledException) {
        mo28556a(AbsTask.State.CANCELLED);
        f12466j.obtainMessage(1000000006, new C5188b(this, cancelledException)).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onError(Throwable th, boolean z) {
        mo28556a(AbsTask.State.ERROR);
        f12466j.obtainMessage(1000000004, new C5188b(this, th)).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onFinished() {
        f12466j.obtainMessage(1000000007, this).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
        mo28556a(AbsTask.State.STARTED);
        f12466j.obtainMessage(1000000002, this).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onSuccess(ResultType resulttype) {
        mo28556a(AbsTask.State.SUCCESS);
        f12466j.obtainMessage(1000000003, this).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onUpdate(int i, Object... objArr) {
        f12466j.obtainMessage(1000000005, i, i, new C5188b(this, objArr)).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onWaiting() {
        mo28556a(AbsTask.State.WAITING);
        f12466j.obtainMessage(1000000001, this).sendToTarget();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo28556a(AbsTask.State state) {
        super.mo28556a(state);
        this.f12468f.mo28556a(state);
    }
}
