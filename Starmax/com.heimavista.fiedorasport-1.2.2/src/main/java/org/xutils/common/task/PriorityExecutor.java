package org.xutils.common.task;

import java.util.Comparator;
import java.util.concurrent.Executor;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class PriorityExecutor implements Executor {

    /* renamed from: b */
    private static final AtomicLong f12440b = new AtomicLong(0);

    /* renamed from: c */
    private static final ThreadFactory f12441c = new C5177a();

    /* renamed from: d */
    private static final Comparator<Runnable> f12442d = new C5178b();

    /* renamed from: e */
    private static final Comparator<Runnable> f12443e = new C5179c();

    /* renamed from: a */
    private final ThreadPoolExecutor f12444a;

    /* renamed from: org.xutils.common.task.PriorityExecutor$a */
    static class C5177a implements ThreadFactory {

        /* renamed from: P */
        private final AtomicInteger f12445P = new AtomicInteger(1);

        C5177a() {
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "xTID#" + this.f12445P.getAndIncrement());
        }
    }

    /* renamed from: org.xutils.common.task.PriorityExecutor$b */
    static class C5178b implements Comparator<Runnable> {
        C5178b() {
        }

        /* renamed from: a */
        public int compare(Runnable runnable, Runnable runnable2) {
            if (!(runnable instanceof PriorityRunnable) || !(runnable2 instanceof PriorityRunnable)) {
                return 0;
            }
            PriorityRunnable aVar = (PriorityRunnable) runnable;
            PriorityRunnable aVar2 = (PriorityRunnable) runnable2;
            int ordinal = aVar.f12464Q.ordinal() - aVar2.f12464Q.ordinal();
            return ordinal == 0 ? (int) (aVar.f12463P - aVar2.f12463P) : ordinal;
        }
    }

    /* renamed from: org.xutils.common.task.PriorityExecutor$c */
    static class C5179c implements Comparator<Runnable> {
        C5179c() {
        }

        /* renamed from: a */
        public int compare(Runnable runnable, Runnable runnable2) {
            if (!(runnable instanceof PriorityRunnable) || !(runnable2 instanceof PriorityRunnable)) {
                return 0;
            }
            PriorityRunnable aVar = (PriorityRunnable) runnable;
            PriorityRunnable aVar2 = (PriorityRunnable) runnable2;
            int ordinal = aVar.f12464Q.ordinal() - aVar2.f12464Q.ordinal();
            return ordinal == 0 ? (int) (aVar2.f12463P - aVar.f12463P) : ordinal;
        }
    }

    public PriorityExecutor(boolean z) {
        this(5, z);
    }

    public void execute(Runnable runnable) {
        if (runnable instanceof PriorityRunnable) {
            ((PriorityRunnable) runnable).f12463P = f12440b.getAndIncrement();
        }
        this.f12444a.execute(runnable);
    }

    public int getPoolSize() {
        return this.f12444a.getCorePoolSize();
    }

    public ThreadPoolExecutor getThreadPoolExecutor() {
        return this.f12444a;
    }

    public boolean isBusy() {
        return this.f12444a.getActiveCount() >= this.f12444a.getCorePoolSize();
    }

    public void setPoolSize(int i) {
        if (i > 0) {
            this.f12444a.setCorePoolSize(i);
        }
    }

    public PriorityExecutor(int i, boolean z) {
        this.f12444a = new ThreadPoolExecutor(i, 256, 1, TimeUnit.SECONDS, new PriorityBlockingQueue(256, z ? f12442d : f12443e), f12441c);
    }
}
