package org.xutils.common.util;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import org.xutils.C5217x;

public class FileUtil {
    private FileUtil() {
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.io.OutputStream, java.io.Closeable, java.io.FileOutputStream] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean copy(java.lang.String r3, java.lang.String r4) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r3)
            boolean r3 = r0.exists()
            r1 = 0
            if (r3 != 0) goto L_0x000d
            return r1
        L_0x000d:
            java.io.File r3 = new java.io.File
            r3.<init>(r4)
            org.xutils.common.util.IOUtil.deleteFileOrDir(r3)
            java.io.File r4 = r3.getParentFile()
            boolean r2 = r4.exists()
            if (r2 != 0) goto L_0x0025
            boolean r4 = r4.mkdirs()
            if (r4 == 0) goto L_0x004f
        L_0x0025:
            r4 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0040 }
            r2.<init>(r0)     // Catch:{ all -> 0x0040 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ all -> 0x003e }
            r0.<init>(r3)     // Catch:{ all -> 0x003e }
            org.xutils.common.util.IOUtil.copy(r2, r0)     // Catch:{ all -> 0x003b }
            r1 = 1
            org.xutils.common.util.IOUtil.closeQuietly(r2)
            org.xutils.common.util.IOUtil.closeQuietly(r0)
            goto L_0x004f
        L_0x003b:
            r3 = move-exception
            r4 = r0
            goto L_0x0042
        L_0x003e:
            r3 = move-exception
            goto L_0x0042
        L_0x0040:
            r3 = move-exception
            r2 = r4
        L_0x0042:
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x0050 }
            org.xutils.common.util.LogUtil.m20118d(r0, r3)     // Catch:{ all -> 0x0050 }
            org.xutils.common.util.IOUtil.closeQuietly(r2)
            org.xutils.common.util.IOUtil.closeQuietly(r4)
        L_0x004f:
            return r1
        L_0x0050:
            r3 = move-exception
            org.xutils.common.util.IOUtil.closeQuietly(r2)
            org.xutils.common.util.IOUtil.closeQuietly(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xutils.common.util.FileUtil.copy(java.lang.String, java.lang.String):boolean");
    }

    public static Boolean existsSdcard() {
        return Boolean.valueOf(Environment.getExternalStorageState().equals("mounted"));
    }

    public static File getCacheDir(String str) {
        File file;
        if (existsSdcard().booleanValue()) {
            File externalCacheDir = C5217x.app().getExternalCacheDir();
            if (externalCacheDir == null) {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                file = new File(externalStorageDirectory, "Android/data/" + C5217x.app().getPackageName() + "/cache/" + str);
            } else {
                file = new File(externalCacheDir, str);
            }
        } else {
            file = new File(C5217x.app().getCacheDir(), str);
        }
        if (file.exists() || file.mkdirs()) {
            return file;
        }
        return null;
    }

    public static long getDiskAvailableSize() {
        if (!existsSdcard().booleanValue()) {
            return 0;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static long getFileOrDirSize(File file) {
        long j = 0;
        if (!file.exists()) {
            return 0;
        }
        if (!file.isDirectory()) {
            return file.length();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                j += getFileOrDirSize(file2);
            }
        }
        return j;
    }

    public static boolean isDiskAvailable() {
        return getDiskAvailableSize() > 10485760;
    }
}
