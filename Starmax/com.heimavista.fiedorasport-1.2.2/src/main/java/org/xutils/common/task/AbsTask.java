package org.xutils.common.task;

import java.util.concurrent.Executor;
import org.xutils.common.Callback;

public abstract class AbsTask<ResultType> implements Callback.Cancelable {

    /* renamed from: a */
    private TaskProxy f12432a;

    /* renamed from: b */
    private final Callback.Cancelable f12433b;

    /* renamed from: c */
    private volatile boolean f12434c;

    /* renamed from: d */
    private volatile State f12435d;

    /* renamed from: e */
    private ResultType f12436e;

    public enum State {
        IDLE(0),
        WAITING(1),
        STARTED(2),
        SUCCESS(3),
        CANCELLED(4),
        ERROR(5);
        

        /* renamed from: P */
        private final int f12438P;

        private State(int i) {
            this.f12438P = i;
        }

        public int value() {
            return this.f12438P;
        }
    }

    public AbsTask() {
        this(null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28556a(State state) {
        this.f12435d = state;
    }

    public final synchronized void cancel() {
        if (!this.f12434c) {
            this.f12434c = true;
            cancelWorks();
            if (this.f12433b != null && !this.f12433b.isCancelled()) {
                this.f12433b.cancel();
            }
            if (this.f12435d == State.WAITING || (this.f12435d == State.STARTED && isCancelFast())) {
                if (this.f12432a != null) {
                    this.f12432a.onCancelled(new Callback.CancelledException("cancelled by user"));
                    this.f12432a.onFinished();
                } else if (this instanceof TaskProxy) {
                    onCancelled(new Callback.CancelledException("cancelled by user"));
                    onFinished();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void cancelWorks() {
    }

    /* access modifiers changed from: protected */
    public abstract ResultType doBackground();

    public Executor getExecutor() {
        return null;
    }

    public Priority getPriority() {
        return null;
    }

    public final ResultType getResult() {
        return this.f12436e;
    }

    public final State getState() {
        return this.f12435d;
    }

    /* access modifiers changed from: protected */
    public boolean isCancelFast() {
        return false;
    }

    public final boolean isCancelled() {
        Callback.Cancelable cancelable;
        return this.f12434c || this.f12435d == State.CANCELLED || ((cancelable = this.f12433b) != null && cancelable.isCancelled());
    }

    public final boolean isFinished() {
        return this.f12435d.value() > State.STARTED.value();
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Callback.CancelledException cancelledException) {
    }

    /* access modifiers changed from: protected */
    public abstract void onError(Throwable th, boolean z);

    /* access modifiers changed from: protected */
    public void onFinished() {
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
    }

    /* access modifiers changed from: protected */
    public abstract void onSuccess(ResultType resulttype);

    /* access modifiers changed from: protected */
    public void onUpdate(int i, Object... objArr) {
    }

    /* access modifiers changed from: protected */
    public void onWaiting() {
    }

    /* access modifiers changed from: protected */
    public final void update(int i, Object... objArr) {
        TaskProxy bVar = this.f12432a;
        if (bVar != null) {
            bVar.onUpdate(i, objArr);
        }
    }

    public AbsTask(Callback.Cancelable cancelable) {
        this.f12432a = null;
        this.f12434c = false;
        this.f12435d = State.IDLE;
        this.f12433b = cancelable;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo28557a(TaskProxy bVar) {
        this.f12432a = bVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo28555a(ResultType resulttype) {
        this.f12436e = resulttype;
    }
}
