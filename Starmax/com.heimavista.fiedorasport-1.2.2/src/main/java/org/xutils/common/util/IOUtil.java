package org.xutils.common.util;

import android.database.Cursor;
import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class IOUtil {
    private IOUtil() {
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                LogUtil.m20118d(th.getMessage(), th);
            }
        }
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) {
        if (!(inputStream instanceof BufferedInputStream)) {
            inputStream = new BufferedInputStream(inputStream);
        }
        if (!(outputStream instanceof BufferedOutputStream)) {
            outputStream = new BufferedOutputStream(outputStream);
        }
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    public static boolean deleteFileOrDir(File file) {
        if (file == null || !file.exists()) {
            return true;
        }
        if (file.isFile()) {
            return file.delete();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                deleteFileOrDir(file2);
            }
        }
        return file.delete();
    }

    public static byte[] readBytes(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream;
        if (!(inputStream instanceof BufferedInputStream)) {
            inputStream = new BufferedInputStream(inputStream);
        }
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    } else {
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        closeQuietly(byteArrayOutputStream);
                        return byteArray;
                    }
                }
            } catch (Throwable th) {
                th = th;
                closeQuietly(byteArrayOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            byteArrayOutputStream = null;
            closeQuietly(byteArrayOutputStream);
            throw th;
        }
    }

    public static String readStr(InputStream inputStream) {
        return readStr(inputStream, C1750C.UTF8_NAME);
    }

    public static void writeStr(OutputStream outputStream, String str) {
        writeStr(outputStream, str, C1750C.UTF8_NAME);
    }

    public static String readStr(InputStream inputStream, String str) {
        if (TextUtils.isEmpty(str)) {
            str = C1750C.UTF8_NAME;
        }
        if (!(inputStream instanceof BufferedInputStream)) {
            inputStream = new BufferedInputStream(inputStream);
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, str);
        StringBuilder sb = new StringBuilder();
        char[] cArr = new char[1024];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read < 0) {
                return sb.toString();
            }
            sb.append(cArr, 0, read);
        }
    }

    public static void writeStr(OutputStream outputStream, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = C1750C.UTF8_NAME;
        }
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, str2);
        outputStreamWriter.write(str);
        outputStreamWriter.flush();
    }

    public static void closeQuietly(Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Throwable th) {
                LogUtil.m20118d(th.getMessage(), th);
            }
        }
    }

    public static byte[] readBytes(InputStream inputStream, long j, int i) {
        if (j > 0) {
            while (j > 0) {
                long skip = inputStream.skip(j);
                if (skip <= 0) {
                    break;
                }
                j -= skip;
            }
        }
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) inputStream.read();
        }
        return bArr;
    }
}
