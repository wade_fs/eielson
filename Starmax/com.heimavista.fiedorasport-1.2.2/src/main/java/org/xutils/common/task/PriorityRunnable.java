package org.xutils.common.task;

/* renamed from: org.xutils.common.task.a */
class PriorityRunnable implements Runnable {

    /* renamed from: P */
    long f12463P;

    /* renamed from: Q */
    public final Priority f12464Q;

    /* renamed from: R */
    private final Runnable f12465R;

    public PriorityRunnable(Priority priority, Runnable runnable) {
        this.f12464Q = priority == null ? Priority.DEFAULT : priority;
        this.f12465R = runnable;
    }

    public final void run() {
        this.f12465R.run();
    }
}
