package org.xutils.common.task;

import android.os.Looper;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.common.TaskController;
import org.xutils.common.util.LogUtil;

public final class TaskControllerImpl implements TaskController {

    /* renamed from: a */
    private static volatile TaskController f12446a;

    /* renamed from: org.xutils.common.task.TaskControllerImpl$a */
    class C5180a implements Runnable {

        /* renamed from: P */
        private final int f12447P = this.f12449R.length;

        /* renamed from: Q */
        private final AtomicInteger f12448Q = new AtomicInteger(0);

        /* renamed from: R */
        final /* synthetic */ AbsTask[] f12449R;

        /* renamed from: S */
        final /* synthetic */ Callback.GroupCallback f12450S;

        C5180a(TaskControllerImpl taskControllerImpl, AbsTask[] absTaskArr, Callback.GroupCallback groupCallback) {
            this.f12449R = absTaskArr;
            this.f12450S = groupCallback;
        }

        public void run() {
            Callback.GroupCallback groupCallback;
            if (this.f12448Q.incrementAndGet() == this.f12447P && (groupCallback = this.f12450S) != null) {
                groupCallback.onAllFinished();
            }
        }
    }

    /* renamed from: org.xutils.common.task.TaskControllerImpl$b */
    class C5181b extends TaskProxy {

        /* renamed from: l */
        final /* synthetic */ Callback.GroupCallback f12451l;

        /* renamed from: m */
        final /* synthetic */ AbsTask f12452m;

        /* renamed from: n */
        final /* synthetic */ Runnable f12453n;

        /* renamed from: org.xutils.common.task.TaskControllerImpl$b$a */
        class C5182a implements Runnable {
            C5182a() {
            }

            public void run() {
                C5181b bVar = C5181b.this;
                Callback.GroupCallback groupCallback = bVar.f12451l;
                if (groupCallback != null) {
                    groupCallback.onSuccess(bVar.f12452m);
                }
            }
        }

        /* renamed from: org.xutils.common.task.TaskControllerImpl$b$b */
        class C5183b implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Callback.CancelledException f12456P;

            C5183b(Callback.CancelledException cancelledException) {
                this.f12456P = cancelledException;
            }

            public void run() {
                C5181b bVar = C5181b.this;
                Callback.GroupCallback groupCallback = bVar.f12451l;
                if (groupCallback != null) {
                    groupCallback.onCancelled(bVar.f12452m, this.f12456P);
                }
            }
        }

        /* renamed from: org.xutils.common.task.TaskControllerImpl$b$c */
        class C5184c implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Throwable f12458P;

            /* renamed from: Q */
            final /* synthetic */ boolean f12459Q;

            C5184c(Throwable th, boolean z) {
                this.f12458P = th;
                this.f12459Q = z;
            }

            public void run() {
                C5181b bVar = C5181b.this;
                Callback.GroupCallback groupCallback = bVar.f12451l;
                if (groupCallback != null) {
                    groupCallback.onError(bVar.f12452m, this.f12458P, this.f12459Q);
                }
            }
        }

        /* renamed from: org.xutils.common.task.TaskControllerImpl$b$d */
        class C5185d implements Runnable {
            C5185d() {
            }

            public void run() {
                C5181b bVar = C5181b.this;
                Callback.GroupCallback groupCallback = bVar.f12451l;
                if (groupCallback != null) {
                    groupCallback.onFinished(bVar.f12452m);
                }
                C5181b.this.f12453n.run();
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C5181b(AbsTask absTask, Callback.GroupCallback groupCallback, AbsTask absTask2, Runnable runnable) {
            super(absTask);
            this.f12451l = groupCallback;
            this.f12452m = absTask2;
            this.f12453n = runnable;
        }

        /* access modifiers changed from: protected */
        public void onCancelled(Callback.CancelledException cancelledException) {
            super.onCancelled(cancelledException);
            TaskControllerImpl.this.post(new C5183b(cancelledException));
        }

        /* access modifiers changed from: protected */
        public void onError(Throwable th, boolean z) {
            super.onError(th, z);
            TaskControllerImpl.this.post(new C5184c(th, z));
        }

        /* access modifiers changed from: protected */
        public void onFinished() {
            super.onFinished();
            TaskControllerImpl.this.post(new C5185d());
        }

        /* access modifiers changed from: protected */
        public void onSuccess(Object obj) {
            super.onSuccess(obj);
            TaskControllerImpl.this.post(new C5182a());
        }
    }

    /* renamed from: org.xutils.common.task.TaskControllerImpl$c */
    class C5186c implements Callback.Cancelable {

        /* renamed from: a */
        final /* synthetic */ AbsTask[] f12462a;

        C5186c(TaskControllerImpl taskControllerImpl, AbsTask[] absTaskArr) {
            this.f12462a = absTaskArr;
        }

        public void cancel() {
            for (AbsTask absTask : this.f12462a) {
                absTask.cancel();
            }
        }

        public boolean isCancelled() {
            boolean z = true;
            for (AbsTask absTask : this.f12462a) {
                if (!absTask.isCancelled()) {
                    z = false;
                }
            }
            return z;
        }
    }

    private TaskControllerImpl() {
    }

    public static void registerInstance() {
        if (f12446a == null) {
            synchronized (TaskController.class) {
                if (f12446a == null) {
                    f12446a = new TaskControllerImpl();
                }
            }
        }
        C5217x.Ext.setTaskController(f12446a);
    }

    public void autoPost(Runnable runnable) {
        if (runnable != null) {
            if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                runnable.run();
            } else {
                TaskProxy.f12466j.post(runnable);
            }
        }
    }

    public void post(Runnable runnable) {
        if (runnable != null) {
            TaskProxy.f12466j.post(runnable);
        }
    }

    public void postDelayed(Runnable runnable, long j) {
        if (runnable != null) {
            TaskProxy.f12466j.postDelayed(runnable, j);
        }
    }

    public void removeCallbacks(Runnable runnable) {
        TaskProxy.f12466j.removeCallbacks(runnable);
    }

    public void run(Runnable runnable) {
        if (!TaskProxy.f12467k.isBusy()) {
            TaskProxy.f12467k.execute(runnable);
        } else {
            new Thread(runnable).start();
        }
    }

    public <T> AbsTask<T> start(AbsTask<T> absTask) {
        TaskProxy bVar;
        if (absTask instanceof TaskProxy) {
            bVar = (TaskProxy) absTask;
        } else {
            bVar = new TaskProxy(absTask);
        }
        try {
            bVar.doBackground();
        } catch (Throwable th) {
            LogUtil.m20120e(th.getMessage(), th);
        }
        return bVar;
    }

    public <T> T startSync(AbsTask<T> absTask) {
        T t = null;
        try {
            absTask.onWaiting();
            absTask.onStarted();
            t = absTask.doBackground();
            absTask.onSuccess(t);
        } catch (Callback.CancelledException e) {
            absTask.onCancelled(e);
        } catch (Throwable th) {
            absTask.onFinished();
            throw th;
        }
        absTask.onFinished();
        return t;
    }

    public <T extends AbsTask<?>> Callback.Cancelable startTasks(Callback.GroupCallback<T> groupCallback, T... tArr) {
        if (tArr != null) {
            C5180a aVar = new C5180a(this, tArr, groupCallback);
            for (T t : tArr) {
                start(new C5181b(t, groupCallback, t, aVar));
            }
            return new C5186c(this, tArr);
        }
        throw new IllegalArgumentException("task must not be null");
    }
}
