package com.scwang.smartrefresh.layout.p209d;

import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p208c.CoordinatorLayoutListener;

/* renamed from: com.scwang.smartrefresh.layout.d.a */
public class DesignUtil {

    /* renamed from: com.scwang.smartrefresh.layout.d.a$a */
    /* compiled from: DesignUtil */
    static class C4708a implements AppBarLayout.OnOffsetChangedListener {

        /* renamed from: a */
        final /* synthetic */ CoordinatorLayoutListener f9749a;

        C4708a(CoordinatorLayoutListener aVar) {
            this.f9749a = aVar;
        }

        public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            CoordinatorLayoutListener aVar = this.f9749a;
            boolean z = true;
            boolean z2 = i >= 0;
            if (appBarLayout.getTotalScrollRange() + i > 0) {
                z = false;
            }
            aVar.mo26140a(z2, z);
        }
    }

    /* renamed from: a */
    public static void m15571a(View view, RefreshKernel iVar, CoordinatorLayoutListener aVar) {
        try {
            if (view instanceof CoordinatorLayout) {
                iVar.mo26111b().mo26054b(false);
                m15572a((ViewGroup) view, aVar);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: a */
    private static void m15572a(ViewGroup viewGroup, CoordinatorLayoutListener aVar) {
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt instanceof AppBarLayout) {
                ((AppBarLayout) childAt).addOnOffsetChangedListener((AppBarLayout.OnOffsetChangedListener) new C4708a(aVar));
            }
        }
    }
}
