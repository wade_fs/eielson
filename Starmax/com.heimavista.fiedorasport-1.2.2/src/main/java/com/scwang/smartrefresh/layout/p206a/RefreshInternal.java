package com.scwang.smartrefresh.layout.p206a;

import android.view.View;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p208c.OnStateChangedListener;

/* renamed from: com.scwang.smartrefresh.layout.a.h */
public interface RefreshInternal extends OnStateChangedListener {
    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    int mo24847a(@NonNull RefreshLayout jVar, boolean z);

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    void mo26128a(float f, int i, int i2);

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    void mo24848a(@NonNull RefreshKernel iVar, int i, int i2);

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    void mo26129a(@NonNull RefreshLayout jVar, int i, int i2);

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    void mo24850a(boolean z, float f, int i, int i2, int i3);

    /* renamed from: a */
    boolean mo26130a();

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: b */
    void mo24851b(@NonNull RefreshLayout jVar, int i, int i2);

    @NonNull
    SpinnerStyle getSpinnerStyle();

    @NonNull
    View getView();

    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    void setPrimaryColors(@ColorInt int... iArr);
}
