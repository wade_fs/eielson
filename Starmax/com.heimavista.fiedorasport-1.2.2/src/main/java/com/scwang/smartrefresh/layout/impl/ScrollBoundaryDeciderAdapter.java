package com.scwang.smartrefresh.layout.impl;

import android.graphics.PointF;
import android.view.View;
import com.scwang.smartrefresh.layout.p206a.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

/* renamed from: com.scwang.smartrefresh.layout.impl.b */
public class ScrollBoundaryDeciderAdapter implements ScrollBoundaryDecider {

    /* renamed from: a */
    public PointF f9849a;

    /* renamed from: b */
    public ScrollBoundaryDecider f9850b;

    /* renamed from: c */
    public boolean f9851c = true;

    /* renamed from: a */
    public boolean mo26133a(View view) {
        ScrollBoundaryDecider kVar = this.f9850b;
        if (kVar != null) {
            return kVar.mo26133a(view);
        }
        return SmartUtil.m15578a(view, this.f9849a);
    }

    /* renamed from: b */
    public boolean mo26134b(View view) {
        ScrollBoundaryDecider kVar = this.f9850b;
        if (kVar != null) {
            return kVar.mo26134b(view);
        }
        return SmartUtil.m15579a(view, this.f9849a, this.f9851c);
    }
}
