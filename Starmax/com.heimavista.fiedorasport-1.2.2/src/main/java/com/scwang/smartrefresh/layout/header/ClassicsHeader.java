package com.scwang.smartrefresh.layout.header;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.scwang.smartrefresh.layout.R$id;
import com.scwang.smartrefresh.layout.R$layout;
import com.scwang.smartrefresh.layout.R$string;
import com.scwang.smartrefresh.layout.R$styleable;
import com.scwang.smartrefresh.layout.internal.ArrowDrawable;
import com.scwang.smartrefresh.layout.internal.InternalClassics;
import com.scwang.smartrefresh.layout.internal.ProgressDrawable;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ClassicsHeader extends InternalClassics<ClassicsHeader> implements RefreshHeader {

    /* renamed from: A0 */
    public static String f9803A0;

    /* renamed from: B0 */
    public static String f9804B0;

    /* renamed from: C0 */
    public static String f9805C0;

    /* renamed from: D0 */
    public static String f9806D0;

    /* renamed from: w0 */
    public static String f9807w0;

    /* renamed from: x0 */
    public static String f9808x0;

    /* renamed from: y0 */
    public static String f9809y0;

    /* renamed from: z0 */
    public static String f9810z0;

    /* renamed from: i0 */
    protected String f9811i0;

    /* renamed from: j0 */
    protected Date f9812j0;

    /* renamed from: k0 */
    protected TextView f9813k0;

    /* renamed from: l0 */
    protected SharedPreferences f9814l0;

    /* renamed from: m0 */
    protected DateFormat f9815m0;

    /* renamed from: n0 */
    protected boolean f9816n0;

    /* renamed from: o0 */
    protected String f9817o0;

    /* renamed from: p0 */
    protected String f9818p0;

    /* renamed from: q0 */
    protected String f9819q0;

    /* renamed from: r0 */
    protected String f9820r0;

    /* renamed from: s0 */
    protected String f9821s0;

    /* renamed from: t0 */
    protected String f9822t0;

    /* renamed from: u0 */
    protected String f9823u0;

    /* renamed from: v0 */
    protected String f9824v0;

    /* renamed from: com.scwang.smartrefresh.layout.header.ClassicsHeader$a */
    static /* synthetic */ class C4712a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9825a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a = r0
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.RefreshReleased     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToRefresh     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToTwoLevel     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.f9825a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.header.ClassicsHeader.C4712a.<clinit>():void");
        }
    }

    public ClassicsHeader(Context context) {
        this(context, null);
    }

    public ClassicsHeader(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        if (z) {
            super.f9855S.setText(this.f9821s0);
            if (this.f9812j0 != null) {
                mo26164a(new Date());
            }
        } else {
            super.f9855S.setText(this.f9822t0);
        }
        return super.mo24847a(jVar, z);
    }

    public ClassicsHeader(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FragmentManager supportFragmentManager;
        List<Fragment> fragments;
        this.f9811i0 = "LAST_UPDATE_TIME";
        this.f9816n0 = true;
        View.inflate(context, R$layout.srl_classics_header, this);
        ImageView imageView = (ImageView) findViewById(R$id.srl_classics_arrow);
        super.f9856T = imageView;
        TextView textView = (TextView) findViewById(R$id.srl_classics_update);
        this.f9813k0 = textView;
        ImageView imageView2 = (ImageView) findViewById(R$id.srl_classics_progress);
        super.f9857U = imageView2;
        super.f9855S = (TextView) findViewById(R$id.srl_classics_title);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ClassicsHeader);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) imageView2.getLayoutParams();
        new LinearLayout.LayoutParams(-2, -2).topMargin = obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsHeader_srlTextTimeMarginTop, SmartUtil.m15574a(0.0f));
        layoutParams2.rightMargin = obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsFooter_srlDrawableMarginRight, SmartUtil.m15574a(20.0f));
        layoutParams.rightMargin = layoutParams2.rightMargin;
        layoutParams.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableArrowSize, layoutParams.width);
        layoutParams.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableArrowSize, layoutParams.height);
        layoutParams2.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableProgressSize, layoutParams2.width);
        layoutParams2.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableProgressSize, layoutParams2.height);
        layoutParams.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableSize, layoutParams.width);
        layoutParams.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableSize, layoutParams.height);
        layoutParams2.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableSize, layoutParams2.width);
        layoutParams2.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsHeader_srlDrawableSize, layoutParams2.height);
        super.f9864e0 = obtainStyledAttributes.getInt(R$styleable.ClassicsHeader_srlFinishDuration, super.f9864e0);
        this.f9816n0 = obtainStyledAttributes.getBoolean(R$styleable.ClassicsHeader_srlEnableLastTime, this.f9816n0);
        this.f9853Q = SpinnerStyle.f9747g[obtainStyledAttributes.getInt(R$styleable.ClassicsHeader_srlClassicsSpinnerStyle, this.f9853Q.f9748a)];
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlDrawableArrow)) {
            super.f9856T.setImageDrawable(obtainStyledAttributes.getDrawable(R$styleable.ClassicsHeader_srlDrawableArrow));
        } else if (super.f9856T.getDrawable() == null) {
            super.f9859W = new ArrowDrawable();
            super.f9859W.mo26183a(-10066330);
            super.f9856T.setImageDrawable(super.f9859W);
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlDrawableProgress)) {
            super.f9857U.setImageDrawable(obtainStyledAttributes.getDrawable(R$styleable.ClassicsHeader_srlDrawableProgress));
        } else if (super.f9857U.getDrawable() == null) {
            super.f9860a0 = new ProgressDrawable();
            super.f9860a0.mo26183a(-10066330);
            super.f9857U.setImageDrawable(super.f9860a0);
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextSizeTitle)) {
            super.f9855S.setTextSize(0, (float) obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsHeader_srlTextSizeTitle, SmartUtil.m15574a(16.0f)));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextSizeTime)) {
            this.f9813k0.setTextSize(0, (float) obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsHeader_srlTextSizeTime, SmartUtil.m15574a(12.0f)));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlPrimaryColor)) {
            super.mo26179b(obtainStyledAttributes.getColor(R$styleable.ClassicsHeader_srlPrimaryColor, 0));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlAccentColor)) {
            m15610a(obtainStyledAttributes.getColor(R$styleable.ClassicsHeader_srlAccentColor, 0));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextPulling)) {
            this.f9817o0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextPulling);
        } else {
            String str = f9807w0;
            if (str != null) {
                this.f9817o0 = str;
            } else {
                this.f9817o0 = context.getString(R$string.srl_header_pulling);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextLoading)) {
            this.f9819q0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextLoading);
        } else {
            String str2 = f9809y0;
            if (str2 != null) {
                this.f9819q0 = str2;
            } else {
                this.f9819q0 = context.getString(R$string.srl_header_loading);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextRelease)) {
            this.f9820r0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextRelease);
        } else {
            String str3 = f9810z0;
            if (str3 != null) {
                this.f9820r0 = str3;
            } else {
                this.f9820r0 = context.getString(R$string.srl_header_release);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextFinish)) {
            this.f9821s0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextFinish);
        } else {
            String str4 = f9803A0;
            if (str4 != null) {
                this.f9821s0 = str4;
            } else {
                this.f9821s0 = context.getString(R$string.srl_header_finish);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextFailed)) {
            this.f9822t0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextFailed);
        } else {
            String str5 = f9804B0;
            if (str5 != null) {
                this.f9822t0 = str5;
            } else {
                this.f9822t0 = context.getString(R$string.srl_header_failed);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextSecondary)) {
            this.f9824v0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextSecondary);
        } else {
            String str6 = f9806D0;
            if (str6 != null) {
                this.f9824v0 = str6;
            } else {
                this.f9824v0 = context.getString(R$string.srl_header_secondary);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextRefreshing)) {
            this.f9818p0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextRefreshing);
        } else {
            String str7 = f9808x0;
            if (str7 != null) {
                this.f9818p0 = str7;
            } else {
                this.f9818p0 = context.getString(R$string.srl_header_refreshing);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsHeader_srlTextUpdate)) {
            this.f9823u0 = obtainStyledAttributes.getString(R$styleable.ClassicsHeader_srlTextUpdate);
        } else {
            String str8 = f9805C0;
            if (str8 != null) {
                this.f9823u0 = str8;
            } else {
                this.f9823u0 = context.getString(R$string.srl_header_update);
            }
        }
        this.f9815m0 = new SimpleDateFormat(this.f9823u0, Locale.getDefault());
        obtainStyledAttributes.recycle();
        imageView2.animate().setInterpolator(null);
        textView.setVisibility(this.f9816n0 ? 0 : 8);
        super.f9855S.setText(isInEditMode() ? this.f9818p0 : this.f9817o0);
        if (isInEditMode()) {
            imageView.setVisibility(8);
        } else {
            imageView2.setVisibility(8);
        }
        try {
            if ((context instanceof FragmentActivity) && (supportFragmentManager = ((FragmentActivity) context).getSupportFragmentManager()) != null && (fragments = supportFragmentManager.getFragments()) != null && fragments.size() > 0) {
                mo26164a(new Date());
                return;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        this.f9811i0 += context.getClass().getName();
        this.f9814l0 = context.getSharedPreferences("ClassicsHeader", 0);
        mo26164a(new Date(this.f9814l0.getLong(this.f9811i0, System.currentTimeMillis())));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        ImageView imageView = super.f9856T;
        TextView textView = this.f9813k0;
        int i = 8;
        switch (C4712a.f9825a[bVar2.ordinal()]) {
            case 1:
                if (this.f9816n0) {
                    i = 0;
                }
                textView.setVisibility(i);
                break;
            case 2:
                break;
            case 3:
            case 4:
                super.f9855S.setText(this.f9818p0);
                imageView.setVisibility(8);
                return;
            case 5:
                super.f9855S.setText(this.f9820r0);
                imageView.animate().rotation(180.0f);
                return;
            case 6:
                super.f9855S.setText(this.f9824v0);
                imageView.animate().rotation(0.0f);
                return;
            case 7:
                imageView.setVisibility(8);
                if (this.f9816n0) {
                    i = 4;
                }
                textView.setVisibility(i);
                super.f9855S.setText(this.f9819q0);
                return;
            default:
                return;
        }
        super.f9855S.setText(this.f9817o0);
        imageView.setVisibility(0);
        imageView.animate().rotation(0.0f);
    }

    /* renamed from: a */
    public ClassicsHeader mo26164a(Date date) {
        this.f9812j0 = date;
        this.f9813k0.setText(this.f9815m0.format(date));
        if (this.f9814l0 != null && !isInEditMode()) {
            this.f9814l0.edit().putLong(this.f9811i0, date.getTime()).apply();
        }
        return this;
    }

    /* renamed from: a */
    public ClassicsHeader m15610a(@ColorInt int i) {
        this.f9813k0.setTextColor((16777215 & i) | -872415232);
        return (ClassicsHeader) super.mo26163a(i);
    }
}
