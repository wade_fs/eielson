package com.scwang.smartrefresh.layout.p208c;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;

/* renamed from: com.scwang.smartrefresh.layout.c.f */
public interface OnStateChangedListener {
    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2);
}
