package com.scwang.smartrefresh.layout.internal;

import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.internal.a */
public class ArrowDrawable extends PaintDrawable {

    /* renamed from: Q */
    private int f9868Q = 0;

    /* renamed from: R */
    private int f9869R = 0;

    /* renamed from: S */
    private Path f9870S = new Path();

    public void draw(@NonNull Canvas canvas) {
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        if (!(this.f9868Q == width && this.f9869R == height)) {
            this.f9870S.reset();
            float f = (float) ((width * 30) / 225);
            float f2 = f * 0.70710677f;
            float f3 = f / 0.70710677f;
            float f4 = (float) width;
            float f5 = f4 / 2.0f;
            float f6 = (float) height;
            this.f9870S.moveTo(f5, f6);
            float f7 = f6 / 2.0f;
            this.f9870S.lineTo(0.0f, f7);
            float f8 = f7 - f2;
            this.f9870S.lineTo(f2, f8);
            float f9 = f / 2.0f;
            float f10 = f5 - f9;
            float f11 = (f6 - f3) - f9;
            this.f9870S.lineTo(f10, f11);
            this.f9870S.lineTo(f10, 0.0f);
            float f12 = f5 + f9;
            this.f9870S.lineTo(f12, 0.0f);
            this.f9870S.lineTo(f12, f11);
            this.f9870S.lineTo(f4 - f2, f8);
            this.f9870S.lineTo(f4, f7);
            this.f9870S.close();
            this.f9868Q = width;
            this.f9869R = height;
        }
        canvas.drawPath(this.f9870S, super.f9871P);
    }
}
