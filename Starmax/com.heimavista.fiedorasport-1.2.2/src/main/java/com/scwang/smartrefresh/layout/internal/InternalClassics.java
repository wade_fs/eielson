package com.scwang.smartrefresh.layout.internal;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.internal.InternalClassics;
import com.scwang.smartrefresh.layout.p206a.RefreshInternal;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public abstract class InternalClassics<T extends InternalClassics> extends InternalAbstract implements RefreshInternal {

    /* renamed from: S */
    protected TextView f9855S;

    /* renamed from: T */
    protected ImageView f9856T;

    /* renamed from: U */
    protected ImageView f9857U;

    /* renamed from: V */
    protected RefreshKernel f9858V;

    /* renamed from: W */
    protected PaintDrawable f9859W;

    /* renamed from: a0 */
    protected PaintDrawable f9860a0;

    /* renamed from: b0 */
    protected boolean f9861b0;

    /* renamed from: c0 */
    protected boolean f9862c0;

    /* renamed from: d0 */
    protected int f9863d0;

    /* renamed from: e0 */
    protected int f9864e0 = 500;

    /* renamed from: f0 */
    protected int f9865f0 = 20;

    /* renamed from: g0 */
    protected int f9866g0 = 20;

    /* renamed from: h0 */
    protected int f9867h0 = 0;

    public InternalClassics(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.f9853Q = SpinnerStyle.f9742b;
    }

    /* renamed from: a */
    public void mo24848a(@NonNull RefreshKernel iVar, int i, int i2) {
        this.f9858V = iVar;
        this.f9858V.mo26105a(this, this.f9863d0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public T mo26178b() {
        return this;
    }

    /* renamed from: b */
    public void mo24851b(@NonNull RefreshLayout jVar, int i, int i2) {
        mo26129a(jVar, i, i2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (Build.VERSION.SDK_INT >= 14) {
            ImageView imageView = this.f9856T;
            ImageView imageView2 = this.f9857U;
            imageView.animate().cancel();
            imageView2.animate().cancel();
        }
        Drawable drawable = this.f9857U.getDrawable();
        if (drawable instanceof Animatable) {
            Animatable animatable = (Animatable) drawable;
            if (animatable.isRunning()) {
                animatable.stop();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.f9867h0 == 0) {
            this.f9865f0 = getPaddingTop();
            this.f9866g0 = getPaddingBottom();
            if (this.f9865f0 == 0 || this.f9866g0 == 0) {
                int paddingLeft = getPaddingLeft();
                int paddingRight = getPaddingRight();
                int i3 = this.f9865f0;
                if (i3 == 0) {
                    i3 = SmartUtil.m15574a(20.0f);
                }
                this.f9865f0 = i3;
                int i4 = this.f9866g0;
                if (i4 == 0) {
                    i4 = SmartUtil.m15574a(20.0f);
                }
                this.f9866g0 = i4;
                setPadding(paddingLeft, this.f9865f0, paddingRight, this.f9866g0);
            }
            setClipToPadding(false);
        }
        if (View.MeasureSpec.getMode(i2) == 1073741824) {
            int size = View.MeasureSpec.getSize(i2);
            int i5 = this.f9867h0;
            if (size < i5) {
                int i6 = (size - i5) / 2;
                setPadding(getPaddingLeft(), i6, getPaddingRight(), i6);
            } else {
                setPadding(getPaddingLeft(), 0, getPaddingRight(), 0);
            }
        } else {
            setPadding(getPaddingLeft(), this.f9865f0, getPaddingRight(), this.f9866g0);
        }
        super.onMeasure(i, i2);
        if (this.f9867h0 == 0) {
            for (int i7 = 0; i7 < getChildCount(); i7++) {
                int measuredHeight = getChildAt(i7).getMeasuredHeight();
                if (this.f9867h0 < measuredHeight) {
                    this.f9867h0 = measuredHeight;
                }
            }
        }
    }

    public void setPrimaryColors(@ColorInt int... iArr) {
        if (iArr.length > 0) {
            if (!(getBackground() instanceof BitmapDrawable) && !this.f9862c0) {
                mo26179b(iArr[0]);
                this.f9862c0 = false;
            }
            if (!this.f9861b0) {
                if (iArr.length > 1) {
                    mo26163a(iArr[1]);
                }
                this.f9861b0 = false;
            }
        }
    }

    /* renamed from: b */
    public T mo26179b(@ColorInt int i) {
        this.f9862c0 = true;
        this.f9863d0 = i;
        RefreshKernel iVar = this.f9858V;
        if (iVar != null) {
            iVar.mo26105a(this, i);
        }
        return mo26178b();
    }

    /* renamed from: a */
    public void mo26129a(@NonNull RefreshLayout jVar, int i, int i2) {
        ImageView imageView = this.f9857U;
        if (imageView.getVisibility() != 0) {
            imageView.setVisibility(0);
            Drawable drawable = this.f9857U.getDrawable();
            if (drawable instanceof Animatable) {
                ((Animatable) drawable).start();
            } else {
                imageView.animate().rotation(36000.0f).setDuration(100000);
            }
        }
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        ImageView imageView = this.f9857U;
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof Animatable) {
            Animatable animatable = (Animatable) drawable;
            if (animatable.isRunning()) {
                animatable.stop();
            }
        } else {
            imageView.animate().rotation(0.0f).setDuration(0);
        }
        imageView.setVisibility(8);
        return this.f9864e0;
    }

    /* renamed from: a */
    public T mo26163a(@ColorInt int i) {
        this.f9861b0 = true;
        this.f9855S.setTextColor(i);
        PaintDrawable bVar = this.f9859W;
        if (bVar != null) {
            bVar.mo26183a(i);
            this.f9856T.invalidateDrawable(this.f9859W);
        }
        PaintDrawable bVar2 = this.f9860a0;
        if (bVar2 != null) {
            bVar2.mo26183a(i);
            this.f9857U.invalidateDrawable(this.f9860a0);
        }
        return mo26178b();
    }
}
