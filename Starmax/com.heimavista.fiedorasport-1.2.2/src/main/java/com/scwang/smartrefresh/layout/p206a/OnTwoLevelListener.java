package com.scwang.smartrefresh.layout.p206a;

import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.d */
public interface OnTwoLevelListener {
    /* renamed from: a */
    boolean mo26116a(@NonNull RefreshLayout jVar);
}
