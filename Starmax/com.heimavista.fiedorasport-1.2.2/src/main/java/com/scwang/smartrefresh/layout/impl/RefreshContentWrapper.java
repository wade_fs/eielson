package com.scwang.smartrefresh.layout.impl;

import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.core.view.NestedScrollingChild;
import androidx.core.view.NestedScrollingParent;
import androidx.legacy.widget.Space;
import androidx.viewpager.widget.ViewPager;
import com.scwang.smartrefresh.layout.p206a.RefreshContent;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.p208c.CoordinatorLayoutListener;
import com.scwang.smartrefresh.layout.p209d.DesignUtil;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;
import java.util.LinkedList;

/* renamed from: com.scwang.smartrefresh.layout.impl.a */
public class RefreshContentWrapper implements RefreshContent, CoordinatorLayoutListener, ValueAnimator.AnimatorUpdateListener {

    /* renamed from: P */
    protected View f9840P;

    /* renamed from: Q */
    protected View f9841Q;

    /* renamed from: R */
    protected View f9842R;

    /* renamed from: S */
    protected View f9843S;

    /* renamed from: T */
    protected View f9844T;

    /* renamed from: U */
    protected int f9845U = 0;

    /* renamed from: V */
    protected boolean f9846V = true;

    /* renamed from: W */
    protected boolean f9847W = true;

    /* renamed from: X */
    protected ScrollBoundaryDeciderAdapter f9848X = new ScrollBoundaryDeciderAdapter();

    public RefreshContentWrapper(@NonNull View view) {
        this.f9842R = view;
        this.f9841Q = view;
        this.f9840P = view;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26176a(View view, RefreshKernel iVar) {
        boolean isInEditMode = this.f9840P.isInEditMode();
        View view2 = null;
        while (true) {
            if (view2 != null && (!(view2 instanceof NestedScrollingParent) || (view2 instanceof NestedScrollingChild))) {
                break;
            }
            view = mo26175a(view, view2 == null);
            if (view == view2) {
                break;
            }
            if (!isInEditMode) {
                DesignUtil.m15571a(view, iVar, this);
            }
            view2 = view;
        }
        if (view2 != null) {
            this.f9842R = view2;
        }
    }

    /* renamed from: b */
    public boolean mo26124b() {
        return this.f9847W && this.f9848X.mo26134b(this.f9840P);
    }

    @NonNull
    /* renamed from: c */
    public View mo26125c() {
        return this.f9842R;
    }

    @NonNull
    public View getView() {
        return this.f9840P;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        try {
            float scaleY = ((float) (intValue - this.f9845U)) * this.f9842R.getScaleY();
            if (this.f9842R instanceof AbsListView) {
                SmartUtil.m15575a((AbsListView) this.f9842R, (int) scaleY);
            } else {
                this.f9842R.scrollBy(0, (int) scaleY);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        this.f9845U = intValue;
    }

    /* renamed from: a */
    public void mo26140a(boolean z, boolean z2) {
        this.f9846V = z;
        this.f9847W = z2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public View mo26175a(View view, boolean z) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(view);
        View view2 = null;
        while (linkedList.size() > 0 && view2 == null) {
            View view3 = (View) linkedList.poll();
            if (view3 != null) {
                if ((z || view3 != view) && SmartUtil.m15576a(view3)) {
                    view2 = view3;
                } else if (view3 instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view3;
                    for (int i = 0; i < viewGroup.getChildCount(); i++) {
                        linkedList.add(viewGroup.getChildAt(i));
                    }
                }
            }
        }
        return view2 == null ? view : view2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public View mo26174a(View view, PointF pointF, View view2) {
        if ((view instanceof ViewGroup) && pointF != null) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            PointF pointF2 = new PointF();
            while (childCount > 0) {
                View childAt = viewGroup.getChildAt(childCount - 1);
                if (!SmartUtil.m15580a(viewGroup, childAt, pointF.x, pointF.y, pointF2)) {
                    childCount--;
                } else if (!(childAt instanceof ViewPager) && SmartUtil.m15576a(childAt)) {
                    return childAt;
                } else {
                    pointF.offset(pointF2.x, pointF2.y);
                    View a = mo26174a(childAt, pointF, view2);
                    pointF.offset(-pointF2.x, -pointF2.y);
                    return a;
                }
            }
        }
        return view2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo26118a(int r6, int r7, int r8) {
        /*
            r5 = this;
            r0 = 1
            r1 = -1
            r2 = 0
            r3 = 0
            if (r7 == r1) goto L_0x0021
            android.view.View r4 = r5.f9841Q
            android.view.View r7 = r4.findViewById(r7)
            if (r7 == 0) goto L_0x0021
            if (r6 <= 0) goto L_0x0016
            float r4 = (float) r6
            r7.setTranslationY(r4)
            r7 = 1
            goto L_0x0022
        L_0x0016:
            float r4 = r7.getTranslationY()
            int r4 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r4 <= 0) goto L_0x0021
            r7.setTranslationY(r3)
        L_0x0021:
            r7 = 0
        L_0x0022:
            if (r8 == r1) goto L_0x003f
            android.view.View r1 = r5.f9841Q
            android.view.View r8 = r1.findViewById(r8)
            if (r8 == 0) goto L_0x003f
            if (r6 >= 0) goto L_0x0034
            float r7 = (float) r6
            r8.setTranslationY(r7)
            r7 = 1
            goto L_0x003f
        L_0x0034:
            float r0 = r8.getTranslationY()
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x003f
            r8.setTranslationY(r3)
        L_0x003f:
            if (r7 != 0) goto L_0x0048
            android.view.View r7 = r5.f9841Q
            float r8 = (float) r6
            r7.setTranslationY(r8)
            goto L_0x004d
        L_0x0048:
            android.view.View r7 = r5.f9841Q
            r7.setTranslationY(r3)
        L_0x004d:
            android.view.View r7 = r5.f9843S
            if (r7 == 0) goto L_0x0059
            int r8 = java.lang.Math.max(r2, r6)
            float r8 = (float) r8
            r7.setTranslationY(r8)
        L_0x0059:
            android.view.View r7 = r5.f9844T
            if (r7 == 0) goto L_0x0065
            int r6 = java.lang.Math.min(r2, r6)
            float r6 = (float) r6
            r7.setTranslationY(r6)
        L_0x0065:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.impl.RefreshContentWrapper.mo26118a(int, int, int):void");
    }

    /* renamed from: a */
    public boolean mo26123a() {
        return this.f9846V && this.f9848X.mo26133a(this.f9840P);
    }

    /* renamed from: a */
    public void mo26119a(MotionEvent motionEvent) {
        PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
        pointF.offset((float) (-this.f9840P.getLeft()), (float) (-this.f9840P.getTop()));
        View view = this.f9842R;
        View view2 = this.f9840P;
        if (view != view2) {
            this.f9842R = mo26174a(view2, pointF, view);
        }
        if (this.f9842R == this.f9840P) {
            this.f9848X.f9849a = null;
        } else {
            this.f9848X.f9849a = pointF;
        }
    }

    /* renamed from: a */
    public void mo26120a(RefreshKernel iVar, View view, View view2) {
        mo26176a(this.f9840P, iVar);
        if (view != null || view2 != null) {
            this.f9843S = view;
            this.f9844T = view2;
            FrameLayout frameLayout = new FrameLayout(this.f9840P.getContext());
            iVar.mo26111b().getLayout().removeView(this.f9840P);
            ViewGroup.LayoutParams layoutParams = this.f9840P.getLayoutParams();
            frameLayout.addView(this.f9840P, -1, -1);
            iVar.mo26111b().getLayout().addView(frameLayout, layoutParams);
            this.f9840P = frameLayout;
            if (view != null) {
                view.setTag("fixed-top");
                ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                int indexOfChild = viewGroup.indexOfChild(view);
                viewGroup.removeView(view);
                layoutParams2.height = SmartUtil.m15584c(view);
                viewGroup.addView(new Space(this.f9840P.getContext()), indexOfChild, layoutParams2);
                frameLayout.addView(view);
            }
            if (view2 != null) {
                view2.setTag("fixed-bottom");
                ViewGroup.LayoutParams layoutParams3 = view2.getLayoutParams();
                ViewGroup viewGroup2 = (ViewGroup) view2.getParent();
                int indexOfChild2 = viewGroup2.indexOfChild(view2);
                viewGroup2.removeView(view2);
                FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(layoutParams3);
                layoutParams3.height = SmartUtil.m15584c(view2);
                viewGroup2.addView(new Space(this.f9840P.getContext()), indexOfChild2, layoutParams3);
                layoutParams4.gravity = 80;
                frameLayout.addView(view2, layoutParams4);
            }
        }
    }

    /* renamed from: a */
    public void mo26121a(ScrollBoundaryDecider kVar) {
        if (kVar instanceof ScrollBoundaryDeciderAdapter) {
            this.f9848X = (ScrollBoundaryDeciderAdapter) kVar;
        } else {
            this.f9848X.f9850b = kVar;
        }
    }

    /* renamed from: a */
    public void mo26122a(boolean z) {
        this.f9848X.f9851c = z;
    }

    /* renamed from: a */
    public ValueAnimator.AnimatorUpdateListener mo26117a(int i) {
        View view = this.f9842R;
        if (view == null || i == 0) {
            return null;
        }
        if ((i >= 0 || !SmartUtil.m15577a(view, 1)) && (i <= 0 || !SmartUtil.m15577a(this.f9842R, -1))) {
            return null;
        }
        this.f9845U = i;
        return this;
    }
}
