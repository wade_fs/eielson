package com.scwang.smartrefresh.layout.p207b;

/* renamed from: com.scwang.smartrefresh.layout.b.a */
public class DimensionStatus {

    /* renamed from: c */
    public static final DimensionStatus f9702c = new DimensionStatus(0, false);

    /* renamed from: d */
    public static final DimensionStatus f9703d = new DimensionStatus(1, true);

    /* renamed from: e */
    public static final DimensionStatus f9704e = new DimensionStatus(2, false);

    /* renamed from: f */
    public static final DimensionStatus f9705f = new DimensionStatus(3, true);

    /* renamed from: g */
    public static final DimensionStatus f9706g = new DimensionStatus(4, false);

    /* renamed from: h */
    public static final DimensionStatus f9707h = new DimensionStatus(5, true);

    /* renamed from: i */
    public static final DimensionStatus f9708i = new DimensionStatus(6, false);

    /* renamed from: j */
    public static final DimensionStatus f9709j = new DimensionStatus(7, true);

    /* renamed from: k */
    public static final DimensionStatus f9710k = new DimensionStatus(8, false);

    /* renamed from: l */
    public static final DimensionStatus f9711l = new DimensionStatus(9, true);

    /* renamed from: m */
    public static final DimensionStatus f9712m = new DimensionStatus(10, false);

    /* renamed from: n */
    public static final DimensionStatus f9713n = new DimensionStatus(10, true);

    /* renamed from: o */
    public static final DimensionStatus[] f9714o = {f9702c, f9703d, f9704e, f9705f, f9706g, f9707h, f9708i, f9709j, f9710k, f9711l, f9712m, f9713n};

    /* renamed from: a */
    public final int f9715a;

    /* renamed from: b */
    public final boolean f9716b;

    private DimensionStatus(int i, boolean z) {
        this.f9715a = i;
        this.f9716b = z;
    }

    /* renamed from: a */
    public DimensionStatus mo26135a() {
        return !this.f9716b ? f9714o[this.f9715a + 1] : this;
    }

    /* renamed from: b */
    public DimensionStatus mo26137b() {
        if (!this.f9716b) {
            return this;
        }
        DimensionStatus aVar = f9714o[this.f9715a - 1];
        if (!aVar.f9716b) {
            return aVar;
        }
        return f9702c;
    }

    /* renamed from: a */
    public boolean mo26136a(DimensionStatus aVar) {
        return this.f9715a < aVar.f9715a || ((!this.f9716b || f9711l == this) && this.f9715a == aVar.f9715a);
    }
}
