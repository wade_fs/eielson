package com.scwang.smartrefresh.layout;

public final class R$id {
    public static final int FixedBehind = 2131296264;
    public static final int FixedFront = 2131296265;
    public static final int MatchLayout = 2131296268;
    public static final int Scale = 2131296279;
    public static final int Translate = 2131296284;
    public static final int srl_classics_arrow = 2131296903;
    public static final int srl_classics_center = 2131296904;
    public static final int srl_classics_progress = 2131296905;
    public static final int srl_classics_title = 2131296906;
    public static final int srl_classics_update = 2131296907;

    private R$id() {
    }
}
