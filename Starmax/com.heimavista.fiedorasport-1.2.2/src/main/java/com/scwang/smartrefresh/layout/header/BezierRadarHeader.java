package com.scwang.smartrefresh.layout.header;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import com.scwang.smartrefresh.layout.R$styleable;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public class BezierRadarHeader extends InternalAbstract implements RefreshHeader {

    /* renamed from: S */
    protected int f9779S;

    /* renamed from: T */
    protected int f9780T;

    /* renamed from: U */
    protected boolean f9781U;

    /* renamed from: V */
    protected boolean f9782V;

    /* renamed from: W */
    protected boolean f9783W;

    /* renamed from: a0 */
    protected boolean f9784a0;

    /* renamed from: b0 */
    protected Path f9785b0;

    /* renamed from: c0 */
    protected Paint f9786c0;

    /* renamed from: d0 */
    protected int f9787d0;

    /* renamed from: e0 */
    protected int f9788e0;

    /* renamed from: f0 */
    protected int f9789f0;

    /* renamed from: g0 */
    protected float f9790g0;

    /* renamed from: h0 */
    protected float f9791h0;

    /* renamed from: i0 */
    protected float f9792i0;

    /* renamed from: j0 */
    protected float f9793j0;

    /* renamed from: k0 */
    protected int f9794k0;

    /* renamed from: l0 */
    protected float f9795l0;

    /* renamed from: m0 */
    protected float f9796m0;

    /* renamed from: n0 */
    protected float f9797n0;

    /* renamed from: o0 */
    protected Animator f9798o0;

    /* renamed from: p0 */
    protected RectF f9799p0;

    /* renamed from: com.scwang.smartrefresh.layout.header.BezierRadarHeader$a */
    static /* synthetic */ class C4710a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9800a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.scwang.smartrefresh.layout.header.BezierRadarHeader.C4710a.f9800a = r0
                int[] r0 = com.scwang.smartrefresh.layout.header.BezierRadarHeader.C4710a.f9800a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.scwang.smartrefresh.layout.header.BezierRadarHeader.C4710a.f9800a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.header.BezierRadarHeader.C4710a.<clinit>():void");
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.header.BezierRadarHeader$b */
    protected class C4711b implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: P */
        byte f9801P;

        C4711b(byte b) {
            this.f9801P = b;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            byte b = this.f9801P;
            if (b == 0) {
                BezierRadarHeader.this.f9797n0 = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            } else if (1 == b) {
                BezierRadarHeader bezierRadarHeader = BezierRadarHeader.this;
                if (bezierRadarHeader.f9783W) {
                    valueAnimator.cancel();
                    return;
                }
                bezierRadarHeader.f9788e0 = ((Integer) valueAnimator.getAnimatedValue()).intValue() / 2;
            } else if (2 == b) {
                BezierRadarHeader.this.f9790g0 = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            } else if (3 == b) {
                BezierRadarHeader.this.f9793j0 = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            } else if (4 == b) {
                BezierRadarHeader.this.f9794k0 = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            }
            BezierRadarHeader.this.invalidate();
        }
    }

    public BezierRadarHeader(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26155a(Canvas canvas, int i) {
        this.f9785b0.reset();
        this.f9785b0.lineTo(0.0f, (float) this.f9787d0);
        Path path = this.f9785b0;
        int i2 = this.f9789f0;
        float f = i2 >= 0 ? (float) i2 : ((float) i) / 2.0f;
        int i3 = this.f9787d0;
        float f2 = (float) i;
        path.quadTo(f, (float) (this.f9788e0 + i3), f2, (float) i3);
        this.f9785b0.lineTo(f2, 0.0f);
        this.f9786c0.setColor(this.f9780T);
        canvas.drawPath(this.f9785b0, this.f9786c0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo26158b(Canvas canvas, int i, int i2) {
        if (this.f9798o0 != null || isInEditMode()) {
            float f = this.f9795l0;
            float f2 = this.f9797n0;
            float f3 = f * f2;
            float f4 = this.f9796m0 * f2;
            this.f9786c0.setColor(this.f9779S);
            this.f9786c0.setStyle(Paint.Style.FILL);
            float f5 = ((float) i) / 2.0f;
            float f6 = ((float) i2) / 2.0f;
            canvas.drawCircle(f5, f6, f3, this.f9786c0);
            this.f9786c0.setStyle(Paint.Style.STROKE);
            float f7 = f4 + f3;
            canvas.drawCircle(f5, f6, f7, this.f9786c0);
            this.f9786c0.setColor((this.f9780T & ViewCompat.MEASURED_SIZE_MASK) | 1426063360);
            this.f9786c0.setStyle(Paint.Style.FILL);
            this.f9799p0.set(f5 - f3, f6 - f3, f5 + f3, f3 + f6);
            canvas.drawArc(this.f9799p0, 270.0f, (float) this.f9794k0, true, this.f9786c0);
            this.f9786c0.setStyle(Paint.Style.STROKE);
            this.f9799p0.set(f5 - f7, f6 - f7, f5 + f7, f6 + f7);
            canvas.drawArc(this.f9799p0, 270.0f, (float) this.f9794k0, false, this.f9786c0);
            this.f9786c0.setStyle(Paint.Style.FILL);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo26159c(Canvas canvas, int i, int i2) {
        if (this.f9793j0 > 0.0f) {
            this.f9786c0.setColor(this.f9779S);
            canvas.drawCircle(((float) i) / 2.0f, ((float) i2) / 2.0f, this.f9793j0, this.f9786c0);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        mo26155a(canvas, width);
        mo26156a(canvas, width, height);
        mo26158b(canvas, width, height);
        mo26159c(canvas, width, height);
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Animator animator = this.f9798o0;
        if (animator != null) {
            animator.removeAllListeners();
            this.f9798o0.end();
            this.f9798o0 = null;
        }
    }

    @Deprecated
    public void setPrimaryColors(@ColorInt int... iArr) {
        if (iArr.length > 0 && !this.f9781U) {
            mo26157b(iArr[0]);
            this.f9781U = false;
        }
        if (iArr.length > 1 && !this.f9782V) {
            mo26154a(iArr[1]);
            this.f9782V = false;
        }
    }

    public BezierRadarHeader(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BezierRadarHeader(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9784a0 = false;
        this.f9789f0 = -1;
        this.f9794k0 = 0;
        this.f9795l0 = 0.0f;
        this.f9796m0 = 0.0f;
        this.f9797n0 = 0.0f;
        this.f9799p0 = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        super.f9853Q = SpinnerStyle.f9743c;
        this.f9785b0 = new Path();
        this.f9786c0 = new Paint();
        this.f9786c0.setAntiAlias(true);
        this.f9792i0 = (float) SmartUtil.m15574a(7.0f);
        this.f9795l0 = (float) SmartUtil.m15574a(20.0f);
        this.f9796m0 = (float) SmartUtil.m15574a(7.0f);
        this.f9786c0.setStrokeWidth((float) SmartUtil.m15574a(3.0f));
        setMinimumHeight(SmartUtil.m15574a(100.0f));
        if (isInEditMode()) {
            this.f9787d0 = 1000;
            this.f9797n0 = 1.0f;
            this.f9794k0 = 270;
        } else {
            this.f9797n0 = 0.0f;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.BezierRadarHeader);
        this.f9784a0 = obtainStyledAttributes.getBoolean(R$styleable.BezierRadarHeader_srlEnableHorizontalDrag, this.f9784a0);
        mo26154a(obtainStyledAttributes.getColor(R$styleable.BezierRadarHeader_srlAccentColor, -1));
        mo26157b(obtainStyledAttributes.getColor(R$styleable.BezierRadarHeader_srlPrimaryColor, -14540254));
        this.f9782V = obtainStyledAttributes.hasValue(R$styleable.BezierRadarHeader_srlAccentColor);
        this.f9781U = obtainStyledAttributes.hasValue(R$styleable.BezierRadarHeader_srlPrimaryColor);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26156a(Canvas canvas, int i, int i2) {
        float f = 0.0f;
        if (this.f9790g0 > 0.0f) {
            this.f9786c0.setColor(this.f9779S);
            float a = SmartUtil.m15573a(i2);
            float f2 = (float) i;
            float f3 = 7.0f;
            float f4 = (f2 * 1.0f) / 7.0f;
            float f5 = this.f9791h0;
            float f6 = (f4 * f5) - (f5 > 1.0f ? ((f5 - 1.0f) * f4) / f5 : 0.0f);
            float f7 = (float) i2;
            float f8 = this.f9791h0;
            if (f8 > 1.0f) {
                f = (((f8 - 1.0f) * f7) / 2.0f) / f8;
            }
            float f9 = f7 - f;
            int i3 = 0;
            while (i3 < 7) {
                float f10 = (((float) i3) + 1.0f) - 4.0f;
                this.f9786c0.setAlpha((int) (((double) (this.f9790g0 * (1.0f - ((Math.abs(f10) / f3) * 2.0f)) * 255.0f)) * (1.0d - (1.0d / Math.pow((((double) a) / 800.0d) + 1.0d, 15.0d)))));
                float f11 = this.f9792i0 * (1.0f - (1.0f / ((a / 10.0f) + 1.0f)));
                canvas.drawCircle(((f2 / 2.0f) - (f11 / 2.0f)) + (f6 * f10), f9 / 2.0f, f11, this.f9786c0);
                i3++;
                f3 = 7.0f;
            }
            this.f9786c0.setAlpha(255);
        }
    }

    /* renamed from: a */
    public void mo24850a(boolean z, float f, int i, int i2, int i3) {
        if (z || this.f9783W) {
            this.f9783W = true;
            this.f9787d0 = Math.min(i2, i);
            this.f9788e0 = (int) (((float) Math.max(0, i - i2)) * 1.9f);
            this.f9791h0 = f;
        }
    }

    /* renamed from: b */
    public void mo24851b(@NonNull RefreshLayout jVar, int i, int i2) {
        this.f9787d0 = i - 1;
        this.f9783W = false;
        SmartUtil bVar = new SmartUtil();
        ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
        ofFloat.setInterpolator(bVar);
        ofFloat.addUpdateListener(new C4711b((byte) 2));
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setInterpolator(bVar);
        ofFloat2.addUpdateListener(new C4711b((byte) 0));
        ValueAnimator ofInt = ValueAnimator.ofInt(0, 360);
        ofInt.setDuration(720L);
        ofInt.setRepeatCount(-1);
        ofInt.setInterpolator(new AccelerateDecelerateInterpolator());
        ofInt.addUpdateListener(new C4711b((byte) 4));
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(ofFloat, ofFloat2, ofInt);
        animatorSet.start();
        int i3 = this.f9788e0;
        ValueAnimator ofInt2 = ValueAnimator.ofInt(i3, 0, -((int) (((float) i3) * 0.8f)), 0, -((int) (((float) i3) * 0.4f)), 0);
        ofInt2.addUpdateListener(new C4711b((byte) 1));
        ofInt2.setInterpolator(bVar);
        ofInt2.setDuration(800L);
        ofInt2.start();
        this.f9798o0 = animatorSet;
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        Animator animator = this.f9798o0;
        if (animator != null) {
            animator.removeAllListeners();
            this.f9798o0.end();
            this.f9798o0 = null;
        }
        int width = getWidth();
        int height = getHeight();
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, (float) Math.sqrt((double) ((width * width) + (height * height))));
        ofFloat.setDuration(400L);
        ofFloat.addUpdateListener(new C4711b((byte) 3));
        ofFloat.start();
        return 400;
    }

    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        int i = C4710a.f9800a[bVar2.ordinal()];
        if (i == 1 || i == 2) {
            this.f9790g0 = 1.0f;
            this.f9797n0 = 0.0f;
            this.f9793j0 = 0.0f;
        }
    }

    /* renamed from: a */
    public boolean mo26130a() {
        return this.f9784a0;
    }

    /* renamed from: a */
    public void mo26128a(float f, int i, int i2) {
        this.f9789f0 = i;
        invalidate();
    }

    /* renamed from: a */
    public BezierRadarHeader mo26154a(@ColorInt int i) {
        this.f9779S = i;
        this.f9782V = true;
        return this;
    }

    /* renamed from: b */
    public BezierRadarHeader mo26157b(@ColorInt int i) {
        this.f9780T = i;
        this.f9781U = true;
        return this;
    }
}
