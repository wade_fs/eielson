package com.scwang.smartrefresh.layout;

public final class R$string {
    public static final int srl_component_falsify = 2131821120;
    public static final int srl_content_empty = 2131821121;
    public static final int srl_footer_failed = 2131821122;
    public static final int srl_footer_finish = 2131821123;
    public static final int srl_footer_loading = 2131821124;
    public static final int srl_footer_nothing = 2131821125;
    public static final int srl_footer_pulling = 2131821126;
    public static final int srl_footer_refreshing = 2131821127;
    public static final int srl_footer_release = 2131821128;
    public static final int srl_header_failed = 2131821129;
    public static final int srl_header_finish = 2131821130;
    public static final int srl_header_loading = 2131821131;
    public static final int srl_header_pulling = 2131821132;
    public static final int srl_header_refreshing = 2131821133;
    public static final int srl_header_release = 2131821134;
    public static final int srl_header_secondary = 2131821135;
    public static final int srl_header_update = 2131821136;

    private R$string() {
    }
}
