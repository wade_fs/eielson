package com.scwang.smartrefresh.layout.p207b;

/* renamed from: com.scwang.smartrefresh.layout.b.c */
public class SpinnerStyle {

    /* renamed from: b */
    public static final SpinnerStyle f9742b = new SpinnerStyle(0);

    /* renamed from: c */
    public static final SpinnerStyle f9743c = new SpinnerStyle(1);

    /* renamed from: d */
    public static final SpinnerStyle f9744d = new SpinnerStyle(2);

    /* renamed from: e */
    public static final SpinnerStyle f9745e = new SpinnerStyle(3);

    /* renamed from: f */
    public static final SpinnerStyle f9746f = new SpinnerStyle(4);

    /* renamed from: g */
    public static final SpinnerStyle[] f9747g = {f9742b, f9743c, f9744d, f9745e, f9746f};

    /* renamed from: a */
    public final int f9748a;

    private SpinnerStyle(int i) {
        this.f9748a = i;
    }
}
