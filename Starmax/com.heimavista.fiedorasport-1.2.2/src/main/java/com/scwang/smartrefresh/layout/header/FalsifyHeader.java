package com.scwang.smartrefresh.layout.header;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.R$string;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public class FalsifyHeader extends InternalAbstract implements RefreshHeader {

    /* renamed from: S */
    protected RefreshKernel f9826S;

    public FalsifyHeader(Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public void mo24848a(@NonNull RefreshKernel iVar, int i, int i2) {
        this.f9826S = iVar;
    }

    /* renamed from: b */
    public void mo24851b(@NonNull RefreshLayout jVar, int i, int i2) {
        RefreshKernel iVar = this.f9826S;
        if (iVar != null) {
            iVar.mo26107a(RefreshState.None);
            this.f9826S.mo26107a(RefreshState.RefreshFinish);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (isInEditMode()) {
            int a = SmartUtil.m15574a(5.0f);
            Context context = getContext();
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(-858993460);
            paint.setStrokeWidth((float) SmartUtil.m15574a(1.0f));
            float f = (float) a;
            paint.setPathEffect(new DashPathEffect(new float[]{f, f, f, f}, 1.0f));
            canvas.drawRect(f, f, (float) (getWidth() - a), (float) (getBottom() - a), paint);
            TextView textView = new TextView(context);
            textView.setText(context.getString(R$string.srl_component_falsify, FalsifyHeader.class.getSimpleName(), Float.valueOf(SmartUtil.m15573a(getHeight()))));
            textView.setTextColor(-858993460);
            textView.setGravity(17);
            textView.measure(View.MeasureSpec.makeMeasureSpec(getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getHeight(), 1073741824));
            textView.layout(0, 0, getWidth(), getHeight());
            textView.draw(canvas);
        }
    }

    public FalsifyHeader(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FalsifyHeader(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
