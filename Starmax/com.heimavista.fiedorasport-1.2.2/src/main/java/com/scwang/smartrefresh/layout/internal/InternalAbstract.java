package com.scwang.smartrefresh.layout.internal;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.impl.RefreshFooterWrapper;
import com.scwang.smartrefresh.layout.impl.RefreshHeaderWrapper;
import com.scwang.smartrefresh.layout.p206a.RefreshFooter;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshInternal;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;

public abstract class InternalAbstract extends RelativeLayout implements RefreshInternal {

    /* renamed from: P */
    protected View f9852P;

    /* renamed from: Q */
    protected SpinnerStyle f9853Q;

    /* renamed from: R */
    protected RefreshInternal f9854R;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    protected InternalAbstract(@NonNull View view) {
        this(view, view instanceof RefreshInternal ? (RefreshInternal) view : null);
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        RefreshInternal hVar = this.f9854R;
        if (hVar == null || hVar == this) {
            return 0;
        }
        return hVar.mo24847a(jVar, z);
    }

    /* renamed from: b */
    public void mo24851b(@NonNull RefreshLayout jVar, int i, int i2) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            hVar.mo24851b(jVar, i, i2);
        }
    }

    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            return true;
        }
        if (!(obj instanceof RefreshInternal) || getView() != ((RefreshInternal) obj).getView()) {
            return false;
        }
        return true;
    }

    @NonNull
    public SpinnerStyle getSpinnerStyle() {
        int i;
        SpinnerStyle cVar = this.f9853Q;
        if (cVar != null) {
            return cVar;
        }
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            return hVar.getSpinnerStyle();
        }
        View view = this.f9852P;
        if (view != null) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams instanceof SmartRefreshLayout.C4705l) {
                this.f9853Q = ((SmartRefreshLayout.C4705l) layoutParams).f9699b;
                SpinnerStyle cVar2 = this.f9853Q;
                if (cVar2 != null) {
                    return cVar2;
                }
            }
            if (layoutParams != null && ((i = layoutParams.height) == 0 || i == -1)) {
                SpinnerStyle cVar3 = SpinnerStyle.f9743c;
                this.f9853Q = cVar3;
                return cVar3;
            }
        }
        SpinnerStyle cVar4 = SpinnerStyle.f9742b;
        this.f9853Q = cVar4;
        return cVar4;
    }

    @NonNull
    public View getView() {
        View view = this.f9852P;
        return view == null ? this : view;
    }

    public void setPrimaryColors(@ColorInt int... iArr) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            hVar.setPrimaryColors(iArr);
        }
    }

    protected InternalAbstract(@NonNull View view, @Nullable RefreshInternal hVar) {
        super(view.getContext(), null, 0);
        this.f9852P = view;
        this.f9854R = hVar;
        if (this instanceof RefreshFooterWrapper) {
            RefreshInternal hVar2 = this.f9854R;
            if ((hVar2 instanceof RefreshHeader) && hVar2.getSpinnerStyle() == SpinnerStyle.f9746f) {
                hVar.getView().setScaleY(-1.0f);
                return;
            }
        }
        if (this instanceof RefreshHeaderWrapper) {
            RefreshInternal hVar3 = this.f9854R;
            if ((hVar3 instanceof RefreshFooter) && hVar3.getSpinnerStyle() == SpinnerStyle.f9746f) {
                hVar.getView().setScaleY(-1.0f);
            }
        }
    }

    /* renamed from: a */
    public void mo24848a(@NonNull RefreshKernel iVar, int i, int i2) {
        RefreshInternal hVar = this.f9854R;
        if (hVar == null || hVar == this) {
            View view = this.f9852P;
            if (view != null) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (layoutParams instanceof SmartRefreshLayout.C4705l) {
                    iVar.mo26105a(this, ((SmartRefreshLayout.C4705l) layoutParams).f9698a);
                    return;
                }
                return;
            }
            return;
        }
        hVar.mo24848a(iVar, i, i2);
    }

    protected InternalAbstract(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: a */
    public boolean mo26130a() {
        RefreshInternal hVar = this.f9854R;
        return (hVar == null || hVar == this || !hVar.mo26130a()) ? false : true;
    }

    /* renamed from: a */
    public void mo26128a(float f, int i, int i2) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            hVar.mo26128a(f, i, i2);
        }
    }

    /* renamed from: a */
    public void mo24850a(boolean z, float f, int i, int i2, int i3) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            hVar.mo24850a(z, f, i, i2, i3);
        }
    }

    /* renamed from: a */
    public void mo26129a(@NonNull RefreshLayout jVar, int i, int i2) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            hVar.mo26129a(jVar, i, i2);
        }
    }

    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        RefreshInternal hVar = this.f9854R;
        if (hVar != null && hVar != this) {
            if ((this instanceof RefreshFooterWrapper) && (hVar instanceof RefreshHeader)) {
                if (bVar.f9736Q) {
                    bVar = bVar.mo26139h();
                }
                if (bVar2.f9736Q) {
                    bVar2 = bVar2.mo26139h();
                }
            } else if ((this instanceof RefreshHeaderWrapper) && (this.f9854R instanceof RefreshFooter)) {
                if (bVar.f9735P) {
                    bVar = bVar.mo26138a();
                }
                if (bVar2.f9735P) {
                    bVar2 = bVar2.mo26138a();
                }
            }
            RefreshInternal hVar2 = this.f9854R;
            if (hVar2 != null) {
                hVar2.mo24849a(jVar, bVar, bVar2);
            }
        }
    }

    /* renamed from: a */
    public boolean mo26127a(boolean z) {
        RefreshInternal hVar = this.f9854R;
        return (hVar instanceof RefreshFooter) && ((RefreshFooter) hVar).mo26127a(z);
    }
}
