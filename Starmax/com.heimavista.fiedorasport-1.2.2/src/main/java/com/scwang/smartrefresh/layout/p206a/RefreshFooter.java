package com.scwang.smartrefresh.layout.p206a;

import androidx.annotation.RestrictTo;

/* renamed from: com.scwang.smartrefresh.layout.a.f */
public interface RefreshFooter extends RefreshInternal {
    @RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP, RestrictTo.Scope.SUBCLASSES})
    /* renamed from: a */
    boolean mo26127a(boolean z);
}
