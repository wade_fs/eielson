package com.scwang.smartrefresh.layout.p206a;

import android.view.ViewGroup;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.j */
public interface RefreshLayout {
    /* renamed from: a */
    RefreshLayout mo26038a();

    /* renamed from: a */
    RefreshLayout mo26039a(@FloatRange(from = 1.0d, mo446to = 10.0d) float f);

    /* renamed from: a */
    RefreshLayout mo26049a(boolean z);

    /* renamed from: b */
    RefreshLayout mo26054b(boolean z);

    @NonNull
    ViewGroup getLayout();
}
