package com.scwang.smartrefresh.layout.impl;

import android.annotation.SuppressLint;
import android.view.View;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;

@SuppressLint({"ViewConstructor"})
public class RefreshHeaderWrapper extends InternalAbstract implements RefreshHeader {
    public RefreshHeaderWrapper(View view) {
        super(view);
    }
}
