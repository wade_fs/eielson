package com.scwang.smartrefresh.layout.p209d;

import android.content.res.Resources;
import android.graphics.PointF;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.core.view.NestedScrollingChild;
import androidx.core.view.NestedScrollingParent;
import androidx.core.view.ScrollingView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

/* renamed from: com.scwang.smartrefresh.layout.d.b */
public class SmartUtil implements Interpolator {

    /* renamed from: a */
    private static float f9750a = Resources.getSystem().getDisplayMetrics().density;

    /* renamed from: b */
    private static final float f9751b = (1.0f / m15581b(1.0f));

    /* renamed from: c */
    private static final float f9752c = (1.0f - (f9751b * m15581b(1.0f)));

    /* renamed from: a */
    public static void m15575a(@NonNull AbsListView absListView, int i) {
        View childAt;
        if (Build.VERSION.SDK_INT >= 19) {
            absListView.scrollListBy(i);
        } else if (absListView instanceof ListView) {
            int firstVisiblePosition = absListView.getFirstVisiblePosition();
            if (firstVisiblePosition != -1 && (childAt = absListView.getChildAt(0)) != null) {
                ((ListView) absListView).setSelectionFromTop(firstVisiblePosition, childAt.getTop() - i);
            }
        } else {
            absListView.smoothScrollBy(i, 0);
        }
    }

    /* renamed from: b */
    public static boolean m15583b(View view) {
        return (view instanceof AbsListView) || (view instanceof ScrollView) || (view instanceof ScrollingView) || (view instanceof WebView) || (view instanceof NestedScrollingChild);
    }

    /* renamed from: c */
    public static int m15584c(View view) {
        int i;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i2 = layoutParams.height;
        if (i2 > 0) {
            i = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
        } else {
            i = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(childMeasureSpec, i);
        return view.getMeasuredHeight();
    }

    public float getInterpolation(float f) {
        float b = f9751b * m15581b(f);
        return b > 0.0f ? b + f9752c : b;
    }

    /* renamed from: b */
    public static void m15582b(View view, int i) {
        if (view instanceof ScrollView) {
            ((ScrollView) view).fling(i);
        } else if (view instanceof AbsListView) {
            if (Build.VERSION.SDK_INT >= 21) {
                ((AbsListView) view).fling(i);
            }
        } else if (view instanceof WebView) {
            ((WebView) view).flingScroll(0, i);
        } else if (view instanceof NestedScrollView) {
            ((NestedScrollView) view).fling(i);
        } else if (view instanceof RecyclerView) {
            ((RecyclerView) view).fling(0, i);
        }
    }

    /* renamed from: a */
    public static boolean m15576a(View view) {
        return m15583b(view) || (view instanceof ViewPager) || (view instanceof NestedScrollingParent);
    }

    /* renamed from: a */
    public static boolean m15578a(@NonNull View view, PointF pointF) {
        if (m15577a(view, -1) && view.getVisibility() == 0) {
            return false;
        }
        if (!(view instanceof ViewGroup) || pointF == null) {
            return true;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        PointF pointF2 = new PointF();
        while (childCount > 0) {
            View childAt = viewGroup.getChildAt(childCount - 1);
            if (!m15580a(viewGroup, childAt, pointF.x, pointF.y, pointF2)) {
                childCount--;
            } else if ("fixed".equals(childAt.getTag()) || "fixed-bottom".equals(childAt.getTag())) {
                return false;
            } else {
                pointF.offset(pointF2.x, pointF2.y);
                boolean a = m15578a(childAt, pointF);
                pointF.offset(-pointF2.x, -pointF2.y);
                return a;
            }
        }
        return true;
    }

    /* renamed from: b */
    private static float m15581b(float f) {
        float f2 = f * 8.0f;
        if (f2 < 1.0f) {
            return f2 - (1.0f - ((float) Math.exp((double) (-f2))));
        }
        return ((1.0f - ((float) Math.exp((double) (1.0f - f2)))) * 0.63212055f) + 0.36787945f;
    }

    /* renamed from: a */
    public static boolean m15579a(@NonNull View view, PointF pointF, boolean z) {
        if (m15577a(view, 1) && view.getVisibility() == 0) {
            return false;
        }
        if ((view instanceof ViewGroup) && pointF != null && !m15583b(view)) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            PointF pointF2 = new PointF();
            while (childCount > 0) {
                View childAt = viewGroup.getChildAt(childCount - 1);
                if (!m15580a(viewGroup, childAt, pointF.x, pointF.y, pointF2)) {
                    childCount--;
                } else if ("fixed".equals(childAt.getTag()) || "fixed-top".equals(childAt.getTag())) {
                    return false;
                } else {
                    pointF.offset(pointF2.x, pointF2.y);
                    boolean a = m15579a(childAt, pointF, z);
                    pointF.offset(-pointF2.x, -pointF2.y);
                    return a;
                }
            }
        }
        if (z || m15577a(view, -1)) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public static boolean m15577a(@NonNull View view, int i) {
        int i2;
        if (Build.VERSION.SDK_INT >= 14) {
            return view.canScrollVertically(i);
        }
        if (view instanceof AbsListView) {
            ViewGroup viewGroup = (ViewGroup) view;
            AbsListView absListView = (AbsListView) view;
            int childCount = viewGroup.getChildCount();
            if (i > 0) {
                if (childCount <= 0 || (absListView.getLastVisiblePosition() >= (i2 = childCount - 1) && viewGroup.getChildAt(i2).getBottom() <= view.getPaddingBottom())) {
                    return false;
                }
                return true;
            } else if (childCount <= 0 || (absListView.getFirstVisiblePosition() <= 0 && viewGroup.getChildAt(0).getTop() >= view.getPaddingTop())) {
                return false;
            } else {
                return true;
            }
        } else if (i > 0) {
            if (view.getScrollY() < 0) {
                return true;
            }
            return false;
        } else if (view.getScrollY() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: a */
    public static boolean m15580a(@NonNull View view, @NonNull View view2, float f, float f2, PointF pointF) {
        if (view2.getVisibility() != 0) {
            return false;
        }
        float[] fArr = {f, f2};
        fArr[0] = fArr[0] + ((float) (view.getScrollX() - view2.getLeft()));
        fArr[1] = fArr[1] + ((float) (view.getScrollY() - view2.getTop()));
        boolean z = fArr[0] >= 0.0f && fArr[1] >= 0.0f && fArr[0] < ((float) view2.getWidth()) && fArr[1] < ((float) view2.getHeight());
        if (z && pointF != null) {
            pointF.set(fArr[0] - f, fArr[1] - f2);
        }
        return z;
    }

    /* renamed from: a */
    public static int m15574a(float f) {
        return (int) ((f * f9750a) + 0.5f);
    }

    /* renamed from: a */
    public static float m15573a(int i) {
        return ((float) i) / f9750a;
    }
}
