package com.scwang.smartrefresh.layout.p206a;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.c */
public interface DefaultRefreshInitializer {
    /* renamed from: a */
    void mo26115a(@NonNull Context context, @NonNull RefreshLayout jVar);
}
