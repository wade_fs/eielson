package com.scwang.smartrefresh.layout.p208c;

import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;

/* renamed from: com.scwang.smartrefresh.layout.c.b */
public interface OnLoadMoreListener {
    /* renamed from: b */
    void mo24430b(@NonNull RefreshLayout jVar);
}
