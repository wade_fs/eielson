package com.scwang.smartrefresh.layout.p206a;

import android.animation.ValueAnimator;
import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.p207b.RefreshState;

/* renamed from: com.scwang.smartrefresh.layout.a.i */
public interface RefreshKernel {
    /* renamed from: a */
    ValueAnimator mo26102a(int i);

    /* renamed from: a */
    RefreshKernel mo26103a();

    /* renamed from: a */
    RefreshKernel mo26104a(int i, boolean z);

    /* renamed from: a */
    RefreshKernel mo26105a(@NonNull RefreshInternal hVar, int i);

    /* renamed from: a */
    RefreshKernel mo26106a(@NonNull RefreshInternal hVar, boolean z);

    /* renamed from: a */
    RefreshKernel mo26107a(@NonNull RefreshState bVar);

    /* renamed from: a */
    RefreshKernel mo26108a(boolean z);

    /* renamed from: b */
    RefreshKernel mo26109b(int i);

    /* renamed from: b */
    RefreshKernel mo26110b(@NonNull RefreshInternal hVar, boolean z);

    @NonNull
    /* renamed from: b */
    RefreshLayout mo26111b();
}
