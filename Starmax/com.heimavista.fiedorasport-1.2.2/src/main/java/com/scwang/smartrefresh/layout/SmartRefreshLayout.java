package com.scwang.smartrefresh.layout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.NestedScrollingChildHelper;
import androidx.core.view.NestedScrollingParent;
import androidx.core.view.NestedScrollingParentHelper;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.vectordrawable.graphics.drawable.PathInterpolatorCompat;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.header.BezierRadarHeader;
import com.scwang.smartrefresh.layout.impl.RefreshContentWrapper;
import com.scwang.smartrefresh.layout.p206a.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.p206a.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.p206a.DefaultRefreshInitializer;
import com.scwang.smartrefresh.layout.p206a.RefreshContent;
import com.scwang.smartrefresh.layout.p206a.RefreshFooter;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshInternal;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p206a.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.p207b.DimensionStatus;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p208c.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.p208c.OnMultiPurposeListener;
import com.scwang.smartrefresh.layout.p208c.OnRefreshListener;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

@SuppressLint({"RestrictedApi"})
public class SmartRefreshLayout extends ViewGroup implements RefreshLayout, NestedScrollingParent {

    /* renamed from: B1 */
    protected static DefaultRefreshFooterCreator f9566B1;

    /* renamed from: C1 */
    protected static DefaultRefreshHeaderCreator f9567C1;

    /* renamed from: D1 */
    protected static DefaultRefreshInitializer f9568D1;

    /* renamed from: E1 */
    protected static ViewGroup.MarginLayoutParams f9569E1 = new ViewGroup.MarginLayoutParams(-1, -1);

    /* renamed from: A0 */
    protected boolean f9570A0;

    /* renamed from: A1 */
    protected ValueAnimator f9571A1;

    /* renamed from: B0 */
    protected boolean f9572B0;

    /* renamed from: C0 */
    protected boolean f9573C0;

    /* renamed from: D0 */
    protected boolean f9574D0;

    /* renamed from: E0 */
    protected boolean f9575E0;

    /* renamed from: F0 */
    protected boolean f9576F0;

    /* renamed from: G0 */
    protected boolean f9577G0;

    /* renamed from: H0 */
    protected boolean f9578H0;

    /* renamed from: I0 */
    protected boolean f9579I0;

    /* renamed from: J0 */
    protected boolean f9580J0;

    /* renamed from: K0 */
    protected boolean f9581K0;

    /* renamed from: L0 */
    protected boolean f9582L0;

    /* renamed from: M0 */
    protected boolean f9583M0;

    /* renamed from: N0 */
    protected boolean f9584N0;

    /* renamed from: O0 */
    protected boolean f9585O0;

    /* renamed from: P */
    protected int f9586P;

    /* renamed from: P0 */
    protected OnRefreshListener f9587P0;

    /* renamed from: Q */
    protected int f9588Q;

    /* renamed from: Q0 */
    protected OnLoadMoreListener f9589Q0;

    /* renamed from: R */
    protected int f9590R;

    /* renamed from: R0 */
    protected OnMultiPurposeListener f9591R0;

    /* renamed from: S */
    protected int f9592S;

    /* renamed from: S0 */
    protected ScrollBoundaryDecider f9593S0;

    /* renamed from: T */
    protected int f9594T;

    /* renamed from: T0 */
    protected int f9595T0;

    /* renamed from: U */
    protected int f9596U;

    /* renamed from: U0 */
    protected boolean f9597U0;

    /* renamed from: V */
    protected int f9598V;

    /* renamed from: V0 */
    protected int[] f9599V0;

    /* renamed from: W */
    protected float f9600W;

    /* renamed from: W0 */
    protected NestedScrollingChildHelper f9601W0;

    /* renamed from: X0 */
    protected NestedScrollingParentHelper f9602X0;

    /* renamed from: Y0 */
    protected int f9603Y0;

    /* renamed from: Z0 */
    protected DimensionStatus f9604Z0;

    /* renamed from: a0 */
    protected float f9605a0;

    /* renamed from: a1 */
    protected int f9606a1;

    /* renamed from: b0 */
    protected float f9607b0;

    /* renamed from: b1 */
    protected DimensionStatus f9608b1;

    /* renamed from: c0 */
    protected float f9609c0;

    /* renamed from: c1 */
    protected int f9610c1;

    /* renamed from: d0 */
    protected float f9611d0;

    /* renamed from: d1 */
    protected int f9612d1;

    /* renamed from: e0 */
    protected char f9613e0;

    /* renamed from: e1 */
    protected float f9614e1;

    /* renamed from: f0 */
    protected boolean f9615f0;

    /* renamed from: f1 */
    protected float f9616f1;

    /* renamed from: g0 */
    protected boolean f9617g0;

    /* renamed from: g1 */
    protected float f9618g1;

    /* renamed from: h0 */
    protected int f9619h0;

    /* renamed from: h1 */
    protected float f9620h1;

    /* renamed from: i0 */
    protected int f9621i0;

    /* renamed from: i1 */
    protected RefreshInternal f9622i1;

    /* renamed from: j0 */
    protected int f9623j0;

    /* renamed from: j1 */
    protected RefreshInternal f9624j1;

    /* renamed from: k0 */
    protected int f9625k0;

    /* renamed from: k1 */
    protected RefreshContent f9626k1;

    /* renamed from: l0 */
    protected int f9627l0;

    /* renamed from: l1 */
    protected Paint f9628l1;

    /* renamed from: m0 */
    protected int f9629m0;

    /* renamed from: m1 */
    protected Handler f9630m1;

    /* renamed from: n0 */
    protected int f9631n0;

    /* renamed from: n1 */
    protected RefreshKernel f9632n1;

    /* renamed from: o0 */
    protected Scroller f9633o0;

    /* renamed from: o1 */
    protected RefreshState f9634o1;

    /* renamed from: p0 */
    protected VelocityTracker f9635p0;

    /* renamed from: p1 */
    protected RefreshState f9636p1;

    /* renamed from: q0 */
    protected Interpolator f9637q0;

    /* renamed from: q1 */
    protected long f9638q1;

    /* renamed from: r0 */
    protected int[] f9639r0;

    /* renamed from: r1 */
    protected int f9640r1;

    /* renamed from: s0 */
    protected boolean f9641s0;

    /* renamed from: s1 */
    protected int f9642s1;

    /* renamed from: t0 */
    protected boolean f9643t0;

    /* renamed from: t1 */
    protected boolean f9644t1;

    /* renamed from: u0 */
    protected boolean f9645u0;

    /* renamed from: u1 */
    protected boolean f9646u1;

    /* renamed from: v0 */
    protected boolean f9647v0;

    /* renamed from: v1 */
    protected boolean f9648v1;

    /* renamed from: w0 */
    protected boolean f9649w0;

    /* renamed from: w1 */
    protected boolean f9650w1;

    /* renamed from: x0 */
    protected boolean f9651x0;

    /* renamed from: x1 */
    protected boolean f9652x1;

    /* renamed from: y0 */
    protected boolean f9653y0;

    /* renamed from: y1 */
    protected MotionEvent f9654y1;

    /* renamed from: z0 */
    protected boolean f9655z0;

    /* renamed from: z1 */
    protected Runnable f9656z1;

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$a */
    static /* synthetic */ class C4690a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9657a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(36:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a = r0
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpToLoad     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownCanceled     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpCanceled     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToRefresh     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToLoad     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToTwoLevel     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x006e }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.RefreshReleased     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x007a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadReleased     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0086 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x0092 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading     // Catch:{ NoSuchFieldError -> 0x0092 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0092 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0092 }
            L_0x0092:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x009e }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.RefreshFinish     // Catch:{ NoSuchFieldError -> 0x009e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009e }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009e }
            L_0x009e:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x00aa }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadFinish     // Catch:{ NoSuchFieldError -> 0x00aa }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00aa }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00aa }
            L_0x00aa:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x00b6 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevelReleased     // Catch:{ NoSuchFieldError -> 0x00b6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b6 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b6 }
            L_0x00b6:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x00c2 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevelFinish     // Catch:{ NoSuchFieldError -> 0x00c2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c2 }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00c2 }
            L_0x00c2:
                int[] r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.f9657a     // Catch:{ NoSuchFieldError -> 0x00ce }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevel     // Catch:{ NoSuchFieldError -> 0x00ce }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ce }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ce }
            L_0x00ce:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.C4690a.<clinit>():void");
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$b */
    class C4691b extends AnimatorListenerAdapter {

        /* renamed from: P */
        final /* synthetic */ boolean f9658P;

        C4691b(boolean z) {
            this.f9658P = z;
        }

        public void onAnimationEnd(Animator animator) {
            SmartRefreshLayout.this.setStateDirectLoading(this.f9658P);
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$c */
    class C4692c extends AnimatorListenerAdapter {

        /* renamed from: P */
        final /* synthetic */ boolean f9660P;

        C4692c(boolean z) {
            this.f9660P = z;
        }

        public void onAnimationEnd(Animator animator) {
            SmartRefreshLayout.this.f9638q1 = System.currentTimeMillis();
            SmartRefreshLayout.this.mo26050a(RefreshState.Refreshing);
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            OnRefreshListener dVar = smartRefreshLayout.f9587P0;
            if (dVar != null) {
                if (this.f9660P) {
                    dVar.mo24707a(smartRefreshLayout);
                }
            } else if (smartRefreshLayout.f9591R0 == null) {
                smartRefreshLayout.mo26053b((int) PathInterpolatorCompat.MAX_NUM_POINTS);
            }
            SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
            RefreshInternal hVar = smartRefreshLayout2.f9622i1;
            if (hVar != null) {
                int i = smartRefreshLayout2.f9603Y0;
                hVar.mo26129a(smartRefreshLayout2, i, (int) (smartRefreshLayout2.f9614e1 * ((float) i)));
            }
            SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
            OnMultiPurposeListener cVar = smartRefreshLayout3.f9591R0;
            if (cVar != null && (smartRefreshLayout3.f9622i1 instanceof RefreshHeader)) {
                if (this.f9660P) {
                    cVar.mo24707a(smartRefreshLayout3);
                }
                SmartRefreshLayout smartRefreshLayout4 = SmartRefreshLayout.this;
                int i2 = smartRefreshLayout4.f9603Y0;
                smartRefreshLayout4.f9591R0.mo26148b((RefreshHeader) smartRefreshLayout4.f9622i1, i2, (int) (smartRefreshLayout4.f9614e1 * ((float) i2)));
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$d */
    class C4693d extends AnimatorListenerAdapter {
        C4693d() {
        }

        public void onAnimationEnd(Animator animator) {
            RefreshState bVar;
            RefreshState bVar2;
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            smartRefreshLayout.f9571A1 = null;
            if (smartRefreshLayout.f9588Q != 0 || (bVar = smartRefreshLayout.f9634o1) == (bVar2 = RefreshState.None) || bVar.f9739T || bVar.f9738S) {
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                RefreshState bVar3 = smartRefreshLayout2.f9634o1;
                if (bVar3 != smartRefreshLayout2.f9636p1) {
                    smartRefreshLayout2.setViceState(bVar3);
                    return;
                }
                return;
            }
            smartRefreshLayout.mo26050a(bVar2);
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$e */
    class C4694e implements ValueAnimator.AnimatorUpdateListener {
        C4694e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
         arg types: [int, int]
         candidates:
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            SmartRefreshLayout.this.f9632n1.mo26104a(((Integer) valueAnimator.getAnimatedValue()).intValue(), false);
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$f */
    class C4695f implements Runnable {
        C4695f() {
        }

        public void run() {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            OnLoadMoreListener bVar = smartRefreshLayout.f9589Q0;
            if (bVar != null) {
                bVar.mo24430b(smartRefreshLayout);
            } else if (smartRefreshLayout.f9591R0 == null) {
                smartRefreshLayout.mo26040a(2000);
            }
            SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
            OnMultiPurposeListener cVar = smartRefreshLayout2.f9591R0;
            if (cVar != null) {
                cVar.mo24430b(smartRefreshLayout2);
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$g */
    class C4696g implements Runnable {

        /* renamed from: P */
        int f9665P = 0;

        /* renamed from: Q */
        final /* synthetic */ int f9666Q;

        /* renamed from: R */
        final /* synthetic */ Boolean f9667R;

        /* renamed from: S */
        final /* synthetic */ boolean f9668S;

        C4696g(int i, Boolean bool, boolean z) {
            this.f9666Q = i;
            this.f9667R = bool;
            this.f9668S = z;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
         arg types: [int, int]
         candidates:
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
        public void run() {
            ValueAnimator.AnimatorUpdateListener animatorUpdateListener = null;
            boolean z = false;
            if (this.f9665P == 0) {
                SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                if (smartRefreshLayout.f9634o1 == RefreshState.None && smartRefreshLayout.f9636p1 == RefreshState.Refreshing) {
                    smartRefreshLayout.f9636p1 = RefreshState.None;
                    return;
                }
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                if (smartRefreshLayout2.f9571A1 != null) {
                    RefreshState bVar = smartRefreshLayout2.f9634o1;
                    if (bVar.f9735P && (bVar.f9738S || bVar == RefreshState.RefreshReleased)) {
                        SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
                        ValueAnimator valueAnimator = smartRefreshLayout3.f9571A1;
                        smartRefreshLayout3.f9571A1 = null;
                        valueAnimator.cancel();
                        SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.None);
                        return;
                    }
                }
                SmartRefreshLayout smartRefreshLayout4 = SmartRefreshLayout.this;
                if (smartRefreshLayout4.f9634o1 == RefreshState.Refreshing && smartRefreshLayout4.f9622i1 != null && smartRefreshLayout4.f9626k1 != null) {
                    this.f9665P++;
                    smartRefreshLayout4.f9630m1.postDelayed(this, (long) this.f9666Q);
                    SmartRefreshLayout.this.mo26050a(RefreshState.RefreshFinish);
                    Boolean bool = this.f9667R;
                    if (bool != null) {
                        SmartRefreshLayout smartRefreshLayout5 = SmartRefreshLayout.this;
                        if (bool == Boolean.TRUE) {
                            z = true;
                        }
                        smartRefreshLayout5.mo26070g(z);
                        return;
                    }
                    return;
                }
                return;
            }
            SmartRefreshLayout smartRefreshLayout6 = SmartRefreshLayout.this;
            int a = smartRefreshLayout6.f9622i1.mo24847a(smartRefreshLayout6, this.f9668S);
            SmartRefreshLayout smartRefreshLayout7 = SmartRefreshLayout.this;
            OnMultiPurposeListener cVar = smartRefreshLayout7.f9591R0;
            if (cVar != null) {
                RefreshInternal hVar = smartRefreshLayout7.f9622i1;
                if (hVar instanceof RefreshHeader) {
                    cVar.mo26145a((RefreshHeader) hVar, this.f9668S);
                }
            }
            if (a < Integer.MAX_VALUE) {
                SmartRefreshLayout smartRefreshLayout8 = SmartRefreshLayout.this;
                if (smartRefreshLayout8.f9615f0 || smartRefreshLayout8.f9597U0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    SmartRefreshLayout smartRefreshLayout9 = SmartRefreshLayout.this;
                    if (smartRefreshLayout9.f9615f0) {
                        float f = smartRefreshLayout9.f9609c0;
                        smartRefreshLayout9.f9605a0 = f;
                        smartRefreshLayout9.f9592S = 0;
                        smartRefreshLayout9.f9615f0 = false;
                        long j = currentTimeMillis;
                        boolean unused = SmartRefreshLayout.super.dispatchTouchEvent(MotionEvent.obtain(currentTimeMillis, j, 0, smartRefreshLayout9.f9607b0, (f + ((float) smartRefreshLayout9.f9588Q)) - ((float) (smartRefreshLayout9.f9586P * 2)), 0));
                        SmartRefreshLayout smartRefreshLayout10 = SmartRefreshLayout.this;
                        boolean unused2 = SmartRefreshLayout.super.dispatchTouchEvent(MotionEvent.obtain(currentTimeMillis, j, 2, smartRefreshLayout10.f9607b0, smartRefreshLayout10.f9609c0 + ((float) smartRefreshLayout10.f9588Q), 0));
                    }
                    SmartRefreshLayout smartRefreshLayout11 = SmartRefreshLayout.this;
                    if (smartRefreshLayout11.f9597U0) {
                        smartRefreshLayout11.f9595T0 = 0;
                        boolean unused3 = SmartRefreshLayout.super.dispatchTouchEvent(MotionEvent.obtain(currentTimeMillis, currentTimeMillis, 1, smartRefreshLayout11.f9607b0, smartRefreshLayout11.f9609c0, 0));
                        SmartRefreshLayout smartRefreshLayout12 = SmartRefreshLayout.this;
                        smartRefreshLayout12.f9597U0 = false;
                        smartRefreshLayout12.f9592S = 0;
                    }
                }
                SmartRefreshLayout smartRefreshLayout13 = SmartRefreshLayout.this;
                int i = smartRefreshLayout13.f9588Q;
                if (i > 0) {
                    ValueAnimator a2 = smartRefreshLayout13.mo26037a(0, a, smartRefreshLayout13.f9637q0, smartRefreshLayout13.f9596U);
                    SmartRefreshLayout smartRefreshLayout14 = SmartRefreshLayout.this;
                    if (smartRefreshLayout14.f9576F0) {
                        animatorUpdateListener = smartRefreshLayout14.f9626k1.mo26117a(smartRefreshLayout14.f9588Q);
                    }
                    if (a2 != null && animatorUpdateListener != null) {
                        a2.addUpdateListener(animatorUpdateListener);
                    }
                } else if (i < 0) {
                    smartRefreshLayout13.mo26037a(0, a, smartRefreshLayout13.f9637q0, smartRefreshLayout13.f9596U);
                } else {
                    smartRefreshLayout13.f9632n1.mo26104a(0, false);
                    SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.None);
                }
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$h */
    class C4697h implements Runnable {

        /* renamed from: P */
        int f9670P = 0;

        /* renamed from: Q */
        final /* synthetic */ int f9671Q;

        /* renamed from: R */
        final /* synthetic */ boolean f9672R;

        /* renamed from: S */
        final /* synthetic */ boolean f9673S;

        /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$h$a */
        class C4698a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ int f9675P;

            /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$h$a$a */
            class C4699a extends AnimatorListenerAdapter {
                C4699a() {
                }

                public void onAnimationEnd(Animator animator) {
                    C4697h hVar = C4697h.this;
                    SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                    smartRefreshLayout.f9650w1 = false;
                    if (hVar.f9672R) {
                        smartRefreshLayout.mo26070g(true);
                    }
                    SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                    if (smartRefreshLayout2.f9634o1 == RefreshState.LoadFinish) {
                        smartRefreshLayout2.mo26050a(RefreshState.None);
                    }
                }
            }

            C4698a(int i) {
                this.f9675P = i;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
             arg types: [int, int]
             candidates:
              com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
              com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
              com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
            public void run() {
                ValueAnimator valueAnimator;
                SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                ValueAnimator.AnimatorUpdateListener a = (!smartRefreshLayout.f9575E0 || this.f9675P >= 0) ? null : smartRefreshLayout.f9626k1.mo26117a(smartRefreshLayout.f9588Q);
                if (a != null) {
                    a.onAnimationUpdate(ValueAnimator.ofInt(0, 0));
                }
                C4699a aVar = new C4699a();
                C4697h hVar = C4697h.this;
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                int i = smartRefreshLayout2.f9588Q;
                if (i > 0) {
                    valueAnimator = smartRefreshLayout2.f9632n1.mo26102a(0);
                } else {
                    if (a != null || i == 0) {
                        ValueAnimator valueAnimator2 = SmartRefreshLayout.this.f9571A1;
                        if (valueAnimator2 != null) {
                            valueAnimator2.cancel();
                            SmartRefreshLayout.this.f9571A1 = null;
                        }
                        SmartRefreshLayout.this.f9632n1.mo26104a(0, false);
                        SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.None);
                    } else if (!hVar.f9672R || !smartRefreshLayout2.f9653y0) {
                        valueAnimator = SmartRefreshLayout.this.f9632n1.mo26102a(0);
                    } else {
                        int i2 = smartRefreshLayout2.f9606a1;
                        if (i >= (-i2)) {
                            smartRefreshLayout2.mo26050a(RefreshState.None);
                        } else {
                            valueAnimator = smartRefreshLayout2.f9632n1.mo26102a(-i2);
                        }
                    }
                    valueAnimator = null;
                }
                if (valueAnimator != null) {
                    valueAnimator.addListener(aVar);
                } else {
                    aVar.onAnimationEnd(null);
                }
            }
        }

        C4697h(int i, boolean z, boolean z2) {
            this.f9671Q = i;
            this.f9672R = z;
            this.f9673S = z2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a9, code lost:
            if (r2.f9626k1.mo26124b() != false) goto L_0x00ad;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r15 = this;
                int r0 = r15.f9670P
                r1 = 1
                if (r0 != 0) goto L_0x0072
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r0.f9634o1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.None
                if (r2 != r3) goto L_0x0018
                com.scwang.smartrefresh.layout.b.b r2 = r0.f9636p1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading
                if (r2 != r3) goto L_0x0018
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.None
                r0.f9636p1 = r2
                goto L_0x0067
            L_0x0018:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                android.animation.ValueAnimator r2 = r0.f9571A1
                if (r2 == 0) goto L_0x0042
                com.scwang.smartrefresh.layout.b.b r0 = r0.f9634o1
                boolean r2 = r0.f9738S
                if (r2 != 0) goto L_0x0028
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadReleased
                if (r0 != r2) goto L_0x0042
            L_0x0028:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r0.f9634o1
                boolean r2 = r2.f9736Q
                if (r2 == 0) goto L_0x0042
                android.animation.ValueAnimator r2 = r0.f9571A1
                r3 = 0
                r0.f9571A1 = r3
                r2.cancel()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.i r0 = r0.f9632n1
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.None
                r0.mo26107a(r2)
                goto L_0x0067
            L_0x0042:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r0.f9634o1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading
                if (r2 != r3) goto L_0x0067
                com.scwang.smartrefresh.layout.a.h r2 = r0.f9624j1
                if (r2 == 0) goto L_0x0067
                com.scwang.smartrefresh.layout.a.e r2 = r0.f9626k1
                if (r2 == 0) goto L_0x0067
                int r2 = r15.f9670P
                int r2 = r2 + r1
                r15.f9670P = r2
                android.os.Handler r0 = r0.f9630m1
                int r1 = r15.f9671Q
                long r1 = (long) r1
                r0.postDelayed(r15, r1)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadFinish
                r0.mo26050a(r1)
                return
            L_0x0067:
                boolean r0 = r15.f9672R
                if (r0 == 0) goto L_0x0142
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                r0.mo26070g(r1)
                goto L_0x0142
            L_0x0072:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r0.f9624j1
                boolean r3 = r15.f9673S
                int r0 = r2.mo24847a(r0, r3)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.c.c r3 = r2.f9591R0
                if (r3 == 0) goto L_0x008f
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9624j1
                boolean r4 = r2 instanceof com.scwang.smartrefresh.layout.p206a.RefreshFooter
                if (r4 == 0) goto L_0x008f
                com.scwang.smartrefresh.layout.a.f r2 = (com.scwang.smartrefresh.layout.p206a.RefreshFooter) r2
                boolean r4 = r15.f9673S
                r3.mo26142a(r2, r4)
            L_0x008f:
                r2 = 2147483647(0x7fffffff, float:NaN)
                if (r0 >= r2) goto L_0x0142
                boolean r2 = r15.f9672R
                r3 = 0
                if (r2 == 0) goto L_0x00ac
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r2.f9653y0
                if (r4 == 0) goto L_0x00ac
                int r4 = r2.f9588Q
                if (r4 >= 0) goto L_0x00ac
                com.scwang.smartrefresh.layout.a.e r2 = r2.f9626k1
                boolean r2 = r2.mo26124b()
                if (r2 == 0) goto L_0x00ac
                goto L_0x00ad
            L_0x00ac:
                r1 = 0
            L_0x00ad:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r4 = r2.f9588Q
                if (r1 == 0) goto L_0x00bb
                int r1 = r2.f9606a1
                int r1 = -r1
                int r1 = java.lang.Math.max(r4, r1)
                goto L_0x00bc
            L_0x00bb:
                r1 = 0
            L_0x00bc:
                int r1 = r4 - r1
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r2.f9615f0
                if (r4 != 0) goto L_0x00c8
                boolean r2 = r2.f9597U0
                if (r2 == 0) goto L_0x012c
            L_0x00c8:
                long r12 = java.lang.System.currentTimeMillis()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r2.f9615f0
                if (r4 == 0) goto L_0x010f
                float r4 = r2.f9609c0
                r2.f9605a0 = r4
                int r4 = r2.f9588Q
                int r4 = r4 - r1
                r2.f9592S = r4
                r2.f9615f0 = r3
                boolean r2 = r2.f9651x0
                if (r2 == 0) goto L_0x00e3
                r2 = r1
                goto L_0x00e4
            L_0x00e3:
                r2 = 0
            L_0x00e4:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r14 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                r8 = 0
                float r9 = r14.f9607b0
                float r4 = r14.f9609c0
                float r2 = (float) r2
                float r4 = r4 + r2
                int r5 = r14.f9586P
                int r5 = r5 * 2
                float r5 = (float) r5
                float r10 = r4 + r5
                r11 = 0
                r4 = r12
                r6 = r12
                android.view.MotionEvent r4 = android.view.MotionEvent.obtain(r4, r6, r8, r9, r10, r11)
                boolean unused = com.scwang.smartrefresh.layout.SmartRefreshLayout.super.dispatchTouchEvent(r4)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r14 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                r8 = 2
                float r9 = r14.f9607b0
                float r4 = r14.f9609c0
                float r10 = r4 + r2
                r4 = r12
                android.view.MotionEvent r2 = android.view.MotionEvent.obtain(r4, r6, r8, r9, r10, r11)
                boolean unused = com.scwang.smartrefresh.layout.SmartRefreshLayout.super.dispatchTouchEvent(r2)
            L_0x010f:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r2.f9597U0
                if (r4 == 0) goto L_0x012c
                r2.f9595T0 = r3
                r8 = 1
                float r9 = r2.f9607b0
                float r10 = r2.f9609c0
                r11 = 0
                r4 = r12
                r6 = r12
                android.view.MotionEvent r4 = android.view.MotionEvent.obtain(r4, r6, r8, r9, r10, r11)
                boolean unused = com.scwang.smartrefresh.layout.SmartRefreshLayout.super.dispatchTouchEvent(r4)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                r2.f9597U0 = r3
                r2.f9592S = r3
            L_0x012c:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                android.os.Handler r2 = r2.f9630m1
                com.scwang.smartrefresh.layout.SmartRefreshLayout$h$a r3 = new com.scwang.smartrefresh.layout.SmartRefreshLayout$h$a
                r3.<init>(r1)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r1 = r1.f9588Q
                if (r1 >= 0) goto L_0x013d
                long r0 = (long) r0
                goto L_0x013f
            L_0x013d:
                r0 = 0
            L_0x013f:
                r2.postDelayed(r3, r0)
            L_0x0142:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.C4697h.run():void");
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$i */
    class C4700i implements Runnable {

        /* renamed from: P */
        final /* synthetic */ float f9678P;

        /* renamed from: Q */
        final /* synthetic */ int f9679Q;

        /* renamed from: R */
        final /* synthetic */ boolean f9680R;

        /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$i$a */
        class C4701a implements ValueAnimator.AnimatorUpdateListener {
            C4701a() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
             arg types: [int, int]
             candidates:
              com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
              com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
              com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                if (smartRefreshLayout.f9571A1 != null) {
                    smartRefreshLayout.f9632n1.mo26104a(((Integer) valueAnimator.getAnimatedValue()).intValue(), true);
                }
            }
        }

        /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$i$b */
        class C4702b extends AnimatorListenerAdapter {
            C4702b() {
            }

            public void onAnimationEnd(Animator animator) {
                SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                if (smartRefreshLayout.f9571A1 != null) {
                    smartRefreshLayout.f9571A1 = null;
                    RefreshState bVar = smartRefreshLayout.f9634o1;
                    RefreshState bVar2 = RefreshState.ReleaseToRefresh;
                    if (bVar != bVar2) {
                        smartRefreshLayout.f9632n1.mo26107a(bVar2);
                    }
                    C4700i iVar = C4700i.this;
                    SmartRefreshLayout.this.setStateRefreshing(!iVar.f9680R);
                }
            }
        }

        C4700i(float f, int i, boolean z) {
            this.f9678P = f;
            this.f9679Q = i;
            this.f9680R = z;
        }

        public void run() {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            if (smartRefreshLayout.f9636p1 == RefreshState.Refreshing) {
                ValueAnimator valueAnimator = smartRefreshLayout.f9571A1;
                if (valueAnimator != null) {
                    valueAnimator.cancel();
                }
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                smartRefreshLayout2.f9607b0 = ((float) smartRefreshLayout2.getMeasuredWidth()) / 2.0f;
                SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.PullDownToRefresh);
                SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
                smartRefreshLayout3.f9571A1 = ValueAnimator.ofInt(smartRefreshLayout3.f9588Q, (int) (((float) smartRefreshLayout3.f9603Y0) * this.f9678P));
                SmartRefreshLayout.this.f9571A1.setDuration((long) this.f9679Q);
                SmartRefreshLayout.this.f9571A1.setInterpolator(new SmartUtil());
                SmartRefreshLayout.this.f9571A1.addUpdateListener(new C4701a());
                SmartRefreshLayout.this.f9571A1.addListener(new C4702b());
                SmartRefreshLayout.this.f9571A1.start();
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$j */
    protected class C4703j implements Runnable {

        /* renamed from: P */
        int f9684P = 0;

        /* renamed from: Q */
        int f9685Q = 10;

        /* renamed from: R */
        int f9686R;

        /* renamed from: S */
        long f9687S;

        /* renamed from: T */
        float f9688T = 0.0f;

        /* renamed from: U */
        float f9689U;

        C4703j(float f, int i) {
            this.f9689U = f;
            this.f9686R = i;
            this.f9687S = AnimationUtils.currentAnimationTimeMillis();
            SmartRefreshLayout.this.f9630m1.postDelayed(this, (long) this.f9685Q);
            if (f > 0.0f) {
                SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.PullDownToRefresh);
            } else {
                SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.PullUpToLoad);
            }
        }

        public void run() {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            if (smartRefreshLayout.f9656z1 == this && !smartRefreshLayout.f9634o1.f9740U) {
                if (Math.abs(smartRefreshLayout.f9588Q) < Math.abs(this.f9686R)) {
                    int i = this.f9684P + 1;
                    this.f9684P = i;
                    this.f9689U = (float) (((double) this.f9689U) * Math.pow(0.949999988079071d, (double) (i * 2)));
                } else if (this.f9686R != 0) {
                    int i2 = this.f9684P + 1;
                    this.f9684P = i2;
                    this.f9689U = (float) (((double) this.f9689U) * Math.pow(0.44999998807907104d, (double) (i2 * 2)));
                } else {
                    int i3 = this.f9684P + 1;
                    this.f9684P = i3;
                    this.f9689U = (float) (((double) this.f9689U) * Math.pow(0.8500000238418579d, (double) (i3 * 2)));
                }
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float f = this.f9689U * ((((float) (currentAnimationTimeMillis - this.f9687S)) * 1.0f) / 1000.0f);
                if (Math.abs(f) >= 1.0f) {
                    this.f9687S = currentAnimationTimeMillis;
                    this.f9688T += f;
                    SmartRefreshLayout.this.mo26059c(this.f9688T);
                    SmartRefreshLayout.this.f9630m1.postDelayed(this, (long) this.f9685Q);
                    return;
                }
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                RefreshState bVar = smartRefreshLayout2.f9636p1;
                if (!bVar.f9738S || !bVar.f9735P) {
                    SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
                    RefreshState bVar2 = smartRefreshLayout3.f9636p1;
                    if (bVar2.f9738S && bVar2.f9736Q) {
                        smartRefreshLayout3.f9632n1.mo26107a(RefreshState.PullUpCanceled);
                    }
                } else {
                    smartRefreshLayout2.f9632n1.mo26107a(RefreshState.PullDownCanceled);
                }
                SmartRefreshLayout smartRefreshLayout4 = SmartRefreshLayout.this;
                smartRefreshLayout4.f9656z1 = null;
                if (Math.abs(smartRefreshLayout4.f9588Q) >= Math.abs(this.f9686R)) {
                    SmartRefreshLayout smartRefreshLayout5 = SmartRefreshLayout.this;
                    smartRefreshLayout5.mo26037a(this.f9686R, 0, smartRefreshLayout5.f9637q0, Math.min(Math.max((int) SmartUtil.m15573a(Math.abs(SmartRefreshLayout.this.f9588Q - this.f9686R)), 30), 100) * 10);
                }
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$k */
    protected class C4704k implements Runnable {

        /* renamed from: P */
        int f9691P;

        /* renamed from: Q */
        int f9692Q = 10;

        /* renamed from: R */
        float f9693R;

        /* renamed from: S */
        float f9694S = 0.98f;

        /* renamed from: T */
        long f9695T = 0;

        /* renamed from: U */
        long f9696U = AnimationUtils.currentAnimationTimeMillis();

        C4704k(float f) {
            this.f9693R = f;
            this.f9691P = SmartRefreshLayout.this.f9588Q;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0049, code lost:
            if (r0.f9588Q >= (-r0.f9606a1)) goto L_0x004b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0057, code lost:
            if (r0.f9588Q > r0.f9603Y0) goto L_0x0059;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ab, code lost:
            if (r0 < (-r1.f9606a1)) goto L_0x00ad;
         */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Runnable mo26100a() {
            /*
                r11 = this;
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r1 = r0.f9634o1
                boolean r2 = r1.f9740U
                r3 = 0
                if (r2 == 0) goto L_0x000a
                return r3
            L_0x000a:
                int r2 = r0.f9588Q
                if (r2 == 0) goto L_0x00b2
                boolean r1 = r1.f9739T
                if (r1 != 0) goto L_0x0026
                boolean r1 = r0.f9581K0
                if (r1 == 0) goto L_0x0059
                boolean r1 = r0.f9653y0
                if (r1 == 0) goto L_0x0059
                boolean r1 = r0.f9582L0
                if (r1 == 0) goto L_0x0059
                boolean r1 = r0.f9643t0
                boolean r0 = r0.mo26068e(r1)
                if (r0 == 0) goto L_0x0059
            L_0x0026:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r1 = r0.f9634o1
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading
                if (r1 == r2) goto L_0x0042
                boolean r1 = r0.f9581K0
                if (r1 == 0) goto L_0x004b
                boolean r1 = r0.f9653y0
                if (r1 == 0) goto L_0x004b
                boolean r1 = r0.f9582L0
                if (r1 == 0) goto L_0x004b
                boolean r1 = r0.f9643t0
                boolean r0 = r0.mo26068e(r1)
                if (r0 == 0) goto L_0x004b
            L_0x0042:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r1 = r0.f9588Q
                int r0 = r0.f9606a1
                int r0 = -r0
                if (r1 < r0) goto L_0x0059
            L_0x004b:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r1 = r0.f9634o1
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing
                if (r1 != r2) goto L_0x00b2
                int r1 = r0.f9588Q
                int r0 = r0.f9603Y0
                if (r1 <= r0) goto L_0x00b2
            L_0x0059:
                r0 = 0
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r1 = r1.f9588Q
                float r2 = r11.f9693R
                r0 = r1
                r4 = 0
            L_0x0062:
                int r5 = r1 * r0
                if (r5 <= 0) goto L_0x00b2
                double r5 = (double) r2
                float r2 = r11.f9694S
                double r7 = (double) r2
                int r4 = r4 + 1
                int r2 = r11.f9692Q
                int r2 = r2 * r4
                float r2 = (float) r2
                r9 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 / r9
                double r9 = (double) r2
                double r7 = java.lang.Math.pow(r7, r9)
                double r5 = r5 * r7
                float r2 = (float) r5
                int r5 = r11.f9692Q
                float r5 = (float) r5
                r6 = 1065353216(0x3f800000, float:1.0)
                float r5 = r5 * r6
                r7 = 1148846080(0x447a0000, float:1000.0)
                float r5 = r5 / r7
                float r5 = r5 * r2
                float r7 = java.lang.Math.abs(r5)
                int r6 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
                if (r6 >= 0) goto L_0x00ae
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r1.f9634o1
                boolean r4 = r2.f9739T
                if (r4 == 0) goto L_0x00ad
                com.scwang.smartrefresh.layout.b.b r4 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing
                if (r2 != r4) goto L_0x00a0
                int r1 = r1.f9603Y0
                if (r0 > r1) goto L_0x00ad
            L_0x00a0:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r1.f9634o1
                com.scwang.smartrefresh.layout.b.b r4 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing
                if (r2 == r4) goto L_0x00b2
                int r1 = r1.f9606a1
                int r1 = -r1
                if (r0 >= r1) goto L_0x00b2
            L_0x00ad:
                return r3
            L_0x00ae:
                float r0 = (float) r0
                float r0 = r0 + r5
                int r0 = (int) r0
                goto L_0x0062
            L_0x00b2:
                long r0 = android.view.animation.AnimationUtils.currentAnimationTimeMillis()
                r11.f9695T = r0
                com.scwang.smartrefresh.layout.SmartRefreshLayout r0 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                android.os.Handler r0 = r0.f9630m1
                int r1 = r11.f9692Q
                long r1 = (long) r1
                r0.postDelayed(r11, r1)
                return r11
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.C4704k.mo26100a():java.lang.Runnable");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
         arg types: [int, int]
         candidates:
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
        public void run() {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            if (smartRefreshLayout.f9656z1 == this && !smartRefreshLayout.f9634o1.f9740U) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                this.f9693R = (float) (((double) this.f9693R) * Math.pow((double) this.f9694S, (double) (((float) (currentAnimationTimeMillis - this.f9695T)) / (1000.0f / ((float) this.f9692Q)))));
                float f = this.f9693R * ((((float) (currentAnimationTimeMillis - this.f9696U)) * 1.0f) / 1000.0f);
                if (Math.abs(f) > 1.0f) {
                    this.f9696U = currentAnimationTimeMillis;
                    this.f9691P = (int) (((float) this.f9691P) + f);
                    SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                    int i = smartRefreshLayout2.f9588Q;
                    int i2 = this.f9691P;
                    if (i * i2 > 0) {
                        smartRefreshLayout2.f9632n1.mo26104a(i2, true);
                        SmartRefreshLayout.this.f9630m1.postDelayed(this, (long) this.f9692Q);
                        return;
                    }
                    smartRefreshLayout2.f9656z1 = null;
                    smartRefreshLayout2.f9632n1.mo26104a(0, true);
                    SmartUtil.m15582b(SmartRefreshLayout.this.f9626k1.mo26125c(), (int) (-this.f9693R));
                    SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
                    if (smartRefreshLayout3.f9650w1 && f > 0.0f) {
                        smartRefreshLayout3.f9650w1 = false;
                        return;
                    }
                    return;
                }
                SmartRefreshLayout.this.f9656z1 = null;
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$m */
    public class C4706m implements RefreshKernel {

        /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$m$a */
        class C4707a extends AnimatorListenerAdapter {
            C4707a() {
            }

            public void onAnimationEnd(Animator animator) {
                SmartRefreshLayout.this.f9632n1.mo26107a(RefreshState.TwoLevel);
            }
        }

        public C4706m() {
        }

        /* renamed from: a */
        public RefreshKernel mo26107a(@NonNull RefreshState bVar) {
            switch (C4690a.f9657a[bVar.ordinal()]) {
                case 1:
                    SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                    RefreshState bVar2 = smartRefreshLayout.f9634o1;
                    RefreshState bVar3 = RefreshState.None;
                    if (bVar2 != bVar3 && smartRefreshLayout.f9588Q == 0) {
                        smartRefreshLayout.mo26050a(bVar3);
                        return null;
                    } else if (SmartRefreshLayout.this.f9588Q == 0) {
                        return null;
                    } else {
                        mo26102a(0);
                        return null;
                    }
                case 2:
                    SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                    if (smartRefreshLayout2.f9634o1.f9739T || !smartRefreshLayout2.mo26068e(smartRefreshLayout2.f9641s0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.PullDownToRefresh);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.PullDownToRefresh);
                    return null;
                case 3:
                    SmartRefreshLayout smartRefreshLayout3 = SmartRefreshLayout.this;
                    if (smartRefreshLayout3.mo26068e(smartRefreshLayout3.f9643t0)) {
                        SmartRefreshLayout smartRefreshLayout4 = SmartRefreshLayout.this;
                        RefreshState bVar4 = smartRefreshLayout4.f9634o1;
                        if (!bVar4.f9739T && !bVar4.f9740U && (!smartRefreshLayout4.f9581K0 || !smartRefreshLayout4.f9653y0 || !smartRefreshLayout4.f9582L0)) {
                            SmartRefreshLayout.this.mo26050a(RefreshState.PullUpToLoad);
                            return null;
                        }
                    }
                    SmartRefreshLayout.this.setViceState(RefreshState.PullUpToLoad);
                    return null;
                case 4:
                    SmartRefreshLayout smartRefreshLayout5 = SmartRefreshLayout.this;
                    if (smartRefreshLayout5.f9634o1.f9739T || !smartRefreshLayout5.mo26068e(smartRefreshLayout5.f9641s0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.PullDownCanceled);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.PullDownCanceled);
                    mo26107a(RefreshState.None);
                    return null;
                case 5:
                    SmartRefreshLayout smartRefreshLayout6 = SmartRefreshLayout.this;
                    if (smartRefreshLayout6.mo26068e(smartRefreshLayout6.f9643t0)) {
                        SmartRefreshLayout smartRefreshLayout7 = SmartRefreshLayout.this;
                        if (!smartRefreshLayout7.f9634o1.f9739T && (!smartRefreshLayout7.f9581K0 || !smartRefreshLayout7.f9653y0 || !smartRefreshLayout7.f9582L0)) {
                            SmartRefreshLayout.this.mo26050a(RefreshState.PullUpCanceled);
                            mo26107a(RefreshState.None);
                            return null;
                        }
                    }
                    SmartRefreshLayout.this.setViceState(RefreshState.PullUpCanceled);
                    return null;
                case 6:
                    SmartRefreshLayout smartRefreshLayout8 = SmartRefreshLayout.this;
                    if (smartRefreshLayout8.f9634o1.f9739T || !smartRefreshLayout8.mo26068e(smartRefreshLayout8.f9641s0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.ReleaseToRefresh);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.ReleaseToRefresh);
                    return null;
                case 7:
                    SmartRefreshLayout smartRefreshLayout9 = SmartRefreshLayout.this;
                    if (smartRefreshLayout9.mo26068e(smartRefreshLayout9.f9643t0)) {
                        SmartRefreshLayout smartRefreshLayout10 = SmartRefreshLayout.this;
                        RefreshState bVar5 = smartRefreshLayout10.f9634o1;
                        if (!bVar5.f9739T && !bVar5.f9740U && (!smartRefreshLayout10.f9581K0 || !smartRefreshLayout10.f9653y0 || !smartRefreshLayout10.f9582L0)) {
                            SmartRefreshLayout.this.mo26050a(RefreshState.ReleaseToLoad);
                            return null;
                        }
                    }
                    SmartRefreshLayout.this.setViceState(RefreshState.ReleaseToLoad);
                    return null;
                case 8:
                    SmartRefreshLayout smartRefreshLayout11 = SmartRefreshLayout.this;
                    if (smartRefreshLayout11.f9634o1.f9739T || !smartRefreshLayout11.mo26068e(smartRefreshLayout11.f9641s0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.ReleaseToTwoLevel);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.ReleaseToTwoLevel);
                    return null;
                case 9:
                    SmartRefreshLayout smartRefreshLayout12 = SmartRefreshLayout.this;
                    if (smartRefreshLayout12.f9634o1.f9739T || !smartRefreshLayout12.mo26068e(smartRefreshLayout12.f9641s0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.RefreshReleased);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.RefreshReleased);
                    return null;
                case 10:
                    SmartRefreshLayout smartRefreshLayout13 = SmartRefreshLayout.this;
                    if (smartRefreshLayout13.f9634o1.f9739T || !smartRefreshLayout13.mo26068e(smartRefreshLayout13.f9643t0)) {
                        SmartRefreshLayout.this.setViceState(RefreshState.LoadReleased);
                        return null;
                    }
                    SmartRefreshLayout.this.mo26050a(RefreshState.LoadReleased);
                    return null;
                case 11:
                    SmartRefreshLayout.this.setStateRefreshing(true);
                    return null;
                case 12:
                    SmartRefreshLayout.this.setStateLoading(true);
                    return null;
                case 13:
                    SmartRefreshLayout smartRefreshLayout14 = SmartRefreshLayout.this;
                    if (smartRefreshLayout14.f9634o1 != RefreshState.Refreshing) {
                        return null;
                    }
                    smartRefreshLayout14.mo26050a(RefreshState.RefreshFinish);
                    return null;
                case 14:
                    SmartRefreshLayout smartRefreshLayout15 = SmartRefreshLayout.this;
                    if (smartRefreshLayout15.f9634o1 != RefreshState.Loading) {
                        return null;
                    }
                    smartRefreshLayout15.mo26050a(RefreshState.LoadFinish);
                    return null;
                case 15:
                    SmartRefreshLayout.this.mo26050a(RefreshState.TwoLevelReleased);
                    return null;
                case 16:
                    SmartRefreshLayout.this.mo26050a(RefreshState.TwoLevelFinish);
                    return null;
                case 17:
                    SmartRefreshLayout.this.mo26050a(RefreshState.TwoLevel);
                    return null;
                default:
                    return null;
            }
        }

        @NonNull
        /* renamed from: b */
        public RefreshLayout mo26111b() {
            return SmartRefreshLayout.this;
        }

        /* renamed from: b */
        public RefreshKernel mo26110b(@NonNull RefreshInternal hVar, boolean z) {
            if (hVar.equals(SmartRefreshLayout.this.f9622i1)) {
                SmartRefreshLayout.this.f9644t1 = z;
            } else if (hVar.equals(SmartRefreshLayout.this.f9624j1)) {
                SmartRefreshLayout.this.f9646u1 = z;
            }
            return this;
        }

        /* renamed from: b */
        public RefreshKernel mo26109b(int i) {
            SmartRefreshLayout.this.f9594T = i;
            return this;
        }

        /* renamed from: a */
        public RefreshKernel mo26108a(boolean z) {
            if (z) {
                C4707a aVar = new C4707a();
                ValueAnimator a = mo26102a(SmartRefreshLayout.this.getMeasuredHeight());
                if (a != null) {
                    SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                    if (a == smartRefreshLayout.f9571A1) {
                        a.setDuration((long) smartRefreshLayout.f9594T);
                        a.addListener(aVar);
                    }
                }
                aVar.onAnimationEnd(null);
            } else if (mo26102a(0) == null) {
                SmartRefreshLayout.this.mo26050a(RefreshState.None);
            }
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.scwang.smartrefresh.layout.SmartRefreshLayout.m.a(int, boolean):com.scwang.smartrefresh.layout.a.i
         arg types: [int, int]
         candidates:
          com.scwang.smartrefresh.layout.SmartRefreshLayout.m.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.SmartRefreshLayout.m.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
          com.scwang.smartrefresh.layout.SmartRefreshLayout.m.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
        /* renamed from: a */
        public RefreshKernel mo26103a() {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            if (smartRefreshLayout.f9634o1 == RefreshState.TwoLevel) {
                smartRefreshLayout.f9632n1.mo26107a(RefreshState.TwoLevelFinish);
                if (SmartRefreshLayout.this.f9588Q == 0) {
                    mo26104a(0, false);
                    SmartRefreshLayout.this.mo26050a(RefreshState.None);
                } else {
                    mo26102a(0).setDuration((long) SmartRefreshLayout.this.f9594T);
                }
            }
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:52:0x00b8  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x00bb  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x00c1  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.scwang.smartrefresh.layout.p206a.RefreshKernel mo26104a(int r19, boolean r20) {
            /*
                r18 = this;
                r0 = r18
                r1 = r19
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                if (r3 != r1) goto L_0x0021
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                if (r2 == 0) goto L_0x0014
                boolean r2 = r2.mo26130a()
                if (r2 != 0) goto L_0x0021
            L_0x0014:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9624j1
                if (r2 == 0) goto L_0x0020
                boolean r2 = r2.mo26130a()
                if (r2 != 0) goto L_0x0021
            L_0x0020:
                return r0
            L_0x0021:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r9 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r10 = r9.f9588Q
                r9.f9588Q = r1
                if (r20 == 0) goto L_0x0089
                com.scwang.smartrefresh.layout.b.b r2 = r9.f9636p1
                boolean r3 = r2.f9738S
                if (r3 != 0) goto L_0x0033
                boolean r2 = r2.f9739T
                if (r2 == 0) goto L_0x0089
            L_0x0033:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                float r4 = (float) r3
                int r5 = r2.f9603Y0
                float r5 = (float) r5
                float r6 = r2.f9618g1
                float r5 = r5 * r6
                int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                if (r4 <= 0) goto L_0x0051
                com.scwang.smartrefresh.layout.b.b r3 = r2.f9634o1
                com.scwang.smartrefresh.layout.b.b r4 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToTwoLevel
                if (r3 == r4) goto L_0x0089
                com.scwang.smartrefresh.layout.a.i r2 = r2.f9632n1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToRefresh
                r2.mo26107a(r3)
                goto L_0x0089
            L_0x0051:
                int r3 = -r3
                float r3 = (float) r3
                int r4 = r2.f9606a1
                float r4 = (float) r4
                float r5 = r2.f9620h1
                float r4 = r4 * r5
                int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
                if (r3 <= 0) goto L_0x006a
                boolean r3 = r2.f9581K0
                if (r3 != 0) goto L_0x006a
                com.scwang.smartrefresh.layout.a.i r2 = r2.f9632n1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToLoad
                r2.mo26107a(r3)
                goto L_0x0089
            L_0x006a:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                if (r3 >= 0) goto L_0x007c
                boolean r3 = r2.f9581K0
                if (r3 != 0) goto L_0x007c
                com.scwang.smartrefresh.layout.a.i r2 = r2.f9632n1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpToLoad
                r2.mo26107a(r3)
                goto L_0x0089
            L_0x007c:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                if (r3 <= 0) goto L_0x0089
                com.scwang.smartrefresh.layout.a.i r2 = r2.f9632n1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh
                r2.mo26107a(r3)
            L_0x0089:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.e r3 = r2.f9626k1
                r11 = 1
                r12 = 0
                if (r3 == 0) goto L_0x0153
                if (r1 < 0) goto L_0x00a6
                com.scwang.smartrefresh.layout.a.h r3 = r2.f9622i1
                if (r3 == 0) goto L_0x00a6
                boolean r4 = r2.f9649w0
                boolean r2 = r2.mo26052a(r4, r3)
                if (r2 == 0) goto L_0x00a2
                r2 = r1
            L_0x00a0:
                r3 = 1
                goto L_0x00a8
            L_0x00a2:
                if (r10 >= 0) goto L_0x00a6
                r2 = 0
                goto L_0x00a0
            L_0x00a6:
                r2 = 0
                r3 = 0
            L_0x00a8:
                if (r1 > 0) goto L_0x00bf
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r5 = r4.f9624j1
                if (r5 == 0) goto L_0x00bf
                boolean r6 = r4.f9651x0
                boolean r4 = r4.mo26052a(r6, r5)
                if (r4 == 0) goto L_0x00bb
                r2 = r1
            L_0x00b9:
                r3 = 1
                goto L_0x00bf
            L_0x00bb:
                if (r10 <= 0) goto L_0x00bf
                r2 = 0
                goto L_0x00b9
            L_0x00bf:
                if (r3 == 0) goto L_0x0153
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.e r4 = r3.f9626k1
                int r5 = r3.f9623j0
                int r3 = r3.f9625k0
                r4.mo26118a(r2, r5, r3)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r3.f9581K0
                if (r4 == 0) goto L_0x0102
                boolean r4 = r3.f9582L0
                if (r4 == 0) goto L_0x0102
                boolean r4 = r3.f9653y0
                if (r4 == 0) goto L_0x0102
                com.scwang.smartrefresh.layout.a.h r3 = r3.f9624j1
                boolean r4 = r3 instanceof com.scwang.smartrefresh.layout.p206a.RefreshFooter
                if (r4 == 0) goto L_0x0102
                com.scwang.smartrefresh.layout.b.c r3 = r3.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r4 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9742b
                if (r3 != r4) goto L_0x0102
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r3.f9643t0
                boolean r3 = r3.mo26068e(r4)
                if (r3 == 0) goto L_0x0102
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r3 = r3.f9624j1
                android.view.View r3 = r3.getView()
                int r4 = java.lang.Math.max(r12, r2)
                float r4 = (float) r4
                r3.setTranslationY(r4)
            L_0x0102:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r4 = r3.f9645u0
                if (r4 == 0) goto L_0x0116
                com.scwang.smartrefresh.layout.a.h r3 = r3.f9622i1
                if (r3 == 0) goto L_0x0116
                com.scwang.smartrefresh.layout.b.c r3 = r3.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r4 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9744d
                if (r3 != r4) goto L_0x0116
                r3 = 1
                goto L_0x0117
            L_0x0116:
                r3 = 0
            L_0x0117:
                if (r3 != 0) goto L_0x0122
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r3.f9640r1
                if (r3 == 0) goto L_0x0120
                goto L_0x0122
            L_0x0120:
                r3 = 0
                goto L_0x0123
            L_0x0122:
                r3 = 1
            L_0x0123:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r5 = r4.f9647v0
                if (r5 == 0) goto L_0x0137
                com.scwang.smartrefresh.layout.a.h r4 = r4.f9624j1
                if (r4 == 0) goto L_0x0137
                com.scwang.smartrefresh.layout.b.c r4 = r4.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r5 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9744d
                if (r4 != r5) goto L_0x0137
                r4 = 1
                goto L_0x0138
            L_0x0137:
                r4 = 0
            L_0x0138:
                if (r4 != 0) goto L_0x0143
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r4 = r4.f9642s1
                if (r4 == 0) goto L_0x0141
                goto L_0x0143
            L_0x0141:
                r4 = 0
                goto L_0x0144
            L_0x0143:
                r4 = 1
            L_0x0144:
                if (r3 == 0) goto L_0x014a
                if (r2 >= 0) goto L_0x0150
                if (r10 > 0) goto L_0x0150
            L_0x014a:
                if (r4 == 0) goto L_0x0153
                if (r2 <= 0) goto L_0x0150
                if (r10 >= 0) goto L_0x0153
            L_0x0150:
                r9.invalidate()
            L_0x0153:
                r13 = 1065353216(0x3f800000, float:1.0)
                r14 = 1073741824(0x40000000, float:2.0)
                if (r1 >= 0) goto L_0x015b
                if (r10 <= 0) goto L_0x0278
            L_0x015b:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                if (r2 == 0) goto L_0x0278
                int r8 = java.lang.Math.max(r1, r12)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r15 = r2.f9603Y0
                float r3 = (float) r15
                float r2 = r2.f9614e1
                float r3 = r3 * r2
                int r7 = (int) r3
                float r2 = (float) r8
                float r2 = r2 * r13
                if (r15 != 0) goto L_0x0176
                r3 = 1
                goto L_0x0177
            L_0x0176:
                r3 = r15
            L_0x0177:
                float r3 = (float) r3
                float r16 = r2 / r3
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r3 = r2.f9641s0
                boolean r2 = r2.mo26068e(r3)
                if (r2 != 0) goto L_0x0193
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r2 = r2.f9634o1
                com.scwang.smartrefresh.layout.b.b r3 = com.scwang.smartrefresh.layout.p207b.RefreshState.RefreshFinish
                if (r2 != r3) goto L_0x018f
                if (r20 != 0) goto L_0x018f
                goto L_0x0193
            L_0x018f:
                r17 = r7
                goto L_0x0258
            L_0x0193:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                if (r10 == r3) goto L_0x022f
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                com.scwang.smartrefresh.layout.b.c r2 = r2.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r3 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9742b
                if (r2 != r3) goto L_0x01cb
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                android.view.View r2 = r2.getView()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r3.f9588Q
                float r3 = (float) r3
                r2.setTranslationY(r3)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9640r1
                if (r3 == 0) goto L_0x021f
                android.graphics.Paint r3 = r2.f9628l1
                if (r3 == 0) goto L_0x021f
                boolean r3 = r2.f9649w0
                com.scwang.smartrefresh.layout.a.h r4 = r2.f9622i1
                boolean r2 = r2.mo26052a(r3, r4)
                if (r2 != 0) goto L_0x021f
                r9.invalidate()
                goto L_0x021f
            L_0x01cb:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                com.scwang.smartrefresh.layout.b.c r2 = r2.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r3 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9743c
                if (r2 != r3) goto L_0x021f
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                android.view.View r2 = r2.getView()
                android.view.ViewGroup$LayoutParams r3 = r2.getLayoutParams()
                boolean r4 = r3 instanceof android.view.ViewGroup.MarginLayoutParams
                if (r4 == 0) goto L_0x01ea
                android.view.ViewGroup$MarginLayoutParams r3 = (android.view.ViewGroup.MarginLayoutParams) r3
                goto L_0x01ec
            L_0x01ea:
                android.view.ViewGroup$MarginLayoutParams r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.f9569E1
            L_0x01ec:
                int r4 = r2.getMeasuredWidth()
                int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r14)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r5 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r5 = r5.f9588Q
                int r6 = r3.bottomMargin
                int r5 = r5 - r6
                int r6 = r3.topMargin
                int r5 = r5 - r6
                int r5 = java.lang.Math.max(r5, r12)
                int r5 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r14)
                r2.measure(r4, r5)
                int r4 = r3.leftMargin
                int r3 = r3.topMargin
                com.scwang.smartrefresh.layout.SmartRefreshLayout r5 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r5 = r5.f9610c1
                int r3 = r3 + r5
                int r5 = r2.getMeasuredWidth()
                int r5 = r5 + r4
                int r6 = r2.getMeasuredHeight()
                int r6 = r6 + r3
                r2.layout(r4, r3, r5, r6)
            L_0x021f:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                r3 = r20
                r4 = r16
                r5 = r8
                r6 = r15
                r17 = r7
                r2.mo24850a(r3, r4, r5, r6, r7)
                goto L_0x0231
            L_0x022f:
                r17 = r7
            L_0x0231:
                if (r20 == 0) goto L_0x0258
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                boolean r2 = r2.mo26130a()
                if (r2 == 0) goto L_0x0258
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                float r2 = r2.f9607b0
                int r2 = (int) r2
                int r3 = r9.getWidth()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                float r4 = r4.f9607b0
                if (r3 != 0) goto L_0x024e
                r5 = 1
                goto L_0x024f
            L_0x024e:
                r5 = r3
            L_0x024f:
                float r5 = (float) r5
                float r4 = r4 / r5
                com.scwang.smartrefresh.layout.SmartRefreshLayout r5 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r5 = r5.f9622i1
                r5.mo26128a(r4, r2, r3)
            L_0x0258:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r3 = r2.f9588Q
                if (r10 == r3) goto L_0x0278
                com.scwang.smartrefresh.layout.c.c r3 = r2.f9591R0
                if (r3 == 0) goto L_0x0278
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9622i1
                boolean r4 = r2 instanceof com.scwang.smartrefresh.layout.p206a.RefreshHeader
                if (r4 == 0) goto L_0x0278
                r4 = r2
                com.scwang.smartrefresh.layout.a.g r4 = (com.scwang.smartrefresh.layout.p206a.RefreshHeader) r4
                r2 = r3
                r3 = r4
                r4 = r20
                r5 = r16
                r6 = r8
                r7 = r15
                r8 = r17
                r2.mo26146a(r3, r4, r5, r6, r7, r8)
            L_0x0278:
                if (r1 <= 0) goto L_0x027c
                if (r10 >= 0) goto L_0x0394
            L_0x027c:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r2 = r2.f9624j1
                if (r2 == 0) goto L_0x0394
                int r1 = java.lang.Math.min(r1, r12)
                int r7 = -r1
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r8 = r1.f9606a1
                float r2 = (float) r8
                float r1 = r1.f9616f1
                float r2 = r2 * r1
                int r15 = (int) r2
                float r1 = (float) r7
                float r1 = r1 * r13
                if (r8 != 0) goto L_0x0298
                r2 = 1
                goto L_0x0299
            L_0x0298:
                r2 = r8
            L_0x0299:
                float r2 = (float) r2
                float r13 = r1 / r2
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                boolean r2 = r1.f9643t0
                boolean r1 = r1.mo26068e(r2)
                if (r1 != 0) goto L_0x02b0
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.b.b r1 = r1.f9634o1
                com.scwang.smartrefresh.layout.b.b r2 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadFinish
                if (r1 != r2) goto L_0x0376
                if (r20 != 0) goto L_0x0376
            L_0x02b0:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r2 = r1.f9588Q
                if (r10 == r2) goto L_0x0350
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                com.scwang.smartrefresh.layout.b.c r1 = r1.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r2 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9742b
                if (r1 != r2) goto L_0x02e8
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                android.view.View r1 = r1.getView()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r2 = r2.f9588Q
                float r2 = (float) r2
                r1.setTranslationY(r2)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r2 = r1.f9642s1
                if (r2 == 0) goto L_0x0343
                android.graphics.Paint r2 = r1.f9628l1
                if (r2 == 0) goto L_0x0343
                boolean r2 = r1.f9651x0
                com.scwang.smartrefresh.layout.a.h r3 = r1.f9624j1
                boolean r1 = r1.mo26052a(r2, r3)
                if (r1 != 0) goto L_0x0343
                r9.invalidate()
                goto L_0x0343
            L_0x02e8:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                com.scwang.smartrefresh.layout.b.c r1 = r1.getSpinnerStyle()
                com.scwang.smartrefresh.layout.b.c r2 = com.scwang.smartrefresh.layout.p207b.SpinnerStyle.f9743c
                if (r1 != r2) goto L_0x0343
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                android.view.View r1 = r1.getView()
                android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
                boolean r3 = r2 instanceof android.view.ViewGroup.MarginLayoutParams
                if (r3 == 0) goto L_0x0307
                android.view.ViewGroup$MarginLayoutParams r2 = (android.view.ViewGroup.MarginLayoutParams) r2
                goto L_0x0309
            L_0x0307:
                android.view.ViewGroup$MarginLayoutParams r2 = com.scwang.smartrefresh.layout.SmartRefreshLayout.f9569E1
            L_0x0309:
                int r3 = r1.getMeasuredWidth()
                int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r14)
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r4 = r4.f9588Q
                int r4 = -r4
                int r5 = r2.bottomMargin
                int r4 = r4 - r5
                int r5 = r2.topMargin
                int r4 = r4 - r5
                int r4 = java.lang.Math.max(r4, r12)
                int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r14)
                r1.measure(r3, r4)
                int r3 = r2.leftMargin
                int r2 = r2.topMargin
                int r4 = r9.getMeasuredHeight()
                int r2 = r2 + r4
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r4 = r4.f9612d1
                int r2 = r2 - r4
                int r4 = r1.getMeasuredHeight()
                int r4 = r2 - r4
                int r5 = r1.getMeasuredWidth()
                int r5 = r5 + r3
                r1.layout(r3, r4, r5, r2)
            L_0x0343:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                r2 = r20
                r3 = r13
                r4 = r7
                r5 = r8
                r6 = r15
                r1.mo24850a(r2, r3, r4, r5, r6)
            L_0x0350:
                if (r20 == 0) goto L_0x0376
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                boolean r1 = r1.mo26130a()
                if (r1 == 0) goto L_0x0376
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                float r1 = r1.f9607b0
                int r1 = (int) r1
                int r2 = r9.getWidth()
                com.scwang.smartrefresh.layout.SmartRefreshLayout r3 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                float r3 = r3.f9607b0
                if (r2 != 0) goto L_0x036c
                goto L_0x036d
            L_0x036c:
                r11 = r2
            L_0x036d:
                float r4 = (float) r11
                float r3 = r3 / r4
                com.scwang.smartrefresh.layout.SmartRefreshLayout r4 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                com.scwang.smartrefresh.layout.a.h r4 = r4.f9624j1
                r4.mo26128a(r3, r1, r2)
            L_0x0376:
                com.scwang.smartrefresh.layout.SmartRefreshLayout r1 = com.scwang.smartrefresh.layout.SmartRefreshLayout.this
                int r2 = r1.f9588Q
                if (r10 == r2) goto L_0x0394
                com.scwang.smartrefresh.layout.c.c r2 = r1.f9591R0
                if (r2 == 0) goto L_0x0394
                com.scwang.smartrefresh.layout.a.h r1 = r1.f9624j1
                boolean r3 = r1 instanceof com.scwang.smartrefresh.layout.p206a.RefreshFooter
                if (r3 == 0) goto L_0x0394
                r3 = r1
                com.scwang.smartrefresh.layout.a.f r3 = (com.scwang.smartrefresh.layout.p206a.RefreshFooter) r3
                r1 = r2
                r2 = r3
                r3 = r20
                r4 = r13
                r5 = r7
                r6 = r8
                r7 = r15
                r1.mo26143a(r2, r3, r4, r5, r6, r7)
            L_0x0394:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.C4706m.mo26104a(int, boolean):com.scwang.smartrefresh.layout.a.i");
        }

        /* renamed from: a */
        public ValueAnimator mo26102a(int i) {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            return smartRefreshLayout.mo26037a(i, 0, smartRefreshLayout.f9637q0, smartRefreshLayout.f9596U);
        }

        /* renamed from: a */
        public RefreshKernel mo26105a(@NonNull RefreshInternal hVar, int i) {
            SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
            if (smartRefreshLayout.f9628l1 == null && i != 0) {
                smartRefreshLayout.f9628l1 = new Paint();
            }
            if (hVar.equals(SmartRefreshLayout.this.f9622i1)) {
                SmartRefreshLayout.this.f9640r1 = i;
            } else if (hVar.equals(SmartRefreshLayout.this.f9624j1)) {
                SmartRefreshLayout.this.f9642s1 = i;
            }
            return this;
        }

        /* renamed from: a */
        public RefreshKernel mo26106a(@NonNull RefreshInternal hVar, boolean z) {
            if (hVar.equals(SmartRefreshLayout.this.f9622i1)) {
                SmartRefreshLayout smartRefreshLayout = SmartRefreshLayout.this;
                if (!smartRefreshLayout.f9584N0) {
                    smartRefreshLayout.f9584N0 = true;
                    smartRefreshLayout.f9649w0 = z;
                }
            } else if (hVar.equals(SmartRefreshLayout.this.f9624j1)) {
                SmartRefreshLayout smartRefreshLayout2 = SmartRefreshLayout.this;
                if (!smartRefreshLayout2.f9585O0) {
                    smartRefreshLayout2.f9585O0 = true;
                    smartRefreshLayout2.f9651x0 = z;
                }
            }
            return this;
        }
    }

    public SmartRefreshLayout(Context context) {
        this(context, null);
    }

    public static void setDefaultRefreshFooterCreator(@NonNull DefaultRefreshFooterCreator aVar) {
        f9566B1 = aVar;
    }

    public static void setDefaultRefreshHeaderCreator(@NonNull DefaultRefreshHeaderCreator bVar) {
        f9567C1 = bVar;
    }

    public static void setDefaultRefreshInitializer(@NonNull DefaultRefreshInitializer cVar) {
        f9568D1 = cVar;
    }

    public void computeScroll() {
        float f;
        this.f9633o0.getCurrY();
        if (this.f9633o0.computeScrollOffset()) {
            int finalY = this.f9633o0.getFinalY();
            if ((finalY >= 0 || ((!this.f9641s0 && !this.f9572B0) || !this.f9626k1.mo26123a())) && (finalY <= 0 || ((!this.f9643t0 && !this.f9572B0) || !this.f9626k1.mo26124b()))) {
                this.f9652x1 = true;
                invalidate();
                return;
            }
            if (this.f9652x1) {
                if (Build.VERSION.SDK_INT >= 14) {
                    f = finalY > 0 ? -this.f9633o0.getCurrVelocity() : this.f9633o0.getCurrVelocity();
                } else {
                    f = (((float) (this.f9633o0.getCurrY() - finalY)) * 1.0f) / ((float) Math.max(this.f9633o0.getDuration() - this.f9633o0.timePassed(), 1));
                }
                mo26055b(f);
            }
            this.f9633o0.forceFinished(true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c6, code lost:
        if (r4.f9740U == false) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00cc, code lost:
        if (r0.f9634o1.f9735P == false) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00da, code lost:
        if (r4.f9740U == false) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00e0, code lost:
        if (r0.f9634o1.f9736Q == false) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x010e, code lost:
        if (r6 != 3) goto L_0x031d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x02b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchTouchEvent(android.view.MotionEvent r23) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            int r6 = r23.getActionMasked()
            r2 = 6
            r10 = 0
            r11 = 1
            if (r6 != r2) goto L_0x000f
            r3 = 1
            goto L_0x0010
        L_0x000f:
            r3 = 0
        L_0x0010:
            if (r3 == 0) goto L_0x0017
            int r4 = r23.getActionIndex()
            goto L_0x0018
        L_0x0017:
            r4 = -1
        L_0x0018:
            int r5 = r23.getPointerCount()
            r7 = 0
            r8 = 0
            r9 = 0
            r12 = 0
        L_0x0020:
            if (r8 >= r5) goto L_0x0032
            if (r4 != r8) goto L_0x0025
            goto L_0x002f
        L_0x0025:
            float r13 = r1.getX(r8)
            float r9 = r9 + r13
            float r13 = r1.getY(r8)
            float r12 = r12 + r13
        L_0x002f:
            int r8 = r8 + 1
            goto L_0x0020
        L_0x0032:
            if (r3 == 0) goto L_0x0036
            int r5 = r5 + -1
        L_0x0036:
            float r3 = (float) r5
            float r9 = r9 / r3
            float r8 = r12 / r3
            r3 = 5
            if (r6 == r2) goto L_0x003f
            if (r6 != r3) goto L_0x004c
        L_0x003f:
            boolean r4 = r0.f9615f0
            if (r4 == 0) goto L_0x004c
            float r4 = r0.f9605a0
            float r5 = r0.f9609c0
            float r5 = r8 - r5
            float r4 = r4 + r5
            r0.f9605a0 = r4
        L_0x004c:
            r0.f9607b0 = r9
            r0.f9609c0 = r8
            boolean r4 = r0.f9597U0
            r5 = 2
            if (r4 == 0) goto L_0x00a8
            int r2 = r0.f9595T0
            boolean r1 = super.dispatchTouchEvent(r23)
            if (r6 != r5) goto L_0x00a7
            int r3 = r0.f9595T0
            if (r2 != r3) goto L_0x00a7
            float r2 = r0.f9607b0
            int r2 = (int) r2
            int r3 = r22.getWidth()
            float r4 = r0.f9607b0
            if (r3 != 0) goto L_0x006d
            goto L_0x006e
        L_0x006d:
            r11 = r3
        L_0x006e:
            float r5 = (float) r11
            float r4 = r4 / r5
            boolean r5 = r0.f9641s0
            boolean r5 = r0.mo26068e(r5)
            if (r5 == 0) goto L_0x008c
            int r5 = r0.f9588Q
            if (r5 <= 0) goto L_0x008c
            com.scwang.smartrefresh.layout.a.h r5 = r0.f9622i1
            if (r5 == 0) goto L_0x008c
            boolean r5 = r5.mo26130a()
            if (r5 == 0) goto L_0x008c
            com.scwang.smartrefresh.layout.a.h r5 = r0.f9622i1
            r5.mo26128a(r4, r2, r3)
            goto L_0x00a7
        L_0x008c:
            boolean r5 = r0.f9643t0
            boolean r5 = r0.mo26068e(r5)
            if (r5 == 0) goto L_0x00a7
            int r5 = r0.f9588Q
            if (r5 >= 0) goto L_0x00a7
            com.scwang.smartrefresh.layout.a.h r5 = r0.f9624j1
            if (r5 == 0) goto L_0x00a7
            boolean r5 = r5.mo26130a()
            if (r5 == 0) goto L_0x00a7
            com.scwang.smartrefresh.layout.a.h r5 = r0.f9624j1
            r5.mo26128a(r4, r2, r3)
        L_0x00a7:
            return r1
        L_0x00a8:
            boolean r4 = r22.isEnabled()
            if (r4 == 0) goto L_0x0362
            boolean r4 = r0.f9641s0
            if (r4 != 0) goto L_0x00ba
            boolean r4 = r0.f9643t0
            if (r4 != 0) goto L_0x00ba
            boolean r4 = r0.f9572B0
            if (r4 == 0) goto L_0x0362
        L_0x00ba:
            boolean r4 = r0.f9644t1
            if (r4 == 0) goto L_0x00ce
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            boolean r12 = r4.f9739T
            if (r12 != 0) goto L_0x00c8
            boolean r4 = r4.f9740U
            if (r4 == 0) goto L_0x00ce
        L_0x00c8:
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            boolean r4 = r4.f9735P
            if (r4 != 0) goto L_0x0362
        L_0x00ce:
            boolean r4 = r0.f9646u1
            if (r4 == 0) goto L_0x00e4
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            boolean r12 = r4.f9739T
            if (r12 != 0) goto L_0x00dc
            boolean r4 = r4.f9740U
            if (r4 == 0) goto L_0x00e4
        L_0x00dc:
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            boolean r4 = r4.f9736Q
            if (r4 == 0) goto L_0x00e4
            goto L_0x0362
        L_0x00e4:
            boolean r4 = r0.mo26060c(r6)
            if (r4 != 0) goto L_0x0361
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            boolean r12 = r4.f9740U
            if (r12 != 0) goto L_0x0361
            com.scwang.smartrefresh.layout.b.b r12 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading
            if (r4 != r12) goto L_0x00f8
            boolean r4 = r0.f9580J0
            if (r4 != 0) goto L_0x0361
        L_0x00f8:
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            com.scwang.smartrefresh.layout.b.b r12 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing
            if (r4 != r12) goto L_0x0104
            boolean r4 = r0.f9579I0
            if (r4 == 0) goto L_0x0104
            goto L_0x0361
        L_0x0104:
            r4 = 104(0x68, float:1.46E-43)
            if (r6 == 0) goto L_0x0322
            r2 = 0
            if (r6 == r11) goto L_0x02d4
            r3 = 3
            if (r6 == r5) goto L_0x0112
            if (r6 == r3) goto L_0x02ef
            goto L_0x031d
        L_0x0112:
            float r5 = r0.f9600W
            float r9 = r9 - r5
            float r5 = r0.f9605a0
            float r5 = r8 - r5
            android.view.VelocityTracker r6 = r0.f9635p0
            r6.addMovement(r1)
            boolean r6 = r0.f9615f0
            if (r6 != 0) goto L_0x01e7
            char r6 = r0.f9613e0
            if (r6 == r4) goto L_0x01e7
            com.scwang.smartrefresh.layout.a.e r12 = r0.f9626k1
            if (r12 == 0) goto L_0x01e7
            r12 = 118(0x76, float:1.65E-43)
            if (r6 == r12) goto L_0x0165
            float r6 = java.lang.Math.abs(r5)
            int r13 = r0.f9586P
            float r13 = (float) r13
            int r6 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r6 < 0) goto L_0x0146
            float r6 = java.lang.Math.abs(r9)
            float r13 = java.lang.Math.abs(r5)
            int r6 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r6 >= 0) goto L_0x0146
            goto L_0x0165
        L_0x0146:
            float r6 = java.lang.Math.abs(r9)
            int r13 = r0.f9586P
            float r13 = (float) r13
            int r6 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r6 < 0) goto L_0x01e7
            float r6 = java.lang.Math.abs(r9)
            float r13 = java.lang.Math.abs(r5)
            int r6 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r6 <= 0) goto L_0x01e7
            char r6 = r0.f9613e0
            if (r6 == r12) goto L_0x01e7
            r0.f9613e0 = r4
            goto L_0x01e7
        L_0x0165:
            r0.f9613e0 = r12
            int r4 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x0189
            int r4 = r0.f9588Q
            if (r4 < 0) goto L_0x017f
            boolean r4 = r0.f9572B0
            if (r4 != 0) goto L_0x0177
            boolean r4 = r0.f9641s0
            if (r4 == 0) goto L_0x0189
        L_0x0177:
            com.scwang.smartrefresh.layout.a.e r4 = r0.f9626k1
            boolean r4 = r4.mo26123a()
            if (r4 == 0) goto L_0x0189
        L_0x017f:
            r0.f9615f0 = r11
            int r4 = r0.f9586P
            float r4 = (float) r4
            float r4 = r8 - r4
            r0.f9605a0 = r4
            goto L_0x01b3
        L_0x0189:
            int r4 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r4 >= 0) goto L_0x01b3
            int r4 = r0.f9588Q
            if (r4 > 0) goto L_0x01ab
            boolean r4 = r0.f9572B0
            if (r4 != 0) goto L_0x0199
            boolean r4 = r0.f9643t0
            if (r4 == 0) goto L_0x01b3
        L_0x0199:
            com.scwang.smartrefresh.layout.b.b r4 = r0.f9634o1
            com.scwang.smartrefresh.layout.b.b r6 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading
            if (r4 != r6) goto L_0x01a3
            boolean r4 = r0.f9650w1
            if (r4 != 0) goto L_0x01ab
        L_0x01a3:
            com.scwang.smartrefresh.layout.a.e r4 = r0.f9626k1
            boolean r4 = r4.mo26124b()
            if (r4 == 0) goto L_0x01b3
        L_0x01ab:
            r0.f9615f0 = r11
            int r4 = r0.f9586P
            float r4 = (float) r4
            float r4 = r4 + r8
            r0.f9605a0 = r4
        L_0x01b3:
            boolean r4 = r0.f9615f0
            if (r4 == 0) goto L_0x01e7
            float r4 = r0.f9605a0
            float r5 = r8 - r4
            boolean r4 = r0.f9617g0
            if (r4 == 0) goto L_0x01c5
            r1.setAction(r3)
            super.dispatchTouchEvent(r23)
        L_0x01c5:
            com.scwang.smartrefresh.layout.a.i r4 = r0.f9632n1
            int r6 = r0.f9588Q
            if (r6 > 0) goto L_0x01d5
            if (r6 != 0) goto L_0x01d2
            int r6 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r6 <= 0) goto L_0x01d2
            goto L_0x01d5
        L_0x01d2:
            com.scwang.smartrefresh.layout.b.b r6 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpToLoad
            goto L_0x01d7
        L_0x01d5:
            com.scwang.smartrefresh.layout.b.b r6 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh
        L_0x01d7:
            r4.mo26107a(r6)
            android.view.ViewParent r4 = r22.getParent()
            boolean r6 = r4 instanceof android.view.ViewGroup
            if (r6 == 0) goto L_0x01e7
            android.view.ViewGroup r4 = (android.view.ViewGroup) r4
            r4.requestDisallowInterceptTouchEvent(r11)
        L_0x01e7:
            boolean r4 = r0.f9615f0
            if (r4 == 0) goto L_0x02c2
            int r4 = (int) r5
            int r6 = r0.f9592S
            int r4 = r4 + r6
            com.scwang.smartrefresh.layout.b.b r6 = r0.f9636p1
            boolean r6 = r6.f9735P
            if (r6 == 0) goto L_0x01fb
            if (r4 < 0) goto L_0x0207
            int r6 = r0.f9590R
            if (r6 < 0) goto L_0x0207
        L_0x01fb:
            com.scwang.smartrefresh.layout.b.b r6 = r0.f9636p1
            boolean r6 = r6.f9736Q
            if (r6 == 0) goto L_0x02bd
            if (r4 > 0) goto L_0x0207
            int r6 = r0.f9590R
            if (r6 <= 0) goto L_0x02bd
        L_0x0207:
            r0.f9590R = r4
            long r20 = r23.getEventTime()
            android.view.MotionEvent r1 = r0.f9654y1
            if (r1 != 0) goto L_0x022c
            r16 = 0
            float r1 = r0.f9600W
            float r17 = r1 + r9
            float r1 = r0.f9605a0
            r19 = 0
            r12 = r20
            r14 = r20
            r18 = r1
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r12, r14, r16, r17, r18, r19)
            r0.f9654y1 = r1
            android.view.MotionEvent r1 = r0.f9654y1
            super.dispatchTouchEvent(r1)
        L_0x022c:
            r16 = 2
            float r1 = r0.f9600W
            float r17 = r1 + r9
            float r1 = r0.f9605a0
            float r6 = (float) r4
            float r18 = r1 + r6
            r19 = 0
            r12 = r20
            r14 = r20
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r12, r14, r16, r17, r18, r19)
            super.dispatchTouchEvent(r1)
            boolean r6 = r0.f9650w1
            if (r6 == 0) goto L_0x0255
            int r6 = r0.f9586P
            float r6 = (float) r6
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 <= 0) goto L_0x0255
            int r5 = r0.f9588Q
            if (r5 >= 0) goto L_0x0255
            r0.f9650w1 = r10
        L_0x0255:
            if (r4 <= 0) goto L_0x0276
            boolean r5 = r0.f9572B0
            if (r5 != 0) goto L_0x025f
            boolean r5 = r0.f9641s0
            if (r5 == 0) goto L_0x0276
        L_0x025f:
            com.scwang.smartrefresh.layout.a.e r5 = r0.f9626k1
            boolean r5 = r5.mo26123a()
            if (r5 == 0) goto L_0x0276
            r0.f9609c0 = r8
            r0.f9605a0 = r8
            r0.f9592S = r10
            com.scwang.smartrefresh.layout.a.i r4 = r0.f9632n1
            com.scwang.smartrefresh.layout.b.b r5 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh
            r4.mo26107a(r5)
        L_0x0274:
            r4 = 0
            goto L_0x0296
        L_0x0276:
            if (r4 >= 0) goto L_0x0296
            boolean r5 = r0.f9572B0
            if (r5 != 0) goto L_0x0280
            boolean r5 = r0.f9643t0
            if (r5 == 0) goto L_0x0296
        L_0x0280:
            com.scwang.smartrefresh.layout.a.e r5 = r0.f9626k1
            boolean r5 = r5.mo26124b()
            if (r5 == 0) goto L_0x0296
            r0.f9609c0 = r8
            r0.f9605a0 = r8
            r0.f9592S = r10
            com.scwang.smartrefresh.layout.a.i r4 = r0.f9632n1
            com.scwang.smartrefresh.layout.b.b r5 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpToLoad
            r4.mo26107a(r5)
            goto L_0x0274
        L_0x0296:
            com.scwang.smartrefresh.layout.b.b r5 = r0.f9636p1
            boolean r5 = r5.f9735P
            if (r5 == 0) goto L_0x029e
            if (r4 < 0) goto L_0x02a6
        L_0x029e:
            com.scwang.smartrefresh.layout.b.b r5 = r0.f9636p1
            boolean r5 = r5.f9736Q
            if (r5 == 0) goto L_0x02ae
            if (r4 <= 0) goto L_0x02ae
        L_0x02a6:
            int r1 = r0.f9588Q
            if (r1 == 0) goto L_0x02ad
            r0.mo26059c(r7)
        L_0x02ad:
            return r11
        L_0x02ae:
            android.view.MotionEvent r5 = r0.f9654y1
            if (r5 == 0) goto L_0x02ba
            r0.f9654y1 = r2
            r1.setAction(r3)
            super.dispatchTouchEvent(r1)
        L_0x02ba:
            r1.recycle()
        L_0x02bd:
            float r1 = (float) r4
            r0.mo26059c(r1)
            return r11
        L_0x02c2:
            boolean r2 = r0.f9650w1
            if (r2 == 0) goto L_0x031d
            int r2 = r0.f9586P
            float r2 = (float) r2
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x031d
            int r2 = r0.f9588Q
            if (r2 >= 0) goto L_0x031d
            r0.f9650w1 = r10
            goto L_0x031d
        L_0x02d4:
            android.view.VelocityTracker r3 = r0.f9635p0
            r3.addMovement(r1)
            android.view.VelocityTracker r3 = r0.f9635p0
            r4 = 1000(0x3e8, float:1.401E-42)
            int r5 = r0.f9629m0
            float r5 = (float) r5
            r3.computeCurrentVelocity(r4, r5)
            android.view.VelocityTracker r3 = r0.f9635p0
            float r3 = r3.getYVelocity()
            int r3 = (int) r3
            r0.f9631n0 = r3
            r0.mo26064d(r7)
        L_0x02ef:
            android.view.VelocityTracker r3 = r0.f9635p0
            r3.clear()
            r3 = 110(0x6e, float:1.54E-43)
            r0.f9613e0 = r3
            android.view.MotionEvent r3 = r0.f9654y1
            if (r3 == 0) goto L_0x0313
            r3.recycle()
            r0.f9654y1 = r2
            long r4 = r23.getEventTime()
            float r7 = r0.f9600W
            r9 = 0
            r2 = r4
            android.view.MotionEvent r2 = android.view.MotionEvent.obtain(r2, r4, r6, r7, r8, r9)
            super.dispatchTouchEvent(r2)
            r2.recycle()
        L_0x0313:
            r22.mo26067e()
            boolean r2 = r0.f9615f0
            if (r2 == 0) goto L_0x031d
            r0.f9615f0 = r10
            return r11
        L_0x031d:
            boolean r1 = super.dispatchTouchEvent(r23)
            return r1
        L_0x0322:
            r0.f9631n0 = r10
            android.view.VelocityTracker r5 = r0.f9635p0
            r5.addMovement(r1)
            android.widget.Scroller r5 = r0.f9633o0
            r5.forceFinished(r11)
            r0.f9600W = r9
            r0.f9605a0 = r8
            r0.f9590R = r10
            int r5 = r0.f9588Q
            r0.f9592S = r5
            r0.f9615f0 = r10
            boolean r5 = super.dispatchTouchEvent(r23)
            r0.f9617g0 = r5
            com.scwang.smartrefresh.layout.b.b r5 = r0.f9634o1
            com.scwang.smartrefresh.layout.b.b r6 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevel
            if (r5 != r6) goto L_0x0359
            float r5 = r0.f9605a0
            int r6 = r22.getMeasuredHeight()
            int r6 = r6 * 5
            int r6 = r6 / r2
            float r2 = (float) r6
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0359
            r0.f9613e0 = r4
            boolean r1 = r0.f9617g0
            return r1
        L_0x0359:
            com.scwang.smartrefresh.layout.a.e r2 = r0.f9626k1
            if (r2 == 0) goto L_0x0360
            r2.mo26119a(r1)
        L_0x0360:
            return r11
        L_0x0361:
            return r10
        L_0x0362:
            boolean r1 = super.dispatchTouchEvent(r23)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        Paint paint;
        Paint paint2;
        RefreshContent eVar = this.f9626k1;
        View view2 = eVar != null ? eVar.getView() : null;
        RefreshInternal hVar = this.f9622i1;
        if (hVar != null && hVar.getView() == view) {
            if (!mo26068e(this.f9641s0) || (!this.f9655z0 && isInEditMode())) {
                return true;
            }
            if (view2 != null) {
                int max = Math.max(view2.getTop() + view2.getPaddingTop() + this.f9588Q, view.getTop());
                int i = this.f9640r1;
                if (!(i == 0 || (paint2 = this.f9628l1) == null)) {
                    paint2.setColor(i);
                    if (this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9743c) {
                        max = view.getBottom();
                    } else if (this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9742b) {
                        max = view.getBottom() + this.f9588Q;
                    }
                    canvas.drawRect(0.0f, (float) view.getTop(), (float) getWidth(), (float) max, this.f9628l1);
                }
                if (this.f9645u0 && this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9744d) {
                    canvas.save();
                    canvas.clipRect(view.getLeft(), view.getTop(), view.getRight(), max);
                    boolean drawChild = super.drawChild(canvas, view, j);
                    canvas.restore();
                    return drawChild;
                }
            }
        }
        RefreshInternal hVar2 = this.f9624j1;
        if (hVar2 != null && hVar2.getView() == view) {
            if (!mo26068e(this.f9643t0) || (!this.f9655z0 && isInEditMode())) {
                return true;
            }
            if (view2 != null) {
                int min = Math.min((view2.getBottom() - view2.getPaddingBottom()) + this.f9588Q, view.getBottom());
                int i2 = this.f9642s1;
                if (!(i2 == 0 || (paint = this.f9628l1) == null)) {
                    paint.setColor(i2);
                    if (this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9743c) {
                        min = view.getTop();
                    } else if (this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9742b) {
                        min = view.getTop() + this.f9588Q;
                    }
                    canvas.drawRect(0.0f, (float) min, (float) getWidth(), (float) view.getBottom(), this.f9628l1);
                }
                if (this.f9647v0 && this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9744d) {
                    canvas.save();
                    canvas.clipRect(view.getLeft(), min, view.getRight(), view.getBottom());
                    boolean drawChild2 = super.drawChild(canvas, view, j);
                    canvas.restore();
                    return drawChild2;
                }
            }
        }
        return super.drawChild(canvas, view, j);
    }

    /* renamed from: g */
    public RefreshLayout mo26070g(boolean z) {
        if (this.f9634o1 != RefreshState.Loading || !z) {
            if (this.f9581K0 != z) {
                this.f9581K0 = z;
                RefreshInternal hVar = this.f9624j1;
                if (hVar instanceof RefreshFooter) {
                    if (((RefreshFooter) hVar).mo26127a(z)) {
                        this.f9582L0 = true;
                        if (this.f9581K0 && this.f9653y0 && this.f9588Q > 0 && this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9742b && mo26068e(this.f9643t0) && mo26052a(this.f9641s0, this.f9622i1)) {
                            this.f9624j1.getView().setTranslationY((float) this.f9588Q);
                        }
                    } else {
                        this.f9582L0 = false;
                        new RuntimeException("Footer:" + this.f9624j1 + " NoMoreData is not supported.(不支持NoMoreData，请使用ClassicsFooter或者自定义)").printStackTrace();
                    }
                }
            }
            return this;
        }
        mo26038a();
        return this;
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C4705l(getContext(), attributeSet);
    }

    @NonNull
    public ViewGroup getLayout() {
        return super;
    }

    public int getNestedScrollAxes() {
        return this.f9602X0.getNestedScrollAxes();
    }

    @Nullable
    public RefreshFooter getRefreshFooter() {
        RefreshInternal hVar = this.f9624j1;
        if (hVar instanceof RefreshFooter) {
            return (RefreshFooter) hVar;
        }
        return null;
    }

    @Nullable
    public RefreshHeader getRefreshHeader() {
        RefreshInternal hVar = this.f9622i1;
        if (hVar instanceof RefreshHeader) {
            return (RefreshHeader) hVar;
        }
        return null;
    }

    @NonNull
    public RefreshState getState() {
        return this.f9634o1;
    }

    public boolean isNestedScrollingEnabled() {
        return this.f9578H0;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        RefreshInternal hVar;
        super.onAttachedToWindow();
        boolean z = true;
        this.f9648v1 = true;
        if (!isInEditMode()) {
            if (this.f9622i1 == null) {
                DefaultRefreshHeaderCreator bVar = f9567C1;
                if (bVar != null) {
                    mo26045a(bVar.mo26114a(getContext(), this));
                } else {
                    mo26045a(new BezierRadarHeader(getContext()));
                }
            }
            if (this.f9624j1 == null) {
                DefaultRefreshFooterCreator aVar = f9566B1;
                if (aVar != null) {
                    mo26043a(aVar.mo26113a(getContext(), this));
                } else {
                    boolean z2 = this.f9643t0;
                    mo26043a(new BallPulseFooter(getContext()));
                    this.f9643t0 = z2;
                }
            } else {
                if (!this.f9643t0 && this.f9583M0) {
                    z = false;
                }
                this.f9643t0 = z;
            }
            if (this.f9626k1 == null) {
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = getChildAt(i);
                    RefreshInternal hVar2 = this.f9622i1;
                    if ((hVar2 == null || childAt != hVar2.getView()) && ((hVar = this.f9624j1) == null || childAt != hVar.getView())) {
                        this.f9626k1 = new RefreshContentWrapper(childAt);
                    }
                }
            }
            if (this.f9626k1 == null) {
                int a = SmartUtil.m15574a(20.0f);
                TextView textView = new TextView(getContext());
                textView.setTextColor(-39424);
                textView.setGravity(17);
                textView.setTextSize(20.0f);
                textView.setText(R$string.srl_content_empty);
                super.addView(textView, -1, -1);
                this.f9626k1 = new RefreshContentWrapper(textView);
                this.f9626k1.getView().setPadding(a, a, a, a);
            }
            int i2 = this.f9619h0;
            View view = null;
            View findViewById = i2 > 0 ? findViewById(i2) : null;
            int i3 = this.f9621i0;
            if (i3 > 0) {
                view = findViewById(i3);
            }
            this.f9626k1.mo26121a(this.f9593S0);
            this.f9626k1.mo26122a(this.f9577G0);
            this.f9626k1.mo26120a(this.f9632n1, findViewById, view);
            if (this.f9588Q != 0) {
                mo26050a(RefreshState.None);
                RefreshContent eVar = this.f9626k1;
                this.f9588Q = 0;
                eVar.mo26118a(0, this.f9623j0, this.f9625k0);
            }
        }
        int[] iArr = this.f9639r0;
        if (iArr != null) {
            RefreshInternal hVar3 = this.f9622i1;
            if (hVar3 != null) {
                hVar3.setPrimaryColors(iArr);
            }
            RefreshInternal hVar4 = this.f9624j1;
            if (hVar4 != null) {
                hVar4.setPrimaryColors(this.f9639r0);
            }
        }
        RefreshContent eVar2 = this.f9626k1;
        if (eVar2 != null) {
            super.bringChildToFront(eVar2.getView());
        }
        RefreshInternal hVar5 = this.f9622i1;
        if (!(hVar5 == null || hVar5.getSpinnerStyle() == SpinnerStyle.f9744d)) {
            super.bringChildToFront(this.f9622i1.getView());
        }
        RefreshInternal hVar6 = this.f9624j1;
        if (hVar6 != null && hVar6.getSpinnerStyle() != SpinnerStyle.f9744d) {
            super.bringChildToFront(this.f9624j1.getView());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
     arg types: [int, int]
     candidates:
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f9648v1 = false;
        this.f9632n1.mo26104a(0, true);
        mo26050a(RefreshState.None);
        Handler handler = this.f9630m1;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        this.f9583M0 = true;
        this.f9656z1 = null;
        ValueAnimator valueAnimator = this.f9571A1;
        if (valueAnimator != null) {
            valueAnimator.removeAllListeners();
            this.f9571A1.removeAllUpdateListeners();
            this.f9571A1.cancel();
            this.f9571A1 = null;
        }
        this.f9650w1 = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onFinishInflate() {
        /*
            r11 = this;
            super.onFinishInflate()
            int r0 = super.getChildCount()
            r1 = 3
            if (r0 > r1) goto L_0x009e
            r2 = -1
            r3 = 0
            r4 = 0
            r5 = -1
            r6 = 0
        L_0x000f:
            r7 = 2
            r8 = 1
            if (r4 >= r0) goto L_0x0033
            android.view.View r9 = super.getChildAt(r4)
            boolean r10 = com.scwang.smartrefresh.layout.p209d.SmartUtil.m15576a(r9)
            if (r10 == 0) goto L_0x0024
            if (r6 < r7) goto L_0x0021
            if (r4 != r8) goto L_0x0024
        L_0x0021:
            r5 = r4
            r6 = 2
            goto L_0x0030
        L_0x0024:
            boolean r7 = r9 instanceof com.scwang.smartrefresh.layout.p206a.RefreshInternal
            if (r7 != 0) goto L_0x0030
            if (r6 >= r8) goto L_0x0030
            if (r4 <= 0) goto L_0x002d
            goto L_0x002e
        L_0x002d:
            r8 = 0
        L_0x002e:
            r5 = r4
            r6 = r8
        L_0x0030:
            int r4 = r4 + 1
            goto L_0x000f
        L_0x0033:
            if (r5 < 0) goto L_0x004d
            com.scwang.smartrefresh.layout.impl.a r4 = new com.scwang.smartrefresh.layout.impl.a
            android.view.View r6 = super.getChildAt(r5)
            r4.<init>(r6)
            r11.f9626k1 = r4
            if (r5 != r8) goto L_0x0048
            if (r0 != r1) goto L_0x0046
            r1 = 0
            goto L_0x004f
        L_0x0046:
            r1 = 0
            goto L_0x004e
        L_0x0048:
            if (r0 != r7) goto L_0x004d
            r1 = -1
            r7 = 1
            goto L_0x004f
        L_0x004d:
            r1 = -1
        L_0x004e:
            r7 = -1
        L_0x004f:
            r4 = 0
        L_0x0050:
            if (r4 >= r0) goto L_0x009d
            android.view.View r5 = super.getChildAt(r4)
            if (r4 == r1) goto L_0x008b
            if (r4 == r7) goto L_0x0065
            if (r1 != r2) goto L_0x0065
            com.scwang.smartrefresh.layout.a.h r6 = r11.f9622i1
            if (r6 != 0) goto L_0x0065
            boolean r6 = r5 instanceof com.scwang.smartrefresh.layout.p206a.RefreshHeader
            if (r6 == 0) goto L_0x0065
            goto L_0x008b
        L_0x0065:
            if (r4 == r7) goto L_0x006d
            if (r7 != r2) goto L_0x009a
            boolean r6 = r5 instanceof com.scwang.smartrefresh.layout.p206a.RefreshFooter
            if (r6 == 0) goto L_0x009a
        L_0x006d:
            boolean r6 = r11.f9643t0
            if (r6 != 0) goto L_0x0078
            boolean r6 = r11.f9583M0
            if (r6 != 0) goto L_0x0076
            goto L_0x0078
        L_0x0076:
            r6 = 0
            goto L_0x0079
        L_0x0078:
            r6 = 1
        L_0x0079:
            r11.f9643t0 = r6
            boolean r6 = r5 instanceof com.scwang.smartrefresh.layout.p206a.RefreshFooter
            if (r6 == 0) goto L_0x0082
            com.scwang.smartrefresh.layout.a.f r5 = (com.scwang.smartrefresh.layout.p206a.RefreshFooter) r5
            goto L_0x0088
        L_0x0082:
            com.scwang.smartrefresh.layout.impl.RefreshFooterWrapper r6 = new com.scwang.smartrefresh.layout.impl.RefreshFooterWrapper
            r6.<init>(r5)
            r5 = r6
        L_0x0088:
            r11.f9624j1 = r5
            goto L_0x009a
        L_0x008b:
            boolean r6 = r5 instanceof com.scwang.smartrefresh.layout.p206a.RefreshHeader
            if (r6 == 0) goto L_0x0092
            com.scwang.smartrefresh.layout.a.g r5 = (com.scwang.smartrefresh.layout.p206a.RefreshHeader) r5
            goto L_0x0098
        L_0x0092:
            com.scwang.smartrefresh.layout.impl.RefreshHeaderWrapper r6 = new com.scwang.smartrefresh.layout.impl.RefreshHeaderWrapper
            r6.<init>(r5)
            r5 = r6
        L_0x0098:
            r11.f9622i1 = r5
        L_0x009a:
            int r4 = r4 + 1
            goto L_0x0050
        L_0x009d:
            return
        L_0x009e:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "最多只支持3个子View，Most only support three sub view"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.SmartRefreshLayout.onFinishInflate():void");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        getPaddingBottom();
        int childCount = super.getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = super.getChildAt(i6);
            if (!(childAt.getVisibility() == 8 || childAt.getTag(R$string.srl_component_falsify) == childAt)) {
                RefreshContent eVar = this.f9626k1;
                boolean z2 = true;
                if (eVar != null && eVar.getView() == childAt) {
                    boolean z3 = isInEditMode() && this.f9655z0 && mo26068e(this.f9641s0) && this.f9622i1 != null;
                    View view = this.f9626k1.getView();
                    ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams = layoutParams instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams : f9569E1;
                    int i7 = marginLayoutParams.leftMargin + paddingLeft;
                    int i8 = marginLayoutParams.topMargin + paddingTop;
                    int measuredWidth = view.getMeasuredWidth() + i7;
                    int measuredHeight = view.getMeasuredHeight() + i8;
                    if (z3 && mo26052a(this.f9649w0, this.f9622i1)) {
                        int i9 = this.f9603Y0;
                        i8 += i9;
                        measuredHeight += i9;
                    }
                    view.layout(i7, i8, measuredWidth, measuredHeight);
                }
                RefreshInternal hVar = this.f9622i1;
                if (hVar != null && hVar.getView() == childAt) {
                    boolean z4 = isInEditMode() && this.f9655z0 && mo26068e(this.f9641s0);
                    View view2 = this.f9622i1.getView();
                    ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = layoutParams2 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams2 : f9569E1;
                    int i10 = marginLayoutParams2.leftMargin;
                    int i11 = marginLayoutParams2.topMargin + this.f9610c1;
                    int measuredWidth2 = view2.getMeasuredWidth() + i10;
                    int measuredHeight2 = view2.getMeasuredHeight() + i11;
                    if (!z4 && this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9742b) {
                        int i12 = this.f9603Y0;
                        i11 -= i12;
                        measuredHeight2 -= i12;
                    }
                    view2.layout(i10, i11, measuredWidth2, measuredHeight2);
                }
                RefreshInternal hVar2 = this.f9624j1;
                if (hVar2 != null && hVar2.getView() == childAt) {
                    if (!isInEditMode() || !this.f9655z0 || !mo26068e(this.f9643t0)) {
                        z2 = false;
                    }
                    View view3 = this.f9624j1.getView();
                    ViewGroup.LayoutParams layoutParams3 = view3.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams3 = layoutParams3 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams3 : f9569E1;
                    SpinnerStyle spinnerStyle = this.f9624j1.getSpinnerStyle();
                    int i13 = marginLayoutParams3.leftMargin;
                    int measuredHeight3 = (marginLayoutParams3.topMargin + getMeasuredHeight()) - this.f9612d1;
                    if (this.f9581K0 && this.f9582L0 && this.f9653y0 && this.f9626k1 != null && this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9742b && mo26068e(this.f9643t0)) {
                        View view4 = this.f9626k1.getView();
                        ViewGroup.LayoutParams layoutParams4 = view4.getLayoutParams();
                        measuredHeight3 = view4.getMeasuredHeight() + paddingTop + paddingTop + (layoutParams4 instanceof ViewGroup.MarginLayoutParams ? ((ViewGroup.MarginLayoutParams) layoutParams4).topMargin : 0);
                    }
                    if (spinnerStyle == SpinnerStyle.f9746f) {
                        measuredHeight3 = marginLayoutParams3.topMargin - this.f9612d1;
                    } else {
                        if (z2 || spinnerStyle == SpinnerStyle.f9745e || spinnerStyle == SpinnerStyle.f9744d) {
                            i5 = this.f9606a1;
                        } else if (spinnerStyle == SpinnerStyle.f9743c && this.f9588Q < 0) {
                            i5 = Math.max(mo26068e(this.f9643t0) ? -this.f9588Q : 0, 0);
                        }
                        measuredHeight3 -= i5;
                    }
                    view3.layout(i13, measuredHeight3, view3.getMeasuredWidth() + i13, view3.getMeasuredHeight() + measuredHeight3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = i;
        int i8 = i2;
        boolean z = isInEditMode() && this.f9655z0;
        int childCount = super.getChildCount();
        int i9 = 0;
        for (int i10 = 0; i10 < childCount; i10++) {
            View childAt = super.getChildAt(i10);
            if (childAt.getVisibility() != 8 && childAt.getTag(R$string.srl_component_falsify) != childAt) {
                RefreshInternal hVar = this.f9622i1;
                if (hVar != null && hVar.getView() == childAt) {
                    View view = this.f9622i1.getView();
                    ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams = layoutParams instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams : f9569E1;
                    int childMeasureSpec = ViewGroup.getChildMeasureSpec(i7, marginLayoutParams.leftMargin + marginLayoutParams.rightMargin, layoutParams.width);
                    int i11 = this.f9603Y0;
                    DimensionStatus aVar = this.f9604Z0;
                    if (aVar.f9715a < DimensionStatus.f9708i.f9715a) {
                        int i12 = layoutParams.height;
                        if (i12 > 0) {
                            i11 = i12 + marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
                            if (aVar.mo26136a(DimensionStatus.f9706g)) {
                                this.f9603Y0 = layoutParams.height + marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
                                this.f9604Z0 = DimensionStatus.f9706g;
                            }
                        } else if (i12 == -2 && (this.f9622i1.getSpinnerStyle() != SpinnerStyle.f9746f || !this.f9604Z0.f9716b)) {
                            int max = Math.max((View.MeasureSpec.getSize(i2) - marginLayoutParams.bottomMargin) - marginLayoutParams.topMargin, 0);
                            view.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(max, Integer.MIN_VALUE));
                            int measuredHeight = view.getMeasuredHeight();
                            if (measuredHeight > 0) {
                                if (measuredHeight != max && this.f9604Z0.mo26136a(DimensionStatus.f9704e)) {
                                    this.f9603Y0 = measuredHeight + marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
                                    this.f9604Z0 = DimensionStatus.f9704e;
                                }
                                i11 = -1;
                            }
                        }
                    }
                    if (this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9746f) {
                        i11 = View.MeasureSpec.getSize(i2);
                        i6 = -1;
                        i5 = 0;
                    } else {
                        if (this.f9622i1.getSpinnerStyle() != SpinnerStyle.f9743c || z) {
                            i5 = 0;
                        } else {
                            i5 = 0;
                            i11 = Math.max(0, mo26068e(this.f9641s0) ? this.f9588Q : 0);
                        }
                        i6 = -1;
                    }
                    if (i11 != i6) {
                        view.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(Math.max((i11 - marginLayoutParams.bottomMargin) - marginLayoutParams.topMargin, i5), 1073741824));
                    }
                    DimensionStatus aVar2 = this.f9604Z0;
                    if (!aVar2.f9716b) {
                        this.f9604Z0 = aVar2.mo26135a();
                        RefreshInternal hVar2 = this.f9622i1;
                        RefreshKernel iVar = this.f9632n1;
                        int i13 = this.f9603Y0;
                        hVar2.mo24848a(iVar, i13, (int) (this.f9614e1 * ((float) i13)));
                    }
                    if (z && mo26068e(this.f9641s0)) {
                        i9 += view.getMeasuredHeight();
                    }
                }
                RefreshInternal hVar3 = this.f9624j1;
                if (hVar3 != null && hVar3.getView() == childAt) {
                    View view2 = this.f9624j1.getView();
                    ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = layoutParams2 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams2 : f9569E1;
                    int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(i7, marginLayoutParams2.leftMargin + marginLayoutParams2.rightMargin, layoutParams2.width);
                    int i14 = this.f9606a1;
                    DimensionStatus aVar3 = this.f9608b1;
                    if (aVar3.f9715a < DimensionStatus.f9708i.f9715a) {
                        int i15 = layoutParams2.height;
                        if (i15 > 0) {
                            i14 = marginLayoutParams2.bottomMargin + i15 + marginLayoutParams2.topMargin;
                            if (aVar3.mo26136a(DimensionStatus.f9706g)) {
                                this.f9606a1 = layoutParams2.height + marginLayoutParams2.topMargin + marginLayoutParams2.bottomMargin;
                                this.f9608b1 = DimensionStatus.f9706g;
                            }
                        } else if (i15 == -2 && (this.f9624j1.getSpinnerStyle() != SpinnerStyle.f9746f || !this.f9608b1.f9716b)) {
                            int max2 = Math.max((View.MeasureSpec.getSize(i2) - marginLayoutParams2.bottomMargin) - marginLayoutParams2.topMargin, 0);
                            view2.measure(childMeasureSpec2, View.MeasureSpec.makeMeasureSpec(max2, Integer.MIN_VALUE));
                            int measuredHeight2 = view2.getMeasuredHeight();
                            if (measuredHeight2 > 0) {
                                if (measuredHeight2 != max2 && this.f9608b1.mo26136a(DimensionStatus.f9704e)) {
                                    this.f9606a1 = measuredHeight2 + marginLayoutParams2.topMargin + marginLayoutParams2.bottomMargin;
                                    this.f9608b1 = DimensionStatus.f9704e;
                                }
                                i14 = -1;
                            }
                        }
                    }
                    if (this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9746f) {
                        i14 = View.MeasureSpec.getSize(i2);
                        i4 = -1;
                        i3 = 0;
                    } else {
                        if (this.f9624j1.getSpinnerStyle() != SpinnerStyle.f9743c || z) {
                            i3 = 0;
                        } else {
                            i3 = 0;
                            i14 = Math.max(0, mo26068e(this.f9643t0) ? -this.f9588Q : 0);
                        }
                        i4 = -1;
                    }
                    if (i14 != i4) {
                        view2.measure(childMeasureSpec2, View.MeasureSpec.makeMeasureSpec(Math.max((i14 - marginLayoutParams2.bottomMargin) - marginLayoutParams2.topMargin, i3), 1073741824));
                    }
                    DimensionStatus aVar4 = this.f9608b1;
                    if (!aVar4.f9716b) {
                        this.f9608b1 = aVar4.mo26135a();
                        RefreshInternal hVar4 = this.f9624j1;
                        RefreshKernel iVar2 = this.f9632n1;
                        int i16 = this.f9606a1;
                        hVar4.mo24848a(iVar2, i16, (int) (this.f9616f1 * ((float) i16)));
                    }
                    if (z && mo26068e(this.f9643t0)) {
                        i9 += view2.getMeasuredHeight();
                    }
                }
                RefreshContent eVar = this.f9626k1;
                if (eVar != null && eVar.getView() == childAt) {
                    View view3 = this.f9626k1.getView();
                    ViewGroup.LayoutParams layoutParams3 = view3.getLayoutParams();
                    ViewGroup.MarginLayoutParams marginLayoutParams3 = layoutParams3 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams3 : f9569E1;
                    view3.measure(ViewGroup.getChildMeasureSpec(i7, getPaddingLeft() + getPaddingRight() + marginLayoutParams3.leftMargin + marginLayoutParams3.rightMargin, layoutParams3.width), ViewGroup.getChildMeasureSpec(i8, getPaddingTop() + getPaddingBottom() + marginLayoutParams3.topMargin + marginLayoutParams3.bottomMargin + ((!z || !(this.f9622i1 != null && mo26068e(this.f9641s0) && mo26052a(this.f9649w0, this.f9622i1))) ? 0 : this.f9603Y0) + ((!z || !(this.f9624j1 != null && mo26068e(this.f9643t0) && mo26052a(this.f9651x0, this.f9624j1))) ? 0 : this.f9606a1), layoutParams3.height));
                    i9 += view3.getMeasuredHeight();
                }
            }
        }
        super.setMeasuredDimension(View.resolveSize(super.getSuggestedMinimumWidth(), i7), View.resolveSize(i9, i8));
        this.f9607b0 = ((float) getMeasuredWidth()) / 2.0f;
    }

    public boolean onNestedFling(@NonNull View view, float f, float f2, boolean z) {
        return this.f9601W0.dispatchNestedFling(f, f2, z);
    }

    public boolean onNestedPreFling(@NonNull View view, float f, float f2) {
        return (this.f9650w1 && f2 > 0.0f) || mo26064d(-f2) || this.f9601W0.dispatchNestedPreFling(f, f2);
    }

    public void onNestedPreScroll(@NonNull View view, int i, int i2, @NonNull int[] iArr) {
        int i3;
        int i4 = this.f9595T0;
        if (i2 * i4 > 0) {
            if (Math.abs(i2) > Math.abs(this.f9595T0)) {
                i3 = this.f9595T0;
                this.f9595T0 = 0;
            } else {
                this.f9595T0 -= i2;
                i3 = i2;
            }
            mo26059c((float) this.f9595T0);
        } else if (i2 <= 0 || !this.f9650w1) {
            i3 = 0;
        } else {
            this.f9595T0 = i4 - i2;
            mo26059c((float) this.f9595T0);
            i3 = i2;
        }
        this.f9601W0.dispatchNestedPreScroll(i, i2 - i3, iArr, null);
        iArr[1] = iArr[1] + i3;
    }

    public void onNestedScroll(@NonNull View view, int i, int i2, int i3, int i4) {
        boolean dispatchNestedScroll = this.f9601W0.dispatchNestedScroll(i, i2, i3, i4, this.f9599V0);
        int i5 = i4 + this.f9599V0[1];
        if ((i5 < 0 && (this.f9641s0 || this.f9572B0)) || (i5 > 0 && (this.f9643t0 || this.f9572B0))) {
            RefreshState bVar = this.f9636p1;
            if (bVar == RefreshState.None || bVar.f9739T) {
                this.f9632n1.mo26107a(i5 > 0 ? RefreshState.PullUpToLoad : RefreshState.PullDownToRefresh);
                if (!dispatchNestedScroll) {
                    ViewParent parent = getParent();
                    if (parent instanceof ViewGroup) {
                        ((ViewGroup) parent).requestDisallowInterceptTouchEvent(true);
                    }
                }
            }
            int i6 = this.f9595T0 - i5;
            this.f9595T0 = i6;
            mo26059c((float) i6);
        }
        if (this.f9650w1 && i2 < 0) {
            this.f9650w1 = false;
        }
    }

    public void onNestedScrollAccepted(@NonNull View view, @NonNull View view2, int i) {
        this.f9602X0.onNestedScrollAccepted(view, view2, i);
        this.f9601W0.startNestedScroll(i & 2);
        this.f9595T0 = this.f9588Q;
        this.f9597U0 = true;
        mo26060c(0);
    }

    public boolean onStartNestedScroll(@NonNull View view, @NonNull View view2, int i) {
        if (!(isEnabled() && isNestedScrollingEnabled() && (i & 2) != 0) || (!this.f9572B0 && !this.f9641s0 && !this.f9643t0)) {
            return false;
        }
        return true;
    }

    public void onStopNestedScroll(@NonNull View view) {
        this.f9602X0.onStopNestedScroll(view);
        this.f9597U0 = false;
        this.f9595T0 = 0;
        mo26067e();
        this.f9601W0.stopNestedScroll();
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f9578H0 = z;
        this.f9601W0.setNestedScrollingEnabled(z);
    }

    /* access modifiers changed from: protected */
    public void setStateDirectLoading(boolean z) {
        if (this.f9634o1 != RefreshState.Loading) {
            this.f9638q1 = System.currentTimeMillis();
            this.f9650w1 = true;
            mo26050a(RefreshState.Loading);
            OnLoadMoreListener bVar = this.f9589Q0;
            if (bVar != null) {
                if (z) {
                    bVar.mo24430b(this);
                }
            } else if (this.f9591R0 == null) {
                mo26040a(2000);
            }
            RefreshInternal hVar = this.f9624j1;
            if (hVar != null) {
                int i = this.f9606a1;
                hVar.mo26129a(this, i, (int) (this.f9616f1 * ((float) i)));
            }
            OnMultiPurposeListener cVar = this.f9591R0;
            if (cVar != null && (this.f9624j1 instanceof RefreshFooter)) {
                if (z) {
                    cVar.mo24430b(this);
                }
                int i2 = this.f9606a1;
                this.f9591R0.mo26141a((RefreshFooter) this.f9624j1, i2, (int) (this.f9616f1 * ((float) i2)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setStateLoading(boolean z) {
        C4691b bVar = new C4691b(z);
        mo26050a(RefreshState.LoadReleased);
        ValueAnimator a = this.f9632n1.mo26102a(-this.f9606a1);
        if (a != null) {
            a.addListener(bVar);
        }
        RefreshInternal hVar = this.f9624j1;
        if (hVar != null) {
            int i = this.f9606a1;
            hVar.mo24851b(this, i, (int) (this.f9616f1 * ((float) i)));
        }
        OnMultiPurposeListener cVar = this.f9591R0;
        if (cVar != null) {
            RefreshInternal hVar2 = this.f9624j1;
            if (hVar2 instanceof RefreshFooter) {
                int i2 = this.f9606a1;
                cVar.mo26147b((RefreshFooter) hVar2, i2, (int) (this.f9616f1 * ((float) i2)));
            }
        }
        if (a == null) {
            bVar.onAnimationEnd(null);
        }
    }

    /* access modifiers changed from: protected */
    public void setStateRefreshing(boolean z) {
        C4692c cVar = new C4692c(z);
        mo26050a(RefreshState.RefreshReleased);
        ValueAnimator a = this.f9632n1.mo26102a(this.f9603Y0);
        if (a != null) {
            a.addListener(cVar);
        }
        RefreshInternal hVar = this.f9622i1;
        if (hVar != null) {
            int i = this.f9603Y0;
            hVar.mo24851b(this, i, (int) (this.f9614e1 * ((float) i)));
        }
        OnMultiPurposeListener cVar2 = this.f9591R0;
        if (cVar2 != null) {
            RefreshInternal hVar2 = this.f9622i1;
            if (hVar2 instanceof RefreshHeader) {
                int i2 = this.f9603Y0;
                cVar2.mo26144a((RefreshHeader) hVar2, i2, (int) (this.f9614e1 * ((float) i2)));
            }
        }
        if (a == null) {
            cVar.onAnimationEnd(null);
        }
    }

    /* access modifiers changed from: protected */
    public void setViceState(RefreshState bVar) {
        RefreshState bVar2 = this.f9634o1;
        if (bVar2.f9738S && bVar2.f9735P != bVar.f9735P) {
            mo26050a(RefreshState.None);
        }
        if (this.f9636p1 != bVar) {
            this.f9636p1 = bVar;
        }
    }

    public SmartRefreshLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26050a(RefreshState bVar) {
        RefreshState bVar2 = this.f9634o1;
        if (bVar2 != bVar) {
            this.f9634o1 = bVar;
            this.f9636p1 = bVar;
            RefreshInternal hVar = this.f9622i1;
            RefreshInternal hVar2 = this.f9624j1;
            OnMultiPurposeListener cVar = this.f9591R0;
            if (hVar != null) {
                hVar.mo24849a(this, bVar2, bVar);
            }
            if (hVar2 != null) {
                hVar2.mo24849a(this, bVar2, bVar);
            }
            if (cVar != null) {
                cVar.mo24849a(this, bVar2, bVar);
            }
            if (bVar == RefreshState.LoadFinish) {
                this.f9650w1 = false;
            }
        } else if (this.f9636p1 != bVar2) {
            this.f9636p1 = bVar2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo26055b(float f) {
        RefreshState bVar;
        if (this.f9571A1 != null) {
            return;
        }
        if (f > 0.0f && ((bVar = this.f9634o1) == RefreshState.Refreshing || bVar == RefreshState.TwoLevel)) {
            this.f9656z1 = new C4703j(f, this.f9603Y0);
        } else if (f < 0.0f && (this.f9634o1 == RefreshState.Loading || ((this.f9653y0 && this.f9581K0 && this.f9582L0 && mo26068e(this.f9643t0)) || (this.f9573C0 && !this.f9581K0 && mo26068e(this.f9643t0) && this.f9634o1 != RefreshState.Refreshing)))) {
            this.f9656z1 = new C4703j(f, -this.f9606a1);
        } else if (this.f9588Q == 0 && this.f9570A0) {
            this.f9656z1 = new C4703j(f, 0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public boolean mo26060c(int i) {
        if (i == 0) {
            if (this.f9571A1 != null) {
                RefreshState bVar = this.f9634o1;
                if (bVar.f9740U || bVar == RefreshState.TwoLevelReleased) {
                    return true;
                }
                if (bVar == RefreshState.PullDownCanceled) {
                    this.f9632n1.mo26107a(RefreshState.PullDownToRefresh);
                } else if (bVar == RefreshState.PullUpCanceled) {
                    this.f9632n1.mo26107a(RefreshState.PullUpToLoad);
                }
                this.f9571A1.cancel();
                this.f9571A1 = null;
            }
            this.f9656z1 = null;
        }
        if (this.f9571A1 != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public boolean mo26064d(float f) {
        if (f == 0.0f) {
            f = (float) this.f9631n0;
        }
        if (Build.VERSION.SDK_INT > 27 && this.f9626k1 != null) {
            getScaleY();
            View view = this.f9626k1.getView();
            if (getScaleY() == -1.0f && view.getScaleY() == -1.0f) {
                f = -f;
            }
        }
        if (Math.abs(f) > ((float) this.f9627l0)) {
            int i = this.f9588Q;
            if (((float) i) * f < 0.0f) {
                RefreshState bVar = this.f9634o1;
                if (bVar == RefreshState.Refreshing || bVar == RefreshState.Loading || (i < 0 && this.f9581K0)) {
                    this.f9656z1 = new C4704k(f).mo26100a();
                    return true;
                } else if (this.f9634o1.f9741V) {
                    return true;
                }
            }
            if ((f < 0.0f && ((this.f9570A0 && (this.f9643t0 || this.f9572B0)) || ((this.f9634o1 == RefreshState.Loading && this.f9588Q >= 0) || (this.f9573C0 && mo26068e(this.f9643t0))))) || (f > 0.0f && ((this.f9570A0 && this.f9641s0) || this.f9572B0 || (this.f9634o1 == RefreshState.Refreshing && this.f9588Q <= 0)))) {
                this.f9652x1 = false;
                this.f9633o0.fling(0, 0, 0, (int) (-f), 0, 0, -2147483647, Integer.MAX_VALUE);
                this.f9633o0.computeScrollOffset();
                invalidate();
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean mo26068e(boolean z) {
        return z && !this.f9574D0;
    }

    /* renamed from: f */
    public RefreshLayout mo26069f(boolean z) {
        this.f9641s0 = z;
        return this;
    }

    public SmartRefreshLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9594T = ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION;
        this.f9596U = ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION;
        this.f9611d0 = 0.5f;
        this.f9613e0 = 'n';
        this.f9619h0 = -1;
        this.f9621i0 = -1;
        this.f9623j0 = -1;
        this.f9625k0 = -1;
        this.f9641s0 = true;
        this.f9643t0 = false;
        this.f9645u0 = true;
        this.f9647v0 = true;
        this.f9649w0 = true;
        this.f9651x0 = true;
        this.f9653y0 = false;
        this.f9655z0 = true;
        this.f9570A0 = true;
        this.f9572B0 = false;
        this.f9573C0 = true;
        this.f9574D0 = false;
        this.f9575E0 = true;
        this.f9576F0 = true;
        this.f9577G0 = true;
        this.f9578H0 = true;
        this.f9579I0 = false;
        this.f9580J0 = false;
        this.f9581K0 = false;
        this.f9582L0 = false;
        this.f9583M0 = false;
        this.f9584N0 = false;
        this.f9585O0 = false;
        this.f9599V0 = new int[2];
        this.f9601W0 = new NestedScrollingChildHelper(this);
        this.f9602X0 = new NestedScrollingParentHelper(super);
        DimensionStatus aVar = DimensionStatus.f9702c;
        this.f9604Z0 = aVar;
        this.f9608b1 = aVar;
        this.f9614e1 = 2.5f;
        this.f9616f1 = 2.5f;
        this.f9618g1 = 1.0f;
        this.f9620h1 = 1.0f;
        this.f9632n1 = new C4706m();
        RefreshState bVar = RefreshState.None;
        this.f9634o1 = bVar;
        this.f9636p1 = bVar;
        this.f9638q1 = 0;
        this.f9640r1 = 0;
        this.f9642s1 = 0;
        this.f9650w1 = false;
        this.f9652x1 = false;
        this.f9654y1 = null;
        super.setClipToPadding(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.f9630m1 = new Handler();
        this.f9633o0 = new Scroller(context);
        this.f9635p0 = VelocityTracker.obtain();
        this.f9598V = context.getResources().getDisplayMetrics().heightPixels;
        this.f9637q0 = new SmartUtil();
        this.f9586P = viewConfiguration.getScaledTouchSlop();
        this.f9627l0 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f9629m0 = viewConfiguration.getScaledMaximumFlingVelocity();
        this.f9606a1 = SmartUtil.m15574a(60.0f);
        this.f9603Y0 = SmartUtil.m15574a(100.0f);
        DefaultRefreshInitializer cVar = f9568D1;
        if (cVar != null) {
            cVar.mo26115a(context, this);
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.SmartRefreshLayout);
        this.f9611d0 = obtainStyledAttributes.getFloat(R$styleable.SmartRefreshLayout_srlDragRate, this.f9611d0);
        this.f9614e1 = obtainStyledAttributes.getFloat(R$styleable.SmartRefreshLayout_srlHeaderMaxDragRate, this.f9614e1);
        this.f9616f1 = obtainStyledAttributes.getFloat(R$styleable.SmartRefreshLayout_srlFooterMaxDragRate, this.f9616f1);
        this.f9618g1 = obtainStyledAttributes.getFloat(R$styleable.SmartRefreshLayout_srlHeaderTriggerRate, this.f9618g1);
        this.f9620h1 = obtainStyledAttributes.getFloat(R$styleable.SmartRefreshLayout_srlFooterTriggerRate, this.f9620h1);
        this.f9641s0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableRefresh, this.f9641s0);
        this.f9596U = obtainStyledAttributes.getInt(R$styleable.SmartRefreshLayout_srlReboundDuration, this.f9596U);
        this.f9643t0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableLoadMore, this.f9643t0);
        this.f9603Y0 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.SmartRefreshLayout_srlHeaderHeight, this.f9603Y0);
        this.f9606a1 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.SmartRefreshLayout_srlFooterHeight, this.f9606a1);
        this.f9610c1 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.SmartRefreshLayout_srlHeaderInsetStart, this.f9610c1);
        this.f9612d1 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.SmartRefreshLayout_srlFooterInsetStart, this.f9612d1);
        this.f9579I0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlDisableContentWhenRefresh, this.f9579I0);
        this.f9580J0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlDisableContentWhenLoading, this.f9580J0);
        this.f9649w0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableHeaderTranslationContent, this.f9649w0);
        this.f9651x0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableFooterTranslationContent, this.f9651x0);
        this.f9655z0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnablePreviewInEditMode, this.f9655z0);
        this.f9573C0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableAutoLoadMore, this.f9573C0);
        this.f9570A0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableOverScrollBounce, this.f9570A0);
        this.f9574D0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnablePureScrollMode, this.f9574D0);
        this.f9575E0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableScrollContentWhenLoaded, this.f9575E0);
        this.f9576F0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableScrollContentWhenRefreshed, this.f9576F0);
        this.f9577G0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableLoadMoreWhenContentNotFull, this.f9577G0);
        this.f9653y0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableFooterFollowWhenLoadFinished, this.f9653y0);
        this.f9653y0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableFooterFollowWhenNoMoreData, this.f9653y0);
        this.f9645u0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableClipHeaderWhenFixedBehind, this.f9645u0);
        this.f9647v0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableClipFooterWhenFixedBehind, this.f9647v0);
        this.f9572B0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableOverScrollDrag, this.f9572B0);
        this.f9619h0 = obtainStyledAttributes.getResourceId(R$styleable.SmartRefreshLayout_srlFixedHeaderViewId, this.f9619h0);
        this.f9621i0 = obtainStyledAttributes.getResourceId(R$styleable.SmartRefreshLayout_srlFixedFooterViewId, this.f9621i0);
        this.f9623j0 = obtainStyledAttributes.getResourceId(R$styleable.SmartRefreshLayout_srlHeaderTranslationViewId, this.f9623j0);
        this.f9625k0 = obtainStyledAttributes.getResourceId(R$styleable.SmartRefreshLayout_srlFooterTranslationViewId, this.f9625k0);
        this.f9578H0 = obtainStyledAttributes.getBoolean(R$styleable.SmartRefreshLayout_srlEnableNestedScrolling, this.f9578H0);
        this.f9601W0.setNestedScrollingEnabled(this.f9578H0);
        this.f9583M0 = this.f9583M0 || obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_srlEnableLoadMore);
        this.f9584N0 = this.f9584N0 || obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_srlEnableHeaderTranslationContent);
        this.f9585O0 = this.f9585O0 || obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_srlEnableFooterTranslationContent);
        this.f9604Z0 = obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_srlHeaderHeight) ? DimensionStatus.f9708i : this.f9604Z0;
        this.f9608b1 = obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_srlFooterHeight) ? DimensionStatus.f9708i : this.f9608b1;
        int color = obtainStyledAttributes.getColor(R$styleable.SmartRefreshLayout_srlAccentColor, 0);
        int color2 = obtainStyledAttributes.getColor(R$styleable.SmartRefreshLayout_srlPrimaryColor, 0);
        if (color2 != 0) {
            if (color != 0) {
                this.f9639r0 = new int[]{color2, color};
            } else {
                this.f9639r0 = new int[]{color2};
            }
        } else if (color != 0) {
            this.f9639r0 = new int[]{0, color};
        }
        if (this.f9574D0 && !this.f9583M0 && !this.f9643t0) {
            this.f9643t0 = true;
        }
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo26067e() {
        RefreshState bVar = this.f9634o1;
        if (bVar == RefreshState.TwoLevel) {
            if (this.f9631n0 > -1000 && this.f9588Q > getMeasuredHeight() / 2) {
                ValueAnimator a = this.f9632n1.mo26102a(getMeasuredHeight());
                if (a != null) {
                    a.setDuration((long) this.f9594T);
                }
            } else if (this.f9615f0) {
                this.f9632n1.mo26103a();
            }
        } else if (bVar == RefreshState.Loading || (this.f9653y0 && this.f9581K0 && this.f9582L0 && this.f9588Q < 0 && mo26068e(this.f9643t0))) {
            int i = this.f9588Q;
            int i2 = this.f9606a1;
            if (i < (-i2)) {
                this.f9632n1.mo26102a(-i2);
            } else if (i > 0) {
                this.f9632n1.mo26102a(0);
            }
        } else {
            RefreshState bVar2 = this.f9634o1;
            if (bVar2 == RefreshState.Refreshing) {
                int i3 = this.f9588Q;
                int i4 = this.f9603Y0;
                if (i3 > i4) {
                    this.f9632n1.mo26102a(i4);
                } else if (i3 < 0) {
                    this.f9632n1.mo26102a(0);
                }
            } else if (bVar2 == RefreshState.PullDownToRefresh) {
                this.f9632n1.mo26107a(RefreshState.PullDownCanceled);
            } else if (bVar2 == RefreshState.PullUpToLoad) {
                this.f9632n1.mo26107a(RefreshState.PullUpCanceled);
            } else if (bVar2 == RefreshState.ReleaseToRefresh) {
                this.f9632n1.mo26107a(RefreshState.Refreshing);
            } else if (bVar2 == RefreshState.ReleaseToLoad) {
                this.f9632n1.mo26107a(RefreshState.Loading);
            } else if (bVar2 == RefreshState.ReleaseToTwoLevel) {
                this.f9632n1.mo26107a(RefreshState.TwoLevelReleased);
            } else if (bVar2 == RefreshState.RefreshReleased) {
                if (this.f9571A1 == null) {
                    this.f9632n1.mo26102a(this.f9603Y0);
                }
            } else if (bVar2 == RefreshState.LoadReleased) {
                if (this.f9571A1 == null) {
                    this.f9632n1.mo26102a(-this.f9606a1);
                }
            } else if (this.f9588Q != 0) {
                this.f9632n1.mo26102a(0);
            }
        }
    }

    /* renamed from: com.scwang.smartrefresh.layout.SmartRefreshLayout$l */
    public static class C4705l extends ViewGroup.MarginLayoutParams {

        /* renamed from: a */
        public int f9698a = 0;

        /* renamed from: b */
        public SpinnerStyle f9699b = null;

        public C4705l(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.SmartRefreshLayout_Layout);
            this.f9698a = obtainStyledAttributes.getColor(R$styleable.SmartRefreshLayout_Layout_layout_srlBackgroundColor, this.f9698a);
            if (obtainStyledAttributes.hasValue(R$styleable.SmartRefreshLayout_Layout_layout_srlSpinnerStyle)) {
                this.f9699b = SpinnerStyle.f9747g[obtainStyledAttributes.getInt(R$styleable.SmartRefreshLayout_Layout_layout_srlSpinnerStyle, SpinnerStyle.f9742b.f9748a)];
            }
            obtainStyledAttributes.recycle();
        }

        public C4705l(int i, int i2) {
            super(i, i2);
        }
    }

    /* renamed from: b */
    public RefreshLayout mo26054b(boolean z) {
        setNestedScrollingEnabled(z);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, int, java.lang.Boolean]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j */
    /* renamed from: b */
    public RefreshLayout mo26053b(int i) {
        return mo26041a(i, true, Boolean.FALSE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
     arg types: [int, int]
     candidates:
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo26059c(float f) {
        RefreshState bVar;
        float f2 = (!this.f9597U0 || this.f9577G0 || f >= 0.0f || this.f9626k1.mo26124b()) ? f : 0.0f;
        if (f2 > ((float) (this.f9598V * 5)) && getTag() == null) {
            Toast.makeText(getContext(), "你这么死拉，臣妾做不到啊！", 0).show();
            setTag("你这么死拉，臣妾做不到啊！");
        }
        if (this.f9634o1 == RefreshState.TwoLevel && f2 > 0.0f) {
            this.f9632n1.mo26104a(Math.min((int) f2, getMeasuredHeight()), true);
        } else if (this.f9634o1 == RefreshState.Refreshing && f2 >= 0.0f) {
            int i = this.f9603Y0;
            if (f2 < ((float) i)) {
                this.f9632n1.mo26104a((int) f2, true);
            } else {
                double d = (double) ((this.f9614e1 - 1.0f) * ((float) i));
                int max = Math.max((this.f9598V * 4) / 3, getHeight());
                int i2 = this.f9603Y0;
                double d2 = (double) (max - i2);
                double max2 = (double) Math.max(0.0f, (f2 - ((float) i2)) * this.f9611d0);
                double d3 = -max2;
                if (d2 == 0.0d) {
                    d2 = 1.0d;
                }
                this.f9632n1.mo26104a(((int) Math.min(d * (1.0d - Math.pow(100.0d, d3 / d2)), max2)) + this.f9603Y0, true);
            }
        } else if (f2 < 0.0f && (this.f9634o1 == RefreshState.Loading || ((this.f9653y0 && this.f9581K0 && this.f9582L0 && mo26068e(this.f9643t0)) || (this.f9573C0 && !this.f9581K0 && mo26068e(this.f9643t0))))) {
            int i3 = this.f9606a1;
            if (f2 > ((float) (-i3))) {
                this.f9632n1.mo26104a((int) f2, true);
            } else {
                double d4 = (double) ((this.f9616f1 - 1.0f) * ((float) i3));
                int max3 = Math.max((this.f9598V * 4) / 3, getHeight());
                int i4 = this.f9606a1;
                double d5 = (double) (max3 - i4);
                double d6 = (double) (-Math.min(0.0f, (((float) i4) + f2) * this.f9611d0));
                double d7 = -d6;
                if (d5 == 0.0d) {
                    d5 = 1.0d;
                }
                this.f9632n1.mo26104a(((int) (-Math.min(d4 * (1.0d - Math.pow(100.0d, d7 / d5)), d6))) - this.f9606a1, true);
            }
        } else if (f2 >= 0.0f) {
            double d8 = (double) (this.f9614e1 * ((float) this.f9603Y0));
            double max4 = (double) Math.max(this.f9598V / 2, getHeight());
            double max5 = (double) Math.max(0.0f, this.f9611d0 * f2);
            double d9 = -max5;
            if (max4 == 0.0d) {
                max4 = 1.0d;
            }
            this.f9632n1.mo26104a((int) Math.min(d8 * (1.0d - Math.pow(100.0d, d9 / max4)), max5), true);
        } else {
            double d10 = (double) (this.f9616f1 * ((float) this.f9606a1));
            double max6 = (double) Math.max(this.f9598V / 2, getHeight());
            double d11 = (double) (-Math.min(0.0f, this.f9611d0 * f2));
            double d12 = -d11;
            if (max6 == 0.0d) {
                max6 = 1.0d;
            }
            this.f9632n1.mo26104a((int) (-Math.min(d10 * (1.0d - Math.pow(100.0d, d12 / max6)), d11)), true);
        }
        if (this.f9573C0 && !this.f9581K0 && mo26068e(this.f9643t0) && f2 < 0.0f && (bVar = this.f9634o1) != RefreshState.Refreshing && bVar != RefreshState.Loading && bVar != RefreshState.LoadFinish) {
            if (this.f9580J0) {
                this.f9656z1 = null;
                this.f9632n1.mo26102a(-this.f9606a1);
            }
            setStateDirectLoading(false);
            this.f9630m1.postDelayed(new C4695f(), (long) this.f9596U);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, int, float, boolean):boolean
     arg types: [int, int, float, int]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, int, android.view.animation.Interpolator, int):android.animation.ValueAnimator
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, int, float, boolean):boolean */
    /* renamed from: b */
    public boolean mo26056b() {
        int i = this.f9648v1 ? 0 : 400;
        int i2 = this.f9596U;
        int i3 = this.f9603Y0;
        float f = ((this.f9614e1 / 2.0f) + 0.5f) * ((float) i3) * 1.0f;
        if (i3 == 0) {
            i3 = 1;
        }
        return mo26051a(i, i2, f / ((float) i3), false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo26052a(boolean z, RefreshInternal hVar) {
        return z || this.f9574D0 || hVar == null || hVar.getSpinnerStyle() == SpinnerStyle.f9744d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ValueAnimator mo26037a(int i, int i2, Interpolator interpolator, int i3) {
        if (this.f9588Q == i) {
            return null;
        }
        ValueAnimator valueAnimator = this.f9571A1;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.f9656z1 = null;
        this.f9571A1 = ValueAnimator.ofInt(this.f9588Q, i);
        this.f9571A1.setDuration((long) i3);
        this.f9571A1.setInterpolator(interpolator);
        this.f9571A1.addListener(new C4693d());
        this.f9571A1.addUpdateListener(new C4694e());
        this.f9571A1.setStartDelay((long) i2);
        this.f9571A1.start();
        return this.f9571A1;
    }

    /* renamed from: d */
    public RefreshLayout mo26062d() {
        return mo26063d(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, int, java.lang.Boolean]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j */
    /* renamed from: d */
    public RefreshLayout mo26063d(boolean z) {
        if (z) {
            return mo26041a(Math.min(Math.max(0, 300 - ((int) (System.currentTimeMillis() - this.f9638q1))), 300) << 16, true, Boolean.FALSE);
        }
        return mo26041a(0, false, (Boolean) null);
    }

    /* renamed from: a */
    public RefreshLayout mo26039a(float f) {
        this.f9614e1 = f;
        RefreshInternal hVar = this.f9622i1;
        if (hVar == null || !this.f9648v1) {
            this.f9604Z0 = this.f9604Z0.mo26137b();
        } else {
            RefreshKernel iVar = this.f9632n1;
            int i = this.f9603Y0;
            hVar.mo24848a(iVar, i, (int) (this.f9614e1 * ((float) i)));
        }
        return this;
    }

    /* renamed from: a */
    public RefreshLayout mo26049a(boolean z) {
        this.f9573C0 = z;
        return this;
    }

    /* renamed from: a */
    public RefreshLayout mo26045a(@NonNull RefreshHeader gVar) {
        return mo26046a(gVar, -1, -2);
    }

    /* renamed from: a */
    public RefreshLayout mo26046a(@NonNull RefreshHeader gVar, int i, int i2) {
        RefreshInternal hVar = this.f9622i1;
        if (hVar != null) {
            super.removeView(hVar.getView());
        }
        this.f9622i1 = gVar;
        this.f9640r1 = 0;
        this.f9644t1 = false;
        this.f9604Z0 = this.f9604Z0.mo26137b();
        if (this.f9622i1.getSpinnerStyle() == SpinnerStyle.f9744d) {
            super.addView(this.f9622i1.getView(), 0, new C4705l(i, i2));
        } else {
            super.addView(this.f9622i1.getView(), i, i2);
        }
        return this;
    }

    /* renamed from: a */
    public RefreshLayout mo26043a(@NonNull RefreshFooter fVar) {
        return mo26044a(fVar, -1, -2);
    }

    /* renamed from: a */
    public RefreshLayout mo26044a(@NonNull RefreshFooter fVar, int i, int i2) {
        RefreshInternal hVar = this.f9624j1;
        if (hVar != null) {
            super.removeView(hVar.getView());
        }
        this.f9624j1 = fVar;
        this.f9650w1 = false;
        this.f9642s1 = 0;
        this.f9582L0 = false;
        this.f9646u1 = false;
        this.f9608b1 = this.f9608b1.mo26137b();
        this.f9643t0 = !this.f9583M0 || this.f9643t0;
        if (this.f9624j1.getSpinnerStyle() == SpinnerStyle.f9744d) {
            super.addView(this.f9624j1.getView(), 0, new C4705l(i, i2));
        } else {
            super.addView(this.f9624j1.getView(), i, i2);
        }
        return this;
    }

    /* renamed from: c */
    public RefreshLayout mo26057c() {
        return mo26058c(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, boolean, int]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j */
    /* renamed from: c */
    public RefreshLayout mo26058c(boolean z) {
        return mo26042a(z ? Math.min(Math.max(0, 300 - ((int) (System.currentTimeMillis() - this.f9638q1))), 300) << 16 : 0, z, false);
    }

    /* renamed from: a */
    public RefreshLayout mo26048a(OnRefreshListener dVar) {
        this.f9587P0 = dVar;
        return this;
    }

    /* renamed from: a */
    public RefreshLayout mo26047a(OnLoadMoreListener bVar) {
        this.f9589Q0 = bVar;
        this.f9643t0 = this.f9643t0 || (!this.f9583M0 && bVar != null);
        return this;
    }

    /* renamed from: a */
    public RefreshLayout mo26041a(int i, boolean z, Boolean bool) {
        int i2 = i >> 16;
        int i3 = (i << 16) >> 16;
        C4696g gVar = new C4696g(i2, bool, z);
        if (i3 > 0) {
            this.f9630m1.postDelayed(gVar, (long) i3);
        } else {
            gVar.run();
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, int, int]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j */
    /* renamed from: a */
    public RefreshLayout mo26040a(int i) {
        return mo26042a(i, true, false);
    }

    /* renamed from: a */
    public RefreshLayout mo26042a(int i, boolean z, boolean z2) {
        int i2 = i >> 16;
        int i3 = (i << 16) >> 16;
        C4697h hVar = new C4697h(i2, z2, z);
        if (i3 > 0) {
            this.f9630m1.postDelayed(hVar, (long) i3);
        } else {
            hVar.run();
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j
     arg types: [int, int, int]
     candidates:
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, java.lang.Boolean):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.f, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(com.scwang.smartrefresh.layout.a.g, int, int):com.scwang.smartrefresh.layout.a.j
      com.scwang.smartrefresh.layout.SmartRefreshLayout.a(int, boolean, boolean):com.scwang.smartrefresh.layout.a.j */
    /* renamed from: a */
    public RefreshLayout mo26038a() {
        return mo26042a(Math.min(Math.max(0, 300 - ((int) (System.currentTimeMillis() - this.f9638q1))), 300) << 16, true, true);
    }

    /* renamed from: a */
    public boolean mo26051a(int i, int i2, float f, boolean z) {
        if (this.f9634o1 != RefreshState.None || !mo26068e(this.f9641s0)) {
            return false;
        }
        C4700i iVar = new C4700i(f, i2, z);
        setViceState(RefreshState.Refreshing);
        if (i > 0) {
            this.f9630m1.postDelayed(iVar, (long) i);
            return true;
        }
        iVar.run();
        return true;
    }
}
