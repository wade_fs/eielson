package com.scwang.smartrefresh.layout.footer;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.R$id;
import com.scwang.smartrefresh.layout.R$layout;
import com.scwang.smartrefresh.layout.R$string;
import com.scwang.smartrefresh.layout.R$styleable;
import com.scwang.smartrefresh.layout.internal.ArrowDrawable;
import com.scwang.smartrefresh.layout.internal.InternalClassics;
import com.scwang.smartrefresh.layout.internal.ProgressDrawable;
import com.scwang.smartrefresh.layout.p206a.RefreshFooter;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public class ClassicsFooter extends InternalClassics<ClassicsFooter> implements RefreshFooter {

    /* renamed from: q0 */
    public static String f9762q0;

    /* renamed from: r0 */
    public static String f9763r0;

    /* renamed from: s0 */
    public static String f9764s0;

    /* renamed from: t0 */
    public static String f9765t0;

    /* renamed from: u0 */
    public static String f9766u0;

    /* renamed from: v0 */
    public static String f9767v0;

    /* renamed from: w0 */
    public static String f9768w0;

    /* renamed from: i0 */
    protected String f9769i0;

    /* renamed from: j0 */
    protected String f9770j0;

    /* renamed from: k0 */
    protected String f9771k0;

    /* renamed from: l0 */
    protected String f9772l0;

    /* renamed from: m0 */
    protected String f9773m0;

    /* renamed from: n0 */
    protected String f9774n0;

    /* renamed from: o0 */
    protected String f9775o0;

    /* renamed from: p0 */
    protected boolean f9776p0;

    /* renamed from: com.scwang.smartrefresh.layout.footer.ClassicsFooter$a */
    static /* synthetic */ class C4709a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9777a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a = r0
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullUpToLoad     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Loading     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.LoadReleased     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToLoad     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.f9777a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.footer.ClassicsFooter.C4709a.<clinit>():void");
        }
    }

    public ClassicsFooter(Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public void mo26129a(@NonNull RefreshLayout jVar, int i, int i2) {
        if (!this.f9776p0) {
            super.mo26129a(jVar, i, i2);
        }
    }

    @Deprecated
    public void setPrimaryColors(@ColorInt int... iArr) {
        if (this.f9853Q == SpinnerStyle.f9744d) {
            super.setPrimaryColors(iArr);
        }
    }

    public ClassicsFooter(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ClassicsFooter(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9776p0 = false;
        View.inflate(context, R$layout.srl_classics_footer, this);
        ImageView imageView = (ImageView) findViewById(R$id.srl_classics_arrow);
        super.f9856T = imageView;
        ImageView imageView2 = (ImageView) findViewById(R$id.srl_classics_progress);
        super.f9857U = imageView2;
        super.f9855S = (TextView) findViewById(R$id.srl_classics_title);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ClassicsFooter);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) imageView2.getLayoutParams();
        layoutParams2.rightMargin = obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsFooter_srlDrawableMarginRight, SmartUtil.m15574a(20.0f));
        layoutParams.rightMargin = layoutParams2.rightMargin;
        layoutParams.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableArrowSize, layoutParams.width);
        layoutParams.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableArrowSize, layoutParams.height);
        layoutParams2.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableProgressSize, layoutParams2.width);
        layoutParams2.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableProgressSize, layoutParams2.height);
        layoutParams.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableSize, layoutParams.width);
        layoutParams.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableSize, layoutParams.height);
        layoutParams2.width = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableSize, layoutParams2.width);
        layoutParams2.height = obtainStyledAttributes.getLayoutDimension(R$styleable.ClassicsFooter_srlDrawableSize, layoutParams2.height);
        super.f9864e0 = obtainStyledAttributes.getInt(R$styleable.ClassicsFooter_srlFinishDuration, super.f9864e0);
        this.f9853Q = SpinnerStyle.f9747g[obtainStyledAttributes.getInt(R$styleable.ClassicsFooter_srlClassicsSpinnerStyle, this.f9853Q.f9748a)];
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlDrawableArrow)) {
            super.f9856T.setImageDrawable(obtainStyledAttributes.getDrawable(R$styleable.ClassicsFooter_srlDrawableArrow));
        } else if (super.f9856T.getDrawable() == null) {
            super.f9859W = new ArrowDrawable();
            super.f9859W.mo26183a(-10066330);
            super.f9856T.setImageDrawable(super.f9859W);
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlDrawableProgress)) {
            super.f9857U.setImageDrawable(obtainStyledAttributes.getDrawable(R$styleable.ClassicsFooter_srlDrawableProgress));
        } else if (super.f9857U.getDrawable() == null) {
            super.f9860a0 = new ProgressDrawable();
            super.f9860a0.mo26183a(-10066330);
            super.f9857U.setImageDrawable(super.f9860a0);
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextSizeTitle)) {
            super.f9855S.setTextSize(0, (float) obtainStyledAttributes.getDimensionPixelSize(R$styleable.ClassicsFooter_srlTextSizeTitle, SmartUtil.m15574a(16.0f)));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlPrimaryColor)) {
            super.mo26179b(obtainStyledAttributes.getColor(R$styleable.ClassicsFooter_srlPrimaryColor, 0));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlAccentColor)) {
            super.mo26163a(obtainStyledAttributes.getColor(R$styleable.ClassicsFooter_srlAccentColor, 0));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextPulling)) {
            this.f9769i0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextPulling);
        } else {
            String str = f9762q0;
            if (str != null) {
                this.f9769i0 = str;
            } else {
                this.f9769i0 = context.getString(R$string.srl_footer_pulling);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextRelease)) {
            this.f9770j0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextRelease);
        } else {
            String str2 = f9763r0;
            if (str2 != null) {
                this.f9770j0 = str2;
            } else {
                this.f9770j0 = context.getString(R$string.srl_footer_release);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextLoading)) {
            this.f9771k0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextLoading);
        } else {
            String str3 = f9764s0;
            if (str3 != null) {
                this.f9771k0 = str3;
            } else {
                this.f9771k0 = context.getString(R$string.srl_footer_loading);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextRefreshing)) {
            this.f9772l0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextRefreshing);
        } else {
            String str4 = f9765t0;
            if (str4 != null) {
                this.f9772l0 = str4;
            } else {
                this.f9772l0 = context.getString(R$string.srl_footer_refreshing);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextFinish)) {
            this.f9773m0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextFinish);
        } else {
            String str5 = f9766u0;
            if (str5 != null) {
                this.f9773m0 = str5;
            } else {
                this.f9773m0 = context.getString(R$string.srl_footer_finish);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextFailed)) {
            this.f9774n0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextFailed);
        } else {
            String str6 = f9767v0;
            if (str6 != null) {
                this.f9774n0 = str6;
            } else {
                this.f9774n0 = context.getString(R$string.srl_footer_failed);
            }
        }
        if (obtainStyledAttributes.hasValue(R$styleable.ClassicsFooter_srlTextNothing)) {
            this.f9775o0 = obtainStyledAttributes.getString(R$styleable.ClassicsFooter_srlTextNothing);
        } else {
            String str7 = f9768w0;
            if (str7 != null) {
                this.f9775o0 = str7;
            } else {
                this.f9775o0 = context.getString(R$string.srl_footer_nothing);
            }
        }
        obtainStyledAttributes.recycle();
        imageView2.animate().setInterpolator(null);
        super.f9855S.setText(isInEditMode() ? this.f9771k0 : this.f9769i0);
        if (isInEditMode()) {
            imageView.setVisibility(8);
        } else {
            imageView2.setVisibility(8);
        }
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        if (this.f9776p0) {
            return 0;
        }
        super.f9855S.setText(z ? this.f9773m0 : this.f9774n0);
        return super.mo24847a(jVar, z);
    }

    /* renamed from: a */
    public boolean mo26127a(boolean z) {
        if (this.f9776p0 == z) {
            return true;
        }
        this.f9776p0 = z;
        ImageView imageView = super.f9856T;
        if (z) {
            super.f9855S.setText(this.f9775o0);
            imageView.setVisibility(8);
            return true;
        }
        super.f9855S.setText(this.f9769i0);
        imageView.setVisibility(0);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        ImageView imageView = super.f9856T;
        if (!this.f9776p0) {
            switch (C4709a.f9777a[bVar2.ordinal()]) {
                case 1:
                    imageView.setVisibility(0);
                    break;
                case 2:
                    break;
                case 3:
                case 4:
                    imageView.setVisibility(8);
                    super.f9855S.setText(this.f9771k0);
                    return;
                case 5:
                    super.f9855S.setText(this.f9770j0);
                    imageView.animate().rotation(0.0f);
                    return;
                case 6:
                    super.f9855S.setText(this.f9772l0);
                    imageView.setVisibility(8);
                    return;
                default:
                    return;
            }
            super.f9855S.setText(this.f9769i0);
            imageView.animate().rotation(180.0f);
        }
    }
}
