package com.scwang.smartrefresh.layout.p206a;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.b */
public interface DefaultRefreshHeaderCreator {
    @NonNull
    /* renamed from: a */
    RefreshHeader mo26114a(@NonNull Context context, @NonNull RefreshLayout jVar);
}
