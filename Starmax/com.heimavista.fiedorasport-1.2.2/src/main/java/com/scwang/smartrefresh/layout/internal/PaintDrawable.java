package com.scwang.smartrefresh.layout.internal;

import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/* renamed from: com.scwang.smartrefresh.layout.internal.b */
public abstract class PaintDrawable extends Drawable {

    /* renamed from: P */
    protected Paint f9871P = new Paint();

    protected PaintDrawable() {
        this.f9871P.setStyle(Paint.Style.FILL);
        this.f9871P.setAntiAlias(true);
        this.f9871P.setColor(-5592406);
    }

    /* renamed from: a */
    public void mo26183a(int i) {
        this.f9871P.setColor(i);
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
        this.f9871P.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f9871P.setColorFilter(colorFilter);
    }
}
