package com.scwang.smartrefresh.layout.p207b;

/* renamed from: com.scwang.smartrefresh.layout.b.b */
public enum RefreshState {
    None(0, false, false, false, false, false),
    PullDownToRefresh(1, true, false, false, false, false),
    PullUpToLoad(2, true, false, false, false, false),
    PullDownCanceled(1, false, false, false, false, false),
    PullUpCanceled(2, false, false, false, false, false),
    ReleaseToRefresh(1, true, false, false, false, true),
    ReleaseToLoad(2, true, false, false, false, true),
    ReleaseToTwoLevel(1, true, false, false, true, true),
    TwoLevelReleased(1, false, false, false, true, false),
    RefreshReleased(1, false, false, false, false, false),
    LoadReleased(2, false, false, false, false, false),
    Refreshing(1, false, true, false, false, false),
    Loading(2, false, true, false, false, false),
    TwoLevel(1, false, true, false, true, false),
    RefreshFinish(1, false, false, true, false, false),
    LoadFinish(2, false, false, true, false, false),
    TwoLevelFinish(1, false, false, true, true, false);
    

    /* renamed from: P */
    public final boolean f9735P;

    /* renamed from: Q */
    public final boolean f9736Q;

    /* renamed from: R */
    public final boolean f9737R;

    /* renamed from: S */
    public final boolean f9738S;

    /* renamed from: T */
    public final boolean f9739T;

    /* renamed from: U */
    public final boolean f9740U;

    /* renamed from: V */
    public final boolean f9741V;

    private RefreshState(int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        boolean z6 = false;
        this.f9735P = i == 1;
        this.f9736Q = i == 2 ? true : z6;
        this.f9738S = z;
        this.f9739T = z2;
        this.f9740U = z3;
        this.f9737R = z4;
        this.f9741V = z5;
    }

    /* renamed from: a */
    public RefreshState mo26138a() {
        return (!this.f9735P || this.f9737R) ? this : values()[ordinal() + 1];
    }

    /* renamed from: h */
    public RefreshState mo26139h() {
        return (!this.f9736Q || this.f9737R) ? this : values()[ordinal() - 1];
    }
}
