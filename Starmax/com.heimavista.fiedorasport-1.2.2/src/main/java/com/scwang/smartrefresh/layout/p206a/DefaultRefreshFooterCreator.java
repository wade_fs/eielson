package com.scwang.smartrefresh.layout.p206a;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.a */
public interface DefaultRefreshFooterCreator {
    @NonNull
    /* renamed from: a */
    RefreshFooter mo26113a(@NonNull Context context, @NonNull RefreshLayout jVar);
}
