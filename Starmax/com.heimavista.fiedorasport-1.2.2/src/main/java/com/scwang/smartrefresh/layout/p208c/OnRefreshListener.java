package com.scwang.smartrefresh.layout.p208c;

import androidx.annotation.NonNull;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;

/* renamed from: com.scwang.smartrefresh.layout.c.d */
public interface OnRefreshListener {
    /* renamed from: a */
    void mo24707a(@NonNull RefreshLayout jVar);
}
