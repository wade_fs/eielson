package com.scwang.smartrefresh.layout.p208c;

import com.scwang.smartrefresh.layout.p206a.RefreshFooter;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;

/* renamed from: com.scwang.smartrefresh.layout.c.c */
public interface OnMultiPurposeListener extends OnRefreshLoadMoreListener, OnStateChangedListener {
    /* renamed from: a */
    void mo26141a(RefreshFooter fVar, int i, int i2);

    /* renamed from: a */
    void mo26142a(RefreshFooter fVar, boolean z);

    /* renamed from: a */
    void mo26143a(RefreshFooter fVar, boolean z, float f, int i, int i2, int i3);

    /* renamed from: a */
    void mo26144a(RefreshHeader gVar, int i, int i2);

    /* renamed from: a */
    void mo26145a(RefreshHeader gVar, boolean z);

    /* renamed from: a */
    void mo26146a(RefreshHeader gVar, boolean z, float f, int i, int i2, int i3);

    /* renamed from: b */
    void mo26147b(RefreshFooter fVar, int i, int i2);

    /* renamed from: b */
    void mo26148b(RefreshHeader gVar, int i, int i2);
}
