package com.scwang.smartrefresh.layout.footer;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.ColorUtils;
import com.scwang.smartrefresh.layout.R$styleable;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.RefreshFooter;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public class BallPulseFooter extends InternalAbstract implements RefreshFooter {

    /* renamed from: S */
    protected boolean f9753S;

    /* renamed from: T */
    protected boolean f9754T;

    /* renamed from: U */
    protected Paint f9755U;

    /* renamed from: V */
    protected int f9756V;

    /* renamed from: W */
    protected int f9757W;

    /* renamed from: a0 */
    protected float f9758a0;

    /* renamed from: b0 */
    protected long f9759b0;

    /* renamed from: c0 */
    protected boolean f9760c0;

    /* renamed from: d0 */
    protected TimeInterpolator f9761d0;

    public BallPulseFooter(@NonNull Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public void mo26129a(@NonNull RefreshLayout jVar, int i, int i2) {
        if (!this.f9760c0) {
            invalidate();
            this.f9760c0 = true;
            this.f9759b0 = System.currentTimeMillis();
            this.f9755U.setColor(this.f9757W);
        }
    }

    /* renamed from: b */
    public BallPulseFooter mo26151b(@ColorInt int i) {
        this.f9756V = i;
        this.f9753S = true;
        if (!this.f9760c0) {
            this.f9755U.setColor(i);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        int width = getWidth();
        int height = getHeight();
        float f = this.f9758a0;
        float min = (((float) Math.min(width, height)) - (f * 2.0f)) / 6.0f;
        float f2 = min * 2.0f;
        float f3 = (((float) width) / 2.0f) - (f + f2);
        float f4 = ((float) height) / 2.0f;
        long currentTimeMillis = System.currentTimeMillis();
        int i = 0;
        while (i < 3) {
            int i2 = i + 1;
            long j = (currentTimeMillis - this.f9759b0) - ((long) (i2 * 120));
            float interpolation = this.f9761d0.getInterpolation(j > 0 ? ((float) (j % 750)) / 750.0f : 0.0f);
            canvas.save();
            float f5 = (float) i;
            canvas2.translate((f2 * f5) + f3 + (this.f9758a0 * f5), f4);
            if (((double) interpolation) < 0.5d) {
                float f6 = 1.0f - ((interpolation * 2.0f) * 0.7f);
                canvas2.scale(f6, f6);
            } else {
                float f7 = ((interpolation * 2.0f) * 0.7f) - 0.4f;
                canvas2.scale(f7, f7);
            }
            canvas2.drawCircle(0.0f, 0.0f, min, this.f9755U);
            canvas.restore();
            i = i2;
        }
        super.dispatchDraw(canvas);
        if (this.f9760c0) {
            invalidate();
        }
    }

    @Deprecated
    public void setPrimaryColors(@ColorInt int... iArr) {
        if (!this.f9754T && iArr.length > 1) {
            mo26150a(iArr[0]);
            this.f9754T = false;
        }
        if (!this.f9753S) {
            if (iArr.length > 1) {
                mo26151b(iArr[1]);
            } else if (iArr.length > 0) {
                mo26151b(ColorUtils.compositeColors(-1711276033, iArr[0]));
            }
            this.f9753S = false;
        }
    }

    public BallPulseFooter(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BallPulseFooter(@NonNull Context context, @Nullable AttributeSet attributeSet, @AttrRes int i) {
        super(context, attributeSet, i);
        this.f9756V = -1118482;
        this.f9757W = -1615546;
        this.f9759b0 = 0;
        this.f9760c0 = false;
        this.f9761d0 = new AccelerateDecelerateInterpolator();
        setMinimumHeight(SmartUtil.m15574a(60.0f));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.BallPulseFooter);
        this.f9755U = new Paint();
        this.f9755U.setColor(-1);
        this.f9755U.setStyle(Paint.Style.FILL);
        this.f9755U.setAntiAlias(true);
        super.f9853Q = SpinnerStyle.f9742b;
        super.f9853Q = SpinnerStyle.f9747g[obtainStyledAttributes.getInt(R$styleable.BallPulseFooter_srlClassicsSpinnerStyle, super.f9853Q.f9748a)];
        if (obtainStyledAttributes.hasValue(R$styleable.BallPulseFooter_srlNormalColor)) {
            mo26151b(obtainStyledAttributes.getColor(R$styleable.BallPulseFooter_srlNormalColor, 0));
        }
        if (obtainStyledAttributes.hasValue(R$styleable.BallPulseFooter_srlAnimatingColor)) {
            mo26150a(obtainStyledAttributes.getColor(R$styleable.BallPulseFooter_srlAnimatingColor, 0));
        }
        obtainStyledAttributes.recycle();
        this.f9758a0 = (float) SmartUtil.m15574a(4.0f);
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        this.f9760c0 = false;
        this.f9759b0 = 0;
        this.f9755U.setColor(this.f9756V);
        return 0;
    }

    /* renamed from: a */
    public BallPulseFooter mo26150a(@ColorInt int i) {
        this.f9757W = i;
        this.f9754T = true;
        if (this.f9760c0) {
            this.f9755U.setColor(i);
        }
        return this;
    }
}
