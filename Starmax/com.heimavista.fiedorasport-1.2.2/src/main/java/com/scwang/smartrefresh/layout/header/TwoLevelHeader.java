package com.scwang.smartrefresh.layout.header;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.scwang.smartrefresh.layout.R$styleable;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.OnTwoLevelListener;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshInternal;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;

public class TwoLevelHeader extends InternalAbstract implements RefreshHeader {

    /* renamed from: S */
    protected int f9827S;

    /* renamed from: T */
    protected float f9828T;

    /* renamed from: U */
    protected float f9829U;

    /* renamed from: V */
    protected float f9830V;

    /* renamed from: W */
    protected float f9831W;

    /* renamed from: a0 */
    protected boolean f9832a0;

    /* renamed from: b0 */
    protected boolean f9833b0;

    /* renamed from: c0 */
    protected int f9834c0;

    /* renamed from: d0 */
    protected int f9835d0;

    /* renamed from: e0 */
    protected RefreshInternal f9836e0;

    /* renamed from: f0 */
    protected RefreshKernel f9837f0;

    /* renamed from: g0 */
    protected OnTwoLevelListener f9838g0;

    /* renamed from: com.scwang.smartrefresh.layout.header.TwoLevelHeader$a */
    static /* synthetic */ class C4713a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9839a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.f9839a = r0
                int[] r0 = com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.f9839a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevelReleased     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.f9839a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevel     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.f9839a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.TwoLevelFinish     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.f9839a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.scwang.smartrefresh.layout.header.TwoLevelHeader.C4713a.<clinit>():void");
        }
    }

    public TwoLevelHeader(@NonNull Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public void mo24848a(@NonNull RefreshKernel iVar, int i, int i2) {
        RefreshInternal hVar = this.f9836e0;
        if (hVar != null) {
            if ((((float) (i2 + i)) * 1.0f) / ((float) i) != this.f9829U && this.f9835d0 == 0) {
                this.f9835d0 = i;
                this.f9836e0 = null;
                iVar.mo26111b().mo26039a(this.f9829U);
                this.f9836e0 = hVar;
            }
            if (this.f9837f0 == null && hVar.getSpinnerStyle() == SpinnerStyle.f9742b && !isInEditMode()) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) hVar.getView().getLayoutParams();
                marginLayoutParams.topMargin -= i;
                hVar.getView().setLayoutParams(marginLayoutParams);
            }
            this.f9835d0 = i;
            this.f9837f0 = iVar;
            iVar.mo26109b(this.f9834c0);
            iVar.mo26110b(this, !this.f9833b0);
            hVar.mo24848a(iVar, i, i2);
        }
    }

    public boolean equals(Object obj) {
        RefreshInternal hVar = this.f9836e0;
        return (hVar != null && hVar.equals(obj)) || super.equals(obj);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        super.f9853Q = SpinnerStyle.f9746f;
        if (this.f9836e0 == null) {
            mo26166a(new ClassicsHeader(getContext()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        super.f9853Q = SpinnerStyle.f9744d;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            View childAt = getChildAt(i);
            if (childAt instanceof RefreshHeader) {
                this.f9836e0 = (RefreshHeader) childAt;
                super.f9854R = (RefreshInternal) childAt;
                bringChildToFront(childAt);
                break;
            }
            i++;
        }
        if (this.f9836e0 == null) {
            mo26166a(new ClassicsHeader(getContext()));
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        RefreshInternal hVar = this.f9836e0;
        if (hVar == null) {
            super.onMeasure(i, i2);
        } else if (View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            hVar.getView().measure(i, i2);
            super.setMeasuredDimension(View.resolveSize(super.getSuggestedMinimumWidth(), i), hVar.getView().getMeasuredHeight());
        } else {
            super.onMeasure(i, i2);
        }
    }

    public TwoLevelHeader(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TwoLevelHeader(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9828T = 0.0f;
        this.f9829U = 2.5f;
        this.f9830V = 1.9f;
        this.f9831W = 1.0f;
        this.f9832a0 = true;
        this.f9833b0 = true;
        this.f9834c0 = 1000;
        super.f9853Q = SpinnerStyle.f9744d;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.TwoLevelHeader);
        this.f9829U = obtainStyledAttributes.getFloat(R$styleable.TwoLevelHeader_srlMaxRage, this.f9829U);
        this.f9830V = obtainStyledAttributes.getFloat(R$styleable.TwoLevelHeader_srlFloorRage, this.f9830V);
        this.f9831W = obtainStyledAttributes.getFloat(R$styleable.TwoLevelHeader_srlRefreshRage, this.f9831W);
        this.f9834c0 = obtainStyledAttributes.getInt(R$styleable.TwoLevelHeader_srlFloorDuration, this.f9834c0);
        this.f9832a0 = obtainStyledAttributes.getBoolean(R$styleable.TwoLevelHeader_srlEnableTwoLevel, this.f9832a0);
        this.f9833b0 = obtainStyledAttributes.getBoolean(R$styleable.TwoLevelHeader_srlEnablePullToCloseTwoLevel, this.f9833b0);
        obtainStyledAttributes.recycle();
    }

    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        RefreshInternal hVar = this.f9836e0;
        if (hVar != null) {
            hVar.mo24849a(jVar, bVar, bVar2);
            int i = C4713a.f9839a[bVar2.ordinal()];
            boolean z = true;
            if (i == 1) {
                if (hVar.getView() != this) {
                    hVar.getView().animate().alpha(0.0f).setDuration((long) (this.f9834c0 / 2));
                }
                RefreshKernel iVar = this.f9837f0;
                if (iVar != null) {
                    OnTwoLevelListener dVar = this.f9838g0;
                    if (dVar != null && !dVar.mo26116a(jVar)) {
                        z = false;
                    }
                    iVar.mo26108a(z);
                }
            } else if (i == 2) {
            } else {
                if (i != 3) {
                    if (i == 4 && hVar.getView().getAlpha() == 0.0f && hVar.getView() != this) {
                        hVar.getView().setAlpha(1.0f);
                    }
                } else if (hVar.getView() != this) {
                    hVar.getView().animate().alpha(1.0f).setDuration((long) (this.f9834c0 / 2));
                }
            }
        }
    }

    /* renamed from: a */
    public void mo24850a(boolean z, float f, int i, int i2, int i3) {
        mo26168a(i);
        RefreshInternal hVar = this.f9836e0;
        RefreshKernel iVar = this.f9837f0;
        if (hVar != null) {
            hVar.mo24850a(z, f, i, i2, i3);
        }
        if (z) {
            float f2 = this.f9828T;
            float f3 = this.f9830V;
            if (f2 < f3 && f >= f3 && this.f9832a0) {
                iVar.mo26107a(RefreshState.ReleaseToTwoLevel);
            } else if (this.f9828T < this.f9830V || f >= this.f9831W) {
                float f4 = this.f9828T;
                float f5 = this.f9830V;
                if (f4 >= f5 && f < f5) {
                    iVar.mo26107a(RefreshState.ReleaseToRefresh);
                }
            } else {
                iVar.mo26107a(RefreshState.PullDownToRefresh);
            }
            this.f9828T = f;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26168a(int i) {
        RefreshInternal hVar = this.f9836e0;
        if (this.f9827S != i && hVar != null) {
            this.f9827S = i;
            SpinnerStyle spinnerStyle = hVar.getSpinnerStyle();
            if (spinnerStyle == SpinnerStyle.f9742b) {
                hVar.getView().setTranslationY((float) i);
            } else if (spinnerStyle == SpinnerStyle.f9743c) {
                View view = hVar.getView();
                view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getTop() + Math.max(0, i));
            }
        }
    }

    /* renamed from: a */
    public TwoLevelHeader mo26166a(RefreshHeader gVar) {
        return mo26167a(gVar, -1, -2);
    }

    /* renamed from: a */
    public TwoLevelHeader mo26167a(RefreshHeader gVar, int i, int i2) {
        if (gVar != null) {
            RefreshInternal hVar = this.f9836e0;
            if (hVar != null) {
                removeView(hVar.getView());
            }
            if (gVar.getSpinnerStyle() == SpinnerStyle.f9744d) {
                addView(gVar.getView(), 0, new RelativeLayout.LayoutParams(i, i2));
            } else {
                addView(gVar.getView(), i, i2);
            }
            this.f9836e0 = gVar;
            super.f9854R = gVar;
        }
        return this;
    }
}
