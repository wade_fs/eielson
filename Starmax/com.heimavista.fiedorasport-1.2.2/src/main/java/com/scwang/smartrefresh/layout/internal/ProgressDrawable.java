package com.scwang.smartrefresh.layout.internal;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.internal.c */
public class ProgressDrawable extends PaintDrawable implements Animatable, ValueAnimator.AnimatorUpdateListener {

    /* renamed from: Q */
    protected int f9872Q = 0;

    /* renamed from: R */
    protected int f9873R = 0;

    /* renamed from: S */
    protected int f9874S = 0;

    /* renamed from: T */
    protected ValueAnimator f9875T = ValueAnimator.ofInt(30, 3600);

    /* renamed from: U */
    protected Path f9876U = new Path();

    public ProgressDrawable() {
        this.f9875T.setDuration(10000L);
        this.f9875T.setInterpolator(null);
        this.f9875T.setRepeatCount(-1);
        this.f9875T.setRepeatMode(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void draw(@NonNull Canvas canvas) {
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        float f = (float) width;
        float max = Math.max(1.0f, f / 22.0f);
        if (!(this.f9872Q == width && this.f9873R == height)) {
            this.f9876U.reset();
            float f2 = f - max;
            float f3 = ((float) height) / 2.0f;
            this.f9876U.addCircle(f2, f3, max, Path.Direction.CW);
            float f4 = f - (5.0f * max);
            this.f9876U.addRect(f4, f3 - max, f2, f3 + max, Path.Direction.CW);
            this.f9876U.addCircle(f4, f3, max, Path.Direction.CW);
            this.f9872Q = width;
            this.f9873R = height;
        }
        canvas.save();
        float f5 = f / 2.0f;
        float f6 = ((float) height) / 2.0f;
        canvas.rotate((float) this.f9874S, f5, f6);
        for (int i = 0; i < 12; i++) {
            super.f9871P.setAlpha((i + 5) * 17);
            canvas.rotate(30.0f, f5, f6);
            canvas.drawPath(this.f9876U, super.f9871P);
        }
        canvas.restore();
    }

    public boolean isRunning() {
        return this.f9875T.isRunning();
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f9874S = (((Integer) valueAnimator.getAnimatedValue()).intValue() / 30) * 30;
        invalidateSelf();
    }

    public void start() {
        if (!this.f9875T.isRunning()) {
            this.f9875T.addUpdateListener(this);
            this.f9875T.start();
        }
    }

    public void stop() {
        if (this.f9875T.isRunning()) {
            this.f9875T.removeAllListeners();
            this.f9875T.removeAllUpdateListeners();
            this.f9875T.cancel();
        }
    }
}
