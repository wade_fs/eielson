package com.scwang.smartrefresh.layout.p206a;

import android.view.View;

/* renamed from: com.scwang.smartrefresh.layout.a.k */
public interface ScrollBoundaryDecider {
    /* renamed from: a */
    boolean mo26133a(View view);

    /* renamed from: b */
    boolean mo26134b(View view);
}
