package com.scwang.smartrefresh.layout.p206a;

import android.animation.ValueAnimator;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.NonNull;

/* renamed from: com.scwang.smartrefresh.layout.a.e */
public interface RefreshContent {
    /* renamed from: a */
    ValueAnimator.AnimatorUpdateListener mo26117a(int i);

    /* renamed from: a */
    void mo26118a(int i, int i2, int i3);

    /* renamed from: a */
    void mo26119a(MotionEvent motionEvent);

    /* renamed from: a */
    void mo26120a(RefreshKernel iVar, View view, View view2);

    /* renamed from: a */
    void mo26121a(ScrollBoundaryDecider kVar);

    /* renamed from: a */
    void mo26122a(boolean z);

    /* renamed from: a */
    boolean mo26123a();

    /* renamed from: b */
    boolean mo26124b();

    @NonNull
    /* renamed from: c */
    View mo26125c();

    @NonNull
    View getView();
}
