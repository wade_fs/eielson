package com.linecorp.linesdk;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LineFriendProfile extends LineProfile {
    public static final Parcelable.Creator<LineFriendProfile> CREATOR = new Parcelable.Creator<LineFriendProfile>() {
        /* class com.linecorp.linesdk.LineFriendProfile.C46371 */

        public LineFriendProfile createFromParcel(Parcel parcel) {
            return new LineFriendProfile(parcel);
        }

        public LineFriendProfile[] newArray(int i) {
            return new LineFriendProfile[i];
        }
    };
    @Nullable
    private final String overriddenDisplayName;

    public LineFriendProfile(@NonNull String str, @NonNull String str2, @Nullable Uri uri, @Nullable String str3, @NonNull String str4) {
        super(str, str2, uri, str3);
        this.overriddenDisplayName = str4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LineFriendProfile) || !super.equals(obj)) {
            return false;
        }
        String str = this.overriddenDisplayName;
        String str2 = ((LineFriendProfile) obj).overriddenDisplayName;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    @NonNull
    public String getAvailableDisplayName() {
        if (!TextUtils.isEmpty(this.overriddenDisplayName)) {
            return this.overriddenDisplayName;
        }
        return getDisplayName();
    }

    @Nullable
    public String getOverriddenDisplayName() {
        return this.overriddenDisplayName;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.overriddenDisplayName;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public String toString() {
        return "LineFriendProfile{userId='" + getUserId() + '\'' + ", displayName='" + getDisplayName() + '\'' + ", pictureUrl=" + getPictureUrl() + ", statusMessage='" + getStatusMessage() + '\'' + ", overriddenDisplayName='" + this.overriddenDisplayName + '\'' + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.overriddenDisplayName);
    }

    protected LineFriendProfile(@NonNull Parcel parcel) {
        super(parcel);
        this.overriddenDisplayName = parcel.readString();
    }
}
