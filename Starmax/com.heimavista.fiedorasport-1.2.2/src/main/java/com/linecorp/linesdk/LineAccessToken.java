package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import p119e.p220f.p221a.p222a.SecurityUtils;

public class LineAccessToken implements Parcelable {
    public static final Parcelable.Creator<LineAccessToken> CREATOR = new Parcelable.Creator<LineAccessToken>() {
        /* class com.linecorp.linesdk.LineAccessToken.C46341 */

        public LineAccessToken createFromParcel(Parcel parcel) {
            return new LineAccessToken(parcel);
        }

        public LineAccessToken[] newArray(int i) {
            return new LineAccessToken[i];
        }
    };
    @NonNull
    private final String accessToken;
    private final long expiresInMillis;
    private final long issuedClientTimeMillis;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineAccessToken.class != obj.getClass()) {
            return false;
        }
        LineAccessToken lineAccessToken = (LineAccessToken) obj;
        if (this.expiresInMillis == lineAccessToken.expiresInMillis && this.issuedClientTimeMillis == lineAccessToken.issuedClientTimeMillis) {
            return this.accessToken.equals(lineAccessToken.accessToken);
        }
        return false;
    }

    public long getEstimatedExpirationTimeMillis() {
        return getIssuedClientTimeMillis() + getExpiresInMillis();
    }

    public long getExpiresInMillis() {
        return this.expiresInMillis;
    }

    public long getIssuedClientTimeMillis() {
        return this.issuedClientTimeMillis;
    }

    @NonNull
    public String getTokenString() {
        return this.accessToken;
    }

    public int hashCode() {
        long j = this.expiresInMillis;
        long j2 = this.issuedClientTimeMillis;
        return (((this.accessToken.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "LineAccessToken{accessToken='" + SecurityUtils.m17194a(this.accessToken) + '\'' + ", expiresInMillis=" + this.expiresInMillis + ", issuedClientTimeMillis=" + this.issuedClientTimeMillis + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.accessToken);
        parcel.writeLong(this.expiresInMillis);
        parcel.writeLong(this.issuedClientTimeMillis);
    }

    public LineAccessToken(@NonNull String str, long j, long j2) {
        this.accessToken = str;
        this.expiresInMillis = j;
        this.issuedClientTimeMillis = j2;
    }

    private LineAccessToken(@NonNull Parcel parcel) {
        this.accessToken = parcel.readString();
        this.expiresInMillis = parcel.readLong();
        this.issuedClientTimeMillis = parcel.readLong();
    }
}
