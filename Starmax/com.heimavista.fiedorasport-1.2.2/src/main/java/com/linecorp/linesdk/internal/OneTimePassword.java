package com.linecorp.linesdk.internal;

import androidx.annotation.NonNull;

public class OneTimePassword {
    @NonNull

    /* renamed from: id */
    private final String f9550id;
    @NonNull
    private final String password;

    public OneTimePassword(@NonNull String str, @NonNull String str2) {
        this.f9550id = str;
        this.password = str2;
    }

    @NonNull
    public String getId() {
        return this.f9550id;
    }

    @NonNull
    public String getPassword() {
        return this.password;
    }

    public String toString() {
        return "OneTimeIdAndPassword{id='" + "#####" + '\'' + ", password='" + "#####" + '\'' + '}';
    }
}
