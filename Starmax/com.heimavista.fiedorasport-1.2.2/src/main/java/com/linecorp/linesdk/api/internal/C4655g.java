package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;
import java.util.List;

/* renamed from: com.linecorp.linesdk.api.internal.g */
/* compiled from: lambda */
public final /* synthetic */ class C4655g implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9525a;

    /* renamed from: b */
    private final /* synthetic */ List f9526b;

    /* renamed from: c */
    private final /* synthetic */ List f9527c;

    /* renamed from: d */
    private final /* synthetic */ boolean f9528d;

    public /* synthetic */ C4655g(LineApiClientImpl lineApiClientImpl, List list, List list2, boolean z) {
        this.f9525a = lineApiClientImpl;
        this.f9526b = list;
        this.f9527c = list2;
        this.f9528d = z;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9525a.mo25662a(this.f9526b, this.f9527c, this.f9528d, internalAccessToken);
    }
}
