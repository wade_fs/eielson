package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import org.json.JSONObject;

public abstract class MessageData implements Jsonable {
    @NonNull
    public abstract Type getType();

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", getType().name().toLowerCase());
        return jSONObject;
    }
}
