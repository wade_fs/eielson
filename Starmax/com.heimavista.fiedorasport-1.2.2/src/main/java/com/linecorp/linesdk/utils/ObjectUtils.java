package com.linecorp.linesdk.utils;

public final class ObjectUtils {
    public static <T> T defaultIfNull(T t, T t2) {
        return t != null ? t : t2;
    }
}
