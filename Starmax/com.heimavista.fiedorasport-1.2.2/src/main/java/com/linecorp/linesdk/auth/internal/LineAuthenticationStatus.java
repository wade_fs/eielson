package com.linecorp.linesdk.auth.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.internal.OneTimePassword;

class LineAuthenticationStatus implements Parcelable {
    public static final Parcelable.Creator<LineAuthenticationStatus> CREATOR = new Parcelable.Creator<LineAuthenticationStatus>() {
        /* class com.linecorp.linesdk.auth.internal.LineAuthenticationStatus.C46631 */

        public LineAuthenticationStatus createFromParcel(Parcel parcel) {
            return new LineAuthenticationStatus(parcel);
        }

        public LineAuthenticationStatus[] newArray(int i) {
            return new LineAuthenticationStatus[i];
        }
    };
    @Nullable
    private String oAuthState;
    @Nullable
    private OneTimePassword oneTimePassword;
    @Nullable
    private String openIdNonce;
    @Nullable
    private String sentRedirectUri;
    private Status status;

    enum Status {
        INIT,
        STARTED,
        INTENT_RECEIVED,
        INTENT_HANDLED
    }

    /* access modifiers changed from: package-private */
    public void authenticationIntentHandled() {
        this.status = Status.INTENT_HANDLED;
    }

    /* access modifiers changed from: package-private */
    public void authenticationIntentReceived() {
        this.status = Status.INTENT_RECEIVED;
    }

    public void authenticationStarted() {
        this.status = Status.STARTED;
    }

    public int describeContents() {
        return 0;
    }

    @Nullable
    public String getOAuthState() {
        return this.oAuthState;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public OneTimePassword getOneTimePassword() {
        return this.oneTimePassword;
    }

    @Nullable
    public String getOpenIdNonce() {
        return this.openIdNonce;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getSentRedirectUri() {
        return this.sentRedirectUri;
    }

    @NonNull
    public Status getStatus() {
        return this.status;
    }

    public void setOAuthState(@Nullable String str) {
        this.oAuthState = str;
    }

    /* access modifiers changed from: package-private */
    public void setOneTimePassword(@Nullable OneTimePassword oneTimePassword2) {
        this.oneTimePassword = oneTimePassword2;
    }

    public void setOpenIdNonce(@Nullable String str) {
        this.openIdNonce = str;
    }

    /* access modifiers changed from: package-private */
    public void setSentRedirectUri(@Nullable String str) {
        this.sentRedirectUri = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        OneTimePassword oneTimePassword2 = this.oneTimePassword;
        String str = null;
        parcel.writeString(oneTimePassword2 == null ? null : oneTimePassword2.getId());
        OneTimePassword oneTimePassword3 = this.oneTimePassword;
        if (oneTimePassword3 != null) {
            str = oneTimePassword3.getPassword();
        }
        parcel.writeString(str);
        parcel.writeString(this.sentRedirectUri);
        parcel.writeByte((byte) this.status.ordinal());
        parcel.writeString(this.oAuthState);
        parcel.writeString(this.openIdNonce);
    }

    LineAuthenticationStatus() {
        this.status = Status.INIT;
    }

    private LineAuthenticationStatus(@NonNull Parcel parcel) {
        this.status = Status.INIT;
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        this.oneTimePassword = (TextUtils.isEmpty(readString) || TextUtils.isEmpty(readString2)) ? null : new OneTimePassword(readString, readString2);
        this.sentRedirectUri = parcel.readString();
        this.status = Status.values()[parcel.readByte()];
        this.oAuthState = parcel.readString();
        this.openIdNonce = parcel.readString();
    }
}
