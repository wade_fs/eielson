package com.linecorp.linesdk.dialog.internal;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.dialog.internal.TargetListAdapter;
import java.util.List;

public class TargetListWithSearchView extends ConstraintLayout {
    /* access modifiers changed from: private */
    public AppCompatTextView emptyView;
    private TargetListAdapter.OnSelectedChangeListener listener;
    /* access modifiers changed from: private */
    public int noMembersResId;
    /* access modifiers changed from: private */
    public RecyclerView recyclerView;
    /* access modifiers changed from: private */
    public SearchView searchView;

    public TargetListWithSearchView(Context context, @StringRes int i, TargetListAdapter.OnSelectedChangeListener onSelectedChangeListener) {
        super(context);
        this.noMembersResId = i;
        this.listener = onSelectedChangeListener;
        init();
    }

    private void init() {
        View inflate = ViewGroup.inflate(getContext(), C4642R.C4646layout.layout_select_target, this);
        this.recyclerView = (RecyclerView) inflate.findViewById(C4642R.C4645id.recyclerView);
        this.searchView = (SearchView) inflate.findViewById(C4642R.C4645id.searchView);
        this.emptyView = (AppCompatTextView) inflate.findViewById(C4642R.C4645id.emptyView);
        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            /* class com.linecorp.linesdk.dialog.internal.TargetListWithSearchView.C46731 */

            private void searchText(String str) {
                TargetListAdapter targetListAdapter = (TargetListAdapter) TargetListWithSearchView.this.recyclerView.getAdapter();
                if (targetListAdapter == null) {
                    return;
                }
                if (targetListAdapter.filter(str) == 0) {
                    TargetListWithSearchView.this.emptyView.setVisibility(0);
                    if (!str.isEmpty()) {
                        TargetListWithSearchView.this.emptyView.setText(C4642R.string.search_no_results);
                    } else {
                        TargetListWithSearchView.this.emptyView.setText(TargetListWithSearchView.this.noMembersResId);
                    }
                } else {
                    TargetListWithSearchView.this.emptyView.setVisibility(4);
                }
            }

            public boolean onQueryTextChange(String str) {
                searchText(str);
                return true;
            }

            public boolean onQueryTextSubmit(String str) {
                searchText(str);
                TargetListWithSearchView.this.searchView.clearFocus();
                return true;
            }
        });
    }

    public void addTargetUsers(List<TargetUser> list) {
        TargetListAdapter targetListAdapter = (TargetListAdapter) this.recyclerView.getAdapter();
        if (targetListAdapter == null) {
            this.recyclerView.setAdapter(new TargetListAdapter(list, this.listener));
        } else {
            targetListAdapter.addAll(list);
        }
        if (this.recyclerView.getAdapter().getItemCount() == 0) {
            this.emptyView.setText(this.noMembersResId);
            this.emptyView.setVisibility(0);
        }
    }

    public void unSelect(TargetUser targetUser) {
        TargetListAdapter targetListAdapter = (TargetListAdapter) this.recyclerView.getAdapter();
        if (targetListAdapter != null) {
            targetListAdapter.unSelect(targetUser);
        }
    }

    public TargetListWithSearchView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public TargetListWithSearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
