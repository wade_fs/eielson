package com.linecorp.linesdk.internal.nwclient.core;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Locale;

class UserAgentGenerator {
    private static final String DEFAULT_PACKAGE_NAME = "UNK";
    private static final String DEFAULT_VERSION_NAME = "UNK";
    @Nullable
    private String cachedUserAgent;
    @Nullable
    private final PackageInfo packageInfo;
    @NonNull
    private final String sdkVersion;

    UserAgentGenerator(@NonNull Context context, @NonNull String str) {
        this.packageInfo = getPackageInfo(context);
        this.sdkVersion = str;
    }

    @Nullable
    private static PackageInfo getPackageInfo(@NonNull Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException unused) {
            throw null;
        }
    }

    @NonNull
    public String getUserAgent() {
        String str = this.cachedUserAgent;
        if (str != null) {
            return str;
        }
        PackageInfo packageInfo2 = this.packageInfo;
        String str2 = "UNK";
        String str3 = packageInfo2 == null ? str2 : packageInfo2.packageName;
        PackageInfo packageInfo3 = this.packageInfo;
        if (packageInfo3 != null) {
            str2 = packageInfo3.versionName;
        }
        Locale locale = Locale.getDefault();
        this.cachedUserAgent = str3 + "/" + str2 + " ChannelSDK/" + this.sdkVersion + " (Linux; U; Android " + Build.VERSION.RELEASE + "; " + locale.getLanguage() + "-" + locale.getCountry() + "; " + Build.MODEL + " Build/" + Build.ID + ")";
        return this.cachedUserAgent;
    }
}
