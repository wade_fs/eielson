package com.linecorp.linesdk.internal.nwclient;

import androidx.annotation.NonNull;
import com.linecorp.linesdk.internal.JWKSet;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import p241io.jsonwebtoken.JwsHeader;

public class JWKSetParser extends JsonToObjectBaseResponseParser<JWKSet> {
    /* access modifiers changed from: protected */
    @NonNull
    public JWKSet parseJsonToObject(@NonNull JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("keys");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            arrayList.add(new JWKSet.JWK.Builder().keyType(jSONObject2.getString("kty")).algorithm(jSONObject2.getString(JwsHeader.ALGORITHM)).use(jSONObject2.getString("use")).keyId(jSONObject2.getString(JwsHeader.KEY_ID)).curve(jSONObject2.getString("crv")).mo25877x(jSONObject2.getString("x")).mo25878y(jSONObject2.getString("y")).build());
        }
        return new JWKSet.Builder().keys(arrayList).build();
    }
}
