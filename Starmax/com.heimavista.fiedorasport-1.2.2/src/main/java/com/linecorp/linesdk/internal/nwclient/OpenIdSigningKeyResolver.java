package com.linecorp.linesdk.internal.nwclient;

import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.internal.JWKSet;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import p241io.jsonwebtoken.Claims;
import p241io.jsonwebtoken.JwsHeader;
import p241io.jsonwebtoken.SignatureAlgorithm;
import p241io.jsonwebtoken.SigningKeyResolver;
import p241io.jsonwebtoken.security.SecurityException;
import p245k.p251b.p261c.C5158a;
import p245k.p251b.p261c.p262b.ECNamedCurveParameterSpec;
import p245k.p251b.p261c.p262b.ECNamedCurveSpec;

public class OpenIdSigningKeyResolver implements SigningKeyResolver {
    private static final String TAG = "OpenIdSignKeyResolver";
    @NonNull
    private final LineAuthenticationApiClient apiClient;

    public OpenIdSigningKeyResolver(@NonNull LineAuthenticationApiClient lineAuthenticationApiClient) {
        this.apiClient = lineAuthenticationApiClient;
    }

    private static BigInteger decodeBase64(String str) {
        return new BigInteger(1, Base64.decode(str, 8));
    }

    private static ECPublicKey generateECPublicKey(JWKSet.JWK jwk) {
        BigInteger decodeBase64 = decodeBase64(jwk.getX());
        BigInteger decodeBase642 = decodeBase64(jwk.getY());
        try {
            KeyFactory instance = KeyFactory.getInstance("EC");
            ECPoint eCPoint = new ECPoint(decodeBase64, decodeBase642);
            ECNamedCurveParameterSpec a = C5158a.m18645a(jwk.getCurve());
            return (ECPublicKey) instance.generatePublic(new ECPublicKeySpec(eCPoint, new ECNamedCurveSpec(jwk.getCurve(), a.mo28229a(), a.mo28230b(), a.mo28231c())));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Log.e(TAG, "failed to generate EC Public Key from JWK: " + jwk, e);
            return null;
        }
    }

    public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {
        return resolveSigningKey(jwsHeader);
    }

    public Key resolveSigningKey(JwsHeader jwsHeader, String str) {
        return resolveSigningKey(jwsHeader);
    }

    private Key resolveSigningKey(JwsHeader jwsHeader) {
        LineApiResponse<JWKSet> jWKSet = this.apiClient.getJWKSet();
        if (!jWKSet.isSuccess()) {
            Log.e(TAG, "failed to get LINE JSON Web Key Set [JWK] document.");
            return null;
        }
        String keyId = jwsHeader.getKeyId();
        JWKSet.JWK jwk = jWKSet.getResponseData().getJWK(keyId);
        if (jwk == null) {
            Log.e(TAG, "failed to find Key by Id: " + keyId);
            return null;
        }
        String algorithm = jwsHeader.getAlgorithm();
        if (SignatureAlgorithm.forName(algorithm).isEllipticCurve()) {
            return generateECPublicKey(jwk);
        }
        throw new SecurityException("Unsupported signature algorithm '" + algorithm + '\'');
    }
}
