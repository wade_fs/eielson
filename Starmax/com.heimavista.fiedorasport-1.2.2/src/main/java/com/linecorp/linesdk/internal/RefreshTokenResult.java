package com.linecorp.linesdk.internal;

import androidx.annotation.NonNull;
import com.linecorp.linesdk.Scope;
import java.util.List;
import p119e.p220f.p221a.p222a.SecurityUtils;

public class RefreshTokenResult {
    @NonNull
    private final String accessToken;
    private final long expiresInMillis;
    @NonNull
    private final String refreshToken;
    @NonNull
    private final List<Scope> scopes;

    public RefreshTokenResult(@NonNull String str, long j, @NonNull String str2, @NonNull List<Scope> list) {
        this.accessToken = str;
        this.expiresInMillis = j;
        this.refreshToken = str2;
        this.scopes = list;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RefreshTokenResult.class != obj.getClass()) {
            return false;
        }
        RefreshTokenResult refreshTokenResult = (RefreshTokenResult) obj;
        if (this.expiresInMillis == refreshTokenResult.expiresInMillis && this.accessToken.equals(refreshTokenResult.accessToken) && this.refreshToken.equals(refreshTokenResult.refreshToken)) {
            return this.scopes.equals(refreshTokenResult.scopes);
        }
        return false;
    }

    @NonNull
    public String getAccessToken() {
        return this.accessToken;
    }

    public long getExpiresInMillis() {
        return this.expiresInMillis;
    }

    @NonNull
    public String getRefreshToken() {
        return this.refreshToken;
    }

    @NonNull
    public List<Scope> getScopes() {
        return this.scopes;
    }

    public int hashCode() {
        long j = this.expiresInMillis;
        return (((((this.accessToken.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.refreshToken.hashCode()) * 31) + this.scopes.hashCode();
    }

    public String toString() {
        return "RefreshTokenResult{accessToken='" + SecurityUtils.m17194a(this.accessToken) + '\'' + ", expiresInMillis=" + this.expiresInMillis + ", refreshToken='" + SecurityUtils.m17194a(this.refreshToken) + '\'' + ", scopes=" + this.scopes + '}';
    }
}
