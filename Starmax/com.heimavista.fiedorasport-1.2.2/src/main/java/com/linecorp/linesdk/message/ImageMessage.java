package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.share.internal.ShareConstants;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class ImageMessage extends MessageData {
    private Boolean animated = false;
    @Nullable
    private String extension;
    @Nullable
    private Long fileSize;
    @NonNull
    private final String originalContentUrl;
    @NonNull
    private final String previewImageUrl;
    @Nullable
    private MessageSender sentBy;

    public ImageMessage(@NonNull String str, @NonNull String str2) {
        this.originalContentUrl = str;
        this.previewImageUrl = str2;
    }

    @NonNull
    public Type getType() {
        return Type.IMAGE;
    }

    public void setAnimated(Boolean bool) {
        this.animated = bool;
    }

    public void setExtension(@Nullable String str) {
        this.extension = str;
    }

    public void setFileSize(Long l) {
        this.fileSize = l;
    }

    public void setSentBy(@Nullable MessageSender messageSender) {
        this.sentBy = messageSender;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("originalContentUrl", this.originalContentUrl);
        jsonObject.put("previewImageUrl", this.previewImageUrl);
        jsonObject.put("animated", this.animated);
        jsonObject.put(ShareConstants.MEDIA_EXTENSION, this.extension);
        jsonObject.put("fileSize", this.fileSize);
        JSONUtils.put(jsonObject, "sentBy", this.sentBy);
        return jsonObject;
    }
}
