package com.linecorp.linesdk.internal;

import android.content.Context;
import androidx.annotation.NonNull;
import java.util.concurrent.Executors;
import p119e.p220f.p221a.p222a.p223c.StringCipher;

public class EncryptorHolder {
    private static final int DEFAULT_ITERATION_COUNT = 5000;
    private static final String ENCRYPTION_SALT_SHARED_PREFERENCE_NAME = "com.linecorp.linesdk.sharedpreference.encryptionsalt";
    /* access modifiers changed from: private */
    public static final StringCipher ENCRYPTOR = new StringCipher(ENCRYPTION_SALT_SHARED_PREFERENCE_NAME, 5000, true);
    private static volatile boolean s_isInitializationStarted = false;

    private static class EncryptorInitializationTask implements Runnable {
        @NonNull
        private final Context context;

        EncryptorInitializationTask(@NonNull Context context2) {
            this.context = context2;
        }

        public void run() {
            EncryptorHolder.ENCRYPTOR.mo27291a(this.context);
        }
    }

    private EncryptorHolder() {
    }

    @NonNull
    public static StringCipher getEncryptor() {
        return ENCRYPTOR;
    }

    public static void initializeOnWorkerThread(@NonNull Context context) {
        if (!s_isInitializationStarted) {
            s_isInitializationStarted = true;
            Executors.newSingleThreadExecutor().execute(new EncryptorInitializationTask(context.getApplicationContext()));
        }
    }
}
