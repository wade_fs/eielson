package com.linecorp.linesdk.internal.nwclient.core;

import androidx.annotation.NonNull;

public class StringResponseParser implements ResponseDataParser<String> {
    private static final String DEFAULT_CHARSET_NAME = "UTF-8";
    @NonNull
    private final String charsetName;

    public StringResponseParser() {
        this("UTF-8");
    }

    public StringResponseParser(@NonNull String str) {
        this.charsetName = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a  */
    @androidx.annotation.NonNull
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getResponseData(@androidx.annotation.NonNull java.io.InputStream r6) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0027 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0027 }
            java.lang.String r4 = r5.charsetName     // Catch:{ all -> 0x0027 }
            r3.<init>(r6, r4)     // Catch:{ all -> 0x0027 }
            r2.<init>(r3)     // Catch:{ all -> 0x0027 }
        L_0x0012:
            java.lang.String r6 = r2.readLine()     // Catch:{ all -> 0x0024 }
            if (r6 == 0) goto L_0x001c
            r0.append(r6)     // Catch:{ all -> 0x0024 }
            goto L_0x0012
        L_0x001c:
            r2.close()
            java.lang.String r6 = r0.toString()
            return r6
        L_0x0024:
            r6 = move-exception
            r1 = r2
            goto L_0x0028
        L_0x0027:
            r6 = move-exception
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.linecorp.linesdk.internal.nwclient.core.StringResponseParser.getResponseData(java.io.InputStream):java.lang.String");
    }
}
