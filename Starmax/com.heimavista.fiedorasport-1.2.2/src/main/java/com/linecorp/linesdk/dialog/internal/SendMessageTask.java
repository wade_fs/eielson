package com.linecorp.linesdk.dialog.internal;

import android.os.AsyncTask;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.SendMessageResponse;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.message.MessageData;
import java.util.ArrayList;
import java.util.List;

public class SendMessageTask extends AsyncTask<List<TargetUser>, Void, LineApiResponse<List<SendMessageResponse>>> {
    @Nullable
    private ApiStatusListener apiStatusListener;
    private LineApiClient lineApiClient;
    private List<MessageData> messages;

    SendMessageTask(LineApiClient lineApiClient2, List<MessageData> list) {
        this(lineApiClient2, list, null);
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return doInBackground((List<TargetUser>[]) ((List[]) objArr));
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((LineApiResponse<List<SendMessageResponse>>) ((LineApiResponse) obj));
    }

    SendMessageTask(LineApiClient lineApiClient2, List<MessageData> list, @Nullable ApiStatusListener apiStatusListener2) {
        this.lineApiClient = lineApiClient2;
        this.messages = list;
        this.apiStatusListener = apiStatusListener2;
    }

    /* access modifiers changed from: protected */
    public LineApiResponse<List<SendMessageResponse>> doInBackground(List<TargetUser>... listArr) {
        ArrayList arrayList = new ArrayList();
        for (TargetUser targetUser : listArr[0]) {
            arrayList.add(targetUser.getId());
        }
        return this.lineApiClient.sendMessageToMultipleUsers(arrayList, this.messages, true);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(LineApiResponse<List<SendMessageResponse>> lineApiResponse) {
        if (this.apiStatusListener == null) {
            return;
        }
        if (lineApiResponse.isSuccess()) {
            this.apiStatusListener.onSuccess();
        } else {
            this.apiStatusListener.onFailure();
        }
    }
}
