package com.linecorp.linesdk.auth;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.utils.ObjectUtils;

public class LineAuthenticationConfig implements Parcelable {
    public static final Parcelable.Creator<LineAuthenticationConfig> CREATOR = new Parcelable.Creator<LineAuthenticationConfig>() {
        /* class com.linecorp.linesdk.auth.LineAuthenticationConfig.C46591 */

        public LineAuthenticationConfig createFromParcel(Parcel parcel) {
            return new LineAuthenticationConfig(parcel);
        }

        public LineAuthenticationConfig[] newArray(int i) {
            return new LineAuthenticationConfig[i];
        }
    };
    private static int FLAGS_ENCRYPTOR_PREPARATION_DISABLED = 2;
    private static int FLAGS_LINE_APP_AUTHENTICATION_DISABLED = 1;
    @NonNull
    private final Uri apiBaseUrl;
    @NonNull
    private final String channelId;
    private final boolean isEncryptorPreparationDisabled;
    private final boolean isLineAppAuthenticationDisabled;
    @NonNull
    private final Uri openidDiscoveryDocumentUrl;
    @NonNull
    private final Uri webLoginPageUrl;

    public static class Builder {
        /* access modifiers changed from: private */
        @NonNull
        public Uri apiBaseUrl;
        /* access modifiers changed from: private */
        @NonNull
        public final String channelId;
        /* access modifiers changed from: private */
        public boolean isEncryptorPreparationDisabled;
        /* access modifiers changed from: private */
        public boolean isLineAppAuthenticationDisabled;
        /* access modifiers changed from: private */
        @NonNull
        public Uri openidDiscoveryDocumentUrl;
        /* access modifiers changed from: private */
        @NonNull
        public Uri webLoginPageUrl;

        public Builder(@NonNull String str) {
            if (!TextUtils.isEmpty(str)) {
                this.channelId = str;
                this.openidDiscoveryDocumentUrl = Uri.parse(BuildConfig.OPENID_DISCOVERY_DOCUMENT_URL);
                this.apiBaseUrl = Uri.parse(BuildConfig.API_SERVER_BASE_URI);
                this.webLoginPageUrl = Uri.parse(BuildConfig.WEB_LOGIN_PAGE_URL);
                return;
            }
            throw new IllegalArgumentException("channelId is empty.");
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public Builder apiBaseUrl(@Nullable Uri uri) {
            this.apiBaseUrl = (Uri) ObjectUtils.defaultIfNull(uri, Uri.parse(BuildConfig.API_SERVER_BASE_URI));
            return this;
        }

        @NonNull
        public LineAuthenticationConfig build() {
            return new LineAuthenticationConfig(this);
        }

        @NonNull
        public Builder disableEncryptorPreparation() {
            this.isEncryptorPreparationDisabled = true;
            return this;
        }

        @NonNull
        public Builder disableLineAppAuthentication() {
            this.isLineAppAuthenticationDisabled = true;
            return this;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public Builder openidDiscoveryDocumentUrl(@Nullable Uri uri) {
            this.openidDiscoveryDocumentUrl = (Uri) ObjectUtils.defaultIfNull(uri, Uri.parse(BuildConfig.OPENID_DISCOVERY_DOCUMENT_URL));
            return this;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public Builder webLoginPageUrl(@Nullable Uri uri) {
            this.webLoginPageUrl = (Uri) ObjectUtils.defaultIfNull(uri, Uri.parse(BuildConfig.WEB_LOGIN_PAGE_URL));
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineAuthenticationConfig.class != obj.getClass()) {
            return false;
        }
        LineAuthenticationConfig lineAuthenticationConfig = (LineAuthenticationConfig) obj;
        if (this.isLineAppAuthenticationDisabled == lineAuthenticationConfig.isLineAppAuthenticationDisabled && this.isEncryptorPreparationDisabled == lineAuthenticationConfig.isEncryptorPreparationDisabled && this.channelId.equals(lineAuthenticationConfig.channelId) && this.openidDiscoveryDocumentUrl.equals(lineAuthenticationConfig.openidDiscoveryDocumentUrl) && this.apiBaseUrl.equals(lineAuthenticationConfig.apiBaseUrl)) {
            return this.webLoginPageUrl.equals(lineAuthenticationConfig.webLoginPageUrl);
        }
        return false;
    }

    @NonNull
    public Uri getApiBaseUrl() {
        return this.apiBaseUrl;
    }

    @NonNull
    public String getChannelId() {
        return this.channelId;
    }

    @NonNull
    public Uri getOpenidDiscoveryDocumentUrl() {
        return this.openidDiscoveryDocumentUrl;
    }

    @NonNull
    public Uri getWebLoginPageUrl() {
        return this.webLoginPageUrl;
    }

    public int hashCode() {
        return (((((((((this.channelId.hashCode() * 31) + this.openidDiscoveryDocumentUrl.hashCode()) * 31) + this.apiBaseUrl.hashCode()) * 31) + this.webLoginPageUrl.hashCode()) * 31) + (this.isLineAppAuthenticationDisabled ? 1 : 0)) * 31) + (this.isEncryptorPreparationDisabled ? 1 : 0);
    }

    public boolean isEncryptorPreparationDisabled() {
        return this.isEncryptorPreparationDisabled;
    }

    public boolean isLineAppAuthenticationDisabled() {
        return this.isLineAppAuthenticationDisabled;
    }

    public String toString() {
        return "LineAuthenticationConfig{channelId='" + this.channelId + '\'' + ", openidDiscoveryDocumentUrl=" + this.openidDiscoveryDocumentUrl + ", apiBaseUrl=" + this.apiBaseUrl + ", webLoginPageUrl=" + this.webLoginPageUrl + ", isLineAppAuthenticationDisabled=" + this.isLineAppAuthenticationDisabled + ", isEncryptorPreparationDisabled=" + this.isEncryptorPreparationDisabled + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.channelId);
        parcel.writeParcelable(this.openidDiscoveryDocumentUrl, i);
        parcel.writeParcelable(this.apiBaseUrl, i);
        parcel.writeParcelable(this.webLoginPageUrl, i);
        int i2 = 0;
        int i3 = (this.isLineAppAuthenticationDisabled ? FLAGS_LINE_APP_AUTHENTICATION_DISABLED : 0) | 0;
        if (this.isEncryptorPreparationDisabled) {
            i2 = FLAGS_ENCRYPTOR_PREPARATION_DISABLED;
        }
        parcel.writeInt(i3 | i2);
    }

    private LineAuthenticationConfig(@NonNull Builder builder) {
        this.channelId = builder.channelId;
        this.openidDiscoveryDocumentUrl = builder.openidDiscoveryDocumentUrl;
        this.apiBaseUrl = builder.apiBaseUrl;
        this.webLoginPageUrl = builder.webLoginPageUrl;
        this.isLineAppAuthenticationDisabled = builder.isLineAppAuthenticationDisabled;
        this.isEncryptorPreparationDisabled = builder.isEncryptorPreparationDisabled;
    }

    private LineAuthenticationConfig(@NonNull Parcel parcel) {
        this.channelId = parcel.readString();
        this.openidDiscoveryDocumentUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.apiBaseUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.webLoginPageUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        int readInt = parcel.readInt();
        boolean z = true;
        this.isLineAppAuthenticationDisabled = (FLAGS_LINE_APP_AUTHENTICATION_DISABLED & readInt) > 0;
        this.isEncryptorPreparationDisabled = (readInt & FLAGS_ENCRYPTOR_PREPARATION_DISABLED) <= 0 ? false : z;
    }
}
