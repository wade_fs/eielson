package com.linecorp.linesdk.internal.nwclient;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.linecorp.linesdk.internal.nwclient.core.JsonResponseParser;
import com.linecorp.linesdk.internal.nwclient.core.ResponseDataParser;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

abstract class JsonToObjectBaseResponseParser<T> implements ResponseDataParser<T> {
    @NonNull
    private final JsonResponseParser jsonResponseParser;

    JsonToObjectBaseResponseParser() {
        this(new JsonResponseParser());
    }

    @NonNull
    public T getResponseData(@NonNull InputStream inputStream) {
        try {
            return parseJsonToObject(this.jsonResponseParser.getResponseData(inputStream));
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public abstract T parseJsonToObject(@NonNull JSONObject jSONObject);

    JsonToObjectBaseResponseParser(@NonNull String str) {
        this(new JsonResponseParser(str));
    }

    @VisibleForTesting
    JsonToObjectBaseResponseParser(@NonNull JsonResponseParser jsonResponseParser2) {
        this.jsonResponseParser = jsonResponseParser2;
    }
}
