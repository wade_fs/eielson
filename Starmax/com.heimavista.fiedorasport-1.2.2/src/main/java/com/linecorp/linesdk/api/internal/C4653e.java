package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.e */
/* compiled from: lambda */
public final /* synthetic */ class C4653e implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9521a;

    public /* synthetic */ C4653e(LineApiClientImpl lineApiClientImpl) {
        this.f9521a = lineApiClientImpl;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9521a.logout(internalAccessToken);
    }
}
