package com.linecorp.linesdk.api;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.api.internal.AutoRefreshLineApiClientProxy;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.AccessTokenCache;
import com.linecorp.linesdk.internal.EncryptorHolder;
import com.linecorp.linesdk.internal.nwclient.LineAuthenticationApiClient;
import com.linecorp.linesdk.internal.nwclient.TalkApiClient;
import com.linecorp.linesdk.utils.ObjectUtils;

public class LineApiClientBuilder {
    @NonNull
    private Uri apiBaseUri;
    @NonNull
    private final String channelId;
    @NonNull
    private final Context context;
    private boolean isEncryptorPreparationDisabled;
    private boolean isTokenAutoRefreshDisabled;
    @NonNull
    private Uri openidDiscoveryDocumentUrl;

    public LineApiClientBuilder(@NonNull Context context2, @NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            this.context = context2.getApplicationContext();
            this.channelId = str;
            this.openidDiscoveryDocumentUrl = Uri.parse(BuildConfig.OPENID_DISCOVERY_DOCUMENT_URL);
            this.apiBaseUri = Uri.parse(BuildConfig.API_SERVER_BASE_URI);
            return;
        }
        throw new IllegalArgumentException("channel id is empty");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public LineApiClientBuilder apiBaseUri(@Nullable Uri uri) {
        this.apiBaseUri = (Uri) ObjectUtils.defaultIfNull(uri, Uri.parse(BuildConfig.API_SERVER_BASE_URI));
        return this;
    }

    @NonNull
    public LineApiClient build() {
        if (!this.isEncryptorPreparationDisabled) {
            EncryptorHolder.initializeOnWorkerThread(this.context);
        }
        LineApiClientImpl lineApiClientImpl = new LineApiClientImpl(this.channelId, new LineAuthenticationApiClient(this.context, this.openidDiscoveryDocumentUrl, this.apiBaseUri), new TalkApiClient(this.context, this.apiBaseUri), new AccessTokenCache(this.context, this.channelId));
        return this.isTokenAutoRefreshDisabled ? lineApiClientImpl : AutoRefreshLineApiClientProxy.newProxy(lineApiClientImpl);
    }

    @NonNull
    public LineApiClientBuilder disableEncryptorPreparation() {
        this.isEncryptorPreparationDisabled = true;
        return this;
    }

    @NonNull
    public LineApiClientBuilder disableTokenAutoRefresh() {
        this.isTokenAutoRefreshDisabled = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public LineApiClientBuilder openidDiscoveryDocumentUrl(@Nullable Uri uri) {
        this.openidDiscoveryDocumentUrl = (Uri) ObjectUtils.defaultIfNull(uri, Uri.parse(BuildConfig.OPENID_DISCOVERY_DOCUMENT_URL));
        return this;
    }
}
