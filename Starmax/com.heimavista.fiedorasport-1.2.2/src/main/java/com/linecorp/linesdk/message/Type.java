package com.linecorp.linesdk.message;

public enum Type {
    TEXT,
    IMAGE,
    VIDEO,
    AUDIO,
    LOCATION,
    TEMPLATE,
    FLEX
}
