package com.linecorp.linesdk;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public class GetGroupsResponse {
    @NonNull
    private List<LineGroup> groups;
    @Nullable
    private String nextPageRequestToken;

    public GetGroupsResponse(@NonNull List<LineGroup> list) {
        this.groups = list;
    }

    @NonNull
    public List<LineGroup> getGroups() {
        return this.groups;
    }

    @Nullable
    public String getNextPageRequestToken() {
        return this.nextPageRequestToken;
    }

    public String toString() {
        return "GetFriendsResponse{groups=" + this.groups + ", nextPageRequestToken='" + this.nextPageRequestToken + '\'' + '}';
    }

    public GetGroupsResponse(@NonNull List<LineGroup> list, @Nullable String str) {
        this.groups = list;
        this.nextPageRequestToken = str;
    }
}
