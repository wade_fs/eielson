package com.linecorp.linesdk.internal.nwclient;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.GetFriendsResponse;
import com.linecorp.linesdk.GetGroupsResponse;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineFriendProfile;
import com.linecorp.linesdk.LineFriendshipStatus;
import com.linecorp.linesdk.LineGroup;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.SendMessageResponse;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.nwclient.core.ChannelServiceHttpClient;
import com.linecorp.linesdk.internal.nwclient.core.ResponseDataParser;
import com.linecorp.linesdk.message.MessageData;
import com.linecorp.linesdk.message.MessageSendRequest;
import com.linecorp.linesdk.message.OttRequest;
import com.linecorp.linesdk.utils.UriUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TalkApiClient {
    private static final String BASE_PATH_COMMON_API = "v2";
    private static final String BASE_PATH_FRIENDSHIP_API = "friendship/v1";
    static final String BASE_PATH_GRAPH_API = "graph/v2";
    static final String BASE_PATH_MESSAGE_API = "message/v3";
    private static final ResponseDataParser<LineFriendshipStatus> FRIENDSHIP_STATUS_PARSER = new FriendshipStatusParser();
    private static final ResponseDataParser<GetFriendsResponse> FRIENDS_PARSER = new FriendsParser();
    private static final ResponseDataParser<GetGroupsResponse> GROUP_PARSER = new GroupParser();
    static final String PATH_FRIENDS = "friends";
    static final String PATH_GROUPS = "groups";
    static final String PATH_OTS_FRIENDS = "ots/friends";
    static final String PATH_OTS_GROUPS = "ots/groups";
    static final String PATH_OTT_ISSUE = "ott/issue";
    static final String PATH_OTT_SHARE = "ott/share";
    private static final ResponseDataParser<LineProfile> PROFILE_PARSER = new ProfileParser();
    private static final String REQUEST_HEADER_ACCESS_TOKEN = "Authorization";
    private static final String TOKEN_TYPE_BEARER = "Bearer";
    @NonNull
    private final Uri apiBaseUrl;
    @NonNull
    private final ChannelServiceHttpClient httpClient;

    @VisibleForTesting
    static class FriendProfileParser extends JsonToObjectBaseResponseParser<LineFriendProfile> {
        FriendProfileParser() {
        }

        /* access modifiers changed from: private */
        public static LineFriendProfile parseLineFriendProfile(@NonNull JSONObject jSONObject) {
            LineProfile access$100 = ProfileParser.parseLineProfile(jSONObject);
            return new LineFriendProfile(access$100.getUserId(), access$100.getDisplayName(), access$100.getPictureUrl(), access$100.getStatusMessage(), jSONObject.optString("displayNameOverridden", null));
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public LineFriendProfile parseJsonToObject(@NonNull JSONObject jSONObject) {
            return parseLineFriendProfile(jSONObject);
        }
    }

    @VisibleForTesting
    static class FriendsParser extends JsonToObjectBaseResponseParser<GetFriendsResponse> {
        FriendsParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public GetFriendsResponse parseJsonToObject(@NonNull JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("friends");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(FriendProfileParser.parseLineFriendProfile(jSONArray.getJSONObject(i)));
            }
            return new GetFriendsResponse(arrayList, jSONObject.optString("pageToken", null));
        }
    }

    @VisibleForTesting
    static class FriendshipStatusParser extends JsonToObjectBaseResponseParser<LineFriendshipStatus> {
        FriendshipStatusParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public LineFriendshipStatus parseJsonToObject(@NonNull JSONObject jSONObject) {
            return new LineFriendshipStatus(jSONObject.getBoolean("friendFlag"));
        }
    }

    @VisibleForTesting
    static class GroupParser extends JsonToObjectBaseResponseParser<GetGroupsResponse> {
        GroupParser() {
        }

        @NonNull
        private static LineGroup parseLineGroup(@NonNull JSONObject jSONObject) {
            Uri uri = null;
            String optString = jSONObject.optString("pictureUrl", null);
            String string = jSONObject.getString("groupId");
            String string2 = jSONObject.getString("groupName");
            if (optString != null) {
                uri = Uri.parse(optString);
            }
            return new LineGroup(string, string2, uri);
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public GetGroupsResponse parseJsonToObject(@NonNull JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray(TalkApiClient.PATH_GROUPS);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(parseLineGroup(jSONArray.getJSONObject(i)));
            }
            return new GetGroupsResponse(arrayList, jSONObject.optString("pageToken", null));
        }
    }

    @VisibleForTesting
    static class MultiSendResponseParser extends JsonToObjectBaseResponseParser<List<SendMessageResponse>> {
        MultiSendResponseParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public List<SendMessageResponse> parseJsonToObject(@NonNull JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList();
            if (jSONObject.has("results")) {
                JSONArray jSONArray = jSONObject.getJSONArray("results");
                for (int i = 0; i < jSONArray.length(); i++) {
                    arrayList.add(SendMessageResponse.fromJsonObject(jSONArray.getJSONObject(i)));
                }
            }
            return arrayList;
        }
    }

    @VisibleForTesting
    static class ProfileParser extends JsonToObjectBaseResponseParser<LineProfile> {
        ProfileParser() {
        }

        /* access modifiers changed from: private */
        public static LineProfile parseLineProfile(@NonNull JSONObject jSONObject) {
            Uri uri;
            String optString = jSONObject.optString("pictureUrl", null);
            String string = jSONObject.getString("userId");
            String string2 = jSONObject.getString("displayName");
            if (optString == null) {
                uri = null;
            } else {
                uri = Uri.parse(optString);
            }
            return new LineProfile(string, string2, uri, jSONObject.optString("statusMessage", null));
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public LineProfile parseJsonToObject(@NonNull JSONObject jSONObject) {
            return parseLineProfile(jSONObject);
        }
    }

    @VisibleForTesting
    static class StringParser extends JsonToObjectBaseResponseParser<String> {
        private String jsonKey;

        StringParser(String str) {
            this.jsonKey = str;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public String parseJsonToObject(@NonNull JSONObject jSONObject) {
            return jSONObject.getString(this.jsonKey);
        }
    }

    public TalkApiClient(Context context, @NonNull Uri uri) {
        this(uri, new ChannelServiceHttpClient(context, BuildConfig.VERSION_NAME));
    }

    @NonNull
    private static Map<String, String> buildRequestHeaders(@NonNull InternalAccessToken internalAccessToken) {
        return UriUtils.buildParams(REQUEST_HEADER_ACCESS_TOKEN, "Bearer " + internalAccessToken.getAccessToken());
    }

    @NonNull
    private LineApiResponse<String> getOtt(@NonNull InternalAccessToken internalAccessToken, @NonNull List<String> list) {
        try {
            return this.httpClient.postWithJson(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_MESSAGE_API, PATH_OTT_ISSUE), buildRequestHeaders(internalAccessToken), new OttRequest(list).toJsonString(), new StringParser("token"));
        } catch (JSONException e) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError(e));
        }
    }

    @NonNull
    private LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsersUsingUserIds(@NonNull InternalAccessToken internalAccessToken, @NonNull List<String> list, @NonNull List<MessageData> list2) {
        try {
            return this.httpClient.postWithJson(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_MESSAGE_API, "multisend"), buildRequestHeaders(internalAccessToken), MessageSendRequest.createMultiUsersType(list, list2).toJsonString(), new MultiSendResponseParser());
        } catch (JSONException e) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError(e));
        }
    }

    @NonNull
    public LineApiResponse<GetFriendsResponse> getFriends(@NonNull InternalAccessToken internalAccessToken, @NonNull FriendSortField friendSortField, @Nullable String str, boolean z) {
        Uri buildUri = UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_GRAPH_API, z ? PATH_OTS_FRIENDS : "friends");
        Map<String, String> buildParams = UriUtils.buildParams("sort", friendSortField.getServerKey());
        if (!TextUtils.isEmpty(str)) {
            buildParams.put("pageToken", str);
        }
        return this.httpClient.get(buildUri, buildRequestHeaders(internalAccessToken), buildParams, FRIENDS_PARSER);
    }

    @NonNull
    public LineApiResponse<GetFriendsResponse> getFriendsApprovers(@NonNull InternalAccessToken internalAccessToken, @NonNull FriendSortField friendSortField, @Nullable String str) {
        Uri buildUri = UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_GRAPH_API, "friends", "approvers");
        Map<String, String> buildParams = UriUtils.buildParams("sort", friendSortField.getServerKey());
        if (!TextUtils.isEmpty(str)) {
            buildParams.put("pageToken", str);
        }
        return this.httpClient.get(buildUri, buildRequestHeaders(internalAccessToken), buildParams, FRIENDS_PARSER);
    }

    @NonNull
    public LineApiResponse<LineFriendshipStatus> getFriendshipStatus(@NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.get(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_FRIENDSHIP_API, "status"), buildRequestHeaders(internalAccessToken), Collections.emptyMap(), FRIENDSHIP_STATUS_PARSER);
    }

    @NonNull
    public LineApiResponse<GetFriendsResponse> getGroupApprovers(@NonNull InternalAccessToken internalAccessToken, @NonNull String str, @Nullable String str2) {
        Map<String, String> map;
        Uri buildUri = UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_GRAPH_API, PATH_GROUPS, str, "approvers");
        if (!TextUtils.isEmpty(str2)) {
            map = UriUtils.buildParams("pageToken", str2);
        } else {
            map = Collections.emptyMap();
        }
        return this.httpClient.get(buildUri, buildRequestHeaders(internalAccessToken), map, FRIENDS_PARSER);
    }

    @NonNull
    public LineApiResponse<GetGroupsResponse> getGroups(@NonNull InternalAccessToken internalAccessToken, @Nullable String str, boolean z) {
        Map<String, String> map;
        Uri buildUri = UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_GRAPH_API, z ? PATH_OTS_GROUPS : PATH_GROUPS);
        if (!TextUtils.isEmpty(str)) {
            map = UriUtils.buildParams("pageToken", str);
        } else {
            map = Collections.emptyMap();
        }
        return this.httpClient.get(buildUri, buildRequestHeaders(internalAccessToken), map, GROUP_PARSER);
    }

    @NonNull
    public LineApiResponse<LineProfile> getProfile(@NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.get(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_COMMON_API, "profile"), buildRequestHeaders(internalAccessToken), Collections.emptyMap(), PROFILE_PARSER);
    }

    @NonNull
    public LineApiResponse<String> sendMessage(@NonNull InternalAccessToken internalAccessToken, @NonNull String str, @NonNull List<MessageData> list) {
        try {
            return this.httpClient.postWithJson(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_MESSAGE_API, "send"), buildRequestHeaders(internalAccessToken), MessageSendRequest.createSingleUserType(str, list).toJsonString(), new StringParser("status"));
        } catch (JSONException e) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError(e));
        }
    }

    @NonNull
    public LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull InternalAccessToken internalAccessToken, @NonNull List<String> list, @NonNull List<MessageData> list2) {
        return sendMessageToMultipleUsers(internalAccessToken, list, list2, false);
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    @NonNull
    public LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsersUsingOtt(@NonNull InternalAccessToken internalAccessToken, @NonNull String str, @NonNull List<MessageData> list) {
        try {
            return this.httpClient.postWithJson(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_MESSAGE_API, PATH_OTT_SHARE), buildRequestHeaders(internalAccessToken), MessageSendRequest.createOttType(str, list).toJsonString(), new MultiSendResponseParser());
        } catch (JSONException e) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError(e));
        }
    }

    @VisibleForTesting
    TalkApiClient(@NonNull Uri uri, @NonNull ChannelServiceHttpClient channelServiceHttpClient) {
        this.apiBaseUrl = uri;
        this.httpClient = channelServiceHttpClient;
    }

    @NonNull
    public LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull InternalAccessToken internalAccessToken, @NonNull List<String> list, @NonNull List<MessageData> list2, boolean z) {
        if (!z) {
            return sendMessageToMultipleUsersUsingUserIds(internalAccessToken, list, list2);
        }
        LineApiResponse<String> ott = getOtt(internalAccessToken, list);
        if (ott.isSuccess()) {
            return sendMessageToMultipleUsersUsingOtt(internalAccessToken, ott.getResponseData(), list2);
        }
        return LineApiResponse.createAsError(ott.getResponseCode(), ott.getErrorData());
    }
}
