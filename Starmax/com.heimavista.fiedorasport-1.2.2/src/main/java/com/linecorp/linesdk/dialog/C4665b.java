package com.linecorp.linesdk.dialog;

import android.view.View;
import com.linecorp.linesdk.dialog.internal.TargetUser;

/* renamed from: com.linecorp.linesdk.dialog.b */
/* compiled from: lambda */
public final /* synthetic */ class C4665b implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ SendMessageDialog f9537P;

    /* renamed from: Q */
    private final /* synthetic */ TargetUser f9538Q;

    public /* synthetic */ C4665b(SendMessageDialog sendMessageDialog, TargetUser targetUser) {
        this.f9537P = sendMessageDialog;
        this.f9538Q = targetUser;
    }

    public final void onClick(View view) {
        this.f9537P.mo25781a(this.f9538Q, view);
    }
}
