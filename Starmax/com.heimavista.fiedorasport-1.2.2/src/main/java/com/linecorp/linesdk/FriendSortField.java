package com.linecorp.linesdk;

import androidx.annotation.NonNull;

public enum FriendSortField {
    NAME("name"),
    RELATION("relation");
    
    @NonNull
    private final String serverKey;

    private FriendSortField(@NonNull String str) {
        this.serverKey = str;
    }

    @NonNull
    public String getServerKey() {
        return this.serverKey;
    }
}
