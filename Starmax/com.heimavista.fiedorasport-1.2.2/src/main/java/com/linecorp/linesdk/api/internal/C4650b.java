package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.nwclient.TalkApiClient;

/* renamed from: com.linecorp.linesdk.api.internal.b */
/* compiled from: lambda */
public final /* synthetic */ class C4650b implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ TalkApiClient f9513a;

    public /* synthetic */ C4650b(TalkApiClient talkApiClient) {
        this.f9513a = talkApiClient;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9513a.getFriendshipStatus(internalAccessToken);
    }
}
