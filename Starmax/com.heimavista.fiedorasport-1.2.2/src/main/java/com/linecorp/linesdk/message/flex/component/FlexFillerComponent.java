package com.linecorp.linesdk.message.flex.component;

import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;

public class FlexFillerComponent extends FlexMessageComponent {
    public FlexFillerComponent() {
        super(FlexMessageComponent.Type.FILLER);
    }
}
