package com.linecorp.linesdk.auth.internal;

import androidx.annotation.Nullable;

class LineAppVersion {
    private final int major;
    private final int minor;
    private final int revision;

    public LineAppVersion(int i, int i2, int i3) {
        this.major = i;
        this.minor = i2;
        this.revision = i3;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    @androidx.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.linecorp.linesdk.auth.internal.LineAppVersion getLineAppVersion(@androidx.annotation.NonNull android.content.Context r4) {
        /*
            r0 = 0
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch:{  }
            java.lang.String r1 = "jp.naver.line.android"
            r2 = 128(0x80, float:1.794E-43)
            android.content.pm.PackageInfo r4 = r4.getPackageInfo(r1, r2)     // Catch:{  }
            java.lang.String r4 = r4.versionName
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 == 0) goto L_0x0016
            return r0
        L_0x0016:
            java.util.StringTokenizer r1 = new java.util.StringTokenizer
            java.lang.String r2 = "."
            r1.<init>(r4, r2)
            com.linecorp.linesdk.auth.internal.LineAppVersion r4 = new com.linecorp.linesdk.auth.internal.LineAppVersion     // Catch:{ NameNotFoundException -> 0x003b }
            java.lang.String r2 = r1.nextToken()     // Catch:{ NameNotFoundException -> 0x003b }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ NameNotFoundException -> 0x003b }
            java.lang.String r3 = r1.nextToken()     // Catch:{ NameNotFoundException -> 0x003b }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NameNotFoundException -> 0x003b }
            java.lang.String r1 = r1.nextToken()     // Catch:{ NameNotFoundException -> 0x003b }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NameNotFoundException -> 0x003b }
            r4.<init>(r2, r3, r1)     // Catch:{ NameNotFoundException -> 0x003b }
            return r4
        L_0x003b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.linecorp.linesdk.auth.internal.LineAppVersion.getLineAppVersion(android.content.Context):com.linecorp.linesdk.auth.internal.LineAppVersion");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineAppVersion.class != obj.getClass()) {
            return false;
        }
        LineAppVersion lineAppVersion = (LineAppVersion) obj;
        return this.major == lineAppVersion.major && this.minor == lineAppVersion.minor && this.revision == lineAppVersion.revision;
    }

    public int getMajor() {
        return this.major;
    }

    public int getMinor() {
        return this.minor;
    }

    public int getRevision() {
        return this.revision;
    }

    public int hashCode() {
        return (((this.major * 31) + this.minor) * 31) + this.revision;
    }

    public boolean isEqualOrGreaterThan(@Nullable LineAppVersion lineAppVersion) {
        if (lineAppVersion == null) {
            return false;
        }
        int i = this.major;
        int i2 = lineAppVersion.major;
        if (i != i2) {
            return i >= i2;
        }
        int i3 = this.minor;
        int i4 = lineAppVersion.minor;
        if (i3 != i4) {
            if (i3 >= i4) {
                return true;
            }
            return false;
        } else if (this.revision >= lineAppVersion.revision) {
            return true;
        } else {
            return false;
        }
    }
}
