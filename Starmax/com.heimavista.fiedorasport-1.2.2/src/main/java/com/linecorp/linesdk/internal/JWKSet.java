package com.linecorp.linesdk.internal;

import android.text.TextUtils;
import java.util.List;

public class JWKSet {
    private final List<JWK> keys;

    public static final class Builder {
        /* access modifiers changed from: private */
        public List<JWK> keys;

        public JWKSet build() {
            return new JWKSet(this);
        }

        public Builder keys(List<JWK> list) {
            this.keys = list;
            return this;
        }
    }

    public static class JWK {
        private final String algorithm;
        private final String curve;
        private final String keyId;
        private final String keyType;
        private final String use;

        /* renamed from: x */
        private final String f9546x;

        /* renamed from: y */
        private final String f9547y;

        public static final class Builder {
            /* access modifiers changed from: private */
            public String algorithm;
            /* access modifiers changed from: private */
            public String curve;
            /* access modifiers changed from: private */
            public String keyId;
            /* access modifiers changed from: private */
            public String keyType;
            /* access modifiers changed from: private */
            public String use;
            /* access modifiers changed from: private */

            /* renamed from: x */
            public String f9548x;
            /* access modifiers changed from: private */

            /* renamed from: y */
            public String f9549y;

            public Builder algorithm(String str) {
                this.algorithm = str;
                return this;
            }

            public JWK build() {
                return new JWK(this);
            }

            public Builder curve(String str) {
                this.curve = str;
                return this;
            }

            public Builder keyId(String str) {
                this.keyId = str;
                return this;
            }

            public Builder keyType(String str) {
                this.keyType = str;
                return this;
            }

            public Builder use(String str) {
                this.use = str;
                return this;
            }

            /* renamed from: x */
            public Builder mo25877x(String str) {
                this.f9548x = str;
                return this;
            }

            /* renamed from: y */
            public Builder mo25878y(String str) {
                this.f9549y = str;
                return this;
            }
        }

        public String getAlgorithm() {
            return this.algorithm;
        }

        public String getCurve() {
            return this.curve;
        }

        public String getKeyId() {
            return this.keyId;
        }

        public String getKeyType() {
            return this.keyType;
        }

        public String getUse() {
            return this.use;
        }

        public String getX() {
            return this.f9546x;
        }

        public String getY() {
            return this.f9547y;
        }

        public String toString() {
            return "JWK{keyType='" + this.keyType + '\'' + ", algorithm='" + this.algorithm + '\'' + ", use='" + this.use + '\'' + ", keyId='" + this.keyId + '\'' + ", curve='" + this.curve + '\'' + ", x='" + this.f9546x + '\'' + ", y='" + this.f9547y + '\'' + '}';
        }

        private JWK(Builder builder) {
            this.keyType = builder.keyType;
            this.algorithm = builder.algorithm;
            this.use = builder.use;
            this.keyId = builder.keyId;
            this.curve = builder.curve;
            this.f9546x = builder.f9548x;
            this.f9547y = builder.f9549y;
        }
    }

    public JWK getJWK(String str) {
        for (JWK jwk : this.keys) {
            if (TextUtils.equals(jwk.getKeyId(), str)) {
                return jwk;
            }
        }
        return null;
    }

    public List<JWK> getKeys() {
        return this.keys;
    }

    public String toString() {
        return "JWKSet{keys=" + this.keys + '}';
    }

    private JWKSet(Builder builder) {
        this.keys = builder.keys;
    }
}
