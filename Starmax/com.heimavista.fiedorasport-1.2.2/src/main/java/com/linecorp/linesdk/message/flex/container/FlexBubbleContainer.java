package com.linecorp.linesdk.message.flex.container;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.Jsonable;
import com.linecorp.linesdk.message.flex.component.FlexBoxComponent;
import com.linecorp.linesdk.message.flex.component.FlexImageComponent;
import com.linecorp.linesdk.message.flex.container.FlexMessageContainer;
import com.linecorp.linesdk.message.flex.style.FlexBlockStyle;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexBubbleContainer extends FlexMessageContainer {
    @Nullable
    private FlexBoxComponent body;
    @Nullable
    private Direction direction;
    @Nullable
    private FlexBoxComponent footer;
    @Nullable
    private FlexBoxComponent header;
    @Nullable
    private FlexImageComponent hero;
    @Nullable
    private Style styles;

    public static final class Builder {
        /* access modifiers changed from: private */
        public FlexBoxComponent body;
        /* access modifiers changed from: private */
        public Direction direction;
        /* access modifiers changed from: private */
        public FlexBoxComponent footer;
        /* access modifiers changed from: private */
        public FlexBoxComponent header;
        /* access modifiers changed from: private */
        public FlexImageComponent hero;
        /* access modifiers changed from: private */
        public Style styles;

        public FlexBubbleContainer build() {
            return new FlexBubbleContainer(this);
        }

        public Builder setBody(FlexBoxComponent flexBoxComponent) {
            this.body = flexBoxComponent;
            return this;
        }

        public Builder setDirection(Direction direction2) {
            this.direction = direction2;
            return this;
        }

        public Builder setFooter(FlexBoxComponent flexBoxComponent) {
            this.footer = flexBoxComponent;
            return this;
        }

        public Builder setHeader(FlexBoxComponent flexBoxComponent) {
            this.header = flexBoxComponent;
            return this;
        }

        public Builder setHero(FlexImageComponent flexImageComponent) {
            this.hero = flexImageComponent;
            return this;
        }

        public Builder setStyles(Style style) {
            this.styles = style;
            return this;
        }

        private Builder() {
        }
    }

    public enum Direction {
        LEFT_TO_RIGHT("ltr"),
        RIGHT_TO_LEFT("rtl");
        
        private String value;

        private Direction(String str) {
            this.value = str;
        }

        public String getValue() {
            return this.value;
        }
    }

    public static class Style implements Jsonable {
        private FlexBlockStyle body;
        private FlexBlockStyle footer;
        private FlexBlockStyle header;
        private FlexBlockStyle hero;

        @NonNull
        public JSONObject toJsonObject() {
            JSONObject jSONObject = new JSONObject();
            JSONUtils.put(jSONObject, "header", this.header);
            JSONUtils.put(jSONObject, "hero", this.hero);
            JSONUtils.put(jSONObject, TtmlNode.TAG_BODY, this.body);
            JSONUtils.put(jSONObject, "footer", this.footer);
            return jSONObject;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        Direction direction2 = this.direction;
        Object obj = direction2;
        if (direction2 != null) {
            obj = direction2.getValue();
        }
        JSONUtils.put(jsonObject, "direction", obj);
        JSONUtils.put(jsonObject, "header", this.header);
        JSONUtils.put(jsonObject, "hero", this.hero);
        JSONUtils.put(jsonObject, TtmlNode.TAG_BODY, this.body);
        JSONUtils.put(jsonObject, "footer", this.footer);
        JSONUtils.put(jsonObject, "styles", this.styles);
        return jsonObject;
    }

    private FlexBubbleContainer() {
        super(FlexMessageContainer.Type.BUBBLE);
        this.direction = Direction.LEFT_TO_RIGHT;
    }

    private FlexBubbleContainer(Builder builder) {
        this();
        this.direction = builder.direction;
        this.header = builder.header;
        this.hero = builder.hero;
        this.body = builder.body;
        this.footer = builder.footer;
        this.styles = builder.styles;
    }
}
