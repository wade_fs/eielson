package com.linecorp.linesdk.dialog.internal;

interface ApiStatusListener {
    void onFailure();

    void onSuccess();
}
