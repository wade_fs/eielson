package com.linecorp.linesdk.message.flex.action;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.share.internal.ShareConstants;
import com.linecorp.linesdk.message.flex.action.Action;
import org.json.JSONObject;

public class UriAction extends Action {
    @NonNull
    private String uri;

    public UriAction(@NonNull String str, @Nullable String str2) {
        super(Action.Type.URI, str2);
        this.uri = str;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put(ShareConstants.MEDIA_URI, this.uri);
        return jsonObject;
    }

    public UriAction(@NonNull String str) {
        this(str, null);
    }
}
