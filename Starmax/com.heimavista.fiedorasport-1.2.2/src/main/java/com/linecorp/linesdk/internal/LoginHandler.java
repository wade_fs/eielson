package com.linecorp.linesdk.internal;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LoginListener;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;
import java.util.ArrayList;
import java.util.Iterator;

public class LoginHandler {
    private static int REQUEST_CODE_LOGIN = 1;
    private static String TAG = "LoginHandler";
    @NonNull
    private ArrayList<LoginListener> loginListeners = new ArrayList<>();

    @NonNull
    private Intent getLoginIntent(@NonNull Activity activity, boolean z, @NonNull String str, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        if (z) {
            return LineLoginApi.getLoginIntent(activity, str, lineAuthenticationParams);
        }
        return LineLoginApi.getLoginIntentWithoutLineAppAuth(activity, str, lineAuthenticationParams);
    }

    private boolean isLoginCanceled(int i, Intent intent) {
        return i != -1 || intent == null;
    }

    private boolean isLoginRequestCode(int i) {
        return i == REQUEST_CODE_LOGIN;
    }

    private boolean isLoginSuccess(@Nullable LineLoginResult lineLoginResult) {
        return lineLoginResult != null && lineLoginResult.getResponseCode() == LineApiResponseCode.SUCCESS;
    }

    private void onLoginFailure(@Nullable LineLoginResult lineLoginResult) {
        Iterator<LoginListener> it = this.loginListeners.iterator();
        while (it.hasNext()) {
            it.next().onLoginFailure(lineLoginResult);
        }
    }

    private void onLoginSuccess(LineLoginResult lineLoginResult) {
        Iterator<LoginListener> it = this.loginListeners.iterator();
        while (it.hasNext()) {
            it.next().onLoginSuccess(lineLoginResult);
        }
    }

    public void addLoginListener(@NonNull LoginListener loginListener) {
        this.loginListeners.add(loginListener);
    }

    /* access modifiers changed from: package-private */
    public boolean onActivityResult(int i, int i2, Intent intent) {
        if (!isLoginRequestCode(i)) {
            Log.w(TAG, "Unexpected login request code");
            return false;
        } else if (isLoginCanceled(i2, intent)) {
            Log.w(TAG, "Login failed");
            return false;
        } else {
            LineLoginResult loginResultFromIntent = LineLoginApi.getLoginResultFromIntent(intent);
            if (isLoginSuccess(loginResultFromIntent)) {
                onLoginSuccess(loginResultFromIntent);
                return true;
            }
            onLoginFailure(loginResultFromIntent);
            return true;
        }
    }

    public void performLogin(@NonNull Activity activity, boolean z, @NonNull String str, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        activity.startActivityForResult(getLoginIntent(activity, z, str, lineAuthenticationParams), REQUEST_CODE_LOGIN);
    }

    public void removeLoginListener(@NonNull LoginListener loginListener) {
        this.loginListeners.remove(loginListener);
    }

    public void performLogin(@NonNull Activity activity, @NonNull FragmentWrapper fragmentWrapper, boolean z, @NonNull String str, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        fragmentWrapper.startActivityForResult(getLoginIntent(activity, z, str, lineAuthenticationParams), REQUEST_CODE_LOGIN);
    }
}
