package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;
import java.util.List;

/* renamed from: com.linecorp.linesdk.api.internal.j */
/* compiled from: lambda */
public final /* synthetic */ class C4658j implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9533a;

    /* renamed from: b */
    private final /* synthetic */ String f9534b;

    /* renamed from: c */
    private final /* synthetic */ List f9535c;

    public /* synthetic */ C4658j(LineApiClientImpl lineApiClientImpl, String str, List list) {
        this.f9533a = lineApiClientImpl;
        this.f9534b = str;
        this.f9535c = list;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9533a.mo25660a(this.f9534b, this.f9535c, internalAccessToken);
    }
}
