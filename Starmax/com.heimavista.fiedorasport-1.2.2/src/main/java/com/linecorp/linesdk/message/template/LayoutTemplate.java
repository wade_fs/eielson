package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;
import com.linecorp.linesdk.message.Jsonable;
import org.json.JSONObject;

public abstract class LayoutTemplate implements Jsonable {
    @NonNull
    private final Type type;

    public LayoutTemplate(@NonNull Type type2) {
        this.type = type2;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", this.type.getServerKey());
        return jSONObject;
    }
}
