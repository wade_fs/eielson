package com.linecorp.linesdk;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scope {
    public static final Scope FRIEND = new Scope(NativeProtocol.AUDIENCE_FRIENDS);
    public static final Scope GROUP = new Scope("groups");
    public static final Scope MESSAGE = new Scope("message.write");
    public static final Scope OC_ADDRESS = new Scope("address");
    public static final Scope OC_BIRTHDATE = new Scope("birthdate");
    public static final Scope OC_EMAIL = new Scope(NotificationCompat.CATEGORY_EMAIL);
    public static final Scope OC_GENDER = new Scope("gender");
    public static final Scope OC_PHONE_NUMBER = new Scope("phone");
    public static final Scope OC_REAL_NAME = new Scope("real_name");
    public static final Scope ONE_TIME_SHARE = new Scope("onetime.share");
    public static final Scope OPENID_CONNECT = new Scope("openid");
    public static final Scope PROFILE = new Scope("profile");
    private static final String SCOPE_DELIMITER = " ";
    private static final Map<String, Scope> scopeInstanceMap = new HashMap();
    @NonNull
    private final String code;

    protected Scope(@NonNull String str) {
        if (!scopeInstanceMap.containsKey(str)) {
            this.code = str;
            scopeInstanceMap.put(str, this);
            return;
        }
        throw new IllegalArgumentException("Scope code already exists: " + str);
    }

    public static List<String> convertToCodeList(List<Scope> list) {
        ArrayList arrayList = new ArrayList();
        for (Scope scope : list) {
            arrayList.add(scope.code);
        }
        return arrayList;
    }

    public static List<Scope> convertToScopeList(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String str : list) {
            Scope findScope = findScope(str);
            if (findScope != null) {
                arrayList.add(findScope);
            }
        }
        return arrayList;
    }

    @Nullable
    public static Scope findScope(String str) {
        return scopeInstanceMap.get(str);
    }

    public static String join(@Nullable List<Scope> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        return TextUtils.join(SCOPE_DELIMITER, convertToCodeList(list));
    }

    public static List<Scope> parseToList(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return Collections.emptyList();
        }
        return convertToScopeList(Arrays.asList(str.split(SCOPE_DELIMITER)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Scope.class != obj.getClass()) {
            return false;
        }
        return this.code.equals(((Scope) obj).code);
    }

    @NonNull
    public String getCode() {
        return this.code;
    }

    public int hashCode() {
        return this.code.hashCode();
    }

    public String toString() {
        return "Scope{code='" + this.code + '\'' + '}';
    }
}
