package com.linecorp.linesdk.api.internal;

import androidx.annotation.NonNull;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.LineApiClient;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AutoRefreshLineApiClientProxy {

    private static class TokenAutoRefreshInvocationHandler implements InvocationHandler {
        @NonNull
        private final Map<Method, Boolean> autoRefreshStateCache;
        @NonNull
        private final LineApiClient target;

        private boolean isAutoRefreshEnabled(@NonNull Method method) {
            Boolean bool = this.autoRefreshStateCache.get(method);
            if (bool != null) {
                return bool.booleanValue();
            }
            String name = method.getName();
            Class<?>[] parameterTypes = method.getParameterTypes();
            Class<?> cls = this.target.getClass();
            while (cls != null) {
                try {
                    if (((TokenAutoRefresh) cls.getDeclaredMethod(name, parameterTypes).getAnnotation(TokenAutoRefresh.class)) != null) {
                        this.autoRefreshStateCache.put(method, true);
                        return true;
                    }
                    cls = cls.getSuperclass();
                } catch (NoSuchMethodException unused) {
                }
            }
            this.autoRefreshStateCache.put(method, false);
            return false;
        }

        private static boolean shouldRefreshToken(@NonNull Object obj) {
            if ((obj instanceof LineApiResponse) && ((LineApiResponse) obj).getErrorData().getHttpResponseCode() == 401) {
                return true;
            }
            return false;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            try {
                Object invoke = method.invoke(this.target, objArr);
                if (!isAutoRefreshEnabled(method) || !shouldRefreshToken(invoke)) {
                    return invoke;
                }
                LineApiResponse<LineAccessToken> refreshAccessToken = this.target.refreshAccessToken();
                if (!refreshAccessToken.isSuccess()) {
                    return refreshAccessToken.isNetworkError() ? refreshAccessToken : invoke;
                }
                try {
                    return method.invoke(this.target, objArr);
                } catch (InvocationTargetException e) {
                    throw e.getTargetException();
                }
            } catch (InvocationTargetException e2) {
                throw e2.getTargetException();
            }
        }

        private TokenAutoRefreshInvocationHandler(@NonNull LineApiClient lineApiClient) {
            this.target = lineApiClient;
            this.autoRefreshStateCache = new ConcurrentHashMap(0);
        }
    }

    private AutoRefreshLineApiClientProxy() {
    }

    @NonNull
    public static LineApiClient newProxy(@NonNull LineApiClient lineApiClient) {
        return (LineApiClient) Proxy.newProxyInstance(lineApiClient.getClass().getClassLoader(), new Class[]{LineApiClient.class}, new TokenAutoRefreshInvocationHandler(lineApiClient));
    }
}
