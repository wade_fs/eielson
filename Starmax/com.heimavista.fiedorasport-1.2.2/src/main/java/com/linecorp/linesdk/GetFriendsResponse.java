package com.linecorp.linesdk;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public class GetFriendsResponse {
    @NonNull
    private List<LineFriendProfile> friends;
    @Nullable
    private String nextPageRequestToken;

    public GetFriendsResponse(@NonNull List<LineFriendProfile> list) {
        this.friends = list;
    }

    @NonNull
    public List<LineFriendProfile> getFriends() {
        return this.friends;
    }

    @Nullable
    public String getNextPageRequestToken() {
        return this.nextPageRequestToken;
    }

    public String toString() {
        return "GetFriendsResponse{friends=" + this.friends + ", nextPageRequestToken='" + this.nextPageRequestToken + '\'' + '}';
    }

    public GetFriendsResponse(@NonNull List<LineFriendProfile> list, @Nullable String str) {
        this.friends = list;
        this.nextPageRequestToken = str;
    }
}
