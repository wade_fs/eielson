package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.d */
/* compiled from: lambda */
public final /* synthetic */ class C4652d implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9517a;

    /* renamed from: b */
    private final /* synthetic */ FriendSortField f9518b;

    /* renamed from: c */
    private final /* synthetic */ String f9519c;

    /* renamed from: d */
    private final /* synthetic */ boolean f9520d;

    public /* synthetic */ C4652d(LineApiClientImpl lineApiClientImpl, FriendSortField friendSortField, String str, boolean z) {
        this.f9517a = lineApiClientImpl;
        this.f9518b = friendSortField;
        this.f9519c = str;
        this.f9520d = z;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9517a.mo25658a(this.f9518b, this.f9519c, this.f9520d, internalAccessToken);
    }
}
