package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.linecorp.linesdk.message.template.LayoutTemplate;
import org.json.JSONObject;

public class TemplateMessage extends MessageData {
    @NonNull
    private String altText;
    @NonNull
    private LayoutTemplate template;

    public TemplateMessage(@NonNull String str, @NonNull LayoutTemplate layoutTemplate) {
        this.altText = str;
        this.template = layoutTemplate;
    }

    @NonNull
    public Type getType() {
        return Type.TEMPLATE;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("altText", this.altText);
        jsonObject.put(MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE, this.template.toJsonObject());
        return jsonObject;
    }
}
