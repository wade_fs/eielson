package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.c */
/* compiled from: lambda */
public final /* synthetic */ class C4651c implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9514a;

    /* renamed from: b */
    private final /* synthetic */ FriendSortField f9515b;

    /* renamed from: c */
    private final /* synthetic */ String f9516c;

    public /* synthetic */ C4651c(LineApiClientImpl lineApiClientImpl, FriendSortField friendSortField, String str) {
        this.f9514a = lineApiClientImpl;
        this.f9515b = friendSortField;
        this.f9516c = str;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9514a.mo25657a(this.f9515b, this.f9516c, internalAccessToken);
    }
}
