package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import p119e.p220f.p221a.p222a.SecurityUtils;

public class LineCredential implements Parcelable {
    public static final Parcelable.Creator<LineCredential> CREATOR = new Parcelable.Creator<LineCredential>() {
        /* class com.linecorp.linesdk.LineCredential.C46361 */

        public LineCredential createFromParcel(Parcel parcel) {
            return new LineCredential(parcel);
        }

        public LineCredential[] newArray(int i) {
            return new LineCredential[i];
        }
    };
    @NonNull
    private final LineAccessToken accessToken;
    @NonNull
    private final List<Scope> scopes;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineCredential.class != obj.getClass()) {
            return false;
        }
        LineCredential lineCredential = (LineCredential) obj;
        if (!this.accessToken.equals(lineCredential.accessToken)) {
            return false;
        }
        return this.scopes.equals(lineCredential.scopes);
    }

    @NonNull
    public LineAccessToken getAccessToken() {
        return this.accessToken;
    }

    @NonNull
    public List<Scope> getScopes() {
        return this.scopes;
    }

    public int hashCode() {
        return (this.accessToken.hashCode() * 31) + this.scopes.hashCode();
    }

    public String toString() {
        return "LineCredential{accessToken=" + SecurityUtils.m17194a(this.accessToken) + ", scopes=" + this.scopes + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.accessToken, i);
        parcel.writeStringList(Scope.convertToCodeList(this.scopes));
    }

    public LineCredential(@NonNull LineAccessToken lineAccessToken, @NonNull List<Scope> list) {
        this.accessToken = lineAccessToken;
        this.scopes = list;
    }

    private LineCredential(@NonNull Parcel parcel) {
        this.accessToken = (LineAccessToken) parcel.readParcelable(LineAccessToken.class.getClassLoader());
        ArrayList arrayList = new ArrayList(8);
        parcel.readStringList(arrayList);
        this.scopes = Scope.convertToScopeList(arrayList);
    }
}
