package com.linecorp.linesdk.internal;

import android.app.Fragment;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FragmentWrapper {
    @Nullable
    private Fragment fragment;
    @Nullable
    private androidx.fragment.app.Fragment supportFragment;

    public FragmentWrapper(@NonNull Fragment fragment2) {
        this.fragment = fragment2;
    }

    public void startActivityForResult(Intent intent, int i) {
        Fragment fragment2 = this.fragment;
        if (fragment2 != null) {
            fragment2.startActivityForResult(intent, i);
            return;
        }
        androidx.fragment.app.Fragment fragment3 = this.supportFragment;
        if (fragment3 != null) {
            fragment3.startActivityForResult(intent, i);
        }
    }

    public FragmentWrapper(@NonNull androidx.fragment.app.Fragment fragment2) {
        this.supportFragment = fragment2;
    }
}
