package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.utils.ParcelUtils;
import java.util.Date;
import java.util.List;

public class LineIdToken implements Parcelable {
    public static final Parcelable.Creator<LineIdToken> CREATOR = new Parcelable.Creator<LineIdToken>() {
        /* class com.linecorp.linesdk.LineIdToken.C46391 */

        public LineIdToken createFromParcel(Parcel parcel) {
            return new LineIdToken(parcel);
        }

        public LineIdToken[] newArray(int i) {
            return new LineIdToken[i];
        }
    };
    @Nullable
    private final Address address;
    @Nullable
    private final List<String> amr;
    @NonNull
    private final String audience;
    @Nullable
    private final Date authTime;
    @Nullable
    private final String birthdate;
    @Nullable
    private final String email;
    @NonNull
    private final Date expiresAt;
    @Nullable
    private final String familyName;
    @Nullable
    private final String familyNamePronunciation;
    @Nullable
    private final String gender;
    @Nullable
    private final String givenName;
    @Nullable
    private final String givenNamePronunciation;
    @NonNull
    private final Date issuedAt;
    @NonNull
    private final String issuer;
    @Nullable
    private final String middleName;
    @Nullable
    private final String name;
    @Nullable
    private final String nonce;
    @Nullable
    private final String phoneNumber;
    @Nullable
    private final String picture;
    @NonNull
    private final String rawString;
    @NonNull
    private final String subject;

    public static final class Builder {
        /* access modifiers changed from: private */
        public Address address;
        /* access modifiers changed from: private */
        public List<String> amr;
        /* access modifiers changed from: private */
        public String audience;
        /* access modifiers changed from: private */
        public Date authTime;
        /* access modifiers changed from: private */
        public String birthdate;
        /* access modifiers changed from: private */
        public String email;
        /* access modifiers changed from: private */
        public Date expiresAt;
        /* access modifiers changed from: private */
        public String familyName;
        /* access modifiers changed from: private */
        public String familyNamePronunciation;
        /* access modifiers changed from: private */
        public String gender;
        /* access modifiers changed from: private */
        public String givenName;
        /* access modifiers changed from: private */
        public String givenNamePronunciation;
        /* access modifiers changed from: private */
        public Date issuedAt;
        /* access modifiers changed from: private */
        public String issuer;
        /* access modifiers changed from: private */
        public String middleName;
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public String nonce;
        /* access modifiers changed from: private */
        public String phoneNumber;
        /* access modifiers changed from: private */
        public String picture;
        /* access modifiers changed from: private */
        public String rawString;
        /* access modifiers changed from: private */
        public String subject;

        public Builder address(Address address2) {
            this.address = address2;
            return this;
        }

        public Builder amr(List<String> list) {
            this.amr = list;
            return this;
        }

        public Builder audience(String str) {
            this.audience = str;
            return this;
        }

        public Builder authTime(Date date) {
            this.authTime = date;
            return this;
        }

        public Builder birthdate(String str) {
            this.birthdate = str;
            return this;
        }

        public LineIdToken build() {
            return new LineIdToken(this);
        }

        public Builder email(String str) {
            this.email = str;
            return this;
        }

        public Builder expiresAt(Date date) {
            this.expiresAt = date;
            return this;
        }

        public Builder familyName(String str) {
            this.familyName = str;
            return this;
        }

        public Builder familyNamePronunciation(String str) {
            this.familyNamePronunciation = str;
            return this;
        }

        public Builder gender(String str) {
            this.gender = str;
            return this;
        }

        public Builder givenName(String str) {
            this.givenName = str;
            return this;
        }

        public Builder givenNamePronunciation(String str) {
            this.givenNamePronunciation = str;
            return this;
        }

        public Builder issuedAt(Date date) {
            this.issuedAt = date;
            return this;
        }

        public Builder issuer(String str) {
            this.issuer = str;
            return this;
        }

        public Builder middleName(String str) {
            this.middleName = str;
            return this;
        }

        public Builder name(String str) {
            this.name = str;
            return this;
        }

        public Builder nonce(String str) {
            this.nonce = str;
            return this;
        }

        public Builder phoneNumber(String str) {
            this.phoneNumber = str;
            return this;
        }

        public Builder picture(String str) {
            this.picture = str;
            return this;
        }

        public Builder rawString(String str) {
            this.rawString = str;
            return this;
        }

        public Builder subject(String str) {
            this.subject = str;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineIdToken.class != obj.getClass()) {
            return false;
        }
        LineIdToken lineIdToken = (LineIdToken) obj;
        if (!this.rawString.equals(lineIdToken.rawString) || !this.issuer.equals(lineIdToken.issuer) || !this.subject.equals(lineIdToken.subject) || !this.audience.equals(lineIdToken.audience) || !this.expiresAt.equals(lineIdToken.expiresAt) || !this.issuedAt.equals(lineIdToken.issuedAt)) {
            return false;
        }
        Date date = this.authTime;
        if (date == null ? lineIdToken.authTime != null : !date.equals(lineIdToken.authTime)) {
            return false;
        }
        String str = this.nonce;
        if (str == null ? lineIdToken.nonce != null : !str.equals(lineIdToken.nonce)) {
            return false;
        }
        List<String> list = this.amr;
        if (list == null ? lineIdToken.amr != null : !list.equals(lineIdToken.amr)) {
            return false;
        }
        String str2 = this.name;
        if (str2 == null ? lineIdToken.name != null : !str2.equals(lineIdToken.name)) {
            return false;
        }
        String str3 = this.picture;
        if (str3 == null ? lineIdToken.picture != null : !str3.equals(lineIdToken.picture)) {
            return false;
        }
        String str4 = this.phoneNumber;
        if (str4 == null ? lineIdToken.phoneNumber != null : !str4.equals(lineIdToken.phoneNumber)) {
            return false;
        }
        String str5 = this.email;
        if (str5 == null ? lineIdToken.email != null : !str5.equals(lineIdToken.email)) {
            return false;
        }
        String str6 = this.gender;
        if (str6 == null ? lineIdToken.gender != null : !str6.equals(lineIdToken.gender)) {
            return false;
        }
        String str7 = this.birthdate;
        if (str7 == null ? lineIdToken.birthdate != null : !str7.equals(lineIdToken.birthdate)) {
            return false;
        }
        Address address2 = this.address;
        if (address2 == null ? lineIdToken.address != null : !address2.equals(lineIdToken.address)) {
            return false;
        }
        String str8 = this.givenName;
        if (str8 == null ? lineIdToken.givenName != null : !str8.equals(lineIdToken.givenName)) {
            return false;
        }
        String str9 = this.givenNamePronunciation;
        if (str9 == null ? lineIdToken.givenNamePronunciation != null : !str9.equals(lineIdToken.givenNamePronunciation)) {
            return false;
        }
        String str10 = this.middleName;
        if (str10 == null ? lineIdToken.middleName != null : !str10.equals(lineIdToken.middleName)) {
            return false;
        }
        String str11 = this.familyName;
        if (str11 == null ? lineIdToken.familyName != null : !str11.equals(lineIdToken.familyName)) {
            return false;
        }
        String str12 = this.familyNamePronunciation;
        String str13 = lineIdToken.familyNamePronunciation;
        if (str12 != null) {
            return str12.equals(str13);
        }
        if (str13 == null) {
            return true;
        }
        return false;
    }

    @Nullable
    public Address getAddress() {
        return this.address;
    }

    @Nullable
    public List<String> getAmr() {
        return this.amr;
    }

    @NonNull
    public String getAudience() {
        return this.audience;
    }

    @Nullable
    public Date getAuthTime() {
        return this.authTime;
    }

    @Nullable
    public String getBirthdate() {
        return this.birthdate;
    }

    @Nullable
    public String getEmail() {
        return this.email;
    }

    @NonNull
    public Date getExpiresAt() {
        return this.expiresAt;
    }

    @Nullable
    public String getFamilyName() {
        return this.familyName;
    }

    @Nullable
    public String getFamilyNamePronunciation() {
        return this.familyNamePronunciation;
    }

    @Nullable
    public String getGender() {
        return this.gender;
    }

    @Nullable
    public String getGivenName() {
        return this.givenName;
    }

    @Nullable
    public String getGivenNamePronunciation() {
        return this.givenNamePronunciation;
    }

    @NonNull
    public Date getIssuedAt() {
        return this.issuedAt;
    }

    @NonNull
    public String getIssuer() {
        return this.issuer;
    }

    @Nullable
    public String getMiddleName() {
        return this.middleName;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public String getNonce() {
        return this.nonce;
    }

    @Nullable
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    @Nullable
    public String getPicture() {
        return this.picture;
    }

    @NonNull
    public String getRawString() {
        return this.rawString;
    }

    @NonNull
    public String getSubject() {
        return this.subject;
    }

    public int hashCode() {
        int hashCode = ((((((((((this.rawString.hashCode() * 31) + this.issuer.hashCode()) * 31) + this.subject.hashCode()) * 31) + this.audience.hashCode()) * 31) + this.expiresAt.hashCode()) * 31) + this.issuedAt.hashCode()) * 31;
        Date date = this.authTime;
        int i = 0;
        int hashCode2 = (hashCode + (date != null ? date.hashCode() : 0)) * 31;
        String str = this.nonce;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        List<String> list = this.amr;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.picture;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.phoneNumber;
        int hashCode7 = (hashCode6 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.email;
        int hashCode8 = (hashCode7 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.gender;
        int hashCode9 = (hashCode8 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.birthdate;
        int hashCode10 = (hashCode9 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Address address2 = this.address;
        int hashCode11 = (hashCode10 + (address2 != null ? address2.hashCode() : 0)) * 31;
        String str8 = this.givenName;
        int hashCode12 = (hashCode11 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.givenNamePronunciation;
        int hashCode13 = (hashCode12 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.middleName;
        int hashCode14 = (hashCode13 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.familyName;
        int hashCode15 = (hashCode14 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.familyNamePronunciation;
        if (str12 != null) {
            i = str12.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        return "LineIdToken{rawString='" + this.rawString + '\'' + ", issuer='" + this.issuer + '\'' + ", subject='" + this.subject + '\'' + ", audience='" + this.audience + '\'' + ", expiresAt=" + this.expiresAt + ", issuedAt=" + this.issuedAt + ", authTime=" + this.authTime + ", nonce='" + this.nonce + '\'' + ", amr=" + this.amr + ", name='" + this.name + '\'' + ", picture='" + this.picture + '\'' + ", phoneNumber='" + this.phoneNumber + '\'' + ", email='" + this.email + '\'' + ", gender='" + this.gender + '\'' + ", birthdate='" + this.birthdate + '\'' + ", address=" + this.address + ", givenName='" + this.givenName + '\'' + ", givenNamePronunciation='" + this.givenNamePronunciation + '\'' + ", middleName='" + this.middleName + '\'' + ", familyName='" + this.familyName + '\'' + ", familyNamePronunciation='" + this.familyNamePronunciation + '\'' + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.rawString);
        parcel.writeString(this.issuer);
        parcel.writeString(this.subject);
        parcel.writeString(this.audience);
        ParcelUtils.writeDate(parcel, this.expiresAt);
        ParcelUtils.writeDate(parcel, this.issuedAt);
        ParcelUtils.writeDate(parcel, this.authTime);
        parcel.writeString(this.nonce);
        parcel.writeStringList(this.amr);
        parcel.writeString(this.name);
        parcel.writeString(this.picture);
        parcel.writeString(this.phoneNumber);
        parcel.writeString(this.email);
        parcel.writeString(this.gender);
        parcel.writeString(this.birthdate);
        parcel.writeParcelable(this.address, i);
        parcel.writeString(this.givenName);
        parcel.writeString(this.givenNamePronunciation);
        parcel.writeString(this.middleName);
        parcel.writeString(this.familyName);
        parcel.writeString(this.familyNamePronunciation);
    }

    public static class Address implements Parcelable {
        public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
            /* class com.linecorp.linesdk.LineIdToken.Address.C46401 */

            public Address createFromParcel(Parcel parcel) {
                return new Address(parcel);
            }

            public Address[] newArray(int i) {
                return new Address[i];
            }
        };
        @Nullable
        private final String country;
        @Nullable
        private final String locality;
        @Nullable
        private final String postalCode;
        @Nullable
        private final String region;
        @Nullable
        private final String streetAddress;

        public static final class Builder {
            /* access modifiers changed from: private */
            public String country;
            /* access modifiers changed from: private */
            public String locality;
            /* access modifiers changed from: private */
            public String postalCode;
            /* access modifiers changed from: private */
            public String region;
            /* access modifiers changed from: private */
            public String streetAddress;

            public Address build() {
                return new Address(this);
            }

            public Builder country(String str) {
                this.country = str;
                return this;
            }

            public Builder locality(String str) {
                this.locality = str;
                return this;
            }

            public Builder postalCode(String str) {
                this.postalCode = str;
                return this;
            }

            public Builder region(String str) {
                this.region = str;
                return this;
            }

            public Builder streetAddress(String str) {
                this.streetAddress = str;
                return this;
            }
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Address.class != obj.getClass()) {
                return false;
            }
            Address address = (Address) obj;
            String str = this.streetAddress;
            if (str == null ? address.streetAddress != null : !str.equals(address.streetAddress)) {
                return false;
            }
            String str2 = this.locality;
            if (str2 == null ? address.locality != null : !str2.equals(address.locality)) {
                return false;
            }
            String str3 = this.region;
            if (str3 == null ? address.region != null : !str3.equals(address.region)) {
                return false;
            }
            String str4 = this.postalCode;
            if (str4 == null ? address.postalCode != null : !str4.equals(address.postalCode)) {
                return false;
            }
            String str5 = this.country;
            String str6 = address.country;
            if (str5 != null) {
                return str5.equals(str6);
            }
            if (str6 == null) {
                return true;
            }
            return false;
        }

        @Nullable
        public String getCountry() {
            return this.country;
        }

        @Nullable
        public String getLocality() {
            return this.locality;
        }

        @Nullable
        public String getPostalCode() {
            return this.postalCode;
        }

        @Nullable
        public String getRegion() {
            return this.region;
        }

        @Nullable
        public String getStreetAddress() {
            return this.streetAddress;
        }

        public int hashCode() {
            String str = this.streetAddress;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.locality;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.region;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.postalCode;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.country;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "Address{streetAddress='" + this.streetAddress + '\'' + ", locality='" + this.locality + '\'' + ", region='" + this.region + '\'' + ", postalCode='" + this.postalCode + '\'' + ", country='" + this.country + '\'' + '}';
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.streetAddress);
            parcel.writeString(this.locality);
            parcel.writeString(this.region);
            parcel.writeString(this.postalCode);
            parcel.writeString(this.country);
        }

        private Address(Builder builder) {
            this.streetAddress = builder.streetAddress;
            this.locality = builder.locality;
            this.region = builder.region;
            this.postalCode = builder.postalCode;
            this.country = builder.country;
        }

        private Address(@NonNull Parcel parcel) {
            this.streetAddress = parcel.readString();
            this.locality = parcel.readString();
            this.region = parcel.readString();
            this.postalCode = parcel.readString();
            this.country = parcel.readString();
        }
    }

    private LineIdToken(Builder builder) {
        this.rawString = builder.rawString;
        this.issuer = builder.issuer;
        this.subject = builder.subject;
        this.audience = builder.audience;
        this.expiresAt = builder.expiresAt;
        this.issuedAt = builder.issuedAt;
        this.authTime = builder.authTime;
        this.nonce = builder.nonce;
        this.amr = builder.amr;
        this.name = builder.name;
        this.picture = builder.picture;
        this.phoneNumber = builder.phoneNumber;
        this.email = builder.email;
        this.gender = builder.gender;
        this.birthdate = builder.birthdate;
        this.address = builder.address;
        this.givenName = builder.givenName;
        this.givenNamePronunciation = builder.givenNamePronunciation;
        this.middleName = builder.middleName;
        this.familyName = builder.familyName;
        this.familyNamePronunciation = builder.familyNamePronunciation;
    }

    private LineIdToken(@NonNull Parcel parcel) {
        this.rawString = parcel.readString();
        this.issuer = parcel.readString();
        this.subject = parcel.readString();
        this.audience = parcel.readString();
        this.expiresAt = ParcelUtils.readDate(parcel);
        this.issuedAt = ParcelUtils.readDate(parcel);
        this.authTime = ParcelUtils.readDate(parcel);
        this.nonce = parcel.readString();
        this.amr = parcel.createStringArrayList();
        this.name = parcel.readString();
        this.picture = parcel.readString();
        this.phoneNumber = parcel.readString();
        this.email = parcel.readString();
        this.gender = parcel.readString();
        this.birthdate = parcel.readString();
        this.address = (Address) parcel.readParcelable(Address.class.getClassLoader());
        this.givenName = parcel.readString();
        this.givenNamePronunciation = parcel.readString();
        this.middleName = parcel.readString();
        this.familyName = parcel.readString();
        this.familyNamePronunciation = parcel.readString();
    }
}
