package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexSeparatorComponent extends FlexMessageComponent {
    @Nullable
    private String color;
    @Nullable
    private FlexMessageComponent.Margin margin;

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public String color;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;

        public FlexSeparatorComponent build() {
            return new FlexSeparatorComponent(this);
        }

        public Builder setColor(@Nullable String str) {
            this.color = str;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        private Builder() {
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, "margin", this.margin);
        JSONUtils.put(jsonObject, TtmlNode.ATTR_TTS_COLOR, this.color);
        return jsonObject;
    }

    public FlexSeparatorComponent() {
        super(FlexMessageComponent.Type.SEPARATOR);
    }

    private FlexSeparatorComponent(@NonNull Builder builder) {
        this();
        this.margin = builder.margin;
        this.color = builder.color;
    }
}
