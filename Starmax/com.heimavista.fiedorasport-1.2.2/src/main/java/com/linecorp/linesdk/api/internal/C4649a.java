package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.nwclient.TalkApiClient;

/* renamed from: com.linecorp.linesdk.api.internal.a */
/* compiled from: lambda */
public final /* synthetic */ class C4649a implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ TalkApiClient f9512a;

    public /* synthetic */ C4649a(TalkApiClient talkApiClient) {
        this.f9512a = talkApiClient;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9512a.getProfile(internalAccessToken);
    }
}
