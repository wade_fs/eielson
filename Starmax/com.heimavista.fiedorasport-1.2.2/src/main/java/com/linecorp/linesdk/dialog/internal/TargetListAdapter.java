package com.linecorp.linesdk.dialog.internal;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import java.util.ArrayList;
import java.util.List;

public class TargetListAdapter extends RecyclerView.Adapter<TargetViewHolder> {
    /* access modifiers changed from: private */
    public OnSelectedChangeListener externalListener;
    private OnSelectedChangeListener listener = new OnSelectedChangeListener() {
        /* class com.linecorp.linesdk.dialog.internal.TargetListAdapter.C46722 */

        public void onSelected(TargetUser targetUser, boolean z) {
            TargetListAdapter.this.externalListener.onSelected(targetUser, z);
        }
    };
    /* access modifiers changed from: private */
    public List<TargetUser> originalTargetList;
    /* access modifiers changed from: private */
    public String queryString = "";
    private List<TargetUser> targetList;

    public interface OnSelectedChangeListener {
        void onSelected(TargetUser targetUser, boolean z);
    }

    public class TargetViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBox;
        private int highlightTextColor = 0;
        private ImageView imageView;
        private TextView textView;
        private ViewGroup viewContainer;

        public TargetViewHolder(ViewGroup viewGroup) {
            super(viewGroup);
            this.viewContainer = viewGroup;
            this.textView = (TextView) viewGroup.findViewById(C4642R.C4645id.textView);
            this.imageView = (ImageView) viewGroup.findViewById(C4642R.C4645id.imageView);
            this.checkBox = (CheckBox) viewGroup.findViewById(C4642R.C4645id.checkBox);
            this.highlightTextColor = viewGroup.getResources().getColor(C4642R.C4643color.text_highlight);
        }

        private SpannableString createHighlightTextSpan(String str, String str2) {
            int indexOf;
            SpannableString spannableString = new SpannableString(str);
            if (!str2.isEmpty() && (indexOf = str.toLowerCase().indexOf(str2.toLowerCase())) != -1) {
                spannableString.setSpan(new ForegroundColorSpan(this.highlightTextColor), indexOf, str2.length() + indexOf, 0);
            }
            return spannableString;
        }

        /* renamed from: a */
        public /* synthetic */ void mo25822a(TargetUser targetUser, OnSelectedChangeListener onSelectedChangeListener, View view) {
            boolean z = !targetUser.getSelected().booleanValue();
            this.viewContainer.setSelected(z);
            targetUser.setSelected(Boolean.valueOf(z));
            this.checkBox.setChecked(z);
            onSelectedChangeListener.onSelected(targetUser, z);
        }

        public void bind(TargetUser targetUser, OnSelectedChangeListener onSelectedChangeListener) {
            this.viewContainer.setSelected(targetUser.getSelected().booleanValue());
            this.checkBox.setChecked(targetUser.getSelected().booleanValue());
            this.textView.setText(createHighlightTextSpan(targetUser.getDisplayName(), TargetListAdapter.this.queryString));
            this.viewContainer.setOnClickListener(new C4675b(this, targetUser, onSelectedChangeListener));
            int i = targetUser.getType() == TargetUser.Type.FRIEND ? C4642R.C4644drawable.friend_thumbnail : C4642R.C4644drawable.group_thumbnail;
            RequestCreator a = Picasso.m15799b().mo26309a(targetUser.getPictureUri());
            a.mo26345a(i);
            a.mo26347a(this.imageView);
        }
    }

    public TargetListAdapter(List<TargetUser> list, OnSelectedChangeListener onSelectedChangeListener) {
        this.originalTargetList = list;
        this.targetList = new ArrayList<TargetUser>() {
            /* class com.linecorp.linesdk.dialog.internal.TargetListAdapter.C46711 */

            {
                addAll(TargetListAdapter.this.originalTargetList);
            }
        };
        this.externalListener = onSelectedChangeListener;
    }

    public void addAll(List<TargetUser> list) {
        this.originalTargetList.addAll(list);
        this.targetList.addAll(list);
        notifyItemRangeInserted(this.targetList.size() - 1, list.size());
    }

    public int filter(String str) {
        this.queryString = str;
        this.targetList.clear();
        if (str.isEmpty()) {
            this.targetList.addAll(this.originalTargetList);
        } else {
            String lowerCase = str.toLowerCase();
            for (TargetUser targetUser : this.originalTargetList) {
                if (targetUser.getDisplayName().toLowerCase().contains(lowerCase)) {
                    this.targetList.add(targetUser);
                }
            }
        }
        notifyDataSetChanged();
        return this.targetList.size();
    }

    public int getItemCount() {
        return this.targetList.size();
    }

    public void unSelect(TargetUser targetUser) {
        for (int i = 0; i < this.targetList.size(); i++) {
            TargetUser targetUser2 = this.targetList.get(i);
            if (targetUser2.getId().equals(targetUser.getId())) {
                targetUser2.setSelected(false);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public void onBindViewHolder(TargetViewHolder targetViewHolder, int i) {
        targetViewHolder.bind(this.targetList.get(i), this.listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public TargetViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new TargetViewHolder((ViewGroup) LayoutInflater.from(viewGroup.getContext()).inflate(C4642R.C4646layout.layout_target_item, viewGroup, false));
    }
}
