package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.f */
/* compiled from: lambda */
public final /* synthetic */ class C4654f implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9522a;

    /* renamed from: b */
    private final /* synthetic */ String f9523b;

    /* renamed from: c */
    private final /* synthetic */ String f9524c;

    public /* synthetic */ C4654f(LineApiClientImpl lineApiClientImpl, String str, String str2) {
        this.f9522a = lineApiClientImpl;
        this.f9523b = str;
        this.f9524c = str2;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9522a.mo25659a(this.f9523b, this.f9524c, internalAccessToken);
    }
}
