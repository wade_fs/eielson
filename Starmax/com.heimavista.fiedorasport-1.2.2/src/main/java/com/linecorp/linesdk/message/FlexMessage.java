package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import com.linecorp.linesdk.message.flex.container.FlexMessageContainer;
import org.json.JSONObject;

public class FlexMessage extends MessageData {
    @NonNull
    private String altText;
    @NonNull
    private FlexMessageContainer contents;

    public FlexMessage(@NonNull String str, @NonNull FlexMessageContainer flexMessageContainer) {
        this.altText = str;
        this.contents = flexMessageContainer;
    }

    @NonNull
    public Type getType() {
        return Type.FLEX;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("altText", this.altText);
        jsonObject.put("contents", this.contents.toJsonObject());
        return jsonObject;
    }
}
