package com.linecorp.linesdk.auth;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.utils.ParcelUtils;
import java.util.List;
import java.util.Locale;

public class LineAuthenticationParams implements Parcelable {
    public static final Parcelable.Creator<LineAuthenticationParams> CREATOR = new Parcelable.Creator<LineAuthenticationParams>() {
        /* class com.linecorp.linesdk.auth.LineAuthenticationParams.C46601 */

        public LineAuthenticationParams createFromParcel(Parcel parcel) {
            return new LineAuthenticationParams(parcel);
        }

        public LineAuthenticationParams[] newArray(int i) {
            return new LineAuthenticationParams[i];
        }
    };
    @Nullable
    private final BotPrompt botPrompt;
    @Nullable
    private final String nonce;
    @NonNull
    private final List<Scope> scopes;
    @Nullable
    private final Locale uiLocale;

    public enum BotPrompt {
        normal,
        aggressive
    }

    public static final class Builder {
        /* access modifiers changed from: private */
        public BotPrompt botPrompt;
        /* access modifiers changed from: private */
        public String nonce;
        /* access modifiers changed from: private */
        public List<Scope> scopes;
        /* access modifiers changed from: private */
        public Locale uiLocale;

        public Builder botPrompt(BotPrompt botPrompt2) {
            this.botPrompt = botPrompt2;
            return this;
        }

        public LineAuthenticationParams build() {
            return new LineAuthenticationParams(this);
        }

        public Builder nonce(String str) {
            this.nonce = str;
            return this;
        }

        public Builder scopes(List<Scope> list) {
            this.scopes = list;
            return this;
        }

        public Builder uiLocale(Locale locale) {
            this.uiLocale = locale;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    @Nullable
    public BotPrompt getBotPrompt() {
        return this.botPrompt;
    }

    @Nullable
    public String getNonce() {
        return this.nonce;
    }

    @NonNull
    public List<Scope> getScopes() {
        return this.scopes;
    }

    @Nullable
    public Locale getUILocale() {
        return this.uiLocale;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(Scope.convertToCodeList(this.scopes));
        parcel.writeString(this.nonce);
        ParcelUtils.writeEnum(parcel, this.botPrompt);
        parcel.writeSerializable(this.uiLocale);
    }

    private LineAuthenticationParams(Builder builder) {
        this.scopes = builder.scopes;
        this.nonce = builder.nonce;
        this.botPrompt = builder.botPrompt;
        this.uiLocale = builder.uiLocale;
    }

    private LineAuthenticationParams(@NonNull Parcel parcel) {
        this.scopes = Scope.convertToScopeList(parcel.createStringArrayList());
        this.nonce = parcel.readString();
        this.botPrompt = (BotPrompt) ParcelUtils.readEnum(parcel, BotPrompt.class);
        this.uiLocale = (Locale) parcel.readSerializable();
    }
}
