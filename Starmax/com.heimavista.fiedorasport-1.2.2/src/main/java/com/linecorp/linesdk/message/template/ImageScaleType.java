package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;

enum ImageScaleType {
    COVER("cover"),
    CONTAIN("contain");
    
    @NonNull
    private String serverKey;

    private ImageScaleType(@NonNull String str) {
        this.serverKey = str;
    }

    @NonNull
    public String getServerKey() {
        return this.serverKey;
    }
}
