package com.linecorp.linesdk.internal.nwclient;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.facebook.AccessToken;
import com.facebook.internal.ServerProtocol;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineIdToken;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.internal.AccessTokenVerificationResult;
import com.linecorp.linesdk.internal.IdTokenKeyType;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.IssueAccessTokenResult;
import com.linecorp.linesdk.internal.JWKSet;
import com.linecorp.linesdk.internal.OneTimePassword;
import com.linecorp.linesdk.internal.OpenIdDiscoveryDocument;
import com.linecorp.linesdk.internal.RefreshTokenResult;
import com.linecorp.linesdk.internal.nwclient.core.ChannelServiceHttpClient;
import com.linecorp.linesdk.internal.nwclient.core.ResponseDataParser;
import com.linecorp.linesdk.utils.UriUtils;
import java.util.Collections;
import org.json.JSONException;
import org.json.JSONObject;

public class LineAuthenticationApiClient {
    private static final String AVAILABLE_TOKEN_TYPE = "Bearer";
    private static final String BASE_PATH_OAUTH_V21_API = "oauth2/v2.1";
    private static final ResponseDataParser<JWKSet> JWK_SET_PARSER = new JWKSetParser();
    private static final ResponseDataParser<?> NO_RESULT_RESPONSE_PARSER = new NoResultResponseParser();
    private static final ResponseDataParser<OneTimePassword> ONE_TIME_PASSWORD_PARSER = new OneTimePasswordParser();
    private static final ResponseDataParser<OpenIdDiscoveryDocument> OPEN_ID_DISCOVERY_DOCUMENT_PARSER = new OpenIdDiscoveryDocumentParser();
    private static final ResponseDataParser<RefreshTokenResult> REFRESH_TOKEN_RESULT_PARSER = new RefreshTokenResultParser();
    private static final String TAG = "LineAuthApiClient";
    private static final ResponseDataParser<AccessTokenVerificationResult> VERIFICATION_RESULT_PARSER = new VerificationResultParser();
    private final ResponseDataParser<IssueAccessTokenResult> ISSUE_ACCESS_TOKEN_RESULT_PARSER;
    @NonNull
    private final Uri apiBaseUrl;
    @NonNull
    private final ChannelServiceHttpClient httpClient;
    @NonNull
    private final Uri openidDiscoveryDocumentUrl;
    /* access modifiers changed from: private */
    public final OpenIdSigningKeyResolver signingKeyResolver;

    private class IssueAccessTokenResultParser extends JsonToObjectBaseResponseParser<IssueAccessTokenResult> {
        private IssueAccessTokenResultParser() {
        }

        private LineIdToken parseIdToken(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            return IdTokenParser.parse(str, LineAuthenticationApiClient.this.signingKeyResolver);
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public IssueAccessTokenResult parseJsonToObject(@NonNull JSONObject jSONObject) {
            String string = jSONObject.getString("token_type");
            if (LineAuthenticationApiClient.AVAILABLE_TOKEN_TYPE.equals(string)) {
                try {
                    return new IssueAccessTokenResult(new InternalAccessToken(jSONObject.getString("access_token"), 1000 * jSONObject.getLong(AccessToken.EXPIRES_IN_KEY), System.currentTimeMillis(), jSONObject.getString("refresh_token")), Scope.parseToList(jSONObject.getString("scope")), parseIdToken(jSONObject.optString("id_token")));
                } catch (Exception e) {
                    throw new JSONException(e.getMessage());
                }
            } else {
                throw new JSONException("Illegal token type. token_type=" + string);
            }
        }
    }

    private static class OneTimePasswordParser extends JsonToObjectBaseResponseParser<OneTimePassword> {
        private OneTimePasswordParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public OneTimePassword parseJsonToObject(@NonNull JSONObject jSONObject) {
            return new OneTimePassword(jSONObject.getString("otpId"), jSONObject.getString("otp"));
        }
    }

    private static class RefreshTokenResultParser extends JsonToObjectBaseResponseParser<RefreshTokenResult> {
        private RefreshTokenResultParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public RefreshTokenResult parseJsonToObject(@NonNull JSONObject jSONObject) {
            String string = jSONObject.getString("token_type");
            if (LineAuthenticationApiClient.AVAILABLE_TOKEN_TYPE.equals(string)) {
                return new RefreshTokenResult(jSONObject.getString("access_token"), 1000 * jSONObject.getLong(AccessToken.EXPIRES_IN_KEY), jSONObject.getString("refresh_token"), Scope.parseToList(jSONObject.getString("scope")));
            }
            throw new JSONException("Illegal token type. token_type=" + string);
        }
    }

    private static class VerificationResultParser extends JsonToObjectBaseResponseParser<AccessTokenVerificationResult> {
        private VerificationResultParser() {
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public AccessTokenVerificationResult parseJsonToObject(@NonNull JSONObject jSONObject) {
            return new AccessTokenVerificationResult(jSONObject.getString("client_id"), jSONObject.getLong(AccessToken.EXPIRES_IN_KEY) * 1000, Scope.parseToList(jSONObject.getString("scope")));
        }
    }

    public LineAuthenticationApiClient(@NonNull Context context, @NonNull Uri uri, @NonNull Uri uri2) {
        this(uri, uri2, new ChannelServiceHttpClient(context, BuildConfig.VERSION_NAME));
    }

    @NonNull
    public LineApiResponse<JWKSet> getJWKSet() {
        LineApiResponse<OpenIdDiscoveryDocument> openIdDiscoveryDocument = getOpenIdDiscoveryDocument();
        if (!openIdDiscoveryDocument.isSuccess()) {
            return LineApiResponse.createAsError(openIdDiscoveryDocument.getResponseCode(), openIdDiscoveryDocument.getErrorData());
        }
        LineApiResponse<JWKSet> lineApiResponse = this.httpClient.get(Uri.parse(openIdDiscoveryDocument.getResponseData().getJwksUri()), Collections.emptyMap(), Collections.emptyMap(), JWK_SET_PARSER);
        if (!lineApiResponse.isSuccess()) {
            Log.e(TAG, "getJWKSet failed: " + lineApiResponse);
        }
        return lineApiResponse;
    }

    @NonNull
    public LineApiResponse<OneTimePassword> getOneTimeIdAndPassword(@NonNull String str) {
        return this.httpClient.post(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "otp"), Collections.emptyMap(), UriUtils.buildParams("client_id", str), ONE_TIME_PASSWORD_PARSER);
    }

    @NonNull
    public LineApiResponse<OpenIdDiscoveryDocument> getOpenIdDiscoveryDocument() {
        LineApiResponse<OpenIdDiscoveryDocument> lineApiResponse = this.httpClient.get(UriUtils.buildUri(this.openidDiscoveryDocumentUrl, new String[0]), Collections.emptyMap(), Collections.emptyMap(), OPEN_ID_DISCOVERY_DOCUMENT_PARSER);
        if (!lineApiResponse.isSuccess()) {
            Log.e(TAG, "getOpenIdDiscoveryDocument failed: " + lineApiResponse);
        }
        return lineApiResponse;
    }

    @NonNull
    public LineApiResponse<IssueAccessTokenResult> issueAccessToken(@NonNull String str, @NonNull String str2, @NonNull OneTimePassword oneTimePassword, @NonNull String str3) {
        return this.httpClient.post(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "token"), Collections.emptyMap(), UriUtils.buildParams("grant_type", "authorization_code", "code", str2, ServerProtocol.DIALOG_PARAM_REDIRECT_URI, str3, "client_id", str, "otp", oneTimePassword.getPassword(), "id_token_key_type", IdTokenKeyType.JWK.name(), "client_version", "LINE SDK Android v5.4.0"), this.ISSUE_ACCESS_TOKEN_RESULT_PARSER);
    }

    @NonNull
    public LineApiResponse<RefreshTokenResult> refreshToken(@NonNull String str, @NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.post(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "token"), Collections.emptyMap(), UriUtils.buildParams("grant_type", "refresh_token", "refresh_token", internalAccessToken.getRefreshToken(), "client_id", str), REFRESH_TOKEN_RESULT_PARSER);
    }

    @NonNull
    public LineApiResponse<?> revokeAccessToken(@NonNull String str, @NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.post(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "revoke"), Collections.emptyMap(), UriUtils.buildParams("access_token", internalAccessToken.getAccessToken(), "client_id", str), NO_RESULT_RESPONSE_PARSER);
    }

    @NonNull
    public LineApiResponse<?> revokeRefreshToken(@NonNull String str, @NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.post(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "revoke"), Collections.emptyMap(), UriUtils.buildParams("refresh_token", internalAccessToken.getRefreshToken(), "client_id", str), NO_RESULT_RESPONSE_PARSER);
    }

    @NonNull
    public LineApiResponse<AccessTokenVerificationResult> verifyAccessToken(@NonNull InternalAccessToken internalAccessToken) {
        return this.httpClient.get(UriUtils.buildUri(this.apiBaseUrl, BASE_PATH_OAUTH_V21_API, "verify"), Collections.emptyMap(), UriUtils.buildParams("access_token", internalAccessToken.getAccessToken()), VERIFICATION_RESULT_PARSER);
    }

    @VisibleForTesting
    LineAuthenticationApiClient(@NonNull Uri uri, @NonNull Uri uri2, @NonNull ChannelServiceHttpClient channelServiceHttpClient) {
        this.ISSUE_ACCESS_TOKEN_RESULT_PARSER = new IssueAccessTokenResultParser();
        this.signingKeyResolver = new OpenIdSigningKeyResolver(this);
        this.openidDiscoveryDocumentUrl = uri;
        this.apiBaseUrl = uri2;
        this.httpClient = channelServiceHttpClient;
    }
}
