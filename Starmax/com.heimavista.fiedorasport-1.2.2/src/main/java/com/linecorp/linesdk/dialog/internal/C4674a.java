package com.linecorp.linesdk.dialog.internal;

import com.linecorp.linesdk.dialog.internal.GetTargetUserTask;
import java.util.List;

/* renamed from: com.linecorp.linesdk.dialog.internal.a */
/* compiled from: lambda */
public final /* synthetic */ class C4674a implements GetTargetUserTask.NextAction {

    /* renamed from: a */
    private final /* synthetic */ TargetListWithSearchView f9542a;

    public /* synthetic */ C4674a(TargetListWithSearchView targetListWithSearchView) {
        this.f9542a = targetListWithSearchView;
    }

    public final void run(List list) {
        this.f9542a.addTargetUsers(list);
    }
}
