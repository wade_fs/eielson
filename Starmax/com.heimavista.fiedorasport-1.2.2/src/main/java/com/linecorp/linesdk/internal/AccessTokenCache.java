package com.linecorp.linesdk.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.linecorp.linesdk.utils.ObjectUtils;
import p119e.p220f.p221a.p222a.p223c.EncryptionException;
import p119e.p220f.p221a.p222a.p223c.StringCipher;

public class AccessTokenCache {
    private static final String DATA_KEY_ACCESS_TOKEN = "accessToken";
    private static final String DATA_KEY_EXPIRES_IN_MILLIS = "expiresIn";
    private static final String DATA_KEY_ISSUED_CLIENT_TIME_MILLIS = "issuedClientTime";
    private static final String DATA_KEY_REFRESH_TOKEN = "refreshToken";
    private static final long NO_DATA = -1;
    private static final String SHARED_PREFERENCE_KEY_PREFIX = "com.linecorp.linesdk.accesstoken.";
    @NonNull
    private final Context context;
    @NonNull
    private final StringCipher encryptor;
    @NonNull
    private final String sharedPreferenceKey;

    public AccessTokenCache(@NonNull Context context2, @NonNull String str) {
        this(context2.getApplicationContext(), str, EncryptorHolder.getEncryptor());
    }

    private long decryptToLong(@Nullable String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.valueOf(this.encryptor.mo27290a(this.context, str)).longValue();
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @Nullable
    private String decryptToString(@Nullable String str) {
        if (str == null) {
            return null;
        }
        return this.encryptor.mo27290a(this.context, str);
    }

    @NonNull
    private String encryptLong(long j) {
        return this.encryptor.mo27292b(this.context, String.valueOf(j));
    }

    @NonNull
    private String encryptString(@NonNull String str) {
        return this.encryptor.mo27292b(this.context, str);
    }

    public void clear() {
        this.context.getSharedPreferences(this.sharedPreferenceKey, 0).edit().clear().apply();
    }

    @Nullable
    public InternalAccessToken getAccessToken() {
        SharedPreferences sharedPreferences = this.context.getSharedPreferences(this.sharedPreferenceKey, 0);
        try {
            String decryptToString = decryptToString(sharedPreferences.getString(DATA_KEY_ACCESS_TOKEN, null));
            long decryptToLong = decryptToLong(sharedPreferences.getString(DATA_KEY_EXPIRES_IN_MILLIS, null));
            long decryptToLong2 = decryptToLong(sharedPreferences.getString(DATA_KEY_ISSUED_CLIENT_TIME_MILLIS, null));
            if (TextUtils.isEmpty(decryptToString) || decryptToLong == -1 || decryptToLong2 == -1) {
                return null;
            }
            return new InternalAccessToken(decryptToString, decryptToLong, decryptToLong2, (String) ObjectUtils.defaultIfNull(decryptToString(sharedPreferences.getString(DATA_KEY_REFRESH_TOKEN, null)), ""));
        } catch (EncryptionException unused) {
            clear();
            return null;
        }
    }

    public void saveAccessToken(@NonNull InternalAccessToken internalAccessToken) {
        this.context.getSharedPreferences(this.sharedPreferenceKey, 0).edit().putString(DATA_KEY_ACCESS_TOKEN, encryptString(internalAccessToken.getAccessToken())).putString(DATA_KEY_EXPIRES_IN_MILLIS, encryptLong(internalAccessToken.getExpiresInMillis())).putString(DATA_KEY_ISSUED_CLIENT_TIME_MILLIS, encryptLong(internalAccessToken.getIssuedClientTimeMillis())).putString(DATA_KEY_REFRESH_TOKEN, encryptString(internalAccessToken.getRefreshToken())).apply();
    }

    @VisibleForTesting
    public AccessTokenCache(@NonNull Context context2, @NonNull String str, @NonNull StringCipher bVar) {
        this.context = context2;
        this.sharedPreferenceKey = SHARED_PREFERENCE_KEY_PREFIX + str;
        this.encryptor = bVar;
    }
}
