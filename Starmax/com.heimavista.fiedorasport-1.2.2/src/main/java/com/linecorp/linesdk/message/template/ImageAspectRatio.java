package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;

public enum ImageAspectRatio {
    RECTANGLE("rectangle"),
    SQUARE(MessengerShareContentUtility.IMAGE_RATIO_SQUARE);
    
    @NonNull
    private String serverKey;

    private ImageAspectRatio(@NonNull String str) {
        this.serverKey = str;
    }

    @NonNull
    public String getServerKey() {
        return this.serverKey;
    }
}
