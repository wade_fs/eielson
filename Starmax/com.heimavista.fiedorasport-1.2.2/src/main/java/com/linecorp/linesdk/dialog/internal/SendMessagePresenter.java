package com.linecorp.linesdk.dialog.internal;

import android.os.AsyncTask;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.dialog.internal.GetTargetUserTask;
import com.linecorp.linesdk.dialog.internal.SendMessageContract;
import com.linecorp.linesdk.dialog.internal.TargetListAdapter;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import com.linecorp.linesdk.message.MessageData;
import java.util.ArrayList;
import java.util.List;

public class SendMessagePresenter implements SendMessageContract.Presenter, TargetListAdapter.OnSelectedChangeListener {
    private static final int MAX_TARGET_SIZE = 10;
    private ApiStatusListener apiStatusListener = new ApiStatusListener() {
        /* class com.linecorp.linesdk.dialog.internal.SendMessagePresenter.C46681 */

        public void onFailure() {
            SendMessagePresenter.this.view.onSendMessageFailure();
        }

        public void onSuccess() {
            SendMessagePresenter.this.view.onSendMessageSuccess();
        }
    };
    private List<AsyncTask> asyncTaskList = new ArrayList();
    private LineApiClient lineApiClient;
    private List<TargetUser> targetUserList = new ArrayList();
    /* access modifiers changed from: private */
    public SendMessageContract.View view;

    public SendMessagePresenter(LineApiClient lineApiClient2, SendMessageContract.View view2) {
        this.lineApiClient = lineApiClient2;
        this.view = view2;
    }

    private void getTargets(TargetUser.Type type, GetTargetUserTask.NextAction nextAction) {
        GetTargetUserTask getTargetUserTask = new GetTargetUserTask(type, this.lineApiClient, nextAction);
        getTargetUserTask.execute(new Void[0]);
        this.asyncTaskList.add(getTargetUserTask);
    }

    public void addTargetUser(TargetUser targetUser) {
        this.targetUserList.add(targetUser);
        this.view.onTargetUserAdded(targetUser);
    }

    public void getFriends(GetTargetUserTask.NextAction nextAction) {
        getTargets(TargetUser.Type.FRIEND, nextAction);
    }

    public void getGroups(GetTargetUserTask.NextAction nextAction) {
        getTargets(TargetUser.Type.GROUP, nextAction);
    }

    public int getTargetUserListSize() {
        return this.targetUserList.size();
    }

    public void onSelected(TargetUser targetUser, boolean z) {
        if (!z) {
            removeTargetUser(targetUser);
        } else if (this.targetUserList.size() < 10) {
            addTargetUser(targetUser);
        } else {
            this.view.onTargetUserRemoved(targetUser);
            this.view.onExceedMaxTargetUserCount(10);
        }
    }

    public void release() {
        for (AsyncTask asyncTask : this.asyncTaskList) {
            asyncTask.cancel(true);
        }
    }

    public void removeTargetUser(TargetUser targetUser) {
        this.targetUserList.remove(targetUser);
        this.view.onTargetUserRemoved(targetUser);
    }

    public void sendMessage(final MessageData messageData) {
        SendMessageTask sendMessageTask = new SendMessageTask(this.lineApiClient, new ArrayList<MessageData>() {
            /* class com.linecorp.linesdk.dialog.internal.SendMessagePresenter.C46692 */

            {
                add(messageData);
            }
        }, this.apiStatusListener);
        this.asyncTaskList.add(sendMessageTask);
        sendMessageTask.execute(this.targetUserList);
    }
}
