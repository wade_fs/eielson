package com.linecorp.linesdk.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.Jsonable;
import com.linecorp.linesdk.message.Stringable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class JSONUtils {
    public static <T> void put(@NonNull JSONObject jSONObject, @NonNull String str, @Nullable T t) {
        if (t != null) {
            if (t instanceof Jsonable) {
                jSONObject.put(str, ((Jsonable) t).toJsonObject());
            } else if (t instanceof Stringable) {
                jSONObject.put(str, ((Stringable) t).name().toLowerCase());
            } else {
                jSONObject.put(str, t);
            }
        }
    }

    public static <T> void putArray(@NonNull JSONObject jSONObject, @NonNull String str, @Nullable List<T> list) {
        if (list != null) {
            JSONArray jSONArray = new JSONArray();
            for (T t : list) {
                if (t instanceof Jsonable) {
                    jSONArray.put(((Jsonable) t).toJsonObject());
                } else {
                    jSONArray.put(t);
                }
            }
            jSONObject.put(str, jSONArray);
        }
    }

    public static List<String> toStringList(@NonNull JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }
}
