package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexSpacerComponent extends FlexMessageComponent {
    @Nullable
    private FlexMessageComponent.Size size;

    public FlexSpacerComponent() {
        super(FlexMessageComponent.Type.SPACER);
    }

    public void setSize(@Nullable FlexMessageComponent.Size size2) {
        this.size = size2;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, "size", this.size);
        return jsonObject;
    }
}
