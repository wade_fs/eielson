package com.linecorp.linesdk.internal;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.LoginDelegate;

public class LoginDelegateImpl implements LoginDelegate {
    @Nullable
    private LoginHandler loginHandler;

    public boolean onActivityResult(int i, int i2, Intent intent) {
        LoginHandler loginHandler2 = this.loginHandler;
        return loginHandler2 != null && loginHandler2.onActivityResult(i, i2, intent);
    }

    public void setLoginHandler(@NonNull LoginHandler loginHandler2) {
        this.loginHandler = loginHandler2;
    }
}
