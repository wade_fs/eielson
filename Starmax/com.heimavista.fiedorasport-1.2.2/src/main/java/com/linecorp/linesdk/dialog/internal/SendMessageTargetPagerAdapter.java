package com.linecorp.linesdk.dialog.internal;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.dialog.internal.TargetListAdapter;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import java.util.HashMap;

public class SendMessageTargetPagerAdapter extends PagerAdapter {
    private Context context;
    private TargetListAdapter.OnSelectedChangeListener listener;
    private SendMessagePresenter presenter;
    private HashMap<TargetUser.Type, TargetListWithSearchView> viewHashMap = new HashMap<>();

    /* renamed from: com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter$1 */
    static /* synthetic */ class C46701 {
        static final /* synthetic */ int[] $SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type = new int[TargetUser.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.linecorp.linesdk.dialog.internal.TargetUser$Type[] r0 = com.linecorp.linesdk.dialog.internal.TargetUser.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter.C46701.$SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type = r0
                int[] r0 = com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter.C46701.$SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.linecorp.linesdk.dialog.internal.TargetUser$Type r1 = com.linecorp.linesdk.dialog.internal.TargetUser.Type.FRIEND     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter.C46701.$SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type     // Catch:{ NoSuchFieldError -> 0x001f }
                com.linecorp.linesdk.dialog.internal.TargetUser$Type r1 = com.linecorp.linesdk.dialog.internal.TargetUser.Type.GROUP     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter.C46701.<clinit>():void");
        }
    }

    public SendMessageTargetPagerAdapter(Context context2, SendMessagePresenter sendMessagePresenter, TargetListAdapter.OnSelectedChangeListener onSelectedChangeListener) {
        this.context = context2;
        this.presenter = sendMessagePresenter;
        this.listener = onSelectedChangeListener;
    }

    public int getCount() {
        return TargetUser.getTargetTypeCount();
    }

    @Nullable
    public CharSequence getPageTitle(int i) {
        int i2 = C46701.$SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type[TargetUser.Type.values()[i].ordinal()];
        if (i2 == 1) {
            return this.context.getString(C4642R.string.select_tab_friends);
        }
        if (i2 != 2) {
            return "";
        }
        return this.context.getString(C4642R.string.select_tab_groups);
    }

    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }

    public void unSelect(TargetUser targetUser) {
        this.viewHashMap.get(targetUser.getType()).unSelect(targetUser);
    }

    @NonNull
    public View instantiateItem(@NonNull ViewGroup viewGroup, int i) {
        TargetListWithSearchView targetListWithSearchView;
        TargetUser.Type type = TargetUser.Type.values()[i];
        int i2 = C46701.$SwitchMap$com$linecorp$linesdk$dialog$internal$TargetUser$Type[type.ordinal()];
        if (i2 == 1) {
            targetListWithSearchView = new TargetListWithSearchView(this.context, C4642R.string.search_no_fiend, this.listener);
            Class<TargetListWithSearchView> cls = TargetListWithSearchView.class;
            this.presenter.getFriends(new C4674a(targetListWithSearchView));
        } else if (i2 != 2) {
            return null;
        } else {
            targetListWithSearchView = new TargetListWithSearchView(this.context, C4642R.string.search_no_group, this.listener);
            Class<TargetListWithSearchView> cls2 = TargetListWithSearchView.class;
            this.presenter.getGroups(new C4674a(targetListWithSearchView));
        }
        this.viewHashMap.put(type, targetListWithSearchView);
        viewGroup.addView(targetListWithSearchView);
        return targetListWithSearchView;
    }
}
