package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import org.json.JSONObject;

public class TextMessage extends MessageData {
    @Nullable
    private final MessageSender sendBy;
    @NonNull
    private final String text;

    public TextMessage(@NonNull String str) {
        this.text = str;
        this.sendBy = null;
    }

    @NonNull
    public Type getType() {
        return Type.TEXT;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("text", this.text);
        MessageSender messageSender = this.sendBy;
        if (messageSender != null) {
            jsonObject.put("sentBy", messageSender.toJsonObject());
        }
        return jsonObject;
    }

    public TextMessage(@NonNull String str, @Nullable MessageSender messageSender) {
        this.text = str;
        this.sendBy = messageSender;
    }
}
