package com.linecorp.linesdk;

public class LineFriendshipStatus {
    private boolean friendFlag;

    public LineFriendshipStatus(boolean z) {
        this.friendFlag = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && LineFriendshipStatus.class == obj.getClass() && this.friendFlag == ((LineFriendshipStatus) obj).friendFlag) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.friendFlag ? 1 : 0;
    }

    public boolean isFriend() {
        return this.friendFlag;
    }

    public String toString() {
        return "LineFriendshipStatus{friendFlag=" + this.friendFlag + '}';
    }
}
