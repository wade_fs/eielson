package com.linecorp.linesdk.dialog.internal;

import android.os.AsyncTask;
import androidx.annotation.NonNull;
import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.GetFriendsResponse;
import com.linecorp.linesdk.GetGroupsResponse;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineFriendProfile;
import com.linecorp.linesdk.LineGroup;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetTargetUserTask extends AsyncTask<Void, List<TargetUser>, Void> {
    private LineApiClient lineApiClient;
    private NextAction nextAction;
    private TargetUser.Type type;

    @FunctionalInterface
    public interface NextAction {
        void run(List<TargetUser> list);
    }

    public GetTargetUserTask(TargetUser.Type type2, LineApiClient lineApiClient2, NextAction nextAction2) {
        this.type = type2;
        this.lineApiClient = lineApiClient2;
        this.nextAction = nextAction2;
    }

    @NonNull
    private List<TargetUser> convertFriendsToTargetUsers(List<LineFriendProfile> list) {
        ArrayList arrayList = new ArrayList();
        for (LineFriendProfile lineFriendProfile : list) {
            arrayList.add(TargetUser.createInstance(lineFriendProfile));
        }
        return arrayList;
    }

    @NonNull
    private List<TargetUser> convertGroupsToTargetUsers(List<LineGroup> list) {
        ArrayList arrayList = new ArrayList();
        for (LineGroup lineGroup : list) {
            arrayList.add(TargetUser.createInstance(lineGroup));
        }
        return arrayList;
    }

    private void getAllFriends() {
        String str = "";
        while (str != null) {
            LineApiResponse<GetFriendsResponse> friends = this.lineApiClient.getFriends(FriendSortField.RELATION, str, true);
            if (!friends.isSuccess()) {
                publishProgress(Collections.emptyList());
                return;
            }
            GetFriendsResponse responseData = friends.getResponseData();
            publishProgress(convertFriendsToTargetUsers(responseData.getFriends()));
            str = responseData.getNextPageRequestToken();
        }
    }

    private void getAllGroups() {
        String str = "";
        while (str != null) {
            LineApiResponse<GetGroupsResponse> groups = this.lineApiClient.getGroups(str, true);
            if (!groups.isSuccess()) {
                publishProgress(Collections.emptyList());
                return;
            }
            GetGroupsResponse responseData = groups.getResponseData();
            publishProgress(convertGroupsToTargetUsers(responseData.getGroups()));
            str = responseData.getNextPageRequestToken();
        }
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        onProgressUpdate((List<TargetUser>[]) ((List[]) objArr));
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... voidArr) {
        TargetUser.Type type2 = this.type;
        if (type2 == TargetUser.Type.FRIEND) {
            getAllFriends();
            return null;
        } else if (type2 != TargetUser.Type.GROUP) {
            return null;
        } else {
            getAllGroups();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(List<TargetUser>... listArr) {
        this.nextAction.run(listArr[0]);
    }
}
