package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import org.json.JSONObject;

public class LocationMessage extends MessageData {
    @NonNull
    private final String address;
    @NonNull
    private final Double latitude;
    @NonNull
    private final Double longitude;
    @NonNull
    private final String title;

    public LocationMessage(@NonNull String str, @NonNull String str2, @NonNull Double d, @NonNull Double d2) {
        this.title = str;
        this.address = str2;
        this.latitude = d;
        this.longitude = d2;
    }

    @NonNull
    public Type getType() {
        return Type.LOCATION;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("title", this.title);
        jsonObject.put("address", this.address);
        jsonObject.put("latitude", this.latitude);
        jsonObject.put("longitude", this.longitude);
        return jsonObject;
    }
}
