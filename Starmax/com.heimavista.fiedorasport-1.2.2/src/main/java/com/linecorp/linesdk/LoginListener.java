package com.linecorp.linesdk;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.auth.LineLoginResult;

public interface LoginListener {
    void onLoginFailure(@Nullable LineLoginResult lineLoginResult);

    void onLoginSuccess(@NonNull LineLoginResult lineLoginResult);
}
