package com.linecorp.linesdk.dialog.internal;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class UserThumbnailView extends ConstraintLayout {
    private ImageView imageView;
    private TextView targetUserName;

    public UserThumbnailView(Context context) {
        super(context);
        init();
    }

    private void init() {
        ViewGroup.inflate(getContext(), C4642R.C4646layout.target_user_thumbnail, this);
        this.targetUserName = (TextView) findViewById(C4642R.C4645id.textViewDisplayName);
        this.imageView = (ImageView) findViewById(C4642R.C4645id.imageViewTargetUser);
    }

    public void setTargetUser(TargetUser targetUser) {
        this.targetUserName.setText(targetUser.getDisplayName());
        int i = targetUser.getType() == TargetUser.Type.FRIEND ? C4642R.C4644drawable.friend_thumbnail : C4642R.C4644drawable.group_thumbnail;
        RequestCreator a = Picasso.m15799b().mo26309a(targetUser.getPictureUri());
        a.mo26345a(i);
        a.mo26347a(this.imageView);
    }
}
