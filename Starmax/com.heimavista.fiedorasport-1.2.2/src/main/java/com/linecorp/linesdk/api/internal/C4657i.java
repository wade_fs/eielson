package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.i */
/* compiled from: lambda */
public final /* synthetic */ class C4657i implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9530a;

    /* renamed from: b */
    private final /* synthetic */ String f9531b;

    /* renamed from: c */
    private final /* synthetic */ boolean f9532c;

    public /* synthetic */ C4657i(LineApiClientImpl lineApiClientImpl, String str, boolean z) {
        this.f9530a = lineApiClientImpl;
        this.f9531b = str;
        this.f9532c = z;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9530a.mo25661a(this.f9531b, this.f9532c, internalAccessToken);
    }
}
