package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import org.json.JSONObject;

public class AudioMessage extends MessageData {
    @NonNull
    private final Long durationMillis;
    @NonNull
    private final String originalContentUrl;

    public AudioMessage(@NonNull String str, @NonNull Long l) {
        this.originalContentUrl = str;
        this.durationMillis = l;
    }

    @NonNull
    public Type getType() {
        return Type.AUDIO;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("originalContentUrl", this.originalContentUrl);
        jsonObject.put("duration", this.durationMillis);
        return jsonObject;
    }
}
