package com.linecorp.linesdk.message.flex.style;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.Jsonable;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexBlockStyle implements Jsonable {
    @Nullable
    private String backgroundColor;
    @Nullable
    private boolean separator;
    @Nullable
    private String separatorColor;

    public FlexBlockStyle(@Nullable String str, @Nullable boolean z, @Nullable String str2) {
        this.backgroundColor = str;
        this.separator = z;
        this.separatorColor = str2;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        JSONUtils.put(jSONObject, TtmlNode.ATTR_TTS_BACKGROUND_COLOR, this.backgroundColor);
        JSONUtils.put(jSONObject, "separator", Boolean.valueOf(this.separator));
        JSONUtils.put(jSONObject, "separatorColor", this.separatorColor);
        return jSONObject;
    }
}
