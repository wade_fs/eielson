package com.linecorp.linesdk.auth.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineIdToken;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.auth.LineAuthenticationConfig;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginResult;
import com.linecorp.linesdk.auth.internal.BrowserAuthenticationApi;
import com.linecorp.linesdk.auth.internal.LineAuthenticationStatus;
import com.linecorp.linesdk.internal.AccessTokenCache;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.IssueAccessTokenResult;
import com.linecorp.linesdk.internal.OneTimePassword;
import com.linecorp.linesdk.internal.OpenIdDiscoveryDocument;
import com.linecorp.linesdk.internal.nwclient.IdTokenValidator;
import com.linecorp.linesdk.internal.nwclient.LineAuthenticationApiClient;
import com.linecorp.linesdk.internal.nwclient.TalkApiClient;
import java.util.List;

class LineAuthenticationController {
    private static final long CANCEL_DELAY_MILLIS = 1000;
    private static final int REQUEST_CODE = 3;
    /* access modifiers changed from: private */
    @Nullable
    public static Intent intentResultFromLineAPP;
    /* access modifiers changed from: private */
    @NonNull
    public final AccessTokenCache accessTokenCache;
    /* access modifiers changed from: private */
    @NonNull
    public final LineAuthenticationActivity activity;
    /* access modifiers changed from: private */
    @NonNull
    public final LineAuthenticationApiClient authApiClient;
    /* access modifiers changed from: private */
    @NonNull
    public final LineAuthenticationStatus authenticationStatus;
    /* access modifiers changed from: private */
    @NonNull
    public final BrowserAuthenticationApi browserAuthenticationApi;
    /* access modifiers changed from: private */
    @NonNull
    public final LineAuthenticationConfig config;
    /* access modifiers changed from: private */
    @NonNull
    public final LineAuthenticationParams params;
    /* access modifiers changed from: private */
    @NonNull
    public final TalkApiClient talkApiClient;

    private class AccessTokenRequestTask extends AsyncTask<BrowserAuthenticationApi.Result, Void, LineLoginResult> {
        private AccessTokenRequestTask() {
        }

        private void validateIdToken(LineIdToken lineIdToken, String str) {
            LineApiResponse<OpenIdDiscoveryDocument> openIdDiscoveryDocument = LineAuthenticationController.this.authApiClient.getOpenIdDiscoveryDocument();
            if (openIdDiscoveryDocument.isSuccess()) {
                new IdTokenValidator.Builder().idToken(lineIdToken).expectedIssuer(openIdDiscoveryDocument.getResponseData().getIssuer()).expectedUserId(str).expectedChannelId(LineAuthenticationController.this.config.getChannelId()).expectedNonce(LineAuthenticationController.this.authenticationStatus.getOpenIdNonce()).build().validate();
                return;
            }
            throw new RuntimeException("Failed to get OpenId Discovery Document.  Response Code: " + openIdDiscoveryDocument.getResponseCode() + " Error Data: " + openIdDiscoveryDocument.getErrorData());
        }

        /* access modifiers changed from: protected */
        public LineLoginResult doInBackground(@Nullable BrowserAuthenticationApi.Result... resultArr) {
            String str;
            BrowserAuthenticationApi.Result result = resultArr[0];
            String requestToken = result.getRequestToken();
            OneTimePassword oneTimePassword = LineAuthenticationController.this.authenticationStatus.getOneTimePassword();
            String sentRedirectUri = LineAuthenticationController.this.authenticationStatus.getSentRedirectUri();
            if (TextUtils.isEmpty(requestToken) || oneTimePassword == null || TextUtils.isEmpty(sentRedirectUri)) {
                return LineLoginResult.internalError("Requested data is missing.");
            }
            LineApiResponse<IssueAccessTokenResult> issueAccessToken = LineAuthenticationController.this.authApiClient.issueAccessToken(LineAuthenticationController.this.config.getChannelId(), requestToken, oneTimePassword, sentRedirectUri);
            if (!issueAccessToken.isSuccess()) {
                return LineLoginResult.error(issueAccessToken);
            }
            IssueAccessTokenResult responseData = issueAccessToken.getResponseData();
            InternalAccessToken accessToken = responseData.getAccessToken();
            List<Scope> scopes = responseData.getScopes();
            LineProfile lineProfile = null;
            if (scopes.contains(Scope.PROFILE)) {
                LineApiResponse<LineProfile> profile = LineAuthenticationController.this.talkApiClient.getProfile(accessToken);
                if (!profile.isSuccess()) {
                    return LineLoginResult.error(profile);
                }
                lineProfile = profile.getResponseData();
                str = lineProfile.getUserId();
            } else {
                str = null;
            }
            LineAuthenticationController.this.accessTokenCache.saveAccessToken(accessToken);
            LineIdToken idToken = responseData.getIdToken();
            if (idToken != null) {
                try {
                    validateIdToken(idToken, str);
                } catch (Exception e) {
                    return LineLoginResult.internalError(e.getMessage());
                }
            }
            return new LineLoginResult.Builder().nonce(LineAuthenticationController.this.authenticationStatus.getOpenIdNonce()).lineProfile(lineProfile).lineIdToken(idToken).friendshipStatusChanged(result.getFriendshipStatusChanged()).lineCredential(new LineCredential(new LineAccessToken(accessToken.getAccessToken(), accessToken.getExpiresInMillis(), accessToken.getIssuedClientTimeMillis()), scopes)).build();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(@NonNull LineLoginResult lineLoginResult) {
            LineAuthenticationController.this.authenticationStatus.authenticationIntentHandled();
            LineAuthenticationController.this.activity.onAuthenticationFinished(lineLoginResult);
        }
    }

    private class CancelAuthenticationTask implements Runnable {
        private CancelAuthenticationTask() {
        }

        @MainThread
        public void run() {
            if (LineAuthenticationController.this.authenticationStatus.getStatus() != LineAuthenticationStatus.Status.INTENT_RECEIVED && !LineAuthenticationController.this.activity.isFinishing()) {
                if (LineAuthenticationController.intentResultFromLineAPP != null) {
                    LineAuthenticationController.this.handleIntentFromLineApp(LineAuthenticationController.intentResultFromLineAPP);
                    Intent unused = LineAuthenticationController.intentResultFromLineAPP = null;
                    return;
                }
                LineAuthenticationController.this.activity.onAuthenticationFinished(LineLoginResult.canceledError());
            }
        }
    }

    private class RequestTokenRequestTask extends AsyncTask<Void, Void, LineApiResponse<OneTimePassword>> {
        private RequestTokenRequestTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(@NonNull Object obj) {
            onPostExecute((LineApiResponse<OneTimePassword>) ((LineApiResponse) obj));
        }

        /* access modifiers changed from: protected */
        public LineApiResponse<OneTimePassword> doInBackground(@Nullable Void... voidArr) {
            return LineAuthenticationController.this.authApiClient.getOneTimeIdAndPassword(LineAuthenticationController.this.config.getChannelId());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(@NonNull LineApiResponse<OneTimePassword> lineApiResponse) {
            if (!lineApiResponse.isSuccess()) {
                LineAuthenticationController.this.authenticationStatus.authenticationIntentHandled();
                LineAuthenticationController.this.activity.onAuthenticationFinished(LineLoginResult.error(lineApiResponse));
                return;
            }
            OneTimePassword responseData = lineApiResponse.getResponseData();
            LineAuthenticationController.this.authenticationStatus.setOneTimePassword(responseData);
            try {
                BrowserAuthenticationApi.Request request = LineAuthenticationController.this.browserAuthenticationApi.getRequest(LineAuthenticationController.this.activity, LineAuthenticationController.this.config, responseData, LineAuthenticationController.this.params);
                if (request.isLineAppAuthentication()) {
                    if (Build.VERSION.SDK_INT >= 16) {
                        LineAuthenticationController.this.activity.startActivity(request.getIntent(), request.getStartActivityOptions());
                    } else {
                        LineAuthenticationController.this.activity.startActivity(request.getIntent());
                    }
                } else if (Build.VERSION.SDK_INT >= 16) {
                    LineAuthenticationController.this.activity.startActivityForResult(request.getIntent(), 3, request.getStartActivityOptions());
                } else {
                    LineAuthenticationController.this.activity.startActivityForResult(request.getIntent(), 3);
                }
                LineAuthenticationController.this.authenticationStatus.setSentRedirectUri(request.getRedirectUri());
            } catch (ActivityNotFoundException e) {
                LineAuthenticationController.this.authenticationStatus.authenticationIntentHandled();
                LineAuthenticationController.this.activity.onAuthenticationFinished(LineLoginResult.internalError(e));
            }
        }
    }

    LineAuthenticationController(@NonNull LineAuthenticationActivity lineAuthenticationActivity, @NonNull LineAuthenticationConfig lineAuthenticationConfig, @NonNull LineAuthenticationStatus lineAuthenticationStatus, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        this(lineAuthenticationActivity, lineAuthenticationConfig, new LineAuthenticationApiClient(lineAuthenticationActivity.getApplicationContext(), lineAuthenticationConfig.getOpenidDiscoveryDocumentUrl(), lineAuthenticationConfig.getApiBaseUrl()), new TalkApiClient(lineAuthenticationActivity.getApplicationContext(), lineAuthenticationConfig.getApiBaseUrl()), new BrowserAuthenticationApi(lineAuthenticationStatus), new AccessTokenCache(lineAuthenticationActivity.getApplicationContext(), lineAuthenticationConfig.getChannelId()), lineAuthenticationStatus, lineAuthenticationParams);
    }

    @MainThread
    public static void setIntent(Intent intent) {
        intentResultFromLineAPP = intent;
    }

    /* access modifiers changed from: package-private */
    @MainThread
    public void handleCancel() {
        new Handler(Looper.getMainLooper()).postDelayed(new CancelAuthenticationTask(), 1000);
    }

    /* access modifiers changed from: package-private */
    @MainThread
    public void handleIntentFromLineApp(@NonNull Intent intent) {
        LineLoginResult lineLoginResult;
        this.authenticationStatus.authenticationIntentReceived();
        BrowserAuthenticationApi.Result authenticationResultFrom = this.browserAuthenticationApi.getAuthenticationResultFrom(intent);
        if (!authenticationResultFrom.isSuccess()) {
            this.authenticationStatus.authenticationIntentHandled();
            if (authenticationResultFrom.isAuthenticationAgentError()) {
                lineLoginResult = LineLoginResult.authenticationAgentError(authenticationResultFrom.getLineApiError());
            } else {
                lineLoginResult = LineLoginResult.internalError(authenticationResultFrom.getLineApiError());
            }
            this.activity.onAuthenticationFinished(lineLoginResult);
            return;
        }
        new AccessTokenRequestTask().execute(authenticationResultFrom);
    }

    /* access modifiers changed from: package-private */
    @MainThread
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        if (i == 3 && this.authenticationStatus.getStatus() != LineAuthenticationStatus.Status.INTENT_RECEIVED) {
            new Handler(Looper.getMainLooper()).postDelayed(new CancelAuthenticationTask(), 1000);
        }
    }

    /* access modifiers changed from: package-private */
    @MainThread
    public void startLineAuthentication() {
        this.authenticationStatus.authenticationStarted();
        new RequestTokenRequestTask().execute(new Void[0]);
    }

    @VisibleForTesting
    LineAuthenticationController(@NonNull LineAuthenticationActivity lineAuthenticationActivity, @NonNull LineAuthenticationConfig lineAuthenticationConfig, @NonNull LineAuthenticationApiClient lineAuthenticationApiClient, @NonNull TalkApiClient talkApiClient2, @NonNull BrowserAuthenticationApi browserAuthenticationApi2, @NonNull AccessTokenCache accessTokenCache2, @NonNull LineAuthenticationStatus lineAuthenticationStatus, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        this.activity = lineAuthenticationActivity;
        this.config = lineAuthenticationConfig;
        this.authApiClient = lineAuthenticationApiClient;
        this.talkApiClient = talkApiClient2;
        this.browserAuthenticationApi = browserAuthenticationApi2;
        this.accessTokenCache = accessTokenCache2;
        this.authenticationStatus = lineAuthenticationStatus;
        this.params = lineAuthenticationParams;
    }
}
