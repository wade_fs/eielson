package com.linecorp.linesdk.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.GetFriendsResponse;
import com.linecorp.linesdk.GetGroupsResponse;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineFriendshipStatus;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.SendMessageResponse;
import com.linecorp.linesdk.message.MessageData;
import java.util.List;

public interface LineApiClient {
    @NonNull
    LineApiResponse<LineAccessToken> getCurrentAccessToken();

    @NonNull
    LineApiResponse<GetFriendsResponse> getFriends(@NonNull FriendSortField friendSortField, @Nullable String str);

    @NonNull
    LineApiResponse<GetFriendsResponse> getFriends(@NonNull FriendSortField friendSortField, @Nullable String str, boolean z);

    @NonNull
    LineApiResponse<GetFriendsResponse> getFriendsApprovers(@NonNull FriendSortField friendSortField, @Nullable String str);

    @NonNull
    LineApiResponse<LineFriendshipStatus> getFriendshipStatus();

    @NonNull
    LineApiResponse<GetFriendsResponse> getGroupApprovers(@NonNull String str, @Nullable String str2);

    @NonNull
    LineApiResponse<GetGroupsResponse> getGroups(@Nullable String str);

    @NonNull
    LineApiResponse<GetGroupsResponse> getGroups(@Nullable String str, boolean z);

    @NonNull
    LineApiResponse<LineProfile> getProfile();

    @NonNull
    LineApiResponse<?> logout();

    @NonNull
    LineApiResponse<LineAccessToken> refreshAccessToken();

    @NonNull
    LineApiResponse<String> sendMessage(@NonNull String str, @NonNull List<MessageData> list);

    @NonNull
    LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull List<String> list, @NonNull List<MessageData> list2);

    @NonNull
    LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull List<String> list, @NonNull List<MessageData> list2, boolean z);

    @NonNull
    LineApiResponse<LineCredential> verifyToken();
}
