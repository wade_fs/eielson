package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import com.linecorp.linesdk.message.flex.action.Action;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexTextComponent extends FlexMessageComponent {
    protected static final int MAXLINES_VALUE_NONE = -1;
    @Nullable
    private Action action;
    @Nullable
    private FlexMessageComponent.Alignment align;
    @Nullable
    private String color;
    @Nullable
    private int flex;
    @Nullable
    private FlexMessageComponent.Gravity gravity;
    @Nullable
    private FlexMessageComponent.Margin margin;
    private int maxLines;
    @Nullable
    private FlexMessageComponent.Size size;
    @NonNull
    private String text;
    @Nullable
    private FlexMessageComponent.Weight weight;
    private Boolean wrap;

    public static Builder newBuilder(@NonNull String str) {
        return new Builder(str);
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("text", this.text);
        JSONUtils.put(jsonObject, "margin", this.margin);
        FlexMessageComponent.Size size2 = this.size;
        JSONUtils.put(jsonObject, "size", size2 != null ? size2.getValue() : null);
        JSONUtils.put(jsonObject, "align", this.align);
        JSONUtils.put(jsonObject, "gravity", this.gravity);
        JSONUtils.put(jsonObject, "wrap", this.wrap);
        JSONUtils.put(jsonObject, "weight", this.weight);
        JSONUtils.put(jsonObject, TtmlNode.ATTR_TTS_COLOR, this.color);
        JSONUtils.put(jsonObject, NativeProtocol.WEB_DIALOG_ACTION, this.action);
        int i = this.flex;
        if (i != -1) {
            jsonObject.put("flex", i);
        }
        int i2 = this.maxLines;
        if (i2 != -1) {
            jsonObject.put("maxLines", i2);
        }
        return jsonObject;
    }

    private FlexTextComponent() {
        super(FlexMessageComponent.Type.TEXT);
    }

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public Action action;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Alignment align;
        /* access modifiers changed from: private */
        @Nullable
        public String color;
        /* access modifiers changed from: private */
        public int flex;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Gravity gravity;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;
        /* access modifiers changed from: private */
        public int maxLines;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Size size;
        /* access modifiers changed from: private */
        @NonNull
        public String text;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Weight weight;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean wrap;

        private Builder() {
            this.flex = -1;
            this.maxLines = -1;
        }

        public FlexTextComponent build() {
            return new FlexTextComponent(this);
        }

        public Builder setAction(@Nullable Action action2) {
            this.action = action2;
            return this;
        }

        public Builder setAlign(@Nullable FlexMessageComponent.Alignment alignment) {
            this.align = alignment;
            return this;
        }

        public Builder setColor(@Nullable String str) {
            this.color = str;
            return this;
        }

        public Builder setFlex(int i) {
            this.flex = i;
            return this;
        }

        public Builder setGravity(@Nullable FlexMessageComponent.Gravity gravity2) {
            this.gravity = gravity2;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        public Builder setMaxLines(int i) {
            this.maxLines = i;
            return this;
        }

        public Builder setSize(@Nullable FlexMessageComponent.Size size2) {
            this.size = size2;
            return this;
        }

        public Builder setWeight(@Nullable FlexMessageComponent.Weight weight2) {
            this.weight = weight2;
            return this;
        }

        public Builder setWrap(@Nullable Boolean bool) {
            this.wrap = bool;
            return this;
        }

        public Builder(@NonNull String str) {
            this();
            this.text = str;
        }
    }

    private FlexTextComponent(@NonNull Builder builder) {
        this();
        this.text = builder.text;
        this.flex = builder.flex;
        this.margin = builder.margin;
        this.size = builder.size;
        this.align = builder.align;
        this.gravity = builder.gravity;
        this.wrap = builder.wrap;
        this.maxLines = builder.maxLines;
        this.weight = builder.weight;
        this.color = builder.color;
        this.action = builder.action;
    }
}
