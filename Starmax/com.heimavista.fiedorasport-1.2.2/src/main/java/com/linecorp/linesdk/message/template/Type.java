package com.linecorp.linesdk.message.template;

import com.facebook.share.internal.MessengerShareContentUtility;

public enum Type {
    BUTTONS(MessengerShareContentUtility.BUTTONS),
    CONFIRM("confirm"),
    CAROUSEL("carousel"),
    IMAGE_CAROUSEL("image_carousel");
    
    private final String serverKey;

    private Type(String str) {
        this.serverKey = str;
    }

    public String getServerKey() {
        return this.serverKey;
    }
}
