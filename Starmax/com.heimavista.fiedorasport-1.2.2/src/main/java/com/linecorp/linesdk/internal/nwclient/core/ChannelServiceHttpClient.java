package com.linecorp.linesdk.internal.nwclient.core;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.utils.UriUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;
import p119e.p220f.p221a.p222a.TLSSocketFactory;

public class ChannelServiceHttpClient {
    private static final int DEFAULT_CONNECT_TIMEOUT_MILLIS = 90000;
    private static final int DEFAULT_READ_TIMEOUT_MILLIS = 90000;
    private static final byte[] EMPTY_DATA = new byte[0];
    private static final String SERVER_SIDE_CHARSET = "UTF-8";
    private static final String TAG = "ChannelHttpClient";
    private int connectTimeoutMillis;
    @NonNull
    private final StringResponseParser errorResponseParser;
    private int readTimeoutMillis;
    @NonNull
    private final UserAgentGenerator userAgentGenerator;

    public ChannelServiceHttpClient(@NonNull Context context, @NonNull String str) {
        this(new UserAgentGenerator(context, str));
    }

    @NonNull
    private static byte[] convertPostDataToBytes(@NonNull Map<String, String> map) {
        if (map.isEmpty()) {
            return EMPTY_DATA;
        }
        try {
            return UriUtils.appendQueryParams("", map).getEncodedQuery().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    private static <T> LineApiResponse<T> getChannelServiceResponse(@NonNull HttpURLConnection httpURLConnection, @NonNull ResponseDataParser<T> responseDataParser, @NonNull ResponseDataParser<String> responseDataParser2) {
        InputStream inputStreamFrom = getInputStreamFrom(httpURLConnection);
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            return LineApiResponse.createAsSuccess(responseDataParser.getResponseData(inputStreamFrom));
        }
        try {
            return LineApiResponse.createAsError(LineApiResponseCode.SERVER_ERROR, new LineApiError(responseCode, responseDataParser2.getResponseData(inputStreamFrom)));
        } catch (IOException e) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError(responseCode, e));
        }
    }

    @NonNull
    private static InputStream getInputStreamFrom(@NonNull HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        if (httpURLConnection.getResponseCode() < 400) {
            inputStream = httpURLConnection.getInputStream();
        } else {
            inputStream = httpURLConnection.getErrorStream();
        }
        return isGzipUsed(httpURLConnection) ? new GZIPInputStream(inputStream) : inputStream;
    }

    private static boolean isGzipUsed(@NonNull HttpURLConnection httpURLConnection) {
        List list = httpURLConnection.getHeaderFields().get("Content-Encoding");
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (((String) list.get(i)).equalsIgnoreCase("gzip")) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void logExceptionForDebug(@NonNull LineApiResponse<?> lineApiResponse, @NonNull Exception exc) {
    }

    private static void logRequestForDebug(@NonNull HttpURLConnection httpURLConnection, @Nullable byte[] bArr) {
        Log.d(TAG, httpURLConnection.getRequestMethod() + " : " + httpURLConnection.getURL());
        for (Map.Entry entry : httpURLConnection.getRequestProperties().entrySet()) {
            Log.d(TAG, "    " + ((String) entry.getKey()) + " : " + Arrays.toString(((List) entry.getValue()).toArray()));
        }
        if (bArr != null) {
            try {
                Log.d(TAG, "== Request body ==");
                Log.d(TAG, new String(bArr, "utf-8"));
            } catch (UnsupportedEncodingException unused) {
            }
        }
    }

    @NonNull
    private static InputStream logResponseBodyForDebug(@NonNull InputStream inputStream) {
        byte[] byteArray = toByteArray(inputStream);
        Log.d(TAG, "== response body ==");
        Log.d(TAG, new StringResponseParser().getResponseData((InputStream) new ByteArrayInputStream(byteArray)));
        return new ByteArrayInputStream(byteArray);
    }

    private static void logResponseHeadersForDebug(@NonNull HttpURLConnection httpURLConnection) {
        Log.d(TAG, httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage());
        for (Map.Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
            Log.d(TAG, "    " + ((String) entry.getKey()) + " : " + Arrays.toString(((List) entry.getValue()).toArray()));
        }
    }

    @NonNull
    private HttpURLConnection openDeleteConnection(@NonNull Uri uri) {
        HttpURLConnection openHttpConnection = openHttpConnection(uri);
        openHttpConnection.setInstanceFollowRedirects(true);
        openHttpConnection.setRequestProperty("User-Agent", this.userAgentGenerator.getUserAgent());
        openHttpConnection.setRequestProperty("Accept-Encoding", "gzip");
        openHttpConnection.setConnectTimeout(this.connectTimeoutMillis);
        openHttpConnection.setReadTimeout(this.readTimeoutMillis);
        openHttpConnection.setRequestMethod("DELETE");
        return openHttpConnection;
    }

    @NonNull
    private HttpURLConnection openGetConnection(@NonNull Uri uri) {
        HttpURLConnection openHttpConnection = openHttpConnection(uri);
        openHttpConnection.setInstanceFollowRedirects(true);
        openHttpConnection.setRequestProperty("User-Agent", this.userAgentGenerator.getUserAgent());
        openHttpConnection.setRequestProperty("Accept-Encoding", "gzip");
        openHttpConnection.setConnectTimeout(this.connectTimeoutMillis);
        openHttpConnection.setReadTimeout(this.readTimeoutMillis);
        openHttpConnection.setRequestMethod("GET");
        return openHttpConnection;
    }

    @NonNull
    private HttpURLConnection openPostConnection(@NonNull Uri uri, int i) {
        HttpURLConnection openHttpConnection = openHttpConnection(uri);
        openHttpConnection.setInstanceFollowRedirects(true);
        openHttpConnection.setRequestProperty("User-Agent", this.userAgentGenerator.getUserAgent());
        openHttpConnection.setRequestProperty("Accept-Encoding", "gzip");
        openHttpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        openHttpConnection.setRequestProperty("Content-Length", String.valueOf(i));
        openHttpConnection.setConnectTimeout(this.connectTimeoutMillis);
        openHttpConnection.setReadTimeout(this.readTimeoutMillis);
        openHttpConnection.setRequestMethod("POST");
        openHttpConnection.setDoOutput(true);
        return openHttpConnection;
    }

    @NonNull
    private HttpURLConnection openPostConnectionWithJson(@NonNull Uri uri, int i) {
        HttpURLConnection openHttpConnection = openHttpConnection(uri);
        openHttpConnection.setInstanceFollowRedirects(true);
        openHttpConnection.setRequestProperty("User-Agent", this.userAgentGenerator.getUserAgent());
        openHttpConnection.setRequestProperty("Accept-Encoding", "gzip");
        openHttpConnection.setRequestProperty("Content-Type", "application/json");
        openHttpConnection.setRequestProperty("Content-Length", String.valueOf(i));
        openHttpConnection.setConnectTimeout(this.connectTimeoutMillis);
        openHttpConnection.setReadTimeout(this.readTimeoutMillis);
        openHttpConnection.setRequestMethod("POST");
        openHttpConnection.setDoOutput(true);
        return openHttpConnection;
    }

    private static void setRequestHeaders(@NonNull HttpURLConnection httpURLConnection, @NonNull Map<String, String> map) {
        for (Map.Entry entry : map.entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
    }

    @NonNull
    private static byte[] toByteArray(@NonNull InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read < 0) {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    @WorkerThread
    @NonNull
    public <T> LineApiResponse<T> delete(@NonNull Uri uri, @NonNull Map<String, String> map, @NonNull ResponseDataParser<T> responseDataParser) {
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection openDeleteConnection = openDeleteConnection(uri);
            setRequestHeaders(openDeleteConnection, map);
            openDeleteConnection.connect();
            LineApiResponse<T> channelServiceResponse = getChannelServiceResponse(openDeleteConnection, responseDataParser, this.errorResponseParser);
            if (openDeleteConnection != null) {
                openDeleteConnection.disconnect();
            }
            return channelServiceResponse;
        } catch (IOException e) {
            LineApiResponse<T> createAsError = LineApiResponse.createAsError(LineApiResponseCode.NETWORK_ERROR, new LineApiError(e));
            logExceptionForDebug(createAsError, e);
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return createAsError;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    @WorkerThread
    @NonNull
    public <T> LineApiResponse<T> get(@NonNull Uri uri, @NonNull Map<String, String> map, @NonNull Map<String, String> map2, @NonNull ResponseDataParser<T> responseDataParser) {
        Uri appendQueryParams = UriUtils.appendQueryParams(uri, map2);
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection openGetConnection = openGetConnection(appendQueryParams);
            setRequestHeaders(openGetConnection, map);
            openGetConnection.connect();
            LineApiResponse<T> channelServiceResponse = getChannelServiceResponse(openGetConnection, responseDataParser, this.errorResponseParser);
            if (openGetConnection != null) {
                openGetConnection.disconnect();
            }
            return channelServiceResponse;
        } catch (IOException e) {
            LineApiResponse<T> createAsError = LineApiResponse.createAsError(LineApiResponseCode.NETWORK_ERROR, new LineApiError(e));
            logExceptionForDebug(createAsError, e);
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return createAsError;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    @NonNull
    public HttpURLConnection openHttpConnection(@NonNull Uri uri) {
        URLConnection openConnection = new URL(uri.toString()).openConnection();
        if (!(openConnection instanceof HttpsURLConnection)) {
            throw new IllegalArgumentException("The scheme of the server url must be https." + uri);
        } else if (Build.VERSION.SDK_INT >= 24) {
            return (HttpURLConnection) openConnection;
        } else {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) openConnection;
            httpsURLConnection.setSSLSocketFactory(new TLSSocketFactory(httpsURLConnection.getSSLSocketFactory()));
            return httpsURLConnection;
        }
    }

    @WorkerThread
    @NonNull
    public <T> LineApiResponse<T> post(@NonNull Uri uri, @NonNull Map<String, String> map, @NonNull Map<String, String> map2, @NonNull ResponseDataParser<T> responseDataParser) {
        byte[] convertPostDataToBytes = convertPostDataToBytes(map2);
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection openPostConnection = openPostConnection(uri, convertPostDataToBytes.length);
            setRequestHeaders(openPostConnection, map);
            openPostConnection.connect();
            OutputStream outputStream = openPostConnection.getOutputStream();
            outputStream.write(convertPostDataToBytes);
            outputStream.flush();
            LineApiResponse<T> channelServiceResponse = getChannelServiceResponse(openPostConnection, responseDataParser, this.errorResponseParser);
            if (openPostConnection != null) {
                openPostConnection.disconnect();
            }
            return channelServiceResponse;
        } catch (IOException e) {
            LineApiResponse<T> createAsError = LineApiResponse.createAsError(LineApiResponseCode.NETWORK_ERROR, new LineApiError(e));
            logExceptionForDebug(createAsError, e);
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return createAsError;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    @WorkerThread
    @NonNull
    public <T> LineApiResponse<T> postWithJson(@NonNull Uri uri, @NonNull Map<String, String> map, @NonNull String str, @NonNull ResponseDataParser<T> responseDataParser) {
        byte[] bytes = str.getBytes();
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection openPostConnectionWithJson = openPostConnectionWithJson(uri, bytes.length);
            setRequestHeaders(openPostConnectionWithJson, map);
            openPostConnectionWithJson.connect();
            OutputStream outputStream = openPostConnectionWithJson.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            LineApiResponse<T> channelServiceResponse = getChannelServiceResponse(openPostConnectionWithJson, responseDataParser, this.errorResponseParser);
            if (openPostConnectionWithJson != null) {
                openPostConnectionWithJson.disconnect();
            }
            return channelServiceResponse;
        } catch (IOException e) {
            LineApiResponse<T> createAsError = LineApiResponse.createAsError(LineApiResponseCode.NETWORK_ERROR, new LineApiError(e));
            logExceptionForDebug(createAsError, e);
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return createAsError;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    public void setConnectTimeoutMillis(int i) {
        this.connectTimeoutMillis = i;
    }

    public void setReadTimeoutMillis(int i) {
        this.readTimeoutMillis = i;
    }

    @VisibleForTesting
    protected ChannelServiceHttpClient(@NonNull UserAgentGenerator userAgentGenerator2) {
        this.userAgentGenerator = userAgentGenerator2;
        this.errorResponseParser = new StringResponseParser("UTF-8");
        this.connectTimeoutMillis = 90000;
        this.readTimeoutMillis = 90000;
    }
}
