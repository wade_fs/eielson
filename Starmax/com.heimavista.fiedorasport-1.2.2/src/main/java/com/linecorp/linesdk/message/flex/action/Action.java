package com.linecorp.linesdk.message.flex.action;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.Jsonable;
import com.linecorp.linesdk.message.Stringable;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public abstract class Action implements Jsonable {
    @Nullable
    protected String label;
    @NonNull
    protected final Type type;

    public enum Type implements Stringable {
        POSTBACK,
        MESSAGE,
        URI,
        DATETIMEPICKER,
        CAMERA,
        CAMERAROLL,
        LOCATION
    }

    public Action(@NonNull Type type2, @Nullable String str) {
        this.type = type2;
        this.label = str;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", this.type.name().toLowerCase());
        JSONUtils.put(jSONObject, "label", this.label);
        return jSONObject;
    }

    public Action(@NonNull Type type2) {
        this(type2, null);
    }
}
