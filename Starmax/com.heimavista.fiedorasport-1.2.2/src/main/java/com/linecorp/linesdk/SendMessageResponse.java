package com.linecorp.linesdk;

import androidx.annotation.NonNull;
import org.json.JSONObject;

public class SendMessageResponse {
    @NonNull
    private String receiverId;
    @NonNull
    private Status status;

    public enum Status {
        OK,
        DISCARDED
    }

    public SendMessageResponse(@NonNull String str, @NonNull Status status2) {
        this.receiverId = str;
        this.status = status2;
    }

    @NonNull
    public static SendMessageResponse fromJsonObject(@NonNull JSONObject jSONObject) {
        Status status2;
        if (jSONObject.get("status").equals(Status.OK.name().toLowerCase())) {
            status2 = Status.OK;
        } else {
            status2 = Status.DISCARDED;
        }
        return new SendMessageResponse(jSONObject.getString("to"), status2);
    }

    @NonNull
    public Status getStatus() {
        return this.status;
    }

    @NonNull
    public String getTargetUserId() {
        return this.receiverId;
    }

    public String toString() {
        return "SendMessageResponse{receiverId='" + this.receiverId + '\'' + ", status='" + this.status + '\'' + '}';
    }
}
