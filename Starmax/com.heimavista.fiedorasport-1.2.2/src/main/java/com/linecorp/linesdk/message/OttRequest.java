package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class OttRequest {
    @NonNull
    private List<String> targetUserIds;

    public OttRequest(@NonNull List<String> list) {
        this.targetUserIds = list;
    }

    @NonNull
    private JSONObject toJsonObject() {
        JSONArray jSONArray = new JSONArray();
        for (String str : this.targetUserIds) {
            jSONArray.put(str);
        }
        return new JSONObject().put("userIds", jSONArray);
    }

    @NonNull
    public String toJsonString() {
        return toJsonObject().toString();
    }
}
