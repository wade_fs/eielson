package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import com.linecorp.linesdk.message.flex.action.Action;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexImageComponent extends FlexMessageComponent {
    @Nullable
    private Action action;
    @Nullable
    private FlexMessageComponent.Alignment align;
    @Nullable
    private FlexMessageComponent.AspectMode aspectMode;
    @Nullable
    private FlexMessageComponent.AspectRatio aspectRatio;
    @Nullable
    private String backgroundColor;
    @Nullable
    private int flex;
    @Nullable
    private FlexMessageComponent.Gravity gravity;
    @Nullable
    private FlexMessageComponent.Margin margin;
    @Nullable
    private FlexMessageComponent.Size size;
    @NonNull
    private String url;

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public Action action;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Alignment align;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.AspectMode aspectMode;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.AspectRatio aspectRatio;
        /* access modifiers changed from: private */
        @Nullable
        public String backgroundColor;
        /* access modifiers changed from: private */
        public int flex;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Gravity gravity;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Size size;
        /* access modifiers changed from: private */
        @NonNull
        public String url;

        public FlexImageComponent build() {
            return new FlexImageComponent(this);
        }

        public Builder setAction(@Nullable Action action2) {
            this.action = action2;
            return this;
        }

        public Builder setAlign(@Nullable FlexMessageComponent.Alignment alignment) {
            this.align = alignment;
            return this;
        }

        public Builder setAspectMode(@Nullable FlexMessageComponent.AspectMode aspectMode2) {
            this.aspectMode = aspectMode2;
            return this;
        }

        public Builder setAspectRatio(@Nullable FlexMessageComponent.AspectRatio aspectRatio2) {
            this.aspectRatio = aspectRatio2;
            return this;
        }

        public Builder setBackgroundColor(@Nullable String str) {
            this.backgroundColor = str;
            return this;
        }

        public Builder setFlex(int i) {
            this.flex = i;
            return this;
        }

        public Builder setGravity(@Nullable FlexMessageComponent.Gravity gravity2) {
            this.gravity = gravity2;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        public Builder setSize(@Nullable FlexMessageComponent.Size size2) {
            this.size = size2;
            return this;
        }

        private Builder(@NonNull String str) {
            this.flex = -1;
            this.url = str;
        }
    }

    public static Builder newBuilder(@NonNull String str) {
        return new Builder(str);
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("url", this.url);
        int i = this.flex;
        if (i != -1) {
            jsonObject.put("flex", i);
        }
        JSONUtils.put(jsonObject, "margin", this.margin);
        JSONUtils.put(jsonObject, "align", this.align);
        JSONUtils.put(jsonObject, "gravity", this.gravity);
        FlexMessageComponent.Size size2 = this.size;
        String str = null;
        JSONUtils.put(jsonObject, "size", size2 != null ? size2.getValue() : null);
        FlexMessageComponent.AspectRatio aspectRatio2 = this.aspectRatio;
        if (aspectRatio2 != null) {
            str = aspectRatio2.getValue();
        }
        JSONUtils.put(jsonObject, "aspectRatio", str);
        JSONUtils.put(jsonObject, "aspectMode", this.aspectMode);
        JSONUtils.put(jsonObject, TtmlNode.ATTR_TTS_BACKGROUND_COLOR, this.backgroundColor);
        JSONUtils.put(jsonObject, NativeProtocol.WEB_DIALOG_ACTION, this.action);
        return jsonObject;
    }

    private FlexImageComponent() {
        super(FlexMessageComponent.Type.IMAGE);
        this.align = FlexMessageComponent.Alignment.CENTER;
        this.gravity = FlexMessageComponent.Gravity.TOP;
    }

    private FlexImageComponent(@NonNull Builder builder) {
        this();
        this.url = builder.url;
        this.flex = builder.flex;
        this.margin = builder.margin;
        this.align = builder.align;
        this.gravity = builder.gravity;
        this.size = builder.size;
        this.aspectRatio = builder.aspectRatio;
        this.aspectMode = builder.aspectMode;
        this.backgroundColor = builder.backgroundColor;
        this.action = builder.action;
    }
}
