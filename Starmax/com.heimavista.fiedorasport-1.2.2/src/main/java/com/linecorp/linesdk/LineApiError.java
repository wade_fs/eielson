package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.PrintWriter;
import java.io.StringWriter;

public class LineApiError implements Parcelable {
    public static final Parcelable.Creator<LineApiError> CREATOR = new Parcelable.Creator<LineApiError>() {
        /* class com.linecorp.linesdk.LineApiError.C46351 */

        public LineApiError createFromParcel(Parcel parcel) {
            return new LineApiError(parcel);
        }

        public LineApiError[] newArray(int i) {
            return new LineApiError[i];
        }
    };
    public static final LineApiError DEFAULT = new LineApiError(-1, "");
    private static final int DEFAULT_HTTP_RESPONSE_CODE = -1;
    private final int httpResponseCode;
    @Nullable
    private final String message;

    @Nullable
    private static String toString(@Nullable Exception exc) {
        if (exc == null) {
            return null;
        }
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineApiError.class != obj.getClass()) {
            return false;
        }
        LineApiError lineApiError = (LineApiError) obj;
        if (this.httpResponseCode != lineApiError.httpResponseCode) {
            return false;
        }
        String str = this.message;
        String str2 = lineApiError.message;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int getHttpResponseCode() {
        return this.httpResponseCode;
    }

    @Nullable
    public String getMessage() {
        return this.message;
    }

    public int hashCode() {
        int i = this.httpResponseCode * 31;
        String str = this.message;
        return i + (str != null ? str.hashCode() : 0);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.httpResponseCode);
        parcel.writeString(this.message);
    }

    public LineApiError(@Nullable Exception exc) {
        this(-1, toString(exc));
    }

    public LineApiError(@Nullable String str) {
        this(-1, str);
    }

    public LineApiError(int i, @Nullable Exception exc) {
        this(i, toString(exc));
    }

    public String toString() {
        return "LineApiError{httpResponseCode=" + this.httpResponseCode + ", message='" + this.message + '\'' + '}';
    }

    public LineApiError(int i, @Nullable String str) {
        this.httpResponseCode = i;
        this.message = str;
    }

    private LineApiError(@NonNull Parcel parcel) {
        this.httpResponseCode = parcel.readInt();
        this.message = parcel.readString();
    }
}
