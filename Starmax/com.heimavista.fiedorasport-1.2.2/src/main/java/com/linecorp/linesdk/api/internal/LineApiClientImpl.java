package com.linecorp.linesdk.api.internal;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.FriendSortField;
import com.linecorp.linesdk.GetFriendsResponse;
import com.linecorp.linesdk.GetGroupsResponse;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineFriendshipStatus;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.SendMessageResponse;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.internal.AccessTokenCache;
import com.linecorp.linesdk.internal.AccessTokenVerificationResult;
import com.linecorp.linesdk.internal.InternalAccessToken;
import com.linecorp.linesdk.internal.RefreshTokenResult;
import com.linecorp.linesdk.internal.nwclient.LineAuthenticationApiClient;
import com.linecorp.linesdk.internal.nwclient.TalkApiClient;
import com.linecorp.linesdk.message.MessageData;
import java.util.List;

public class LineApiClientImpl implements LineApiClient {
    private static final LineApiResponse ERROR_RESPONSE_NO_TOKEN = LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token is null"));
    @NonNull
    private final AccessTokenCache accessTokenCache;
    @NonNull
    private final String channelId;
    @NonNull
    private final LineAuthenticationApiClient oauthApiClient;
    @NonNull
    private final TalkApiClient talkApiClient;

    @FunctionalInterface
    private interface APIWithAccessToken<T> {
        LineApiResponse<T> call(InternalAccessToken internalAccessToken);
    }

    public LineApiClientImpl(@NonNull String str, @NonNull LineAuthenticationApiClient lineAuthenticationApiClient, @NonNull TalkApiClient talkApiClient2, @NonNull AccessTokenCache accessTokenCache2) {
        this.channelId = str;
        this.oauthApiClient = lineAuthenticationApiClient;
        this.talkApiClient = talkApiClient2;
        this.accessTokenCache = accessTokenCache2;
    }

    @NonNull
    private <T> LineApiResponse<T> callWithAccessToken(@NonNull APIWithAccessToken<T> aPIWithAccessToken) {
        InternalAccessToken accessToken = this.accessTokenCache.getAccessToken();
        if (accessToken == null) {
            return ERROR_RESPONSE_NO_TOKEN;
        }
        return aPIWithAccessToken.call(accessToken);
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25658a(@NonNull FriendSortField friendSortField, @Nullable String str, boolean z, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.getFriends(internalAccessToken, friendSortField, str, z);
    }

    @NonNull
    public LineApiResponse<LineAccessToken> getCurrentAccessToken() {
        InternalAccessToken accessToken = this.accessTokenCache.getAccessToken();
        if (accessToken == null) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("The cached access token does not exist."));
        }
        return LineApiResponse.createAsSuccess(new LineAccessToken(accessToken.getAccessToken(), accessToken.getExpiresInMillis(), accessToken.getIssuedClientTimeMillis()));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetFriendsResponse> getFriends(@NonNull FriendSortField friendSortField, @Nullable String str) {
        return getFriends(friendSortField, str, false);
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetFriendsResponse> getFriendsApprovers(FriendSortField friendSortField, @Nullable String str) {
        return callWithAccessToken(new C4651c(this, friendSortField, str));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<LineFriendshipStatus> getFriendshipStatus() {
        TalkApiClient talkApiClient2 = this.talkApiClient;
        talkApiClient2.getClass();
        return callWithAccessToken(new C4650b(talkApiClient2));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetFriendsResponse> getGroupApprovers(@NonNull String str, @Nullable String str2) {
        return callWithAccessToken(new C4654f(this, str, str2));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetGroupsResponse> getGroups(@Nullable String str) {
        return getGroups(str, false);
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<LineProfile> getProfile() {
        TalkApiClient talkApiClient2 = this.talkApiClient;
        talkApiClient2.getClass();
        return callWithAccessToken(new C4649a(talkApiClient2));
    }

    @NonNull
    public LineApiResponse<?> logout() {
        return callWithAccessToken(new C4653e(this));
    }

    @NonNull
    public LineApiResponse<LineAccessToken> refreshAccessToken() {
        InternalAccessToken accessToken = this.accessTokenCache.getAccessToken();
        if (accessToken == null || TextUtils.isEmpty(accessToken.getRefreshToken())) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token or refresh token is not found."));
        }
        LineApiResponse<RefreshTokenResult> refreshToken = this.oauthApiClient.refreshToken(this.channelId, accessToken);
        if (!refreshToken.isSuccess()) {
            return LineApiResponse.createAsError(refreshToken.getResponseCode(), refreshToken.getErrorData());
        }
        RefreshTokenResult responseData = refreshToken.getResponseData();
        InternalAccessToken internalAccessToken = new InternalAccessToken(responseData.getAccessToken(), responseData.getExpiresInMillis(), System.currentTimeMillis(), TextUtils.isEmpty(responseData.getRefreshToken()) ? accessToken.getRefreshToken() : responseData.getRefreshToken());
        this.accessTokenCache.saveAccessToken(internalAccessToken);
        return LineApiResponse.createAsSuccess(new LineAccessToken(internalAccessToken.getAccessToken(), internalAccessToken.getExpiresInMillis(), internalAccessToken.getIssuedClientTimeMillis()));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<String> sendMessage(@NonNull String str, @NonNull List<MessageData> list) {
        return callWithAccessToken(new C4658j(this, str, list));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull List<String> list, @NonNull List<MessageData> list2) {
        return sendMessageToMultipleUsers(list, list2, false);
    }

    @NonNull
    public LineApiResponse<LineCredential> verifyToken() {
        return callWithAccessToken(new C4656h(this));
    }

    /* access modifiers changed from: private */
    @NonNull
    public LineApiResponse<?> logout(@NonNull InternalAccessToken internalAccessToken) {
        LineApiResponse<?> revokeRefreshToken = this.oauthApiClient.revokeRefreshToken(this.channelId, internalAccessToken);
        if (revokeRefreshToken.isSuccess()) {
            this.accessTokenCache.clear();
        }
        return revokeRefreshToken;
    }

    /* access modifiers changed from: private */
    @NonNull
    public LineApiResponse<LineCredential> verifyToken(@NonNull InternalAccessToken internalAccessToken) {
        LineApiResponse<AccessTokenVerificationResult> verifyAccessToken = this.oauthApiClient.verifyAccessToken(internalAccessToken);
        if (!verifyAccessToken.isSuccess()) {
            return LineApiResponse.createAsError(verifyAccessToken.getResponseCode(), verifyAccessToken.getErrorData());
        }
        AccessTokenVerificationResult responseData = verifyAccessToken.getResponseData();
        long currentTimeMillis = System.currentTimeMillis();
        this.accessTokenCache.saveAccessToken(new InternalAccessToken(internalAccessToken.getAccessToken(), responseData.getExpiresInMillis(), currentTimeMillis, internalAccessToken.getRefreshToken()));
        return LineApiResponse.createAsSuccess(new LineCredential(new LineAccessToken(internalAccessToken.getAccessToken(), responseData.getExpiresInMillis(), currentTimeMillis), responseData.getScopes()));
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25657a(FriendSortField friendSortField, @Nullable String str, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.getFriendsApprovers(internalAccessToken, friendSortField, str);
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetFriendsResponse> getFriends(@NonNull FriendSortField friendSortField, @Nullable String str, boolean z) {
        return callWithAccessToken(new C4652d(this, friendSortField, str, z));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<GetGroupsResponse> getGroups(@Nullable String str, boolean z) {
        return callWithAccessToken(new C4657i(this, str, z));
    }

    @NonNull
    @TokenAutoRefresh
    public LineApiResponse<List<SendMessageResponse>> sendMessageToMultipleUsers(@NonNull List<String> list, @NonNull List<MessageData> list2, boolean z) {
        return callWithAccessToken(new C4655g(this, list, list2, z));
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25661a(@Nullable String str, boolean z, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.getGroups(internalAccessToken, str, z);
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25659a(@NonNull String str, @Nullable String str2, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.getGroupApprovers(internalAccessToken, str, str2);
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25660a(@NonNull String str, @NonNull List list, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.sendMessage(internalAccessToken, str, list);
    }

    /* renamed from: a */
    public /* synthetic */ LineApiResponse mo25662a(@NonNull List list, @NonNull List list2, boolean z, InternalAccessToken internalAccessToken) {
        return this.talkApiClient.sendMessageToMultipleUsers(internalAccessToken, list, list2, z);
    }
}
