package com.linecorp.linesdk.utils;

import android.os.Parcel;
import java.util.Date;

public final class ParcelUtils {
    private static final long TIME_NONE = -1;

    private ParcelUtils() {
    }

    public static Date readDate(Parcel parcel) {
        long readLong = parcel.readLong();
        if (readLong != -1) {
            return new Date(readLong);
        }
        return null;
    }

    public static <T extends Enum<T>> T readEnum(Parcel parcel, Class<T> cls) {
        String readString = parcel.readString();
        if (readString != null) {
            return Enum.valueOf(cls, readString);
        }
        return null;
    }

    public static void writeDate(Parcel parcel, Date date) {
        parcel.writeLong(date != null ? date.getTime() : -1);
    }

    public static <T extends Enum> void writeEnum(Parcel parcel, T t) {
        parcel.writeString(t != null ? t.name() : null);
    }
}
