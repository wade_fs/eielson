package com.linecorp.linesdk.message.template;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import com.linecorp.linesdk.message.MessageSender;
import com.linecorp.linesdk.utils.JSONUtils;
import java.util.List;
import org.json.JSONObject;

public class ButtonsLayoutTemplate extends LayoutTemplate {
    @NonNull
    private List<ClickActionForTemplateMessage> actions;
    @Nullable
    private ClickActionForTemplateMessage defaultAction;
    @NonNull
    private ImageAspectRatio imageAspectRatio = ImageAspectRatio.RECTANGLE;
    @ColorInt
    private int imageBackgroundColor = -1;
    @NonNull
    private ImageScaleType imageScaleType = ImageScaleType.COVER;
    @Nullable
    private MessageSender messageSender;
    @NonNull
    private String text;
    @Nullable
    private String thumbnailImageUrl;
    @Nullable
    private String title;

    public ButtonsLayoutTemplate(@NonNull String str, @NonNull List<ClickActionForTemplateMessage> list) {
        super(Type.BUTTONS);
        this.text = str;
        this.actions = list;
    }

    @NonNull
    private String getColorString(@ColorInt int i) {
        return String.format("#%06X", Integer.valueOf(i & ViewCompat.MEASURED_SIZE_MASK));
    }

    public void setDefaultAction(@Nullable ClickActionForTemplateMessage clickActionForTemplateMessage) {
        this.defaultAction = clickActionForTemplateMessage;
    }

    public void setImageAspectRatio(@NonNull ImageAspectRatio imageAspectRatio2) {
        this.imageAspectRatio = imageAspectRatio2;
    }

    public void setImageBackgroundColor(@ColorInt int i) {
        this.imageBackgroundColor = i;
    }

    public void setImageScaleType(@NonNull ImageScaleType imageScaleType2) {
        this.imageScaleType = imageScaleType2;
    }

    public void setMessageSender(@Nullable MessageSender messageSender2) {
        this.messageSender = messageSender2;
    }

    public void setThumbnailImageUrl(@Nullable String str) {
        this.thumbnailImageUrl = str;
    }

    public void setTitle(@Nullable String str) {
        this.title = str;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, "text", this.text);
        JSONUtils.put(jsonObject, "thumbnailImageUrl", this.thumbnailImageUrl);
        JSONUtils.put(jsonObject, "imageAspectRatio", this.imageAspectRatio.getServerKey());
        JSONUtils.put(jsonObject, "imageSize", this.imageScaleType.getServerKey());
        JSONUtils.put(jsonObject, "imageBackgroundColor", getColorString(this.imageBackgroundColor));
        JSONUtils.put(jsonObject, "title", this.title);
        JSONUtils.put(jsonObject, "defaultAction", this.defaultAction);
        JSONUtils.put(jsonObject, "sentBy", this.messageSender);
        JSONUtils.putArray(jsonObject, "actions", this.actions);
        return jsonObject;
    }
}
