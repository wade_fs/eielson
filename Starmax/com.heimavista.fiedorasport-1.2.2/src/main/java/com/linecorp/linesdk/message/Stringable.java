package com.linecorp.linesdk.message;

public interface Stringable {
    String name();
}
