package com.linecorp.linesdk.widget;

import android.view.View;

/* renamed from: com.linecorp.linesdk.widget.b */
/* compiled from: lambda */
public final /* synthetic */ class C4689b implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ LoginButton f9564P;

    /* renamed from: Q */
    private final /* synthetic */ View.OnClickListener f9565Q;

    public /* synthetic */ C4689b(LoginButton loginButton, View.OnClickListener onClickListener) {
        this.f9564P = loginButton;
        this.f9565Q = onClickListener;
    }

    public final void onClick(View view) {
        this.f9564P.mo26024a(this.f9565Q, view);
    }
}
