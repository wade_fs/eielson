package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import com.linecorp.linesdk.message.flex.action.Action;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import java.util.List;
import org.json.JSONObject;

public class FlexBoxComponent extends FlexMessageComponent {
    @Nullable
    private Action action;
    @NonNull
    private List<FlexMessageComponent> contents;
    @Nullable
    private int flex;
    @NonNull
    private FlexMessageComponent.Layout layout;
    @Nullable
    private FlexMessageComponent.Margin margin;
    @Nullable
    private FlexMessageComponent.Margin spacing;

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public Action action;
        /* access modifiers changed from: private */
        @NonNull
        public List<FlexMessageComponent> contents;
        /* access modifiers changed from: private */
        public int flex;
        /* access modifiers changed from: private */
        @NonNull
        public FlexMessageComponent.Layout layout;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin spacing;

        public FlexBoxComponent build() {
            return new FlexBoxComponent(this);
        }

        public Builder setAction(@Nullable Action action2) {
            this.action = action2;
            return this;
        }

        public Builder setFlex(int i) {
            this.flex = i;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        public Builder setSpacing(@Nullable FlexMessageComponent.Margin margin2) {
            this.spacing = margin2;
            return this;
        }

        private Builder(@NonNull FlexMessageComponent.Layout layout2, @NonNull List<FlexMessageComponent> list) {
            this.flex = -1;
            this.layout = layout2;
            this.contents = list;
        }
    }

    public static Builder newBuilder(@NonNull FlexMessageComponent.Layout layout2, @NonNull List<FlexMessageComponent> list) {
        return new Builder(layout2, list);
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, TtmlNode.TAG_LAYOUT, this.layout);
        JSONUtils.putArray(jsonObject, "contents", this.contents);
        JSONUtils.put(jsonObject, "spacing", this.spacing);
        JSONUtils.put(jsonObject, "margin", this.margin);
        JSONUtils.put(jsonObject, NativeProtocol.WEB_DIALOG_ACTION, this.action);
        int i = this.flex;
        if (i != -1) {
            jsonObject.put("flex", i);
        }
        return jsonObject;
    }

    private FlexBoxComponent() {
        super(FlexMessageComponent.Type.BOX);
    }

    private FlexBoxComponent(@NonNull Builder builder) {
        this();
        this.layout = builder.layout;
        this.contents = builder.contents;
        this.flex = builder.flex;
        this.spacing = builder.spacing;
        this.margin = builder.margin;
        this.action = builder.action;
    }
}
