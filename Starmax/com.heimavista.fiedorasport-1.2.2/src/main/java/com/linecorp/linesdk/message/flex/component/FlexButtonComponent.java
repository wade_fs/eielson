package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.facebook.internal.NativeProtocol;
import com.linecorp.linesdk.message.flex.action.Action;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexButtonComponent extends FlexMessageComponent {
    @NonNull
    private Action action;
    @Nullable
    private String color;
    private int flex;
    @Nullable
    private FlexMessageComponent.Gravity gravity;
    @Nullable
    private FlexMessageComponent.Height height;
    @Nullable
    private FlexMessageComponent.Margin margin;
    @Nullable
    private FlexMessageComponent.Style style;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull
        public Action action;
        /* access modifiers changed from: private */
        @Nullable
        public String color;
        /* access modifiers changed from: private */
        public int flex;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Gravity gravity;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Height height;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Style style;

        public FlexButtonComponent build() {
            return new FlexButtonComponent(this);
        }

        public Builder setColor(@Nullable String str) {
            this.color = str;
            return this;
        }

        public Builder setFlex(int i) {
            this.flex = i;
            return this;
        }

        public Builder setGravity(@Nullable FlexMessageComponent.Gravity gravity2) {
            this.gravity = gravity2;
            return this;
        }

        public Builder setHeight(@Nullable FlexMessageComponent.Height height2) {
            this.height = height2;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        public Builder setStyle(@Nullable FlexMessageComponent.Style style2) {
            this.style = style2;
            return this;
        }

        private Builder(@NonNull Action action2) {
            this.flex = -1;
            this.action = action2;
        }
    }

    public static Builder newBuilder(@NonNull Action action2) {
        return new Builder(action2);
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, NativeProtocol.WEB_DIALOG_ACTION, this.action);
        JSONUtils.put(jsonObject, "margin", this.margin);
        JSONUtils.put(jsonObject, ViewHierarchyConstants.DIMENSION_HEIGHT_KEY, this.height);
        JSONUtils.put(jsonObject, "style", this.style);
        JSONUtils.put(jsonObject, TtmlNode.ATTR_TTS_COLOR, this.color);
        JSONUtils.put(jsonObject, "gravity", this.gravity);
        int i = this.flex;
        if (i != -1) {
            jsonObject.put("flex", i);
        }
        return jsonObject;
    }

    private FlexButtonComponent() {
        super(FlexMessageComponent.Type.BUTTON);
    }

    private FlexButtonComponent(@NonNull Builder builder) {
        this();
        this.action = builder.action;
        this.flex = builder.flex;
        this.margin = builder.margin;
        this.height = builder.height;
        this.style = builder.style;
        this.color = builder.color;
        this.gravity = builder.gravity;
    }
}
