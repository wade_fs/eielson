package com.linecorp.linesdk.api.internal;

import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.internal.LineApiClientImpl;
import com.linecorp.linesdk.internal.InternalAccessToken;

/* renamed from: com.linecorp.linesdk.api.internal.h */
/* compiled from: lambda */
public final /* synthetic */ class C4656h implements LineApiClientImpl.APIWithAccessToken {

    /* renamed from: a */
    private final /* synthetic */ LineApiClientImpl f9529a;

    public /* synthetic */ C4656h(LineApiClientImpl lineApiClientImpl) {
        this.f9529a = lineApiClientImpl;
    }

    public final LineApiResponse call(InternalAccessToken internalAccessToken) {
        return this.f9529a.verifyToken(internalAccessToken);
    }
}
