package com.linecorp.linesdk;

import android.content.Intent;
import com.linecorp.linesdk.internal.LoginDelegateImpl;

public interface LoginDelegate {

    public static class Factory {
        public static LoginDelegate create() {
            return new LoginDelegateImpl();
        }
    }

    boolean onActivityResult(int i, int i2, Intent intent);
}
