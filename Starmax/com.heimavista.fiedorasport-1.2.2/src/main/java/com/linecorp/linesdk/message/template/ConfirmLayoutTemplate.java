package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import com.linecorp.linesdk.utils.JSONUtils;
import java.util.List;
import org.json.JSONObject;

public class ConfirmLayoutTemplate extends LayoutTemplate {
    @NonNull
    private List<ClickActionForTemplateMessage> actions;
    @NonNull
    private String text;

    public ConfirmLayoutTemplate(@NonNull String str, @Size(2) @NonNull List<ClickActionForTemplateMessage> list) {
        super(Type.CONFIRM);
        this.text = str;
        this.actions = list;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.put(jsonObject, "text", this.text);
        JSONUtils.putArray(jsonObject, "actions", this.actions);
        return jsonObject;
    }
}
