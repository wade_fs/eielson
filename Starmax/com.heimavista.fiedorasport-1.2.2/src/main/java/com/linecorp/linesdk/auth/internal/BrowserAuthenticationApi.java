package com.linecorp.linesdk.auth.internal;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.auth.LineAuthenticationConfig;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.internal.OneTimePassword;
import com.linecorp.linesdk.utils.UriUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p220f.p221a.p222a.SecurityUtils;

class BrowserAuthenticationApi {
    private static final LineAppVersion AUTO_LOGIN_FOR_LINE_SDK_ENABLED_VERSION = new LineAppVersion(6, 9, 0);
    @NonNull
    private final LineAuthenticationStatus authenticationStatus;

    @VisibleForTesting
    static class AuthenticationIntentHolder {
        @NonNull
        private final Intent intent;
        /* access modifiers changed from: private */
        public final boolean isLineAppAuthentication;
        @Nullable
        private final Bundle startActivityOptions;

        AuthenticationIntentHolder(@NonNull Intent intent2, @Nullable Bundle bundle, boolean z) {
            this.intent = intent2;
            this.startActivityOptions = bundle;
            this.isLineAppAuthentication = z;
        }

        @NonNull
        public Intent getIntent() {
            return this.intent;
        }

        @Nullable
        public Bundle getStartActivityOptions() {
            return this.startActivityOptions;
        }

        public boolean isLineAppAuthentication() {
            return this.isLineAppAuthentication;
        }
    }

    static class Request {
        @NonNull
        private final Intent intent;
        private final boolean isLineAppAuthentication;
        @NonNull
        private final String redirectUri;
        @Nullable
        private final Bundle startActivityOptions;

        @VisibleForTesting
        Request(@NonNull Intent intent2, @Nullable Bundle bundle, @NonNull String str, boolean z) {
            this.intent = intent2;
            this.startActivityOptions = bundle;
            this.redirectUri = str;
            this.isLineAppAuthentication = z;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public Intent getIntent() {
            return this.intent;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public String getRedirectUri() {
            return this.redirectUri;
        }

        /* access modifiers changed from: package-private */
        @Nullable
        public Bundle getStartActivityOptions() {
            return this.startActivityOptions;
        }

        /* access modifiers changed from: package-private */
        public boolean isLineAppAuthentication() {
            return this.isLineAppAuthentication;
        }
    }

    static class Result {
        @Nullable
        private final Boolean friendshipStatusChanged;
        @Nullable
        private final String internalErrorMessage;
        @Nullable
        private final String requestToken;
        @Nullable
        private final String serverErrorCode;
        @Nullable
        private final String serverErrorDescription;

        private Result(@Nullable String str, @Nullable Boolean bool, @Nullable String str2, @Nullable String str3, @Nullable String str4) {
            this.requestToken = str;
            this.friendshipStatusChanged = bool;
            this.serverErrorCode = str2;
            this.serverErrorDescription = str3;
            this.internalErrorMessage = str4;
        }

        private void checkRequestToken() {
            if (TextUtils.isEmpty(this.requestToken)) {
                throw new UnsupportedOperationException("requestToken is null. Please check result by isSuccess before.");
            }
        }

        @VisibleForTesting
        @NonNull
        static Result createAsAuthenticationAgentError(@NonNull String str, @NonNull String str2) {
            return new Result(null, null, str, str2, null);
        }

        @VisibleForTesting
        @NonNull
        static Result createAsInternalError(@NonNull String str) {
            return new Result(null, null, null, null, str);
        }

        @VisibleForTesting
        @NonNull
        static Result createAsSuccess(@NonNull String str, @Nullable Boolean bool) {
            return new Result(str, bool, null, null, null);
        }

        /* access modifiers changed from: package-private */
        @Nullable
        public Boolean getFriendshipStatusChanged() {
            checkRequestToken();
            return this.friendshipStatusChanged;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public LineApiError getLineApiError() {
            if (!isAuthenticationAgentError()) {
                return new LineApiError(this.internalErrorMessage);
            }
            try {
                return new LineApiError(new JSONObject().putOpt("error", this.serverErrorCode).putOpt(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION, this.serverErrorDescription).toString());
            } catch (JSONException e) {
                return new LineApiError(e);
            }
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public String getRequestToken() {
            checkRequestToken();
            return this.requestToken;
        }

        /* access modifiers changed from: package-private */
        public boolean isAuthenticationAgentError() {
            return TextUtils.isEmpty(this.internalErrorMessage) && !isSuccess();
        }

        /* access modifiers changed from: package-private */
        public boolean isSuccess() {
            return !TextUtils.isEmpty(this.requestToken);
        }
    }

    BrowserAuthenticationApi(@NonNull LineAuthenticationStatus lineAuthenticationStatus) {
        this.authenticationStatus = lineAuthenticationStatus;
    }

    @NonNull
    private static List<Intent> convertToIntents(@NonNull Uri uri, @NonNull Collection<ResolveInfo> collection, @Nullable Bundle bundle) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (ResolveInfo resolveInfo : collection) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(uri);
            intent.setPackage(resolveInfo.activityInfo.packageName);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            arrayList.add(intent);
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public Uri createLoginUrl(@NonNull LineAuthenticationConfig lineAuthenticationConfig, @NonNull OneTimePassword oneTimePassword, @NonNull LineAuthenticationParams lineAuthenticationParams, @NonNull String str, @Nullable String str2, @NonNull String str3) {
        Map<String, String> buildParams = UriUtils.buildParams(ServerProtocol.DIALOG_PARAM_RESPONSE_TYPE, "code", "client_id", lineAuthenticationConfig.getChannelId(), ServerProtocol.DIALOG_PARAM_STATE, str, "otpId", oneTimePassword.getId(), ServerProtocol.DIALOG_PARAM_REDIRECT_URI, str3, "sdk_ver", BuildConfig.VERSION_NAME, "scope", Scope.join(lineAuthenticationParams.getScopes()));
        if (!TextUtils.isEmpty(str2)) {
            buildParams.put("nonce", str2);
        }
        if (lineAuthenticationParams.getBotPrompt() != null) {
            buildParams.put("bot_prompt", lineAuthenticationParams.getBotPrompt().name().toLowerCase());
        }
        Map<String, String> buildParams2 = UriUtils.buildParams("returnUri", UriUtils.appendQueryParams("/oauth2/v2.1/authorize/consent", buildParams).toString(), "loginChannelId", lineAuthenticationConfig.getChannelId());
        if (lineAuthenticationParams.getUILocale() != null) {
            buildParams2.put("ui_locales", lineAuthenticationParams.getUILocale().toString());
        }
        return UriUtils.appendQueryParams(lineAuthenticationConfig.getWebLoginPageUrl(), buildParams2);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public String createRedirectUri(@NonNull Context context) {
        return "intent://result#Intent;package=" + context.getPackageName() + ";scheme=lineauth;end";
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public AuthenticationIntentHolder getAuthenticationIntentHolder(@NonNull Context context, @NonNull Uri uri, boolean z) {
        Intent intent;
        Bundle bundle;
        if (isChromeCustomTabSupported()) {
            CustomTabsIntent build = new CustomTabsIntent.Builder().setToolbarColor(ContextCompat.getColor(context, 17170443)).build();
            intent = build.intent.setData(uri);
            bundle = build.startAnimationBundle;
        } else {
            intent = new Intent("android.intent.action.VIEW").setData(uri);
            bundle = null;
        }
        LineAppVersion lineAppVersion = LineAppVersion.getLineAppVersion(context);
        if (lineAppVersion == null) {
            return new AuthenticationIntentHolder(intent, bundle, false);
        }
        if (!z && lineAppVersion.isEqualOrGreaterThan(AUTO_LOGIN_FOR_LINE_SDK_ENABLED_VERSION)) {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setData(uri);
            intent2.setPackage(BuildConfig.LINE_APP_PACKAGE_NAME);
            return new AuthenticationIntentHolder(intent2, bundle, true);
        }
        List<Intent> convertToIntents = convertToIntents(uri, context.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("http://")), 0), intent.getExtras());
        int size = convertToIntents.size();
        if (size == 0) {
            throw new ActivityNotFoundException("Activity for LINE log-in is not found. uri=" + uri);
        } else if (size == 1) {
            return new AuthenticationIntentHolder(convertToIntents.get(0), bundle, false);
        } else {
            Intent createChooser = Intent.createChooser(convertToIntents.remove(0), null);
            createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) convertToIntents.toArray(new Parcelable[convertToIntents.size()]));
            return new AuthenticationIntentHolder(createChooser, bundle, false);
        }
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Result getAuthenticationResultFrom(@NonNull Intent intent) {
        Uri data = intent.getData();
        if (data == null) {
            return Result.createAsInternalError("Illegal redirection from external application.");
        }
        String oAuthState = this.authenticationStatus.getOAuthState();
        String queryParameter = data.getQueryParameter(ServerProtocol.DIALOG_PARAM_STATE);
        if (oAuthState == null || !oAuthState.equals(queryParameter)) {
            return Result.createAsInternalError("Illegal parameter value of 'state'.");
        }
        String queryParameter2 = data.getQueryParameter("code");
        String queryParameter3 = data.getQueryParameter("friendship_status_changed");
        Boolean bool = null;
        if (!TextUtils.isEmpty(queryParameter3)) {
            bool = Boolean.valueOf(Boolean.parseBoolean(queryParameter3));
        }
        if (!TextUtils.isEmpty(queryParameter2)) {
            return Result.createAsSuccess(queryParameter2, bool);
        }
        return Result.createAsAuthenticationAgentError(data.getQueryParameter("error"), data.getQueryParameter(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Request getRequest(@NonNull Context context, @NonNull LineAuthenticationConfig lineAuthenticationConfig, @NonNull OneTimePassword oneTimePassword, @NonNull LineAuthenticationParams lineAuthenticationParams) {
        String str;
        String a = SecurityUtils.m17195a(8);
        this.authenticationStatus.setOAuthState(a);
        if (lineAuthenticationParams.getScopes().contains(Scope.OPENID_CONNECT)) {
            str = !TextUtils.isEmpty(lineAuthenticationParams.getNonce()) ? lineAuthenticationParams.getNonce() : SecurityUtils.m17195a(8);
        } else {
            str = null;
        }
        String str2 = str;
        this.authenticationStatus.setOpenIdNonce(str2);
        String createRedirectUri = createRedirectUri(context);
        AuthenticationIntentHolder authenticationIntentHolder = getAuthenticationIntentHolder(context, createLoginUrl(lineAuthenticationConfig, oneTimePassword, lineAuthenticationParams, a, str2, createRedirectUri), lineAuthenticationConfig.isLineAppAuthenticationDisabled());
        return new Request(authenticationIntentHolder.getIntent(), authenticationIntentHolder.getStartActivityOptions(), createRedirectUri, authenticationIntentHolder.isLineAppAuthentication);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean isChromeCustomTabSupported() {
        return Build.VERSION.SDK_INT >= 16;
    }
}
