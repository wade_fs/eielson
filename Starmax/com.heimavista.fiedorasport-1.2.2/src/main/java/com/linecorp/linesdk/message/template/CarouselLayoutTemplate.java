package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.Jsonable;
import com.linecorp.linesdk.utils.JSONUtils;
import java.util.List;
import org.json.JSONObject;

public class CarouselLayoutTemplate extends LayoutTemplate {
    @NonNull
    private List<CarouselColumn> columns;
    @Nullable
    private ImageAspectRatio imageAspectRatio = ImageAspectRatio.RECTANGLE;
    @Nullable
    private ImageScaleType imageScaleType = ImageScaleType.COVER;

    public static class CarouselColumn implements Jsonable {
        @NonNull
        private List<ClickActionForTemplateMessage> actions;
        @Nullable
        private ClickActionForTemplateMessage defaultAction;
        @Nullable
        private String imageBackgroundColor;
        @NonNull
        private String text;
        @Nullable
        private String thumbnailImageUrl;
        @Nullable
        private String title;

        public CarouselColumn(@NonNull String str, @NonNull List<ClickActionForTemplateMessage> list) {
            this.text = str;
            this.actions = list;
        }

        public void setDefaultAction(@Nullable ClickActionForTemplateMessage clickActionForTemplateMessage) {
            this.defaultAction = clickActionForTemplateMessage;
        }

        public void setImageBackgroundColor(@Nullable String str) {
            this.imageBackgroundColor = str;
        }

        public void setThumbnailImageUrl(@Nullable String str) {
            this.thumbnailImageUrl = str;
        }

        public void setTitle(@Nullable String str) {
            this.title = str;
        }

        @NonNull
        public JSONObject toJsonObject() {
            JSONObject jSONObject = new JSONObject();
            JSONUtils.put(jSONObject, "text", this.text);
            JSONUtils.putArray(jSONObject, "actions", this.actions);
            JSONUtils.put(jSONObject, "thumbnailImageUrl", this.thumbnailImageUrl);
            JSONUtils.put(jSONObject, "imageBackgroundColor", this.imageBackgroundColor);
            JSONUtils.put(jSONObject, "title", this.title);
            JSONUtils.put(jSONObject, "defaultAction", this.defaultAction);
            return jSONObject;
        }
    }

    public CarouselLayoutTemplate(@NonNull List<CarouselColumn> list) {
        super(Type.CAROUSEL);
        this.columns = list;
    }

    public void setImageAspectRatio(@Nullable ImageAspectRatio imageAspectRatio2) {
        this.imageAspectRatio = imageAspectRatio2;
    }

    public void setImageScaleType(@Nullable ImageScaleType imageScaleType2) {
        this.imageScaleType = imageScaleType2;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        JSONUtils.putArray(jsonObject, "columns", this.columns);
        JSONUtils.putArray(jsonObject, "columns", this.columns);
        JSONUtils.put(jsonObject, "imageAspectRatio", this.imageAspectRatio.getServerKey());
        JSONUtils.put(jsonObject, "imageSize", this.imageScaleType.getServerKey());
        return jsonObject;
    }
}
