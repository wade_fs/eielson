package com.linecorp.linesdk.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.dialog.internal.SendMessageContract;
import com.linecorp.linesdk.dialog.internal.SendMessagePresenter;
import com.linecorp.linesdk.dialog.internal.SendMessageTargetPagerAdapter;
import com.linecorp.linesdk.dialog.internal.TargetUser;
import com.linecorp.linesdk.dialog.internal.UserThumbnailView;
import com.linecorp.linesdk.message.MessageData;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SendMessageDialog extends AppCompatDialog implements SendMessageContract.View {
    private Button buttonConfirm;
    private View.OnClickListener confirmClickListener = new C4667d(this);
    private HorizontalScrollView horizontalScrollView;
    private LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
    private LinearLayout linearLayoutTargetUser;
    private MessageData messageData;
    private OnSendListener onSendListener;
    private SendMessagePresenter presenter;
    private SendMessageTargetPagerAdapter sendMessageTargetAdapter;
    private TabLayout tabLayout;
    private Map<String, View> targetUserViewCacheMap = new HashMap();
    private ViewPager viewPager;

    public interface OnSendListener {
        void onSendFailure(DialogInterface dialogInterface);

        void onSendSuccess(DialogInterface dialogInterface);
    }

    public SendMessageDialog(@NonNull Context context, @NonNull LineApiClient lineApiClient) {
        super(context, C4642R.C4647style.DialogTheme);
        this.presenter = new SendMessagePresenter(lineApiClient, this);
        SendMessagePresenter sendMessagePresenter = this.presenter;
        this.sendMessageTargetAdapter = new SendMessageTargetPagerAdapter(context, sendMessagePresenter, sendMessagePresenter);
    }

    @NonNull
    private UserThumbnailView createUserThumbnailView(TargetUser targetUser) {
        UserThumbnailView userThumbnailView = new UserThumbnailView(getContext());
        userThumbnailView.setOnClickListener(new C4665b(this, targetUser));
        userThumbnailView.setTargetUser(targetUser);
        return userThumbnailView;
    }

    private void setupUi() {
        this.viewPager.setAdapter(this.sendMessageTargetAdapter);
        this.tabLayout.setupWithViewPager(this.viewPager);
        this.buttonConfirm.setOnClickListener(this.confirmClickListener);
        this.viewPager.post(new C4666c(this));
    }

    private void updateConfirmButtonLabel() {
        int targetUserListSize = this.presenter.getTargetUserListSize();
        if (targetUserListSize == 0) {
            this.buttonConfirm.setText(17039370);
            this.buttonConfirm.setVisibility(8);
            return;
        }
        this.buttonConfirm.setText(getContext().getString(17039370) + " (" + targetUserListSize + ")");
        this.buttonConfirm.setVisibility(0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo25780a(View view) {
        this.presenter.sendMessage(this.messageData);
    }

    /* renamed from: c */
    public /* synthetic */ void mo25782c() {
        this.horizontalScrollView.fullScroll(66);
    }

    /* renamed from: d */
    public /* synthetic */ void mo25783d() {
        getWindow().clearFlags(131080);
    }

    public void dismiss() {
        this.presenter.release();
        super.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        View inflate = LayoutInflater.from(getContext()).inflate(C4642R.C4646layout.dialog_send_message, (ViewGroup) null);
        setContentView(inflate);
        this.viewPager = (ViewPager) inflate.findViewById(C4642R.C4645id.viewPager);
        this.tabLayout = (TabLayout) inflate.findViewById(C4642R.C4645id.tabLayout);
        this.buttonConfirm = (Button) inflate.findViewById(C4642R.C4645id.buttonConfirm);
        this.linearLayoutTargetUser = (LinearLayout) inflate.findViewById(C4642R.C4645id.linearLayoutTargetUserList);
        this.horizontalScrollView = (HorizontalScrollView) inflate.findViewById(C4642R.C4645id.horizontalScrollView);
        setupUi();
    }

    public void onExceedMaxTargetUserCount(int i) {
        Toast.makeText(getContext(), String.format(Locale.getDefault(), "You can only select up to %1$d.", Integer.valueOf(i)), 1).show();
    }

    public void onSendMessageFailure() {
        OnSendListener onSendListener2 = this.onSendListener;
        if (onSendListener2 != null) {
            onSendListener2.onSendFailure(this);
        }
        dismiss();
    }

    public void onSendMessageSuccess() {
        OnSendListener onSendListener2 = this.onSendListener;
        if (onSendListener2 != null) {
            onSendListener2.onSendSuccess(this);
        }
        dismiss();
    }

    public void onTargetUserAdded(TargetUser targetUser) {
        if (this.targetUserViewCacheMap.get(targetUser.getId()) == null) {
            this.targetUserViewCacheMap.put(targetUser.getId(), createUserThumbnailView(targetUser));
        }
        this.linearLayoutTargetUser.addView(this.targetUserViewCacheMap.get(targetUser.getId()), this.layoutParams);
        this.horizontalScrollView.post(new C4664a(this));
        updateConfirmButtonLabel();
    }

    public void onTargetUserRemoved(TargetUser targetUser) {
        this.linearLayoutTargetUser.removeView(this.targetUserViewCacheMap.get(targetUser.getId()));
        this.sendMessageTargetAdapter.unSelect(targetUser);
        updateConfirmButtonLabel();
    }

    public void setMessageData(MessageData messageData2) {
        this.messageData = messageData2;
    }

    public void setOnSendListener(@Nullable OnSendListener onSendListener2) {
        if (this.onSendListener == null) {
            this.onSendListener = onSendListener2;
            return;
        }
        throw new IllegalStateException("OnSendListener is already taken and can not be replaced.");
    }

    /* renamed from: a */
    public /* synthetic */ void mo25781a(TargetUser targetUser, View view) {
        this.presenter.removeTargetUser(targetUser);
    }
}
