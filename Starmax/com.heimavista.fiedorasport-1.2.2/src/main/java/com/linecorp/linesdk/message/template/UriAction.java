package com.linecorp.linesdk.message.template;

import androidx.annotation.NonNull;
import com.facebook.share.internal.ShareConstants;
import org.json.JSONObject;

public class UriAction extends ClickActionForTemplateMessage {
    @NonNull
    private String label;
    @NonNull
    private String uri;

    public UriAction(@NonNull String str, @NonNull String str2) {
        super.type = ShareConstants.MEDIA_URI;
        this.uri = str2;
        this.label = str;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put(ShareConstants.MEDIA_URI, this.uri);
        jsonObject.put("label", this.label);
        return jsonObject;
    }
}
