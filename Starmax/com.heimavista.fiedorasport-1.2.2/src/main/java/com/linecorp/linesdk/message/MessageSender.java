package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import org.json.JSONObject;

public class MessageSender implements Jsonable {
    @NonNull
    private final String footerIconUrl;
    @Nullable
    private final String footerLinkUrl;
    @NonNull
    private final String label;

    public MessageSender(@NonNull String str, @NonNull String str2, @Nullable String str3) {
        this.label = str;
        this.footerIconUrl = str2;
        this.footerLinkUrl = str3;
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("label", this.label);
        jSONObject.put("iconUrl", this.footerIconUrl);
        String str = this.footerLinkUrl;
        if (str != null) {
            jSONObject.put("linkUrl", str);
        }
        return jSONObject;
    }
}
