package com.linecorp.linesdk.widget;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import com.linecorp.linesdk.C4642R;
import com.linecorp.linesdk.LoginDelegate;
import com.linecorp.linesdk.LoginListener;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.internal.FragmentWrapper;
import com.linecorp.linesdk.internal.LoginDelegateImpl;
import com.linecorp.linesdk.internal.LoginHandler;
import java.util.Arrays;

public class LoginButton extends AppCompatTextView {
    @NonNull
    private LineAuthenticationParams authenticationParams = new LineAuthenticationParams.Builder().scopes(Arrays.asList(Scope.PROFILE)).build();
    @Nullable
    private String channelId;
    @Nullable
    private FragmentWrapper fragmentWrapper;
    @NonNull
    private View.OnClickListener internalListener = new C4688a(this);
    private boolean isLineAppAuthEnabled = true;
    @Nullable
    private LoginDelegate loginDelegate;
    @NonNull
    private LoginHandler loginHandler = new LoginHandler();

    public LoginButton(Context context) {
        super(context);
        init();
    }

    @NonNull
    private Activity getActivity() {
        Context context = getContext();
        while ((context instanceof ContextWrapper) && !(context instanceof Activity)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof Activity) {
            return (Activity) context;
        }
        throw new RuntimeException("Cannot find an Activity");
    }

    private void init() {
        setAllCaps(false);
        setGravity(17);
        setText(C4642R.string.btn_line_login);
        setTextColor(ContextCompat.getColor(getContext(), C4642R.C4643color.text_login_btn));
        setBackgroundResource(C4642R.C4644drawable.background_login_btn);
        super.setOnClickListener(this.internalListener);
    }

    private void performLoginWithActivity(@NonNull String str, @NonNull Activity activity) {
        this.loginHandler.performLogin(activity, this.isLineAppAuthEnabled, str, this.authenticationParams);
    }

    private void performLoginWithFragment(@NonNull String str, @NonNull FragmentWrapper fragmentWrapper2) {
        this.loginHandler.performLogin(getActivity(), fragmentWrapper2, this.isLineAppAuthEnabled, str, this.authenticationParams);
    }

    /* renamed from: a */
    public /* synthetic */ void mo26025a(View view) {
        String str = this.channelId;
        if (str == null) {
            throw new RuntimeException("Channel id should be set.");
        } else if (!str.isEmpty()) {
            FragmentWrapper fragmentWrapper2 = this.fragmentWrapper;
            if (fragmentWrapper2 != null) {
                performLoginWithFragment(this.channelId, fragmentWrapper2);
            } else {
                performLoginWithActivity(this.channelId, getActivity());
            }
        } else {
            throw new RuntimeException("Channel id should not be empty.");
        }
    }

    public void addLoginListener(@NonNull LoginListener loginListener) {
        if (this.loginDelegate != null) {
            this.loginHandler.addLoginListener(loginListener);
            return;
        }
        throw new RuntimeException("You must set LoginDelegate through setLoginDelegate()  first");
    }

    public void enableLineAppAuthentication(boolean z) {
        this.isLineAppAuthEnabled = z;
    }

    public void removeLoginListener(@NonNull LoginListener loginListener) {
        this.loginHandler.removeLoginListener(loginListener);
    }

    public void setAuthenticationParams(@NonNull LineAuthenticationParams lineAuthenticationParams) {
        this.authenticationParams = lineAuthenticationParams;
    }

    public void setChannelId(@NonNull String str) {
        this.channelId = str;
    }

    public void setFragment(@NonNull Fragment fragment) {
        this.fragmentWrapper = new FragmentWrapper(fragment);
    }

    public void setLoginDelegate(@NonNull LoginDelegate loginDelegate2) {
        if (loginDelegate2 instanceof LoginDelegateImpl) {
            ((LoginDelegateImpl) loginDelegate2).setLoginHandler(this.loginHandler);
            this.loginDelegate = loginDelegate2;
            return;
        }
        throw new RuntimeException("Unexpected LoginDelegate, please use the provided Factory to create the instance");
    }

    public void setOnClickListener(@Nullable View.OnClickListener onClickListener) {
        super.setOnClickListener(new C4689b(this, onClickListener));
    }

    public void setFragment(@NonNull androidx.fragment.app.Fragment fragment) {
        this.fragmentWrapper = new FragmentWrapper(fragment);
    }

    /* renamed from: a */
    public /* synthetic */ void mo26024a(@Nullable View.OnClickListener onClickListener, View view) {
        this.internalListener.onClick(view);
        if (onClickListener != null) {
            onClickListener.onClick(view);
        }
    }

    public LoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public LoginButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
