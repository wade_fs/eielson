package com.linecorp.linesdk.auth;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineIdToken;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.utils.ParcelUtils;

public class LineLoginResult implements Parcelable {
    public static final Parcelable.Creator<LineLoginResult> CREATOR = new Parcelable.Creator<LineLoginResult>() {
        /* class com.linecorp.linesdk.auth.LineLoginResult.C46611 */

        public LineLoginResult createFromParcel(Parcel parcel) {
            return new LineLoginResult(parcel);
        }

        public LineLoginResult[] newArray(int i) {
            return new LineLoginResult[i];
        }
    };
    @NonNull
    private final LineApiError errorData;
    @Nullable
    private final Boolean friendshipStatusChanged;
    @Nullable
    private final LineCredential lineCredential;
    @Nullable
    private final LineIdToken lineIdToken;
    @Nullable
    private final LineProfile lineProfile;
    @Nullable
    private final String nonce;
    @NonNull
    private final LineApiResponseCode responseCode;

    public static final class Builder {
        /* access modifiers changed from: private */
        public LineApiError errorData = LineApiError.DEFAULT;
        /* access modifiers changed from: private */
        public Boolean friendshipStatusChanged;
        /* access modifiers changed from: private */
        public LineCredential lineCredential;
        /* access modifiers changed from: private */
        public LineIdToken lineIdToken;
        /* access modifiers changed from: private */
        public LineProfile lineProfile;
        /* access modifiers changed from: private */
        public String nonce;
        /* access modifiers changed from: private */
        public LineApiResponseCode responseCode = LineApiResponseCode.SUCCESS;

        public LineLoginResult build() {
            return new LineLoginResult(this);
        }

        public Builder errorData(LineApiError lineApiError) {
            this.errorData = lineApiError;
            return this;
        }

        public Builder friendshipStatusChanged(Boolean bool) {
            this.friendshipStatusChanged = bool;
            return this;
        }

        public Builder lineCredential(LineCredential lineCredential2) {
            this.lineCredential = lineCredential2;
            return this;
        }

        public Builder lineIdToken(LineIdToken lineIdToken2) {
            this.lineIdToken = lineIdToken2;
            return this;
        }

        public Builder lineProfile(LineProfile lineProfile2) {
            this.lineProfile = lineProfile2;
            return this;
        }

        public Builder nonce(String str) {
            this.nonce = str;
            return this;
        }

        public Builder responseCode(LineApiResponseCode lineApiResponseCode) {
            this.responseCode = lineApiResponseCode;
            return this;
        }
    }

    public static LineLoginResult authenticationAgentError(@NonNull LineApiError lineApiError) {
        return error(LineApiResponseCode.AUTHENTICATION_AGENT_ERROR, lineApiError);
    }

    public static LineLoginResult canceledError() {
        return error(LineApiResponseCode.CANCEL, LineApiError.DEFAULT);
    }

    public static LineLoginResult error(@NonNull LineApiResponseCode lineApiResponseCode, @NonNull LineApiError lineApiError) {
        return new Builder().responseCode(lineApiResponseCode).errorData(lineApiError).build();
    }

    public static LineLoginResult internalError(@NonNull LineApiError lineApiError) {
        return error(LineApiResponseCode.INTERNAL_ERROR, lineApiError);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineLoginResult.class != obj.getClass()) {
            return false;
        }
        LineLoginResult lineLoginResult = (LineLoginResult) obj;
        if (this.responseCode != lineLoginResult.responseCode) {
            return false;
        }
        String str = this.nonce;
        if (str == null ? lineLoginResult.nonce != null : !str.equals(lineLoginResult.nonce)) {
            return false;
        }
        LineProfile lineProfile2 = this.lineProfile;
        if (lineProfile2 == null ? lineLoginResult.lineProfile != null : !lineProfile2.equals(lineLoginResult.lineProfile)) {
            return false;
        }
        LineIdToken lineIdToken2 = this.lineIdToken;
        if (lineIdToken2 == null ? lineLoginResult.lineIdToken != null : !lineIdToken2.equals(lineLoginResult.lineIdToken)) {
            return false;
        }
        Boolean bool = this.friendshipStatusChanged;
        if (bool == null ? lineLoginResult.friendshipStatusChanged != null : !bool.equals(lineLoginResult.friendshipStatusChanged)) {
            return false;
        }
        LineCredential lineCredential2 = this.lineCredential;
        if (lineCredential2 == null ? lineLoginResult.lineCredential == null : lineCredential2.equals(lineLoginResult.lineCredential)) {
            return this.errorData.equals(lineLoginResult.errorData);
        }
        return false;
    }

    @NonNull
    public LineApiError getErrorData() {
        return this.errorData;
    }

    @NonNull
    public Boolean getFriendshipStatusChanged() {
        Boolean bool = this.friendshipStatusChanged;
        if (bool == null) {
            return false;
        }
        return bool;
    }

    @Nullable
    public LineCredential getLineCredential() {
        return this.lineCredential;
    }

    @Nullable
    public LineIdToken getLineIdToken() {
        return this.lineIdToken;
    }

    @Nullable
    public LineProfile getLineProfile() {
        return this.lineProfile;
    }

    @Nullable
    public String getNonce() {
        return this.nonce;
    }

    @NonNull
    public LineApiResponseCode getResponseCode() {
        return this.responseCode;
    }

    public int hashCode() {
        int hashCode = this.responseCode.hashCode() * 31;
        String str = this.nonce;
        int i = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        LineProfile lineProfile2 = this.lineProfile;
        int hashCode3 = (hashCode2 + (lineProfile2 != null ? lineProfile2.hashCode() : 0)) * 31;
        LineIdToken lineIdToken2 = this.lineIdToken;
        int hashCode4 = (hashCode3 + (lineIdToken2 != null ? lineIdToken2.hashCode() : 0)) * 31;
        Boolean bool = this.friendshipStatusChanged;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        LineCredential lineCredential2 = this.lineCredential;
        if (lineCredential2 != null) {
            i = lineCredential2.hashCode();
        }
        return ((hashCode5 + i) * 31) + this.errorData.hashCode();
    }

    public boolean isSuccess() {
        return this.responseCode == LineApiResponseCode.SUCCESS;
    }

    public String toString() {
        return "LineLoginResult{responseCode=" + this.responseCode + ", nonce='" + this.nonce + '\'' + ", lineProfile=" + this.lineProfile + ", lineIdToken=" + this.lineIdToken + ", friendshipStatusChanged=" + this.friendshipStatusChanged + ", lineCredential=" + this.lineCredential + ", errorData=" + this.errorData + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        ParcelUtils.writeEnum(parcel, this.responseCode);
        parcel.writeString(this.nonce);
        parcel.writeParcelable(this.lineProfile, i);
        parcel.writeParcelable(this.lineIdToken, i);
        parcel.writeValue(this.friendshipStatusChanged);
        parcel.writeParcelable(this.lineCredential, i);
        parcel.writeParcelable(this.errorData, i);
    }

    public static LineLoginResult internalError(@NonNull String str) {
        return internalError(new LineApiError(str));
    }

    private LineLoginResult(Builder builder) {
        this.responseCode = builder.responseCode;
        this.nonce = builder.nonce;
        this.lineProfile = builder.lineProfile;
        this.lineIdToken = builder.lineIdToken;
        this.friendshipStatusChanged = builder.friendshipStatusChanged;
        this.lineCredential = builder.lineCredential;
        this.errorData = builder.errorData;
    }

    public static LineLoginResult internalError(@NonNull Exception exc) {
        return internalError(new LineApiError(exc));
    }

    public static LineLoginResult error(@NonNull LineApiResponse<?> lineApiResponse) {
        return error(lineApiResponse.getResponseCode(), lineApiResponse.getErrorData());
    }

    private LineLoginResult(@NonNull Parcel parcel) {
        this.responseCode = (LineApiResponseCode) ParcelUtils.readEnum(parcel, LineApiResponseCode.class);
        this.nonce = parcel.readString();
        this.lineProfile = (LineProfile) parcel.readParcelable(LineProfile.class.getClassLoader());
        this.lineIdToken = (LineIdToken) parcel.readParcelable(LineIdToken.class.getClassLoader());
        this.friendshipStatusChanged = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.lineCredential = (LineCredential) parcel.readParcelable(LineCredential.class.getClassLoader());
        this.errorData = (LineApiError) parcel.readParcelable(LineApiError.class.getClassLoader());
    }
}
