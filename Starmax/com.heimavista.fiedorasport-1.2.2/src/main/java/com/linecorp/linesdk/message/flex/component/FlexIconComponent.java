package com.linecorp.linesdk.message.flex.component;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.linecorp.linesdk.message.flex.component.FlexMessageComponent;
import com.linecorp.linesdk.utils.JSONUtils;
import org.json.JSONObject;

public class FlexIconComponent extends FlexMessageComponent {
    private FlexMessageComponent.AspectRatio aspectRatio;
    private FlexMessageComponent.Margin margin;
    private FlexMessageComponent.Size size;
    private String url;

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.AspectRatio aspectRatio;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Margin margin;
        /* access modifiers changed from: private */
        @Nullable
        public FlexMessageComponent.Size size;
        /* access modifiers changed from: private */
        @NonNull
        public String url;

        public FlexIconComponent build() {
            return new FlexIconComponent(this);
        }

        public Builder setAspectRatio(@Nullable FlexMessageComponent.AspectRatio aspectRatio2) {
            this.aspectRatio = aspectRatio2;
            return this;
        }

        public Builder setMargin(@Nullable FlexMessageComponent.Margin margin2) {
            this.margin = margin2;
            return this;
        }

        public Builder setSize(@Nullable FlexMessageComponent.Size size2) {
            this.size = size2;
            return this;
        }

        private Builder(@NonNull String str) {
            this.url = str;
        }
    }

    public static Builder newBuilder(@NonNull String str) {
        return new Builder(str);
    }

    @NonNull
    public JSONObject toJsonObject() {
        JSONObject jsonObject = super.toJsonObject();
        jsonObject.put("url", this.url);
        JSONUtils.put(jsonObject, "margin", this.margin);
        JSONUtils.put(jsonObject, "size", this.size);
        FlexMessageComponent.AspectRatio aspectRatio2 = this.aspectRatio;
        JSONUtils.put(jsonObject, "aspectRatio", aspectRatio2 != null ? aspectRatio2.getValue() : null);
        return jsonObject;
    }

    private FlexIconComponent() {
        super(FlexMessageComponent.Type.ICON);
    }

    private FlexIconComponent(@NonNull Builder builder) {
        this();
        this.url = builder.url;
        this.margin = builder.margin;
        this.size = builder.size;
        this.aspectRatio = builder.aspectRatio;
    }
}
