package com.linecorp.linesdk;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LineGroup implements Parcelable {
    public static final Parcelable.Creator<LineGroup> CREATOR = new Parcelable.Creator<LineGroup>() {
        /* class com.linecorp.linesdk.LineGroup.C46381 */

        public LineGroup createFromParcel(Parcel parcel) {
            return new LineGroup(parcel);
        }

        public LineGroup[] newArray(int i) {
            return new LineGroup[i];
        }
    };
    @NonNull
    private final String groupId;
    @NonNull
    private final String groupName;
    @Nullable
    private final Uri pictureUrl;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LineGroup.class != obj.getClass()) {
            return false;
        }
        LineGroup lineGroup = (LineGroup) obj;
        if (!this.groupId.equals(lineGroup.groupId) || !this.groupName.equals(lineGroup.groupName)) {
            return false;
        }
        Uri uri = this.pictureUrl;
        Uri uri2 = lineGroup.pictureUrl;
        return uri == null ? uri2 == null : uri.equals(uri2);
    }

    @NonNull
    public String getGroupId() {
        return this.groupId;
    }

    @NonNull
    public String getGroupName() {
        return this.groupName;
    }

    @Nullable
    public Uri getPictureUrl() {
        return this.pictureUrl;
    }

    public int hashCode() {
        int hashCode = ((this.groupId.hashCode() * 31) + this.groupName.hashCode()) * 31;
        Uri uri = this.pictureUrl;
        return hashCode + (uri != null ? uri.hashCode() : 0);
    }

    public String toString() {
        return "LineProfile{groupName='" + this.groupName + '\'' + ", groupId='" + this.groupId + '\'' + ", pictureUrl='" + this.pictureUrl + '\'' + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.groupId);
        parcel.writeString(this.groupName);
        parcel.writeParcelable(this.pictureUrl, i);
    }

    public LineGroup(@NonNull String str, @NonNull String str2, @Nullable Uri uri) {
        this.groupId = str;
        this.groupName = str2;
        this.pictureUrl = uri;
    }

    private LineGroup(@NonNull Parcel parcel) {
        this.groupId = parcel.readString();
        this.groupName = parcel.readString();
        this.pictureUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
    }
}
