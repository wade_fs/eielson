package com.linecorp.linesdk.dialog.internal;

import android.view.View;
import com.linecorp.linesdk.dialog.internal.TargetListAdapter;

/* renamed from: com.linecorp.linesdk.dialog.internal.b */
/* compiled from: lambda */
public final /* synthetic */ class C4675b implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ TargetListAdapter.TargetViewHolder f9543P;

    /* renamed from: Q */
    private final /* synthetic */ TargetUser f9544Q;

    /* renamed from: R */
    private final /* synthetic */ TargetListAdapter.OnSelectedChangeListener f9545R;

    public /* synthetic */ C4675b(TargetListAdapter.TargetViewHolder targetViewHolder, TargetUser targetUser, TargetListAdapter.OnSelectedChangeListener onSelectedChangeListener) {
        this.f9543P = targetViewHolder;
        this.f9544Q = targetUser;
        this.f9545R = onSelectedChangeListener;
    }

    public final void onClick(View view) {
        this.f9543P.mo25822a(this.f9544Q, this.f9545R, view);
    }
}
