package com.linecorp.linesdk.message;

import androidx.annotation.NonNull;
import org.json.JSONObject;

public interface Jsonable {
    @NonNull
    JSONObject toJsonObject();
}
