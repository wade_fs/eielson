package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.d.l.g */
public class C1230g {
    /* renamed from: a */
    public static int m3309a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return -1;
        }
        return bArr[0];
    }

    /* renamed from: b */
    public static int[] m3310b(byte[] bArr) {
        if (bArr == null || bArr.length <= 1) {
            return null;
        }
        byte[] bArr2 = new byte[(bArr.length - 1)];
        System.arraycopy(bArr, 1, bArr2, 0, bArr2.length);
        String stringBuffer = new StringBuffer(Long.toBinaryString(C1295d.m3509a(bArr2))).reverse().toString();
        C1293b.m3504b("parseSupportLanguageArray: " + stringBuffer);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < stringBuffer.length()) {
            int i2 = i + 1;
            if (Integer.parseInt(stringBuffer.substring(i, i2)) > 0) {
                arrayList.add(Integer.valueOf(i));
            }
            i = i2;
        }
        int[] iArr = new int[arrayList.size()];
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            iArr[i3] = ((Integer) arrayList.get(i3)).intValue();
        }
        return iArr;
    }
}
