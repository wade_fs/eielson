package com.crrepa.ble.nrf.dfu.p077q.p078c;

/* renamed from: com.crrepa.ble.nrf.dfu.q.c.b */
public class C1347b extends Exception {
    private static final long serialVersionUID = -6901728550661937942L;

    /* renamed from: P */
    private final int f2184P;

    public C1347b(String str, int i) {
        super(str);
        this.f2184P = i;
    }

    /* renamed from: a */
    public int mo11197a() {
        return this.f2184P;
    }

    public String getMessage() {
        return super.getMessage() + " (error " + (this.f2184P & -16385) + ")";
    }
}
