package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p049d.p052f.C1177m;

/* renamed from: com.crrepa.ble.d.e.c */
public class C1143c {
    /* renamed from: a */
    public static byte[] m2952a(C1177m mVar) {
        if (mVar == null) {
            return null;
        }
        int d = mVar.mo10860d();
        return C1146f.m2959a(18, new byte[]{(byte) mVar.mo10859c(), (byte) d, (byte) mVar.mo10857a(), (byte) mVar.mo10858b()});
    }
}
