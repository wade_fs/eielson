package com.crrepa.ble.p049d.p062p;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1294c;
import java.util.List;
import java.util.UUID;

/* renamed from: com.crrepa.ble.d.p.a */
public class C1255a {

    /* renamed from: a */
    private BluetoothGattCharacteristic f1959a;

    /* renamed from: b */
    private BluetoothGattCharacteristic f1960b;

    /* renamed from: c */
    private BluetoothGattCharacteristic f1961c;

    /* renamed from: d */
    private BluetoothGattCharacteristic f1962d;

    /* renamed from: e */
    private BluetoothGattCharacteristic f1963e;

    /* renamed from: f */
    private BluetoothGattCharacteristic f1964f;

    /* renamed from: g */
    private BluetoothGattCharacteristic f1965g;

    /* renamed from: h */
    private BluetoothGattCharacteristic f1966h;

    /* renamed from: i */
    private BluetoothGattCharacteristic f1967i;

    /* renamed from: j */
    private BluetoothGattCharacteristic f1968j;

    /* renamed from: k */
    private BluetoothGattCharacteristic f1969k;

    /* renamed from: l */
    private BluetoothGattCharacteristic f1970l;

    public C1255a(List<BluetoothGattService> list) {
        for (BluetoothGattService bluetoothGattService : list) {
            UUID uuid = bluetoothGattService.getUuid();
            C1293b.m3504b("uuid: " + uuid.toString());
            if (C1294c.f2001a.equals(uuid)) {
                this.f1959a = bluetoothGattService.getCharacteristic(C1294c.f2005e);
                this.f1960b = bluetoothGattService.getCharacteristic(C1294c.f2006f);
                this.f1961c = bluetoothGattService.getCharacteristic(C1294c.f2007g);
                this.f1966h = bluetoothGattService.getCharacteristic(C1294c.f2013m);
                this.f1967i = bluetoothGattService.getCharacteristic(C1294c.f2014n);
                this.f1968j = bluetoothGattService.getCharacteristic(C1294c.f2015o);
                this.f1969k = bluetoothGattService.getCharacteristic(C1294c.f2016p);
            } else if (C1294c.f2002b.equals(uuid)) {
                this.f1963e = bluetoothGattService.getCharacteristic(C1294c.f2009i);
                this.f1964f = bluetoothGattService.getCharacteristic(C1294c.f2011k);
                this.f1970l = bluetoothGattService.getCharacteristic(C1294c.f2010j);
            } else if (C1294c.f2003c.equals(uuid)) {
                this.f1962d = bluetoothGattService.getCharacteristic(C1294c.f2008h);
            } else if (C1294c.f2004d.equals(uuid)) {
                this.f1965g = bluetoothGattService.getCharacteristic(C1294c.f2012l);
            }
        }
    }

    /* renamed from: a */
    public BluetoothGattCharacteristic mo10985a() {
        return this.f1962d;
    }

    /* renamed from: b */
    public BluetoothGattCharacteristic mo10986b() {
        return this.f1963e;
    }

    /* renamed from: c */
    public BluetoothGattCharacteristic mo10987c() {
        return this.f1964f;
    }

    /* renamed from: d */
    public BluetoothGattCharacteristic mo10988d() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic = this.f1968j;
        return bluetoothGattCharacteristic != null ? bluetoothGattCharacteristic : this.f1969k;
    }

    /* renamed from: e */
    public BluetoothGattCharacteristic mo10989e() {
        return this.f1965g;
    }

    /* renamed from: f */
    public BluetoothGattCharacteristic mo10990f() {
        return this.f1961c;
    }

    /* renamed from: g */
    public BluetoothGattCharacteristic mo10991g() {
        return this.f1970l;
    }

    /* renamed from: h */
    public BluetoothGattCharacteristic mo10992h() {
        return this.f1959a;
    }

    /* renamed from: i */
    public BluetoothGattCharacteristic mo10993i() {
        return this.f1967i;
    }

    /* renamed from: j */
    public BluetoothGattCharacteristic mo10994j() {
        return this.f1966h;
    }

    /* renamed from: k */
    public BluetoothGattCharacteristic mo10995k() {
        return this.f1960b;
    }

    /* renamed from: l */
    public boolean mo10996l() {
        return this.f1969k != null;
    }

    /* renamed from: m */
    public boolean mo10997m() {
        return (this.f1959a == null || this.f1960b == null || this.f1961c == null || this.f1963e == null || this.f1962d == null) ? false : true;
    }
}
