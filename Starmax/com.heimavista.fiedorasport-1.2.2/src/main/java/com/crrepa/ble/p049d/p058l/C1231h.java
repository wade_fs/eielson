package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p051e.C1142b;
import com.crrepa.ble.p049d.p052f.C1167d;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.crrepa.ble.d.l.h */
public class C1231h {
    /* renamed from: a */
    public static List<C1167d> m3311a(byte[] bArr) {
        if (bArr == null || bArr.length != 72) {
            return null;
        }
        int length = bArr.length / 3;
        byte[] bArr2 = new byte[length];
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 3; i++) {
            System.arraycopy(bArr, length * i, bArr2, 0, length);
            C1293b.m3504b("heart rate data: " + C1295d.m3512b(bArr2));
            arrayList.add(m3312b(bArr2));
        }
        return arrayList;
    }

    /* renamed from: b */
    private static C1167d m3312b(byte[] bArr) {
        byte[] bArr2 = new byte[4];
        System.arraycopy(bArr, 0, bArr2, 0, 4);
        long a = C1142b.m2950a(C1295d.m3513c(bArr2) * 1000);
        System.arraycopy(bArr, 4, bArr2, 0, 4);
        long a2 = C1142b.m2950a(C1295d.m3513c(bArr2) * 1000);
        if (a2 - a < DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS) {
            return null;
        }
        byte b = bArr[11];
        System.arraycopy(bArr, 12, bArr2, 0, 4);
        System.arraycopy(bArr, 16, bArr2, 0, 4);
        int c = (int) C1295d.m3513c(bArr2);
        int a3 = C1295d.m3507a(bArr[21], bArr[20]);
        C1167d dVar = new C1167d();
        dVar.mo10806d(b);
        dVar.mo10802b(a);
        dVar.mo10799a(a2);
        dVar.mo10808e(C1295d.m3507a(bArr[9], bArr[8]) / 60);
        dVar.mo10804c((int) C1295d.m3513c(bArr2));
        dVar.mo10801b(c);
        dVar.mo10798a(a3);
        return dVar;
    }
}
