package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.e */
class C1325e extends C1324d {

    /* renamed from: A */
    protected static UUID f2083A = f2085y;

    /* renamed from: B */
    protected static UUID f2084B = f2086z;

    /* renamed from: y */
    protected static final UUID f2085y = C1338o.f2140D;

    /* renamed from: z */
    protected static final UUID f2086z = new UUID(-8157989228746813600L, -6937650605005804976L);

    /* renamed from: x */
    private BluetoothGattCharacteristic f2087x;

    C1325e(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    public void mo11151a(Intent intent) {
        mo11134b("Buttonless service with bond sharing found -> SDK 14 or newer");
        if (!mo11137e()) {
            mo11135c("Device is not paired, cancelling DFU");
            this.f2077n.mo11087a(15, "Device is not bonded");
            this.f2077n.mo11089a(this.f2067d, 4110);
            return;
        }
        super.mo11151a(intent);
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(f2083A);
        if (service == null) {
            return false;
        }
        this.f2087x = service.getCharacteristic(f2084B);
        return this.f2087x != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public BluetoothGattCharacteristic mo11117j() {
        return this.f2087x;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public int mo11152k() {
        return 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public boolean mo11153l() {
        return false;
    }
}
