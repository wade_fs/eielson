package com.crrepa.ble.p049d.p055i;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import com.crrepa.ble.p049d.C1137a;
import com.crrepa.ble.p049d.C1138b;
import com.crrepa.ble.p049d.C1140d;
import com.crrepa.ble.p068f.C1296e;

/* renamed from: com.crrepa.ble.d.i.c */
public class C1211c implements C1138b {

    /* renamed from: a */
    private Context f1875a;

    /* renamed from: b */
    private BluetoothDevice f1876b;

    /* renamed from: c */
    private BluetoothManager f1877c;

    /* renamed from: d */
    private C1140d f1878d = new C1210b(this.f1875a, this.f1876b);

    public C1211c(Context context, BluetoothDevice bluetoothDevice, BluetoothManager bluetoothManager) {
        this.f1875a = context;
        this.f1876b = bluetoothDevice;
        this.f1877c = bluetoothManager;
    }

    /* renamed from: a */
    public void mo10778a() {
        this.f1878d.mo10782a();
    }

    /* renamed from: b */
    public C1137a mo10779b() {
        C1296e.m3522b(this.f1876b.getAddress());
        return this.f1878d.mo10781a(new C1212d());
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: c */
    public boolean mo10780c() {
        return this.f1877c.getConnectionState(this.f1876b, 7) == 2;
    }

    /* renamed from: d */
    public BluetoothDevice mo10897d() {
        return this.f1876b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C1211c)) {
            return false;
        }
        return this.f1876b.equals(((C1211c) obj).mo10897d());
    }

    public int hashCode() {
        return this.f1876b.hashCode();
    }
}
