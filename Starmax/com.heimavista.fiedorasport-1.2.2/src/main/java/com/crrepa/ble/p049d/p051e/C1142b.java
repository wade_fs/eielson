package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p068f.C1295d;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/* renamed from: com.crrepa.ble.d.e.b */
public class C1142b {
    /* renamed from: a */
    public static int m2949a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return -1;
        }
        return bArr[0];
    }

    /* renamed from: a */
    public static long m2950a(long j) {
        TimeZone timeZone = TimeZone.getDefault();
        int rawOffset = timeZone.getRawOffset();
        int dSTSavings = timeZone.getDSTSavings();
        boolean inDaylightTime = timeZone.inDaylightTime(new Date(j));
        long rawOffset2 = j - ((long) (rawOffset - TimeZone.getTimeZone("GMT+8").getRawOffset()));
        return inDaylightTime ? rawOffset2 - ((long) dSTSavings) : rawOffset2;
    }

    /* renamed from: a */
    public static byte[] m2951a() {
        Date date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        try {
            date = simpleDateFormat.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        if (date == null) {
            return null;
        }
        byte[] bArr = new byte[5];
        System.arraycopy(C1295d.m3511a(date.getTime() / 1000), 0, bArr, 0, 4);
        bArr[4] = 8;
        return C1146f.m2959a(49, bArr);
    }
}
