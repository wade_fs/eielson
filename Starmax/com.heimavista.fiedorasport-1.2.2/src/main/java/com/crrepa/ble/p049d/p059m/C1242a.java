package com.crrepa.ble.p049d.p059m;

import android.bluetooth.BluetoothGatt;
import androidx.annotation.NonNull;

/* renamed from: com.crrepa.ble.d.m.a */
public class C1242a {

    /* renamed from: a */
    private BluetoothGatt f1942a;

    /* renamed from: b */
    private int f1943b;

    /* renamed from: com.crrepa.ble.d.m.a$b */
    private static class C1244b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C1242a f1944a = new C1242a();
    }

    private C1242a() {
        this.f1943b = 20;
    }

    /* renamed from: c */
    public static C1242a m3332c() {
        return C1244b.f1944a;
    }

    /* renamed from: a */
    public BluetoothGatt mo10961a() {
        return this.f1942a;
    }

    /* renamed from: a */
    public void mo10962a(int i) {
        int i2 = i - 3;
        this.f1943b = i2 - (i2 % 4);
    }

    /* renamed from: a */
    public void mo10963a(@NonNull BluetoothGatt bluetoothGatt) {
        this.f1942a = bluetoothGatt;
    }

    /* renamed from: b */
    public int mo10964b() {
        return this.f1943b;
    }
}
