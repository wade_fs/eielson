package com.crrepa.ble.p049d.p055i;

import android.graphics.Bitmap;
import com.crrepa.ble.p049d.C1137a;
import com.crrepa.ble.p049d.p051e.C1142b;
import com.crrepa.ble.p049d.p051e.C1143c;
import com.crrepa.ble.p049d.p051e.C1144d;
import com.crrepa.ble.p049d.p051e.C1145e;
import com.crrepa.ble.p049d.p051e.C1147g;
import com.crrepa.ble.p049d.p051e.C1148h;
import com.crrepa.ble.p049d.p051e.C1150j;
import com.crrepa.ble.p049d.p051e.C1152l;
import com.crrepa.ble.p049d.p051e.C1153m;
import com.crrepa.ble.p049d.p051e.C1154n;
import com.crrepa.ble.p049d.p051e.C1157q;
import com.crrepa.ble.p049d.p051e.C1158r;
import com.crrepa.ble.p049d.p051e.C1159s;
import com.crrepa.ble.p049d.p051e.C1160t;
import com.crrepa.ble.p049d.p051e.C1161u;
import com.crrepa.ble.p049d.p051e.C1162v;
import com.crrepa.ble.p049d.p052f.C1163a;
import com.crrepa.ble.p049d.p052f.C1168e;
import com.crrepa.ble.p049d.p052f.C1170g;
import com.crrepa.ble.p049d.p052f.C1177m;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p049d.p054h.C1180a;
import com.crrepa.ble.p049d.p054h.C1181b;
import com.crrepa.ble.p049d.p054h.C1185f;
import com.crrepa.ble.p049d.p054h.C1189j;
import com.crrepa.ble.p049d.p054h.C1190k;
import com.crrepa.ble.p049d.p054h.C1193n;
import com.crrepa.ble.p049d.p054h.C1195p;
import com.crrepa.ble.p049d.p054h.C1197r;
import com.crrepa.ble.p049d.p054h.C1200u;
import com.crrepa.ble.p049d.p054h.C1201v;
import com.crrepa.ble.p049d.p054h.C1202w;
import com.crrepa.ble.p049d.p056j.C1213a;
import com.crrepa.ble.p049d.p057k.C1215a;
import com.crrepa.ble.p049d.p057k.C1216b;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p049d.p061o.C1252c;
import com.crrepa.ble.p049d.p063q.C1259a;
import com.crrepa.ble.p049d.p063q.C1260b;
import com.crrepa.ble.p049d.p063q.C1263e;
import com.crrepa.ble.p049d.p063q.C1264f;
import com.crrepa.ble.p049d.p063q.C1267i;
import com.crrepa.ble.p049d.p063q.C1268j;
import com.crrepa.ble.p049d.p063q.C1271m;
import com.crrepa.ble.p049d.p063q.C1272n;
import com.crrepa.ble.p049d.p063q.C1273o;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.crrepa.ble.p064e.p065a.p066a.C1282b;

/* renamed from: com.crrepa.ble.d.i.a */
public class C1209a implements C1137a {

    /* renamed from: a */
    private C1212d f1869a;

    /* renamed from: b */
    private C1217c f1870b = C1217c.m3269e();

    /* renamed from: c */
    private C1216b f1871c;

    /* renamed from: d */
    private C1215a f1872d;

    public C1209a(C1212d dVar) {
        this.f1869a = dVar;
        this.f1871c = this.f1869a.mo10903b();
        this.f1872d = this.f1869a.mo10900a();
        C1252c.m3359d().mo10979a(this.f1871c);
    }

    /* renamed from: a */
    private void m3117a(byte[] bArr) {
        this.f1870b.mo10937a(bArr);
    }

    /* renamed from: c */
    private void m3118c(boolean z) {
        m3117a(C1153m.m2980b(z));
    }

    /* renamed from: a */
    public void mo10730a() {
        m3117a(C1142b.m2951a());
    }

    /* renamed from: a */
    public void mo10731a(byte b) {
        m3117a(C1162v.m2997a(b));
    }

    /* renamed from: a */
    public void mo10732a(int i) {
        if (i > 0) {
            m3117a(C1153m.m2976a(i));
        }
    }

    /* renamed from: a */
    public void mo10733a(Bitmap bitmap, boolean z, C1275q qVar) {
        C1282b c = C1282b.m3446c();
        c.mo11049a(qVar);
        c.mo11048a(bitmap, z);
    }

    /* renamed from: a */
    public void mo10734a(C1163a aVar) {
        if (aVar != null) {
            m3117a(C1147g.m2963a(aVar));
        }
    }

    /* renamed from: a */
    public void mo10735a(C1168e eVar) {
        if (eVar != null) {
            m3117a(C1158r.m2990a(eVar));
        }
    }

    /* renamed from: a */
    public void mo10736a(C1170g gVar) {
        m3117a(C1159s.m2991a(gVar));
    }

    /* renamed from: a */
    public void mo10737a(C1177m mVar) {
        m3117a(C1143c.m2952a(mVar));
    }

    /* renamed from: a */
    public void mo10738a(C1178n nVar) {
        if (nVar != null) {
            m3117a(C1145e.m2957a(nVar));
        }
    }

    /* renamed from: a */
    public void mo10739a(C1180a aVar) {
        this.f1872d.mo10913a(aVar);
        m3117a(C1147g.m2962a());
    }

    /* renamed from: a */
    public void mo10740a(C1181b bVar) {
        this.f1871c.mo10933a(bVar);
    }

    /* renamed from: a */
    public void mo10741a(C1185f fVar) {
        this.f1872d.mo10914a(fVar);
        m3117a(C1144d.m2954a());
    }

    /* renamed from: a */
    public void mo10742a(C1189j jVar) {
        this.f1872d.mo10915a(jVar);
        m3117a(C1152l.m2972a());
    }

    /* renamed from: a */
    public void mo10743a(C1190k kVar) {
        this.f1872d.mo10916a(kVar);
        m3117a(C1154n.m2982a());
    }

    /* renamed from: a */
    public void mo10744a(C1193n nVar) {
        this.f1872d.mo10917a(nVar);
        m3117a(C1158r.m2989a());
    }

    /* renamed from: a */
    public void mo10745a(C1195p pVar) {
        this.f1872d.mo10918a(pVar);
        m3117a(C1157q.m2987a());
    }

    /* renamed from: a */
    public void mo10746a(C1197r rVar) {
        this.f1872d.mo10919a(rVar);
        m3117a(C1160t.m2993a());
    }

    /* renamed from: a */
    public void mo10747a(C1200u uVar) {
        this.f1872d.mo10920a(uVar);
        m3117a(C1153m.m2981c());
    }

    /* renamed from: a */
    public void mo10748a(C1201v vVar) {
        this.f1872d.mo10921a(vVar);
        m3117a(C1148h.m2965a());
    }

    /* renamed from: a */
    public void mo10749a(C1202w wVar) {
        this.f1872d.mo10922a(wVar);
        m3117a(C1145e.m2956a());
    }

    /* renamed from: a */
    public void mo10750a(C1259a aVar) {
        this.f1869a.mo10902a(aVar);
    }

    /* renamed from: a */
    public void mo10751a(C1260b bVar) {
        this.f1872d.mo10923a(bVar);
    }

    /* renamed from: a */
    public void mo10752a(C1263e eVar) {
        this.f1872d.mo10924a(eVar);
    }

    /* renamed from: a */
    public void mo10753a(C1264f fVar) {
        this.f1872d.mo10925a(fVar);
    }

    /* renamed from: a */
    public void mo10754a(C1267i iVar) {
        this.f1872d.mo10926a(iVar);
    }

    /* renamed from: a */
    public void mo10755a(C1268j jVar) {
        this.f1872d.mo10927a(jVar);
    }

    /* renamed from: a */
    public void mo10756a(C1271m mVar) {
        this.f1872d.mo10928a(mVar);
    }

    /* renamed from: a */
    public void mo10757a(C1272n nVar) {
        this.f1872d.mo10929a(nVar);
        this.f1871c.mo10935a(nVar);
    }

    /* renamed from: a */
    public void mo10758a(C1273o oVar) {
        this.f1872d.mo10930a(oVar);
    }

    /* renamed from: a */
    public void mo10759a(String str, int i, int i2) {
        m3117a(C1213a.m3184a(str, i, i2));
    }

    /* renamed from: a */
    public void mo10760a(boolean z) {
        m3117a(C1160t.m2994a(z));
    }

    /* renamed from: b */
    public void mo10761b() {
        m3117a(C1153m.m2979b((byte) 4));
    }

    /* renamed from: b */
    public void mo10762b(byte b) {
        m3117a(C1148h.m2966a(b));
    }

    /* renamed from: b */
    public void mo10763b(int i) {
        m3117a(C1152l.m2973a(i));
    }

    /* renamed from: b */
    public void mo10764b(C1168e eVar) {
        if (eVar != null) {
            m3117a(C1150j.m2969a(eVar));
        }
    }

    /* renamed from: b */
    public void mo10765b(C1193n nVar) {
        this.f1872d.mo10917a(nVar);
        m3117a(C1150j.m2968a());
    }

    /* renamed from: b */
    public void mo10766b(boolean z) {
        m3117a(C1157q.m2988a(z));
    }

    /* renamed from: c */
    public void mo10767c() {
        m3117a(C1153m.m2978b());
    }

    /* renamed from: c */
    public void mo10768c(byte b) {
        m3117a(C1161u.m2996a(b));
    }

    /* renamed from: c */
    public void mo10769c(int i) {
        m3117a(i == 1 ? C1153m.m2979b((byte) 0) : i == 2 ? C1153m.m2975a((byte) 0) : null);
    }

    /* renamed from: d */
    public void mo10770d() {
        m3117a(C1161u.m2995a());
    }

    /* renamed from: d */
    public void mo10771d(byte b) {
        m3117a(C1154n.m2983a(b));
    }

    /* renamed from: e */
    public void mo10772e() {
        m3118c(true);
    }

    /* renamed from: e */
    public void mo10773e(byte b) {
        m3117a(C1144d.m2955a(b));
    }

    /* renamed from: f */
    public void mo10774f() {
        this.f1871c.mo10936c();
    }

    /* renamed from: g */
    public void mo10775g() {
        m3117a(C1153m.m2977a(false));
    }

    /* renamed from: h */
    public void mo10776h() {
        m3117a(C1153m.m2976a(-1));
    }

    /* renamed from: i */
    public void mo10777i() {
        m3117a(C1213a.m3183a());
    }
}
