package com.crrepa.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.crrepa.ble.p045b.p046a.C1134a;
import com.crrepa.ble.p048c.C1136a;
import com.crrepa.ble.p049d.C1138b;
import com.crrepa.ble.p049d.p055i.C1211c;
import com.crrepa.ble.p068f.C1296e;
import com.crrepa.ble.p069g.C1302a;
import com.crrepa.ble.p072i.p074b.C1307a;

/* renamed from: com.crrepa.ble.a */
public class C1133a {

    /* renamed from: d */
    private static C1133a f1797d;

    /* renamed from: a */
    private C1136a f1798a;

    /* renamed from: b */
    private BluetoothManager f1799b;

    /* renamed from: c */
    private C1302a f1800c;

    private C1133a(Context context) {
        C1296e.m3519a(context);
        this.f1799b = (BluetoothManager) context.getSystemService("bluetooth");
        BluetoothAdapter adapter = this.f1799b.getAdapter();
        this.f1800c = new C1302a(adapter);
        this.f1798a = new C1136a(adapter);
    }

    /* renamed from: a */
    public static C1133a m2876a(@NonNull Context context) {
        if (f1797d == null) {
            synchronized (C1133a.class) {
                if (f1797d == null) {
                    if (context != null) {
                        f1797d = new C1133a(context.getApplicationContext());
                    } else {
                        throw new IllegalArgumentException("the provided context must not be null!");
                    }
                }
            }
        }
        return f1797d;
    }

    /* renamed from: a */
    public C1138b mo10718a(String str) {
        BluetoothDevice a;
        if (TextUtils.isEmpty(str) || (a = this.f1800c.mo11072a(str)) == null) {
            return null;
        }
        C1134a.m2881a(str);
        return new C1211c(C1296e.m3518a(), a, this.f1799b);
    }

    /* renamed from: a */
    public void mo10719a() {
        this.f1798a.mo10723a();
    }

    /* renamed from: a */
    public boolean mo10720a(C1307a aVar, long j) {
        return this.f1798a.mo10727a(aVar, j);
    }
}
