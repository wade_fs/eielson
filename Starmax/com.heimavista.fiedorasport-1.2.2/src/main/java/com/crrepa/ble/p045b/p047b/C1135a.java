package com.crrepa.ble.p045b.p047b;

import android.content.Context;
import android.content.SharedPreferences;
import com.crrepa.ble.p068f.C1296e;

/* renamed from: com.crrepa.ble.b.b.a */
public class C1135a {

    /* renamed from: c */
    private static C1135a f1801c;

    /* renamed from: a */
    private SharedPreferences f1802a;

    /* renamed from: b */
    private SharedPreferences.Editor f1803b = this.f1802a.edit();

    private C1135a(Context context) {
        this.f1802a = context.getSharedPreferences("crp", 0);
    }

    /* renamed from: a */
    public static C1135a m2883a() {
        if (f1801c == null) {
            m2884a(C1296e.m3518a());
        }
        return f1801c;
    }

    /* renamed from: a */
    private static void m2884a(Context context) {
        if (f1801c == null) {
            synchronized (C1135a.class) {
                if (f1801c == null) {
                    f1801c = new C1135a(context);
                }
            }
        }
    }

    /* renamed from: a */
    public String mo10721a(String str, String str2) {
        return this.f1802a.getString(str, str2);
    }

    /* renamed from: b */
    public void mo10722b(String str, String str2) {
        this.f1803b.putString(str, str2);
        this.f1803b.commit();
    }
}
