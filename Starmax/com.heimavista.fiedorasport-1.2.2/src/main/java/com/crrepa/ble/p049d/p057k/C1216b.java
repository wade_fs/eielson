package com.crrepa.ble.p049d.p057k;

import android.bluetooth.BluetoothGattCharacteristic;
import androidx.annotation.Nullable;
import com.crrepa.ble.p049d.p051e.C1149i;
import com.crrepa.ble.p049d.p054h.C1181b;
import com.crrepa.ble.p049d.p054h.C1184e;
import com.crrepa.ble.p049d.p054h.C1187h;
import com.crrepa.ble.p049d.p054h.C1204y;
import com.crrepa.ble.p049d.p058l.C1234k;
import com.crrepa.ble.p049d.p058l.C1238o;
import com.crrepa.ble.p049d.p059m.C1242a;
import com.crrepa.ble.p049d.p061o.C1250a;
import com.crrepa.ble.p049d.p061o.C1252c;
import com.crrepa.ble.p049d.p062p.C1255a;
import com.crrepa.ble.p049d.p063q.C1272n;
import com.crrepa.ble.p068f.C1294c;
import com.crrepa.ble.p068f.C1296e;
import java.util.UUID;

/* renamed from: com.crrepa.ble.d.k.b */
public class C1216b extends C1223f {

    /* renamed from: a */
    private C1187h f1924a;

    /* renamed from: b */
    private C1181b f1925b;

    /* renamed from: c */
    private C1272n f1926c;

    /* renamed from: d */
    private C1184e f1927d;

    /* renamed from: e */
    private C1204y f1928e;

    /* renamed from: a */
    private void m3250a(byte b) {
        C1252c.m3359d().mo10980a(new C1250a(3, new byte[]{b}));
    }

    /* renamed from: a */
    private void m3251a(C1204y.C1205a aVar) {
        C1204y yVar = this.f1928e;
        if (yVar != null) {
            yVar.mo10895a(aVar);
        }
    }

    /* renamed from: a */
    private void m3252a(byte[] bArr) {
        if (this.f1924a != null) {
            String str = new String(bArr);
            C1296e.m3520a(str);
            this.f1924a.mo10878a(str);
        }
    }

    /* renamed from: b */
    private void m3253b(byte b) {
        C1181b bVar = this.f1925b;
        if (bVar != null) {
            bVar.mo10872a(b);
        }
    }

    /* renamed from: b */
    private void m3254b(int i) {
        C1184e eVar = this.f1927d;
        if (eVar != null) {
            eVar.mo10875a(i);
        }
    }

    /* renamed from: b */
    private void m3255b(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if (bluetoothGattCharacteristic == null) {
            C1252c.m3359d().mo10983b();
        } else if (!C1242a.m3332c().mo10961a().readCharacteristic(bluetoothGattCharacteristic)) {
            mo10952a();
        }
    }

    /* renamed from: b */
    private void m3256b(byte[] bArr) {
        m3254b(C1149i.m2967a(bArr));
    }

    /* renamed from: c */
    private void m3257c(byte[] bArr) {
        m3251a(C1234k.m3315a(bArr));
    }

    @Nullable
    /* renamed from: d */
    private C1255a m3258d() {
        C1255a b = mo10953b();
        if (b == null) {
            return null;
        }
        return b;
    }

    /* renamed from: d */
    private void m3259d(byte[] bArr) {
        C1238o.m3323a(bArr, this.f1926c);
    }

    /* renamed from: a */
    public void mo10931a(int i) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic;
        C1255a d = m3258d();
        if (d != null) {
            switch (i) {
                case 16:
                    bluetoothGattCharacteristic = d.mo10992h();
                    m3255b(bluetoothGattCharacteristic);
                    return;
                case 17:
                    bluetoothGattCharacteristic = d.mo10986b();
                    m3255b(bluetoothGattCharacteristic);
                    return;
                case 18:
                    bluetoothGattCharacteristic = d.mo10985a();
                    m3255b(bluetoothGattCharacteristic);
                    return;
                case 19:
                    bluetoothGattCharacteristic = d.mo10987c();
                    if (bluetoothGattCharacteristic == null) {
                        m3254b(0);
                    }
                    m3255b(bluetoothGattCharacteristic);
                    return;
                case 20:
                    bluetoothGattCharacteristic = d.mo10991g();
                    m3255b(bluetoothGattCharacteristic);
                    return;
            }
        }
        C1252c.m3359d().mo10983b();
    }

    /* renamed from: a */
    public void mo10932a(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value != null && value.length > 0) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            if (C1294c.f2009i.equals(uuid)) {
                m3252a(value);
            } else if (C1294c.f2008h.equals(uuid)) {
                m3253b(value[0]);
            } else if (C1294c.f2005e.equals(uuid)) {
                m3259d(value);
            } else if (C1294c.f2011k.equals(uuid)) {
                m3256b(value);
            } else if (C1294c.f2010j.equals(uuid)) {
                m3257c(value);
            }
            C1252c.m3359d().mo10983b();
        }
    }

    /* renamed from: a */
    public void mo10933a(C1181b bVar) {
        this.f1925b = bVar;
        m3250a((byte) 18);
    }

    /* renamed from: a */
    public void mo10934a(C1204y yVar) {
        this.f1928e = yVar;
        m3250a((byte) 20);
    }

    /* renamed from: a */
    public void mo10935a(C1272n nVar) {
        this.f1926c = nVar;
    }

    /* renamed from: c */
    public void mo10936c() {
        m3250a((byte) 16);
    }
}
