package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.d.l.q */
public class C1240q {
    /* renamed from: a */
    public static C1178n m3330a(byte[] bArr) {
        C1178n nVar = new C1178n();
        nVar.mo10867c(bArr[0]);
        nVar.mo10869d(bArr[1]);
        nVar.mo10865b(bArr[2]);
        nVar.mo10862a(C1295d.m3507a(bArr[3], bArr[4]));
        byte[] bArr2 = new byte[32];
        System.arraycopy(bArr, 5, bArr2, 0, bArr2.length);
        nVar.mo10863a(C1295d.m3515e(bArr2));
        return nVar;
    }
}
