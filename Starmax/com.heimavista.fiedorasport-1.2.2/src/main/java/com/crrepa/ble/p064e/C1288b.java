package com.crrepa.ble.p064e;

import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.e.b */
public class C1288b {
    /* renamed from: a */
    public static byte[] m3470a(byte[] bArr, int i) {
        for (byte b : bArr) {
            byte b2 = (((i & 255) << 8) | ((65280 & i) >> 8)) ^ (b & 255);
            byte b3 = b2 ^ ((b2 & 255) >> 4);
            byte b4 = b3 ^ (((b3 & 255) << 8) << 4);
            i = b4 ^ (((b4 & 255) << 4) << 1);
        }
        return C1295d.m3510a(i);
    }
}
