package com.crrepa.ble.p072i.p074b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;
import com.crrepa.ble.p048c.C1136a;
import com.crrepa.ble.p072i.p073a.C1306a;

/* renamed from: com.crrepa.ble.i.b.c */
public class C1309c implements BluetoothAdapter.LeScanCallback {

    /* renamed from: c */
    private static Handler f2025c = new Handler(Looper.getMainLooper());

    /* renamed from: a */
    private long f2026a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C1136a f2027b;

    /* renamed from: com.crrepa.ble.i.b.c$a */
    class C1310a implements Runnable {
        C1310a() {
        }

        public void run() {
            C1309c.this.f2027b.mo10725a(C1309c.this);
            C1309c.this.f2027b.mo10729c();
        }
    }

    public C1309c(long j) {
        this.f2026a = j;
    }

    /* renamed from: a */
    public C1309c mo11078a(C1136a aVar) {
        this.f2027b = aVar;
        return this;
    }

    /* renamed from: a */
    public void mo11079a() {
        f2025c.removeCallbacksAndMessages(null);
    }

    /* renamed from: b */
    public void mo11080b() {
        this.f2027b.mo10725a(this);
        this.f2027b.mo10728b();
    }

    /* renamed from: c */
    public void mo11081c() {
        if (this.f2026a > 0) {
            mo11079a();
            f2025c.postDelayed(new C1310a(), this.f2026a);
        }
    }

    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        if (bluetoothDevice != null) {
            this.f2027b.mo10724a(new C1306a(bluetoothDevice, i, bArr));
        }
    }
}
