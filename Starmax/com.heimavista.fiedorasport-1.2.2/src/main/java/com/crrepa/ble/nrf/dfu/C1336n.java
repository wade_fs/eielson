package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import com.crrepa.ble.nrf.dfu.C1320b;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1352g;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.n */
class C1336n extends C1320b {

    /* renamed from: E */
    protected static final UUID f2118E = new UUID(23296205844446L, 1523193452336828707L);

    /* renamed from: F */
    protected static final UUID f2119F = new UUID(23300500811742L, 1523193452336828707L);

    /* renamed from: G */
    protected static final UUID f2120G = new UUID(23304795779038L, 1523193452336828707L);

    /* renamed from: H */
    protected static final UUID f2121H = new UUID(23313385713630L, 1523193452336828707L);

    /* renamed from: I */
    protected static UUID f2122I = f2118E;

    /* renamed from: J */
    protected static UUID f2123J = f2119F;

    /* renamed from: K */
    protected static UUID f2124K = f2120G;

    /* renamed from: L */
    protected static UUID f2125L = f2121H;

    /* renamed from: M */
    private static final byte[] f2126M = {1, 0};

    /* renamed from: N */
    private static final byte[] f2127N = {2};

    /* renamed from: O */
    private static final byte[] f2128O = {2, 0};

    /* renamed from: P */
    private static final byte[] f2129P = {2, 1};

    /* renamed from: Q */
    private static final byte[] f2130Q = {3};

    /* renamed from: R */
    private static final byte[] f2131R = {4};

    /* renamed from: S */
    private static final byte[] f2132S = {5};

    /* renamed from: T */
    private static final byte[] f2133T = {6};

    /* renamed from: U */
    private static final byte[] f2134U = {8, 0, 0};

    /* renamed from: A */
    private BluetoothGattCharacteristic f2135A;

    /* renamed from: B */
    private BluetoothGattCharacteristic f2136B;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public boolean f2137C;

    /* renamed from: D */
    private final C1337a f2138D = new C1337a();

    /* renamed from: com.crrepa.ble.nrf.dfu.n$a */
    protected class C1337a extends C1320b.C1321a {
        protected C1337a() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.n.a(com.crrepa.ble.nrf.dfu.n, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.n, int]
         candidates:
          com.crrepa.ble.nrf.dfu.n.a(byte[], int):int
          com.crrepa.ble.nrf.dfu.n.a(android.bluetooth.BluetoothGatt, android.content.Intent):void
          com.crrepa.ble.nrf.dfu.n.a(android.bluetooth.BluetoothGattCharacteristic, byte[]):void
          com.crrepa.ble.nrf.dfu.n.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
          com.crrepa.ble.nrf.dfu.b.a(com.crrepa.ble.nrf.dfu.b, boolean):boolean
          com.crrepa.ble.nrf.dfu.b.a(android.bluetooth.BluetoothGattCharacteristic, java.util.zip.CRC32):void
          com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, int):void
          com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void
          com.crrepa.ble.nrf.dfu.c.a(java.lang.String, java.lang.Throwable):void
          com.crrepa.ble.nrf.dfu.j.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
          com.crrepa.ble.nrf.dfu.n.a(com.crrepa.ble.nrf.dfu.n, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo11120a(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            if (C1336n.this.f2137C) {
                DfuBaseService dfuBaseService = C1336n.this.f2077n;
                dfuBaseService.mo11087a(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
                boolean unused = C1336n.this.f2137C = false;
            }
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (bluetoothGattCharacteristic.getIntValue(17, 0).intValue() == 17) {
                C1336n.this.f2078o.mo11159b(bluetoothGattCharacteristic.getIntValue(20, 1).intValue());
                mo11121b(bluetoothGatt, bluetoothGattCharacteristic);
            } else if (!C1336n.this.f2058z) {
                if (bluetoothGattCharacteristic.getIntValue(17, 2).intValue() != 1) {
                    C1336n.this.f2058z = true;
                }
                mo11119a(bluetoothGatt, bluetoothGattCharacteristic);
            }
            C1336n.this.mo11138f();
        }
    }

    C1336n(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    private int m3690a(byte[] bArr, int i) {
        if (bArr != null && bArr.length == 3 && bArr[0] == 16 && bArr[1] == i && bArr[2] >= 1 && bArr[2] <= 6) {
            return bArr[2];
        }
        throw new C1352g("Invalid response received", bArr, 16, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.crrepa.ble.nrf.dfu.n.a(byte[], int):int
      com.crrepa.ble.nrf.dfu.n.a(android.bluetooth.BluetoothGatt, android.content.Intent):void
      com.crrepa.ble.nrf.dfu.n.a(android.bluetooth.BluetoothGattCharacteristic, byte[]):void
      com.crrepa.ble.nrf.dfu.n.a(com.crrepa.ble.nrf.dfu.n, boolean):boolean
      com.crrepa.ble.nrf.dfu.n.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
      com.crrepa.ble.nrf.dfu.b.a(com.crrepa.ble.nrf.dfu.b, boolean):boolean
      com.crrepa.ble.nrf.dfu.b.a(android.bluetooth.BluetoothGattCharacteristic, java.util.zip.CRC32):void
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, int):void
      com.crrepa.ble.nrf.dfu.c.a(java.lang.String, java.lang.Throwable):void
      com.crrepa.ble.nrf.dfu.j.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
      com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void */
    /* renamed from: a */
    private void m3691a(BluetoothGatt bluetoothGatt, Intent intent) {
        this.f2077n.mo11087a(15, "Last upload interrupted. Restarting device...");
        this.f2078o.mo11165e(-5);
        mo11134b("Sending Reset command (Op Code = 6)");
        m3693a(this.f2135A, f2133T);
        this.f2077n.mo11087a(10, "Reset request sent");
        this.f2077n.mo11094d();
        this.f2077n.mo11087a(5, "Disconnected by the remote device");
        BluetoothGattService service = bluetoothGatt.getService(C1322c.f2060r);
        this.f2077n.mo11090a(bluetoothGatt, !((service == null || service.getCharacteristic(C1322c.f2061s) == null) ? false : true));
        this.f2077n.mo11088a(bluetoothGatt);
        mo11134b("Restarting the service");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        mo11128a(intent2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3692a(android.bluetooth.BluetoothGattCharacteristic r4, int r5, int r6, int r7) {
        /*
            r3 = this;
            r0 = 0
            r3.f2075l = r0
            r0 = 0
            r3.f2074k = r0
            r1 = 1
            r3.f2137C = r1
            r4.setWriteType(r1)
            r2 = 12
            byte[] r2 = new byte[r2]
            r4.setValue(r2)
            r2 = 20
            r4.setValue(r5, r2, r0)
            r5 = 4
            r4.setValue(r6, r2, r5)
            r5 = 8
            r4.setValue(r7, r2, r5)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r3.f2077n
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Writing to characteristic "
            r6.append(r7)
            java.util.UUID r7 = r4.getUuid()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.mo11087a(r1, r6)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r3.f2077n
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "gatt.writeCharacteristic("
            r6.append(r7)
            java.util.UUID r7 = r4.getUuid()
            r6.append(r7)
            java.lang.String r7 = ")"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.mo11087a(r0, r6)
            android.bluetooth.BluetoothGatt r5 = r3.f2067d
            r5.writeCharacteristic(r4)
            java.lang.Object r4 = r3.f2064a     // Catch:{ InterruptedException -> 0x0081 }
            monitor-enter(r4)     // Catch:{ InterruptedException -> 0x0081 }
        L_0x0062:
            boolean r5 = r3.f2137C     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0072
            boolean r5 = r3.f2071h     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0072
            int r5 = r3.f2074k     // Catch:{ all -> 0x007e }
            if (r5 != 0) goto L_0x0072
            boolean r5 = r3.f2070g     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0076
        L_0x0072:
            boolean r5 = r3.f2069f     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x007c
        L_0x0076:
            java.lang.Object r5 = r3.f2064a     // Catch:{ all -> 0x007e }
            r5.wait()     // Catch:{ all -> 0x007e }
            goto L_0x0062
        L_0x007c:
            monitor-exit(r4)     // Catch:{ all -> 0x007e }
            goto L_0x0087
        L_0x007e:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x007e }
            throw r5     // Catch:{ InterruptedException -> 0x0081 }
        L_0x0081:
            r4 = move-exception
            java.lang.String r5 = "Sleeping interrupted"
            r3.mo11130a(r5, r4)
        L_0x0087:
            boolean r4 = r3.f2070g
            if (r4 != 0) goto L_0x00a4
            int r4 = r3.f2074k
            if (r4 != 0) goto L_0x009c
            boolean r4 = r3.f2071h
            if (r4 == 0) goto L_0x0094
            return
        L_0x0094:
            com.crrepa.ble.nrf.dfu.q.c.a r4 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r5 = "Unable to write Image Sizes: device disconnected"
            r4.<init>(r5)
            throw r4
        L_0x009c:
            com.crrepa.ble.nrf.dfu.q.c.b r5 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r6 = "Unable to write Image Sizes"
            r5.<init>(r6, r4)
            throw r5
        L_0x00a4:
            com.crrepa.ble.nrf.dfu.q.c.h r4 = new com.crrepa.ble.nrf.dfu.q.c.h
            r4.<init>()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1336n.m3692a(android.bluetooth.BluetoothGattCharacteristic, int, int, int):void");
    }

    /* renamed from: a */
    private void m3693a(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        boolean z = false;
        if (bArr[0] == 6 || bArr[0] == 5) {
            z = true;
        }
        mo11127a(bluetoothGattCharacteristic, bArr, z);
    }

    /* renamed from: b */
    private int m3696b(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if (bluetoothGattCharacteristic != null) {
            return bluetoothGattCharacteristic.getIntValue(18, 0).intValue();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3697b(android.bluetooth.BluetoothGattCharacteristic r5, int r6) {
        /*
            r4 = this;
            r0 = 0
            r4.f2075l = r0
            r0 = 0
            r4.f2074k = r0
            r1 = 1
            r4.f2137C = r1
            r5.setWriteType(r1)
            r2 = 4
            byte[] r2 = new byte[r2]
            r5.setValue(r2)
            r2 = 20
            r5.setValue(r6, r2, r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r6 = r4.f2077n
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Writing to characteristic "
            r2.append(r3)
            java.util.UUID r3 = r5.getUuid()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r6.mo11087a(r1, r2)
            com.crrepa.ble.nrf.dfu.DfuBaseService r6 = r4.f2077n
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "gatt.writeCharacteristic("
            r1.append(r2)
            java.util.UUID r2 = r5.getUuid()
            r1.append(r2)
            java.lang.String r2 = ")"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r6.mo11087a(r0, r1)
            android.bluetooth.BluetoothGatt r6 = r4.f2067d
            r6.writeCharacteristic(r5)
            java.lang.Object r5 = r4.f2064a     // Catch:{ InterruptedException -> 0x0077 }
            monitor-enter(r5)     // Catch:{ InterruptedException -> 0x0077 }
        L_0x0058:
            boolean r6 = r4.f2137C     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0068
            boolean r6 = r4.f2071h     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0068
            int r6 = r4.f2074k     // Catch:{ all -> 0x0074 }
            if (r6 != 0) goto L_0x0068
            boolean r6 = r4.f2070g     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x006c
        L_0x0068:
            boolean r6 = r4.f2069f     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0072
        L_0x006c:
            java.lang.Object r6 = r4.f2064a     // Catch:{ all -> 0x0074 }
            r6.wait()     // Catch:{ all -> 0x0074 }
            goto L_0x0058
        L_0x0072:
            monitor-exit(r5)     // Catch:{ all -> 0x0074 }
            goto L_0x007d
        L_0x0074:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0074 }
            throw r6     // Catch:{ InterruptedException -> 0x0077 }
        L_0x0077:
            r5 = move-exception
            java.lang.String r6 = "Sleeping interrupted"
            r4.mo11130a(r6, r5)
        L_0x007d:
            boolean r5 = r4.f2070g
            if (r5 != 0) goto L_0x009a
            int r5 = r4.f2074k
            if (r5 != 0) goto L_0x0092
            boolean r5 = r4.f2071h
            if (r5 == 0) goto L_0x008a
            return
        L_0x008a:
            com.crrepa.ble.nrf.dfu.q.c.a r5 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r6 = "Unable to write Image Size: device disconnected"
            r5.<init>(r6)
            throw r5
        L_0x0092:
            com.crrepa.ble.nrf.dfu.q.c.b r6 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r0 = "Unable to write Image Size"
            r6.<init>(r0, r5)
            throw r6
        L_0x009a:
            com.crrepa.ble.nrf.dfu.q.c.h r5 = new com.crrepa.ble.nrf.dfu.q.c.h
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1336n.m3697b(android.bluetooth.BluetoothGattCharacteristic, int):void");
    }

    /* renamed from: b */
    private void m3698b(byte[] bArr, int i) {
        bArr[1] = (byte) (i & 255);
        bArr[2] = (byte) ((i >> 8) & 255);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        r1.f2077n.mo11087a(1, "Sending only SD/BL");
        r4 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r4.append("Resending Start DFU command (Op Code = 1, Upload Mode = ");
        r4.append(r14);
        r4.append(")");
        mo11134b(r4.toString());
        m3693a(r1.f2135A, com.crrepa.ble.nrf.dfu.C1336n.f2126M);
        r4 = r1.f2077n;
        r5 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        r5.append("DFU Start sent (Op Code = 1, Upload Mode = ");
        r5.append(r14);
        r5.append(")");
        r4.mo11087a(10, r5.toString());
        r4 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        r4.append("Sending image size array to DFU Packet: [");
        r4.append(r15);
        r4.append("b, ");
        r4.append(r13);
        r4.append("b, ");
        r4.append(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        r4.append("b]");
        mo11134b(r4.toString());
        m3692a(r1.f2136B, r15, r13, 0);
        r4 = r1.f2077n;
        r5 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:?, code lost:
        r5.append("Firmware image size sent [");
        r5.append(r15);
        r5.append("b, ");
        r5.append(r13);
        r5.append("b, ");
        r5.append(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:?, code lost:
        r5.append("b]");
        r4.mo11087a(10, r5.toString());
        r4 = mo11139g();
        r8 = m3690a(r4, 1);
        r5 = r1.f2077n;
        r11 = new java.lang.StringBuilder();
        r11.append("Response received (Op Code = ");
        r11.append((int) r4[1]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:?, code lost:
        r11.append(" Status = ");
        r11.append(r8);
        r11.append(")");
        r5.mo11087a(10, r11.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02d0, code lost:
        if (r8 == 2) goto L_0x02d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02d2, code lost:
        m3691a(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02d5, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02d7, code lost:
        if (r8 != 1) goto L_0x02dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02e1, code lost:
        throw new com.crrepa.ble.nrf.dfu.p077q.p078c.C1349d("Starting DFU failed", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x02e3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02e4, code lost:
        r3 = r0;
        r4 = r19;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x02eb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x02ec, code lost:
        r3 = r0;
        r4 = r19;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02f3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x02f4, code lost:
        r4 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02fa, code lost:
        if (r4.mo11199a() != 3) goto L_0x0642;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02ff, code lost:
        r1.f2058z = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:?, code lost:
        mo11135c("DFU target does not support DFU v.2");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        r1.f2077n.mo11087a(15, "DFU target does not support DFU v.2");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:?, code lost:
        r1.f2077n.mo11087a(1, "Switching to DFU v.1");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        mo11134b("Resending Start DFU command (Op Code = 1)");
        m3693a(r1.f2135A, com.crrepa.ble.nrf.dfu.C1336n.f2126M);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        r1.f2077n.mo11087a(10, "DFU Start sent (Op Code = 1)");
        r4 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:?, code lost:
        r4.append("Sending application image size to DFU Packet: ");
        r4.append(r1.f2079p);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:?, code lost:
        r4.append(" bytes");
        mo11134b(r4.toString());
        m3697b(r1.f2136B, r1.f2079p);
        r4 = r1.f2077n;
        r5 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:?, code lost:
        r5.append("Firmware image size sent (");
        r5.append(r1.f2079p);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:?, code lost:
        r5.append(" bytes)");
        r4.mo11087a(10, r5.toString());
        r4 = mo11139g();
        r8 = m3690a(r4, 1);
        r5 = r1.f2077n;
        r5.mo11087a(10, "Response received (Op Code = " + ((int) r4[1]) + ", Status = " + r8 + ")");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x039a, code lost:
        if (r8 == 2) goto L_0x039c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x039c, code lost:
        m3691a(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x039f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03a1, code lost:
        if (r8 == 1) goto L_0x03a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03a3, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x0634, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:0x0635, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:?, code lost:
        mo11129a("Disconnected while sending data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x063b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x0641, code lost:
        throw new com.crrepa.ble.nrf.dfu.p077q.p078c.C1349d("Starting DFU failed", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x0642, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x0643, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x0645, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x0646, code lost:
        r3 = r0;
        r2 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x0649, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x064a, code lost:
        r3 = r0;
        r2 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:314:0x064e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x064f, code lost:
        r19 = r4;
        r20 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x0653, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01be, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01e3, code lost:
        r1.f2058z = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        mo11135c("DFU target does not support (SD/BL)+App update");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r1.f2077n.mo11087a(15, "DFU target does not support (SD/BL)+App update");
        r14 = r14 & -5;
        r1.f2068e = r14;
        com.crrepa.ble.nrf.dfu.C1336n.f2126M[1] = (byte) r14;
        r1.f2078o.mo11167f(2);
        ((com.crrepa.ble.nrf.dfu.p077q.C1344a) r1.f2065b).mo11176a(r14);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x02e3 A[ExcHandler: g (r0v9 'e' com.crrepa.ble.nrf.dfu.q.c.g A[CUSTOM_DECLARE]), PHI: r20 10  PHI: (r20v6 java.lang.String) = (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String) binds: [B:234:0x04ae, B:235:?, B:237:0x04be, B:238:?, B:303:0x0638, B:245:0x04de, B:246:?, B:248:0x04fb, B:298:0x0630, B:253:0x0543, B:254:?, B:256:0x0551, B:257:?, B:259:0x055a, B:260:?, B:262:0x056d, B:263:?, B:265:0x0575, B:266:?, B:268:0x0583, B:269:?, B:271:0x0593, B:272:?, B:274:0x05af, B:293:0x0628, B:279:0x05f8, B:280:?, B:282:0x0608, B:283:?, B:285:0x0615, B:228:0x0475, B:229:?, B:231:0x049a, B:232:?, B:182:0x03ae, B:183:?, B:214:0x0456, B:201:0x03f8, B:202:?, B:204:0x0409, B:205:?, B:207:0x0413, B:186:0x03b5, B:187:?, B:189:0x03c6, B:190:?, B:192:0x03d0, B:193:?, B:195:0x03e2, B:196:?, B:198:0x03f2, B:199:?, B:87:0x01d6, B:139:0x02f5, B:146:0x0304, B:147:?, B:149:0x030d, B:150:?, B:152:0x0315, B:153:?, B:155:0x031a, B:156:?, B:158:0x032a, B:159:?, B:161:0x0334, B:162:?, B:164:0x033e, B:165:?, B:167:0x0358, B:168:?, B:170:0x0362, B:95:0x01e8, B:96:?, B:98:0x01f1, B:99:?, B:101:0x0210, B:102:?, B:104:0x021a, B:105:?, B:107:0x023a, B:108:?, B:110:0x0253, B:111:?, B:113:0x0268, B:114:?, B:116:0x0281, B:117:?, B:119:0x0296, B:120:?, B:122:0x02bd, B:49:0x00f7, B:50:?, B:52:0x0119, B:53:?, B:55:0x0132, B:56:?, B:58:0x0146, B:59:?, B:61:0x015e, B:62:?, B:64:0x0172, B:65:?, B:67:0x019a] A[DONT_GENERATE, DONT_INLINE], Splitter:B:49:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x02eb A[ExcHandler: h (r0v8 'e' com.crrepa.ble.nrf.dfu.q.c.h A[CUSTOM_DECLARE]), PHI: r20 10  PHI: (r20v5 java.lang.String) = (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String) binds: [B:234:0x04ae, B:235:?, B:237:0x04be, B:238:?, B:303:0x0638, B:245:0x04de, B:246:?, B:248:0x04fb, B:298:0x0630, B:253:0x0543, B:254:?, B:256:0x0551, B:257:?, B:259:0x055a, B:260:?, B:262:0x056d, B:263:?, B:265:0x0575, B:266:?, B:268:0x0583, B:269:?, B:271:0x0593, B:272:?, B:274:0x05af, B:293:0x0628, B:279:0x05f8, B:280:?, B:282:0x0608, B:283:?, B:285:0x0615, B:228:0x0475, B:229:?, B:231:0x049a, B:232:?, B:182:0x03ae, B:183:?, B:214:0x0456, B:201:0x03f8, B:202:?, B:204:0x0409, B:205:?, B:207:0x0413, B:186:0x03b5, B:187:?, B:189:0x03c6, B:190:?, B:192:0x03d0, B:193:?, B:195:0x03e2, B:196:?, B:198:0x03f2, B:199:?, B:87:0x01d6, B:139:0x02f5, B:146:0x0304, B:147:?, B:149:0x030d, B:150:?, B:152:0x0315, B:153:?, B:155:0x031a, B:156:?, B:158:0x032a, B:159:?, B:161:0x0334, B:162:?, B:164:0x033e, B:165:?, B:167:0x0358, B:168:?, B:170:0x0362, B:95:0x01e8, B:96:?, B:98:0x01f1, B:99:?, B:101:0x0210, B:102:?, B:104:0x021a, B:105:?, B:107:0x023a, B:108:?, B:110:0x0253, B:111:?, B:113:0x0268, B:114:?, B:116:0x0281, B:117:?, B:119:0x0296, B:120:?, B:122:0x02bd, B:49:0x00f7, B:50:?, B:52:0x0119, B:53:?, B:55:0x0132, B:56:?, B:58:0x0146, B:59:?, B:61:0x015e, B:62:?, B:64:0x0172, B:65:?, B:67:0x019a] A[DONT_GENERATE, DONT_INLINE], Splitter:B:49:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x03a8 A[Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x045c A[Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x046e A[Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x053c A[Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x062c A[Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:310:0x0645 A[ExcHandler: g (r0v5 'e' com.crrepa.ble.nrf.dfu.q.c.g A[CUSTOM_DECLARE]), Splitter:B:9:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0649 A[ExcHandler: h (r0v4 'e' com.crrepa.ble.nrf.dfu.q.c.h A[CUSTOM_DECLARE]), Splitter:B:9:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:314:0x064e A[ExcHandler: d (e com.crrepa.ble.nrf.dfu.q.c.d), Splitter:B:7:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01dd A[ADDED_TO_REGION, Catch:{ a -> 0x0634, d -> 0x02f3, h -> 0x02eb, g -> 0x02e3, h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo11151a(android.content.Intent r23) {
        /*
            r22 = this;
            r1 = r22
            r2 = r23
            java.lang.String r3 = "Starting DFU failed"
            java.lang.String r4 = "Reset request sent"
            java.lang.String r5 = "Sending Reset command (Op Code = 6)"
            java.lang.String r6 = ", Status = "
            java.lang.String r7 = "Response received (Op Code = "
            java.lang.String r8 = "b, "
            java.lang.String r9 = ")"
            java.lang.String r10 = "Legacy DFU bootloader found"
            r1.mo11135c(r10)
            com.crrepa.ble.nrf.dfu.i r10 = r1.f2078o
            r11 = -2
            r10.mo11165e(r11)
            com.crrepa.ble.nrf.dfu.DfuBaseService r10 = r1.f2077n
            r11 = 1000(0x3e8, float:1.401E-42)
            r10.mo11086a(r11)
            android.bluetooth.BluetoothGatt r10 = r1.f2067d
            java.util.UUID r11 = com.crrepa.ble.nrf.dfu.C1336n.f2122I
            android.bluetooth.BluetoothGattService r11 = r10.getService(r11)
            java.util.UUID r12 = com.crrepa.ble.nrf.dfu.C1336n.f2125L
            android.bluetooth.BluetoothGattCharacteristic r11 = r11.getCharacteristic(r12)
            int r11 = r1.m3696b(r11)
            r12 = 5
            r13 = 20
            if (r11 < r12) goto L_0x0062
            java.io.InputStream r14 = r1.f2066c
            if (r14 != 0) goto L_0x0062
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Init packet not set for the DFU Bootloader version "
            r2.append(r3)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            r1.mo11135c(r2)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            java.lang.String r3 = "The Init packet is required by this version DFU Bootloader"
            r2.mo11087a(r13, r3)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r3 = 4107(0x100b, float:5.755E-42)
            r2.mo11089a(r10, r3)
            return
        L_0x0062:
            r15 = 1
            r12 = 10
            android.bluetooth.BluetoothGattCharacteristic r14 = r1.f2135A     // Catch:{ h -> 0x06be, g -> 0x0690, d -> 0x064e }
            r1.mo11126a(r14, r15)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            com.crrepa.ble.nrf.dfu.DfuBaseService r14 = r1.f2077n     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            java.lang.String r15 = "Notifications enabled"
            r14.mo11087a(r12, r15)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            com.crrepa.ble.nrf.dfu.DfuBaseService r14 = r1.f2077n     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            r15 = 1000(0x3e8, float:1.401E-42)
            r14.mo11086a(r15)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            int r14 = r1.f2068e     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            r15 = r14 & 1
            if (r15 <= 0) goto L_0x0089
            int r15 = r1.f2079p     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            goto L_0x008a
        L_0x0081:
            r0 = move-exception
            r2 = r0
            r19 = r4
            r20 = r5
            goto L_0x0654
        L_0x0089:
            r15 = 0
        L_0x008a:
            r17 = r14 & 2
            if (r17 <= 0) goto L_0x0091
            int r12 = r1.f2079p     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            goto L_0x0092
        L_0x0091:
            r12 = 0
        L_0x0092:
            r18 = r14 & 4
            if (r18 <= 0) goto L_0x009b
            int r13 = r1.f2079p     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            r19 = r12
            goto L_0x009e
        L_0x009b:
            r19 = r12
            r13 = 0
        L_0x009e:
            java.io.InputStream r12 = r1.f2065b     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            boolean r12 = r12 instanceof com.crrepa.ble.nrf.dfu.p077q.C1344a     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x064e }
            if (r12 == 0) goto L_0x00e2
            java.io.InputStream r12 = r1.f2065b     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            com.crrepa.ble.nrf.dfu.q.a r12 = (com.crrepa.ble.nrf.dfu.p077q.C1344a) r12     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            boolean r13 = r12.mo11189u()     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            if (r13 == 0) goto L_0x00d5
            java.lang.String r2 = "Secure DFU is required to upload selected firmware"
            r1.mo11129a(r2)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            java.lang.String r3 = "The device does not support given firmware."
            r6 = 20
            r2.mo11087a(r6, r3)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            r1.mo11134b(r5)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.f2135A     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            byte[] r3 = com.crrepa.ble.nrf.dfu.C1336n.f2133T     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            r1.m3693a(r2, r3)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            r3 = 10
            r2.mo11087a(r3, r4)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            r3 = 4099(0x1003, float:5.744E-42)
            r2.mo11089a(r10, r3)     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            return
        L_0x00d5:
            int r15 = r12.mo11190v()     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            int r13 = r12.mo11178b()     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            int r12 = r12.mo11175a()     // Catch:{ h -> 0x0649, g -> 0x0645, d -> 0x0081 }
            goto L_0x00e5
        L_0x00e2:
            r12 = r13
            r13 = r19
        L_0x00e5:
            r19 = r4
            byte[] r20 = com.crrepa.ble.nrf.dfu.C1336n.f2126M     // Catch:{ d -> 0x01d0, h -> 0x01c9, g -> 0x01c2 }
            byte r4 = (byte) r14     // Catch:{ d -> 0x01d0, h -> 0x01c9, g -> 0x01c2 }
            r16 = 1
            r20[r16] = r4     // Catch:{ d -> 0x01d0, h -> 0x01c9, g -> 0x01c2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ d -> 0x01d0, h -> 0x01c9, g -> 0x01c2 }
            r4.<init>()     // Catch:{ d -> 0x01d0, h -> 0x01c9, g -> 0x01c2 }
            r20 = r5
            java.lang.String r5 = "Sending Start DFU command (Op Code = 1, Upload Mode = "
            r4.append(r5)     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r14)     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r9)     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r1.mo11134b(r4)     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2126M     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r1.m3693a(r4, r5)     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r5.<init>()     // Catch:{ d -> 0x01c0, h -> 0x02eb, g -> 0x02e3 }
            r21 = r11
            java.lang.String r11 = "DFU Start sent (Op Code = 1, Upload Mode = "
            r5.append(r11)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r14)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r9)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r11 = 10
            r4.mo11087a(r11, r5)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.<init>()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "Sending image size array to DFU Packet ("
            r4.append(r5)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r15)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r8)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r13)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r8)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r12)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "b)"
            r4.append(r5)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r1.mo11134b(r4)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2136B     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r1.m3692a(r4, r15, r13, r12)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.<init>()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r11 = "Firmware image size sent ("
            r5.append(r11)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r15)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r8)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r13)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r8)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r12)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r11 = "b)"
            r5.append(r11)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r11 = 10
            r4.mo11087a(r11, r5)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            byte[] r4 = r22.mo11139g()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r5 = 1
            int r11 = r1.m3690a(r4, r5)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12.<init>()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12.append(r7)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r16 = 1
            byte r4 = r4[r16]     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12.append(r4)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = " Status = "
            r12.append(r4)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12.append(r11)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12.append(r9)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r12.toString()     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r12 = 10
            r5.mo11087a(r12, r4)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4 = 2
            if (r11 != r4) goto L_0x01b3
            r1.m3691a(r10, r2)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            return
        L_0x01b3:
            r4 = 1
            if (r11 != r4) goto L_0x01b8
            goto L_0x02d9
        L_0x01b8:
            com.crrepa.ble.nrf.dfu.q.c.d r4 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            r4.<init>(r3, r11)     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
            throw r4     // Catch:{ d -> 0x01be, h -> 0x02eb, g -> 0x02e3 }
        L_0x01be:
            r0 = move-exception
            goto L_0x01d5
        L_0x01c0:
            r0 = move-exception
            goto L_0x01d3
        L_0x01c2:
            r0 = move-exception
            r3 = r0
            r2 = r5
            r4 = r19
            goto L_0x0693
        L_0x01c9:
            r0 = move-exception
            r3 = r0
            r2 = r5
            r4 = r19
            goto L_0x06c1
        L_0x01d0:
            r0 = move-exception
            r20 = r5
        L_0x01d3:
            r21 = r11
        L_0x01d5:
            r4 = r0
            int r5 = r4.mo11199a()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11 = 3
            if (r5 != r11) goto L_0x02e2
            if (r18 <= 0) goto L_0x02e2
            r5 = r14 & 3
            if (r5 <= 0) goto L_0x02e2
            r4 = 0
            r1.f2058z = r4     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = "DFU target does not support (SD/BL)+App update"
            r1.mo11135c(r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5 = 15
            java.lang.String r11 = "DFU target does not support (SD/BL)+App update"
            r4.mo11087a(r5, r11)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r14 = r14 & -5
            r1.f2068e = r14     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2126M     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            byte r5 = (byte) r14     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11 = 1
            r4[r11] = r5     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.i r4 = r1.f2078o     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5 = 2
            r4.mo11167f(r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.io.InputStream r4 = r1.f2065b     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.q.a r4 = (com.crrepa.ble.nrf.dfu.p077q.C1344a) r4     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.mo11176a(r14)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "Sending only SD/BL"
            r11 = 1
            r4.mo11087a(r11, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.<init>()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "Resending Start DFU command (Op Code = 1, Upload Mode = "
            r4.append(r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r14)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r9)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r1.mo11134b(r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2126M     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r1.m3693a(r4, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.<init>()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r11 = "DFU Start sent (Op Code = 1, Upload Mode = "
            r5.append(r11)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r14)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r9)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11 = 10
            r4.mo11087a(r11, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.<init>()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "Sending image size array to DFU Packet: ["
            r4.append(r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r15)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r13)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5 = 0
            r4.append(r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = "b]"
            r4.append(r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r1.mo11134b(r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2136B     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5 = 0
            r1.m3692a(r4, r15, r13, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.<init>()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r11 = "Firmware image size sent ["
            r5.append(r11)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r15)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r13)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r8 = 0
            r5.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r8 = "b]"
            r5.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r8 = 10
            r4.mo11087a(r8, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            byte[] r4 = r22.mo11139g()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r5 = 1
            int r8 = r1.m3690a(r4, r5)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11.<init>()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11.append(r7)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r12 = 1
            byte r4 = r4[r12]     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11.append(r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = " Status = "
            r11.append(r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11.append(r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11.append(r9)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            java.lang.String r4 = r11.toString()     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r11 = 10
            r5.mo11087a(r11, r4)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4 = 2
            if (r8 != r4) goto L_0x02d6
            r1.m3691a(r10, r2)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            return
        L_0x02d6:
            r4 = 1
            if (r8 != r4) goto L_0x02dc
        L_0x02d9:
            r3 = 1
            goto L_0x03a4
        L_0x02dc:
            com.crrepa.ble.nrf.dfu.q.c.d r4 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            r4.<init>(r3, r8)     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
            throw r4     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
        L_0x02e2:
            throw r4     // Catch:{ d -> 0x02f3, h -> 0x02eb, g -> 0x02e3 }
        L_0x02e3:
            r0 = move-exception
            r3 = r0
            r4 = r19
            r2 = r20
            goto L_0x0693
        L_0x02eb:
            r0 = move-exception
            r3 = r0
            r4 = r19
            r2 = r20
            goto L_0x06c1
        L_0x02f3:
            r0 = move-exception
            r4 = r0
            int r5 = r4.mo11199a()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 3
            if (r5 != r8) goto L_0x0642
            r5 = 4
            if (r14 != r5) goto L_0x0642
            r4 = 0
            r1.f2058z = r4     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "DFU target does not support DFU v.2"
            r1.mo11135c(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 15
            java.lang.String r8 = "DFU target does not support DFU v.2"
            r4.mo11087a(r5, r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Switching to DFU v.1"
            r8 = 1
            r4.mo11087a(r8, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Resending Start DFU command (Op Code = 1)"
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2126M     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "DFU Start sent (Op Code = 1)"
            r8 = 10
            r4.mo11087a(r8, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Sending application image size to DFU Packet: "
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r5 = r1.f2079p     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = " bytes"
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2136B     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r5 = r1.f2079p     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3697b(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r8 = "Firmware image size sent ("
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r8 = r1.f2079p     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r8 = " bytes)"
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = r5.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 10
            r4.mo11087a(r8, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r4 = r22.mo11139g()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 1
            int r8 = r1.m3690a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r12 = 1
            byte r4 = r4[r12]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r11.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11 = 10
            r5.mo11087a(r11, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4 = 2
            if (r8 != r4) goto L_0x03a0
            r1.m3691a(r10, r2)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            return
        L_0x03a0:
            r4 = 1
            if (r8 != r4) goto L_0x063c
            r3 = 0
        L_0x03a4:
            java.io.InputStream r4 = r1.f2066c     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            if (r4 == 0) goto L_0x045a
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Writing Initialize DFU Parameters..."
            r8 = 10
            r4.mo11087a(r8, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            if (r3 == 0) goto L_0x03f6
            java.lang.String r4 = "Sending the Initialize DFU Parameters START (Op Code = 2, Value = 0)"
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2128O     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Sending "
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r5 = r1.f2080q     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = " bytes of init packet"
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2136B     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 0
            r1.mo11115a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Sending the Initialize DFU Parameters COMPLETE (Op Code = 2, Value = 1)"
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2129P     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r4 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Initialize DFU Parameters completed"
            r8 = 10
            r4.mo11087a(r8, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            goto L_0x0423
        L_0x03f6:
            java.lang.String r4 = "Sending the Initialize DFU Parameters (Op Code = 2)"
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2127N     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Sending "
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r5 = r1.f2080q     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = " bytes of init packet"
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.f2136B     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 0
            r1.mo11115a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x0423:
            byte[] r4 = r22.mo11139g()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 2
            int r8 = r1.m3690a(r4, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r12 = 1
            byte r4 = r4[r12]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r11.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r11 = 10
            r5.mo11087a(r11, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4 = 1
            if (r8 != r4) goto L_0x0452
            goto L_0x045a
        L_0x0452:
            com.crrepa.ble.nrf.dfu.q.c.d r2 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = "Device returned error after sending init packet"
            r2.<init>(r3, r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            throw r2     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x045a:
            if (r3 != 0) goto L_0x046a
            int r3 = r1.f2056x     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            if (r3 <= 0) goto L_0x0467
            int r3 = r1.f2056x     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4 = 10
            if (r3 > r4) goto L_0x0467
            goto L_0x046a
        L_0x0467:
            r12 = 10
            goto L_0x046c
        L_0x046a:
            int r12 = r1.f2056x     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x046c:
            if (r12 <= 0) goto L_0x04ac
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Sending the number of packets before notifications (Op Code = 8, Value = "
            r3.append(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3.append(r12)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = r3.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r3 = com.crrepa.ble.nrf.dfu.C1336n.f2134U     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3698b(r3, r12)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2134U     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Packet Receipt Notif Req (Op Code = 8) sent (Value = "
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r12)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 10
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x04ac:
            java.lang.String r3 = "Sending Receive Firmware Image request (Op Code = 3)"
            r1.mo11134b(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2130Q     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Receive Firmware Image request sent"
            r5 = 10
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.i r5 = r1.f2078o     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 0
            r5.mo11161c(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Uploading firmware..."
            r1.mo11134b(r5)     // Catch:{ a -> 0x0634 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ a -> 0x0634 }
            java.lang.String r8 = "Uploading firmware..."
            r11 = 10
            r5.mo11087a(r11, r8)     // Catch:{ a -> 0x0634 }
            android.bluetooth.BluetoothGattCharacteristic r5 = r1.f2136B     // Catch:{ a -> 0x0634 }
            r1.mo11114a(r5)     // Catch:{ a -> 0x0634 }
            long r11 = android.os.SystemClock.elapsedRealtime()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r5 = r22.mo11139g()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 3
            int r8 = r1.m3690a(r5, r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14 = 0
            byte r15 = r5[r14]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r15)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r14 = ", Req Op Code = "
            r13.append(r14)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14 = 1
            byte r15 = r5[r14]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r15)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14 = 2
            byte r15 = r5[r14]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r15)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r13.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r13 = r13.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r13)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r13 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r15 = 1
            byte r5 = r5[r15]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = r14.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r14 = 10
            r13.mo11087a(r14, r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 1
            if (r8 != r5) goto L_0x062c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r8 = "Transfer of "
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.i r8 = r1.f2078o     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            int r8 = r8.mo11160c()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r8 = " bytes has taken "
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            long r11 = r11 - r3
            r5.append(r11)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = " ms"
            r5.append(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = r5.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = "Upload completed in "
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4.append(r11)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = " ms"
            r4.append(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5 = 10
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = "Sending Validate request (Op Code = 4)"
            r1.mo11134b(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2131R     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Validate request sent"
            r5 = 10
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r3 = r22.mo11139g()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4 = 4
            int r4 = r1.m3690a(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 0
            byte r11 = r3[r8]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r11)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r8 = ", Req Op Code = "
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 1
            byte r11 = r3[r8]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r11)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8 = 2
            byte r8 = r3[r8]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r5.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r5 = r5.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.mo11134b(r5)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.<init>()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.append(r7)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r7 = 1
            byte r3 = r3[r7]     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.append(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.append(r6)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.append(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r8.append(r9)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = r8.toString()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r6 = 10
            r5.mo11087a(r6, r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3 = 1
            if (r4 != r3) goto L_0x0624
            com.crrepa.ble.nrf.dfu.i r3 = r1.f2078o     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r4 = -5
            r3.mo11165e(r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = "Sending Activate and Reset request (Op Code = 5)"
            r1.mo11134b(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.f2135A     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2132S     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r1.m3693a(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Activate and Reset request sent"
            r5 = 10
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3.mo11094d()     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r4 = "Disconnected by the remote device"
            r5 = 5
            r3.mo11087a(r5, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r3 = r21
            if (r3 != r5) goto L_0x061e
            r3 = 1
            goto L_0x061f
        L_0x061e:
            r3 = 0
        L_0x061f:
            r1.mo11116b(r2, r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            goto L_0x06bd
        L_0x0624:
            com.crrepa.ble.nrf.dfu.q.c.d r2 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = "Device returned validation error"
            r2.<init>(r3, r4)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            throw r2     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x062c:
            com.crrepa.ble.nrf.dfu.q.c.d r2 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            java.lang.String r3 = "Device returned error after sending file"
            r2.<init>(r3, r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            throw r2     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x0634:
            r0 = move-exception
            r2 = r0
            java.lang.String r3 = "Disconnected while sending data"
            r1.mo11129a(r3)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            throw r2     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x063c:
            com.crrepa.ble.nrf.dfu.q.c.d r2 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            r2.<init>(r3, r8)     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
            throw r2     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x0642:
            throw r4     // Catch:{ h -> 0x02eb, g -> 0x02e3, d -> 0x0643 }
        L_0x0643:
            r0 = move-exception
            goto L_0x0653
        L_0x0645:
            r0 = move-exception
            r3 = r0
            r2 = r5
            goto L_0x0693
        L_0x0649:
            r0 = move-exception
            r3 = r0
            r2 = r5
            goto L_0x06c1
        L_0x064e:
            r0 = move-exception
            r19 = r4
            r20 = r5
        L_0x0653:
            r2 = r0
        L_0x0654:
            int r3 = r2.mo11199a()
            r3 = r3 | 8192(0x2000, float:1.14794E-41)
            java.lang.String r2 = r2.getMessage()
            r1.mo11129a(r2)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.String r5 = com.crrepa.ble.p070h.p071a.C1304b.m3539a(r3)
            r6 = 0
            r4[r6] = r5
            java.lang.String r5 = "Remote DFU error: %s"
            java.lang.String r4 = java.lang.String.format(r5, r4)
            r5 = 20
            r2.mo11087a(r5, r4)
            r2 = r20
            r1.mo11134b(r2)
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.f2135A
            byte[] r4 = com.crrepa.ble.nrf.dfu.C1336n.f2133T
            r1.m3693a(r2, r4)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r4 = r19
            r5 = 10
            r2.mo11087a(r5, r4)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            goto L_0x06ba
        L_0x0690:
            r0 = move-exception
            r2 = r5
            r3 = r0
        L_0x0693:
            java.lang.String r5 = r3.getMessage()
            r1.mo11129a(r5)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r1.f2077n
            java.lang.String r3 = r3.getMessage()
            r6 = 20
            r5.mo11087a(r6, r3)
            r1.mo11134b(r2)
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.f2135A
            byte[] r3 = com.crrepa.ble.nrf.dfu.C1336n.f2133T
            r1.m3693a(r2, r3)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r3 = 10
            r2.mo11087a(r3, r4)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r3 = 4104(0x1008, float:5.751E-42)
        L_0x06ba:
            r2.mo11089a(r10, r3)
        L_0x06bd:
            return
        L_0x06be:
            r0 = move-exception
            r2 = r5
            r3 = r0
        L_0x06c1:
            r1.mo11134b(r2)
            r2 = 0
            r1.f2070g = r2
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.f2135A
            byte[] r5 = com.crrepa.ble.nrf.dfu.C1336n.f2133T
            r1.m3693a(r2, r5)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r1.f2077n
            r5 = 10
            r2.mo11087a(r5, r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1336n.mo11151a(android.content.Intent):void");
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(f2122I);
        if (service == null) {
            return false;
        }
        this.f2135A = service.getCharacteristic(f2123J);
        this.f2136B = service.getCharacteristic(f2124K);
        return (this.f2135A == null || this.f2136B == null) ? false : true;
    }

    /* renamed from: c */
    public C1320b.C1321a m3702c() {
        return this.f2138D;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public UUID mo11117j() {
        return f2122I;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public UUID mo11118k() {
        return f2124K;
    }
}
