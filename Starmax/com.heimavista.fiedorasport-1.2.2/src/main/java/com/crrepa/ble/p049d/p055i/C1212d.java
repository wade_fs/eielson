package com.crrepa.ble.p049d.p055i;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import com.crrepa.ble.p049d.p054h.C1203x;
import com.crrepa.ble.p049d.p054h.C1206z;
import com.crrepa.ble.p049d.p057k.C1215a;
import com.crrepa.ble.p049d.p057k.C1216b;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p049d.p057k.C1219d;
import com.crrepa.ble.p049d.p057k.C1220e;
import com.crrepa.ble.p049d.p059m.C1242a;
import com.crrepa.ble.p049d.p061o.C1252c;
import com.crrepa.ble.p049d.p062p.C1256b;
import com.crrepa.ble.p049d.p063q.C1259a;
import com.crrepa.ble.p049d.p063q.C1266h;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.d.i.d */
public class C1212d extends BluetoothGattCallback {

    /* renamed from: a */
    private C1259a f1879a;

    /* renamed from: b */
    private C1220e f1880b = new C1220e();

    /* renamed from: c */
    private C1219d f1881c = new C1219d();

    /* renamed from: d */
    private C1216b f1882d = new C1216b();

    /* renamed from: e */
    private C1215a f1883e = new C1215a();

    /* renamed from: f */
    private C1266h f1884f;

    /* renamed from: g */
    private C1203x f1885g;

    /* renamed from: a */
    private void m3174a(int i) {
        C1259a aVar = this.f1879a;
        if (aVar != null) {
            aVar.mo11003a(i);
        }
    }

    /* renamed from: c */
    private void m3175c() {
        this.f1882d.mo10934a(C1206z.m3111b());
    }

    /* renamed from: d */
    private void m3176d() {
        mo10901a((C1203x) null);
    }

    /* renamed from: e */
    private void m3177e() {
        C1252c.m3359d().mo10984c();
        C1252c.m3359d().mo10981a(true);
    }

    /* renamed from: a */
    public C1215a mo10900a() {
        return this.f1883e;
    }

    /* renamed from: a */
    public void mo10901a(C1203x xVar) {
        this.f1885g = xVar;
    }

    /* renamed from: a */
    public void mo10902a(C1259a aVar) {
        this.f1879a = aVar;
        m3174a(1);
    }

    /* renamed from: b */
    public C1216b mo10903b() {
        return this.f1882d;
    }

    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        C1293b.m3504b("onCharacteristicChanged: " + C1295d.m3512b(bluetoothGattCharacteristic.getValue()));
        this.f1883e.mo10912a(bluetoothGattCharacteristic);
    }

    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        C1293b.m3504b("onCharacteristicRead: " + C1295d.m3512b(bluetoothGattCharacteristic.getValue()));
        this.f1882d.mo10932a(bluetoothGattCharacteristic);
    }

    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        C1293b.m3504b("onCharacteristicWrite: " + bluetoothGattCharacteristic.getUuid().toString());
        C1293b.m3504b("onCharacteristicWrite: " + C1295d.m3512b(bluetoothGattCharacteristic.getValue()));
        C1217c.m3269e().mo10941d();
    }

    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        C1293b.m3504b("BleGattCallback：onConnectionStateChange \nstatus: " + i + 10 + "newState: " + i2);
        if (i2 == 2) {
            C1256b.m3384d().mo11001c();
            m3177e();
        } else if (i2 == 0) {
            m3174a(i2);
            this.f1880b.mo10948a();
            m3176d();
        }
    }

    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        C1293b.m3504b("onDescriptorWrite: " + C1295d.m3512b(bluetoothGattDescriptor.getValue()));
        if (this.f1881c.mo10947a(bluetoothGatt, bluetoothGattDescriptor)) {
            m3175c();
        }
    }

    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onMtuChanged(bluetoothGatt, i, i2);
        C1293b.m3504b("onMtuChanged: " + i);
        C1242a.m3332c().mo10962a(i);
        C1203x xVar = this.f1885g;
        if (xVar != null) {
            xVar.mo10894a(i);
            return;
        }
        C1293b.m3504b("STATE_CONNECTED");
        m3174a(2);
    }

    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        C1266h hVar = this.f1884f;
        if (hVar != null) {
            hVar.mo11016a(i);
        }
    }

    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        super.onServicesDiscovered(bluetoothGatt, i);
        if (C1256b.m3384d().mo10999a(bluetoothGatt.getServices())) {
            this.f1881c.mo10946a(bluetoothGatt);
        }
    }
}
