package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import androidx.core.view.InputDeviceCompat;
import androidx.fragment.app.FragmentTransaction;
import com.crrepa.ble.nrf.dfu.C1322c;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1347b;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1348c;
import java.io.IOException;
import java.util.UUID;
import java.util.zip.CRC32;

/* renamed from: com.crrepa.ble.nrf.dfu.b */
abstract class C1320b extends C1322c {
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f2054v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public boolean f2055w;

    /* renamed from: x */
    protected final int f2056x;

    /* renamed from: y */
    protected int f2057y;

    /* renamed from: z */
    protected boolean f2058z;

    /* renamed from: com.crrepa.ble.nrf.dfu.b$a */
    protected class C1321a extends C1322c.C1323a {
        protected C1321a() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.b, int]
         candidates:
          com.crrepa.ble.nrf.dfu.b.b(android.content.Intent, boolean):void
          com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo11119a(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            DfuBaseService dfuBaseService = C1320b.this.f2077n;
            dfuBaseService.mo11087a(5, "Notification received from " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
            C1320b.this.f2075l = bluetoothGattCharacteristic.getValue();
            boolean unused = C1320b.this.f2055w = false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo11120a(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.b, int]
         candidates:
          com.crrepa.ble.nrf.dfu.b.b(android.content.Intent, boolean):void
          com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void mo11121b(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            int i;
            C1320b bVar;
            if (!C1320b.this.f2055w) {
                mo11119a(bluetoothGatt, bluetoothGattCharacteristic);
                return;
            }
            BluetoothGattCharacteristic characteristic = bluetoothGatt.getService(C1320b.this.mo11117j()).getCharacteristic(C1320b.this.mo11118k());
            try {
                C1320b.this.f2057y = 0;
                C1320b.this.mo11141i();
                if (!C1320b.this.f2070g && C1320b.this.f2074k == 0 && !C1320b.this.f2058z) {
                    if (!C1320b.this.f2073j) {
                        boolean h = C1320b.this.f2078o.mo11169h();
                        boolean j = C1320b.this.f2078o.mo11171j();
                        if (!h) {
                            if (!j) {
                                int a = C1320b.this.f2078o.mo11155a();
                                byte[] bArr = C1320b.this.f2076m;
                                if (a < bArr.length) {
                                    bArr = new byte[a];
                                }
                                C1320b.this.m3594a(bluetoothGatt, characteristic, bArr, C1320b.this.f2065b.read(bArr));
                                return;
                            }
                        }
                        boolean unused = C1320b.this.f2055w = false;
                        C1320b.this.mo11138f();
                        return;
                    }
                }
                boolean unused2 = C1320b.this.f2055w = false;
                C1320b.this.f2077n.mo11087a(15, "Upload terminated");
            } catch (C1348c unused3) {
                C1320b.this.mo11129a("Invalid HEX file");
                bVar = C1320b.this;
                i = FragmentTransaction.TRANSIT_FRAGMENT_FADE;
                bVar.f2074k = i;
            } catch (IOException e) {
                C1320b.this.mo11130a("Error while reading the input stream", e);
                bVar = C1320b.this;
                i = 4100;
                bVar.f2074k = i;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.b.a(com.crrepa.ble.nrf.dfu.b, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.b, int]
         candidates:
          com.crrepa.ble.nrf.dfu.b.a(android.bluetooth.BluetoothGattCharacteristic, java.util.zip.CRC32):void
          com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, int):void
          com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void
          com.crrepa.ble.nrf.dfu.c.a(java.lang.String, java.lang.Throwable):void
          com.crrepa.ble.nrf.dfu.j.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
          com.crrepa.ble.nrf.dfu.b.a(com.crrepa.ble.nrf.dfu.b, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.b, int]
         candidates:
          com.crrepa.ble.nrf.dfu.b.b(android.content.Intent, boolean):void
          com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean */
        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            int i2;
            C1320b bVar;
            C1320b bVar2;
            boolean z = true;
            if (i != 0) {
                bVar2 = C1320b.this;
                if (!bVar2.f2073j) {
                    bVar2.mo11129a("Characteristic write error: " + i);
                    bVar = C1320b.this;
                    i2 = i | 16384;
                    bVar.f2074k = i2;
                }
                bVar2.f2072i = true;
            } else if (!bluetoothGattCharacteristic.getUuid().equals(C1320b.this.mo11118k())) {
                C1320b.this.f2077n.mo11087a(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
                bVar2 = C1320b.this;
                bVar2.f2072i = true;
            } else if (C1320b.this.f2054v) {
                C1320b.this.f2077n.mo11087a(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
                boolean unused = C1320b.this.f2054v = false;
            } else if (C1320b.this.f2055w) {
                C1320b.this.f2078o.mo11157a(bluetoothGattCharacteristic.getValue().length);
                C1320b bVar3 = C1320b.this;
                bVar3.f2057y++;
                int i3 = bVar3.f2056x;
                if (i3 <= 0 || bVar3.f2057y != i3) {
                    z = false;
                }
                boolean h = C1320b.this.f2078o.mo11169h();
                boolean j = C1320b.this.f2078o.mo11171j();
                if (!z) {
                    if (h || j) {
                        boolean unused2 = C1320b.this.f2055w = false;
                    } else {
                        try {
                            C1320b.this.mo11141i();
                            if (!C1320b.this.f2070g && C1320b.this.f2074k == 0 && !C1320b.this.f2058z) {
                                if (!C1320b.this.f2073j) {
                                    int a = C1320b.this.f2078o.mo11155a();
                                    byte[] bArr = C1320b.this.f2076m;
                                    if (a < bArr.length) {
                                        bArr = new byte[a];
                                    }
                                    C1320b.this.m3594a(bluetoothGatt, bluetoothGattCharacteristic, bArr, C1320b.this.f2065b.read(bArr));
                                    return;
                                }
                            }
                            boolean unused3 = C1320b.this.f2055w = false;
                            C1320b.this.f2077n.mo11087a(15, "Upload terminated");
                            C1320b.this.mo11138f();
                            return;
                        } catch (C1348c unused4) {
                            C1320b.this.mo11129a("Invalid HEX file");
                            bVar = C1320b.this;
                            i2 = FragmentTransaction.TRANSIT_FRAGMENT_FADE;
                        } catch (IOException e) {
                            C1320b.this.mo11130a("Error while reading the input stream", e);
                            bVar = C1320b.this;
                            i2 = 4100;
                        }
                    }
                } else {
                    return;
                }
            } else {
                mo11120a(bluetoothGatt, bluetoothGattCharacteristic, i);
            }
            C1320b.this.mo11138f();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004d, code lost:
        if (r8 <= 65535) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        if (r9 == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        if (r9 == false) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    C1320b(android.content.Intent r8, com.crrepa.ble.nrf.dfu.DfuBaseService r9) {
        /*
            r7 = this;
            r7.<init>(r8, r9)
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_ENABLED"
            boolean r1 = r8.hasExtra(r0)
            r2 = 65535(0xffff, float:9.1834E-41)
            r3 = 1
            r4 = 23
            r5 = 0
            r6 = 12
            if (r1 == 0) goto L_0x002d
            int r9 = android.os.Build.VERSION.SDK_INT
            if (r9 >= r4) goto L_0x0019
            goto L_0x001a
        L_0x0019:
            r3 = 0
        L_0x001a:
            boolean r9 = r8.getBooleanExtra(r0, r3)
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_VALUE"
            int r8 = r8.getIntExtra(r0, r6)
            if (r8 < 0) goto L_0x0028
            if (r8 <= r2) goto L_0x002a
        L_0x0028:
            r8 = 12
        L_0x002a:
            if (r9 != 0) goto L_0x0054
            goto L_0x0053
        L_0x002d:
            android.content.SharedPreferences r8 = android.preference.PreferenceManager.getDefaultSharedPreferences(r9)
            int r9 = android.os.Build.VERSION.SDK_INT
            if (r9 >= r4) goto L_0x0036
            goto L_0x0037
        L_0x0036:
            r3 = 0
        L_0x0037:
            java.lang.String r9 = "settings_packet_receipt_notification_enabled"
            boolean r9 = r8.getBoolean(r9, r3)
            java.lang.String r0 = java.lang.String.valueOf(r6)
            java.lang.String r1 = "settings_number_of_packets"
            java.lang.String r8 = r8.getString(r1, r0)
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ NumberFormatException -> 0x004f }
            if (r8 < 0) goto L_0x004f
            if (r8 <= r2) goto L_0x0051
        L_0x004f:
            r8 = 12
        L_0x0051:
            if (r9 != 0) goto L_0x0054
        L_0x0053:
            r8 = 0
        L_0x0054:
            r7.f2056x = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1320b.<init>(android.content.Intent, com.crrepa.ble.nrf.dfu.DfuBaseService):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3594a(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, int i) {
        if (i > 0) {
            if (bArr.length != i) {
                byte[] bArr2 = new byte[i];
                System.arraycopy(bArr, 0, bArr2, 0, i);
                bArr = bArr2;
            }
            bluetoothGattCharacteristic.setWriteType(1);
            bluetoothGattCharacteristic.setValue(bArr);
            bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ac  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3595a(android.bluetooth.BluetoothGattCharacteristic r4, byte[] r5, int r6) {
        /*
            r3 = this;
            boolean r0 = r3.f2070g
            if (r0 != 0) goto L_0x00b4
            int r0 = r5.length
            r1 = 0
            if (r0 == r6) goto L_0x000e
            byte[] r0 = new byte[r6]
            java.lang.System.arraycopy(r5, r1, r0, r1, r6)
            r5 = r0
        L_0x000e:
            r6 = 0
            r3.f2075l = r6
            r3.f2074k = r1
            r6 = 1
            r3.f2054v = r6
            r4.setWriteType(r6)
            r4.setValue(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Sending init packet (Value = "
            r0.append(r2)
            java.lang.String r5 = r3.mo11123a(r5)
            r0.append(r5)
            java.lang.String r5 = ")"
            r0.append(r5)
            java.lang.String r5 = r0.toString()
            r3.mo11134b(r5)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r3.f2077n
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Writing to characteristic "
            r0.append(r2)
            java.util.UUID r2 = r4.getUuid()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.mo11087a(r6, r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r3.f2077n
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r0 = "gatt.writeCharacteristic("
            r6.append(r0)
            java.util.UUID r0 = r4.getUuid()
            r6.append(r0)
            java.lang.String r0 = ")"
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r5.mo11087a(r1, r6)
            android.bluetooth.BluetoothGatt r5 = r3.f2067d
            r5.writeCharacteristic(r4)
            java.lang.Object r4 = r3.f2064a     // Catch:{ InterruptedException -> 0x0095 }
            monitor-enter(r4)     // Catch:{ InterruptedException -> 0x0095 }
        L_0x007a:
            boolean r5 = r3.f2054v     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0086
            boolean r5 = r3.f2071h     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0086
            int r5 = r3.f2074k     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x008a
        L_0x0086:
            boolean r5 = r3.f2069f     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0090
        L_0x008a:
            java.lang.Object r5 = r3.f2064a     // Catch:{ all -> 0x0092 }
            r5.wait()     // Catch:{ all -> 0x0092 }
            goto L_0x007a
        L_0x0090:
            monitor-exit(r4)     // Catch:{ all -> 0x0092 }
            goto L_0x009b
        L_0x0092:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0092 }
            throw r5     // Catch:{ InterruptedException -> 0x0095 }
        L_0x0095:
            r4 = move-exception
            java.lang.String r5 = "Sleeping interrupted"
            r3.mo11130a(r5, r4)
        L_0x009b:
            int r4 = r3.f2074k
            if (r4 != 0) goto L_0x00ac
            boolean r4 = r3.f2071h
            if (r4 == 0) goto L_0x00a4
            return
        L_0x00a4:
            com.crrepa.ble.nrf.dfu.q.c.a r4 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r5 = "Unable to write Init DFU Parameters: device disconnected"
            r4.<init>(r5)
            throw r4
        L_0x00ac:
            com.crrepa.ble.nrf.dfu.q.c.b r5 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r6 = "Unable to write Init DFU Parameters"
            r5.<init>(r6, r4)
            throw r5
        L_0x00b4:
            com.crrepa.ble.nrf.dfu.q.c.h r4 = new com.crrepa.ble.nrf.dfu.q.c.h
            r4.<init>()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1320b.m3595a(android.bluetooth.BluetoothGattCharacteristic, byte[], int):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0074  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo11114a(android.bluetooth.BluetoothGattCharacteristic r7) {
        /*
            r6 = this;
            boolean r0 = r6.f2070g
            if (r0 != 0) goto L_0x0090
            r0 = 0
            r6.f2075l = r0
            r0 = 0
            r6.f2074k = r0
            r1 = 1
            r6.f2055w = r1
            r6.f2057y = r0
            byte[] r0 = r6.f2076m
            java.io.InputStream r2 = r6.f2065b     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            int r2 = r2.read(r0)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r6.f2077n     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            r4.<init>()     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.lang.String r5 = "Sending firmware to characteristic "
            r4.append(r5)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.util.UUID r5 = r7.getUuid()     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            r4.append(r5)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.lang.String r5 = "..."
            r4.append(r5)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.lang.String r4 = r4.toString()     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            r3.mo11087a(r1, r4)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            android.bluetooth.BluetoothGatt r1 = r6.f2067d     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            r6.m3594a(r1, r7, r0, r2)     // Catch:{ c -> 0x0086, IOException -> 0x007c }
            java.lang.Object r7 = r6.f2064a     // Catch:{ InterruptedException -> 0x005d }
            monitor-enter(r7)     // Catch:{ InterruptedException -> 0x005d }
        L_0x003e:
            boolean r0 = r6.f2055w     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x004e
            byte[] r0 = r6.f2075l     // Catch:{ all -> 0x005a }
            if (r0 != 0) goto L_0x004e
            boolean r0 = r6.f2071h     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x004e
            int r0 = r6.f2074k     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0052
        L_0x004e:
            boolean r0 = r6.f2069f     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0058
        L_0x0052:
            java.lang.Object r0 = r6.f2064a     // Catch:{ all -> 0x005a }
            r0.wait()     // Catch:{ all -> 0x005a }
            goto L_0x003e
        L_0x0058:
            monitor-exit(r7)     // Catch:{ all -> 0x005a }
            goto L_0x0063
        L_0x005a:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x005a }
            throw r0     // Catch:{ InterruptedException -> 0x005d }
        L_0x005d:
            r7 = move-exception
            java.lang.String r0 = "Sleeping interrupted"
            r6.mo11130a(r0, r7)
        L_0x0063:
            int r7 = r6.f2074k
            if (r7 != 0) goto L_0x0074
            boolean r7 = r6.f2071h
            if (r7 == 0) goto L_0x006c
            return
        L_0x006c:
            com.crrepa.ble.nrf.dfu.q.c.a r7 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r0 = "Uploading Firmware Image failed: device disconnected"
            r7.<init>(r0)
            throw r7
        L_0x0074:
            com.crrepa.ble.nrf.dfu.q.c.b r0 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r1 = "Uploading Firmware Image failed"
            r0.<init>(r1, r7)
            throw r0
        L_0x007c:
            com.crrepa.ble.nrf.dfu.q.c.b r7 = new com.crrepa.ble.nrf.dfu.q.c.b
            r0 = 4100(0x1004, float:5.745E-42)
            java.lang.String r1 = "Error while reading file"
            r7.<init>(r1, r0)
            throw r7
        L_0x0086:
            com.crrepa.ble.nrf.dfu.q.c.b r7 = new com.crrepa.ble.nrf.dfu.q.c.b
            r0 = 4099(0x1003, float:5.744E-42)
            java.lang.String r1 = "HEX file not valid"
            r7.<init>(r1, r0)
            throw r7
        L_0x0090:
            com.crrepa.ble.nrf.dfu.q.c.h r7 = new com.crrepa.ble.nrf.dfu.q.c.h
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1320b.mo11114a(android.bluetooth.BluetoothGattCharacteristic):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11115a(BluetoothGattCharacteristic bluetoothGattCharacteristic, CRC32 crc32) {
        try {
            byte[] bArr = super.f2076m;
            while (true) {
                int read = super.f2066c.read(bArr, 0, bArr.length);
                if (read != -1) {
                    m3595a(bluetoothGattCharacteristic, bArr, read);
                    if (crc32 != null) {
                        crc32.update(bArr, 0, read);
                    }
                } else {
                    return;
                }
            }
        } catch (IOException e) {
            mo11130a("Error while reading Init packet file", e);
            throw new C1347b("Error while reading Init packet file", InputDeviceCompat.SOURCE_TOUCHSCREEN);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.crrepa.ble.nrf.dfu.b.a(com.crrepa.ble.nrf.dfu.b, boolean):boolean
      com.crrepa.ble.nrf.dfu.b.a(android.bluetooth.BluetoothGattCharacteristic, java.util.zip.CRC32):void
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, int):void
      com.crrepa.ble.nrf.dfu.c.a(java.lang.String, java.lang.Throwable):void
      com.crrepa.ble.nrf.dfu.j.a(android.content.Intent, android.bluetooth.BluetoothGatt):boolean
      com.crrepa.ble.nrf.dfu.c.a(android.content.Intent, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11116b(Intent intent, boolean z) {
        boolean z2;
        boolean z3 = false;
        boolean booleanExtra = intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_KEEP_BOND", false);
        super.f2077n.mo11090a(super.f2067d, z || !booleanExtra);
        super.f2077n.mo11088a(super.f2067d);
        if (super.f2067d.getDevice().getBondState() == 12) {
            boolean booleanExtra2 = intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_RESTORE_BOND", false);
            if (booleanExtra2 || !booleanExtra) {
                mo11140h();
                super.f2077n.mo11086a(2000);
                z2 = true;
            } else {
                z2 = false;
            }
            if (!booleanExtra2 || (super.f2068e & 4) <= 0) {
                z3 = z2;
            } else {
                mo11136d();
            }
        }
        if (super.f2078o.mo11170i()) {
            if (!z3) {
                super.f2077n.mo11086a(1400);
            }
            super.f2078o.mo11165e(-6);
            return;
        }
        mo11134b("Starting service that will upload application");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        intent2.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_MIME_TYPE", "application/zip");
        intent2.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_FILE_TYPE", 4);
        intent2.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PART_CURRENT", super.f2078o.mo11162d() + 1);
        intent2.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PARTS_TOTAL", super.f2078o.mo11168g());
        mo11128a(intent2, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public abstract UUID mo11117j();

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public abstract UUID mo11118k();
}
