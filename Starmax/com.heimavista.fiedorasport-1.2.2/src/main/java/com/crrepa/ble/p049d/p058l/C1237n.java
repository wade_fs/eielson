package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1172i;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1298g;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.d.l.n */
public class C1237n {
    /* renamed from: a */
    private static int m3318a(int i, int i2, int i3, int i4) {
        if (i > i3) {
            i3 += 24;
        }
        return (((i3 - i) * 60) + i4) - i2;
    }

    /* renamed from: a */
    public static C1172i.C1173a m3319a(int i, int i2, int i3, int i4, int i5, int i6) {
        C1172i.C1173a aVar = new C1172i.C1173a();
        aVar.mo10848c(i6);
        aVar.mo10850d(i);
        aVar.mo10846b(C1298g.m3525a(i2, i3));
        aVar.mo10844a(C1298g.m3525a(i4, i5));
        return aVar;
    }

    /* renamed from: a */
    public static C1172i m3320a(byte[] bArr, boolean z) {
        byte b;
        byte[] bArr2 = bArr;
        C1172i iVar = new C1172i();
        if (bArr2 != null && bArr2.length % 3 == 0) {
            int length = bArr2.length;
            byte[] bArr3 = new byte[3];
            int i = 0;
            byte b2 = -1;
            byte b3 = 0;
            byte b4 = 0;
            int i2 = 0;
            while (i2 < length) {
                System.arraycopy(bArr2, i2, bArr3, i, bArr3.length);
                byte b5 = bArr3[1];
                byte b6 = bArr3[2];
                int a = m3318a(b3, b4, b5, b6);
                if (b2 >= 0) {
                    m3321a(iVar, b2, b3, b4, b5, b6, a);
                }
                byte b7 = bArr3[i];
                if (z || i2 != length - 3 || b7 == 0) {
                    b = b7;
                } else {
                    int intValue = Integer.valueOf(C1298g.m3526a("HH")).intValue();
                    int intValue2 = Integer.valueOf(C1298g.m3526a("mm")).intValue();
                    a = m3318a(b5, b6, intValue, intValue2);
                    b = b7;
                    m3321a(iVar, b7, b5, b6, intValue, intValue2, a);
                }
                C1293b.m3504b("state: " + ((int) b) + " light: " + a);
                i2 += 3;
                b2 = b;
                b3 = b5;
                b4 = b6;
                i = 0;
            }
            iVar.mo10841d(iVar.mo10838c() + iVar.mo10836b());
        }
        return iVar;
    }

    /* renamed from: a */
    private static void m3321a(C1172i iVar, int i, int i2, int i3, int i4, int i5, int i6) {
        if (i == 0) {
            iVar.mo10839c(iVar.mo10840d() + i6);
        } else if (i == 1) {
            iVar.mo10834a(iVar.mo10836b() + i6);
        } else if (i == 2) {
            iVar.mo10837b(iVar.mo10838c() + i6);
        }
        C1172i.C1173a a = m3319a(i, i2, i3, i4, i5, i6);
        if (iVar.mo10833a() == null) {
            iVar.mo10835a(new ArrayList());
        }
        if (a != null) {
            iVar.mo10833a().add(a);
        }
    }
}
