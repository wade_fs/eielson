package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import com.crrepa.ble.nrf.dfu.C1327g;

/* renamed from: com.crrepa.ble.nrf.dfu.k */
class C1333k implements C1327g {

    /* renamed from: a */
    private C1322c f2104a;

    /* renamed from: b */
    private boolean f2105b;

    /* renamed from: c */
    private boolean f2106c;

    C1333k() {
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C1332j mo11172a(Intent intent, DfuBaseService dfuBaseService, BluetoothGatt bluetoothGatt) {
        try {
            this.f2104a = new C1325e(intent, dfuBaseService);
            if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                C1322c cVar = this.f2104a;
                C1322c cVar2 = this.f2104a;
                if (cVar2 != null) {
                    if (this.f2105b) {
                        cVar2.pause();
                    }
                    if (this.f2106c) {
                        this.f2104a.mo11124a();
                    }
                }
                return cVar;
            }
            this.f2104a = new C1326f(intent, dfuBaseService);
            if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                C1322c cVar3 = this.f2104a;
                C1322c cVar4 = this.f2104a;
                if (cVar4 != null) {
                    if (this.f2105b) {
                        cVar4.pause();
                    }
                    if (this.f2106c) {
                        this.f2104a.mo11124a();
                    }
                }
                return cVar3;
            }
            this.f2104a = new C1338o(intent, dfuBaseService);
            if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                C1322c cVar5 = this.f2104a;
                C1322c cVar6 = this.f2104a;
                if (cVar6 != null) {
                    if (this.f2105b) {
                        cVar6.pause();
                    }
                    if (this.f2106c) {
                        this.f2104a.mo11124a();
                    }
                }
                return cVar5;
            }
            this.f2104a = new C1335m(intent, dfuBaseService);
            if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                C1322c cVar7 = this.f2104a;
                C1322c cVar8 = this.f2104a;
                if (cVar8 != null) {
                    if (this.f2105b) {
                        cVar8.pause();
                    }
                    if (this.f2106c) {
                        this.f2104a.mo11124a();
                    }
                }
                return cVar7;
            }
            this.f2104a = new C1336n(intent, dfuBaseService);
            if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                C1322c cVar9 = this.f2104a;
                C1322c cVar10 = this.f2104a;
                if (cVar10 != null) {
                    if (this.f2105b) {
                        cVar10.pause();
                    }
                    if (this.f2106c) {
                        this.f2104a.mo11124a();
                    }
                }
                return cVar9;
            }
            if (intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_UNSAFE_EXPERIMENTAL_BUTTONLESS_DFU", false)) {
                this.f2104a = new C1334l(intent, dfuBaseService);
                if (this.f2104a.mo11154a(intent, bluetoothGatt)) {
                    C1322c cVar11 = this.f2104a;
                    C1322c cVar12 = this.f2104a;
                    if (cVar12 != null) {
                        if (this.f2105b) {
                            cVar12.pause();
                        }
                        if (this.f2106c) {
                            this.f2104a.mo11124a();
                        }
                    }
                    return cVar11;
                }
            }
            C1322c cVar13 = this.f2104a;
            if (cVar13 != null) {
                if (this.f2105b) {
                    cVar13.pause();
                }
                if (this.f2106c) {
                    this.f2104a.mo11124a();
                }
            }
            return null;
        } catch (Throwable th) {
            C1322c cVar14 = this.f2104a;
            if (cVar14 != null) {
                if (this.f2105b) {
                    cVar14.pause();
                }
                if (this.f2106c) {
                    this.f2104a.mo11124a();
                }
            }
            throw th;
        }
    }

    /* renamed from: a */
    public void mo11124a() {
        this.f2106c = true;
        C1322c cVar = this.f2104a;
        if (cVar != null) {
            cVar.mo11124a();
        }
    }

    /* renamed from: a */
    public void mo11125a(int i) {
        C1322c cVar = this.f2104a;
        if (cVar != null) {
            cVar.mo11125a(i);
        }
    }

    /* renamed from: b */
    public void mo11132b() {
        this.f2105b = false;
    }

    /* renamed from: c */
    public C1327g.C1328a mo11111c() {
        C1322c cVar = this.f2104a;
        if (cVar != null) {
            return cVar.mo11111c();
        }
        return null;
    }

    public void pause() {
        this.f2105b = true;
    }
}
