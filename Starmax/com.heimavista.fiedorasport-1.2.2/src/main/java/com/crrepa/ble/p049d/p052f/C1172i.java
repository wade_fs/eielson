package com.crrepa.ble.p049d.p052f;

import java.util.List;

/* renamed from: com.crrepa.ble.d.f.i */
public class C1172i {

    /* renamed from: a */
    private int f1839a;

    /* renamed from: b */
    private int f1840b;

    /* renamed from: c */
    private int f1841c;

    /* renamed from: d */
    private int f1842d;

    /* renamed from: e */
    private List<C1173a> f1843e;

    /* renamed from: com.crrepa.ble.d.f.i$a */
    public static class C1173a {

        /* renamed from: a */
        private int f1844a;

        /* renamed from: b */
        private int f1845b;

        /* renamed from: c */
        private int f1846c;

        /* renamed from: d */
        private int f1847d;

        /* renamed from: a */
        public int mo10843a() {
            return this.f1845b;
        }

        /* renamed from: a */
        public void mo10844a(int i) {
            this.f1845b = i;
        }

        /* renamed from: b */
        public int mo10845b() {
            return this.f1844a;
        }

        /* renamed from: b */
        public void mo10846b(int i) {
            this.f1844a = i;
        }

        /* renamed from: c */
        public int mo10847c() {
            return this.f1846c;
        }

        /* renamed from: c */
        public void mo10848c(int i) {
            this.f1846c = i;
        }

        /* renamed from: d */
        public int mo10849d() {
            return this.f1847d;
        }

        /* renamed from: d */
        public void mo10850d(int i) {
            this.f1847d = i;
        }
    }

    /* renamed from: a */
    public List<C1173a> mo10833a() {
        return this.f1843e;
    }

    /* renamed from: a */
    public void mo10834a(int i) {
        this.f1841c = i;
    }

    /* renamed from: a */
    public void mo10835a(List<C1173a> list) {
        this.f1843e = list;
    }

    /* renamed from: b */
    public int mo10836b() {
        return this.f1841c;
    }

    /* renamed from: b */
    public void mo10837b(int i) {
        this.f1840b = i;
    }

    /* renamed from: c */
    public int mo10838c() {
        return this.f1840b;
    }

    /* renamed from: c */
    public void mo10839c(int i) {
        this.f1842d = i;
    }

    /* renamed from: d */
    public int mo10840d() {
        return this.f1842d;
    }

    /* renamed from: d */
    public void mo10841d(int i) {
        this.f1839a = i;
    }

    /* renamed from: e */
    public int mo10842e() {
        return this.f1839a;
    }
}
