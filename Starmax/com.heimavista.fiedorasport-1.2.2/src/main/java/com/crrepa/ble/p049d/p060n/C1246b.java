package com.crrepa.ble.p049d.p060n;

import android.bluetooth.BluetoothGattCharacteristic;
import com.crrepa.ble.p049d.p053g.C1179a;
import com.crrepa.ble.p049d.p061o.C1252c;
import com.crrepa.ble.p068f.C1293b;

/* renamed from: com.crrepa.ble.d.n.b */
public class C1246b extends C1249c {

    /* renamed from: a */
    private byte[] f1945a;

    /* renamed from: b */
    private int f1946b;

    /* renamed from: c */
    private boolean f1947c;

    /* renamed from: d */
    private boolean f1948d;

    /* renamed from: com.crrepa.ble.d.n.b$a */
    class C1247a implements Runnable {
        C1247a() {
        }

        public void run() {
            C1246b.this.m3345e();
            C1252c.m3359d().mo10983b();
        }
    }

    /* renamed from: com.crrepa.ble.d.n.b$b */
    private static class C1248b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1246b f1950a = new C1246b(null);
    }

    private C1246b() {
        this.f1945a = null;
        this.f1946b = 0;
        this.f1947c = true;
        this.f1948d = false;
    }

    /* synthetic */ C1246b(C1247a aVar) {
        this();
    }

    /* renamed from: a */
    private BluetoothGattCharacteristic m3340a(boolean z) {
        C1245a b = mo10971b();
        if (b == null) {
            return null;
        }
        if (z) {
            b.mo10965a();
            throw null;
        }
        b.mo10966b();
        throw null;
    }

    /* renamed from: a */
    private void m3342a(byte[] bArr, boolean z) {
        C1293b.m3504b("sendBleMessage: " + this.f1947c);
        if (this.f1947c) {
            this.f1945a = bArr;
            this.f1947c = false;
            this.f1948d = z;
            m3346f();
        }
    }

    /* renamed from: c */
    public static C1246b m3343c() {
        return C1248b.f1950a;
    }

    /* renamed from: d */
    private void m3344d() {
        C1179a.m3085a(new C1247a(), 50);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m3345e() {
        this.f1946b = 0;
        this.f1947c = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008e, code lost:
        return;
     */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m3346f() {
        /*
            r8 = this;
            monitor-enter(r8)
            byte[] r0 = r8.f1945a     // Catch:{ all -> 0x0094 }
            int r0 = r0.length     // Catch:{ all -> 0x0094 }
            int r1 = r8.f1946b     // Catch:{ all -> 0x0094 }
            int r0 = r0 - r1
            r1 = 20
            if (r0 <= r1) goto L_0x000e
            r0 = 20
            goto L_0x0015
        L_0x000e:
            if (r0 > 0) goto L_0x0015
            r8.m3344d()     // Catch:{ all -> 0x0094 }
            monitor-exit(r8)
            return
        L_0x0015:
            boolean r1 = r8.f1948d     // Catch:{ all -> 0x0094 }
            android.bluetooth.BluetoothGattCharacteristic r1 = r8.m3340a(r1)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r2.<init>()     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = "characteristic uuid: "
            r2.append(r3)     // Catch:{ all -> 0x0094 }
            java.util.UUID r3 = r1.getUuid()     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0094 }
            r2.append(r3)     // Catch:{ all -> 0x0094 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0094 }
            com.crrepa.ble.p068f.C1293b.m3504b(r2)     // Catch:{ all -> 0x0094 }
            com.crrepa.ble.d.m.a r2 = com.crrepa.ble.p049d.p059m.C1242a.m3332c()     // Catch:{ all -> 0x0094 }
            android.bluetooth.BluetoothGatt r2 = r2.mo10961a()     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x008f
            if (r2 != 0) goto L_0x0044
            goto L_0x008f
        L_0x0044:
            byte[] r3 = new byte[r0]     // Catch:{ all -> 0x0094 }
            byte[] r4 = r8.f1945a     // Catch:{ all -> 0x0094 }
            int r5 = r8.f1946b     // Catch:{ all -> 0x0094 }
            int r6 = r3.length     // Catch:{ all -> 0x0094 }
            r7 = 0
            java.lang.System.arraycopy(r4, r5, r3, r7, r6)     // Catch:{ all -> 0x0094 }
            r1.setValue(r3)     // Catch:{ all -> 0x0094 }
            r4 = 1
            r1.setWriteType(r4)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r4.<init>()     // Catch:{ all -> 0x0094 }
            java.lang.String r5 = "characteristic write data: "
            r4.append(r5)     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = com.crrepa.ble.p068f.C1295d.m3512b(r3)     // Catch:{ all -> 0x0094 }
            r4.append(r3)     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = r4.toString()     // Catch:{ all -> 0x0094 }
            com.crrepa.ble.p068f.C1293b.m3504b(r3)     // Catch:{ all -> 0x0094 }
            boolean r1 = r2.writeCharacteristic(r1)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r2.<init>()     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = "characteristic write success: "
            r2.append(r3)     // Catch:{ all -> 0x0094 }
            r2.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0094 }
            com.crrepa.ble.p068f.C1293b.m3504b(r2)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x008d
            int r1 = r8.f1946b     // Catch:{ all -> 0x0094 }
            int r1 = r1 + r0
            r8.f1946b = r1     // Catch:{ all -> 0x0094 }
        L_0x008d:
            monitor-exit(r8)
            return
        L_0x008f:
            r8.mo10970a()     // Catch:{ all -> 0x0094 }
            monitor-exit(r8)
            return
        L_0x0094:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.p049d.p060n.C1246b.m3346f():void");
    }

    /* renamed from: a */
    public void mo10967a(byte[] bArr) {
        m3342a(bArr, true);
    }

    /* renamed from: b */
    public void mo10968b(byte[] bArr) {
        m3342a(bArr, false);
    }
}
