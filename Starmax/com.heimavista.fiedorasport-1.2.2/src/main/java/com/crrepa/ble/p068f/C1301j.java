package com.crrepa.ble.p068f;

import android.graphics.Color;
import androidx.annotation.ColorInt;

/* renamed from: com.crrepa.ble.f.j */
public class C1301j {
    /* renamed from: a */
    public static int m3534a(@ColorInt int i) {
        int a = (m3535a(Color.red(i), 5) << 11) + (m3535a(Color.green(i), 6) << 5) + m3535a(Color.blue(i), 5);
        return a == 2081 ? a + 1 : a;
    }

    /* renamed from: a */
    private static int m3535a(int i, int i2) {
        return (i >> (8 - i2)) & 255;
    }
}
