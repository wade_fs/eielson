package com.crrepa.ble.p069g;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import androidx.annotation.Nullable;

/* renamed from: com.crrepa.ble.g.a */
public class C1302a {

    /* renamed from: a */
    private final BluetoothAdapter f2022a;

    public C1302a(@Nullable BluetoothAdapter bluetoothAdapter) {
        this.f2022a = bluetoothAdapter;
    }

    /* renamed from: a */
    public BluetoothDevice mo11072a(String str) {
        return this.f2022a.getRemoteDevice(str);
    }
}
