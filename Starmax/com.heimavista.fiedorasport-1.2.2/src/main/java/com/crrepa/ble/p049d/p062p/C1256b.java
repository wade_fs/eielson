package com.crrepa.ble.p049d.p062p;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattService;
import com.crrepa.ble.p049d.p053g.C1179a;
import com.crrepa.ble.p049d.p057k.C1220e;
import com.crrepa.ble.p049d.p059m.C1242a;
import com.crrepa.ble.p049d.p060n.C1245a;
import com.crrepa.ble.p068f.C1293b;
import java.util.List;

/* renamed from: com.crrepa.ble.d.p.b */
public class C1256b {

    /* renamed from: a */
    private C1255a f1971a;

    /* renamed from: b */
    private C1245a f1972b;

    /* renamed from: com.crrepa.ble.d.p.b$a */
    class C1257a implements Runnable {
        C1257a(C1256b bVar) {
        }

        public void run() {
            BluetoothGatt a = C1242a.m3332c().mo10961a();
            boolean discoverServices = a != null ? a.discoverServices() : false;
            C1293b.m3503a("discoverServices: " + discoverServices);
            if (!discoverServices) {
                new C1220e().mo10949b();
            }
        }
    }

    /* renamed from: com.crrepa.ble.d.p.b$b */
    private static class C1258b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1256b f1973a = new C1256b();
    }

    /* renamed from: d */
    public static C1256b m3384d() {
        return C1258b.f1973a;
    }

    /* renamed from: a */
    public C1255a mo10998a() {
        return this.f1971a;
    }

    /* renamed from: a */
    public boolean mo10999a(List<BluetoothGattService> list) {
        boolean z;
        if (list != null) {
            this.f1971a = new C1255a(list);
            z = this.f1971a.mo10997m();
        } else {
            z = false;
        }
        if (!z) {
            new C1220e().mo10949b();
        }
        return z;
    }

    /* renamed from: b */
    public C1245a mo11000b() {
        return this.f1972b;
    }

    /* renamed from: c */
    public void mo11001c() {
        C1179a.m3085a(new C1257a(this), 0);
    }
}
