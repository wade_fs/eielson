package com.crrepa.ble.p064e;

import com.crrepa.ble.p068f.C1295d;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/* renamed from: com.crrepa.ble.e.e */
public class C1291e {

    /* renamed from: a */
    private RandomAccessFile f1996a;

    /* renamed from: b */
    private int f1997b;

    /* renamed from: c */
    private int f1998c;

    /* renamed from: d */
    private int f1999d;

    private C1291e(File file, int i, int i2) {
        this.f1998c = i;
        this.f1999d = i2;
        try {
            this.f1996a = new RandomAccessFile(file, "r");
            this.f1997b = (int) ((this.f1996a.length() - ((long) i2)) / ((long) this.f1998c));
        } catch (Exception e) {
            e.printStackTrace();
            mo11066a();
        }
    }

    /* renamed from: a */
    public static C1291e m3494a(File file, int i, int i2) {
        if (file == null || !file.exists()) {
            return null;
        }
        C1291e eVar = new C1291e(file, i, i2);
        if (eVar.f1996a == null) {
            return null;
        }
        return eVar;
    }

    /* renamed from: a */
    public void mo11066a() {
        try {
            if (this.f1996a != null) {
                this.f1996a.close();
                this.f1996a = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public byte[] mo11067a(int i) {
        int i2 = this.f1998c;
        byte[] bArr = new byte[i2];
        try {
            this.f1996a.seek((long) (this.f1999d + (i * i2)));
            int read = this.f1996a.read(bArr);
            if (read == this.f1998c) {
                return bArr;
            }
            if (read == -1) {
                return null;
            }
            byte[] bArr2 = new byte[read];
            System.arraycopy(bArr, 0, bArr2, 0, read);
            return bArr2;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public int mo11068b() {
        return this.f1998c;
    }

    /* renamed from: c */
    public int mo11069c() {
        byte[] bArr;
        try {
            this.f1996a.seek((long) this.f1999d);
            int i = 65258;
            byte[] bArr2 = new byte[4096];
            while (true) {
                int read = this.f1996a.read(bArr2);
                if (read == -1) {
                    return i;
                }
                if (read == 4096) {
                    bArr = bArr2;
                } else {
                    bArr = new byte[read];
                    System.arraycopy(bArr2, 0, bArr, 0, read);
                }
                byte[] a = C1288b.m3470a(bArr, i);
                i = C1295d.m3507a(a[0], a[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* renamed from: d */
    public long mo11070d() {
        try {
            return this.f1996a.length() - ((long) this.f1999d);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* renamed from: e */
    public int mo11071e() {
        return this.f1997b + 1;
    }
}
