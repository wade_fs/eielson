package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1164b;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.d.l.d */
public class C1227d {
    /* renamed from: a */
    public static C1164b m3294a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        int i = 1;
        if (bArr.length < 1) {
            return null;
        }
        C1164b bVar = new C1164b();
        if (bArr[0] != -1) {
            bVar.mo10792a(true);
            i = 0;
        }
        ArrayList arrayList = new ArrayList();
        int length = bArr.length;
        while (i < length) {
            byte b = bArr[i];
            if (b > 0) {
                arrayList.add(Integer.valueOf(b));
            }
            i++;
        }
        bVar.mo10791a(arrayList);
        return bVar;
    }
}
