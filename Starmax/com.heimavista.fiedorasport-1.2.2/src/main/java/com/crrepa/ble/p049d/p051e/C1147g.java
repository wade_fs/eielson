package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p049d.p052f.C1163a;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/* renamed from: com.crrepa.ble.d.e.g */
public class C1147g {
    /* renamed from: a */
    private static Date m2960a(int i) {
        if (i == 0) {
            return null;
        }
        String binaryString = Integer.toBinaryString(i);
        int length = 16 - binaryString.length();
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < length; i2++) {
            sb.append(0);
        }
        sb.append(binaryString);
        String sb2 = sb.toString();
        String substring = sb2.substring(0, 4);
        String substring2 = sb2.substring(4, 8);
        int a = C1295d.m3508a(sb2.substring(8, 16));
        Calendar instance = Calendar.getInstance();
        instance.set(1, C1295d.m3508a(substring) + 2015);
        instance.set(2, C1295d.m3508a(substring2) - 1);
        instance.set(5, a);
        return instance.getTime();
    }

    /* renamed from: a */
    public static List<C1163a> m2961a(byte[] bArr) {
        Date date;
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        byte[] bArr2 = new byte[8];
        int length = bArr.length / bArr2.length;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < length; i++) {
            System.arraycopy(bArr, bArr2.length * i, bArr2, 0, 8);
            C1293b.m3504b("alarm data: " + C1295d.m3512b(bArr2));
            boolean z = bArr2[1] == 1;
            byte b = bArr2[2];
            byte b2 = bArr2[3];
            byte b3 = bArr2[4];
            byte b4 = bArr2[7];
            if (b == 1) {
                date = null;
                b4 = Byte.MAX_VALUE;
            } else if (b == 0) {
                date = m2960a(C1295d.m3507a(bArr2[5], bArr2[6]));
                b4 = 0;
            } else {
                date = null;
            }
            C1163a aVar = new C1163a(i, b2, b3, b4, z);
            aVar.mo10784a(date);
            arrayList.add(aVar);
        }
        return arrayList;
    }

    /* renamed from: a */
    public static byte[] m2962a() {
        return C1146f.m2959a(33, null);
    }

    /* renamed from: a */
    public static byte[] m2963a(C1163a aVar) {
        int i;
        byte[] bArr = new byte[8];
        bArr[0] = (byte) aVar.mo10786c();
        byte b = 1;
        bArr[1] = aVar.mo10789f() ? (byte) 1 : 0;
        int e = aVar.mo10788e();
        if (e == 0) {
            Calendar instance = Calendar.getInstance();
            if (aVar.mo10783a() != null) {
                instance.setTime(aVar.mo10783a());
            }
            i = ((instance.get(1) - 2015) << 12) + ((instance.get(2) + 1) << 8) + instance.get(5);
            b = 0;
        } else if (e == 127) {
            i = 0;
        } else {
            i = 0;
            b = 2;
        }
        bArr[2] = b;
        bArr[3] = (byte) aVar.mo10785b();
        bArr[4] = (byte) aVar.mo10787d();
        System.arraycopy(C1295d.m3510a(i), 0, bArr, 5, 2);
        bArr[7] = (byte) e;
        return C1146f.m2959a(17, bArr);
    }
}
