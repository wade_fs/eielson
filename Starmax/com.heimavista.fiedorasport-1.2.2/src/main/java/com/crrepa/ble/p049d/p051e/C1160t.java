package com.crrepa.ble.p049d.p051e;

/* renamed from: com.crrepa.ble.d.e.t */
public class C1160t {
    /* renamed from: a */
    public static boolean m2992a(byte[] bArr) {
        return bArr != null && bArr.length > 0 && bArr[0] == 1;
    }

    /* renamed from: a */
    public static byte[] m2993a() {
        return C1146f.m2959a(45, null);
    }

    /* renamed from: a */
    public static byte[] m2994a(boolean z) {
        return C1146f.m2959a(29, new byte[]{z ? (byte) 1 : 0});
    }
}
