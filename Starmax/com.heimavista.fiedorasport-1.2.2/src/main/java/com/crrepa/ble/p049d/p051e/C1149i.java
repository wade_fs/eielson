package com.crrepa.ble.p049d.p051e;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: com.crrepa.ble.d.e.i */
public class C1149i {
    /* renamed from: a */
    public static int m2967a(byte[] bArr) {
        String str = new String(bArr);
        if (!str.contains("DFU")) {
            return 0;
        }
        Matcher matcher = Pattern.compile("\\d+").matcher(str);
        ArrayList arrayList = new ArrayList();
        while (matcher.find()) {
            arrayList.add(Integer.valueOf(matcher.group(0)));
        }
        return (!arrayList.isEmpty() && ((Integer) arrayList.get(0)).intValue() > 0) ? 1 : 0;
    }
}
