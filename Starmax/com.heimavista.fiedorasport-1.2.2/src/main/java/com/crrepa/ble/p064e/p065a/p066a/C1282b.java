package com.crrepa.ble.p064e.p065a.p066a;

import android.graphics.Bitmap;
import com.crrepa.ble.R$string;
import com.crrepa.ble.p049d.p051e.C1146f;
import com.crrepa.ble.p049d.p054h.C1206z;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p049d.p059m.C1242a;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.crrepa.ble.p064e.C1288b;
import com.crrepa.ble.p064e.C1290d;
import com.crrepa.ble.p064e.p065a.p066a.p067a.C1277a;
import com.crrepa.ble.p064e.p065a.p066a.p067a.C1278b;
import com.crrepa.ble.p064e.p065a.p066a.p067a.C1279c;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import com.crrepa.ble.p068f.C1296e;
import com.crrepa.ble.trans.upgrade.p081a.C1363a;

/* renamed from: com.crrepa.ble.e.a.a.b */
public class C1282b {

    /* renamed from: a */
    private C1275q f1985a;

    /* renamed from: b */
    private byte[] f1986b;

    /* renamed from: c */
    private int f1987c;

    /* renamed from: d */
    private int f1988d;

    /* renamed from: e */
    private int f1989e;

    /* renamed from: com.crrepa.ble.e.a.a.b$a */
    class C1283a implements C1277a {
        C1283a() {
        }

        /* renamed from: a */
        public void mo11038a(C1278b bVar) {
            C1282b.this.m3440a(bVar);
        }
    }

    /* renamed from: com.crrepa.ble.e.a.a.b$b */
    private static class C1284b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C1282b f1991a = new C1282b(null);
    }

    private C1282b() {
        this.f1987c = 0;
        this.f1988d = 0;
        this.f1989e = C1206z.m3111b().mo10896a() ? C1242a.m3332c().mo10964b() : 256;
    }

    /* synthetic */ C1282b(C1283a aVar) {
        this();
    }

    /* renamed from: a */
    private void m3439a(int i) {
        int d = m3449d();
        C1293b.m3504b("transFileCrc: " + i);
        C1293b.m3504b("calcFileCrc: " + d);
        boolean z = i == d;
        m3443a(z);
        if (!z) {
            m3447c(mo11052a().length);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3440a(C1278b bVar) {
        byte[] d = bVar.mo11045d();
        if (d.length > 0) {
            mo11051a(d);
            m3447c(d.length);
            return;
        }
        m3442a(C1296e.m3518a().getString(R$string.trans_error_msg));
    }

    /* renamed from: a */
    private void m3442a(String str) {
        C1275q qVar = this.f1985a;
        if (qVar != null) {
            qVar.onError(str);
        }
        mo11053b();
    }

    /* renamed from: a */
    private void m3443a(boolean z) {
        byte[] bArr = new byte[4];
        if (z) {
            m3451e();
        } else {
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = -1;
            }
        }
        m3445b(C1146f.m2959a(110, bArr));
    }

    /* renamed from: b */
    private void m3444b(int i) {
        int i2;
        int i3;
        C1275q qVar = this.f1985a;
        if (qVar != null && (i2 = this.f1987c) != 0 && (i3 = (i * 100) / i2) != this.f1988d) {
            this.f1988d = i3;
            qVar.mo11034a(i3);
        }
    }

    /* renamed from: b */
    private void m3445b(byte[] bArr) {
        C1217c.m3269e().mo10937a(bArr);
    }

    /* renamed from: c */
    public static C1282b m3446c() {
        return C1284b.f1991a;
    }

    /* renamed from: c */
    private void m3447c(int i) {
        m3445b(C1146f.m2959a(110, C1295d.m3511a((long) i)));
        this.f1987c = (i / this.f1989e) + 1;
    }

    /* renamed from: c */
    private void m3448c(byte[] bArr) {
        C1217c.m3269e().mo10940c(bArr);
    }

    /* renamed from: d */
    private int m3449d() {
        byte[] a = C1288b.m3470a(mo11052a(), 65258);
        return C1295d.m3507a(a[0], a[1]);
    }

    /* renamed from: d */
    private void m3450d(int i) {
        int i2 = this.f1989e;
        int i3 = i * i2;
        int i4 = i2 + i3;
        byte[] a = mo11052a();
        if (a != null) {
            int length = a.length;
            if (length < i4) {
                i4 = length;
            }
            byte[] bArr = new byte[(i4 - i3)];
            System.arraycopy(a, i3, bArr, 0, bArr.length);
            m3448c(C1290d.m3492a(bArr, this.f1989e));
        }
    }

    /* renamed from: e */
    private void m3451e() {
        C1275q qVar = this.f1985a;
        if (qVar != null) {
            qVar.mo11033a();
        }
        mo11053b();
    }

    /* renamed from: f */
    private void m3452f() {
        C1275q qVar = this.f1985a;
        if (qVar != null) {
            qVar.mo11035b();
        }
    }

    /* renamed from: a */
    public void mo11048a(Bitmap bitmap, boolean z) {
        if (bitmap == null) {
            m3442a(C1296e.m3518a().getString(R$string.trans_error_msg));
            return;
        }
        m3452f();
        C1279c.m3436a().mo11046a(bitmap, z, new C1283a());
    }

    /* renamed from: a */
    public void mo11049a(C1275q qVar) {
        this.f1985a = qVar;
    }

    /* renamed from: a */
    public void mo11050a(C1363a aVar) {
        int b = aVar.mo11213b();
        C1293b.m3504b("watch face trans offset: " + b);
        if (b >= 0) {
            if (b == 65535) {
                m3439a(aVar.mo11212a());
                return;
            }
            m3450d(b);
            m3444b(b);
        }
    }

    /* renamed from: a */
    public void mo11051a(byte[] bArr) {
        this.f1986b = bArr;
    }

    /* renamed from: a */
    public byte[] mo11052a() {
        return this.f1986b;
    }

    /* renamed from: b */
    public void mo11053b() {
        this.f1986b = null;
    }
}
