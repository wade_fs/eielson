package com.crrepa.ble.p070h.p071a;

/* renamed from: com.crrepa.ble.h.a.b */
public final class C1304b {
    /* renamed from: a */
    public static String m3539a(int i) {
        int i2 = i & -8193;
        if (i2 == 2) {
            return "REMOTE DFU INVALID STATE";
        }
        if (i2 == 3) {
            return "REMOTE DFU NOT SUPPORTED";
        }
        if (i2 == 4) {
            return "REMOTE DFU DATA SIZE EXCEEDS LIMIT";
        }
        if (i2 == 5) {
            return "REMOTE DFU INVALID CRC ERROR";
        }
        if (i2 == 6) {
            return "REMOTE DFU OPERATION FAILED";
        }
        return "UNKNOWN (" + i + ")";
    }
}
