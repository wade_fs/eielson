package com.crrepa.ble.nrf.dfu.p077q.p078c;

/* renamed from: com.crrepa.ble.nrf.dfu.q.c.d */
public class C1349d extends Exception {
    private static final long serialVersionUID = -6901728550661937942L;

    /* renamed from: P */
    private final int f2185P;

    public C1349d(String str, int i) {
        super(str);
        this.f2185P = i;
    }

    /* renamed from: a */
    public int mo11199a() {
        return this.f2185P;
    }

    public String getMessage() {
        return super.getMessage() + " (error " + this.f2185P + ")";
    }
}
