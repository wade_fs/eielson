package com.crrepa.ble.p049d.p052f;

import java.util.List;

/* renamed from: com.crrepa.ble.d.f.c */
public class C1165c {

    /* renamed from: a */
    private long f1816a;

    /* renamed from: b */
    private List<Integer> f1817b;

    /* renamed from: c */
    private int f1818c;

    /* renamed from: d */
    private C1166a f1819d;

    /* renamed from: com.crrepa.ble.d.f.c$a */
    public enum C1166a {
        PART_HEART_RATE,
        TODAY_HEART_RATE,
        YESTERDAY_HEART_RATE
    }

    public C1165c(long j, List<Integer> list, int i, C1166a aVar) {
        this.f1816a = j;
        this.f1817b = list;
        this.f1818c = i;
        this.f1819d = aVar;
    }

    /* renamed from: a */
    public C1166a mo10793a() {
        return this.f1819d;
    }

    /* renamed from: b */
    public List<Integer> mo10794b() {
        return this.f1817b;
    }

    /* renamed from: c */
    public long mo10795c() {
        return this.f1816a;
    }

    /* renamed from: d */
    public int mo10796d() {
        return this.f1818c;
    }
}
