package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p051e.C1142b;
import com.crrepa.ble.p049d.p051e.C1153m;
import com.crrepa.ble.p049d.p052f.C1165c;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p068f.C1295d;
import com.crrepa.ble.p068f.C1297f;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.crrepa.ble.d.l.e */
public class C1228e {

    /* renamed from: a */
    private List<Integer> f1935a = new ArrayList();

    /* renamed from: b */
    private List<Integer> f1936b = new ArrayList();

    /* renamed from: c */
    private List<Integer> f1937c = new ArrayList();

    /* renamed from: d */
    private List<Integer> f1938d = new ArrayList();

    /* renamed from: e */
    private long f1939e;

    /* renamed from: a */
    private C1165c m3295a() {
        List<Integer> list = this.f1935a;
        if (list == null || list.size() < 1) {
            return null;
        }
        if (this.f1935a.get(0).intValue() <= 0) {
            this.f1935a.remove(0);
        }
        return new C1165c(C1142b.m2950a(this.f1939e), this.f1935a, 1, C1165c.C1166a.PART_HEART_RATE);
    }

    /* renamed from: a */
    private List<Integer> m3296a(List<Integer> list, int i) {
        int a = C1297f.m3523a() / i;
        if (list.size() <= a) {
            return list;
        }
        while (a < list.size()) {
            list.set(a, 0);
            a++;
        }
        return list;
    }

    /* renamed from: a */
    private void m3297a(byte b) {
        C1217c.m3269e().mo10937a(C1153m.m2975a(b));
    }

    /* renamed from: b */
    private void m3298b() {
        C1217c.m3269e().mo10937a(C1153m.m2974a());
    }

    /* renamed from: b */
    private void m3299b(byte b) {
        C1217c.m3269e().mo10937a(C1153m.m2979b(b));
    }

    /* renamed from: g */
    private List<Integer> m3300g(byte[] bArr) {
        List<Integer> h = m3301h(bArr);
        h.remove(0);
        return h;
    }

    /* renamed from: h */
    private List<Integer> m3301h(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        if (bArr != null && bArr.length >= 1) {
            for (byte b : bArr) {
                int a = C1295d.m3506a(b);
                if (a < 40 || a > 200) {
                    a = 0;
                }
                arrayList.add(Integer.valueOf(a));
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public C1165c mo10954a(byte[] bArr) {
        if (bArr != null && bArr.length >= 1) {
            byte b = bArr[0];
            if (b == 0) {
                this.f1938d = new ArrayList();
            }
            this.f1938d.addAll(m3300g(bArr));
            if (19 == b) {
                long a = C1297f.m3524a(0);
                List<Integer> list = this.f1938d;
                m3296a(list, 1);
                this.f1938d = list;
                return new C1165c(a, this.f1938d, 1, C1165c.C1166a.TODAY_HEART_RATE);
            }
            m3297a((byte) (b + 1));
        }
        return null;
    }

    /* renamed from: b */
    public C1165c mo10955b(byte[] bArr) {
        byte[] bArr2;
        if (bArr != null && bArr.length >= 1) {
            int a = C1295d.m3506a(bArr[0]);
            if (a != 0) {
                if (a == 1) {
                    byte[] bArr3 = new byte[(bArr.length - 1)];
                    System.arraycopy(bArr, 1, bArr3, 0, bArr3.length);
                    bArr2 = bArr3;
                } else if (a == 2) {
                    return m3295a();
                } else {
                    bArr2 = null;
                }
            } else if (bArr.length < 5) {
                return null;
            } else {
                this.f1935a.clear();
                byte[] bArr4 = new byte[4];
                System.arraycopy(bArr, 1, bArr4, 0, 4);
                this.f1939e = C1295d.m3513c(bArr4) * 1000;
                bArr2 = new byte[(bArr.length - 5)];
                System.arraycopy(bArr, 5, bArr2, 0, bArr2.length);
            }
            this.f1935a.addAll(m3301h(bArr2));
            m3298b();
        }
        return null;
    }

    /* renamed from: c */
    public int mo10956c(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return -1;
        }
        return C1295d.m3506a(bArr[1]);
    }

    /* renamed from: d */
    public int mo10957d(byte[] bArr) {
        byte b = 0;
        if (bArr != null && bArr.length > 0) {
            b = bArr[0];
        }
        return C1295d.m3506a(b);
    }

    /* renamed from: e */
    public boolean mo10958e(byte[] bArr) {
        return (bArr == null || bArr.length < 1 || bArr[0] == 0) ? false : true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.crrepa.ble.p049d.p052f.C1165c mo10959f(byte[] r14) {
        /*
            r13 = this;
            r0 = 0
            if (r14 == 0) goto L_0x005e
            int r1 = r14.length
            r2 = 1
            if (r1 >= r2) goto L_0x0008
            goto L_0x005e
        L_0x0008:
            r1 = 0
            byte r3 = r14[r1]
            if (r3 != 0) goto L_0x0013
            java.util.List<java.lang.Integer> r4 = r13.f1936b
        L_0x000f:
            r4.clear()
            goto L_0x0019
        L_0x0013:
            r4 = 4
            if (r4 != r3) goto L_0x0019
            java.util.List<java.lang.Integer> r4 = r13.f1937c
            goto L_0x000f
        L_0x0019:
            java.util.List r14 = r13.m3300g(r14)
            r4 = 7
            r5 = 3
            if (r3 > r5) goto L_0x0027
            java.util.List<java.lang.Integer> r6 = r13.f1936b
        L_0x0023:
            r6.addAll(r14)
            goto L_0x002c
        L_0x0027:
            if (r3 > r4) goto L_0x002c
            java.util.List<java.lang.Integer> r6 = r13.f1937c
            goto L_0x0023
        L_0x002c:
            if (r5 != r3) goto L_0x0046
            long r8 = com.crrepa.ble.p068f.C1297f.m3524a(r1)
            java.util.List<java.lang.Integer> r14 = r13.f1936b
            r0 = 5
            r13.m3296a(r14, r0)
            r13.f1936b = r14
            com.crrepa.ble.d.f.c r14 = new com.crrepa.ble.d.f.c
            java.util.List<java.lang.Integer> r10 = r13.f1936b
            com.crrepa.ble.d.f.c$a r12 = com.crrepa.ble.p049d.p052f.C1165c.C1166a.TODAY_HEART_RATE
            r11 = 5
            r7 = r14
            r7.<init>(r8, r10, r11, r12)
            return r14
        L_0x0046:
            if (r4 != r3) goto L_0x0059
            r14 = -1
            long r1 = com.crrepa.ble.p068f.C1297f.m3524a(r14)
            com.crrepa.ble.d.f.c r14 = new com.crrepa.ble.d.f.c
            java.util.List<java.lang.Integer> r3 = r13.f1937c
            com.crrepa.ble.d.f.c$a r5 = com.crrepa.ble.p049d.p052f.C1165c.C1166a.YESTERDAY_HEART_RATE
            r4 = 5
            r0 = r14
            r0.<init>(r1, r3, r4, r5)
            return r14
        L_0x0059:
            int r3 = r3 + r2
            byte r14 = (byte) r3
            r13.m3299b(r14)
        L_0x005e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.p049d.p058l.C1228e.mo10959f(byte[]):com.crrepa.ble.d.f.c");
    }
}
