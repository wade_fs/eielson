package com.crrepa.ble.nrf.dfu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import androidx.core.internal.view.SupportMenu;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.crrepa.ble.R$drawable;
import com.crrepa.ble.R$string;
import com.crrepa.ble.nrf.dfu.C1330i;
import com.crrepa.ble.nrf.dfu.p077q.C1344a;
import com.crrepa.ble.nrf.dfu.p077q.C1345b;
import com.google.android.exoplayer2.C1750C;
import com.tencent.p214mm.opensdk.modelbase.BaseResp;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Locale;

public abstract class DfuBaseService extends IntentService implements C1330i.C1331a {

    /* renamed from: f0 */
    static boolean f2031f0 = false;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final Object f2032P = new Object();

    /* renamed from: Q */
    private BluetoothAdapter f2033Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public String f2034R;

    /* renamed from: S */
    private String f2035S;

    /* renamed from: T */
    private boolean f2036T;

    /* renamed from: U */
    protected int f2037U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public int f2038V;

    /* renamed from: W */
    private int f2039W = -1;

    /* renamed from: X */
    C1330i f2040X;

    /* renamed from: Y */
    private long f2041Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public boolean f2042Z;
    /* access modifiers changed from: private */

    /* renamed from: a0 */
    public C1327g f2043a0;

    /* renamed from: b0 */
    private final BroadcastReceiver f2044b0 = new C1314a();

    /* renamed from: c0 */
    private final BroadcastReceiver f2045c0 = new C1315b();

    /* renamed from: d0 */
    private final BroadcastReceiver f2046d0 = new C1316c();

    /* renamed from: e0 */
    private final BluetoothGattCallback f2047e0 = new C1317d();

    /* renamed from: com.crrepa.ble.nrf.dfu.DfuBaseService$a */
    class C1314a extends BroadcastReceiver {
        C1314a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.DfuBaseService, int]
         candidates:
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, int):int
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, java.lang.String):void
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(java.lang.String, java.lang.Throwable):void
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(int, java.lang.String):void
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, int):void
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, boolean):void
          com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, boolean):boolean */
        public void onReceive(Context context, Intent intent) {
            int intExtra = intent.getIntExtra("no.nordicsemi.android.dfu.extra.EXTRA_ACTION", 0);
            DfuBaseService dfuBaseService = DfuBaseService.this;
            dfuBaseService.m3576c("User action received: " + intExtra);
            if (intExtra == 0) {
                DfuBaseService.this.mo11087a(15, "[Broadcast] Pause action received");
                if (DfuBaseService.this.f2043a0 != null) {
                    DfuBaseService.this.f2043a0.pause();
                }
            } else if (intExtra == 1) {
                DfuBaseService.this.mo11087a(15, "[Broadcast] Resume action received");
                if (DfuBaseService.this.f2043a0 != null) {
                    DfuBaseService.this.f2043a0.mo11132b();
                }
            } else if (intExtra == 2) {
                DfuBaseService.this.mo11087a(15, "[Broadcast] Abort action received");
                boolean unused = DfuBaseService.this.f2042Z = true;
                if (DfuBaseService.this.f2043a0 != null) {
                    DfuBaseService.this.f2043a0.mo11124a();
                }
            }
        }
    }

    /* renamed from: com.crrepa.ble.nrf.dfu.DfuBaseService$b */
    class C1315b extends BroadcastReceiver {
        C1315b() {
        }

        public void onReceive(Context context, Intent intent) {
            int intExtra;
            if (((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getAddress().equals(DfuBaseService.this.f2034R) && (intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1)) != 11 && DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11125a(intExtra);
            }
        }
    }

    /* renamed from: com.crrepa.ble.nrf.dfu.DfuBaseService$c */
    class C1316c extends BroadcastReceiver {
        C1316c() {
        }

        public void onReceive(Context context, Intent intent) {
            if (((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getAddress().equals(DfuBaseService.this.f2034R)) {
                String action = intent.getAction();
                DfuBaseService dfuBaseService = DfuBaseService.this;
                dfuBaseService.m3576c("Action received: " + action);
                DfuBaseService dfuBaseService2 = DfuBaseService.this;
                dfuBaseService2.mo11087a(0, "[Broadcast] Action received: " + action);
            }
        }
    }

    /* renamed from: com.crrepa.ble.nrf.dfu.DfuBaseService$d */
    class C1317d extends BluetoothGattCallback {
        C1317d() {
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
            }
        }

        public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
            }
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e9 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onConnectionStateChange(android.bluetooth.BluetoothGatt r4, int r5, int r6) {
            /*
                r3 = this;
                r0 = 0
                if (r5 != 0) goto L_0x00a2
                r5 = 2
                if (r6 != r5) goto L_0x0081
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.String r6 = "Connected to GATT server"
                r5.m3576c(r6)
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r1 = "Connected to "
                r6.append(r1)
                com.crrepa.ble.nrf.dfu.DfuBaseService r1 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.String r1 = r1.f2034R
                r6.append(r1)
                java.lang.String r6 = r6.toString()
                r1 = 5
                r5.mo11087a(r1, r6)
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r6 = -2
                r5.f2037U = r6
                android.bluetooth.BluetoothDevice r5 = r4.getDevice()
                int r5 = r5.getBondState()
                r6 = 12
                if (r5 != r6) goto L_0x0049
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.String r6 = "Waiting 1600 ms for a possible Service Changed indication..."
                r5.m3576c(r6)
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r6 = 1600(0x640, float:2.242E-42)
                r5.mo11086a(r6)
            L_0x0049:
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r6 = 1
                java.lang.String r1 = "Discovering services..."
                r5.mo11087a(r6, r1)
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.String r6 = "gatt.discoverServices()"
                r5.mo11087a(r0, r6)
                boolean r4 = r4.discoverServices()
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r0 = "Attempting to start service discovery... "
                r6.append(r0)
                if (r4 == 0) goto L_0x006d
                java.lang.String r0 = "succeed"
                goto L_0x006f
            L_0x006d:
                java.lang.String r0 = "failed"
            L_0x006f:
                r6.append(r0)
                java.lang.String r6 = r6.toString()
                r5.m3576c(r6)
                if (r4 != 0) goto L_0x0080
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r5 = 4101(0x1005, float:5.747E-42)
                goto L_0x00df
            L_0x0080:
                return
            L_0x0081:
                if (r6 != 0) goto L_0x00e2
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.String r5 = "Disconnected from GATT server"
                r4.m3576c(r5)
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r4.f2037U = r0
                com.crrepa.ble.nrf.dfu.g r4 = r4.f2043a0
                if (r4 == 0) goto L_0x00e2
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                com.crrepa.ble.nrf.dfu.g r4 = r4.f2043a0
                com.crrepa.ble.nrf.dfu.g$a r4 = r4.mo11111c()
                r4.mo11146a()
                goto L_0x00e2
            L_0x00a2:
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Connection state change error: "
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = " newState: "
                r1.append(r2)
                r1.append(r6)
                java.lang.String r1 = r1.toString()
                r4.m3573b(r1)
                if (r6 != 0) goto L_0x00d9
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r4.f2037U = r0
                com.crrepa.ble.nrf.dfu.g r4 = r4.f2043a0
                if (r4 == 0) goto L_0x00d9
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                com.crrepa.ble.nrf.dfu.g r4 = r4.f2043a0
                com.crrepa.ble.nrf.dfu.g$a r4 = r4.mo11111c()
                r4.mo11146a()
            L_0x00d9:
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                r6 = 32768(0x8000, float:4.5918E-41)
                r5 = r5 | r6
            L_0x00df:
                int unused = r4.f2038V = r5
            L_0x00e2:
                com.crrepa.ble.nrf.dfu.DfuBaseService r4 = com.crrepa.ble.nrf.dfu.DfuBaseService.this
                java.lang.Object r4 = r4.f2032P
                monitor-enter(r4)
                com.crrepa.ble.nrf.dfu.DfuBaseService r5 = com.crrepa.ble.nrf.dfu.DfuBaseService.this     // Catch:{ all -> 0x00f4 }
                java.lang.Object r5 = r5.f2032P     // Catch:{ all -> 0x00f4 }
                r5.notifyAll()     // Catch:{ all -> 0x00f4 }
                monitor-exit(r4)     // Catch:{ all -> 0x00f4 }
                return
            L_0x00f4:
                r5 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x00f4 }
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.DfuBaseService.C1317d.onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int):void");
        }

        public void onDescriptorRead(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onDescriptorRead(bluetoothGatt, bluetoothGattDescriptor, i);
            }
        }

        public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
            }
        }

        @SuppressLint({"NewApi"})
        public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
            if (DfuBaseService.this.f2043a0 != null) {
                DfuBaseService.this.f2043a0.mo11111c().onMtuChanged(bluetoothGatt, i, i2);
            }
        }

        public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
            DfuBaseService dfuBaseService = DfuBaseService.this;
            if (i == 0) {
                dfuBaseService.m3576c("Services discovered");
                DfuBaseService.this.f2037U = -3;
            } else {
                dfuBaseService.m3573b("Service discovery error: " + i);
                int unused = DfuBaseService.this.f2038V = i | 16384;
            }
            synchronized (DfuBaseService.this.f2032P) {
                DfuBaseService.this.f2032P.notifyAll();
            }
        }
    }

    public DfuBaseService() {
        super("DfuBaseService");
    }

    /* renamed from: a */
    private InputStream m3563a(int i, String str, int i2, int i3) {
        InputStream openRawResource = getResources().openRawResource(i);
        if ("application/zip".equals(str)) {
            return new C1344a(openRawResource, i2, i3);
        }
        openRawResource.mark(2);
        int read = openRawResource.read();
        openRawResource.reset();
        return read == 58 ? new C1345b(openRawResource, i2) : openRawResource;
    }

    /* renamed from: a */
    private InputStream m3564a(Uri uri, String str, int i, int i2) {
        InputStream openInputStream = getContentResolver().openInputStream(uri);
        if ("application/zip".equals(str)) {
            return new C1344a(openInputStream, i, i2);
        }
        Cursor query = getContentResolver().query(uri, new String[]{"_display_name"}, null, null, null);
        try {
            if (query.moveToNext()) {
                if (query.getString(0).toLowerCase(Locale.US).endsWith("hex")) {
                    return new C1345b(openInputStream, i);
                }
            }
            query.close();
            return openInputStream;
        } finally {
            query.close();
        }
    }

    /* renamed from: a */
    private InputStream m3565a(String str, String str2, int i, int i2) {
        FileInputStream fileInputStream = new FileInputStream(str);
        return "application/zip".equals(str2) ? new C1344a(fileInputStream, i, i2) : str.toLowerCase(Locale.US).endsWith("hex") ? new C1345b(fileInputStream, i) : fileInputStream;
    }

    /* renamed from: a */
    private void m3567a(C1330i iVar) {
        Intent intent = new Intent("no.nordicsemi.android.dfu.broadcast.BROADCAST_PROGRESS");
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DATA", iVar.mo11164e());
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", this.f2034R);
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PART_CURRENT", iVar.mo11162d());
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PARTS_TOTAL", iVar.mo11168g());
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_SPEED_B_PER_MS", iVar.mo11166f());
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_AVG_SPEED_B_PER_MS", iVar.mo11158b());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /* renamed from: a */
    private void m3568a(String str, Throwable th) {
        Log.e("DfuBaseService", str, th);
    }

    /* renamed from: b */
    private void m3571b(int i) {
        m3575c(i);
        if (!this.f2036T) {
            String str = this.f2034R;
            String str2 = this.f2035S;
            if (str2 == null) {
                str2 = getString(R$string.dfu_unknown_name);
            }
            NotificationCompat.Builder autoCancel = new NotificationCompat.Builder(this).setSmallIcon(17301640).setOnlyAlertOnce(true).setColor(SupportMenu.CATEGORY_MASK).setOngoing(false).setContentTitle(getString(R$string.dfu_status_error)).setSmallIcon(17301641).setContentText(getString(R$string.dfu_status_error_msg)).setAutoCancel(true);
            Intent intent = new Intent(this, mo11091b());
            intent.addFlags(C1750C.ENCODING_PCM_MU_LAW);
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", str);
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_NAME", str2);
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PROGRESS", i);
            autoCancel.setContentIntent(PendingIntent.getActivity(this, 0, intent, 134217728));
            ((NotificationManager) getSystemService("notification")).notify(283, autoCancel.build());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m3573b(String str) {
        Log.e("DfuBaseService", str);
    }

    /* renamed from: c */
    private void m3575c(int i) {
        int i2;
        Intent intent = new Intent("no.nordicsemi.android.dfu.broadcast.BROADCAST_ERROR");
        if ((i & 16384) > 0) {
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DATA", i & -16385);
            i2 = 2;
        } else if ((32768 & i) > 0) {
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DATA", i & -32769);
            i2 = 1;
        } else {
            int i3 = i & 8192;
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DATA", i);
            i2 = i3 > 0 ? 3 : 0;
        }
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_ERROR_TYPE", i2);
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", this.f2034R);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m3576c(String str) {
        if (f2031f0) {
            Log.i("DfuBaseService", str);
        }
    }

    /* renamed from: d */
    private void m3577d(String str) {
        if (f2031f0) {
            Log.w("DfuBaseService", str);
        }
    }

    /* renamed from: e */
    private boolean m3578e() {
        String str;
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService("bluetooth");
        if (bluetoothManager == null) {
            str = "Unable to initialize BluetoothManager.";
        } else {
            this.f2033Q = bluetoothManager.getAdapter();
            if (this.f2033Q != null) {
                return true;
            }
            str = "Unable to obtain a BluetoothAdapter.";
        }
        m3573b(str);
        return false;
    }

    /* renamed from: f */
    private static IntentFilter m3579f() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("no.nordicsemi.android.dfu.broadcast.BROADCAST_ACTION");
        return intentFilter;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public BluetoothGatt mo11084a(String str) {
        if (!this.f2033Q.isEnabled()) {
            return null;
        }
        this.f2037U = -1;
        m3576c("Connecting to the device...");
        BluetoothDevice remoteDevice = this.f2033Q.getRemoteDevice(str);
        mo11087a(0, "gatt = device.connectGatt(autoConnect = false)");
        BluetoothGatt connectGatt = remoteDevice.connectGatt(this, false, this.f2047e0);
        try {
            synchronized (this.f2032P) {
                while (true) {
                    if ((this.f2037U == -1 || this.f2037U == -2) && this.f2038V == 0) {
                        this.f2032P.wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            m3568a("Sleeping interrupted", e);
        }
        return connectGatt;
    }

    /* renamed from: a */
    public void mo11085a() {
        String str;
        NotificationCompat.Builder builder;
        int i;
        String str2;
        C1330i iVar = this.f2040X;
        int e = iVar.mo11164e();
        if (this.f2039W != e) {
            this.f2039W = e;
            m3567a(iVar);
            if (!this.f2036T) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (elapsedRealtime - this.f2041Y >= 250) {
                    this.f2041Y = elapsedRealtime;
                    String str3 = this.f2034R;
                    String str4 = this.f2035S;
                    if (str4 == null) {
                        str4 = getString(R$string.dfu_unknown_name);
                    }
                    NotificationCompat.Builder onlyAlertOnce = new NotificationCompat.Builder(this).setSmallIcon(17301640).setOnlyAlertOnce(true);
                    onlyAlertOnce.setColor(-7829368);
                    switch (e) {
                        case -7:
                            onlyAlertOnce.setOngoing(false).setContentTitle(getString(R$string.dfu_status_aborted)).setSmallIcon(17301641).setContentText(getString(R$string.dfu_status_aborted_msg)).setAutoCancel(true);
                            break;
                        case BaseResp.ErrCode.ERR_BAN /*-6*/:
                            onlyAlertOnce.setOngoing(false).setContentTitle(getString(R$string.dfu_status_completed)).setSmallIcon(17301641).setContentText(getString(R$string.dfu_status_completed_msg)).setAutoCancel(true).setColor(-16730086);
                            break;
                        case -5:
                            builder = onlyAlertOnce.setOngoing(true).setContentTitle(getString(R$string.dfu_status_disconnecting));
                            str = getString(R$string.dfu_status_disconnecting_msg, new Object[]{str4});
                            builder.setContentText(str).setProgress(100, 0, true);
                            break;
                        case -4:
                            builder = onlyAlertOnce.setOngoing(true).setContentTitle(getString(R$string.dfu_status_validating));
                            i = R$string.dfu_status_validating_msg;
                            str = getString(i);
                            builder.setContentText(str).setProgress(100, 0, true);
                            break;
                        case -3:
                            builder = onlyAlertOnce.setOngoing(true).setContentTitle(getString(R$string.dfu_status_switching_to_dfu));
                            i = R$string.dfu_status_switching_to_dfu_msg;
                            str = getString(i);
                            builder.setContentText(str).setProgress(100, 0, true);
                            break;
                        case -2:
                            builder = onlyAlertOnce.setOngoing(true).setContentTitle(getString(R$string.dfu_status_starting));
                            i = R$string.dfu_status_starting_msg;
                            str = getString(i);
                            builder.setContentText(str).setProgress(100, 0, true);
                            break;
                        case -1:
                            builder = onlyAlertOnce.setOngoing(true).setContentTitle(getString(R$string.dfu_status_connecting));
                            str = getString(R$string.dfu_status_connecting_msg, new Object[]{str4});
                            builder.setContentText(str).setProgress(100, 0, true);
                            break;
                        default:
                            if (iVar.mo11168g() == 1) {
                                str2 = getString(R$string.dfu_status_uploading);
                            } else {
                                str2 = getString(R$string.dfu_status_uploading_part, new Object[]{Integer.valueOf(iVar.mo11162d()), Integer.valueOf(iVar.mo11168g())});
                            }
                            onlyAlertOnce.setOngoing(true).setContentTitle(str2).setContentText(getString(R$string.dfu_status_uploading_msg, new Object[]{str4})).setProgress(100, e, false);
                            break;
                    }
                    Intent intent = new Intent(this, mo11091b());
                    intent.addFlags(C1750C.ENCODING_PCM_MU_LAW);
                    intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", str3);
                    intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_NAME", str4);
                    intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_PROGRESS", e);
                    onlyAlertOnce.setContentIntent(PendingIntent.getActivity(this, 0, intent, 134217728));
                    if (!(e == -7 || e == -6)) {
                        Intent intent2 = new Intent("no.nordicsemi.android.dfu.broadcast.BROADCAST_ACTION");
                        intent2.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_ACTION", 2);
                        onlyAlertOnce.addAction(R$drawable.ic_action_notify_cancel, getString(R$string.dfu_action_abort), PendingIntent.getBroadcast(this, 1, intent2, 134217728));
                    }
                    ((NotificationManager) getSystemService("notification")).notify(283, onlyAlertOnce.build());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11086a(int i) {
        synchronized (this.f2032P) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("wait(");
                sb.append(i);
                sb.append(")");
                mo11087a(0, sb.toString());
                this.f2032P.wait((long) i);
            } catch (InterruptedException e) {
                m3568a("Sleeping interrupted", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11087a(int i, String str) {
        Intent intent = new Intent("no.nordicsemi.android.dfu.broadcast.BROADCAST_LOG");
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_LOG_INFO", "[DFU] " + str);
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_LOG_LEVEL", i);
        intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", this.f2034R);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11088a(BluetoothGatt bluetoothGatt) {
        m3576c("Cleaning up...");
        mo11087a(0, "gatt.close()");
        bluetoothGatt.close();
        this.f2037U = -5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, boolean):void
     arg types: [android.bluetooth.BluetoothGatt, int]
     candidates:
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, int):int
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, java.lang.String):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(java.lang.String, java.lang.Throwable):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, boolean):boolean
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(int, java.lang.String):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, int):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11089a(BluetoothGatt bluetoothGatt, int i) {
        if (this.f2037U != 0) {
            mo11092b(bluetoothGatt);
        }
        mo11090a(bluetoothGatt, false);
        mo11088a(bluetoothGatt);
        mo11086a(600);
        if (i != 0) {
            m3571b(i);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11090a(BluetoothGatt bluetoothGatt, boolean z) {
        if (z || bluetoothGatt.getDevice().getBondState() == 10) {
            mo11087a(0, "gatt.refresh() (hidden)");
            try {
                Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                if (method != null) {
                    boolean booleanValue = ((Boolean) method.invoke(bluetoothGatt, new Object[0])).booleanValue();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Refreshing result: ");
                    sb.append(booleanValue);
                    m3576c(sb.toString());
                }
            } catch (Exception e) {
                m3568a("An exception occurred while refreshing device", e);
                mo11087a(15, "Refreshing failed");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract Class<? extends Activity> mo11091b();

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11092b(BluetoothGatt bluetoothGatt) {
        if (this.f2037U != 0) {
            mo11087a(1, "Disconnecting...");
            this.f2040X.mo11165e(-5);
            this.f2037U = -4;
            m3576c("Disconnecting from the device...");
            mo11087a(0, "gatt.disconnect()");
            bluetoothGatt.disconnect();
            mo11094d();
            mo11087a(5, "Disconnected");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public boolean mo11093c() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11094d() {
        try {
            synchronized (this.f2032P) {
                while (this.f2037U != 0 && this.f2038V == 0) {
                    this.f2032P.wait();
                }
            }
        } catch (InterruptedException e) {
            m3568a("Sleeping interrupted", e);
        }
    }

    public void onCreate() {
        super.onCreate();
        f2031f0 = mo11093c();
        m3578e();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        IntentFilter f = m3579f();
        instance.registerReceiver(this.f2044b0, f);
        registerReceiver(this.f2044b0, f);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        registerReceiver(this.f2046d0, intentFilter);
        registerReceiver(this.f2045c0, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.f2044b0);
        unregisterReceiver(this.f2044b0);
        unregisterReceiver(this.f2046d0);
        unregisterReceiver(this.f2045c0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, boolean):void
     arg types: [android.bluetooth.BluetoothGatt, int]
     candidates:
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, int):int
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, java.lang.String):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(java.lang.String, java.lang.Throwable):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(com.crrepa.ble.nrf.dfu.DfuBaseService, boolean):boolean
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(int, java.lang.String):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, int):void
      com.crrepa.ble.nrf.dfu.DfuBaseService.a(android.bluetooth.BluetoothGatt, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x033b, code lost:
        if (r11 != null) goto L_0x03ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0397, code lost:
        if (r11 != null) goto L_0x03ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x03c8, code lost:
        if (r11 != null) goto L_0x03ca;
     */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x0353  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x03b5 A[Catch:{ h -> 0x03b6, a -> 0x039d, b -> 0x0347, all -> 0x0343, all -> 0x039a }] */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x03cf A[SYNTHETIC, Splitter:B:270:0x03cf] */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x03d5 A[SYNTHETIC, Splitter:B:274:0x03d5] */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x03d9 A[SYNTHETIC, Splitter:B:277:0x03d9] */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x041d A[SYNTHETIC, Splitter:B:299:0x041d] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0448 A[SYNTHETIC, Splitter:B:311:0x0448] */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0460 A[SYNTHETIC, Splitter:B:323:0x0460] */
    /* JADX WARNING: Removed duplicated region for block: B:335:0x0478 A[SYNTHETIC, Splitter:B:335:0x0478] */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x0490 A[SYNTHETIC, Splitter:B:347:0x0490] */
    /* JADX WARNING: Removed duplicated region for block: B:353:0x0499 A[SYNTHETIC, Splitter:B:353:0x0499] */
    /* JADX WARNING: Removed duplicated region for block: B:381:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:384:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:387:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:390:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:393:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:396:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ef A[SYNTHETIC, Splitter:B:47:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x010c A[Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0128 A[Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r20) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            java.lang.String r2 = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS"
            java.lang.String r2 = r0.getStringExtra(r2)
            java.lang.String r3 = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_NAME"
            java.lang.String r3 = r0.getStringExtra(r3)
            r8 = 0
            java.lang.String r4 = "no.nordicsemi.android.dfu.extra.EXTRA_DISABLE_NOTIFICATION"
            boolean r4 = r0.getBooleanExtra(r4, r8)
            java.lang.String r5 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_PATH"
            java.lang.String r5 = r0.getStringExtra(r5)
            java.lang.String r6 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_URI"
            android.os.Parcelable r6 = r0.getParcelableExtra(r6)
            android.net.Uri r6 = (android.net.Uri) r6
            java.lang.String r7 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_RES_ID"
            int r7 = r0.getIntExtra(r7, r8)
            java.lang.String r9 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_PATH"
            java.lang.String r9 = r0.getStringExtra(r9)
            java.lang.String r10 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_URI"
            android.os.Parcelable r10 = r0.getParcelableExtra(r10)
            android.net.Uri r10 = (android.net.Uri) r10
            java.lang.String r11 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_RES_ID"
            int r11 = r0.getIntExtra(r11, r8)
            java.lang.String r12 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_TYPE"
            int r12 = r0.getIntExtra(r12, r8)
            if (r5 == 0) goto L_0x005b
            if (r12 != 0) goto L_0x005b
            java.util.Locale r12 = java.util.Locale.US
            java.lang.String r12 = r5.toLowerCase(r12)
            java.lang.String r14 = "zip"
            boolean r12 = r12.endsWith(r14)
            if (r12 == 0) goto L_0x005a
            r12 = 0
            goto L_0x005b
        L_0x005a:
            r12 = 4
        L_0x005b:
            java.lang.String r14 = "no.nordicsemi.android.dfu.extra.EXTRA_MIME_TYPE"
            java.lang.String r14 = r0.getStringExtra(r14)
            java.lang.String r15 = "application/octet-stream"
            java.lang.String r8 = "application/zip"
            if (r14 == 0) goto L_0x0068
            goto L_0x006d
        L_0x0068:
            if (r12 != 0) goto L_0x006c
            r14 = r8
            goto L_0x006d
        L_0x006c:
            r14 = r15
        L_0x006d:
            r16 = r12 & -8
            if (r16 > 0) goto L_0x049d
            boolean r16 = r8.equals(r14)
            if (r16 != 0) goto L_0x007f
            boolean r16 = r15.equals(r14)
            if (r16 != 0) goto L_0x007f
            goto L_0x049d
        L_0x007f:
            boolean r15 = r15.equals(r14)
            r13 = 1
            if (r15 == 0) goto L_0x00a0
            if (r12 == r13) goto L_0x00a0
            r15 = 2
            if (r12 == r15) goto L_0x00a0
            r15 = 4
            if (r12 == r15) goto L_0x00a0
            java.lang.String r0 = "Unable to determine file type"
            r1.m3577d(r0)
            java.lang.String r0 = "Unable to determine file type"
        L_0x0095:
            r2 = 15
            r1.mo11087a(r2, r0)
            r0 = 4105(0x1009, float:5.752E-42)
            r1.m3571b(r0)
            return
        L_0x00a0:
            com.crrepa.ble.nrf.dfu.C1343p.m3724a(r20)
            r1.f2034R = r2
            r1.f2035S = r3
            r1.f2036T = r4
            r3 = 0
            r1.f2037U = r3
            r1.f2038V = r3
            android.content.SharedPreferences r3 = android.preference.PreferenceManager.getDefaultSharedPreferences(r19)
            r15 = 4096(0x1000, float:5.74E-42)
            java.lang.String r4 = java.lang.String.valueOf(r15)
            java.lang.String r15 = "settings_mbr_size"
            java.lang.String r3 = r3.getString(r15, r4)
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x00c6 }
            if (r3 >= 0) goto L_0x00c8
            r3 = 0
            goto L_0x00c8
        L_0x00c6:
            r3 = 4096(0x1000, float:5.74E-42)
        L_0x00c8:
            java.lang.String r4 = "DFU service started"
            r1.mo11087a(r13, r4)
            java.lang.String r4 = "Opening file..."
            r15 = 20
            r1.mo11087a(r13, r4)     // Catch:{ SecurityException -> 0x047c, FileNotFoundException -> 0x0464, f -> 0x044c, IOException -> 0x0421, Exception -> 0x03f6, all -> 0x03f2 }
            if (r6 == 0) goto L_0x00dd
            java.io.InputStream r3 = r1.m3564a(r6, r14, r3, r12)     // Catch:{ SecurityException -> 0x047c, FileNotFoundException -> 0x0464, f -> 0x044c, IOException -> 0x0421, Exception -> 0x03f6, all -> 0x03f2 }
        L_0x00da:
            r18 = r3
            goto L_0x00ed
        L_0x00dd:
            if (r5 == 0) goto L_0x00e4
            java.io.InputStream r3 = r1.m3565a(r5, r14, r3, r12)     // Catch:{ SecurityException -> 0x047c, FileNotFoundException -> 0x0464, f -> 0x044c, IOException -> 0x0421, Exception -> 0x03f6, all -> 0x03f2 }
            goto L_0x00da
        L_0x00e4:
            if (r7 <= 0) goto L_0x00eb
            java.io.InputStream r3 = r1.m3563a(r7, r14, r3, r12)     // Catch:{ SecurityException -> 0x047c, FileNotFoundException -> 0x0464, f -> 0x044c, IOException -> 0x0421, Exception -> 0x03f6, all -> 0x03f2 }
            goto L_0x00da
        L_0x00eb:
            r18 = 0
        L_0x00ed:
            if (r10 == 0) goto L_0x010c
            android.content.ContentResolver r3 = r19.getContentResolver()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.io.InputStream r3 = r3.openInputStream(r10)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            goto L_0x0120
        L_0x00f8:
            r0 = move-exception
            r15 = r18
            goto L_0x0497
        L_0x00fd:
            r0 = move-exception
            goto L_0x03e1
        L_0x0100:
            r0 = move-exception
            goto L_0x03e4
        L_0x0103:
            r0 = move-exception
            goto L_0x03e7
        L_0x0106:
            r0 = move-exception
            goto L_0x03ea
        L_0x0109:
            r0 = move-exception
            goto L_0x03ee
        L_0x010c:
            if (r9 == 0) goto L_0x0114
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3.<init>(r9)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            goto L_0x0120
        L_0x0114:
            if (r11 <= 0) goto L_0x011f
            android.content.res.Resources r3 = r19.getResources()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.io.InputStream r3 = r3.openRawResource(r11)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            goto L_0x0120
        L_0x011f:
            r3 = 0
        L_0x0120:
            int r4 = r18.available()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            int r5 = r4 % 4
            if (r5 != 0) goto L_0x03d9
            if (r12 != 0) goto L_0x0139
            boolean r5 = r8.equals(r14)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            if (r5 == 0) goto L_0x0139
            r5 = r18
            com.crrepa.ble.nrf.dfu.q.a r5 = (com.crrepa.ble.nrf.dfu.p077q.C1344a) r5     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            int r5 = r5.mo11182e()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            goto L_0x013a
        L_0x0139:
            r5 = r12
        L_0x013a:
            boolean r6 = r8.equals(r14)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            if (r6 == 0) goto L_0x01a5
            r6 = r18
            com.crrepa.ble.nrf.dfu.q.a r6 = (com.crrepa.ble.nrf.dfu.p077q.C1344a) r6     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r7 = r5 & 4
            if (r7 <= 0) goto L_0x0159
            int r7 = r6.mo11175a()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r8 = 4
            int r7 = r7 % r8
            if (r7 != 0) goto L_0x0151
            goto L_0x0159
        L_0x0151:
            com.crrepa.ble.nrf.dfu.q.c.f r0 = new com.crrepa.ble.nrf.dfu.q.c.f     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r2 = "Application firmware is not word-aligned."
            r0.<init>(r2)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            throw r0     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
        L_0x0159:
            r7 = r5 & 2
            if (r7 <= 0) goto L_0x016e
            int r7 = r6.mo11178b()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r8 = 4
            int r7 = r7 % r8
            if (r7 != 0) goto L_0x0166
            goto L_0x016e
        L_0x0166:
            com.crrepa.ble.nrf.dfu.q.c.f r0 = new com.crrepa.ble.nrf.dfu.q.c.f     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r2 = "Bootloader firmware is not word-aligned."
            r0.<init>(r2)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            throw r0     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
        L_0x016e:
            r7 = r5 & 1
            if (r7 <= 0) goto L_0x0183
            int r7 = r6.mo11190v()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r8 = 4
            int r7 = r7 % r8
            if (r7 != 0) goto L_0x017b
            goto L_0x0183
        L_0x017b:
            com.crrepa.ble.nrf.dfu.q.c.f r0 = new com.crrepa.ble.nrf.dfu.q.c.f     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r2 = "Soft Device firmware is not word-aligned."
            r0.<init>(r2)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            throw r0     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
        L_0x0183:
            r7 = 4
            if (r5 != r7) goto L_0x0196
            byte[] r7 = r6.mo11179c()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            if (r7 == 0) goto L_0x01a5
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            byte[] r6 = r6.mo11179c()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3.<init>(r6)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            goto L_0x01a5
        L_0x0196:
            byte[] r7 = r6.mo11188t()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            if (r7 == 0) goto L_0x01a5
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            byte[] r6 = r6.mo11188t()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3.<init>(r6)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
        L_0x01a5:
            r7 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3.<init>()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r6 = "Image file opened ("
            r3.append(r6)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3.append(r4)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r4 = " bytes in total)"
            r3.append(r4)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r3 = r3.toString()     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r4 = 5
            r1.mo11087a(r4, r3)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            r3 = 1000(0x3e8, float:1.401E-42)
            r1.mo11086a(r3)     // Catch:{ all -> 0x00f8 }
            r3 = 1000(0x3e8, float:1.401E-42)
            r1.mo11086a(r3)     // Catch:{ all -> 0x00f8 }
            com.crrepa.ble.nrf.dfu.i r3 = new com.crrepa.ble.nrf.dfu.i     // Catch:{ all -> 0x00f8 }
            r3.<init>(r1)     // Catch:{ all -> 0x00f8 }
            r1.f2040X = r3     // Catch:{ all -> 0x00f8 }
            boolean r3 = r1.f2042Z     // Catch:{ all -> 0x00f8 }
            r8 = -7
            java.lang.String r9 = "Upload aborted"
            if (r3 == 0) goto L_0x01eb
            r1.m3577d(r9)     // Catch:{ all -> 0x00f8 }
            r0 = 15
            r1.mo11087a(r0, r9)     // Catch:{ all -> 0x00f8 }
            com.crrepa.ble.nrf.dfu.i r0 = r1.f2040X     // Catch:{ all -> 0x00f8 }
            r0.mo11165e(r8)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x01ea
            r18.close()     // Catch:{ IOException -> 0x01ea }
        L_0x01ea:
            return
        L_0x01eb:
            java.lang.String r3 = "Connecting to DFU target..."
            r1.mo11087a(r13, r3)     // Catch:{ all -> 0x00f8 }
            com.crrepa.ble.nrf.dfu.i r3 = r1.f2040X     // Catch:{ all -> 0x00f8 }
            r6 = -1
            r3.mo11165e(r6)     // Catch:{ all -> 0x00f8 }
            android.bluetooth.BluetoothGatt r10 = r1.mo11084a(r2)     // Catch:{ all -> 0x00f8 }
            if (r10 != 0) goto L_0x0211
            java.lang.String r0 = "Bluetooth adapter disabled"
            r1.m3573b(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = "Bluetooth adapter disabled"
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x00f8 }
            r0 = 4106(0x100a, float:5.754E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x0210
            r18.close()     // Catch:{ IOException -> 0x0210 }
        L_0x0210:
            return
        L_0x0211:
            int r2 = r1.f2037U     // Catch:{ all -> 0x00f8 }
            if (r2 != 0) goto L_0x022a
            java.lang.String r0 = "Device got disconnected before service discovery finished"
            r1.m3573b(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = "Disconnected"
            r1.mo11087a(r4, r0)     // Catch:{ all -> 0x00f8 }
            r2 = 4096(0x1000, float:5.74E-42)
            r1.mo11089a(r10, r2)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x0229
            r18.close()     // Catch:{ IOException -> 0x0229 }
        L_0x0229:
            return
        L_0x022a:
            int r2 = r1.f2038V     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = "no.nordicsemi.android.dfu.extra.EXTRA_ATTEMPT"
            if (r2 <= 0) goto L_0x02db
            int r2 = r1.f2038V     // Catch:{ all -> 0x00f8 }
            r4 = 32768(0x8000, float:4.5918E-41)
            r2 = r2 & r4
            if (r2 <= 0) goto L_0x0269
            int r2 = r1.f2038V     // Catch:{ all -> 0x00f8 }
            r4 = -32769(0xffffffffffff7fff, float:NaN)
            r2 = r2 & r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r4.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r5 = "An error occurred while connecting to the device:"
            r4.append(r5)     // Catch:{ all -> 0x00f8 }
            r4.append(r2)     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f8 }
            r1.m3573b(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = "Connection failed (0x%02X): %s"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f8 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x00f8 }
            r7 = 0
            r5[r7] = r6     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = com.crrepa.ble.p070h.p071a.C1303a.m3538b(r2)     // Catch:{ all -> 0x00f8 }
            r5[r13] = r2     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x00f8 }
            goto L_0x0297
        L_0x0269:
            int r2 = r1.f2038V     // Catch:{ all -> 0x00f8 }
            r2 = r2 & -16385(0xffffffffffffbfff, float:NaN)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r4.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r5 = "An error occurred during discovering services:"
            r4.append(r5)     // Catch:{ all -> 0x00f8 }
            r4.append(r2)     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f8 }
            r1.m3573b(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = "Connection failed (0x%02X): %s"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f8 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x00f8 }
            r7 = 0
            r5[r7] = r6     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = com.crrepa.ble.p070h.p071a.C1303a.m3537a(r2)     // Catch:{ all -> 0x00f8 }
            r5[r13] = r2     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x00f8 }
        L_0x0297:
            r1.mo11087a(r15, r2)     // Catch:{ all -> 0x00f8 }
            r2 = 0
            int r2 = r0.getIntExtra(r3, r2)     // Catch:{ all -> 0x00f8 }
            if (r2 != 0) goto L_0x02d0
            java.lang.String r2 = "Retrying..."
            r4 = 15
            r1.mo11087a(r4, r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r1.f2037U     // Catch:{ all -> 0x00f8 }
            if (r2 == 0) goto L_0x02af
            r1.mo11092b(r10)     // Catch:{ all -> 0x00f8 }
        L_0x02af:
            r1.mo11090a(r10, r13)     // Catch:{ all -> 0x00f8 }
            r1.mo11088a(r10)     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = "Restarting the service"
            r1.m3576c(r2)     // Catch:{ all -> 0x00f8 }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ all -> 0x00f8 }
            r2.<init>()     // Catch:{ all -> 0x00f8 }
            r4 = 24
            r2.fillIn(r0, r4)     // Catch:{ all -> 0x00f8 }
            r2.putExtra(r3, r13)     // Catch:{ all -> 0x00f8 }
            r1.startService(r2)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x02cf
            r18.close()     // Catch:{ IOException -> 0x02cf }
        L_0x02cf:
            return
        L_0x02d0:
            int r0 = r1.f2038V     // Catch:{ all -> 0x00f8 }
            r1.mo11089a(r10, r0)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x02da
            r18.close()     // Catch:{ IOException -> 0x02da }
        L_0x02da:
            return
        L_0x02db:
            boolean r2 = r1.f2042Z     // Catch:{ all -> 0x00f8 }
            if (r2 == 0) goto L_0x02f6
            r1.m3577d(r9)     // Catch:{ all -> 0x00f8 }
            r0 = 15
            r1.mo11087a(r0, r9)     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1.mo11089a(r10, r2)     // Catch:{ all -> 0x00f8 }
            com.crrepa.ble.nrf.dfu.i r0 = r1.f2040X     // Catch:{ all -> 0x00f8 }
            r0.mo11165e(r8)     // Catch:{ all -> 0x00f8 }
            if (r18 == 0) goto L_0x02f5
            r18.close()     // Catch:{ IOException -> 0x02f5 }
        L_0x02f5:
            return
        L_0x02f6:
            java.lang.String r2 = "Services discovered"
            r1.mo11087a(r4, r2)     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r0.putExtra(r3, r2)     // Catch:{ all -> 0x00f8 }
            com.crrepa.ble.nrf.dfu.k r2 = new com.crrepa.ble.nrf.dfu.k     // Catch:{ h -> 0x03b6, a -> 0x039d, b -> 0x0347, all -> 0x0343 }
            r2.<init>()     // Catch:{ h -> 0x03b6, a -> 0x039d, b -> 0x0347, all -> 0x0343 }
            r1.f2043a0 = r2     // Catch:{ h -> 0x03b6, a -> 0x039d, b -> 0x0347, all -> 0x0343 }
            com.crrepa.ble.nrf.dfu.j r11 = r2.mo11172a(r0, r1, r10)     // Catch:{ h -> 0x03b6, a -> 0x039d, b -> 0x0347, all -> 0x0343 }
            r1.f2043a0 = r11     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
            if (r11 != 0) goto L_0x032c
            java.lang.String r0 = "DfuBaseService"
            java.lang.String r2 = "DFU Service not found."
            android.util.Log.w(r0, r2)     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
            java.lang.String r0 = "DFU Service not found"
            r2 = 15
            r1.mo11087a(r2, r0)     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
            r0 = 4102(0x1006, float:5.748E-42)
            r1.mo11089a(r10, r0)     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
            if (r11 == 0) goto L_0x0326
            r11.release()     // Catch:{ all -> 0x00f8 }
        L_0x0326:
            if (r18 == 0) goto L_0x032b
            r18.close()     // Catch:{ IOException -> 0x032b }
        L_0x032b:
            return
        L_0x032c:
            r2 = r11
            r3 = r20
            r4 = r10
            r6 = r18
            boolean r2 = r2.mo11131a(r3, r4, r5, r6, r7)     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
            if (r2 == 0) goto L_0x033b
            r11.mo11151a(r0)     // Catch:{ h -> 0x03b7, a -> 0x0341, b -> 0x033f }
        L_0x033b:
            if (r11 == 0) goto L_0x03cd
            goto L_0x03ca
        L_0x033f:
            r0 = move-exception
            goto L_0x0349
        L_0x0341:
            r0 = move-exception
            goto L_0x039f
        L_0x0343:
            r0 = move-exception
            r15 = 0
            goto L_0x03d3
        L_0x0347:
            r0 = move-exception
            r11 = 0
        L_0x0349:
            int r2 = r0.mo11197a()     // Catch:{ all -> 0x039a }
            r3 = 32768(0x8000, float:4.5918E-41)
            r3 = r3 & r2
            if (r3 <= 0) goto L_0x036e
            r3 = -32769(0xffffffffffff7fff, float:NaN)
            r2 = r2 & r3
            java.lang.String r3 = "Error (0x%02X): %s"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x039a }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x039a }
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x039a }
            java.lang.String r2 = com.crrepa.ble.p070h.p071a.C1303a.m3538b(r2)     // Catch:{ all -> 0x039a }
            r4[r13] = r2     // Catch:{ all -> 0x039a }
            java.lang.String r2 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x039a }
            goto L_0x0386
        L_0x036e:
            r2 = r2 & -16385(0xffffffffffffbfff, float:NaN)
            java.lang.String r3 = "Error (0x%02X): %s"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x039a }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x039a }
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x039a }
            java.lang.String r2 = com.crrepa.ble.p070h.p071a.C1303a.m3537a(r2)     // Catch:{ all -> 0x039a }
            r4[r13] = r2     // Catch:{ all -> 0x039a }
            java.lang.String r2 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x039a }
        L_0x0386:
            r1.mo11087a(r15, r2)     // Catch:{ all -> 0x039a }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x039a }
            r1.m3573b(r2)     // Catch:{ all -> 0x039a }
            int r0 = r0.mo11197a()     // Catch:{ all -> 0x039a }
            r1.mo11089a(r10, r0)     // Catch:{ all -> 0x039a }
            if (r11 == 0) goto L_0x03cd
            goto L_0x03ca
        L_0x039a:
            r0 = move-exception
            r15 = r11
            goto L_0x03d3
        L_0x039d:
            r0 = move-exception
            r11 = 0
        L_0x039f:
            java.lang.String r2 = "Device has disconnected"
            r1.mo11087a(r15, r2)     // Catch:{ all -> 0x039a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x039a }
            r1.m3573b(r0)     // Catch:{ all -> 0x039a }
            r1.mo11088a(r10)     // Catch:{ all -> 0x039a }
            r2 = 4096(0x1000, float:5.74E-42)
            r1.m3571b(r2)     // Catch:{ all -> 0x039a }
            if (r11 == 0) goto L_0x03cd
            goto L_0x03ca
        L_0x03b6:
            r11 = 0
        L_0x03b7:
            r1.m3577d(r9)     // Catch:{ all -> 0x039a }
            r0 = 15
            r1.mo11087a(r0, r9)     // Catch:{ all -> 0x039a }
            r2 = 0
            r1.mo11089a(r10, r2)     // Catch:{ all -> 0x039a }
            com.crrepa.ble.nrf.dfu.i r0 = r1.f2040X     // Catch:{ all -> 0x039a }
            r0.mo11165e(r8)     // Catch:{ all -> 0x039a }
            if (r11 == 0) goto L_0x03cd
        L_0x03ca:
            r11.release()     // Catch:{ all -> 0x00f8 }
        L_0x03cd:
            if (r18 == 0) goto L_0x03d2
            r18.close()     // Catch:{ IOException -> 0x03d2 }
        L_0x03d2:
            return
        L_0x03d3:
            if (r15 == 0) goto L_0x03d8
            r15.release()     // Catch:{ all -> 0x00f8 }
        L_0x03d8:
            throw r0     // Catch:{ all -> 0x00f8 }
        L_0x03d9:
            com.crrepa.ble.nrf.dfu.q.c.f r0 = new com.crrepa.ble.nrf.dfu.q.c.f     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            java.lang.String r2 = "The new firmware is not word-aligned."
            r0.<init>(r2)     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
            throw r0     // Catch:{ SecurityException -> 0x0109, FileNotFoundException -> 0x0106, f -> 0x0103, IOException -> 0x0100, Exception -> 0x00fd }
        L_0x03e1:
            r17 = r18
            goto L_0x03f9
        L_0x03e4:
            r17 = r18
            goto L_0x0424
        L_0x03e7:
            r17 = r18
            goto L_0x044f
        L_0x03ea:
            r17 = r18
            goto L_0x0467
        L_0x03ee:
            r17 = r18
            goto L_0x047f
        L_0x03f2:
            r0 = move-exception
            r15 = 0
            goto L_0x0497
        L_0x03f6:
            r0 = move-exception
            r17 = 0
        L_0x03f9:
            java.lang.String r2 = "An exception occurred while opening files. Did you set the firmware file?"
            r1.m3568a(r2, r0)     // Catch:{ all -> 0x0494 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0494 }
            r2.<init>()     // Catch:{ all -> 0x0494 }
            java.lang.String r3 = "Opening file failed: "
            r2.append(r3)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0494 }
            r2.append(r0)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0494 }
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x0494 }
            r0 = 4098(0x1002, float:5.743E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x0494 }
            if (r17 == 0) goto L_0x0420
            r17.close()     // Catch:{ IOException -> 0x0420 }
        L_0x0420:
            return
        L_0x0421:
            r0 = move-exception
            r17 = 0
        L_0x0424:
            java.lang.String r2 = "An exception occurred while calculating file size"
            r1.m3568a(r2, r0)     // Catch:{ all -> 0x0494 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0494 }
            r2.<init>()     // Catch:{ all -> 0x0494 }
            java.lang.String r3 = "Opening file failed: "
            r2.append(r3)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0494 }
            r2.append(r0)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0494 }
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x0494 }
            r0 = 4098(0x1002, float:5.743E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x0494 }
            if (r17 == 0) goto L_0x044b
            r17.close()     // Catch:{ IOException -> 0x044b }
        L_0x044b:
            return
        L_0x044c:
            r0 = move-exception
            r17 = 0
        L_0x044f:
            java.lang.String r2 = "Firmware not word-aligned"
            r1.m3568a(r2, r0)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = "Opening file failed: Firmware size must be word-aligned"
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x0494 }
            r0 = 4108(0x100c, float:5.757E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x0494 }
            if (r17 == 0) goto L_0x0463
            r17.close()     // Catch:{ IOException -> 0x0463 }
        L_0x0463:
            return
        L_0x0464:
            r0 = move-exception
            r17 = 0
        L_0x0467:
            java.lang.String r2 = "An exception occurred while opening file"
            r1.m3568a(r2, r0)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = "Opening file failed: File not found"
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x0494 }
            r0 = 4097(0x1001, float:5.741E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x0494 }
            if (r17 == 0) goto L_0x047b
            r17.close()     // Catch:{ IOException -> 0x047b }
        L_0x047b:
            return
        L_0x047c:
            r0 = move-exception
            r17 = 0
        L_0x047f:
            java.lang.String r2 = "A security exception occurred while opening file"
            r1.m3568a(r2, r0)     // Catch:{ all -> 0x0494 }
            java.lang.String r0 = "Opening file failed: Permission required"
            r1.mo11087a(r15, r0)     // Catch:{ all -> 0x0494 }
            r0 = 4097(0x1001, float:5.741E-42)
            r1.m3571b(r0)     // Catch:{ all -> 0x0494 }
            if (r17 == 0) goto L_0x0493
            r17.close()     // Catch:{ IOException -> 0x0493 }
        L_0x0493:
            return
        L_0x0494:
            r0 = move-exception
            r15 = r17
        L_0x0497:
            if (r15 == 0) goto L_0x049c
            r15.close()     // Catch:{ IOException -> 0x049c }
        L_0x049c:
            throw r0
        L_0x049d:
            java.lang.String r0 = "File type or file mime-type not supported"
            r1.m3577d(r0)
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.DfuBaseService.onHandleIntent(android.content.Intent):void");
    }

    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        ((NotificationManager) getSystemService("notification")).cancel(283);
        stopSelf();
    }
}
