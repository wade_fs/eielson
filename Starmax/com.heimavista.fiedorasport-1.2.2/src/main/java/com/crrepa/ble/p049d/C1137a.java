package com.crrepa.ble.p049d;

import android.graphics.Bitmap;
import com.crrepa.ble.p049d.p052f.C1163a;
import com.crrepa.ble.p049d.p052f.C1168e;
import com.crrepa.ble.p049d.p052f.C1170g;
import com.crrepa.ble.p049d.p052f.C1177m;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p049d.p054h.C1180a;
import com.crrepa.ble.p049d.p054h.C1181b;
import com.crrepa.ble.p049d.p054h.C1185f;
import com.crrepa.ble.p049d.p054h.C1189j;
import com.crrepa.ble.p049d.p054h.C1190k;
import com.crrepa.ble.p049d.p054h.C1193n;
import com.crrepa.ble.p049d.p054h.C1195p;
import com.crrepa.ble.p049d.p054h.C1197r;
import com.crrepa.ble.p049d.p054h.C1200u;
import com.crrepa.ble.p049d.p054h.C1201v;
import com.crrepa.ble.p049d.p054h.C1202w;
import com.crrepa.ble.p049d.p063q.C1259a;
import com.crrepa.ble.p049d.p063q.C1260b;
import com.crrepa.ble.p049d.p063q.C1263e;
import com.crrepa.ble.p049d.p063q.C1264f;
import com.crrepa.ble.p049d.p063q.C1267i;
import com.crrepa.ble.p049d.p063q.C1268j;
import com.crrepa.ble.p049d.p063q.C1271m;
import com.crrepa.ble.p049d.p063q.C1272n;
import com.crrepa.ble.p049d.p063q.C1273o;
import com.crrepa.ble.p049d.p063q.C1275q;

/* renamed from: com.crrepa.ble.d.a */
public interface C1137a {
    /* renamed from: a */
    void mo10730a();

    /* renamed from: a */
    void mo10731a(byte b);

    /* renamed from: a */
    void mo10732a(int i);

    /* renamed from: a */
    void mo10733a(Bitmap bitmap, boolean z, C1275q qVar);

    /* renamed from: a */
    void mo10734a(C1163a aVar);

    /* renamed from: a */
    void mo10735a(C1168e eVar);

    /* renamed from: a */
    void mo10736a(C1170g gVar);

    /* renamed from: a */
    void mo10737a(C1177m mVar);

    /* renamed from: a */
    void mo10738a(C1178n nVar);

    /* renamed from: a */
    void mo10739a(C1180a aVar);

    /* renamed from: a */
    void mo10740a(C1181b bVar);

    /* renamed from: a */
    void mo10741a(C1185f fVar);

    /* renamed from: a */
    void mo10742a(C1189j jVar);

    /* renamed from: a */
    void mo10743a(C1190k kVar);

    /* renamed from: a */
    void mo10744a(C1193n nVar);

    /* renamed from: a */
    void mo10745a(C1195p pVar);

    /* renamed from: a */
    void mo10746a(C1197r rVar);

    /* renamed from: a */
    void mo10747a(C1200u uVar);

    /* renamed from: a */
    void mo10748a(C1201v vVar);

    /* renamed from: a */
    void mo10749a(C1202w wVar);

    /* renamed from: a */
    void mo10750a(C1259a aVar);

    /* renamed from: a */
    void mo10751a(C1260b bVar);

    /* renamed from: a */
    void mo10752a(C1263e eVar);

    /* renamed from: a */
    void mo10753a(C1264f fVar);

    /* renamed from: a */
    void mo10754a(C1267i iVar);

    /* renamed from: a */
    void mo10755a(C1268j jVar);

    /* renamed from: a */
    void mo10756a(C1271m mVar);

    /* renamed from: a */
    void mo10757a(C1272n nVar);

    /* renamed from: a */
    void mo10758a(C1273o oVar);

    /* renamed from: a */
    void mo10759a(String str, int i, int i2);

    /* renamed from: a */
    void mo10760a(boolean z);

    /* renamed from: b */
    void mo10761b();

    /* renamed from: b */
    void mo10762b(byte b);

    /* renamed from: b */
    void mo10763b(int i);

    /* renamed from: b */
    void mo10764b(C1168e eVar);

    /* renamed from: b */
    void mo10765b(C1193n nVar);

    /* renamed from: b */
    void mo10766b(boolean z);

    /* renamed from: c */
    void mo10767c();

    /* renamed from: c */
    void mo10768c(byte b);

    /* renamed from: c */
    void mo10769c(int i);

    /* renamed from: d */
    void mo10770d();

    /* renamed from: d */
    void mo10771d(byte b);

    /* renamed from: e */
    void mo10772e();

    /* renamed from: e */
    void mo10773e(byte b);

    /* renamed from: f */
    void mo10774f();

    /* renamed from: g */
    void mo10775g();

    /* renamed from: h */
    void mo10776h();

    /* renamed from: i */
    void mo10777i();
}
