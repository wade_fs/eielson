package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1171h;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.d.l.m */
public class C1236m {
    /* renamed from: a */
    public static C1171h m3317a(byte[] bArr) {
        if (bArr.length < 61) {
            return null;
        }
        C1171h hVar = new C1171h();
        hVar.mo10831a(bArr[0]);
        ArrayList arrayList = new ArrayList();
        for (int i = 1; i < bArr.length; i++) {
            arrayList.add(Integer.valueOf(bArr[i]));
        }
        hVar.mo10832a(arrayList);
        return hVar;
    }
}
