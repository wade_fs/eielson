package com.crrepa.ble;

public final class R$string {
    public static final int abc_action_bar_home_description = 2131820551;
    public static final int abc_action_bar_up_description = 2131820552;
    public static final int abc_action_menu_overflow_description = 2131820553;
    public static final int abc_action_mode_done = 2131820554;
    public static final int abc_activity_chooser_view_see_all = 2131820555;
    public static final int abc_activitychooserview_choose_application = 2131820556;
    public static final int abc_capital_off = 2131820557;
    public static final int abc_capital_on = 2131820558;
    public static final int abc_menu_alt_shortcut_label = 2131820559;
    public static final int abc_menu_ctrl_shortcut_label = 2131820560;
    public static final int abc_menu_delete_shortcut_label = 2131820561;
    public static final int abc_menu_enter_shortcut_label = 2131820562;
    public static final int abc_menu_function_shortcut_label = 2131820563;
    public static final int abc_menu_meta_shortcut_label = 2131820564;
    public static final int abc_menu_shift_shortcut_label = 2131820565;
    public static final int abc_menu_space_shortcut_label = 2131820566;
    public static final int abc_menu_sym_shortcut_label = 2131820567;
    public static final int abc_prepend_shortcut_label = 2131820568;
    public static final int abc_search_hint = 2131820569;
    public static final int abc_searchview_description_clear = 2131820570;
    public static final int abc_searchview_description_query = 2131820571;
    public static final int abc_searchview_description_search = 2131820572;
    public static final int abc_searchview_description_submit = 2131820573;
    public static final int abc_searchview_description_voice = 2131820574;
    public static final int abc_shareactionprovider_share_with = 2131820575;
    public static final int abc_shareactionprovider_share_with_application = 2131820576;
    public static final int abc_toolbar_collapse_description = 2131820577;
    public static final int dfu_action_abort = 2131820757;
    public static final int dfu_status_aborted = 2131820758;
    public static final int dfu_status_aborted_msg = 2131820759;
    public static final int dfu_status_aborting = 2131820760;
    public static final int dfu_status_completed = 2131820761;
    public static final int dfu_status_completed_msg = 2131820762;
    public static final int dfu_status_connecting = 2131820763;
    public static final int dfu_status_connecting_msg = 2131820764;
    public static final int dfu_status_disconnecting = 2131820765;
    public static final int dfu_status_disconnecting_msg = 2131820766;
    public static final int dfu_status_error = 2131820767;
    public static final int dfu_status_error_msg = 2131820768;
    public static final int dfu_status_initializing = 2131820769;
    public static final int dfu_status_starting = 2131820770;
    public static final int dfu_status_starting_msg = 2131820771;
    public static final int dfu_status_switching_to_dfu = 2131820772;
    public static final int dfu_status_switching_to_dfu_msg = 2131820773;
    public static final int dfu_status_uploading = 2131820774;
    public static final int dfu_status_uploading_msg = 2131820775;
    public static final int dfu_status_uploading_part = 2131820776;
    public static final int dfu_status_validating = 2131820777;
    public static final int dfu_status_validating_msg = 2131820778;
    public static final int dfu_unknown_name = 2131820779;
    public static final int search_menu_title = 2131821048;
    public static final int status_bar_notification_info_overflow = 2131821141;
    public static final int trans_error_msg = 2131821181;

    private R$string() {
    }
}
