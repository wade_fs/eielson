package com.crrepa.ble.p068f;

import com.facebook.appevents.AppEventsConstants;

/* renamed from: com.crrepa.ble.f.d */
public class C1295d {
    /* renamed from: a */
    public static byte m3505a(char c) {
        return (byte) "0123456789abcdef".indexOf(c);
    }

    /* renamed from: a */
    public static int m3506a(byte b) {
        return b & 255;
    }

    /* renamed from: a */
    public static int m3507a(byte b, byte b2) {
        return ((b & 255) << 8) + (b2 & 255);
    }

    /* renamed from: a */
    public static int m3508a(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (m3505a(str.charAt(i2)) > 0) {
                i = (int) (((double) i) + Math.pow(2.0d, (double) ((length - i2) - 1)));
            }
        }
        return i;
    }

    /* renamed from: a */
    public static long m3509a(byte[] bArr) {
        return (long) (((bArr[0] & 255) << 24) + ((bArr[1] & 255) << 16) + ((bArr[2] & 255) << 8) + (bArr[3] & 255));
    }

    /* renamed from: a */
    public static byte[] m3510a(int i) {
        return new byte[]{(byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    /* renamed from: a */
    public static byte[] m3511a(long j) {
        return new byte[]{(byte) ((int) ((j >> 24) & 255)), (byte) ((int) ((j >> 16) & 255)), (byte) ((int) ((j >> 8) & 255)), (byte) ((int) (j & 255))};
    }

    /* renamed from: b */
    public static String m3512b(byte[] bArr) {
        if (bArr == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() == 1) {
                hexString = AppEventsConstants.EVENT_PARAM_VALUE_NO + hexString;
            }
            sb.append(hexString);
            sb.append(" ");
        }
        return sb.toString();
    }

    /* renamed from: c */
    public static long m3513c(byte[] bArr) {
        return (long) (((bArr[3] & 255) << 24) + ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8) + (bArr[0] & 255));
    }

    /* renamed from: d */
    public static int m3514d(byte[] bArr) {
        if (!m3517g(bArr)) {
            return 0;
        }
        return ((bArr[2] << 24) >>> 8) | (bArr[0] & 255) | ((bArr[1] << 8) & 65280);
    }

    /* renamed from: e */
    public static String m3515e(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append("0123456789abcdef".charAt(b % 16));
        }
        return sb.toString();
    }

    /* renamed from: f */
    public static int m3516f(byte[] bArr) {
        if (!m3517g(bArr)) {
            return 0;
        }
        return ((bArr[1] << 8) & 65280) | (bArr[0] & 255);
    }

    /* renamed from: g */
    private static boolean m3517g(byte[] bArr) {
        for (byte b : bArr) {
            if (b != -1) {
                return true;
            }
        }
        return false;
    }
}
