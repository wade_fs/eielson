package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import com.crrepa.ble.p068f.C1301j;

/* renamed from: com.crrepa.ble.d.e.e */
public class C1145e {
    /* renamed from: a */
    public static byte[] m2956a() {
        return C1146f.m2959a(57, null);
    }

    /* renamed from: a */
    public static byte[] m2957a(C1178n nVar) {
        byte[] bArr = new byte[37];
        bArr[0] = (byte) nVar.mo10868d();
        bArr[1] = (byte) nVar.mo10870e();
        bArr[2] = (byte) nVar.mo10866c();
        int a = C1301j.m3534a(nVar.mo10864b());
        C1293b.m3504b("colorInt: " + a);
        byte[] a2 = C1295d.m3510a(a);
        System.arraycopy(a2, 0, bArr, 3, a2.length);
        byte[] bArr2 = new byte[32];
        String a3 = nVar.mo10861a();
        int length = a3.length();
        for (int i = 0; i < length; i++) {
            bArr2[i] = C1295d.m3505a(a3.charAt(i));
        }
        System.arraycopy(bArr2, 0, bArr, 5, length);
        return C1146f.m2959a(56, bArr);
    }
}
