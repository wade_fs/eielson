package com.crrepa.ble.p049d.p054h;

import com.crrepa.ble.p049d.p054h.C1204y;
import com.crrepa.ble.p049d.p059m.C1242a;

/* renamed from: com.crrepa.ble.d.h.z */
public class C1206z implements C1204y {

    /* renamed from: a */
    private boolean f1867a;

    /* renamed from: com.crrepa.ble.d.h.z$b */
    private static class C1208b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1206z f1868a = new C1206z();
    }

    private C1206z() {
        this.f1867a = false;
    }

    /* renamed from: b */
    public static C1206z m3111b() {
        return C1208b.f1868a;
    }

    /* renamed from: b */
    private void m3112b(C1204y.C1205a aVar) {
        this.f1867a = aVar == C1204y.C1205a.MTU_VERSION;
    }

    /* renamed from: c */
    private void m3113c() {
        C1242a.m3332c().mo10961a().requestMtu(512);
    }

    /* renamed from: a */
    public void mo10895a(C1204y.C1205a aVar) {
        m3112b(aVar);
        m3113c();
    }

    /* renamed from: a */
    public boolean mo10896a() {
        return this.f1867a;
    }
}
