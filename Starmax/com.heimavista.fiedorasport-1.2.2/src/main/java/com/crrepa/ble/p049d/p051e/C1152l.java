package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.d.e.l */
public class C1152l {
    /* renamed from: a */
    public static int m2971a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return 0;
        }
        return C1295d.m3514d(bArr);
    }

    /* renamed from: a */
    public static byte[] m2972a() {
        return C1146f.m2959a(38, null);
    }

    /* renamed from: a */
    public static byte[] m2973a(int i) {
        return C1146f.m2959a(22, C1295d.m3511a((long) i));
    }
}
