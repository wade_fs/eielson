package com.crrepa.ble.p049d.p051e;

/* renamed from: com.crrepa.ble.d.e.m */
public class C1153m {
    /* renamed from: a */
    public static byte[] m2974a() {
        return C1146f.m2959a(52, null);
    }

    /* renamed from: a */
    public static byte[] m2975a(byte b) {
        return C1146f.m2959a(54, new byte[]{b});
    }

    /* renamed from: a */
    public static byte[] m2976a(int i) {
        if (i <= 0) {
            i = 0;
        }
        return C1146f.m2959a(31, new byte[]{(byte) i});
    }

    /* renamed from: a */
    public static byte[] m2977a(boolean z) {
        byte[] bArr = new byte[1];
        bArr[0] = (byte) (z ? 0 : -1);
        return C1146f.m2959a(104, bArr);
    }

    /* renamed from: b */
    public static byte[] m2978b() {
        return C1146f.m2959a(55, null);
    }

    /* renamed from: b */
    public static byte[] m2979b(byte b) {
        return C1146f.m2959a(53, new byte[]{b});
    }

    /* renamed from: b */
    public static byte[] m2980b(boolean z) {
        byte[] bArr = new byte[1];
        bArr[0] = (byte) (z ? 0 : -1);
        return C1146f.m2959a(109, bArr);
    }

    /* renamed from: c */
    public static byte[] m2981c() {
        return C1146f.m2959a(47, null);
    }
}
