package com.crrepa.ble.p072i.p073a;

import android.bluetooth.BluetoothDevice;

/* renamed from: com.crrepa.ble.i.a.a */
public class C1306a {

    /* renamed from: a */
    private BluetoothDevice f2023a;

    /* renamed from: b */
    private int f2024b;

    public C1306a(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        this.f2023a = bluetoothDevice;
        this.f2024b = i;
    }

    /* renamed from: a */
    public BluetoothDevice mo11073a() {
        return this.f2023a;
    }

    /* renamed from: b */
    public int mo11074b() {
        return this.f2024b;
    }
}
