package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import com.crrepa.ble.nrf.dfu.C1322c;

/* renamed from: com.crrepa.ble.nrf.dfu.a */
abstract class C1318a extends C1322c {

    /* renamed from: v */
    private final C1319a f2052v = new C1319a();

    /* renamed from: com.crrepa.ble.nrf.dfu.a$a */
    protected class C1319a extends C1322c.C1323a {
        protected C1319a() {
            super();
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            DfuBaseService dfuBaseService = C1318a.this.f2077n;
            dfuBaseService.mo11087a(5, "Notification received from " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
            C1318a.this.f2075l = bluetoothGattCharacteristic.getValue();
            C1318a.this.mo11138f();
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            C1318a aVar = C1318a.this;
            aVar.f2072i = true;
            aVar.mo11138f();
        }
    }

    C1318a(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11110a(Intent intent, boolean z, boolean z2) {
        boolean booleanExtra = intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_KEEP_BOND", false);
        super.f2077n.mo11090a(super.f2067d, z || !booleanExtra);
        super.f2077n.mo11088a(super.f2067d);
        if (super.f2067d.getDevice().getBondState() == 12 && (intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_RESTORE_BOND", false) || !booleanExtra)) {
            mo11140h();
            super.f2077n.mo11086a(2000);
        }
        mo11134b("Restarting to bootloader mode");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        mo11128a(intent2, z2);
    }

    /* renamed from: c */
    public C1322c.C1323a m3593c() {
        return this.f2052v;
    }
}
