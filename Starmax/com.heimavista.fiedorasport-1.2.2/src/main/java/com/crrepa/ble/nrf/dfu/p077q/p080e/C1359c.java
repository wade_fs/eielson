package com.crrepa.ble.nrf.dfu.p077q.p080e;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import com.google.android.exoplayer2.DefaultRenderersFactory;

/* renamed from: com.crrepa.ble.nrf.dfu.q.e.c */
public class C1359c implements C1357a, BluetoothAdapter.LeScanCallback {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Object f2192a = new Object();

    /* renamed from: b */
    private String f2193b;

    /* renamed from: c */
    private String f2194c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f2195d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f2196e;

    /* renamed from: com.crrepa.ble.nrf.dfu.q.e.c$a */
    class C1360a implements Runnable {
        C1360a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.q.e.c.a(com.crrepa.ble.nrf.dfu.q.e.c, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.q.e.c, int]
         candidates:
          com.crrepa.ble.nrf.dfu.q.e.c.a(com.crrepa.ble.nrf.dfu.q.e.c, java.lang.String):java.lang.String
          com.crrepa.ble.nrf.dfu.q.e.c.a(com.crrepa.ble.nrf.dfu.q.e.c, boolean):boolean */
        public void run() {
            try {
                Thread.sleep(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
            } catch (InterruptedException unused) {
            }
            if (!C1359c.this.f2196e) {
                String unused2 = C1359c.this.f2195d = (String) null;
                boolean unused3 = C1359c.this.f2196e = true;
                synchronized (C1359c.this.f2192a) {
                    C1359c.this.f2192a.notifyAll();
                }
            }
        }
    }

    /* renamed from: a */
    public String mo11206a(String str) {
        String substring = str.substring(0, 15);
        String format = String.format("%02X", Integer.valueOf((Integer.valueOf(str.substring(15), 16).intValue() + 1) & 255));
        this.f2193b = str;
        this.f2194c = substring + format;
        this.f2195d = null;
        this.f2196e = false;
        new Thread(new C1360a(), "Scanner timer").start();
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        defaultAdapter.startLeScan(this);
        try {
            synchronized (this.f2192a) {
                while (!this.f2196e) {
                    this.f2192a.wait();
                }
            }
        } catch (InterruptedException unused) {
        }
        defaultAdapter.stopLeScan(this);
        return this.f2195d;
    }

    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        String address = bluetoothDevice.getAddress();
        if (this.f2193b.equals(address) || this.f2194c.equals(address)) {
            this.f2195d = address;
            this.f2196e = true;
            synchronized (this.f2192a) {
                this.f2192a.notifyAll();
            }
        }
    }
}
