package com.crrepa.ble.p048c;

import android.bluetooth.BluetoothAdapter;
import com.crrepa.ble.p072i.p073a.C1306a;
import com.crrepa.ble.p072i.p074b.C1307a;
import com.crrepa.ble.p072i.p074b.C1308b;
import com.crrepa.ble.p072i.p074b.C1309c;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.crrepa.ble.c.a */
public class C1136a {

    /* renamed from: a */
    private BluetoothAdapter f1804a;

    /* renamed from: b */
    private C1307a f1805b;

    /* renamed from: c */
    private List<C1306a> f1806c = new ArrayList();

    /* renamed from: d */
    private AtomicBoolean f1807d = new AtomicBoolean(false);

    /* renamed from: e */
    private C1309c f1808e;

    /* renamed from: f */
    private C1308b f1809f;

    public C1136a(BluetoothAdapter bluetoothAdapter) {
        this.f1804a = bluetoothAdapter;
    }

    /* renamed from: a */
    public void mo10723a() {
        C1309c cVar = this.f1808e;
        if (cVar != null) {
            cVar.mo11080b();
        }
        C1308b bVar = this.f1809f;
        if (bVar != null) {
            bVar.mo11077a();
            throw null;
        }
    }

    /* renamed from: a */
    public void mo10724a(C1306a aVar) {
        if (aVar != null) {
            synchronized (this) {
                this.f1807d.set(false);
                for (C1306a aVar2 : this.f1806c) {
                    if (aVar2.mo11073a().equals(aVar.mo11073a())) {
                        this.f1807d.set(true);
                    }
                }
                if (!this.f1807d.get()) {
                    this.f1806c.add(aVar);
                    this.f1805b.mo11075a(aVar);
                }
            }
        }
    }

    /* renamed from: a */
    public void mo10725a(C1309c cVar) {
        cVar.mo11079a();
        this.f1804a.stopLeScan(cVar);
    }

    /* renamed from: a */
    public boolean mo10726a(long j) {
        this.f1808e = new C1309c(j);
        C1309c cVar = this.f1808e;
        cVar.mo11078a(this);
        cVar.mo11081c();
        boolean startLeScan = this.f1804a.startLeScan(this.f1808e);
        if (!startLeScan) {
            this.f1808e.mo11079a();
        }
        return startLeScan;
    }

    /* renamed from: a */
    public boolean mo10727a(C1307a aVar, long j) {
        this.f1805b = aVar;
        boolean a = mo10726a(j);
        this.f1806c.clear();
        return a;
    }

    /* renamed from: b */
    public void mo10728b() {
        this.f1805b.mo11076a(this.f1806c);
    }

    /* renamed from: c */
    public void mo10729c() {
        this.f1805b.mo11076a(this.f1806c);
    }
}
