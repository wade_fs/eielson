package com.crrepa.ble.p049d.p061o;

import java.util.LinkedList;

/* renamed from: com.crrepa.ble.d.o.b */
public class C1251b {

    /* renamed from: a */
    private LinkedList<C1250a> f1953a = new LinkedList<>();

    /* renamed from: a */
    public int mo10974a() {
        return this.f1953a.size();
    }

    /* renamed from: a */
    public synchronized void mo10975a(C1250a aVar) {
        this.f1953a.add(aVar);
    }

    /* renamed from: b */
    public synchronized C1250a mo10976b() {
        C1250a aVar;
        aVar = null;
        if (mo10977c()) {
            aVar = this.f1953a.get(0);
        }
        return aVar;
    }

    /* renamed from: c */
    public boolean mo10977c() {
        return mo10974a() > 0;
    }

    /* renamed from: d */
    public synchronized void mo10978d() {
        if (mo10977c()) {
            this.f1953a.remove(0);
        }
    }
}
