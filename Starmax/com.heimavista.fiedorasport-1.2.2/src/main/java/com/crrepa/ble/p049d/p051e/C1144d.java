package com.crrepa.ble.p049d.p051e;

/* renamed from: com.crrepa.ble.d.e.d */
public class C1144d {
    /* renamed from: a */
    public static int m2953a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return -1;
        }
        return bArr[0];
    }

    /* renamed from: a */
    public static byte[] m2954a() {
        return C1146f.m2959a(41, null);
    }

    /* renamed from: a */
    public static byte[] m2955a(int i) {
        return C1146f.m2959a(25, new byte[]{(byte) i});
    }
}
