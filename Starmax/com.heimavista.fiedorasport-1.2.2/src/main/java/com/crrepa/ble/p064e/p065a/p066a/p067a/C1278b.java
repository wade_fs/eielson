package com.crrepa.ble.p064e.p065a.p066a.p067a;

import com.crrepa.ble.p068f.C1295d;
import java.util.List;

/* renamed from: com.crrepa.ble.e.a.a.a.b */
public class C1278b {

    /* renamed from: a */
    private List<Integer> f1974a;

    /* renamed from: b */
    private List<Integer> f1975b;

    /* renamed from: c */
    private List<Integer> f1976c;

    /* renamed from: d */
    private int f1977d;

    /* renamed from: e */
    private int f1978e;

    /* renamed from: f */
    private boolean f1979f = true;

    public C1278b(List<Integer> list, List<Integer> list2, List<Integer> list3) {
        this.f1974a = list;
        this.f1975b = list2;
        this.f1976c = list3;
    }

    /* renamed from: e */
    private int m3423e() {
        int a = mo11039a() * mo11042b() * 2;
        if (a <= 0) {
            return 115200;
        }
        return a;
    }

    /* renamed from: f */
    private byte[] m3424f() {
        byte[] bArr = new byte[(this.f1976c.size() * 2)];
        for (int i = 0; i < this.f1976c.size(); i++) {
            byte[] a = C1295d.m3510a(this.f1976c.get(i).intValue());
            int i2 = i * 2;
            bArr[i2] = a[0];
            bArr[i2 + 1] = a[1];
        }
        return bArr;
    }

    /* renamed from: g */
    private byte[] m3425g() {
        byte[] bArr = new byte[((this.f1974a.size() * 3) + 2)];
        bArr[0] = 8;
        bArr[1] = 33;
        for (int i = 0; i < this.f1974a.size(); i++) {
            byte[] a = C1295d.m3510a(this.f1974a.get(i).intValue());
            int i2 = i * 3;
            bArr[i2 + 2] = a[0];
            bArr[i2 + 3] = a[1];
            bArr[i2 + 4] = (byte) this.f1975b.get(i).intValue();
        }
        return bArr;
    }

    /* renamed from: a */
    public int mo11039a() {
        return this.f1978e;
    }

    /* renamed from: a */
    public void mo11040a(int i) {
        this.f1978e = i;
    }

    /* renamed from: a */
    public void mo11041a(boolean z) {
        this.f1979f = z;
    }

    /* renamed from: b */
    public int mo11042b() {
        return this.f1977d;
    }

    /* renamed from: b */
    public void mo11043b(int i) {
        this.f1977d = i;
    }

    /* renamed from: c */
    public boolean mo11044c() {
        return this.f1979f;
    }

    /* renamed from: d */
    public byte[] mo11045d() {
        if (!mo11044c()) {
            return m3424f();
        }
        byte[] g = m3425g();
        return m3423e() < g.length ? m3424f() : g;
    }
}
