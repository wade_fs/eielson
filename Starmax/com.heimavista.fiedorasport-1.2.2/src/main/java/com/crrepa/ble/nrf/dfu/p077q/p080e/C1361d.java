package com.crrepa.ble.nrf.dfu.p077q.p080e;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import java.util.List;

@TargetApi(21)
/* renamed from: com.crrepa.ble.nrf.dfu.q.e.d */
public class C1361d extends ScanCallback implements C1357a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Object f2198a = new Object();

    /* renamed from: b */
    private String f2199b;

    /* renamed from: c */
    private String f2200c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f2201d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f2202e;

    /* renamed from: com.crrepa.ble.nrf.dfu.q.e.d$a */
    class C1362a implements Runnable {
        C1362a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crrepa.ble.nrf.dfu.q.e.d.a(com.crrepa.ble.nrf.dfu.q.e.d, boolean):boolean
         arg types: [com.crrepa.ble.nrf.dfu.q.e.d, int]
         candidates:
          com.crrepa.ble.nrf.dfu.q.e.d.a(com.crrepa.ble.nrf.dfu.q.e.d, java.lang.String):java.lang.String
          com.crrepa.ble.nrf.dfu.q.e.d.a(com.crrepa.ble.nrf.dfu.q.e.d, boolean):boolean */
        public void run() {
            try {
                Thread.sleep(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
            } catch (InterruptedException unused) {
            }
            if (!C1361d.this.f2202e) {
                String unused2 = C1361d.this.f2201d = (String) null;
                boolean unused3 = C1361d.this.f2202e = true;
                synchronized (C1361d.this.f2198a) {
                    C1361d.this.f2198a.notifyAll();
                }
            }
        }
    }

    /* renamed from: a */
    public String mo11206a(String str) {
        String substring = str.substring(0, 15);
        String format = String.format("%02X", Integer.valueOf((Integer.valueOf(str.substring(15), 16).intValue() + 1) & 255));
        this.f2199b = str;
        this.f2200c = substring + format;
        this.f2201d = null;
        this.f2202e = false;
        new Thread(new C1362a(), "Scanner timer").start();
        BluetoothLeScanner bluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
        bluetoothLeScanner.startScan((List<ScanFilter>) null, new ScanSettings.Builder().setScanMode(2).build(), super);
        try {
            synchronized (this.f2198a) {
                while (!this.f2202e) {
                    this.f2198a.wait();
                }
            }
        } catch (InterruptedException unused) {
        }
        bluetoothLeScanner.stopScan(super);
        return this.f2201d;
    }

    public void onScanResult(int i, ScanResult scanResult) {
        String address = scanResult.getDevice().getAddress();
        if (this.f2199b.equals(address) || this.f2200c.equals(address)) {
            this.f2201d = address;
            this.f2202e = true;
            synchronized (this.f2198a) {
                this.f2198a.notifyAll();
            }
        }
    }
}
