package com.crrepa.ble.p068f;

import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: com.crrepa.ble.f.g */
public class C1298g {
    /* renamed from: a */
    public static int m3525a(int i, int i2) {
        return (i * 60) + i2;
    }

    /* renamed from: a */
    public static String m3526a(String str) {
        Date date = new Date(System.currentTimeMillis());
        return TextUtils.isEmpty(str) ? date.toString() : m3527a(date, str);
    }

    /* renamed from: a */
    public static String m3527a(Date date, String str) {
        if (date != null || !TextUtils.isEmpty(str)) {
            return new SimpleDateFormat(str).format(date);
        }
        return null;
    }
}
