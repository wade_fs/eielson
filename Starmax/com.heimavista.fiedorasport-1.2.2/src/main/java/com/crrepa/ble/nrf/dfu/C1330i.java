package com.crrepa.ble.nrf.dfu;

import android.os.SystemClock;
import androidx.annotation.NonNull;

/* renamed from: com.crrepa.ble.nrf.dfu.i */
class C1330i {

    /* renamed from: a */
    private final C1331a f2093a;

    /* renamed from: b */
    private int f2094b;

    /* renamed from: c */
    private int f2095c;

    /* renamed from: d */
    private int f2096d;

    /* renamed from: e */
    private int f2097e;

    /* renamed from: f */
    private int f2098f;

    /* renamed from: g */
    private int f2099g;

    /* renamed from: h */
    private int f2100h;

    /* renamed from: i */
    private int f2101i;

    /* renamed from: j */
    private long f2102j;

    /* renamed from: k */
    private long f2103k;

    /* renamed from: com.crrepa.ble.nrf.dfu.i$a */
    interface C1331a {
        /* renamed from: a */
        void mo11085a();
    }

    C1330i(@NonNull C1331a aVar) {
        this.f2093a = aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo11155a() {
        int i = this.f2098f;
        int i2 = this.f2095c;
        int i3 = this.f2099g;
        return Math.min(i - i2, i3 - (i2 % i3));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C1330i mo11156a(int i, int i2, int i3) {
        this.f2098f = i;
        this.f2099g = Integer.MAX_VALUE;
        this.f2100h = i2;
        this.f2101i = i3;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11157a(int i) {
        mo11161c(this.f2095c + i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public float mo11158b() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.f2102j;
        if (elapsedRealtime != 0) {
            return ((float) (this.f2095c - this.f2096d)) / ((float) elapsedRealtime);
        }
        return 0.0f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo11159b(int i) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo11160c() {
        return this.f2095c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo11161c(int i) {
        if (this.f2102j == 0) {
            this.f2102j = SystemClock.elapsedRealtime();
            this.f2096d = i;
        }
        this.f2095c = i;
        this.f2094b = (int) ((((float) i) * 100.0f) / ((float) this.f2098f));
        this.f2093a.mo11085a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo11162d() {
        return this.f2100h;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo11163d(int i) {
        this.f2099g = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public int mo11164e() {
        return this.f2094b;
    }

    /* renamed from: e */
    public void mo11165e(int i) {
        this.f2094b = i;
        this.f2093a.mo11085a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public float mo11166f() {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        float f = elapsedRealtime - this.f2102j != 0 ? ((float) (this.f2095c - this.f2097e)) / ((float) (elapsedRealtime - this.f2103k)) : 0.0f;
        this.f2103k = elapsedRealtime;
        this.f2097e = this.f2095c;
        return f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public C1330i mo11167f(int i) {
        this.f2101i = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public int mo11168g() {
        return this.f2101i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public boolean mo11169h() {
        return this.f2095c == this.f2098f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public boolean mo11170i() {
        return this.f2100h == this.f2101i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public boolean mo11171j() {
        return this.f2095c % this.f2099g == 0;
    }
}
