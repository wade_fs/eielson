package com.crrepa.ble.p068f;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: com.crrepa.ble.f.a */
public class C1292a {
    /* renamed from: a */
    public static int m3501a(String str) {
        String[] a = m3502a(str, "-");
        int i = 0;
        if (a != null && a.length >= 3) {
            Matcher matcher = Pattern.compile("\\d+").matcher(a[2]);
            ArrayList<Integer> arrayList = new ArrayList<>();
            while (matcher.find()) {
                arrayList.add(Integer.valueOf(matcher.group(0)));
            }
            for (Integer num : arrayList) {
                i = (i * 10) + num.intValue();
            }
        }
        return i;
    }

    /* renamed from: a */
    private static String[] m3502a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str.split(str2);
    }
}
