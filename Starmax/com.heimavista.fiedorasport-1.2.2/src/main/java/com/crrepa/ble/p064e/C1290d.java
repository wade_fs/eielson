package com.crrepa.ble.p064e;

import com.crrepa.ble.p049d.p054h.C1206z;
import com.crrepa.ble.p049d.p059m.C1242a;
import com.crrepa.ble.p068f.C1292a;

/* renamed from: com.crrepa.ble.e.d */
public class C1290d {
    /* renamed from: a */
    public static int m3490a(String str) {
        return C1206z.m3111b().mo10896a() ? C1242a.m3332c().mo10964b() : 165 <= C1292a.m3501a(str) ? 256 : 64;
    }

    /* renamed from: a */
    private static byte[] m3491a(byte[] bArr) {
        byte[] a = C1288b.m3470a(bArr, 65258);
        byte[] bArr2 = new byte[(bArr.length + a.length + 2)];
        bArr2[0] = -2;
        System.arraycopy(a, 0, bArr2, 1, a.length);
        bArr2[a.length + 1] = (byte) bArr.length;
        System.arraycopy(bArr, 0, bArr2, a.length + 2, bArr.length);
        return bArr2;
    }

    /* renamed from: a */
    public static byte[] m3492a(byte[] bArr, int i) {
        return C1206z.m3111b().mo10896a() ? bArr : i == 64 ? m3493b(bArr) : m3491a(bArr);
    }

    /* renamed from: b */
    private static byte[] m3493b(byte[] bArr) {
        byte[] a = C1288b.m3470a(bArr, 65258);
        byte[] bArr2 = new byte[((byte) (bArr.length + a.length + 3))];
        bArr2[0] = -1;
        bArr2[1] = -1;
        System.arraycopy(a, 0, bArr2, 2, a.length);
        bArr2[a.length + 2] = (byte) bArr.length;
        System.arraycopy(bArr, 0, bArr2, a.length + 3, bArr.length);
        return bArr2;
    }
}
