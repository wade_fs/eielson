package com.crrepa.ble.p049d.p057k;

import android.bluetooth.BluetoothGatt;
import com.crrepa.ble.p049d.p053g.C1179a;
import com.crrepa.ble.p049d.p059m.C1242a;

/* renamed from: com.crrepa.ble.d.k.e */
public class C1220e {

    /* renamed from: com.crrepa.ble.d.k.e$a */
    class C1221a implements Runnable {
        C1221a(C1220e eVar) {
        }

        public void run() {
            BluetoothGatt a = C1242a.m3332c().mo10961a();
            if (a != null) {
                a.disconnect();
            }
        }
    }

    /* renamed from: com.crrepa.ble.d.k.e$b */
    class C1222b implements Runnable {
        C1222b(C1220e eVar) {
        }

        public void run() {
            BluetoothGatt a = C1242a.m3332c().mo10961a();
            if (a != null) {
                a.disconnect();
                a.close();
            }
        }
    }

    /* renamed from: a */
    public void mo10948a() {
        C1179a.m3085a(new C1222b(this), 0);
    }

    /* renamed from: b */
    public void mo10949b() {
        C1179a.m3085a(new C1221a(this), 0);
    }
}
