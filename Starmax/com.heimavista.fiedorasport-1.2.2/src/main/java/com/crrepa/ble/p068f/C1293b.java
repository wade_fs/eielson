package com.crrepa.ble.p068f;

import android.util.Log;

/* renamed from: com.crrepa.ble.f.b */
public final class C1293b {

    /* renamed from: a */
    private static boolean f2000a = false;

    /* renamed from: a */
    public static int m3503a(Object obj) {
        if (!f2000a || obj == null) {
            return -1;
        }
        return Log.d("crp", obj.toString());
    }

    /* renamed from: b */
    public static int m3504b(Object obj) {
        if (!f2000a || obj == null) {
            return -1;
        }
        return Log.i("crp", obj.toString());
    }
}
