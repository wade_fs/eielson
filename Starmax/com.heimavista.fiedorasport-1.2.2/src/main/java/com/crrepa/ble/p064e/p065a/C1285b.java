package com.crrepa.ble.p064e.p065a;

import com.crrepa.ble.R$string;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.crrepa.ble.p064e.C1289c;
import com.crrepa.ble.p068f.C1296e;

/* renamed from: com.crrepa.ble.e.a.b */
public class C1285b extends C1289c {

    /* renamed from: c */
    private C1275q f1992c;

    /* renamed from: com.crrepa.ble.e.a.b$b */
    private static class C1287b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C1285b f1993a = new C1285b();
    }

    private C1285b() {
    }

    /* renamed from: h */
    public static C1285b m3461h() {
        return C1287b.f1993a;
    }

    /* renamed from: i */
    private void m3462i() {
        mo11063a(false);
        if (this.f1992c != null) {
            this.f1992c.onError(C1296e.m3518a().getString(R$string.trans_error_msg));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11054a(int i) {
        C1275q qVar = this.f1992c;
        if (qVar != null) {
            qVar.mo11034a(i);
        }
    }

    /* renamed from: b */
    public int mo11055b() {
        return 116;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11056c() {
        mo11065g();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11057d() {
        C1275q qVar = this.f1992c;
        if (qVar != null) {
            qVar.mo11033a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11058e() {
        m3462i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11059f() {
        mo11063a(false);
    }
}
