package com.crrepa.ble.p049d.p063q;

import com.crrepa.ble.p049d.p052f.C1165c;
import com.crrepa.ble.p049d.p052f.C1167d;
import java.util.List;

/* renamed from: com.crrepa.ble.d.q.j */
public interface C1268j {
    /* renamed from: a */
    void mo11018a(int i);

    /* renamed from: a */
    void mo11019a(C1165c cVar);

    /* renamed from: a */
    void mo11020a(List<C1167d> list);

    /* renamed from: b */
    void mo11021b(int i);

    /* renamed from: b */
    void mo11022b(C1165c cVar);
}
