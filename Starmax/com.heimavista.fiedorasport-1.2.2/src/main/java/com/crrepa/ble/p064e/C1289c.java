package com.crrepa.ble.p064e;

import com.crrepa.ble.p049d.p051e.C1146f;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import com.crrepa.ble.trans.upgrade.p081a.C1363a;
import java.io.File;

/* renamed from: com.crrepa.ble.e.c */
public abstract class C1289c {

    /* renamed from: a */
    protected C1291e f1994a;

    /* renamed from: b */
    protected int f1995b = -1;

    /* renamed from: b */
    private void m3471b(int i) {
        C1291e eVar = this.f1994a;
        if (eVar == null) {
            m3477j();
            return;
        }
        int c = eVar.mo11069c();
        C1293b.m3504b("upgradeFileCrc: " + i);
        C1293b.m3504b("calcFileCrc: " + c);
        boolean z = i == c;
        mo11063a(z);
        if (z) {
            mo11215i();
        } else {
            mo11056c();
        }
    }

    /* renamed from: b */
    private void m3472b(byte[] bArr) {
        C1217c.m3269e().mo10937a(bArr);
    }

    /* renamed from: c */
    private void m3473c(int i) {
        int e;
        C1291e eVar = this.f1994a;
        if (eVar != null && (e = (i * 100) / eVar.mo11071e()) != this.f1995b) {
            this.f1995b = e;
            mo11054a(e);
        }
    }

    /* renamed from: d */
    private void m3474d(int i) {
        C1291e eVar = this.f1994a;
        if (eVar == null) {
            C1293b.m3504b("FileManager is null");
            mo11059f();
            return;
        }
        byte[] a = eVar.mo11067a(i);
        int b = this.f1994a.mo11068b();
        if (a == null) {
            m3477j();
        } else {
            mo11064a(C1290d.m3492a(a, b));
        }
    }

    /* renamed from: h */
    private void mo11214h() {
        mo11060a();
    }

    /* renamed from: i */
    private void mo11215i() {
        mo11057d();
        mo11214h();
    }

    /* renamed from: j */
    private void m3477j() {
        mo11058e();
        mo11214h();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11060a() {
        C1291e eVar = this.f1994a;
        if (eVar != null) {
            eVar.mo11066a();
            this.f1994a = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo11054a(int i);

    /* renamed from: a */
    public void mo11061a(C1363a aVar) {
        int b = aVar.mo11213b();
        C1293b.m3504b("trans offset: " + b);
        if (b >= 0) {
            if (b == 65535) {
                m3471b(aVar.mo11212a());
                return;
            }
            m3474d(b);
            m3473c(b);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11062a(File file, int i, int i2) {
        this.f1994a = C1291e.m3494a(file, i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11063a(boolean z) {
        C1293b.m3503a("sendFileCheckResult: " + z);
        int b = mo11055b();
        if (b > 0) {
            byte[] bArr = new byte[4];
            if (!z) {
                for (int i = 0; i < bArr.length; i++) {
                    bArr[i] = -1;
                }
            }
            m3472b(C1146f.m2959a(b, bArr));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11064a(byte[] bArr) {
        C1217c e = C1217c.m3269e();
        int b = mo11055b();
        if (b == 99) {
            e.mo10938b(bArr);
        } else if (b == 108 || b == 116) {
            e.mo10940c(bArr);
        }
    }

    /* renamed from: b */
    public abstract int mo11055b();

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract void mo11056c();

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract void mo11057d();

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public abstract void mo11058e();

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract void mo11059f();

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo11065g() {
        long d = this.f1994a.mo11070d();
        if (d < 0) {
            m3477j();
            return;
        }
        m3472b(C1146f.m2959a(mo11055b(), C1295d.m3511a(d)));
    }
}
