package com.crrepa.ble.p049d.p052f;

import java.util.List;

/* renamed from: com.crrepa.ble.d.f.k */
public class C1175k {

    /* renamed from: a */
    private int f1851a;

    /* renamed from: b */
    private int f1852b;

    /* renamed from: c */
    private List<Integer> f1853c;

    public C1175k(int i, int i2, List<Integer> list) {
        this.f1851a = i;
        this.f1852b = i2;
        this.f1853c = list;
    }

    /* renamed from: a */
    public int mo10854a() {
        return this.f1851a;
    }

    /* renamed from: b */
    public List<Integer> mo10855b() {
        return this.f1853c;
    }

    /* renamed from: c */
    public int mo10856c() {
        return this.f1852b;
    }
}
