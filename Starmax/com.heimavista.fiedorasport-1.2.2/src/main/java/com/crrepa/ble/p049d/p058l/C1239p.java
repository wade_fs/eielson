package com.crrepa.ble.p049d.p058l;

import androidx.annotation.NonNull;
import com.crrepa.ble.p049d.p051e.C1141a;
import com.crrepa.ble.p049d.p052f.C1175k;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p068f.C1295d;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.crrepa.ble.d.l.p */
public class C1239p {

    /* renamed from: a */
    private List<Integer> f1940a = new ArrayList();

    /* renamed from: b */
    private List<Integer> f1941b = new ArrayList();

    @NonNull
    /* renamed from: a */
    private C1175k m3324a(int i, List<Integer> list) {
        return new C1175k(i, 30, list);
    }

    /* renamed from: a */
    private void m3325a(byte b) {
        List<Integer> list;
        if (b == 0) {
            list = this.f1940a;
        } else if (2 == b) {
            list = this.f1941b;
        } else {
            return;
        }
        list.clear();
    }

    /* renamed from: a */
    private void m3326a(byte b, List<Integer> list) {
        List<Integer> list2;
        if (b <= 1) {
            list2 = this.f1940a;
        } else if (b <= 3) {
            list2 = this.f1941b;
        } else {
            return;
        }
        list2.addAll(list);
    }

    /* renamed from: a */
    private void m3327a(int i) {
        C1217c.m3269e().mo10937a(C1141a.m2948a(i));
    }

    /* renamed from: b */
    private List<Integer> m3328b(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 1; i < bArr.length; i += 2) {
            arrayList.add(Integer.valueOf(C1295d.m3507a(bArr[i + 1], bArr[i])));
        }
        return arrayList;
    }

    /* renamed from: a */
    public C1175k mo10960a(byte[] bArr) {
        if (bArr != null && bArr.length >= 1) {
            byte b = bArr[0];
            m3325a(b);
            m3326a(b, m3328b(bArr));
            if (1 == b) {
                return m3324a(0, this.f1940a);
            }
            if (3 == b) {
                return m3324a(2, this.f1941b);
            }
            m3327a((int) ((byte) (b + 1)));
        }
        return null;
    }
}
