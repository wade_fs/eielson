package com.crrepa.ble.p049d.p056j;

/* renamed from: com.crrepa.ble.d.j.b */
public class C1214b {
    /* renamed from: a */
    public static int m3185a(int i, int i2) {
        if (i2 < 161) {
            if (i == 34) {
                return 2;
            }
            if (i != 35) {
                return i;
            }
            return 3;
        } else if (i == 34) {
            return 4;
        } else {
            if (i == 35) {
                return 5;
            }
            switch (i) {
                case 0:
                    return 0;
                case 1:
                    return 1;
                case 2:
                case 5:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 8;
                case 6:
                    return 6;
                case 7:
                    return 7;
                case 8:
                    return 10;
                case 9:
                    return 9;
                default:
                    return i;
            }
        }
    }
}
