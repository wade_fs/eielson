package com.crrepa.ble.p049d.p051e;

/* renamed from: com.crrepa.ble.d.e.h */
public class C1148h {
    /* renamed from: a */
    public static byte m2964a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return -1;
        }
        return bArr[0];
    }

    /* renamed from: a */
    public static byte[] m2965a() {
        return C1146f.m2959a(46, null);
    }

    /* renamed from: a */
    public static byte[] m2966a(int i) {
        return C1146f.m2959a(30, new byte[]{(byte) i});
    }
}
