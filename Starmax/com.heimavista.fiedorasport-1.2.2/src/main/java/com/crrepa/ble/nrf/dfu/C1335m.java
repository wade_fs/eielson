package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.preference.PreferenceManager;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.m */
class C1335m extends C1318a {

    /* renamed from: A */
    protected static UUID f2112A = C1336n.f2121H;

    /* renamed from: B */
    private static final byte[] f2113B = {1, 4};

    /* renamed from: y */
    protected static UUID f2114y = C1336n.f2118E;

    /* renamed from: z */
    protected static UUID f2115z = C1336n.f2119F;

    /* renamed from: w */
    private BluetoothGattCharacteristic f2116w;

    /* renamed from: x */
    private int f2117x;

    C1335m(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008e  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m3686a(android.bluetooth.BluetoothGatt r6, android.bluetooth.BluetoothGattCharacteristic r7) {
        /*
            r5 = this;
            boolean r0 = r5.f2071h
            if (r0 == 0) goto L_0x009c
            boolean r0 = r5.f2070g
            if (r0 != 0) goto L_0x0096
            r0 = 0
            if (r7 != 0) goto L_0x000c
            return r0
        L_0x000c:
            r1 = 0
            r5.f2075l = r1
            r5.f2074k = r0
            java.lang.String r2 = "Reading DFU version number..."
            r5.mo11134b(r2)
            com.crrepa.ble.nrf.dfu.DfuBaseService r2 = r5.f2077n
            r3 = 1
            java.lang.String r4 = "Reading DFU version number..."
            r2.mo11087a(r3, r4)
            r7.setValue(r1)
            com.crrepa.ble.nrf.dfu.DfuBaseService r1 = r5.f2077n
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "gatt.readCharacteristic("
            r2.append(r3)
            java.util.UUID r3 = r7.getUuid()
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.mo11087a(r0, r2)
            r6.readCharacteristic(r7)
            java.lang.Object r6 = r5.f2064a     // Catch:{ InterruptedException -> 0x006d }
            monitor-enter(r6)     // Catch:{ InterruptedException -> 0x006d }
        L_0x0046:
            boolean r1 = r5.f2072i     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0050
            byte[] r1 = r7.getValue()     // Catch:{ all -> 0x006a }
            if (r1 != 0) goto L_0x005c
        L_0x0050:
            boolean r1 = r5.f2071h     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x005c
            int r1 = r5.f2074k     // Catch:{ all -> 0x006a }
            if (r1 != 0) goto L_0x005c
            boolean r1 = r5.f2070g     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0060
        L_0x005c:
            boolean r1 = r5.f2069f     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0068
        L_0x0060:
            r5.f2072i = r0     // Catch:{ all -> 0x006a }
            java.lang.Object r1 = r5.f2064a     // Catch:{ all -> 0x006a }
            r1.wait()     // Catch:{ all -> 0x006a }
            goto L_0x0046
        L_0x0068:
            monitor-exit(r6)     // Catch:{ all -> 0x006a }
            goto L_0x0073
        L_0x006a:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x006a }
            throw r1     // Catch:{ InterruptedException -> 0x006d }
        L_0x006d:
            r6 = move-exception
            java.lang.String r1 = "Sleeping interrupted"
            r5.mo11130a(r1, r6)
        L_0x0073:
            int r6 = r5.f2074k
            if (r6 != 0) goto L_0x008e
            boolean r6 = r5.f2071h
            if (r6 == 0) goto L_0x0086
            r6 = 18
            java.lang.Integer r6 = r7.getIntValue(r6, r0)
            int r6 = r6.intValue()
            return r6
        L_0x0086:
            com.crrepa.ble.nrf.dfu.q.c.a r6 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r7 = "Unable to read version number: device disconnected"
            r6.<init>(r7)
            throw r6
        L_0x008e:
            com.crrepa.ble.nrf.dfu.q.c.b r7 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r0 = "Unable to read version number"
            r7.<init>(r0, r6)
            throw r7
        L_0x0096:
            com.crrepa.ble.nrf.dfu.q.c.h r6 = new com.crrepa.ble.nrf.dfu.q.c.h
            r6.<init>()
            throw r6
        L_0x009c:
            com.crrepa.ble.nrf.dfu.q.c.a r6 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r7 = "Unable to read version number: device disconnected"
            r6.<init>(r7)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1335m.m3686a(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic):int");
    }

    /* renamed from: c */
    private String m3687c(int i) {
        return i != 0 ? i != 1 ? i != 5 ? i != 6 ? i != 7 ? i != 8 ? "Unknown version" : "Bootloader from SDK 9.0 or newer. Signature supported" : "Bootloader from SDK 8.0 or newer. SHA-256 used instead of CRC-16 in the Init Packet" : "Bootloader from SDK 8.0 or newer. Bond sharing supported" : "Bootloader from SDK 7.0 or newer. No bond sharing" : "Application with Legacy buttonless update from SDK 7.0 or newer" : "Bootloader from SDK 6.1 or older";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void
     arg types: [android.bluetooth.BluetoothGattCharacteristic, byte[], int]
     candidates:
      com.crrepa.ble.nrf.dfu.a.a(android.content.Intent, boolean, boolean):void
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void */
    /* renamed from: a */
    public void mo11151a(Intent intent) {
        mo11135c("Application with legacy buttonless update found");
        this.f2077n.mo11087a(15, "Application with buttonless update found");
        boolean z = true;
        this.f2077n.mo11087a(1, "Jumping to the DFU Bootloader...");
        mo11126a(this.f2116w, 1);
        this.f2077n.mo11087a(10, "Notifications enabled");
        this.f2077n.mo11086a(1000);
        this.f2078o.mo11165e(-3);
        mo11134b("Sending Start DFU command (Op Code = 1, Upload Mode = 4)");
        mo11127a(this.f2116w, f2113B, true);
        this.f2077n.mo11087a(10, "Jump to bootloader sent (Op Code = 1, Upload Mode = 4)");
        this.f2077n.mo11094d();
        this.f2077n.mo11087a(5, "Disconnected by the remote device");
        BluetoothGatt bluetoothGatt = this.f2067d;
        BluetoothGattService service = bluetoothGatt.getService(C1322c.f2060r);
        this.f2077n.mo11090a(bluetoothGatt, !((service == null || service.getCharacteristic(C1322c.f2061s) == null) ? false : true));
        this.f2077n.mo11088a(bluetoothGatt);
        mo11134b("Starting service that will connect to the DFU bootloader");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        if (this.f2117x != 0) {
            z = false;
        }
        mo11128a(intent2, z);
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        int i;
        BluetoothGattService service = bluetoothGatt.getService(f2114y);
        if (service == null) {
            return false;
        }
        this.f2116w = service.getCharacteristic(f2115z);
        if (this.f2116w == null) {
            return false;
        }
        this.f2078o.mo11165e(-2);
        this.f2077n.mo11086a(1000);
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(f2112A);
        if (characteristic != null) {
            i = m3686a(bluetoothGatt, characteristic);
            this.f2117x = i;
            int i2 = i & 15;
            int i3 = i >> 8;
            mo11134b("Version number read: " + i3 + "." + i2 + " -> " + m3687c(i));
            DfuBaseService dfuBaseService = this.f2077n;
            StringBuilder sb = new StringBuilder();
            sb.append("Version number read: ");
            sb.append(i3);
            sb.append(".");
            sb.append(i2);
            dfuBaseService.mo11087a(10, sb.toString());
        } else {
            mo11134b("No DFU Version characteristic found -> " + m3687c(0));
            this.f2077n.mo11087a(10, "DFU Version characteristic not found");
            i = 0;
        }
        boolean z = PreferenceManager.getDefaultSharedPreferences(this.f2077n).getBoolean("settings_assume_dfu_mode", false);
        if (intent.hasExtra("no.nordicsemi.android.dfu.extra.EXTRA_FORCE_DFU")) {
            z = intent.getBooleanExtra("no.nordicsemi.android.dfu.extra.EXTRA_FORCE_DFU", false);
        }
        boolean z2 = bluetoothGatt.getServices().size() > 3;
        if (i == 0 && z2) {
            mo11134b("Additional services found -> Bootloader from SDK 6.1. Updating SD and BL supported, extended init packet not supported");
        }
        return i == 1 || (!z && i == 0 && z2);
    }
}
