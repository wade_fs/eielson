package com.crrepa.ble.p075j.p076a;

import com.crrepa.ble.R$string;
import com.crrepa.ble.p049d.p063q.C1274p;
import com.crrepa.ble.p064e.C1289c;
import com.crrepa.ble.p068f.C1296e;

/* renamed from: com.crrepa.ble.j.a.a */
public class C1311a extends C1289c {

    /* renamed from: c */
    private C1274p f2029c;

    /* renamed from: com.crrepa.ble.j.a.a$b */
    private static class C1313b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1311a f2030a = new C1311a();
    }

    private C1311a() {
    }

    /* renamed from: h */
    public static C1311a m3552h() {
        return C1313b.f2030a;
    }

    /* renamed from: i */
    private void m3553i() {
        mo11063a(false);
        if (this.f2029c != null) {
            this.f2029c.onError(C1296e.m3518a().getString(R$string.trans_error_msg));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11054a(int i) {
        C1274p pVar = this.f2029c;
        if (pVar != null) {
            pVar.mo11031a(i);
        }
    }

    /* renamed from: b */
    public int mo11055b() {
        return 108;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11056c() {
        mo11065g();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11057d() {
        C1274p pVar = this.f2029c;
        if (pVar != null) {
            pVar.mo11030a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11058e() {
        m3553i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11059f() {
        mo11063a(false);
    }
}
