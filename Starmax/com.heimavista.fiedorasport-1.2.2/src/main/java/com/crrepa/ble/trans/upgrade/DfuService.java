package com.crrepa.ble.trans.upgrade;

import android.app.Activity;
import com.crrepa.ble.nrf.dfu.DfuBaseService;

public class DfuService extends DfuBaseService {
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Class<? extends Activity> mo11091b() {
        return NotificationActivity.class;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public boolean mo11093c() {
        return false;
    }
}
