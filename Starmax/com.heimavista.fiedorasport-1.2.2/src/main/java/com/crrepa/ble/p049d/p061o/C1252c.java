package com.crrepa.ble.p049d.p061o;

import com.crrepa.ble.p049d.p057k.C1216b;
import com.crrepa.ble.p049d.p057k.C1217c;
import com.crrepa.ble.p068f.C1293b;
import com.google.android.exoplayer2.DefaultRenderersFactory;

/* renamed from: com.crrepa.ble.d.o.c */
public class C1252c {

    /* renamed from: a */
    private C1251b f1954a;

    /* renamed from: b */
    private boolean f1955b;

    /* renamed from: c */
    private long f1956c;

    /* renamed from: d */
    private C1216b f1957d;

    /* renamed from: com.crrepa.ble.d.o.c$b */
    private static class C1254b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1252c f1958a = new C1252c();
    }

    private C1252c() {
        this.f1954a = new C1251b();
        this.f1955b = true;
        this.f1956c = 0;
    }

    /* renamed from: d */
    public static C1252c m3359d() {
        return C1254b.f1958a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        return;
     */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m3360e() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.crrepa.ble.d.o.b r0 = r5.f1954a     // Catch:{ all -> 0x00c4 }
            boolean r0 = r0.mo10977c()     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x000b
            monitor-exit(r5)
            return
        L_0x000b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c4 }
            r0.<init>()     // Catch:{ all -> 0x00c4 }
            java.lang.String r1 = "isMessageHandleComplete: "
            r0.append(r1)     // Catch:{ all -> 0x00c4 }
            boolean r1 = r5.mo10982a()     // Catch:{ all -> 0x00c4 }
            r0.append(r1)     // Catch:{ all -> 0x00c4 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c4 }
            com.crrepa.ble.p068f.C1293b.m3504b(r0)     // Catch:{ all -> 0x00c4 }
            boolean r0 = r5.mo10982a()     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x0037
            boolean r0 = r5.m3361f()     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x0035
            r5.mo10984c()     // Catch:{ all -> 0x00c4 }
            r5.mo10983b()     // Catch:{ all -> 0x00c4 }
        L_0x0035:
            monitor-exit(r5)
            return
        L_0x0037:
            com.crrepa.ble.d.o.b r0 = r5.f1954a     // Catch:{ all -> 0x00c4 }
            com.crrepa.ble.d.o.a r0 = r0.mo10976b()     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x0046
            java.lang.String r0 = "ble message is null"
            com.crrepa.ble.p068f.C1293b.m3504b(r0)     // Catch:{ all -> 0x00c4 }
            monitor-exit(r5)
            return
        L_0x0046:
            r1 = 0
            r5.mo10981a(r1)     // Catch:{ all -> 0x00c4 }
            int r2 = r0.mo10973b()     // Catch:{ all -> 0x00c4 }
            byte[] r0 = r0.mo10972a()     // Catch:{ all -> 0x00c4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c4 }
            r3.<init>()     // Catch:{ all -> 0x00c4 }
            java.lang.String r4 = "message type: "
            r3.append(r4)     // Catch:{ all -> 0x00c4 }
            r3.append(r2)     // Catch:{ all -> 0x00c4 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c4 }
            com.crrepa.ble.p068f.C1293b.m3504b(r3)     // Catch:{ all -> 0x00c4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c4 }
            r3.<init>()     // Catch:{ all -> 0x00c4 }
            java.lang.String r4 = "message content: "
            r3.append(r4)     // Catch:{ all -> 0x00c4 }
            java.lang.String r4 = com.crrepa.ble.p068f.C1295d.m3512b(r0)     // Catch:{ all -> 0x00c4 }
            r3.append(r4)     // Catch:{ all -> 0x00c4 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c4 }
            com.crrepa.ble.p068f.C1293b.m3504b(r3)     // Catch:{ all -> 0x00c4 }
            if (r2 == 0) goto L_0x00b8
            r3 = 1
            if (r2 == r3) goto L_0x00b0
            r3 = 2
            if (r2 == r3) goto L_0x00a8
            r3 = 3
            if (r2 == r3) goto L_0x00a0
            r1 = 5
            if (r2 == r1) goto L_0x0098
            r1 = 6
            if (r2 == r1) goto L_0x0090
            goto L_0x00bf
        L_0x0090:
            com.crrepa.ble.d.n.b r1 = com.crrepa.ble.p049d.p060n.C1246b.m3343c()     // Catch:{ all -> 0x00c4 }
            r1.mo10968b(r0)     // Catch:{ all -> 0x00c4 }
            goto L_0x00bf
        L_0x0098:
            com.crrepa.ble.d.n.b r1 = com.crrepa.ble.p049d.p060n.C1246b.m3343c()     // Catch:{ all -> 0x00c4 }
            r1.mo10967a(r0)     // Catch:{ all -> 0x00c4 }
            goto L_0x00bf
        L_0x00a0:
            com.crrepa.ble.d.k.b r2 = r5.f1957d     // Catch:{ all -> 0x00c4 }
            byte r0 = r0[r1]     // Catch:{ all -> 0x00c4 }
            r2.mo10931a(r0)     // Catch:{ all -> 0x00c4 }
            goto L_0x00bf
        L_0x00a8:
            com.crrepa.ble.d.k.c r1 = com.crrepa.ble.p049d.p057k.C1217c.m3269e()     // Catch:{ all -> 0x00c4 }
            r1.mo10944f(r0)     // Catch:{ all -> 0x00c4 }
            goto L_0x00bf
        L_0x00b0:
            com.crrepa.ble.d.k.c r1 = com.crrepa.ble.p049d.p057k.C1217c.m3269e()     // Catch:{ all -> 0x00c4 }
            r1.mo10943e(r0)     // Catch:{ all -> 0x00c4 }
            goto L_0x00bf
        L_0x00b8:
            com.crrepa.ble.d.k.c r1 = com.crrepa.ble.p049d.p057k.C1217c.m3269e()     // Catch:{ all -> 0x00c4 }
            r1.mo10942d(r0)     // Catch:{ all -> 0x00c4 }
        L_0x00bf:
            r5.m3363h()     // Catch:{ all -> 0x00c4 }
            monitor-exit(r5)
            return
        L_0x00c4:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.p049d.p061o.C1252c.m3360e():void");
    }

    /* renamed from: f */
    private boolean m3361f() {
        long currentTimeMillis = System.currentTimeMillis() - this.f1956c;
        C1293b.m3504b("period: " + currentTimeMillis);
        return currentTimeMillis >= DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
    }

    /* renamed from: g */
    private void m3362g() {
        mo10981a(true);
        m3360e();
    }

    /* renamed from: h */
    private void m3363h() {
        this.f1956c = System.currentTimeMillis();
    }

    /* renamed from: a */
    public void mo10979a(C1216b bVar) {
        this.f1957d = bVar;
    }

    /* renamed from: a */
    public void mo10980a(C1250a aVar) {
        this.f1954a.mo10975a(aVar);
        m3360e();
    }

    /* renamed from: a */
    public void mo10981a(boolean z) {
        this.f1955b = z;
    }

    /* renamed from: a */
    public boolean mo10982a() {
        return this.f1955b;
    }

    /* renamed from: b */
    public void mo10983b() {
        this.f1954a.mo10978d();
        m3362g();
    }

    /* renamed from: c */
    public void mo10984c() {
        C1217c.m3269e().mo10939c();
    }
}
