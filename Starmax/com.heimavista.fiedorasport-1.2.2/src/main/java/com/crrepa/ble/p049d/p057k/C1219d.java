package com.crrepa.ble.p049d.p057k;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import com.crrepa.ble.p049d.p062p.C1255a;
import com.crrepa.ble.p068f.C1294c;

/* renamed from: com.crrepa.ble.d.k.d */
public class C1219d extends C1223f {
    /* renamed from: a */
    public static boolean m3281a(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        if (bluetoothGatt == null || bluetoothGattCharacteristic == null || bArr == null || !bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, true)) {
            return false;
        }
        BluetoothGattDescriptor descriptor = bluetoothGattCharacteristic.getDescriptor(C1294c.f2017q);
        descriptor.setValue(bArr);
        return bluetoothGatt.writeDescriptor(descriptor);
    }

    /* renamed from: a */
    private boolean m3282a(C1255a aVar, BluetoothGatt bluetoothGatt) {
        BluetoothGattCharacteristic d = aVar.mo10988d();
        if (d == null) {
            return true;
        }
        m3281a(bluetoothGatt, d, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        return false;
    }

    /* renamed from: a */
    public void mo10946a(BluetoothGatt bluetoothGatt) {
        C1255a b = mo10953b();
        if (!(b != null ? m3281a(bluetoothGatt, b.mo10990f(), BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE) : false)) {
            mo10952a();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0066 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo10947a(android.bluetooth.BluetoothGatt r5, android.bluetooth.BluetoothGattDescriptor r6) {
        /*
            r4 = this;
            com.crrepa.ble.d.p.a r0 = r4.mo10953b()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x005b
            android.bluetooth.BluetoothGattCharacteristic r6 = r6.getCharacteristic()
            java.util.UUID r6 = r6.getUuid()
            java.util.UUID r3 = com.crrepa.ble.p068f.C1294c.f2007g
            boolean r3 = r3.equals(r6)
            if (r3 == 0) goto L_0x0023
            android.bluetooth.BluetoothGattCharacteristic r6 = r0.mo10992h()
            byte[] r3 = android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            boolean r5 = m3281a(r5, r6, r3)
            goto L_0x005c
        L_0x0023:
            java.util.UUID r3 = com.crrepa.ble.p068f.C1294c.f2005e
            boolean r3 = r3.equals(r6)
            if (r3 == 0) goto L_0x0039
            android.bluetooth.BluetoothGattCharacteristic r6 = r0.mo10989e()
            if (r6 != 0) goto L_0x0032
            goto L_0x0041
        L_0x0032:
            byte[] r3 = android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            boolean r5 = m3281a(r5, r6, r3)
            goto L_0x005c
        L_0x0039:
            java.util.UUID r3 = com.crrepa.ble.p068f.C1294c.f2012l
            boolean r3 = r3.equals(r6)
            if (r3 == 0) goto L_0x0048
        L_0x0041:
            boolean r5 = r4.m3282a(r0, r5)
            r6 = r5
            r5 = 1
            goto L_0x005d
        L_0x0048:
            java.util.UUID r5 = com.crrepa.ble.p068f.C1294c.f2015o
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0058
            java.util.UUID r5 = com.crrepa.ble.p068f.C1294c.f2016p
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x005b
        L_0x0058:
            r5 = 1
            r6 = 1
            goto L_0x005d
        L_0x005b:
            r5 = 1
        L_0x005c:
            r6 = 0
        L_0x005d:
            if (r0 == 0) goto L_0x0061
            if (r5 != 0) goto L_0x0064
        L_0x0061:
            r4.mo10952a()
        L_0x0064:
            if (r5 == 0) goto L_0x0069
            if (r6 == 0) goto L_0x0069
            r1 = 1
        L_0x0069:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.p049d.p057k.C1219d.mo10947a(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattDescriptor):boolean");
    }
}
