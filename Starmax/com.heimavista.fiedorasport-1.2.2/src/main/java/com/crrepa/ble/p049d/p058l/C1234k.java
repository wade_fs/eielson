package com.crrepa.ble.p049d.p058l;

import android.text.TextUtils;
import com.crrepa.ble.p049d.p054h.C1204y;
import com.crrepa.ble.p068f.C1293b;

/* renamed from: com.crrepa.ble.d.l.k */
public class C1234k {
    /* renamed from: a */
    public static C1204y.C1205a m3315a(byte[] bArr) {
        String str = new String(bArr);
        C1293b.m3503a("version: " + str);
        return TextUtils.equals("MOYOUNG-V2", str) ? C1204y.C1205a.MTU_VERSION : C1204y.C1205a.NORMAL_VERSION;
    }
}
