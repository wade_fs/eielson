package com.crrepa.ble.nrf.dfu.p077q.p080e;

import android.os.Build;

/* renamed from: com.crrepa.ble.nrf.dfu.q.e.b */
public class C1358b {
    /* renamed from: a */
    public static C1357a m3753a() {
        return Build.VERSION.SDK_INT >= 21 ? new C1361d() : new C1359c();
    }
}
