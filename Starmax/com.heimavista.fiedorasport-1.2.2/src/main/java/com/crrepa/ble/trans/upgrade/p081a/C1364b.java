package com.crrepa.ble.trans.upgrade.p081a;

import com.crrepa.ble.R$string;
import com.crrepa.ble.p049d.p063q.C1261c;
import com.crrepa.ble.p064e.C1289c;
import com.crrepa.ble.p064e.C1290d;
import com.crrepa.ble.p064e.C1291e;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1296e;
import com.crrepa.ble.trans.upgrade.p082b.C1367a;
import java.io.File;

/* renamed from: com.crrepa.ble.trans.upgrade.a.b */
public class C1364b extends C1289c {

    /* renamed from: c */
    private C1261c f2206c;

    /* renamed from: d */
    private File f2207d;

    /* renamed from: com.crrepa.ble.trans.upgrade.a.b$b */
    private static class C1366b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C1364b f2208a = new C1364b();
    }

    private C1364b() {
    }

    /* renamed from: a */
    private void m3768a(String str) {
        C1261c cVar = this.f2206c;
        if (cVar != null) {
            cVar.mo11011a(23, str);
        }
    }

    /* renamed from: j */
    private void m3769j() {
        mo11060a();
        mo11214h();
    }

    /* renamed from: k */
    public static C1364b m3770k() {
        return C1366b.f2208a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11054a(int i) {
        C1261c cVar = this.f2206c;
        if (cVar != null) {
            cVar.mo11010a(i, 0.0f);
        }
    }

    /* renamed from: b */
    public int mo11055b() {
        return 99;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11056c() {
        mo11065g();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11057d() {
        C1261c cVar = this.f2206c;
        if (cVar != null) {
            cVar.mo11009a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11058e() {
        m3769j();
        mo11215i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11059f() {
        mo11214h();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo11214h() {
        String b = C1296e.m3521b();
        C1293b.m3504b("firmwareVersion: " + b);
        int a = C1290d.m3490a(b);
        File file = this.f2207d;
        if (file == null) {
            file = C1367a.m3780a();
        }
        mo11062a(file, a, 0);
    }

    /* renamed from: i */
    public void mo11215i() {
        mo11214h();
        C1291e eVar = super.f1994a;
        if (eVar != null) {
            mo11065g();
        } else if (eVar == null) {
            m3768a(C1296e.m3518a().getString(R$string.dfu_status_error_msg));
        }
    }
}
