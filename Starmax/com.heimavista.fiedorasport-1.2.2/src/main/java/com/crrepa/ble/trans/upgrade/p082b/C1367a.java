package com.crrepa.ble.trans.upgrade.p082b;

import com.crrepa.ble.p045b.p046a.C1134a;
import com.crrepa.ble.p049d.p062p.C1255a;
import com.crrepa.ble.p049d.p062p.C1256b;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1300i;
import com.crrepa.ble.trans.upgrade.C1368c;
import java.io.File;

/* renamed from: com.crrepa.ble.trans.upgrade.b.a */
public class C1367a {
    /* renamed from: a */
    public static File m3780a() {
        File[] listFiles;
        File file = new File(C1368c.f2209a);
        if (!(!file.exists() || (listFiles = file.listFiles()) == null || listFiles.length == 0)) {
            String a = C1134a.m2880a();
            C1293b.m3503a("md5: " + a);
            for (File file2 : listFiles) {
                if (file2.isFile() && C1300i.m3533a(a, file2)) {
                    return file2;
                }
            }
        }
        return null;
    }

    /* renamed from: b */
    public static boolean m3781b() {
        C1255a a = C1256b.m3384d().mo10998a();
        return (a == null || a.mo10994j() == null) ? false : true;
    }
}
