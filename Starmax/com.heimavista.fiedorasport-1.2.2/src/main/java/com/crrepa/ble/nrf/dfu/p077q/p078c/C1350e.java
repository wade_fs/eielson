package com.crrepa.ble.nrf.dfu.p077q.p078c;

/* renamed from: com.crrepa.ble.nrf.dfu.q.c.e */
public class C1350e extends C1349d {
    private static final long serialVersionUID = -6901728550661937942L;

    /* renamed from: Q */
    private final int f2186Q;

    public C1350e(String str, int i) {
        super(str, 11);
        this.f2186Q = i;
    }

    /* renamed from: e */
    public int mo11201e() {
        return this.f2186Q;
    }

    public String getMessage() {
        return super.getMessage() + " (error 11." + this.f2186Q + ")";
    }
}
