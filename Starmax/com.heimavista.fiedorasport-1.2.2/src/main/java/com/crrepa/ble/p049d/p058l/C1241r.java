package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1176l;
import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.d.l.r */
public class C1241r {
    /* renamed from: a */
    public static C1176l m3331a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length < 2) {
            return null;
        }
        int a = C1295d.m3507a(bArr[0], bArr[1]);
        C1293b.m3504b("watch face indec: " + a);
        ArrayList arrayList = new ArrayList();
        for (int i = 2; i < bArr.length; i++) {
            arrayList.add(Integer.valueOf(bArr[i]));
        }
        return new C1176l(a, arrayList);
    }
}
