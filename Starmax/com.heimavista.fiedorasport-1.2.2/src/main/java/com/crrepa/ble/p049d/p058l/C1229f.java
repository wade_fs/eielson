package com.crrepa.ble.p049d.p058l;

import com.facebook.appevents.AppEventsConstants;

/* renamed from: com.crrepa.ble.d.l.f */
public class C1229f {
    /* renamed from: a */
    public static String m3308a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int length = bArr.length - 1; length >= 0; length--) {
            String hexString = Integer.toHexString(bArr[length] & 255);
            if (hexString.length() == 1) {
                hexString = AppEventsConstants.EVENT_PARAM_VALUE_NO + hexString;
            }
            sb.append(hexString.toUpperCase());
            if (length != 0) {
                sb.append(":");
            }
        }
        return sb.toString();
    }
}
