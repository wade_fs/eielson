package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.f */
class C1326f extends C1324d {

    /* renamed from: A */
    protected static UUID f2088A = f2090y;

    /* renamed from: B */
    protected static UUID f2089B = f2091z;

    /* renamed from: y */
    protected static final UUID f2090y = C1338o.f2140D;

    /* renamed from: z */
    protected static final UUID f2091z = new UUID(-8157989233041780896L, -6937650605005804976L);

    /* renamed from: x */
    private BluetoothGattCharacteristic f2092x;

    C1326f(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    public void mo11151a(Intent intent) {
        mo11134b("Buttonless service without bond sharing found -> SDK 13 or newer");
        super.mo11151a(intent);
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(f2088A);
        if (service == null) {
            return false;
        }
        this.f2092x = service.getCharacteristic(f2089B);
        return this.f2092x != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public BluetoothGattCharacteristic mo11117j() {
        return this.f2092x;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public int mo11152k() {
        return 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public boolean mo11153l() {
        return true;
    }
}
