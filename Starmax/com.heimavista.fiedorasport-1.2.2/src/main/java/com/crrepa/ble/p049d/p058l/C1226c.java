package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p068f.C1293b;
import com.crrepa.ble.p068f.C1295d;
import java.util.Date;

/* renamed from: com.crrepa.ble.d.l.c */
public class C1226c {
    /* renamed from: a */
    public static int[] m3291a(byte[] bArr) {
        if (bArr == null || bArr.length % 4 != 0) {
            return null;
        }
        int[] iArr = new int[(bArr.length / 4)];
        byte[] bArr2 = new byte[4];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length && iArr.length > i; i2 += 4) {
            System.arraycopy(bArr, i2, bArr2, 0, bArr2.length);
            int c = (int) C1295d.m3513c(bArr2);
            C1293b.m3504b("ecg: " + c);
            iArr[i] = c;
            i++;
        }
        return iArr;
    }

    /* renamed from: b */
    public static Date m3292b(byte[] bArr) {
        if (bArr.length < 5) {
            return null;
        }
        byte[] bArr2 = new byte[4];
        System.arraycopy(bArr, 1, bArr2, 0, bArr2.length);
        long c = C1295d.m3513c(bArr2) * 1000;
        if (c <= 0) {
            return null;
        }
        return new Date(c);
    }

    /* renamed from: c */
    public static int m3293c(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return -1;
        }
        return bArr[0];
    }
}
