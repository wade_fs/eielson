package com.crrepa.ble.p049d.p056j;

import android.text.TextUtils;
import com.crrepa.ble.p049d.p051e.C1146f;
import com.crrepa.ble.p068f.C1299h;
import com.crrepa.ble.trans.upgrade.p082b.C1367a;
import com.google.android.exoplayer2.C1750C;
import java.io.UnsupportedEncodingException;

/* renamed from: com.crrepa.ble.d.j.a */
public class C1213a {
    /* renamed from: a */
    public static String m3182a(String str, int i) {
        int i2 = i == 0 ? 38 : C1367a.m3781b() ? 230 : 92;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= str.length()) {
                break;
            }
            int i5 = i3 + 1;
            try {
                i2 -= str.substring(i3, i5).getBytes(C1750C.UTF8_NAME).length;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (i2 <= 0) {
                str = (String) TextUtils.concat(str.substring(0, i4), String.valueOf(8230));
                break;
            }
            i4++;
            i3 = i5;
        }
        return str.trim();
    }

    /* renamed from: a */
    public static byte[] m3183a() {
        return C1146f.m2959a(65, new byte[]{-1});
    }

    /* renamed from: a */
    public static byte[] m3184a(String str, int i, int i2) {
        byte[] bArr = null;
        if (TextUtils.isEmpty(str) || i < 0) {
            return null;
        }
        try {
            byte[] bytes = m3182a(C1299h.m3528a(str.trim()), i).getBytes(C1750C.UTF8_NAME);
            bArr = new byte[(bytes.length + 1)];
            bArr[0] = (byte) (C1214b.m3185a(i, i2) & 255);
            System.arraycopy(bytes, 0, bArr, 1, bytes.length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return C1146f.m2959a(65, bArr);
    }
}
