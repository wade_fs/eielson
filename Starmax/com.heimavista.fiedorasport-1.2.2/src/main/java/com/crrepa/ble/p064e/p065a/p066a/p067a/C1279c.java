package com.crrepa.ble.p064e.p065a.p066a.p067a;

import android.graphics.Bitmap;
import com.crrepa.ble.p068f.C1301j;
import java.util.ArrayList;

/* renamed from: com.crrepa.ble.e.a.a.a.c */
public class C1279c {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C1277a f1980a;

    /* renamed from: com.crrepa.ble.e.a.a.a.c$a */
    class C1280a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ Bitmap f1981P;

        /* renamed from: Q */
        final /* synthetic */ boolean f1982Q;

        C1280a(Bitmap bitmap, boolean z) {
            this.f1981P = bitmap;
            this.f1982Q = z;
        }

        public void run() {
            C1278b a = C1279c.this.m3434a(this.f1981P);
            a.mo11041a(this.f1982Q);
            C1279c.this.f1980a.mo11038a(a);
        }
    }

    /* renamed from: com.crrepa.ble.e.a.a.a.c$b */
    private static class C1281b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C1279c f1984a = new C1279c(null);
    }

    private C1279c() {
    }

    /* synthetic */ C1279c(C1280a aVar) {
        this();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C1278b m3434a(Bitmap bitmap) {
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int i = 0;
        int i2 = -1;
        int i3 = 0;
        while (i < height) {
            int i4 = i3;
            int i5 = i2;
            int i6 = 0;
            while (i6 < width) {
                int a = C1301j.m3534a(bitmap.getPixel(i6, i));
                arrayList.add(Integer.valueOf(a));
                if (i6 == 0 && i == 0) {
                    i4++;
                } else {
                    if (a != i5 || i4 == 255) {
                        arrayList2.add(Integer.valueOf(i5));
                        arrayList3.add(Integer.valueOf(i4));
                        i4 = 0;
                    }
                    i4++;
                    if (i == width - 1 && i6 == height - 1) {
                        arrayList2.add(Integer.valueOf(a));
                        arrayList3.add(Integer.valueOf(i4));
                    }
                }
                i6++;
                i5 = a;
            }
            i++;
            i2 = i5;
            i3 = i4;
        }
        C1278b bVar = new C1278b(arrayList2, arrayList3, arrayList);
        bVar.mo11043b(width);
        bVar.mo11040a(height);
        return bVar;
    }

    /* renamed from: a */
    public static C1279c m3436a() {
        return C1281b.f1984a;
    }

    /* renamed from: a */
    public void mo11046a(Bitmap bitmap, boolean z, C1277a aVar) {
        this.f1980a = aVar;
        new Thread(new C1280a(bitmap, z)).start();
    }
}
