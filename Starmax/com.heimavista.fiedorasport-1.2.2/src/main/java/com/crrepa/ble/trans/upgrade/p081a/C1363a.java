package com.crrepa.ble.trans.upgrade.p081a;

import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.trans.upgrade.a.a */
public class C1363a {

    /* renamed from: a */
    private byte[] f2204a;

    /* renamed from: b */
    private int f2205b = 0;

    public C1363a(byte[] bArr) {
        this.f2204a = bArr;
    }

    /* renamed from: a */
    public int mo11212a() {
        return this.f2205b;
    }

    /* renamed from: b */
    public int mo11213b() {
        byte[] bArr = this.f2204a;
        if (bArr == null || bArr.length < 2) {
            return -1;
        }
        if (bArr.length == 4) {
            this.f2205b = C1295d.m3507a(bArr[2], bArr[3]);
        }
        byte[] bArr2 = this.f2204a;
        return C1295d.m3507a(bArr2[0], bArr2[1]);
    }
}
