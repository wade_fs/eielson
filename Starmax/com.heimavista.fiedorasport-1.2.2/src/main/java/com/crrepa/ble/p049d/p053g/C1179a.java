package com.crrepa.ble.p049d.p053g;

import android.os.Handler;
import android.os.Looper;

/* renamed from: com.crrepa.ble.d.g.a */
public class C1179a {

    /* renamed from: a */
    private static Handler f1863a = new Handler(Looper.getMainLooper());

    /* renamed from: a */
    public static void m3085a(Runnable runnable, long j) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else if (j <= 0) {
            f1863a.post(runnable);
        } else {
            f1863a.postDelayed(runnable, j);
        }
    }
}
