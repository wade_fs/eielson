package com.crrepa.ble.p068f;

import java.util.Calendar;

/* renamed from: com.crrepa.ble.f.f */
public class C1297f {
    /* renamed from: a */
    public static int m3523a() {
        Calendar instance = Calendar.getInstance();
        return (instance.get(11) * 60) + instance.get(12);
    }

    /* renamed from: a */
    public static long m3524a(int i) {
        Calendar instance = Calendar.getInstance();
        instance.add(5, i);
        instance.set(11, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(14, 0);
        return instance.getTimeInMillis();
    }
}
