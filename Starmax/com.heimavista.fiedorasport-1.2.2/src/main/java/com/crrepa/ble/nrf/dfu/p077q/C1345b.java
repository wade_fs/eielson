package com.crrepa.ble.nrf.dfu.p077q;

import com.crrepa.ble.nrf.dfu.p077q.p078c.C1348c;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: com.crrepa.ble.nrf.dfu.q.b */
public class C1345b extends FilterInputStream {

    /* renamed from: P */
    private final byte[] f2176P = new byte[128];

    /* renamed from: Q */
    private int f2177Q = 128;

    /* renamed from: R */
    private int f2178R;

    /* renamed from: S */
    private int f2179S = this.f2176P.length;

    /* renamed from: T */
    private int f2180T = 0;

    /* renamed from: U */
    private int f2181U;

    /* renamed from: V */
    private int f2182V;

    /* renamed from: W */
    private final int f2183W;

    public C1345b(InputStream inputStream, int i) {
        super(new BufferedInputStream(inputStream));
        this.f2183W = i;
        this.f2181U = m3741b(i);
    }

    public C1345b(byte[] bArr, int i) {
        super(new ByteArrayInputStream(bArr));
        this.f2183W = i;
        this.f2181U = m3741b(i);
    }

    /* renamed from: a */
    private int m3737a() {
        int i;
        if (this.f2178R == -1) {
            return 0;
        }
        InputStream inputStream = super.in;
        while (true) {
            int read = inputStream.read();
            this.f2178R++;
            if (!(read == 10 || read == 13)) {
                m3743c(read);
                int b = m3742b(inputStream);
                this.f2178R += 2;
                int a = m3739a(inputStream);
                this.f2178R += 4;
                int b2 = m3742b(inputStream);
                this.f2178R += 2;
                if (b2 != 0) {
                    if (b2 != 1) {
                        if (b2 == 2) {
                            i = m3739a(inputStream) << 4;
                            this.f2178R += 4;
                            if (this.f2182V > 0 && (i >> 16) != (this.f2180T >> 16) + 1) {
                                return 0;
                            }
                        } else if (b2 != 4) {
                            this.f2178R = (int) (((long) this.f2178R) + m3740a(inputStream, (long) ((b * 2) + 2)));
                        } else {
                            int a2 = m3739a(inputStream);
                            this.f2178R += 4;
                            if (this.f2182V > 0 && a2 != (this.f2180T >> 16) + 1) {
                                return 0;
                            }
                            i = a2 << 16;
                        }
                        this.f2180T = i;
                        this.f2178R = (int) (((long) this.f2178R) + m3740a(inputStream, 2));
                    } else {
                        this.f2178R = -1;
                        return 0;
                    }
                } else if (this.f2180T + a < this.f2183W) {
                    this.f2178R = (int) (((long) this.f2178R) + m3740a(inputStream, (long) ((b * 2) + 2)));
                    b2 = -1;
                }
                if (b2 == 0) {
                    int i2 = 0;
                    while (i2 < this.f2176P.length && i2 < b) {
                        int b3 = m3742b(inputStream);
                        this.f2178R += 2;
                        this.f2176P[i2] = (byte) b3;
                        i2++;
                    }
                    this.f2178R = (int) (((long) this.f2178R) + m3740a(inputStream, 2));
                    this.f2177Q = 0;
                    return b;
                }
            }
        }
    }

    /* renamed from: a */
    private int m3738a(int i) {
        if (i >= 65) {
            return i - 55;
        }
        if (i >= 48) {
            return i - 48;
        }
        return -1;
    }

    /* renamed from: a */
    private int m3739a(InputStream inputStream) {
        return m3742b(inputStream) | (m3742b(inputStream) << 8);
    }

    /* renamed from: a */
    private long m3740a(InputStream inputStream, long j) {
        long skip = inputStream.skip(j);
        return skip < j ? skip + inputStream.skip(j - skip) : skip;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0064 A[ADDED_TO_REGION, SYNTHETIC] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m3741b(int r11) {
        /*
            r10 = this;
            java.io.InputStream r0 = r10.in
            int r1 = r0.available()
            r0.mark(r1)
            int r1 = r0.read()     // Catch:{ all -> 0x0071 }
            r2 = 0
            r3 = 0
        L_0x000f:
            r10.m3743c(r1)     // Catch:{ all -> 0x0071 }
            int r1 = r10.m3742b(r0)     // Catch:{ all -> 0x0071 }
            int r4 = r10.m3739a(r0)     // Catch:{ all -> 0x0071 }
            int r5 = r10.m3742b(r0)     // Catch:{ all -> 0x0071 }
            r6 = 2
            r8 = 2
            if (r5 == 0) goto L_0x0059
            r4 = 1
            if (r5 == r4) goto L_0x0055
            r9 = 4
            if (r5 == r8) goto L_0x003e
            if (r5 == r9) goto L_0x002c
            goto L_0x005d
        L_0x002c:
            int r1 = r10.m3739a(r0)     // Catch:{ all -> 0x0071 }
            if (r3 <= 0) goto L_0x003b
            int r2 = r2 >> 16
            int r2 = r2 + r4
            if (r1 == r2) goto L_0x003b
            r0.reset()
            return r3
        L_0x003b:
            int r2 = r1 << 16
            goto L_0x0061
        L_0x003e:
            int r1 = r10.m3739a(r0)     // Catch:{ all -> 0x0071 }
            int r1 = r1 << r9
            if (r3 <= 0) goto L_0x0050
            int r5 = r1 >> 16
            int r2 = r2 >> 16
            int r2 = r2 + r4
            if (r5 == r2) goto L_0x0050
            r0.reset()
            return r3
        L_0x0050:
            r10.m3740a(r0, r6)     // Catch:{ all -> 0x0071 }
            r2 = r1
            goto L_0x0064
        L_0x0055:
            r0.reset()
            return r3
        L_0x0059:
            int r4 = r4 + r2
            if (r4 < r11) goto L_0x005d
            int r3 = r3 + r1
        L_0x005d:
            int r1 = r1 * 2
            int r1 = r1 + r8
            long r6 = (long) r1
        L_0x0061:
            r10.m3740a(r0, r6)     // Catch:{ all -> 0x0071 }
        L_0x0064:
            int r1 = r0.read()     // Catch:{ all -> 0x0071 }
            r4 = 10
            if (r1 == r4) goto L_0x0064
            r4 = 13
            if (r1 == r4) goto L_0x0064
            goto L_0x000f
        L_0x0071:
            r11 = move-exception
            r0.reset()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.p077q.C1345b.m3741b(int):int");
    }

    /* renamed from: b */
    private int m3742b(InputStream inputStream) {
        return m3738a(inputStream.read()) | (m3738a(inputStream.read()) << 4);
    }

    /* renamed from: c */
    private void m3743c(int i) {
        if (i != 58) {
            throw new C1348c("Not a HEX file");
        }
    }

    /* renamed from: a */
    public int mo11191a(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int i2 = this.f2177Q;
            if (i2 < this.f2179S) {
                byte[] bArr2 = this.f2176P;
                this.f2177Q = i2 + 1;
                bArr[i] = bArr2[i2];
                i++;
            } else {
                int i3 = this.f2182V;
                int a = m3737a();
                this.f2179S = a;
                this.f2182V = i3 + a;
                if (this.f2179S == 0) {
                    break;
                }
            }
        }
        return i;
    }

    public int available() {
        return this.f2181U - this.f2182V;
    }

    public int read() {
        throw new UnsupportedOperationException("Please, use readPacket() method instead");
    }

    public int read(byte[] bArr) {
        return mo11191a(bArr);
    }

    public int read(byte[] bArr, int i, int i2) {
        throw new UnsupportedOperationException("Please, use readPacket() method instead");
    }

    public synchronized void reset() {
        super.reset();
        this.f2178R = 0;
        this.f2182V = 0;
        this.f2177Q = 0;
    }
}
