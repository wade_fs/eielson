package com.crrepa.ble.p068f;

import android.text.TextUtils;

/* renamed from: com.crrepa.ble.f.h */
public class C1299h {
    /* renamed from: a */
    public static String m3528a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str.replaceAll("[^\\u0000-\\uFFFF]", "[emoji]");
    }
}
