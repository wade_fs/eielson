package com.crrepa.ble.nrf.dfu.p077q;

import androidx.annotation.NonNull;
import com.crrepa.ble.nrf.dfu.p077q.p079d.C1355b;
import com.crrepa.ble.nrf.dfu.p077q.p079d.C1356c;
import com.google.android.exoplayer2.C1750C;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import p000a.p001a.p002a.C0139q;

/* renamed from: com.crrepa.ble.nrf.dfu.q.a */
public class C1344a extends ZipInputStream {

    /* renamed from: P */
    private Map<String, byte[]> f2159P = new HashMap();

    /* renamed from: Q */
    private C1355b f2160Q;

    /* renamed from: R */
    private CRC32 f2161R = new CRC32();

    /* renamed from: S */
    private byte[] f2162S;

    /* renamed from: T */
    private byte[] f2163T;

    /* renamed from: U */
    private byte[] f2164U;

    /* renamed from: V */
    private byte[] f2165V;

    /* renamed from: W */
    private byte[] f2166W;

    /* renamed from: X */
    private byte[] f2167X;

    /* renamed from: Y */
    private byte[] f2168Y;

    /* renamed from: Z */
    private int f2169Z = 0;

    /* renamed from: a0 */
    private int f2170a0;

    /* renamed from: b0 */
    private int f2171b0;

    /* renamed from: c0 */
    private int f2172c0;

    /* renamed from: d0 */
    private int f2173d0 = 0;

    /* renamed from: e0 */
    private byte[] f2174e0;

    /* renamed from: f0 */
    private int f2175f0;

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0074 A[Catch:{ all -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0084 A[Catch:{ all -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ad A[Catch:{ all -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00bd A[Catch:{ all -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00d4 A[Catch:{ all -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00db A[SYNTHETIC, Splitter:B:62:0x00db] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C1344a(java.io.InputStream r5, int r6, int r7) {
        /*
            r4 = this;
            r4.<init>(r5)
            java.util.zip.CRC32 r5 = new java.util.zip.CRC32
            r5.<init>()
            r4.f2161R = r5
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            r4.f2159P = r5
            r5 = 0
            r4.f2173d0 = r5
            r4.f2169Z = r5
            r4.m3725b(r6)     // Catch:{ all -> 0x00ea }
            com.crrepa.ble.nrf.dfu.q.d.b r6 = r4.f2160Q     // Catch:{ all -> 0x00ea }
            if (r6 != 0) goto L_0x00e3
            r6 = 1
            if (r7 == 0) goto L_0x0024
            r0 = r7 & 4
            if (r0 <= 0) goto L_0x005b
        L_0x0024:
            java.util.Map<java.lang.String, byte[]> r0 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = "application.hex"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00ea }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x00ea }
            r4.f2162S = r0     // Catch:{ all -> 0x00ea }
            byte[] r0 = r4.f2162S     // Catch:{ all -> 0x00ea }
            if (r0 != 0) goto L_0x0040
            java.util.Map<java.lang.String, byte[]> r0 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = "application.bin"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00ea }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x00ea }
            r4.f2162S = r0     // Catch:{ all -> 0x00ea }
        L_0x0040:
            byte[] r0 = r4.f2162S     // Catch:{ all -> 0x00ea }
            if (r0 == 0) goto L_0x005b
            byte[] r0 = r4.f2162S     // Catch:{ all -> 0x00ea }
            int r0 = r0.length     // Catch:{ all -> 0x00ea }
            r4.f2172c0 = r0     // Catch:{ all -> 0x00ea }
            java.util.Map<java.lang.String, byte[]> r0 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = "application.dat"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00ea }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x00ea }
            r4.f2167X = r0     // Catch:{ all -> 0x00ea }
            byte[] r0 = r4.f2162S     // Catch:{ all -> 0x00ea }
            r4.f2168Y = r0     // Catch:{ all -> 0x00ea }
            r0 = 1
            goto L_0x005c
        L_0x005b:
            r0 = 0
        L_0x005c:
            java.lang.String r1 = "system.dat"
            if (r7 == 0) goto L_0x0064
            r2 = r7 & 2
            if (r2 <= 0) goto L_0x0098
        L_0x0064:
            java.util.Map<java.lang.String, byte[]> r2 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = "bootloader.hex"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x00ea }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x00ea }
            r4.f2164U = r2     // Catch:{ all -> 0x00ea }
            byte[] r2 = r4.f2164U     // Catch:{ all -> 0x00ea }
            if (r2 != 0) goto L_0x0080
            java.util.Map<java.lang.String, byte[]> r2 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = "bootloader.bin"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x00ea }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x00ea }
            r4.f2164U = r2     // Catch:{ all -> 0x00ea }
        L_0x0080:
            byte[] r2 = r4.f2164U     // Catch:{ all -> 0x00ea }
            if (r2 == 0) goto L_0x0098
            byte[] r0 = r4.f2164U     // Catch:{ all -> 0x00ea }
            int r0 = r0.length     // Catch:{ all -> 0x00ea }
            r4.f2171b0 = r0     // Catch:{ all -> 0x00ea }
            java.util.Map<java.lang.String, byte[]> r0 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00ea }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x00ea }
            r4.f2166W = r0     // Catch:{ all -> 0x00ea }
            byte[] r0 = r4.f2164U     // Catch:{ all -> 0x00ea }
            r4.f2168Y = r0     // Catch:{ all -> 0x00ea }
            r0 = 1
        L_0x0098:
            if (r7 == 0) goto L_0x009d
            r7 = r7 & r6
            if (r7 <= 0) goto L_0x00d1
        L_0x009d:
            java.util.Map<java.lang.String, byte[]> r7 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = "softdevice.hex"
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x00ea }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x00ea }
            r4.f2163T = r7     // Catch:{ all -> 0x00ea }
            byte[] r7 = r4.f2163T     // Catch:{ all -> 0x00ea }
            if (r7 != 0) goto L_0x00b9
            java.util.Map<java.lang.String, byte[]> r7 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = "softdevice.bin"
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x00ea }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x00ea }
            r4.f2163T = r7     // Catch:{ all -> 0x00ea }
        L_0x00b9:
            byte[] r7 = r4.f2163T     // Catch:{ all -> 0x00ea }
            if (r7 == 0) goto L_0x00d1
            byte[] r7 = r4.f2163T     // Catch:{ all -> 0x00ea }
            int r7 = r7.length     // Catch:{ all -> 0x00ea }
            r4.f2170a0 = r7     // Catch:{ all -> 0x00ea }
            java.util.Map<java.lang.String, byte[]> r7 = r4.f2159P     // Catch:{ all -> 0x00ea }
            java.lang.Object r7 = r7.get(r1)     // Catch:{ all -> 0x00ea }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x00ea }
            r4.f2166W = r7     // Catch:{ all -> 0x00ea }
            byte[] r7 = r4.f2163T     // Catch:{ all -> 0x00ea }
            r4.f2168Y = r7     // Catch:{ all -> 0x00ea }
            goto L_0x00d2
        L_0x00d1:
            r6 = r0
        L_0x00d2:
            if (r6 == 0) goto L_0x00db
            r4.mark(r5)     // Catch:{ all -> 0x00ea }
            super.close()
            return
        L_0x00db:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x00ea }
            java.lang.String r6 = "The ZIP file must contain an Application, a Soft Device and/or a Bootloader."
            r5.<init>(r6)     // Catch:{ all -> 0x00ea }
            throw r5     // Catch:{ all -> 0x00ea }
        L_0x00e3:
            com.crrepa.ble.nrf.dfu.q.d.b r5 = r4.f2160Q     // Catch:{ all -> 0x00ea }
            r5.mo11203a()     // Catch:{ all -> 0x00ea }
            r5 = 0
            throw r5
        L_0x00ea:
            r5 = move-exception
            super.close()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.p077q.C1344a.<init>(java.io.InputStream, int, int):void");
    }

    /* renamed from: b */
    private void m3725b(int i) {
        byte[] bArr = new byte[1024];
        String str = null;
        while (true) {
            ZipEntry nextEntry = getNextEntry();
            if (nextEntry == null) {
                break;
            }
            String name = nextEntry.getName();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = super.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (name.toLowerCase(Locale.US).endsWith("hex")) {
                C1345b bVar = new C1345b(byteArray, i);
                byteArray = new byte[bVar.available()];
                bVar.read(byteArray);
                bVar.close();
            }
            if ("manifest.json".equals(name)) {
                str = new String(byteArray, C1750C.UTF8_NAME);
            } else {
                this.f2159P.put(name, byteArray);
            }
        }
        if (str != null) {
            this.f2160Q = ((C1356c) new C0139q().mo197a(str, C1356c.class)).mo11205a();
        }
    }

    /* renamed from: w */
    private byte[] m3726w() {
        byte[] bArr;
        if (this.f2168Y != this.f2163T || (bArr = this.f2164U) == null) {
            byte[] bArr2 = this.f2168Y;
            byte[] bArr3 = this.f2162S;
            if (bArr2 == bArr3 || bArr3 == null) {
                bArr = null;
                this.f2168Y = null;
            } else {
                this.f2168Y = bArr3;
                bArr = bArr3;
            }
        } else {
            this.f2168Y = bArr;
        }
        this.f2169Z = 0;
        return bArr;
    }

    /* renamed from: a */
    public int mo11175a() {
        return this.f2172c0;
    }

    /* renamed from: a */
    public int mo11176a(int i) {
        if (this.f2173d0 <= 0) {
            int e = i & mo11182e();
            if ((e & 1) == 0) {
                this.f2163T = null;
                if (this.f2165V != null) {
                    this.f2165V = null;
                    this.f2171b0 = 0;
                }
                this.f2170a0 = 0;
            }
            if ((e & 2) == 0) {
                this.f2164U = null;
                if (this.f2165V != null) {
                    this.f2165V = null;
                    this.f2170a0 = 0;
                }
                this.f2171b0 = 0;
            }
            if ((e & 4) == 0) {
                this.f2162S = null;
                this.f2172c0 = 0;
            }
            mark(0);
            return e;
        }
        throw new UnsupportedOperationException("Content type must not be change after reading content");
    }

    public int available() {
        byte[] bArr = this.f2165V;
        return (((bArr != null && this.f2170a0 == 0 && this.f2171b0 == 0) ? bArr.length : this.f2170a0 + this.f2171b0) + this.f2172c0) - this.f2173d0;
    }

    /* renamed from: b */
    public int mo11178b() {
        return this.f2171b0;
    }

    /* renamed from: c */
    public byte[] mo11179c() {
        return this.f2167X;
    }

    public void close() {
        this.f2163T = null;
        this.f2164U = null;
        this.f2163T = null;
        this.f2165V = null;
        this.f2172c0 = 0;
        this.f2171b0 = 0;
        this.f2170a0 = 0;
        this.f2168Y = null;
        this.f2169Z = 0;
        this.f2173d0 = 0;
        super.close();
    }

    /* renamed from: d */
    public int mo11181d() {
        return this.f2173d0;
    }

    /* renamed from: e */
    public int mo11182e() {
        byte b = this.f2165V != null ? (byte) 3 : 0;
        if (this.f2170a0 > 0) {
            b = (byte) (b | 1);
        }
        if (this.f2171b0 > 0) {
            b = (byte) (b | 2);
        }
        return this.f2172c0 > 0 ? (byte) (b | 4) : b;
    }

    /* renamed from: g */
    public long mo11183g() {
        return this.f2161R.getValue();
    }

    public void mark(int i) {
        this.f2174e0 = this.f2168Y;
        this.f2175f0 = this.f2169Z;
    }

    public boolean markSupported() {
        return true;
    }

    public int read(@NonNull byte[] bArr) {
        int length = this.f2168Y.length - this.f2169Z;
        if (bArr.length <= length) {
            length = bArr.length;
        }
        System.arraycopy(this.f2168Y, this.f2169Z, bArr, 0, length);
        this.f2169Z += length;
        if (bArr.length > length && m3726w() != null) {
            int length2 = this.f2168Y.length;
            if (bArr.length - length <= length2) {
                length2 = bArr.length - length;
            }
            System.arraycopy(this.f2168Y, 0, bArr, length, length2);
            this.f2169Z += length2;
            length += length2;
        }
        this.f2173d0 += length;
        this.f2161R.update(bArr, 0, length);
        return length;
    }

    public void reset() {
        byte[] bArr;
        if (this.f2162S == null || (this.f2163T == null && this.f2164U == null && this.f2165V == null)) {
            this.f2168Y = this.f2174e0;
            int i = this.f2175f0;
            this.f2169Z = i;
            this.f2173d0 = i;
            this.f2161R.reset();
            if (this.f2168Y == this.f2164U && (bArr = this.f2163T) != null) {
                this.f2161R.update(bArr);
                this.f2173d0 += this.f2170a0;
            }
            this.f2161R.update(this.f2168Y, 0, this.f2169Z);
            return;
        }
        throw new UnsupportedOperationException("Application must be sent in a separate connection.");
    }

    /* renamed from: t */
    public byte[] mo11188t() {
        return this.f2166W;
    }

    /* renamed from: u */
    public boolean mo11189u() {
        C1355b bVar = this.f2160Q;
        if (bVar == null) {
            return false;
        }
        bVar.mo11204b();
        throw null;
    }

    /* renamed from: v */
    public int mo11190v() {
        return this.f2170a0;
    }
}
