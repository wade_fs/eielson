package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.l */
class C1334l extends C1324d {

    /* renamed from: A */
    protected static UUID f2107A = f2109y;

    /* renamed from: B */
    protected static UUID f2108B = f2110z;

    /* renamed from: y */
    protected static final UUID f2109y = new UUID(-8196551313441075360L, -6937650605005804976L);

    /* renamed from: z */
    protected static final UUID f2110z = new UUID(-8196551313441075360L, -6937650605005804976L);

    /* renamed from: x */
    private BluetoothGattCharacteristic f2111x;

    C1334l(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    public void mo11151a(Intent intent) {
        mo11134b("Experimental buttonless service found -> SDK 12.x");
        super.mo11151a(intent);
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(f2107A);
        if (service == null) {
            return false;
        }
        this.f2111x = service.getCharacteristic(f2108B);
        return this.f2111x != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public BluetoothGattCharacteristic mo11117j() {
        return this.f2111x;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public int mo11152k() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public boolean mo11153l() {
        return true;
    }
}
