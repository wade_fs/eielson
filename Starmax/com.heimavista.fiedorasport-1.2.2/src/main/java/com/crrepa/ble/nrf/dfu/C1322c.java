package com.crrepa.ble.nrf.dfu;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import com.crrepa.ble.nrf.dfu.C1327g;
import com.crrepa.ble.nrf.dfu.p077q.C1344a;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1346a;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1347b;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1353h;
import com.crrepa.ble.nrf.dfu.p077q.p080e.C1358b;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.c */
abstract class C1322c implements C1332j {

    /* renamed from: r */
    protected static final UUID f2060r = new UUID(26392574038016L, -9223371485494954757L);

    /* renamed from: s */
    protected static final UUID f2061s = new UUID(46200963207168L, -9223371485494954757L);

    /* renamed from: t */
    protected static final UUID f2062t = new UUID(45088566677504L, -9223371485494954757L);

    /* renamed from: u */
    protected static final char[] f2063u = "0123456789ABCDEF".toCharArray();

    /* renamed from: a */
    protected final Object f2064a = new Object();

    /* renamed from: b */
    protected InputStream f2065b;

    /* renamed from: c */
    protected InputStream f2066c;

    /* renamed from: d */
    protected BluetoothGatt f2067d;

    /* renamed from: e */
    protected int f2068e;

    /* renamed from: f */
    protected boolean f2069f;

    /* renamed from: g */
    protected boolean f2070g;

    /* renamed from: h */
    protected boolean f2071h;

    /* renamed from: i */
    protected boolean f2072i;

    /* renamed from: j */
    protected boolean f2073j;

    /* renamed from: k */
    protected int f2074k;

    /* renamed from: l */
    protected byte[] f2075l = null;

    /* renamed from: m */
    protected byte[] f2076m = new byte[20];

    /* renamed from: n */
    protected DfuBaseService f2077n;

    /* renamed from: o */
    protected C1330i f2078o;

    /* renamed from: p */
    protected int f2079p;

    /* renamed from: q */
    protected int f2080q;

    /* renamed from: com.crrepa.ble.nrf.dfu.c$a */
    protected class C1323a extends C1327g.C1328a {
        protected C1323a() {
        }

        /* renamed from: a */
        private String m3630a(byte[] bArr) {
            int length;
            if (bArr == null || (length = bArr.length) == 0) {
                return "";
            }
            char[] cArr = new char[((length * 3) - 1)];
            for (int i = 0; i < length; i++) {
                byte b = bArr[i] & 255;
                int i2 = i * 3;
                char[] cArr2 = C1322c.f2063u;
                cArr[i2] = cArr2[b >>> 4];
                cArr[i2 + 1] = cArr2[b & 15];
                if (i != length - 1) {
                    cArr[i2 + 2] = '-';
                }
            }
            return new String(cArr);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String mo11144a(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            return m3630a(bluetoothGattCharacteristic.getValue());
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String mo11145a(BluetoothGattDescriptor bluetoothGattDescriptor) {
            return m3630a(bluetoothGattDescriptor.getValue());
        }

        /* renamed from: a */
        public void mo11146a() {
            C1322c cVar = C1322c.this;
            cVar.f2071h = false;
            cVar.mo11138f();
        }

        public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            C1322c cVar = C1322c.this;
            if (i == 0) {
                DfuBaseService dfuBaseService = cVar.f2077n;
                dfuBaseService.mo11087a(5, "Read Response received from " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + mo11144a(bluetoothGattCharacteristic));
                C1322c.this.f2075l = bluetoothGattCharacteristic.getValue();
                C1322c.this.f2072i = true;
            } else {
                cVar.mo11129a("Characteristic read error: " + i);
                C1322c.this.f2074k = i | 16384;
            }
            C1322c.this.mo11138f();
        }

        public void onDescriptorRead(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            if (i != 0) {
                C1322c cVar = C1322c.this;
                cVar.mo11129a("Descriptor read error: " + i);
                C1322c.this.f2074k = i | 16384;
            } else if (C1322c.f2062t.equals(bluetoothGattDescriptor.getUuid())) {
                DfuBaseService dfuBaseService = C1322c.this.f2077n;
                dfuBaseService.mo11087a(5, "Read Response received from descr." + bluetoothGattDescriptor.getCharacteristic().getUuid() + ", value (0x): " + mo11145a(bluetoothGattDescriptor));
                if (C1322c.f2061s.equals(bluetoothGattDescriptor.getCharacteristic().getUuid())) {
                    C1322c.this.f2072i = true;
                } else {
                    C1322c.this.mo11129a("Unknown descriptor read");
                }
            }
            C1322c.this.mo11138f();
        }

        public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            DfuBaseService dfuBaseService;
            String str;
            StringBuilder sb;
            if (i != 0) {
                C1322c cVar = C1322c.this;
                cVar.mo11129a("Descriptor write error: " + i);
                C1322c.this.f2074k = i | 16384;
            } else if (C1322c.f2062t.equals(bluetoothGattDescriptor.getUuid())) {
                DfuBaseService dfuBaseService2 = C1322c.this.f2077n;
                dfuBaseService2.mo11087a(5, "Data written to descr." + bluetoothGattDescriptor.getCharacteristic().getUuid() + ", value (0x): " + mo11145a(bluetoothGattDescriptor));
                if (C1322c.f2061s.equals(bluetoothGattDescriptor.getCharacteristic().getUuid())) {
                    dfuBaseService = C1322c.this.f2077n;
                    sb = new StringBuilder();
                    str = "Indications enabled for ";
                } else {
                    dfuBaseService = C1322c.this.f2077n;
                    sb = new StringBuilder();
                    str = "Notifications enabled for ";
                }
                sb.append(str);
                sb.append(bluetoothGattDescriptor.getCharacteristic().getUuid());
                dfuBaseService.mo11087a(1, sb.toString());
            }
            C1322c.this.mo11138f();
        }

        public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
            String str;
            C1322c cVar = C1322c.this;
            if (i2 == 0) {
                cVar.f2077n.mo11087a(5, "MTU changed to: " + i);
                int i3 = i + -3;
                C1322c cVar2 = C1322c.this;
                if (i3 > cVar2.f2076m.length) {
                    cVar2.f2076m = new byte[i3];
                }
                cVar = C1322c.this;
                str = "MTU changed to: " + i;
            } else {
                str = "Changing MTU failed: " + i2 + " (mtu: " + i + ")";
            }
            cVar.mo11135c(str);
            C1322c cVar3 = C1322c.this;
            cVar3.f2072i = true;
            cVar3.mo11138f();
        }
    }

    C1322c(Intent intent, DfuBaseService dfuBaseService) {
        this.f2077n = dfuBaseService;
        this.f2078o = dfuBaseService.f2040X;
        this.f2071h = true;
    }

    /* renamed from: a */
    private boolean m3609a(BluetoothDevice bluetoothDevice) {
        try {
            Method method = bluetoothDevice.getClass().getMethod("createBond", new Class[0]);
            if (method != null) {
                this.f2077n.mo11087a(0, "gatt.getDevice().createBond() (hidden)");
                return ((Boolean) method.invoke(bluetoothDevice, new Object[0])).booleanValue();
            }
        } catch (Exception e) {
            Log.w("DfuImpl", "An exception occurred while creating bond", e);
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b5  */
    /* renamed from: j */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean mo11117j() {
        /*
            r7 = this;
            boolean r0 = r7.f2071h
            if (r0 == 0) goto L_0x00c3
            boolean r0 = r7.f2070g
            if (r0 != 0) goto L_0x00bd
            android.bluetooth.BluetoothGatt r0 = r7.f2067d
            java.util.UUID r1 = com.crrepa.ble.nrf.dfu.C1322c.f2060r
            android.bluetooth.BluetoothGattService r1 = r0.getService(r1)
            r2 = 0
            if (r1 != 0) goto L_0x0014
            return r2
        L_0x0014:
            java.util.UUID r3 = com.crrepa.ble.nrf.dfu.C1322c.f2061s
            android.bluetooth.BluetoothGattCharacteristic r1 = r1.getCharacteristic(r3)
            if (r1 != 0) goto L_0x001d
            return r2
        L_0x001d:
            java.util.UUID r3 = com.crrepa.ble.nrf.dfu.C1322c.f2062t
            android.bluetooth.BluetoothGattDescriptor r1 = r1.getDescriptor(r3)
            if (r1 != 0) goto L_0x0026
            return r2
        L_0x0026:
            r7.f2072i = r2
            r7.f2074k = r2
            java.lang.String r3 = "Reading Service Changed CCCD value..."
            r7.mo11134b(r3)
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r7.f2077n
            r4 = 1
            java.lang.String r5 = "Reading Service Changed CCCD value..."
            r3.mo11087a(r4, r5)
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r7.f2077n
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "gatt.readDescriptor("
            r5.append(r6)
            java.util.UUID r6 = r1.getUuid()
            r5.append(r6)
            java.lang.String r6 = ")"
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r3.mo11087a(r2, r5)
            r0.readDescriptor(r1)
            java.lang.Object r0 = r7.f2064a     // Catch:{ InterruptedException -> 0x0077 }
            monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0077 }
        L_0x005c:
            boolean r3 = r7.f2072i     // Catch:{ all -> 0x0074 }
            if (r3 != 0) goto L_0x0068
            boolean r3 = r7.f2071h     // Catch:{ all -> 0x0074 }
            if (r3 == 0) goto L_0x0068
            int r3 = r7.f2074k     // Catch:{ all -> 0x0074 }
            if (r3 == 0) goto L_0x006c
        L_0x0068:
            boolean r3 = r7.f2069f     // Catch:{ all -> 0x0074 }
            if (r3 == 0) goto L_0x0072
        L_0x006c:
            java.lang.Object r3 = r7.f2064a     // Catch:{ all -> 0x0074 }
            r3.wait()     // Catch:{ all -> 0x0074 }
            goto L_0x005c
        L_0x0072:
            monitor-exit(r0)     // Catch:{ all -> 0x0074 }
            goto L_0x007d
        L_0x0074:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0074 }
            throw r3     // Catch:{ InterruptedException -> 0x0077 }
        L_0x0077:
            r0 = move-exception
            java.lang.String r3 = "Sleeping interrupted"
            r7.mo11130a(r3, r0)
        L_0x007d:
            int r0 = r7.f2074k
            if (r0 != 0) goto L_0x00b5
            boolean r0 = r7.f2071h
            if (r0 == 0) goto L_0x00ad
            byte[] r0 = r1.getValue()
            if (r0 == 0) goto L_0x00ac
            byte[] r0 = r1.getValue()
            int r0 = r0.length
            r3 = 2
            if (r0 != r3) goto L_0x00ac
            byte[] r0 = r1.getValue()
            byte r0 = r0[r2]
            byte[] r3 = android.bluetooth.BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
            byte r3 = r3[r2]
            if (r0 != r3) goto L_0x00ac
            byte[] r0 = r1.getValue()
            byte r0 = r0[r4]
            byte[] r1 = android.bluetooth.BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
            byte r1 = r1[r4]
            if (r0 != r1) goto L_0x00ac
            r2 = 1
        L_0x00ac:
            return r2
        L_0x00ad:
            com.crrepa.ble.nrf.dfu.q.c.a r0 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r1 = "Unable to read Service Changed CCCD: device disconnected"
            r0.<init>(r1)
            throw r0
        L_0x00b5:
            com.crrepa.ble.nrf.dfu.q.c.b r1 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r2 = "Unable to read Service Changed CCCD"
            r1.<init>(r2, r0)
            throw r1
        L_0x00bd:
            com.crrepa.ble.nrf.dfu.q.c.h r0 = new com.crrepa.ble.nrf.dfu.q.c.h
            r0.<init>()
            throw r0
        L_0x00c3:
            com.crrepa.ble.nrf.dfu.q.c.a r0 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r1 = "Unable to read Service Changed CCCD: device disconnected"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1322c.mo11117j():boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String mo11123a(byte[] bArr) {
        int length;
        if (bArr == null || (length = bArr.length) == 0) {
            return "";
        }
        char[] cArr = new char[((length * 3) - 1)];
        for (int i = 0; i < length; i++) {
            byte b = bArr[i] & 255;
            int i2 = i * 3;
            char[] cArr2 = f2063u;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & 15];
            if (i != length - 1) {
                cArr[i2 + 2] = '-';
            }
        }
        return new String(cArr);
    }

    /* renamed from: a */
    public void mo11124a() {
        this.f2069f = false;
        this.f2070g = true;
        mo11138f();
    }

    /* renamed from: a */
    public void mo11125a(int i) {
        this.f2072i = true;
        mo11138f();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00db, code lost:
        if (r10.f2074k == 0) goto L_0x00e1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0137  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo11126a(android.bluetooth.BluetoothGattCharacteristic r11, int r12) {
        /*
            r10 = this;
            android.bluetooth.BluetoothGatt r0 = r10.f2067d
            r1 = 1
            if (r12 != r1) goto L_0x0008
            java.lang.String r2 = "notifications"
            goto L_0x000a
        L_0x0008:
            java.lang.String r2 = "indications"
        L_0x000a:
            boolean r3 = r10.f2071h
            if (r3 == 0) goto L_0x015b
            boolean r3 = r10.f2070g
            if (r3 != 0) goto L_0x0155
            r3 = 0
            r10.f2075l = r3
            r3 = 0
            r10.f2074k = r3
            java.util.UUID r4 = com.crrepa.ble.nrf.dfu.C1322c.f2062t
            android.bluetooth.BluetoothGattDescriptor r4 = r11.getDescriptor(r4)
            byte[] r5 = r4.getValue()
            r6 = 2
            if (r5 == 0) goto L_0x003e
            byte[] r5 = r4.getValue()
            int r5 = r5.length
            if (r5 != r6) goto L_0x003e
            byte[] r5 = r4.getValue()
            byte r5 = r5[r3]
            if (r5 <= 0) goto L_0x003e
            byte[] r5 = r4.getValue()
            byte r5 = r5[r1]
            if (r5 != 0) goto L_0x003e
            r5 = 1
            goto L_0x003f
        L_0x003e:
            r5 = 0
        L_0x003f:
            if (r5 == 0) goto L_0x0042
            return
        L_0x0042:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Enabling "
            r7.append(r8)
            r7.append(r2)
            java.lang.String r8 = "..."
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r10.mo11134b(r7)
            com.crrepa.ble.nrf.dfu.DfuBaseService r7 = r10.f2077n
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Enabling "
            r8.append(r9)
            r8.append(r2)
            java.lang.String r9 = " for "
            r8.append(r9)
            java.util.UUID r9 = r11.getUuid()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.mo11087a(r1, r8)
            com.crrepa.ble.nrf.dfu.DfuBaseService r7 = r10.f2077n
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "gatt.setCharacteristicNotification("
            r8.append(r9)
            java.util.UUID r9 = r11.getUuid()
            r8.append(r9)
            java.lang.String r9 = ", true)"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.mo11087a(r3, r8)
            r0.setCharacteristicNotification(r11, r1)
            if (r12 != r1) goto L_0x00a4
            byte[] r11 = android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            goto L_0x00a6
        L_0x00a4:
            byte[] r11 = android.bluetooth.BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        L_0x00a6:
            r4.setValue(r11)
            com.crrepa.ble.nrf.dfu.DfuBaseService r11 = r10.f2077n
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "gatt.writeDescriptor("
            r7.append(r8)
            java.util.UUID r8 = r4.getUuid()
            r7.append(r8)
            if (r12 != r1) goto L_0x00c1
            java.lang.String r12 = ", value=0x01-00)"
            goto L_0x00c3
        L_0x00c1:
            java.lang.String r12 = ", value=0x02-00)"
        L_0x00c3:
            r7.append(r12)
            java.lang.String r12 = r7.toString()
            r11.mo11087a(r3, r12)
            r0.writeDescriptor(r4)
            java.lang.Object r11 = r10.f2064a     // Catch:{ InterruptedException -> 0x010c }
            monitor-enter(r11)     // Catch:{ InterruptedException -> 0x010c }
        L_0x00d3:
            if (r5 != 0) goto L_0x00dd
            boolean r12 = r10.f2071h     // Catch:{ all -> 0x0109 }
            if (r12 == 0) goto L_0x00dd
            int r12 = r10.f2074k     // Catch:{ all -> 0x0109 }
            if (r12 == 0) goto L_0x00e1
        L_0x00dd:
            boolean r12 = r10.f2069f     // Catch:{ all -> 0x0109 }
            if (r12 == 0) goto L_0x0107
        L_0x00e1:
            java.lang.Object r12 = r10.f2064a     // Catch:{ all -> 0x0109 }
            r12.wait()     // Catch:{ all -> 0x0109 }
            byte[] r12 = r4.getValue()     // Catch:{ all -> 0x0109 }
            if (r12 == 0) goto L_0x0105
            byte[] r12 = r4.getValue()     // Catch:{ all -> 0x0109 }
            int r12 = r12.length     // Catch:{ all -> 0x0109 }
            if (r12 != r6) goto L_0x0105
            byte[] r12 = r4.getValue()     // Catch:{ all -> 0x0109 }
            byte r12 = r12[r3]     // Catch:{ all -> 0x0109 }
            if (r12 <= 0) goto L_0x0105
            byte[] r12 = r4.getValue()     // Catch:{ all -> 0x0109 }
            byte r12 = r12[r1]     // Catch:{ all -> 0x0109 }
            if (r12 != 0) goto L_0x0105
            r5 = 1
            goto L_0x00d3
        L_0x0105:
            r5 = 0
            goto L_0x00d3
        L_0x0107:
            monitor-exit(r11)     // Catch:{ all -> 0x0109 }
            goto L_0x0112
        L_0x0109:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0109 }
            throw r12     // Catch:{ InterruptedException -> 0x010c }
        L_0x010c:
            r11 = move-exception
            java.lang.String r12 = "Sleeping interrupted"
            r10.mo11130a(r12, r11)
        L_0x0112:
            int r11 = r10.f2074k
            if (r11 != 0) goto L_0x0137
            boolean r11 = r10.f2071h
            if (r11 == 0) goto L_0x011b
            return
        L_0x011b:
            com.crrepa.ble.nrf.dfu.q.c.a r11 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "Unable to set "
            r12.append(r0)
            r12.append(r2)
            java.lang.String r0 = " state: device disconnected"
            r12.append(r0)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12)
            throw r11
        L_0x0137:
            com.crrepa.ble.nrf.dfu.q.c.b r11 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "Unable to set "
            r12.append(r0)
            r12.append(r2)
            java.lang.String r0 = " state"
            r12.append(r0)
            java.lang.String r12 = r12.toString()
            int r0 = r10.f2074k
            r11.<init>(r12, r0)
            throw r11
        L_0x0155:
            com.crrepa.ble.nrf.dfu.q.c.h r11 = new com.crrepa.ble.nrf.dfu.q.c.h
            r11.<init>()
            throw r11
        L_0x015b:
            com.crrepa.ble.nrf.dfu.q.c.a r11 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "Unable to set "
            r12.append(r0)
            r12.append(r2)
            java.lang.String r0 = " state: device disconnected"
            r12.append(r0)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1322c.mo11126a(android.bluetooth.BluetoothGattCharacteristic, int):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11127a(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, boolean z) {
        if (!this.f2070g) {
            this.f2075l = null;
            this.f2074k = 0;
            this.f2072i = false;
            this.f2073j = z;
            bluetoothGattCharacteristic.setWriteType(2);
            bluetoothGattCharacteristic.setValue(bArr);
            DfuBaseService dfuBaseService = this.f2077n;
            dfuBaseService.mo11087a(1, "Writing to characteristic " + bluetoothGattCharacteristic.getUuid());
            DfuBaseService dfuBaseService2 = this.f2077n;
            dfuBaseService2.mo11087a(0, "gatt.writeCharacteristic(" + bluetoothGattCharacteristic.getUuid() + ")");
            this.f2067d.writeCharacteristic(bluetoothGattCharacteristic);
            try {
                synchronized (this.f2064a) {
                    while (true) {
                        if ((!this.f2072i && this.f2071h && this.f2074k == 0) || this.f2069f) {
                            this.f2064a.wait();
                        }
                    }
                    if (this.f2073j && this.f2074k != 0) {
                        throw new C1347b("Unable to write Op Code " + ((int) bArr[0]), this.f2074k);
                    } else if (!this.f2073j && !this.f2071h) {
                        throw new C1346a("Unable to write Op Code " + ((int) bArr[0]) + ": device disconnected");
                    } else {
                        return;
                    }
                }
                break;
            } catch (InterruptedException e) {
                mo11130a("Sleeping interrupted", e);
            }
            if (this.f2073j) {
            }
            if (!this.f2073j) {
                return;
            }
            return;
        }
        throw new C1353h();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11128a(Intent intent, boolean z) {
        String str;
        String str2;
        if (z) {
            this.f2077n.mo11087a(1, "Scanning for the DFU Bootloader...");
            str = C1358b.m3753a().mo11206a(this.f2067d.getDevice().getAddress());
            mo11134b("Scanning for new address finished with: " + str);
            DfuBaseService dfuBaseService = this.f2077n;
            if (str != null) {
                str2 = "DFU Bootloader found with address " + str;
            } else {
                str2 = "DFU Bootloader not found. Trying the same address...";
            }
            dfuBaseService.mo11087a(5, str2);
        } else {
            str = null;
        }
        if (str != null) {
            intent.putExtra("no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS", str);
        }
        this.f2077n.startService(intent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11129a(String str) {
        Log.e("DfuImpl", str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11130a(String str, Throwable th) {
        Log.e("DfuImpl", str, th);
    }

    /* renamed from: a */
    public boolean mo11131a(Intent intent, BluetoothGatt bluetoothGatt, int i, InputStream inputStream, InputStream inputStream2) {
        int i2;
        BluetoothGattService service;
        BluetoothGattCharacteristic characteristic;
        this.f2067d = bluetoothGatt;
        this.f2068e = i;
        this.f2065b = inputStream;
        this.f2066c = inputStream2;
        int intExtra = intent.getIntExtra("no.nordicsemi.android.dfu.extra.EXTRA_PART_CURRENT", 1);
        int intExtra2 = intent.getIntExtra("no.nordicsemi.android.dfu.extra.EXTRA_PARTS_TOTAL", 1);
        if (i > 4) {
            mo11135c("DFU target does not support (SD/BL)+App update, splitting into 2 parts");
            this.f2077n.mo11087a(15, "Sending system components");
            this.f2068e &= -5;
            ((C1344a) this.f2065b).mo11176a(this.f2068e);
            intExtra2 = 2;
        }
        if (intExtra == 2) {
            this.f2077n.mo11087a(15, "Sending application");
        }
        int i3 = 0;
        try {
            i2 = inputStream2.available();
        } catch (Exception unused) {
            i2 = 0;
        }
        this.f2080q = i2;
        try {
            i3 = inputStream.available();
        } catch (Exception unused2) {
        }
        this.f2079p = i3;
        this.f2078o.mo11156a(i3, intExtra, intExtra2);
        if (Build.VERSION.SDK_INT < 23 && bluetoothGatt.getDevice().getBondState() == 12 && (service = bluetoothGatt.getService(f2060r)) != null && (characteristic = service.getCharacteristic(f2061s)) != null) {
            if (!mo11117j()) {
                mo11126a(characteristic, 2);
            }
            this.f2077n.mo11087a(10, "Service Changed indications enabled");
        }
        return true;
    }

    /* renamed from: b */
    public void mo11132b() {
        this.f2069f = false;
        mo11138f();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005c  */
    @androidx.annotation.RequiresApi(api = 21)
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo11133b(int r5) {
        /*
            r4 = this;
            boolean r0 = r4.f2070g
            if (r0 != 0) goto L_0x0064
            r0 = 0
            r4.f2072i = r0
            com.crrepa.ble.nrf.dfu.DfuBaseService r1 = r4.f2077n
            r2 = 1
            java.lang.String r3 = "Requesting new MTU..."
            r1.mo11087a(r2, r3)
            com.crrepa.ble.nrf.dfu.DfuBaseService r1 = r4.f2077n
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "gatt.requestMtu("
            r2.append(r3)
            r2.append(r5)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.mo11087a(r0, r2)
            android.bluetooth.BluetoothGatt r0 = r4.f2067d
            boolean r5 = r0.requestMtu(r5)
            if (r5 != 0) goto L_0x0033
            return
        L_0x0033:
            java.lang.Object r5 = r4.f2064a     // Catch:{ InterruptedException -> 0x0051 }
            monitor-enter(r5)     // Catch:{ InterruptedException -> 0x0051 }
        L_0x0036:
            boolean r0 = r4.f2072i     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x0042
            boolean r0 = r4.f2071h     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0042
            int r0 = r4.f2074k     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0046
        L_0x0042:
            boolean r0 = r4.f2069f     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x004c
        L_0x0046:
            java.lang.Object r0 = r4.f2064a     // Catch:{ all -> 0x004e }
            r0.wait()     // Catch:{ all -> 0x004e }
            goto L_0x0036
        L_0x004c:
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            goto L_0x0057
        L_0x004e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            throw r0     // Catch:{ InterruptedException -> 0x0051 }
        L_0x0051:
            r5 = move-exception
            java.lang.String r0 = "Sleeping interrupted"
            r4.mo11130a(r0, r5)
        L_0x0057:
            boolean r5 = r4.f2071h
            if (r5 == 0) goto L_0x005c
            return
        L_0x005c:
            com.crrepa.ble.nrf.dfu.q.c.a r5 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r0 = "Unable to read Service Changed CCCD: device disconnected"
            r5.<init>(r0)
            throw r5
        L_0x0064:
            com.crrepa.ble.nrf.dfu.q.c.h r5 = new com.crrepa.ble.nrf.dfu.q.c.h
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1322c.mo11133b(int):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo11134b(String str) {
        if (DfuBaseService.f2031f0) {
            Log.i("DfuImpl", str);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo11135c(String str) {
        if (DfuBaseService.f2031f0) {
            Log.w("DfuImpl", str);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: d */
    public boolean mo11136d() {
        boolean z;
        BluetoothDevice device = this.f2067d.getDevice();
        if (device.getBondState() == 12) {
            return true;
        }
        this.f2072i = false;
        this.f2077n.mo11087a(1, "Starting pairing...");
        if (Build.VERSION.SDK_INT >= 19) {
            this.f2077n.mo11087a(0, "gatt.getDevice().createBond()");
            z = device.createBond();
        } else {
            z = m3609a(device);
        }
        try {
            synchronized (this.f2064a) {
                while (!this.f2072i && !this.f2070g) {
                    this.f2064a.wait();
                }
            }
        } catch (InterruptedException e) {
            mo11130a("Sleeping interrupted", e);
        }
        return z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean mo11137e() {
        return this.f2067d.getDevice().getBondState() == 12;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11138f() {
        synchronized (this.f2064a) {
            this.f2064a.notifyAll();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0047  */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] mo11139g() {
        /*
            r3 = this;
            java.lang.Object r0 = r3.f2064a     // Catch:{ InterruptedException -> 0x0022 }
            monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0022 }
        L_0x0003:
            byte[] r1 = r3.f2075l     // Catch:{ all -> 0x001f }
            if (r1 != 0) goto L_0x0013
            boolean r1 = r3.f2071h     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x0013
            int r1 = r3.f2074k     // Catch:{ all -> 0x001f }
            if (r1 != 0) goto L_0x0013
            boolean r1 = r3.f2070g     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x0017
        L_0x0013:
            boolean r1 = r3.f2069f     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x001d
        L_0x0017:
            java.lang.Object r1 = r3.f2064a     // Catch:{ all -> 0x001f }
            r1.wait()     // Catch:{ all -> 0x001f }
            goto L_0x0003
        L_0x001d:
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            goto L_0x0028
        L_0x001f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            throw r1     // Catch:{ InterruptedException -> 0x0022 }
        L_0x0022:
            r0 = move-exception
            java.lang.String r1 = "Sleeping interrupted"
            r3.mo11130a(r1, r0)
        L_0x0028:
            boolean r0 = r3.f2070g
            if (r0 != 0) goto L_0x0047
            int r0 = r3.f2074k
            if (r0 != 0) goto L_0x003f
            boolean r0 = r3.f2071h
            if (r0 == 0) goto L_0x0037
            byte[] r0 = r3.f2075l
            return r0
        L_0x0037:
            com.crrepa.ble.nrf.dfu.q.c.a r0 = new com.crrepa.ble.nrf.dfu.q.c.a
            java.lang.String r1 = "Unable to write Op Code: device disconnected"
            r0.<init>(r1)
            throw r0
        L_0x003f:
            com.crrepa.ble.nrf.dfu.q.c.b r1 = new com.crrepa.ble.nrf.dfu.q.c.b
            java.lang.String r2 = "Unable to write Op Code"
            r1.<init>(r2, r0)
            throw r1
        L_0x0047:
            com.crrepa.ble.nrf.dfu.q.c.h r0 = new com.crrepa.ble.nrf.dfu.q.c.h
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1322c.mo11139g():byte[]");
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public boolean mo11140h() {
        BluetoothDevice device = this.f2067d.getDevice();
        boolean z = true;
        if (device.getBondState() == 10) {
            return true;
        }
        this.f2077n.mo11087a(1, "Removing bond information...");
        try {
            Method method = device.getClass().getMethod("removeBond", new Class[0]);
            if (method != null) {
                this.f2072i = false;
                this.f2077n.mo11087a(0, "gatt.getDevice().removeBond() (hidden)");
                boolean booleanValue = ((Boolean) method.invoke(device, new Object[0])).booleanValue();
                try {
                    synchronized (this.f2064a) {
                        while (!this.f2072i && !this.f2070g) {
                            this.f2064a.wait();
                        }
                    }
                } catch (InterruptedException e) {
                    try {
                        mo11130a("Sleeping interrupted", e);
                    } catch (Exception e2) {
                        z = booleanValue;
                        e = e2;
                    }
                }
            }
        } catch (Exception e3) {
            e = e3;
            z = false;
            Log.w("DfuImpl", "An exception occurred while removing bond information", e);
            return z;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo11141i() {
        try {
            synchronized (this.f2064a) {
                while (this.f2069f) {
                    this.f2064a.wait();
                }
            }
        } catch (InterruptedException e) {
            mo11130a("Sleeping interrupted", e);
        }
    }

    public void pause() {
        this.f2069f = true;
    }

    public void release() {
        this.f2077n = null;
    }
}
