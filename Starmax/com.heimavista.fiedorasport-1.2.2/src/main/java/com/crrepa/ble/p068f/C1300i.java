package com.crrepa.ble.p068f;

import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: com.crrepa.ble.f.i */
public class C1300i {

    /* renamed from: a */
    private static final char[] f2020a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b */
    private static MessageDigest f2021b;

    static {
        f2021b = null;
        try {
            f2021b = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public static String m3529a(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read > 0) {
                f2021b.update(bArr, 0, read);
            } else {
                fileInputStream.close();
                return m3530a(f2021b.digest());
            }
        }
    }

    /* renamed from: a */
    private static String m3530a(byte[] bArr) {
        return m3531a(bArr, 0, bArr.length);
    }

    /* renamed from: a */
    private static String m3531a(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(i2 * 2);
        int i3 = i2 + i;
        while (i < i3) {
            m3532a(bArr[i], stringBuffer);
            i++;
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    private static void m3532a(byte b, StringBuffer stringBuffer) {
        char[] cArr = f2020a;
        char c = cArr[(b & 240) >> 4];
        char c2 = cArr[b & 15];
        stringBuffer.append(c);
        stringBuffer.append(c2);
    }

    /* renamed from: a */
    public static boolean m3533a(String str, File file) {
        if (!TextUtils.isEmpty(str) && file != null && file.exists()) {
            String str2 = null;
            try {
                str2 = m3529a(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str2 != null && TextUtils.equals(str.toLowerCase(), str2.toLowerCase());
        }
    }
}
