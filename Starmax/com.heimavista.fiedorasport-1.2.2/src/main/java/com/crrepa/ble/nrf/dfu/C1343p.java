package com.crrepa.ble.nrf.dfu;

/* renamed from: com.crrepa.ble.nrf.dfu.p */
class C1343p {
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x012b  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void m3724a(android.content.Intent r7) {
        /*
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_LEGACY_DFU"
            android.os.Parcelable[] r0 = r7.getParcelableArrayExtra(r0)
            r1 = 3
            r2 = 2
            r3 = 1
            r4 = 0
            if (r0 == 0) goto L_0x005f
            int r5 = r0.length
            r6 = 4
            if (r5 != r6) goto L_0x005f
            r5 = r0[r4]
            if (r5 == 0) goto L_0x001d
            r5 = r0[r4]
            android.os.ParcelUuid r5 = (android.os.ParcelUuid) r5
            java.util.UUID r5 = r5.getUuid()
            goto L_0x001f
        L_0x001d:
            java.util.UUID r5 = com.crrepa.ble.nrf.dfu.C1336n.f2118E
        L_0x001f:
            com.crrepa.ble.nrf.dfu.C1336n.f2122I = r5
            r5 = r0[r3]
            if (r5 == 0) goto L_0x002e
            r5 = r0[r3]
            android.os.ParcelUuid r5 = (android.os.ParcelUuid) r5
            java.util.UUID r5 = r5.getUuid()
            goto L_0x0030
        L_0x002e:
            java.util.UUID r5 = com.crrepa.ble.nrf.dfu.C1336n.f2119F
        L_0x0030:
            com.crrepa.ble.nrf.dfu.C1336n.f2123J = r5
            r5 = r0[r2]
            if (r5 == 0) goto L_0x003f
            r5 = r0[r2]
            android.os.ParcelUuid r5 = (android.os.ParcelUuid) r5
            java.util.UUID r5 = r5.getUuid()
            goto L_0x0041
        L_0x003f:
            java.util.UUID r5 = com.crrepa.ble.nrf.dfu.C1336n.f2120G
        L_0x0041:
            com.crrepa.ble.nrf.dfu.C1336n.f2124K = r5
            r5 = r0[r1]
            if (r5 == 0) goto L_0x0050
            r0 = r0[r1]
            android.os.ParcelUuid r0 = (android.os.ParcelUuid) r0
            java.util.UUID r0 = r0.getUuid()
            goto L_0x0052
        L_0x0050:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2121H
        L_0x0052:
            com.crrepa.ble.nrf.dfu.C1336n.f2125L = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2122I
            com.crrepa.ble.nrf.dfu.C1335m.f2114y = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2123J
            com.crrepa.ble.nrf.dfu.C1335m.f2115z = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2125L
            goto L_0x0079
        L_0x005f:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2118E
            com.crrepa.ble.nrf.dfu.C1336n.f2122I = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2119F
            com.crrepa.ble.nrf.dfu.C1336n.f2123J = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2120G
            com.crrepa.ble.nrf.dfu.C1336n.f2124K = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2121H
            com.crrepa.ble.nrf.dfu.C1336n.f2125L = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2118E
            com.crrepa.ble.nrf.dfu.C1335m.f2114y = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2119F
            com.crrepa.ble.nrf.dfu.C1335m.f2115z = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1336n.f2121H
        L_0x0079:
            com.crrepa.ble.nrf.dfu.C1335m.f2112A = r0
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_SECURE_DFU"
            android.os.Parcelable[] r0 = r7.getParcelableArrayExtra(r0)
            if (r0 == 0) goto L_0x00b5
            int r5 = r0.length
            if (r5 != r1) goto L_0x00b5
            r1 = r0[r4]
            if (r1 == 0) goto L_0x0093
            r1 = r0[r4]
            android.os.ParcelUuid r1 = (android.os.ParcelUuid) r1
            java.util.UUID r1 = r1.getUuid()
            goto L_0x0095
        L_0x0093:
            java.util.UUID r1 = com.crrepa.ble.nrf.dfu.C1338o.f2140D
        L_0x0095:
            com.crrepa.ble.nrf.dfu.C1338o.f2143G = r1
            r1 = r0[r3]
            if (r1 == 0) goto L_0x00a4
            r1 = r0[r3]
            android.os.ParcelUuid r1 = (android.os.ParcelUuid) r1
            java.util.UUID r1 = r1.getUuid()
            goto L_0x00a6
        L_0x00a4:
            java.util.UUID r1 = com.crrepa.ble.nrf.dfu.C1338o.f2141E
        L_0x00a6:
            com.crrepa.ble.nrf.dfu.C1338o.f2144H = r1
            r1 = r0[r2]
            if (r1 == 0) goto L_0x00bd
            r0 = r0[r2]
            android.os.ParcelUuid r0 = (android.os.ParcelUuid) r0
            java.util.UUID r0 = r0.getUuid()
            goto L_0x00bf
        L_0x00b5:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1338o.f2140D
            com.crrepa.ble.nrf.dfu.C1338o.f2143G = r0
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1338o.f2141E
            com.crrepa.ble.nrf.dfu.C1338o.f2144H = r0
        L_0x00bd:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1338o.f2142F
        L_0x00bf:
            com.crrepa.ble.nrf.dfu.C1338o.f2145I = r0
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_EXPERIMENTAL_BUTTONLESS_DFU"
            android.os.Parcelable[] r0 = r7.getParcelableArrayExtra(r0)
            if (r0 == 0) goto L_0x00ea
            int r1 = r0.length
            if (r1 != r2) goto L_0x00ea
            r1 = r0[r4]
            if (r1 == 0) goto L_0x00d9
            r1 = r0[r4]
            android.os.ParcelUuid r1 = (android.os.ParcelUuid) r1
            java.util.UUID r1 = r1.getUuid()
            goto L_0x00db
        L_0x00d9:
            java.util.UUID r1 = com.crrepa.ble.nrf.dfu.C1334l.f2109y
        L_0x00db:
            com.crrepa.ble.nrf.dfu.C1334l.f2107A = r1
            r1 = r0[r3]
            if (r1 == 0) goto L_0x00ee
            r0 = r0[r3]
            android.os.ParcelUuid r0 = (android.os.ParcelUuid) r0
            java.util.UUID r0 = r0.getUuid()
            goto L_0x00f0
        L_0x00ea:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1334l.f2109y
            com.crrepa.ble.nrf.dfu.C1334l.f2107A = r0
        L_0x00ee:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1334l.f2110z
        L_0x00f0:
            com.crrepa.ble.nrf.dfu.C1334l.f2108B = r0
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITHOUT_BOND_SHARING"
            android.os.Parcelable[] r0 = r7.getParcelableArrayExtra(r0)
            if (r0 == 0) goto L_0x011b
            int r1 = r0.length
            if (r1 != r2) goto L_0x011b
            r1 = r0[r4]
            if (r1 == 0) goto L_0x010a
            r1 = r0[r4]
            android.os.ParcelUuid r1 = (android.os.ParcelUuid) r1
            java.util.UUID r1 = r1.getUuid()
            goto L_0x010c
        L_0x010a:
            java.util.UUID r1 = com.crrepa.ble.nrf.dfu.C1326f.f2090y
        L_0x010c:
            com.crrepa.ble.nrf.dfu.C1326f.f2088A = r1
            r1 = r0[r3]
            if (r1 == 0) goto L_0x011f
            r0 = r0[r3]
            android.os.ParcelUuid r0 = (android.os.ParcelUuid) r0
            java.util.UUID r0 = r0.getUuid()
            goto L_0x0121
        L_0x011b:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1326f.f2090y
            com.crrepa.ble.nrf.dfu.C1326f.f2088A = r0
        L_0x011f:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1326f.f2091z
        L_0x0121:
            com.crrepa.ble.nrf.dfu.C1326f.f2089B = r0
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITH_BOND_SHARING"
            android.os.Parcelable[] r7 = r7.getParcelableArrayExtra(r0)
            if (r7 == 0) goto L_0x014c
            int r0 = r7.length
            if (r0 != r2) goto L_0x014c
            r0 = r7[r4]
            if (r0 == 0) goto L_0x013b
            r0 = r7[r4]
            android.os.ParcelUuid r0 = (android.os.ParcelUuid) r0
            java.util.UUID r0 = r0.getUuid()
            goto L_0x013d
        L_0x013b:
            java.util.UUID r0 = com.crrepa.ble.nrf.dfu.C1325e.f2085y
        L_0x013d:
            com.crrepa.ble.nrf.dfu.C1325e.f2083A = r0
            r0 = r7[r3]
            if (r0 == 0) goto L_0x0150
            r7 = r7[r3]
            android.os.ParcelUuid r7 = (android.os.ParcelUuid) r7
            java.util.UUID r7 = r7.getUuid()
            goto L_0x0152
        L_0x014c:
            java.util.UUID r7 = com.crrepa.ble.nrf.dfu.C1325e.f2085y
            com.crrepa.ble.nrf.dfu.C1325e.f2083A = r7
        L_0x0150:
            java.util.UUID r7 = com.crrepa.ble.nrf.dfu.C1325e.f2086z
        L_0x0152:
            com.crrepa.ble.nrf.dfu.C1325e.f2084B = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1343p.m3724a(android.content.Intent):void");
    }
}
