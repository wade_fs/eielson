package com.crrepa.ble.p049d.p057k;

import android.bluetooth.BluetoothGattCharacteristic;
import com.crrepa.ble.p049d.p053g.C1179a;
import com.crrepa.ble.p049d.p061o.C1250a;
import com.crrepa.ble.p049d.p061o.C1252c;
import com.crrepa.ble.p049d.p062p.C1255a;
import com.crrepa.ble.p049d.p063q.C1262d;
import com.crrepa.ble.p068f.C1293b;

/* renamed from: com.crrepa.ble.d.k.c */
public class C1217c extends C1223f {

    /* renamed from: f */
    private static C1217c f1929f;

    /* renamed from: a */
    private byte[] f1930a = null;

    /* renamed from: b */
    private boolean f1931b = true;

    /* renamed from: c */
    private int f1932c = 0;

    /* renamed from: d */
    private byte f1933d;

    /* renamed from: e */
    private C1262d f1934e;

    /* renamed from: com.crrepa.ble.d.k.c$a */
    class C1218a implements Runnable {
        C1218a(C1217c cVar) {
        }

        public void run() {
            C1252c.m3359d().mo10983b();
        }
    }

    private C1217c() {
    }

    /* renamed from: a */
    private void m3266a(int i, long j) {
        C1262d dVar;
        if (this.f1933d == 99 && (dVar = this.f1934e) != null) {
            dVar.mo11012a(i);
        }
        C1179a.m3085a(new C1218a(this), j);
    }

    /* renamed from: a */
    private void m3267a(byte[] bArr, byte b) {
        C1293b.m3503a("writeCompleted: " + this.f1931b);
        if (this.f1931b) {
            this.f1933d = b;
            C1293b.m3504b("WriteCmd: " + ((int) this.f1933d));
            this.f1930a = bArr;
            this.f1931b = false;
            m3272h();
        }
    }

    /* renamed from: a */
    private void m3268a(byte[] bArr, int i) {
        if (bArr != null) {
            C1252c.m3359d().mo10980a(new C1250a(i, bArr));
        }
    }

    /* renamed from: e */
    public static C1217c m3269e() {
        if (f1929f == null) {
            f1929f = new C1217c();
        }
        return f1929f;
    }

    /* renamed from: f */
    private void m3270f() {
        byte b = this.f1933d;
        long j = (b == 1 || b == 2) ? 0 : 50;
        mo10939c();
        m3266a(1, j);
    }

    /* renamed from: g */
    private BluetoothGattCharacteristic m3271g() {
        BluetoothGattCharacteristic i;
        C1255a b = mo10953b();
        if (b == null) {
            return null;
        }
        byte b2 = this.f1933d;
        if (b2 == 1) {
            i = b.mo10994j();
        } else if (b2 != 2) {
            return b.mo10995k();
        } else {
            i = b.mo10993i();
        }
        i.setWriteType(1);
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        return;
     */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m3272h() {
        /*
            r8 = this;
            monitor-enter(r8)
            byte[] r0 = r8.f1930a     // Catch:{ all -> 0x005c }
            int r0 = r0.length     // Catch:{ all -> 0x005c }
            int r1 = r8.f1932c     // Catch:{ all -> 0x005c }
            int r0 = r0 - r1
            int r1 = com.crrepa.ble.p049d.p051e.C1146f.m2958a()     // Catch:{ all -> 0x005c }
            if (r0 <= r1) goto L_0x000f
            r0 = r1
            goto L_0x0016
        L_0x000f:
            if (r0 > 0) goto L_0x0016
            r8.m3270f()     // Catch:{ all -> 0x005c }
            monitor-exit(r8)
            return
        L_0x0016:
            android.bluetooth.BluetoothGattCharacteristic r1 = r8.m3271g()     // Catch:{ all -> 0x005c }
            com.crrepa.ble.d.m.a r2 = com.crrepa.ble.p049d.p059m.C1242a.m3332c()     // Catch:{ all -> 0x005c }
            android.bluetooth.BluetoothGatt r2 = r2.mo10961a()     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0057
            if (r2 != 0) goto L_0x0027
            goto L_0x0057
        L_0x0027:
            byte[] r3 = new byte[r0]     // Catch:{ all -> 0x005c }
            byte[] r4 = r8.f1930a     // Catch:{ all -> 0x005c }
            int r5 = r8.f1932c     // Catch:{ all -> 0x005c }
            int r6 = r3.length     // Catch:{ all -> 0x005c }
            r7 = 0
            java.lang.System.arraycopy(r4, r5, r3, r7, r6)     // Catch:{ all -> 0x005c }
            r1.setValue(r3)     // Catch:{ all -> 0x005c }
            boolean r1 = r2.writeCharacteristic(r1)     // Catch:{ all -> 0x005c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
            r2.<init>()     // Catch:{ all -> 0x005c }
            java.lang.String r3 = "writeCharacteristic: "
            r2.append(r3)     // Catch:{ all -> 0x005c }
            r2.append(r1)     // Catch:{ all -> 0x005c }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x005c }
            com.crrepa.ble.p068f.C1293b.m3504b(r2)     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0055
            int r1 = r8.f1932c     // Catch:{ all -> 0x005c }
            int r1 = r1 + r0
            r8.f1932c = r1     // Catch:{ all -> 0x005c }
        L_0x0055:
            monitor-exit(r8)
            return
        L_0x0057:
            r8.mo10952a()     // Catch:{ all -> 0x005c }
            monitor-exit(r8)
            return
        L_0x005c:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.p049d.p057k.C1217c.m3272h():void");
    }

    /* renamed from: a */
    public void mo10937a(byte[] bArr) {
        m3268a(bArr, 0);
    }

    /* renamed from: b */
    public void mo10938b(byte[] bArr) {
        m3268a(bArr, 1);
    }

    /* renamed from: c */
    public void mo10939c() {
        this.f1932c = 0;
        this.f1931b = true;
    }

    /* renamed from: c */
    public void mo10940c(byte[] bArr) {
        m3268a(bArr, 2);
    }

    /* renamed from: d */
    public void mo10941d() {
        m3272h();
    }

    /* renamed from: d */
    public void mo10942d(byte[] bArr) {
        m3267a(bArr, bArr[4]);
    }

    /* renamed from: e */
    public void mo10943e(byte[] bArr) {
        m3267a(bArr, (byte) 1);
    }

    /* renamed from: f */
    public void mo10944f(byte[] bArr) {
        m3267a(bArr, (byte) 2);
    }
}
