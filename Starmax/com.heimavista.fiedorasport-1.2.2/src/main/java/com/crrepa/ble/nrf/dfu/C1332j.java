package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import java.io.InputStream;

/* renamed from: com.crrepa.ble.nrf.dfu.j */
interface C1332j extends C1327g {
    /* renamed from: a */
    void mo11151a(Intent intent);

    /* renamed from: a */
    boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt);

    /* renamed from: a */
    boolean mo11131a(Intent intent, BluetoothGatt bluetoothGatt, int i, InputStream inputStream, InputStream inputStream2);

    void release();
}
