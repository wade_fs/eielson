package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1174j;
import com.crrepa.ble.p049d.p063q.C1272n;
import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.d.l.o */
public class C1238o {
    /* renamed from: a */
    public static C1174j m3322a(byte[] bArr) {
        if (bArr == null || bArr.length != 9) {
            return null;
        }
        byte[] bArr2 = new byte[3];
        int i = 0;
        System.arraycopy(bArr, 0, bArr2, 0, 3);
        int d = C1295d.m3514d(bArr2);
        System.arraycopy(bArr, 3, bArr2, 0, 3);
        int d2 = C1295d.m3514d(bArr2);
        System.arraycopy(bArr, 6, bArr2, 0, 3);
        int d3 = C1295d.m3514d(bArr2);
        if (d == 0 || d3 == 0 || d2 == 0) {
            d3 = 0;
            d = 0;
        } else {
            i = d2;
        }
        return new C1174j(i, d, d3);
    }

    /* renamed from: a */
    public static void m3323a(byte[] bArr, C1272n nVar) {
        if (nVar != null) {
            nVar.mo11028a(m3322a(bArr));
        }
    }
}
