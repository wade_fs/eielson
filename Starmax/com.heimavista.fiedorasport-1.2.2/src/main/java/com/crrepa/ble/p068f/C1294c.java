package com.crrepa.ble.p068f;

import java.util.UUID;

/* renamed from: com.crrepa.ble.f.c */
public interface C1294c {

    /* renamed from: a */
    public static final UUID f2001a = UUID.fromString("0000feea-0000-1000-8000-00805f9b34fb");

    /* renamed from: b */
    public static final UUID f2002b = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");

    /* renamed from: c */
    public static final UUID f2003c = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");

    /* renamed from: d */
    public static final UUID f2004d = UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");

    /* renamed from: e */
    public static final UUID f2005e = UUID.fromString("0000fee1-0000-1000-8000-00805f9b34fb");

    /* renamed from: f */
    public static final UUID f2006f = UUID.fromString("0000fee2-0000-1000-8000-00805f9b34fb");

    /* renamed from: g */
    public static final UUID f2007g = UUID.fromString("0000fee3-0000-1000-8000-00805f9b34fb");

    /* renamed from: h */
    public static final UUID f2008h = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");

    /* renamed from: i */
    public static final UUID f2009i = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");

    /* renamed from: j */
    public static final UUID f2010j = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");

    /* renamed from: k */
    public static final UUID f2011k = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");

    /* renamed from: l */
    public static final UUID f2012l = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");

    /* renamed from: m */
    public static final UUID f2013m = UUID.fromString("0000fee5-0000-1000-8000-00805f9b34fb");

    /* renamed from: n */
    public static final UUID f2014n = UUID.fromString("0000fee6-0000-1000-8000-00805f9b34fb");

    /* renamed from: o */
    public static final UUID f2015o = UUID.fromString("0000fee7-0000-1000-8000-00805f9b34fb");

    /* renamed from: p */
    public static final UUID f2016p = UUID.fromString("0000fee8-0000-1000-8000-00805f9b34fb");

    /* renamed from: q */
    public static final UUID f2017q = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    static {
        UUID.fromString("0000fee7-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00001530-1212-efde-1523-785feabcd123");
        UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
        UUID.fromString("0000ff02-0000-1000-8000-00805f9b34fb");
        UUID.fromString("6e40ff02-b5a3-f393-e0a9-e50e24dcca9e");
        UUID.fromString("6e40ff03-b5a3-f393-e0a9-e50e24dcca9e");
        UUID.fromString("0000ff03-0000-1000-8000-00805f9b34fb");
        UUID.fromString("0000ff04-0000-1000-8000-00805f9b34fb");
    }
}
