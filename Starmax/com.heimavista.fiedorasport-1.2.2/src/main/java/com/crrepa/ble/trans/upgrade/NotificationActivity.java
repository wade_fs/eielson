package com.crrepa.ble.trans.upgrade;

import android.app.Activity;
import android.os.Bundle;

public class NotificationActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        isTaskRoot();
        finish();
    }
}
