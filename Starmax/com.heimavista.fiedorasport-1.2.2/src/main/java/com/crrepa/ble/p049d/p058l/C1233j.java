package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1169f;
import java.util.Calendar;

/* renamed from: com.crrepa.ble.d.l.j */
public class C1233j {
    /* renamed from: a */
    public static C1169f m3314a(byte[] bArr) {
        if (bArr == null || bArr.length < 14) {
            return null;
        }
        C1169f fVar = new C1169f();
        byte b = bArr[0];
        if ((b & 1) == 1) {
            fVar.mo10817a(true);
        }
        if ((b & 2) == 2) {
            fVar.mo10822d(true);
        }
        if ((b & 4) == 4) {
            fVar.mo10819b(true);
        }
        if ((b & 8) == 8) {
            fVar.mo10821c(true);
        }
        fVar.mo10818b(bArr[2]);
        fVar.mo10815a(bArr[3]);
        Calendar instance = Calendar.getInstance();
        instance.set(2, bArr[4]);
        instance.set(5, bArr[5]);
        fVar.mo10816a(instance.getTime());
        fVar.mo10820c(bArr[6]);
        fVar.mo10820c(bArr[7]);
        return fVar;
    }
}
