package com.crrepa.ble.nrf.dfu.p077q.p078c;

/* renamed from: com.crrepa.ble.nrf.dfu.q.c.g */
public class C1352g extends Exception {

    /* renamed from: S */
    private static final char[] f2187S = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final long serialVersionUID = -8716125467309979289L;

    /* renamed from: P */
    private final byte[] f2188P;

    /* renamed from: Q */
    private final int f2189Q;

    /* renamed from: R */
    private final int f2190R;

    public C1352g(String str, byte[] bArr, int i, int i2) {
        super(str);
        this.f2188P = bArr == null ? new byte[0] : bArr;
        this.f2189Q = i;
        this.f2190R = i2;
    }

    /* renamed from: a */
    public static String m3748a(byte[] bArr, int i, int i2) {
        if (bArr == null || bArr.length <= i || i2 <= 0) {
            return "";
        }
        int min = Math.min(i2, bArr.length - i);
        char[] cArr = new char[(min * 2)];
        for (int i3 = 0; i3 < min; i3++) {
            byte b = bArr[i + i3] & 255;
            int i4 = i3 * 2;
            char[] cArr2 = f2187S;
            cArr[i4] = cArr2[b >>> 4];
            cArr[i4 + 1] = cArr2[b & 15];
        }
        return "0x" + new String(cArr);
    }

    public String getMessage() {
        byte[] bArr = this.f2188P;
        return String.format("%s (response: %s, expected: 0x%02X%02X..)", super.getMessage(), m3748a(bArr, 0, bArr.length), Integer.valueOf(this.f2189Q), Integer.valueOf(this.f2190R));
    }
}
