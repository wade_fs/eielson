package com.crrepa.ble.p049d.p051e;

/* renamed from: com.crrepa.ble.d.e.q */
public class C1157q {
    /* renamed from: a */
    public static boolean m2986a(byte[] bArr) {
        return bArr != null && bArr.length > 0 && bArr[0] == 1;
    }

    /* renamed from: a */
    public static byte[] m2987a() {
        return C1146f.m2959a(40, null);
    }

    /* renamed from: a */
    public static byte[] m2988a(boolean z) {
        return C1146f.m2959a(24, new byte[]{z ? (byte) 1 : 0});
    }
}
