package com.crrepa.ble.p049d.p051e;

import com.crrepa.ble.p049d.p054h.C1206z;
import com.crrepa.ble.p049d.p059m.C1242a;

/* renamed from: com.crrepa.ble.d.e.f */
public class C1146f {
    /* renamed from: a */
    public static int m2958a() {
        if (C1206z.m3111b().mo10896a()) {
            return C1242a.m3332c().mo10964b();
        }
        return 20;
    }

    /* renamed from: a */
    public static byte[] m2959a(int i, byte[] bArr) {
        int length = bArr != null ? bArr.length : 0;
        int a = m2958a();
        byte[] bArr2 = new byte[(length + 5)];
        int length2 = bArr2.length;
        bArr2[0] = -2;
        bArr2[1] = -22;
        if (a == 20) {
            bArr2[2] = 16;
        } else {
            bArr2[2] = (byte) (((length2 >> 8) & 255) + 32);
        }
        bArr2[3] = (byte) length2;
        bArr2[4] = (byte) i;
        if (length > 0) {
            System.arraycopy(bArr, 0, bArr2, 5, bArr.length);
        }
        return bArr2;
    }
}
