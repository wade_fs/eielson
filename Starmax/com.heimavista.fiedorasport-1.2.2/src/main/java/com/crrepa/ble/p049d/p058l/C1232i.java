package com.crrepa.ble.p049d.p058l;

import com.crrepa.ble.p049d.p052f.C1168e;
import com.crrepa.ble.p068f.C1295d;

/* renamed from: com.crrepa.ble.d.l.i */
public class C1232i {
    /* renamed from: a */
    public static C1168e m3313a(byte[] bArr) {
        if (bArr == null || bArr.length != 4) {
            return null;
        }
        byte[] bArr2 = new byte[2];
        System.arraycopy(bArr, 0, bArr2, 0, 2);
        long f = (long) C1295d.m3516f(bArr2);
        System.arraycopy(bArr, 2, bArr2, 0, 2);
        long f2 = (long) C1295d.m3516f(bArr2);
        return new C1168e((int) (f / 60), (int) (f % 60), (int) (f2 / 60), (int) (f2 % 60));
    }
}
