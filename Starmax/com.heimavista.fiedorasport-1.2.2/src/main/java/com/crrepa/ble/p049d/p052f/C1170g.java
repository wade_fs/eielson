package com.crrepa.ble.p049d.p052f;

/* renamed from: com.crrepa.ble.d.f.g */
public class C1170g {

    /* renamed from: a */
    private byte f1835a;

    /* renamed from: b */
    private byte f1836b;

    /* renamed from: c */
    private byte f1837c;

    /* renamed from: d */
    private byte f1838d;

    public C1170g() {
    }

    public C1170g(byte b, byte b2, byte b3, byte b4) {
        this.f1835a = b;
        this.f1836b = b2;
        this.f1837c = b3;
        this.f1838d = b4;
    }

    /* renamed from: a */
    public byte mo10823a() {
        return this.f1838d;
    }

    /* renamed from: a */
    public void mo10824a(byte b) {
        this.f1838d = b;
    }

    /* renamed from: b */
    public byte mo10825b() {
        return this.f1835a;
    }

    /* renamed from: b */
    public void mo10826b(byte b) {
        this.f1835a = b;
    }

    /* renamed from: c */
    public byte mo10827c() {
        return this.f1837c;
    }

    /* renamed from: c */
    public void mo10828c(byte b) {
        this.f1837c = b;
    }

    /* renamed from: d */
    public byte mo10829d() {
        return this.f1836b;
    }

    /* renamed from: d */
    public void mo10830d(byte b) {
        this.f1836b = b;
    }
}
