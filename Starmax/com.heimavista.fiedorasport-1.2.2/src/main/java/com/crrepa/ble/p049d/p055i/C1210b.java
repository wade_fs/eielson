package com.crrepa.ble.p049d.p055i;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCallback;
import android.content.Context;
import android.os.Build;
import com.crrepa.ble.p049d.C1137a;
import com.crrepa.ble.p049d.C1140d;
import com.crrepa.ble.p049d.p057k.C1220e;
import com.crrepa.ble.p049d.p059m.C1242a;

/* renamed from: com.crrepa.ble.d.i.b */
public class C1210b implements C1140d {

    /* renamed from: a */
    private Context f1873a;

    /* renamed from: b */
    private BluetoothDevice f1874b;

    public C1210b(Context context, BluetoothDevice bluetoothDevice) {
        this.f1873a = context;
        this.f1874b = bluetoothDevice;
    }

    /* renamed from: a */
    private void m3167a(BluetoothGattCallback bluetoothGattCallback) {
        C1242a.m3332c().mo10963a(Build.VERSION.SDK_INT >= 23 ? this.f1874b.connectGatt(this.f1873a, false, bluetoothGattCallback, 2) : this.f1874b.connectGatt(this.f1873a, false, bluetoothGattCallback));
    }

    /* renamed from: a */
    public C1137a mo10781a(C1212d dVar) {
        m3167a((BluetoothGattCallback) dVar);
        return new C1209a(dVar);
    }

    /* renamed from: a */
    public void mo10782a() {
        new C1220e().mo10949b();
    }
}
