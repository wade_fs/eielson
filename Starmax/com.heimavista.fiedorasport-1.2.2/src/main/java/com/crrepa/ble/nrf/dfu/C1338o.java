package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import com.crrepa.ble.nrf.dfu.C1320b;
import com.crrepa.ble.nrf.dfu.C1322c;
import com.crrepa.ble.nrf.dfu.p077q.C1344a;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1346a;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1349d;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1350e;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1352g;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1353h;
import com.crrepa.ble.p070h.p071a.C1305c;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.UUID;

/* renamed from: com.crrepa.ble.nrf.dfu.o */
class C1338o extends C1320b {

    /* renamed from: D */
    protected static final UUID f2140D = new UUID(279658205548544L, -9223371485494954757L);

    /* renamed from: E */
    protected static final UUID f2141E = new UUID(-8157989241631715488L, -6937650605005804976L);

    /* renamed from: F */
    protected static final UUID f2142F = new UUID(-8157989237336748192L, -6937650605005804976L);

    /* renamed from: G */
    protected static UUID f2143G = f2140D;

    /* renamed from: H */
    protected static UUID f2144H = f2141E;

    /* renamed from: I */
    protected static UUID f2145I = f2142F;

    /* renamed from: J */
    private static final byte[] f2146J = {1, 1, 0, 0, 0, 0};

    /* renamed from: K */
    private static final byte[] f2147K = {1, 2, 0, 0, 0, 0};

    /* renamed from: L */
    private static final byte[] f2148L = {2, 0, 0};

    /* renamed from: M */
    private static final byte[] f2149M = {3};

    /* renamed from: N */
    private static final byte[] f2150N = {4};

    /* renamed from: O */
    private static final byte[] f2151O = {6, 0};

    /* renamed from: A */
    private BluetoothGattCharacteristic f2152A;

    /* renamed from: B */
    private BluetoothGattCharacteristic f2153B;

    /* renamed from: C */
    private final C1342d f2154C = new C1342d();

    /* renamed from: com.crrepa.ble.nrf.dfu.o$b */
    private class C1340b {

        /* renamed from: a */
        protected int f2155a;

        /* renamed from: b */
        protected int f2156b;

        private C1340b(C1338o oVar) {
        }
    }

    /* renamed from: com.crrepa.ble.nrf.dfu.o$c */
    private class C1341c extends C1340b {

        /* renamed from: c */
        protected int f2157c;

        private C1341c(C1338o oVar) {
            super();
        }
    }

    /* renamed from: com.crrepa.ble.nrf.dfu.o$d */
    protected class C1342d extends C1320b.C1321a {
        protected C1342d() {
            super();
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            C1338o oVar;
            if (bluetoothGattCharacteristic.getValue() == null || bluetoothGattCharacteristic.getValue().length < 3) {
                C1338o oVar2 = C1338o.this;
                oVar2.mo11129a("Empty response: " + mo11144a(bluetoothGattCharacteristic));
                oVar = C1338o.this;
                oVar.f2074k = 4104;
            } else {
                if (bluetoothGattCharacteristic.getIntValue(17, 0).intValue() != 96) {
                    C1338o oVar3 = C1338o.this;
                    oVar3.mo11129a("Invalid response: " + mo11144a(bluetoothGattCharacteristic));
                    C1338o.this.f2074k = 4104;
                } else if (bluetoothGattCharacteristic.getIntValue(17, 1).intValue() == 3) {
                    C1338o.this.f2078o.mo11159b(bluetoothGattCharacteristic.getIntValue(20, 3).intValue());
                    mo11121b(bluetoothGatt, bluetoothGattCharacteristic);
                } else if (!C1338o.this.f2058z) {
                    if (bluetoothGattCharacteristic.getIntValue(17, 2).intValue() != 1) {
                        C1338o.this.f2058z = true;
                    }
                    mo11119a(bluetoothGatt, bluetoothGattCharacteristic);
                }
                oVar = C1338o.this;
            }
            oVar.mo11138f();
        }
    }

    C1338o(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    private int m3706a(byte[] bArr, int i) {
        if (bArr != null && bArr.length >= 3 && bArr[0] == 96 && bArr[1] == i && (bArr[2] == 1 || bArr[2] == 2 || bArr[2] == 3 || bArr[2] == 4 || bArr[2] == 5 || bArr[2] == 7 || bArr[2] == 8 || bArr[2] == 10 || bArr[2] == 11)) {
            return bArr[2];
        }
        throw new C1352g("Invalid response received", bArr, 96, i);
    }

    /* renamed from: a */
    private void m3707a(int i, int i2) {
        if (this.f2071h) {
            byte[] bArr = i == 1 ? f2146J : f2147K;
            m3713c(bArr, i2);
            m3709a(this.f2152A, bArr);
            byte[] g = mo11139g();
            int a = m3706a(g, 1);
            if (a == 11) {
                throw new C1350e("Creating Command object failed", g[3]);
            } else if (a != 1) {
                throw new C1349d("Creating Command object failed", a);
            }
        } else {
            throw new C1346a("Unable to create object: device disconnected");
        }
    }

    /* renamed from: a */
    private void m3708a(BluetoothGatt bluetoothGatt) {
        int i;
        boolean z;
        String str;
        String str2;
        boolean z2;
        String str3;
        DfuBaseService dfuBaseService;
        BluetoothGatt bluetoothGatt2 = bluetoothGatt;
        int i2 = super.f2056x;
        String str4 = ")";
        int i3 = 10;
        if (i2 > 0) {
            m3714d(i2);
            this.f2077n.mo11087a(10, "Packet Receipt Notif Req (Op Code = 2) sent (Value = " + i2 + str4);
        }
        mo11134b("Setting object to Data (Op Code = 6, Type = 2)");
        C1341c c = m3712c(2);
        mo11134b(String.format(Locale.US, "Data object info received (Max size = %d, Offset = %d, CRC = %08X)", Integer.valueOf(c.f2157c), Integer.valueOf(c.f2155a), Integer.valueOf(c.f2156b)));
        this.f2077n.mo11087a(10, String.format(Locale.US, "Data object info received (Max size = %d, Offset = %d, CRC = %08X)", Integer.valueOf(c.f2157c), Integer.valueOf(c.f2155a), Integer.valueOf(c.f2156b)));
        this.f2078o.mo11163d(c.f2157c);
        int i4 = this.f2079p;
        int i5 = c.f2157c;
        int i6 = ((i4 + i5) - 1) / i5;
        int i7 = c.f2155a;
        String str5 = "Error while reading firmware stream";
        if (i7 > 0) {
            try {
                i = i7 / i5;
                int i8 = i5 * i;
                int i9 = i7 - i8;
                if (i9 == 0) {
                    i8 -= i5;
                    i9 = i5;
                }
                int i10 = i8;
                if (i10 > 0) {
                    this.f2065b.read(new byte[i10]);
                    this.f2065b.mark(c.f2157c);
                }
                this.f2065b.read(new byte[i9]);
                str = "Data object executed";
                if (((int) (((C1344a) this.f2065b).mo11183g() & 4294967295L)) == c.f2156b) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(c.f2155a);
                    sb.append(" bytes of data sent before, CRC match");
                    mo11134b(sb.toString());
                    DfuBaseService dfuBaseService2 = this.f2077n;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(c.f2155a);
                    sb2.append(" bytes of data sent before, CRC match");
                    dfuBaseService2.mo11087a(10, sb2.toString());
                    this.f2078o.mo11161c(c.f2155a);
                    this.f2078o.mo11159b(c.f2155a);
                    if (i9 != c.f2157c || c.f2155a >= this.f2079p) {
                        z = true;
                    } else {
                        mo11134b("Executing data object (Op Code = 4)");
                        m3716m();
                        dfuBaseService = this.f2077n;
                        str3 = str;
                    }
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(c.f2155a);
                    sb3.append(" bytes sent before, CRC does not match");
                    mo11134b(sb3.toString());
                    DfuBaseService dfuBaseService3 = this.f2077n;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(c.f2155a);
                    sb4.append(" bytes sent before, CRC does not match");
                    dfuBaseService3.mo11087a(15, sb4.toString());
                    this.f2078o.mo11161c(i10);
                    this.f2078o.mo11159b(i10);
                    c.f2155a -= i9;
                    c.f2156b = 0;
                    this.f2065b.reset();
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Resuming from byte ");
                    sb5.append(c.f2155a);
                    sb5.append("...");
                    mo11134b(sb5.toString());
                    dfuBaseService = this.f2077n;
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Resuming from byte ");
                    sb6.append(c.f2155a);
                    sb6.append("...");
                    str3 = sb6.toString();
                }
                dfuBaseService.mo11087a(10, str3);
                z = false;
            } catch (IOException e) {
                mo11130a(str5, e);
                this.f2077n.mo11089a(bluetoothGatt2, 4100);
                return;
            }
        } else {
            str = "Data object executed";
            this.f2078o.mo11161c(0);
            z = false;
            i = 0;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (c.f2155a < this.f2079p) {
            int i11 = 1;
            while (this.f2078o.mo11155a() > 0) {
                if (!z) {
                    int a = this.f2078o.mo11155a();
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append("Creating Data object (Op Code = 1, Type = 2, Size = ");
                    sb7.append(a);
                    sb7.append(") (");
                    int i12 = i + 1;
                    sb7.append(i12);
                    sb7.append("/");
                    sb7.append(i6);
                    sb7.append(str4);
                    mo11134b(sb7.toString());
                    m3707a(2, a);
                    DfuBaseService dfuBaseService4 = this.f2077n;
                    StringBuilder sb8 = new StringBuilder();
                    str2 = str4;
                    sb8.append("Data object (");
                    sb8.append(i12);
                    sb8.append("/");
                    sb8.append(i6);
                    sb8.append(") created");
                    dfuBaseService4.mo11087a(10, sb8.toString());
                    this.f2077n.mo11087a(10, "Uploading firmware...");
                    z2 = z;
                } else {
                    str2 = str4;
                    this.f2077n.mo11087a(i3, "Resuming uploading firmware...");
                    z2 = false;
                }
                try {
                    mo11134b("Uploading firmware...");
                    mo11114a(this.f2153B);
                    mo11134b("Sending Calculate Checksum command (Op Code = 3)");
                    C1340b l = m3715l();
                    mo11134b(String.format(Locale.US, "Checksum received (Offset = %d, CRC = %08X)", Integer.valueOf(l.f2155a), Integer.valueOf(l.f2156b)));
                    this.f2077n.mo11087a(10, String.format(Locale.US, "Checksum received (Offset = %d, CRC = %08X)", Integer.valueOf(l.f2155a), Integer.valueOf(l.f2156b)));
                    int c2 = this.f2078o.mo11160c() - l.f2155a;
                    if (c2 > 0) {
                        mo11135c(c2 + " bytes were lost!");
                        this.f2077n.mo11087a(15, c2 + " bytes were lost");
                        try {
                            this.f2065b.reset();
                            this.f2065b.read(new byte[(c.f2157c - c2)]);
                            this.f2078o.mo11161c(l.f2155a);
                        } catch (IOException e2) {
                            mo11130a(str5, e2);
                            this.f2077n.mo11089a(bluetoothGatt2, 4100);
                            return;
                        }
                    }
                    String str6 = str5;
                    int g = (int) (((C1344a) this.f2065b).mo11183g() & 4294967295L);
                    if (g != l.f2156b) {
                        int i13 = i6;
                        String format = String.format(Locale.US, "CRC does not match! Expected %08X but found %08X.", Integer.valueOf(g), Integer.valueOf(l.f2156b));
                        if (i11 < 3) {
                            i11++;
                            String str7 = format + String.format(Locale.US, " Retrying...(%d/%d)", Integer.valueOf(i11), 3);
                            mo11134b(str7);
                            this.f2077n.mo11087a(15, str7);
                            try {
                                this.f2065b.reset();
                                this.f2078o.mo11161c(((C1344a) this.f2065b).mo11181d());
                                str5 = str6;
                                i6 = i13;
                                str4 = str2;
                                i3 = 10;
                            } catch (IOException e3) {
                                mo11130a("Error while resetting the firmware stream", e3);
                                this.f2077n.mo11089a(bluetoothGatt2, 4100);
                                return;
                            }
                        } else {
                            mo11129a(format);
                            this.f2077n.mo11087a(20, format);
                            this.f2077n.mo11089a(bluetoothGatt2, 4109);
                            return;
                        }
                    } else if (c2 > 0) {
                        str5 = str6;
                        str4 = str2;
                        i3 = 10;
                        z2 = true;
                    } else {
                        mo11134b("Executing data object (Op Code = 4)");
                        m3716m();
                        this.f2077n.mo11087a(10, str);
                        i++;
                        this.f2065b.mark(0);
                        str5 = str6;
                        str4 = str2;
                        i3 = 10;
                        i11 = 1;
                    }
                } catch (C1346a e4) {
                    mo11129a("Disconnected while sending data");
                    throw e4;
                }
            }
        } else {
            mo11134b("Executing data object (Op Code = 4)");
            m3716m();
            this.f2077n.mo11087a(10, str);
        }
        long elapsedRealtime2 = SystemClock.elapsedRealtime();
        StringBuilder sb9 = new StringBuilder();
        sb9.append("Transfer of ");
        sb9.append(this.f2078o.mo11160c() - c.f2155a);
        sb9.append(" bytes has taken ");
        long j = elapsedRealtime2 - elapsedRealtime;
        sb9.append(j);
        sb9.append(" ms");
        mo11134b(sb9.toString());
        this.f2077n.mo11087a(10, "Upload completed in " + j + " ms");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void
     arg types: [android.bluetooth.BluetoothGattCharacteristic, byte[], int]
     candidates:
      com.crrepa.ble.nrf.dfu.b.a(android.bluetooth.BluetoothGattCharacteristic, byte[], int):void
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void */
    /* renamed from: a */
    private void m3709a(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        mo11127a(bluetoothGattCharacteristic, bArr, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0111  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3710b(android.bluetooth.BluetoothGatt r20) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            java.util.zip.CRC32 r3 = new java.util.zip.CRC32
            r3.<init>()
            java.lang.String r0 = "Setting object to Command (Op Code = 6, Type = 1)"
            r1.mo11134b(r0)
            r4 = 1
            com.crrepa.ble.nrf.dfu.o$c r5 = r1.m3712c(r4)
            java.util.Locale r0 = java.util.Locale.US
            r6 = 3
            java.lang.Object[] r7 = new java.lang.Object[r6]
            int r8 = r5.f2157c
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r9 = 0
            r7[r9] = r8
            int r8 = r5.f2155a
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r4] = r8
            int r8 = r5.f2156b
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r10 = 2
            r7[r10] = r8
            java.lang.String r8 = "Command object info received (Max size = %d, Offset = %d, CRC = %08X)"
            java.lang.String r0 = java.lang.String.format(r0, r8, r7)
            r1.mo11134b(r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            java.util.Locale r7 = java.util.Locale.US
            java.lang.Object[] r11 = new java.lang.Object[r6]
            int r12 = r5.f2157c
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r9] = r12
            int r12 = r5.f2155a
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r4] = r12
            int r12 = r5.f2156b
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r10] = r12
            java.lang.String r7 = java.lang.String.format(r7, r8, r11)
            r8 = 10
            r0.mo11087a(r8, r7)
            int r0 = r1.f2080q
            int r7 = r5.f2157c
            int r7 = r5.f2155a
            r11 = 4100(0x1004, float:5.745E-42)
            java.lang.String r12 = "Error while resetting the init packet stream"
            r13 = 4294967295(0xffffffff, double:2.1219957905E-314)
            if (r7 <= 0) goto L_0x010d
            if (r7 > r0) goto L_0x010d
            byte[] r0 = new byte[r7]     // Catch:{ IOException -> 0x00da }
            java.io.InputStream r7 = r1.f2066c     // Catch:{ IOException -> 0x00da }
            r7.read(r0)     // Catch:{ IOException -> 0x00da }
            r3.update(r0)     // Catch:{ IOException -> 0x00da }
            long r15 = r3.getValue()     // Catch:{ IOException -> 0x00da }
            long r6 = r15 & r13
            int r0 = (int) r6     // Catch:{ IOException -> 0x00da }
            int r6 = r5.f2156b     // Catch:{ IOException -> 0x00da }
            if (r6 != r0) goto L_0x00cf
            java.lang.String r0 = "Init packet CRC is the same"
            r1.mo11134b(r0)     // Catch:{ IOException -> 0x00da }
            int r0 = r5.f2155a     // Catch:{ IOException -> 0x00da }
            int r6 = r1.f2080q     // Catch:{ IOException -> 0x00da }
            if (r0 != r6) goto L_0x00a4
            java.lang.String r0 = "-> Whole Init packet was sent before"
            r1.mo11134b(r0)     // Catch:{ IOException -> 0x00da }
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n     // Catch:{ IOException -> 0x00a1 }
            java.lang.String r6 = "Received CRC match Init packet"
            r7 = 1
            r15 = 0
            goto L_0x00c5
        L_0x00a1:
            r0 = move-exception
            r7 = 1
            goto L_0x00dc
        L_0x00a4:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00da }
            r0.<init>()     // Catch:{ IOException -> 0x00da }
            java.lang.String r6 = "-> "
            r0.append(r6)     // Catch:{ IOException -> 0x00da }
            int r6 = r5.f2155a     // Catch:{ IOException -> 0x00da }
            r0.append(r6)     // Catch:{ IOException -> 0x00da }
            java.lang.String r6 = " bytes of Init packet were sent before"
            r0.append(r6)     // Catch:{ IOException -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00da }
            r1.mo11134b(r0)     // Catch:{ IOException -> 0x00da }
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n     // Catch:{ IOException -> 0x00cb }
            java.lang.String r6 = "Resuming sending Init packet..."
            r7 = 0
            r15 = 1
        L_0x00c5:
            r0.mo11087a(r8, r6)     // Catch:{ IOException -> 0x00c9 }
            goto L_0x010f
        L_0x00c9:
            r0 = move-exception
            goto L_0x00dd
        L_0x00cb:
            r0 = move-exception
            r7 = 0
            r15 = 1
            goto L_0x00dd
        L_0x00cf:
            java.io.InputStream r0 = r1.f2066c     // Catch:{ IOException -> 0x00da }
            r0.reset()     // Catch:{ IOException -> 0x00da }
            r3.reset()     // Catch:{ IOException -> 0x00da }
            r5.f2155a = r9     // Catch:{ IOException -> 0x00da }
            goto L_0x010d
        L_0x00da:
            r0 = move-exception
            r7 = 0
        L_0x00dc:
            r15 = 0
        L_0x00dd:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r10 = "Error while reading "
            r6.append(r10)
            int r10 = r5.f2155a
            r6.append(r10)
            java.lang.String r10 = " bytes from the init packet stream"
            r6.append(r10)
            java.lang.String r6 = r6.toString()
            r1.mo11130a(r6, r0)
            java.io.InputStream r0 = r1.f2066c     // Catch:{ IOException -> 0x0103 }
            r0.reset()     // Catch:{ IOException -> 0x0103 }
            r3.reset()     // Catch:{ IOException -> 0x0103 }
            r5.f2155a = r9     // Catch:{ IOException -> 0x0103 }
            goto L_0x010f
        L_0x0103:
            r0 = move-exception
            r1.mo11130a(r12, r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            r0.mo11089a(r2, r11)
            return
        L_0x010d:
            r7 = 0
            r15 = 0
        L_0x010f:
            if (r7 != 0) goto L_0x024e
            r1.m3714d(r9)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            java.lang.String r6 = "Packet Receipt Notif disabled (Op Code = 2, Value = 0)"
            r0.mo11087a(r8, r6)
            r0 = 1
            r6 = 3
        L_0x011d:
            if (r0 > r6) goto L_0x024e
            java.lang.String r6 = ")"
            if (r15 != 0) goto L_0x0148
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r10 = "Creating Init packet object (Op Code = 1, Type = 1, Size = "
            r7.append(r10)
            int r10 = r1.f2080q
            r7.append(r10)
            r7.append(r6)
            java.lang.String r7 = r7.toString()
            r1.mo11134b(r7)
            int r7 = r1.f2080q
            r1.m3707a(r4, r7)
            com.crrepa.ble.nrf.dfu.DfuBaseService r7 = r1.f2077n
            java.lang.String r10 = "Command object created"
            r7.mo11087a(r8, r10)
        L_0x0148:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r10 = "Sending "
            r7.append(r10)
            int r10 = r1.f2080q
            int r15 = r5.f2155a
            int r10 = r10 - r15
            r7.append(r10)
            java.lang.String r10 = " bytes of init packet..."
            r7.append(r10)
            java.lang.String r7 = r7.toString()
            r1.mo11134b(r7)
            android.bluetooth.BluetoothGattCharacteristic r7 = r1.f2153B
            r1.mo11115a(r7, r3)
            long r17 = r3.getValue()
            r10 = r12
            long r11 = r17 & r13
            int r12 = (int) r11
            com.crrepa.ble.nrf.dfu.DfuBaseService r11 = r1.f2077n
            java.util.Locale r15 = java.util.Locale.US
            java.lang.Object[] r7 = new java.lang.Object[r4]
            java.lang.Integer r18 = java.lang.Integer.valueOf(r12)
            r7[r9] = r18
            java.lang.String r13 = "Command object sent (CRC = %08X)"
            java.lang.String r7 = java.lang.String.format(r15, r13, r7)
            r11.mo11087a(r8, r7)
            java.lang.String r7 = "Sending Calculate Checksum command (Op Code = 3)"
            r1.mo11134b(r7)
            com.crrepa.ble.nrf.dfu.o$b r7 = r19.m3715l()
            com.crrepa.ble.nrf.dfu.DfuBaseService r11 = r1.f2077n
            java.util.Locale r13 = java.util.Locale.US
            r14 = 2
            java.lang.Object[] r15 = new java.lang.Object[r14]
            int r14 = r7.f2155a
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r15[r9] = r14
            int r14 = r7.f2156b
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r15[r4] = r14
            java.lang.String r14 = "Checksum received (Offset = %d, CRC = %08X)"
            java.lang.String r13 = java.lang.String.format(r13, r14, r15)
            r11.mo11087a(r8, r13)
            java.util.Locale r11 = java.util.Locale.US
            r13 = 2
            java.lang.Object[] r15 = new java.lang.Object[r13]
            int r13 = r7.f2155a
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r15[r9] = r13
            int r13 = r7.f2156b
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r15[r4] = r13
            java.lang.String r11 = java.lang.String.format(r11, r14, r15)
            r1.mo11134b(r11)
            int r7 = r7.f2156b
            if (r12 != r7) goto L_0x01d3
            goto L_0x024e
        L_0x01d3:
            r7 = 3
            if (r0 >= r7) goto L_0x023a
            int r0 = r0 + 1
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "CRC does not match! Retrying...("
            r11.append(r12)
            r11.append(r0)
            java.lang.String r13 = "/"
            r11.append(r13)
            r11.append(r7)
            r11.append(r6)
            java.lang.String r11 = r11.toString()
            r1.mo11134b(r11)
            com.crrepa.ble.nrf.dfu.DfuBaseService r11 = r1.f2077n
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r12)
            r14.append(r0)
            r14.append(r13)
            r14.append(r7)
            r14.append(r6)
            java.lang.String r6 = r14.toString()
            r12 = 15
            r11.mo11087a(r12, r6)
            r5.f2155a = r9     // Catch:{ IOException -> 0x022e }
            r5.f2156b = r9     // Catch:{ IOException -> 0x022e }
            java.io.InputStream r6 = r1.f2066c     // Catch:{ IOException -> 0x022e }
            r6.reset()     // Catch:{ IOException -> 0x022e }
            r3.reset()     // Catch:{ IOException -> 0x022e }
            r12 = r10
            r6 = 3
            r11 = 4100(0x1004, float:5.745E-42)
            r13 = 4294967295(0xffffffff, double:2.1219957905E-314)
            r15 = 0
            goto L_0x011d
        L_0x022e:
            r0 = move-exception
            r1.mo11130a(r10, r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            r3 = 4100(0x1004, float:5.745E-42)
            r0.mo11089a(r2, r3)
            return
        L_0x023a:
            java.lang.String r0 = "CRC does not match!"
            r1.mo11129a(r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r1.f2077n
            r4 = 20
            r3.mo11087a(r4, r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            r3 = 4109(0x100d, float:5.758E-42)
            r0.mo11089a(r2, r3)
            return
        L_0x024e:
            java.lang.String r0 = "Executing init packet (Op Code = 4)"
            r1.mo11134b(r0)
            r19.m3716m()
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r1.f2077n
            java.lang.String r2 = "Command object executed"
            r0.mo11087a(r8, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1338o.m3710b(android.bluetooth.BluetoothGatt):void");
    }

    /* renamed from: b */
    private void m3711b(byte[] bArr, int i) {
        bArr[1] = (byte) (i & 255);
        bArr[2] = (byte) ((i >> 8) & 255);
    }

    /* renamed from: c */
    private C1341c m3712c(int i) {
        if (this.f2071h) {
            byte[] bArr = f2151O;
            bArr[1] = (byte) i;
            m3709a(this.f2152A, bArr);
            byte[] g = mo11139g();
            int a = m3706a(g, 6);
            if (a == 11) {
                throw new C1350e("Selecting object failed", g[3]);
            } else if (a == 1) {
                C1341c cVar = new C1341c();
                cVar.f2157c = this.f2152A.getIntValue(20, 3).intValue();
                cVar.f2155a = this.f2152A.getIntValue(20, 7).intValue();
                cVar.f2156b = this.f2152A.getIntValue(20, 11).intValue();
                return cVar;
            } else {
                throw new C1349d("Selecting object failed", a);
            }
        } else {
            throw new C1346a("Unable to read object info: device disconnected");
        }
    }

    /* renamed from: c */
    private void m3713c(byte[] bArr, int i) {
        bArr[2] = (byte) (i & 255);
        bArr[3] = (byte) ((i >> 8) & 255);
        bArr[4] = (byte) ((i >> 16) & 255);
        bArr[5] = (byte) ((i >> 24) & 255);
    }

    /* renamed from: d */
    private void m3714d(int i) {
        if (this.f2071h) {
            mo11134b("Sending the number of packets before notifications (Op Code = 2, Value = " + i + ")");
            m3711b(f2148L, i);
            m3709a(this.f2152A, f2148L);
            byte[] g = mo11139g();
            int a = m3706a(g, 2);
            if (a == 11) {
                throw new C1350e("Sending the number of packets failed", g[3]);
            } else if (a != 1) {
                throw new C1349d("Sending the number of packets failed", a);
            }
        } else {
            throw new C1346a("Unable to read Checksum: device disconnected");
        }
    }

    /* renamed from: l */
    private C1340b m3715l() {
        if (this.f2071h) {
            m3709a(this.f2152A, f2149M);
            byte[] g = mo11139g();
            int a = m3706a(g, 3);
            if (a == 11) {
                throw new C1350e("Receiving Checksum failed", g[3]);
            } else if (a == 1) {
                C1340b bVar = new C1340b();
                bVar.f2155a = this.f2152A.getIntValue(20, 3).intValue();
                bVar.f2156b = this.f2152A.getIntValue(20, 7).intValue();
                return bVar;
            } else {
                throw new C1349d("Receiving Checksum failed", a);
            }
        } else {
            throw new C1346a("Unable to read Checksum: device disconnected");
        }
    }

    /* renamed from: m */
    private void m3716m() {
        if (this.f2071h) {
            m3709a(this.f2152A, f2150N);
            byte[] g = mo11139g();
            int a = m3706a(g, 4);
            if (a == 11) {
                throw new C1350e("Executing object failed", g[3]);
            } else if (a != 1) {
                throw new C1349d("Executing object failed", a);
            }
        } else {
            throw new C1346a("Unable to read Checksum: device disconnected");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.b.b(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.crrepa.ble.nrf.dfu.o.b(byte[], int):void
      com.crrepa.ble.nrf.dfu.b.b(com.crrepa.ble.nrf.dfu.b, boolean):boolean
      com.crrepa.ble.nrf.dfu.b.b(android.content.Intent, boolean):void */
    /* renamed from: a */
    public void mo11151a(Intent intent) {
        DfuBaseService dfuBaseService;
        int i;
        mo11135c("Secure DFU bootloader found");
        this.f2078o.mo11165e(-2);
        this.f2077n.mo11086a(1000);
        BluetoothGatt bluetoothGatt = this.f2067d;
        if (Build.VERSION.SDK_INT >= 21) {
            mo11134b("Requesting MTU = 517");
            mo11133b(517);
        }
        try {
            mo11126a(this.f2152A, 1);
            this.f2077n.mo11087a(10, "Notifications enabled");
            this.f2077n.mo11086a(1000);
            m3710b(bluetoothGatt);
            m3708a(bluetoothGatt);
            this.f2078o.mo11165e(-5);
            this.f2077n.mo11094d();
            this.f2077n.mo11087a(5, "Disconnected by the remote device");
            mo11116b(intent, false);
        } catch (C1353h e) {
            throw e;
        } catch (C1352g e2) {
            mo11129a(e2.getMessage());
            this.f2077n.mo11087a(20, e2.getMessage());
            dfuBaseService = this.f2077n;
            i = 4104;
            dfuBaseService.mo11089a(bluetoothGatt, i);
        } catch (C1349d e3) {
            i = e3.mo11199a() | 8192;
            mo11129a(e3.getMessage());
            this.f2077n.mo11087a(20, String.format("Remote DFU error: %s", C1305c.m3540a(i)));
            if (e3 instanceof C1350e) {
                C1350e eVar = (C1350e) e3;
                mo11134b("Extended Error details: " + C1305c.m3541b(eVar.mo11201e()));
                DfuBaseService dfuBaseService2 = this.f2077n;
                dfuBaseService2.mo11087a(20, "Details: " + C1305c.m3541b(eVar.mo11201e()) + " (Code = " + eVar.mo11201e() + ")");
            }
            dfuBaseService = this.f2077n;
            dfuBaseService.mo11089a(bluetoothGatt, i);
        }
    }

    /* renamed from: a */
    public boolean mo11154a(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(f2143G);
        if (service == null) {
            return false;
        }
        this.f2152A = service.getCharacteristic(f2144H);
        this.f2153B = service.getCharacteristic(f2145I);
        return (this.f2152A == null || this.f2153B == null) ? false : true;
    }

    /* renamed from: a */
    public boolean mo11131a(Intent intent, BluetoothGatt bluetoothGatt, int i, InputStream inputStream, InputStream inputStream2) {
        if (inputStream2 != null) {
            return super.mo11131a(intent, bluetoothGatt, i, inputStream, inputStream2);
        }
        this.f2077n.mo11087a(20, "The Init packet is required by this version DFU Bootloader");
        this.f2077n.mo11089a(bluetoothGatt, 4107);
        return false;
    }

    /* renamed from: c */
    public C1322c.C1323a m3721c() {
        return this.f2154C;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public UUID mo11117j() {
        return f2143G;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public UUID mo11118k() {
        return f2145I;
    }
}
