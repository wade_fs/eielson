package com.crrepa.ble.p049d.p052f;

import java.io.File;
import java.util.Date;

/* renamed from: com.crrepa.ble.d.f.a */
public class C1163a {

    /* renamed from: a */
    private int f1810a;

    /* renamed from: b */
    private int f1811b;

    /* renamed from: c */
    private int f1812c;

    /* renamed from: d */
    private int f1813d;

    /* renamed from: e */
    private boolean f1814e;

    /* renamed from: f */
    private Date f1815f;

    public C1163a() {
    }

    public C1163a(int i, int i2, int i3, int i4, boolean z) {
        this.f1810a = i;
        this.f1811b = i2;
        this.f1812c = i3;
        this.f1813d = i4;
        this.f1814e = z;
    }

    /* renamed from: a */
    public Date mo10783a() {
        return this.f1815f;
    }

    /* renamed from: a */
    public void mo10784a(Date date) {
        this.f1815f = date;
    }

    /* renamed from: b */
    public int mo10785b() {
        return this.f1811b;
    }

    /* renamed from: c */
    public int mo10786c() {
        return this.f1810a;
    }

    /* renamed from: d */
    public int mo10787d() {
        return this.f1812c;
    }

    /* renamed from: e */
    public int mo10788e() {
        return this.f1813d;
    }

    /* renamed from: f */
    public boolean mo10789f() {
        return this.f1814e;
    }

    public String toString() {
        return "ID: " + this.f1810a + File.separator + "time: " + this.f1811b + ":" + this.f1812c + File.separator + "repeatMode: " + this.f1813d + File.separator + "enable: " + this.f1814e;
    }
}
