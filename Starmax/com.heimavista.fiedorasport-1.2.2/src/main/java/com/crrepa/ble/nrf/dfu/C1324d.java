package com.crrepa.ble.nrf.dfu;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import com.crrepa.ble.nrf.dfu.p077q.p078c.C1352g;

/* renamed from: com.crrepa.ble.nrf.dfu.d */
abstract class C1324d extends C1318a {

    /* renamed from: w */
    private static final byte[] f2082w = {1};

    C1324d(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* renamed from: a */
    private int m3634a(byte[] bArr, int i) {
        if (bArr != null && bArr.length >= 3 && bArr[0] == 32 && bArr[1] == i && (bArr[2] == 1 || bArr[2] == 2 || bArr[2] == 4)) {
            return bArr[2];
        }
        throw new C1352g("Invalid response received", bArr, 32, i);
    }

    /* renamed from: c */
    private static String m3635c(int i) {
        int i2 = i & -8193;
        if (i2 == 2) {
            return "REMOTE DFU OP CODE NOT SUPPORTED";
        }
        if (i2 == 4) {
            return "REMOTE DFU OPERATION FAILED";
        }
        return "UNKNOWN (" + i + ")";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void
     arg types: [android.bluetooth.BluetoothGattCharacteristic, byte[], int]
     candidates:
      com.crrepa.ble.nrf.dfu.a.a(android.content.Intent, boolean, boolean):void
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crrepa.ble.nrf.dfu.a.a(android.content.Intent, boolean, boolean):void
     arg types: [android.content.Intent, int, boolean]
     candidates:
      com.crrepa.ble.nrf.dfu.c.a(android.bluetooth.BluetoothGattCharacteristic, byte[], boolean):void
      com.crrepa.ble.nrf.dfu.a.a(android.content.Intent, boolean, boolean):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:15|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r5 = r12.f2075l;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0079 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo11151a(android.content.Intent r13) {
        /*
            r12 = this;
            java.lang.String r0 = ")"
            java.lang.String r1 = ", Status = "
            java.lang.String r2 = "Response received (Op Code = "
            com.crrepa.ble.nrf.dfu.i r3 = r12.f2078o
            r4 = -2
            r3.mo11165e(r4)
            com.crrepa.ble.nrf.dfu.DfuBaseService r3 = r12.f2077n
            r4 = 1000(0x3e8, float:1.401E-42)
            r3.mo11086a(r4)
            android.bluetooth.BluetoothGatt r3 = r12.f2067d
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r12.f2077n
            r6 = 15
            java.lang.String r7 = "Application with buttonless update found"
            r5.mo11087a(r6, r7)
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r12.f2077n
            r6 = 1
            java.lang.String r7 = "Jumping to the DFU Bootloader..."
            r5.mo11087a(r6, r7)
            android.bluetooth.BluetoothGattCharacteristic r5 = r12.mo11117j()
            int r7 = r12.mo11152k()
            int r8 = r12.mo11152k()
            r12.mo11126a(r5, r8)
            com.crrepa.ble.nrf.dfu.DfuBaseService r8 = r12.f2077n
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = 2
            if (r7 != r10) goto L_0x0042
            java.lang.String r7 = "Indications"
            goto L_0x0044
        L_0x0042:
            java.lang.String r7 = "Notifications"
        L_0x0044:
            r9.append(r7)
            java.lang.String r7 = " enabled"
            r9.append(r7)
            java.lang.String r7 = r9.toString()
            r9 = 10
            r8.mo11087a(r9, r7)
            com.crrepa.ble.nrf.dfu.DfuBaseService r7 = r12.f2077n
            r7.mo11086a(r4)
            r4 = 0
            r7 = 20
            com.crrepa.ble.nrf.dfu.i r8 = r12.f2078o     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10 = -3
            r8.mo11165e(r10)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.String r8 = "Sending Enter Bootloader (Op Code = 1)"
            r12.mo11134b(r8)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            byte[] r8 = com.crrepa.ble.nrf.dfu.C1324d.f2082w     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r12.mo11127a(r5, r8, r6)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r5 = r12.f2077n     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.String r8 = "Enter bootloader sent (Op Code = 1)"
            r5.mo11087a(r9, r8)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            byte[] r5 = r12.mo11139g()     // Catch:{ a -> 0x0079 }
            goto L_0x007b
        L_0x0079:
            byte[] r5 = r12.f2075l     // Catch:{ g -> 0x0106, d -> 0x00e2 }
        L_0x007b:
            if (r5 == 0) goto L_0x00cd
            int r8 = r12.m3634a(r5, r6)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.<init>()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.append(r2)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            byte r11 = r5[r6]     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.append(r11)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.append(r1)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.append(r8)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.append(r0)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.String r10 = r10.toString()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r12.mo11134b(r10)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            com.crrepa.ble.nrf.dfu.DfuBaseService r10 = r12.f2077n     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.<init>()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.append(r2)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            byte r2 = r5[r6]     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.append(r2)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.append(r1)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.append(r8)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r11.append(r0)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.String r0 = r11.toString()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r10.mo11087a(r9, r0)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            if (r8 != r6) goto L_0x00c5
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r12.f2077n     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r0.mo11094d()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            goto L_0x00d2
        L_0x00c5:
            com.crrepa.ble.nrf.dfu.q.c.d r13 = new com.crrepa.ble.nrf.dfu.q.c.d     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            java.lang.String r0 = "Device returned error after sending Enter Bootloader"
            r13.<init>(r0, r8)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            throw r13     // Catch:{ g -> 0x0106, d -> 0x00e2 }
        L_0x00cd:
            java.lang.String r0 = "Device disconnected before receiving notification"
            r12.mo11134b(r0)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
        L_0x00d2:
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r12.f2077n     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r1 = 5
            java.lang.String r2 = "Disconnected by the remote device"
            r0.mo11087a(r1, r2)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            boolean r0 = r12.mo11153l()     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            r12.mo11110a(r13, r4, r0)     // Catch:{ g -> 0x0106, d -> 0x00e2 }
            goto L_0x011e
        L_0x00e2:
            r13 = move-exception
            int r0 = r13.mo11199a()
            r0 = r0 | 8192(0x2000, float:1.14794E-41)
            java.lang.String r13 = r13.getMessage()
            r12.mo11129a(r13)
            com.crrepa.ble.nrf.dfu.DfuBaseService r13 = r12.f2077n
            java.lang.Object[] r1 = new java.lang.Object[r6]
            java.lang.String r2 = m3635c(r0)
            r1[r4] = r2
            java.lang.String r2 = "Remote DFU error: %s"
            java.lang.String r1 = java.lang.String.format(r2, r1)
            r13.mo11087a(r7, r1)
            com.crrepa.ble.nrf.dfu.DfuBaseService r13 = r12.f2077n
            goto L_0x011b
        L_0x0106:
            r13 = move-exception
            java.lang.String r0 = r13.getMessage()
            r12.mo11129a(r0)
            com.crrepa.ble.nrf.dfu.DfuBaseService r0 = r12.f2077n
            java.lang.String r13 = r13.getMessage()
            r0.mo11087a(r7, r13)
            com.crrepa.ble.nrf.dfu.DfuBaseService r13 = r12.f2077n
            r0 = 4104(0x1008, float:5.751E-42)
        L_0x011b:
            r13.mo11089a(r3, r0)
        L_0x011e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crrepa.ble.nrf.dfu.C1324d.mo11151a(android.content.Intent):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public abstract BluetoothGattCharacteristic mo11117j();

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public abstract int mo11152k();

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public abstract boolean mo11153l();
}
