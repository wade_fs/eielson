package com.crrepa.ble.p049d.p052f;

import androidx.annotation.ColorInt;

/* renamed from: com.crrepa.ble.d.f.n */
public class C1178n {

    /* renamed from: a */
    private int f1858a;

    /* renamed from: b */
    private int f1859b;

    /* renamed from: c */
    private int f1860c;

    /* renamed from: d */
    private int f1861d;

    /* renamed from: e */
    private String f1862e;

    public C1178n() {
    }

    public C1178n(int i, int i2, int i3, int i4, String str) {
        this.f1858a = i;
        this.f1859b = i2;
        this.f1860c = i3;
        this.f1861d = i4;
        this.f1862e = str;
    }

    /* renamed from: a */
    public String mo10861a() {
        return this.f1862e;
    }

    /* renamed from: a */
    public void mo10862a(@ColorInt int i) {
        this.f1861d = i;
    }

    /* renamed from: a */
    public void mo10863a(String str) {
        this.f1862e = str;
    }

    /* renamed from: b */
    public int mo10864b() {
        return this.f1861d;
    }

    /* renamed from: b */
    public void mo10865b(int i) {
        this.f1860c = i;
    }

    /* renamed from: c */
    public int mo10866c() {
        return this.f1860c;
    }

    /* renamed from: c */
    public void mo10867c(int i) {
        this.f1858a = i;
    }

    /* renamed from: d */
    public int mo10868d() {
        return this.f1858a;
    }

    /* renamed from: d */
    public void mo10869d(int i) {
        this.f1859b = i;
    }

    /* renamed from: e */
    public int mo10870e() {
        return this.f1859b;
    }
}
