package com.blankj.utilcode.util;

import android.graphics.Bitmap;

/* renamed from: com.blankj.utilcode.util.m */
public final class ImageUtils {
    /* renamed from: a */
    public static Bitmap m1101a(Bitmap bitmap, int i, int i2) {
        return m1102a(bitmap, i, i2, false);
    }

    /* renamed from: a */
    public static Bitmap m1102a(Bitmap bitmap, int i, int i2, boolean z) {
        if (m1103a(bitmap)) {
            return null;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, i, i2, true);
        if (z && !bitmap.isRecycled() && createScaledBitmap != bitmap) {
            bitmap.recycle();
        }
        return createScaledBitmap;
    }

    /* renamed from: a */
    private static boolean m1103a(Bitmap bitmap) {
        return bitmap == null || bitmap.getWidth() == 0 || bitmap.getHeight() == 0;
    }
}
