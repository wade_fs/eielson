package com.blankj.utilcode.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;

/* renamed from: com.blankj.utilcode.util.c */
public final class AppUtils {
    /* renamed from: a */
    public static String m943a() {
        return Utils.m891d().getPackageName();
    }

    /* renamed from: b */
    public static String m945b(String str) {
        if (m948d(str)) {
            return "";
        }
        try {
            PackageInfo packageInfo = Utils.m891d().getPackageManager().getPackageInfo(str, 0);
            if (packageInfo == null) {
                return null;
            }
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: c */
    public static boolean m947c(@NonNull String str) {
        if (str != null) {
            try {
                if (Utils.m891d().getPackageManager().getApplicationInfo(str, 0) != null) {
                    return true;
                }
                return false;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            throw new NullPointerException("Argument 'pkgName' of type String (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    /* renamed from: d */
    private static boolean m948d(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static int m942a(String str) {
        if (m948d(str)) {
            return -1;
        }
        try {
            PackageInfo packageInfo = Utils.m891d().getPackageManager().getPackageInfo(str, 0);
            if (packageInfo == null) {
                return -1;
            }
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* renamed from: c */
    public static String m946c() {
        return m945b(Utils.m891d().getPackageName());
    }

    /* renamed from: b */
    public static int m944b() {
        return m942a(Utils.m891d().getPackageName());
    }
}
