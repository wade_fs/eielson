package com.blankj.utilcode.util;

import android.view.inputmethod.InputMethodManager;

/* renamed from: com.blankj.utilcode.util.o */
public final class KeyboardUtils {
    /* renamed from: a */
    public static void m1109a() {
        InputMethodManager inputMethodManager = (InputMethodManager) Utils.m891d().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(0, 0);
        }
    }
}
