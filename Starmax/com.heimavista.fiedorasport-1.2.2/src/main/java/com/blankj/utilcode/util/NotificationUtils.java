package com.blankj.utilcode.util;

import androidx.core.app.NotificationManagerCompat;

/* renamed from: com.blankj.utilcode.util.r */
public class NotificationUtils {
    /* renamed from: a */
    public static boolean m1185a() {
        return NotificationManagerCompat.from(Utils.m891d()).areNotificationsEnabled();
    }
}
