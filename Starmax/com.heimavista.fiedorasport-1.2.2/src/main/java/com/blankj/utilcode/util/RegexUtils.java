package com.blankj.utilcode.util;

import androidx.collection.SimpleArrayMap;
import java.util.regex.Pattern;

/* renamed from: com.blankj.utilcode.util.t */
public final class RegexUtils {
    static {
        new SimpleArrayMap();
    }

    /* renamed from: a */
    public static boolean m1235a(CharSequence charSequence) {
        return m1236a("^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(16[6])|(17[0,1,3,5-8])|(18[0-9])|(19[1,8,9]))\\d{8}$", charSequence);
    }

    /* renamed from: a */
    public static boolean m1236a(String str, CharSequence charSequence) {
        return charSequence != null && charSequence.length() > 0 && Pattern.matches(str, charSequence);
    }
}
