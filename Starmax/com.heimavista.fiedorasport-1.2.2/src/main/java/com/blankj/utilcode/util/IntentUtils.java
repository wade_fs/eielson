package com.blankj.utilcode.util;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.core.content.FileProvider;
import com.google.android.exoplayer2.C1750C;
import java.io.File;

/* renamed from: com.blankj.utilcode.util.n */
public final class IntentUtils {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.n.a(java.lang.String, java.io.File, boolean):android.content.Intent
     arg types: [java.lang.String, java.io.File, int]
     candidates:
      com.blankj.utilcode.util.n.a(java.lang.String, android.net.Uri, boolean):android.content.Intent
      com.blankj.utilcode.util.n.a(java.lang.String, java.io.File, boolean):android.content.Intent */
    /* renamed from: a */
    public static Intent m1106a(String str, File file) {
        return m1107a(str, file, false);
    }

    /* renamed from: a */
    public static Intent m1107a(String str, File file, boolean z) {
        if (file == null || !file.isFile()) {
            return null;
        }
        return m1105a(str, m1108a(file), z);
    }

    /* renamed from: a */
    public static Intent m1105a(String str, Uri uri, boolean z) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", str);
        intent.putExtra("android.intent.extra.STREAM", uri);
        intent.setType("image/*");
        return m1104a(intent, z);
    }

    /* renamed from: a */
    private static Intent m1104a(Intent intent, boolean z) {
        return z ? intent.addFlags(C1750C.ENCODING_PCM_MU_LAW) : intent;
    }

    /* renamed from: a */
    private static Uri m1108a(File file) {
        if (file == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.fromFile(file);
        }
        return FileProvider.getUriForFile(Utils.m891d(), Utils.m891d().getPackageName() + ".utilcode.provider", file);
    }
}
