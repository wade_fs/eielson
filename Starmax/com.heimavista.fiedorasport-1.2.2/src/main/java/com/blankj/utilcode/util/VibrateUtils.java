package com.blankj.utilcode.util;

import android.os.Vibrator;
import androidx.annotation.RequiresPermission;

/* renamed from: com.blankj.utilcode.util.h0 */
public final class VibrateUtils {

    /* renamed from: a */
    private static Vibrator f769a;

    @RequiresPermission("android.permission.VIBRATE")
    /* renamed from: a */
    public static void m1058a(long[] jArr, int i) {
        Vibrator b = m1059b();
        if (b != null) {
            b.vibrate(jArr, i);
        }
    }

    /* renamed from: b */
    private static Vibrator m1059b() {
        if (f769a == null) {
            f769a = (Vibrator) Utils.m891d().getSystemService("vibrator");
        }
        return f769a;
    }

    @RequiresPermission("android.permission.VIBRATE")
    /* renamed from: a */
    public static void m1057a() {
        Vibrator b = m1059b();
        if (b != null) {
            b.cancel();
        }
    }
}
