package com.blankj.utilcode.util;

import android.content.ContentResolver;
import android.provider.Settings;
import androidx.annotation.IntRange;

/* renamed from: com.blankj.utilcode.util.e */
public final class BrightnessUtils {
    /* renamed from: a */
    public static int m994a() {
        try {
            return Settings.System.getInt(Utils.m891d().getContentResolver(), "screen_brightness");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* renamed from: a */
    public static boolean m995a(@IntRange(from = 0, mo464to = 255) int i) {
        ContentResolver contentResolver = Utils.m891d().getContentResolver();
        boolean putInt = Settings.System.putInt(contentResolver, "screen_brightness", i);
        contentResolver.notifyChange(Settings.System.getUriFor("screen_brightness"), null);
        return putInt;
    }
}
