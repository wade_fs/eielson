package com.blankj.utilcode.util;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"ApplySharedPref"})
/* renamed from: com.blankj.utilcode.util.w */
public final class SPUtils {

    /* renamed from: b */
    private static final Map<String, SPUtils> f824b = new HashMap();

    /* renamed from: a */
    private SharedPreferences f825a;

    private SPUtils(String str, int i) {
        this.f825a = Utils.m891d().getSharedPreferences(str, i);
    }

    /* renamed from: c */
    public static SPUtils m1243c(String str) {
        return m1244c(str, 0);
    }

    /* renamed from: d */
    private static boolean m1245d(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public void mo9876a(@NonNull String str, String str2, boolean z) {
        if (str == null) {
            throw new NullPointerException("Argument 'key' of type String (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (z) {
            this.f825a.edit().putString(str, str2).commit();
        } else {
            this.f825a.edit().putString(str, str2).apply();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, int, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, long, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, boolean, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String, boolean):void */
    /* renamed from: b */
    public void mo9883b(@NonNull String str, String str2) {
        if (str != null) {
            mo9876a(str, str2, false);
            return;
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: c */
    public static SPUtils m1244c(String str, int i) {
        if (m1245d(str)) {
            str = "spUtils";
        }
        SPUtils wVar = f824b.get(str);
        if (wVar == null) {
            synchronized (SPUtils.class) {
                wVar = f824b.get(str);
                if (wVar == null) {
                    wVar = new SPUtils(str, i);
                    f824b.put(str, wVar);
                }
            }
        }
        return wVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, int, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, long, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, boolean, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, int, boolean):void */
    /* renamed from: b */
    public void mo9881b(@NonNull String str, int i) {
        if (str != null) {
            mo9874a(str, i, false);
            return;
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public String mo9871a(@NonNull String str) {
        if (str != null) {
            return mo9872a(str, "");
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, long, boolean):void
     arg types: [java.lang.String, long, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, int, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, boolean, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, long, boolean):void */
    /* renamed from: b */
    public void mo9882b(@NonNull String str, long j) {
        if (str != null) {
            mo9875a(str, j, false);
            return;
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public String mo9872a(@NonNull String str, String str2) {
        if (str != null) {
            return this.f825a.getString(str, str2);
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, boolean, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, int, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, long, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String, boolean):void
      com.blankj.utilcode.util.w.a(java.lang.String, boolean, boolean):void */
    /* renamed from: b */
    public void mo9884b(@NonNull String str, boolean z) {
        if (str != null) {
            mo9877a(str, z, false);
            return;
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public void mo9874a(@NonNull String str, int i, boolean z) {
        if (str == null) {
            throw new NullPointerException("Argument 'key' of type String (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (z) {
            this.f825a.edit().putInt(str, i).commit();
        } else {
            this.f825a.edit().putInt(str, i).apply();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.c(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.w.c(java.lang.String, int):com.blankj.utilcode.util.w
      com.blankj.utilcode.util.w.c(java.lang.String, boolean):void */
    /* renamed from: b */
    public void mo9880b(@NonNull String str) {
        if (str != null) {
            mo9885c(str, false);
            return;
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: c */
    public void mo9885c(@NonNull String str, boolean z) {
        if (str == null) {
            throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (z) {
            this.f825a.edit().remove(str).commit();
        } else {
            this.f825a.edit().remove(str).apply();
        }
    }

    /* renamed from: a */
    public int mo9869a(@NonNull String str, int i) {
        if (str != null) {
            return this.f825a.getInt(str, i);
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public void mo9875a(@NonNull String str, long j, boolean z) {
        if (str == null) {
            throw new NullPointerException("Argument 'key' of type String (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (z) {
            this.f825a.edit().putLong(str, j).commit();
        } else {
            this.f825a.edit().putLong(str, j).apply();
        }
    }

    /* renamed from: a */
    public long mo9870a(@NonNull String str, long j) {
        if (str != null) {
            return this.f825a.getLong(str, j);
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public void mo9877a(@NonNull String str, boolean z, boolean z2) {
        if (str == null) {
            throw new NullPointerException("Argument 'key' of type String (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (z2) {
            this.f825a.edit().putBoolean(str, z).commit();
        } else {
            this.f825a.edit().putBoolean(str, z).apply();
        }
    }

    /* renamed from: a */
    public boolean mo9879a(@NonNull String str, boolean z) {
        if (str != null) {
            return this.f825a.getBoolean(str, z);
        }
        throw new NullPointerException("Argument 'key' of type String (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public void mo9873a() {
        mo9878a(false);
    }

    /* renamed from: a */
    public void mo9878a(boolean z) {
        if (z) {
            this.f825a.edit().clear().commit();
        } else {
            this.f825a.edit().clear().apply();
        }
    }
}
