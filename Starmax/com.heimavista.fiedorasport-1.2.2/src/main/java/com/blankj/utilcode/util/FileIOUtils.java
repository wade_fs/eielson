package com.blankj.utilcode.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/* renamed from: com.blankj.utilcode.util.j */
public final class FileIOUtils {

    /* renamed from: a */
    private static int f771a = 524288;

    /* renamed from: com.blankj.utilcode.util.j$a */
    /* compiled from: FileIOUtils */
    public interface C0905a {
        /* renamed from: a */
        void mo9828a(double d);
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x0070 A[SYNTHETIC, Splitter:B:41:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0083 A[SYNTHETIC, Splitter:B:51:0x0083] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m1078a(java.io.File r7, java.io.InputStream r8, boolean r9, com.blankj.utilcode.util.FileIOUtils.C0905a r10) {
        /*
            r0 = 0
            if (r8 == 0) goto L_0x008c
            boolean r1 = m1083b(r7)
            if (r1 != 0) goto L_0x000b
            goto L_0x008c
        L_0x000b:
            r1 = 0
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0062 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0062 }
            r3.<init>(r7, r9)     // Catch:{ IOException -> 0x0062 }
            int r7 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x0062 }
            r2.<init>(r3, r7)     // Catch:{ IOException -> 0x0062 }
            r7 = -1
            if (r10 != 0) goto L_0x0029
            int r9 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            byte[] r9 = new byte[r9]     // Catch:{ IOException -> 0x005c, all -> 0x005a }
        L_0x001f:
            int r10 = r8.read(r9)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            if (r10 == r7) goto L_0x0048
            r2.write(r9, r0, r10)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            goto L_0x001f
        L_0x0029:
            int r9 = r8.available()     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            double r3 = (double) r9     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            r5 = 0
            r10.mo9828a(r5)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            int r9 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            byte[] r9 = new byte[r9]     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            r1 = 0
        L_0x0038:
            int r5 = r8.read(r9)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            if (r5 == r7) goto L_0x0048
            r2.write(r9, r0, r5)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            int r1 = r1 + r5
            double r5 = (double) r1     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            double r5 = r5 / r3
            r10.mo9828a(r5)     // Catch:{ IOException -> 0x005c, all -> 0x005a }
            goto L_0x0038
        L_0x0048:
            r7 = 1
            r8.close()     // Catch:{ IOException -> 0x004d }
            goto L_0x0051
        L_0x004d:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0051:
            r2.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0059
        L_0x0055:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0059:
            return r7
        L_0x005a:
            r7 = move-exception
            goto L_0x0079
        L_0x005c:
            r7 = move-exception
            r1 = r2
            goto L_0x0063
        L_0x005f:
            r7 = move-exception
            r2 = r1
            goto L_0x0079
        L_0x0062:
            r7 = move-exception
        L_0x0063:
            r7.printStackTrace()     // Catch:{ all -> 0x005f }
            r8.close()     // Catch:{ IOException -> 0x006a }
            goto L_0x006e
        L_0x006a:
            r7 = move-exception
            r7.printStackTrace()
        L_0x006e:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0078
        L_0x0074:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0078:
            return r0
        L_0x0079:
            r8.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0081:
            if (r2 == 0) goto L_0x008b
            r2.close()     // Catch:{ IOException -> 0x0087 }
            goto L_0x008b
        L_0x0087:
            r8 = move-exception
            r8.printStackTrace()
        L_0x008b:
            throw r7
        L_0x008c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.FileIOUtils.m1078a(java.io.File, java.io.InputStream, boolean, com.blankj.utilcode.util.j$a):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.j.a(java.io.File, java.lang.String, boolean):boolean
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.j.a(java.lang.String, byte[], boolean):boolean
      com.blankj.utilcode.util.j.a(java.io.File, java.lang.String, boolean):boolean */
    /* renamed from: b */
    public static boolean m1084b(File file, String str) {
        return m1079a(file, str, false);
    }

    /* renamed from: c */
    private static boolean m1086c(File file) {
        return file != null && file.exists();
    }

    /* renamed from: d */
    public static byte[] m1087d(File file) {
        return m1082a(file, (C0905a) null);
    }

    /* renamed from: e */
    public static String m1088e(File file) {
        return m1076a(file, (String) null);
    }

    /* renamed from: b */
    private static boolean m1083b(File file) {
        if (file == null) {
            return false;
        }
        if (file.exists()) {
            return file.isFile();
        }
        if (!m1077a(file.getParentFile())) {
            return false;
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: b */
    private static boolean m1085b(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static boolean m1081a(String str, byte[] bArr, boolean z) {
        return m1080a(m1075a(str), bArr, z, (C0905a) null);
    }

    /* renamed from: a */
    public static boolean m1080a(File file, byte[] bArr, boolean z, C0905a aVar) {
        if (bArr == null) {
            return false;
        }
        return m1078a(file, new ByteArrayInputStream(bArr), z, aVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0033 A[SYNTHETIC, Splitter:B:26:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x003e A[SYNTHETIC, Splitter:B:32:0x003e] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m1079a(java.io.File r4, java.lang.String r5, boolean r6) {
        /*
            r0 = 0
            if (r4 == 0) goto L_0x0047
            if (r5 != 0) goto L_0x0006
            goto L_0x0047
        L_0x0006:
            boolean r1 = m1083b(r4)
            if (r1 != 0) goto L_0x000d
            return r0
        L_0x000d:
            r1 = 0
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x002d }
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ IOException -> 0x002d }
            r3.<init>(r4, r6)     // Catch:{ IOException -> 0x002d }
            r2.<init>(r3)     // Catch:{ IOException -> 0x002d }
            r2.write(r5)     // Catch:{ IOException -> 0x0028, all -> 0x0025 }
            r4 = 1
            r2.close()     // Catch:{ IOException -> 0x0020 }
            goto L_0x0024
        L_0x0020:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0024:
            return r4
        L_0x0025:
            r4 = move-exception
            r1 = r2
            goto L_0x003c
        L_0x0028:
            r4 = move-exception
            r1 = r2
            goto L_0x002e
        L_0x002b:
            r4 = move-exception
            goto L_0x003c
        L_0x002d:
            r4 = move-exception
        L_0x002e:
            r4.printStackTrace()     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x003b
        L_0x0037:
            r4 = move-exception
            r4.printStackTrace()
        L_0x003b:
            return r0
        L_0x003c:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x0046
        L_0x0042:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0046:
            throw r4
        L_0x0047:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.FileIOUtils.m1079a(java.io.File, java.lang.String, boolean):boolean");
    }

    /* renamed from: a */
    public static String m1076a(File file, String str) {
        byte[] d = m1087d(file);
        if (d == null) {
            return null;
        }
        if (m1085b(str)) {
            return new String(d);
        }
        try {
            return new String(d, str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x0073 A[SYNTHETIC, Splitter:B:42:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0087 A[SYNTHETIC, Splitter:B:55:0x0087] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] m1082a(java.io.File r10, com.blankj.utilcode.util.FileIOUtils.C0905a r11) {
        /*
            boolean r0 = m1086c(r10)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ FileNotFoundException -> 0x0090 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0090 }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x0090 }
            int r10 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ FileNotFoundException -> 0x0090 }
            r0.<init>(r2, r10)     // Catch:{ FileNotFoundException -> 0x0090 }
            java.io.ByteArrayOutputStream r10 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0064, all -> 0x0061 }
            r10.<init>()     // Catch:{ IOException -> 0x0064, all -> 0x0061 }
            int r2 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x005f }
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x005f }
            r3 = -1
            r4 = 0
            if (r11 != 0) goto L_0x002d
        L_0x0021:
            int r11 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x005f }
            int r11 = r0.read(r2, r4, r11)     // Catch:{ IOException -> 0x005f }
            if (r11 == r3) goto L_0x004a
            r10.write(r2, r4, r11)     // Catch:{ IOException -> 0x005f }
            goto L_0x0021
        L_0x002d:
            int r5 = r0.available()     // Catch:{ IOException -> 0x005f }
            double r5 = (double) r5     // Catch:{ IOException -> 0x005f }
            r7 = 0
            r11.mo9828a(r7)     // Catch:{ IOException -> 0x005f }
            r7 = 0
        L_0x0038:
            int r8 = com.blankj.utilcode.util.FileIOUtils.f771a     // Catch:{ IOException -> 0x005f }
            int r8 = r0.read(r2, r4, r8)     // Catch:{ IOException -> 0x005f }
            if (r8 == r3) goto L_0x004a
            r10.write(r2, r4, r8)     // Catch:{ IOException -> 0x005f }
            int r7 = r7 + r8
            double r8 = (double) r7     // Catch:{ IOException -> 0x005f }
            double r8 = r8 / r5
            r11.mo9828a(r8)     // Catch:{ IOException -> 0x005f }
            goto L_0x0038
        L_0x004a:
            byte[] r11 = r10.toByteArray()     // Catch:{ IOException -> 0x005f }
            r0.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x0056
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x0056:
            r10.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x005e
        L_0x005a:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x005e:
            return r11
        L_0x005f:
            r11 = move-exception
            goto L_0x0066
        L_0x0061:
            r11 = move-exception
            r10 = r1
            goto L_0x007d
        L_0x0064:
            r11 = move-exception
            r10 = r1
        L_0x0066:
            r11.printStackTrace()     // Catch:{ all -> 0x007c }
            r0.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0071
        L_0x006d:
            r11 = move-exception
            r11.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x0071:
            if (r10 == 0) goto L_0x007b
            r10.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x007b
        L_0x0077:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x007b:
            return r1
        L_0x007c:
            r11 = move-exception
        L_0x007d:
            r0.close()     // Catch:{ IOException -> 0x0081 }
            goto L_0x0085
        L_0x0081:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x0085:
            if (r10 == 0) goto L_0x008f
            r10.close()     // Catch:{ IOException -> 0x008b }
            goto L_0x008f
        L_0x008b:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x008f:
            throw r11     // Catch:{ FileNotFoundException -> 0x0090 }
        L_0x0090:
            r10 = move-exception
            r10.printStackTrace()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.FileIOUtils.m1082a(java.io.File, com.blankj.utilcode.util.j$a):byte[]");
    }

    /* renamed from: a */
    private static File m1075a(String str) {
        if (m1085b(str)) {
            return null;
        }
        return new File(str);
    }

    /* renamed from: a */
    private static boolean m1077a(File file) {
        return file != null && (!file.exists() ? file.mkdirs() : file.isDirectory());
    }
}
