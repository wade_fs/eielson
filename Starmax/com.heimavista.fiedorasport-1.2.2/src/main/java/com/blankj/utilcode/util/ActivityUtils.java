package com.blankj.utilcode.util;

import android.app.Activity;
import androidx.annotation.NonNull;
import java.util.LinkedList;

/* renamed from: com.blankj.utilcode.util.a */
public final class ActivityUtils {
    /* renamed from: a */
    public static void m923a(@NonNull Class<? extends Activity> cls) {
        if (cls != null) {
            m924a(cls, false);
            return;
        }
        throw new NullPointerException("Argument 'clz' of type Class<? extends Activity> (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: b */
    public static Activity m926b() {
        return Utils.m889b().mo9768a();
    }

    /* renamed from: a */
    public static void m924a(@NonNull Class<? extends Activity> cls, boolean z) {
        if (cls != null) {
            for (Activity activity : Utils.m890c()) {
                if (activity.getClass().equals(cls)) {
                    activity.finish();
                    if (!z) {
                        activity.overridePendingTransition(0, 0);
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Argument 'clz' of type Class<? extends Activity> (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static void m922a() {
        m925a(false);
    }

    /* renamed from: a */
    public static void m925a(boolean z) {
        LinkedList<Activity> c = Utils.m890c();
        for (int size = c.size() - 1; size >= 0; size--) {
            Activity activity = c.get(size);
            activity.finish();
            if (!z) {
                activity.overridePendingTransition(0, 0);
            }
        }
    }
}
