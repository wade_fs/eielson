package com.blankj.utilcode.util;

import android.content.res.Resources;

/* renamed from: com.blankj.utilcode.util.h */
public final class ConvertUtils {
    /* renamed from: a */
    public static int m1055a(float f) {
        return (int) ((f * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    /* renamed from: b */
    public static int m1056b(float f) {
        return (int) ((f * Resources.getSystem().getDisplayMetrics().scaledDensity) + 0.5f);
    }
}
