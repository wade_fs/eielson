package com.blankj.utilcode.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

/* renamed from: com.blankj.utilcode.util.d */
public final class BarUtils {
    /* renamed from: a */
    public static int m981a() {
        Resources resources = Utils.m891d().getResources();
        return resources.getDimensionPixelSize(resources.getIdentifier("status_bar_height", "dimen", "android"));
    }

    /* renamed from: b */
    public static View m990b(@NonNull Activity activity, @ColorInt int i, boolean z) {
        if (activity == null) {
            throw new NullPointerException("Argument 'activity' of type Activity (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (Build.VERSION.SDK_INT < 19) {
            return null;
        } else {
            m986a(activity);
            return m983a(activity, i, z);
        }
    }

    /* renamed from: a */
    public static void m987a(@NonNull Activity activity, boolean z) {
        if (activity != null) {
            m989a(activity.getWindow(), z);
            return;
        }
        throw new NullPointerException("Argument 'activity' of type Activity (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static void m989a(@NonNull Window window, boolean z) {
        if (window == null) {
            throw new NullPointerException("Argument 'window' of type Window (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (Build.VERSION.SDK_INT >= 23) {
            View decorView = window.getDecorView();
            int systemUiVisibility = decorView.getSystemUiVisibility();
            decorView.setSystemUiVisibility(z ? systemUiVisibility | 8192 : systemUiVisibility & -8193);
        }
    }

    /* renamed from: a */
    public static View m982a(@NonNull Activity activity, @ColorInt int i) {
        if (activity != null) {
            return m990b(activity, i, false);
        }
        throw new NullPointerException("Argument 'activity' of type Activity (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    private static View m983a(Activity activity, int i, boolean z) {
        return m985a(activity.getWindow(), i, z);
    }

    /* renamed from: a */
    private static View m985a(Window window, int i, boolean z) {
        ViewGroup viewGroup;
        if (z) {
            viewGroup = (ViewGroup) window.getDecorView();
        } else {
            viewGroup = (ViewGroup) window.findViewById(16908290);
        }
        View findViewWithTag = viewGroup.findViewWithTag("TAG_STATUS_BAR");
        if (findViewWithTag != null) {
            if (findViewWithTag.getVisibility() == 8) {
                findViewWithTag.setVisibility(0);
            }
            findViewWithTag.setBackgroundColor(i);
            return findViewWithTag;
        }
        View a = m984a(window.getContext(), i);
        viewGroup.addView(a);
        return a;
    }

    /* renamed from: a */
    private static View m984a(Context context, int i) {
        View view = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, m981a()));
        view.setBackgroundColor(i);
        view.setTag("TAG_STATUS_BAR");
        return view;
    }

    /* renamed from: a */
    public static void m986a(Activity activity) {
        m988a(activity.getWindow());
    }

    /* renamed from: a */
    public static void m988a(Window window) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            if (i >= 21) {
                window.clearFlags(67108864);
                window.addFlags(Integer.MIN_VALUE);
                window.getDecorView().setSystemUiVisibility(window.getDecorView().getSystemUiVisibility() | 1280);
                window.setStatusBarColor(0);
                return;
            }
            window.addFlags(67108864);
        }
    }
}
