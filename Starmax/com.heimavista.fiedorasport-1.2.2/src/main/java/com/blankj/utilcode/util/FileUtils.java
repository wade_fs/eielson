package com.blankj.utilcode.util;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: com.blankj.utilcode.util.k */
public final class FileUtils {

    /* renamed from: com.blankj.utilcode.util.k$a */
    /* compiled from: FileUtils */
    static class C0906a implements FileFilter {
        C0906a() {
        }

        public boolean accept(File file) {
            return true;
        }
    }

    static {
        System.getProperty("line.separator");
    }

    /* renamed from: a */
    public static File m1090a(String str) {
        if (m1097b(str)) {
            return null;
        }
        return new File(str);
    }

    /* renamed from: b */
    private static boolean m1097b(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: c */
    public static List<File> m1098c(String str) {
        return m1095a(str, (Comparator<File>) null);
    }

    /* renamed from: a */
    public static boolean m1096a(File file) {
        return file != null && file.exists() && file.isDirectory();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.k.a(java.io.File, boolean):java.util.List<java.io.File>
     arg types: [java.io.File, int]
     candidates:
      com.blankj.utilcode.util.k.a(java.lang.String, java.util.Comparator<java.io.File>):java.util.List<java.io.File>
      com.blankj.utilcode.util.k.a(java.io.File, boolean):java.util.List<java.io.File> */
    /* renamed from: a */
    public static List<File> m1095a(String str, Comparator<File> comparator) {
        return m1093a(m1090a(str), false);
    }

    /* renamed from: a */
    public static List<File> m1093a(File file, boolean z) {
        return m1094a(file, z, (Comparator<File>) null);
    }

    /* renamed from: a */
    public static List<File> m1094a(File file, boolean z, Comparator<File> comparator) {
        return m1092a(file, new C0906a(), z, comparator);
    }

    /* renamed from: a */
    public static List<File> m1092a(File file, FileFilter fileFilter, boolean z, Comparator<File> comparator) {
        List<File> a = m1091a(file, fileFilter, z);
        if (comparator != null) {
            Collections.sort(a, comparator);
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.k.a(java.io.File, java.io.FileFilter, boolean):java.util.List<java.io.File>
     arg types: [java.io.File, java.io.FileFilter, int]
     candidates:
      com.blankj.utilcode.util.k.a(java.io.File, boolean, java.util.Comparator<java.io.File>):java.util.List<java.io.File>
      com.blankj.utilcode.util.k.a(java.io.File, java.io.FileFilter, boolean):java.util.List<java.io.File> */
    /* renamed from: a */
    private static List<File> m1091a(File file, FileFilter fileFilter, boolean z) {
        File[] listFiles;
        ArrayList arrayList = new ArrayList();
        if (!(!m1096a(file) || (listFiles = file.listFiles()) == null || listFiles.length == 0)) {
            for (File file2 : listFiles) {
                if (fileFilter.accept(file2)) {
                    arrayList.add(file2);
                }
                if (z && file2.isDirectory()) {
                    arrayList.addAll(m1091a(file2, fileFilter, true));
                }
            }
        }
        return arrayList;
    }
}
