package com.blankj.utilcode.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.blankj.utilcode.util.b */
public final class AdaptScreenUtils {

    /* renamed from: a */
    private static List<Field> f699a;

    /* renamed from: a */
    private static void m938a(Resources resources, float f) {
        resources.getDisplayMetrics().xdpi = f;
        Utils.m891d().getResources().getDisplayMetrics().xdpi = f;
        m940c(resources, f);
    }

    /* renamed from: b */
    private static void m939b(Resources resources, float f) {
        for (Field field : f699a) {
            try {
                DisplayMetrics displayMetrics = (DisplayMetrics) field.get(resources);
                if (displayMetrics != null) {
                    displayMetrics.xdpi = f;
                }
            } catch (Exception e) {
                Log.e("AdaptScreenUtils", "applyMetricsFields: " + e);
            }
        }
    }

    /* renamed from: c */
    private static void m940c(Resources resources, float f) {
        if (f699a == null) {
            f699a = new ArrayList();
            Class<?> cls = resources.getClass();
            Field[] declaredFields = cls.getDeclaredFields();
            while (declaredFields != null && declaredFields.length > 0) {
                for (Field field : declaredFields) {
                    if (field.getType().isAssignableFrom(DisplayMetrics.class)) {
                        field.setAccessible(true);
                        DisplayMetrics a = m936a(resources, field);
                        if (a != null) {
                            f699a.add(field);
                            a.xdpi = f;
                        }
                    }
                }
                cls = cls.getSuperclass();
                if (cls != null) {
                    declaredFields = cls.getDeclaredFields();
                } else {
                    return;
                }
            }
            return;
        }
        m939b(resources, f);
    }

    /* renamed from: a */
    static void m937a() {
        m938a(Resources.getSystem(), Resources.getSystem().getDisplayMetrics().xdpi);
    }

    /* renamed from: a */
    private static DisplayMetrics m936a(Resources resources, Field field) {
        try {
            return (DisplayMetrics) field.get(resources);
        } catch (Exception e) {
            Log.e("AdaptScreenUtils", "getMetricsFromField: " + e);
            return null;
        }
    }
}
