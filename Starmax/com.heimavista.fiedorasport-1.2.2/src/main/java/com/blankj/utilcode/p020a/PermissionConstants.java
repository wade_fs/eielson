package com.blankj.utilcode.p020a;

import android.annotation.SuppressLint;
import android.os.Build;

@SuppressLint({"InlinedApi"})
/* renamed from: com.blankj.utilcode.a.b */
public final class PermissionConstants {

    /* renamed from: a */
    private static final String[] f575a = {"android.permission.READ_CALENDAR", "android.permission.WRITE_CALENDAR"};

    /* renamed from: b */
    private static final String[] f576b = {"android.permission.CAMERA"};

    /* renamed from: c */
    private static final String[] f577c = {"android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.GET_ACCOUNTS"};

    /* renamed from: d */
    private static final String[] f578d = {"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"};

    /* renamed from: e */
    private static final String[] f579e = {"android.permission.RECORD_AUDIO"};

    /* renamed from: f */
    private static final String[] f580f = {"android.permission.READ_PHONE_STATE", "android.permission.READ_PHONE_NUMBERS", "android.permission.CALL_PHONE", "android.permission.READ_CALL_LOG", "android.permission.WRITE_CALL_LOG", "com.android.voicemail.permission.ADD_VOICEMAIL", "android.permission.USE_SIP", "android.permission.PROCESS_OUTGOING_CALLS", "android.permission.ANSWER_PHONE_CALLS"};

    /* renamed from: g */
    private static final String[] f581g = {"android.permission.READ_PHONE_STATE", "android.permission.READ_PHONE_NUMBERS", "android.permission.CALL_PHONE", "android.permission.READ_CALL_LOG", "android.permission.WRITE_CALL_LOG", "com.android.voicemail.permission.ADD_VOICEMAIL", "android.permission.USE_SIP", "android.permission.PROCESS_OUTGOING_CALLS"};

    /* renamed from: h */
    private static final String[] f582h = {"android.permission.BODY_SENSORS"};

    /* renamed from: i */
    private static final String[] f583i = {"android.permission.SEND_SMS", "android.permission.RECEIVE_SMS", "android.permission.READ_SMS", "android.permission.RECEIVE_WAP_PUSH", "android.permission.RECEIVE_MMS"};

    /* renamed from: j */
    private static final String[] f584j = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: a */
    public static String[] m848a(String str) {
        char c;
        switch (str.hashCode()) {
            case -1639857183:
                if (str.equals("android.permission-group.CONTACTS")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case -1410061184:
                if (str.equals("android.permission-group.PHONE")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case -1250730292:
                if (str.equals("android.permission-group.CALENDAR")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case -1140935117:
                if (str.equals("android.permission-group.CAMERA")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 421761675:
                if (str.equals("android.permission-group.SENSORS")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case 828638019:
                if (str.equals("android.permission-group.LOCATION")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 852078861:
                if (str.equals("android.permission-group.STORAGE")) {
                    c = 8;
                    break;
                }
                c = 65535;
                break;
            case 1581272376:
                if (str.equals("android.permission-group.MICROPHONE")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 1795181803:
                if (str.equals("android.permission-group.SMS")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                return f575a;
            case 1:
                return f576b;
            case 2:
                return f577c;
            case 3:
                return f578d;
            case 4:
                return f579e;
            case 5:
                if (Build.VERSION.SDK_INT < 26) {
                    return f581g;
                }
                return f580f;
            case 6:
                return f582h;
            case 7:
                return f583i;
            case 8:
                return f584j;
            default:
                return new String[]{str};
        }
    }
}
