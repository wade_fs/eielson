package com.blankj.utilcode.util;

import com.facebook.appevents.AppEventsConstants;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.blankj.utilcode.util.i */
public final class EncryptUtils {

    /* renamed from: a */
    private static final char[] f770a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a */
    public static String m1061a(String str) {
        return (str == null || str.length() == 0) ? "" : m1070c(str.getBytes());
    }

    /* renamed from: b */
    public static byte[] m1068b(byte[] bArr) {
        return m1064a(bArr, "MD5");
    }

    /* renamed from: c */
    public static String m1070c(byte[] bArr) {
        return m1062a(m1068b(bArr));
    }

    /* renamed from: d */
    public static String m1073d(byte[] bArr, byte[] bArr2, String str, byte[] bArr3) {
        return m1062a(m1072c(bArr, bArr2, str, bArr3));
    }

    /* renamed from: e */
    public static byte[] m1074e(byte[] bArr, byte[] bArr2, String str, byte[] bArr3) {
        return m1065a(bArr, bArr2, "DES", str, bArr3, true);
    }

    /* renamed from: b */
    public static byte[] m1069b(byte[] bArr, byte[] bArr2, String str, byte[] bArr3) {
        return m1065a(bArr, bArr2, "DES", str, bArr3, false);
    }

    /* renamed from: c */
    public static byte[] m1072c(byte[] bArr, byte[] bArr2, String str, byte[] bArr3) {
        return m1065a(bArr, bArr2, "AES", str, bArr3, true);
    }

    /* renamed from: a */
    private static byte[] m1064a(byte[] bArr, String str) {
        if (bArr != null && bArr.length > 0) {
            try {
                MessageDigest instance = MessageDigest.getInstance(str);
                instance.update(bArr);
                return instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* renamed from: b */
    private static byte[] m1067b(String str) {
        if (m1071c(str)) {
            return null;
        }
        int length = str.length();
        if (length % 2 != 0) {
            str = AppEventsConstants.EVENT_PARAM_VALUE_NO + str;
            length++;
        }
        char[] charArray = str.toUpperCase().toCharArray();
        byte[] bArr = new byte[(length >> 1)];
        for (int i = 0; i < length; i += 2) {
            bArr[i >> 1] = (byte) ((m1060a(charArray[i]) << 4) | m1060a(charArray[i + 1]));
        }
        return bArr;
    }

    /* renamed from: c */
    private static boolean m1071c(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static byte[] m1063a(String str, byte[] bArr, String str2, byte[] bArr2) {
        return m1066a(m1067b(str), bArr, str2, bArr2);
    }

    /* renamed from: a */
    public static byte[] m1066a(byte[] bArr, byte[] bArr2, String str, byte[] bArr3) {
        return m1065a(bArr, bArr2, "AES", str, bArr3, false);
    }

    /* renamed from: a */
    private static byte[] m1065a(byte[] bArr, byte[] bArr2, String str, String str2, byte[] bArr3, boolean z) {
        SecretKey secretKey;
        if (!(bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0)) {
            try {
                if ("DES".equals(str)) {
                    secretKey = SecretKeyFactory.getInstance(str).generateSecret(new DESKeySpec(bArr2));
                } else {
                    secretKey = new SecretKeySpec(bArr2, str);
                }
                Cipher instance = Cipher.getInstance(str2);
                int i = 1;
                if (bArr3 != null) {
                    if (bArr3.length != 0) {
                        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr3);
                        if (!z) {
                            i = 2;
                        }
                        instance.init(i, secretKey, ivParameterSpec);
                        return instance.doFinal(bArr);
                    }
                }
                if (!z) {
                    i = 2;
                }
                instance.init(i, secretKey);
                return instance.doFinal(bArr);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    /* renamed from: a */
    private static String m1062a(byte[] bArr) {
        int length;
        if (bArr == null || (length = bArr.length) <= 0) {
            return "";
        }
        char[] cArr = new char[(length << 1)];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i + 1;
            char[] cArr2 = f770a;
            cArr[i] = cArr2[(bArr[i2] >> 4) & 15];
            i = i3 + 1;
            cArr[i3] = cArr2[bArr[i2] & 15];
        }
        return new String(cArr);
    }

    /* renamed from: a */
    private static int m1060a(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return (c - 'A') + 10;
        }
        throw new IllegalArgumentException();
    }
}
