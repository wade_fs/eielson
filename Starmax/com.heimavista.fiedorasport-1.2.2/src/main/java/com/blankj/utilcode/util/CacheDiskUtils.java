package com.blankj.utilcode.util;

import android.util.Log;
import androidx.annotation.NonNull;
import com.blankj.utilcode.p020a.CacheConstants;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.blankj.utilcode.util.f */
public final class CacheDiskUtils implements CacheConstants {

    /* renamed from: f */
    private static final Map<String, CacheDiskUtils> f733f = new HashMap();

    /* renamed from: a */
    private final String f734a;

    /* renamed from: b */
    private final File f735b;

    /* renamed from: c */
    private final long f736c;

    /* renamed from: d */
    private final int f737d;

    /* renamed from: e */
    private C0888b f738e;

    /* renamed from: com.blankj.utilcode.util.f$b */
    /* compiled from: CacheDiskUtils */
    private static final class C0888b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final AtomicLong f739a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final AtomicInteger f740b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public final Map<File, Long> f741c;

        /* renamed from: d */
        private final File f742d;

        /* renamed from: e */
        private final Thread f743e;

        /* renamed from: com.blankj.utilcode.util.f$b$a */
        /* compiled from: CacheDiskUtils */
        class C0889a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ File f744P;

            /* renamed from: com.blankj.utilcode.util.f$b$a$a */
            /* compiled from: CacheDiskUtils */
            class C0890a implements FilenameFilter {
                C0890a(C0889a aVar) {
                }

                public boolean accept(File file, String str) {
                    return str.startsWith("cdu_");
                }
            }

            C0889a(File file) {
                this.f744P = file;
            }

            public void run() {
                File[] listFiles = this.f744P.listFiles(new C0890a(this));
                if (listFiles != null) {
                    int i = 0;
                    int i2 = 0;
                    for (File file : listFiles) {
                        i = (int) (((long) i) + file.length());
                        i2++;
                        C0888b.this.f741c.put(file, Long.valueOf(file.lastModified()));
                    }
                    C0888b.this.f739a.getAndAdd((long) i);
                    C0888b.this.f740b.getAndAdd(i2);
                }
            }
        }

        /* renamed from: com.blankj.utilcode.util.f$b$b */
        /* compiled from: CacheDiskUtils */
        class C0891b implements FilenameFilter {
            C0891b(C0888b bVar) {
            }

            public boolean accept(File file, String str) {
                return str.startsWith("cdu_");
            }
        }

        private C0888b(File file, long j, int i) {
            this.f741c = Collections.synchronizedMap(new HashMap());
            this.f742d = file;
            this.f739a = new AtomicLong();
            this.f740b = new AtomicInteger();
            this.f743e = new Thread(new C0889a(file));
            this.f743e.start();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m1021a() {
            File[] listFiles = this.f742d.listFiles(new C0891b(this));
            if (listFiles == null || listFiles.length <= 0) {
                return true;
            }
            boolean z = true;
            for (File file : listFiles) {
                if (!file.delete()) {
                    z = false;
                } else {
                    this.f739a.addAndGet(-file.length());
                    this.f740b.addAndGet(-1);
                    this.f741c.remove(file);
                }
            }
            if (z) {
                this.f741c.clear();
                this.f739a.set(0);
                this.f740b.set(0);
            }
            return z;
        }
    }

    private CacheDiskUtils(String str, File file, long j, int i) {
        this.f734a = str;
        this.f735b = file;
        this.f736c = j;
        this.f737d = i;
    }

    /* renamed from: a */
    public static CacheDiskUtils m1016a(String str, long j, int i) {
        if (m1017a(str)) {
            str = "cacheUtils";
        }
        return m1015a(new File(Utils.m891d().getCacheDir(), str), j, i);
    }

    /* renamed from: b */
    private C0888b m1018b() {
        if (this.f735b.exists()) {
            if (this.f738e == null) {
                this.f738e = new C0888b(this.f735b, this.f736c, this.f737d);
            }
        } else if (this.f735b.mkdirs()) {
            this.f738e = new C0888b(this.f735b, this.f736c, this.f737d);
        } else {
            Log.e("CacheDiskUtils", "can't make dirs in " + this.f735b.getAbsolutePath());
        }
        return this.f738e;
    }

    /* renamed from: c */
    public static CacheDiskUtils m1019c() {
        return m1016a("", Long.MAX_VALUE, Integer.MAX_VALUE);
    }

    public String toString() {
        return this.f734a + "@" + Integer.toHexString(hashCode());
    }

    /* renamed from: a */
    public static CacheDiskUtils m1015a(@NonNull File file, long j, int i) {
        if (file != null) {
            String str = file.getAbsoluteFile() + "_" + j + "_" + i;
            CacheDiskUtils fVar = f733f.get(str);
            if (fVar == null) {
                synchronized (CacheDiskUtils.class) {
                    fVar = f733f.get(str);
                    if (fVar == null) {
                        fVar = new CacheDiskUtils(str, file, j, i);
                        f733f.put(str, fVar);
                    }
                }
            }
            return fVar;
        }
        throw new NullPointerException("Argument 'cacheDir' of type File (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public boolean mo9811a() {
        C0888b b = m1018b();
        if (b == null) {
            return true;
        }
        return b.m1021a();
    }

    /* renamed from: a */
    private static boolean m1017a(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
