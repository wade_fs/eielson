package com.blankj.utilcode.util;

import android.graphics.Point;
import android.os.Build;
import android.view.WindowManager;

/* renamed from: com.blankj.utilcode.util.x */
public final class ScreenUtils {
    /* renamed from: a */
    public static int m1263a() {
        WindowManager windowManager = (WindowManager) Utils.m891d().getSystemService("window");
        if (windowManager == null) {
            return -1;
        }
        Point point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
        return point.x;
    }

    /* renamed from: b */
    public static int m1264b() {
        WindowManager windowManager = (WindowManager) Utils.m891d().getSystemService("window");
        if (windowManager == null) {
            return -1;
        }
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            windowManager.getDefaultDisplay().getRealSize(point);
        } else {
            windowManager.getDefaultDisplay().getSize(point);
        }
        return point.y;
    }

    /* renamed from: c */
    public static int m1265c() {
        WindowManager windowManager = (WindowManager) Utils.m891d().getSystemService("window");
        if (windowManager == null) {
            return -1;
        }
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            windowManager.getDefaultDisplay().getRealSize(point);
        } else {
            windowManager.getDefaultDisplay().getSize(point);
        }
        return point.x;
    }
}
