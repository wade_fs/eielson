package com.blankj.utilcode.util;

import android.os.Environment;

/* renamed from: com.blankj.utilcode.util.v */
public final class SDCardUtils {
    /* renamed from: a */
    public static String m1241a() {
        return "mounted".equals(Environment.getExternalStorageState()) ? Environment.getExternalStorageDirectory().getAbsolutePath() : "";
    }

    /* renamed from: b */
    public static boolean m1242b() {
        return "mounted".equals(Environment.getExternalStorageState());
    }
}
