package com.blankj.utilcode.util;

import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import java.io.File;

/* renamed from: com.blankj.utilcode.util.g0 */
public final class UriUtils {
    /* renamed from: a */
    public static Uri m1051a(@NonNull File file) {
        if (file == null) {
            throw new NullPointerException("Argument 'file' of type File (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (Build.VERSION.SDK_INT < 24) {
            return Uri.fromFile(file);
        } else {
            return FileProvider.getUriForFile(Utils.m891d(), Utils.m891d().getPackageName() + ".utilcode.provider", file);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0180 A[Catch:{ Exception -> 0x01d5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0181 A[Catch:{ Exception -> 0x01d5 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File m1052a(@androidx.annotation.NonNull android.net.Uri r17) {
        /*
            r1 = r17
            if (r1 == 0) goto L_0x0319
            java.lang.String r0 = r17.toString()
            java.lang.String r2 = "UriUtils"
            android.util.Log.d(r2, r0)
            java.lang.String r0 = r17.getAuthority()
            java.lang.String r3 = r17.getScheme()
            java.lang.String r4 = r17.getPath()
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 24
            java.lang.String r7 = "/"
            r8 = 0
            if (r5 < r6) goto L_0x008e
            if (r4 == 0) goto L_0x008e
            java.lang.String r5 = "/external"
            java.lang.String r6 = "/external_path"
            java.lang.String[] r5 = new java.lang.String[]{r5, r6}
            int r6 = r5.length
            r9 = 0
        L_0x002e:
            if (r9 >= r6) goto L_0x008e
            r10 = r5[r9]
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r10)
            r11.append(r7)
            java.lang.String r11 = r11.toString()
            boolean r11 = r4.startsWith(r11)
            if (r11 == 0) goto L_0x008b
            java.io.File r11 = new java.io.File
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.io.File r13 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r13 = r13.getAbsolutePath()
            r12.append(r13)
            java.lang.String r13 = ""
            java.lang.String r13 = r4.replace(r10, r13)
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12)
            boolean r12 = r11.exists()
            if (r12 == 0) goto L_0x008b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " -> "
            r0.append(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            return r11
        L_0x008b:
            int r9 = r9 + 1
            goto L_0x002e
        L_0x008e:
            java.lang.String r5 = "file"
            boolean r5 = r5.equals(r3)
            r6 = 0
            if (r5 == 0) goto L_0x00b8
            if (r4 == 0) goto L_0x009f
            java.io.File r0 = new java.io.File
            r0.<init>(r4)
            return r0
        L_0x009f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 0"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            return r6
        L_0x00b8:
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 19
            java.lang.String r9 = "content"
            if (r4 < r5) goto L_0x02f2
            android.app.Application r4 = com.blankj.utilcode.util.Utils.m891d()
            boolean r4 = android.provider.DocumentsContract.isDocumentUri(r4, r1)
            if (r4 == 0) goto L_0x02f2
            java.lang.String r4 = "com.android.externalstorage.documents"
            boolean r4 = r4.equals(r0)
            java.lang.String r5 = ":"
            r10 = 1
            if (r4 == 0) goto L_0x0214
            java.lang.String r0 = android.provider.DocumentsContract.getDocumentId(r17)
            java.lang.String[] r0 = r0.split(r5)
            r3 = r0[r8]
            java.lang.String r4 = "primary"
            boolean r4 = r4.equalsIgnoreCase(r3)
            if (r4 == 0) goto L_0x0105
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()
            r2.append(r3)
            r2.append(r7)
            r0 = r0[r10]
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            return r1
        L_0x0105:
            android.app.Application r4 = com.blankj.utilcode.util.Utils.m891d()
            java.lang.String r5 = "storage"
            java.lang.Object r4 = r4.getSystemService(r5)
            android.os.storage.StorageManager r4 = (android.os.storage.StorageManager) r4
            java.lang.String r5 = "android.os.storage.StorageVolume"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x01d5 }
            java.lang.Class r9 = r4.getClass()     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r11 = "getVolumeList"
            java.lang.Class[] r12 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r9 = r9.getMethod(r11, r12)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r11 = "getUuid"
            java.lang.Class[] r12 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r11 = r5.getMethod(r11, r12)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r12 = "getState"
            java.lang.Class[] r13 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r12 = r5.getMethod(r12, r13)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r13 = "getPath"
            java.lang.Class[] r14 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r13 = r5.getMethod(r13, r14)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r14 = "isPrimary"
            java.lang.Class[] r15 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r14 = r5.getMethod(r14, r15)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r15 = "isEmulated"
            java.lang.Class[] r6 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.reflect.Method r5 = r5.getMethod(r15, r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object[] r6 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r4 = r9.invoke(r4, r6)     // Catch:{ Exception -> 0x01d5 }
            int r6 = java.lang.reflect.Array.getLength(r4)     // Catch:{ Exception -> 0x01d5 }
            r9 = 0
        L_0x0156:
            if (r9 >= r6) goto L_0x01fa
            java.lang.Object r15 = java.lang.reflect.Array.get(r4, r9)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r10 = "mounted"
            r16 = r4
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r4 = r12.invoke(r15, r4)     // Catch:{ Exception -> 0x01d5 }
            boolean r4 = r10.equals(r4)     // Catch:{ Exception -> 0x01d5 }
            if (r4 != 0) goto L_0x017d
            java.lang.String r4 = "mounted_ro"
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r10 = r12.invoke(r15, r10)     // Catch:{ Exception -> 0x01d5 }
            boolean r4 = r4.equals(r10)     // Catch:{ Exception -> 0x01d5 }
            if (r4 == 0) goto L_0x017b
            goto L_0x017d
        L_0x017b:
            r4 = 0
            goto L_0x017e
        L_0x017d:
            r4 = 1
        L_0x017e:
            if (r4 != 0) goto L_0x0181
            goto L_0x01cf
        L_0x0181:
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r4 = r14.invoke(r15, r4)     // Catch:{ Exception -> 0x01d5 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ Exception -> 0x01d5 }
            boolean r4 = r4.booleanValue()     // Catch:{ Exception -> 0x01d5 }
            if (r4 == 0) goto L_0x019e
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r4 = r5.invoke(r15, r4)     // Catch:{ Exception -> 0x01d5 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ Exception -> 0x01d5 }
            boolean r4 = r4.booleanValue()     // Catch:{ Exception -> 0x01d5 }
            if (r4 == 0) goto L_0x019e
            goto L_0x01cf
        L_0x019e:
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r4 = r11.invoke(r15, r4)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x01d5 }
            if (r4 == 0) goto L_0x01cf
            boolean r4 = r4.equals(r3)     // Catch:{ Exception -> 0x01d5 }
            if (r4 == 0) goto L_0x01cf
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x01d5 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d5 }
            r4.<init>()     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object[] r5 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01d5 }
            java.lang.Object r5 = r13.invoke(r15, r5)     // Catch:{ Exception -> 0x01d5 }
            r4.append(r5)     // Catch:{ Exception -> 0x01d5 }
            r4.append(r7)     // Catch:{ Exception -> 0x01d5 }
            r5 = 1
            r0 = r0[r5]     // Catch:{ Exception -> 0x01d5 }
            r4.append(r0)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x01d5 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x01d5 }
            return r3
        L_0x01cf:
            int r9 = r9 + 1
            r4 = r16
            r10 = 1
            goto L_0x0156
        L_0x01d5:
            r0 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r17.toString()
            r3.append(r4)
            java.lang.String r4 = " parse failed. "
            r3.append(r4)
            java.lang.String r0 = r0.toString()
            r3.append(r0)
            java.lang.String r0 = " -> 1_0"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            android.util.Log.d(r2, r0)
        L_0x01fa:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 1_0"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r1 = 0
            return r1
        L_0x0214:
            java.lang.String r4 = "com.android.providers.downloads.documents"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x026d
            java.lang.String r0 = android.provider.DocumentsContract.getDocumentId(r17)
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 != 0) goto L_0x0253
            java.lang.String r3 = "content://downloads/public_downloads"
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ NumberFormatException -> 0x023f }
            java.lang.Long r4 = java.lang.Long.valueOf(r0)     // Catch:{ NumberFormatException -> 0x023f }
            long r4 = r4.longValue()     // Catch:{ NumberFormatException -> 0x023f }
            android.net.Uri r3 = android.content.ContentUris.withAppendedId(r3, r4)     // Catch:{ NumberFormatException -> 0x023f }
            java.lang.String r4 = "1_1"
            java.io.File r0 = m1053a(r3, r4)     // Catch:{ NumberFormatException -> 0x023f }
            return r0
        L_0x023f:
            java.lang.String r3 = "raw:"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x0253
            java.io.File r1 = new java.io.File
            r2 = 4
            java.lang.String r0 = r0.substring(r2)
            r1.<init>(r0)
            return r1
        L_0x0253:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 1_1"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r1 = 0
            return r1
        L_0x026d:
            java.lang.String r4 = "com.android.providers.media.documents"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x02cb
            java.lang.String r0 = android.provider.DocumentsContract.getDocumentId(r17)
            java.lang.String[] r0 = r0.split(r5)
            r3 = r0[r8]
            java.lang.String r4 = "image"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x028b
            android.net.Uri r1 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        L_0x0289:
            r2 = 1
            goto L_0x02a2
        L_0x028b:
            java.lang.String r4 = "video"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0297
            android.net.Uri r1 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            goto L_0x0289
        L_0x0297:
            java.lang.String r4 = "audio"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x02b1
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            goto L_0x0289
        L_0x02a2:
            java.lang.String[] r3 = new java.lang.String[r2]
            r0 = r0[r2]
            r3[r8] = r0
            java.lang.String r0 = "_id=?"
            java.lang.String r2 = "1_2"
            java.io.File r0 = m1054a(r1, r0, r3, r2)
            return r0
        L_0x02b1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 1_2"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r1 = 0
            return r1
        L_0x02cb:
            boolean r0 = r9.equals(r3)
            if (r0 == 0) goto L_0x02d8
            java.lang.String r0 = "1_3"
            java.io.File r0 = m1053a(r1, r0)
            return r0
        L_0x02d8:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 1_4"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r1 = 0
            return r1
        L_0x02f2:
            boolean r0 = r9.equals(r3)
            if (r0 == 0) goto L_0x02ff
            java.lang.String r0 = "2"
            java.io.File r0 = m1053a(r1, r0)
            return r0
        L_0x02ff:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r17.toString()
            r0.append(r1)
            java.lang.String r1 = " parse failed. -> 3"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r1 = 0
            return r1
        L_0x0319:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Argument 'uri' of type Uri (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.UriUtils.m1052a(android.net.Uri):java.io.File");
    }

    /* renamed from: a */
    private static File m1053a(Uri uri, String str) {
        return m1054a(uri, null, null, str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:4|5|(2:7|(3:9|10|11)(4:12|13|14|15))(4:16|17|18|19)|21|22|23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0097, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b8, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bb, code lost:
        throw r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0099 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.io.File m1054a(android.net.Uri r7, java.lang.String r8, java.lang.String[] r9, java.lang.String r10) {
        /*
            android.app.Application r0 = com.blankj.utilcode.util.Utils.m891d()
            android.content.ContentResolver r1 = r0.getContentResolver()
            java.lang.String r0 = "_data"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            r6 = 0
            r2 = r7
            r4 = r8
            r5 = r9
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)
            r9 = 0
            java.lang.String r1 = "UriUtils"
            if (r8 != 0) goto L_0x0037
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r7 = r7.toString()
            r8.append(r7)
            java.lang.String r7 = " parse failed(cursor is null). -> "
            r8.append(r7)
            r8.append(r10)
            java.lang.String r7 = r8.toString()
            android.util.Log.d(r1, r7)
            return r9
        L_0x0037:
            boolean r2 = r8.moveToFirst()     // Catch:{ Exception -> 0x0099 }
            if (r2 == 0) goto L_0x0078
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x0099 }
            r2 = -1
            if (r0 <= r2) goto L_0x0051
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0099 }
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x0099 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0099 }
            r8.close()
            return r2
        L_0x0051:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0099 }
            r2.<init>()     // Catch:{ Exception -> 0x0099 }
            java.lang.String r3 = r7.toString()     // Catch:{ Exception -> 0x0099 }
            r2.append(r3)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r3 = " parse failed(columnIndex: "
            r2.append(r3)     // Catch:{ Exception -> 0x0099 }
            r2.append(r0)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r0 = " is wrong). -> "
            r2.append(r0)     // Catch:{ Exception -> 0x0099 }
            r2.append(r10)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0099 }
            android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0099 }
            r8.close()
            return r9
        L_0x0078:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0099 }
            r0.<init>()     // Catch:{ Exception -> 0x0099 }
            java.lang.String r2 = r7.toString()     // Catch:{ Exception -> 0x0099 }
            r0.append(r2)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r2 = " parse failed(moveToFirst return false). -> "
            r0.append(r2)     // Catch:{ Exception -> 0x0099 }
            r0.append(r10)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0099 }
            android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0099 }
            r8.close()
            return r9
        L_0x0097:
            r7 = move-exception
            goto L_0x00b8
        L_0x0099:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0097 }
            r0.append(r7)     // Catch:{ all -> 0x0097 }
            java.lang.String r7 = " parse failed. -> "
            r0.append(r7)     // Catch:{ all -> 0x0097 }
            r0.append(r10)     // Catch:{ all -> 0x0097 }
            java.lang.String r7 = r0.toString()     // Catch:{ all -> 0x0097 }
            android.util.Log.d(r1, r7)     // Catch:{ all -> 0x0097 }
            r8.close()
            return r9
        L_0x00b8:
            r8.close()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.UriUtils.m1054a(android.net.Uri, java.lang.String, java.lang.String[], java.lang.String):java.io.File");
    }
}
