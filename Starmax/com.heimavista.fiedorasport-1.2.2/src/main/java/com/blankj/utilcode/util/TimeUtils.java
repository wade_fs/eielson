package com.blankj.utilcode.util;

import androidx.annotation.NonNull;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* renamed from: com.blankj.utilcode.util.e0 */
public final class TimeUtils {

    /* renamed from: a */
    private static final ThreadLocal<SimpleDateFormat> f732a = new ThreadLocal<>();

    static {
        new String[]{"猴", "鸡", "狗", "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊"};
        new String[]{"水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座"};
    }

    /* renamed from: a */
    private static SimpleDateFormat m1004a() {
        return m1005a("yyyy-MM-dd HH:mm:ss");
    }

    /* renamed from: b */
    public static String m1009b(long j) {
        return m999a(j, m1004a());
    }

    /* renamed from: c */
    public static long m1013c(String str) {
        return m997a(str, m1004a());
    }

    /* renamed from: a */
    private static SimpleDateFormat m1005a(String str) {
        SimpleDateFormat simpleDateFormat = f732a.get();
        if (simpleDateFormat == null) {
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(str, Locale.getDefault());
            f732a.set(simpleDateFormat2);
            return simpleDateFormat2;
        }
        simpleDateFormat.applyPattern(str);
        return simpleDateFormat;
    }

    /* renamed from: b */
    public static String m1008b() {
        return m999a(System.currentTimeMillis(), m1004a());
    }

    /* renamed from: c */
    private static long m1012c() {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(14, 0);
        return instance.getTimeInMillis();
    }

    /* renamed from: b */
    public static boolean m1010b(String str) {
        return m1007a(m997a(str, m1004a()));
    }

    /* renamed from: b */
    public static boolean m1011b(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return m1006a(instance.get(1));
    }

    /* renamed from: a */
    public static String m998a(long j, @NonNull String str) {
        if (str != null) {
            return m999a(j, m1005a(str));
        }
        throw new NullPointerException("Argument 'pattern' of type String (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static String m999a(long j, @NonNull DateFormat dateFormat) {
        if (dateFormat != null) {
            return dateFormat.format(new Date(j));
        }
        throw new NullPointerException("Argument 'format' of type DateFormat (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: c */
    public static boolean m1014c(Date date) {
        return m1007a(date.getTime());
    }

    /* renamed from: a */
    public static long m996a(String str, @NonNull String str2) {
        if (str2 != null) {
            return m997a(str, m1005a(str2));
        }
        throw new NullPointerException("Argument 'pattern' of type String (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static long m997a(String str, @NonNull DateFormat dateFormat) {
        if (dateFormat != null) {
            try {
                return dateFormat.parse(str).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            throw new NullPointerException("Argument 'format' of type DateFormat (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    /* renamed from: a */
    public static String m1001a(Date date) {
        return m1003a(date, m1004a());
    }

    /* renamed from: a */
    public static String m1002a(Date date, @NonNull String str) {
        if (str != null) {
            return m1005a(str).format(date);
        }
        throw new NullPointerException("Argument 'pattern' of type String (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static String m1003a(Date date, @NonNull DateFormat dateFormat) {
        if (dateFormat != null) {
            return dateFormat.format(date);
        }
        throw new NullPointerException("Argument 'format' of type DateFormat (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static String m1000a(@NonNull DateFormat dateFormat) {
        if (dateFormat != null) {
            return m999a(System.currentTimeMillis(), dateFormat);
        }
        throw new NullPointerException("Argument 'format' of type DateFormat (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public static boolean m1007a(long j) {
        long c = m1012c();
        return j >= c && j < c + 86400000;
    }

    /* renamed from: a */
    public static boolean m1006a(int i) {
        return (i % 4 == 0 && i % 100 != 0) || i % 400 == 0;
    }
}
