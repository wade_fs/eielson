package com.blankj.utilcode.util;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.RequiresPermission;
import java.util.HashSet;
import java.util.Set;

public final class NetworkUtils {

    public static final class NetworkChangedReceiver extends BroadcastReceiver {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public C0853a f590a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public Set<C0854b> f591b = new HashSet();

        /* renamed from: com.blankj.utilcode.util.NetworkUtils$NetworkChangedReceiver$a */
        class C0852a implements Runnable {
            C0852a() {
            }

            public void run() {
                C0853a b = NetworkUtils.m857b();
                if (NetworkChangedReceiver.this.f590a != b) {
                    LogUtils.m1131b(b);
                    C0853a unused = NetworkChangedReceiver.this.f590a = b;
                    if (b == C0853a.NETWORK_NO) {
                        for (C0854b bVar : NetworkChangedReceiver.this.f591b) {
                            bVar.mo9730a();
                        }
                        return;
                    }
                    for (C0854b bVar2 : NetworkChangedReceiver.this.f591b) {
                        bVar2.mo9731a(b);
                    }
                }
            }
        }

        @SuppressLint({"MissingPermission"})
        public void onReceive(Context context, Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                Utils.m888a(new C0852a(), 1000);
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.NetworkUtils$a */
    public enum C0853a {
        NETWORK_ETHERNET,
        NETWORK_WIFI,
        NETWORK_4G,
        NETWORK_3G,
        NETWORK_2G,
        NETWORK_UNKNOWN,
        NETWORK_NO
    }

    /* renamed from: com.blankj.utilcode.util.NetworkUtils$b */
    public interface C0854b {
        /* renamed from: a */
        void mo9730a();

        /* renamed from: a */
        void mo9731a(C0853a aVar);
    }

    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    /* renamed from: a */
    private static NetworkInfo m856a() {
        ConnectivityManager connectivityManager = (ConnectivityManager) Utils.m891d().getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        return connectivityManager.getActiveNetworkInfo();
    }

    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    /* renamed from: b */
    public static C0853a m857b() {
        if (m859d()) {
            return C0853a.NETWORK_ETHERNET;
        }
        NetworkInfo a = m856a();
        if (a == null || !a.isAvailable()) {
            return C0853a.NETWORK_NO;
        }
        if (a.getType() == 1) {
            return C0853a.NETWORK_WIFI;
        }
        if (a.getType() != 0) {
            return C0853a.NETWORK_UNKNOWN;
        }
        switch (a.getSubtype()) {
            case 1:
            case 2:
            case 4:
            case 7:
            case 11:
            case 16:
                return C0853a.NETWORK_2G;
            case 3:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 12:
            case 14:
            case 15:
            case 17:
                return C0853a.NETWORK_3G;
            case 13:
            case 18:
                return C0853a.NETWORK_4G;
            default:
                String subtypeName = a.getSubtypeName();
                if (subtypeName.equalsIgnoreCase("TD-SCDMA") || subtypeName.equalsIgnoreCase("WCDMA") || subtypeName.equalsIgnoreCase("CDMA2000")) {
                    return C0853a.NETWORK_3G;
                }
                return C0853a.NETWORK_UNKNOWN;
        }
    }

    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    /* renamed from: c */
    public static boolean m858c() {
        NetworkInfo a = m856a();
        return a != null && a.isConnected();
    }

    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    /* renamed from: d */
    private static boolean m859d() {
        NetworkInfo networkInfo;
        NetworkInfo.State state;
        ConnectivityManager connectivityManager = (ConnectivityManager) Utils.m891d().getSystemService("connectivity");
        if (connectivityManager == null || (networkInfo = connectivityManager.getNetworkInfo(9)) == null || (state = networkInfo.getState()) == null) {
            return false;
        }
        if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
            return true;
        }
        return false;
    }
}
