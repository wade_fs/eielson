package com.blankj.utilcode.util;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.internal.view.SupportMenu;
import com.google.android.material.snackbar.Snackbar;
import java.lang.ref.WeakReference;

/* renamed from: com.blankj.utilcode.util.a0 */
public final class SnackbarUtils {

    /* renamed from: k */
    private static WeakReference<Snackbar> f688k;

    /* renamed from: a */
    private View f689a;

    /* renamed from: b */
    private CharSequence f690b;

    /* renamed from: c */
    private int f691c;

    /* renamed from: d */
    private int f692d;

    /* renamed from: e */
    private int f693e;

    /* renamed from: f */
    private int f694f;

    /* renamed from: g */
    private CharSequence f695g;

    /* renamed from: h */
    private int f696h;

    /* renamed from: i */
    private View.OnClickListener f697i;

    /* renamed from: j */
    private int f698j;

    private SnackbarUtils(View view) {
        m928d();
        this.f689a = view;
    }

    /* renamed from: a */
    public static SnackbarUtils m927a(@NonNull View view) {
        if (view != null) {
            return new SnackbarUtils(view);
        }
        throw new NullPointerException("Argument 'view' of type View (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: d */
    private void m928d() {
        this.f690b = "";
        this.f691c = -16777217;
        this.f692d = -16777217;
        this.f693e = -1;
        this.f694f = -1;
        this.f695g = "";
        this.f696h = -16777217;
        this.f698j = 0;
    }

    /* renamed from: b */
    public void mo9787b() {
        this.f692d = SupportMenu.CATEGORY_MASK;
        this.f691c = -1;
        this.f696h = -1;
        mo9786a();
    }

    /* renamed from: c */
    public void mo9788c() {
        this.f692d = -13912576;
        this.f691c = -1;
        this.f696h = -1;
        mo9786a();
    }

    /* renamed from: a */
    public SnackbarUtils mo9783a(@NonNull CharSequence charSequence) {
        if (charSequence != null) {
            this.f690b = charSequence;
            return this;
        }
        throw new NullPointerException("Argument 'msg' of type CharSequence (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public SnackbarUtils mo9782a(int i) {
        this.f694f = i;
        return this;
    }

    /* renamed from: a */
    public SnackbarUtils mo9785a(@NonNull CharSequence charSequence, @NonNull View.OnClickListener onClickListener) {
        if (charSequence == null) {
            throw new NullPointerException("Argument 'text' of type CharSequence (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (onClickListener != null) {
            mo9784a(charSequence, -16777217, onClickListener);
            return this;
        } else {
            throw new NullPointerException("Argument 'listener' of type View.OnClickListener (#1 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    /* renamed from: a */
    public SnackbarUtils mo9784a(@NonNull CharSequence charSequence, @ColorInt int i, @NonNull View.OnClickListener onClickListener) {
        if (charSequence == null) {
            throw new NullPointerException("Argument 'text' of type CharSequence (#0 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        } else if (onClickListener != null) {
            this.f695g = charSequence;
            this.f696h = i;
            this.f697i = onClickListener;
            return this;
        } else {
            throw new NullPointerException("Argument 'listener' of type View.OnClickListener (#2 out of 3, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    /* renamed from: a */
    public Snackbar mo9786a() {
        View view = this.f689a;
        if (view == null) {
            return null;
        }
        if (this.f691c != -16777217) {
            SpannableString spannableString = new SpannableString(this.f690b);
            spannableString.setSpan(new ForegroundColorSpan(this.f691c), 0, spannableString.length(), 33);
            f688k = new WeakReference<>(Snackbar.make(view, spannableString, this.f694f));
        } else {
            f688k = new WeakReference<>(Snackbar.make(view, this.f690b, this.f694f));
        }
        Snackbar snackbar = f688k.get();
        View view2 = snackbar.getView();
        int i = this.f693e;
        if (i != -1) {
            view2.setBackgroundResource(i);
        } else {
            int i2 = this.f692d;
            if (i2 != -16777217) {
                view2.setBackgroundColor(i2);
            }
        }
        if (this.f698j != 0) {
            ((ViewGroup.MarginLayoutParams) view2.getLayoutParams()).bottomMargin = this.f698j;
        }
        if (this.f695g.length() > 0 && this.f697i != null) {
            int i3 = this.f696h;
            if (i3 != -16777217) {
                snackbar.setActionTextColor(i3);
            }
            snackbar.setAction(this.f695g, this.f697i);
        }
        snackbar.show();
        return snackbar;
    }
}
