package com.blankj.utilcode.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/* renamed from: com.blankj.utilcode.util.u */
public final class ResourceUtils {
    /* renamed from: a */
    public static String m1237a(String str, String str2) {
        try {
            byte[] a = m1239a(Utils.m891d().getAssets().open(str));
            if (a == null) {
                return null;
            }
            if (m1238a(str2)) {
                return new String(a);
            }
            try {
                return new String(a, str2);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public static String m1240b(String str) {
        return m1237a(str, null);
    }

    /* renamed from: a */
    private static boolean m1238a(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0044 A[SYNTHETIC, Splitter:B:33:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0058 A[SYNTHETIC, Splitter:B:44:0x0058] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] m1239a(java.io.InputStream r8) {
        /*
            r0 = 0
            if (r8 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0035, all -> 0x0030 }
            r1.<init>()     // Catch:{ IOException -> 0x0035, all -> 0x0030 }
            r2 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r2]     // Catch:{ IOException -> 0x002e }
        L_0x000d:
            r4 = 0
            int r5 = r8.read(r3, r4, r2)     // Catch:{ IOException -> 0x002e }
            r6 = -1
            if (r5 == r6) goto L_0x0019
            r1.write(r3, r4, r5)     // Catch:{ IOException -> 0x002e }
            goto L_0x000d
        L_0x0019:
            byte[] r0 = r1.toByteArray()     // Catch:{ IOException -> 0x002e }
            r8.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0025
        L_0x0021:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0025:
            r1.close()     // Catch:{ IOException -> 0x0029 }
            goto L_0x002d
        L_0x0029:
            r8 = move-exception
            r8.printStackTrace()
        L_0x002d:
            return r0
        L_0x002e:
            r2 = move-exception
            goto L_0x0037
        L_0x0030:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x004e
        L_0x0035:
            r2 = move-exception
            r1 = r0
        L_0x0037:
            r2.printStackTrace()     // Catch:{ all -> 0x004d }
            r8.close()     // Catch:{ IOException -> 0x003e }
            goto L_0x0042
        L_0x003e:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0042:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x004c
        L_0x0048:
            r8 = move-exception
            r8.printStackTrace()
        L_0x004c:
            return r0
        L_0x004d:
            r0 = move-exception
        L_0x004e:
            r8.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x0056
        L_0x0052:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0056:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x005c }
            goto L_0x0060
        L_0x005c:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0060:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.ResourceUtils.m1239a(java.io.InputStream):byte[]");
    }
}
