package com.blankj.utilcode.util;

import android.content.ClipData;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import androidx.annotation.RequiresApi;
import androidx.collection.SimpleArrayMap;
import androidx.exifinterface.media.ExifInterface;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.GsonBuilder;

/* renamed from: com.blankj.utilcode.util.q */
public final class LogUtils {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final char[] f773a = {'V', 'D', 'I', 'W', 'E', 'A'};
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static final String f774b = System.getProperty("file.separator");
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final String f775c = System.getProperty("line.separator");

    /* renamed from: d */
    private static final C0910d f776d = new C0910d(null);

    /* renamed from: e */
    private static SimpleDateFormat f777e;

    /* renamed from: f */
    private static final ExecutorService f778f = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static final SimpleArrayMap<Class, C0912f> f779g = new SimpleArrayMap<>();

    /* renamed from: com.blankj.utilcode.util.q$a */
    /* compiled from: LogUtils */
    static class C0907a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ int f780P;

        /* renamed from: Q */
        final /* synthetic */ C0914h f781Q;

        /* renamed from: R */
        final /* synthetic */ String f782R;

        C0907a(int i, C0914h hVar, String str) {
            this.f780P = i;
            this.f781Q = hVar;
            this.f782R = str;
        }

        public void run() {
            int i = this.f780P;
            String str = this.f781Q.f804a;
            LogUtils.m1129b(i, str, this.f781Q.f806c + this.f782R);
        }
    }

    /* renamed from: com.blankj.utilcode.util.q$b */
    /* compiled from: LogUtils */
    static class C0908b implements FilenameFilter {
        C0908b() {
        }

        public boolean accept(File file, String str) {
            return LogUtils.m1143d(str);
        }
    }

    /* renamed from: com.blankj.utilcode.util.q$c */
    /* compiled from: LogUtils */
    static class C0909c implements Runnable {

        /* renamed from: P */
        final /* synthetic */ File f783P;

        C0909c(File file) {
            this.f783P = file;
        }

        public void run() {
            if (!this.f783P.delete()) {
                Log.e("LogUtils", "delete " + this.f783P + " failed!");
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.q$d */
    /* compiled from: LogUtils */
    public static final class C0910d {

        /* renamed from: a */
        private String f784a;

        /* renamed from: b */
        private String f785b;

        /* renamed from: c */
        private String f786c;

        /* renamed from: d */
        private String f787d;

        /* renamed from: e */
        private boolean f788e;

        /* renamed from: f */
        private boolean f789f;

        /* renamed from: g */
        private String f790g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public boolean f791h;

        /* renamed from: i */
        private boolean f792i;

        /* renamed from: j */
        private boolean f793j;

        /* renamed from: k */
        private boolean f794k;

        /* renamed from: l */
        private boolean f795l;
        /* access modifiers changed from: private */

        /* renamed from: m */
        public int f796m;
        /* access modifiers changed from: private */

        /* renamed from: n */
        public int f797n;

        /* renamed from: o */
        private int f798o;

        /* renamed from: p */
        private int f799p;

        /* renamed from: q */
        private int f800q;

        /* renamed from: r */
        private String f801r;
        /* access modifiers changed from: private */

        /* renamed from: s */
        public C0911e f802s;

        /* synthetic */ C0910d(C0907a aVar) {
            this();
        }

        /* renamed from: e */
        public final String mo9841e() {
            return this.f786c;
        }

        /* renamed from: f */
        public final String mo9842f() {
            if (LogUtils.m1146e(this.f790g)) {
                return "";
            }
            return this.f790g;
        }

        /* renamed from: g */
        public final String mo9843g() {
            String str = this.f801r;
            if (str == null) {
                return "";
            }
            return str.replace(":", "_");
        }

        /* renamed from: h */
        public final int mo9844h() {
            return this.f800q;
        }

        /* renamed from: i */
        public final int mo9845i() {
            return this.f798o;
        }

        /* renamed from: j */
        public final int mo9846j() {
            return this.f799p;
        }

        /* renamed from: k */
        public final boolean mo9847k() {
            return this.f789f;
        }

        /* renamed from: l */
        public final boolean mo9848l() {
            return this.f793j;
        }

        /* renamed from: m */
        public final boolean mo9849m() {
            return this.f794k;
        }

        /* renamed from: n */
        public final boolean mo9850n() {
            return this.f792i;
        }

        /* renamed from: o */
        public final boolean mo9851o() {
            return this.f788e;
        }

        /* renamed from: p */
        public final boolean mo9852p() {
            return this.f795l;
        }

        public String toString() {
            return "process: " + mo9843g() + LogUtils.f775c + "switch: " + mo9851o() + LogUtils.f775c + "console: " + mo9847k() + LogUtils.f775c + "tag: " + mo9842f() + LogUtils.f775c + "head: " + mo9850n() + LogUtils.f775c + "file: " + mo9848l() + LogUtils.f775c + "dir: " + mo9837b() + LogUtils.f775c + "filePrefix: " + mo9841e() + LogUtils.f775c + "border: " + mo9849m() + LogUtils.f775c + "singleTag: " + mo9852p() + LogUtils.f775c + "consoleFilter: " + mo9833a() + LogUtils.f775c + "fileFilter: " + mo9840d() + LogUtils.f775c + "stackDeep: " + mo9845i() + LogUtils.f775c + "stackOffset: " + mo9846j() + LogUtils.f775c + "saveDays: " + mo9844h() + LogUtils.f775c + "formatter: " + LogUtils.f779g;
        }

        private C0910d() {
            this.f786c = "util";
            this.f787d = ".txt";
            this.f788e = true;
            this.f789f = true;
            this.f790g = "";
            this.f791h = true;
            this.f792i = true;
            this.f793j = false;
            this.f794k = true;
            this.f795l = true;
            this.f796m = 2;
            this.f797n = 2;
            this.f798o = 1;
            this.f799p = 0;
            this.f800q = -1;
            this.f801r = Utils.m893f();
            if (this.f784a == null) {
                if (!"mounted".equals(Environment.getExternalStorageState()) || Utils.m891d().getExternalCacheDir() == null) {
                    this.f784a = Utils.m891d().getCacheDir() + LogUtils.f774b + "log" + LogUtils.f774b;
                    return;
                }
                this.f784a = Utils.m891d().getExternalCacheDir() + LogUtils.f774b + "log" + LogUtils.f774b;
            }
        }

        /* renamed from: a */
        public final C0910d mo9834a(String str) {
            if (LogUtils.m1146e(str)) {
                this.f790g = "";
                this.f791h = true;
            } else {
                this.f790g = str;
                this.f791h = false;
            }
            return this;
        }

        /* renamed from: b */
        public final C0910d mo9836b(boolean z) {
            this.f792i = z;
            return this;
        }

        /* renamed from: c */
        public final C0910d mo9838c(boolean z) {
            this.f788e = z;
            return this;
        }

        /* renamed from: d */
        public final char mo9840d() {
            return LogUtils.f773a[this.f797n - 2];
        }

        /* renamed from: b */
        public final String mo9837b() {
            String str = this.f785b;
            return str == null ? this.f784a : str;
        }

        /* renamed from: c */
        public final String mo9839c() {
            return this.f787d;
        }

        /* renamed from: a */
        public final C0910d mo9835a(boolean z) {
            this.f794k = z;
            return this;
        }

        /* renamed from: a */
        public final char mo9833a() {
            return LogUtils.f773a[this.f796m - 2];
        }
    }

    /* renamed from: com.blankj.utilcode.util.q$e */
    /* compiled from: LogUtils */
    public interface C0911e {
        /* renamed from: a */
        void mo9854a(String str, String str2);
    }

    /* renamed from: com.blankj.utilcode.util.q$f */
    /* compiled from: LogUtils */
    public static abstract class C0912f<T> {
        /* renamed from: a */
        public abstract String mo9855a(T t);
    }

    /* renamed from: com.blankj.utilcode.util.q$h */
    /* compiled from: LogUtils */
    private static final class C0914h {

        /* renamed from: a */
        String f804a;

        /* renamed from: b */
        String[] f805b;

        /* renamed from: c */
        String f806c;

        C0914h(String str, String[] strArr, String str2) {
            this.f804a = str;
            this.f805b = strArr;
            this.f806c = str2;
        }
    }

    /* renamed from: e */
    public static C0910d m1144e() {
        return f776d;
    }

    /* renamed from: f */
    private static C0914h m1147f(String str) {
        String str2;
        String str3;
        if (f776d.f791h || f776d.mo9850n()) {
            StackTraceElement[] stackTrace = new Throwable().getStackTrace();
            int j = f776d.mo9846j() + 3;
            if (j >= stackTrace.length) {
                String a = m1117a(stackTrace[3]);
                if (!f776d.f791h || !m1146e(str)) {
                    a = str;
                } else {
                    int indexOf = a.indexOf(46);
                    if (indexOf != -1) {
                        a = a.substring(0, indexOf);
                    }
                }
                return new C0914h(a, null, ": ");
            }
            StackTraceElement stackTraceElement = stackTrace[j];
            String a2 = m1117a(stackTraceElement);
            if (!f776d.f791h || !m1146e(str)) {
                str3 = str;
            } else {
                int indexOf2 = a2.indexOf(46);
                str3 = indexOf2 == -1 ? a2 : a2.substring(0, indexOf2);
            }
            if (f776d.mo9850n()) {
                String name = Thread.currentThread().getName();
                String formatter = new Formatter().format("%s, %s.%s(%s:%d)", name, stackTraceElement.getClassName(), stackTraceElement.getMethodName(), a2, Integer.valueOf(stackTraceElement.getLineNumber())).toString();
                String str4 = " [" + formatter + "]: ";
                if (f776d.mo9845i() <= 1) {
                    return new C0914h(str3, new String[]{formatter}, str4);
                }
                String[] strArr = new String[Math.min(f776d.mo9845i(), stackTrace.length - j)];
                strArr[0] = formatter;
                String formatter2 = new Formatter().format("%" + (name.length() + 2) + "s", "").toString();
                int length = strArr.length;
                for (int i = 1; i < length; i++) {
                    StackTraceElement stackTraceElement2 = stackTrace[i + j];
                    strArr[i] = new Formatter().format("%s%s.%s(%s:%d)", formatter2, stackTraceElement2.getClassName(), stackTraceElement2.getMethodName(), m1117a(stackTraceElement2), Integer.valueOf(stackTraceElement2.getLineNumber())).toString();
                }
                return new C0914h(str3, strArr, str4);
            }
            str2 = str3;
        } else {
            str2 = f776d.mo9842f();
        }
        return new C0914h(str2, null, ": ");
    }

    /* renamed from: c */
    public static void m1139c(Object... objArr) {
        m1120a(4, f776d.mo9842f(), objArr);
    }

    /* renamed from: d */
    private static void m1141d(int i, String str, String str2) {
        int length = str2.length();
        int i2 = 1100;
        int i3 = f776d.mo9849m() ? (length - 113) / 1100 : length / 1100;
        if (i3 > 0) {
            int i4 = 1;
            if (f776d.mo9849m()) {
                Log.println(i, str, str2.substring(0, 1100) + f775c + "└────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
                while (i4 < i3) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(" ");
                    sb.append(f775c);
                    sb.append("┌────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
                    sb.append(f775c);
                    sb.append("│ ");
                    int i5 = i2 + 1100;
                    sb.append(str2.substring(i2, i5));
                    sb.append(f775c);
                    sb.append("└────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
                    Log.println(i, str, sb.toString());
                    i4++;
                    i2 = i5;
                }
                if (i2 != length - 113) {
                    Log.println(i, str, " " + f775c + "┌────────────────────────────────────────────────────────────────────────────────────────────────────────────────" + f775c + "│ " + str2.substring(i2, length));
                    return;
                }
                return;
            }
            Log.println(i, str, str2.substring(0, 1100));
            while (i4 < i3) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(" ");
                sb2.append(f775c);
                int i6 = i2 + 1100;
                sb2.append(str2.substring(i2, i6));
                Log.println(i, str, sb2.toString());
                i4++;
                i2 = i6;
            }
            if (i2 != length) {
                Log.println(i, str, " " + f775c + str2.substring(i2, length));
                return;
            }
            return;
        }
        Log.println(i, str, str2);
    }

    /* renamed from: e */
    private static void m1145e(int i, String str, String str2) {
        if (!f776d.mo9849m()) {
            Log.println(i, str, str2);
            return;
        }
        String[] split = str2.split(f775c);
        for (String str3 : split) {
            Log.println(i, str, "│ " + str3);
        }
    }

    /* renamed from: b */
    public static void m1131b(Object... objArr) {
        m1120a(6, f776d.mo9842f(), objArr);
    }

    /* renamed from: c */
    private static void m1137c(int i, String str, String str2) {
        int length = str2.length();
        int i2 = length / 1100;
        if (i2 > 0) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < i2) {
                int i5 = i4 + 1100;
                m1145e(i, str, str2.substring(i4, i5));
                i3++;
                i4 = i5;
            }
            if (i4 != length) {
                m1145e(i, str, str2.substring(i4, length));
                return;
            }
            return;
        }
        m1145e(i, str, str2);
    }

    /* renamed from: com.blankj.utilcode.util.q$g */
    /* compiled from: LogUtils */
    private static final class C0913g {

        /* renamed from: a */
        private static final Gson f803a;

        static {
            GsonBuilder gVar = new GsonBuilder();
            gVar.mo23809c();
            gVar.mo23808b();
            f803a = gVar.mo23807a();
        }

        /* renamed from: a */
        static String m1178a(Object obj, int i) {
            if (obj.getClass().isArray()) {
                return m1177a(obj);
            }
            if (obj instanceof Throwable) {
                return m1180a((Throwable) obj);
            }
            if (obj instanceof Bundle) {
                return m1176a((Bundle) obj);
            }
            if (obj instanceof Intent) {
                return m1175a((Intent) obj);
            }
            if (i == 32) {
                return m1182b(obj);
            }
            if (i == 48) {
                return m1183b(obj.toString());
            }
            return obj.toString();
        }

        /* renamed from: b */
        private static String m1182b(Object obj) {
            if (obj instanceof CharSequence) {
                return m1179a(obj.toString());
            }
            try {
                return f803a.mo23794a(obj);
            } catch (Throwable unused) {
                return obj.toString();
            }
        }

        /* renamed from: c */
        static String m1184c(Object obj) {
            return m1178a(obj, -1);
        }

        /* renamed from: b */
        private static String m1183b(String str) {
            try {
                StreamSource streamSource = new StreamSource(new StringReader(str));
                StreamResult streamResult = new StreamResult(new StringWriter());
                Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
                newTransformer.setOutputProperty("indent", "yes");
                newTransformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", ExifInterface.GPS_MEASUREMENT_2D);
                newTransformer.transform(streamSource, streamResult);
                String obj = streamResult.getWriter().toString();
                return obj.replaceFirst(">", ">" + LogUtils.f775c);
            } catch (Exception e) {
                e.printStackTrace();
                return str;
            }
        }

        /* renamed from: a */
        private static String m1180a(Throwable th) {
            return ThrowableUtils.m991a(th);
        }

        /* renamed from: a */
        private static String m1176a(Bundle bundle) {
            String str;
            Iterator<String> it = bundle.keySet().iterator();
            if (!it.hasNext()) {
                return "Bundle {}";
            }
            StringBuilder sb = new StringBuilder(128);
            sb.append("Bundle { ");
            while (true) {
                String next = it.next();
                Object obj = bundle.get(next);
                sb.append(next);
                sb.append('=');
                if (obj instanceof Bundle) {
                    if (obj == bundle) {
                        str = "(this Bundle)";
                    } else {
                        str = m1176a((Bundle) obj);
                    }
                    sb.append(str);
                } else {
                    sb.append(LogUtils.m1128b(obj));
                }
                if (!it.hasNext()) {
                    sb.append(" }");
                    return sb.toString();
                }
                sb.append(',');
                sb.append(' ');
            }
        }

        /* renamed from: a */
        private static String m1175a(Intent intent) {
            boolean z;
            Intent selector;
            String str;
            ClipData clipData;
            StringBuilder sb = new StringBuilder(128);
            sb.append("Intent { ");
            String action = intent.getAction();
            boolean z2 = true;
            if (action != null) {
                sb.append("act=");
                sb.append(action);
                z = false;
            } else {
                z = true;
            }
            Set<String> categories = intent.getCategories();
            if (categories != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("cat=[");
                for (String str2 : categories) {
                    if (!z2) {
                        sb.append(',');
                    }
                    sb.append(str2);
                    z2 = false;
                }
                sb.append("]");
                z = false;
            }
            Uri data = intent.getData();
            if (data != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("dat=");
                sb.append(data);
                z = false;
            }
            String type = intent.getType();
            if (type != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("typ=");
                sb.append(type);
                z = false;
            }
            int flags = intent.getFlags();
            if (flags != 0) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("flg=0x");
                sb.append(Integer.toHexString(flags));
                z = false;
            }
            String str3 = intent.getPackage();
            if (str3 != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("pkg=");
                sb.append(str3);
                z = false;
            }
            ComponentName component = intent.getComponent();
            if (component != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("cmp=");
                sb.append(component.flattenToShortString());
                z = false;
            }
            Rect sourceBounds = intent.getSourceBounds();
            if (sourceBounds != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("bnds=");
                sb.append(sourceBounds.toShortString());
                z = false;
            }
            if (Build.VERSION.SDK_INT >= 16 && (clipData = intent.getClipData()) != null) {
                if (!z) {
                    sb.append(' ');
                }
                m1181a(clipData, sb);
                z = false;
            }
            Bundle extras = intent.getExtras();
            if (extras != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("extras={");
                sb.append(m1176a(extras));
                sb.append('}');
                z = false;
            }
            if (Build.VERSION.SDK_INT >= 15 && (selector = intent.getSelector()) != null) {
                if (!z) {
                    sb.append(' ');
                }
                sb.append("sel={");
                if (selector == intent) {
                    str = "(this Intent)";
                } else {
                    str = m1175a(selector);
                }
                sb.append(str);
                sb.append("}");
            }
            sb.append(" }");
            return sb.toString();
        }

        @RequiresApi(api = 16)
        /* renamed from: a */
        private static void m1181a(ClipData clipData, StringBuilder sb) {
            ClipData.Item itemAt = clipData.getItemAt(0);
            if (itemAt == null) {
                sb.append("ClipData.Item {}");
                return;
            }
            sb.append("ClipData.Item { ");
            String htmlText = itemAt.getHtmlText();
            if (htmlText != null) {
                sb.append("H:");
                sb.append(htmlText);
                sb.append("}");
                return;
            }
            CharSequence text = itemAt.getText();
            if (text != null) {
                sb.append("T:");
                sb.append(text);
                sb.append("}");
                return;
            }
            Uri uri = itemAt.getUri();
            if (uri != null) {
                sb.append("U:");
                sb.append(uri);
                sb.append("}");
                return;
            }
            Intent intent = itemAt.getIntent();
            if (intent != null) {
                sb.append("I:");
                sb.append(m1175a(intent));
                sb.append("}");
                return;
            }
            sb.append("NULL");
            sb.append("}");
        }

        /* renamed from: a */
        private static String m1179a(String str) {
            try {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt == '{') {
                        return new JSONObject(str).toString(2);
                    }
                    if (charAt == '[') {
                        return new JSONArray(str).toString(2);
                    }
                    if (!Character.isWhitespace(charAt)) {
                        return str;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return str;
        }

        /* renamed from: a */
        private static String m1177a(Object obj) {
            if (obj instanceof Object[]) {
                return Arrays.deepToString((Object[]) obj);
            }
            if (obj instanceof boolean[]) {
                return Arrays.toString((boolean[]) obj);
            }
            if (obj instanceof byte[]) {
                return Arrays.toString((byte[]) obj);
            }
            if (obj instanceof char[]) {
                return Arrays.toString((char[]) obj);
            }
            if (obj instanceof double[]) {
                return Arrays.toString((double[]) obj);
            }
            if (obj instanceof float[]) {
                return Arrays.toString((float[]) obj);
            }
            if (obj instanceof int[]) {
                return Arrays.toString((int[]) obj);
            }
            if (obj instanceof long[]) {
                return Arrays.toString((long[]) obj);
            }
            if (obj instanceof short[]) {
                return Arrays.toString((short[]) obj);
            }
            throw new IllegalArgumentException("Array has incompatible type: " + obj.getClass());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static String m1128b(Object obj) {
        C0912f fVar;
        if (obj == null) {
            return "null";
        }
        if (f779g.isEmpty() || (fVar = f779g.get(m1134c(obj))) == null) {
            return C0913g.m1184c(obj);
        }
        return fVar.mo9855a(obj);
    }

    /* renamed from: a */
    public static void m1123a(Object... objArr) {
        m1120a(3, f776d.mo9842f(), objArr);
    }

    /* renamed from: a */
    public static void m1120a(int i, String str, Object... objArr) {
        if (f776d.mo9851o()) {
            int i2 = i & 15;
            int i3 = i & PsExtractor.VIDEO_STREAM_MASK;
            if (!f776d.mo9847k() && !f776d.mo9848l() && i3 != 16) {
                return;
            }
            if (i2 >= f776d.f796m || i2 >= f776d.f797n) {
                C0914h f = m1147f(str);
                String a = m1115a(i3, objArr);
                if (f776d.mo9847k() && i3 != 16 && i2 >= f776d.f796m) {
                    m1122a(i2, f.f804a, f.f805b, a);
                }
                if ((f776d.mo9848l() || i3 == 16) && i2 >= f776d.f797n) {
                    f778f.execute(new C0907a(i2, f, a));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static boolean m1146e(String str) {
        if (str == null) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    private static String m1127b(int i, String str, String[] strArr, String str2) {
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        if (f776d.mo9849m()) {
            sb.append(" ");
            sb.append(f775c);
            sb.append("┌────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
            sb.append(f775c);
            if (strArr != null) {
                for (String str3 : strArr) {
                    sb.append("│ ");
                    sb.append(str3);
                    sb.append(f775c);
                }
                sb.append("├┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄");
                sb.append(f775c);
            }
            String[] split = str2.split(f775c);
            int length = split.length;
            while (i2 < length) {
                String str4 = split[i2];
                sb.append("│ ");
                sb.append(str4);
                sb.append(f775c);
                i2++;
            }
            sb.append("└────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
        } else {
            if (strArr != null) {
                sb.append(" ");
                sb.append(f775c);
                int length2 = strArr.length;
                while (i2 < length2) {
                    sb.append(strArr[i2]);
                    sb.append(f775c);
                    i2++;
                }
            }
            sb.append(str2);
        }
        return sb.toString();
    }

    /* renamed from: c */
    private static String m1136c(String str) {
        Matcher matcher = Pattern.compile("[0-9]{4}_[0-9]{2}_[0-9]{2}").matcher(str);
        return matcher.find() ? matcher.group() : "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0050 A[SYNTHETIC, Splitter:B:20:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0056 A[SYNTHETIC, Splitter:B:23:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m1138c(java.lang.String r5, java.lang.String r6) {
        /*
            com.blankj.utilcode.util.q$d r0 = com.blankj.utilcode.util.LogUtils.f776d
            com.blankj.utilcode.util.q$e r0 = r0.f802s
            if (r0 != 0) goto L_0x005f
            r0 = 0
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x002f }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x002f }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002f }
            r4 = 1
            r3.<init>(r6, r4)     // Catch:{ IOException -> 0x002f }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r3, r4)     // Catch:{ IOException -> 0x002f }
            r1.<init>(r2)     // Catch:{ IOException -> 0x002f }
            r1.write(r5)     // Catch:{ IOException -> 0x002a, all -> 0x0027 }
            r1.close()     // Catch:{ IOException -> 0x0022 }
            goto L_0x0068
        L_0x0022:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0068
        L_0x0027:
            r5 = move-exception
            r0 = r1
            goto L_0x0054
        L_0x002a:
            r5 = move-exception
            r0 = r1
            goto L_0x0030
        L_0x002d:
            r5 = move-exception
            goto L_0x0054
        L_0x002f:
            r5 = move-exception
        L_0x0030:
            r5.printStackTrace()     // Catch:{ all -> 0x002d }
            java.lang.String r5 = "LogUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x002d }
            r1.<init>()     // Catch:{ all -> 0x002d }
            java.lang.String r2 = "log to "
            r1.append(r2)     // Catch:{ all -> 0x002d }
            r1.append(r6)     // Catch:{ all -> 0x002d }
            java.lang.String r6 = " failed!"
            r1.append(r6)     // Catch:{ all -> 0x002d }
            java.lang.String r6 = r1.toString()     // Catch:{ all -> 0x002d }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x002d }
            if (r0 == 0) goto L_0x0068
            r0.close()     // Catch:{ IOException -> 0x0022 }
            goto L_0x0068
        L_0x0054:
            if (r0 == 0) goto L_0x005e
            r0.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x005e
        L_0x005a:
            r6 = move-exception
            r6.printStackTrace()
        L_0x005e:
            throw r5
        L_0x005f:
            com.blankj.utilcode.util.q$d r0 = com.blankj.utilcode.util.LogUtils.f776d
            com.blankj.utilcode.util.q$e r0 = r0.f802s
            r0.mo9854a(r6, r5)
        L_0x0068:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.LogUtils.m1138c(java.lang.String, java.lang.String):void");
    }

    /* renamed from: a */
    private static String m1117a(StackTraceElement stackTraceElement) {
        String fileName = stackTraceElement.getFileName();
        if (fileName != null) {
            return fileName;
        }
        String className = stackTraceElement.getClassName();
        String[] split = className.split("\\.");
        if (split.length > 0) {
            className = split[split.length - 1];
        }
        int indexOf = className.indexOf(36);
        if (indexOf != -1) {
            className = className.substring(0, indexOf);
        }
        return className + ".java";
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static boolean m1143d(String str) {
        return str.matches("^" + f776d.mo9841e() + "_[0-9]{4}_[0-9]{2}_[0-9]{2}_.*$");
    }

    /* renamed from: d */
    private static void m1142d(String str, String str2) {
        String str3 = "";
        int i = 0;
        try {
            PackageInfo packageInfo = Utils.m891d().getPackageManager().getPackageInfo(Utils.m891d().getPackageName(), 0);
            if (packageInfo != null) {
                str3 = packageInfo.versionName;
                i = packageInfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        m1138c("************* Log Head ****************\nDate of Log        : " + str2 + "\nDevice Manufacturer: " + Build.MANUFACTURER + "\nDevice Model       : " + Build.MODEL + "\nAndroid Version    : " + Build.VERSION.RELEASE + "\nAndroid SDK        : " + Build.VERSION.SDK_INT + "\nApp VersionName    : " + str3 + "\nApp VersionCode    : " + i + "\n************* Log Head ****************\n\n", str);
    }

    /* renamed from: a */
    private static String m1115a(int i, Object... objArr) {
        String str;
        if (objArr != null) {
            if (objArr.length == 1) {
                str = m1114a(i, objArr[0]);
            } else {
                StringBuilder sb = new StringBuilder();
                int length = objArr.length;
                for (int i2 = 0; i2 < length; i2++) {
                    Object obj = objArr[i2];
                    sb.append("args");
                    sb.append("[");
                    sb.append(i2);
                    sb.append("]");
                    sb.append(" = ");
                    sb.append(m1128b(obj));
                    sb.append(f775c);
                }
                str = sb.toString();
            }
        } else {
            str = "null";
        }
        return str.length() == 0 ? "log nothing" : str;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m1129b(int i, String str, String str2) {
        String format = m1148f().format(new Date());
        String substring = format.substring(0, 10);
        String substring2 = format.substring(11);
        String str3 = f776d.mo9837b() + f776d.mo9841e() + "_" + substring + "_" + f776d.mo9843g() + f776d.mo9839c();
        if (!m1126a(str3, substring)) {
            Log.e("LogUtils", "create " + str3 + " failed!");
            return;
        }
        m1138c(substring2 + f773a[i - 2] + "/" + str + str2 + f775c, str3);
    }

    /* renamed from: c */
    private static Class m1134c(Object obj) {
        String str;
        Class<?> cls = obj.getClass();
        if (cls.isAnonymousClass() || cls.isSynthetic()) {
            Type[] genericInterfaces = cls.getGenericInterfaces();
            if (genericInterfaces.length == 1) {
                Type type = genericInterfaces[0];
                while (type instanceof ParameterizedType) {
                    type = ((ParameterizedType) type).getRawType();
                }
                str = type.toString();
            } else {
                Type genericSuperclass = cls.getGenericSuperclass();
                while (genericSuperclass instanceof ParameterizedType) {
                    genericSuperclass = ((ParameterizedType) genericSuperclass).getRawType();
                }
                str = genericSuperclass.toString();
            }
            if (str.startsWith("class ")) {
                str = str.substring(6);
            } else if (str.startsWith("interface ")) {
                str = str.substring(10);
            }
            try {
                return Class.forName(str);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return cls;
    }

    /* renamed from: b */
    private static void m1130b(String str, String str2) {
        File[] listFiles;
        if (f776d.mo9844h() > 0 && (listFiles = new File(str).getParentFile().listFiles(new C0908b())) != null && listFiles.length > 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd", Locale.getDefault());
            try {
                long time = simpleDateFormat.parse(str2).getTime() - (((long) f776d.mo9844h()) * 86400000);
                for (File file : listFiles) {
                    String name = file.getName();
                    name.length();
                    if (simpleDateFormat.parse(m1136c(name)).getTime() <= time) {
                        f778f.execute(new C0909c(file));
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    private static String m1114a(int i, Object obj) {
        if (obj == null) {
            return "null";
        }
        if (i == 32) {
            return C0913g.m1178a(obj, 32);
        }
        if (i == 48) {
            return C0913g.m1178a(obj, 48);
        }
        return m1128b(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.q.a(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.q.a(int, java.lang.String, java.lang.String):void
      com.blankj.utilcode.util.q.a(int, java.lang.String, java.lang.Object[]):void
      com.blankj.utilcode.util.q.a(int, java.lang.String, java.lang.String[]):void
      com.blankj.utilcode.util.q.a(int, java.lang.String, boolean):void */
    /* renamed from: a */
    private static void m1122a(int i, String str, String[] strArr, String str2) {
        if (f776d.mo9852p()) {
            m1141d(i, str, m1127b(i, str, strArr, str2));
            return;
        }
        m1119a(i, str, true);
        m1121a(i, str, strArr);
        m1137c(i, str, str2);
        m1119a(i, str, false);
    }

    /* renamed from: f */
    private static SimpleDateFormat m1148f() {
        if (f777e == null) {
            f777e = new SimpleDateFormat("yyyy_MM_dd HH:mm:ss.SSS ", Locale.getDefault());
        }
        return f777e;
    }

    /* renamed from: a */
    private static void m1119a(int i, String str, boolean z) {
        if (f776d.mo9849m()) {
            Log.println(i, str, z ? "┌────────────────────────────────────────────────────────────────────────────────────────────────────────────────" : "└────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
        }
    }

    /* renamed from: a */
    private static void m1121a(int i, String str, String[] strArr) {
        if (strArr != null) {
            for (String str2 : strArr) {
                if (f776d.mo9849m()) {
                    str2 = "│ " + str2;
                }
                Log.println(i, str, str2);
            }
            if (f776d.mo9849m()) {
                Log.println(i, str, "├┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄");
            }
        }
    }

    /* renamed from: a */
    private static boolean m1126a(String str, String str2) {
        File file = new File(str);
        if (file.exists()) {
            return file.isFile();
        }
        if (!m1124a(file.getParentFile())) {
            return false;
        }
        try {
            m1130b(str, str2);
            boolean createNewFile = file.createNewFile();
            if (createNewFile) {
                m1142d(str, str2);
            }
            return createNewFile;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: a */
    private static boolean m1124a(File file) {
        return file != null && (!file.exists() ? file.mkdirs() : file.isDirectory());
    }
}
