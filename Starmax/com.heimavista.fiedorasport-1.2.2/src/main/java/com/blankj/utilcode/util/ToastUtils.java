package com.blankj.utilcode.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.NotificationManagerCompat;
import com.blankj.utilcode.util.Utils;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import java.lang.reflect.Field;

/* renamed from: com.blankj.utilcode.util.f0 */
public final class ToastUtils {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static C0894c f746a = null;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static int f747b = -1;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static int f748c = -1;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static int f749d = -1;

    /* renamed from: e */
    private static int f750e = -16777217;

    /* renamed from: f */
    private static int f751f = -1;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static int f752g = -16777217;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static int f753h = -1;

    /* renamed from: com.blankj.utilcode.util.f0$a */
    /* compiled from: ToastUtils */
    static class C0892a implements Runnable {

        /* renamed from: P */
        final /* synthetic */ CharSequence f754P;

        /* renamed from: Q */
        final /* synthetic */ int f755Q;

        C0892a(CharSequence charSequence, int i) {
            this.f754P = charSequence;
            this.f755Q = i;
        }

        @SuppressLint({"ShowToast"})
        public void run() {
            ToastUtils.m1037g();
            C0894c unused = ToastUtils.f746a = C0897e.m1041b(Utils.m891d(), this.f754P, this.f755Q);
            View view = ToastUtils.f746a.getView();
            if (view != null) {
                TextView textView = (TextView) view.findViewById(16908299);
                if (ToastUtils.f752g != -16777217) {
                    textView.setTextColor(ToastUtils.f752g);
                }
                if (ToastUtils.f753h != -1) {
                    textView.setTextSize((float) ToastUtils.f753h);
                }
                if (!(ToastUtils.f747b == -1 && ToastUtils.f748c == -1 && ToastUtils.f749d == -1)) {
                    ToastUtils.f746a.mo9817a(ToastUtils.f747b, ToastUtils.f748c, ToastUtils.f749d);
                }
                ToastUtils.m1032b(textView);
                ToastUtils.f746a.show();
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.f0$b */
    /* compiled from: ToastUtils */
    static abstract class C0893b implements C0894c {

        /* renamed from: a */
        Toast f756a;

        C0893b(Toast toast) {
            this.f756a = toast;
        }

        /* renamed from: a */
        public void mo9817a(int i, int i2, int i3) {
            this.f756a.setGravity(i, i2, i3);
        }

        public View getView() {
            return this.f756a.getView();
        }
    }

    /* renamed from: com.blankj.utilcode.util.f0$c */
    /* compiled from: ToastUtils */
    interface C0894c {
        /* renamed from: a */
        void mo9817a(int i, int i2, int i3);

        void cancel();

        View getView();

        void show();
    }

    /* renamed from: com.blankj.utilcode.util.f0$d */
    /* compiled from: ToastUtils */
    static class C0895d extends C0893b {

        /* renamed from: com.blankj.utilcode.util.f0$d$a */
        /* compiled from: ToastUtils */
        static class C0896a extends Handler {

            /* renamed from: a */
            private Handler f757a;

            C0896a(Handler handler) {
                this.f757a = super;
            }

            public void dispatchMessage(Message message) {
                try {
                    this.f757a.dispatchMessage(message);
                } catch (Exception e) {
                    Log.e("ToastUtils", e.toString());
                }
            }

            public void handleMessage(Message message) {
                this.f757a.handleMessage(message);
            }
        }

        C0895d(Toast toast) {
            super(toast);
            if (Build.VERSION.SDK_INT == 25) {
                try {
                    Field declaredField = Toast.class.getDeclaredField("mTN");
                    declaredField.setAccessible(true);
                    Object obj = declaredField.get(toast);
                    Field declaredField2 = declaredField.getType().getDeclaredField("mHandler");
                    declaredField2.setAccessible(true);
                    declaredField2.set(obj, new C0896a((Handler) declaredField2.get(obj)));
                } catch (Exception unused) {
                }
            }
        }

        public void cancel() {
            super.f756a.cancel();
        }

        public void show() {
            super.f756a.show();
        }
    }

    /* renamed from: com.blankj.utilcode.util.f0$e */
    /* compiled from: ToastUtils */
    static class C0897e {
        /* renamed from: a */
        private static Toast m1040a(Context context, CharSequence charSequence, int i) {
            Toast makeText = Toast.makeText(context, "", i);
            makeText.setText(charSequence);
            return makeText;
        }

        /* renamed from: b */
        static C0894c m1041b(Context context, CharSequence charSequence, int i) {
            if (NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                return new C0895d(m1040a(context, charSequence, i));
            }
            return new C0898f(m1040a(context, charSequence, i));
        }
    }

    /* renamed from: com.blankj.utilcode.util.f0$f */
    /* compiled from: ToastUtils */
    static class C0898f extends C0893b {

        /* renamed from: e */
        private static final Utils.C0871d f758e = new C0899a();

        /* renamed from: b */
        private View f759b;

        /* renamed from: c */
        private WindowManager f760c;

        /* renamed from: d */
        private WindowManager.LayoutParams f761d = new WindowManager.LayoutParams();

        /* renamed from: com.blankj.utilcode.util.f0$f$a */
        /* compiled from: ToastUtils */
        static class C0899a implements Utils.C0871d {
            C0899a() {
            }

            public void onActivityDestroyed(Activity activity) {
                if (ToastUtils.f746a != null) {
                    activity.getWindow().getDecorView().setVisibility(8);
                    ToastUtils.f746a.cancel();
                }
            }
        }

        /* renamed from: com.blankj.utilcode.util.f0$f$b */
        /* compiled from: ToastUtils */
        class C0900b implements Runnable {
            C0900b() {
            }

            public void run() {
                C0898f.this.m1042a();
            }
        }

        /* renamed from: com.blankj.utilcode.util.f0$f$c */
        /* compiled from: ToastUtils */
        class C0901c implements Runnable {
            C0901c() {
            }

            public void run() {
                C0898f.this.cancel();
            }
        }

        C0898f(Toast toast) {
            super(toast);
        }

        public void cancel() {
            try {
                if (this.f760c != null) {
                    this.f760c.removeViewImmediate(this.f759b);
                }
            } catch (Exception unused) {
            }
            this.f759b = null;
            this.f760c = null;
            super.f756a = null;
        }

        public void show() {
            Utils.m888a(new C0900b(), 300);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1042a() {
            Toast toast = super.f756a;
            if (toast != null) {
                this.f759b = toast.getView();
                if (this.f759b != null) {
                    Context context = super.f756a.getView().getContext();
                    if (Build.VERSION.SDK_INT < 25) {
                        this.f760c = (WindowManager) context.getSystemService("window");
                        this.f761d.type = 2005;
                    } else {
                        Context k = Utils.m898k();
                        if (!(k instanceof Activity)) {
                            Log.e("ToastUtils", "Couldn't get top Activity.");
                            return;
                        }
                        Activity activity = (Activity) k;
                        if (activity.isFinishing() || activity.isDestroyed()) {
                            Log.e("ToastUtils", activity + " is useless");
                            return;
                        }
                        this.f760c = activity.getWindowManager();
                        this.f761d.type = 99;
                        Utils.m889b().mo9769a(activity, f758e);
                    }
                    WindowManager.LayoutParams layoutParams = this.f761d;
                    layoutParams.height = -2;
                    layoutParams.width = -2;
                    layoutParams.format = -3;
                    layoutParams.windowAnimations = 16973828;
                    layoutParams.setTitle("ToastWithoutNotification");
                    WindowManager.LayoutParams layoutParams2 = this.f761d;
                    layoutParams2.flags = 152;
                    layoutParams2.packageName = Utils.m891d().getPackageName();
                    this.f761d.gravity = super.f756a.getGravity();
                    WindowManager.LayoutParams layoutParams3 = this.f761d;
                    if ((layoutParams3.gravity & 7) == 7) {
                        layoutParams3.horizontalWeight = 1.0f;
                    }
                    WindowManager.LayoutParams layoutParams4 = this.f761d;
                    if ((layoutParams4.gravity & 112) == 112) {
                        layoutParams4.verticalWeight = 1.0f;
                    }
                    this.f761d.x = super.f756a.getXOffset();
                    this.f761d.y = super.f756a.getYOffset();
                    this.f761d.horizontalMargin = super.f756a.getHorizontalMargin();
                    this.f761d.verticalMargin = super.f756a.getVerticalMargin();
                    try {
                        if (this.f760c != null) {
                            this.f760c.addView(this.f759b, this.f761d);
                        }
                    } catch (Exception unused) {
                    }
                    Utils.m888a(new C0901c(), super.f756a.getDuration() == 0 ? AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS : 3500);
                }
            }
        }
    }

    /* renamed from: g */
    public static void m1037g() {
        C0894c cVar = f746a;
        if (cVar != null) {
            cVar.cancel();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m1032b(TextView textView) {
        if (f751f != -1) {
            f746a.getView().setBackgroundResource(f751f);
            textView.setBackgroundColor(0);
        } else if (f750e != -16777217) {
            View view = f746a.getView();
            Drawable background = view.getBackground();
            Drawable background2 = textView.getBackground();
            if (background != null && background2 != null) {
                background.setColorFilter(new PorterDuffColorFilter(f750e, PorterDuff.Mode.SRC_IN));
                textView.setBackgroundColor(0);
            } else if (background != null) {
                background.setColorFilter(new PorterDuffColorFilter(f750e, PorterDuff.Mode.SRC_IN));
            } else if (background2 != null) {
                background2.setColorFilter(new PorterDuffColorFilter(f750e, PorterDuff.Mode.SRC_IN));
            } else {
                view.setBackgroundColor(f750e);
            }
        }
    }

    /* renamed from: a */
    public static void m1029a(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "null";
        }
        m1030a(charSequence, 0);
    }

    /* renamed from: a */
    private static void m1030a(CharSequence charSequence, int i) {
        Utils.m887a(new C0892a(charSequence, i));
    }
}
