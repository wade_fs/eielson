package com.blankj.utilcode.util;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import java.lang.Thread;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.blankj.utilcode.util.c0 */
public final class ThreadUtils {

    /* renamed from: a */
    private static final Map<Integer, Map<Integer, ExecutorService>> f700a = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static final Map<C0877e, C0882f> f701b = new ConcurrentHashMap();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final int f702c = Runtime.getRuntime().availableProcessors();

    /* renamed from: d */
    private static final Timer f703d = new Timer();

    /* renamed from: e */
    private static Executor f704e;

    /* renamed from: com.blankj.utilcode.util.c0$a */
    /* compiled from: ThreadUtils */
    static class C0873a extends TimerTask {

        /* renamed from: P */
        final /* synthetic */ ExecutorService f705P;

        /* renamed from: Q */
        final /* synthetic */ C0877e f706Q;

        C0873a(ExecutorService executorService, C0877e eVar) {
            this.f705P = executorService;
            this.f706Q = eVar;
        }

        public void run() {
            this.f705P.execute(this.f706Q);
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$b */
    /* compiled from: ThreadUtils */
    static class C0874b extends TimerTask {

        /* renamed from: P */
        final /* synthetic */ ExecutorService f707P;

        /* renamed from: Q */
        final /* synthetic */ C0877e f708Q;

        C0874b(ExecutorService executorService, C0877e eVar) {
            this.f707P = executorService;
            this.f708Q = eVar;
        }

        public void run() {
            this.f707P.execute(this.f708Q);
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$c */
    /* compiled from: ThreadUtils */
    static class C0875c implements Executor {

        /* renamed from: a */
        private final Handler f709a = new Handler(Looper.getMainLooper());

        C0875c() {
        }

        public void execute(@NonNull Runnable runnable) {
            if (runnable != null) {
                this.f709a.post(runnable);
                return;
            }
            throw new NullPointerException("Argument 'command' of type Runnable (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$d */
    /* compiled from: ThreadUtils */
    private static final class C0876d extends LinkedBlockingQueue<Runnable> {
        /* access modifiers changed from: private */

        /* renamed from: P */
        public volatile C0883g f710P;

        /* renamed from: Q */
        private int f711Q = Integer.MAX_VALUE;

        C0876d() {
        }

        /* renamed from: a */
        public boolean offer(@NonNull Runnable runnable) {
            if (runnable == null) {
                throw new NullPointerException("Argument 'runnable' of type Runnable (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            } else if (this.f711Q > size() || this.f710P == null || this.f710P.getPoolSize() >= this.f710P.getMaximumPoolSize()) {
                return super.offer(runnable);
            } else {
                return false;
            }
        }

        C0876d(boolean z) {
            if (z) {
                this.f711Q = 0;
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$e */
    /* compiled from: ThreadUtils */
    public static abstract class C0877e<T> implements Runnable {

        /* renamed from: P */
        private final AtomicInteger f712P = new AtomicInteger(0);

        /* renamed from: Q */
        private volatile boolean f713Q;

        /* renamed from: R */
        private volatile Thread f714R;

        /* renamed from: S */
        private Timer f715S;

        /* renamed from: T */
        private Executor f716T;

        /* renamed from: com.blankj.utilcode.util.c0$e$a */
        /* compiled from: ThreadUtils */
        class C0878a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Object f717P;

            C0878a(Object obj) {
                this.f717P = obj;
            }

            public void run() {
                C0877e.this.mo9795a(this.f717P);
            }
        }

        /* renamed from: com.blankj.utilcode.util.c0$e$b */
        /* compiled from: ThreadUtils */
        class C0879b implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Object f719P;

            C0879b(Object obj) {
                this.f719P = obj;
            }

            public void run() {
                C0877e.this.mo9795a(this.f719P);
                C0877e.this.mo9800d();
            }
        }

        /* renamed from: com.blankj.utilcode.util.c0$e$c */
        /* compiled from: ThreadUtils */
        class C0880c implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Throwable f721P;

            C0880c(Throwable th) {
                this.f721P = th;
            }

            public void run() {
                C0877e.this.mo9796a(this.f721P);
                C0877e.this.mo9800d();
            }
        }

        /* renamed from: com.blankj.utilcode.util.c0$e$d */
        /* compiled from: ThreadUtils */
        class C0881d implements Runnable {
            C0881d() {
            }

            public void run() {
                C0877e.this.mo9799c();
                C0877e.this.mo9800d();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m969b(boolean z) {
            this.f713Q = z;
        }

        /* renamed from: e */
        private Executor m970e() {
            Executor executor = this.f716T;
            return executor == null ? ThreadUtils.m965e() : executor;
        }

        /* renamed from: a */
        public abstract void mo9795a(T t);

        /* renamed from: a */
        public abstract void mo9796a(Throwable th);

        /* renamed from: b */
        public abstract T mo9798b();

        /* renamed from: c */
        public abstract void mo9799c();

        /* access modifiers changed from: protected */
        @CallSuper
        /* renamed from: d */
        public void mo9800d() {
            ThreadUtils.f701b.remove(this);
            Timer timer = this.f715S;
            if (timer != null) {
                timer.cancel();
                this.f715S = null;
            }
        }

        public void run() {
            if (this.f713Q) {
                if (this.f714R == null) {
                    if (this.f712P.compareAndSet(0, 1)) {
                        this.f714R = Thread.currentThread();
                    } else {
                        return;
                    }
                } else if (this.f712P.get() != 1) {
                    return;
                }
            } else if (this.f712P.compareAndSet(0, 1)) {
                this.f714R = Thread.currentThread();
            } else {
                return;
            }
            try {
                Object b = mo9798b();
                if (this.f713Q) {
                    if (this.f712P.get() == 1) {
                        m970e().execute(new C0878a(b));
                    }
                } else if (this.f712P.compareAndSet(1, 3)) {
                    m970e().execute(new C0879b(b));
                }
            } catch (InterruptedException unused) {
                this.f712P.compareAndSet(4, 5);
            } catch (Throwable th) {
                if (this.f712P.compareAndSet(1, 2)) {
                    m970e().execute(new C0880c(th));
                }
            }
        }

        /* renamed from: a */
        public void mo9794a() {
            mo9797a(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
            if (r3.f714R == null) goto L_0x0020;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
            r3.f714R.interrupt();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
            m970e().execute(new com.blankj.utilcode.util.ThreadUtils.C0877e.C0881d(r3));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
            if (r4 == false) goto L_0x0020;
         */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo9797a(boolean r4) {
            /*
                r3 = this;
                java.util.concurrent.atomic.AtomicInteger r0 = r3.f712P
                monitor-enter(r0)
                java.util.concurrent.atomic.AtomicInteger r1 = r3.f712P     // Catch:{ all -> 0x002d }
                int r1 = r1.get()     // Catch:{ all -> 0x002d }
                r2 = 1
                if (r1 <= r2) goto L_0x000e
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                return
            L_0x000e:
                java.util.concurrent.atomic.AtomicInteger r1 = r3.f712P     // Catch:{ all -> 0x002d }
                r2 = 4
                r1.set(r2)     // Catch:{ all -> 0x002d }
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                if (r4 == 0) goto L_0x0020
                java.lang.Thread r4 = r3.f714R
                if (r4 == 0) goto L_0x0020
                java.lang.Thread r4 = r3.f714R
                r4.interrupt()
            L_0x0020:
                java.util.concurrent.Executor r4 = r3.m970e()
                com.blankj.utilcode.util.c0$e$d r0 = new com.blankj.utilcode.util.c0$e$d
                r0.<init>()
                r4.execute(r0)
                return
            L_0x002d:
                r4 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.ThreadUtils.C0877e.mo9797a(boolean):void");
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$f */
    /* compiled from: ThreadUtils */
    private static class C0882f {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public TimerTask f724a;

        /* synthetic */ C0882f(ExecutorService executorService, C0873a aVar) {
            this(executorService);
        }

        private C0882f(ExecutorService executorService) {
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$g */
    /* compiled from: ThreadUtils */
    static final class C0883g extends ThreadPoolExecutor {

        /* renamed from: a */
        private final AtomicInteger f725a = new AtomicInteger();

        /* renamed from: b */
        private C0876d f726b;

        C0883g(int i, int i2, long j, TimeUnit timeUnit, C0876d dVar, ThreadFactory threadFactory) {
            super(i, i2, j, timeUnit, dVar, threadFactory);
            C0883g unused = dVar.f710P = this;
            this.f726b = dVar;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static ExecutorService m980b(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (i3 == -8) {
                return new C0883g(ThreadUtils.f702c + 1, (ThreadUtils.f702c * 2) + 1, 30, TimeUnit.SECONDS, new C0876d(true), new C0884h("cpu", i4));
            } else if (i3 == -4) {
                return new C0883g((ThreadUtils.f702c * 2) + 1, (ThreadUtils.f702c * 2) + 1, 30, TimeUnit.SECONDS, new C0876d(), new C0884h("io", i4));
            } else {
                if (i3 == -2) {
                    return new C0883g(0, 128, 60, TimeUnit.SECONDS, new C0876d(true), new C0884h("cached", i4));
                } else if (i3 == -1) {
                    return new C0883g(1, 1, 0, TimeUnit.MILLISECONDS, new C0876d(), new C0884h("single", i4));
                } else {
                    TimeUnit timeUnit = TimeUnit.MILLISECONDS;
                    C0876d dVar = new C0876d();
                    return new C0883g(i, i, 0, timeUnit, dVar, new C0884h("fixed(" + i3 + ")", i4));
                }
            }
        }

        /* access modifiers changed from: protected */
        public void afterExecute(Runnable runnable, Throwable th) {
            this.f725a.decrementAndGet();
            super.afterExecute(runnable, th);
        }

        public void execute(@NonNull Runnable runnable) {
            if (runnable == null) {
                throw new NullPointerException("Argument 'command' of type Runnable (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            } else if (!isShutdown()) {
                this.f725a.incrementAndGet();
                try {
                    super.execute(runnable);
                } catch (RejectedExecutionException unused) {
                    Log.e("ThreadUtils", "This will not happen!");
                    this.f726b.offer(runnable);
                } catch (Throwable unused2) {
                    this.f725a.decrementAndGet();
                }
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.c0$h */
    /* compiled from: ThreadUtils */
    private static final class C0884h extends AtomicLong implements ThreadFactory {

        /* renamed from: S */
        private static final AtomicInteger f727S = new AtomicInteger(1);
        private static final long serialVersionUID = -9209200509960368598L;

        /* renamed from: P */
        private final String f728P;

        /* renamed from: Q */
        private final int f729Q;

        /* renamed from: R */
        private final boolean f730R;

        /* renamed from: com.blankj.utilcode.util.c0$h$a */
        /* compiled from: ThreadUtils */
        class C0885a extends Thread {
            C0885a(C0884h hVar, Runnable runnable, String str) {
                super(runnable, str);
            }

            public void run() {
                try {
                    super.run();
                } catch (Throwable th) {
                    Log.e("ThreadUtils", "Request threw uncaught throwable", th);
                }
            }
        }

        /* renamed from: com.blankj.utilcode.util.c0$h$b */
        /* compiled from: ThreadUtils */
        class C0886b implements Thread.UncaughtExceptionHandler {
            C0886b(C0884h hVar) {
            }

            public void uncaughtException(Thread thread, Throwable th) {
                System.out.println(th);
            }
        }

        C0884h(String str, int i) {
            this(str, i, false);
        }

        public Thread newThread(@NonNull Runnable runnable) {
            if (runnable != null) {
                C0885a aVar = new C0885a(this, runnable, this.f728P + getAndIncrement());
                aVar.setDaemon(this.f730R);
                aVar.setUncaughtExceptionHandler(new C0886b(this));
                aVar.setPriority(this.f729Q);
                return aVar;
            }
            throw new NullPointerException("Argument 'r' of type Runnable (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }

        C0884h(String str, int i, boolean z) {
            this.f728P = str + "-pool-" + f727S.getAndIncrement() + "-thread-";
            this.f729Q = i;
            this.f730R = z;
        }
    }

    /* renamed from: d */
    public static ExecutorService m964d() {
        return m950a(-2);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static Executor m965e() {
        if (f704e == null) {
            f704e = new C0875c();
        }
        return f704e;
    }

    /* renamed from: a */
    public static <T> void m954a(C0877e<T> eVar, long j, TimeUnit timeUnit) {
        m957a(m950a(-4), eVar, j, timeUnit);
    }

    /* renamed from: b */
    public static <T> void m960b(C0877e<T> eVar, long j, TimeUnit timeUnit) {
        m957a(m950a(-1), eVar, j, timeUnit);
    }

    /* renamed from: c */
    public static <T> void m963c(C0877e<T> eVar) {
        m955a(m950a(-1), eVar);
    }

    /* renamed from: a */
    public static <T> void m953a(C0877e<T> eVar, long j, long j2, TimeUnit timeUnit) {
        m961b(m950a(-4), eVar, j, j2, timeUnit);
    }

    /* renamed from: b */
    public static <T> void m959b(C0877e<T> eVar) {
        m955a(m950a(-4), eVar);
    }

    /* renamed from: a */
    public static void m952a(C0877e eVar) {
        if (eVar != null) {
            eVar.mo9794a();
        }
    }

    /* renamed from: b */
    private static <T> void m961b(ExecutorService executorService, C0877e<T> eVar, long j, long j2, TimeUnit timeUnit) {
        m956a(executorService, eVar, j, j2, timeUnit);
    }

    /* renamed from: a */
    private static <T> void m955a(ExecutorService executorService, C0877e<T> eVar) {
        m956a(executorService, eVar, 0, 0, null);
    }

    /* renamed from: a */
    private static <T> void m957a(ExecutorService executorService, C0877e<T> eVar, long j, TimeUnit timeUnit) {
        m956a(executorService, eVar, j, 0, timeUnit);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        if (r8 != 0) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r6 != 0) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r4.execute(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        r8 = new com.blankj.utilcode.util.ThreadUtils.C0873a(r4, r5);
        com.blankj.utilcode.util.ThreadUtils.C0882f.m978a(r1, r8);
        com.blankj.utilcode.util.ThreadUtils.f703d.schedule(r8, r10.toMillis(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        com.blankj.utilcode.util.ThreadUtils.C0877e.m968a(r5, true);
        r0 = new com.blankj.utilcode.util.ThreadUtils.C0874b(r4, r5);
        com.blankj.utilcode.util.ThreadUtils.C0882f.m978a(r1, r0);
        com.blankj.utilcode.util.ThreadUtils.f703d.scheduleAtFixedRate(r0, r10.toMillis(r6), r10.toMillis(r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> void m956a(java.util.concurrent.ExecutorService r4, com.blankj.utilcode.util.ThreadUtils.C0877e<T> r5, long r6, long r8, java.util.concurrent.TimeUnit r10) {
        /*
            java.util.Map<com.blankj.utilcode.util.c0$e, com.blankj.utilcode.util.c0$f> r0 = com.blankj.utilcode.util.ThreadUtils.f701b
            monitor-enter(r0)
            java.util.Map<com.blankj.utilcode.util.c0$e, com.blankj.utilcode.util.c0$f> r1 = com.blankj.utilcode.util.ThreadUtils.f701b     // Catch:{ all -> 0x005b }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x0014
            java.lang.String r4 = "ThreadUtils"
            java.lang.String r5 = "Task can only be executed once."
            android.util.Log.e(r4, r5)     // Catch:{ all -> 0x005b }
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            return
        L_0x0014:
            com.blankj.utilcode.util.c0$f r1 = new com.blankj.utilcode.util.c0$f     // Catch:{ all -> 0x005b }
            r2 = 0
            r1.<init>(r4, r2)     // Catch:{ all -> 0x005b }
            java.util.Map<com.blankj.utilcode.util.c0$e, com.blankj.utilcode.util.c0$f> r2 = com.blankj.utilcode.util.ThreadUtils.f701b     // Catch:{ all -> 0x005b }
            r2.put(r5, r1)     // Catch:{ all -> 0x005b }
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            r2 = 0
            int r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0040
            int r8 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x002e
            r4.execute(r5)
            goto L_0x005a
        L_0x002e:
            com.blankj.utilcode.util.c0$a r8 = new com.blankj.utilcode.util.c0$a
            r8.<init>(r4, r5)
            java.util.TimerTask unused = r1.f724a = r8
            java.util.Timer r4 = com.blankj.utilcode.util.ThreadUtils.f703d
            long r5 = r10.toMillis(r6)
            r4.schedule(r8, r5)
            goto L_0x005a
        L_0x0040:
            r0 = 1
            r5.m969b(r0)
            com.blankj.utilcode.util.c0$b r0 = new com.blankj.utilcode.util.c0$b
            r0.<init>(r4, r5)
            java.util.TimerTask unused = r1.f724a = r0
            java.util.Timer r4 = com.blankj.utilcode.util.ThreadUtils.f703d
            long r6 = r10.toMillis(r6)
            long r8 = r10.toMillis(r8)
            r5 = r0
            r4.scheduleAtFixedRate(r5, r6, r8)
        L_0x005a:
            return
        L_0x005b:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.blankj.utilcode.util.ThreadUtils.m956a(java.util.concurrent.ExecutorService, com.blankj.utilcode.util.c0$e, long, long, java.util.concurrent.TimeUnit):void");
    }

    /* renamed from: a */
    private static ExecutorService m950a(int i) {
        return m951a(i, 5);
    }

    /* renamed from: a */
    private static ExecutorService m951a(int i, int i2) {
        ExecutorService executorService;
        synchronized (f700a) {
            Map map = f700a.get(Integer.valueOf(i));
            if (map == null) {
                ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
                executorService = C0883g.m980b(i, i2);
                concurrentHashMap.put(Integer.valueOf(i2), executorService);
                f700a.put(Integer.valueOf(i), concurrentHashMap);
            } else {
                executorService = (ExecutorService) map.get(Integer.valueOf(i2));
                if (executorService == null) {
                    executorService = C0883g.m980b(i, i2);
                    map.put(Integer.valueOf(i2), executorService);
                }
            }
        }
        return executorService;
    }
}
