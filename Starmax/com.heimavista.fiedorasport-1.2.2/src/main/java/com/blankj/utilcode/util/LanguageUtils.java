package com.blankj.utilcode.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import androidx.annotation.NonNull;
import java.util.Locale;

/* renamed from: com.blankj.utilcode.util.p */
public class LanguageUtils {
    /* renamed from: a */
    static void m1110a(@NonNull Activity activity) {
        if (activity != null) {
            String a = Utils.m897j().mo9871a("KEY_LOCALE");
            if (!TextUtils.isEmpty(a)) {
                if ("VALUE_FOLLOW_SYSTEM".equals(a)) {
                    Locale locale = Resources.getSystem().getConfiguration().locale;
                    m1111a(Utils.m891d(), locale);
                    m1111a(activity, locale);
                    return;
                }
                String[] split = a.split("\\$");
                if (split.length != 2) {
                    Log.e("LanguageUtils", "The string of " + a + " is not in the correct format.");
                    return;
                }
                Locale locale2 = new Locale(split[0], split[1]);
                m1111a(Utils.m891d(), locale2);
                m1111a(activity, locale2);
                return;
            }
            return;
        }
        throw new NullPointerException("Argument 'activity' of type Activity (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    private static void m1111a(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale locale2 = configuration.locale;
        if (!m1112a(locale2.getLanguage(), locale.getLanguage()) || !m1112a(locale2.getCountry(), locale.getCountry())) {
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            if (Build.VERSION.SDK_INT >= 17) {
                configuration.setLocale(locale);
                context.createConfigurationContext(configuration);
            } else {
                configuration.locale = locale;
            }
            resources.updateConfiguration(configuration, displayMetrics);
        }
    }

    /* renamed from: a */
    private static boolean m1112a(CharSequence charSequence, CharSequence charSequence2) {
        int length;
        if (charSequence == charSequence2) {
            return true;
        }
        if (charSequence == null || charSequence2 == null || (length = charSequence.length()) != charSequence2.length()) {
            return false;
        }
        if ((charSequence instanceof String) && (charSequence2 instanceof String)) {
            return charSequence.equals(charSequence2);
        }
        for (int i = 0; i < length; i++) {
            if (charSequence.charAt(i) != charSequence2.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
