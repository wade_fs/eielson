package com.blankj.utilcode.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import com.blankj.utilcode.p020a.PermissionConstants;
import com.blankj.utilcode.util.Utils;
import com.google.android.exoplayer2.C1750C;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* renamed from: com.blankj.utilcode.util.s */
public final class PermissionUtils {

    /* renamed from: j */
    private static final List<String> f807j = m1211e();
    /* access modifiers changed from: private */

    /* renamed from: k */
    public static PermissionUtils f808k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public static C0922e f809l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static C0922e f810m;

    /* renamed from: a */
    private C0917c f811a;

    /* renamed from: b */
    private C0922e f812b;

    /* renamed from: c */
    private C0916b f813c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C0923f f814d;

    /* renamed from: e */
    private Set<String> f815e = new LinkedHashSet();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public List<String> f816f;

    /* renamed from: g */
    private List<String> f817g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public List<String> f818h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public List<String> f819i;

    /* renamed from: com.blankj.utilcode.util.s$a */
    /* compiled from: PermissionUtils */
    class C0915a implements C0917c.C0918a {

        /* renamed from: a */
        final /* synthetic */ Activity f820a;

        C0915a(Activity activity) {
            this.f820a = activity;
        }

        /* renamed from: a */
        public void mo9860a(boolean z) {
            this.f820a.finish();
            if (z) {
                List unused = PermissionUtils.this.f818h = new ArrayList();
                List unused2 = PermissionUtils.this.f819i = new ArrayList();
                PermissionUtils.this.m1216j();
                return;
            }
            PermissionUtils.this.m1215i();
        }
    }

    /* renamed from: com.blankj.utilcode.util.s$b */
    /* compiled from: PermissionUtils */
    public interface C0916b {
        /* renamed from: a */
        void mo9861a(List<String> list);

        /* renamed from: a */
        void mo9862a(List<String> list, List<String> list2);
    }

    /* renamed from: com.blankj.utilcode.util.s$c */
    /* compiled from: PermissionUtils */
    public interface C0917c {

        /* renamed from: com.blankj.utilcode.util.s$c$a */
        /* compiled from: PermissionUtils */
        public interface C0918a {
            /* renamed from: a */
            void mo9860a(boolean z);
        }

        /* renamed from: a */
        void mo9863a(C0918a aVar);
    }

    @RequiresApi(api = 23)
    /* renamed from: com.blankj.utilcode.util.s$d */
    /* compiled from: PermissionUtils */
    static final class C0919d extends Utils.TransActivity.C0866a {

        /* renamed from: a */
        private static C0919d f822a = new C0919d();

        /* renamed from: com.blankj.utilcode.util.s$d$a */
        /* compiled from: PermissionUtils */
        static class C0920a implements Utils.C0870c<Void, Intent> {

            /* renamed from: a */
            final /* synthetic */ int f823a;

            C0920a(int i) {
                this.f823a = i;
            }

            /* renamed from: a */
            public Void call(Intent intent) {
                intent.putExtra("TYPE", this.f823a);
                return null;
            }
        }

        /* renamed from: com.blankj.utilcode.util.s$d$b */
        /* compiled from: PermissionUtils */
        class C0921b implements Runnable {
            C0921b(C0919d dVar) {
            }

            public void run() {
                if (PermissionUtils.m1212f()) {
                    PermissionUtils.f810m.mo9866a();
                } else {
                    PermissionUtils.f810m.mo9867b();
                }
                C0922e unused = PermissionUtils.f810m = (C0922e) null;
            }
        }

        C0919d() {
        }

        /* renamed from: a */
        public static void m1226a(int i) {
            Utils.TransActivity.m901a(new C0920a(i), f822a);
        }

        /* renamed from: b */
        public void mo9762b(Activity activity, @Nullable Bundle bundle) {
            activity.getWindow().addFlags(262160);
            int intExtra = activity.getIntent().getIntExtra("TYPE", -1);
            if (intExtra == 1) {
                if (PermissionUtils.f808k == null) {
                    Log.e("PermissionUtils", "request permissions failed");
                    activity.finish();
                    return;
                }
                if (PermissionUtils.f808k.f814d != null) {
                    PermissionUtils.f808k.f814d.mo9868a(activity);
                }
                if (!PermissionUtils.f808k.m1207c(activity) && PermissionUtils.f808k.f816f != null) {
                    int size = PermissionUtils.f808k.f816f.size();
                    if (size <= 0) {
                        activity.finish();
                    } else {
                        activity.requestPermissions((String[]) PermissionUtils.f808k.f816f.toArray(new String[size]), 1);
                    }
                }
            } else if (intExtra == 2) {
                PermissionUtils.m1210d(activity, 2);
            } else if (intExtra == 3) {
                PermissionUtils.m1206c(activity, 3);
            } else {
                activity.finish();
                Log.e("PermissionUtils", "type is wrong.");
            }
        }

        /* renamed from: a */
        public void mo9758a(Activity activity, int i, String[] strArr, int[] iArr) {
            if (!(PermissionUtils.f808k == null || PermissionUtils.f808k.f816f == null)) {
                PermissionUtils.f808k.m1198b(activity);
            }
            activity.finish();
        }

        /* renamed from: a */
        public boolean mo9760a(Activity activity, MotionEvent motionEvent) {
            activity.finish();
            return true;
        }

        /* renamed from: a */
        public void mo9757a(Activity activity, int i, int i2, Intent intent) {
            if (i == 2) {
                if (PermissionUtils.f809l != null) {
                    if (PermissionUtils.m1213g()) {
                        PermissionUtils.f809l.mo9866a();
                    } else {
                        PermissionUtils.f809l.mo9867b();
                    }
                    C0922e unused = PermissionUtils.f809l = (C0922e) null;
                } else {
                    return;
                }
            } else if (i == 3) {
                if (PermissionUtils.f810m != null) {
                    Utils.m888a(new C0921b(this), 100);
                } else {
                    return;
                }
            }
            activity.finish();
        }
    }

    /* renamed from: com.blankj.utilcode.util.s$e */
    /* compiled from: PermissionUtils */
    public interface C0922e {
        /* renamed from: a */
        void mo9866a();

        /* renamed from: b */
        void mo9867b();
    }

    /* renamed from: com.blankj.utilcode.util.s$f */
    /* compiled from: PermissionUtils */
    public interface C0923f {
        /* renamed from: a */
        void mo9868a(Activity activity);
    }

    private PermissionUtils(String... strArr) {
        for (String str : strArr) {
            String[] a = PermissionConstants.m848a(str);
            for (String str2 : a) {
                if (f807j.contains(str2)) {
                    this.f815e.add(str2);
                }
            }
        }
        f808k = this;
    }

    /* renamed from: e */
    public static List<String> m1211e() {
        return m1187a(Utils.m891d().getPackageName());
    }

    @RequiresApi(api = 23)
    /* renamed from: f */
    public static boolean m1212f() {
        return Settings.canDrawOverlays(Utils.m891d());
    }

    @RequiresApi(api = 23)
    /* renamed from: g */
    public static boolean m1213g() {
        return Settings.System.canWrite(Utils.m891d());
    }

    /* renamed from: h */
    public static void m1214h() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.parse("package:" + Utils.m891d().getPackageName()));
        if (m1192a(intent)) {
            Utils.m891d().startActivity(intent.addFlags(C1750C.ENCODING_PCM_MU_LAW));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m1215i() {
        if (this.f812b != null) {
            if (this.f816f.size() == 0 || this.f815e.size() == this.f817g.size()) {
                this.f812b.mo9866a();
            } else if (!this.f818h.isEmpty()) {
                this.f812b.mo9867b();
            }
            this.f812b = null;
        }
        if (this.f813c != null) {
            if (this.f816f.size() == 0 || this.f817g.size() > 0) {
                this.f813c.mo9861a(this.f817g);
            }
            if (!this.f818h.isEmpty()) {
                this.f813c.mo9862a(this.f819i, this.f818h);
            }
            this.f813c = null;
        }
        this.f811a = null;
        this.f814d = null;
    }

    /* access modifiers changed from: private */
    @RequiresApi(api = 23)
    /* renamed from: j */
    public void m1216j() {
        C0919d.m1226a(1);
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    /* renamed from: d */
    public static void m1210d(Activity activity, int i) {
        Intent intent = new Intent("android.settings.action.MANAGE_WRITE_SETTINGS");
        intent.setData(Uri.parse("package:" + Utils.m891d().getPackageName()));
        if (!m1192a(intent)) {
            m1214h();
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    /* renamed from: c */
    public static void m1206c(Activity activity, int i) {
        Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION");
        intent.setData(Uri.parse("package:" + Utils.m891d().getPackageName()));
        if (!m1192a(intent)) {
            m1214h();
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    /* renamed from: a */
    public static List<String> m1187a(String str) {
        try {
            String[] strArr = Utils.m891d().getPackageManager().getPackageInfo(str, 4096).requestedPermissions;
            if (strArr == null) {
                return Collections.emptyList();
            }
            return Arrays.asList(strArr);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /* renamed from: b */
    private static boolean m1202b(String str) {
        return Build.VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(Utils.m891d(), str) == 0;
    }

    /* renamed from: b */
    public static PermissionUtils m1196b(String... strArr) {
        return new PermissionUtils(strArr);
    }

    /* access modifiers changed from: private */
    @RequiresApi(api = 23)
    /* renamed from: c */
    public boolean m1207c(Activity activity) {
        boolean z = false;
        if (this.f811a != null) {
            Iterator<String> it = this.f816f.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (activity.shouldShowRequestPermissionRationale(it.next())) {
                        m1188a(activity);
                        this.f811a.mo9863a(new C0915a(activity));
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.f811a = null;
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m1198b(Activity activity) {
        m1188a(activity);
        m1215i();
    }

    /* renamed from: a */
    public static boolean m1193a(String... strArr) {
        for (String str : strArr) {
            if (!m1202b(str)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    private static boolean m1192a(Intent intent) {
        return Utils.m891d().getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }

    /* renamed from: a */
    public PermissionUtils mo9857a(C0917c cVar) {
        this.f811a = cVar;
        return this;
    }

    /* renamed from: a */
    public PermissionUtils mo9858a(C0922e eVar) {
        this.f812b = eVar;
        return this;
    }

    /* renamed from: a */
    public PermissionUtils mo9856a(C0916b bVar) {
        this.f813c = bVar;
        return this;
    }

    /* renamed from: a */
    public void mo9859a() {
        this.f817g = new ArrayList();
        this.f816f = new ArrayList();
        this.f818h = new ArrayList();
        this.f819i = new ArrayList();
        if (Build.VERSION.SDK_INT < 23) {
            this.f817g.addAll(this.f815e);
            m1215i();
            return;
        }
        for (String str : this.f815e) {
            if (m1202b(str)) {
                this.f817g.add(str);
            } else {
                this.f816f.add(str);
            }
        }
        if (this.f816f.isEmpty()) {
            m1215i();
        } else {
            m1216j();
        }
    }

    /* renamed from: a */
    private void m1188a(Activity activity) {
        for (String str : this.f816f) {
            if (m1202b(str)) {
                this.f817g.add(str);
            } else {
                this.f818h.add(str);
                if (!activity.shouldShowRequestPermissionRationale(str)) {
                    this.f819i.add(str);
                }
            }
        }
    }
}
