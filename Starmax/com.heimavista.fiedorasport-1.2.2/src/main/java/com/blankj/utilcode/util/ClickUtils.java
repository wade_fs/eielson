package com.blankj.utilcode.util;

import android.view.View;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

/* renamed from: com.blankj.utilcode.util.g */
public class ClickUtils {

    /* renamed from: com.blankj.utilcode.util.g$a */
    /* compiled from: ClickUtils */
    static class C0902a extends C0903b {

        /* renamed from: T */
        final /* synthetic */ View.OnClickListener f764T;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C0902a(boolean z, long j, View.OnClickListener onClickListener) {
            super(z, j);
            this.f764T = onClickListener;
        }

        /* renamed from: a */
        public void mo9825a(View view) {
            this.f764T.onClick(view);
        }
    }

    /* renamed from: com.blankj.utilcode.util.g$b */
    /* compiled from: ClickUtils */
    public static abstract class C0903b implements View.OnClickListener {
        /* access modifiers changed from: private */

        /* renamed from: R */
        public static boolean f765R = true;

        /* renamed from: S */
        private static final Runnable f766S = new C0904a();

        /* renamed from: P */
        private long f767P;

        /* renamed from: Q */
        private boolean f768Q;

        /* renamed from: com.blankj.utilcode.util.g$b$a */
        /* compiled from: ClickUtils */
        static class C0904a implements Runnable {
            C0904a() {
            }

            public void run() {
                boolean unused = C0903b.f765R = true;
            }
        }

        public C0903b(boolean z, long j) {
            this.f768Q = z;
            this.f767P = j;
        }

        /* renamed from: a */
        public abstract void mo9825a(View view);

        public final void onClick(View view) {
            if (this.f768Q) {
                if (f765R) {
                    f765R = false;
                    view.postDelayed(f766S, this.f767P);
                    mo9825a(view);
                }
            } else if (m1048a(view, this.f767P)) {
                mo9825a(view);
            }
        }

        /* renamed from: a */
        private static boolean m1048a(@NonNull View view, long j) {
            if (view != null) {
                long currentTimeMillis = System.currentTimeMillis();
                Object tag = view.getTag(-7);
                if (!(tag instanceof Long)) {
                    view.setTag(-7, Long.valueOf(currentTimeMillis));
                    return true;
                } else if (currentTimeMillis - ((Long) tag).longValue() <= j) {
                    return false;
                } else {
                    view.setTag(-7, Long.valueOf(currentTimeMillis));
                    return true;
                }
            } else {
                throw new NullPointerException("Argument 'view' of type View (#0 out of 2, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            }
        }
    }

    /* renamed from: a */
    public static void m1045a(View[] viewArr, View.OnClickListener onClickListener) {
        m1044a(viewArr, 200, onClickListener);
    }

    /* renamed from: a */
    public static void m1044a(View[] viewArr, @IntRange(from = 0) long j, View.OnClickListener onClickListener) {
        m1046a(viewArr, false, j, onClickListener);
    }

    /* renamed from: a */
    private static void m1046a(View[] viewArr, boolean z, @IntRange(from = 0) long j, View.OnClickListener onClickListener) {
        if (viewArr != null && viewArr.length != 0 && onClickListener != null) {
            for (View view : viewArr) {
                if (view != null) {
                    view.setOnClickListener(new C0902a(z, j, onClickListener));
                }
            }
        }
    }
}
