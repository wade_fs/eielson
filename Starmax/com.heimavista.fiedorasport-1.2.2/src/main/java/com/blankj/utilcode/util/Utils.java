package com.blankj.utilcode.util;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import com.google.android.exoplayer2.C1750C;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

public final class Utils {

    /* renamed from: a */
    private static final C0868b f674a = new C0868b();

    /* renamed from: b */
    private static final ExecutorService f675b = ThreadUtils.m964d();

    /* renamed from: c */
    private static final Handler f676c = new Handler(Looper.getMainLooper());
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: d */
    private static Application f677d;

    public static final class FileProvider4UtilCode extends FileProvider {
        public boolean onCreate() {
            Utils.m885a(getContext());
            try {
                Class.forName("com.blankj.utildebug.DebugUtils");
                return true;
            } catch (Exception unused) {
                return true;
            }
        }
    }

    public static final class TransActivity extends FragmentActivity {

        /* renamed from: P */
        private static final Map<TransActivity, C0866a> f678P = new HashMap();

        /* renamed from: Q */
        private static C0866a f679Q;

        /* renamed from: com.blankj.utilcode.util.Utils$TransActivity$a */
        public static abstract class C0866a {
            /* renamed from: a */
            public void mo9756a(Activity activity) {
            }

            /* renamed from: a */
            public void mo9757a(Activity activity, int i, int i2, Intent intent) {
            }

            /* renamed from: a */
            public void mo9758a(Activity activity, int i, String[] strArr, int[] iArr) {
            }

            /* renamed from: a */
            public void mo9759a(Activity activity, @Nullable Bundle bundle) {
            }

            /* renamed from: a */
            public boolean mo9760a(Activity activity, MotionEvent motionEvent) {
                return false;
            }

            /* renamed from: b */
            public void mo9761b(Activity activity) {
            }

            /* renamed from: b */
            public void mo9762b(Activity activity, @Nullable Bundle bundle) {
            }

            /* renamed from: c */
            public void mo9763c(Activity activity) {
            }

            /* renamed from: c */
            public void mo9764c(Activity activity, Bundle bundle) {
            }

            /* renamed from: d */
            public void mo9765d(Activity activity) {
            }

            /* renamed from: e */
            public void mo9766e(Activity activity) {
            }
        }

        /* renamed from: a */
        public static void m901a(C0870c<Void, Intent> cVar, C0866a aVar) {
            if (aVar != null) {
                Intent intent = new Intent(Utils.m891d(), TransActivity.class);
                intent.addFlags(C1750C.ENCODING_PCM_MU_LAW);
                if (cVar != null) {
                    cVar.call(intent);
                }
                Utils.m891d().startActivity(intent);
                f679Q = aVar;
            }
        }

        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            C0866a aVar = f678P.get(this);
            if (aVar == null) {
                return super.dispatchTouchEvent(motionEvent);
            }
            if (aVar.mo9760a(this, motionEvent)) {
                return true;
            }
            return super.dispatchTouchEvent(motionEvent);
        }

        /* access modifiers changed from: protected */
        public void onActivityResult(int i, int i2, Intent intent) {
            super.onActivityResult(i, i2, intent);
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9757a(this, i, i2, intent);
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(@Nullable Bundle bundle) {
            overridePendingTransition(0, 0);
            C0866a aVar = f679Q;
            if (aVar == null) {
                super.onCreate(bundle);
                finish();
                return;
            }
            f678P.put(this, aVar);
            f679Q.mo9759a(this, bundle);
            super.onCreate(bundle);
            f679Q.mo9762b(this, bundle);
            f679Q = null;
        }

        /* access modifiers changed from: protected */
        public void onDestroy() {
            super.onDestroy();
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9756a(this);
                f678P.remove(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onPause() {
            overridePendingTransition(0, 0);
            super.onPause();
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9761b(this);
            }
        }

        public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9758a(this, i, strArr, iArr);
            }
        }

        /* access modifiers changed from: protected */
        public void onResume() {
            super.onResume();
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9763c(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onSaveInstanceState(Bundle bundle) {
            super.onSaveInstanceState(bundle);
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9764c(this, bundle);
            }
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            super.onStart();
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9765d(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onStop() {
            super.onStop();
            C0866a aVar = f678P.get(this);
            if (aVar != null) {
                aVar.mo9766e(this);
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.Utils$a */
    static class C0867a implements Runnable {
        C0867a() {
        }

        public void run() {
            AdaptScreenUtils.m937a();
        }
    }

    /* renamed from: com.blankj.utilcode.util.Utils$c */
    public interface C0870c<Ret, Par> {
        Ret call(Par par);
    }

    /* renamed from: com.blankj.utilcode.util.Utils$d */
    public interface C0871d {
        void onActivityDestroyed(Activity activity);
    }

    /* renamed from: com.blankj.utilcode.util.Utils$e */
    public interface C0872e {
        /* renamed from: a */
        void mo9780a(Activity activity);

        /* renamed from: b */
        void mo9781b(Activity activity);
    }

    /* renamed from: b */
    static C0868b m889b() {
        return f674a;
    }

    /* renamed from: c */
    static LinkedList<Activity> m890c() {
        return f674a.f680P;
    }

    /* renamed from: d */
    public static Application m891d() {
        Application application = f677d;
        if (application != null) {
            return application;
        }
        Application e = m892e();
        m884a(e);
        return e;
    }

    /* renamed from: e */
    private static Application m892e() {
        try {
            Class<?> cls = Class.forName("android.app.ActivityThread");
            Object invoke = cls.getMethod("getApplication", new Class[0]).invoke(cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]), new Object[0]);
            if (invoke != null) {
                return (Application) invoke;
            }
            throw new NullPointerException("u should init first");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new NullPointerException("u should init first");
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            throw new NullPointerException("u should init first");
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
            throw new NullPointerException("u should init first");
        } catch (ClassNotFoundException e4) {
            e4.printStackTrace();
            throw new NullPointerException("u should init first");
        }
    }

    /* renamed from: f */
    static String m893f() {
        String h = m895h();
        if (!TextUtils.isEmpty(h)) {
            return h;
        }
        String g = m894g();
        if (!TextUtils.isEmpty(g)) {
            return g;
        }
        return m896i();
    }

    /* renamed from: g */
    private static String m894g() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        String str;
        ActivityManager activityManager = (ActivityManager) m891d().getSystemService("activity");
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.size() == 0)) {
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.pid == myPid && (str = runningAppProcessInfo.processName) != null) {
                    return str;
                }
            }
        }
        return "";
    }

    /* renamed from: h */
    private static String m895h() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("/proc/" + Process.myPid() + "/cmdline")));
            String trim = bufferedReader.readLine().trim();
            bufferedReader.close();
            return trim;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: i */
    private static String m896i() {
        try {
            Application d = m891d();
            Field field = d.getClass().getField("mLoadedApk");
            field.setAccessible(true);
            Object obj = field.get(d);
            Field declaredField = obj.getClass().getDeclaredField("mActivityThread");
            declaredField.setAccessible(true);
            Object obj2 = declaredField.get(obj);
            return (String) obj2.getClass().getDeclaredMethod("getProcessName", new Class[0]).invoke(obj2, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: j */
    static SPUtils m897j() {
        return SPUtils.m1243c("Utils");
    }

    /* renamed from: k */
    static Context m898k() {
        if (!m899l()) {
            return m891d();
        }
        Activity a = f674a.mo9768a();
        return a == null ? m891d() : a;
    }

    /* renamed from: l */
    static boolean m899l() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        ActivityManager activityManager = (ActivityManager) m891d().getSystemService("activity");
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.size() == 0)) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.importance == 100 && runningAppProcessInfo.processName.equals(m891d().getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public static void m900m() {
        if (Build.VERSION.SDK_INT < 26 || !ValueAnimator.areAnimatorsEnabled()) {
            try {
                Field declaredField = ValueAnimator.class.getDeclaredField("sDurationScale");
                declaredField.setAccessible(true);
                if (((Float) declaredField.get(null)).floatValue() == 0.0f) {
                    declaredField.set(null, Float.valueOf(1.0f));
                    Log.i("Utils", "setAnimatorsEnabled: Animators are enabled now!");
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public static void m885a(Context context) {
        if (context == null) {
            m884a(m892e());
        } else {
            m884a((Application) context.getApplicationContext());
        }
    }

    /* renamed from: a */
    public static void m884a(Application application) {
        if (f677d == null) {
            if (application == null) {
                f677d = m892e();
            } else {
                f677d = application;
            }
            f677d.registerActivityLifecycleCallbacks(f674a);
            f675b.execute(new C0867a());
        } else if (application != null && application.getClass() != f677d.getClass()) {
            f677d.unregisterActivityLifecycleCallbacks(f674a);
            f674a.f680P.clear();
            f677d = application;
            f677d.registerActivityLifecycleCallbacks(f674a);
        }
    }

    /* renamed from: com.blankj.utilcode.util.Utils$b */
    static class C0868b implements Application.ActivityLifecycleCallbacks {

        /* renamed from: P */
        final LinkedList<Activity> f680P = new LinkedList<>();

        /* renamed from: Q */
        final List<C0872e> f681Q = new ArrayList();

        /* renamed from: R */
        final Map<Activity, List<C0871d>> f682R = new HashMap();

        /* renamed from: S */
        private int f683S = 0;

        /* renamed from: T */
        private int f684T = 0;

        /* renamed from: U */
        private boolean f685U = false;

        /* renamed from: com.blankj.utilcode.util.Utils$b$a */
        class C0869a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Activity f686P;

            /* renamed from: Q */
            final /* synthetic */ Object f687Q;

            C0869a(C0868b bVar, Activity activity, Object obj) {
                this.f686P = activity;
                this.f687Q = obj;
            }

            public void run() {
                Window window = this.f686P.getWindow();
                if (window != null) {
                    window.setSoftInputMode(((Integer) this.f687Q).intValue());
                }
            }
        }

        C0868b() {
        }

        /* renamed from: b */
        private void m917b(Activity activity, boolean z) {
            if (z) {
                activity.getWindow().getDecorView().setTag(-123, Integer.valueOf(activity.getWindow().getAttributes().softInputMode));
                activity.getWindow().setSoftInputMode(3);
                return;
            }
            Object tag = activity.getWindow().getDecorView().getTag(-123);
            if (tag instanceof Integer) {
                Utils.m888a(new C0869a(this, activity, tag), 100);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public Activity mo9768a() {
            if (!this.f680P.isEmpty()) {
                for (int size = this.f680P.size() - 1; size >= 0; size--) {
                    Activity activity = this.f680P.get(size);
                    if (activity != null && !activity.isFinishing() && (Build.VERSION.SDK_INT < 17 || !activity.isDestroyed())) {
                        return activity;
                    }
                }
            }
            Activity b = m915b();
            if (b != null) {
                m916b(b);
            }
            return b;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            LanguageUtils.m1110a(activity);
            Utils.m900m();
            m916b(activity);
        }

        public void onActivityDestroyed(Activity activity) {
            this.f680P.remove(activity);
            m913a(activity);
            Utils.m886a(activity.getWindow());
        }

        public void onActivityPaused(Activity activity) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.blankj.utilcode.util.Utils.b.a(android.app.Activity, boolean):void
         arg types: [android.app.Activity, int]
         candidates:
          com.blankj.utilcode.util.Utils.b.a(android.app.Activity, com.blankj.utilcode.util.Utils$d):void
          com.blankj.utilcode.util.Utils.b.a(android.app.Activity, boolean):void */
        public void onActivityResumed(Activity activity) {
            m916b(activity);
            if (this.f685U) {
                this.f685U = false;
                m914a(activity, true);
            }
            m917b(activity, false);
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
            if (!this.f685U) {
                m916b(activity);
            }
            int i = this.f684T;
            if (i < 0) {
                this.f684T = i + 1;
            } else {
                this.f683S++;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.blankj.utilcode.util.Utils.b.a(android.app.Activity, boolean):void
         arg types: [android.app.Activity, int]
         candidates:
          com.blankj.utilcode.util.Utils.b.a(android.app.Activity, com.blankj.utilcode.util.Utils$d):void
          com.blankj.utilcode.util.Utils.b.a(android.app.Activity, boolean):void */
        public void onActivityStopped(Activity activity) {
            if (activity.isChangingConfigurations()) {
                this.f684T--;
            } else {
                this.f683S--;
                if (this.f683S <= 0) {
                    this.f685U = true;
                    m914a(activity, false);
                }
            }
            m917b(activity, true);
        }

        /* renamed from: b */
        private void m916b(Activity activity) {
            if (!this.f680P.contains(activity)) {
                this.f680P.addLast(activity);
            } else if (!this.f680P.getLast().equals(activity)) {
                this.f680P.remove(activity);
                this.f680P.addLast(activity);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo9769a(Activity activity, C0871d dVar) {
            if (activity != null && dVar != null) {
                List list = this.f682R.get(activity);
                if (list == null) {
                    list = new CopyOnWriteArrayList();
                    this.f682R.put(activity, list);
                } else if (list.contains(dVar)) {
                    return;
                }
                list.add(dVar);
            }
        }

        /* renamed from: a */
        private void m914a(Activity activity, boolean z) {
            if (!this.f681Q.isEmpty()) {
                for (C0872e eVar : this.f681Q) {
                    if (z) {
                        eVar.mo9780a(activity);
                    } else {
                        eVar.mo9781b(activity);
                    }
                }
            }
        }

        /* renamed from: b */
        private Activity m915b() {
            try {
                Class<?> cls = Class.forName("android.app.ActivityThread");
                Object invoke = cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]);
                Field declaredField = cls.getDeclaredField("mActivityList");
                declaredField.setAccessible(true);
                Map map = (Map) declaredField.get(invoke);
                if (map == null) {
                    return null;
                }
                for (Object obj : map.values()) {
                    Class<?> cls2 = obj.getClass();
                    Field declaredField2 = cls2.getDeclaredField("paused");
                    declaredField2.setAccessible(true);
                    if (!declaredField2.getBoolean(obj)) {
                        Field declaredField3 = cls2.getDeclaredField("activity");
                        declaredField3.setAccessible(true);
                        return (Activity) declaredField3.get(obj);
                    }
                }
                return null;
            } catch (Exception e) {
                Log.e("Utils", e.getMessage());
            }
        }

        /* renamed from: a */
        private void m913a(Activity activity) {
            Iterator<Map.Entry<Activity, List<C0871d>>> it = this.f682R.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                if (next.getKey() == activity) {
                    for (C0871d dVar : (List) next.getValue()) {
                        dVar.onActivityDestroyed(activity);
                    }
                    it.remove();
                }
            }
        }
    }

    /* renamed from: a */
    public static void m887a(Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            f676c.post(runnable);
        }
    }

    /* renamed from: a */
    public static void m888a(Runnable runnable, long j) {
        f676c.postDelayed(runnable, j);
    }

    /* renamed from: a */
    static void m886a(Window window) {
        InputMethodManager inputMethodManager = (InputMethodManager) m891d().getSystemService("input_method");
        if (inputMethodManager != null) {
            for (String str : new String[]{"mLastSrvView", "mCurRootView", "mServedView", "mNextServedView"}) {
                try {
                    Field declaredField = InputMethodManager.class.getDeclaredField(str);
                    if (!declaredField.isAccessible()) {
                        declaredField.setAccessible(true);
                    }
                    Object obj = declaredField.get(inputMethodManager);
                    if (obj instanceof View) {
                        if (((View) obj).getRootView() == window.getDecorView().getRootView()) {
                            declaredField.set(inputMethodManager, null);
                        }
                    }
                } catch (Throwable unused) {
                }
            }
        }
    }
}
