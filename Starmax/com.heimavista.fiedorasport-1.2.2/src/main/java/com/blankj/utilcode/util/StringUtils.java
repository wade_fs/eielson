package com.blankj.utilcode.util;

/* renamed from: com.blankj.utilcode.util.b0 */
public final class StringUtils {
    /* renamed from: a */
    public static String m941a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        if (!Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        return String.valueOf((char) (str.charAt(0) - ' ')) + str.substring(1);
    }
}
