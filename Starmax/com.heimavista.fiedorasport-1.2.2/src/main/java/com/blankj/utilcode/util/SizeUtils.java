package com.blankj.utilcode.util;

import android.content.res.Resources;

/* renamed from: com.blankj.utilcode.util.z */
public final class SizeUtils {
    /* renamed from: a */
    public static int m1272a(float f) {
        return (int) ((f * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }
}
