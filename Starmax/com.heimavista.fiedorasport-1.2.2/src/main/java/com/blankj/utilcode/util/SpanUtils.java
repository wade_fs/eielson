package com.blankj.utilcode.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.LineHeightSpan;
import android.text.style.MaskFilterSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ReplacementSpan;
import android.text.style.ScaleXSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.text.style.UpdateAppearance;
import android.util.Log;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;

public final class SpanUtils {

    /* renamed from: X */
    private static final String f601X = System.getProperty("line.separator");

    /* renamed from: A */
    private String f602A;

    /* renamed from: B */
    private Typeface f603B;

    /* renamed from: C */
    private Layout.Alignment f604C;

    /* renamed from: D */
    private int f605D;

    /* renamed from: E */
    private ClickableSpan f606E;

    /* renamed from: F */
    private String f607F;

    /* renamed from: G */
    private float f608G;

    /* renamed from: H */
    private BlurMaskFilter.Blur f609H;

    /* renamed from: I */
    private Shader f610I;

    /* renamed from: J */
    private float f611J;

    /* renamed from: K */
    private float f612K;

    /* renamed from: L */
    private float f613L;

    /* renamed from: M */
    private int f614M;

    /* renamed from: N */
    private Object[] f615N;

    /* renamed from: O */
    private Bitmap f616O;

    /* renamed from: P */
    private Drawable f617P;

    /* renamed from: Q */
    private Uri f618Q;

    /* renamed from: R */
    private int f619R;

    /* renamed from: S */
    private int f620S;

    /* renamed from: T */
    private int f621T;

    /* renamed from: U */
    private int f622U;

    /* renamed from: V */
    private C0861g f623V;

    /* renamed from: W */
    private int f624W;

    /* renamed from: a */
    private TextView f625a;

    /* renamed from: b */
    private CharSequence f626b;

    /* renamed from: c */
    private int f627c;

    /* renamed from: d */
    private int f628d;

    /* renamed from: e */
    private int f629e;

    /* renamed from: f */
    private int f630f;

    /* renamed from: g */
    private int f631g;

    /* renamed from: h */
    private int f632h;

    /* renamed from: i */
    private int f633i;

    /* renamed from: j */
    private int f634j;

    /* renamed from: k */
    private int f635k;

    /* renamed from: l */
    private int f636l;

    /* renamed from: m */
    private int f637m;

    /* renamed from: n */
    private int f638n;

    /* renamed from: o */
    private int f639o;

    /* renamed from: p */
    private int f640p;

    /* renamed from: q */
    private boolean f641q;

    /* renamed from: r */
    private float f642r;

    /* renamed from: s */
    private float f643s;

    /* renamed from: t */
    private boolean f644t;

    /* renamed from: u */
    private boolean f645u;

    /* renamed from: v */
    private boolean f646v;

    /* renamed from: w */
    private boolean f647w;

    /* renamed from: x */
    private boolean f648x;

    /* renamed from: y */
    private boolean f649y;

    /* renamed from: z */
    private boolean f650z;

    @SuppressLint({"ParcelCreator"})
    static class CustomTypefaceSpan extends TypefaceSpan {

        /* renamed from: P */
        private final Typeface f651P;

        /* renamed from: a */
        private void m879a(Paint paint, Typeface typeface) {
            int i;
            Typeface typeface2 = paint.getTypeface();
            if (typeface2 == null) {
                i = 0;
            } else {
                i = typeface2.getStyle();
            }
            int i2 = i & (~typeface.getStyle());
            if ((i2 & 1) != 0) {
                paint.setFakeBoldText(true);
            }
            if ((i2 & 2) != 0) {
                paint.setTextSkewX(-0.25f);
            }
            paint.getShader();
            paint.setTypeface(typeface);
        }

        public void updateDrawState(TextPaint textPaint) {
            m879a(textPaint, this.f651P);
        }

        public void updateMeasureState(TextPaint textPaint) {
            m879a(textPaint, this.f651P);
        }

        private CustomTypefaceSpan(Typeface typeface) {
            super("");
            this.f651P = typeface;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$b */
    static class C0856b implements LeadingMarginSpan {

        /* renamed from: P */
        private final int f652P;

        /* renamed from: Q */
        private final int f653Q;

        /* renamed from: R */
        private final int f654R;

        /* renamed from: S */
        private Path f655S;

        public void drawLeadingMargin(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, boolean z, Layout layout) {
            if (((Spanned) charSequence).getSpanStart(this) == i6) {
                Paint.Style style = paint.getStyle();
                int color = paint.getColor();
                paint.setColor(this.f652P);
                paint.setStyle(Paint.Style.FILL);
                if (canvas.isHardwareAccelerated()) {
                    if (this.f655S == null) {
                        this.f655S = new Path();
                        this.f655S.addCircle(0.0f, 0.0f, (float) this.f653Q, Path.Direction.CW);
                    }
                    canvas.save();
                    canvas.translate((float) (i + (i2 * this.f653Q)), ((float) (i3 + i5)) / 2.0f);
                    canvas.drawPath(this.f655S, paint);
                    canvas.restore();
                } else {
                    int i8 = this.f653Q;
                    canvas.drawCircle((float) (i + (i2 * i8)), ((float) (i3 + i5)) / 2.0f, (float) i8, paint);
                }
                paint.setColor(color);
                paint.setStyle(style);
            }
        }

        public int getLeadingMargin(boolean z) {
            return (this.f653Q * 2) + this.f654R;
        }

        private C0856b(int i, int i2, int i3) {
            this.f655S = null;
            this.f652P = i;
            this.f653Q = i2;
            this.f654R = i3;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$c */
    static abstract class C0857c extends ReplacementSpan {

        /* renamed from: P */
        final int f656P;

        /* renamed from: Q */
        private WeakReference<Drawable> f657Q;

        /* renamed from: b */
        private Drawable m880b() {
            WeakReference<Drawable> weakReference = this.f657Q;
            Drawable drawable = weakReference != null ? weakReference.get() : null;
            if (drawable != null) {
                return drawable;
            }
            Drawable a = mo9743a();
            this.f657Q = new WeakReference<>(a);
            return a;
        }

        /* renamed from: a */
        public abstract Drawable mo9743a();

        public void draw(@NonNull Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, @NonNull Paint paint) {
            float f2;
            int height;
            if (canvas == null) {
                throw new NullPointerException("Argument 'canvas' of type Canvas (#0 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            } else if (paint != null) {
                Drawable b = m880b();
                Rect bounds = b.getBounds();
                canvas.save();
                if (bounds.height() < i5 - i3) {
                    int i6 = this.f656P;
                    if (i6 == 3) {
                        f2 = (float) i3;
                    } else {
                        if (i6 == 2) {
                            height = ((i5 + i3) - bounds.height()) / 2;
                        } else if (i6 == 1) {
                            f2 = (float) (i4 - bounds.height());
                        } else {
                            height = i5 - bounds.height();
                        }
                        f2 = (float) height;
                    }
                    canvas.translate(f, f2);
                } else {
                    canvas.translate(f, (float) i3);
                }
                b.draw(canvas);
                canvas.restore();
            } else {
                throw new NullPointerException("Argument 'paint' of type Paint (#8 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            }
        }

        public int getSize(@NonNull Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
            int i3;
            if (paint != null) {
                Rect bounds = m880b().getBounds();
                if (fontMetricsInt != null && (i3 = fontMetricsInt.bottom - fontMetricsInt.top) < bounds.height()) {
                    int i4 = this.f656P;
                    if (i4 == 3) {
                        fontMetricsInt.top = fontMetricsInt.top;
                        fontMetricsInt.bottom = bounds.height() + fontMetricsInt.top;
                    } else if (i4 == 2) {
                        int i5 = i3 / 4;
                        fontMetricsInt.top = ((-bounds.height()) / 2) - i5;
                        fontMetricsInt.bottom = (bounds.height() / 2) - i5;
                    } else {
                        int i6 = fontMetricsInt.bottom;
                        fontMetricsInt.top = (-bounds.height()) + i6;
                        fontMetricsInt.bottom = i6;
                    }
                    fontMetricsInt.ascent = fontMetricsInt.top;
                    fontMetricsInt.descent = fontMetricsInt.bottom;
                }
                return bounds.right;
            }
            throw new NullPointerException("Argument 'paint' of type Paint (#0 out of 5, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }

        private C0857c(int i) {
            this.f656P = i;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$e */
    static class C0859e implements LineHeightSpan {

        /* renamed from: R */
        static Paint.FontMetricsInt f661R;

        /* renamed from: P */
        private final int f662P;

        /* renamed from: Q */
        final int f663Q;

        C0859e(int i, int i2) {
            this.f662P = i;
            this.f663Q = i2;
        }

        public void chooseHeight(CharSequence charSequence, int i, int i2, int i3, int i4, Paint.FontMetricsInt fontMetricsInt) {
            LogUtils.m1131b(fontMetricsInt, f661R);
            Paint.FontMetricsInt fontMetricsInt2 = f661R;
            if (fontMetricsInt2 == null) {
                f661R = new Paint.FontMetricsInt();
                Paint.FontMetricsInt fontMetricsInt3 = f661R;
                fontMetricsInt3.top = fontMetricsInt.top;
                fontMetricsInt3.ascent = fontMetricsInt.ascent;
                fontMetricsInt3.descent = fontMetricsInt.descent;
                fontMetricsInt3.bottom = fontMetricsInt.bottom;
                fontMetricsInt3.leading = fontMetricsInt.leading;
            } else {
                fontMetricsInt.top = fontMetricsInt2.top;
                fontMetricsInt.ascent = fontMetricsInt2.ascent;
                fontMetricsInt.descent = fontMetricsInt2.descent;
                fontMetricsInt.bottom = fontMetricsInt2.bottom;
                fontMetricsInt.leading = fontMetricsInt2.leading;
            }
            int i5 = this.f662P;
            int i6 = fontMetricsInt.descent;
            int i7 = fontMetricsInt.ascent;
            int i8 = i5 - (((i4 + i6) - i7) - i3);
            if (i8 > 0) {
                int i9 = this.f663Q;
                if (i9 == 3) {
                    fontMetricsInt.descent = i6 + i8;
                } else if (i9 == 2) {
                    int i10 = i8 / 2;
                    fontMetricsInt.descent = i6 + i10;
                    fontMetricsInt.ascent = i7 - i10;
                } else {
                    fontMetricsInt.ascent = i7 - i8;
                }
            }
            int i11 = this.f662P;
            int i12 = fontMetricsInt.bottom;
            int i13 = fontMetricsInt.top;
            int i14 = i11 - (((i4 + i12) - i13) - i3);
            if (i14 > 0) {
                int i15 = this.f663Q;
                if (i15 == 3) {
                    fontMetricsInt.bottom = i12 + i14;
                } else if (i15 == 2) {
                    int i16 = i14 / 2;
                    fontMetricsInt.bottom = i12 + i16;
                    fontMetricsInt.top = i13 - i16;
                } else {
                    fontMetricsInt.top = i13 - i14;
                }
            }
            if (i2 == ((Spanned) charSequence).getSpanEnd(this)) {
                f661R = null;
            }
            LogUtils.m1131b(fontMetricsInt, f661R);
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$f */
    static class C0860f implements LeadingMarginSpan {

        /* renamed from: P */
        private final int f664P;

        /* renamed from: Q */
        private final int f665Q;

        /* renamed from: R */
        private final int f666R;

        public void drawLeadingMargin(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, boolean z, Layout layout) {
            int i8 = i;
            Paint.Style style = paint.getStyle();
            int color = paint.getColor();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(this.f664P);
            canvas.drawRect((float) i8, (float) i3, (float) (i8 + (this.f665Q * i2)), (float) i5, paint);
            paint.setStyle(style);
            paint.setColor(color);
        }

        public int getLeadingMargin(boolean z) {
            return this.f665Q + this.f666R;
        }

        private C0860f(int i, int i2, int i3) {
            this.f664P = i;
            this.f665Q = i2;
            this.f666R = i3;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$g */
    private static class C0861g extends SpannableStringBuilder implements Serializable {
        private static final long serialVersionUID = 4909567650765875771L;

        private C0861g() {
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$h */
    static class C0862h extends CharacterStyle implements UpdateAppearance {

        /* renamed from: P */
        private Shader f667P;

        public void updateDrawState(TextPaint textPaint) {
            textPaint.setShader(this.f667P);
        }

        private C0862h(Shader shader) {
            this.f667P = shader;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$i */
    static class C0863i extends CharacterStyle implements UpdateAppearance {

        /* renamed from: P */
        private float f668P;

        /* renamed from: Q */
        private float f669Q;

        /* renamed from: R */
        private float f670R;

        /* renamed from: S */
        private int f671S;

        public void updateDrawState(TextPaint textPaint) {
            textPaint.setShadowLayer(this.f668P, this.f669Q, this.f670R, this.f671S);
        }

        private C0863i(float f, float f2, float f3, int i) {
            this.f668P = f;
            this.f669Q = f2;
            this.f670R = f3;
            this.f671S = i;
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$j */
    static class C0864j extends ReplacementSpan {

        /* renamed from: P */
        private final int f672P;

        /* renamed from: Q */
        private final Paint f673Q;

        public void draw(@NonNull Canvas canvas, CharSequence charSequence, @IntRange(from = 0) int i, @IntRange(from = 0) int i2, float f, int i3, int i4, int i5, @NonNull Paint paint) {
            if (canvas == null) {
                throw new NullPointerException("Argument 'canvas' of type Canvas (#0 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            } else if (paint != null) {
                canvas.drawRect(f, (float) i3, f + ((float) this.f672P), (float) i5, this.f673Q);
            } else {
                throw new NullPointerException("Argument 'paint' of type Paint (#8 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            }
        }

        public int getSize(@NonNull Paint paint, CharSequence charSequence, @IntRange(from = 0) int i, @IntRange(from = 0) int i2, @Nullable Paint.FontMetricsInt fontMetricsInt) {
            if (paint != null) {
                return this.f672P;
            }
            throw new NullPointerException("Argument 'paint' of type Paint (#0 out of 5, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }

        private C0864j(int i, int i2) {
            this.f673Q = new Paint();
            this.f672P = i;
            this.f673Q.setColor(i2);
            this.f673Q.setStyle(Paint.Style.FILL);
        }
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$k */
    static class C0865k extends ReplacementSpan {
        C0865k(int i) {
        }

        public void draw(@NonNull Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, @NonNull Paint paint) {
            if (canvas == null) {
                throw new NullPointerException("Argument 'canvas' of type Canvas (#0 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            } else if (paint != null) {
                CharSequence subSequence = charSequence.subSequence(i, i2);
                Paint.FontMetricsInt fontMetricsInt = paint.getFontMetricsInt();
                canvas.drawText(subSequence.toString(), f, (float) (i4 - (((((fontMetricsInt.descent + i4) + i4) + fontMetricsInt.ascent) / 2) - ((i5 + i3) / 2))), paint);
            } else {
                throw new NullPointerException("Argument 'paint' of type Paint (#8 out of 9, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
            }
        }

        public int getSize(@NonNull Paint paint, CharSequence charSequence, int i, int i2, @Nullable Paint.FontMetricsInt fontMetricsInt) {
            if (paint != null) {
                return (int) paint.measureText(charSequence.subSequence(i, i2).toString());
            }
            throw new NullPointerException("Argument 'paint' of type Paint (#0 out of 5, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
        }
    }

    private SpanUtils(TextView textView) {
        this();
        this.f625a = textView;
    }

    /* renamed from: d */
    private void m867d() {
        int i = this.f624W;
        if (i == 0) {
            m869f();
        } else if (i == 1) {
            m870g();
        } else if (i == 2) {
            m871h();
        }
        m868e();
    }

    /* renamed from: e */
    private void m868e() {
        this.f627c = 33;
        this.f628d = -16777217;
        this.f629e = -16777217;
        this.f630f = -1;
        this.f632h = -16777217;
        this.f635k = -1;
        this.f637m = -16777217;
        this.f640p = -1;
        this.f642r = -1.0f;
        this.f643s = -1.0f;
        this.f644t = false;
        this.f645u = false;
        this.f646v = false;
        this.f647w = false;
        this.f648x = false;
        this.f649y = false;
        this.f650z = false;
        this.f602A = null;
        this.f603B = null;
        this.f604C = null;
        this.f605D = -1;
        this.f606E = null;
        this.f607F = null;
        this.f608G = -1.0f;
        this.f610I = null;
        this.f611J = -1.0f;
        this.f615N = null;
        this.f616O = null;
        this.f617P = null;
        this.f618Q = null;
        this.f619R = -1;
        this.f621T = -1;
    }

    /* renamed from: f */
    private void m869f() {
        if (this.f626b.length() != 0) {
            int length = this.f623V.length();
            if (length == 0 && this.f630f != -1) {
                this.f623V.append((CharSequence) Character.toString(2)).append((CharSequence) "\n").setSpan(new AbsoluteSizeSpan(0), 0, 2, 33);
                length = 2;
            }
            this.f623V.append(this.f626b);
            int length2 = this.f623V.length();
            int i = this.f605D;
            if (i != -1) {
                this.f623V.setSpan(new C0865k(i), length, length2, this.f627c);
            }
            int i2 = this.f628d;
            if (i2 != -16777217) {
                this.f623V.setSpan(new ForegroundColorSpan(i2), length, length2, this.f627c);
            }
            int i3 = this.f629e;
            if (i3 != -16777217) {
                this.f623V.setSpan(new BackgroundColorSpan(i3), length, length2, this.f627c);
            }
            int i4 = this.f635k;
            if (i4 != -1) {
                this.f623V.setSpan(new LeadingMarginSpan.Standard(i4, this.f636l), length, length2, this.f627c);
            }
            int i5 = this.f632h;
            if (i5 != -16777217) {
                this.f623V.setSpan(new C0860f(i5, this.f633i, this.f634j), length, length2, this.f627c);
            }
            int i6 = this.f637m;
            if (i6 != -16777217) {
                this.f623V.setSpan(new C0856b(i6, this.f638n, this.f639o), length, length2, this.f627c);
            }
            int i7 = this.f640p;
            if (i7 != -1) {
                this.f623V.setSpan(new AbsoluteSizeSpan(i7, this.f641q), length, length2, this.f627c);
            }
            float f = this.f642r;
            if (f != -1.0f) {
                this.f623V.setSpan(new RelativeSizeSpan(f), length, length2, this.f627c);
            }
            float f2 = this.f643s;
            if (f2 != -1.0f) {
                this.f623V.setSpan(new ScaleXSpan(f2), length, length2, this.f627c);
            }
            int i8 = this.f630f;
            if (i8 != -1) {
                this.f623V.setSpan(new C0859e(i8, this.f631g), length, length2, this.f627c);
            }
            if (this.f644t) {
                this.f623V.setSpan(new StrikethroughSpan(), length, length2, this.f627c);
            }
            if (this.f645u) {
                this.f623V.setSpan(new UnderlineSpan(), length, length2, this.f627c);
            }
            if (this.f646v) {
                this.f623V.setSpan(new SuperscriptSpan(), length, length2, this.f627c);
            }
            if (this.f647w) {
                this.f623V.setSpan(new SubscriptSpan(), length, length2, this.f627c);
            }
            if (this.f648x) {
                this.f623V.setSpan(new StyleSpan(1), length, length2, this.f627c);
            }
            if (this.f649y) {
                this.f623V.setSpan(new StyleSpan(2), length, length2, this.f627c);
            }
            if (this.f650z) {
                this.f623V.setSpan(new StyleSpan(3), length, length2, this.f627c);
            }
            String str = this.f602A;
            if (str != null) {
                this.f623V.setSpan(new TypefaceSpan(str), length, length2, this.f627c);
            }
            Typeface typeface = this.f603B;
            if (typeface != null) {
                this.f623V.setSpan(new CustomTypefaceSpan(typeface), length, length2, this.f627c);
            }
            Layout.Alignment alignment = this.f604C;
            if (alignment != null) {
                this.f623V.setSpan(new AlignmentSpan.Standard(alignment), length, length2, this.f627c);
            }
            ClickableSpan clickableSpan = this.f606E;
            if (clickableSpan != null) {
                this.f623V.setSpan(clickableSpan, length, length2, this.f627c);
            }
            String str2 = this.f607F;
            if (str2 != null) {
                this.f623V.setSpan(new URLSpan(str2), length, length2, this.f627c);
            }
            float f3 = this.f608G;
            if (f3 != -1.0f) {
                this.f623V.setSpan(new MaskFilterSpan(new BlurMaskFilter(f3, this.f609H)), length, length2, this.f627c);
            }
            Shader shader = this.f610I;
            if (shader != null) {
                this.f623V.setSpan(new C0862h(shader), length, length2, this.f627c);
            }
            float f4 = this.f611J;
            if (f4 != -1.0f) {
                this.f623V.setSpan(new C0863i(f4, this.f612K, this.f613L, this.f614M), length, length2, this.f627c);
            }
            Object[] objArr = this.f615N;
            if (objArr != null) {
                for (Object obj : objArr) {
                    this.f623V.setSpan(obj, length, length2, this.f627c);
                }
            }
        }
    }

    /* renamed from: g */
    private void m870g() {
        int length = this.f623V.length();
        this.f626b = "<img>";
        m869f();
        int length2 = this.f623V.length();
        Bitmap bitmap = this.f616O;
        if (bitmap != null) {
            this.f623V.setSpan(new C0858d(bitmap, this.f620S), length, length2, this.f627c);
            return;
        }
        Drawable drawable = this.f617P;
        if (drawable != null) {
            this.f623V.setSpan(new C0858d(drawable, this.f620S), length, length2, this.f627c);
            return;
        }
        Uri uri = this.f618Q;
        if (uri != null) {
            this.f623V.setSpan(new C0858d(uri, this.f620S), length, length2, this.f627c);
            return;
        }
        int i = this.f619R;
        if (i != -1) {
            this.f623V.setSpan(new C0858d(i, this.f620S), length, length2, this.f627c);
        }
    }

    /* renamed from: h */
    private void m871h() {
        int length = this.f623V.length();
        this.f626b = "< >";
        m869f();
        this.f623V.setSpan(new C0864j(this.f621T, this.f622U), length, this.f623V.length(), this.f627c);
    }

    /* renamed from: a */
    public SpanUtils mo9733a(@IntRange(from = 0) int i) {
        mo9734a(i, false);
        return this;
    }

    /* renamed from: b */
    public SpanUtils mo9737b(@ColorInt int i) {
        this.f628d = i;
        return this;
    }

    /* renamed from: c */
    public SpanUtils mo9738c() {
        this.f648x = true;
        return this;
    }

    /* renamed from: c */
    private void m866c(int i) {
        m867d();
        this.f624W = i;
    }

    /* renamed from: a */
    public SpanUtils mo9734a(@IntRange(from = 0) int i, boolean z) {
        this.f640p = i;
        this.f641q = z;
        return this;
    }

    /* renamed from: b */
    public SpannableStringBuilder mo9736b() {
        m867d();
        TextView textView = this.f625a;
        if (textView != null) {
            textView.setText(this.f623V);
        }
        return this.f623V;
    }

    public SpanUtils() {
        this.f623V = new C0861g();
        this.f626b = "";
        this.f624W = -1;
        m868e();
    }

    /* renamed from: com.blankj.utilcode.util.SpanUtils$d */
    static class C0858d extends C0857c {

        /* renamed from: R */
        private Drawable f658R;

        /* renamed from: S */
        private Uri f659S;

        /* renamed from: T */
        private int f660T;

        /* renamed from: a */
        public Drawable mo9743a() {
            Drawable drawable;
            Drawable drawable2 = this.f658R;
            if (drawable2 != null) {
                return drawable2;
            }
            BitmapDrawable bitmapDrawable = null;
            if (this.f659S != null) {
                try {
                    InputStream openInputStream = Utils.m891d().getContentResolver().openInputStream(this.f659S);
                    BitmapDrawable bitmapDrawable2 = new BitmapDrawable(Utils.m891d().getResources(), BitmapFactory.decodeStream(openInputStream));
                    try {
                        bitmapDrawable2.setBounds(0, 0, bitmapDrawable2.getIntrinsicWidth(), bitmapDrawable2.getIntrinsicHeight());
                        if (openInputStream != null) {
                            openInputStream.close();
                        }
                        return bitmapDrawable2;
                    } catch (Exception e) {
                        e = e;
                        bitmapDrawable = bitmapDrawable2;
                        Log.e("sms", "Failed to loaded content " + this.f659S, e);
                        return bitmapDrawable;
                    }
                } catch (Exception e2) {
                    e = e2;
                    Log.e("sms", "Failed to loaded content " + this.f659S, e);
                    return bitmapDrawable;
                }
            } else {
                try {
                    drawable = ContextCompat.getDrawable(Utils.m891d(), this.f660T);
                    try {
                        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                        return drawable;
                    } catch (Exception unused) {
                    }
                } catch (Exception unused2) {
                    drawable = null;
                    Log.e("sms", "Unable to find resource: " + this.f660T);
                    return drawable;
                }
            }
        }

        private C0858d(Bitmap bitmap, int i) {
            super(i);
            this.f658R = new BitmapDrawable(Utils.m891d().getResources(), bitmap);
            Drawable drawable = this.f658R;
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), this.f658R.getIntrinsicHeight());
        }

        private C0858d(Drawable drawable, int i) {
            super(i);
            this.f658R = drawable;
            Drawable drawable2 = this.f658R;
            drawable2.setBounds(0, 0, drawable2.getIntrinsicWidth(), this.f658R.getIntrinsicHeight());
        }

        private C0858d(Uri uri, int i) {
            super(i);
            this.f659S = uri;
        }

        private C0858d(@DrawableRes int i, int i2) {
            super(i2);
            this.f660T = i;
        }
    }

    /* renamed from: a */
    public SpanUtils mo9735a(@NonNull CharSequence charSequence) {
        if (charSequence != null) {
            m866c(0);
            this.f626b = charSequence;
            return this;
        }
        throw new NullPointerException("Argument 'text' of type CharSequence (#0 out of 1, zero-based) is marked by @androidx.annotation.NonNull but got null for it");
    }

    /* renamed from: a */
    public SpanUtils mo9732a() {
        m866c(0);
        this.f626b = f601X;
        return this;
    }

    /* renamed from: a */
    public static SpanUtils m865a(TextView textView) {
        return new SpanUtils(textView);
    }
}
