package com.blankj.utilcode.util;

import android.app.ActivityManager;
import android.content.Intent;
import android.content.ServiceConnection;
import java.util.List;

/* renamed from: com.blankj.utilcode.util.y */
public final class ServiceUtils {
    /* renamed from: a */
    public static void m1267a(Class<?> cls, ServiceConnection serviceConnection, int i) {
        Utils.m891d().bindService(new Intent(Utils.m891d(), cls), serviceConnection, i);
    }

    /* renamed from: b */
    public static void m1270b(Class<?> cls) {
        Utils.m891d().startService(new Intent(Utils.m891d(), cls));
    }

    /* renamed from: c */
    public static boolean m1271c(Class<?> cls) {
        return Utils.m891d().stopService(new Intent(Utils.m891d(), cls));
    }

    /* renamed from: a */
    public static void m1266a(ServiceConnection serviceConnection) {
        Utils.m891d().unbindService(serviceConnection);
    }

    /* renamed from: a */
    public static boolean m1268a(Class<?> cls) {
        return m1269a(cls.getName());
    }

    /* renamed from: a */
    public static boolean m1269a(String str) {
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) Utils.m891d().getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
        if (!(runningServices == null || runningServices.size() == 0)) {
            for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServices) {
                if (str.equals(runningServiceInfo.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }
}
