package com.blankj.utilcode.util;

import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.GsonBuilder;

/* renamed from: com.blankj.utilcode.util.l */
public final class GsonUtils {

    /* renamed from: a */
    private static final Gson f772a = m1099a(true);

    static {
        m1099a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.f.a(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      e.d.b.f.a(java.lang.Object, e.d.b.a0.a):void
      e.d.b.f.a(e.d.b.w, e.d.b.z.a):e.d.b.v<T>
      e.d.b.f.a(e.d.b.a0.a, java.lang.reflect.Type):T
      e.d.b.f.a(java.io.Reader, java.lang.reflect.Type):T
      e.d.b.f.a(java.lang.String, java.lang.reflect.Type):T
      e.d.b.f.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      e.d.b.f.a(e.d.b.l, e.d.b.a0.c):void
      e.d.b.f.a(e.d.b.l, java.lang.Appendable):void
      e.d.b.f.a(java.lang.String, java.lang.Class):T */
    /* renamed from: a */
    public static <T> T m1100a(String str, Class<T> cls) {
        return f772a.mo23791a(str, (Class) cls);
    }

    /* renamed from: a */
    private static Gson m1099a(boolean z) {
        GsonBuilder gVar = new GsonBuilder();
        if (z) {
            gVar.mo23808b();
        }
        return gVar.mo23807a();
    }
}
