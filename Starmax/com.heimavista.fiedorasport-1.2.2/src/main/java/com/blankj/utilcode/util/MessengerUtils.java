package com.blankj.utilcode.util;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class MessengerUtils {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static ConcurrentHashMap<String, C0851a> f585a = new ConcurrentHashMap<>();

    public static class ServerService extends Service {
        /* access modifiers changed from: private */

        /* renamed from: P */
        public final ConcurrentHashMap<Integer, Messenger> f586P = new ConcurrentHashMap<>();
        @SuppressLint({"HandlerLeak"})

        /* renamed from: Q */
        private final Handler f587Q = new C0850a();

        /* renamed from: R */
        private final Messenger f588R = new Messenger(this.f587Q);

        /* renamed from: com.blankj.utilcode.util.MessengerUtils$ServerService$a */
        class C0850a extends Handler {
            C0850a() {
            }

            public void handleMessage(Message message) {
                int i = message.what;
                if (i == 0) {
                    ServerService.this.f586P.put(Integer.valueOf(message.arg1), message.replyTo);
                } else if (i == 1) {
                    ServerService.this.f586P.remove(Integer.valueOf(message.arg1));
                } else if (i != 2) {
                    super.handleMessage(message);
                } else {
                    ServerService.this.m853b(message);
                    ServerService.this.m851a(message);
                }
            }
        }

        @Nullable
        public IBinder onBind(Intent intent) {
            return this.f588R.getBinder();
        }

        public int onStartCommand(Intent intent, int i, int i2) {
            Bundle extras;
            if (!(intent == null || (extras = intent.getExtras()) == null)) {
                Message obtain = Message.obtain(this.f587Q, 2);
                obtain.replyTo = this.f588R;
                obtain.setData(extras);
                m853b(obtain);
                m851a(obtain);
            }
            return 2;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m853b(Message message) {
            for (Messenger messenger : this.f586P.values()) {
                if (messenger != null) {
                    try {
                        messenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m851a(Message message) {
            String string;
            C0851a aVar;
            Bundle data = message.getData();
            if (data != null && (string = data.getString("MESSENGER_UTILS")) != null && (aVar = (C0851a) MessengerUtils.f585a.get(string)) != null) {
                aVar.mo9727a(data);
            }
        }
    }

    /* renamed from: com.blankj.utilcode.util.MessengerUtils$a */
    public interface C0851a {
        /* renamed from: a */
        void mo9727a(Bundle bundle);
    }

    static {
        new HashMap();
    }
}
