package com.blankj.swipepanel;

public final class R$attr {
    public static final int bottomDrawable = 2130968676;
    public static final int bottomEdgeSize = 2130968677;
    public static final int bottomSwipeColor = 2130968681;
    public static final int isBottomCenter = 2130969023;
    public static final int isBottomEnabled = 2130969024;
    public static final int isLeftCenter = 2130969025;
    public static final int isLeftEnabled = 2130969026;
    public static final int isRightCenter = 2130969029;
    public static final int isRightEnabled = 2130969030;
    public static final int isTopCenter = 2130969031;
    public static final int isTopEnabled = 2130969032;
    public static final int leftDrawable = 2130969139;
    public static final int leftEdgeSize = 2130969140;
    public static final int leftSwipeColor = 2130969141;
    public static final int rightDrawable = 2130969311;
    public static final int rightEdgeSize = 2130969312;
    public static final int rightSwipeColor = 2130969313;
    public static final int topDrawable = 2130969620;
    public static final int topEdgeSize = 2130969621;
    public static final int topSwipeColor = 2130969622;

    private R$attr() {
    }
}
