package com.blankj.swipepanel;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;

public class SwipePanel extends FrameLayout {

    /* renamed from: u0 */
    private static final Object f542u0 = new Object();

    /* renamed from: v0 */
    private static TypedValue f543v0;

    /* renamed from: P */
    private int f544P;

    /* renamed from: Q */
    private int f545Q;

    /* renamed from: R */
    private Paint f546R;

    /* renamed from: S */
    private float f547S;

    /* renamed from: T */
    private float f548T;

    /* renamed from: U */
    private int f549U;

    /* renamed from: V */
    private Path[] f550V;

    /* renamed from: W */
    private int[] f551W;

    /* renamed from: a0 */
    private int[] f552a0;

    /* renamed from: b0 */
    private Drawable[] f553b0;

    /* renamed from: c0 */
    private boolean[] f554c0;

    /* renamed from: d0 */
    private float[] f555d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public float[] f556e0;

    /* renamed from: f0 */
    private float[] f557f0;

    /* renamed from: g0 */
    private boolean[] f558g0;

    /* renamed from: h0 */
    private boolean[] f559h0;

    /* renamed from: i0 */
    private float f560i0;

    /* renamed from: j0 */
    private float f561j0;

    /* renamed from: k0 */
    private float f562k0;

    /* renamed from: l0 */
    private float f563l0;

    /* renamed from: m0 */
    private Rect f564m0;

    /* renamed from: n0 */
    private boolean f565n0;

    /* renamed from: o0 */
    private int f566o0;

    /* renamed from: p0 */
    private int f567p0;

    /* renamed from: q0 */
    private C0848b f568q0;
    /* access modifiers changed from: private */

    /* renamed from: r0 */
    public C0849c f569r0;

    /* renamed from: s0 */
    private float f570s0;

    /* renamed from: t0 */
    private float f571t0;

    /* renamed from: com.blankj.swipepanel.SwipePanel$a */
    class C0847a implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: P */
        final /* synthetic */ int f572P;

        /* renamed from: Q */
        final /* synthetic */ ValueAnimator f573Q;

        C0847a(int i, ValueAnimator valueAnimator) {
            this.f572P = i;
            this.f573Q = valueAnimator;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            SwipePanel.this.f556e0[this.f572P] = ((Float) this.f573Q.getAnimatedValue()).floatValue();
            if (SwipePanel.this.f569r0 != null) {
                SwipePanel.this.f569r0.mo9722a(this.f572P, SwipePanel.this.f556e0[this.f572P], false);
            }
            SwipePanel.this.postInvalidate();
        }
    }

    /* renamed from: com.blankj.swipepanel.SwipePanel$b */
    public interface C0848b {
        /* renamed from: a */
        void mo9721a(int i);
    }

    /* renamed from: com.blankj.swipepanel.SwipePanel$c */
    public interface C0849c {
        /* renamed from: a */
        void mo9722a(int i, float f, boolean z);
    }

    public SwipePanel(@NonNull Context context) {
        this(context, null);
    }

    /* renamed from: c */
    private void m838c(int i) {
        float[] fArr = this.f556e0;
        if (fArr[i] > 0.0f) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr[i], 0.0f);
            ofFloat.addUpdateListener(new C0847a(i, ofFloat));
            ofFloat.setDuration(100L).start();
        }
    }

    /* renamed from: d */
    private void m839d(int i) {
        if (i == 0 || i == 2) {
            if (this.f558g0[i]) {
                this.f555d0[i] = ((float) this.f545Q) / 2.0f;
            } else {
                float f = this.f561j0;
                float f2 = this.f547S;
                if (f < f2) {
                    this.f555d0[i] = f2;
                } else {
                    int i2 = this.f545Q;
                    if (f >= ((float) i2) - f2) {
                        this.f555d0[i] = ((float) i2) - f2;
                    } else {
                        this.f555d0[i] = f;
                    }
                }
            }
        } else if (this.f558g0[i]) {
            this.f555d0[i] = ((float) this.f544P) / 2.0f;
        } else {
            float f3 = this.f560i0;
            float f4 = this.f547S;
            if (f3 < f4) {
                this.f555d0[i] = f4;
            } else {
                int i3 = this.f544P;
                if (f3 >= ((float) i3) - f4) {
                    this.f555d0[i] = ((float) i3) - f4;
                } else {
                    this.f555d0[i] = f3;
                }
            }
        }
        this.f566o0 = i;
        Path[] pathArr = this.f550V;
        if (pathArr[i] == null) {
            pathArr[i] = new Path();
        }
        this.f557f0[i] = 0.0f;
        m834b();
        requestDisallowInterceptTouchEvent(true);
    }

    /* renamed from: e */
    private Path m840e(int i) {
        int i2;
        if (this.f557f0[i] != this.f556e0[i]) {
            this.f550V[i].reset();
            float f = this.f555d0[i];
            int i3 = -1;
            float f2 = 0.0f;
            if (i == 0 || i == 1) {
                i3 = 1;
            } else {
                if (i == 2) {
                    i2 = this.f544P;
                } else {
                    i2 = this.f545Q;
                }
                f2 = (float) i2;
            }
            if (i == 0 || i == 2) {
                this.f570s0 = f2;
                this.f571t0 = f - this.f547S;
            } else {
                this.f570s0 = f - this.f547S;
                this.f571t0 = f2;
            }
            this.f550V[i].moveTo(this.f570s0, this.f571t0);
            m826a(f2, f - this.f547S, i);
            float f3 = this.f556e0[i];
            float f4 = this.f548T;
            float f5 = (float) i3;
            m826a((f3 * f4 * f5) + f2, (f - this.f547S) + (f4 * 5.0f), i);
            m826a((this.f556e0[i] * 10.0f * this.f548T * f5) + f2, f, i);
            float f6 = this.f556e0[i];
            float f7 = this.f548T;
            m826a((f6 * f7 * f5) + f2, (this.f547S + f) - (f7 * 5.0f), i);
            m826a(f2, this.f547S + f, i);
            m826a(f2, f + this.f547S, i);
        }
        return this.f550V[i];
    }

    /* renamed from: f */
    private void m841f(int i) {
        this.f546R.setColor(this.f551W[i]);
        float f = this.f556e0[i];
        if (f < 0.25f) {
            f = 0.25f;
        } else if (f > 0.75f) {
            f = 0.75f;
        }
        this.f546R.setAlpha((int) (f * 255.0f));
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        m828a(canvas);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.swipepanel.SwipePanel.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.blankj.swipepanel.SwipePanel.a(android.content.Context, int):android.graphics.drawable.Drawable
      com.blankj.swipepanel.SwipePanel.a(int, int):void
      com.blankj.swipepanel.SwipePanel.a(android.graphics.Canvas, int):void
      com.blankj.swipepanel.SwipePanel.a(android.graphics.drawable.Drawable, int):void
      com.blankj.swipepanel.SwipePanel.a(boolean, int):void
      com.blankj.swipepanel.SwipePanel.a(int, boolean):void */
    @SuppressLint({"WrongConstant"})
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        super.dispatchTouchEvent(motionEvent);
        int action = motionEvent.getAction();
        boolean z = false;
        if (action == 0) {
            this.f560i0 = motionEvent.getX();
            this.f561j0 = motionEvent.getY();
            this.f554c0[0] = this.f559h0[0] && this.f553b0[0] != null && !mo9686b(0) && this.f560i0 <= ((float) this.f552a0[0]);
            this.f554c0[1] = this.f559h0[1] && this.f553b0[1] != null && !mo9686b(1) && this.f561j0 <= ((float) this.f552a0[1]);
            this.f554c0[2] = this.f559h0[2] && this.f553b0[2] != null && !mo9686b(2) && this.f560i0 >= ((float) (getWidth() - this.f552a0[2]));
            this.f554c0[3] = this.f559h0[3] && this.f553b0[3] != null && !mo9686b(3) && this.f561j0 >= ((float) (getHeight() - this.f552a0[3]));
            boolean[] zArr = this.f554c0;
            if (zArr[0] || zArr[1] || zArr[2] || zArr[3]) {
                z = true;
            }
            this.f565n0 = z;
            if (this.f565n0) {
                this.f566o0 = -1;
            }
            return true;
        }
        if (this.f565n0) {
            if (action == 2) {
                this.f562k0 = motionEvent.getX();
                this.f563l0 = motionEvent.getY();
                if (this.f566o0 == -1) {
                    float f = this.f562k0 - this.f560i0;
                    float f2 = this.f563l0 - this.f561j0;
                    float abs = Math.abs(f);
                    float abs2 = Math.abs(f2);
                    int i = this.f549U;
                    if (abs > ((float) i) || abs2 > ((float) i)) {
                        if (abs >= abs2) {
                            if (this.f554c0[0] && f > 0.0f) {
                                m839d(0);
                            } else if (this.f554c0[2] && f < 0.0f) {
                                m839d(2);
                            }
                        } else if (this.f554c0[1] && f2 > 0.0f) {
                            m839d(1);
                        } else if (this.f554c0[3] && f2 < 0.0f) {
                            m839d(3);
                        }
                    }
                }
                int i2 = this.f566o0;
                if (i2 != -1) {
                    float[] fArr = this.f557f0;
                    float f3 = fArr[i2];
                    float[] fArr2 = this.f556e0;
                    fArr[i2] = fArr2[i2];
                    fArr2[i2] = m823a();
                    if (((double) Math.abs(f3 - this.f556e0[this.f566o0])) > 0.01d) {
                        postInvalidate();
                        C0849c cVar = this.f569r0;
                        if (cVar != null) {
                            int i3 = this.f566o0;
                            cVar.mo9722a(i3, this.f556e0[i3], true);
                        }
                    } else {
                        this.f557f0[this.f566o0] = f3;
                    }
                }
            } else if ((action == 1 || action == 3) && this.f566o0 != -1) {
                this.f562k0 = motionEvent.getX();
                this.f563l0 = motionEvent.getY();
                this.f556e0[this.f566o0] = m823a();
                if (mo9686b(this.f566o0)) {
                    C0848b bVar = this.f568q0;
                    if (bVar != null) {
                        bVar.mo9721a(this.f566o0);
                    }
                } else {
                    mo9684a(this.f566o0, true);
                }
            }
        }
        return true;
    }

    public Drawable getBottomDrawable() {
        return this.f553b0[3];
    }

    public Drawable getLeftDrawable() {
        return this.f553b0[0];
    }

    public Drawable getRightDrawable() {
        return this.f553b0[2];
    }

    public Drawable getTopDrawable() {
        return this.f553b0[1];
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.f544P = getMeasuredWidth();
        this.f545Q = getMeasuredHeight();
        this.f567p0 = Math.min(this.f544P, this.f545Q) / 3;
    }

    public void setBottomCenter(boolean z) {
        m831a(z, 3);
    }

    public void setBottomDrawable(@DrawableRes int i) {
        m827a(i, 3);
    }

    public void setBottomEdgeSize(int i) {
        this.f552a0[3] = i;
    }

    public void setBottomEnabled(boolean z) {
        m837b(z, 3);
    }

    public void setBottomSwipeColor(int i) {
        m835b(i, 3);
    }

    public void setLeftCenter(boolean z) {
        m831a(z, 0);
    }

    public void setLeftDrawable(@DrawableRes int i) {
        m827a(i, 0);
    }

    public void setLeftEdgeSize(int i) {
        this.f552a0[0] = i;
    }

    public void setLeftEnabled(boolean z) {
        m837b(z, 0);
    }

    public void setLeftSwipeColor(int i) {
        m835b(i, 0);
    }

    public void setOnFullSwipeListener(C0848b bVar) {
        this.f568q0 = bVar;
    }

    public void setOnProgressChangedListener(C0849c cVar) {
        this.f569r0 = cVar;
    }

    public void setRightCenter(boolean z) {
        m831a(z, 2);
    }

    public void setRightDrawable(@DrawableRes int i) {
        m827a(i, 2);
    }

    public void setRightEdgeSize(int i) {
        this.f552a0[2] = i;
    }

    public void setRightEnabled(boolean z) {
        m837b(z, 2);
    }

    public void setRightSwipeColor(int i) {
        m835b(i, 2);
    }

    public void setTopCenter(boolean z) {
        m831a(z, 1);
    }

    public void setTopDrawable(@DrawableRes int i) {
        m827a(i, 1);
    }

    public void setTopEdgeSize(int i) {
        this.f552a0[1] = i;
    }

    public void setTopEnabled(boolean z) {
        m837b(z, 1);
    }

    public void setTopSwipeColor(int i) {
        m835b(i, 1);
    }

    public SwipePanel(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f550V = new Path[4];
        this.f551W = new int[4];
        this.f552a0 = new int[4];
        this.f553b0 = new Drawable[4];
        this.f554c0 = new boolean[4];
        this.f555d0 = new float[4];
        this.f556e0 = new float[4];
        this.f557f0 = new float[4];
        this.f558g0 = new boolean[4];
        this.f559h0 = new boolean[]{true, true, true, true};
        this.f564m0 = new Rect();
        this.f566o0 = -1;
        int scaledEdgeSlop = ViewConfiguration.get(context).getScaledEdgeSlop();
        this.f549U = ViewConfiguration.get(context).getScaledTouchSlop();
        this.f546R = new Paint(5);
        this.f546R.setStyle(Paint.Style.FILL);
        this.f547S = (float) m824a(72.0f);
        this.f548T = this.f547S / 16.0f;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.SwipePanel);
            setLeftSwipeColor(obtainStyledAttributes.getColor(R$styleable.SwipePanel_leftSwipeColor, ViewCompat.MEASURED_STATE_MASK));
            setTopSwipeColor(obtainStyledAttributes.getColor(R$styleable.SwipePanel_topSwipeColor, ViewCompat.MEASURED_STATE_MASK));
            setRightSwipeColor(obtainStyledAttributes.getColor(R$styleable.SwipePanel_rightSwipeColor, ViewCompat.MEASURED_STATE_MASK));
            setBottomSwipeColor(obtainStyledAttributes.getColor(R$styleable.SwipePanel_bottomSwipeColor, ViewCompat.MEASURED_STATE_MASK));
            setLeftEdgeSize(obtainStyledAttributes.getDimensionPixelSize(R$styleable.SwipePanel_leftEdgeSize, scaledEdgeSlop));
            setTopEdgeSize(obtainStyledAttributes.getDimensionPixelSize(R$styleable.SwipePanel_topEdgeSize, scaledEdgeSlop));
            setRightEdgeSize(obtainStyledAttributes.getDimensionPixelSize(R$styleable.SwipePanel_rightEdgeSize, scaledEdgeSlop));
            setBottomEdgeSize(obtainStyledAttributes.getDimensionPixelSize(R$styleable.SwipePanel_bottomEdgeSize, scaledEdgeSlop));
            setLeftDrawable(obtainStyledAttributes.getDrawable(R$styleable.SwipePanel_leftDrawable));
            setTopDrawable(obtainStyledAttributes.getDrawable(R$styleable.SwipePanel_topDrawable));
            setRightDrawable(obtainStyledAttributes.getDrawable(R$styleable.SwipePanel_rightDrawable));
            setBottomDrawable(obtainStyledAttributes.getDrawable(R$styleable.SwipePanel_bottomDrawable));
            setLeftCenter(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isLeftCenter, false));
            setTopCenter(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isTopCenter, false));
            setRightCenter(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isRightCenter, false));
            setBottomCenter(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isBottomCenter, false));
            setLeftEnabled(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isLeftEnabled, true));
            setTopEnabled(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isTopEnabled, true));
            setRightEnabled(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isRightEnabled, true));
            setBottomEnabled(obtainStyledAttributes.getBoolean(R$styleable.SwipePanel_isBottomEnabled, true));
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: a */
    private void m827a(int i, int i2) {
        this.f553b0[i2] = m825a(getContext(), i);
    }

    /* renamed from: b */
    private void m835b(int i, int i2) {
        this.f551W[i2] = i;
    }

    public void setBottomDrawable(Drawable drawable) {
        m830a(drawable, 3);
    }

    public void setLeftDrawable(Drawable drawable) {
        m830a(drawable, 0);
    }

    public void setRightDrawable(Drawable drawable) {
        m830a(drawable, 2);
    }

    public void setTopDrawable(Drawable drawable) {
        m830a(drawable, 1);
    }

    /* renamed from: a */
    private void m830a(Drawable drawable, int i) {
        this.f553b0[i] = drawable;
    }

    /* renamed from: b */
    private void m837b(boolean z, int i) {
        this.f559h0[i] = z;
    }

    /* renamed from: a */
    private void m831a(boolean z, int i) {
        this.f558g0[i] = z;
    }

    /* renamed from: b */
    public boolean mo9686b(int i) {
        return this.f556e0[i] >= 0.95f;
    }

    /* renamed from: b */
    private void m836b(Canvas canvas, int i) {
        if (this.f550V[i] != null && this.f556e0[i] > 0.0f) {
            m841f(i);
            canvas.drawPath(m840e(i), this.f546R);
            m829a(canvas, i);
        }
    }

    /* renamed from: a */
    public void mo9685a(@NonNull View view) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) parent;
            int indexOfChild = viewGroup.indexOfChild(view);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            viewGroup.removeViewAt(indexOfChild);
            viewGroup.addView(this, indexOfChild, layoutParams);
            addView(view, -1, -1);
            return;
        }
        addView(view, -1, -1);
    }

    /* renamed from: b */
    private void m834b() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).dispatchTouchEvent(obtain);
        }
        obtain.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.swipepanel.SwipePanel.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.blankj.swipepanel.SwipePanel.a(android.content.Context, int):android.graphics.drawable.Drawable
      com.blankj.swipepanel.SwipePanel.a(int, int):void
      com.blankj.swipepanel.SwipePanel.a(android.graphics.Canvas, int):void
      com.blankj.swipepanel.SwipePanel.a(android.graphics.drawable.Drawable, int):void
      com.blankj.swipepanel.SwipePanel.a(boolean, int):void
      com.blankj.swipepanel.SwipePanel.a(int, boolean):void */
    /* renamed from: a */
    public void mo9683a(int i) {
        mo9684a(i, true);
    }

    /* renamed from: a */
    public void mo9684a(int i, boolean z) {
        if (z) {
            m838c(i);
            return;
        }
        this.f556e0[i] = 0.0f;
        postInvalidate();
    }

    /* renamed from: a */
    private void m828a(Canvas canvas) {
        m836b(canvas, 0);
        m836b(canvas, 1);
        m836b(canvas, 2);
        m836b(canvas, 3);
    }

    /* renamed from: a */
    private void m829a(Canvas canvas, int i) {
        int i2;
        int i3;
        int i4;
        Drawable[] drawableArr = this.f553b0;
        if (drawableArr[i] != null) {
            int intrinsicWidth = drawableArr[i].getIntrinsicWidth();
            int intrinsicHeight = this.f553b0[i].getIntrinsicHeight();
            int i5 = (int) (this.f556e0[i] * 5.0f * this.f548T);
            if (intrinsicWidth >= intrinsicHeight) {
                i4 = (intrinsicHeight * i5) / intrinsicWidth;
                i2 = i5 - i4;
                i3 = 0;
            } else {
                int i6 = (intrinsicWidth * i5) / intrinsicHeight;
                i3 = i5 - i6;
                i2 = 0;
                int i7 = i5;
                i5 = i6;
                i4 = i7;
            }
            if (i == 0) {
                Rect rect = this.f564m0;
                rect.left = (int) ((this.f556e0[i] * this.f548T * 1.0f) + 0.0f + ((float) ((i3 / 2) * 1)));
                rect.top = (int) (this.f555d0[0] - ((float) (i4 / 2)));
                rect.right = rect.left + i5;
                rect.bottom = rect.top + i4;
            } else if (i == 2) {
                Rect rect2 = this.f564m0;
                rect2.right = (int) (((float) this.f544P) + (this.f556e0[i] * this.f548T * -1.0f) + ((float) ((i3 / 2) * -1)));
                rect2.top = (int) (this.f555d0[2] - (((float) i4) / 2.0f));
                rect2.left = rect2.right - i5;
                rect2.bottom = rect2.top + i4;
            } else if (i == 1) {
                Rect rect3 = this.f564m0;
                rect3.left = (int) (this.f555d0[1] - ((float) (i5 / 2)));
                rect3.top = (int) ((this.f556e0[i] * this.f548T * 1.0f) + 0.0f + ((float) ((i2 / 2) * 1)));
                rect3.right = rect3.left + i5;
                rect3.bottom = rect3.top + i4;
            } else {
                Rect rect4 = this.f564m0;
                rect4.left = (int) (this.f555d0[3] - ((float) (i5 / 2)));
                rect4.bottom = (int) (((float) this.f545Q) + (this.f556e0[i] * this.f548T * -1.0f) + ((float) ((i2 / 2) * -1)));
                rect4.top = rect4.bottom - i4;
                rect4.right = rect4.left + i5;
            }
            this.f553b0[i].setBounds(this.f564m0);
            this.f553b0[i].draw(canvas);
        }
    }

    /* renamed from: a */
    private void m826a(float f, float f2, int i) {
        float f3 = this.f570s0;
        float f4 = this.f571t0;
        if (i == 0 || i == 2) {
            this.f570s0 = f;
            this.f571t0 = f2;
        } else {
            this.f570s0 = f2;
            this.f571t0 = f;
        }
        this.f550V[i].quadTo(f3, f4, (this.f570s0 + f3) / 2.0f, (this.f571t0 + f4) / 2.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* renamed from: a */
    private float m823a() {
        int i = this.f566o0;
        if (i == 0) {
            float f = this.f562k0 - this.f560i0;
            if (f <= 0.0f) {
                return 0.0f;
            }
            return Math.min(f / ((float) this.f567p0), 1.0f);
        } else if (i == 1) {
            float f2 = this.f563l0 - this.f561j0;
            if (f2 <= 0.0f) {
                return 0.0f;
            }
            return Math.min(f2 / ((float) this.f567p0), 1.0f);
        } else if (i == 2) {
            float f3 = this.f562k0 - this.f560i0;
            if (f3 >= 0.0f) {
                return 0.0f;
            }
            return Math.min((-f3) / ((float) this.f567p0), 1.0f);
        } else {
            float f4 = this.f563l0 - this.f561j0;
            if (f4 >= 0.0f) {
                return 0.0f;
            }
            return Math.min((-f4) / ((float) this.f567p0), 1.0f);
        }
    }

    /* renamed from: a */
    private static int m824a(float f) {
        return (int) ((f * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* renamed from: a */
    private static Drawable m825a(@NonNull Context context, @DrawableRes int i) {
        int i2;
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            return context.getDrawable(i);
        }
        if (i3 >= 16) {
            return context.getResources().getDrawable(i);
        }
        synchronized (f542u0) {
            if (f543v0 == null) {
                f543v0 = new TypedValue();
            }
            context.getResources().getValue(i, f543v0, true);
            i2 = f543v0.resourceId;
        }
        return context.getResources().getDrawable(i2);
    }
}
