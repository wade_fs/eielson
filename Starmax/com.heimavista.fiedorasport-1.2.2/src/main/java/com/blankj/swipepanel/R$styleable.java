package com.blankj.swipepanel;

public final class R$styleable {
    public static final int[] SwipePanel = {2130968676, 2130968677, 2130968681, 2130969023, 2130969024, 2130969025, 2130969026, 2130969029, 2130969030, 2130969031, 2130969032, 2130969139, 2130969140, 2130969141, 2130969311, 2130969312, 2130969313, 2130969620, 2130969621, 2130969622};
    public static final int SwipePanel_bottomDrawable = 0;
    public static final int SwipePanel_bottomEdgeSize = 1;
    public static final int SwipePanel_bottomSwipeColor = 2;
    public static final int SwipePanel_isBottomCenter = 3;
    public static final int SwipePanel_isBottomEnabled = 4;
    public static final int SwipePanel_isLeftCenter = 5;
    public static final int SwipePanel_isLeftEnabled = 6;
    public static final int SwipePanel_isRightCenter = 7;
    public static final int SwipePanel_isRightEnabled = 8;
    public static final int SwipePanel_isTopCenter = 9;
    public static final int SwipePanel_isTopEnabled = 10;
    public static final int SwipePanel_leftDrawable = 11;
    public static final int SwipePanel_leftEdgeSize = 12;
    public static final int SwipePanel_leftSwipeColor = 13;
    public static final int SwipePanel_rightDrawable = 14;
    public static final int SwipePanel_rightEdgeSize = 15;
    public static final int SwipePanel_rightSwipeColor = 16;
    public static final int SwipePanel_topDrawable = 17;
    public static final int SwipePanel_topEdgeSize = 18;
    public static final int SwipePanel_topSwipeColor = 19;

    private R$styleable() {
    }
}
