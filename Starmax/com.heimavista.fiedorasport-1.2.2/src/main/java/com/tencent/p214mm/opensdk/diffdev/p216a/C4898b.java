package com.tencent.p214mm.opensdk.diffdev.p216a;

import com.tencent.p214mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p214mm.opensdk.diffdev.OAuthListener;
import com.tencent.p214mm.opensdk.utils.Log;
import java.util.ArrayList;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.b */
final class C4898b implements OAuthListener {

    /* renamed from: f */
    final /* synthetic */ C4897a f11080f;

    C4898b(C4897a aVar) {
        this.f11080f = aVar;
    }

    public final void onAuthFinish(OAuthErrCode oAuthErrCode, String str) {
        Log.m17063d("MicroMsg.SDK.ListenerWrapper", String.format("onAuthFinish, errCode = %s, authCode = %s", oAuthErrCode.toString(), str));
        C4900d unused = this.f11080f.f11078d = null;
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f11080f.f11077c);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onAuthFinish(oAuthErrCode, str);
        }
    }

    public final void onAuthGotQrcode(String str, byte[] bArr) {
        Log.m17063d("MicroMsg.SDK.ListenerWrapper", "onAuthGotQrcode, qrcodeImgPath = " + str);
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f11080f.f11077c);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onAuthGotQrcode(str, bArr);
        }
    }

    public final void onQrcodeScanned() {
        Log.m17063d("MicroMsg.SDK.ListenerWrapper", "onQrcodeScanned");
        if (this.f11080f.handler != null) {
            this.f11080f.handler.post(new C4899c(this));
        }
    }
}
