package com.tencent.p214mm.opensdk.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.tencent.mm.opensdk.utils.d */
public final class C4912d {

    /* renamed from: D */
    public static Context f11109D;

    /* renamed from: E */
    private static final int f11110E;

    /* renamed from: F */
    private static final int f11111F;

    /* renamed from: G */
    private static final int f11112G = ((f11110E * 2) + 1);

    /* renamed from: H */
    public static ThreadPoolExecutor f11113H = new ThreadPoolExecutor(f11111F, f11112G, 1, TimeUnit.SECONDS, new LinkedBlockingDeque());

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        f11110E = availableProcessors;
        f11111F = availableProcessors + 1;
    }

    /* renamed from: a */
    private static int m17072a(ContentResolver contentResolver, Uri uri) {
        Log.m17065i("MicroMsg.SDK.Util", "getFileSize with content url");
        if (contentResolver == null || uri == null) {
            Log.m17067w("MicroMsg.SDK.Util", "getFileSize fail, resolver or uri is null");
            return 0;
        }
        InputStream inputStream = null;
        try {
            inputStream = contentResolver.openInputStream(uri);
            if (inputStream == null) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                    }
                }
                return 0;
            }
            int available = inputStream.available();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused2) {
                }
            }
            return available;
        } catch (Exception e) {
            Log.m17067w("MicroMsg.SDK.Util", "getFileSize fail, " + e.getMessage());
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused3) {
                }
            }
            return 0;
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused4) {
                }
            }
            throw th;
        }
    }

    /* renamed from: a */
    public static boolean m17073a(int i) {
        return i == 36 || i == 46;
    }

    /* renamed from: b */
    public static boolean m17074b(String str) {
        return str == null || str.length() <= 0;
    }

    /* renamed from: c */
    public static int m17075c(String str) {
        if (str != null) {
            try {
                if (str.length() > 0) {
                    return Integer.parseInt(str);
                }
            } catch (Exception unused) {
            }
        }
        return 0;
    }

    public static int getFileSize(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        File file = new File(str);
        if (file.exists()) {
            return (int) file.length();
        }
        if (f11109D != null && str.startsWith("content")) {
            try {
                return m17072a(f11109D.getContentResolver(), Uri.parse(str));
            } catch (Exception unused) {
            }
        }
        return 0;
    }
}
