package com.tencent.p214mm.opensdk.utils;

/* renamed from: com.tencent.mm.opensdk.utils.Log */
public class Log {
    private static ILog logImpl;

    /* renamed from: d */
    public static void m17063d(String str, String str2) {
        ILog iLog = logImpl;
        if (iLog == null) {
            android.util.Log.d(str, str2);
        } else {
            iLog.mo27018d(str, str2);
        }
    }

    /* renamed from: e */
    public static void m17064e(String str, String str2) {
        ILog iLog = logImpl;
        if (iLog == null) {
            android.util.Log.e(str, str2);
        } else {
            iLog.mo27019e(str, str2);
        }
    }

    /* renamed from: i */
    public static void m17065i(String str, String str2) {
        ILog iLog = logImpl;
        if (iLog == null) {
            android.util.Log.i(str, str2);
        } else {
            iLog.mo27020i(str, str2);
        }
    }

    public static void setLogImpl(ILog iLog) {
        logImpl = iLog;
    }

    /* renamed from: v */
    public static void m17066v(String str, String str2) {
        ILog iLog = logImpl;
        if (iLog == null) {
            android.util.Log.v(str, str2);
        } else {
            iLog.mo27021v(str, str2);
        }
    }

    /* renamed from: w */
    public static void m17067w(String str, String str2) {
        ILog iLog = logImpl;
        if (iLog == null) {
            android.util.Log.w(str, str2);
        } else {
            iLog.mo27022w(str, str2);
        }
    }
}
