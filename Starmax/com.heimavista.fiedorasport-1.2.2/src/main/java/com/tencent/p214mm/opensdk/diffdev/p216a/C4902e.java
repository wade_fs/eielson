package com.tencent.p214mm.opensdk.diffdev.p216a;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.e */
public final class C4902e {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:114:0x0117 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:96:0x00ec */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:78:0x00c1 */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v8, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v30 */
    /* JADX WARN: Type inference failed for: r4v31 */
    /* JADX WARN: Type inference failed for: r4v32 */
    /* JADX WARN: Type inference failed for: r4v33 */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0076 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0108 A[SYNTHETIC, Splitter:B:103:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x010f A[SYNTHETIC, Splitter:B:107:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x012c A[SYNTHETIC, Splitter:B:117:0x012c] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0133 A[SYNTHETIC, Splitter:B:121:0x0133] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x013a A[SYNTHETIC, Splitter:B:125:0x013a] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0141 A[SYNTHETIC, Splitter:B:131:0x0141] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0148 A[SYNTHETIC, Splitter:B:135:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x014f A[SYNTHETIC, Splitter:B:139:0x014f] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00d6 A[SYNTHETIC, Splitter:B:81:0x00d6] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x00dd A[SYNTHETIC, Splitter:B:85:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x00e4 A[SYNTHETIC, Splitter:B:89:0x00e4] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0101 A[SYNTHETIC, Splitter:B:99:0x0101] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:78:0x00c1=Splitter:B:78:0x00c1, B:114:0x0117=Splitter:B:114:0x0117, B:96:0x00ec=Splitter:B:96:0x00ec} */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] m17056a(java.lang.String r9) {
        /*
            java.lang.String r0 = "httpGet ex:"
            java.lang.String r1 = "MicroMsg.SDK.NetUtil"
            r2 = 0
            if (r9 == 0) goto L_0x0153
            int r3 = r9.length()
            if (r3 != 0) goto L_0x000f
            goto L_0x0153
        L_0x000f:
            java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0113, IOException -> 0x00e8, Exception -> 0x00bd, all -> 0x00b7 }
            r3.<init>(r9)     // Catch:{ MalformedURLException -> 0x0113, IOException -> 0x00e8, Exception -> 0x00bd, all -> 0x00b7 }
            java.net.URLConnection r9 = r3.openConnection()     // Catch:{ MalformedURLException -> 0x0113, IOException -> 0x00e8, Exception -> 0x00bd, all -> 0x00b7 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ MalformedURLException -> 0x0113, IOException -> 0x00e8, Exception -> 0x00bd, all -> 0x00b7 }
            if (r9 != 0) goto L_0x0027
            java.lang.String r3 = "open connection failed."
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r3)     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            if (r9 == 0) goto L_0x0026
            r9.disconnect()     // Catch:{ all -> 0x0026 }
        L_0x0026:
            return r2
        L_0x0027:
            java.lang.String r3 = "GET"
            r9.setRequestMethod(r3)     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            r3 = 60000(0xea60, float:8.4078E-41)
            r9.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            r9.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            int r3 = r9.getResponseCode()     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            r4 = 300(0x12c, float:4.2E-43)
            if (r3 < r4) goto L_0x0048
            java.lang.String r3 = "httpURLConnectionGet 300"
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r3)     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            if (r9 == 0) goto L_0x0047
            r9.disconnect()     // Catch:{ all -> 0x0047 }
        L_0x0047:
            return r2
        L_0x0048:
            java.io.InputStream r3 = r9.getInputStream()     // Catch:{ MalformedURLException -> 0x00b3, IOException -> 0x00b0, Exception -> 0x00ad, all -> 0x00aa }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ MalformedURLException -> 0x00a3, IOException -> 0x009d, Exception -> 0x0097, all -> 0x0092 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x00a3, IOException -> 0x009d, Exception -> 0x0097, all -> 0x0092 }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r5]     // Catch:{ MalformedURLException -> 0x008b, IOException -> 0x0084, Exception -> 0x007d, all -> 0x007a }
        L_0x0055:
            int r6 = r3.read(r5)     // Catch:{ MalformedURLException -> 0x008b, IOException -> 0x0084, Exception -> 0x007d, all -> 0x007a }
            r7 = -1
            if (r6 == r7) goto L_0x0061
            r7 = 0
            r4.write(r5, r7, r6)     // Catch:{ MalformedURLException -> 0x008b, IOException -> 0x0084, Exception -> 0x007d, all -> 0x007a }
            goto L_0x0055
        L_0x0061:
            byte[] r5 = r4.toByteArray()     // Catch:{ MalformedURLException -> 0x008b, IOException -> 0x0084, Exception -> 0x007d, all -> 0x007a }
            java.lang.String r6 = "httpGet end"
            com.tencent.p214mm.opensdk.utils.Log.m17063d(r1, r6)     // Catch:{ MalformedURLException -> 0x008b, IOException -> 0x0084, Exception -> 0x007d, all -> 0x007a }
            if (r9 == 0) goto L_0x0071
            r9.disconnect()     // Catch:{ all -> 0x0070 }
            goto L_0x0071
        L_0x0070:
        L_0x0071:
            if (r3 == 0) goto L_0x0076
            r3.close()     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r4.close()     // Catch:{ all -> 0x0079 }
        L_0x0079:
            return r5
        L_0x007a:
            r0 = move-exception
            r5 = r4
            goto L_0x0094
        L_0x007d:
            r5 = move-exception
            r8 = r4
            r4 = r3
            r3 = r5
            r5 = r8
            goto L_0x00c1
        L_0x0084:
            r5 = move-exception
            r8 = r4
            r4 = r3
            r3 = r5
            r5 = r8
            goto L_0x00ec
        L_0x008b:
            r5 = move-exception
            r8 = r4
            r4 = r3
            r3 = r5
            r5 = r8
            goto L_0x0117
        L_0x0092:
            r0 = move-exception
            r5 = r2
        L_0x0094:
            r4 = r3
            goto L_0x013f
        L_0x0097:
            r4 = move-exception
            r5 = r2
            r8 = r4
            r4 = r3
            r3 = r8
            goto L_0x00c1
        L_0x009d:
            r4 = move-exception
            r5 = r2
            r8 = r4
            r4 = r3
            r3 = r8
            goto L_0x00ec
        L_0x00a3:
            r4 = move-exception
            r5 = r2
            r8 = r4
            r4 = r3
            r3 = r8
            goto L_0x0117
        L_0x00aa:
            r0 = move-exception
            r4 = r2
            goto L_0x00ba
        L_0x00ad:
            r3 = move-exception
            r4 = r2
            goto L_0x00c0
        L_0x00b0:
            r3 = move-exception
            r4 = r2
            goto L_0x00eb
        L_0x00b3:
            r3 = move-exception
            r4 = r2
            goto L_0x0116
        L_0x00b7:
            r0 = move-exception
            r9 = r2
            r4 = r9
        L_0x00ba:
            r5 = r4
            goto L_0x013f
        L_0x00bd:
            r3 = move-exception
            r9 = r2
            r4 = r9
        L_0x00c0:
            r5 = r4
        L_0x00c1:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x013e }
            r6.<init>(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x013e }
            r6.append(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x013e }
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r0)     // Catch:{ all -> 0x013e }
            if (r9 == 0) goto L_0x00db
            r9.disconnect()     // Catch:{ all -> 0x00da }
            goto L_0x00db
        L_0x00da:
        L_0x00db:
            if (r4 == 0) goto L_0x00e2
            r4.close()     // Catch:{ all -> 0x00e1 }
            goto L_0x00e2
        L_0x00e1:
        L_0x00e2:
            if (r5 == 0) goto L_0x00e7
            r5.close()     // Catch:{ all -> 0x00e7 }
        L_0x00e7:
            return r2
        L_0x00e8:
            r3 = move-exception
            r9 = r2
            r4 = r9
        L_0x00eb:
            r5 = r4
        L_0x00ec:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x013e }
            r6.<init>(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x013e }
            r6.append(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x013e }
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r0)     // Catch:{ all -> 0x013e }
            if (r9 == 0) goto L_0x0106
            r9.disconnect()     // Catch:{ all -> 0x0105 }
            goto L_0x0106
        L_0x0105:
        L_0x0106:
            if (r4 == 0) goto L_0x010d
            r4.close()     // Catch:{ all -> 0x010c }
            goto L_0x010d
        L_0x010c:
        L_0x010d:
            if (r5 == 0) goto L_0x0112
            r5.close()     // Catch:{ all -> 0x0112 }
        L_0x0112:
            return r2
        L_0x0113:
            r3 = move-exception
            r9 = r2
            r4 = r9
        L_0x0116:
            r5 = r4
        L_0x0117:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x013e }
            r6.<init>(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x013e }
            r6.append(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x013e }
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r0)     // Catch:{ all -> 0x013e }
            if (r9 == 0) goto L_0x0131
            r9.disconnect()     // Catch:{ all -> 0x0130 }
            goto L_0x0131
        L_0x0130:
        L_0x0131:
            if (r4 == 0) goto L_0x0138
            r4.close()     // Catch:{ all -> 0x0137 }
            goto L_0x0138
        L_0x0137:
        L_0x0138:
            if (r5 == 0) goto L_0x013d
            r5.close()     // Catch:{ all -> 0x013d }
        L_0x013d:
            return r2
        L_0x013e:
            r0 = move-exception
        L_0x013f:
            if (r9 == 0) goto L_0x0146
            r9.disconnect()     // Catch:{ all -> 0x0145 }
            goto L_0x0146
        L_0x0145:
        L_0x0146:
            if (r4 == 0) goto L_0x014d
            r4.close()     // Catch:{ all -> 0x014c }
            goto L_0x014d
        L_0x014c:
        L_0x014d:
            if (r5 == 0) goto L_0x0152
            r5.close()     // Catch:{ all -> 0x0152 }
        L_0x0152:
            throw r0
        L_0x0153:
            java.lang.String r9 = "httpGet, url is null"
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r9)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.p214mm.opensdk.diffdev.p216a.C4902e.m17056a(java.lang.String):byte[]");
    }
}
