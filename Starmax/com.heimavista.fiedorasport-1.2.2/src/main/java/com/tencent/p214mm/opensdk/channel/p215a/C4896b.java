package com.tencent.p214mm.opensdk.channel.p215a;

import com.tencent.p214mm.opensdk.utils.C4908b;

/* renamed from: com.tencent.mm.opensdk.channel.a.b */
public final class C4896b {
    /* renamed from: a */
    public static byte[] m17050a(String str, int i, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            stringBuffer.append(str);
        }
        stringBuffer.append(i);
        stringBuffer.append(str2);
        stringBuffer.append("mMcShCsTr");
        return C4908b.m17070c(stringBuffer.toString().substring(1, 9).getBytes()).getBytes();
    }
}
