package com.tencent.p214mm.opensdk.diffdev.p216a;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.tencent.p214mm.opensdk.diffdev.IDiffDevOAuth;
import com.tencent.p214mm.opensdk.diffdev.OAuthListener;
import com.tencent.p214mm.opensdk.utils.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.a */
public final class C4897a implements IDiffDevOAuth {
    /* access modifiers changed from: private */

    /* renamed from: c */
    public List<OAuthListener> f11077c = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C4900d f11078d;

    /* renamed from: e */
    private OAuthListener f11079e = new C4898b(this);
    /* access modifiers changed from: private */
    public Handler handler = null;

    public final void addListener(OAuthListener oAuthListener) {
        if (!this.f11077c.contains(oAuthListener)) {
            this.f11077c.add(oAuthListener);
        }
    }

    public final boolean auth(String str, String str2, String str3, String str4, String str5, OAuthListener oAuthListener) {
        String str6 = str;
        Log.m17065i("MicroMsg.SDK.DiffDevOAuth", "start auth, appId = " + str);
        if (str6 == null || str.length() <= 0 || str2 == null || str2.length() <= 0) {
            Log.m17063d("MicroMsg.SDK.DiffDevOAuth", String.format("auth fail, invalid argument, appId = %s, scope = %s", str6, str2));
            return false;
        }
        if (this.handler == null) {
            this.handler = new Handler(Looper.getMainLooper());
        }
        addListener(oAuthListener);
        if (this.f11078d != null) {
            Log.m17063d("MicroMsg.SDK.DiffDevOAuth", "auth, already running, no need to start auth again");
            return true;
        }
        this.f11078d = new C4900d(str, str2, str3, str4, str5, this.f11079e);
        C4900d dVar = this.f11078d;
        if (Build.VERSION.SDK_INT >= 11) {
            dVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } else {
            dVar.execute(new Void[0]);
        }
        return true;
    }

    public final void detach() {
        Log.m17065i("MicroMsg.SDK.DiffDevOAuth", "detach");
        this.f11077c.clear();
        stopAuth();
    }

    public final void removeAllListeners() {
        this.f11077c.clear();
    }

    public final void removeListener(OAuthListener oAuthListener) {
        this.f11077c.remove(oAuthListener);
    }

    public final boolean stopAuth() {
        boolean z;
        Log.m17065i("MicroMsg.SDK.DiffDevOAuth", "stopAuth");
        try {
            z = this.f11078d == null ? true : this.f11078d.mo26955a();
        } catch (Exception e) {
            Log.m17067w("MicroMsg.SDK.DiffDevOAuth", "stopAuth fail, ex = " + e.getMessage());
            z = false;
        }
        this.f11078d = null;
        return z;
    }
}
