package com.tencent.p214mm.opensdk.diffdev.p216a;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import com.tencent.p214mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p214mm.opensdk.diffdev.OAuthListener;
import com.tencent.p214mm.opensdk.utils.Log;
import org.json.JSONObject;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.d */
public final class C4900d extends AsyncTask<Void, Void, C4901a> {

    /* renamed from: h */
    private static String f11082h = "https://open.weixin.qq.com/connect/sdk/qrconnect?appid=%s&noncestr=%s&timestamp=%s&scope=%s&signature=%s";
    private String appId;

    /* renamed from: i */
    private String f11083i;

    /* renamed from: j */
    private String f11084j;

    /* renamed from: k */
    private OAuthListener f11085k;

    /* renamed from: l */
    private C4903f f11086l;
    private String scope;
    private String signature;

    /* renamed from: com.tencent.mm.opensdk.diffdev.a.d$a */
    static class C4901a {

        /* renamed from: m */
        public OAuthErrCode f11087m;

        /* renamed from: n */
        public String f11088n;

        /* renamed from: o */
        public String f11089o;

        /* renamed from: p */
        public String f11090p;

        /* renamed from: q */
        public int f11091q;

        /* renamed from: r */
        public String f11092r;

        /* renamed from: s */
        public byte[] f11093s;

        private C4901a() {
        }

        /* renamed from: a */
        public static C4901a m17055a(byte[] bArr) {
            OAuthErrCode oAuthErrCode;
            String format;
            C4901a aVar = new C4901a();
            if (bArr == null || bArr.length == 0) {
                Log.m17064e("MicroMsg.SDK.GetQRCodeResult", "parse fail, buf is null");
                oAuthErrCode = OAuthErrCode.WechatAuth_Err_NetworkErr;
            } else {
                try {
                    try {
                        JSONObject jSONObject = new JSONObject(new String(bArr, "utf-8"));
                        int i = jSONObject.getInt("errcode");
                        if (i != 0) {
                            Log.m17064e("MicroMsg.SDK.GetQRCodeResult", String.format("resp errcode = %d", Integer.valueOf(i)));
                            aVar.f11087m = OAuthErrCode.WechatAuth_Err_NormalErr;
                            aVar.f11091q = i;
                            aVar.f11092r = jSONObject.optString("errmsg");
                            return aVar;
                        }
                        String string = jSONObject.getJSONObject("qrcode").getString("qrcodebase64");
                        if (string != null) {
                            if (string.length() != 0) {
                                byte[] decode = Base64.decode(string, 0);
                                if (decode != null) {
                                    if (decode.length != 0) {
                                        aVar.f11087m = OAuthErrCode.WechatAuth_Err_OK;
                                        aVar.f11093s = decode;
                                        aVar.f11088n = jSONObject.getString("uuid");
                                        aVar.f11089o = jSONObject.getString("appname");
                                        Log.m17063d("MicroMsg.SDK.GetQRCodeResult", String.format("parse succ, save in memory, uuid = %s, appname = %s, imgBufLength = %d", aVar.f11088n, aVar.f11089o, Integer.valueOf(aVar.f11093s.length)));
                                        return aVar;
                                    }
                                }
                                Log.m17064e("MicroMsg.SDK.GetQRCodeResult", "parse fail, qrcodeBuf is null");
                                aVar.f11087m = OAuthErrCode.WechatAuth_Err_JsonDecodeErr;
                                return aVar;
                            }
                        }
                        Log.m17064e("MicroMsg.SDK.GetQRCodeResult", "parse fail, qrcodeBase64 is null");
                        aVar.f11087m = OAuthErrCode.WechatAuth_Err_JsonDecodeErr;
                        return aVar;
                    } catch (Exception e) {
                        format = String.format("parse json fail, ex = %s", e.getMessage());
                        Log.m17064e("MicroMsg.SDK.GetQRCodeResult", format);
                        oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                        aVar.f11087m = oAuthErrCode;
                        return aVar;
                    }
                } catch (Exception e2) {
                    format = String.format("parse fail, build String fail, ex = %s", e2.getMessage());
                    Log.m17064e("MicroMsg.SDK.GetQRCodeResult", format);
                    oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                    aVar.f11087m = oAuthErrCode;
                    return aVar;
                }
            }
            aVar.f11087m = oAuthErrCode;
            return aVar;
        }
    }

    public C4900d(String str, String str2, String str3, String str4, String str5, OAuthListener oAuthListener) {
        this.appId = str;
        this.scope = str2;
        this.f11083i = str3;
        this.f11084j = str4;
        this.signature = str5;
        this.f11085k = oAuthListener;
    }

    /* renamed from: a */
    public final boolean mo26955a() {
        Log.m17065i("MicroMsg.SDK.GetQRCodeTask", "cancelTask");
        C4903f fVar = this.f11086l;
        return fVar == null ? cancel(true) : super.cancel(true);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        Thread.currentThread().setName("OpenSdkGetQRCodeTask");
        Log.m17065i("MicroMsg.SDK.GetQRCodeTask", "doInBackground");
        String format = String.format(f11082h, this.appId, this.f11083i, this.f11084j, this.scope, this.signature);
        long currentTimeMillis = System.currentTimeMillis();
        byte[] a = C4902e.m17056a(format);
        Log.m17063d("MicroMsg.SDK.GetQRCodeTask", String.format("doInBackground, url = %s, time consumed = %d(ms)", format, Long.valueOf(System.currentTimeMillis() - currentTimeMillis)));
        return C4901a.m17055a(a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        C4901a aVar = (C4901a) obj;
        OAuthErrCode oAuthErrCode = aVar.f11087m;
        if (oAuthErrCode == OAuthErrCode.WechatAuth_Err_OK) {
            Log.m17063d("MicroMsg.SDK.GetQRCodeTask", "onPostExecute, get qrcode success imgBufSize = " + aVar.f11093s.length);
            this.f11085k.onAuthGotQrcode(aVar.f11090p, aVar.f11093s);
            this.f11086l = new C4903f(aVar.f11088n, this.f11085k);
            C4903f fVar = this.f11086l;
            if (Build.VERSION.SDK_INT >= 11) {
                super.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            } else {
                super.execute(new Void[0]);
            }
        } else {
            Log.m17064e("MicroMsg.SDK.GetQRCodeTask", String.format("onPostExecute, get qrcode fail, OAuthErrCode = %s", oAuthErrCode));
            this.f11085k.onAuthFinish(aVar.f11087m, null);
        }
    }
}
