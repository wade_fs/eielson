package com.tencent.p214mm.opensdk.diffdev.p216a;

import android.os.AsyncTask;
import com.tencent.p214mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p214mm.opensdk.diffdev.OAuthListener;
import com.tencent.p214mm.opensdk.utils.Log;
import org.json.JSONObject;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.f */
final class C4903f extends AsyncTask<Void, Void, C4904a> {

    /* renamed from: k */
    private OAuthListener f11094k;

    /* renamed from: n */
    private String f11095n;

    /* renamed from: t */
    private int f11096t;
    private String url;

    /* renamed from: com.tencent.mm.opensdk.diffdev.a.f$a */
    static class C4904a {

        /* renamed from: m */
        public OAuthErrCode f11097m;

        /* renamed from: u */
        public String f11098u;

        /* renamed from: v */
        public int f11099v;

        C4904a() {
        }

        /* renamed from: b */
        public static C4904a m17057b(byte[] bArr) {
            OAuthErrCode oAuthErrCode;
            String format;
            OAuthErrCode oAuthErrCode2;
            C4904a aVar = new C4904a();
            Log.m17063d("MicroMsg.SDK.NoopingResult", "star parse NoopingResult");
            if (bArr == null || bArr.length == 0) {
                Log.m17064e("MicroMsg.SDK.NoopingResult", "parse fail, buf is null");
                oAuthErrCode = OAuthErrCode.WechatAuth_Err_NetworkErr;
            } else {
                try {
                } catch (Exception e) {
                    format = String.format("parse fail, build String fail, ex = %s", e.getMessage());
                    Log.m17064e("MicroMsg.SDK.NoopingResult", format);
                    oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                    aVar.f11097m = oAuthErrCode;
                    return aVar;
                }
                try {
                    JSONObject jSONObject = new JSONObject(new String(bArr, "utf-8"));
                    aVar.f11099v = jSONObject.getInt("wx_errcode");
                    Log.m17063d("MicroMsg.SDK.NoopingResult", String.format("nooping uuidStatusCode = %d", Integer.valueOf(aVar.f11099v)));
                    int i = aVar.f11099v;
                    if (i == 408) {
                        oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_OK;
                    } else if (i != 500) {
                        switch (i) {
                            case 402:
                                oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_Timeout;
                                break;
                            case 403:
                                oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_Cancel;
                                break;
                            case 404:
                                oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_OK;
                                break;
                            case 405:
                                aVar.f11097m = OAuthErrCode.WechatAuth_Err_OK;
                                aVar.f11098u = jSONObject.getString("wx_code");
                                break;
                            default:
                                oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_NormalErr;
                                break;
                        }
                        return aVar;
                    } else {
                        oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_NormalErr;
                    }
                    aVar.f11097m = oAuthErrCode2;
                    return aVar;
                } catch (Exception e2) {
                    format = String.format("parse json fail, ex = %s", e2.getMessage());
                    Log.m17064e("MicroMsg.SDK.NoopingResult", format);
                    oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                    aVar.f11097m = oAuthErrCode;
                    return aVar;
                }
            }
            aVar.f11097m = oAuthErrCode;
            return aVar;
        }
    }

    public C4903f(String str, OAuthListener oAuthListener) {
        this.f11095n = str;
        this.f11094k = oAuthListener;
        this.url = String.format("https://long.open.weixin.qq.com/connect/l/qrconnect?f=json&uuid=%s", str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        C4904a aVar;
        OAuthErrCode oAuthErrCode;
        String str;
        Thread.currentThread().setName("OpenSdkNoopingTask");
        String str2 = this.f11095n;
        if (str2 == null || str2.length() == 0) {
            Log.m17064e("MicroMsg.SDK.NoopingTask", "run fail, uuid is null");
            aVar = new C4904a();
            oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
        } else {
            Log.m17065i("MicroMsg.SDK.NoopingTask", "doInBackground start " + isCancelled());
            while (!isCancelled()) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.url);
                if (this.f11096t == 0) {
                    str = "";
                } else {
                    str = "&last=" + this.f11096t;
                }
                sb.append(str);
                String sb2 = sb.toString();
                long currentTimeMillis = System.currentTimeMillis();
                byte[] a = C4902e.m17056a(sb2);
                long currentTimeMillis2 = System.currentTimeMillis();
                C4904a b = C4904a.m17057b(a);
                Log.m17063d("MicroMsg.SDK.NoopingTask", String.format("nooping, url = %s, errCode = %s, uuidStatusCode = %d, time consumed = %d(ms)", sb2, b.f11097m.toString(), Integer.valueOf(b.f11099v), Long.valueOf(currentTimeMillis2 - currentTimeMillis)));
                OAuthErrCode oAuthErrCode2 = b.f11097m;
                if (oAuthErrCode2 == OAuthErrCode.WechatAuth_Err_OK) {
                    int i = b.f11099v;
                    this.f11096t = i;
                    if (i == C4905g.UUID_SCANED.getCode()) {
                        this.f11094k.onQrcodeScanned();
                    } else if (b.f11099v != C4905g.UUID_KEEP_CONNECT.getCode() && b.f11099v == C4905g.UUID_CONFIRM.getCode()) {
                        String str3 = b.f11098u;
                        if (str3 == null || str3.length() == 0) {
                            Log.m17064e("MicroMsg.SDK.NoopingTask", "nooping fail, confirm with an empty code!!!");
                            b.f11097m = OAuthErrCode.WechatAuth_Err_NormalErr;
                        }
                        return b;
                    }
                } else {
                    Log.m17064e("MicroMsg.SDK.NoopingTask", String.format("nooping fail, errCode = %s, uuidStatusCode = %d", oAuthErrCode2.toString(), Integer.valueOf(b.f11099v)));
                    return b;
                }
            }
            Log.m17065i("MicroMsg.SDK.NoopingTask", "IDiffDevOAuth.stopAuth / detach invoked");
            aVar = new C4904a();
            oAuthErrCode = OAuthErrCode.WechatAuth_Err_Auth_Stopped;
        }
        aVar.f11097m = oAuthErrCode;
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        C4904a aVar = (C4904a) obj;
        this.f11094k.onAuthFinish(aVar.f11097m, aVar.f11098u);
    }
}
