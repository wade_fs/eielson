package com.tencent.p214mm.opensdk.channel.p215a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.tencent.p214mm.opensdk.constants.Build;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import com.tencent.p214mm.opensdk.utils.C4912d;
import com.tencent.p214mm.opensdk.utils.Log;

/* renamed from: com.tencent.mm.opensdk.channel.a.a */
public final class C4894a {

    /* renamed from: com.tencent.mm.opensdk.channel.a.a$a */
    public static class C4895a {

        /* renamed from: a */
        public String f11075a;
        public String action;

        /* renamed from: b */
        public long f11076b;
        public Bundle bundle;
        public String content;
    }

    /* renamed from: a */
    public static boolean m17049a(Context context, C4895a aVar) {
        String str;
        if (context == null) {
            str = "send fail, invalid argument";
        } else if (C4912d.m17074b(aVar.action)) {
            str = "send fail, action is null";
        } else {
            String str2 = null;
            if (!C4912d.m17074b(aVar.f11075a)) {
                str2 = aVar.f11075a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(aVar.action);
            Bundle bundle = aVar.bundle;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra(ConstantsAPI.SDK_VERSION, (int) Build.SDK_INT);
            intent.putExtra(ConstantsAPI.APP_PACKAGE, packageName);
            intent.putExtra(ConstantsAPI.CONTENT, aVar.content);
            intent.putExtra(ConstantsAPI.APP_SUPORT_CONTENT_TYPE, aVar.f11076b);
            intent.putExtra(ConstantsAPI.CHECK_SUM, C4896b.m17050a(aVar.content, Build.SDK_INT, packageName));
            context.sendBroadcast(intent, str2);
            Log.m17063d("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str2);
            return true;
        }
        Log.m17064e("MicroMsg.SDK.MMessage", str);
        return false;
    }
}
