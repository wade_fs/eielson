package com.tencent.p214mm.opensdk.diffdev.p216a;

import com.tencent.p214mm.opensdk.diffdev.OAuthListener;
import java.util.ArrayList;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.c */
final class C4899c implements Runnable {

    /* renamed from: g */
    final /* synthetic */ C4898b f11081g;

    C4899c(C4898b bVar) {
        this.f11081g = bVar;
    }

    public final void run() {
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f11081g.f11080f.f11077c);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onQrcodeScanned();
        }
    }
}
