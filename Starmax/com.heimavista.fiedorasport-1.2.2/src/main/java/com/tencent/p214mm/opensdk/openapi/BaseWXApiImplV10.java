package com.tencent.p214mm.opensdk.openapi;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.NativeProtocol;
import com.tencent.p214mm.opensdk.channel.MMessageActV2;
import com.tencent.p214mm.opensdk.channel.p215a.C4894a;
import com.tencent.p214mm.opensdk.channel.p215a.C4896b;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import com.tencent.p214mm.opensdk.modelbase.BaseReq;
import com.tencent.p214mm.opensdk.modelbase.BaseResp;
import com.tencent.p214mm.opensdk.modelbiz.AddCardToWXCardPackage;
import com.tencent.p214mm.opensdk.modelbiz.ChooseCardFromWXCardPackage;
import com.tencent.p214mm.opensdk.modelbiz.CreateChatroom;
import com.tencent.p214mm.opensdk.modelbiz.HandleScanResult;
import com.tencent.p214mm.opensdk.modelbiz.JoinChatroom;
import com.tencent.p214mm.opensdk.modelbiz.OpenWebview;
import com.tencent.p214mm.opensdk.modelbiz.SubscribeMessage;
import com.tencent.p214mm.opensdk.modelbiz.SubscribeMiniProgramMsg;
import com.tencent.p214mm.opensdk.modelbiz.WXInvoiceAuthInsert;
import com.tencent.p214mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.p214mm.opensdk.modelbiz.WXLaunchMiniProgramWithToken;
import com.tencent.p214mm.opensdk.modelbiz.WXNontaxPay;
import com.tencent.p214mm.opensdk.modelbiz.WXOpenBusinessView;
import com.tencent.p214mm.opensdk.modelbiz.WXOpenBusinessWebview;
import com.tencent.p214mm.opensdk.modelbiz.WXPayInsurance;
import com.tencent.p214mm.opensdk.modelbiz.WXPreloadMiniProgram;
import com.tencent.p214mm.opensdk.modelmsg.GetMessageFromWX;
import com.tencent.p214mm.opensdk.modelmsg.LaunchFromWX;
import com.tencent.p214mm.opensdk.modelmsg.SendAuth;
import com.tencent.p214mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.p214mm.opensdk.modelmsg.ShowMessageFromWX;
import com.tencent.p214mm.opensdk.modelpay.JumpToOfflinePay;
import com.tencent.p214mm.opensdk.modelpay.PayResp;
import com.tencent.p214mm.opensdk.modelpay.WXJointPay;
import com.tencent.p214mm.opensdk.utils.C4912d;
import com.tencent.p214mm.opensdk.utils.ILog;
import com.tencent.p214mm.opensdk.utils.Log;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

/* renamed from: com.tencent.mm.opensdk.openapi.BaseWXApiImplV10 */
class BaseWXApiImplV10 implements IWXAPI {
    protected static final String TAG = "MicroMsg.SDK.WXApiImplV10";
    private static String wxappPayEntryClassname;
    protected String appId;
    protected boolean checkSignature = false;
    protected Context context;
    protected boolean detached = false;
    /* access modifiers changed from: private */
    public int wxSdkVersion;

    BaseWXApiImplV10(Context context2, String str, boolean z) {
        Log.m17063d(TAG, "<init>, appId = " + str + ", checkSignature = " + z);
        this.context = context2;
        this.appId = str;
        this.checkSignature = z;
        C4912d.f11109D = context2.getApplicationContext();
    }

    private boolean checkSumConsistent(byte[] bArr, byte[] bArr2) {
        String str;
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            str = "checkSumConsistent fail, invalid arguments";
        } else if (bArr.length != bArr2.length) {
            str = "checkSumConsistent fail, length is different";
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
        Log.m17064e(TAG, str);
        return false;
    }

    private boolean createChatroom(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/createChatroom"), null, null, new String[]{this.appId, bundle.getString("_wxapi_basereq_transaction", ""), bundle.getString("_wxapi_create_chatroom_group_id", ""), bundle.getString("_wxapi_create_chatroom_chatroom_name", ""), bundle.getString("_wxapi_create_chatroom_chatroom_nickname", ""), bundle.getString("_wxapi_create_chatroom_ext_msg", ""), bundle.getString("_wxapi_basereq_openid", "")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private String getTokenFromWX(Context context2) {
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/genTokenForOpenSdk"), null, null, new String[]{this.appId, "621086720"}, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        String string = query.getString(0);
        Log.m17065i(TAG, "getTokenFromWX token is " + string);
        query.close();
        return string;
    }

    private boolean handleWxInternalRespType(String str, IWXAPIEventHandler iWXAPIEventHandler) {
        Log.m17065i(TAG, "handleWxInternalRespType, extInfo = " + str);
        try {
            Uri parse = Uri.parse(str);
            String queryParameter = parse.getQueryParameter("wx_internal_resptype");
            Log.m17065i(TAG, "handleWxInternalRespType, respType = " + queryParameter);
            if (C4912d.m17074b(queryParameter)) {
                Log.m17064e(TAG, "handleWxInternalRespType fail, respType is null");
                return false;
            } else if (queryParameter.equals("subscribemessage")) {
                SubscribeMessage.Resp resp = new SubscribeMessage.Resp();
                String queryParameter2 = parse.getQueryParameter("ret");
                if (queryParameter2 != null && queryParameter2.length() > 0) {
                    resp.errCode = C4912d.m17075c(queryParameter2);
                }
                resp.openId = parse.getQueryParameter("openid");
                resp.templateID = parse.getQueryParameter("template_id");
                resp.scene = C4912d.m17075c(parse.getQueryParameter("scene"));
                resp.action = parse.getQueryParameter(NativeProtocol.WEB_DIALOG_ACTION);
                resp.reserved = parse.getQueryParameter("reserved");
                iWXAPIEventHandler.onResp(resp);
                return true;
            } else if (queryParameter.contains("invoice_auth_insert")) {
                WXInvoiceAuthInsert.Resp resp2 = new WXInvoiceAuthInsert.Resp();
                String queryParameter3 = parse.getQueryParameter("ret");
                if (queryParameter3 != null && queryParameter3.length() > 0) {
                    resp2.errCode = C4912d.m17075c(queryParameter3);
                }
                resp2.wxOrderId = parse.getQueryParameter("wx_order_id");
                iWXAPIEventHandler.onResp(resp2);
                return true;
            } else if (queryParameter.contains("payinsurance")) {
                WXPayInsurance.Resp resp3 = new WXPayInsurance.Resp();
                String queryParameter4 = parse.getQueryParameter("ret");
                if (queryParameter4 != null && queryParameter4.length() > 0) {
                    resp3.errCode = C4912d.m17075c(queryParameter4);
                }
                resp3.wxOrderId = parse.getQueryParameter("wx_order_id");
                iWXAPIEventHandler.onResp(resp3);
                return true;
            } else if (queryParameter.contains("nontaxpay")) {
                WXNontaxPay.Resp resp4 = new WXNontaxPay.Resp();
                String queryParameter5 = parse.getQueryParameter("ret");
                if (queryParameter5 != null && queryParameter5.length() > 0) {
                    resp4.errCode = C4912d.m17075c(queryParameter5);
                }
                resp4.wxOrderId = parse.getQueryParameter("wx_order_id");
                iWXAPIEventHandler.onResp(resp4);
                return true;
            } else {
                if (!"subscribeminiprogrammsg".equals(queryParameter)) {
                    if (!"5".equals(queryParameter)) {
                        Log.m17064e(TAG, "this open sdk version not support the request type");
                        return false;
                    }
                }
                SubscribeMiniProgramMsg.Resp resp5 = new SubscribeMiniProgramMsg.Resp();
                String queryParameter6 = parse.getQueryParameter("ret");
                if (queryParameter6 != null && queryParameter6.length() > 0) {
                    resp5.errCode = C4912d.m17075c(queryParameter6);
                }
                resp5.openId = parse.getQueryParameter("openid");
                resp5.unionId = parse.getQueryParameter("unionid");
                resp5.nickname = parse.getQueryParameter("nickname");
                resp5.errStr = parse.getQueryParameter("errmsg");
                iWXAPIEventHandler.onResp(resp5);
                return true;
            }
        } catch (Exception e) {
            Log.m17064e(TAG, "handleWxInternalRespType fail, ex = " + e.getMessage());
        }
    }

    private boolean joinChatroom(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/joinChatroom"), null, null, new String[]{this.appId, bundle.getString("_wxapi_basereq_transaction", ""), bundle.getString("_wxapi_join_chatroom_group_id", ""), bundle.getString("_wxapi_join_chatroom_chatroom_nickname", ""), bundle.getString("_wxapi_join_chatroom_ext_msg", ""), bundle.getString("_wxapi_basereq_openid", "")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private void launchWXIfNeed() {
        if (Build.VERSION.SDK_INT >= 28) {
            Log.m17065i(TAG, "openWXApp before api call");
            openWXApp();
        }
    }

    private boolean sendAddCardToWX(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), null, null, new String[]{this.appId, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendChooseCardFromWX(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/chooseCardFromWX"), null, null, new String[]{bundle.getString("_wxapi_choose_card_from_wx_card_app_id"), bundle.getString("_wxapi_choose_card_from_wx_card_location_id"), bundle.getString("_wxapi_choose_card_from_wx_card_sign_type"), bundle.getString("_wxapi_choose_card_from_wx_card_card_sign"), bundle.getString("_wxapi_choose_card_from_wx_card_time_stamp"), bundle.getString("_wxapi_choose_card_from_wx_card_nonce_str"), bundle.getString("_wxapi_choose_card_from_wx_card_card_id"), bundle.getString("_wxapi_choose_card_from_wx_card_card_type"), bundle.getString("_wxapi_choose_card_from_wx_card_can_multi_select"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendHandleScanResult(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/handleScanResult"), null, null, new String[]{this.appId, bundle.getString("_wxapi_scan_qrcode_result")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendInvoiceAuthInsert(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openTypeWebview"), null, null, new String[]{this.appId, ExifInterface.GPS_MEASUREMENT_2D, URLEncoder.encode(String.format("url=%s", URLEncoder.encode(((WXInvoiceAuthInsert.Req) baseReq).url)))}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendJumpToOfflinePayReq(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToOfflinePay"), null, null, new String[]{this.appId}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendLaunchWXMiniprogram(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        WXLaunchMiniProgram.Req req = (WXLaunchMiniProgram.Req) baseReq;
        ContentResolver contentResolver = context2.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/launchWXMiniprogram");
        StringBuilder sb = new StringBuilder();
        sb.append(req.miniprogramType);
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.appId, req.userName, req.path, sb.toString(), req.extData}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendLaunchWXMiniprogramWithToken(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/launchWXMiniprogramWithToken"), null, null, new String[]{this.appId, ((WXLaunchMiniProgramWithToken.Req) baseReq).token}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendNonTaxPay(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openTypeWebview"), null, null, new String[]{this.appId, ExifInterface.GPS_MEASUREMENT_3D, URLEncoder.encode(String.format("url=%s", URLEncoder.encode(((WXNontaxPay.Req) baseReq).url)))}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendOpenBusiLuckyMoney(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusiLuckyMoney"), null, null, new String[]{this.appId, bundle.getString("_wxapi_open_busi_lucky_money_timeStamp"), bundle.getString("_wxapi_open_busi_lucky_money_nonceStr"), bundle.getString("_wxapi_open_busi_lucky_money_signType"), bundle.getString("_wxapi_open_busi_lucky_money_signature"), bundle.getString("_wxapi_open_busi_lucky_money_package")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendOpenBusinessView(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        WXOpenBusinessView.Req req = (WXOpenBusinessView.Req) baseReq;
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusinessView"), null, null, new String[]{this.appId, req.businessType, req.query, req.extInfo, req.transaction}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendOpenBusinessWebview(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        WXOpenBusinessWebview.Req req = (WXOpenBusinessWebview.Req) baseReq;
        ContentResolver contentResolver = context2.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusinessWebview");
        HashMap<String, String> hashMap = req.queryInfo;
        String jSONObject = (hashMap == null || hashMap.size() <= 0) ? "" : new JSONObject(req.queryInfo).toString();
        StringBuilder sb = new StringBuilder();
        sb.append(req.businessType);
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.appId, sb.toString(), jSONObject}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendOpenRankListReq(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openRankList"), null, null, new String[0], null);
        if (query == null) {
            return true;
        }
        query.close();
        return true;
    }

    private boolean sendOpenWebview(Context context2, Bundle bundle) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openWebview"), null, null, new String[]{this.appId, bundle.getString("_wxapi_jump_to_webview_url"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendPayInSurance(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openTypeWebview"), null, null, new String[]{this.appId, "4", URLEncoder.encode(String.format("url=%s", URLEncoder.encode(((WXPayInsurance.Req) baseReq).url)))}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendPayReq(Context context2, Bundle bundle) {
        if (wxappPayEntryClassname == null) {
            wxappPayEntryClassname = new MMSharedPreferences(context2).getString("_wxapp_pay_entry_classname_", null);
            Log.m17063d(TAG, "pay, set wxappPayEntryClassname = " + wxappPayEntryClassname);
            if (wxappPayEntryClassname == null) {
                try {
                    wxappPayEntryClassname = context2.getPackageManager().getApplicationInfo("com.tencent.mm", 128).metaData.getString("com.tencent.mm.BuildInfo.OPEN_SDK_PAY_ENTRY_CLASSNAME", null);
                } catch (Exception e) {
                    Log.m17064e(TAG, "get from metaData failed : " + e.getMessage());
                }
            }
            if (wxappPayEntryClassname == null) {
                Log.m17064e(TAG, "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        MMessageActV2.Args args = new MMessageActV2.Args();
        args.bundle = bundle;
        args.targetPkgName = "com.tencent.mm";
        args.targetClassName = wxappPayEntryClassname;
        return MMessageActV2.send(context2, args);
    }

    private boolean sendPreloadWXMiniprogram(Context context2, BaseReq baseReq) {
        WXPreloadMiniProgram.Req req = (WXPreloadMiniProgram.Req) baseReq;
        ContentResolver contentResolver = context2.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/preloadWXMiniprogram");
        StringBuilder sb = new StringBuilder();
        sb.append(req.miniprogramType);
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.appId, req.userName, req.path, sb.toString(), req.extData}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendSubscribeMessage(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        SubscribeMessage.Req req = (SubscribeMessage.Req) baseReq;
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openTypeWebview"), null, null, new String[]{this.appId, AppEventsConstants.EVENT_PARAM_VALUE_YES, String.valueOf(req.scene), req.templateID, req.reserved}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendSubscribeMiniProgramMsg(Context context2, BaseReq baseReq) {
        launchWXIfNeed();
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openTypeWebview"), null, null, new String[]{this.appId, "5", ((SubscribeMiniProgramMsg.Req) baseReq).miniProgramAppId}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    public void detach() {
        Log.m17063d(TAG, "detach");
        this.detached = true;
        this.context = null;
    }

    public int getWXAppSupportAPI() {
        if (this.detached) {
            throw new IllegalStateException("getWXAppSupportAPI fail, WXMsgImpl has been detached");
        } else if (!isWXAppInstalled()) {
            Log.m17064e(TAG, "open wx app failed, not installed or signature check failed");
            return 0;
        } else {
            this.wxSdkVersion = 0;
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            C4912d.f11113H.submit(new Runnable() {
                /* class com.tencent.p214mm.opensdk.openapi.BaseWXApiImplV10.C49061 */

                public void run() {
                    try {
                        int unused = BaseWXApiImplV10.this.wxSdkVersion = new MMSharedPreferences(BaseWXApiImplV10.this.context).getInt("_build_info_sdk_int_", 0);
                    } catch (Exception e) {
                        Log.m17067w(BaseWXApiImplV10.TAG, e.getMessage());
                    }
                    countDownLatch.countDown();
                }
            });
            try {
                countDownLatch.await(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Log.m17067w(TAG, e.getMessage());
            }
            Log.m17063d(TAG, "wxSdkVersion = " + this.wxSdkVersion);
            if (this.wxSdkVersion == 0) {
                try {
                    this.wxSdkVersion = this.context.getPackageManager().getApplicationInfo("com.tencent.mm", 128).metaData.getInt("com.tencent.mm.BuildInfo.OPEN_SDK_VERSION", 0);
                    Log.m17063d(TAG, "OPEN_SDK_VERSION = " + this.wxSdkVersion);
                } catch (Exception e2) {
                    Log.m17064e(TAG, "get from metaData failed : " + e2.getMessage());
                }
            }
            return this.wxSdkVersion;
        }
    }

    public boolean handleIntent(Intent intent, IWXAPIEventHandler iWXAPIEventHandler) {
        try {
            if (!WXApiImplComm.isIntentFromWx(intent, ConstantsAPI.Token.WX_TOKEN_VALUE_MSG)) {
                Log.m17065i(TAG, "handleIntent fail, intent not from weixin msg");
                return false;
            } else if (!this.detached) {
                String stringExtra = intent.getStringExtra(ConstantsAPI.CONTENT);
                int intExtra = intent.getIntExtra(ConstantsAPI.SDK_VERSION, 0);
                String stringExtra2 = intent.getStringExtra(ConstantsAPI.APP_PACKAGE);
                if (stringExtra2 != null) {
                    if (stringExtra2.length() != 0) {
                        if (!checkSumConsistent(intent.getByteArrayExtra(ConstantsAPI.CHECK_SUM), C4896b.m17050a(stringExtra, intExtra, stringExtra2))) {
                            Log.m17064e(TAG, "checksum fail");
                            return false;
                        }
                        int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                        Log.m17065i(TAG, "handleIntent, cmd = " + intExtra2);
                        switch (intExtra2) {
                            case 1:
                                iWXAPIEventHandler.onResp(new SendAuth.Resp(intent.getExtras()));
                                return true;
                            case 2:
                                iWXAPIEventHandler.onResp(new SendMessageToWX.Resp(intent.getExtras()));
                                return true;
                            case 3:
                                iWXAPIEventHandler.onReq(new GetMessageFromWX.Req(intent.getExtras()));
                                return true;
                            case 4:
                                ShowMessageFromWX.Req req = new ShowMessageFromWX.Req(intent.getExtras());
                                String str = req.message.messageExt;
                                if (str == null || !str.contains("wx_internal_resptype")) {
                                    if (str != null && str.contains("openbusinesswebview")) {
                                        try {
                                            Uri parse = Uri.parse(str);
                                            if (parse == null || !"openbusinesswebview".equals(parse.getHost())) {
                                                Log.m17063d(TAG, "not openbusinesswebview %" + str);
                                            } else {
                                                WXOpenBusinessWebview.Resp resp = new WXOpenBusinessWebview.Resp();
                                                String queryParameter = parse.getQueryParameter("ret");
                                                if (queryParameter != null && queryParameter.length() > 0) {
                                                    resp.errCode = C4912d.m17075c(queryParameter);
                                                }
                                                resp.resultInfo = parse.getQueryParameter("resultInfo");
                                                resp.errStr = parse.getQueryParameter("errmsg");
                                                String queryParameter2 = parse.getQueryParameter("type");
                                                if (queryParameter2 != null && queryParameter2.length() > 0) {
                                                    resp.businessType = C4912d.m17075c(queryParameter2);
                                                }
                                                iWXAPIEventHandler.onResp(resp);
                                                return true;
                                            }
                                        } catch (Exception e) {
                                            Log.m17064e(TAG, "parse fail, ex = " + e.getMessage());
                                        }
                                    }
                                    iWXAPIEventHandler.onReq(req);
                                    return true;
                                }
                                boolean handleWxInternalRespType = handleWxInternalRespType(str, iWXAPIEventHandler);
                                Log.m17065i(TAG, "handleIntent, extInfo contains wx_internal_resptype, ret = " + handleWxInternalRespType);
                                return handleWxInternalRespType;
                            case 5:
                                iWXAPIEventHandler.onResp(new PayResp(intent.getExtras()));
                                return true;
                            case 6:
                                iWXAPIEventHandler.onReq(new LaunchFromWX.Req(intent.getExtras()));
                                return true;
                            case 7:
                            case 8:
                            case 10:
                            case 11:
                            case 13:
                            case 18:
                            case 20:
                            case 21:
                            case 22:
                            case 23:
                            default:
                                Log.m17064e(TAG, "unknown cmd = " + intExtra2);
                                break;
                            case 9:
                                iWXAPIEventHandler.onResp(new AddCardToWXCardPackage.Resp(intent.getExtras()));
                                return true;
                            case 12:
                                iWXAPIEventHandler.onResp(new OpenWebview.Resp(intent.getExtras()));
                                return true;
                            case 14:
                                iWXAPIEventHandler.onResp(new CreateChatroom.Resp(intent.getExtras()));
                                return true;
                            case 15:
                                iWXAPIEventHandler.onResp(new JoinChatroom.Resp(intent.getExtras()));
                                return true;
                            case 16:
                                iWXAPIEventHandler.onResp(new ChooseCardFromWXCardPackage.Resp(intent.getExtras()));
                                return true;
                            case 17:
                                iWXAPIEventHandler.onResp(new HandleScanResult.Resp(intent.getExtras()));
                                return true;
                            case 19:
                                iWXAPIEventHandler.onResp(new WXLaunchMiniProgram.Resp(intent.getExtras()));
                                return true;
                            case 24:
                                iWXAPIEventHandler.onResp(new JumpToOfflinePay.Resp(intent.getExtras()));
                                return true;
                            case 25:
                                iWXAPIEventHandler.onResp(new WXOpenBusinessWebview.Resp(intent.getExtras()));
                                return true;
                            case 26:
                                iWXAPIEventHandler.onResp(new WXOpenBusinessView.Resp(intent.getExtras()));
                                return true;
                            case 27:
                                iWXAPIEventHandler.onResp(new WXJointPay.JointPayResp(intent.getExtras()));
                                return true;
                            case 28:
                                iWXAPIEventHandler.onResp(new WXPreloadMiniProgram.Resp(intent.getExtras()));
                                return true;
                            case 29:
                                iWXAPIEventHandler.onResp(new WXLaunchMiniProgramWithToken.Resp(intent.getExtras()));
                                return true;
                        }
                        return false;
                    }
                }
                Log.m17064e(TAG, "invalid argument");
                return false;
            } else {
                throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
            }
        } catch (Exception e2) {
            Log.m17064e(TAG, "handleIntent fail, ex = " + e2.getMessage());
        }
    }

    public boolean isWXAppInstalled() {
        if (!this.detached) {
            try {
                PackageInfo packageInfo = this.context.getPackageManager().getPackageInfo("com.tencent.mm", 64);
                if (packageInfo == null) {
                    return false;
                }
                return WXApiImplComm.validateAppSignature(this.context, packageInfo.signatures, this.checkSignature);
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        } else {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
    }

    public boolean openWXApp() {
        String str;
        if (!this.detached) {
            if (!isWXAppInstalled()) {
                str = "open wx app failed, not installed or signature check failed";
            } else {
                try {
                    this.context.startActivity(this.context.getPackageManager().getLaunchIntentForPackage("com.tencent.mm"));
                    return true;
                } catch (Exception e) {
                    str = "startActivity fail, exception = " + e.getMessage();
                }
            }
            Log.m17064e(TAG, str);
            return false;
        }
        throw new IllegalStateException("openWXApp fail, WXMsgImpl has been detached");
    }

    public boolean registerApp(String str) {
        return registerApp(str, 0);
    }

    public boolean registerApp(String str, long j) {
        if (this.detached) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            Log.m17064e(TAG, "register app failed for wechat app signature check failed");
            return false;
        } else {
            Log.m17063d(TAG, "registerApp, appId = " + str);
            if (str != null) {
                this.appId = str;
            }
            Log.m17063d(TAG, "registerApp, appId = " + str);
            if (str != null) {
                this.appId = str;
            }
            Log.m17063d(TAG, "register app " + this.context.getPackageName());
            C4894a.C4895a aVar = new C4894a.C4895a();
            aVar.f11075a = "com.tencent.mm";
            aVar.action = ConstantsAPI.ACTION_HANDLE_APP_REGISTER;
            aVar.content = "weixin://registerapp?appid=" + this.appId;
            aVar.f11076b = j;
            return C4894a.m17049a(this.context, aVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:0x020a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean sendReq(com.tencent.p214mm.opensdk.modelbase.BaseReq r12) {
        /*
            r11 = this;
            boolean r0 = r11.detached
            if (r0 != 0) goto L_0x0261
            android.content.Context r0 = r11.context
            boolean r1 = r11.checkSignature
            java.lang.String r2 = "com.tencent.mm"
            boolean r0 = com.tencent.p214mm.opensdk.openapi.WXApiImplComm.validateAppSignatureForPackage(r0, r2, r1)
            java.lang.String r1 = "MicroMsg.SDK.WXApiImplV10"
            r3 = 0
            if (r0 != 0) goto L_0x0019
            java.lang.String r12 = "sendReq failed for wechat app signature check failed"
        L_0x0015:
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r12)
            return r3
        L_0x0019:
            boolean r0 = r12.checkArgs()
            if (r0 != 0) goto L_0x0022
            java.lang.String r12 = "sendReq checkArgs fail"
            goto L_0x0015
        L_0x0022:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = "sendReq, req type = "
            r0.<init>(r4)
            int r4 = r12.getType()
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.tencent.p214mm.opensdk.utils.Log.m17065i(r1, r0)
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            r12.toBundle(r0)
            int r4 = r12.getType()
            r5 = 5
            if (r4 == r5) goto L_0x025a
            int r4 = r12.getType()
            r5 = 27
            if (r4 != r5) goto L_0x0050
            goto L_0x025a
        L_0x0050:
            int r4 = r12.getType()
            r5 = 9
            if (r4 != r5) goto L_0x005f
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendAddCardToWX(r12, r0)
            return r12
        L_0x005f:
            int r4 = r12.getType()
            r5 = 16
            if (r4 != r5) goto L_0x006e
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendChooseCardFromWX(r12, r0)
            return r12
        L_0x006e:
            int r4 = r12.getType()
            r5 = 11
            if (r4 != r5) goto L_0x007d
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendOpenRankListReq(r12, r0)
            return r12
        L_0x007d:
            int r4 = r12.getType()
            r5 = 12
            if (r4 != r5) goto L_0x008c
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendOpenWebview(r12, r0)
            return r12
        L_0x008c:
            int r4 = r12.getType()
            r5 = 25
            if (r4 != r5) goto L_0x009b
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendOpenBusinessWebview(r0, r12)
            return r12
        L_0x009b:
            int r4 = r12.getType()
            r5 = 13
            if (r4 != r5) goto L_0x00aa
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendOpenBusiLuckyMoney(r12, r0)
            return r12
        L_0x00aa:
            int r4 = r12.getType()
            r5 = 14
            if (r4 != r5) goto L_0x00b9
            android.content.Context r12 = r11.context
            boolean r12 = r11.createChatroom(r12, r0)
            return r12
        L_0x00b9:
            int r4 = r12.getType()
            r5 = 15
            if (r4 != r5) goto L_0x00c8
            android.content.Context r12 = r11.context
            boolean r12 = r11.joinChatroom(r12, r0)
            return r12
        L_0x00c8:
            int r4 = r12.getType()
            r5 = 17
            if (r4 != r5) goto L_0x00d7
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendHandleScanResult(r12, r0)
            return r12
        L_0x00d7:
            int r4 = r12.getType()
            r5 = 18
            if (r4 != r5) goto L_0x00e6
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendSubscribeMessage(r0, r12)
            return r12
        L_0x00e6:
            int r4 = r12.getType()
            r5 = 28
            if (r4 != r5) goto L_0x00f5
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendPreloadWXMiniprogram(r0, r12)
            return r12
        L_0x00f5:
            int r4 = r12.getType()
            r5 = 29
            if (r4 != r5) goto L_0x0104
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendLaunchWXMiniprogramWithToken(r0, r12)
            return r12
        L_0x0104:
            int r4 = r12.getType()
            r5 = 23
            if (r4 != r5) goto L_0x0113
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendSubscribeMiniProgramMsg(r0, r12)
            return r12
        L_0x0113:
            int r4 = r12.getType()
            r5 = 19
            if (r4 != r5) goto L_0x0122
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendLaunchWXMiniprogram(r0, r12)
            return r12
        L_0x0122:
            int r4 = r12.getType()
            r5 = 26
            if (r4 != r5) goto L_0x0131
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendOpenBusinessView(r0, r12)
            return r12
        L_0x0131:
            int r4 = r12.getType()
            r5 = 20
            if (r4 != r5) goto L_0x0140
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendInvoiceAuthInsert(r0, r12)
            return r12
        L_0x0140:
            int r4 = r12.getType()
            r5 = 21
            if (r4 != r5) goto L_0x014f
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendNonTaxPay(r0, r12)
            return r12
        L_0x014f:
            int r4 = r12.getType()
            r5 = 22
            if (r4 != r5) goto L_0x015e
            android.content.Context r0 = r11.context
            boolean r12 = r11.sendPayInSurance(r0, r12)
            return r12
        L_0x015e:
            int r4 = r12.getType()
            r5 = 24
            if (r4 != r5) goto L_0x016d
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendJumpToOfflinePayReq(r12, r0)
            return r12
        L_0x016d:
            int r4 = r12.getType()
            r5 = 2
            if (r4 != r5) goto L_0x020f
            r4 = r12
            com.tencent.mm.opensdk.modelmsg.SendMessageToWX$Req r4 = (com.tencent.p214mm.opensdk.modelmsg.SendMessageToWX.Req) r4
            com.tencent.mm.opensdk.modelmsg.WXMediaMessage r6 = r4.message
            int r6 = r6.getType()
            boolean r7 = com.tencent.p214mm.opensdk.utils.C4912d.m17073a(r6)
            if (r7 == 0) goto L_0x020f
            int r7 = r11.getWXAppSupportAPI()
            r8 = 620756993(0x25000001, float:1.1102232E-16)
            java.lang.String r9 = "_wxminiprogram_webpageurl"
            if (r7 >= r8) goto L_0x019e
            com.tencent.mm.opensdk.modelmsg.WXWebpageObject r6 = new com.tencent.mm.opensdk.modelmsg.WXWebpageObject
            r6.<init>()
        L_0x0193:
            java.lang.String r7 = r0.getString(r9)
            r6.webpageUrl = r7
            com.tencent.mm.opensdk.modelmsg.WXMediaMessage r7 = r4.message
            r7.mediaObject = r6
            goto L_0x0205
        L_0x019e:
            r7 = 46
            if (r6 != r7) goto L_0x01b1
            int r6 = r11.getWXAppSupportAPI()
            r7 = 620953856(0x25030100, float:1.1362778E-16)
            if (r6 >= r7) goto L_0x01b1
            com.tencent.mm.opensdk.modelmsg.WXWebpageObject r6 = new com.tencent.mm.opensdk.modelmsg.WXWebpageObject
            r6.<init>()
            goto L_0x0193
        L_0x01b1:
            com.tencent.mm.opensdk.modelmsg.WXMediaMessage r6 = r4.message
            com.tencent.mm.opensdk.modelmsg.WXMediaMessage$IMediaObject r6 = r6.mediaObject
            com.tencent.mm.opensdk.modelmsg.WXMiniProgramObject r6 = (com.tencent.p214mm.opensdk.modelmsg.WXMiniProgramObject) r6
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = r6.userName
            r7.append(r8)
            java.lang.String r8 = "@app"
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.userName = r7
            java.lang.String r7 = r6.path
            boolean r8 = com.tencent.p214mm.opensdk.utils.C4912d.m17074b(r7)
            if (r8 != 0) goto L_0x0205
            java.lang.String r8 = "\\?"
            java.lang.String[] r7 = r7.split(r8)
            int r8 = r7.length
            r9 = 1
            if (r8 <= r9) goto L_0x01f0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r10 = r7[r3]
            r8.append(r10)
            java.lang.String r10 = ".html?"
            r8.append(r10)
            r7 = r7[r9]
            goto L_0x01fc
        L_0x01f0:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r7 = r7[r3]
            r8.append(r7)
            java.lang.String r7 = ".html"
        L_0x01fc:
            r8.append(r7)
            java.lang.String r7 = r8.toString()
            r6.path = r7
        L_0x0205:
            int r6 = r4.scene
            r7 = 3
            if (r6 == r7) goto L_0x020c
            r4.scene = r3
        L_0x020c:
            r12.toBundle(r0)
        L_0x020f:
            com.tencent.mm.opensdk.channel.MMessageActV2$Args r3 = new com.tencent.mm.opensdk.channel.MMessageActV2$Args
            r3.<init>()
            r3.bundle = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = "weixin://sendreq?appid="
            r0.<init>(r4)
            java.lang.String r4 = r11.appId
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r3.content = r0
            r3.targetPkgName = r2
            java.lang.String r0 = "com.tencent.mm.plugin.base.stub.WXEntryActivity"
            r3.targetClassName = r0
            int r12 = r12.getType()
            if (r12 != r5) goto L_0x0253
            android.content.Context r12 = r11.context     // Catch:{ Exception -> 0x023d }
            java.lang.String r12 = r11.getTokenFromWX(r12)     // Catch:{ Exception -> 0x023d }
            r3.token = r12     // Catch:{ Exception -> 0x023d }
            goto L_0x0253
        L_0x023d:
            r12 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "getTokenFromWX fail, exception = "
            r0.<init>(r2)
            java.lang.String r12 = r12.getMessage()
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            com.tencent.p214mm.opensdk.utils.Log.m17064e(r1, r12)
        L_0x0253:
            android.content.Context r12 = r11.context
            boolean r12 = com.tencent.p214mm.opensdk.channel.MMessageActV2.send(r12, r3)
            return r12
        L_0x025a:
            android.content.Context r12 = r11.context
            boolean r12 = r11.sendPayReq(r12, r0)
            return r12
        L_0x0261:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "sendReq fail, WXMsgImpl has been detached"
            r12.<init>(r0)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.p214mm.opensdk.openapi.BaseWXApiImplV10.sendReq(com.tencent.mm.opensdk.modelbase.BaseReq):boolean");
    }

    public boolean sendResp(BaseResp baseResp) {
        String str;
        if (!this.detached) {
            if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
                str = "sendResp failed for wechat app signature check failed";
            } else if (!baseResp.checkArgs()) {
                str = "sendResp checkArgs fail";
            } else {
                Bundle bundle = new Bundle();
                baseResp.toBundle(bundle);
                MMessageActV2.Args args = new MMessageActV2.Args();
                args.bundle = bundle;
                args.content = "weixin://sendresp?appid=" + this.appId;
                args.targetPkgName = "com.tencent.mm";
                args.targetClassName = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
                return MMessageActV2.send(this.context, args);
            }
            Log.m17064e(TAG, str);
            return false;
        }
        throw new IllegalStateException("sendResp fail, WXMsgImpl has been detached");
    }

    public void setLogImpl(ILog iLog) {
        Log.setLogImpl(iLog);
    }

    public void unregisterApp() {
        if (this.detached) {
            throw new IllegalStateException("unregisterApp fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            Log.m17064e(TAG, "unregister app failed for wechat app signature check failed");
        } else {
            Log.m17063d(TAG, "unregisterApp, appId = " + this.appId);
            String str = this.appId;
            if (str == null || str.length() == 0) {
                Log.m17064e(TAG, "unregisterApp fail, appId is empty");
                return;
            }
            Log.m17063d(TAG, "unregister app " + this.context.getPackageName());
            C4894a.C4895a aVar = new C4894a.C4895a();
            aVar.f11075a = "com.tencent.mm";
            aVar.action = ConstantsAPI.ACTION_HANDLE_APP_UNREGISTER;
            aVar.content = "weixin://unregisterapp?appid=" + this.appId;
            C4894a.m17049a(this.context, aVar);
        }
    }
}
