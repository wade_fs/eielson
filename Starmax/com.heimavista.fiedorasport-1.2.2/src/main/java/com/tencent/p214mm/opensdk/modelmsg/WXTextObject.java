package com.tencent.p214mm.opensdk.modelmsg;

import android.os.Bundle;
import com.tencent.p214mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.p214mm.opensdk.utils.Log;

/* renamed from: com.tencent.mm.opensdk.modelmsg.WXTextObject */
public class WXTextObject implements WXMediaMessage.IMediaObject {
    private static final int LENGTH_LIMIT = 10240;
    private static final String TAG = "MicroMsg.SDK.WXTextObject";
    public String text;

    public WXTextObject() {
        this(null);
    }

    public WXTextObject(String str) {
        this.text = str;
    }

    public boolean checkArgs() {
        String str = this.text;
        if (str != null && str.length() != 0 && this.text.length() <= LENGTH_LIMIT) {
            return true;
        }
        Log.m17064e(TAG, "checkArgs fail, text is invalid");
        return false;
    }

    public void serialize(Bundle bundle) {
        bundle.putString("_wxtextobject_text", this.text);
    }

    public int type() {
        return 1;
    }

    public void unserialize(Bundle bundle) {
        this.text = bundle.getString("_wxtextobject_text");
    }
}
