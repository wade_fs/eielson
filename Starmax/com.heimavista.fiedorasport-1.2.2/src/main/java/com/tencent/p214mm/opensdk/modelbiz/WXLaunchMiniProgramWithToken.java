package com.tencent.p214mm.opensdk.modelbiz;

import android.os.Bundle;
import com.tencent.p214mm.opensdk.modelbase.BaseReq;
import com.tencent.p214mm.opensdk.modelbase.BaseResp;
import com.tencent.p214mm.opensdk.utils.C4912d;
import com.tencent.p214mm.opensdk.utils.Log;

/* renamed from: com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgramWithToken */
public class WXLaunchMiniProgramWithToken {

    /* renamed from: com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgramWithToken$Req */
    public static final class Req extends BaseReq {
        private static final String TAG = "MicroMsg.SDK.WXLaunchMiniProgramWithToken.Req";
        public String token;

        public final boolean checkArgs() {
            if (!C4912d.m17074b(this.token)) {
                return true;
            }
            Log.m17064e(TAG, "token is null");
            return false;
        }

        public final int getType() {
            return 29;
        }

        public final void toBundle(Bundle bundle) {
            super.toBundle(bundle);
            bundle.putString("_launch_wxminiprogram_token", this.token);
        }
    }

    /* renamed from: com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgramWithToken$Resp */
    public static final class Resp extends BaseResp {
        public static final int ERR_INVALID_TOKEN = -1000;
        public String extMsg;

        public Resp() {
        }

        public Resp(Bundle bundle) {
            fromBundle(bundle);
        }

        public final boolean checkArgs() {
            return true;
        }

        public final void fromBundle(Bundle bundle) {
            super.fromBundle(bundle);
            this.extMsg = bundle.getString("_launch_wxminiprogram_ext_msg");
        }

        public final int getType() {
            return 29;
        }

        public final void toBundle(Bundle bundle) {
            super.toBundle(bundle);
            bundle.putString("_launch_wxminiprogram_ext_msg", this.extMsg);
        }
    }
}
