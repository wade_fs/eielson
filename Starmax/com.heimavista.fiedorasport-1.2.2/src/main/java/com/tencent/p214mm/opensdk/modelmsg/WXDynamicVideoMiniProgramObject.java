package com.tencent.p214mm.opensdk.modelmsg;

import android.os.Bundle;
import com.tencent.p214mm.opensdk.utils.C4912d;
import com.tencent.p214mm.opensdk.utils.Log;

/* renamed from: com.tencent.mm.opensdk.modelmsg.WXDynamicVideoMiniProgramObject */
public class WXDynamicVideoMiniProgramObject extends WXMiniProgramObject {
    private static final String TAG = "MicroMsg.SDK.WXDynamicVideoMiniProgramObject";
    public String appThumbUrl;
    public String videoSource;

    public boolean checkArgs() {
        String str;
        if (C4912d.m17074b(super.webpageUrl)) {
            str = "webPageUrl is null";
        } else if (C4912d.m17074b(super.userName)) {
            str = "userName is null";
        } else {
            int i = super.miniprogramType;
            if (i >= 0 && i <= 2) {
                return true;
            }
            str = "miniprogram type should between MINIPTOGRAM_TYPE_RELEASE and MINIPROGRAM_TYPE_PREVIEW";
        }
        Log.m17064e(TAG, str);
        return false;
    }

    public void serialize(Bundle bundle) {
        bundle.putString("_wxminiprogram_webpageurl", super.webpageUrl);
        bundle.putString("_wxminiprogram_username", super.userName);
        bundle.putString("_wxminiprogram_path", super.path);
        bundle.putString("_wxminiprogram_videoSource", this.videoSource);
        bundle.putString("_wxminiprogram_appThumbUrl", this.appThumbUrl);
        bundle.putBoolean("_wxminiprogram_withsharetiket", super.withShareTicket);
        bundle.putInt("_wxminiprogram_type", super.miniprogramType);
        bundle.putInt("_wxminiprogram_disableforward", super.disableforward);
    }

    public int type() {
        return 46;
    }

    public void unserialize(Bundle bundle) {
        super.webpageUrl = bundle.getString("_wxminiprogram_webpageurl");
        super.userName = bundle.getString("_wxminiprogram_username");
        super.path = bundle.getString("_wxminiprogram_path");
        this.videoSource = bundle.getString("_wxminiprogram_videoSource");
        this.appThumbUrl = bundle.getString("_wxminiprogram_appThumbUrl");
        super.withShareTicket = bundle.getBoolean("_wxminiprogram_withsharetiket");
        super.miniprogramType = bundle.getInt("_wxminiprogram_type");
        super.disableforward = bundle.getInt("_wxminiprogram_disableforward");
    }
}
