package com.tencent.bugly;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;

/* compiled from: BUGLY */
public class Bugly {
    public static final String SDK_IS_DEV = "false";

    /* renamed from: a */
    private static boolean f10388a = false;
    public static Context applicationContext = null;

    /* renamed from: b */
    private static String[] f10389b = {"BuglyCrashModule", "BuglyRqdModule", "BuglyBetaModule"};

    /* renamed from: c */
    private static String[] f10390c = {"BuglyRqdModule", "BuglyCrashModule", "BuglyBetaModule"};
    public static boolean enable = true;
    public static Boolean isDev;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, com.tencent.bugly.proguard.o):java.util.Map
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]> */
    public static synchronized String getAppChannel() {
        byte[] bArr;
        synchronized (Bugly.class) {
            C4806a b = C4806a.m16492b();
            if (b == null) {
                return null;
            }
            if (TextUtils.isEmpty(b.f10534l)) {
                C4875p a = C4875p.m16899a();
                if (a == null) {
                    String str = b.f10534l;
                    return str;
                }
                Map<String, byte[]> a2 = a.mo26900a(556, (C4874o) null, true);
                if (!(a2 == null || (bArr = a2.get("app_channel")) == null)) {
                    String str2 = new String(bArr);
                    return str2;
                }
            }
            String str3 = b.f10534l;
            return str3;
        }
    }

    public static void init(Context context, String str, boolean z) {
        init(context, str, z, null);
    }

    public static boolean isDev() {
        if (isDev == null) {
            isDev = Boolean.valueOf(Boolean.parseBoolean(SDK_IS_DEV.replace("@", "")));
        }
        return isDev.booleanValue();
    }

    public static synchronized void init(Context context, String str, boolean z, BuglyStrategy buglyStrategy) {
        synchronized (Bugly.class) {
            if (!f10388a) {
                f10388a = true;
                Context a = C4893z.m17002a(context);
                applicationContext = a;
                if (a == null) {
                    Log.e(C4888x.f11043a, "init arg 'context' should not be null!");
                    return;
                }
                if (isDev()) {
                    f10389b = f10390c;
                }
                String[] strArr = f10389b;
                for (String str2 : strArr) {
                    try {
                        if (str2.equals("BuglyCrashModule")) {
                            C4792b.m16436a(CrashModule.getInstance());
                        } else if (!str2.equals("BuglyBetaModule") && !str2.equals("BuglyRqdModule")) {
                            str2.equals("BuglyFeedbackModule");
                        }
                    } catch (Throwable th) {
                        C4888x.m16981b(th);
                    }
                }
                C4792b.f10412a = enable;
                C4792b.m16435a(applicationContext, str, z, buglyStrategy);
            }
        }
    }
}
