package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: com.tencent.bugly.proguard.c */
/* compiled from: BUGLY */
public class C4859c extends C4836a {

    /* renamed from: d */
    protected HashMap<String, byte[]> f10910d = null;

    /* renamed from: e */
    private HashMap<String, Object> f10911e = new HashMap<>();

    /* renamed from: f */
    private C4865i f10912f = new C4865i();

    /* renamed from: a */
    public final /* bridge */ /* synthetic */ void mo26815a(String str) {
        super.mo26815a(str);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v16, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: byte[]} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> T mo26845b(java.lang.String r6, T r7) {
        /*
            r5 = this;
            java.util.HashMap<java.lang.String, byte[]> r0 = r5.f10910d
            r1 = 1
            r2 = 0
            r3 = 0
            if (r0 == 0) goto L_0x0046
            boolean r0 = r0.containsKey(r6)
            if (r0 != 0) goto L_0x000e
            return r2
        L_0x000e:
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r5.f10911e
            boolean r0 = r0.containsKey(r6)
            if (r0 == 0) goto L_0x001d
            java.util.HashMap<java.lang.String, java.lang.Object> r7 = r5.f10911e
            java.lang.Object r6 = r7.get(r6)
            return r6
        L_0x001d:
            java.util.HashMap<java.lang.String, byte[]> r0 = r5.f10910d
            java.lang.Object r0 = r0.get(r6)
            byte[] r0 = (byte[]) r0
            com.tencent.bugly.proguard.i r2 = r5.f10912f     // Catch:{ Exception -> 0x003f }
            r2.mo26869a(r0)     // Catch:{ Exception -> 0x003f }
            com.tencent.bugly.proguard.i r0 = r5.f10912f     // Catch:{ Exception -> 0x003f }
            java.lang.String r2 = r5.f10784b     // Catch:{ Exception -> 0x003f }
            r0.mo26863a(r2)     // Catch:{ Exception -> 0x003f }
            com.tencent.bugly.proguard.i r0 = r5.f10912f     // Catch:{ Exception -> 0x003f }
            java.lang.Object r7 = r0.mo26866a(r7, r3, r1)     // Catch:{ Exception -> 0x003f }
            if (r7 == 0) goto L_0x003e
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r5.f10911e     // Catch:{ Exception -> 0x003f }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x003f }
        L_0x003e:
            return r7
        L_0x003f:
            r6 = move-exception
            com.tencent.bugly.proguard.b r7 = new com.tencent.bugly.proguard.b
            r7.<init>(r6)
            throw r7
        L_0x0046:
            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>> r0 = r5.f10783a
            boolean r0 = r0.containsKey(r6)
            if (r0 != 0) goto L_0x004f
            return r2
        L_0x004f:
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r5.f10911e
            boolean r0 = r0.containsKey(r6)
            if (r0 == 0) goto L_0x005e
            java.util.HashMap<java.lang.String, java.lang.Object> r7 = r5.f10911e
            java.lang.Object r6 = r7.get(r6)
            return r6
        L_0x005e:
            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>> r0 = r5.f10783a
            java.lang.Object r0 = r0.get(r6)
            java.util.HashMap r0 = (java.util.HashMap) r0
            byte[] r2 = new byte[r3]
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x0086
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            r0.getKey()
            java.lang.Object r0 = r0.getValue()
            r2 = r0
            byte[] r2 = (byte[]) r2
        L_0x0086:
            com.tencent.bugly.proguard.i r0 = r5.f10912f     // Catch:{ Exception -> 0x009e }
            r0.mo26869a(r2)     // Catch:{ Exception -> 0x009e }
            com.tencent.bugly.proguard.i r0 = r5.f10912f     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r5.f10784b     // Catch:{ Exception -> 0x009e }
            r0.mo26863a(r2)     // Catch:{ Exception -> 0x009e }
            com.tencent.bugly.proguard.i r0 = r5.f10912f     // Catch:{ Exception -> 0x009e }
            java.lang.Object r7 = r0.mo26866a(r7, r3, r1)     // Catch:{ Exception -> 0x009e }
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r5.f10911e     // Catch:{ Exception -> 0x009e }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x009e }
            return r7
        L_0x009e:
            r6 = move-exception
            com.tencent.bugly.proguard.b r7 = new com.tencent.bugly.proguard.b
            r7.<init>(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4859c.mo26845b(java.lang.String, java.lang.Object):java.lang.Object");
    }

    /* renamed from: c */
    public void mo26846c() {
        this.f10910d = new HashMap<>();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void */
    /* renamed from: a */
    public <T> void mo26816a(String str, T t) {
        if (this.f10910d == null) {
            super.mo26816a(str, t);
        } else if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (!(t instanceof Set)) {
            C4867j jVar = new C4867j();
            jVar.mo26873a(super.f10784b);
            jVar.mo26879a((Object) t, 0);
            this.f10910d.put(str, C4869l.m16876a(jVar.mo26874a()));
        } else {
            throw new IllegalArgumentException("can not support Set");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public byte[] mo26818a() {
        if (this.f10910d == null) {
            return super.mo26818a();
        }
        C4867j jVar = new C4867j(0);
        jVar.mo26873a(super.f10784b);
        jVar.mo26882a((Map) this.f10910d, 0);
        return C4869l.m16876a(jVar.mo26874a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* renamed from: a */
    public void mo26817a(byte[] bArr) {
        try {
            super.mo26817a(bArr);
        } catch (Exception unused) {
            this.f10912f.mo26869a(bArr);
            this.f10912f.mo26863a(super.f10784b);
            HashMap hashMap = new HashMap(1);
            hashMap.put("", new byte[0]);
            this.f10910d = this.f10912f.mo26867a((Map) hashMap, 0, false);
        }
    }
}
