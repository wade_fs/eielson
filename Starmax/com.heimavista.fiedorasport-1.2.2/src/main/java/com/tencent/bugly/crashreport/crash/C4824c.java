package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import android.os.Build;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.common.info.AppInfo;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.anr.C4819b;
import com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4878r;
import com.tencent.bugly.proguard.C4881u;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.tencent.bugly.crashreport.crash.c */
/* compiled from: BUGLY */
public final class C4824c {

    /* renamed from: a */
    public static int f10689a = 0;

    /* renamed from: b */
    public static boolean f10690b = false;

    /* renamed from: c */
    public static int f10691c = 2;

    /* renamed from: d */
    public static boolean f10692d = true;

    /* renamed from: e */
    public static int f10693e = 20480;

    /* renamed from: f */
    public static int f10694f = 20480;

    /* renamed from: g */
    public static long f10695g = 604800000;

    /* renamed from: h */
    public static String f10696h = null;

    /* renamed from: i */
    public static boolean f10697i = false;

    /* renamed from: j */
    public static String f10698j = null;

    /* renamed from: k */
    public static int f10699k = 5000;

    /* renamed from: l */
    public static boolean f10700l = true;

    /* renamed from: m */
    public static boolean f10701m = false;

    /* renamed from: n */
    public static String f10702n;

    /* renamed from: o */
    public static String f10703o;

    /* renamed from: r */
    private static C4824c f10704r;

    /* renamed from: p */
    public final C4822b f10705p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public final Context f10706q;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public final C4830e f10707s;

    /* renamed from: t */
    private final NativeCrashHandler f10708t;

    /* renamed from: u */
    private C4809a f10709u = C4809a.m16589a();

    /* renamed from: v */
    private C4886w f10710v;

    /* renamed from: w */
    private final C4819b f10711w;

    /* renamed from: x */
    private Boolean f10712x;

    /* renamed from: com.tencent.bugly.crashreport.crash.c$a */
    /* compiled from: BUGLY */
    class C4825a implements Runnable {

        /* renamed from: P */
        private /* synthetic */ boolean f10713P;

        /* renamed from: Q */
        private /* synthetic */ Thread f10714Q;

        /* renamed from: R */
        private /* synthetic */ Throwable f10715R;

        /* renamed from: S */
        private /* synthetic */ String f10716S;

        /* renamed from: T */
        private /* synthetic */ byte[] f10717T;

        /* renamed from: U */
        private /* synthetic */ boolean f10718U;

        C4825a(boolean z, Thread thread, Throwable th, String str, byte[] bArr, boolean z2) {
            this.f10713P = z;
            this.f10714Q = thread;
            this.f10715R = th;
            this.f10716S = str;
            this.f10717T = bArr;
            this.f10718U = z2;
        }

        public final void run() {
            try {
                C4888x.m16982c("post a throwable %b", Boolean.valueOf(this.f10713P));
                C4824c.this.f10707s.mo26777a(this.f10714Q, this.f10715R, false, this.f10716S, this.f10717T);
                if (this.f10718U) {
                    C4888x.m16977a("clear user datas", new Object[0]);
                    C4806a.m16491a(C4824c.this.f10706q).mo26657C();
                }
            } catch (Throwable th) {
                if (!C4888x.m16981b(th)) {
                    th.printStackTrace();
                }
                C4888x.m16984e("java catch error: %s", this.f10715R.toString());
            }
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.crash.c$b */
    /* compiled from: BUGLY */
    class C4826b extends Thread {
        C4826b() {
        }

        public final void run() {
            ArrayList arrayList;
            if (C4893z.m17021a(C4824c.this.f10706q, "local_crash_lock", 10000)) {
                List<CrashDetailBean> a = C4824c.this.f10705p.mo26746a();
                if (a != null && a.size() > 0) {
                    C4888x.m16982c("Size of crash list: %s", Integer.valueOf(a.size()));
                    int size = a.size();
                    if (((long) size) > 20) {
                        ArrayList arrayList2 = new ArrayList();
                        Collections.sort(a);
                        for (int i = 0; ((long) i) < 20; i++) {
                            arrayList2.add(a.get((size - 1) - i));
                        }
                        arrayList = arrayList2;
                    } else {
                        arrayList = a;
                    }
                    C4824c.this.f10705p.mo26748a(arrayList, 0, false, false, false);
                }
                C4893z.m17040b(C4824c.this.f10706q, "local_crash_lock");
            }
        }
    }

    private C4824c(int i, Context context, C4886w wVar, boolean z, BuglyStrategy.C4791a aVar, C4874o oVar, String str) {
        f10689a = i;
        Context a = C4893z.m17002a(context);
        this.f10706q = a;
        this.f10710v = wVar;
        this.f10705p = new C4822b(i, a, C4881u.m16932a(), C4875p.m16899a(), this.f10709u, aVar, oVar);
        C4806a a2 = C4806a.m16491a(a);
        this.f10707s = new C4830e(a, this.f10705p, this.f10709u, a2);
        C4886w wVar2 = wVar;
        this.f10708t = NativeCrashHandler.getInstance(a, a2, this.f10705p, this.f10709u, wVar2, z, str);
        a2.f10475D = this.f10708t;
        this.f10711w = new C4819b(a, this.f10709u, a2, wVar2, this.f10705p);
    }

    /* renamed from: c */
    public final synchronized void mo26760c() {
        this.f10707s.mo26775a();
        this.f10708t.setUserOpened(true);
        if (Build.VERSION.SDK_INT <= 19) {
            this.f10711w.mo26738a(true);
        } else {
            this.f10711w.mo26742c();
        }
    }

    /* renamed from: d */
    public final synchronized void mo26761d() {
        this.f10707s.mo26778b();
        this.f10708t.setUserOpened(false);
        if (Build.VERSION.SDK_INT < 19) {
            this.f10711w.mo26738a(false);
        } else {
            this.f10711w.mo26743d();
        }
    }

    /* renamed from: e */
    public final void mo26762e() {
        this.f10707s.mo26775a();
    }

    /* renamed from: f */
    public final void mo26763f() {
        this.f10708t.setUserOpened(false);
    }

    /* renamed from: g */
    public final void mo26764g() {
        this.f10708t.setUserOpened(true);
    }

    /* renamed from: h */
    public final void mo26765h() {
        if (Build.VERSION.SDK_INT <= 19) {
            this.f10711w.mo26738a(true);
        } else {
            this.f10711w.mo26742c();
        }
    }

    /* renamed from: i */
    public final void mo26766i() {
        if (Build.VERSION.SDK_INT < 19) {
            this.f10711w.mo26738a(false);
        } else {
            this.f10711w.mo26743d();
        }
    }

    /* renamed from: j */
    public final synchronized void mo26767j() {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (i < 30) {
                try {
                    C4888x.m16977a("try main sleep for make a test anr! try:%d/30 , kill it if you don't want to wait!", Integer.valueOf(i2));
                    C4893z.m17037b((long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                    i = i2;
                } catch (Throwable th) {
                    if (!C4888x.m16978a(th)) {
                        th.printStackTrace();
                    }
                    return;
                }
            }
        }
    }

    /* renamed from: k */
    public final boolean mo26768k() {
        return this.f10711w.mo26739a();
    }

    /* renamed from: l */
    public final void mo26769l() {
        this.f10708t.checkUploadRecordCrash();
    }

    /* renamed from: m */
    public final void mo26770m() {
        if (C4806a.m16492b().f10526d.equals(AppInfo.m16478a(this.f10706q))) {
            this.f10708t.removeEmptyNativeRecordFiles();
        }
    }

    /* renamed from: a */
    public static synchronized C4824c m16657a(int i, Context context, boolean z, BuglyStrategy.C4791a aVar, C4874o oVar, String str) {
        C4824c cVar;
        synchronized (C4824c.class) {
            if (f10704r == null) {
                f10704r = new C4824c(1004, context, C4886w.m16969a(), z, aVar, null, null);
            }
            cVar = f10704r;
        }
        return cVar;
    }

    /* renamed from: b */
    public final boolean mo26759b() {
        Boolean bool = this.f10712x;
        if (bool != null) {
            return bool.booleanValue();
        }
        String str = C4806a.m16492b().f10526d;
        List<C4878r> a = C4875p.m16899a().mo26899a(1);
        ArrayList arrayList = new ArrayList();
        if (a == null || a.size() <= 0) {
            this.f10712x = false;
            return false;
        }
        for (C4878r rVar : a) {
            if (str.equals(rVar.f10983c)) {
                this.f10712x = true;
                arrayList.add(rVar);
            }
        }
        if (arrayList.size() > 0) {
            C4875p.m16899a().mo26901a(arrayList);
        }
        return true;
    }

    /* renamed from: a */
    public static synchronized C4824c m16656a() {
        C4824c cVar;
        synchronized (C4824c.class) {
            cVar = f10704r;
        }
        return cVar;
    }

    /* renamed from: a */
    public final void mo26755a(StrategyBean strategyBean) {
        this.f10707s.mo26776a(strategyBean);
        this.f10708t.onStrategyChanged(strategyBean);
        this.f10711w.mo26736a(strategyBean);
        C4886w.m16969a().mo26935a(new C4826b(), 3000);
    }

    /* renamed from: a */
    public final synchronized void mo26758a(boolean z, boolean z2, boolean z3) {
        this.f10708t.testNativeCrash(z, z2, z3);
    }

    /* renamed from: a */
    public final void mo26757a(Thread thread, Throwable th, boolean z, String str, byte[] bArr, boolean z2) {
        this.f10710v.mo26934a(new C4825a(false, thread, th, null, null, z2));
    }

    /* renamed from: a */
    public final void mo26756a(CrashDetailBean crashDetailBean) {
        this.f10705p.mo26753d(crashDetailBean);
    }

    /* renamed from: a */
    public final void mo26754a(long j) {
        C4886w.m16969a().mo26935a(new C4826b(), j);
    }
}
