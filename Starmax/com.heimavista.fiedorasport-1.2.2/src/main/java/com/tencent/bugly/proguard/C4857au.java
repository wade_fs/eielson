package com.tencent.bugly.proguard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.au */
/* compiled from: BUGLY */
public final class C4857au extends C4868k implements Cloneable {

    /* renamed from: f */
    private static ArrayList<C4856at> f10903f;

    /* renamed from: g */
    private static Map<String, String> f10904g;

    /* renamed from: a */
    public byte f10905a = 0;

    /* renamed from: b */
    public String f10906b = "";

    /* renamed from: c */
    public String f10907c = "";

    /* renamed from: d */
    public ArrayList<C4856at> f10908d = null;

    /* renamed from: e */
    public Map<String, String> f10909e = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.at>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26875a(this.f10905a, 0);
        String str = this.f10906b;
        if (str != null) {
            jVar.mo26880a(str, 1);
        }
        String str2 = this.f10907c;
        if (str2 != null) {
            jVar.mo26880a(str2, 2);
        }
        ArrayList<C4856at> arrayList = this.f10908d;
        if (arrayList != null) {
            jVar.mo26881a((Collection) arrayList, 3);
        }
        Map<String, String> map = this.f10909e;
        if (map != null) {
            jVar.mo26882a((Map) map, 4);
        }
    }

    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.at>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10905a = iVar.mo26861a(this.f10905a, 0, true);
        this.f10906b = iVar.mo26871b(1, false);
        this.f10907c = iVar.mo26871b(2, false);
        if (f10903f == null) {
            f10903f = new ArrayList<>();
            f10903f.add(new C4856at());
        }
        this.f10908d = (ArrayList) iVar.mo26866a((Object) f10903f, 3, false);
        if (f10904g == null) {
            f10904g = new HashMap();
            f10904g.put("", "");
        }
        this.f10909e = (Map) iVar.mo26866a((Object) f10904g, 4, false);
    }
}
