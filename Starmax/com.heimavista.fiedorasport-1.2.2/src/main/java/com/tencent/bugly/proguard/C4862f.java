package com.tencent.bugly.proguard;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.f */
/* compiled from: BUGLY */
public final class C4862f extends C4868k {

    /* renamed from: k */
    private static byte[] f10917k = null;

    /* renamed from: l */
    private static Map<String, String> f10918l = null;

    /* renamed from: m */
    private static /* synthetic */ boolean f10919m = (!C4862f.class.desiredAssertionStatus());

    /* renamed from: a */
    public short f10920a = 0;

    /* renamed from: b */
    public int f10921b = 0;

    /* renamed from: c */
    public String f10922c = null;

    /* renamed from: d */
    public String f10923d = null;

    /* renamed from: e */
    public byte[] f10924e;

    /* renamed from: f */
    private byte f10925f = 0;

    /* renamed from: g */
    private int f10926g = 0;

    /* renamed from: h */
    private int f10927h = 0;

    /* renamed from: i */
    private Map<String, String> f10928i;

    /* renamed from: j */
    private Map<String, String> f10929j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26883a(this.f10920a, 1);
        jVar.mo26875a(this.f10925f, 2);
        jVar.mo26876a(this.f10926g, 3);
        jVar.mo26876a(this.f10921b, 4);
        jVar.mo26880a(this.f10922c, 5);
        jVar.mo26880a(this.f10923d, 6);
        jVar.mo26885a(this.f10924e, 7);
        jVar.mo26876a(this.f10927h, 8);
        jVar.mo26882a((Map) this.f10928i, 9);
        jVar.mo26882a((Map) this.f10929j, 10);
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f10919m) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final boolean equals(Object obj) {
        C4862f fVar = (C4862f) obj;
        return C4869l.m16872a(1, fVar.f10920a) && C4869l.m16872a(1, fVar.f10925f) && C4869l.m16872a(1, fVar.f10926g) && C4869l.m16872a(1, fVar.f10921b) && C4869l.m16874a(1, fVar.f10922c) && C4869l.m16874a(1, fVar.f10923d) && C4869l.m16874a(1, fVar.f10924e) && C4869l.m16872a(1, fVar.f10927h) && C4869l.m16874a(1, fVar.f10928i) && C4869l.m16874a(1, fVar.f10929j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        try {
            this.f10920a = iVar.mo26868a(this.f10920a, 1, true);
            this.f10925f = iVar.mo26861a(this.f10925f, 2, true);
            this.f10926g = iVar.mo26862a(this.f10926g, 3, true);
            this.f10921b = iVar.mo26862a(this.f10921b, 4, true);
            this.f10922c = iVar.mo26871b(5, true);
            this.f10923d = iVar.mo26871b(6, true);
            if (f10917k == null) {
                f10917k = new byte[]{0};
            }
            this.f10924e = iVar.mo26872c(7, true);
            this.f10927h = iVar.mo26862a(this.f10927h, 8, true);
            if (f10918l == null) {
                HashMap hashMap = new HashMap();
                f10918l = hashMap;
                hashMap.put("", "");
            }
            this.f10928i = (Map) iVar.mo26866a((Object) f10918l, 9, true);
            if (f10918l == null) {
                HashMap hashMap2 = new HashMap();
                f10918l = hashMap2;
                hashMap2.put("", "");
            }
            this.f10929j = (Map) iVar.mo26866a((Object) f10918l, 10, true);
        } catch (Exception e) {
            e.printStackTrace();
            PrintStream printStream = System.out;
            printStream.println("RequestPacket decode error " + C4861e.m16810a(this.f10924e));
            throw new RuntimeException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.h.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(int, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(long, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(com.tencent.bugly.proguard.k, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(short, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(boolean, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h */
    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
        C4864h hVar = new C4864h(sb, i);
        hVar.mo26858a(this.f10920a, "iVersion");
        hVar.mo26852a(this.f10925f, "cPacketType");
        hVar.mo26853a(this.f10926g, "iMessageType");
        hVar.mo26853a(this.f10921b, "iRequestId");
        hVar.mo26856a(this.f10922c, "sServantName");
        hVar.mo26856a(this.f10923d, "sFuncName");
        hVar.mo26860a(this.f10924e, "sBuffer");
        hVar.mo26853a(this.f10927h, "iTimeout");
        hVar.mo26857a((Map) this.f10928i, "context");
        hVar.mo26857a((Map) this.f10929j, "status");
    }
}
