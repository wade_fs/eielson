package com.tencent.bugly.proguard;

import android.content.Context;
import android.os.Process;
import android.util.Base64;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.tencent.bugly.C4792b;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.tencent.bugly.proguard.u */
/* compiled from: BUGLY */
public final class C4881u {

    /* renamed from: b */
    private static C4881u f10991b;

    /* renamed from: a */
    public boolean f10992a = true;

    /* renamed from: c */
    private final C4875p f10993c;

    /* renamed from: d */
    private final Context f10994d;

    /* renamed from: e */
    private Map<Integer, Long> f10995e = new HashMap();

    /* renamed from: f */
    private long f10996f;

    /* renamed from: g */
    private long f10997g;

    /* renamed from: h */
    private LinkedBlockingQueue<Runnable> f10998h = new LinkedBlockingQueue<>();

    /* renamed from: i */
    private LinkedBlockingQueue<Runnable> f10999i = new LinkedBlockingQueue<>();
    /* access modifiers changed from: private */

    /* renamed from: j */
    public final Object f11000j = new Object();

    /* renamed from: k */
    private String f11001k = null;

    /* renamed from: l */
    private byte[] f11002l = null;

    /* renamed from: m */
    private long f11003m = 0;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public byte[] f11004n = null;

    /* renamed from: o */
    private long f11005o = 0;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public String f11006p = null;

    /* renamed from: q */
    private long f11007q = 0;

    /* renamed from: r */
    private final Object f11008r = new Object();
    /* access modifiers changed from: private */

    /* renamed from: s */
    public boolean f11009s = false;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public final Object f11010t = new Object();

    /* renamed from: u */
    private int f11011u = 0;

    /* renamed from: com.tencent.bugly.proguard.u$a */
    /* compiled from: BUGLY */
    class C4882a implements Runnable {

        /* renamed from: P */
        private /* synthetic */ Runnable f11012P;

        C4882a(Runnable runnable) {
            this.f11012P = runnable;
        }

        public final void run() {
            this.f11012P.run();
            synchronized (C4881u.this.f11000j) {
                C4881u.m16942b(C4881u.this);
            }
        }
    }

    /* renamed from: com.tencent.bugly.proguard.u$b */
    /* compiled from: BUGLY */
    class C4883b implements Runnable {

        /* renamed from: P */
        private /* synthetic */ int f11014P;

        /* renamed from: Q */
        private /* synthetic */ LinkedBlockingQueue f11015Q;

        C4883b(C4881u uVar, int i, LinkedBlockingQueue linkedBlockingQueue) {
            this.f11014P = i;
            this.f11015Q = linkedBlockingQueue;
        }

        public final void run() {
            Runnable runnable;
            for (int i = 0; i < this.f11014P && (runnable = (Runnable) this.f11015Q.poll()) != null; i++) {
                runnable.run();
            }
        }
    }

    private C4881u(Context context) {
        this.f10994d = context;
        this.f10993c = C4875p.m16899a();
        try {
            Class.forName("android.util.Base64");
        } catch (ClassNotFoundException unused) {
            C4888x.m16977a("[UploadManager] Error: Can not find Base64 class, will not use stronger security way to upload", new Object[0]);
            this.f10992a = false;
        }
        if (this.f10992a) {
            this.f11001k = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDP9x32s5pPtZBXzJBz2GWM/sbTvVO2+RvW0PH01IdaBxc/" + "fB6fbHZocC9T3nl1+J5eAFjIRVuV8vHDky7Qo82Mnh0PVvcZIEQvMMVKU8dsMQopxgsOs2gkSHJwgWdinKNS8CmWobo6pFwPUW11lMv714jAUZRq2GBOqiO2vQI6iwIDAQAB";
        }
    }

    /* renamed from: b */
    static /* synthetic */ int m16942b(C4881u uVar) {
        int i = uVar.f11011u - 1;
        uVar.f11011u = i;
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o, boolean):boolean
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o, boolean):long
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o, boolean):boolean */
    /* renamed from: c */
    private static boolean m16944c() {
        C4888x.m16982c("[UploadManager] Drop security info of database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C4875p a = C4875p.m16899a();
            if (a != null) {
                return a.mo26902a(555, "security_info", (C4874o) null, true);
            }
            C4888x.m16983d("[UploadManager] Failed to get Database", new Object[0]);
            return false;
        } catch (Throwable th) {
            C4888x.m16978a(th);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean */
    /* renamed from: d */
    private boolean m16947d() {
        C4888x.m16982c("[UploadManager] Record security info to database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C4875p a = C4875p.m16899a();
            if (a == null) {
                C4888x.m16983d("[UploadManager] Failed to get database", new Object[0]);
                return false;
            }
            StringBuilder sb = new StringBuilder();
            if (this.f11004n != null) {
                sb.append(Base64.encodeToString(this.f11004n, 0));
                sb.append("#");
                if (this.f11005o != 0) {
                    sb.append(Long.toString(this.f11005o));
                } else {
                    sb.append("null");
                }
                sb.append("#");
                if (this.f11006p != null) {
                    sb.append(this.f11006p);
                } else {
                    sb.append("null");
                }
                sb.append("#");
                if (this.f11007q != 0) {
                    sb.append(Long.toString(this.f11007q));
                } else {
                    sb.append("null");
                }
                a.mo26903a(555, "security_info", sb.toString().getBytes(), (C4874o) null, true);
                return true;
            }
            C4888x.m16982c("[UploadManager] AES key is null, will not record", new Object[0]);
            return false;
        } catch (Throwable th) {
            C4888x.m16978a(th);
            m16944c();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, com.tencent.bugly.proguard.o):java.util.Map
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]> */
    /* access modifiers changed from: private */
    /* renamed from: e */
    public boolean m16949e() {
        boolean z;
        C4888x.m16982c("[UploadManager] Load security info from database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C4875p a = C4875p.m16899a();
            if (a == null) {
                C4888x.m16983d("[UploadManager] Failed to get database", new Object[0]);
                return false;
            }
            Map<String, byte[]> a2 = a.mo26900a(555, (C4874o) null, true);
            if (a2 != null && a2.containsKey("security_info")) {
                String str = new String(a2.get("security_info"));
                String[] split = str.split("#");
                if (split.length == 4) {
                    if (!split[0].isEmpty()) {
                        if (!split[0].equals("null")) {
                            this.f11004n = Base64.decode(split[0], 0);
                        }
                    }
                    z = false;
                    if (!z && !split[1].isEmpty() && !split[1].equals("null")) {
                        try {
                            this.f11005o = Long.parseLong(split[1]);
                        } catch (Throwable th) {
                            C4888x.m16978a(th);
                            z = true;
                        }
                    }
                    if (!z && !split[2].isEmpty() && !split[2].equals("null")) {
                        this.f11006p = split[2];
                    }
                    if (!z && !split[3].isEmpty() && !split[3].equals("null")) {
                        try {
                            this.f11007q = Long.parseLong(split[3]);
                        } catch (Throwable th2) {
                            C4888x.m16978a(th2);
                        }
                    }
                } else {
                    C4888x.m16977a("SecurityInfo = %s, Strings.length = %d", str, Integer.valueOf(split.length));
                    z = true;
                }
                if (z) {
                    m16944c();
                }
            }
            return true;
        } catch (Throwable th3) {
            C4888x.m16978a(th3);
            return false;
        }
    }

    /* renamed from: b */
    public final boolean mo26926b(int i) {
        if (C4792b.f10414c) {
            C4888x.m16982c("Uploading frequency will not be checked if SDK is in debug mode.", new Object[0]);
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - mo26914a(i);
        C4888x.m16982c("[UploadManager] Time interval is %d seconds since last uploading(ID: %d).", Long.valueOf(currentTimeMillis / 1000), Integer.valueOf(i));
        if (currentTimeMillis >= 30000) {
            return true;
        }
        C4888x.m16977a("[UploadManager] Data only be uploaded once in %d seconds.", 30L);
        return false;
    }

    /* renamed from: com.tencent.bugly.proguard.u$c */
    /* compiled from: BUGLY */
    class C4884c implements Runnable {

        /* renamed from: P */
        private final Context f11016P;

        /* renamed from: Q */
        private final Runnable f11017Q;

        /* renamed from: R */
        private final long f11018R;

        public C4884c(Context context) {
            this.f11016P = context;
            this.f11017Q = null;
            this.f11018R = 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, boolean):boolean
         arg types: [com.tencent.bugly.proguard.u, int]
         candidates:
          com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, int):void
          com.tencent.bugly.proguard.u.a(java.lang.Runnable, long):void
          com.tencent.bugly.proguard.u.a(java.lang.Runnable, boolean):boolean
          com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, byte[]):byte[]
          com.tencent.bugly.proguard.u.a(int, long):void
          com.tencent.bugly.proguard.u.a(int, com.tencent.bugly.proguard.aq):void
          com.tencent.bugly.proguard.u.a(long, boolean):void
          com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, boolean):boolean */
        public final void run() {
            if (!C4893z.m17021a(this.f11016P, "security_info", 30000)) {
                C4888x.m16982c("[UploadManager] Sleep %d try to lock security file again (pid=%d | tid=%d)", 5000, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                C4893z.m17037b((long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                if (C4893z.m17016a(this, "BUGLY_ASYNC_UPLOAD") == null) {
                    C4888x.m16983d("[UploadManager] Failed to start a thread to execute task of initializing security context, try to post it into thread pool.", new Object[0]);
                    C4886w a = C4886w.m16969a();
                    if (a != null) {
                        a.mo26934a(this);
                    } else {
                        C4888x.m16984e("[UploadManager] Asynchronous thread pool is unavailable now, try next time.", new Object[0]);
                    }
                }
            } else {
                if (!C4881u.this.m16949e()) {
                    C4888x.m16983d("[UploadManager] Failed to load security info from database", new Object[0]);
                    C4881u.this.mo26924b(false);
                }
                if (C4881u.this.f11006p != null) {
                    if (C4881u.this.mo26925b()) {
                        C4888x.m16982c("[UploadManager] Sucessfully got session ID, try to execute upload tasks now (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                        Runnable runnable = this.f11017Q;
                        if (runnable != null) {
                            C4881u.this.m16937a(runnable, this.f11018R);
                        }
                        C4881u.this.m16943c(0);
                        C4893z.m17040b(this.f11016P, "security_info");
                        synchronized (C4881u.this.f11010t) {
                            boolean unused = C4881u.this.f11009s = false;
                        }
                        return;
                    }
                    C4888x.m16977a("[UploadManager] Session ID is expired, drop it.", new Object[0]);
                    C4881u.this.mo26924b(true);
                }
                byte[] a2 = C4893z.m17025a(128);
                if (a2 == null || (a2.length << 3) != 128) {
                    C4888x.m16983d("[UploadManager] Failed to create AES key (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                    C4881u.this.mo26924b(false);
                    C4893z.m17040b(this.f11016P, "security_info");
                    synchronized (C4881u.this.f11010t) {
                        boolean unused2 = C4881u.this.f11009s = false;
                    }
                    return;
                }
                byte[] unused3 = C4881u.this.f11004n = a2;
                C4888x.m16982c("[UploadManager] Execute one upload task for requesting session ID (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                Runnable runnable2 = this.f11017Q;
                if (runnable2 != null) {
                    C4881u.this.m16937a(runnable2, this.f11018R);
                } else {
                    C4881u.this.m16943c(1);
                }
            }
        }

        public C4884c(Context context, Runnable runnable, long j) {
            this.f11016P = context;
            this.f11017Q = runnable;
            this.f11018R = j;
        }
    }

    /* renamed from: a */
    public static synchronized C4881u m16933a(Context context) {
        C4881u uVar;
        synchronized (C4881u.class) {
            if (f10991b == null) {
                f10991b = new C4881u(context);
            }
            uVar = f10991b;
        }
        return uVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.u.a(java.lang.Runnable, boolean):boolean
     arg types: [java.lang.Runnable, int]
     candidates:
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, int):void
      com.tencent.bugly.proguard.u.a(java.lang.Runnable, long):void
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, boolean):boolean
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, byte[]):byte[]
      com.tencent.bugly.proguard.u.a(int, long):void
      com.tencent.bugly.proguard.u.a(int, com.tencent.bugly.proguard.aq):void
      com.tencent.bugly.proguard.u.a(long, boolean):void
      com.tencent.bugly.proguard.u.a(java.lang.Runnable, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b9, code lost:
        if (r5 <= 0) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00bb, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Execute urgent upload tasks of queue which has %d tasks (pid=%d | tid=%d)", java.lang.Integer.valueOf(r5), java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00dc, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00dd, code lost:
        if (r7 >= r5) goto L_0x012b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00df, code lost:
        r8 = (java.lang.Runnable) r2.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e5, code lost:
        if (r8 == null) goto L_0x012b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00e7, code lost:
        r10 = r12.f11000j;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e9, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00ec, code lost:
        if (r12.f11011u < 2) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ee, code lost:
        if (r1 == null) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f0, code lost:
        r1.mo26934a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00f3, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00f5, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f6, code lost:
        com.tencent.bugly.proguard.C4888x.m16977a("[UploadManager] Create and start a new thread to execute a upload task: %s", "BUGLY_ASYNC_UPLOAD");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x010c, code lost:
        if (com.tencent.bugly.proguard.C4893z.m17016a(new com.tencent.bugly.proguard.C4881u.C4882a(r12, r8), "BUGLY_ASYNC_UPLOAD") == null) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x010e, code lost:
        r8 = r12.f11000j;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0110, code lost:
        monitor-enter(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r12.f11011u++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0116, code lost:
        monitor-exit(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x011b, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("[UploadManager] Failed to start a thread to execute asynchronous upload task, will try again next time.", new java.lang.Object[0]);
        m16940a(r8, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0125, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x012b, code lost:
        if (r13 <= 0) goto L_0x014e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x012d, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Execute upload tasks of queue which has %d tasks (pid=%d | tid=%d)", java.lang.Integer.valueOf(r13), java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x014e, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0150, code lost:
        r1.mo26934a(new com.tencent.bugly.proguard.C4881u.C4883b(r12, r13, r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006c A[Catch:{ all -> 0x007f }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0093 A[Catch:{ all -> 0x007f }] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m16943c(int r13) {
        /*
            r12 = this;
            r0 = 0
            if (r13 >= 0) goto L_0x000b
            java.lang.Object[] r13 = new java.lang.Object[r0]
            java.lang.String r0 = "[UploadManager] Number of task to execute should >= 0"
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r13)
            return
        L_0x000b:
            com.tencent.bugly.proguard.w r1 = com.tencent.bugly.proguard.C4886w.m16969a()
            java.util.concurrent.LinkedBlockingQueue r2 = new java.util.concurrent.LinkedBlockingQueue
            r2.<init>()
            java.util.concurrent.LinkedBlockingQueue r3 = new java.util.concurrent.LinkedBlockingQueue
            r3.<init>()
            java.lang.Object r4 = r12.f11000j
            monitor-enter(r4)
            java.lang.String r5 = "[UploadManager] Try to poll all upload task need and put them into temp queue (pid=%d | tid=%d)"
            r6 = 2
            java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ all -> 0x0159 }
            int r8 = android.os.Process.myPid()     // Catch:{ all -> 0x0159 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0159 }
            r7[r0] = r8     // Catch:{ all -> 0x0159 }
            int r8 = android.os.Process.myTid()     // Catch:{ all -> 0x0159 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0159 }
            r9 = 1
            r7[r9] = r8     // Catch:{ all -> 0x0159 }
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r7)     // Catch:{ all -> 0x0159 }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r5 = r12.f10998h     // Catch:{ all -> 0x0159 }
            int r5 = r5.size()     // Catch:{ all -> 0x0159 }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r7 = r12.f10999i     // Catch:{ all -> 0x0159 }
            int r7 = r7.size()     // Catch:{ all -> 0x0159 }
            if (r5 != 0) goto L_0x0052
            if (r7 != 0) goto L_0x0052
            java.lang.String r13 = "[UploadManager] There is no upload task in queue."
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0159 }
            com.tencent.bugly.proguard.C4888x.m16982c(r13, r0)     // Catch:{ all -> 0x0159 }
            monitor-exit(r4)     // Catch:{ all -> 0x0159 }
            return
        L_0x0052:
            if (r13 == 0) goto L_0x005f
            if (r13 >= r5) goto L_0x0059
            r5 = r13
            r13 = 0
            goto L_0x0060
        L_0x0059:
            int r8 = r5 + r7
            if (r13 >= r8) goto L_0x005f
            int r13 = r13 - r5
            goto L_0x0060
        L_0x005f:
            r13 = r7
        L_0x0060:
            if (r1 == 0) goto L_0x0068
            boolean r7 = r1.mo26937c()     // Catch:{ all -> 0x0159 }
            if (r7 != 0) goto L_0x0069
        L_0x0068:
            r13 = 0
        L_0x0069:
            r7 = 0
        L_0x006a:
            if (r7 >= r5) goto L_0x0090
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f10998h     // Catch:{ all -> 0x0159 }
            java.lang.Object r8 = r8.peek()     // Catch:{ all -> 0x0159 }
            java.lang.Runnable r8 = (java.lang.Runnable) r8     // Catch:{ all -> 0x0159 }
            if (r8 == 0) goto L_0x0090
            r2.put(r8)     // Catch:{ all -> 0x007f }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f10998h     // Catch:{ all -> 0x007f }
            r8.poll()     // Catch:{ all -> 0x007f }
            goto L_0x008d
        L_0x007f:
            r8 = move-exception
            java.lang.String r10 = "[UploadManager] Failed to add upload task to temp urgent queue: %s"
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0159 }
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x0159 }
            r11[r0] = r8     // Catch:{ all -> 0x0159 }
            com.tencent.bugly.proguard.C4888x.m16984e(r10, r11)     // Catch:{ all -> 0x0159 }
        L_0x008d:
            int r7 = r7 + 1
            goto L_0x006a
        L_0x0090:
            r7 = 0
        L_0x0091:
            if (r7 >= r13) goto L_0x00b7
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f10999i     // Catch:{ all -> 0x0159 }
            java.lang.Object r8 = r8.peek()     // Catch:{ all -> 0x0159 }
            java.lang.Runnable r8 = (java.lang.Runnable) r8     // Catch:{ all -> 0x0159 }
            if (r8 == 0) goto L_0x00b7
            r3.put(r8)     // Catch:{ all -> 0x00a6 }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f10999i     // Catch:{ all -> 0x00a6 }
            r8.poll()     // Catch:{ all -> 0x00a6 }
            goto L_0x00b4
        L_0x00a6:
            r8 = move-exception
            java.lang.String r10 = "[UploadManager] Failed to add upload task to temp urgent queue: %s"
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0159 }
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x0159 }
            r11[r0] = r8     // Catch:{ all -> 0x0159 }
            com.tencent.bugly.proguard.C4888x.m16984e(r10, r11)     // Catch:{ all -> 0x0159 }
        L_0x00b4:
            int r7 = r7 + 1
            goto L_0x0091
        L_0x00b7:
            monitor-exit(r4)     // Catch:{ all -> 0x0159 }
            r4 = 3
            if (r5 <= 0) goto L_0x00dc
            java.lang.Object[] r7 = new java.lang.Object[r4]
            java.lang.Integer r8 = java.lang.Integer.valueOf(r5)
            r7[r0] = r8
            int r8 = android.os.Process.myPid()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r9] = r8
            int r8 = android.os.Process.myTid()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r6] = r8
            java.lang.String r8 = "[UploadManager] Execute urgent upload tasks of queue which has %d tasks (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r7)
        L_0x00dc:
            r7 = 0
        L_0x00dd:
            if (r7 >= r5) goto L_0x012b
            java.lang.Object r8 = r2.poll()
            java.lang.Runnable r8 = (java.lang.Runnable) r8
            if (r8 == 0) goto L_0x012b
            java.lang.Object r10 = r12.f11000j
            monitor-enter(r10)
            int r11 = r12.f11011u     // Catch:{ all -> 0x0128 }
            if (r11 < r6) goto L_0x00f5
            if (r1 == 0) goto L_0x00f5
            r1.mo26934a(r8)     // Catch:{ all -> 0x0128 }
            monitor-exit(r10)     // Catch:{ all -> 0x0128 }
            goto L_0x0125
        L_0x00f5:
            monitor-exit(r10)
            java.lang.Object[] r10 = new java.lang.Object[r9]
            java.lang.String r11 = "BUGLY_ASYNC_UPLOAD"
            r10[r0] = r11
            java.lang.String r11 = "[UploadManager] Create and start a new thread to execute a upload task: %s"
            com.tencent.bugly.proguard.C4888x.m16977a(r11, r10)
            com.tencent.bugly.proguard.u$a r10 = new com.tencent.bugly.proguard.u$a
            r10.<init>(r8)
            java.lang.String r11 = "BUGLY_ASYNC_UPLOAD"
            java.lang.Thread r10 = com.tencent.bugly.proguard.C4893z.m17016a(r10, r11)
            if (r10 == 0) goto L_0x011b
            java.lang.Object r8 = r12.f11000j
            monitor-enter(r8)
            int r10 = r12.f11011u     // Catch:{ all -> 0x0118 }
            int r10 = r10 + r9
            r12.f11011u = r10     // Catch:{ all -> 0x0118 }
            monitor-exit(r8)     // Catch:{ all -> 0x0118 }
            goto L_0x0125
        L_0x0118:
            r13 = move-exception
            monitor-exit(r8)
            throw r13
        L_0x011b:
            java.lang.Object[] r10 = new java.lang.Object[r0]
            java.lang.String r11 = "[UploadManager] Failed to start a thread to execute asynchronous upload task, will try again next time."
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r10)
            r12.m16940a(r8, r9)
        L_0x0125:
            int r7 = r7 + 1
            goto L_0x00dd
        L_0x0128:
            r13 = move-exception
            monitor-exit(r10)
            throw r13
        L_0x012b:
            if (r13 <= 0) goto L_0x014e
            java.lang.Object[] r2 = new java.lang.Object[r4]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            r2[r0] = r4
            int r0 = android.os.Process.myPid()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r9] = r0
            int r0 = android.os.Process.myTid()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r6] = r0
            java.lang.String r0 = "[UploadManager] Execute upload tasks of queue which has %d tasks (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r2)
        L_0x014e:
            if (r1 == 0) goto L_0x0158
            com.tencent.bugly.proguard.u$b r0 = new com.tencent.bugly.proguard.u$b
            r0.<init>(r12, r13, r3)
            r1.mo26934a(r0)
        L_0x0158:
            return
        L_0x0159:
            r13 = move-exception
            monitor-exit(r4)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4881u.m16943c(int):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final boolean mo26925b() {
        if (this.f11006p == null || this.f11007q == 0) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis() + this.f11003m;
        long j = this.f11007q;
        if (j >= currentTimeMillis) {
            return true;
        }
        C4888x.m16982c("[UploadManager] Session ID expired time from server is: %d(%s), but now is: %d(%s)", Long.valueOf(j), new Date(this.f11007q).toString(), Long.valueOf(currentTimeMillis), new Date(currentTimeMillis).toString());
        return false;
    }

    /* renamed from: a */
    public static synchronized C4881u m16932a() {
        C4881u uVar;
        synchronized (C4881u.class) {
            uVar = f10991b;
        }
        return uVar;
    }

    /* renamed from: a */
    public final void mo26918a(int i, C4852ap apVar, String str, String str2, C4880t tVar, long j, boolean z) {
        try {
            m16938a(new C4885v(this.f10994d, i, apVar.f10849g, C4836a.m16730a((Object) apVar), str, str2, tVar, this.f10992a, z), true, true, j);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public final void mo26916a(int i, int i2, byte[] bArr, String str, String str2, C4880t tVar, int i3, int i4, boolean z, Map<String, String> map) {
        try {
            m16938a(new C4885v(this.f10994d, i, i2, bArr, str, str2, tVar, this.f10992a, i3, i4, false, map), z, false, 0);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo26924b(boolean z) {
        synchronized (this.f11008r) {
            C4888x.m16982c("[UploadManager] Clear security context (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            this.f11004n = null;
            this.f11006p = null;
            this.f11007q = 0;
        }
        if (z) {
            m16944c();
        }
    }

    /* renamed from: a */
    public final void mo26919a(int i, C4852ap apVar, String str, String str2, C4880t tVar, boolean z) {
        mo26916a(i, apVar.f10849g, C4836a.m16730a((Object) apVar), str, str2, tVar, 0, 0, z, null);
    }

    /* renamed from: a */
    public final long mo26915a(boolean z) {
        long j;
        long b = C4893z.m17032b();
        int i = z ? 5 : 3;
        List<C4878r> a = this.f10993c.mo26899a(i);
        if (a == null || a.size() <= 0) {
            j = z ? this.f10997g : this.f10996f;
        } else {
            j = 0;
            try {
                C4878r rVar = a.get(0);
                if (rVar.f10985e >= b) {
                    j = C4893z.m17045c(rVar.f10987g);
                    if (i == 3) {
                        this.f10996f = j;
                    } else {
                        this.f10997g = j;
                    }
                    a.remove(rVar);
                }
            } catch (Throwable th) {
                C4888x.m16978a(th);
            }
            if (a.size() > 0) {
                this.f10993c.mo26901a(a);
            }
        }
        C4888x.m16982c("[UploadManager] Local network consume: %d KB", Long.valueOf(j / ConstantsAPI.AppSupportContentFlag.MMAPP_SUPPORT_XLS));
        return j;
    }

    /* renamed from: b */
    public final byte[] mo26927b(byte[] bArr) {
        byte[] bArr2 = this.f11004n;
        if (bArr2 != null && (bArr2.length << 3) == 128) {
            return C4893z.m17026a(2, bArr, bArr2);
        }
        C4888x.m16983d("[UploadManager] AES key is invalid (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final synchronized void mo26921a(long j, boolean z) {
        int i = z ? 5 : 3;
        C4878r rVar = new C4878r();
        rVar.f10982b = i;
        rVar.f10985e = C4893z.m17032b();
        rVar.f10983c = "";
        rVar.f10984d = "";
        rVar.f10987g = C4893z.m17048c(j);
        this.f10993c.mo26905b(i);
        this.f10993c.mo26904a(rVar);
        if (z) {
            this.f10997g = j;
        } else {
            this.f10996f = j;
        }
        C4888x.m16982c("[UploadManager] Network total consume: %d KB", Long.valueOf(j / ConstantsAPI.AppSupportContentFlag.MMAPP_SUPPORT_XLS));
    }

    /* renamed from: a */
    public final synchronized void mo26917a(int i, long j) {
        if (i >= 0) {
            this.f10995e.put(Integer.valueOf(i), Long.valueOf(j));
            C4878r rVar = new C4878r();
            rVar.f10982b = i;
            rVar.f10985e = j;
            rVar.f10983c = "";
            rVar.f10984d = "";
            rVar.f10987g = new byte[0];
            this.f10993c.mo26905b(i);
            this.f10993c.mo26904a(rVar);
            C4888x.m16982c("[UploadManager] Uploading(ID:%d) time: %s", Integer.valueOf(i), C4893z.m17009a(j));
            return;
        }
        C4888x.m16984e("[UploadManager] Unknown uploading ID: %d", Integer.valueOf(i));
    }

    /* renamed from: a */
    public final synchronized long mo26914a(int i) {
        long j;
        j = 0;
        if (i >= 0) {
            Long l = this.f10995e.get(Integer.valueOf(i));
            if (l != null) {
                return l.longValue();
            }
            List<C4878r> a = this.f10993c.mo26899a(i);
            if (a != null && a.size() > 0) {
                if (a.size() > 1) {
                    for (C4878r rVar : a) {
                        if (rVar.f10985e > j) {
                            j = rVar.f10985e;
                        }
                    }
                    this.f10993c.mo26905b(i);
                } else {
                    try {
                        j = a.get(0).f10985e;
                    } catch (Throwable th) {
                        C4888x.m16978a(th);
                    }
                }
            }
        } else {
            C4888x.m16984e("[UploadManager] Unknown upload ID: %d", Integer.valueOf(i));
        }
        return j;
    }

    /* renamed from: a */
    private boolean m16940a(Runnable runnable, boolean z) {
        if (runnable == null) {
            C4888x.m16977a("[UploadManager] Upload task should not be null", new Object[0]);
            return false;
        }
        try {
            C4888x.m16982c("[UploadManager] Add upload task to queue (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            synchronized (this.f11000j) {
                if (z) {
                    this.f10998h.put(runnable);
                } else {
                    this.f10999i.put(runnable);
                }
            }
            return true;
        } catch (Throwable th) {
            C4888x.m16984e("[UploadManager] Failed to add upload task to queue: %s", th.getMessage());
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.u.a(java.lang.Runnable, boolean):boolean
     arg types: [java.lang.Runnable, int]
     candidates:
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, int):void
      com.tencent.bugly.proguard.u.a(java.lang.Runnable, long):void
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, boolean):boolean
      com.tencent.bugly.proguard.u.a(com.tencent.bugly.proguard.u, byte[]):byte[]
      com.tencent.bugly.proguard.u.a(int, long):void
      com.tencent.bugly.proguard.u.a(int, com.tencent.bugly.proguard.aq):void
      com.tencent.bugly.proguard.u.a(long, boolean):void
      com.tencent.bugly.proguard.u.a(java.lang.Runnable, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16937a(Runnable runnable, long j) {
        if (runnable == null) {
            C4888x.m16983d("[UploadManager] Upload task should not be null", new Object[0]);
            return;
        }
        C4888x.m16982c("[UploadManager] Execute synchronized upload task (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        Thread a = C4893z.m17016a(runnable, "BUGLY_SYNC_UPLOAD");
        if (a == null) {
            C4888x.m16984e("[UploadManager] Failed to start a thread to execute synchronized upload task, add it to queue.", new Object[0]);
            m16940a(runnable, true);
            return;
        }
        try {
            a.join(j);
        } catch (Throwable th) {
            C4888x.m16984e("[UploadManager] Failed to join upload synchronized task with message: %s. Add it to queue.", th.getMessage());
            m16940a(runnable, true);
            m16943c(0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008c, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Initialize security context now (pid=%d | tid=%d)", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a7, code lost:
        if (r14 == false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a9, code lost:
        m16937a(new com.tencent.bugly.proguard.C4881u.C4884c(r2, r7.f10994d, r12, r15), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b9, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ba, code lost:
        m16940a(r12, r13);
        r0 = new com.tencent.bugly.proguard.C4881u.C4884c(r11, r7.f10994d);
        com.tencent.bugly.proguard.C4888x.m16977a("[UploadManager] Create and start a new thread to execute a task of initializing security context: %s", "BUGLY_ASYNC_UPLOAD");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d5, code lost:
        if (com.tencent.bugly.proguard.C4893z.m17016a(r0, "BUGLY_ASYNC_UPLOAD") != null) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d7, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("[UploadManager] Failed to start a thread to execute task of initializing security context, try to post it into thread pool.", new java.lang.Object[0]);
        r2 = com.tencent.bugly.proguard.C4886w.m16969a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00e2, code lost:
        if (r2 == null) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e4, code lost:
        r2.mo26934a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e7, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e8, code lost:
        com.tencent.bugly.proguard.C4888x.m16984e("[UploadManager] Asynchronous thread pool is unavailable now, try next time.", new java.lang.Object[0]);
        r2 = r7.f11010t;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f1, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r7.f11009s = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f4, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f5, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f9, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m16938a(java.lang.Runnable r12, boolean r13, boolean r14, long r15) {
        /*
            r11 = this;
            r7 = r11
            r0 = r12
            r1 = 0
            if (r0 != 0) goto L_0x000c
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.String r3 = "[UploadManager] Upload task should not be null"
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r2)
        L_0x000c:
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            int r4 = android.os.Process.myPid()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3[r1] = r4
            int r4 = android.os.Process.myTid()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r5 = 1
            r3[r5] = r4
            java.lang.String r4 = "[UploadManager] Add upload task (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r4, r3)
            java.lang.String r3 = r7.f11006p
            if (r3 == 0) goto L_0x007c
            boolean r3 = r11.mo26925b()
            if (r3 == 0) goto L_0x005c
            java.lang.Object[] r2 = new java.lang.Object[r2]
            int r3 = android.os.Process.myPid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r1] = r3
            int r3 = android.os.Process.myTid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r5] = r3
            java.lang.String r3 = "[UploadManager] Sucessfully got session ID, try to execute upload task now (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r2)
            if (r14 == 0) goto L_0x0055
            r8 = r15
            r11.m16937a(r12, r8)
            return
        L_0x0055:
            r11.m16940a(r12, r13)
            r11.m16943c(r1)
            return
        L_0x005c:
            r8 = r15
            java.lang.Object[] r3 = new java.lang.Object[r2]
            int r4 = android.os.Process.myPid()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3[r1] = r4
            int r4 = android.os.Process.myTid()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3[r5] = r4
            java.lang.String r4 = "[UploadManager] Session ID is expired, drop it (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16977a(r4, r3)
            r11.mo26924b(r1)
            goto L_0x007d
        L_0x007c:
            r8 = r15
        L_0x007d:
            java.lang.Object r3 = r7.f11010t
            monitor-enter(r3)
            boolean r4 = r7.f11009s     // Catch:{ all -> 0x00fa }
            if (r4 == 0) goto L_0x0089
            r11.m16940a(r12, r13)     // Catch:{ all -> 0x00fa }
            monitor-exit(r3)     // Catch:{ all -> 0x00fa }
            return
        L_0x0089:
            r7.f11009s = r5     // Catch:{ all -> 0x00fa }
            monitor-exit(r3)     // Catch:{ all -> 0x00fa }
            java.lang.Object[] r2 = new java.lang.Object[r2]
            int r3 = android.os.Process.myPid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r1] = r3
            int r3 = android.os.Process.myTid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r5] = r3
            java.lang.String r3 = "[UploadManager] Initialize security context now (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r2)
            if (r14 == 0) goto L_0x00ba
            com.tencent.bugly.proguard.u$c r10 = new com.tencent.bugly.proguard.u$c
            android.content.Context r3 = r7.f10994d
            r1 = r10
            r2 = r11
            r4 = r12
            r5 = r15
            r1.<init>(r3, r4, r5)
            r0 = 0
            r11.m16937a(r10, r0)
            return
        L_0x00ba:
            r11.m16940a(r12, r13)
            com.tencent.bugly.proguard.u$c r0 = new com.tencent.bugly.proguard.u$c
            android.content.Context r2 = r7.f10994d
            r0.<init>(r2)
            java.lang.Object[] r2 = new java.lang.Object[r5]
            java.lang.String r3 = "BUGLY_ASYNC_UPLOAD"
            r2[r1] = r3
            java.lang.String r3 = "[UploadManager] Create and start a new thread to execute a task of initializing security context: %s"
            com.tencent.bugly.proguard.C4888x.m16977a(r3, r2)
            java.lang.String r2 = "BUGLY_ASYNC_UPLOAD"
            java.lang.Thread r2 = com.tencent.bugly.proguard.C4893z.m17016a(r0, r2)
            if (r2 != 0) goto L_0x00f9
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.String r3 = "[UploadManager] Failed to start a thread to execute task of initializing security context, try to post it into thread pool."
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r2)
            com.tencent.bugly.proguard.w r2 = com.tencent.bugly.proguard.C4886w.m16969a()
            if (r2 == 0) goto L_0x00e8
            r2.mo26934a(r0)
            return
        L_0x00e8:
            java.lang.Object[] r0 = new java.lang.Object[r1]
            java.lang.String r2 = "[UploadManager] Asynchronous thread pool is unavailable now, try next time."
            com.tencent.bugly.proguard.C4888x.m16984e(r2, r0)
            java.lang.Object r2 = r7.f11010t
            monitor-enter(r2)
            r7.f11009s = r1     // Catch:{ all -> 0x00f6 }
            monitor-exit(r2)     // Catch:{ all -> 0x00f6 }
            return
        L_0x00f6:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x00f9:
            return
        L_0x00fa:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4881u.m16938a(java.lang.Runnable, boolean, boolean, long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r9 == null) goto L_0x0107;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Record security context (pid=%d | tid=%d)", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r8 = r9.f10875g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        if (r8 == null) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        if (r8.containsKey("S1") == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0063, code lost:
        if (r8.containsKey("S2") == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        r7.f11003m = r9.f10873e - java.lang.System.currentTimeMillis();
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Time lag of server is: %d", java.lang.Long.valueOf(r7.f11003m));
        r7.f11006p = r8.get("S1");
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Session ID from server is: %s", r7.f11006p);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0098, code lost:
        if (r7.f11006p.length() <= 0) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r7.f11007q = java.lang.Long.parseLong(r8.get("S2"));
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Session expired time from server is: %d(%s)", java.lang.Long.valueOf(r7.f11007q), new java.util.Date(r7.f11007q).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00cd, code lost:
        if (r7.f11007q >= 1000) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00cf, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("[UploadManager] Session expired time from server is less than 1 second, will set to default value", new java.lang.Object[0]);
        r7.f11007q = 259200000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("[UploadManager] Session expired time is invalid, will set to default value", new java.lang.Object[0]);
        r7.f11007q = 259200000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f5, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Session ID from server is invalid, try next time", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fd, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fe, code lost:
        com.tencent.bugly.proguard.C4888x.m16978a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0107, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("[UploadManager] Fail to init security context and clear local info (pid=%d | tid=%d)", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
        mo26924b(false);
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0128 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo26920a(int r8, com.tencent.bugly.proguard.C4853aq r9) {
        /*
            r7 = this;
            boolean r0 = r7.f10992a
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 2
            r1 = 1
            r2 = 0
            if (r8 != r0) goto L_0x002a
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r9 = android.os.Process.myPid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r2] = r9
            int r9 = android.os.Process.myTid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r1] = r9
            java.lang.String r9 = "[UploadManager] Session ID is invalid, will clear security context (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r9, r8)
            r7.mo26924b(r1)
            goto L_0x0125
        L_0x002a:
            java.lang.Object r8 = r7.f11010t
            monitor-enter(r8)
            boolean r3 = r7.f11009s     // Catch:{ all -> 0x013a }
            if (r3 != 0) goto L_0x0033
            monitor-exit(r8)     // Catch:{ all -> 0x013a }
            return
        L_0x0033:
            monitor-exit(r8)
            if (r9 == 0) goto L_0x0107
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r3 = android.os.Process.myPid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r8[r2] = r3
            int r3 = android.os.Process.myTid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r8[r1] = r3
            java.lang.String r3 = "[UploadManager] Record security context (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r8)
            java.util.Map<java.lang.String, java.lang.String> r8 = r9.f10875g     // Catch:{ all -> 0x00fd }
            if (r8 == 0) goto L_0x0101
            java.lang.String r3 = "S1"
            boolean r3 = r8.containsKey(r3)     // Catch:{ all -> 0x00fd }
            if (r3 == 0) goto L_0x0101
            java.lang.String r3 = "S2"
            boolean r3 = r8.containsKey(r3)     // Catch:{ all -> 0x00fd }
            if (r3 == 0) goto L_0x0101
            long r3 = r9.f10873e     // Catch:{ all -> 0x00fd }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fd }
            long r3 = r3 - r5
            r7.f11003m = r3     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "[UploadManager] Time lag of server is: %d"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00fd }
            long r4 = r7.f11003m     // Catch:{ all -> 0x00fd }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x00fd }
            r3[r2] = r4     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C4888x.m16982c(r9, r3)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "S1"
            java.lang.Object r9 = r8.get(r9)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x00fd }
            r7.f11006p = r9     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "[UploadManager] Session ID from server is: %s"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00fd }
            java.lang.String r4 = r7.f11006p     // Catch:{ all -> 0x00fd }
            r3[r2] = r4     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C4888x.m16982c(r9, r3)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = r7.f11006p     // Catch:{ all -> 0x00fd }
            int r9 = r9.length()     // Catch:{ all -> 0x00fd }
            if (r9 <= 0) goto L_0x00f5
            r3 = 259200000(0xf731400, double:1.280618154E-315)
            java.lang.String r9 = "S2"
            java.lang.Object r8 = r8.get(r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ NumberFormatException -> 0x00d9 }
            long r8 = java.lang.Long.parseLong(r8)     // Catch:{ NumberFormatException -> 0x00d9 }
            r7.f11007q = r8     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r8 = "[UploadManager] Session expired time from server is: %d(%s)"
            java.lang.Object[] r9 = new java.lang.Object[r0]     // Catch:{ NumberFormatException -> 0x00d9 }
            long r5 = r7.f11007q     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.Long r0 = java.lang.Long.valueOf(r5)     // Catch:{ NumberFormatException -> 0x00d9 }
            r9[r2] = r0     // Catch:{ NumberFormatException -> 0x00d9 }
            java.util.Date r0 = new java.util.Date     // Catch:{ NumberFormatException -> 0x00d9 }
            long r5 = r7.f11007q     // Catch:{ NumberFormatException -> 0x00d9 }
            r0.<init>(r5)     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r0 = r0.toString()     // Catch:{ NumberFormatException -> 0x00d9 }
            r9[r1] = r0     // Catch:{ NumberFormatException -> 0x00d9 }
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            long r8 = r7.f11007q     // Catch:{ NumberFormatException -> 0x00d9 }
            r5 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x00e2
            java.lang.String r8 = "[UploadManager] Session expired time from server is less than 1 second, will set to default value"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ NumberFormatException -> 0x00d9 }
            com.tencent.bugly.proguard.C4888x.m16983d(r8, r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            r7.f11007q = r3     // Catch:{ NumberFormatException -> 0x00d9 }
            goto L_0x00e2
        L_0x00d9:
            java.lang.String r8 = "[UploadManager] Session expired time is invalid, will set to default value"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C4888x.m16983d(r8, r9)     // Catch:{ all -> 0x00fd }
            r7.f11007q = r3     // Catch:{ all -> 0x00fd }
        L_0x00e2:
            boolean r8 = r7.m16947d()     // Catch:{ all -> 0x00fd }
            if (r8 == 0) goto L_0x00ea
            r1 = 0
            goto L_0x00f1
        L_0x00ea:
            java.lang.String r8 = "[UploadManager] Failed to record database"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r9)     // Catch:{ all -> 0x00fd }
        L_0x00f1:
            r7.m16943c(r2)     // Catch:{ all -> 0x00fd }
            goto L_0x0101
        L_0x00f5:
            java.lang.String r8 = "[UploadManager] Session ID from server is invalid, try next time"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r9)     // Catch:{ all -> 0x00fd }
            goto L_0x0101
        L_0x00fd:
            r8 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r8)
        L_0x0101:
            if (r1 == 0) goto L_0x0125
            r7.mo26924b(r2)
            goto L_0x0125
        L_0x0107:
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r9 = android.os.Process.myPid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r2] = r9
            int r9 = android.os.Process.myTid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r1] = r9
            java.lang.String r9 = "[UploadManager] Fail to init security context and clear local info (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r9, r8)
            r7.mo26924b(r2)
        L_0x0125:
            java.lang.Object r8 = r7.f11010t
            monitor-enter(r8)
            boolean r9 = r7.f11009s     // Catch:{ all -> 0x0137 }
            if (r9 == 0) goto L_0x0135
            r7.f11009s = r2     // Catch:{ all -> 0x0137 }
            android.content.Context r9 = r7.f10994d     // Catch:{ all -> 0x0137 }
            java.lang.String r0 = "security_info"
            com.tencent.bugly.proguard.C4893z.m17040b(r9, r0)     // Catch:{ all -> 0x0137 }
        L_0x0135:
            monitor-exit(r8)     // Catch:{ all -> 0x0137 }
            return
        L_0x0137:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        L_0x013a:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4881u.mo26920a(int, com.tencent.bugly.proguard.aq):void");
    }

    /* renamed from: a */
    public final byte[] mo26923a(byte[] bArr) {
        byte[] bArr2 = this.f11004n;
        if (bArr2 != null && (bArr2.length << 3) == 128) {
            return C4893z.m17026a(1, bArr, bArr2);
        }
        C4888x.m16983d("[UploadManager] AES key is invalid (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        return null;
    }

    /* renamed from: a */
    public final boolean mo26922a(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        C4888x.m16982c("[UploadManager] Integrate security to HTTP headers (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        String str = this.f11006p;
        if (str != null) {
            map.put("secureSessionId", str);
            return true;
        }
        byte[] bArr = this.f11004n;
        if (bArr == null || (bArr.length << 3) != 128) {
            C4888x.m16983d("[UploadManager] AES key is invalid", new Object[0]);
            return false;
        }
        if (this.f11002l == null) {
            this.f11002l = Base64.decode(this.f11001k, 0);
            if (this.f11002l == null) {
                C4888x.m16983d("[UploadManager] Failed to decode RSA public key", new Object[0]);
                return false;
            }
        }
        byte[] b = C4893z.m17041b(1, this.f11004n, this.f11002l);
        if (b == null) {
            C4888x.m16983d("[UploadManager] Failed to encrypt AES key", new Object[0]);
            return false;
        }
        String encodeToString = Base64.encodeToString(b, 0);
        if (encodeToString == null) {
            C4888x.m16983d("[UploadManager] Failed to encode AES key", new Object[0]);
            return false;
        }
        map.put("raKey", encodeToString);
        return true;
    }
}
