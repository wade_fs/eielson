package com.tencent.bugly.crashreport.biz;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;

/* compiled from: BUGLY */
public class UserInfoBean implements Parcelable {
    public static final Parcelable.Creator<UserInfoBean> CREATOR = new C4795a();

    /* renamed from: a */
    public long f10420a;

    /* renamed from: b */
    public int f10421b;

    /* renamed from: c */
    public String f10422c;

    /* renamed from: d */
    public String f10423d;

    /* renamed from: e */
    public long f10424e;

    /* renamed from: f */
    public long f10425f;

    /* renamed from: g */
    public long f10426g;

    /* renamed from: h */
    public long f10427h;

    /* renamed from: i */
    public long f10428i;

    /* renamed from: j */
    public String f10429j;

    /* renamed from: k */
    public long f10430k;

    /* renamed from: l */
    public boolean f10431l;

    /* renamed from: m */
    public String f10432m;

    /* renamed from: n */
    public String f10433n;

    /* renamed from: o */
    public int f10434o;

    /* renamed from: p */
    public int f10435p;

    /* renamed from: q */
    public int f10436q;

    /* renamed from: r */
    public Map<String, String> f10437r;

    /* renamed from: s */
    public Map<String, String> f10438s;

    /* renamed from: com.tencent.bugly.crashreport.biz.UserInfoBean$a */
    /* compiled from: BUGLY */
    static class C4795a implements Parcelable.Creator<UserInfoBean> {
        C4795a() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new UserInfoBean(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new UserInfoBean[i];
        }
    }

    public UserInfoBean() {
        this.f10430k = 0;
        this.f10431l = false;
        this.f10432m = "unknown";
        this.f10435p = -1;
        this.f10436q = -1;
        this.f10437r = null;
        this.f10438s = null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f10421b);
        parcel.writeString(this.f10422c);
        parcel.writeString(this.f10423d);
        parcel.writeLong(this.f10424e);
        parcel.writeLong(this.f10425f);
        parcel.writeLong(this.f10426g);
        parcel.writeLong(this.f10427h);
        parcel.writeLong(this.f10428i);
        parcel.writeString(this.f10429j);
        parcel.writeLong(this.f10430k);
        parcel.writeByte(this.f10431l ? (byte) 1 : 0);
        parcel.writeString(this.f10432m);
        parcel.writeInt(this.f10435p);
        parcel.writeInt(this.f10436q);
        C4893z.m17038b(parcel, this.f10437r);
        C4893z.m17038b(parcel, this.f10438s);
        parcel.writeString(this.f10433n);
        parcel.writeInt(this.f10434o);
    }

    public UserInfoBean(Parcel parcel) {
        this.f10430k = 0;
        boolean z = false;
        this.f10431l = false;
        this.f10432m = "unknown";
        this.f10435p = -1;
        this.f10436q = -1;
        this.f10437r = null;
        this.f10438s = null;
        this.f10421b = parcel.readInt();
        this.f10422c = parcel.readString();
        this.f10423d = parcel.readString();
        this.f10424e = parcel.readLong();
        this.f10425f = parcel.readLong();
        this.f10426g = parcel.readLong();
        this.f10427h = parcel.readLong();
        this.f10428i = parcel.readLong();
        this.f10429j = parcel.readString();
        this.f10430k = parcel.readLong();
        this.f10431l = parcel.readByte() == 1 ? true : z;
        this.f10432m = parcel.readString();
        this.f10435p = parcel.readInt();
        this.f10436q = parcel.readInt();
        this.f10437r = C4893z.m17036b(parcel);
        this.f10438s = C4893z.m17036b(parcel);
        this.f10433n = parcel.readString();
        this.f10434o = parcel.readInt();
    }
}
