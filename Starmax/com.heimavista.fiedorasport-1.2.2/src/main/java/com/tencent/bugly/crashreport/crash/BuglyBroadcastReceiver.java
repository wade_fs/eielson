package com.tencent.bugly.crashreport.crash;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;

/* compiled from: BUGLY */
public class BuglyBroadcastReceiver extends BroadcastReceiver {
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static BuglyBroadcastReceiver f10588d;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public IntentFilter f10589a = new IntentFilter();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Context f10590b;

    /* renamed from: c */
    private String f10591c;

    /* renamed from: e */
    private boolean f10592e = true;

    /* renamed from: com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver$a */
    /* compiled from: BUGLY */
    class C4811a implements Runnable {

        /* renamed from: P */
        private /* synthetic */ BuglyBroadcastReceiver f10593P;

        C4811a(BuglyBroadcastReceiver buglyBroadcastReceiver) {
            this.f10593P = buglyBroadcastReceiver;
        }

        public final void run() {
            try {
                C4888x.m16976a(BuglyBroadcastReceiver.f10588d.getClass(), "Register broadcast receiver of Bugly.", new Object[0]);
                synchronized (this.f10593P) {
                    BuglyBroadcastReceiver.this.f10590b.registerReceiver(BuglyBroadcastReceiver.f10588d, BuglyBroadcastReceiver.this.f10589a);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static synchronized BuglyBroadcastReceiver getInstance() {
        BuglyBroadcastReceiver buglyBroadcastReceiver;
        synchronized (BuglyBroadcastReceiver.class) {
            if (f10588d == null) {
                f10588d = new BuglyBroadcastReceiver();
            }
            buglyBroadcastReceiver = f10588d;
        }
        return buglyBroadcastReceiver;
    }

    public synchronized void addFilter(String str) {
        if (!this.f10589a.hasAction(str)) {
            this.f10589a.addAction(str);
        }
        C4888x.m16982c("add action %s", str);
    }

    public final void onReceive(Context context, Intent intent) {
        try {
            m16602a(context, intent);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    public synchronized void register(Context context) {
        this.f10590b = context;
        C4893z.m17023a(new C4811a(this));
    }

    public synchronized void unregister(Context context) {
        try {
            C4888x.m16976a(getClass(), "Unregister broadcast receiver of Bugly.", new Object[0]);
            context.unregisterReceiver(super);
            this.f10590b = context;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00bc, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ca, code lost:
        return false;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean m16602a(android.content.Context r12, android.content.Intent r13) {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 0
            if (r12 == 0) goto L_0x00c9
            if (r13 == 0) goto L_0x00c9
            java.lang.String r13 = r13.getAction()     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = "android.net.conn.CONNECTIVITY_CHANGE"
            boolean r13 = r13.equals(r1)     // Catch:{ all -> 0x00c6 }
            if (r13 != 0) goto L_0x0014
            goto L_0x00c9
        L_0x0014:
            boolean r13 = r11.f10592e     // Catch:{ all -> 0x00c6 }
            r1 = 1
            if (r13 == 0) goto L_0x001d
            r11.f10592e = r0     // Catch:{ all -> 0x00c6 }
            monitor-exit(r11)
            return r1
        L_0x001d:
            android.content.Context r13 = r11.f10590b     // Catch:{ all -> 0x00c6 }
            java.lang.String r13 = com.tencent.bugly.crashreport.common.info.C4807b.m16557c(r13)     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            java.lang.String r3 = "is Connect BC "
            r2.<init>(r3)     // Catch:{ all -> 0x00c6 }
            r2.append(r13)     // Catch:{ all -> 0x00c6 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c6 }
            java.lang.Object[] r3 = new java.lang.Object[r0]     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.C4888x.m16982c(r2, r3)     // Catch:{ all -> 0x00c6 }
            java.lang.String r2 = "network %s changed to %s"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            r4.<init>()     // Catch:{ all -> 0x00c6 }
            java.lang.String r5 = r11.f10591c     // Catch:{ all -> 0x00c6 }
            r4.append(r5)     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00c6 }
            r3[r0] = r4     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            r4.<init>()     // Catch:{ all -> 0x00c6 }
            r4.append(r13)     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00c6 }
            r3[r1] = r4     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.C4888x.m16977a(r2, r3)     // Catch:{ all -> 0x00c6 }
            if (r13 != 0) goto L_0x0063
            r12 = 0
            r11.f10591c = r12     // Catch:{ all -> 0x00c6 }
            monitor-exit(r11)
            return r1
        L_0x0063:
            java.lang.String r2 = r11.f10591c     // Catch:{ all -> 0x00c6 }
            r11.f10591c = r13     // Catch:{ all -> 0x00c6 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.crashreport.common.strategy.a r5 = com.tencent.bugly.crashreport.common.strategy.C4809a.m16589a()     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.u r6 = com.tencent.bugly.proguard.C4881u.m16932a()     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.crashreport.common.info.a r12 = com.tencent.bugly.crashreport.common.info.C4806a.m16491a(r12)     // Catch:{ all -> 0x00c6 }
            if (r5 == 0) goto L_0x00bd
            if (r6 == 0) goto L_0x00bd
            if (r12 != 0) goto L_0x007e
            goto L_0x00bd
        L_0x007e:
            boolean r12 = r13.equals(r2)     // Catch:{ all -> 0x00c6 }
            if (r12 != 0) goto L_0x00bb
            int r12 = com.tencent.bugly.crashreport.crash.C4824c.f10689a     // Catch:{ all -> 0x00c6 }
            long r12 = r6.mo26914a(r12)     // Catch:{ all -> 0x00c6 }
            long r12 = r3 - r12
            r7 = 30000(0x7530, double:1.4822E-319)
            int r2 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00a4
            java.lang.String r12 = "try to upload crash on network changed."
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.C4888x.m16977a(r12, r13)     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.crashreport.crash.c r12 = com.tencent.bugly.crashreport.crash.C4824c.m16656a()     // Catch:{ all -> 0x00c6 }
            if (r12 == 0) goto L_0x00a4
            r9 = 0
            r12.mo26754a(r9)     // Catch:{ all -> 0x00c6 }
        L_0x00a4:
            r12 = 1001(0x3e9, float:1.403E-42)
            long r12 = r6.mo26914a(r12)     // Catch:{ all -> 0x00c6 }
            long r3 = r3 - r12
            int r12 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r12 <= 0) goto L_0x00bb
            java.lang.String r12 = "try to upload userinfo on network changed."
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.C4888x.m16977a(r12, r13)     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.crashreport.biz.a r12 = com.tencent.bugly.crashreport.biz.C4802b.f10452a     // Catch:{ all -> 0x00c6 }
            r12.mo26636b()     // Catch:{ all -> 0x00c6 }
        L_0x00bb:
            monitor-exit(r11)
            return r1
        L_0x00bd:
            java.lang.String r12 = "not inited BC not work"
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00c6 }
            com.tencent.bugly.proguard.C4888x.m16983d(r12, r13)     // Catch:{ all -> 0x00c6 }
            monitor-exit(r11)
            return r1
        L_0x00c6:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        L_0x00c9:
            monitor-exit(r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver.m16602a(android.content.Context, android.content.Intent):boolean");
    }
}
