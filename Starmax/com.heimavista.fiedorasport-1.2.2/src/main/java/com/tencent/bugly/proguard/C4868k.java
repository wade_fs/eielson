package com.tencent.bugly.proguard;

import java.io.Serializable;

/* renamed from: com.tencent.bugly.proguard.k */
/* compiled from: BUGLY */
public abstract class C4868k implements Serializable {
    /* renamed from: a */
    public abstract void mo26839a(C4865i iVar);

    /* renamed from: a */
    public abstract void mo26840a(C4867j jVar);

    /* renamed from: a */
    public abstract void mo26841a(StringBuilder sb, int i);

    public String toString() {
        StringBuilder sb = new StringBuilder();
        mo26841a(sb, 0);
        return sb.toString();
    }
}
