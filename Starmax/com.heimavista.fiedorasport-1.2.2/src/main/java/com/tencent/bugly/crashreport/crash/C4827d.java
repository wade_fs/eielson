package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.crash.d */
/* compiled from: BUGLY */
public final class C4827d {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static C4827d f10721a;

    /* renamed from: b */
    private C4809a f10722b;

    /* renamed from: c */
    private C4806a f10723c;

    /* renamed from: d */
    private C4822b f10724d;

    /* renamed from: e */
    private Context f10725e;

    /* renamed from: com.tencent.bugly.crashreport.crash.d$a */
    /* compiled from: BUGLY */
    class C4828a implements Runnable {
        C4828a() {
        }

        public final void run() {
            C4827d.m16679a(C4827d.this);
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.crash.d$b */
    /* compiled from: BUGLY */
    static class C4829b implements Runnable {

        /* renamed from: P */
        private /* synthetic */ Thread f10727P;

        /* renamed from: Q */
        private /* synthetic */ int f10728Q;

        /* renamed from: R */
        private /* synthetic */ String f10729R;

        /* renamed from: S */
        private /* synthetic */ String f10730S;

        /* renamed from: T */
        private /* synthetic */ String f10731T;

        /* renamed from: U */
        private /* synthetic */ Map f10732U;

        C4829b(Thread thread, int i, String str, String str2, String str3, Map map) {
            this.f10727P = thread;
            this.f10728Q = i;
            this.f10729R = str;
            this.f10730S = str2;
            this.f10731T = str3;
            this.f10732U = map;
        }

        public final void run() {
            try {
                if (C4827d.f10721a == null) {
                    C4888x.m16984e("[ExtraCrashManager] Extra crash manager has not been initialized.", new Object[0]);
                } else {
                    C4827d.m16680a(C4827d.f10721a, this.f10727P, this.f10728Q, this.f10729R, this.f10730S, this.f10731T, this.f10732U);
                }
            } catch (Throwable th) {
                if (!C4888x.m16981b(th)) {
                    th.printStackTrace();
                }
                C4888x.m16984e("[ExtraCrashManager] Crash error %s %s %s", this.f10729R, this.f10730S, this.f10731T);
            }
        }
    }

    private C4827d(Context context) {
        C4824c a = C4824c.m16656a();
        if (a != null) {
            this.f10722b = C4809a.m16589a();
            this.f10723c = C4806a.m16491a(context);
            this.f10724d = a.f10705p;
            this.f10725e = context;
            C4886w.m16969a().mo26934a(new C4828a());
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m16679a(C4827d dVar) {
        C4888x.m16982c("[ExtraCrashManager] Trying to notify Bugly agents.", new Object[0]);
        try {
            Class<?> cls = Class.forName("com.tencent.bugly.agent.GameAgent");
            dVar.f10723c.getClass();
            C4893z.m17020a(cls, "sdkPackageName", "com.tencent.bugly", (Object) null);
            C4888x.m16982c("[ExtraCrashManager] Bugly game agent has been notified.", new Object[0]);
        } catch (Throwable unused) {
            C4888x.m16977a("[ExtraCrashManager] no game agent", new Object[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.z.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.z.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.z.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.z.a(byte[], int):byte[]
      com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.am
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.an
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x015a A[SYNTHETIC, Splitter:B:53:0x015a] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0165 A[Catch:{ all -> 0x0250, all -> 0x0260 }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01fa A[Catch:{ all -> 0x0250, all -> 0x0260 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0203 A[Catch:{ all -> 0x0250, all -> 0x0260 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0243 A[Catch:{ all -> 0x0250, all -> 0x0260 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void m16680a(com.tencent.bugly.crashreport.crash.C4827d r16, java.lang.Thread r17, int r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, java.util.Map r22) {
        /*
            r0 = r16
            r1 = r18
            r2 = r19
            r3 = r20
            r4 = r21
            r5 = r22
            java.lang.String r6 = "[ExtraCrashManager] Successfully handled."
            if (r17 != 0) goto L_0x0015
            java.lang.Thread r7 = java.lang.Thread.currentThread()
            goto L_0x0017
        L_0x0015:
            r7 = r17
        L_0x0017:
            r8 = 6
            r9 = 4
            r10 = 8
            r11 = 5
            r12 = 1
            r13 = 0
            if (r1 == r9) goto L_0x003a
            if (r1 == r11) goto L_0x0037
            if (r1 == r8) goto L_0x0037
            if (r1 == r10) goto L_0x0034
            java.lang.Object[] r0 = new java.lang.Object[r12]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r18)
            r0[r13] = r1
            java.lang.String r1 = "[ExtraCrashManager] Unknown extra crash type: %d"
            com.tencent.bugly.proguard.C4888x.m16983d(r1, r0)
            return
        L_0x0034:
            java.lang.String r14 = "H5"
            goto L_0x003c
        L_0x0037:
            java.lang.String r14 = "Cocos"
            goto L_0x003c
        L_0x003a:
            java.lang.String r14 = "Unity"
        L_0x003c:
            java.lang.Object[] r15 = new java.lang.Object[r12]
            r15[r13] = r14
            java.lang.String r12 = "[ExtraCrashManager] %s Crash Happen"
            com.tencent.bugly.proguard.C4888x.m16984e(r12, r15)
            com.tencent.bugly.crashreport.common.strategy.a r12 = r0.f10722b     // Catch:{ all -> 0x0250 }
            boolean r12 = r12.mo26719b()     // Catch:{ all -> 0x0250 }
            if (r12 != 0) goto L_0x0054
            java.lang.String r12 = "[ExtraCrashManager] There is no remote strategy, but still store it."
            java.lang.Object[] r15 = new java.lang.Object[r13]     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.proguard.C4888x.m16983d(r12, r15)     // Catch:{ all -> 0x0250 }
        L_0x0054:
            com.tencent.bugly.crashreport.common.strategy.a r12 = r0.f10722b     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r12 = r12.mo26720c()     // Catch:{ all -> 0x0250 }
            boolean r15 = r12.f10560g     // Catch:{ all -> 0x0250 }
            java.lang.String r10 = "\n"
            if (r15 != 0) goto L_0x00a9
            com.tencent.bugly.crashreport.common.strategy.a r15 = r0.f10722b     // Catch:{ all -> 0x0250 }
            boolean r15 = r15.mo26719b()     // Catch:{ all -> 0x0250 }
            if (r15 == 0) goto L_0x00a9
            java.lang.String r1 = "[ExtraCrashManager] Crash report was closed by remote , will not upload to Bugly , print local for helpful!"
            java.lang.Object[] r5 = new java.lang.Object[r13]     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.proguard.C4888x.m16984e(r1, r5)     // Catch:{ all -> 0x0250 }
            java.lang.String r1 = com.tencent.bugly.proguard.C4893z.m17008a()     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r0 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r0 = r0.f10526d     // Catch:{ all -> 0x0250 }
            java.lang.String r5 = r7.getName()     // Catch:{ all -> 0x0250 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0250 }
            r7.<init>()     // Catch:{ all -> 0x0250 }
            r7.append(r2)     // Catch:{ all -> 0x0250 }
            r7.append(r10)     // Catch:{ all -> 0x0250 }
            r7.append(r3)     // Catch:{ all -> 0x0250 }
            r7.append(r10)     // Catch:{ all -> 0x0250 }
            r7.append(r4)     // Catch:{ all -> 0x0250 }
            java.lang.String r2 = r7.toString()     // Catch:{ all -> 0x0250 }
            r3 = 0
            r16 = r14
            r17 = r1
            r18 = r0
            r19 = r5
            r20 = r2
            r21 = r3
            com.tencent.bugly.crashreport.crash.C4822b.m16639a(r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x0250 }
            java.lang.Object[] r0 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r0)
            return
        L_0x00a9:
            if (r1 == r9) goto L_0x00da
            java.lang.String r9 = "[ExtraCrashManager] %s report is disabled."
            if (r1 == r11) goto L_0x00c8
            if (r1 == r8) goto L_0x00c8
            r8 = 8
            if (r1 == r8) goto L_0x00b6
            goto L_0x00dc
        L_0x00b6:
            boolean r8 = r12.f10566m     // Catch:{ all -> 0x0250 }
            if (r8 != 0) goto L_0x00da
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0250 }
            r0[r13] = r14     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.proguard.C4888x.m16984e(r9, r0)     // Catch:{ all -> 0x0250 }
            java.lang.Object[] r0 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r0)
            return
        L_0x00c8:
            boolean r8 = r12.f10565l     // Catch:{ all -> 0x0250 }
            if (r8 != 0) goto L_0x00da
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0250 }
            r0[r13] = r14     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.proguard.C4888x.m16984e(r9, r0)     // Catch:{ all -> 0x0250 }
            java.lang.Object[] r0 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r0)
            return
        L_0x00da:
            r8 = 8
        L_0x00dc:
            if (r1 != r8) goto L_0x00df
            goto L_0x00e0
        L_0x00df:
            r11 = r1
        L_0x00e0:
            com.tencent.bugly.crashreport.crash.CrashDetailBean r1 = new com.tencent.bugly.crashreport.crash.CrashDetailBean     // Catch:{ all -> 0x0250 }
            r1.<init>()     // Catch:{ all -> 0x0250 }
            long r8 = com.tencent.bugly.crashreport.common.info.C4807b.m16572k()     // Catch:{ all -> 0x0250 }
            r1.f10597C = r8     // Catch:{ all -> 0x0250 }
            long r8 = com.tencent.bugly.crashreport.common.info.C4807b.m16568i()     // Catch:{ all -> 0x0250 }
            r1.f10598D = r8     // Catch:{ all -> 0x0250 }
            long r8 = com.tencent.bugly.crashreport.common.info.C4807b.m16576m()     // Catch:{ all -> 0x0250 }
            r1.f10599E = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            long r8 = r8.mo26701p()     // Catch:{ all -> 0x0250 }
            r1.f10600F = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            long r8 = r8.mo26700o()     // Catch:{ all -> 0x0250 }
            r1.f10601G = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            long r8 = r8.mo26702q()     // Catch:{ all -> 0x0250 }
            r1.f10602H = r8     // Catch:{ all -> 0x0250 }
            android.content.Context r8 = r0.f10725e     // Catch:{ all -> 0x0250 }
            int r9 = com.tencent.bugly.crashreport.crash.C4824c.f10693e     // Catch:{ all -> 0x0250 }
            r12 = 0
            java.lang.String r8 = com.tencent.bugly.proguard.C4893z.m17010a(r8, r9, r12)     // Catch:{ all -> 0x0250 }
            r1.f10640w = r8     // Catch:{ all -> 0x0250 }
            r1.f10619b = r11     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.mo26693h()     // Catch:{ all -> 0x0250 }
            r1.f10622e = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.f10532j     // Catch:{ all -> 0x0250 }
            r1.f10623f = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.mo26708w()     // Catch:{ all -> 0x0250 }
            r1.f10624g = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.mo26691g()     // Catch:{ all -> 0x0250 }
            r1.f10630m = r8     // Catch:{ all -> 0x0250 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0250 }
            r8.<init>()     // Catch:{ all -> 0x0250 }
            r8.append(r2)     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0250 }
            r1.f10631n = r8     // Catch:{ all -> 0x0250 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0250 }
            r8.<init>()     // Catch:{ all -> 0x0250 }
            r8.append(r3)     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0250 }
            r1.f10632o = r8     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = ""
            if (r4 == 0) goto L_0x0165
            java.lang.String[] r9 = r4.split(r10)     // Catch:{ all -> 0x0250 }
            int r11 = r9.length     // Catch:{ all -> 0x0250 }
            if (r11 <= 0) goto L_0x0163
            r8 = r9[r13]     // Catch:{ all -> 0x0250 }
        L_0x0163:
            r9 = r4
            goto L_0x0166
        L_0x0165:
            r9 = r8
        L_0x0166:
            r1.f10633p = r8     // Catch:{ all -> 0x0250 }
            r1.f10634q = r9     // Catch:{ all -> 0x0250 }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0250 }
            r1.f10635r = r8     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r1.f10634q     // Catch:{ all -> 0x0250 }
            byte[] r8 = r8.getBytes()     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = com.tencent.bugly.proguard.C4893z.m17035b(r8)     // Catch:{ all -> 0x0250 }
            r1.f10638u = r8     // Catch:{ all -> 0x0250 }
            int r8 = com.tencent.bugly.crashreport.crash.C4824c.f10694f     // Catch:{ all -> 0x0250 }
            java.util.Map r8 = com.tencent.bugly.proguard.C4893z.m17017a(r8, r13)     // Catch:{ all -> 0x0250 }
            r1.f10643z = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.f10526d     // Catch:{ all -> 0x0250 }
            r1.f10595A = r8     // Catch:{ all -> 0x0250 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0250 }
            r8.<init>()     // Catch:{ all -> 0x0250 }
            java.lang.String r9 = r7.getName()     // Catch:{ all -> 0x0250 }
            r8.append(r9)     // Catch:{ all -> 0x0250 }
            java.lang.String r9 = "("
            r8.append(r9)     // Catch:{ all -> 0x0250 }
            long r11 = r7.getId()     // Catch:{ all -> 0x0250 }
            r8.append(r11)     // Catch:{ all -> 0x0250 }
            java.lang.String r9 = ")"
            r8.append(r9)     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0250 }
            r1.f10596B = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.mo26710y()     // Catch:{ all -> 0x0250 }
            r1.f10603I = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.util.Map r8 = r8.mo26707v()     // Catch:{ all -> 0x0250 }
            r1.f10625h = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            long r8 = r8.f10498a     // Catch:{ all -> 0x0250 }
            r1.f10607M = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            boolean r8 = r8.mo26678a()     // Catch:{ all -> 0x0250 }
            r1.f10608N = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            int r8 = r8.mo26662H()     // Catch:{ all -> 0x0250 }
            r1.f10610P = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            int r8 = r8.mo26663I()     // Catch:{ all -> 0x0250 }
            r1.f10611Q = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.util.Map r8 = r8.mo26656B()     // Catch:{ all -> 0x0250 }
            r1.f10612R = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.util.Map r8 = r8.mo26661G()     // Catch:{ all -> 0x0250 }
            r1.f10613S = r8     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.crash.b r8 = r0.f10724d     // Catch:{ all -> 0x0250 }
            r8.mo26752c(r1)     // Catch:{ all -> 0x0250 }
            byte[] r8 = com.tencent.bugly.proguard.C4889y.m16990a()     // Catch:{ all -> 0x0250 }
            r1.f10642y = r8     // Catch:{ all -> 0x0250 }
            java.util.Map<java.lang.String, java.lang.String> r8 = r1.f10609O     // Catch:{ all -> 0x0250 }
            if (r8 != 0) goto L_0x0201
            java.util.LinkedHashMap r8 = new java.util.LinkedHashMap     // Catch:{ all -> 0x0250 }
            r8.<init>()     // Catch:{ all -> 0x0250 }
            r1.f10609O = r8     // Catch:{ all -> 0x0250 }
        L_0x0201:
            if (r5 == 0) goto L_0x0208
            java.util.Map<java.lang.String, java.lang.String> r8 = r1.f10609O     // Catch:{ all -> 0x0250 }
            r8.putAll(r5)     // Catch:{ all -> 0x0250 }
        L_0x0208:
            java.lang.String r5 = com.tencent.bugly.proguard.C4893z.m17008a()     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.common.info.a r8 = r0.f10723c     // Catch:{ all -> 0x0250 }
            java.lang.String r8 = r8.f10526d     // Catch:{ all -> 0x0250 }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x0250 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0250 }
            r9.<init>()     // Catch:{ all -> 0x0250 }
            r9.append(r2)     // Catch:{ all -> 0x0250 }
            r9.append(r10)     // Catch:{ all -> 0x0250 }
            r9.append(r3)     // Catch:{ all -> 0x0250 }
            r9.append(r10)     // Catch:{ all -> 0x0250 }
            r9.append(r4)     // Catch:{ all -> 0x0250 }
            java.lang.String r2 = r9.toString()     // Catch:{ all -> 0x0250 }
            r17 = r14
            r18 = r5
            r19 = r8
            r20 = r7
            r21 = r2
            r22 = r1
            com.tencent.bugly.crashreport.crash.C4822b.m16639a(r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x0250 }
            com.tencent.bugly.crashreport.crash.b r2 = r0.f10724d     // Catch:{ all -> 0x0250 }
            boolean r2 = r2.mo26749a(r1)     // Catch:{ all -> 0x0250 }
            if (r2 != 0) goto L_0x024a
            com.tencent.bugly.crashreport.crash.b r0 = r0.f10724d     // Catch:{ all -> 0x0250 }
            r2 = 3000(0xbb8, double:1.482E-320)
            r0.mo26747a(r1, r2, r13)     // Catch:{ all -> 0x0250 }
        L_0x024a:
            java.lang.Object[] r0 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r0)
            return
        L_0x0250:
            r0 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C4888x.m16978a(r0)     // Catch:{ all -> 0x0260 }
            if (r1 != 0) goto L_0x025a
            r0.printStackTrace()     // Catch:{ all -> 0x0260 }
        L_0x025a:
            java.lang.Object[] r0 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r0)
            return
        L_0x0260:
            r0 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r13]
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4827d.m16680a(com.tencent.bugly.crashreport.crash.d, java.lang.Thread, int, java.lang.String, java.lang.String, java.lang.String, java.util.Map):void");
    }

    /* renamed from: a */
    public static C4827d m16678a(Context context) {
        if (f10721a == null) {
            f10721a = new C4827d(context);
        }
        return f10721a;
    }

    /* renamed from: a */
    public static void m16681a(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        C4886w.m16969a().mo26934a(new C4829b(thread, i, str, str2, str3, map));
    }
}
