package com.tencent.bugly.crashreport.common.strategy;

import android.content.Context;
import com.facebook.appevents.AppEventsConstants;
import com.tencent.bugly.BUGLY;
import com.tencent.bugly.crashreport.biz.C4802b;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.proguard.C4854ar;
import com.tencent.bugly.proguard.C4855as;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4878r;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.common.strategy.a */
/* compiled from: BUGLY */
public final class C4809a {

    /* renamed from: a */
    public static int f10579a = 1000;

    /* renamed from: b */
    private static C4809a f10580b;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static String f10581h;

    /* renamed from: c */
    private final List<BUGLY> f10582c;

    /* renamed from: d */
    private final C4886w f10583d;

    /* renamed from: e */
    private final StrategyBean f10584e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public StrategyBean f10585f = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public Context f10586g;

    /* renamed from: com.tencent.bugly.crashreport.common.strategy.a$a */
    /* compiled from: BUGLY */
    class C4810a extends Thread {
        C4810a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]>
         arg types: [int, ?[OBJECT, ARRAY], int]
         candidates:
          com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
          com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, com.tencent.bugly.proguard.o):java.util.Map
          com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o):boolean
          com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]> */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void
         arg types: [com.tencent.bugly.crashreport.common.strategy.StrategyBean, int]
         candidates:
          com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.a, com.tencent.bugly.crashreport.common.strategy.StrategyBean):com.tencent.bugly.crashreport.common.strategy.StrategyBean
          com.tencent.bugly.crashreport.common.strategy.a.a(android.content.Context, java.util.List<com.tencent.bugly.a>):com.tencent.bugly.crashreport.common.strategy.a
          com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void */
        public final void run() {
            try {
                Map<String, byte[]> a = C4875p.m16899a().mo26900a(C4809a.f10579a, (C4874o) null, true);
                if (a != null) {
                    byte[] bArr = a.get("device");
                    byte[] bArr2 = a.get("gateway");
                    if (bArr != null) {
                        C4806a.m16491a(C4809a.this.f10586g).mo26688e(new String(bArr));
                    }
                    if (bArr2 != null) {
                        C4806a.m16491a(C4809a.this.f10586g).mo26686d(new String(bArr2));
                    }
                }
                StrategyBean unused = C4809a.this.f10585f = C4809a.m16593d();
                if (C4809a.this.f10585f != null && !C4893z.m17024a(C4809a.f10581h) && C4893z.m17047c(C4809a.f10581h)) {
                    C4809a.this.f10585f.f10571r = C4809a.f10581h;
                    C4809a.this.f10585f.f10572s = C4809a.f10581h;
                }
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
            C4809a aVar = C4809a.this;
            aVar.mo26717a(aVar.f10585f, false);
        }
    }

    private C4809a(Context context, List<BUGLY> list) {
        this.f10586g = context;
        this.f10584e = new StrategyBean();
        this.f10582c = list;
        this.f10583d = C4886w.m16969a();
    }

    /* renamed from: d */
    public static StrategyBean m16593d() {
        byte[] bArr;
        List<C4878r> a = C4875p.m16899a().mo26899a(2);
        if (a == null || a.size() <= 0 || (bArr = a.get(0).f10987g) == null) {
            return null;
        }
        return (StrategyBean) C4893z.m17007a(bArr, StrategyBean.CREATOR);
    }

    /* renamed from: c */
    public final StrategyBean mo26720c() {
        StrategyBean strategyBean = this.f10585f;
        if (strategyBean != null) {
            return strategyBean;
        }
        return this.f10584e;
    }

    /* renamed from: b */
    public final synchronized boolean mo26719b() {
        return this.f10585f != null;
    }

    /* renamed from: a */
    public static synchronized C4809a m16590a(Context context, List<BUGLY> list) {
        C4809a aVar;
        synchronized (C4809a.class) {
            if (f10580b == null) {
                f10580b = new C4809a(context, list);
            }
            aVar = f10580b;
        }
        return aVar;
    }

    /* renamed from: a */
    public final void mo26716a(long j) {
        this.f10583d.mo26935a(new C4810a(), j);
    }

    /* renamed from: a */
    public static synchronized C4809a m16589a() {
        C4809a aVar;
        synchronized (C4809a.class) {
            aVar = f10580b;
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo26717a(StrategyBean strategyBean, boolean z) {
        C4888x.m16982c("[Strategy] Notify %s", C4802b.class.getName());
        C4802b.m16461a(strategyBean, z);
        for (BUGLY aVar : this.f10582c) {
            try {
                C4888x.m16982c("[Strategy] Notify %s", aVar.getClass().getName());
                aVar.onServerStrategyChanged(strategyBean);
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    public static void m16591a(String str) {
        if (C4893z.m17024a(str) || !C4893z.m17047c(str)) {
            C4888x.m16983d("URL user set is invalid.", new Object[0]);
        } else {
            f10581h = str;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void
     arg types: [com.tencent.bugly.crashreport.common.strategy.StrategyBean, int]
     candidates:
      com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.a, com.tencent.bugly.crashreport.common.strategy.StrategyBean):com.tencent.bugly.crashreport.common.strategy.StrategyBean
      com.tencent.bugly.crashreport.common.strategy.a.a(android.content.Context, java.util.List<com.tencent.bugly.a>):com.tencent.bugly.crashreport.common.strategy.a
      com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void */
    /* renamed from: a */
    public final void mo26718a(C4855as asVar) {
        if (asVar != null) {
            StrategyBean strategyBean = this.f10585f;
            if (strategyBean == null || asVar.f10889h != strategyBean.f10569p) {
                StrategyBean strategyBean2 = new StrategyBean();
                strategyBean2.f10560g = asVar.f10882a;
                strategyBean2.f10562i = asVar.f10884c;
                strategyBean2.f10561h = asVar.f10883b;
                if (C4893z.m17024a(f10581h) || !C4893z.m17047c(f10581h)) {
                    if (C4893z.m17047c(asVar.f10885d)) {
                        C4888x.m16982c("[Strategy] Upload url changes to %s", asVar.f10885d);
                        strategyBean2.f10571r = asVar.f10885d;
                    }
                    if (C4893z.m17047c(asVar.f10886e)) {
                        C4888x.m16982c("[Strategy] Exception upload url changes to %s", asVar.f10886e);
                        strategyBean2.f10572s = asVar.f10886e;
                    }
                }
                C4854ar arVar = asVar.f10887f;
                if (arVar != null && !C4893z.m17024a(arVar.f10877a)) {
                    strategyBean2.f10574u = asVar.f10887f.f10877a;
                }
                long j = asVar.f10889h;
                if (j != 0) {
                    strategyBean2.f10569p = j;
                }
                Map<String, String> map = asVar.f10888g;
                if (map != null && map.size() > 0) {
                    Map<String, String> map2 = asVar.f10888g;
                    strategyBean2.f10575v = map2;
                    String str = map2.get("B11");
                    if (str == null || !str.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                        strategyBean2.f10563j = false;
                    } else {
                        strategyBean2.f10563j = true;
                    }
                    String str2 = asVar.f10888g.get("B3");
                    if (str2 != null) {
                        strategyBean2.f10578y = Long.valueOf(str2).longValue();
                    }
                    int i = asVar.f10890i;
                    strategyBean2.f10570q = (long) i;
                    strategyBean2.f10577x = (long) i;
                    String str3 = asVar.f10888g.get("B27");
                    if (str3 != null && str3.length() > 0) {
                        try {
                            int parseInt = Integer.parseInt(str3);
                            if (parseInt > 0) {
                                strategyBean2.f10576w = parseInt;
                            }
                        } catch (Exception e) {
                            if (!C4888x.m16978a(e)) {
                                e.printStackTrace();
                            }
                        }
                    }
                    String str4 = asVar.f10888g.get("B25");
                    if (str4 == null || !str4.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                        strategyBean2.f10565l = false;
                    } else {
                        strategyBean2.f10565l = true;
                    }
                }
                C4888x.m16977a("[Strategy] enableCrashReport:%b, enableQuery:%b, enableUserInfo:%b, enableAnr:%b, enableBlock:%b, enableSession:%b, enableSessionTimer:%b, sessionOverTime:%d, enableCocos:%b, strategyLastUpdateTime:%d", Boolean.valueOf(strategyBean2.f10560g), Boolean.valueOf(strategyBean2.f10562i), Boolean.valueOf(strategyBean2.f10561h), Boolean.valueOf(strategyBean2.f10563j), Boolean.valueOf(strategyBean2.f10564k), Boolean.valueOf(strategyBean2.f10567n), Boolean.valueOf(strategyBean2.f10568o), Long.valueOf(strategyBean2.f10570q), Boolean.valueOf(strategyBean2.f10565l), Long.valueOf(strategyBean2.f10569p));
                this.f10585f = strategyBean2;
                C4875p.m16899a().mo26905b(2);
                C4878r rVar = new C4878r();
                rVar.f10982b = 2;
                rVar.f10981a = strategyBean2.f10558e;
                rVar.f10985e = strategyBean2.f10559f;
                rVar.f10987g = C4893z.m17027a(strategyBean2);
                C4875p.m16899a().mo26904a(rVar);
                mo26717a(strategyBean2, true);
            }
        }
    }
}
