package com.tencent.bugly.crashreport.crash.jni;

import android.annotation.SuppressLint;
import android.content.Context;
import com.facebook.internal.ServerProtocol;
import com.heimavista.api.HvApi;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.C4794a;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.C4822b;
import com.tencent.bugly.crashreport.crash.C4824c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.io.File;

/* compiled from: BUGLY */
public class NativeCrashHandler implements C4794a {

    /* renamed from: a */
    private static NativeCrashHandler f10761a = null;

    /* renamed from: l */
    private static boolean f10762l = false;

    /* renamed from: m */
    private static boolean f10763m = false;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public static boolean f10764o = true;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Context f10765b;

    /* renamed from: c */
    private final C4806a f10766c;

    /* renamed from: d */
    private final C4886w f10767d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public NativeExceptionHandler f10768e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f10769f;

    /* renamed from: g */
    private final boolean f10770g;

    /* renamed from: h */
    private boolean f10771h = false;

    /* renamed from: i */
    private boolean f10772i = false;

    /* renamed from: j */
    private boolean f10773j = false;

    /* renamed from: k */
    private boolean f10774k = false;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public C4822b f10775n;

    /* renamed from: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler$a */
    /* compiled from: BUGLY */
    class C4833a implements Runnable {
        C4833a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
         arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
         candidates:
          com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.am
          com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.an
          com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void
         arg types: [int, java.lang.String]
         candidates:
          com.tencent.bugly.crashreport.crash.jni.b.a(java.lang.String, java.lang.String):java.lang.String
          com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void */
        public final void run() {
            if (!C4893z.m17021a(NativeCrashHandler.this.f10765b, "native_record_lock", 10000)) {
                C4888x.m16977a("[Native] Failed to lock file for handling native crash record.", new Object[0]);
                return;
            }
            if (!NativeCrashHandler.f10764o) {
                boolean unused = NativeCrashHandler.this.m16696a((int) HvApi.RET_CODE_LOGOUT, Bugly.SDK_IS_DEV);
            }
            CrashDetailBean a = C4835b.m16707a(NativeCrashHandler.this.f10765b, NativeCrashHandler.this.f10769f, NativeCrashHandler.this.f10768e);
            if (a != null) {
                C4888x.m16977a("[Native] Get crash from native record.", new Object[0]);
                if (!NativeCrashHandler.this.f10775n.mo26749a(a)) {
                    NativeCrashHandler.this.f10775n.mo26747a(a, 3000L, false);
                }
                C4835b.m16713a(false, NativeCrashHandler.this.f10769f);
            }
            NativeCrashHandler.this.mo26782a();
            C4893z.m17040b(NativeCrashHandler.this.f10765b, "native_record_lock");
        }
    }

    @SuppressLint({"SdCardPath"})
    private NativeCrashHandler(Context context, C4806a aVar, C4822b bVar, C4886w wVar, boolean z, String str) {
        this.f10765b = C4893z.m17002a(context);
        try {
            if (C4893z.m17024a(str)) {
                str = context.getDir("bugly", 0).getAbsolutePath();
            }
        } catch (Throwable unused) {
            str = "/data/data/" + C4806a.m16491a(context).f10525c + "/app_bugly";
        }
        this.f10775n = bVar;
        this.f10769f = str;
        this.f10766c = aVar;
        this.f10767d = wVar;
        this.f10770g = z;
        this.f10768e = new C4834a(context, aVar, bVar, C4809a.m16589a());
    }

    public static synchronized NativeCrashHandler getInstance(Context context, C4806a aVar, C4822b bVar, C4809a aVar2, C4886w wVar, boolean z, String str) {
        NativeCrashHandler nativeCrashHandler;
        synchronized (NativeCrashHandler.class) {
            if (f10761a == null) {
                f10761a = new NativeCrashHandler(context, aVar, bVar, wVar, z, str);
            }
            nativeCrashHandler = f10761a;
        }
        return nativeCrashHandler;
    }

    public static boolean isShouldHandleInJava() {
        return f10764o;
    }

    public static void setShouldHandleInJava(boolean z) {
        f10764o = z;
        NativeCrashHandler nativeCrashHandler = f10761a;
        if (nativeCrashHandler != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(z);
            nativeCrashHandler.m16696a((int) HvApi.RET_CODE_LOGOUT, sb.toString());
        }
    }

    public boolean appendLogToNative(String str, String str2, String str3) {
        if (!((!this.f10771h && !this.f10772i) || !f10762l || str == null || str2 == null || str3 == null)) {
            try {
                if (this.f10772i) {
                    return appendNativeLog(str, str2, str3);
                }
                Boolean bool = (Boolean) C4893z.m17006a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "appendNativeLog", null, new Class[]{String.class, String.class, String.class}, new Object[]{str, str2, str3});
                if (bool != null) {
                    return bool.booleanValue();
                }
                return false;
            } catch (UnsatisfiedLinkError unused) {
                f10762l = false;
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public native boolean appendNativeLog(String str, String str2, String str3);

    /* access modifiers changed from: protected */
    public native boolean appendWholeNativeLog(String str);

    public void checkUploadRecordCrash() {
        this.f10767d.mo26934a(new C4833a());
    }

    public boolean filterSigabrtSysLog() {
        return m16696a(998, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
    }

    public synchronized String getDumpFilePath() {
        return this.f10769f;
    }

    public String getLogFromNative() {
        if ((!this.f10771h && !this.f10772i) || !f10762l) {
            return null;
        }
        try {
            if (this.f10772i) {
                return getNativeLog();
            }
            return (String) C4893z.m17006a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "getNativeLog", null, null, null);
        } catch (UnsatisfiedLinkError unused) {
            f10762l = false;
            return null;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    public NativeExceptionHandler getNativeExceptionHandler() {
        return this.f10768e;
    }

    /* access modifiers changed from: protected */
    public native String getNativeKeyValueList();

    /* access modifiers changed from: protected */
    public native String getNativeLog();

    public synchronized boolean isUserOpened() {
        return this.f10774k;
    }

    public synchronized void onStrategyChanged(StrategyBean strategyBean) {
        if (strategyBean != null) {
            if (strategyBean.f10560g != this.f10773j) {
                C4888x.m16983d("server native changed to %b", Boolean.valueOf(strategyBean.f10560g));
            }
        }
        boolean z = C4809a.m16589a().mo26720c().f10560g && this.f10774k;
        if (z != this.f10773j) {
            C4888x.m16977a("native changed to %b", Boolean.valueOf(z));
            m16700b(z);
        }
    }

    public boolean putKeyValueToNative(String str, String str2) {
        if ((this.f10771h || this.f10772i) && f10762l && str != null && str2 != null) {
            try {
                if (this.f10772i) {
                    return putNativeKeyValue(str, str2);
                }
                Boolean bool = (Boolean) C4893z.m17006a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "putNativeKeyValue", null, new Class[]{String.class, String.class}, new Object[]{str, str2});
                if (bool != null) {
                    return bool.booleanValue();
                }
                return false;
            } catch (UnsatisfiedLinkError unused) {
                f10762l = false;
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public native boolean putNativeKeyValue(String str, String str2);

    /* access modifiers changed from: protected */
    public native String regist(String str, boolean z, int i);

    public void removeEmptyNativeRecordFiles() {
        C4835b.m16717c(this.f10769f);
    }

    /* access modifiers changed from: protected */
    public native String removeNativeKeyValue(String str);

    public synchronized void setDumpFilePath(String str) {
        this.f10769f = str;
    }

    public boolean setNativeAppChannel(String str) {
        return m16696a(12, str);
    }

    public boolean setNativeAppPackage(String str) {
        return m16696a(13, str);
    }

    public boolean setNativeAppVersion(String str) {
        return m16696a(10, str);
    }

    /* access modifiers changed from: protected */
    public native void setNativeInfo(int i, String str);

    public boolean setNativeIsAppForeground(boolean z) {
        return m16696a(14, z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : Bugly.SDK_IS_DEV);
    }

    public boolean setNativeLaunchTime(long j) {
        try {
            return m16696a(15, String.valueOf(j));
        } catch (NumberFormatException e) {
            if (C4888x.m16978a(e)) {
                return false;
            }
            e.printStackTrace();
            return false;
        }
    }

    public boolean setNativeUserId(String str) {
        return m16696a(11, str);
    }

    public synchronized void setUserOpened(boolean z) {
        m16704c(z);
        boolean isUserOpened = isUserOpened();
        C4809a a = C4809a.m16589a();
        if (a != null) {
            isUserOpened = isUserOpened && a.mo26720c().f10560g;
        }
        if (isUserOpened != this.f10773j) {
            C4888x.m16977a("native changed to %b", Boolean.valueOf(isUserOpened));
            m16700b(isUserOpened);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        return;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0036=Splitter:B:21:0x0036, B:27:0x006f=Splitter:B:27:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void startNativeMonitor() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.f10772i     // Catch:{ all -> 0x0076 }
            if (r0 != 0) goto L_0x006f
            boolean r0 = r3.f10771h     // Catch:{ all -> 0x0076 }
            if (r0 == 0) goto L_0x000a
            goto L_0x006f
        L_0x000a:
            java.lang.String r0 = "Bugly"
            com.tencent.bugly.crashreport.common.info.a r1 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r1 = r1.f10535m     // Catch:{ all -> 0x0076 }
            boolean r1 = com.tencent.bugly.proguard.C4893z.m17024a(r1)     // Catch:{ all -> 0x0076 }
            if (r1 != 0) goto L_0x0018
            r1 = 1
            goto L_0x0019
        L_0x0018:
            r1 = 0
        L_0x0019:
            com.tencent.bugly.crashreport.common.info.a r2 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r2 = r2.f10535m     // Catch:{ all -> 0x0076 }
            if (r1 != 0) goto L_0x0025
            com.tencent.bugly.crashreport.common.info.a r2 = r3.f10766c     // Catch:{ all -> 0x0076 }
            r2.getClass()     // Catch:{ all -> 0x0076 }
            goto L_0x0026
        L_0x0025:
            r0 = r2
        L_0x0026:
            boolean r0 = m16698a(r0, r1)     // Catch:{ all -> 0x0076 }
            r3.f10772i = r0     // Catch:{ all -> 0x0076 }
            boolean r0 = r3.f10772i     // Catch:{ all -> 0x0076 }
            if (r0 != 0) goto L_0x0036
            boolean r0 = r3.f10771h     // Catch:{ all -> 0x0076 }
            if (r0 != 0) goto L_0x0036
            monitor-exit(r3)
            return
        L_0x0036:
            boolean r0 = r3.f10770g     // Catch:{ all -> 0x0076 }
            r3.m16695a(r0)     // Catch:{ all -> 0x0076 }
            boolean r0 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10762l     // Catch:{ all -> 0x0076 }
            if (r0 == 0) goto L_0x006d
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = r0.f10532j     // Catch:{ all -> 0x0076 }
            r3.setNativeAppVersion(r0)     // Catch:{ all -> 0x0076 }
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = r0.f10534l     // Catch:{ all -> 0x0076 }
            r3.setNativeAppChannel(r0)     // Catch:{ all -> 0x0076 }
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = r0.f10525c     // Catch:{ all -> 0x0076 }
            r3.setNativeAppPackage(r0)     // Catch:{ all -> 0x0076 }
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = r0.mo26691g()     // Catch:{ all -> 0x0076 }
            r3.setNativeUserId(r0)     // Catch:{ all -> 0x0076 }
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            boolean r0 = r0.mo26678a()     // Catch:{ all -> 0x0076 }
            r3.setNativeIsAppForeground(r0)     // Catch:{ all -> 0x0076 }
            com.tencent.bugly.crashreport.common.info.a r0 = r3.f10766c     // Catch:{ all -> 0x0076 }
            long r0 = r0.f10498a     // Catch:{ all -> 0x0076 }
            r3.setNativeLaunchTime(r0)     // Catch:{ all -> 0x0076 }
        L_0x006d:
            monitor-exit(r3)
            return
        L_0x006f:
            boolean r0 = r3.f10770g     // Catch:{ all -> 0x0076 }
            r3.m16695a(r0)     // Catch:{ all -> 0x0076 }
            monitor-exit(r3)
            return
        L_0x0076:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.startNativeMonitor():void");
    }

    /* access modifiers changed from: protected */
    public native void testCrash();

    public void testNativeCrash() {
        if (!this.f10772i) {
            C4888x.m16983d("[Native] Bugly SO file has not been load.", new Object[0]);
        } else {
            testCrash();
        }
    }

    /* access modifiers changed from: protected */
    public native String unregist();

    /* renamed from: c */
    private synchronized void m16703c() {
        if (!this.f10773j) {
            C4888x.m16983d("[Native] Native crash report has already unregistered.", new Object[0]);
            return;
        }
        try {
            if (unregist() != null) {
                C4888x.m16977a("[Native] Successfully closed native crash report.", new Object[0]);
                this.f10773j = false;
                return;
            }
        } catch (Throwable unused) {
            C4888x.m16982c("[Native] Failed to close native crash report.", new Object[0]);
        }
        try {
            C4893z.m17006a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "enableHandler", null, new Class[]{Boolean.TYPE}, new Object[]{false});
            this.f10773j = false;
            C4888x.m16977a("[Native] Successfully closed native crash report.", new Object[0]);
            return;
        } catch (Throwable unused2) {
            C4888x.m16982c("[Native] Failed to close native crash report.", new Object[0]);
            this.f10772i = false;
            this.f10771h = false;
            return;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(16:14|(1:16)(15:18|(1:20)|21|22|(1:24)|25|(1:27)|28|29|(1:31)(1:32)|33|(1:35)(1:36)|37|38|39)|17|21|22|(0)|25|(0)|28|29|(0)(0)|33|(0)(0)|37|38|39) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:7|8|9|(3:11|12|(16:14|(1:16)(15:18|(1:20)|21|22|(1:24)|25|(1:27)|28|29|(1:31)(1:32)|33|(1:35)(1:36)|37|38|39)|17|21|22|(0)|25|(0)|28|29|(0)(0)|33|(0)(0)|37|38|39))(2:44|(7:46|47|48|(1:50)(1:51)|52|(1:54)|(7:56|(1:58)|59|(1:61)|62|63|64)))|65|66|67|68) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x008a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x0196 */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007c A[Catch:{ all -> 0x008a }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0088 A[Catch:{ all -> 0x008a }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008e A[Catch:{ all -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0096 A[Catch:{ all -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a1 A[Catch:{ all -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a9 A[Catch:{ all -> 0x00bd }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m16695a(boolean r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = r10.f10773j     // Catch:{ all -> 0x019c }
            r1 = 0
            if (r0 == 0) goto L_0x000f
            java.lang.String r11 = "[Native] Native crash report has already registered."
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x019c }
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r0)     // Catch:{ all -> 0x019c }
            monitor-exit(r10)
            return
        L_0x000f:
            boolean r0 = r10.f10772i     // Catch:{ all -> 0x019c }
            r2 = 2
            r3 = 1
            if (r0 == 0) goto L_0x00c6
            java.lang.String r0 = r10.f10769f     // Catch:{ all -> 0x00bd }
            java.lang.String r11 = r10.regist(r0, r11, r3)     // Catch:{ all -> 0x00bd }
            if (r11 == 0) goto L_0x0196
            java.lang.String r0 = "[Native] Native Crash Report enable."
            java.lang.Object[] r4 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r4)     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = "[Native] Check extra jni for Bugly NDK v%s"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x00bd }
            r4[r1] = r11     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r4)     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = "2.1.1"
            java.lang.String r4 = "."
            java.lang.String r5 = ""
            java.lang.String r0 = r0.replace(r4, r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r4 = "2.3.0"
            java.lang.String r5 = "."
            java.lang.String r6 = ""
            java.lang.String r4 = r4.replace(r5, r6)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = "."
            java.lang.String r6 = ""
            java.lang.String r5 = r11.replace(r5, r6)     // Catch:{ all -> 0x00bd }
            int r6 = r5.length()     // Catch:{ all -> 0x00bd }
            if (r6 != r2) goto L_0x0061
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r2.<init>()     // Catch:{ all -> 0x00bd }
            r2.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = "0"
        L_0x0059:
            r2.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x00bd }
            goto L_0x0072
        L_0x0061:
            int r2 = r5.length()     // Catch:{ all -> 0x00bd }
            if (r2 != r3) goto L_0x0072
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r2.<init>()     // Catch:{ all -> 0x00bd }
            r2.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = "00"
            goto L_0x0059
        L_0x0072:
            int r2 = java.lang.Integer.parseInt(r5)     // Catch:{ all -> 0x008a }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x008a }
            if (r2 < r0) goto L_0x007e
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10762l = r3     // Catch:{ all -> 0x008a }
        L_0x007e:
            int r0 = java.lang.Integer.parseInt(r5)     // Catch:{ all -> 0x008a }
            int r2 = java.lang.Integer.parseInt(r4)     // Catch:{ all -> 0x008a }
            if (r0 < r2) goto L_0x008a
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10763m = r3     // Catch:{ all -> 0x008a }
        L_0x008a:
            boolean r0 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10763m     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x0096
            java.lang.String r0 = "[Native] Info setting jni can be accessed."
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r2)     // Catch:{ all -> 0x00bd }
            goto L_0x009d
        L_0x0096:
            java.lang.String r0 = "[Native] Info setting jni can not be accessed."
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r2)     // Catch:{ all -> 0x00bd }
        L_0x009d:
            boolean r0 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10762l     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x00a9
            java.lang.String r0 = "[Native] Extra jni can be accessed."
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r2)     // Catch:{ all -> 0x00bd }
            goto L_0x00b0
        L_0x00a9:
            java.lang.String r0 = "[Native] Extra jni can not be accessed."
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r2)     // Catch:{ all -> 0x00bd }
        L_0x00b0:
            com.tencent.bugly.crashreport.common.info.a r0 = r10.f10766c     // Catch:{ all -> 0x00bd }
            r0.f10536n = r11     // Catch:{ all -> 0x00bd }
            boolean r11 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10762l     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4889y.m16989a(r11)     // Catch:{ all -> 0x00bd }
            r10.f10773j = r3     // Catch:{ all -> 0x00bd }
            monitor-exit(r10)
            return
        L_0x00bd:
            java.lang.String r11 = "[Native] Failed to load Bugly SO file."
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x019c }
            com.tencent.bugly.proguard.C4888x.m16982c(r11, r0)     // Catch:{ all -> 0x019c }
            goto L_0x0196
        L_0x00c6:
            boolean r0 = r10.f10771h     // Catch:{ all -> 0x019c }
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r4 = "registNativeExceptionHandler2"
            r5 = 4
            java.lang.Class[] r6 = new java.lang.Class[r5]     // Catch:{ all -> 0x0196 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r6[r1] = r7     // Catch:{ all -> 0x0196 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r6[r3] = r7     // Catch:{ all -> 0x0196 }
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0196 }
            r6[r2] = r7     // Catch:{ all -> 0x0196 }
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0196 }
            r8 = 3
            r6[r8] = r7     // Catch:{ all -> 0x0196 }
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0196 }
            java.lang.String r7 = r10.f10769f     // Catch:{ all -> 0x0196 }
            r5[r1] = r7     // Catch:{ all -> 0x0196 }
            android.content.Context r7 = r10.f10765b     // Catch:{ all -> 0x0196 }
            java.lang.String r7 = com.tencent.bugly.crashreport.common.info.C4807b.m16553a(r7, r1)     // Catch:{ all -> 0x0196 }
            r5[r3] = r7     // Catch:{ all -> 0x0196 }
            r7 = 5
            if (r11 == 0) goto L_0x00f5
            r9 = 1
            goto L_0x00f6
        L_0x00f5:
            r9 = 5
        L_0x00f6:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0196 }
            r5[r2] = r9     // Catch:{ all -> 0x0196 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0196 }
            r5[r8] = r9     // Catch:{ all -> 0x0196 }
            r9 = 0
            java.lang.Object r0 = com.tencent.bugly.proguard.C4893z.m17006a(r0, r4, r9, r6, r5)     // Catch:{ all -> 0x0196 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0196 }
            if (r0 != 0) goto L_0x013e
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r4 = "registNativeExceptionHandler"
            java.lang.Class[] r5 = new java.lang.Class[r8]     // Catch:{ all -> 0x0196 }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r5[r1] = r6     // Catch:{ all -> 0x0196 }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r5[r3] = r6     // Catch:{ all -> 0x0196 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0196 }
            r5[r2] = r6     // Catch:{ all -> 0x0196 }
            java.lang.Object[] r6 = new java.lang.Object[r8]     // Catch:{ all -> 0x0196 }
            java.lang.String r8 = r10.f10769f     // Catch:{ all -> 0x0196 }
            r6[r1] = r8     // Catch:{ all -> 0x0196 }
            android.content.Context r8 = r10.f10765b     // Catch:{ all -> 0x0196 }
            java.lang.String r8 = com.tencent.bugly.crashreport.common.info.C4807b.m16553a(r8, r1)     // Catch:{ all -> 0x0196 }
            r6[r3] = r8     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.crashreport.common.info.C4806a.m16492b()     // Catch:{ all -> 0x0196 }
            int r8 = com.tencent.bugly.crashreport.common.info.C4806a.m16490K()     // Catch:{ all -> 0x0196 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0196 }
            r6[r2] = r8     // Catch:{ all -> 0x0196 }
            java.lang.Object r0 = com.tencent.bugly.proguard.C4893z.m17006a(r0, r4, r9, r5, r6)     // Catch:{ all -> 0x0196 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0196 }
        L_0x013e:
            if (r0 == 0) goto L_0x0196
            r10.f10773j = r3     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.crashreport.common.info.a r2 = com.tencent.bugly.crashreport.common.info.C4806a.m16492b()     // Catch:{ all -> 0x0196 }
            r2.f10536n = r0     // Catch:{ all -> 0x0196 }
            java.lang.String r2 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r4 = "checkExtraJni"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ all -> 0x0196 }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r5[r1] = r6     // Catch:{ all -> 0x0196 }
            java.lang.Object[] r6 = new java.lang.Object[r3]     // Catch:{ all -> 0x0196 }
            r6[r1] = r0     // Catch:{ all -> 0x0196 }
            java.lang.Object r0 = com.tencent.bugly.proguard.C4893z.m17006a(r2, r4, r9, r5, r6)     // Catch:{ all -> 0x0196 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0196 }
            if (r0 == 0) goto L_0x0167
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f10762l = r0     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.proguard.C4889y.m16989a(r0)     // Catch:{ all -> 0x0196 }
        L_0x0167:
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r2 = "enableHandler"
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ all -> 0x0196 }
            java.lang.Class r5 = java.lang.Boolean.TYPE     // Catch:{ all -> 0x0196 }
            r4[r1] = r5     // Catch:{ all -> 0x0196 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ all -> 0x0196 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0196 }
            r5[r1] = r6     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.proguard.C4893z.m17006a(r0, r2, r9, r4, r5)     // Catch:{ all -> 0x0196 }
            if (r11 == 0) goto L_0x017f
            r7 = 1
        L_0x017f:
            java.lang.String r11 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r0 = "setLogMode"
            java.lang.Class[] r2 = new java.lang.Class[r3]     // Catch:{ all -> 0x0196 }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0196 }
            r2[r1] = r4     // Catch:{ all -> 0x0196 }
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0196 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0196 }
            r3[r1] = r4     // Catch:{ all -> 0x0196 }
            com.tencent.bugly.proguard.C4893z.m17006a(r11, r0, r9, r2, r3)     // Catch:{ all -> 0x0196 }
            monitor-exit(r10)
            return
        L_0x0196:
            r10.f10772i = r1     // Catch:{ all -> 0x019c }
            r10.f10771h = r1     // Catch:{ all -> 0x019c }
            monitor-exit(r10)
            return
        L_0x019c:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.m16695a(boolean):void");
    }

    /* renamed from: b */
    private synchronized void m16700b(boolean z) {
        if (z) {
            startNativeMonitor();
        } else {
            m16703c();
        }
    }

    public static synchronized NativeCrashHandler getInstance() {
        NativeCrashHandler nativeCrashHandler;
        synchronized (NativeCrashHandler.class) {
            nativeCrashHandler = f10761a;
        }
        return nativeCrashHandler;
    }

    public void testNativeCrash(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        sb.append(z);
        m16696a(16, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(z2);
        m16696a(17, sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append(z3);
        m16696a(18, sb3.toString());
        testNativeCrash();
    }

    /* renamed from: c */
    private synchronized void m16704c(boolean z) {
        if (this.f10774k != z) {
            C4888x.m16977a("user change native %b", Boolean.valueOf(z));
            this.f10774k = z;
        }
    }

    /* renamed from: a */
    private static boolean m16698a(String str, boolean z) {
        boolean z2;
        try {
            C4888x.m16977a("[Native] Trying to load so: %s", str);
            if (z) {
                System.load(str);
            } else {
                System.loadLibrary(str);
            }
            try {
                C4888x.m16977a("[Native] Successfully loaded SO: %s", str);
                return true;
            } catch (Throwable th) {
                th = th;
                z2 = true;
            }
        } catch (Throwable th2) {
            th = th2;
            z2 = false;
            C4888x.m16983d(th.getMessage(), new Object[0]);
            C4888x.m16983d("[Native] Failed to load so: %s", str);
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo26782a() {
        long b = C4893z.m17032b() - C4824c.f10695g;
        long b2 = C4893z.m17032b() + 86400000;
        File file = new File(this.f10769f);
        if (file.exists() && file.isDirectory()) {
            try {
                File[] listFiles = file.listFiles();
                if (listFiles == null) {
                    return;
                }
                if (listFiles.length != 0) {
                    int i = 0;
                    int i2 = 0;
                    for (File file2 : listFiles) {
                        long lastModified = file2.lastModified();
                        if (lastModified < b || lastModified >= b2) {
                            C4888x.m16977a("[Native] Delete record file: %s", file2.getAbsolutePath());
                            i++;
                            if (file2.delete()) {
                                i2++;
                            }
                        }
                    }
                    C4888x.m16982c("[Native] Number of record files overdue: %d, has deleted: %d", Integer.valueOf(i), Integer.valueOf(i2));
                }
            } catch (Throwable th) {
                C4888x.m16978a(th);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m16696a(int i, String str) {
        if (this.f10772i && f10763m) {
            try {
                setNativeInfo(i, str);
                return true;
            } catch (UnsatisfiedLinkError unused) {
                f10763m = false;
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }
}
