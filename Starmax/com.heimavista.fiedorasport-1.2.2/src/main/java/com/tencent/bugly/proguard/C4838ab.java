package com.tencent.bugly.proguard;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.tencent.bugly.proguard.ab */
/* compiled from: BUGLY */
public final class C4838ab extends Thread {

    /* renamed from: a */
    private boolean f10793a = false;

    /* renamed from: b */
    private List<C4837aa> f10794b = Collections.synchronizedList(new ArrayList());

    /* renamed from: c */
    private List<C4839ac> f10795c = Collections.synchronizedList(new ArrayList());

    /* renamed from: a */
    public final void mo26827a() {
        m16744a(new Handler(Looper.getMainLooper()), DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
    }

    /* renamed from: b */
    public final void mo26829b() {
        m16743a(new Handler(Looper.getMainLooper()));
    }

    /* renamed from: c */
    public final boolean mo26831c() {
        this.f10793a = true;
        if (!isAlive()) {
            return false;
        }
        try {
            interrupt();
        } catch (Exception e) {
            C4888x.m16984e(e.getStackTrace().toString(), new Object[0]);
        }
        return true;
    }

    /* renamed from: d */
    public final boolean mo26832d() {
        if (isAlive()) {
            return false;
        }
        try {
            start();
            return true;
        } catch (Exception e) {
            C4888x.m16984e(e.getStackTrace().toString(), new Object[0]);
            return false;
        }
    }

    public final void run() {
        while (!this.f10793a) {
            for (int i = 0; i < this.f10794b.size(); i++) {
                this.f10794b.get(i).mo26819a();
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            for (long j = 2000; j > 0 && !isInterrupted(); j = AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS - (SystemClock.uptimeMillis() - uptimeMillis)) {
                try {
                    Thread.sleep(j);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.f10794b.size(); i3++) {
                i2 = Math.max(i2, this.f10794b.get(i3).mo26822c());
            }
            if (!(i2 == 0 || i2 == 1)) {
                ArrayList arrayList = new ArrayList();
                for (int i4 = 0; i4 < this.f10794b.size(); i4++) {
                    C4837aa aaVar = this.f10794b.get(i4);
                    if (aaVar.mo26821b()) {
                        arrayList.add(aaVar);
                        aaVar.mo26820a(Long.MAX_VALUE);
                    }
                }
                int i5 = 0;
                boolean z = false;
                while (i5 < arrayList.size()) {
                    C4837aa aaVar2 = (C4837aa) arrayList.get(i5);
                    Thread d = aaVar2.mo26823d();
                    boolean z2 = z;
                    for (int i6 = 0; i6 < this.f10795c.size(); i6++) {
                        if (this.f10795c.get(i6).mo26740a(super)) {
                            z2 = true;
                        }
                    }
                    if (!z2 && aaVar2.mo26824e().contains("main")) {
                        aaVar2.mo26825f();
                        C4888x.m16983d("although thread is blocked ,may not be anr error,so restore handler check wait time and restart check main thread", new Object[0]);
                    }
                    i5++;
                    z = z2;
                }
            }
        }
    }

    /* renamed from: a */
    private void m16744a(Handler handler, long j) {
        if (handler == null) {
            C4888x.m16984e("addThread handler should not be null", new Object[0]);
            return;
        }
        String name = handler.getLooper().getThread().getName();
        for (int i = 0; i < this.f10794b.size(); i++) {
            if (this.f10794b.get(i).mo26824e().equals(handler.getLooper().getThread().getName())) {
                C4888x.m16984e("addThread fail ,this thread has been added in monitor queue", new Object[0]);
                return;
            }
        }
        this.f10794b.add(new C4837aa(handler, name, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS));
    }

    /* renamed from: b */
    public final void mo26830b(C4839ac acVar) {
        this.f10795c.remove(acVar);
    }

    /* renamed from: a */
    private void m16743a(Handler handler) {
        if (handler == null) {
            C4888x.m16984e("removeThread handler should not be null", new Object[0]);
            return;
        }
        for (int i = 0; i < this.f10794b.size(); i++) {
            if (this.f10794b.get(i).mo26824e().equals(handler.getLooper().getThread().getName())) {
                C4888x.m16982c("remove handler::%s", this.f10794b.get(i));
                this.f10794b.remove(i);
            }
        }
    }

    /* renamed from: a */
    public final void mo26828a(C4839ac acVar) {
        if (this.f10795c.contains(acVar)) {
            C4888x.m16984e("addThreadMonitorListeners fail ,this threadMonitorListener has been added in monitor queue", new Object[0]);
        } else {
            this.f10795c.add(acVar);
        }
    }
}
