package com.tencent.bugly.proguard;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.j */
/* compiled from: BUGLY */
public final class C4867j {

    /* renamed from: a */
    private ByteBuffer f10936a;

    /* renamed from: b */
    private String f10937b;

    public C4867j(int i) {
        this.f10937b = "GBK";
        this.f10936a = ByteBuffer.allocate(i);
    }

    /* renamed from: a */
    public final ByteBuffer mo26874a() {
        return this.f10936a;
    }

    /* renamed from: b */
    public final byte[] mo26886b() {
        byte[] bArr = new byte[this.f10936a.position()];
        System.arraycopy(this.f10936a.array(), 0, bArr, 0, this.f10936a.position());
        return bArr;
    }

    /* renamed from: a */
    private void m16853a(int i) {
        if (this.f10936a.remaining() < i) {
            ByteBuffer allocate = ByteBuffer.allocate((this.f10936a.capacity() + i) << 1);
            allocate.put(this.f10936a.array(), 0, this.f10936a.position());
            this.f10936a = allocate;
        }
    }

    /* renamed from: b */
    private void m16854b(byte b, int i) {
        if (i < 15) {
            this.f10936a.put((byte) (b | (i << 4)));
        } else if (i < 256) {
            this.f10936a.put((byte) (b | 240));
            this.f10936a.put((byte) i);
        } else {
            throw new C4858b("tag is too large: " + i);
        }
    }

    public C4867j() {
        this(128);
    }

    /* renamed from: a */
    public final void mo26884a(boolean z, int i) {
        mo26875a(z ? (byte) 1 : 0, i);
    }

    /* renamed from: a */
    public final void mo26875a(byte b, int i) {
        m16853a(3);
        if (b == 0) {
            m16854b((byte) 12, i);
            return;
        }
        m16854b((byte) 0, i);
        this.f10936a.put(b);
    }

    /* renamed from: a */
    public final void mo26883a(short s, int i) {
        m16853a(4);
        if (s < -128 || s > 127) {
            m16854b((byte) 1, i);
            this.f10936a.putShort(s);
            return;
        }
        mo26875a((byte) s, i);
    }

    /* renamed from: a */
    public final void mo26876a(int i, int i2) {
        m16853a(6);
        if (i < -32768 || i > 32767) {
            m16854b((byte) 2, i2);
            this.f10936a.putInt(i);
            return;
        }
        mo26883a((short) i, i2);
    }

    /* renamed from: a */
    public final void mo26877a(long j, int i) {
        m16853a(10);
        if (j < -2147483648L || j > 2147483647L) {
            m16854b((byte) 3, i);
            this.f10936a.putLong(j);
            return;
        }
        mo26876a((int) j, i);
    }

    /* renamed from: a */
    public final void mo26880a(String str, int i) {
        byte[] bArr;
        try {
            bArr = str.getBytes(this.f10937b);
        } catch (UnsupportedEncodingException unused) {
            bArr = str.getBytes();
        }
        m16853a(bArr.length + 10);
        if (bArr.length > 255) {
            m16854b((byte) 7, i);
            this.f10936a.putInt(bArr.length);
            this.f10936a.put(bArr);
            return;
        }
        m16854b((byte) 6, i);
        this.f10936a.put((byte) bArr.length);
        this.f10936a.put(bArr);
    }

    /* renamed from: a */
    public final <K, V> void mo26882a(Map map, int i) {
        int i2;
        m16853a(8);
        m16854b((byte) 8, i);
        if (map == null) {
            i2 = 0;
        } else {
            i2 = map.size();
        }
        mo26876a(i2, 0);
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                mo26879a(entry.getKey(), 0);
                mo26879a(entry.getValue(), 1);
            }
        }
    }

    /* renamed from: a */
    public final void mo26885a(byte[] bArr, int i) {
        m16853a(bArr.length + 8);
        m16854b((byte) 13, i);
        m16854b((byte) 0, 0);
        mo26876a(bArr.length, 0);
        this.f10936a.put(bArr);
    }

    /* renamed from: a */
    public final <T> void mo26881a(Collection collection, int i) {
        int i2;
        m16853a(8);
        m16854b((byte) 9, i);
        if (collection == null) {
            i2 = 0;
        } else {
            i2 = collection.size();
        }
        mo26876a(i2, 0);
        if (collection != null) {
            for (Object obj : collection) {
                mo26879a(obj, 0);
            }
        }
    }

    /* renamed from: a */
    public final void mo26878a(C4868k kVar, int i) {
        m16853a(2);
        m16854b((byte) 10, i);
        kVar.mo26840a(this);
        m16853a(2);
        m16854b((byte) 11, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
     arg types: [java.util.List, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void */
    /* renamed from: a */
    public final void mo26879a(Object obj, int i) {
        if (obj instanceof Byte) {
            mo26875a(((Byte) obj).byteValue(), i);
        } else if (obj instanceof Boolean) {
            mo26875a(((Boolean) obj).booleanValue() ? (byte) 1 : 0, i);
        } else if (obj instanceof Short) {
            mo26883a(((Short) obj).shortValue(), i);
        } else if (obj instanceof Integer) {
            mo26876a(((Integer) obj).intValue(), i);
        } else if (obj instanceof Long) {
            mo26877a(((Long) obj).longValue(), i);
        } else if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            m16853a(6);
            m16854b((byte) 4, i);
            this.f10936a.putFloat(floatValue);
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            m16853a(10);
            m16854b((byte) 5, i);
            this.f10936a.putDouble(doubleValue);
        } else if (obj instanceof String) {
            mo26880a((String) obj, i);
        } else if (obj instanceof Map) {
            mo26882a((Map) obj, i);
        } else if (obj instanceof List) {
            mo26881a((Collection) ((List) obj), i);
        } else if (obj instanceof C4868k) {
            m16853a(2);
            m16854b((byte) 10, i);
            ((C4868k) obj).mo26840a(this);
            m16853a(2);
            m16854b((byte) 11, 0);
        } else if (obj instanceof byte[]) {
            mo26885a((byte[]) obj, i);
        } else if (obj instanceof boolean[]) {
            boolean[] zArr = (boolean[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(zArr.length, 0);
            for (boolean z : zArr) {
                mo26875a(z ? (byte) 1 : 0, 0);
            }
        } else if (obj instanceof short[]) {
            short[] sArr = (short[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(sArr.length, 0);
            for (short s : sArr) {
                mo26883a(s, 0);
            }
        } else if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(iArr.length, 0);
            for (int i2 : iArr) {
                mo26876a(i2, 0);
            }
        } else if (obj instanceof long[]) {
            long[] jArr = (long[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(jArr.length, 0);
            for (long j : jArr) {
                mo26877a(j, 0);
            }
        } else if (obj instanceof float[]) {
            float[] fArr = (float[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(fArr.length, 0);
            for (float f : fArr) {
                m16853a(6);
                m16854b((byte) 4, 0);
                this.f10936a.putFloat(f);
            }
        } else if (obj instanceof double[]) {
            double[] dArr = (double[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(dArr.length, 0);
            for (double d : dArr) {
                m16853a(10);
                m16854b((byte) 5, 0);
                this.f10936a.putDouble(d);
            }
        } else if (obj.getClass().isArray()) {
            Object[] objArr = (Object[]) obj;
            m16853a(8);
            m16854b((byte) 9, i);
            mo26876a(objArr.length, 0);
            for (Object obj2 : objArr) {
                mo26879a(obj2, 0);
            }
        } else if (obj instanceof Collection) {
            mo26881a((Collection) obj, i);
        } else {
            throw new C4858b("write object error: unsupport type. " + obj.getClass());
        }
    }

    /* renamed from: a */
    public final int mo26873a(String str) {
        this.f10937b = str;
        return 0;
    }
}
