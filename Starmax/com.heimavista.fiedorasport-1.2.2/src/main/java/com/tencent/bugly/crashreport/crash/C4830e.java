package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import android.os.Process;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.info.C4807b;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4889y;
import com.tencent.bugly.proguard.C4893z;
import java.lang.Thread;
import java.util.HashMap;

/* renamed from: com.tencent.bugly.crashreport.crash.e */
/* compiled from: BUGLY */
public final class C4830e implements Thread.UncaughtExceptionHandler {

    /* renamed from: h */
    private static String f10733h;

    /* renamed from: i */
    private static final Object f10734i = new Object();

    /* renamed from: a */
    private Context f10735a;

    /* renamed from: b */
    private C4822b f10736b;

    /* renamed from: c */
    private C4809a f10737c;

    /* renamed from: d */
    private C4806a f10738d;

    /* renamed from: e */
    private Thread.UncaughtExceptionHandler f10739e;

    /* renamed from: f */
    private Thread.UncaughtExceptionHandler f10740f;

    /* renamed from: g */
    private boolean f10741g = false;

    /* renamed from: j */
    private int f10742j;

    public C4830e(Context context, C4822b bVar, C4809a aVar, C4806a aVar2) {
        this.f10735a = context;
        this.f10736b = bVar;
        this.f10737c = aVar;
        this.f10738d = aVar2;
    }

    /* renamed from: a */
    public final synchronized void mo26775a() {
        if (this.f10742j >= 10) {
            C4888x.m16977a("java crash handler over %d, no need set.", 10);
            return;
        }
        this.f10741g = true;
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != null) {
            if (!getClass().getName().equals(defaultUncaughtExceptionHandler.getClass().getName())) {
                if ("com.android.internal.os.RuntimeInit$UncaughtHandler".equals(defaultUncaughtExceptionHandler.getClass().getName())) {
                    C4888x.m16977a("backup system java handler: %s", defaultUncaughtExceptionHandler.toString());
                    this.f10740f = defaultUncaughtExceptionHandler;
                    this.f10739e = defaultUncaughtExceptionHandler;
                } else {
                    C4888x.m16977a("backup java handler: %s", defaultUncaughtExceptionHandler.toString());
                    this.f10739e = defaultUncaughtExceptionHandler;
                }
            } else {
                return;
            }
        }
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.f10742j++;
        C4888x.m16977a("registered java monitor: %s", toString());
    }

    /* renamed from: b */
    public final synchronized void mo26778b() {
        this.f10741g = false;
        C4888x.m16977a("close java monitor!", new Object[0]);
        if (Thread.getDefaultUncaughtExceptionHandler().getClass().getName().contains("bugly")) {
            C4888x.m16977a("Java monitor to unregister: %s", toString());
            Thread.setDefaultUncaughtExceptionHandler(this.f10739e);
            this.f10742j--;
        }
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        synchronized (f10734i) {
            mo26777a(thread, th, true, null, null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.z.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.z.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.z.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.z.a(byte[], int):byte[]
      com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* renamed from: b */
    private CrashDetailBean m16685b(Thread thread, Throwable th, boolean z, String str, byte[] bArr) {
        String str2;
        String str3;
        Throwable th2 = th;
        String str4 = str;
        byte[] bArr2 = bArr;
        boolean z2 = false;
        if (th2 == null) {
            C4888x.m16983d("We can do nothing with a null throwable.", new Object[0]);
            return null;
        }
        boolean k = C4824c.m16656a().mo26768k();
        String str5 = (!k || !z) ? "" : " This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful![Bugly]";
        if (k && z) {
            C4888x.m16984e("This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!", new Object[0]);
        }
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        crashDetailBean.f10597C = C4807b.m16572k();
        crashDetailBean.f10598D = C4807b.m16568i();
        crashDetailBean.f10599E = C4807b.m16576m();
        crashDetailBean.f10600F = this.f10738d.mo26701p();
        crashDetailBean.f10601G = this.f10738d.mo26700o();
        crashDetailBean.f10602H = this.f10738d.mo26702q();
        crashDetailBean.f10640w = C4893z.m17010a(this.f10735a, C4824c.f10693e, (String) null);
        crashDetailBean.f10642y = C4889y.m16990a();
        Object[] objArr = new Object[1];
        byte[] bArr3 = crashDetailBean.f10642y;
        objArr[0] = Integer.valueOf(bArr3 == null ? 0 : bArr3.length);
        C4888x.m16977a("user log size:%d", objArr);
        crashDetailBean.f10619b = z ? 0 : 2;
        crashDetailBean.f10622e = this.f10738d.mo26693h();
        C4806a aVar = this.f10738d;
        crashDetailBean.f10623f = aVar.f10532j;
        crashDetailBean.f10624g = aVar.mo26708w();
        crashDetailBean.f10630m = this.f10738d.mo26691g();
        String name = th.getClass().getName();
        String b = m16686b(th2, 1000);
        if (b == null) {
            b = "";
        }
        Object[] objArr2 = new Object[2];
        objArr2[0] = Integer.valueOf(th.getStackTrace().length);
        objArr2[1] = Boolean.valueOf(th.getCause() != null);
        C4888x.m16984e("stack frame :%d, has cause %b", objArr2);
        if (th.getStackTrace().length > 0) {
            str2 = th.getStackTrace()[0].toString();
        } else {
            str2 = "";
        }
        Throwable th3 = th2;
        while (th3 != null && th3.getCause() != null) {
            th3 = th3.getCause();
        }
        if (th3 == null || th3 == th2) {
            crashDetailBean.f10631n = name;
            crashDetailBean.f10632o = b + str5;
            if (crashDetailBean.f10632o == null) {
                crashDetailBean.f10632o = "";
            }
            crashDetailBean.f10633p = str2;
            str3 = m16682a(th2, C4824c.f10694f);
            crashDetailBean.f10634q = str3;
        } else {
            crashDetailBean.f10631n = th3.getClass().getName();
            crashDetailBean.f10632o = m16686b(th3, 1000);
            if (crashDetailBean.f10632o == null) {
                crashDetailBean.f10632o = "";
            }
            if (th3.getStackTrace().length > 0) {
                crashDetailBean.f10633p = th3.getStackTrace()[0].toString();
            }
            StringBuilder sb = new StringBuilder();
            sb.append(name);
            sb.append(":");
            sb.append(b);
            sb.append("\n");
            sb.append(str2);
            sb.append("\n......");
            sb.append("\nCaused by:\n");
            sb.append(crashDetailBean.f10631n);
            sb.append(":");
            sb.append(crashDetailBean.f10632o);
            sb.append("\n");
            str3 = m16682a(th3, C4824c.f10694f);
            sb.append(str3);
            crashDetailBean.f10634q = sb.toString();
        }
        crashDetailBean.f10635r = System.currentTimeMillis();
        crashDetailBean.f10638u = C4893z.m17035b(crashDetailBean.f10634q.getBytes());
        try {
            crashDetailBean.f10643z = C4893z.m17017a(C4824c.f10694f, false);
            crashDetailBean.f10595A = this.f10738d.f10526d;
            crashDetailBean.f10596B = thread.getName() + "(" + thread.getId() + ")";
            crashDetailBean.f10643z.put(crashDetailBean.f10596B, str3);
            crashDetailBean.f10603I = this.f10738d.mo26710y();
            crashDetailBean.f10625h = this.f10738d.mo26707v();
            crashDetailBean.f10626i = this.f10738d.mo26664J();
            crashDetailBean.f10607M = this.f10738d.f10498a;
            crashDetailBean.f10608N = this.f10738d.mo26678a();
            crashDetailBean.f10610P = this.f10738d.mo26662H();
            crashDetailBean.f10611Q = this.f10738d.mo26663I();
            crashDetailBean.f10612R = this.f10738d.mo26656B();
            crashDetailBean.f10613S = this.f10738d.mo26661G();
        } catch (Throwable th4) {
            C4888x.m16984e("handle crash error %s", th4.toString());
        }
        if (z) {
            this.f10736b.mo26752c(crashDetailBean);
        } else {
            boolean z3 = str4 != null && str.length() > 0;
            if (bArr2 != null && bArr2.length > 0) {
                z2 = true;
            }
            if (z3) {
                crashDetailBean.f10609O = new HashMap(1);
                crashDetailBean.f10609O.put("UserData", str4);
            }
            if (z2) {
                crashDetailBean.f10614T = bArr2;
            }
        }
        return crashDetailBean;
    }

    /* renamed from: a */
    private static boolean m16684a(Thread thread) {
        synchronized (f10734i) {
            if (f10733h != null && thread.getName().equals(f10733h)) {
                return true;
            }
            f10733h = thread.getName();
            return false;
        }
    }

    /* renamed from: a */
    public final void mo26777a(Thread thread, Throwable th, boolean z, String str, byte[] bArr) {
        Thread thread2 = thread;
        Throwable th2 = th;
        boolean z2 = z;
        if (z2) {
            C4888x.m16984e("Java Crash Happen cause by %s(%d)", thread.getName(), Long.valueOf(thread.getId()));
            if (m16684a(thread)) {
                C4888x.m16977a("this class has handled this exception", new Object[0]);
                if (this.f10740f != null) {
                    C4888x.m16977a("call system handler", new Object[0]);
                    this.f10740f.uncaughtException(thread2, th2);
                } else {
                    C4888x.m16984e("current process die", new Object[0]);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                }
            }
        } else {
            C4888x.m16984e("Java Catch Happen", new Object[0]);
        }
        try {
            if (!this.f10741g) {
                C4888x.m16982c("Java crash handler is disable. Just return.", new Object[0]);
                if (z2) {
                    Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f10739e;
                    if (uncaughtExceptionHandler != null && m16683a(uncaughtExceptionHandler)) {
                        C4888x.m16984e("sys default last handle start!", new Object[0]);
                        this.f10739e.uncaughtException(thread2, th2);
                        C4888x.m16984e("sys default last handle end!", new Object[0]);
                    } else if (this.f10740f != null) {
                        C4888x.m16984e("system handle start!", new Object[0]);
                        this.f10740f.uncaughtException(thread2, th2);
                        C4888x.m16984e("system handle end!", new Object[0]);
                    } else {
                        C4888x.m16984e("crashreport last handle start!", new Object[0]);
                        C4888x.m16984e("current process die", new Object[0]);
                        Process.killProcess(Process.myPid());
                        System.exit(1);
                        C4888x.m16984e("crashreport last handle end!", new Object[0]);
                    }
                }
            } else {
                if (!this.f10737c.mo26719b()) {
                    C4888x.m16983d("no remote but still store!", new Object[0]);
                }
                if (!this.f10737c.mo26720c().f10560g) {
                    if (this.f10737c.mo26719b()) {
                        C4888x.m16984e("crash report was closed by remote , will not upload to Bugly , print local for helpful!", new Object[0]);
                        C4822b.m16639a(z2 ? "JAVA_CRASH" : "JAVA_CATCH", C4893z.m17008a(), this.f10738d.f10526d, thread.getName(), C4893z.m17013a(th), null);
                        if (z2) {
                            Thread.UncaughtExceptionHandler uncaughtExceptionHandler2 = this.f10739e;
                            if (uncaughtExceptionHandler2 != null && m16683a(uncaughtExceptionHandler2)) {
                                C4888x.m16984e("sys default last handle start!", new Object[0]);
                                this.f10739e.uncaughtException(thread2, th2);
                                C4888x.m16984e("sys default last handle end!", new Object[0]);
                                return;
                            } else if (this.f10740f != null) {
                                C4888x.m16984e("system handle start!", new Object[0]);
                                this.f10740f.uncaughtException(thread2, th2);
                                C4888x.m16984e("system handle end!", new Object[0]);
                                return;
                            } else {
                                C4888x.m16984e("crashreport last handle start!", new Object[0]);
                                C4888x.m16984e("current process die", new Object[0]);
                                Process.killProcess(Process.myPid());
                                System.exit(1);
                                C4888x.m16984e("crashreport last handle end!", new Object[0]);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
                CrashDetailBean b = m16685b(thread, th, z, str, bArr);
                if (b == null) {
                    C4888x.m16984e("pkg crash datas fail!", new Object[0]);
                    if (z2) {
                        Thread.UncaughtExceptionHandler uncaughtExceptionHandler3 = this.f10739e;
                        if (uncaughtExceptionHandler3 != null && m16683a(uncaughtExceptionHandler3)) {
                            C4888x.m16984e("sys default last handle start!", new Object[0]);
                            this.f10739e.uncaughtException(thread2, th2);
                            C4888x.m16984e("sys default last handle end!", new Object[0]);
                        } else if (this.f10740f != null) {
                            C4888x.m16984e("system handle start!", new Object[0]);
                            this.f10740f.uncaughtException(thread2, th2);
                            C4888x.m16984e("system handle end!", new Object[0]);
                        } else {
                            C4888x.m16984e("crashreport last handle start!", new Object[0]);
                            C4888x.m16984e("current process die", new Object[0]);
                            Process.killProcess(Process.myPid());
                            System.exit(1);
                            C4888x.m16984e("crashreport last handle end!", new Object[0]);
                        }
                    }
                } else {
                    C4822b.m16639a(z2 ? "JAVA_CRASH" : "JAVA_CATCH", C4893z.m17008a(), this.f10738d.f10526d, thread.getName(), C4893z.m17013a(th), b);
                    if (!this.f10736b.mo26749a(b)) {
                        this.f10736b.mo26747a(b, 3000, z2);
                    }
                    if (z2) {
                        this.f10736b.mo26751b(b);
                    }
                    if (z2) {
                        Thread.UncaughtExceptionHandler uncaughtExceptionHandler4 = this.f10739e;
                        if (uncaughtExceptionHandler4 != null && m16683a(uncaughtExceptionHandler4)) {
                            C4888x.m16984e("sys default last handle start!", new Object[0]);
                            this.f10739e.uncaughtException(thread2, th2);
                            C4888x.m16984e("sys default last handle end!", new Object[0]);
                        } else if (this.f10740f != null) {
                            C4888x.m16984e("system handle start!", new Object[0]);
                            this.f10740f.uncaughtException(thread2, th2);
                            C4888x.m16984e("system handle end!", new Object[0]);
                        } else {
                            C4888x.m16984e("crashreport last handle start!", new Object[0]);
                            C4888x.m16984e("current process die", new Object[0]);
                            Process.killProcess(Process.myPid());
                            System.exit(1);
                            C4888x.m16984e("crashreport last handle end!", new Object[0]);
                        }
                    }
                }
            }
        } catch (Throwable th3) {
            if (z2) {
                Thread.UncaughtExceptionHandler uncaughtExceptionHandler5 = this.f10739e;
                if (uncaughtExceptionHandler5 != null && m16683a(uncaughtExceptionHandler5)) {
                    C4888x.m16984e("sys default last handle start!", new Object[0]);
                    this.f10739e.uncaughtException(thread2, th2);
                    C4888x.m16984e("sys default last handle end!", new Object[0]);
                } else if (this.f10740f != null) {
                    C4888x.m16984e("system handle start!", new Object[0]);
                    this.f10740f.uncaughtException(thread2, th2);
                    C4888x.m16984e("system handle end!", new Object[0]);
                } else {
                    C4888x.m16984e("crashreport last handle start!", new Object[0]);
                    C4888x.m16984e("current process die", new Object[0]);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    C4888x.m16984e("crashreport last handle end!", new Object[0]);
                }
            }
            throw th3;
        }
    }

    /* renamed from: b */
    private static String m16686b(Throwable th, int i) {
        if (th.getMessage() == null) {
            return "";
        }
        if (th.getMessage().length() <= 1000) {
            return th.getMessage();
        }
        return th.getMessage().substring(0, 1000) + "\n[Message over limit size:1000" + ", has been cutted!]";
    }

    /* renamed from: a */
    private static boolean m16683a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        if (uncaughtExceptionHandler == null) {
            return true;
        }
        String name = uncaughtExceptionHandler.getClass().getName();
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            if (name.equals(className) && "uncaughtException".equals(methodName)) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002b, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void mo26776a(com.tencent.bugly.crashreport.common.strategy.StrategyBean r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 == 0) goto L_0x002a
            boolean r0 = r5.f10560g     // Catch:{ all -> 0x0027 }
            boolean r1 = r4.f10741g     // Catch:{ all -> 0x0027 }
            if (r0 == r1) goto L_0x002a
            java.lang.String r0 = "java changed to %b"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0027 }
            r2 = 0
            boolean r3 = r5.f10560g     // Catch:{ all -> 0x0027 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0027 }
            r1[r2] = r3     // Catch:{ all -> 0x0027 }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r1)     // Catch:{ all -> 0x0027 }
            boolean r5 = r5.f10560g     // Catch:{ all -> 0x0027 }
            if (r5 == 0) goto L_0x0023
            r4.mo26775a()     // Catch:{ all -> 0x0027 }
            monitor-exit(r4)
            return
        L_0x0023:
            r4.mo26778b()     // Catch:{ all -> 0x0027 }
            goto L_0x002a
        L_0x0027:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x002a:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4830e.mo26776a(com.tencent.bugly.crashreport.common.strategy.StrategyBean):void");
    }

    /* renamed from: a */
    private static String m16682a(Throwable th, int i) {
        if (th == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try {
            if (th.getStackTrace() != null) {
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                int i2 = 0;
                while (i2 < length) {
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    if (i <= 0 || sb.length() < i) {
                        sb.append(stackTraceElement.toString());
                        sb.append("\n");
                        i2++;
                    } else {
                        sb.append("\n[Stack over limit size :" + i + " , has been cutted !]");
                        return sb.toString();
                    }
                }
            }
        } catch (Throwable th2) {
            C4888x.m16984e("gen stack error %s", th2.toString());
        }
        return sb.toString();
    }
}
