package com.tencent.bugly.crashreport.crash.anr;

import android.app.ActivityManager;
import android.content.Context;
import android.os.FileObserver;
import android.os.Process;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.info.C4807b;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.crash.C4822b;
import com.tencent.bugly.crashreport.crash.C4824c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C4838ab;
import com.tencent.bugly.proguard.C4839ac;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4889y;
import com.tencent.bugly.proguard.C4893z;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.tencent.bugly.crashreport.crash.anr.b */
/* compiled from: BUGLY */
public final class C4819b implements C4839ac {

    /* renamed from: a */
    private AtomicInteger f10666a = new AtomicInteger(0);

    /* renamed from: b */
    private long f10667b = -1;

    /* renamed from: c */
    private final Context f10668c;

    /* renamed from: d */
    private final C4806a f10669d;

    /* renamed from: e */
    private final C4886w f10670e;

    /* renamed from: f */
    private final C4809a f10671f;

    /* renamed from: g */
    private final String f10672g;

    /* renamed from: h */
    private final C4822b f10673h;

    /* renamed from: i */
    private FileObserver f10674i;

    /* renamed from: j */
    private boolean f10675j = true;

    /* renamed from: k */
    private C4838ab f10676k;

    /* renamed from: l */
    private int f10677l;

    /* renamed from: com.tencent.bugly.crashreport.crash.anr.b$a */
    /* compiled from: BUGLY */
    class C4820a extends FileObserver {
        C4820a(String str, int i) {
            super(str, 8);
        }

        public final void onEvent(int i, String str) {
            if (str != null) {
                String str2 = "/data/anr/" + str;
                if (!str2.contains("trace")) {
                    C4888x.m16983d("not anr file %s", str2);
                    return;
                }
                C4819b.this.mo26737a(str2);
            }
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.crash.anr.b$b */
    /* compiled from: BUGLY */
    class C4821b implements Runnable {
        C4821b() {
        }

        public final void run() {
            C4819b.this.mo26741b();
        }
    }

    public C4819b(Context context, C4809a aVar, C4806a aVar2, C4886w wVar, C4822b bVar) {
        this.f10668c = C4893z.m17002a(context);
        this.f10672g = context.getDir("bugly", 0).getAbsolutePath();
        this.f10669d = aVar2;
        this.f10670e = wVar;
        this.f10671f = aVar;
        this.f10673h = bVar;
    }

    /* renamed from: a */
    private static ActivityManager.ProcessErrorStateInfo m16616a(Context context, long j) {
        try {
            C4888x.m16982c("to find!", new Object[0]);
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            int i = 0;
            while (true) {
                C4888x.m16982c("waiting!", new Object[0]);
                List<ActivityManager.ProcessErrorStateInfo> processesInErrorState = activityManager.getProcessesInErrorState();
                if (processesInErrorState != null) {
                    for (ActivityManager.ProcessErrorStateInfo processErrorStateInfo : processesInErrorState) {
                        if (processErrorStateInfo.condition == 2) {
                            C4888x.m16982c("found!", new Object[0]);
                            return processErrorStateInfo;
                        }
                    }
                }
                C4893z.m17037b(500);
                int i2 = i + 1;
                if (((long) i) >= 20) {
                    C4888x.m16982c("end!", new Object[0]);
                    return null;
                }
                i = i2;
            }
        } catch (Exception e) {
            C4888x.m16981b(e);
            return null;
        }
    }

    /* renamed from: b */
    private synchronized void m16620b(boolean z) {
        if (z) {
            m16622e();
        } else {
            m16623f();
        }
    }

    /* renamed from: c */
    private synchronized void m16621c(boolean z) {
        if (this.f10675j != z) {
            C4888x.m16977a("user change anr %b", Boolean.valueOf(z));
            this.f10675j = z;
        }
    }

    /* renamed from: e */
    private synchronized void m16622e() {
        if (m16624g()) {
            C4888x.m16983d("start when started!", new Object[0]);
            return;
        }
        this.f10674i = new C4820a("/data/anr/", 8);
        try {
            this.f10674i.startWatching();
            C4888x.m16977a("start anr monitor!", new Object[0]);
            this.f10670e.mo26934a(new C4821b());
        } catch (Throwable th) {
            this.f10674i = null;
            C4888x.m16983d("start anr monitor failed!", new Object[0]);
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: f */
    private synchronized void m16623f() {
        if (!m16624g()) {
            C4888x.m16983d("close when closed!", new Object[0]);
            return;
        }
        try {
            this.f10674i.stopWatching();
            this.f10674i = null;
            C4888x.m16983d("close anr monitor!", new Object[0]);
        } catch (Throwable th) {
            C4888x.m16983d("stop anr monitor failed!", new Object[0]);
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: g */
    private synchronized boolean m16624g() {
        return this.f10674i != null;
    }

    /* renamed from: h */
    private synchronized boolean m16625h() {
        return this.f10675j;
    }

    /* renamed from: d */
    public final boolean mo26743d() {
        C4838ab abVar = this.f10676k;
        if (abVar == null) {
            return false;
        }
        abVar.mo26829b();
        this.f10676k.mo26830b(this);
        boolean c = this.f10676k.mo26831c();
        this.f10676k = null;
        return c;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("Trace file that has invalid format: " + r11, new java.lang.Object[0]);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x004f */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo26741b() {
        /*
            r14 = this;
            long r0 = com.tencent.bugly.proguard.C4893z.m17032b()
            long r2 = com.tencent.bugly.crashreport.crash.C4824c.f10695g
            long r0 = r0 - r2
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r14.f10672g
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 == 0) goto L_0x0085
            boolean r3 = r2.isDirectory()
            if (r3 == 0) goto L_0x0085
            java.io.File[] r2 = r2.listFiles()     // Catch:{ all -> 0x0081 }
            if (r2 == 0) goto L_0x0080
            int r3 = r2.length     // Catch:{ all -> 0x0081 }
            if (r3 != 0) goto L_0x0024
            goto L_0x0080
        L_0x0024:
            java.lang.String r3 = "bugly_trace_"
            java.lang.String r4 = ".txt"
            r5 = 12
            int r6 = r2.length     // Catch:{ all -> 0x0081 }
            r7 = 0
            r8 = 0
            r9 = 0
        L_0x002e:
            if (r8 >= r6) goto L_0x006d
            r10 = r2[r8]     // Catch:{ all -> 0x0081 }
            java.lang.String r11 = r10.getName()     // Catch:{ all -> 0x0081 }
            boolean r12 = r11.startsWith(r3)     // Catch:{ all -> 0x0081 }
            if (r12 == 0) goto L_0x006a
            int r12 = r11.indexOf(r4)     // Catch:{ all -> 0x004f }
            if (r12 <= 0) goto L_0x0062
            java.lang.String r12 = r11.substring(r5, r12)     // Catch:{ all -> 0x004f }
            long r11 = java.lang.Long.parseLong(r12)     // Catch:{ all -> 0x004f }
            int r13 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r13 < 0) goto L_0x0062
            goto L_0x006a
        L_0x004f:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0081 }
            java.lang.String r13 = "Trace file that has invalid format: "
            r12.<init>(r13)     // Catch:{ all -> 0x0081 }
            r12.append(r11)     // Catch:{ all -> 0x0081 }
            java.lang.String r11 = r12.toString()     // Catch:{ all -> 0x0081 }
            java.lang.Object[] r12 = new java.lang.Object[r7]     // Catch:{ all -> 0x0081 }
            com.tencent.bugly.proguard.C4888x.m16982c(r11, r12)     // Catch:{ all -> 0x0081 }
        L_0x0062:
            boolean r10 = r10.delete()     // Catch:{ all -> 0x0081 }
            if (r10 == 0) goto L_0x006a
            int r9 = r9 + 1
        L_0x006a:
            int r8 = r8 + 1
            goto L_0x002e
        L_0x006d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0081 }
            java.lang.String r1 = "Number of overdue trace files that has deleted: "
            r0.<init>(r1)     // Catch:{ all -> 0x0081 }
            r0.append(r9)     // Catch:{ all -> 0x0081 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0081 }
            java.lang.Object[] r1 = new java.lang.Object[r7]     // Catch:{ all -> 0x0081 }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r1)     // Catch:{ all -> 0x0081 }
        L_0x0080:
            return
        L_0x0081:
            r0 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r0)
        L_0x0085:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C4819b.mo26741b():void");
    }

    /* renamed from: c */
    public final boolean mo26742c() {
        C4838ab abVar = this.f10676k;
        if (abVar != null && abVar.isAlive()) {
            return false;
        }
        this.f10676k = new C4838ab();
        C4838ab abVar2 = this.f10676k;
        StringBuilder sb = new StringBuilder("Bugly-ThreadMonitor");
        int i = this.f10677l;
        this.f10677l = i + 1;
        sb.append(i);
        abVar2.setName(sb.toString());
        this.f10676k.mo26827a();
        this.f10676k.mo26828a(this);
        return this.f10676k.mo26832d();
    }

    /* renamed from: a */
    private CrashDetailBean m16617a(C4818a aVar) {
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        try {
            crashDetailBean.f10597C = C4807b.m16572k();
            crashDetailBean.f10598D = C4807b.m16568i();
            crashDetailBean.f10599E = C4807b.m16576m();
            crashDetailBean.f10600F = this.f10669d.mo26701p();
            crashDetailBean.f10601G = this.f10669d.mo26700o();
            crashDetailBean.f10602H = this.f10669d.mo26702q();
            crashDetailBean.f10640w = C4893z.m17010a(this.f10668c, C4824c.f10693e, (String) null);
            crashDetailBean.f10619b = 3;
            crashDetailBean.f10622e = this.f10669d.mo26693h();
            crashDetailBean.f10623f = this.f10669d.f10532j;
            crashDetailBean.f10624g = this.f10669d.mo26708w();
            crashDetailBean.f10630m = this.f10669d.mo26691g();
            crashDetailBean.f10631n = "ANR_EXCEPTION";
            crashDetailBean.f10632o = aVar.f10664f;
            crashDetailBean.f10634q = aVar.f10665g;
            crashDetailBean.f10609O = new HashMap();
            crashDetailBean.f10609O.put("BUGLY_CR_01", aVar.f10663e);
            int i = -1;
            if (crashDetailBean.f10634q != null) {
                i = crashDetailBean.f10634q.indexOf("\n");
            }
            crashDetailBean.f10633p = i > 0 ? crashDetailBean.f10634q.substring(0, i) : "GET_FAIL";
            crashDetailBean.f10635r = aVar.f10661c;
            if (crashDetailBean.f10634q != null) {
                crashDetailBean.f10638u = C4893z.m17035b(crashDetailBean.f10634q.getBytes());
            }
            crashDetailBean.f10643z = aVar.f10660b;
            crashDetailBean.f10595A = aVar.f10659a;
            crashDetailBean.f10596B = "main(1)";
            crashDetailBean.f10603I = this.f10669d.mo26710y();
            crashDetailBean.f10625h = this.f10669d.mo26707v();
            crashDetailBean.f10626i = this.f10669d.mo26664J();
            crashDetailBean.f10639v = aVar.f10662d;
            crashDetailBean.f10606L = this.f10669d.f10536n;
            crashDetailBean.f10607M = this.f10669d.f10498a;
            crashDetailBean.f10608N = this.f10669d.mo26678a();
            crashDetailBean.f10610P = this.f10669d.mo26662H();
            crashDetailBean.f10611Q = this.f10669d.mo26663I();
            crashDetailBean.f10612R = this.f10669d.mo26656B();
            crashDetailBean.f10613S = this.f10669d.mo26661G();
            this.f10673h.mo26752c(crashDetailBean);
            crashDetailBean.f10642y = C4889y.m16990a();
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
        return crashDetailBean;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0130 A[Catch:{ all -> 0x0125 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x015c A[SYNTHETIC, Splitter:B:60:0x015c] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x016e A[SYNTHETIC, Splitter:B:68:0x016e] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m16619a(java.lang.String r16, java.lang.String r17, java.lang.String r18) {
        /*
            r1 = r17
            r0 = r18
            java.lang.String r2 = "main"
            java.lang.String r3 = ":"
            r4 = 1
            r5 = r16
            com.tencent.bugly.crashreport.crash.anr.TraceFileHelper$a r5 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readTargetDumpInfo(r0, r5, r4)
            r6 = 0
            if (r5 == 0) goto L_0x01bc
            java.util.Map<java.lang.String, java.lang.String[]> r7 = r5.f10653d
            if (r7 == 0) goto L_0x01bc
            int r7 = r7.size()
            if (r7 > 0) goto L_0x001e
            goto L_0x01bc
        L_0x001e:
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            r7 = 2
            boolean r8 = r0.exists()     // Catch:{ Exception -> 0x0188 }
            if (r8 != 0) goto L_0x003e
            java.io.File r8 = r0.getParentFile()     // Catch:{ Exception -> 0x0188 }
            boolean r8 = r8.exists()     // Catch:{ Exception -> 0x0188 }
            if (r8 != 0) goto L_0x003b
            java.io.File r8 = r0.getParentFile()     // Catch:{ Exception -> 0x0188 }
            r8.mkdirs()     // Catch:{ Exception -> 0x0188 }
        L_0x003b:
            r0.createNewFile()     // Catch:{ Exception -> 0x0188 }
        L_0x003e:
            boolean r8 = r0.exists()
            if (r8 == 0) goto L_0x017e
            boolean r8 = r0.canWrite()
            if (r8 != 0) goto L_0x004c
            goto L_0x017e
        L_0x004c:
            r1 = 0
            java.io.BufferedWriter r8 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x0129 }
            java.io.FileWriter r9 = new java.io.FileWriter     // Catch:{ IOException -> 0x0129 }
            r9.<init>(r0, r6)     // Catch:{ IOException -> 0x0129 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x0129 }
            java.util.Map<java.lang.String, java.lang.String[]> r0 = r5.f10653d     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r1 = "\n\n"
            java.lang.String r9 = "\n"
            java.lang.String r10 = " :\n"
            r11 = 3
            if (r0 == 0) goto L_0x0094
            int r12 = r0.length     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            if (r12 < r11) goto L_0x0094
            r12 = r0[r6]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r13 = r0[r4]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r0 = r0[r7]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r15 = "\"main\" tid="
            r14.<init>(r15)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r0)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r10)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r12)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r9)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r13)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14.append(r1)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r0 = r14.toString()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r8.write(r0)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r8.flush()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
        L_0x0094:
            java.util.Map<java.lang.String, java.lang.String[]> r0 = r5.f10653d     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
        L_0x009e:
            boolean r5 = r0.hasNext()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            if (r5 == 0) goto L_0x0110
            java.lang.Object r5 = r0.next()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.Object r12 = r5.getKey()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            boolean r12 = r12.equals(r2)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            if (r12 != 0) goto L_0x009e
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            if (r12 == 0) goto L_0x010e
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String[] r12 = (java.lang.String[]) r12     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            int r12 = r12.length     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            if (r12 < r11) goto L_0x010e
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String[] r12 = (java.lang.String[]) r12     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r12 = r12[r6]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.Object r13 = r5.getValue()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String[] r13 = (java.lang.String[]) r13     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r13 = r13[r4]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.Object r14 = r5.getValue()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String[] r14 = (java.lang.String[]) r14     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r14 = r14[r7]     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r11 = "\""
            r15.<init>(r11)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.Object r5 = r5.getKey()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r5)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r5 = "\" tid="
            r15.append(r5)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r14)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r10)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r12)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r9)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r13)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r15.append(r1)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            java.lang.String r5 = r15.toString()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r8.write(r5)     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
            r8.flush()     // Catch:{ IOException -> 0x0122, all -> 0x0120 }
        L_0x010e:
            r11 = 3
            goto L_0x009e
        L_0x0110:
            r8.close()     // Catch:{ IOException -> 0x0114 }
            goto L_0x011f
        L_0x0114:
            r0 = move-exception
            r1 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r1)
            if (r0 != 0) goto L_0x011f
            r1.printStackTrace()
        L_0x011f:
            return r4
        L_0x0120:
            r0 = move-exception
            goto L_0x0127
        L_0x0122:
            r0 = move-exception
            r1 = r8
            goto L_0x012a
        L_0x0125:
            r0 = move-exception
            r8 = r1
        L_0x0127:
            r1 = r0
            goto L_0x016c
        L_0x0129:
            r0 = move-exception
        L_0x012a:
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r0)     // Catch:{ all -> 0x0125 }
            if (r2 != 0) goto L_0x0133
            r0.printStackTrace()     // Catch:{ all -> 0x0125 }
        L_0x0133:
            java.lang.String r2 = "dump trace fail %s"
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0125 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r5.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.Class r7 = r0.getClass()     // Catch:{ all -> 0x0125 }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x0125 }
            r5.append(r7)     // Catch:{ all -> 0x0125 }
            r5.append(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0125 }
            r5.append(r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0125 }
            r4[r6] = r0     // Catch:{ all -> 0x0125 }
            com.tencent.bugly.proguard.C4888x.m16984e(r2, r4)     // Catch:{ all -> 0x0125 }
            if (r1 == 0) goto L_0x016b
            r1.close()     // Catch:{ IOException -> 0x0160 }
            goto L_0x016b
        L_0x0160:
            r0 = move-exception
            r1 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r1)
            if (r0 != 0) goto L_0x016b
            r1.printStackTrace()
        L_0x016b:
            return r6
        L_0x016c:
            if (r8 == 0) goto L_0x017d
            r8.close()     // Catch:{ IOException -> 0x0172 }
            goto L_0x017d
        L_0x0172:
            r0 = move-exception
            r2 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r2)
            if (r0 != 0) goto L_0x017d
            r2.printStackTrace()
        L_0x017d:
            throw r1
        L_0x017e:
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r6] = r1
            java.lang.String r1 = "backup file create fail %s"
            com.tencent.bugly.proguard.C4888x.m16984e(r1, r0)
            return r6
        L_0x0188:
            r0 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r0)
            if (r2 != 0) goto L_0x0192
            r0.printStackTrace()
        L_0x0192:
            java.lang.Object[] r2 = new java.lang.Object[r7]
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class r7 = r0.getClass()
            java.lang.String r7 = r7.getName()
            r5.append(r7)
            r5.append(r3)
            java.lang.String r0 = r0.getMessage()
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r2[r6] = r0
            r2[r4] = r1
            java.lang.String r0 = "backup file create error! %s  %s"
            com.tencent.bugly.proguard.C4888x.m16984e(r0, r2)
            return r6
        L_0x01bc:
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r1[r6] = r0
            java.lang.String r0 = "not found trace dump for %s"
            com.tencent.bugly.proguard.C4888x.m16984e(r0, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C4819b.m16619a(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* renamed from: a */
    public final boolean mo26739a() {
        return this.f10666a.get() != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.am
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.an
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* renamed from: a */
    private boolean m16618a(Context context, String str, ActivityManager.ProcessErrorStateInfo processErrorStateInfo, long j, Map<String, String> map) {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, "bugly/bugly_trace_" + j + ".txt");
        C4818a aVar = new C4818a();
        aVar.f10661c = j;
        aVar.f10662d = file.getAbsolutePath();
        String str2 = "";
        aVar.f10659a = processErrorStateInfo != null ? processErrorStateInfo.processName : str2;
        aVar.f10664f = processErrorStateInfo != null ? processErrorStateInfo.shortMsg : str2;
        if (processErrorStateInfo != null) {
            str2 = processErrorStateInfo.longMsg;
        }
        aVar.f10663e = str2;
        aVar.f10660b = map;
        if (map != null) {
            for (String str3 : map.keySet()) {
                if (str3.startsWith("main(")) {
                    aVar.f10665g = map.get(str3);
                }
            }
        }
        Object[] objArr = new Object[6];
        objArr[0] = Long.valueOf(aVar.f10661c);
        objArr[1] = aVar.f10662d;
        objArr[2] = aVar.f10659a;
        objArr[3] = aVar.f10664f;
        objArr[4] = aVar.f10663e;
        Map<String, String> map2 = aVar.f10660b;
        objArr[5] = Integer.valueOf(map2 == null ? 0 : map2.size());
        C4888x.m16982c("anr tm:%d\ntr:%s\nproc:%s\nsMsg:%s\n lMsg:%s\n threads:%d", objArr);
        if (!this.f10671f.mo26719b()) {
            C4888x.m16984e("crash report sync remote fail, will not upload to Bugly , print local for helpful!", new Object[0]);
            C4822b.m16639a("ANR", C4893z.m17008a(), aVar.f10659a, "main", aVar.f10663e, null);
            return false;
        } else if (!this.f10671f.mo26720c().f10563j) {
            C4888x.m16983d("ANR Report is closed!", new Object[0]);
            return false;
        } else {
            C4888x.m16977a("found visiable anr , start to upload!", new Object[0]);
            CrashDetailBean a = m16617a(aVar);
            if (a == null) {
                C4888x.m16984e("pack anr fail!", new Object[0]);
                return false;
            }
            C4824c.m16656a().mo26756a(a);
            if (a.f10618a >= 0) {
                C4888x.m16977a("backup anr record success!", new Object[0]);
            } else {
                C4888x.m16983d("backup anr record fail!", new Object[0]);
            }
            if (str != null && new File(str).exists()) {
                this.f10666a.set(3);
                if (m16619a(str, aVar.f10662d, aVar.f10659a)) {
                    C4888x.m16977a("backup trace success", new Object[0]);
                }
            }
            C4822b.m16639a("ANR", C4893z.m17008a(), aVar.f10659a, "main", aVar.f10663e, a);
            if (!this.f10673h.mo26749a(a)) {
                this.f10673h.mo26747a(a, 3000L, true);
            }
            this.f10673h.mo26751b(a);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.z.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.z.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.z.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.z.a(byte[], int):byte[]
      com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("read trace first dump for create time!", new java.lang.Object[0]);
        r0 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readFirstDumpInfo(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 == null) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        r5 = r0.f10652c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r5 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r5 != -1) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("trace dump fail could not get time!", new java.lang.Object[0]);
        r5 = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        r7 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (java.lang.Math.abs(r7 - r10.f10667b) >= 10000) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004b, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("should not process ANR too Fre in %d", 10000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r10.f10667b = r7;
        r10.f10666a.set(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r9 = com.tencent.bugly.proguard.C4893z.m17017a(com.tencent.bugly.crashreport.crash.C4824c.f10694f, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006d, code lost:
        if (r9 == null) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0073, code lost:
        if (r9.size() > 0) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0076, code lost:
        r6 = m16616a(r10.f10668c, 10000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007c, code lost:
        if (r6 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007e, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("proc state is unvisiable!", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008c, code lost:
        if (r6.pid == android.os.Process.myPid()) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008e, code lost:
        com.tencent.bugly.proguard.C4888x.m16982c("not mind proc!", r6.processName);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        com.tencent.bugly.proguard.C4888x.m16977a("found visiable anr , start to process!", new java.lang.Object[0]);
        m16618a(r10.f10668c, r11, r6, r7, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a9, code lost:
        com.tencent.bugly.proguard.C4888x.m16983d("can't get all thread skip this anr", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b1, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b2, code lost:
        com.tencent.bugly.proguard.C4888x.m16978a(r11);
        com.tencent.bugly.proguard.C4888x.m16984e("get all thread stack fail!", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bd, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c2, code lost:
        if (com.tencent.bugly.proguard.C4888x.m16978a(r11) == false) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c4, code lost:
        r11.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c7, code lost:
        com.tencent.bugly.proguard.C4888x.m16984e("handle anr error %s", r11.getClass().toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d9, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00da, code lost:
        r10.f10666a.set(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00df, code lost:
        throw r11;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo26737a(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f10666a     // Catch:{ all -> 0x00e0 }
            int r0 = r0.get()     // Catch:{ all -> 0x00e0 }
            r1 = 0
            if (r0 == 0) goto L_0x0013
            java.lang.String r11 = "trace started return "
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00e0 }
            com.tencent.bugly.proguard.C4888x.m16982c(r11, r0)     // Catch:{ all -> 0x00e0 }
            monitor-exit(r10)     // Catch:{ all -> 0x00e0 }
            return
        L_0x0013:
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f10666a     // Catch:{ all -> 0x00e0 }
            r2 = 1
            r0.set(r2)     // Catch:{ all -> 0x00e0 }
            monitor-exit(r10)     // Catch:{ all -> 0x00e0 }
            java.lang.String r0 = "read trace first dump for create time!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r3)     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.crashreport.crash.anr.TraceFileHelper$a r0 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readFirstDumpInfo(r11, r1)     // Catch:{ all -> 0x00bd }
            r3 = -1
            if (r0 == 0) goto L_0x002c
            long r5 = r0.f10652c     // Catch:{ all -> 0x00bd }
            goto L_0x002d
        L_0x002c:
            r5 = r3
        L_0x002d:
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "trace dump fail could not get time!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r3)     // Catch:{ all -> 0x00bd }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00bd }
        L_0x003c:
            r7 = r5
            long r3 = r10.f10667b     // Catch:{ all -> 0x00bd }
            long r3 = r7 - r3
            long r3 = java.lang.Math.abs(r3)     // Catch:{ all -> 0x00bd }
            r5 = 10000(0x2710, double:4.9407E-320)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0060
            java.lang.String r11 = "should not process ANR too Fre in %d"
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ all -> 0x00bd }
            r3 = 10000(0x2710, float:1.4013E-41)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00bd }
            r0[r1] = r3     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r0)     // Catch:{ all -> 0x00bd }
        L_0x005a:
            java.util.concurrent.atomic.AtomicInteger r11 = r10.f10666a
            r11.set(r1)
            return
        L_0x0060:
            r10.f10667b = r7     // Catch:{ all -> 0x00bd }
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f10666a     // Catch:{ all -> 0x00bd }
            r0.set(r2)     // Catch:{ all -> 0x00bd }
            int r0 = com.tencent.bugly.crashreport.crash.C4824c.f10694f     // Catch:{ all -> 0x00b1 }
            java.util.Map r9 = com.tencent.bugly.proguard.C4893z.m17017a(r0, r1)     // Catch:{ all -> 0x00b1 }
            if (r9 == 0) goto L_0x00a9
            int r0 = r9.size()     // Catch:{ all -> 0x00bd }
            if (r0 > 0) goto L_0x0076
            goto L_0x00a9
        L_0x0076:
            android.content.Context r0 = r10.f10668c     // Catch:{ all -> 0x00bd }
            android.app.ActivityManager$ProcessErrorStateInfo r6 = m16616a(r0, r5)     // Catch:{ all -> 0x00bd }
            if (r6 != 0) goto L_0x0086
            java.lang.String r11 = "proc state is unvisiable!"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16982c(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x0086:
            int r0 = r6.pid     // Catch:{ all -> 0x00bd }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x00bd }
            if (r0 == r3) goto L_0x009a
            java.lang.String r11 = "not mind proc!"
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ all -> 0x00bd }
            java.lang.String r3 = r6.processName     // Catch:{ all -> 0x00bd }
            r0[r1] = r3     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16982c(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x009a:
            java.lang.String r0 = "found visiable anr , start to process!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r3)     // Catch:{ all -> 0x00bd }
            android.content.Context r4 = r10.f10668c     // Catch:{ all -> 0x00bd }
            r3 = r10
            r5 = r11
            r3.m16618a(r4, r5, r6, r7, r9)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x00a9:
            java.lang.String r11 = "can't get all thread skip this anr"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x00b1:
            r11 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r11)     // Catch:{ all -> 0x00bd }
            java.lang.String r11 = "get all thread stack fail!"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C4888x.m16984e(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x00bd:
            r11 = move-exception
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r11)     // Catch:{ all -> 0x00d9 }
            if (r0 != 0) goto L_0x00c7
            r11.printStackTrace()     // Catch:{ all -> 0x00d9 }
        L_0x00c7:
            java.lang.String r0 = "handle anr error %s"
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d9 }
            java.lang.Class r11 = r11.getClass()     // Catch:{ all -> 0x00d9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00d9 }
            r2[r1] = r11     // Catch:{ all -> 0x00d9 }
            com.tencent.bugly.proguard.C4888x.m16984e(r0, r2)     // Catch:{ all -> 0x00d9 }
            goto L_0x005a
        L_0x00d9:
            r11 = move-exception
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f10666a
            r0.set(r1)
            throw r11
        L_0x00e0:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C4819b.mo26737a(java.lang.String):void");
    }

    /* renamed from: a */
    public final void mo26738a(boolean z) {
        m16621c(z);
        boolean h = m16625h();
        C4809a a = C4809a.m16589a();
        if (a != null) {
            h = h && a.mo26720c().f10560g;
        }
        if (h != m16624g()) {
            C4888x.m16977a("anr changed to %b", Boolean.valueOf(h));
            m16620b(h);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0048, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void mo26736a(com.tencent.bugly.crashreport.common.strategy.StrategyBean r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r6 != 0) goto L_0x0005
            monitor-exit(r5)
            return
        L_0x0005:
            boolean r0 = r6.f10563j     // Catch:{ all -> 0x0057 }
            boolean r1 = r5.m16624g()     // Catch:{ all -> 0x0057 }
            r2 = 0
            r3 = 1
            if (r0 == r1) goto L_0x001e
            java.lang.String r0 = "server anr changed to %b"
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x0057 }
            boolean r4 = r6.f10563j     // Catch:{ all -> 0x0057 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0057 }
            r1[r2] = r4     // Catch:{ all -> 0x0057 }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r1)     // Catch:{ all -> 0x0057 }
        L_0x001e:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0057 }
            r1 = 19
            if (r0 > r1) goto L_0x0049
            boolean r6 = r6.f10563j     // Catch:{ all -> 0x0057 }
            if (r6 == 0) goto L_0x0030
            boolean r6 = r5.m16625h()     // Catch:{ all -> 0x0057 }
            if (r6 == 0) goto L_0x0030
            r6 = 1
            goto L_0x0031
        L_0x0030:
            r6 = 0
        L_0x0031:
            boolean r0 = r5.m16624g()     // Catch:{ all -> 0x0057 }
            if (r6 == r0) goto L_0x0047
            java.lang.String r0 = "anr changed to %b"
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x0057 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x0057 }
            r1[r2] = r3     // Catch:{ all -> 0x0057 }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r1)     // Catch:{ all -> 0x0057 }
            r5.m16620b(r6)     // Catch:{ all -> 0x0057 }
        L_0x0047:
            monitor-exit(r5)
            return
        L_0x0049:
            boolean r6 = r6.f10563j     // Catch:{ all -> 0x0057 }
            if (r6 == 0) goto L_0x0052
            r5.mo26742c()     // Catch:{ all -> 0x0057 }
            monitor-exit(r5)
            return
        L_0x0052:
            r5.mo26743d()     // Catch:{ all -> 0x0057 }
            monitor-exit(r5)
            return
        L_0x0057:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C4819b.mo26736a(com.tencent.bugly.crashreport.common.strategy.StrategyBean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.z.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.z.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.z.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.z.a(byte[], int):byte[]
      com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* renamed from: a */
    public final boolean mo26740a(Thread thread) {
        new HashMap();
        if (thread.getName().contains("main")) {
            ActivityManager.ProcessErrorStateInfo a = m16616a(this.f10668c, 10000);
            if (a == null) {
                C4888x.m16982c("anr handler onThreadBlock proc state is unvisiable!", new Object[0]);
                return false;
            } else if (a.pid != Process.myPid()) {
                C4888x.m16982c("onThreadBlock not mind proc!", a.processName);
                return false;
            } else {
                try {
                    Map<String, String> a2 = C4893z.m17017a(200000, false);
                    C4888x.m16977a("onThreadBlock found visiable anr , start to process!", new Object[0]);
                    m16618a(this.f10668c, "", a, System.currentTimeMillis(), a2);
                } catch (Throwable unused) {
                    return false;
                }
            }
        } else {
            C4888x.m16982c("anr handler onThreadBlock only care main thread", new Object[0]);
        }
        return true;
    }
}
