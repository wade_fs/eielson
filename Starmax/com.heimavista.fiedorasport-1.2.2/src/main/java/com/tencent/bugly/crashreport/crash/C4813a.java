package com.tencent.bugly.crashreport.crash;

/* renamed from: com.tencent.bugly.crashreport.crash.a */
/* compiled from: BUGLY */
public final class C4813a implements Comparable<C4813a> {

    /* renamed from: a */
    public long f10644a = -1;

    /* renamed from: b */
    public long f10645b = -1;

    /* renamed from: c */
    public String f10646c = null;

    /* renamed from: d */
    public boolean f10647d = false;

    /* renamed from: e */
    public boolean f10648e = false;

    /* renamed from: f */
    public int f10649f = 0;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        int i;
        C4813a aVar = (C4813a) obj;
        if (aVar == null || this.f10645b - aVar.f10645b > 0) {
            return 1;
        }
        return i < 0 ? -1 : 0;
    }
}
