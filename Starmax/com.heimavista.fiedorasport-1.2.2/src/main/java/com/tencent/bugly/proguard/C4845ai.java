package com.tencent.bugly.proguard;

import com.google.android.exoplayer2.C1750C;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

/* renamed from: com.tencent.bugly.proguard.ai */
/* compiled from: BUGLY */
public final class C4845ai implements C4846aj {

    /* renamed from: a */
    private String f10797a = null;

    /* renamed from: a */
    public final byte[] mo26837a(byte[] bArr) {
        if (this.f10797a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(2, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f10797a.getBytes(C1750C.UTF8_NAME))), new IvParameterSpec(this.f10797a.getBytes(C1750C.UTF8_NAME)));
        return instance.doFinal(bArr);
    }

    /* renamed from: b */
    public final byte[] mo26838b(byte[] bArr) {
        if (this.f10797a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(1, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f10797a.getBytes(C1750C.UTF8_NAME))), new IvParameterSpec(this.f10797a.getBytes(C1750C.UTF8_NAME)));
        return instance.doFinal(bArr);
    }

    /* renamed from: a */
    public final void mo26836a(String str) {
        if (str != null) {
            this.f10797a = str;
        }
    }
}
