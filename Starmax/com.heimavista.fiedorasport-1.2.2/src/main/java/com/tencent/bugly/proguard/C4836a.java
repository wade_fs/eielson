package com.tencent.bugly.proguard;

import android.text.TextUtils;
import com.tencent.bugly.crashreport.biz.UserInfoBean;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: com.tencent.bugly.proguard.a */
/* compiled from: BUGLY */
public class C4836a {

    /* renamed from: e */
    private static Proxy f10782e;

    /* renamed from: a */
    protected HashMap<String, HashMap<String, byte[]>> f10783a = new HashMap<>();

    /* renamed from: b */
    protected String f10784b;

    /* renamed from: c */
    C4865i f10785c;

    /* renamed from: d */
    private HashMap<String, Object> f10786d;

    C4836a() {
        new HashMap();
        this.f10786d = new HashMap<>();
        this.f10784b = "GBK";
        this.f10785c = new C4865i();
    }

    /* renamed from: a */
    public static C4846aj m16719a(int i) {
        if (i == 1) {
            return new C4845ai();
        }
        if (i == 3) {
            return new C4844ah();
        }
        return null;
    }

    /* renamed from: b */
    public static Proxy m16731b() {
        return f10782e;
    }

    /* renamed from: a */
    public static void m16726a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            f10782e = null;
        } else {
            f10782e = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, i));
        }
    }

    /* renamed from: a */
    public static void m16727a(InetAddress inetAddress, int i) {
        if (inetAddress == null) {
            f10782e = null;
        } else {
            f10782e = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(inetAddress, i));
        }
    }

    /* renamed from: a */
    public void mo26815a(String str) {
        this.f10784b = str;
    }

    /* renamed from: a */
    public static C4856at m16722a(UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            return null;
        }
        C4856at atVar = new C4856at();
        atVar.f10895a = userInfoBean.f10424e;
        atVar.f10899e = userInfoBean.f10429j;
        atVar.f10898d = userInfoBean.f10422c;
        atVar.f10897c = userInfoBean.f10423d;
        atVar.f10901g = C4806a.m16492b().mo26694i();
        atVar.f10902h = userInfoBean.f10434o == 1;
        int i = userInfoBean.f10421b;
        if (i == 1) {
            atVar.f10896b = 1;
        } else if (i == 2) {
            atVar.f10896b = 4;
        } else if (i == 3) {
            atVar.f10896b = 2;
        } else if (i == 4) {
            atVar.f10896b = 3;
        } else if (i < 10 || i >= 20) {
            C4888x.m16984e("unknown uinfo type %d ", Integer.valueOf(userInfoBean.f10421b));
            return null;
        } else {
            atVar.f10896b = (byte) i;
        }
        atVar.f10900f = new HashMap();
        if (userInfoBean.f10435p >= 0) {
            Map<String, String> map = atVar.f10900f;
            StringBuilder sb = new StringBuilder();
            sb.append(userInfoBean.f10435p);
            map.put("C01", sb.toString());
        }
        if (userInfoBean.f10436q >= 0) {
            Map<String, String> map2 = atVar.f10900f;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(userInfoBean.f10436q);
            map2.put("C02", sb2.toString());
        }
        Map<String, String> map3 = userInfoBean.f10437r;
        if (map3 != null && map3.size() > 0) {
            for (Map.Entry entry : userInfoBean.f10437r.entrySet()) {
                Map<String, String> map4 = atVar.f10900f;
                map4.put("C03_" + ((String) entry.getKey()), entry.getValue());
            }
        }
        Map<String, String> map5 = userInfoBean.f10438s;
        if (map5 != null && map5.size() > 0) {
            for (Map.Entry entry2 : userInfoBean.f10438s.entrySet()) {
                Map<String, String> map6 = atVar.f10900f;
                map6.put("C04_" + ((String) entry2.getKey()), entry2.getValue());
            }
        }
        Map<String, String> map7 = atVar.f10900f;
        StringBuilder sb3 = new StringBuilder();
        sb3.append(!userInfoBean.f10431l);
        map7.put("A36", sb3.toString());
        Map<String, String> map8 = atVar.f10900f;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(userInfoBean.f10426g);
        map8.put("F02", sb4.toString());
        Map<String, String> map9 = atVar.f10900f;
        StringBuilder sb5 = new StringBuilder();
        sb5.append(userInfoBean.f10427h);
        map9.put("F03", sb5.toString());
        Map<String, String> map10 = atVar.f10900f;
        map10.put("F04", userInfoBean.f10429j);
        Map<String, String> map11 = atVar.f10900f;
        StringBuilder sb6 = new StringBuilder();
        sb6.append(userInfoBean.f10428i);
        map11.put("F05", sb6.toString());
        Map<String, String> map12 = atVar.f10900f;
        map12.put("F06", userInfoBean.f10432m);
        Map<String, String> map13 = atVar.f10900f;
        StringBuilder sb7 = new StringBuilder();
        sb7.append(userInfoBean.f10430k);
        map13.put("F10", sb7.toString());
        C4888x.m16982c("summary type %d vm:%d", Byte.valueOf(atVar.f10896b), Integer.valueOf(atVar.f10900f.size()));
        return atVar;
    }

    /* renamed from: a */
    public static String m16725a(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            String str = "map";
            if (i < arrayList.size()) {
                String str2 = arrayList.get(i);
                if (str2.equals("java.lang.Integer") || str2.equals("int")) {
                    str = "int32";
                } else if (str2.equals("java.lang.Boolean") || str2.equals("boolean")) {
                    str = "bool";
                } else if (str2.equals("java.lang.Byte") || str2.equals("byte")) {
                    str = "char";
                } else if (str2.equals("java.lang.Double") || str2.equals("double")) {
                    str = "double";
                } else if (str2.equals("java.lang.Float") || str2.equals("float")) {
                    str = "float";
                } else if (str2.equals("java.lang.Long") || str2.equals("long")) {
                    str = "int64";
                } else if (str2.equals("java.lang.Short") || str2.equals("short")) {
                    str = "short";
                } else if (str2.equals("java.lang.Character")) {
                    throw new IllegalArgumentException("can not support java.lang.Character");
                } else if (str2.equals("java.lang.String")) {
                    str = "string";
                } else if (str2.equals("java.util.List")) {
                    str = "list";
                } else if (!str2.equals("java.util.Map")) {
                    str = str2;
                }
                arrayList.set(i, str);
                i++;
            } else {
                Collections.reverse(arrayList);
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    String str3 = arrayList.get(i2);
                    if (str3.equals("list")) {
                        int i3 = i2 - 1;
                        arrayList.set(i3, "<" + arrayList.get(i3));
                        arrayList.set(0, arrayList.get(0) + ">");
                    } else if (str3.equals(str)) {
                        int i4 = i2 - 1;
                        arrayList.set(i4, "<" + arrayList.get(i4) + ",");
                        arrayList.set(0, arrayList.get(0) + ">");
                    } else if (str3.equals("Array")) {
                        int i5 = i2 - 1;
                        arrayList.set(i5, "<" + arrayList.get(i5));
                        arrayList.set(0, arrayList.get(0) + ">");
                    }
                }
                Collections.reverse(arrayList);
                Iterator<String> it = arrayList.iterator();
                while (it.hasNext()) {
                    stringBuffer.append(it.next());
                }
                return stringBuffer.toString();
            }
        }
    }

    /* renamed from: a */
    public <T> void mo26816a(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (obj == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (!(obj instanceof Set)) {
            C4867j jVar = new C4867j();
            jVar.mo26873a(this.f10784b);
            jVar.mo26879a(obj, 0);
            byte[] a = C4869l.m16876a(jVar.mo26874a());
            HashMap hashMap = new HashMap(1);
            ArrayList arrayList = new ArrayList(1);
            m16728a(arrayList, obj);
            hashMap.put(m16725a((ArrayList<String>) arrayList), a);
            this.f10786d.remove(str);
            this.f10783a.put(str, hashMap);
        } else {
            throw new IllegalArgumentException("can not support Set");
        }
    }

    /* renamed from: a */
    public static C4857au m16723a(List<UserInfoBean> list, int i) {
        C4806a b;
        if (list == null || list.size() == 0 || (b = C4806a.m16492b()) == null) {
            return null;
        }
        b.mo26705t();
        C4857au auVar = new C4857au();
        auVar.f10906b = b.f10526d;
        auVar.f10907c = b.mo26693h();
        ArrayList<C4856at> arrayList = new ArrayList<>();
        for (UserInfoBean userInfoBean : list) {
            C4856at a = m16722a(userInfoBean);
            if (a != null) {
                arrayList.add(a);
            }
        }
        auVar.f10908d = arrayList;
        auVar.f10909e = new HashMap();
        Map<String, String> map = auVar.f10909e;
        map.put("A7", b.f10528f);
        Map<String, String> map2 = auVar.f10909e;
        map2.put("A6", b.mo26704s());
        Map<String, String> map3 = auVar.f10909e;
        map3.put("A5", b.mo26703r());
        Map<String, String> map4 = auVar.f10909e;
        StringBuilder sb = new StringBuilder();
        sb.append(b.mo26701p());
        map4.put("A2", sb.toString());
        Map<String, String> map5 = auVar.f10909e;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(b.mo26701p());
        map5.put("A1", sb2.toString());
        Map<String, String> map6 = auVar.f10909e;
        map6.put("A24", b.f10530h);
        Map<String, String> map7 = auVar.f10909e;
        StringBuilder sb3 = new StringBuilder();
        sb3.append(b.mo26702q());
        map7.put("A17", sb3.toString());
        Map<String, String> map8 = auVar.f10909e;
        map8.put("A15", b.mo26708w());
        Map<String, String> map9 = auVar.f10909e;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(b.mo26709x());
        map9.put("A13", sb4.toString());
        Map<String, String> map10 = auVar.f10909e;
        map10.put("F08", b.f10544v);
        Map<String, String> map11 = auVar.f10909e;
        map11.put("F09", b.f10545w);
        Map<String, String> G = b.mo26661G();
        if (G != null && G.size() > 0) {
            for (Map.Entry entry : G.entrySet()) {
                Map<String, String> map12 = auVar.f10909e;
                map12.put("C04_" + ((String) entry.getKey()), entry.getValue());
            }
        }
        if (i == 1) {
            auVar.f10905a = 1;
        } else if (i != 2) {
            C4888x.m16984e("unknown up type %d ", Integer.valueOf(i));
            return null;
        } else {
            auVar.f10905a = 2;
        }
        return auVar;
    }

    /* renamed from: a */
    public static <T extends C4868k> T m16724a(byte[] bArr, Class cls) {
        if (bArr != null && bArr.length > 0) {
            try {
                T t = (C4868k) cls.newInstance();
                C4865i iVar = new C4865i(bArr);
                iVar.mo26863a("utf-8");
                t.mo26839a(iVar);
                return t;
            } catch (Throwable th) {
                if (!C4888x.m16981b(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x02c4, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x02c9, code lost:
        if (com.tencent.bugly.proguard.C4888x.m16981b(r9) == false) goto L_0x02cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02cb, code lost:
        r9.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x02ce, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.bugly.proguard.C4852ap m16720a(android.content.Context r9, int r10, byte[] r11) {
        /*
            com.tencent.bugly.crashreport.common.info.a r0 = com.tencent.bugly.crashreport.common.info.C4806a.m16492b()
            com.tencent.bugly.crashreport.common.strategy.a r1 = com.tencent.bugly.crashreport.common.strategy.C4809a.m16589a()
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r1 = r1.mo26720c()
            r2 = 0
            r3 = 0
            if (r0 == 0) goto L_0x02cf
            if (r1 != 0) goto L_0x0014
            goto L_0x02cf
        L_0x0014:
            com.tencent.bugly.proguard.ap r4 = new com.tencent.bugly.proguard.ap     // Catch:{ all -> 0x02c4 }
            r4.<init>()     // Catch:{ all -> 0x02c4 }
            monitor-enter(r0)     // Catch:{ all -> 0x02c4 }
            r5 = 1
            r4.f10843a = r5     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r0.mo26689f()     // Catch:{ all -> 0x02c1 }
            r4.f10844b = r6     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r0.f10525c     // Catch:{ all -> 0x02c1 }
            r4.f10845c = r6     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r0.f10532j     // Catch:{ all -> 0x02c1 }
            r4.f10846d = r6     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r0.f10534l     // Catch:{ all -> 0x02c1 }
            r4.f10847e = r6     // Catch:{ all -> 0x02c1 }
            r0.getClass()     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = "3.1.0"
            r4.f10848f = r6     // Catch:{ all -> 0x02c1 }
            r4.f10849g = r10     // Catch:{ all -> 0x02c1 }
            if (r11 != 0) goto L_0x0041
            java.lang.String r10 = ""
            byte[] r10 = r10.getBytes()     // Catch:{ all -> 0x02c1 }
            goto L_0x0042
        L_0x0041:
            r10 = r11
        L_0x0042:
            r4.f10850h = r10     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.f10529g     // Catch:{ all -> 0x02c1 }
            r4.f10851i = r10     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.f10530h     // Catch:{ all -> 0x02c1 }
            r4.f10852j = r10     // Catch:{ all -> 0x02c1 }
            java.util.HashMap r10 = new java.util.HashMap     // Catch:{ all -> 0x02c1 }
            r10.<init>()     // Catch:{ all -> 0x02c1 }
            r4.f10853k = r10     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.mo26687e()     // Catch:{ all -> 0x02c1 }
            r4.f10854l = r10     // Catch:{ all -> 0x02c1 }
            long r6 = r1.f10569p     // Catch:{ all -> 0x02c1 }
            r4.f10855m = r6     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.mo26693h()     // Catch:{ all -> 0x02c1 }
            r4.f10857o = r10     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = com.tencent.bugly.crashreport.common.info.C4807b.m16557c(r9)     // Catch:{ all -> 0x02c1 }
            r4.f10858p = r9     // Catch:{ all -> 0x02c1 }
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x02c1 }
            r4.f10859q = r9     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r9.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.mo26696k()     // Catch:{ all -> 0x02c1 }
            r9.append(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x02c1 }
            r4.f10860r = r9     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r0.mo26695j()     // Catch:{ all -> 0x02c1 }
            r4.f10861s = r9     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r9.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.mo26698m()     // Catch:{ all -> 0x02c1 }
            r9.append(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x02c1 }
            r4.f10862t = r9     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r0.mo26697l()     // Catch:{ all -> 0x02c1 }
            r4.f10863u = r9     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r9.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r0.mo26699n()     // Catch:{ all -> 0x02c1 }
            r9.append(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x02c1 }
            r4.f10864v = r9     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = r4.f10858p     // Catch:{ all -> 0x02c1 }
            r4.f10865w = r9     // Catch:{ all -> 0x02c1 }
            r0.getClass()     // Catch:{ all -> 0x02c1 }
            java.lang.String r9 = "com.tencent.bugly"
            r4.f10856n = r9     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A26"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26710y()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A60"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26711z()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A61"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26655A()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A62"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            boolean r7 = r0.mo26671R()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A63"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            boolean r7 = r0.mo26672S()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "F11"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            boolean r7 = r0.f10548z     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "F12"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            boolean r7 = r0.f10547y     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G1"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26706u()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "A64"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26673T()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            boolean r9 = r0.f10473B     // Catch:{ all -> 0x02c1 }
            if (r9 == 0) goto L_0x0219
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G2"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26665L()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G3"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26666M()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G4"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26667N()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G5"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26668O()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G6"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.mo26669P()     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G7"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            long r7 = r0.mo26670Q()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = java.lang.Long.toString(r7)     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
        L_0x0219:
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "D3"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r6.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r0.f10533k     // Catch:{ all -> 0x02c1 }
            r6.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.List<com.tencent.bugly.a> r9 = com.tencent.bugly.C4792b.f10413b     // Catch:{ all -> 0x02c1 }
            if (r9 == 0) goto L_0x0256
            java.util.List<com.tencent.bugly.a> r9 = com.tencent.bugly.C4792b.f10413b     // Catch:{ all -> 0x02c1 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x02c1 }
        L_0x0238:
            boolean r10 = r9.hasNext()     // Catch:{ all -> 0x02c1 }
            if (r10 == 0) goto L_0x0256
            java.lang.Object r10 = r9.next()     // Catch:{ all -> 0x02c1 }
            com.tencent.bugly.a r10 = (com.tencent.bugly.BUGLY) r10     // Catch:{ all -> 0x02c1 }
            java.lang.String r6 = r10.versionKey     // Catch:{ all -> 0x02c1 }
            if (r6 == 0) goto L_0x0238
            java.lang.String r6 = r10.version     // Catch:{ all -> 0x02c1 }
            if (r6 == 0) goto L_0x0238
            java.util.Map<java.lang.String, java.lang.String> r6 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = r10.versionKey     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r10.version     // Catch:{ all -> 0x02c1 }
            r6.put(r7, r10)     // Catch:{ all -> 0x02c1 }
            goto L_0x0238
        L_0x0256:
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "G15"
            java.lang.String r6 = "G15"
            java.lang.String r7 = ""
            java.lang.String r6 = com.tencent.bugly.proguard.C4893z.m17033b(r6, r7)     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            java.util.Map<java.lang.String, java.lang.String> r9 = r4.f10853k     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "D4"
            java.lang.String r6 = "D4"
            java.lang.String r7 = "0"
            java.lang.String r6 = com.tencent.bugly.proguard.C4893z.m17033b(r6, r7)     // Catch:{ all -> 0x02c1 }
            r9.put(r10, r6)     // Catch:{ all -> 0x02c1 }
            monitor-exit(r0)     // Catch:{ all -> 0x02c1 }
            com.tencent.bugly.proguard.u r9 = com.tencent.bugly.proguard.C4881u.m16932a()     // Catch:{ all -> 0x02c4 }
            if (r9 == 0) goto L_0x0298
            boolean r9 = r9.f10992a     // Catch:{ all -> 0x02c4 }
            if (r9 != 0) goto L_0x0298
            if (r11 == 0) goto L_0x0298
            byte[] r9 = r4.f10850h     // Catch:{ all -> 0x02c4 }
            r10 = 2
            java.lang.String r11 = r1.f10574u     // Catch:{ all -> 0x02c4 }
            byte[] r9 = com.tencent.bugly.proguard.C4893z.m17030a(r9, r10, r5, r11)     // Catch:{ all -> 0x02c4 }
            r4.f10850h = r9     // Catch:{ all -> 0x02c4 }
            byte[] r9 = r4.f10850h     // Catch:{ all -> 0x02c4 }
            if (r9 != 0) goto L_0x0298
            java.lang.String r9 = "reqPkg sbuffer error!"
            java.lang.Object[] r10 = new java.lang.Object[r2]     // Catch:{ all -> 0x02c4 }
            com.tencent.bugly.proguard.C4888x.m16984e(r9, r10)     // Catch:{ all -> 0x02c4 }
            return r3
        L_0x0298:
            java.util.Map r9 = r0.mo26660F()     // Catch:{ all -> 0x02c4 }
            if (r9 == 0) goto L_0x02c0
            java.util.Set r9 = r9.entrySet()     // Catch:{ all -> 0x02c4 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x02c4 }
        L_0x02a6:
            boolean r10 = r9.hasNext()     // Catch:{ all -> 0x02c4 }
            if (r10 == 0) goto L_0x02c0
            java.lang.Object r10 = r9.next()     // Catch:{ all -> 0x02c4 }
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10     // Catch:{ all -> 0x02c4 }
            java.util.Map<java.lang.String, java.lang.String> r11 = r4.f10853k     // Catch:{ all -> 0x02c4 }
            java.lang.Object r0 = r10.getKey()     // Catch:{ all -> 0x02c4 }
            java.lang.Object r10 = r10.getValue()     // Catch:{ all -> 0x02c4 }
            r11.put(r0, r10)     // Catch:{ all -> 0x02c4 }
            goto L_0x02a6
        L_0x02c0:
            return r4
        L_0x02c1:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x02c4 }
            throw r9     // Catch:{ all -> 0x02c4 }
        L_0x02c4:
            r9 = move-exception
            boolean r10 = com.tencent.bugly.proguard.C4888x.m16981b(r9)
            if (r10 != 0) goto L_0x02ce
            r9.printStackTrace()
        L_0x02ce:
            return r3
        L_0x02cf:
            java.lang.Object[] r9 = new java.lang.Object[r2]
            java.lang.String r10 = "Can not create request pkg for parameters is invalid."
            com.tencent.bugly.proguard.C4888x.m16984e(r10, r9)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4836a.m16720a(android.content.Context, int, byte[]):com.tencent.bugly.proguard.ap");
    }

    /* renamed from: a */
    private void m16728a(ArrayList<String> arrayList, Object obj) {
        if (obj.getClass().isArray()) {
            if (!obj.getClass().getComponentType().toString().equals("byte")) {
                throw new IllegalArgumentException("only byte[] is supported");
            } else if (Array.getLength(obj) > 0) {
                arrayList.add("java.util.List");
                m16728a(arrayList, Array.get(obj, 0));
            } else {
                arrayList.add("Array");
                arrayList.add("?");
            }
        } else if (obj instanceof Array) {
            throw new IllegalArgumentException("can not support Array, please use List");
        } else if (obj instanceof List) {
            arrayList.add("java.util.List");
            List list = (List) obj;
            if (list.size() > 0) {
                m16728a(arrayList, list.get(0));
            } else {
                arrayList.add("?");
            }
        } else if (obj instanceof Map) {
            arrayList.add("java.util.Map");
            Map map = (Map) obj;
            if (map.size() > 0) {
                Object next = map.keySet().iterator().next();
                Object obj2 = map.get(next);
                arrayList.add(next.getClass().getName());
                m16728a(arrayList, obj2);
                return;
            }
            arrayList.add("?");
            arrayList.add("?");
        } else {
            arrayList.add(obj.getClass().getName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public byte[] mo26818a() {
        C4867j jVar = new C4867j(0);
        jVar.mo26873a(this.f10784b);
        jVar.mo26882a((Map) this.f10783a, 0);
        return C4869l.m16876a(jVar.mo26874a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* renamed from: a */
    public void mo26817a(byte[] bArr) {
        this.f10785c.mo26869a(bArr);
        this.f10785c.mo26863a(this.f10784b);
        HashMap hashMap = new HashMap(1);
        HashMap hashMap2 = new HashMap(1);
        hashMap2.put("", new byte[0]);
        hashMap.put("", hashMap2);
        this.f10783a = this.f10785c.mo26867a((Map) hashMap, 0, false);
    }

    /* renamed from: a */
    public static byte[] m16730a(Object obj) {
        try {
            C4860d dVar = new C4860d();
            dVar.mo26846c();
            dVar.mo26815a("utf-8");
            dVar.mo26847b(1);
            dVar.mo26848b("RqdServer");
            dVar.mo26849c("sync");
            dVar.mo26816a("detail", obj);
            return dVar.mo26818a();
        } catch (Throwable th) {
            if (C4888x.m16981b(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static C4853aq m16721a(byte[] bArr, boolean z) {
        if (bArr != null) {
            try {
                C4860d dVar = new C4860d();
                dVar.mo26846c();
                dVar.mo26815a("utf-8");
                dVar.mo26817a(bArr);
                Object b = dVar.mo26845b("detail", new C4853aq());
                C4853aq cast = C4853aq.class.isInstance(b) ? C4853aq.class.cast(b) : null;
                if (!z && cast != null && cast.f10871c != null && cast.f10871c.length > 0) {
                    C4888x.m16982c("resp buf %d", Integer.valueOf(cast.f10871c.length));
                    cast.f10871c = C4893z.m17043b(cast.f10871c, 2, 1, StrategyBean.f10557d);
                    if (cast.f10871c == null) {
                        C4888x.m16984e("resp sbuffer error!", new Object[0]);
                        return null;
                    }
                }
                return cast;
            } catch (Throwable th) {
                if (!C4888x.m16981b(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public static byte[] m16729a(C4868k kVar) {
        try {
            C4867j jVar = new C4867j();
            jVar.mo26873a("utf-8");
            kVar.mo26840a(jVar);
            return jVar.mo26886b();
        } catch (Throwable th) {
            if (C4888x.m16981b(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }
}
