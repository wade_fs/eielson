package com.tencent.bugly.crashreport.crash;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.crashreport.common.info.PlugInBean;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;
import java.util.UUID;

/* compiled from: BUGLY */
public class CrashDetailBean implements Parcelable, Comparable<CrashDetailBean> {
    public static final Parcelable.Creator<CrashDetailBean> CREATOR = new C4812a();

    /* renamed from: A */
    public String f10595A = "";

    /* renamed from: B */
    public String f10596B = "";

    /* renamed from: C */
    public long f10597C = -1;

    /* renamed from: D */
    public long f10598D = -1;

    /* renamed from: E */
    public long f10599E = -1;

    /* renamed from: F */
    public long f10600F = -1;

    /* renamed from: G */
    public long f10601G = -1;

    /* renamed from: H */
    public long f10602H = -1;

    /* renamed from: I */
    public String f10603I = "";

    /* renamed from: J */
    public String f10604J = "";

    /* renamed from: K */
    public String f10605K = "";

    /* renamed from: L */
    public String f10606L = "";

    /* renamed from: M */
    public long f10607M = -1;

    /* renamed from: N */
    public boolean f10608N = false;

    /* renamed from: O */
    public Map<String, String> f10609O = null;

    /* renamed from: P */
    public int f10610P = -1;

    /* renamed from: Q */
    public int f10611Q = -1;

    /* renamed from: R */
    public Map<String, String> f10612R = null;

    /* renamed from: S */
    public Map<String, String> f10613S = null;

    /* renamed from: T */
    public byte[] f10614T = null;

    /* renamed from: U */
    public String f10615U = null;

    /* renamed from: V */
    public String f10616V = null;

    /* renamed from: W */
    private String f10617W = "";

    /* renamed from: a */
    public long f10618a = -1;

    /* renamed from: b */
    public int f10619b = 0;

    /* renamed from: c */
    public String f10620c = UUID.randomUUID().toString();

    /* renamed from: d */
    public boolean f10621d = false;

    /* renamed from: e */
    public String f10622e = "";

    /* renamed from: f */
    public String f10623f = "";

    /* renamed from: g */
    public String f10624g = "";

    /* renamed from: h */
    public Map<String, PlugInBean> f10625h = null;

    /* renamed from: i */
    public Map<String, PlugInBean> f10626i = null;

    /* renamed from: j */
    public boolean f10627j = false;

    /* renamed from: k */
    public boolean f10628k = false;

    /* renamed from: l */
    public int f10629l = 0;

    /* renamed from: m */
    public String f10630m = "";

    /* renamed from: n */
    public String f10631n = "";

    /* renamed from: o */
    public String f10632o = "";

    /* renamed from: p */
    public String f10633p = "";

    /* renamed from: q */
    public String f10634q = "";

    /* renamed from: r */
    public long f10635r = -1;

    /* renamed from: s */
    public String f10636s = null;

    /* renamed from: t */
    public int f10637t = 0;

    /* renamed from: u */
    public String f10638u = "";

    /* renamed from: v */
    public String f10639v = "";

    /* renamed from: w */
    public String f10640w = null;

    /* renamed from: x */
    public String f10641x = null;

    /* renamed from: y */
    public byte[] f10642y = null;

    /* renamed from: z */
    public Map<String, String> f10643z = null;

    /* renamed from: com.tencent.bugly.crashreport.crash.CrashDetailBean$a */
    /* compiled from: BUGLY */
    static class C4812a implements Parcelable.Creator<CrashDetailBean> {
        C4812a() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new CrashDetailBean(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new CrashDetailBean[i];
        }
    }

    public CrashDetailBean() {
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        int i;
        CrashDetailBean crashDetailBean = (CrashDetailBean) obj;
        if (crashDetailBean == null || this.f10635r - crashDetailBean.f10635r > 0) {
            return 1;
        }
        return i < 0 ? -1 : 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f10619b);
        parcel.writeString(this.f10620c);
        parcel.writeByte(this.f10621d ? (byte) 1 : 0);
        parcel.writeString(this.f10622e);
        parcel.writeString(this.f10623f);
        parcel.writeString(this.f10624g);
        parcel.writeByte(this.f10627j ? (byte) 1 : 0);
        parcel.writeByte(this.f10628k ? (byte) 1 : 0);
        parcel.writeInt(this.f10629l);
        parcel.writeString(this.f10630m);
        parcel.writeString(this.f10631n);
        parcel.writeString(this.f10632o);
        parcel.writeString(this.f10633p);
        parcel.writeString(this.f10634q);
        parcel.writeLong(this.f10635r);
        parcel.writeString(this.f10636s);
        parcel.writeInt(this.f10637t);
        parcel.writeString(this.f10638u);
        parcel.writeString(this.f10639v);
        parcel.writeString(this.f10640w);
        C4893z.m17038b(parcel, this.f10643z);
        parcel.writeString(this.f10595A);
        parcel.writeString(this.f10596B);
        parcel.writeLong(this.f10597C);
        parcel.writeLong(this.f10598D);
        parcel.writeLong(this.f10599E);
        parcel.writeLong(this.f10600F);
        parcel.writeLong(this.f10601G);
        parcel.writeLong(this.f10602H);
        parcel.writeString(this.f10603I);
        parcel.writeString(this.f10617W);
        parcel.writeString(this.f10604J);
        parcel.writeString(this.f10605K);
        parcel.writeString(this.f10606L);
        parcel.writeLong(this.f10607M);
        parcel.writeByte(this.f10608N ? (byte) 1 : 0);
        C4893z.m17038b(parcel, this.f10609O);
        C4893z.m17019a(parcel, this.f10625h);
        C4893z.m17019a(parcel, this.f10626i);
        parcel.writeInt(this.f10610P);
        parcel.writeInt(this.f10611Q);
        C4893z.m17038b(parcel, this.f10612R);
        C4893z.m17038b(parcel, this.f10613S);
        parcel.writeByteArray(this.f10614T);
        parcel.writeByteArray(this.f10642y);
        parcel.writeString(this.f10615U);
        parcel.writeString(this.f10616V);
        parcel.writeString(this.f10641x);
    }

    public CrashDetailBean(Parcel parcel) {
        this.f10619b = parcel.readInt();
        this.f10620c = parcel.readString();
        boolean z = true;
        this.f10621d = parcel.readByte() == 1;
        this.f10622e = parcel.readString();
        this.f10623f = parcel.readString();
        this.f10624g = parcel.readString();
        this.f10627j = parcel.readByte() == 1;
        this.f10628k = parcel.readByte() == 1;
        this.f10629l = parcel.readInt();
        this.f10630m = parcel.readString();
        this.f10631n = parcel.readString();
        this.f10632o = parcel.readString();
        this.f10633p = parcel.readString();
        this.f10634q = parcel.readString();
        this.f10635r = parcel.readLong();
        this.f10636s = parcel.readString();
        this.f10637t = parcel.readInt();
        this.f10638u = parcel.readString();
        this.f10639v = parcel.readString();
        this.f10640w = parcel.readString();
        this.f10643z = C4893z.m17036b(parcel);
        this.f10595A = parcel.readString();
        this.f10596B = parcel.readString();
        this.f10597C = parcel.readLong();
        this.f10598D = parcel.readLong();
        this.f10599E = parcel.readLong();
        this.f10600F = parcel.readLong();
        this.f10601G = parcel.readLong();
        this.f10602H = parcel.readLong();
        this.f10603I = parcel.readString();
        this.f10617W = parcel.readString();
        this.f10604J = parcel.readString();
        this.f10605K = parcel.readString();
        this.f10606L = parcel.readString();
        this.f10607M = parcel.readLong();
        this.f10608N = parcel.readByte() != 1 ? false : z;
        this.f10609O = C4893z.m17036b(parcel);
        this.f10625h = C4893z.m17018a(parcel);
        this.f10626i = C4893z.m17018a(parcel);
        this.f10610P = parcel.readInt();
        this.f10611Q = parcel.readInt();
        this.f10612R = C4893z.m17036b(parcel);
        this.f10613S = C4893z.m17036b(parcel);
        this.f10614T = parcel.createByteArray();
        this.f10642y = parcel.createByteArray();
        this.f10615U = parcel.readString();
        this.f10616V = parcel.readString();
        this.f10641x = parcel.readString();
    }
}
