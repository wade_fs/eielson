package com.tencent.bugly.proguard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.an */
/* compiled from: BUGLY */
public final class C4850an extends C4868k {

    /* renamed from: A */
    private static ArrayList<C4849am> f10810A = new ArrayList<>();

    /* renamed from: B */
    private static Map<String, String> f10811B = new HashMap();

    /* renamed from: C */
    private static Map<String, String> f10812C = new HashMap();

    /* renamed from: v */
    private static Map<String, String> f10813v = new HashMap();

    /* renamed from: w */
    private static C4848al f10814w = new C4848al();

    /* renamed from: x */
    private static C4847ak f10815x = new C4847ak();

    /* renamed from: y */
    private static ArrayList<C4847ak> f10816y = new ArrayList<>();

    /* renamed from: z */
    private static ArrayList<C4847ak> f10817z = new ArrayList<>();

    /* renamed from: a */
    public String f10818a = "";

    /* renamed from: b */
    public long f10819b = 0;

    /* renamed from: c */
    public String f10820c = "";

    /* renamed from: d */
    public String f10821d = "";

    /* renamed from: e */
    public String f10822e = "";

    /* renamed from: f */
    public String f10823f = "";

    /* renamed from: g */
    public String f10824g = "";

    /* renamed from: h */
    public Map<String, String> f10825h = null;

    /* renamed from: i */
    public String f10826i = "";

    /* renamed from: j */
    public C4848al f10827j = null;

    /* renamed from: k */
    public int f10828k = 0;

    /* renamed from: l */
    public String f10829l = "";

    /* renamed from: m */
    public String f10830m = "";

    /* renamed from: n */
    public C4847ak f10831n = null;

    /* renamed from: o */
    public ArrayList<C4847ak> f10832o = null;

    /* renamed from: p */
    public ArrayList<C4847ak> f10833p = null;

    /* renamed from: q */
    public ArrayList<C4849am> f10834q = null;

    /* renamed from: r */
    public Map<String, String> f10835r = null;

    /* renamed from: s */
    public Map<String, String> f10836s = null;

    /* renamed from: t */
    public String f10837t = "";

    /* renamed from: u */
    private boolean f10838u = true;

    static {
        f10813v.put("", "");
        f10816y.add(new C4847ak());
        f10817z.add(new C4847ak());
        f10810A.add(new C4849am());
        f10811B.put("", "");
        f10812C.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
     arg types: [com.tencent.bugly.proguard.al, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
     arg types: [com.tencent.bugly.proguard.ak, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.ak>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.am>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26880a(this.f10818a, 0);
        jVar.mo26877a(this.f10819b, 1);
        jVar.mo26880a(this.f10820c, 2);
        String str = this.f10821d;
        if (str != null) {
            jVar.mo26880a(str, 3);
        }
        String str2 = this.f10822e;
        if (str2 != null) {
            jVar.mo26880a(str2, 4);
        }
        String str3 = this.f10823f;
        if (str3 != null) {
            jVar.mo26880a(str3, 5);
        }
        String str4 = this.f10824g;
        if (str4 != null) {
            jVar.mo26880a(str4, 6);
        }
        Map<String, String> map = this.f10825h;
        if (map != null) {
            jVar.mo26882a((Map) map, 7);
        }
        String str5 = this.f10826i;
        if (str5 != null) {
            jVar.mo26880a(str5, 8);
        }
        C4848al alVar = this.f10827j;
        if (alVar != null) {
            jVar.mo26878a((C4868k) super, 9);
        }
        jVar.mo26876a(this.f10828k, 10);
        String str6 = this.f10829l;
        if (str6 != null) {
            jVar.mo26880a(str6, 11);
        }
        String str7 = this.f10830m;
        if (str7 != null) {
            jVar.mo26880a(str7, 12);
        }
        C4847ak akVar = this.f10831n;
        if (akVar != null) {
            jVar.mo26878a((C4868k) super, 13);
        }
        ArrayList<C4847ak> arrayList = this.f10832o;
        if (arrayList != null) {
            jVar.mo26881a((Collection) arrayList, 14);
        }
        ArrayList<C4847ak> arrayList2 = this.f10833p;
        if (arrayList2 != null) {
            jVar.mo26881a((Collection) arrayList2, 15);
        }
        ArrayList<C4849am> arrayList3 = this.f10834q;
        if (arrayList3 != null) {
            jVar.mo26881a((Collection) arrayList3, 16);
        }
        Map<String, String> map2 = this.f10835r;
        if (map2 != null) {
            jVar.mo26882a((Map) map2, 17);
        }
        Map<String, String> map3 = this.f10836s;
        if (map3 != null) {
            jVar.mo26882a((Map) map3, 18);
        }
        String str8 = this.f10837t;
        if (str8 != null) {
            jVar.mo26880a(str8, 19);
        }
        jVar.mo26884a(this.f10838u, 20);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
     arg types: [com.tencent.bugly.proguard.al, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
     arg types: [com.tencent.bugly.proguard.ak, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.ak>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.bugly.proguard.am>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.i$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.i.a(int, boolean):boolean */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10818a = iVar.mo26871b(0, true);
        this.f10819b = iVar.mo26864a(this.f10819b, 1, true);
        this.f10820c = iVar.mo26871b(2, true);
        this.f10821d = iVar.mo26871b(3, false);
        this.f10822e = iVar.mo26871b(4, false);
        this.f10823f = iVar.mo26871b(5, false);
        this.f10824g = iVar.mo26871b(6, false);
        this.f10825h = (Map) iVar.mo26866a((Object) f10813v, 7, false);
        this.f10826i = iVar.mo26871b(8, false);
        this.f10827j = (C4848al) iVar.mo26865a((C4868k) f10814w, 9, false);
        this.f10828k = iVar.mo26862a(this.f10828k, 10, false);
        this.f10829l = iVar.mo26871b(11, false);
        this.f10830m = iVar.mo26871b(12, false);
        this.f10831n = (C4847ak) iVar.mo26865a((C4868k) f10815x, 13, false);
        this.f10832o = (ArrayList) iVar.mo26866a((Object) f10816y, 14, false);
        this.f10833p = (ArrayList) iVar.mo26866a((Object) f10817z, 15, false);
        this.f10834q = (ArrayList) iVar.mo26866a((Object) f10810A, 16, false);
        this.f10835r = (Map) iVar.mo26866a((Object) f10811B, 17, false);
        this.f10836s = (Map) iVar.mo26866a((Object) f10812C, 18, false);
        this.f10837t = iVar.mo26871b(19, false);
        this.f10838u = iVar.mo26870a(20, false);
    }
}
