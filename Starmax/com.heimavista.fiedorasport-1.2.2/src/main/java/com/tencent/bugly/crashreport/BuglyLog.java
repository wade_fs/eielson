package com.tencent.bugly.crashreport;

import android.util.Log;
import androidx.exifinterface.media.ExifInterface;
import com.tencent.bugly.C4792b;
import com.tencent.bugly.proguard.C4889y;

/* compiled from: BUGLY */
public class BuglyLog {
    /* renamed from: d */
    public static void m16438d(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.d(str, str2);
        }
        C4889y.m16987a("D", str, str2);
    }

    /* renamed from: e */
    public static void m16439e(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.e(str, str2);
        }
        C4889y.m16987a(ExifInterface.LONGITUDE_EAST, str, str2);
    }

    /* renamed from: i */
    public static void m16441i(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.i(str, str2);
        }
        C4889y.m16987a("I", str, str2);
    }

    public static void setCache(int i) {
        C4889y.m16985a(i);
    }

    /* renamed from: v */
    public static void m16442v(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.v(str, str2);
        }
        C4889y.m16987a(ExifInterface.GPS_MEASUREMENT_INTERRUPTED, str, str2);
    }

    /* renamed from: w */
    public static void m16443w(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.w(str, str2);
        }
        C4889y.m16987a(ExifInterface.LONGITUDE_WEST, str, str2);
    }

    /* renamed from: e */
    public static void m16440e(String str, String str2, Throwable th) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C4792b.f10414c) {
            Log.e(str, str2, th);
        }
        C4889y.m16988a(ExifInterface.LONGITUDE_EAST, str, th);
    }
}
