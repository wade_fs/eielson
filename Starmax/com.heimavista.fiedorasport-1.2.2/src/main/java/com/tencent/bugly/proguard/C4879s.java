package com.tencent.bugly.proguard;

import android.content.Context;
import com.tencent.bugly.BuglyStrategy;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.s */
/* compiled from: BUGLY */
public final class C4879s {

    /* renamed from: b */
    private static C4879s f10988b;

    /* renamed from: a */
    public Map<String, String> f10989a = null;

    /* renamed from: c */
    private Context f10990c;

    private C4879s(Context context) {
        this.f10990c = context;
    }

    /* renamed from: a */
    public static C4879s m16925a(Context context) {
        if (f10988b == null) {
            f10988b = new C4879s(context);
        }
        return f10988b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003b A[Catch:{ all -> 0x0049, all -> 0x0050 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040 A[SYNTHETIC, Splitter:B:24:0x0040] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] m16929b(java.net.HttpURLConnection r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0033 }
            java.io.InputStream r5 = r5.getInputStream()     // Catch:{ all -> 0x0033 }
            r1.<init>(r5)     // Catch:{ all -> 0x0033 }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0031 }
            r5.<init>()     // Catch:{ all -> 0x0031 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x0031 }
        L_0x0016:
            int r3 = r1.read(r2)     // Catch:{ all -> 0x0031 }
            if (r3 <= 0) goto L_0x0021
            r4 = 0
            r5.write(r2, r4, r3)     // Catch:{ all -> 0x0031 }
            goto L_0x0016
        L_0x0021:
            r5.flush()     // Catch:{ all -> 0x0031 }
            byte[] r5 = r5.toByteArray()     // Catch:{ all -> 0x0031 }
            r1.close()     // Catch:{ all -> 0x002c }
            goto L_0x0030
        L_0x002c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0030:
            return r5
        L_0x0031:
            r5 = move-exception
            goto L_0x0035
        L_0x0033:
            r5 = move-exception
            r1 = r0
        L_0x0035:
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r5)     // Catch:{ all -> 0x0049 }
            if (r2 != 0) goto L_0x003e
            r5.printStackTrace()     // Catch:{ all -> 0x0049 }
        L_0x003e:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ all -> 0x0044 }
            goto L_0x0048
        L_0x0044:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0048:
            return r0
        L_0x0049:
            r5 = move-exception
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ all -> 0x0050 }
            goto L_0x0054
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0054:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4879s.m16929b(java.net.HttpURLConnection):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x018a A[Catch:{ all -> 0x017d, all -> 0x01a5 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] mo26913a(java.lang.String r23, byte[] r24, com.tencent.bugly.proguard.C4885v r25, java.util.Map<java.lang.String, java.lang.String> r26) {
        /*
            r22 = this;
            r1 = r22
            r2 = r24
            r3 = r25
            r4 = 0
            r5 = 0
            if (r23 != 0) goto L_0x0012
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.String r2 = "Failed for no URL."
            com.tencent.bugly.proguard.C4888x.m16984e(r2, r0)
            return r4
        L_0x0012:
            if (r2 != 0) goto L_0x0017
            r8 = 0
            goto L_0x0019
        L_0x0017:
            int r0 = r2.length
            long r8 = (long) r0
        L_0x0019:
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r5] = r23
            java.lang.Long r10 = java.lang.Long.valueOf(r8)
            r11 = 1
            r0[r11] = r10
            int r10 = android.os.Process.myPid()
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            r12 = 2
            r0[r12] = r10
            r10 = 3
            int r13 = android.os.Process.myTid()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r0[r10] = r13
            java.lang.String r10 = "request: %s, send: %d (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C4888x.m16982c(r10, r0)
            r14 = r23
            r0 = 0
            r10 = 0
            r13 = 0
        L_0x0045:
            if (r0 > 0) goto L_0x01c6
            if (r10 > 0) goto L_0x01c6
            if (r13 == 0) goto L_0x004e
            r6 = r0
            r13 = 0
            goto L_0x007d
        L_0x004e:
            int r0 = r0 + 1
            if (r0 <= r11) goto L_0x007c
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            java.lang.String r6 = "try time: "
            r15.<init>(r6)
            r15.append(r0)
            java.lang.String r6 = r15.toString()
            java.lang.Object[] r7 = new java.lang.Object[r5]
            com.tencent.bugly.proguard.C4888x.m16982c(r6, r7)
            java.util.Random r6 = new java.util.Random
            long r11 = java.lang.System.currentTimeMillis()
            r6.<init>(r11)
            r11 = 10000(0x2710, float:1.4013E-41)
            int r6 = r6.nextInt(r11)
            long r11 = (long) r6
            r18 = 10000(0x2710, double:4.9407E-320)
            long r11 = r11 + r18
            android.os.SystemClock.sleep(r11)
        L_0x007c:
            r6 = r0
        L_0x007d:
            android.content.Context r0 = r1.f10990c
            java.lang.String r0 = com.tencent.bugly.crashreport.common.info.C4807b.m16557c(r0)
            if (r0 != 0) goto L_0x0090
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.String r11 = "Failed to request for network not avail"
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r0)
            r0 = r6
        L_0x008d:
            r11 = 1
            r12 = 2
            goto L_0x0045
        L_0x0090:
            r3.mo26931a(r8)
            r11 = r26
            java.net.HttpURLConnection r12 = r1.m16927a(r14, r2, r0, r11)
            if (r12 == 0) goto L_0x01b1
            int r0 = r12.getResponseCode()     // Catch:{ IOException -> 0x0180 }
            r7 = 200(0xc8, float:2.8E-43)
            if (r0 != r7) goto L_0x00c7
            java.util.Map r0 = m16928a(r12)     // Catch:{ IOException -> 0x0180 }
            r1.f10989a = r0     // Catch:{ IOException -> 0x0180 }
            byte[] r7 = m16929b(r12)     // Catch:{ IOException -> 0x0180 }
            if (r7 != 0) goto L_0x00b2
            r4 = 0
            goto L_0x00b4
        L_0x00b2:
            int r0 = r7.length     // Catch:{ IOException -> 0x0180 }
            long r4 = (long) r0     // Catch:{ IOException -> 0x0180 }
        L_0x00b4:
            r3.mo26932b(r4)     // Catch:{ IOException -> 0x0180 }
            r12.disconnect()     // Catch:{ all -> 0x00bb }
            goto L_0x00c6
        L_0x00bb:
            r0 = move-exception
            r2 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r2)
            if (r0 != 0) goto L_0x00c6
            r2.printStackTrace()
        L_0x00c6:
            return r7
        L_0x00c7:
            r4 = 301(0x12d, float:4.22E-43)
            if (r0 == r4) goto L_0x00da
            r4 = 302(0x12e, float:4.23E-43)
            if (r0 == r4) goto L_0x00da
            r4 = 303(0x12f, float:4.25E-43)
            if (r0 == r4) goto L_0x00da
            r4 = 307(0x133, float:4.3E-43)
            if (r0 != r4) goto L_0x00d8
            goto L_0x00da
        L_0x00d8:
            r4 = 0
            goto L_0x00db
        L_0x00da:
            r4 = 1
        L_0x00db:
            if (r4 == 0) goto L_0x013c
            java.lang.String r4 = "Location"
            java.lang.String r4 = r12.getHeaderField(r4)     // Catch:{ IOException -> 0x0135 }
            if (r4 != 0) goto L_0x0111
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010b }
            java.lang.String r5 = "Failed to redirect: %d"
            r4.<init>(r5)     // Catch:{ IOException -> 0x010b }
            r4.append(r0)     // Catch:{ IOException -> 0x010b }
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x010b }
            r4 = 0
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x010b }
            com.tencent.bugly.proguard.C4888x.m16984e(r0, r5)     // Catch:{ IOException -> 0x010b }
            r12.disconnect()     // Catch:{ all -> 0x00fe }
        L_0x00fc:
            r2 = 0
            goto L_0x010a
        L_0x00fe:
            r0 = move-exception
            r2 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r2)
            if (r0 != 0) goto L_0x00fc
            r2.printStackTrace()
            goto L_0x00fc
        L_0x010a:
            return r2
        L_0x010b:
            r0 = move-exception
            r20 = r8
            r13 = 1
            goto L_0x0183
        L_0x0111:
            int r10 = r10 + 1
            java.lang.String r5 = "redirect code: %d ,to:%s"
            r7 = 2
            java.lang.Object[] r6 = new java.lang.Object[r7]     // Catch:{ IOException -> 0x012b }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x012b }
            r14 = 0
            r6[r14] = r13     // Catch:{ IOException -> 0x012b }
            r15 = 1
            r6[r15] = r4     // Catch:{ IOException -> 0x0129 }
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r6)     // Catch:{ IOException -> 0x0129 }
            r14 = r4
            r5 = 0
            r13 = 1
            goto L_0x013f
        L_0x0129:
            r0 = move-exception
            goto L_0x0130
        L_0x012b:
            r0 = move-exception
            goto L_0x012f
        L_0x012d:
            r0 = move-exception
            r7 = 2
        L_0x012f:
            r15 = 1
        L_0x0130:
            r14 = r4
            r20 = r8
            r6 = 0
            goto L_0x013a
        L_0x0135:
            r0 = move-exception
            r7 = 2
            r15 = 1
            r20 = r8
        L_0x013a:
            r13 = 1
            goto L_0x0184
        L_0x013c:
            r7 = 2
            r15 = 1
            r5 = r6
        L_0x013f:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0178 }
            java.lang.String r6 = "response code "
            r4.<init>(r6)     // Catch:{ IOException -> 0x0178 }
            r4.append(r0)     // Catch:{ IOException -> 0x0178 }
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x0178 }
            r4 = 0
            java.lang.Object[] r6 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x0178 }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r6)     // Catch:{ IOException -> 0x0178 }
            int r0 = r12.getContentLength()     // Catch:{ IOException -> 0x0178 }
            r20 = r8
            long r7 = (long) r0
            r16 = 0
            int r0 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r0 >= 0) goto L_0x0162
            r7 = 0
        L_0x0162:
            r3.mo26932b(r7)     // Catch:{ IOException -> 0x0176 }
            r12.disconnect()     // Catch:{ all -> 0x0169 }
            goto L_0x0174
        L_0x0169:
            r0 = move-exception
            r6 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r6)
            if (r0 != 0) goto L_0x0174
            r6.printStackTrace()
        L_0x0174:
            r0 = r5
            goto L_0x019d
        L_0x0176:
            r0 = move-exception
            goto L_0x017b
        L_0x0178:
            r0 = move-exception
            r20 = r8
        L_0x017b:
            r6 = r5
            goto L_0x0184
        L_0x017d:
            r0 = move-exception
            r2 = r0
            goto L_0x01a1
        L_0x0180:
            r0 = move-exception
            r20 = r8
        L_0x0183:
            r15 = 1
        L_0x0184:
            boolean r5 = com.tencent.bugly.proguard.C4888x.m16978a(r0)     // Catch:{ all -> 0x017d }
            if (r5 != 0) goto L_0x018d
            r0.printStackTrace()     // Catch:{ all -> 0x017d }
        L_0x018d:
            r12.disconnect()     // Catch:{ all -> 0x0191 }
            goto L_0x019c
        L_0x0191:
            r0 = move-exception
            r5 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r5)
            if (r0 != 0) goto L_0x019c
            r5.printStackTrace()
        L_0x019c:
            r0 = r6
        L_0x019d:
            r5 = 0
            r7 = 0
            goto L_0x01c1
        L_0x01a1:
            r12.disconnect()     // Catch:{ all -> 0x01a5 }
            goto L_0x01b0
        L_0x01a5:
            r0 = move-exception
            r3 = r0
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r3)
            if (r0 != 0) goto L_0x01b0
            r3.printStackTrace()
        L_0x01b0:
            throw r2
        L_0x01b1:
            r20 = r8
            r15 = 1
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.String r7 = "Failed to execute post."
            com.tencent.bugly.proguard.C4888x.m16982c(r7, r0)
            r7 = 0
            r3.mo26932b(r7)
            r0 = r6
        L_0x01c1:
            r8 = r20
            r4 = 0
            goto L_0x008d
        L_0x01c6:
            r2 = r4
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4879s.mo26913a(java.lang.String, byte[], com.tencent.bugly.proguard.v, java.util.Map):byte[]");
    }

    /* renamed from: a */
    private static Map<String, String> m16928a(HttpURLConnection httpURLConnection) {
        HashMap hashMap = new HashMap();
        Map<String, List<String>> headerFields = httpURLConnection.getHeaderFields();
        if (headerFields == null || headerFields.size() == 0) {
            return null;
        }
        for (String str : headerFields.keySet()) {
            List list = headerFields.get(str);
            if (list.size() > 0) {
                hashMap.put(str, list.get(0));
            }
        }
        return hashMap;
    }

    /* renamed from: a */
    private HttpURLConnection m16927a(String str, byte[] bArr, String str2, Map<String, String> map) {
        if (str == null) {
            C4888x.m16984e("destUrl is null.", new Object[0]);
            return null;
        }
        HttpURLConnection a = m16926a(str2, str);
        if (a == null) {
            C4888x.m16984e("Failed to get HttpURLConnection object.", new Object[0]);
            return null;
        }
        try {
            a.setRequestProperty("wup_version", "3.0");
            if (map != null) {
                if (map.size() > 0) {
                    for (Map.Entry entry : map.entrySet()) {
                        a.setRequestProperty((String) entry.getKey(), URLEncoder.encode((String) entry.getValue(), "utf-8"));
                    }
                }
            }
            a.setRequestProperty("A37", URLEncoder.encode(str2, "utf-8"));
            a.setRequestProperty("A38", URLEncoder.encode(str2, "utf-8"));
            OutputStream outputStream = a.getOutputStream();
            if (bArr == null) {
                outputStream.write(0);
            } else {
                outputStream.write(bArr);
            }
            return a;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            C4888x.m16984e("Failed to upload, please check your network.", new Object[0]);
            return null;
        }
    }

    /* renamed from: a */
    private static HttpURLConnection m16926a(String str, String str2) {
        HttpURLConnection httpURLConnection;
        try {
            URL url = new URL(str2);
            if (C4836a.m16731b() != null) {
                httpURLConnection = (HttpURLConnection) url.openConnection(C4836a.m16731b());
            } else if (str == null || !str.toLowerCase(Locale.US).contains("wap")) {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            } else {
                httpURLConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(System.getProperty("http.proxyHost"), Integer.parseInt(System.getProperty("http.proxyPort")))));
            }
            httpURLConnection.setConnectTimeout(BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setInstanceFollowRedirects(false);
            return httpURLConnection;
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }
}
