package com.tencent.bugly.crashreport.common.info;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/* compiled from: BUGLY */
public class AppInfo {

    /* renamed from: a */
    private static ActivityManager f10467a;

    static {
        "@buglyAllChannel@".split(",");
        "@buglyAllChannelPriority@".split(",");
    }

    /* renamed from: a */
    public static String m16478a(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return context.getPackageName();
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return "fail";
            }
            th.printStackTrace();
            return "fail";
        }
    }

    /* renamed from: b */
    public static PackageInfo m16482b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(m16478a(context), 0);
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: c */
    public static String m16483c(Context context) {
        CharSequence applicationLabel;
        if (context == null) {
            return null;
        }
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            if (!(packageManager == null || applicationInfo == null || (applicationLabel = packageManager.getApplicationLabel(applicationInfo)) == null)) {
                return applicationLabel.toString();
            }
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
        }
        return null;
    }

    /* renamed from: d */
    public static Map<String, String> m16484d(Context context) {
        if (context == null) {
            return null;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData == null) {
                return null;
            }
            HashMap hashMap = new HashMap();
            Object obj = applicationInfo.metaData.get("BUGLY_DISABLE");
            if (obj != null) {
                hashMap.put("BUGLY_DISABLE", obj.toString());
            }
            Object obj2 = applicationInfo.metaData.get("BUGLY_APPID");
            if (obj2 != null) {
                hashMap.put("BUGLY_APPID", obj2.toString());
            }
            Object obj3 = applicationInfo.metaData.get("BUGLY_APP_CHANNEL");
            if (obj3 != null) {
                hashMap.put("BUGLY_APP_CHANNEL", obj3.toString());
            }
            Object obj4 = applicationInfo.metaData.get("BUGLY_APP_VERSION");
            if (obj4 != null) {
                hashMap.put("BUGLY_APP_VERSION", obj4.toString());
            }
            Object obj5 = applicationInfo.metaData.get("BUGLY_ENABLE_DEBUG");
            if (obj5 != null) {
                hashMap.put("BUGLY_ENABLE_DEBUG", obj5.toString());
            }
            Object obj6 = applicationInfo.metaData.get("com.tencent.rdm.uuid");
            if (obj6 != null) {
                hashMap.put("com.tencent.rdm.uuid", obj6.toString());
            }
            return hashMap;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: e */
    public static String m16485e(Context context) {
        Signature[] signatureArr;
        String a = m16478a(context);
        if (a == null) {
            return null;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(a, 64);
            if (!(packageInfo == null || (signatureArr = packageInfo.signatures) == null || signatureArr.length == 0)) {
                return m16479a(signatureArr[0].toByteArray());
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return null;
    }

    /* renamed from: f */
    public static boolean m16486f(Context context) {
        if (context == null) {
            return false;
        }
        if (f10467a == null) {
            f10467a = (ActivityManager) context.getSystemService("activity");
        }
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            f10467a.getMemoryInfo(memoryInfo);
            if (!memoryInfo.lowMemory) {
                return false;
            }
            C4888x.m16982c("Memory is low.", new Object[0]);
            return true;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return false;
        }
    }

    /* renamed from: g */
    public static String m16487g(Context context) {
        if (context == null) {
            return "";
        }
        String h = m16488h(context);
        if (!C4893z.m17024a(h)) {
            return h;
        }
        return m16489i(context);
    }

    /* renamed from: h */
    private static String m16488h(Context context) {
        String str = "";
        InputStream inputStream = null;
        try {
            String string = C4893z.m17003a("DENGTA_META", context).getString("key_channelpath", str);
            if (C4893z.m17024a(string)) {
                string = "channel.ini";
            }
            C4888x.m16977a("[AppInfo] Beacon channel file path: " + string, new Object[0]);
            if (!string.equals(str)) {
                inputStream = context.getAssets().open(string);
                Properties properties = new Properties();
                properties.load(inputStream);
                str = properties.getProperty("CHANNEL", str);
                C4888x.m16977a("[AppInfo] Beacon channel read from assert: " + str, new Object[0]);
                if (!C4893z.m17024a(str)) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            C4888x.m16978a(e);
                        }
                    }
                    return str;
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    C4888x.m16978a(e2);
                }
            }
        } catch (Exception unused) {
            C4888x.m16983d("[AppInfo] Failed to get get beacon channel", new Object[0]);
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    C4888x.m16978a(e3);
                }
            }
            throw th;
        }
        return str;
    }

    /* renamed from: i */
    private static String m16489i(Context context) {
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("CHANNEL_DENGTA");
            if (obj != null) {
                return obj.toString();
            }
            return "";
        } catch (Throwable th) {
            C4888x.m16983d("[AppInfo] Failed to read beacon channel from manifest.", new Object[0]);
            C4888x.m16978a(th);
            return "";
        }
    }

    /* renamed from: a */
    public static boolean m16481a(Context context, String str) {
        if (!(context == null || str == null || str.trim().length() <= 0)) {
            try {
                String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
                if (strArr != null) {
                    for (String str2 : strArr) {
                        if (str.equals(str2)) {
                            return true;
                        }
                    }
                }
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0045 A[Catch:{ all -> 0x0052, all -> 0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004e A[SYNTHETIC, Splitter:B:25:0x004e] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m16477a(int r6) {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ all -> 0x003e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x003e }
            java.lang.String r3 = "/proc/"
            r2.<init>(r3)     // Catch:{ all -> 0x003e }
            r2.append(r6)     // Catch:{ all -> 0x003e }
            java.lang.String r3 = "/cmdline"
            r2.append(r3)     // Catch:{ all -> 0x003e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x003e }
            r1.<init>(r2)     // Catch:{ all -> 0x003e }
            r0 = 512(0x200, float:7.175E-43)
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0039 }
            r1.read(r0)     // Catch:{ all -> 0x0039 }
            r2 = 0
            r3 = 0
        L_0x0022:
            int r4 = r0.length     // Catch:{ all -> 0x0039 }
            if (r3 >= r4) goto L_0x002c
            char r4 = r0[r3]     // Catch:{ all -> 0x0039 }
            if (r4 == 0) goto L_0x002c
            int r3 = r3 + 1
            goto L_0x0022
        L_0x002c:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x0039 }
            r4.<init>(r0)     // Catch:{ all -> 0x0039 }
            java.lang.String r6 = r4.substring(r2, r3)     // Catch:{ all -> 0x0039 }
            r1.close()     // Catch:{ all -> 0x0038 }
        L_0x0038:
            return r6
        L_0x0039:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003f
        L_0x003e:
            r1 = move-exception
        L_0x003f:
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r1)     // Catch:{ all -> 0x0052 }
            if (r2 != 0) goto L_0x0048
            r1.printStackTrace()     // Catch:{ all -> 0x0052 }
        L_0x0048:
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0051
            r0.close()     // Catch:{ all -> 0x0051 }
        L_0x0051:
            return r6
        L_0x0052:
            r6 = move-exception
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ all -> 0x0058 }
        L_0x0058:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.common.info.AppInfo.m16477a(int):java.lang.String");
    }

    /* renamed from: a */
    public static List<String> m16480a(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        try {
            String str = map.get("BUGLY_DISABLE");
            if (str != null) {
                if (str.length() != 0) {
                    String[] split = str.split(",");
                    for (int i = 0; i < split.length; i++) {
                        split[i] = split[i].trim();
                    }
                    return Arrays.asList(split);
                }
            }
            return null;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    private static String m16479a(byte[] bArr) {
        X509Certificate x509Certificate;
        StringBuilder sb = new StringBuilder();
        if (bArr != null && bArr.length > 0) {
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                if (instance == null || (x509Certificate = (X509Certificate) instance.generateCertificate(new ByteArrayInputStream(bArr))) == null) {
                    return null;
                }
                sb.append("Issuer|");
                Principal issuerDN = x509Certificate.getIssuerDN();
                if (issuerDN != null) {
                    sb.append(issuerDN.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("SerialNumber|");
                BigInteger serialNumber = x509Certificate.getSerialNumber();
                if (issuerDN != null) {
                    sb.append(serialNumber.toString(16));
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("NotBefore|");
                Date notBefore = x509Certificate.getNotBefore();
                if (issuerDN != null) {
                    sb.append(notBefore.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("NotAfter|");
                Date notAfter = x509Certificate.getNotAfter();
                if (issuerDN != null) {
                    sb.append(notAfter.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("SHA1|");
                String a = C4893z.m17015a(MessageDigest.getInstance("SHA1").digest(x509Certificate.getEncoded()));
                if (a == null || a.length() <= 0) {
                    sb.append("unknown");
                } else {
                    sb.append(a.toString());
                }
                sb.append("\n");
                sb.append("MD5|");
                String a2 = C4893z.m17015a(MessageDigest.getInstance("MD5").digest(x509Certificate.getEncoded()));
                if (a2 == null || a2.length() <= 0) {
                    sb.append("unknown");
                } else {
                    sb.append(a2.toString());
                }
            } catch (CertificateException e) {
                if (!C4888x.m16978a(e)) {
                    e.printStackTrace();
                }
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
        if (sb.length() == 0) {
            return "unknown";
        }
        return sb.toString();
    }
}
