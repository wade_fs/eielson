package com.tencent.bugly.proguard;

import com.tencent.bugly.C4792b;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.tencent.bugly.proguard.w */
/* compiled from: BUGLY */
public final class C4886w {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final AtomicInteger f11040a = new AtomicInteger(1);

    /* renamed from: b */
    private static C4886w f11041b;

    /* renamed from: c */
    private ScheduledExecutorService f11042c;

    /* renamed from: com.tencent.bugly.proguard.w$a */
    /* compiled from: BUGLY */
    class C4887a implements ThreadFactory {
        C4887a(C4886w wVar) {
        }

        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setName("BuglyThread-" + C4886w.f11040a.getAndIncrement());
            return thread;
        }
    }

    protected C4886w() {
        this.f11042c = null;
        this.f11042c = Executors.newScheduledThreadPool(3, new C4887a(this));
        ScheduledExecutorService scheduledExecutorService = this.f11042c;
        if (scheduledExecutorService == null || scheduledExecutorService.isShutdown()) {
            C4888x.m16983d("[AsyncTaskHandler] ScheduledExecutorService is not valiable!", new Object[0]);
        }
    }

    /* renamed from: a */
    public static synchronized C4886w m16969a() {
        C4886w wVar;
        synchronized (C4886w.class) {
            if (f11041b == null) {
                f11041b = new C4886w();
            }
            wVar = f11041b;
        }
        return wVar;
    }

    /* renamed from: b */
    public final synchronized void mo26936b() {
        if (this.f11042c != null && !this.f11042c.isShutdown()) {
            C4888x.m16982c("[AsyncTaskHandler] Close async handler.", new Object[0]);
            this.f11042c.shutdownNow();
        }
    }

    /* renamed from: c */
    public final synchronized boolean mo26937c() {
        return this.f11042c != null && !this.f11042c.isShutdown();
    }

    /* renamed from: a */
    public final synchronized boolean mo26935a(Runnable runnable, long j) {
        if (!mo26937c()) {
            C4888x.m16983d("[AsyncTaskHandler] Async handler was closed, should not post task.", new Object[0]);
            return false;
        } else if (runnable == null) {
            C4888x.m16983d("[AsyncTaskHandler] Task input is null.", new Object[0]);
            return false;
        } else {
            if (j <= 0) {
                j = 0;
            }
            C4888x.m16982c("[AsyncTaskHandler] Post a delay(time: %dms) task: %s", Long.valueOf(j), runnable.getClass().getName());
            try {
                this.f11042c.schedule(runnable, j, TimeUnit.MILLISECONDS);
                return true;
            } catch (Throwable th) {
                if (C4792b.f10414c) {
                    th.printStackTrace();
                }
                return false;
            }
        }
    }

    /* renamed from: a */
    public final synchronized boolean mo26934a(Runnable runnable) {
        if (!mo26937c()) {
            C4888x.m16983d("[AsyncTaskHandler] Async handler was closed, should not post task.", new Object[0]);
            return false;
        } else if (runnable == null) {
            C4888x.m16983d("[AsyncTaskHandler] Task input is null.", new Object[0]);
            return false;
        } else {
            C4888x.m16982c("[AsyncTaskHandler] Post a normal task: %s", runnable.getClass().getName());
            try {
                this.f11042c.execute(runnable);
                return true;
            } catch (Throwable th) {
                if (C4792b.f10414c) {
                    th.printStackTrace();
                }
                return false;
            }
        }
    }
}
