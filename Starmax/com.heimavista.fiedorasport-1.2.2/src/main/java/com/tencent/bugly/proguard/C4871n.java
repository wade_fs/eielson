package com.tencent.bugly.proguard;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.tencent.bugly.crashreport.common.info.C4806a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.n */
/* compiled from: BUGLY */
public final class C4871n {

    /* renamed from: a */
    public static final long f10945a = System.currentTimeMillis();

    /* renamed from: b */
    private static C4871n f10946b;

    /* renamed from: c */
    private Context f10947c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f10948d = C4806a.m16492b().f10526d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public Map<Integer, Map<String, C4870m>> f10949e = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public SharedPreferences f10950f;

    /* renamed from: com.tencent.bugly.proguard.n$a */
    /* compiled from: BUGLY */
    class C4872a implements Runnable {

        /* renamed from: P */
        private /* synthetic */ int f10951P;

        /* renamed from: Q */
        private /* synthetic */ int f10952Q;

        C4872a(int i, int i2) {
            this.f10951P = i;
            this.f10952Q = i2;
        }

        public final void run() {
            C4870m mVar;
            try {
                if (!TextUtils.isEmpty(C4871n.this.f10948d)) {
                    List<C4870m> a = C4871n.this.m16887c(this.f10951P);
                    if (a == null) {
                        a = new ArrayList<>();
                    }
                    if (C4871n.this.f10949e.get(Integer.valueOf(this.f10951P)) == null) {
                        C4871n.this.f10949e.put(Integer.valueOf(this.f10951P), new HashMap());
                    }
                    if (((Map) C4871n.this.f10949e.get(Integer.valueOf(this.f10951P))).get(C4871n.this.f10948d) == null) {
                        mVar = new C4870m();
                        mVar.f10938a = (long) this.f10951P;
                        mVar.f10944g = C4871n.f10945a;
                        mVar.f10939b = C4871n.this.f10948d;
                        mVar.f10943f = C4806a.m16492b().f10532j;
                        C4806a.m16492b().getClass();
                        mVar.f10942e = "3.1.0";
                        mVar.f10940c = System.currentTimeMillis();
                        mVar.f10941d = this.f10952Q;
                        ((Map) C4871n.this.f10949e.get(Integer.valueOf(this.f10951P))).put(C4871n.this.f10948d, mVar);
                    } else {
                        mVar = (C4870m) ((Map) C4871n.this.f10949e.get(Integer.valueOf(this.f10951P))).get(C4871n.this.f10948d);
                        mVar.f10941d = this.f10952Q;
                    }
                    ArrayList arrayList = new ArrayList();
                    boolean z = false;
                    for (C4870m mVar2 : a) {
                        if (mVar2.f10944g == mVar.f10944g && mVar2.f10939b != null && mVar2.f10939b.equalsIgnoreCase(mVar.f10939b)) {
                            z = true;
                            mVar2.f10941d = mVar.f10941d;
                        }
                        if ((mVar2.f10942e != null && !mVar2.f10942e.equalsIgnoreCase(mVar.f10942e)) || ((mVar2.f10943f != null && !mVar2.f10943f.equalsIgnoreCase(mVar.f10943f)) || mVar2.f10941d <= 0)) {
                            arrayList.add(mVar2);
                        }
                    }
                    a.removeAll(arrayList);
                    if (!z) {
                        a.add(mVar);
                    }
                    C4871n.this.m16881a(this.f10951P, a);
                }
            } catch (Exception unused) {
                C4888x.m16984e("saveCrashRecord failed", new Object[0]);
            }
        }
    }

    /* renamed from: com.tencent.bugly.proguard.n$b */
    /* compiled from: BUGLY */
    class C4873b implements Runnable {

        /* renamed from: P */
        private /* synthetic */ int f10954P;

        C4873b(int i) {
            this.f10954P = i;
        }

        public final void run() {
            boolean b = C4871n.this.m16884b(this.f10954P);
            SharedPreferences.Editor edit = C4871n.this.f10950f.edit();
            edit.putBoolean(this.f10954P + "_" + C4871n.this.f10948d, !b).commit();
        }
    }

    private C4871n(Context context) {
        this.f10947c = context;
        this.f10950f = context.getSharedPreferences("crashrecord", 0);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
        if (r6 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0052, code lost:
        if (r6 != null) goto L_0x0046;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0058 A[SYNTHETIC, Splitter:B:35:0x0058] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T extends java.util.List<?>> T m16887c(int r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x005e }
            android.content.Context r3 = r5.f10947c     // Catch:{ Exception -> 0x005e }
            java.lang.String r4 = "crashrecord"
            java.io.File r3 = r3.getDir(r4, r1)     // Catch:{ Exception -> 0x005e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005e }
            r4.<init>()     // Catch:{ Exception -> 0x005e }
            r4.append(r6)     // Catch:{ Exception -> 0x005e }
            java.lang.String r6 = r4.toString()     // Catch:{ Exception -> 0x005e }
            r2.<init>(r3, r6)     // Catch:{ Exception -> 0x005e }
            boolean r6 = r2.exists()     // Catch:{ Exception -> 0x005e }
            if (r6 != 0) goto L_0x0024
            monitor-exit(r5)
            return r0
        L_0x0024:
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x004a, ClassNotFoundException -> 0x003c, all -> 0x0039 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x004a, ClassNotFoundException -> 0x003c, all -> 0x0039 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x004a, ClassNotFoundException -> 0x003c, all -> 0x0039 }
            r6.<init>(r3)     // Catch:{ IOException -> 0x004a, ClassNotFoundException -> 0x003c, all -> 0x0039 }
            java.lang.Object r2 = r6.readObject()     // Catch:{ IOException -> 0x004b, ClassNotFoundException -> 0x003d }
            java.util.List r2 = (java.util.List) r2     // Catch:{ IOException -> 0x004b, ClassNotFoundException -> 0x003d }
            r6.close()     // Catch:{ Exception -> 0x005e }
            monitor-exit(r5)
            return r2
        L_0x0039:
            r2 = move-exception
            r6 = r0
            goto L_0x0056
        L_0x003c:
            r6 = r0
        L_0x003d:
            java.lang.String r2 = "get object error"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x0055 }
            com.tencent.bugly.proguard.C4888x.m16977a(r2, r3)     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0065
        L_0x0046:
            r6.close()     // Catch:{ Exception -> 0x005e }
            goto L_0x0065
        L_0x004a:
            r6 = r0
        L_0x004b:
            java.lang.String r2 = "open record file error"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x0055 }
            com.tencent.bugly.proguard.C4888x.m16977a(r2, r3)     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0065
            goto L_0x0046
        L_0x0055:
            r2 = move-exception
        L_0x0056:
            if (r6 == 0) goto L_0x005b
            r6.close()     // Catch:{ Exception -> 0x005e }
        L_0x005b:
            throw r2     // Catch:{ Exception -> 0x005e }
        L_0x005c:
            r6 = move-exception
            goto L_0x0067
        L_0x005e:
            java.lang.String r6 = "readCrashRecord error"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x005c }
            com.tencent.bugly.proguard.C4888x.m16984e(r6, r1)     // Catch:{ all -> 0x005c }
        L_0x0065:
            monitor-exit(r5)
            return r0
        L_0x0067:
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4871n.m16887c(int):java.util.List");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0077, code lost:
        return true;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean m16884b(int r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 0
            java.util.List r1 = r12.m16887c(r13)     // Catch:{ Exception -> 0x0082 }
            if (r1 != 0) goto L_0x000a
            monitor-exit(r12)
            return r0
        L_0x000a:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0082 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0082 }
            r4.<init>()     // Catch:{ Exception -> 0x0082 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0082 }
            r5.<init>()     // Catch:{ Exception -> 0x0082 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ Exception -> 0x0082 }
        L_0x001c:
            boolean r7 = r6.hasNext()     // Catch:{ Exception -> 0x0082 }
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            if (r7 == 0) goto L_0x004b
            java.lang.Object r7 = r6.next()     // Catch:{ Exception -> 0x0082 }
            com.tencent.bugly.proguard.m r7 = (com.tencent.bugly.proguard.C4870m) r7     // Catch:{ Exception -> 0x0082 }
            java.lang.String r10 = r7.f10939b     // Catch:{ Exception -> 0x0082 }
            if (r10 == 0) goto L_0x0040
            java.lang.String r10 = r7.f10939b     // Catch:{ Exception -> 0x0082 }
            java.lang.String r11 = r12.f10948d     // Catch:{ Exception -> 0x0082 }
            boolean r10 = r10.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x0082 }
            if (r10 == 0) goto L_0x0040
            int r10 = r7.f10941d     // Catch:{ Exception -> 0x0082 }
            if (r10 <= 0) goto L_0x0040
            r4.add(r7)     // Catch:{ Exception -> 0x0082 }
        L_0x0040:
            long r10 = r7.f10940c     // Catch:{ Exception -> 0x0082 }
            long r10 = r10 + r8
            int r8 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r8 >= 0) goto L_0x001c
            r5.add(r7)     // Catch:{ Exception -> 0x0082 }
            goto L_0x001c
        L_0x004b:
            java.util.Collections.sort(r4)     // Catch:{ Exception -> 0x0082 }
            int r6 = r4.size()     // Catch:{ Exception -> 0x0082 }
            r7 = 2
            if (r6 < r7) goto L_0x0078
            int r5 = r4.size()     // Catch:{ Exception -> 0x0082 }
            r6 = 1
            if (r5 <= 0) goto L_0x0076
            int r5 = r4.size()     // Catch:{ Exception -> 0x0082 }
            int r5 = r5 - r6
            java.lang.Object r4 = r4.get(r5)     // Catch:{ Exception -> 0x0082 }
            com.tencent.bugly.proguard.m r4 = (com.tencent.bugly.proguard.C4870m) r4     // Catch:{ Exception -> 0x0082 }
            long r4 = r4.f10940c     // Catch:{ Exception -> 0x0082 }
            long r4 = r4 + r8
            int r7 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r7 >= 0) goto L_0x0076
            r1.clear()     // Catch:{ Exception -> 0x0082 }
            r12.m16881a(r13, r1)     // Catch:{ Exception -> 0x0082 }
            monitor-exit(r12)
            return r0
        L_0x0076:
            monitor-exit(r12)
            return r6
        L_0x0078:
            r1.removeAll(r5)     // Catch:{ Exception -> 0x0082 }
            r12.m16881a(r13, r1)     // Catch:{ Exception -> 0x0082 }
            monitor-exit(r12)
            return r0
        L_0x0080:
            r13 = move-exception
            goto L_0x008b
        L_0x0082:
            java.lang.String r13 = "isFrequentCrash failed"
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch:{ all -> 0x0080 }
            com.tencent.bugly.proguard.C4888x.m16984e(r13, r1)     // Catch:{ all -> 0x0080 }
            monitor-exit(r12)
            return r0
        L_0x008b:
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4871n.m16884b(int):boolean");
    }

    /* renamed from: a */
    public static synchronized C4871n m16878a(Context context) {
        C4871n nVar;
        synchronized (C4871n.class) {
            if (f10946b == null) {
                f10946b = new C4871n(context);
            }
            nVar = f10946b;
        }
        return nVar;
    }

    /* renamed from: a */
    public static synchronized C4871n m16877a() {
        C4871n nVar;
        synchronized (C4871n.class) {
            nVar = f10946b;
        }
        return nVar;
    }

    /* renamed from: a */
    public final void mo26889a(int i, int i2) {
        C4886w.m16969a().mo26934a(new C4872a(1004, i2));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0048 A[SYNTHETIC, Splitter:B:23:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004c A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0050 A[SYNTHETIC, Splitter:B:28:0x0050] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T extends java.util.List<?>> void m16881a(int r5, java.util.List r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r6 != 0) goto L_0x0005
            monitor-exit(r4)
            return
        L_0x0005:
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0056 }
            android.content.Context r2 = r4.f10947c     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = "crashrecord"
            java.io.File r2 = r2.getDir(r3, r0)     // Catch:{ Exception -> 0x0056 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0056 }
            r3.<init>()     // Catch:{ Exception -> 0x0056 }
            r3.append(r5)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x0056 }
            r1.<init>(r2, r5)     // Catch:{ Exception -> 0x0056 }
            r5 = 0
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x003b }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x003b }
            r3.<init>(r1)     // Catch:{ IOException -> 0x003b }
            r2.<init>(r3)     // Catch:{ IOException -> 0x003b }
            r2.writeObject(r6)     // Catch:{ IOException -> 0x0035, all -> 0x0031 }
            r2.close()     // Catch:{ Exception -> 0x0056 }
            goto L_0x005d
        L_0x0031:
            r5 = move-exception
            r6 = r5
            r5 = r2
            goto L_0x004e
        L_0x0035:
            r5 = move-exception
            r6 = r5
            r5 = r2
            goto L_0x003c
        L_0x0039:
            r6 = move-exception
            goto L_0x004e
        L_0x003b:
            r6 = move-exception
        L_0x003c:
            r6.printStackTrace()     // Catch:{ all -> 0x0039 }
            java.lang.String r6 = "open record file error"
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch:{ all -> 0x0039 }
            com.tencent.bugly.proguard.C4888x.m16977a(r6, r1)     // Catch:{ all -> 0x0039 }
            if (r5 == 0) goto L_0x004c
            r5.close()     // Catch:{ Exception -> 0x0056 }
            goto L_0x005d
        L_0x004c:
            monitor-exit(r4)
            return
        L_0x004e:
            if (r5 == 0) goto L_0x0053
            r5.close()     // Catch:{ Exception -> 0x0056 }
        L_0x0053:
            throw r6     // Catch:{ Exception -> 0x0056 }
        L_0x0054:
            r5 = move-exception
            goto L_0x005f
        L_0x0056:
            java.lang.String r5 = "writeCrashRecord error"
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch:{ all -> 0x0054 }
            com.tencent.bugly.proguard.C4888x.m16984e(r5, r6)     // Catch:{ all -> 0x0054 }
        L_0x005d:
            monitor-exit(r4)
            return
        L_0x005f:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4871n.m16881a(int, java.util.List):void");
    }

    /* renamed from: a */
    public final synchronized boolean mo26890a(int i) {
        boolean z;
        z = true;
        try {
            SharedPreferences sharedPreferences = this.f10950f;
            z = sharedPreferences.getBoolean(i + "_" + this.f10948d, true);
            C4886w.m16969a().mo26934a(new C4873b(i));
        } catch (Exception unused) {
            C4888x.m16984e("canInit error", new Object[0]);
            return z;
        }
        return z;
    }
}
