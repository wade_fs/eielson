package com.tencent.bugly;

import com.tencent.bugly.crashreport.common.info.C4806a;
import java.util.Map;

/* compiled from: BUGLY */
public class BuglyStrategy {

    /* renamed from: a */
    private String f10391a;

    /* renamed from: b */
    private String f10392b;

    /* renamed from: c */
    private String f10393c;

    /* renamed from: d */
    private long f10394d;

    /* renamed from: e */
    private String f10395e;

    /* renamed from: f */
    private String f10396f;

    /* renamed from: g */
    private boolean f10397g = true;

    /* renamed from: h */
    private boolean f10398h = true;

    /* renamed from: i */
    private boolean f10399i = true;

    /* renamed from: j */
    private Class<?> f10400j = null;

    /* renamed from: k */
    private boolean f10401k = true;

    /* renamed from: l */
    private boolean f10402l = true;

    /* renamed from: m */
    private boolean f10403m = true;

    /* renamed from: n */
    private boolean f10404n = false;

    /* renamed from: o */
    private C4791a f10405o;

    /* renamed from: com.tencent.bugly.BuglyStrategy$a */
    /* compiled from: BUGLY */
    public static class C4791a {
        public static final int CRASHTYPE_ANR = 4;
        public static final int CRASHTYPE_BLOCK = 7;
        public static final int CRASHTYPE_COCOS2DX_JS = 5;
        public static final int CRASHTYPE_COCOS2DX_LUA = 6;
        public static final int CRASHTYPE_JAVA_CATCH = 1;
        public static final int CRASHTYPE_JAVA_CRASH = 0;
        public static final int CRASHTYPE_NATIVE = 2;
        public static final int CRASHTYPE_U3D = 3;
        public static final int MAX_USERDATA_KEY_LENGTH = 100;
        public static final int MAX_USERDATA_VALUE_LENGTH = 30000;

        public synchronized Map<String, String> onCrashHandleStart(int i, String str, String str2, String str3) {
            return null;
        }

        public synchronized byte[] onCrashHandleStart2GetExtraDatas(int i, String str, String str2, String str3) {
            return null;
        }
    }

    public synchronized String getAppChannel() {
        if (this.f10392b == null) {
            return C4806a.m16492b().f10534l;
        }
        return this.f10392b;
    }

    public synchronized String getAppPackageName() {
        if (this.f10393c == null) {
            return C4806a.m16492b().f10525c;
        }
        return this.f10393c;
    }

    public synchronized long getAppReportDelay() {
        return this.f10394d;
    }

    public synchronized String getAppVersion() {
        if (this.f10391a == null) {
            return C4806a.m16492b().f10532j;
        }
        return this.f10391a;
    }

    public synchronized C4791a getCrashHandleCallback() {
        return this.f10405o;
    }

    public synchronized String getDeviceID() {
        return this.f10396f;
    }

    public synchronized String getLibBuglySOFilePath() {
        return this.f10395e;
    }

    public synchronized Class<?> getUserInfoActivity() {
        return this.f10400j;
    }

    public synchronized boolean isBuglyLogUpload() {
        return this.f10401k;
    }

    public synchronized boolean isEnableANRCrashMonitor() {
        return this.f10398h;
    }

    public synchronized boolean isEnableNativeCrashMonitor() {
        return this.f10397g;
    }

    public synchronized boolean isEnableUserInfo() {
        return this.f10399i;
    }

    public boolean isReplaceOldChannel() {
        return this.f10402l;
    }

    public synchronized boolean isUploadProcess() {
        return this.f10403m;
    }

    public synchronized boolean recordUserInfoOnceADay() {
        return this.f10404n;
    }

    public synchronized BuglyStrategy setAppChannel(String str) {
        this.f10392b = str;
        return this;
    }

    public synchronized BuglyStrategy setAppPackageName(String str) {
        this.f10393c = str;
        return this;
    }

    public synchronized BuglyStrategy setAppReportDelay(long j) {
        this.f10394d = j;
        return this;
    }

    public synchronized BuglyStrategy setAppVersion(String str) {
        this.f10391a = str;
        return this;
    }

    public synchronized BuglyStrategy setBuglyLogUpload(boolean z) {
        this.f10401k = z;
        return this;
    }

    public synchronized BuglyStrategy setCrashHandleCallback(C4791a aVar) {
        this.f10405o = aVar;
        return this;
    }

    public synchronized BuglyStrategy setDeviceID(String str) {
        this.f10396f = str;
        return this;
    }

    public synchronized BuglyStrategy setEnableANRCrashMonitor(boolean z) {
        this.f10398h = z;
        return this;
    }

    public synchronized BuglyStrategy setEnableNativeCrashMonitor(boolean z) {
        this.f10397g = z;
        return this;
    }

    public synchronized BuglyStrategy setEnableUserInfo(boolean z) {
        this.f10399i = z;
        return this;
    }

    public synchronized BuglyStrategy setLibBuglySOFilePath(String str) {
        this.f10395e = str;
        return this;
    }

    public synchronized BuglyStrategy setRecordUserInfoOnceADay(boolean z) {
        this.f10404n = z;
        return this;
    }

    public void setReplaceOldChannel(boolean z) {
        this.f10402l = z;
    }

    public synchronized BuglyStrategy setUploadProcess(boolean z) {
        this.f10403m = z;
        return this;
    }

    public synchronized BuglyStrategy setUserInfoActivity(Class<?> cls) {
        this.f10400j = cls;
        return this;
    }
}
