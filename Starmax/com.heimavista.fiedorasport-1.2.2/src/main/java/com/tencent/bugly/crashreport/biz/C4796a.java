package com.tencent.bugly.crashreport.biz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4880t;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.List;

/* renamed from: com.tencent.bugly.crashreport.biz.a */
/* compiled from: BUGLY */
public final class C4796a {

    /* renamed from: a */
    private Context f10439a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public long f10440b;

    /* renamed from: c */
    private int f10441c;

    /* renamed from: d */
    private boolean f10442d = true;

    /* renamed from: com.tencent.bugly.crashreport.biz.a$a */
    /* compiled from: BUGLY */
    class C4797a implements C4880t {

        /* renamed from: a */
        private /* synthetic */ List f10443a;

        C4797a(List list) {
            this.f10443a = list;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
         arg types: [com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void */
        /* renamed from: a */
        public final void mo26637a(boolean z) {
            if (z) {
                C4888x.m16982c("[UserInfo] Successfully uploaded user info.", new Object[0]);
                long currentTimeMillis = System.currentTimeMillis();
                for (UserInfoBean userInfoBean : this.f10443a) {
                    userInfoBean.f10425f = currentTimeMillis;
                    C4796a.m16447a(C4796a.this, userInfoBean, true);
                }
            }
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$b */
    /* compiled from: BUGLY */
    class C4798b implements Runnable {
        C4798b() {
        }

        public final void run() {
            try {
                C4796a.this.m16450c();
            } catch (Throwable th) {
                C4888x.m16978a(th);
            }
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$c */
    /* compiled from: BUGLY */
    class C4799c implements Runnable {

        /* renamed from: P */
        private boolean f10446P;

        /* renamed from: Q */
        private UserInfoBean f10447Q;

        public C4799c(UserInfoBean userInfoBean, boolean z) {
            this.f10447Q = userInfoBean;
            this.f10446P = z;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
         arg types: [com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void */
        public final void run() {
            C4806a b;
            try {
                if (this.f10447Q != null) {
                    UserInfoBean userInfoBean = this.f10447Q;
                    if (!(userInfoBean == null || (b = C4806a.m16492b()) == null)) {
                        userInfoBean.f10429j = b.mo26687e();
                    }
                    C4888x.m16982c("[UserInfo] Record user info.", new Object[0]);
                    C4796a.m16447a(C4796a.this, this.f10447Q, false);
                }
                if (this.f10446P) {
                    C4796a aVar = C4796a.this;
                    C4886w a = C4886w.m16969a();
                    if (a != null) {
                        a.mo26934a(new C4798b());
                    }
                }
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$d */
    /* compiled from: BUGLY */
    class C4800d implements Runnable {
        C4800d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
         arg types: [int, int, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
        public final void run() {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis < C4796a.this.f10440b) {
                C4886w.m16969a().mo26935a(new C4800d(), (C4796a.this.f10440b - currentTimeMillis) + DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                return;
            }
            C4796a.this.mo26635a(3, false, 0L);
            C4796a.this.mo26634a();
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$e */
    /* compiled from: BUGLY */
    class C4801e implements Runnable {

        /* renamed from: P */
        private long f10450P = 21600000;

        public C4801e(long j) {
            this.f10450P = j;
        }

        public final void run() {
            C4796a aVar = C4796a.this;
            C4886w a = C4886w.m16969a();
            if (a != null) {
                a.mo26934a(new C4798b());
            }
            C4796a aVar2 = C4796a.this;
            long j = this.f10450P;
            C4886w.m16969a().mo26935a(new C4801e(j), j);
        }
    }

    public C4796a(Context context, boolean z) {
        this.f10439a = context;
        this.f10442d = z;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f7  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m16450c() {
        /*
            r14 = this;
            monitor-enter(r14)
            boolean r0 = r14.f10442d     // Catch:{ all -> 0x018b }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r14)
            return
        L_0x0007:
            com.tencent.bugly.proguard.u r0 = com.tencent.bugly.proguard.C4881u.m16932a()     // Catch:{ all -> 0x018b }
            if (r0 != 0) goto L_0x000f
            monitor-exit(r14)
            return
        L_0x000f:
            com.tencent.bugly.crashreport.common.strategy.a r1 = com.tencent.bugly.crashreport.common.strategy.C4809a.m16589a()     // Catch:{ all -> 0x018b }
            if (r1 != 0) goto L_0x0017
            monitor-exit(r14)
            return
        L_0x0017:
            boolean r1 = r1.mo26719b()     // Catch:{ all -> 0x018b }
            if (r1 == 0) goto L_0x0027
            r1 = 1001(0x3e9, float:1.403E-42)
            boolean r1 = r0.mo26926b(r1)     // Catch:{ all -> 0x018b }
            if (r1 != 0) goto L_0x0027
            monitor-exit(r14)
            return
        L_0x0027:
            android.content.Context r1 = r14.f10439a     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.info.a r1 = com.tencent.bugly.crashreport.common.info.C4806a.m16491a(r1)     // Catch:{ all -> 0x018b }
            java.lang.String r1 = r1.f10526d     // Catch:{ all -> 0x018b }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x018b }
            r2.<init>()     // Catch:{ all -> 0x018b }
            java.util.List r1 = r14.mo26633a(r1)     // Catch:{ all -> 0x018b }
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x00e6
            int r5 = r1.size()     // Catch:{ all -> 0x018b }
            int r5 = r5 + -20
            if (r5 <= 0) goto L_0x008b
            r6 = 0
        L_0x0045:
            int r7 = r1.size()     // Catch:{ all -> 0x018b }
            int r7 = r7 - r4
            if (r6 >= r7) goto L_0x007e
            int r7 = r6 + 1
            r8 = r7
        L_0x004f:
            int r9 = r1.size()     // Catch:{ all -> 0x018b }
            if (r8 >= r9) goto L_0x007c
            java.lang.Object r9 = r1.get(r6)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r9 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r9     // Catch:{ all -> 0x018b }
            long r9 = r9.f10424e     // Catch:{ all -> 0x018b }
            java.lang.Object r11 = r1.get(r8)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r11 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r11     // Catch:{ all -> 0x018b }
            long r11 = r11.f10424e     // Catch:{ all -> 0x018b }
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 <= 0) goto L_0x0079
            java.lang.Object r9 = r1.get(r6)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r9 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r9     // Catch:{ all -> 0x018b }
            java.lang.Object r10 = r1.get(r8)     // Catch:{ all -> 0x018b }
            r1.set(r6, r10)     // Catch:{ all -> 0x018b }
            r1.set(r8, r9)     // Catch:{ all -> 0x018b }
        L_0x0079:
            int r8 = r8 + 1
            goto L_0x004f
        L_0x007c:
            r6 = r7
            goto L_0x0045
        L_0x007e:
            r6 = 0
        L_0x007f:
            if (r6 >= r5) goto L_0x008b
            java.lang.Object r7 = r1.get(r6)     // Catch:{ all -> 0x018b }
            r2.add(r7)     // Catch:{ all -> 0x018b }
            int r6 = r6 + 1
            goto L_0x007f
        L_0x008b:
            java.util.Iterator r5 = r1.iterator()     // Catch:{ all -> 0x018b }
            r6 = 0
        L_0x0090:
            boolean r7 = r5.hasNext()     // Catch:{ all -> 0x018b }
            if (r7 == 0) goto L_0x00d3
            java.lang.Object r7 = r5.next()     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r7 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r7     // Catch:{ all -> 0x018b }
            long r8 = r7.f10425f     // Catch:{ all -> 0x018b }
            r10 = -1
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 == 0) goto L_0x00b4
            r5.remove()     // Catch:{ all -> 0x018b }
            long r8 = r7.f10424e     // Catch:{ all -> 0x018b }
            long r10 = com.tencent.bugly.proguard.C4893z.m17032b()     // Catch:{ all -> 0x018b }
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 >= 0) goto L_0x00b4
            r2.add(r7)     // Catch:{ all -> 0x018b }
        L_0x00b4:
            long r8 = r7.f10424e     // Catch:{ all -> 0x018b }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x018b }
            r12 = 600000(0x927c0, double:2.964394E-318)
            long r10 = r10 - r12
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x0090
            int r8 = r7.f10421b     // Catch:{ all -> 0x018b }
            if (r8 == r4) goto L_0x00d0
            int r8 = r7.f10421b     // Catch:{ all -> 0x018b }
            r9 = 4
            if (r8 == r9) goto L_0x00d0
            int r7 = r7.f10421b     // Catch:{ all -> 0x018b }
            r8 = 3
            if (r7 != r8) goto L_0x0090
        L_0x00d0:
            int r6 = r6 + 1
            goto L_0x0090
        L_0x00d3:
            r5 = 15
            if (r6 <= r5) goto L_0x00eb
            java.lang.String r5 = "[UserInfo] Upload user info too many times in 10 min: %d"
            java.lang.Object[] r7 = new java.lang.Object[r4]     // Catch:{ all -> 0x018b }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x018b }
            r7[r3] = r6     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16983d(r5, r7)     // Catch:{ all -> 0x018b }
            r5 = 0
            goto L_0x00ec
        L_0x00e6:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x018b }
            r1.<init>()     // Catch:{ all -> 0x018b }
        L_0x00eb:
            r5 = 1
        L_0x00ec:
            int r6 = r2.size()     // Catch:{ all -> 0x018b }
            if (r6 <= 0) goto L_0x00f5
            m16448a(r2)     // Catch:{ all -> 0x018b }
        L_0x00f5:
            if (r5 == 0) goto L_0x0182
            int r2 = r1.size()     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x00ff
            goto L_0x0182
        L_0x00ff:
            java.lang.String r2 = "[UserInfo] Upload user info(size: %d)"
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x018b }
            int r6 = r1.size()     // Catch:{ all -> 0x018b }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x018b }
            r5[r3] = r6     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16982c(r2, r5)     // Catch:{ all -> 0x018b }
            int r2 = r14.f10441c     // Catch:{ all -> 0x018b }
            if (r2 != r4) goto L_0x0116
            r2 = 1
            goto L_0x0117
        L_0x0116:
            r2 = 2
        L_0x0117:
            com.tencent.bugly.proguard.au r2 = com.tencent.bugly.proguard.C4836a.m16723a(r1, r2)     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x0126
            java.lang.String r0 = "[UserInfo] Failed to create UserInfoPackage."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0126:
            byte[] r2 = com.tencent.bugly.proguard.C4836a.m16729a(r2)     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x0135
            java.lang.String r0 = "[UserInfo] Failed to encode data."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0135:
            boolean r5 = r0.f10992a     // Catch:{ all -> 0x018b }
            if (r5 == 0) goto L_0x013c
            r5 = 840(0x348, float:1.177E-42)
            goto L_0x013e
        L_0x013c:
            r5 = 640(0x280, float:8.97E-43)
        L_0x013e:
            android.content.Context r6 = r14.f10439a     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.ap r9 = com.tencent.bugly.proguard.C4836a.m16720a(r6, r5, r2)     // Catch:{ all -> 0x018b }
            if (r9 != 0) goto L_0x014f
            java.lang.String r0 = "[UserInfo] Request package is null."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x014f:
            com.tencent.bugly.crashreport.biz.a$a r12 = new com.tencent.bugly.crashreport.biz.a$a     // Catch:{ all -> 0x018b }
            r12.<init>(r1)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.strategy.a r1 = com.tencent.bugly.crashreport.common.strategy.C4809a.m16589a()     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r1 = r1.mo26720c()     // Catch:{ all -> 0x018b }
            boolean r2 = r0.f10992a     // Catch:{ all -> 0x018b }
            if (r2 == 0) goto L_0x0163
            java.lang.String r1 = r1.f10571r     // Catch:{ all -> 0x018b }
            goto L_0x0165
        L_0x0163:
            java.lang.String r1 = r1.f10573t     // Catch:{ all -> 0x018b }
        L_0x0165:
            r10 = r1
            boolean r0 = r0.f10992a     // Catch:{ all -> 0x018b }
            if (r0 == 0) goto L_0x016d
            java.lang.String r0 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f10555b     // Catch:{ all -> 0x018b }
            goto L_0x016f
        L_0x016d:
            java.lang.String r0 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f10554a     // Catch:{ all -> 0x018b }
        L_0x016f:
            r11 = r0
            com.tencent.bugly.proguard.u r7 = com.tencent.bugly.proguard.C4881u.m16932a()     // Catch:{ all -> 0x018b }
            r8 = 1001(0x3e9, float:1.403E-42)
            int r0 = r14.f10441c     // Catch:{ all -> 0x018b }
            if (r0 != r4) goto L_0x017c
            r13 = 1
            goto L_0x017d
        L_0x017c:
            r13 = 0
        L_0x017d:
            r7.mo26919a(r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0182:
            java.lang.String r0 = "[UserInfo] There is no user info in local database."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x018b:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C4796a.m16450c():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o, boolean):long
     arg types: [java.lang.String, android.content.ContentValues, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o, boolean):long */
    /* renamed from: a */
    static /* synthetic */ void m16447a(C4796a aVar, UserInfoBean userInfoBean, boolean z) {
        List<UserInfoBean> a;
        if (userInfoBean == null) {
            return;
        }
        if (z || userInfoBean.f10421b == 1 || (a = aVar.mo26633a(C4806a.m16491a(aVar.f10439a).f10526d)) == null || a.size() < 20) {
            long a2 = C4875p.m16899a().mo26897a("t_ui", m16444a(userInfoBean), (C4874o) null, true);
            if (a2 >= 0) {
                C4888x.m16982c("[Database] insert %s success with ID: %d", "t_ui", Long.valueOf(a2));
                userInfoBean.f10420a = a2;
                return;
            }
            return;
        }
        C4888x.m16977a("[UserInfo] There are too many user info in local: %d", Integer.valueOf(a.size()));
    }

    /* renamed from: b */
    public final void mo26636b() {
        C4886w a = C4886w.m16969a();
        if (a != null) {
            a.mo26934a(new C4798b());
        }
    }

    /* renamed from: a */
    public final void mo26635a(int i, boolean z, long j) {
        C4809a a = C4809a.m16589a();
        int i2 = 0;
        if (a == null || a.mo26720c().f10561h || i == 1 || i == 3) {
            if (i == 1 || i == 3) {
                this.f10441c++;
            }
            C4806a a2 = C4806a.m16491a(this.f10439a);
            UserInfoBean userInfoBean = new UserInfoBean();
            userInfoBean.f10421b = i;
            userInfoBean.f10422c = a2.f10526d;
            userInfoBean.f10423d = a2.mo26691g();
            userInfoBean.f10424e = System.currentTimeMillis();
            userInfoBean.f10425f = -1;
            userInfoBean.f10433n = a2.f10532j;
            if (i == 1) {
                i2 = 1;
            }
            userInfoBean.f10434o = i2;
            userInfoBean.f10431l = a2.mo26678a();
            userInfoBean.f10432m = a2.f10538p;
            userInfoBean.f10426g = a2.f10539q;
            userInfoBean.f10427h = a2.f10540r;
            userInfoBean.f10428i = a2.f10541s;
            userInfoBean.f10430k = a2.f10542t;
            userInfoBean.f10437r = a2.mo26656B();
            userInfoBean.f10438s = a2.mo26661G();
            userInfoBean.f10435p = a2.mo26662H();
            userInfoBean.f10436q = a2.mo26663I();
            C4886w.m16969a().mo26935a(new C4799c(userInfoBean, z), 0);
            return;
        }
        C4888x.m16984e("UserInfo is disable", new Object[0]);
    }

    /* renamed from: a */
    public final void mo26634a() {
        this.f10440b = C4893z.m17032b() + 86400000;
        C4886w.m16969a().mo26935a(new C4800d(), (this.f10440b - System.currentTimeMillis()) + DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad A[Catch:{ all -> 0x00b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b2 A[DONT_GENERATE] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.tencent.bugly.crashreport.biz.UserInfoBean> mo26633a(java.lang.String r12) {
        /*
            r11 = this;
            r0 = 0
            boolean r1 = com.tencent.bugly.proguard.C4893z.m17024a(r12)     // Catch:{ all -> 0x00a5 }
            if (r1 == 0) goto L_0x0009
            r4 = r0
            goto L_0x001d
        L_0x0009:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = "_pc = '"
            r1.<init>(r2)     // Catch:{ all -> 0x00a5 }
            r1.append(r12)     // Catch:{ all -> 0x00a5 }
            java.lang.String r12 = "'"
            r1.append(r12)     // Catch:{ all -> 0x00a5 }
            java.lang.String r12 = r1.toString()     // Catch:{ all -> 0x00a5 }
            r4 = r12
        L_0x001d:
            com.tencent.bugly.proguard.p r1 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = "t_ui"
            r3 = 0
            r5 = 0
            r6 = 0
            r7 = 1
            android.database.Cursor r12 = r1.mo26898a(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00a5 }
            if (r12 != 0) goto L_0x0033
            if (r12 == 0) goto L_0x0032
            r12.close()
        L_0x0032:
            return r0
        L_0x0033:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a3 }
            r1.<init>()     // Catch:{ all -> 0x00a3 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x00a3 }
            r2.<init>()     // Catch:{ all -> 0x00a3 }
        L_0x003d:
            boolean r3 = r12.moveToNext()     // Catch:{ all -> 0x00a3 }
            r4 = 0
            if (r3 == 0) goto L_0x006e
            com.tencent.bugly.crashreport.biz.UserInfoBean r3 = m16445a(r12)     // Catch:{ all -> 0x00a3 }
            if (r3 == 0) goto L_0x004e
            r2.add(r3)     // Catch:{ all -> 0x00a3 }
            goto L_0x003d
        L_0x004e:
            java.lang.String r3 = "_id"
            int r3 = r12.getColumnIndex(r3)     // Catch:{ all -> 0x0066 }
            long r5 = r12.getLong(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = " or _id"
            r1.append(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = " = "
            r1.append(r3)     // Catch:{ all -> 0x0066 }
            r1.append(r5)     // Catch:{ all -> 0x0066 }
            goto L_0x003d
        L_0x0066:
            java.lang.String r3 = "[Database] unknown id."
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00a3 }
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r4)     // Catch:{ all -> 0x00a3 }
            goto L_0x003d
        L_0x006e:
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00a3 }
            int r3 = r1.length()     // Catch:{ all -> 0x00a3 }
            if (r3 <= 0) goto L_0x009d
            r3 = 4
            java.lang.String r7 = r1.substring(r3)     // Catch:{ all -> 0x00a3 }
            com.tencent.bugly.proguard.p r5 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00a3 }
            java.lang.String r6 = "t_ui"
            r8 = 0
            r9 = 0
            r10 = 1
            int r1 = r5.mo26896a(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x00a3 }
            java.lang.String r3 = "[Database] deleted %s error data %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00a3 }
            java.lang.String r6 = "t_ui"
            r5[r4] = r6     // Catch:{ all -> 0x00a3 }
            r4 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x00a3 }
            r5[r4] = r1     // Catch:{ all -> 0x00a3 }
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r5)     // Catch:{ all -> 0x00a3 }
        L_0x009d:
            if (r12 == 0) goto L_0x00a2
            r12.close()
        L_0x00a2:
            return r2
        L_0x00a3:
            r1 = move-exception
            goto L_0x00a7
        L_0x00a5:
            r1 = move-exception
            r12 = r0
        L_0x00a7:
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r1)     // Catch:{ all -> 0x00b6 }
            if (r2 != 0) goto L_0x00b0
            r1.printStackTrace()     // Catch:{ all -> 0x00b6 }
        L_0x00b0:
            if (r12 == 0) goto L_0x00b5
            r12.close()
        L_0x00b5:
            return r0
        L_0x00b6:
            r0 = move-exception
            if (r12 == 0) goto L_0x00bc
            r12.close()
        L_0x00bc:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C4796a.mo26633a(java.lang.String):java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* renamed from: a */
    private static void m16448a(List<UserInfoBean> list) {
        if (list != null && list.size() != 0) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < list.size() && i < 50) {
                sb.append(" or _id");
                sb.append(" = ");
                sb.append(list.get(i).f10420a);
                i++;
            }
            String sb2 = sb.toString();
            if (sb2.length() > 0) {
                sb2 = sb2.substring(4);
            }
            String str = sb2;
            sb.setLength(0);
            try {
                C4888x.m16982c("[Database] deleted %s data %d", "t_ui", Integer.valueOf(C4875p.m16899a().mo26896a("t_ui", str, (String[]) null, (C4874o) null, true)));
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    private static ContentValues m16444a(UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            return null;
        }
        try {
            ContentValues contentValues = new ContentValues();
            if (userInfoBean.f10420a > 0) {
                contentValues.put("_id", Long.valueOf(userInfoBean.f10420a));
            }
            contentValues.put("_tm", Long.valueOf(userInfoBean.f10424e));
            contentValues.put("_ut", Long.valueOf(userInfoBean.f10425f));
            contentValues.put("_tp", Integer.valueOf(userInfoBean.f10421b));
            contentValues.put("_pc", userInfoBean.f10422c);
            contentValues.put("_dt", C4893z.m17027a(userInfoBean));
            return contentValues;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    private static UserInfoBean m16445a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            byte[] blob = cursor.getBlob(cursor.getColumnIndex("_dt"));
            if (blob == null) {
                return null;
            }
            long j = cursor.getLong(cursor.getColumnIndex("_id"));
            UserInfoBean userInfoBean = (UserInfoBean) C4893z.m17007a(blob, UserInfoBean.CREATOR);
            if (userInfoBean != null) {
                userInfoBean.f10420a = j;
            }
            return userInfoBean;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }
}
