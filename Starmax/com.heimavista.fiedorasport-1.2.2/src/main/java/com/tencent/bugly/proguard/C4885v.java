package com.tencent.bugly.proguard;

import android.content.Context;
import com.google.android.exoplayer2.C1750C;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import java.util.Map;
import java.util.UUID;

/* renamed from: com.tencent.bugly.proguard.v */
/* compiled from: BUGLY */
public final class C4885v implements Runnable {

    /* renamed from: a */
    private int f11020a;

    /* renamed from: b */
    private int f11021b;

    /* renamed from: c */
    private final Context f11022c;

    /* renamed from: d */
    private final int f11023d;

    /* renamed from: e */
    private final byte[] f11024e;

    /* renamed from: f */
    private final C4806a f11025f;

    /* renamed from: g */
    private final C4809a f11026g;

    /* renamed from: h */
    private final C4879s f11027h;

    /* renamed from: i */
    private final C4881u f11028i;

    /* renamed from: j */
    private final int f11029j;

    /* renamed from: k */
    private final C4880t f11030k;

    /* renamed from: l */
    private final C4880t f11031l;

    /* renamed from: m */
    private String f11032m;

    /* renamed from: n */
    private final String f11033n;

    /* renamed from: o */
    private final Map<String, String> f11034o;

    /* renamed from: p */
    private int f11035p;

    /* renamed from: q */
    private long f11036q;

    /* renamed from: r */
    private long f11037r;

    /* renamed from: s */
    private boolean f11038s;

    /* renamed from: t */
    private boolean f11039t;

    public C4885v(Context context, int i, int i2, byte[] bArr, String str, String str2, C4880t tVar, boolean z, boolean z2) {
        this(context, i, i2, bArr, str, str2, tVar, z, 2, BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH, z2, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m16965a(com.tencent.bugly.proguard.C4853aq r4, boolean r5, int r6, java.lang.String r7, int r8) {
        /*
            r3 = this;
            int r4 = r3.f11023d
            r0 = 630(0x276, float:8.83E-43)
            if (r4 == r0) goto L_0x001a
            r0 = 640(0x280, float:8.97E-43)
            if (r4 == r0) goto L_0x0017
            r0 = 830(0x33e, float:1.163E-42)
            if (r4 == r0) goto L_0x001a
            r0 = 840(0x348, float:1.177E-42)
            if (r4 == r0) goto L_0x0017
            java.lang.String r4 = java.lang.String.valueOf(r4)
            goto L_0x001c
        L_0x0017:
            java.lang.String r4 = "userinfo"
            goto L_0x001c
        L_0x001a:
            java.lang.String r4 = "crash"
        L_0x001c:
            r0 = 1
            r1 = 0
            if (r5 == 0) goto L_0x002a
            java.lang.Object[] r6 = new java.lang.Object[r0]
            r6[r1] = r4
            java.lang.String r4 = "[Upload] Success: %s"
            com.tencent.bugly.proguard.C4888x.m16977a(r4, r6)
            goto L_0x0047
        L_0x002a:
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r2[r1] = r6
            r2[r0] = r4
            r4 = 2
            r2[r4] = r7
            java.lang.String r4 = "[Upload] Failed to upload(%d) %s: %s"
            com.tencent.bugly.proguard.C4888x.m16984e(r4, r2)
            boolean r4 = r3.f11038s
            if (r4 == 0) goto L_0x0047
            com.tencent.bugly.proguard.u r4 = r3.f11028i
            r6 = 0
            r4.mo26920a(r8, r6)
        L_0x0047:
            long r6 = r3.f11036q
            long r0 = r3.f11037r
            long r6 = r6 + r0
            r0 = 0
            int r4 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r4 <= 0) goto L_0x0067
            com.tencent.bugly.proguard.u r4 = r3.f11028i
            boolean r6 = r3.f11039t
            long r6 = r4.mo26915a(r6)
            long r0 = r3.f11036q
            long r6 = r6 + r0
            long r0 = r3.f11037r
            long r6 = r6 + r0
            com.tencent.bugly.proguard.u r4 = r3.f11028i
            boolean r8 = r3.f11039t
            r4.mo26921a(r6, r8)
        L_0x0067:
            com.tencent.bugly.proguard.t r4 = r3.f11030k
            if (r4 == 0) goto L_0x006e
            r4.mo26637a(r5)
        L_0x006e:
            com.tencent.bugly.proguard.t r4 = r3.f11031l
            if (r4 == 0) goto L_0x0075
            r4.mo26637a(r5)
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4885v.m16965a(com.tencent.bugly.proguard.aq, boolean, int, java.lang.String, int):void");
    }

    /* renamed from: b */
    public final void mo26932b(long j) {
        this.f11037r += j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02f5, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x02f6, code lost:
        m16965a(null, false, 1, "status of server is " + r10, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x030d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0290, code lost:
        if (r10 == 0) goto L_0x033e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0292, code lost:
        if (r10 != 2) goto L_0x02f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x029d, code lost:
        if ((r7.f11036q + r7.f11037r) <= 0) goto L_0x02b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x029f, code lost:
        r7.f11028i.mo26921a((r7.f11028i.mo26915a(r7.f11039t) + r7.f11036q) + r7.f11037r, r7.f11039t);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x02b4, code lost:
        r7.f11028i.mo26920a(r10, (com.tencent.bugly.proguard.C4853aq) null);
        com.tencent.bugly.proguard.C4888x.m16977a("[Upload] Session ID is invalid, will try again immediately (pid=%d | tid=%d).", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
        r7.f11028i.mo26916a(r7.f11029j, r7.f11023d, r7.f11024e, r7.f11032m, r7.f11033n, r7.f11030k, r7.f11020a, r7.f11021b, true, r7.f11034o);
     */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01fc A[Catch:{ all -> 0x030e, all -> 0x0430 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0263 A[SYNTHETIC, Splitter:B:88:0x0263] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r21 = this;
            r7 = r21
            java.lang.String r0 = "[Upload] Failed to upload for no status header."
            java.lang.String r1 = "Bugly-Version"
            r2 = 0
            r7.f11035p = r2     // Catch:{ all -> 0x0430 }
            r3 = 0
            r7.f11036q = r3     // Catch:{ all -> 0x0430 }
            r7.f11037r = r3     // Catch:{ all -> 0x0430 }
            byte[] r5 = r7.f11024e     // Catch:{ all -> 0x0430 }
            android.content.Context r6 = r7.f11022c     // Catch:{ all -> 0x0430 }
            java.lang.String r6 = com.tencent.bugly.crashreport.common.info.C4807b.m16557c(r6)     // Catch:{ all -> 0x0430 }
            if (r6 != 0) goto L_0x0025
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "network is not available"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0025:
            if (r5 == 0) goto L_0x0424
            int r6 = r5.length     // Catch:{ all -> 0x0430 }
            if (r6 != 0) goto L_0x002c
            goto L_0x0424
        L_0x002c:
            java.lang.String r6 = "[Upload] Run upload task with cmd: %d"
            r8 = 1
            java.lang.Object[] r9 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            int r10 = r7.f11023d     // Catch:{ all -> 0x0430 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0430 }
            r9[r2] = r10     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r6, r9)     // Catch:{ all -> 0x0430 }
            android.content.Context r6 = r7.f11022c     // Catch:{ all -> 0x0430 }
            if (r6 == 0) goto L_0x0418
            com.tencent.bugly.crashreport.common.info.a r6 = r7.f11025f     // Catch:{ all -> 0x0430 }
            if (r6 == 0) goto L_0x0418
            com.tencent.bugly.crashreport.common.strategy.a r6 = r7.f11026g     // Catch:{ all -> 0x0430 }
            if (r6 == 0) goto L_0x0418
            com.tencent.bugly.proguard.s r6 = r7.f11027h     // Catch:{ all -> 0x0430 }
            if (r6 != 0) goto L_0x004e
            goto L_0x0418
        L_0x004e:
            com.tencent.bugly.crashreport.common.strategy.a r6 = r7.f11026g     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r6 = r6.mo26720c()     // Catch:{ all -> 0x0430 }
            if (r6 != 0) goto L_0x0062
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "illegal local strategy"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0062:
            java.util.HashMap r9 = new java.util.HashMap     // Catch:{ all -> 0x0430 }
            r9.<init>()     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "prodId"
            com.tencent.bugly.crashreport.common.info.a r11 = r7.f11025f     // Catch:{ all -> 0x0430 }
            java.lang.String r11 = r11.mo26689f()     // Catch:{ all -> 0x0430 }
            r9.put(r10, r11)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "bundleId"
            com.tencent.bugly.crashreport.common.info.a r11 = r7.f11025f     // Catch:{ all -> 0x0430 }
            java.lang.String r11 = r11.f10525c     // Catch:{ all -> 0x0430 }
            r9.put(r10, r11)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "appVer"
            com.tencent.bugly.crashreport.common.info.a r11 = r7.f11025f     // Catch:{ all -> 0x0430 }
            java.lang.String r11 = r11.f10532j     // Catch:{ all -> 0x0430 }
            r9.put(r10, r11)     // Catch:{ all -> 0x0430 }
            java.util.Map<java.lang.String, java.lang.String> r10 = r7.f11034o     // Catch:{ all -> 0x0430 }
            if (r10 == 0) goto L_0x008d
            java.util.Map<java.lang.String, java.lang.String> r10 = r7.f11034o     // Catch:{ all -> 0x0430 }
            r9.putAll(r10)     // Catch:{ all -> 0x0430 }
        L_0x008d:
            boolean r10 = r7.f11038s     // Catch:{ all -> 0x0430 }
            r11 = 2
            if (r10 == 0) goto L_0x00f7
            java.lang.String r10 = "cmd"
            int r12 = r7.f11023d     // Catch:{ all -> 0x0430 }
            java.lang.String r12 = java.lang.Integer.toString(r12)     // Catch:{ all -> 0x0430 }
            r9.put(r10, r12)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "platformId"
            java.lang.String r12 = java.lang.Byte.toString(r8)     // Catch:{ all -> 0x0430 }
            r9.put(r10, r12)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "sdkVer"
            com.tencent.bugly.crashreport.common.info.a r12 = r7.f11025f     // Catch:{ all -> 0x0430 }
            r12.getClass()     // Catch:{ all -> 0x0430 }
            java.lang.String r12 = "3.1.0"
            r9.put(r10, r12)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "strategylastUpdateTime"
            long r12 = r6.f10569p     // Catch:{ all -> 0x0430 }
            java.lang.String r6 = java.lang.Long.toString(r12)     // Catch:{ all -> 0x0430 }
            r9.put(r10, r6)     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.u r6 = r7.f11028i     // Catch:{ all -> 0x0430 }
            boolean r6 = r6.mo26922a(r9)     // Catch:{ all -> 0x0430 }
            if (r6 != 0) goto L_0x00d1
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to add security info to HTTP headers"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x00d1:
            byte[] r5 = com.tencent.bugly.proguard.C4893z.m17029a(r5, r11)     // Catch:{ all -> 0x0430 }
            if (r5 != 0) goto L_0x00e3
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to zip request body"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x00e3:
            com.tencent.bugly.proguard.u r6 = r7.f11028i     // Catch:{ all -> 0x0430 }
            byte[] r5 = r6.mo26923a(r5)     // Catch:{ all -> 0x0430 }
            if (r5 != 0) goto L_0x00f7
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to encrypt request body"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x00f7:
            com.tencent.bugly.proguard.u r6 = r7.f11028i     // Catch:{ all -> 0x0430 }
            int r10 = r7.f11029j     // Catch:{ all -> 0x0430 }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0430 }
            r6.mo26917a(r10, r12)     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.t r6 = r7.f11030k     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.t r6 = r7.f11031l     // Catch:{ all -> 0x0430 }
            java.lang.String r6 = r7.f11032m     // Catch:{ all -> 0x0430 }
            r10 = -1
            r12 = r6
            r6 = 0
            r10 = 0
            r13 = -1
        L_0x010d:
            int r14 = r6 + 1
            int r15 = r7.f11020a     // Catch:{ all -> 0x0430 }
            if (r6 >= r15) goto L_0x040c
            if (r14 <= r8) goto L_0x0139
            java.lang.String r6 = "[Upload] Failed to upload last time, wait and try(%d) again."
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r14)     // Catch:{ all -> 0x0430 }
            r10[r2] = r15     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r6, r10)     // Catch:{ all -> 0x0430 }
            int r6 = r7.f11021b     // Catch:{ all -> 0x0430 }
            long r3 = (long) r6     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4893z.m17037b(r3)     // Catch:{ all -> 0x0430 }
            int r3 = r7.f11020a     // Catch:{ all -> 0x0430 }
            if (r14 != r3) goto L_0x0139
            java.lang.String r3 = "[Upload] Use the back-up url at the last time: %s"
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            java.lang.String r6 = r7.f11033n     // Catch:{ all -> 0x0430 }
            r4[r2] = r6     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r4)     // Catch:{ all -> 0x0430 }
            java.lang.String r12 = r7.f11033n     // Catch:{ all -> 0x0430 }
        L_0x0139:
            java.lang.String r3 = "[Upload] Send %d bytes"
            java.lang.Object[] r4 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            int r6 = r5.length     // Catch:{ all -> 0x0430 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0430 }
            r4[r2] = r6     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r4)     // Catch:{ all -> 0x0430 }
            boolean r3 = r7.f11038s     // Catch:{ all -> 0x0430 }
            if (r3 == 0) goto L_0x0150
            java.lang.String r3 = m16964a(r12)     // Catch:{ all -> 0x0430 }
            r12 = r3
        L_0x0150:
            java.lang.String r3 = "[Upload] Upload to %s with cmd %d (pid=%d | tid=%d)."
            r4 = 4
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0430 }
            r4[r2] = r12     // Catch:{ all -> 0x0430 }
            int r6 = r7.f11023d     // Catch:{ all -> 0x0430 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0430 }
            r4[r8] = r6     // Catch:{ all -> 0x0430 }
            int r6 = android.os.Process.myPid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0430 }
            r4[r11] = r6     // Catch:{ all -> 0x0430 }
            int r6 = android.os.Process.myTid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0430 }
            r10 = 3
            r4[r10] = r6     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r4)     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.s r3 = r7.f11027h     // Catch:{ all -> 0x0430 }
            byte[] r3 = r3.mo26913a(r12, r5, r7, r9)     // Catch:{ all -> 0x0430 }
            java.lang.String r4 = "[Upload] Failed to upload(%d): %s"
            if (r3 != 0) goto L_0x0196
            java.lang.String r3 = "Failed to upload for no response!"
            java.lang.Object[] r6 = new java.lang.Object[r11]     // Catch:{ all -> 0x0430 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0430 }
            r6[r2] = r10     // Catch:{ all -> 0x0430 }
            r6[r8] = r3     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16984e(r4, r6)     // Catch:{ all -> 0x0430 }
            r6 = r14
            r3 = 0
            r10 = 1
            goto L_0x010d
        L_0x0196:
            com.tencent.bugly.proguard.s r6 = r7.f11027h     // Catch:{ all -> 0x0430 }
            java.util.Map<java.lang.String, java.lang.String> r6 = r6.f10989a     // Catch:{ all -> 0x0430 }
            boolean r15 = r7.f11038s     // Catch:{ all -> 0x0430 }
            if (r15 == 0) goto L_0x033d
            java.lang.String r15 = "status"
            if (r6 == 0) goto L_0x01f0
            int r16 = r6.size()     // Catch:{ all -> 0x0430 }
            if (r16 != 0) goto L_0x01a9
            goto L_0x01f0
        L_0x01a9:
            boolean r16 = r6.containsKey(r15)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = "[Upload] Headers does not contain %s"
            if (r16 != 0) goto L_0x01bb
            java.lang.Object[] r11 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            r11[r2] = r15     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r10, r11)     // Catch:{ all -> 0x0430 }
        L_0x01b8:
            r20 = r1
            goto L_0x01f9
        L_0x01bb:
            boolean r11 = r6.containsKey(r1)     // Catch:{ all -> 0x0430 }
            if (r11 != 0) goto L_0x01c9
            java.lang.Object[] r11 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            r11[r2] = r1     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r10, r11)     // Catch:{ all -> 0x0430 }
            goto L_0x01b8
        L_0x01c9:
            java.lang.Object r10 = r6.get(r1)     // Catch:{ all -> 0x0430 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x0430 }
            java.lang.String r11 = "bugly"
            boolean r11 = r10.contains(r11)     // Catch:{ all -> 0x0430 }
            if (r11 != 0) goto L_0x01e3
            java.lang.String r11 = "[Upload] Bugly version is not valid: %s"
            r20 = r1
            java.lang.Object[] r1 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            r1[r2] = r10     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r11, r1)     // Catch:{ all -> 0x0430 }
            goto L_0x01f9
        L_0x01e3:
            r20 = r1
            java.lang.String r1 = "[Upload] Bugly version from headers is: %s"
            java.lang.Object[] r11 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            r11[r2] = r10     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r1, r11)     // Catch:{ all -> 0x0430 }
            r1 = 1
            goto L_0x01fa
        L_0x01f0:
            r20 = r1
            java.lang.String r1 = "[Upload] Headers is empty."
            java.lang.Object[] r10 = new java.lang.Object[r2]     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16983d(r1, r10)     // Catch:{ all -> 0x0430 }
        L_0x01f9:
            r1 = 0
        L_0x01fa:
            if (r1 != 0) goto L_0x0263
            java.lang.String r1 = "[Upload] Headers from server is not valid, just try again (pid=%d | tid=%d)."
            r3 = 2
            java.lang.Object[] r10 = new java.lang.Object[r3]     // Catch:{ all -> 0x0430 }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0430 }
            r10[r2] = r3     // Catch:{ all -> 0x0430 }
            int r3 = android.os.Process.myTid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0430 }
            r10[r8] = r3     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r1, r10)     // Catch:{ all -> 0x0430 }
            r1 = 2
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x0430 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0430 }
            r3[r2] = r1     // Catch:{ all -> 0x0430 }
            r3[r8] = r0     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16984e(r4, r3)     // Catch:{ all -> 0x0430 }
            if (r6 == 0) goto L_0x0257
            java.util.Set r1 = r6.entrySet()     // Catch:{ all -> 0x0430 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0430 }
        L_0x0230:
            boolean r3 = r1.hasNext()     // Catch:{ all -> 0x0430 }
            if (r3 == 0) goto L_0x0257
            java.lang.Object r3 = r1.next()     // Catch:{ all -> 0x0430 }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ all -> 0x0430 }
            java.lang.String r4 = "[key]: %s, [value]: %s"
            r6 = 2
            java.lang.Object[] r10 = new java.lang.Object[r6]     // Catch:{ all -> 0x0430 }
            java.lang.Object r6 = r3.getKey()     // Catch:{ all -> 0x0430 }
            r10[r2] = r6     // Catch:{ all -> 0x0430 }
            java.lang.Object r3 = r3.getValue()     // Catch:{ all -> 0x0430 }
            r10[r8] = r3     // Catch:{ all -> 0x0430 }
            java.lang.String r3 = java.lang.String.format(r4, r10)     // Catch:{ all -> 0x0430 }
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r4)     // Catch:{ all -> 0x0430 }
            goto L_0x0230
        L_0x0257:
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r1)     // Catch:{ all -> 0x0430 }
            r6 = r14
            r1 = r20
            r3 = 0
            goto L_0x0339
        L_0x0263:
            java.lang.Object r1 = r6.get(r15)     // Catch:{ all -> 0x0312 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0312 }
            int r10 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0312 }
            java.lang.String r1 = "[Upload] Status from server is %d (pid=%d | tid=%d)."
            r11 = 3
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ all -> 0x030e }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x030e }
            r11[r2] = r13     // Catch:{ all -> 0x030e }
            int r13 = android.os.Process.myPid()     // Catch:{ all -> 0x030e }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x030e }
            r11[r8] = r13     // Catch:{ all -> 0x030e }
            int r13 = android.os.Process.myTid()     // Catch:{ all -> 0x030e }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x030e }
            r15 = 2
            r11[r15] = r13     // Catch:{ all -> 0x030e }
            com.tencent.bugly.proguard.C4888x.m16982c(r1, r11)     // Catch:{ all -> 0x030e }
            if (r10 == 0) goto L_0x033e
            if (r10 != r15) goto L_0x02f6
            long r0 = r7.f11036q     // Catch:{ all -> 0x0430 }
            long r3 = r7.f11037r     // Catch:{ all -> 0x0430 }
            long r0 = r0 + r3
            r17 = 0
            int r3 = (r0 > r17 ? 1 : (r0 == r17 ? 0 : -1))
            if (r3 <= 0) goto L_0x02b4
            com.tencent.bugly.proguard.u r0 = r7.f11028i     // Catch:{ all -> 0x0430 }
            boolean r1 = r7.f11039t     // Catch:{ all -> 0x0430 }
            long r0 = r0.mo26915a(r1)     // Catch:{ all -> 0x0430 }
            long r3 = r7.f11036q     // Catch:{ all -> 0x0430 }
            long r0 = r0 + r3
            long r3 = r7.f11037r     // Catch:{ all -> 0x0430 }
            long r0 = r0 + r3
            com.tencent.bugly.proguard.u r3 = r7.f11028i     // Catch:{ all -> 0x0430 }
            boolean r4 = r7.f11039t     // Catch:{ all -> 0x0430 }
            r3.mo26921a(r0, r4)     // Catch:{ all -> 0x0430 }
        L_0x02b4:
            com.tencent.bugly.proguard.u r0 = r7.f11028i     // Catch:{ all -> 0x0430 }
            r1 = 0
            r0.mo26920a(r10, r1)     // Catch:{ all -> 0x0430 }
            java.lang.String r0 = "[Upload] Session ID is invalid, will try again immediately (pid=%d | tid=%d)."
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0430 }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0430 }
            r1[r2] = r3     // Catch:{ all -> 0x0430 }
            int r2 = android.os.Process.myTid()     // Catch:{ all -> 0x0430 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0430 }
            r1[r8] = r2     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16977a(r0, r1)     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.u r9 = r7.f11028i     // Catch:{ all -> 0x0430 }
            int r10 = r7.f11029j     // Catch:{ all -> 0x0430 }
            int r11 = r7.f11023d     // Catch:{ all -> 0x0430 }
            byte[] r12 = r7.f11024e     // Catch:{ all -> 0x0430 }
            java.lang.String r13 = r7.f11032m     // Catch:{ all -> 0x0430 }
            java.lang.String r14 = r7.f11033n     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.t r15 = r7.f11030k     // Catch:{ all -> 0x0430 }
            int r0 = r7.f11020a     // Catch:{ all -> 0x0430 }
            int r1 = r7.f11021b     // Catch:{ all -> 0x0430 }
            r18 = 1
            java.util.Map<java.lang.String, java.lang.String> r2 = r7.f11034o     // Catch:{ all -> 0x0430 }
            r16 = r0
            r17 = r1
            r19 = r2
            r9.mo26916a(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ all -> 0x0430 }
            return
        L_0x02f6:
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0430 }
            java.lang.String r1 = "status of server is "
            r0.<init>(r1)     // Catch:{ all -> 0x0430 }
            r0.append(r10)     // Catch:{ all -> 0x0430 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0430 }
            r1 = r21
            r6 = r10
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x030e:
            r17 = 0
            r13 = r10
            goto L_0x0314
        L_0x0312:
            r17 = 0
        L_0x0314:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0430 }
            java.lang.String r3 = "[Upload] Failed to upload for format of status header is invalid: "
            r1.<init>(r3)     // Catch:{ all -> 0x0430 }
            java.lang.String r3 = java.lang.Integer.toString(r13)     // Catch:{ all -> 0x0430 }
            r1.append(r3)     // Catch:{ all -> 0x0430 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0430 }
            r3 = 2
            java.lang.Object[] r6 = new java.lang.Object[r3]     // Catch:{ all -> 0x0430 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0430 }
            r6[r2] = r3     // Catch:{ all -> 0x0430 }
            r6[r8] = r1     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16984e(r4, r6)     // Catch:{ all -> 0x0430 }
            r6 = r14
            r3 = r17
            r1 = r20
        L_0x0339:
            r10 = 1
            r11 = 2
            goto L_0x010d
        L_0x033d:
            r10 = r13
        L_0x033e:
            java.lang.String r0 = "[Upload] Received %d bytes"
            java.lang.Object[] r1 = new java.lang.Object[r8]     // Catch:{ all -> 0x0430 }
            int r4 = r3.length     // Catch:{ all -> 0x0430 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0430 }
            r1[r2] = r4     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r1)     // Catch:{ all -> 0x0430 }
            boolean r0 = r7.f11038s     // Catch:{ all -> 0x0430 }
            if (r0 == 0) goto L_0x03af
            int r0 = r3.length     // Catch:{ all -> 0x0430 }
            if (r0 != 0) goto L_0x0388
            java.util.Set r0 = r6.entrySet()     // Catch:{ all -> 0x0430 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0430 }
        L_0x035b:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0430 }
            if (r1 == 0) goto L_0x037c
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0430 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ all -> 0x0430 }
            java.lang.String r3 = "[Upload] HTTP headers from server: key = %s, value = %s"
            r4 = 2
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0430 }
            java.lang.Object r4 = r1.getKey()     // Catch:{ all -> 0x0430 }
            r5[r2] = r4     // Catch:{ all -> 0x0430 }
            java.lang.Object r1 = r1.getValue()     // Catch:{ all -> 0x0430 }
            r5[r8] = r1     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r5)     // Catch:{ all -> 0x0430 }
            goto L_0x035b
        L_0x037c:
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "response data from server is empty"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0388:
            com.tencent.bugly.proguard.u r0 = r7.f11028i     // Catch:{ all -> 0x0430 }
            byte[] r0 = r0.mo26927b(r3)     // Catch:{ all -> 0x0430 }
            if (r0 != 0) goto L_0x039c
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed to decrypt response from server"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x039c:
            r1 = 2
            byte[] r3 = com.tencent.bugly.proguard.C4893z.m17042b(r0, r1)     // Catch:{ all -> 0x0430 }
            if (r3 != 0) goto L_0x03af
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed unzip(Gzip) response from server"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x03af:
            boolean r0 = r7.f11038s     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.aq r0 = com.tencent.bugly.proguard.C4836a.m16721a(r3, r0)     // Catch:{ all -> 0x0430 }
            if (r0 != 0) goto L_0x03c3
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed to decode response package"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x03c3:
            boolean r1 = r7.f11038s     // Catch:{ all -> 0x0430 }
            if (r1 == 0) goto L_0x03cc
            com.tencent.bugly.proguard.u r1 = r7.f11028i     // Catch:{ all -> 0x0430 }
            r1.mo26920a(r10, r0)     // Catch:{ all -> 0x0430 }
        L_0x03cc:
            java.lang.String r1 = "[Upload] Response cmd is: %d, length of sBuffer is: %d"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0430 }
            int r4 = r0.f10870b     // Catch:{ all -> 0x0430 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0430 }
            r3[r2] = r4     // Catch:{ all -> 0x0430 }
            byte[] r4 = r0.f10871c     // Catch:{ all -> 0x0430 }
            if (r4 != 0) goto L_0x03de
            goto L_0x03e1
        L_0x03de:
            byte[] r2 = r0.f10871c     // Catch:{ all -> 0x0430 }
            int r2 = r2.length     // Catch:{ all -> 0x0430 }
        L_0x03e1:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0430 }
            r3[r8] = r2     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.proguard.C4888x.m16982c(r1, r3)     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.crashreport.common.info.a r1 = r7.f11025f     // Catch:{ all -> 0x0430 }
            com.tencent.bugly.crashreport.common.strategy.a r2 = r7.f11026g     // Catch:{ all -> 0x0430 }
            boolean r1 = m16966a(r0, r1, r2)     // Catch:{ all -> 0x0430 }
            if (r1 != 0) goto L_0x0400
            r3 = 0
            r4 = 2
            java.lang.String r5 = "failed to process response package"
            r6 = 0
            r1 = r21
            r2 = r0
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0400:
            r3 = 1
            r4 = 2
            java.lang.String r5 = "successfully uploaded"
            r6 = 0
            r1 = r21
            r2 = r0
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x040c:
            r2 = 0
            r3 = 0
            java.lang.String r5 = "failed after many attempts"
            r6 = 0
            r1 = r21
            r4 = r10
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0418:
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "illegal access error"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0424:
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "request package is empty!"
            r6 = 0
            r1 = r21
            r1.m16965a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0430 }
            return
        L_0x0430:
            r0 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C4888x.m16978a(r0)
            if (r1 != 0) goto L_0x043a
            r0.printStackTrace()
        L_0x043a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4885v.run():void");
    }

    public C4885v(Context context, int i, int i2, byte[] bArr, String str, String str2, C4880t tVar, boolean z, int i3, int i4, boolean z2, Map<String, String> map) {
        int i5 = i3;
        int i6 = i4;
        this.f11020a = 2;
        this.f11021b = BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH;
        this.f11032m = null;
        this.f11035p = 0;
        this.f11036q = 0;
        this.f11037r = 0;
        this.f11038s = true;
        this.f11039t = false;
        this.f11022c = context;
        this.f11025f = C4806a.m16491a(context);
        this.f11024e = bArr;
        this.f11026g = C4809a.m16589a();
        this.f11027h = C4879s.m16925a(context);
        this.f11028i = C4881u.m16932a();
        this.f11029j = i;
        this.f11032m = str;
        this.f11033n = str2;
        this.f11030k = tVar;
        this.f11031l = null;
        this.f11038s = z;
        this.f11023d = i2;
        if (i5 > 0) {
            this.f11020a = i5;
        }
        if (i6 > 0) {
            this.f11021b = i6;
        }
        this.f11039t = z2;
        this.f11034o = map;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean */
    /* renamed from: a */
    private static boolean m16966a(C4853aq aqVar, C4806a aVar, C4809a aVar2) {
        if (aqVar == null) {
            C4888x.m16983d("resp == null!", new Object[0]);
            return false;
        }
        byte b = aqVar.f10869a;
        if (b != 0) {
            C4888x.m16984e("resp result error %d", Byte.valueOf(b));
            return false;
        }
        try {
            if (!C4893z.m17024a(aqVar.f10872d)) {
                if (!C4806a.m16492b().mo26694i().equals(aqVar.f10872d)) {
                    C4875p.m16899a().mo26903a(C4809a.f10579a, "gateway", aqVar.f10872d.getBytes(C1750C.UTF8_NAME), (C4874o) null, true);
                    aVar.mo26686d(aqVar.f10872d);
                }
            }
            if (!C4893z.m17024a(aqVar.f10874f) && !C4806a.m16492b().mo26695j().equals(aqVar.f10874f)) {
                C4875p.m16899a().mo26903a(C4809a.f10579a, "device", aqVar.f10874f.getBytes(C1750C.UTF8_NAME), (C4874o) null, true);
                aVar.mo26688e(aqVar.f10874f);
            }
        } catch (Throwable th) {
            C4888x.m16978a(th);
        }
        aVar.f10531i = aqVar.f10873e;
        int i = aqVar.f10870b;
        if (i == 510) {
            byte[] bArr = aqVar.f10871c;
            if (bArr == null) {
                C4888x.m16984e("[Upload] Strategy data is null. Response cmd: %d", Integer.valueOf(i));
                return false;
            }
            C4855as asVar = (C4855as) C4836a.m16724a(bArr, C4855as.class);
            if (asVar == null) {
                C4888x.m16984e("[Upload] Failed to decode strategy from server. Response cmd: %d", Integer.valueOf(aqVar.f10870b));
                return false;
            }
            aVar2.mo26718a(asVar);
        }
        return true;
    }

    /* renamed from: a */
    public final void mo26931a(long j) {
        this.f11035p++;
        this.f11036q += j;
    }

    /* renamed from: a */
    private static String m16964a(String str) {
        if (C4893z.m17024a(str)) {
            return str;
        }
        try {
            return String.format("%s?aid=%s", str, UUID.randomUUID().toString());
        } catch (Throwable th) {
            C4888x.m16978a(th);
            return str;
        }
    }
}
