package com.tencent.bugly.crashreport.crash.jni;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.info.C4807b;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.crash.C4822b;
import com.tencent.bugly.crashreport.crash.C4824c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4889y;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.crash.jni.a */
/* compiled from: BUGLY */
public final class C4834a implements NativeExceptionHandler {

    /* renamed from: a */
    private final Context f10777a;

    /* renamed from: b */
    private final C4822b f10778b;

    /* renamed from: c */
    private final C4806a f10779c;

    /* renamed from: d */
    private final C4809a f10780d;

    public C4834a(Context context, C4806a aVar, C4822b bVar, C4809a aVar2) {
        this.f10777a = context;
        this.f10778b = bVar;
        this.f10779c = aVar;
        this.f10780d = aVar2;
    }

    public final void handleNativeException(int i, int i2, long j, long j2, String str, String str2, String str3, String str4, int i3, String str5, int i4, int i5, int i6, String str6, String str7) {
        C4888x.m16977a("Native Crash Happen v1", new Object[0]);
        handleNativeException2(i, i2, j, j2, str, str2, str3, str4, i3, str5, i4, i5, i6, str6, str7, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.bugly.crashreport.crash.jni.b.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.am
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.an
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:104:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ff A[Catch:{ all -> 0x029d }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01b4 A[Catch:{ all -> 0x029d }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01c7 A[SYNTHETIC, Splitter:B:64:0x01c7] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x022f A[Catch:{ all -> 0x0299 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0238 A[Catch:{ all -> 0x0299 }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x02a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleNativeException2(int r28, int r29, long r30, long r32, java.lang.String r34, java.lang.String r35, java.lang.String r36, java.lang.String r37, int r38, java.lang.String r39, int r40, int r41, int r42, java.lang.String r43, java.lang.String r44, java.lang.String[] r45) {
        /*
            r27 = this;
            r14 = r27
            r0 = r35
            r13 = r38
            r1 = r40
            r2 = r45
            r12 = 0
            java.lang.Object[] r3 = new java.lang.Object[r12]
            java.lang.String r4 = "Native Crash Happen v2"
            com.tencent.bugly.proguard.C4888x.m16977a(r4, r3)
            java.lang.String r11 = com.tencent.bugly.crashreport.crash.jni.C4835b.m16710a(r36)     // Catch:{ all -> 0x029d }
            java.lang.String r3 = "UNKNOWN"
            java.lang.String r4 = ")"
            java.lang.String r5 = "("
            if (r13 <= 0) goto L_0x003e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r1.<init>()     // Catch:{ all -> 0x029d }
            r6 = r34
            r1.append(r6)     // Catch:{ all -> 0x029d }
            r1.append(r5)     // Catch:{ all -> 0x029d }
            r7 = r39
            r1.append(r7)     // Catch:{ all -> 0x029d }
            r1.append(r4)     // Catch:{ all -> 0x029d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x029d }
            java.lang.String r6 = "KERNEL"
            r10 = r1
            r18 = r3
            r9 = r6
            goto L_0x006e
        L_0x003e:
            r6 = r34
            r7 = r39
            if (r1 <= 0) goto L_0x0048
            java.lang.String r3 = com.tencent.bugly.crashreport.common.info.AppInfo.m16477a(r40)     // Catch:{ all -> 0x029d }
        L_0x0048:
            java.lang.String r8 = java.lang.String.valueOf(r40)     // Catch:{ all -> 0x029d }
            boolean r8 = r3.equals(r8)     // Catch:{ all -> 0x029d }
            if (r8 != 0) goto L_0x006a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r8.<init>()     // Catch:{ all -> 0x029d }
            r8.append(r3)     // Catch:{ all -> 0x029d }
            r8.append(r5)     // Catch:{ all -> 0x029d }
            r8.append(r1)     // Catch:{ all -> 0x029d }
            r8.append(r4)     // Catch:{ all -> 0x029d }
            java.lang.String r1 = r8.toString()     // Catch:{ all -> 0x029d }
            r18 = r1
            goto L_0x006c
        L_0x006a:
            r18 = r3
        L_0x006c:
            r10 = r6
            r9 = r7
        L_0x006e:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x029d }
            r1.<init>()     // Catch:{ all -> 0x029d }
            if (r2 == 0) goto L_0x00ae
            r3 = 0
        L_0x0076:
            int r6 = r2.length     // Catch:{ all -> 0x029d }
            if (r3 >= r6) goto L_0x00b5
            r6 = r2[r3]     // Catch:{ all -> 0x029d }
            if (r6 == 0) goto L_0x00ab
            java.lang.String r7 = "Extra message[%d]: %s"
            r15 = 2
            java.lang.Object[] r8 = new java.lang.Object[r15]     // Catch:{ all -> 0x029d }
            java.lang.Integer r16 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x029d }
            r8[r12] = r16     // Catch:{ all -> 0x029d }
            r16 = 1
            r8[r16] = r6     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16977a(r7, r8)     // Catch:{ all -> 0x029d }
            java.lang.String r7 = "="
            java.lang.String[] r7 = r6.split(r7)     // Catch:{ all -> 0x029d }
            int r8 = r7.length     // Catch:{ all -> 0x029d }
            if (r8 != r15) goto L_0x00a1
            r6 = r7[r12]     // Catch:{ all -> 0x029d }
            r8 = 1
            r7 = r7[r8]     // Catch:{ all -> 0x029d }
            r1.put(r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x00ab
        L_0x00a1:
            java.lang.String r7 = "bad extraMsg %s"
            r8 = 1
            java.lang.Object[] r15 = new java.lang.Object[r8]     // Catch:{ all -> 0x029d }
            r15[r12] = r6     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16983d(r7, r15)     // Catch:{ all -> 0x029d }
        L_0x00ab:
            int r3 = r3 + 1
            goto L_0x0076
        L_0x00ae:
            java.lang.String r2 = "not found extraMsg"
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16982c(r2, r3)     // Catch:{ all -> 0x029d }
        L_0x00b5:
            java.lang.String r2 = "HasPendingException"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029d }
            if (r2 == 0) goto L_0x00d1
            java.lang.String r3 = "true"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x029d }
            if (r2 == 0) goto L_0x00d1
            java.lang.String r2 = "Native crash happened with a Java pending exception."
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16977a(r2, r3)     // Catch:{ all -> 0x029d }
            r19 = 1
            goto L_0x00d3
        L_0x00d1:
            r19 = 0
        L_0x00d3:
            java.lang.String r2 = "ExceptionProcessName"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029d }
            if (r2 == 0) goto L_0x00ef
            int r3 = r2.length()     // Catch:{ all -> 0x029d }
            if (r3 != 0) goto L_0x00e4
            goto L_0x00ef
        L_0x00e4:
            java.lang.String r3 = "Name of crash process: %s"
            r6 = 1
            java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ all -> 0x029d }
            r7[r12] = r2     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r7)     // Catch:{ all -> 0x029d }
            goto L_0x00f3
        L_0x00ef:
            com.tencent.bugly.crashreport.common.info.a r2 = r14.f10779c     // Catch:{ all -> 0x029d }
            java.lang.String r2 = r2.f10526d     // Catch:{ all -> 0x029d }
        L_0x00f3:
            r20 = r2
            java.lang.String r2 = "ExceptionThreadName"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029d }
            if (r2 == 0) goto L_0x016d
            int r3 = r2.length()     // Catch:{ all -> 0x029d }
            if (r3 != 0) goto L_0x0106
            goto L_0x016d
        L_0x0106:
            java.lang.String r3 = "Name of crash thread: %s"
            r8 = 1
            java.lang.Object[] r6 = new java.lang.Object[r8]     // Catch:{ all -> 0x029d }
            r6[r12] = r2     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r6)     // Catch:{ all -> 0x029d }
            java.util.Map r3 = java.lang.Thread.getAllStackTraces()     // Catch:{ all -> 0x029d }
            java.util.Set r3 = r3.keySet()     // Catch:{ all -> 0x029d }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x029d }
        L_0x011c:
            boolean r6 = r3.hasNext()     // Catch:{ all -> 0x029d }
            if (r6 == 0) goto L_0x014e
            java.lang.Object r6 = r3.next()     // Catch:{ all -> 0x029d }
            java.lang.Thread r6 = (java.lang.Thread) r6     // Catch:{ all -> 0x029d }
            java.lang.String r7 = r6.getName()     // Catch:{ all -> 0x029d }
            boolean r7 = r7.equals(r2)     // Catch:{ all -> 0x029d }
            if (r7 == 0) goto L_0x011c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r3.<init>()     // Catch:{ all -> 0x029d }
            r3.append(r2)     // Catch:{ all -> 0x029d }
            r3.append(r5)     // Catch:{ all -> 0x029d }
            long r6 = r6.getId()     // Catch:{ all -> 0x029d }
            r3.append(r6)     // Catch:{ all -> 0x029d }
            r3.append(r4)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x029d }
            r3 = r2
            r2 = 1
            goto L_0x0150
        L_0x014e:
            r3 = r2
            r2 = 0
        L_0x0150:
            if (r2 != 0) goto L_0x016a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r2.<init>()     // Catch:{ all -> 0x029d }
            r2.append(r3)     // Catch:{ all -> 0x029d }
            r2.append(r5)     // Catch:{ all -> 0x029d }
            r3 = r29
            r2.append(r3)     // Catch:{ all -> 0x029d }
            r2.append(r4)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x029d }
            goto L_0x018f
        L_0x016a:
            r21 = r3
            goto L_0x0191
        L_0x016d:
            r8 = 1
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x029d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r3.<init>()     // Catch:{ all -> 0x029d }
            java.lang.String r6 = r2.getName()     // Catch:{ all -> 0x029d }
            r3.append(r6)     // Catch:{ all -> 0x029d }
            r3.append(r5)     // Catch:{ all -> 0x029d }
            long r5 = r2.getId()     // Catch:{ all -> 0x029d }
            r3.append(r5)     // Catch:{ all -> 0x029d }
            r3.append(r4)     // Catch:{ all -> 0x029d }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x029d }
        L_0x018f:
            r21 = r2
        L_0x0191:
            r2 = 1000(0x3e8, double:4.94E-321)
            long r4 = r30 * r2
            long r2 = r32 / r2
            long r4 = r4 + r2
            java.lang.String r2 = "SysLogPath"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029d }
            r22 = r2
            java.lang.String r22 = (java.lang.String) r22     // Catch:{ all -> 0x029d }
            java.lang.String r2 = "JniLogPath"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x029d }
            r23 = r1
            java.lang.String r23 = (java.lang.String) r23     // Catch:{ all -> 0x029d }
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f10780d     // Catch:{ all -> 0x029d }
            boolean r1 = r1.mo26719b()     // Catch:{ all -> 0x029d }
            if (r1 != 0) goto L_0x01bb
            java.lang.String r1 = "no remote but still store!"
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16983d(r1, r2)     // Catch:{ all -> 0x029d }
        L_0x01bb:
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f10780d     // Catch:{ all -> 0x029d }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r1 = r1.mo26720c()     // Catch:{ all -> 0x029d }
            boolean r1 = r1.f10560g     // Catch:{ all -> 0x029d }
            java.lang.String r7 = "\n"
            if (r1 != 0) goto L_0x0208
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f10780d     // Catch:{ all -> 0x029d }
            boolean r1 = r1.mo26719b()     // Catch:{ all -> 0x029d }
            if (r1 == 0) goto L_0x0208
            java.lang.String r1 = "crash report was closed by remote , will not upload to Bugly , print local for helpful!"
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4888x.m16984e(r1, r2)     // Catch:{ all -> 0x029d }
            java.lang.String r1 = "NATIVE_CRASH"
            java.lang.String r2 = com.tencent.bugly.proguard.C4893z.m17008a()     // Catch:{ all -> 0x029d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029d }
            r3.<init>()     // Catch:{ all -> 0x029d }
            r3.append(r10)     // Catch:{ all -> 0x029d }
            r3.append(r7)     // Catch:{ all -> 0x029d }
            r3.append(r0)     // Catch:{ all -> 0x029d }
            r3.append(r7)     // Catch:{ all -> 0x029d }
            r3.append(r11)     // Catch:{ all -> 0x029d }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x029d }
            r3 = 0
            r29 = r1
            r30 = r2
            r31 = r20
            r32 = r21
            r33 = r0
            r34 = r3
            com.tencent.bugly.crashreport.crash.C4822b.m16639a(r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x029d }
            com.tencent.bugly.proguard.C4893z.m17039b(r37)     // Catch:{ all -> 0x029d }
            return
        L_0x0208:
            r15 = 0
            r16 = 0
            r17 = 1
            r1 = r27
            r2 = r20
            r3 = r21
            r6 = r10
            r24 = r7
            r7 = r35
            r8 = r11
            r25 = r10
            r10 = r18
            r26 = r11
            r11 = r37
            r12 = r22
            r13 = r23
            r14 = r44
            r18 = r19
            com.tencent.bugly.crashreport.crash.CrashDetailBean r1 = r1.packageCrashDatas(r2, r3, r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0299 }
            if (r1 != 0) goto L_0x0238
            java.lang.String r0 = "pkg crash datas fail!"
            r2 = 0
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x0299 }
            com.tencent.bugly.proguard.C4888x.m16984e(r0, r1)     // Catch:{ all -> 0x0299 }
            return
        L_0x0238:
            r2 = 0
            java.lang.String r3 = "NATIVE_CRASH"
            java.lang.String r4 = com.tencent.bugly.proguard.C4893z.m17008a()     // Catch:{ all -> 0x0299 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0299 }
            r5.<init>()     // Catch:{ all -> 0x0299 }
            r6 = r25
            r5.append(r6)     // Catch:{ all -> 0x0299 }
            r6 = r24
            r5.append(r6)     // Catch:{ all -> 0x0299 }
            r5.append(r0)     // Catch:{ all -> 0x0299 }
            r5.append(r6)     // Catch:{ all -> 0x0299 }
            r0 = r26
            r5.append(r0)     // Catch:{ all -> 0x0299 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0299 }
            r29 = r3
            r30 = r4
            r31 = r20
            r32 = r21
            r33 = r0
            r34 = r1
            com.tencent.bugly.crashreport.crash.C4822b.m16639a(r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x0299 }
            r3 = r27
            com.tencent.bugly.crashreport.crash.b r0 = r3.f10778b     // Catch:{ all -> 0x0297 }
            r4 = r38
            boolean r0 = r0.mo26750a(r1, r4)     // Catch:{ all -> 0x0297 }
            if (r0 != 0) goto L_0x0279
            r2 = 1
        L_0x0279:
            r0 = 0
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler r4 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.getInstance()     // Catch:{ all -> 0x0297 }
            if (r4 == 0) goto L_0x0284
            java.lang.String r0 = r4.getDumpFilePath()     // Catch:{ all -> 0x0297 }
        L_0x0284:
            r4 = 1
            com.tencent.bugly.crashreport.crash.jni.C4835b.m16713a(r4, r0)     // Catch:{ all -> 0x0297 }
            if (r2 == 0) goto L_0x0291
            com.tencent.bugly.crashreport.crash.b r0 = r3.f10778b     // Catch:{ all -> 0x0297 }
            r5 = 3000(0xbb8, double:1.482E-320)
            r0.mo26747a(r1, r5, r4)     // Catch:{ all -> 0x0297 }
        L_0x0291:
            com.tencent.bugly.crashreport.crash.b r0 = r3.f10778b     // Catch:{ all -> 0x0297 }
            r0.mo26751b(r1)     // Catch:{ all -> 0x0297 }
            return
        L_0x0297:
            r0 = move-exception
            goto L_0x029f
        L_0x0299:
            r0 = move-exception
            r3 = r27
            goto L_0x029f
        L_0x029d:
            r0 = move-exception
            r3 = r14
        L_0x029f:
            boolean r1 = com.tencent.bugly.proguard.C4888x.m16978a(r0)
            if (r1 != 0) goto L_0x02a8
            r0.printStackTrace()
        L_0x02a8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.C4834a.handleNativeException2(int, int, long, long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int, int, int, java.lang.String, java.lang.String, java.lang.String[]):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.z.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.z.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.z.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.z.a(byte[], int):byte[]
      com.tencent.bugly.proguard.z.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    public final CrashDetailBean packageCrashDatas(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, byte[] bArr, Map<String, String> map, boolean z, boolean z2) {
        int i;
        String str12;
        int indexOf;
        String str13 = str;
        String str14 = str8;
        byte[] bArr2 = bArr;
        boolean k = C4824c.m16656a().mo26768k();
        if (k) {
            C4888x.m16984e("This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!", new Object[0]);
        }
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        crashDetailBean.f10619b = 1;
        crashDetailBean.f10622e = this.f10779c.mo26693h();
        C4806a aVar = this.f10779c;
        crashDetailBean.f10623f = aVar.f10532j;
        crashDetailBean.f10624g = aVar.mo26708w();
        crashDetailBean.f10630m = this.f10779c.mo26691g();
        crashDetailBean.f10631n = str3;
        String str15 = "";
        crashDetailBean.f10632o = k ? " This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful![Bugly]" : str15;
        crashDetailBean.f10633p = str4;
        if (str5 != null) {
            str15 = str5;
        }
        crashDetailBean.f10634q = str15;
        crashDetailBean.f10635r = j;
        crashDetailBean.f10638u = C4893z.m17035b(crashDetailBean.f10634q.getBytes());
        crashDetailBean.f10595A = str13;
        crashDetailBean.f10596B = str2;
        crashDetailBean.f10603I = this.f10779c.mo26710y();
        crashDetailBean.f10625h = this.f10779c.mo26707v();
        crashDetailBean.f10626i = this.f10779c.mo26664J();
        crashDetailBean.f10639v = str14;
        NativeCrashHandler instance = NativeCrashHandler.getInstance();
        String dumpFilePath = instance != null ? instance.getDumpFilePath() : null;
        String a = C4835b.m16712a(dumpFilePath, str14);
        if (!C4893z.m17024a(a)) {
            crashDetailBean.f10615U = a;
        }
        crashDetailBean.f10616V = C4835b.m16714b(dumpFilePath);
        crashDetailBean.f10640w = C4835b.m16711a(str9, C4824c.f10693e, null, false);
        crashDetailBean.f10641x = C4835b.m16711a(str10, C4824c.f10693e, null, true);
        crashDetailBean.f10604J = str7;
        crashDetailBean.f10605K = str6;
        crashDetailBean.f10606L = str11;
        crashDetailBean.f10600F = this.f10779c.mo26701p();
        crashDetailBean.f10601G = this.f10779c.mo26700o();
        crashDetailBean.f10602H = this.f10779c.mo26702q();
        if (z) {
            crashDetailBean.f10597C = C4807b.m16572k();
            crashDetailBean.f10598D = C4807b.m16568i();
            crashDetailBean.f10599E = C4807b.m16576m();
            if (crashDetailBean.f10640w == null) {
                crashDetailBean.f10640w = C4893z.m17010a(this.f10777a, C4824c.f10693e, (String) null);
            }
            crashDetailBean.f10642y = C4889y.m16990a();
            C4806a aVar2 = this.f10779c;
            crashDetailBean.f10607M = aVar2.f10498a;
            crashDetailBean.f10608N = aVar2.mo26678a();
            crashDetailBean.f10610P = this.f10779c.mo26662H();
            crashDetailBean.f10611Q = this.f10779c.mo26663I();
            crashDetailBean.f10612R = this.f10779c.mo26656B();
            crashDetailBean.f10613S = this.f10779c.mo26661G();
            crashDetailBean.f10643z = C4893z.m17017a(C4824c.f10694f, false);
            int indexOf2 = crashDetailBean.f10634q.indexOf("java:\n");
            if (indexOf2 > 0 && (i = indexOf2 + 6) < crashDetailBean.f10634q.length()) {
                String str16 = crashDetailBean.f10634q;
                String substring = str16.substring(i, str16.length() - 1);
                if (substring.length() > 0 && crashDetailBean.f10643z.containsKey(crashDetailBean.f10596B) && (indexOf = (str12 = crashDetailBean.f10643z.get(crashDetailBean.f10596B)).indexOf(substring)) > 0) {
                    String substring2 = str12.substring(indexOf);
                    crashDetailBean.f10643z.put(crashDetailBean.f10596B, substring2);
                    crashDetailBean.f10634q = crashDetailBean.f10634q.substring(0, i);
                    crashDetailBean.f10634q += substring2;
                }
            }
            if (str13 == null) {
                crashDetailBean.f10595A = this.f10779c.f10526d;
            }
            this.f10778b.mo26752c(crashDetailBean);
        } else {
            crashDetailBean.f10597C = -1;
            crashDetailBean.f10598D = -1;
            crashDetailBean.f10599E = -1;
            if (crashDetailBean.f10640w == null) {
                crashDetailBean.f10640w = "this crash is occurred at last process! Log is miss, when get an terrible ABRT Native Exception etc.";
            }
            crashDetailBean.f10607M = -1;
            crashDetailBean.f10610P = -1;
            crashDetailBean.f10611Q = -1;
            crashDetailBean.f10612R = map;
            crashDetailBean.f10613S = this.f10779c.mo26661G();
            crashDetailBean.f10643z = null;
            if (str13 == null) {
                crashDetailBean.f10595A = "unknown(record)";
            }
            if (bArr2 != null) {
                crashDetailBean.f10642y = bArr2;
            }
        }
        return crashDetailBean;
    }
}
