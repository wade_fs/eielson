package com.tencent.bugly.crashreport.crash.p213h5;

import android.webkit.JavascriptInterface;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.bugly.crashreport.inner.InnerApi;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: com.tencent.bugly.crashreport.crash.h5.H5JavaScriptInterface */
/* compiled from: BUGLY */
public class H5JavaScriptInterface {

    /* renamed from: a */
    private static HashSet<Integer> f10743a = new HashSet<>();

    /* renamed from: b */
    private String f10744b = null;

    /* renamed from: c */
    private Thread f10745c = null;

    /* renamed from: d */
    private String f10746d = null;

    /* renamed from: e */
    private Map<String, String> f10747e = null;

    private H5JavaScriptInterface() {
    }

    /* renamed from: a */
    private static C4831a m16691a(String str) {
        if (str != null && str.length() > 0) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                C4831a aVar = new C4831a();
                aVar.f10748a = jSONObject.getString("projectRoot");
                if (aVar.f10748a == null) {
                    return null;
                }
                aVar.f10749b = jSONObject.getString("context");
                if (aVar.f10749b == null) {
                    return null;
                }
                aVar.f10750c = jSONObject.getString("url");
                if (aVar.f10750c == null) {
                    return null;
                }
                aVar.f10751d = jSONObject.getString("userAgent");
                if (aVar.f10751d == null) {
                    return null;
                }
                aVar.f10752e = jSONObject.getString("language");
                if (aVar.f10752e == null) {
                    return null;
                }
                aVar.f10753f = jSONObject.getString("name");
                if (aVar.f10753f != null) {
                    if (!aVar.f10753f.equals("null")) {
                        String string = jSONObject.getString("stacktrace");
                        if (string == null) {
                            return null;
                        }
                        int indexOf = string.indexOf("\n");
                        if (indexOf < 0) {
                            C4888x.m16983d("H5 crash stack's format is wrong!", new Object[0]);
                            return null;
                        }
                        aVar.f10755h = string.substring(indexOf + 1);
                        aVar.f10754g = string.substring(0, indexOf);
                        int indexOf2 = aVar.f10754g.indexOf(":");
                        if (indexOf2 > 0) {
                            aVar.f10754g = aVar.f10754g.substring(indexOf2 + 1);
                        }
                        aVar.f10756i = jSONObject.getString("file");
                        if (aVar.f10753f == null) {
                            return null;
                        }
                        aVar.f10757j = jSONObject.getLong("lineNumber");
                        if (aVar.f10757j < 0) {
                            return null;
                        }
                        aVar.f10758k = jSONObject.getLong("columnNumber");
                        if (aVar.f10758k < 0) {
                            return null;
                        }
                        C4888x.m16977a("H5 crash information is following: ", new Object[0]);
                        C4888x.m16977a("[projectRoot]: " + aVar.f10748a, new Object[0]);
                        C4888x.m16977a("[context]: " + aVar.f10749b, new Object[0]);
                        C4888x.m16977a("[url]: " + aVar.f10750c, new Object[0]);
                        C4888x.m16977a("[userAgent]: " + aVar.f10751d, new Object[0]);
                        C4888x.m16977a("[language]: " + aVar.f10752e, new Object[0]);
                        C4888x.m16977a("[name]: " + aVar.f10753f, new Object[0]);
                        C4888x.m16977a("[message]: " + aVar.f10754g, new Object[0]);
                        C4888x.m16977a("[stacktrace]: \n" + aVar.f10755h, new Object[0]);
                        C4888x.m16977a("[file]: " + aVar.f10756i, new Object[0]);
                        C4888x.m16977a("[lineNumber]: " + aVar.f10757j, new Object[0]);
                        C4888x.m16977a("[columnNumber]: " + aVar.f10758k, new Object[0]);
                        return aVar;
                    }
                }
                return null;
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    public static H5JavaScriptInterface getInstance(CrashReport.WebViewInterface webViewInterface) {
        String str = null;
        if (webViewInterface == null || f10743a.contains(Integer.valueOf(webViewInterface.hashCode()))) {
            return null;
        }
        H5JavaScriptInterface h5JavaScriptInterface = new H5JavaScriptInterface();
        f10743a.add(Integer.valueOf(webViewInterface.hashCode()));
        h5JavaScriptInterface.f10745c = Thread.currentThread();
        Thread thread = h5JavaScriptInterface.f10745c;
        if (thread != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("\n");
            for (int i = 2; i < thread.getStackTrace().length; i++) {
                StackTraceElement stackTraceElement = thread.getStackTrace()[i];
                if (!stackTraceElement.toString().contains("crashreport")) {
                    sb.append(stackTraceElement.toString());
                    sb.append("\n");
                }
            }
            str = sb.toString();
        }
        h5JavaScriptInterface.f10746d = str;
        HashMap hashMap = new HashMap();
        StringBuilder sb2 = new StringBuilder();
        sb2.append((Object) webViewInterface.getContentDescription());
        hashMap.put("[WebView] ContentDescription", sb2.toString());
        h5JavaScriptInterface.f10747e = hashMap;
        return h5JavaScriptInterface;
    }

    @JavascriptInterface
    public void printLog(String str) {
        C4888x.m16983d("Log from js: %s", str);
    }

    @JavascriptInterface
    public void reportJSException(String str) {
        if (str == null) {
            C4888x.m16983d("Payload from JS is null.", new Object[0]);
            return;
        }
        String b = C4893z.m17035b(str.getBytes());
        String str2 = this.f10744b;
        if (str2 == null || !str2.equals(b)) {
            this.f10744b = b;
            C4888x.m16983d("Handling JS exception ...", new Object[0]);
            C4831a a = m16691a(str);
            if (a == null) {
                C4888x.m16983d("Failed to parse payload.", new Object[0]);
                return;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            String str3 = a.f10748a;
            if (str3 != null) {
                linkedHashMap2.put("[JS] projectRoot", str3);
            }
            String str4 = a.f10749b;
            if (str4 != null) {
                linkedHashMap2.put("[JS] context", str4);
            }
            String str5 = a.f10750c;
            if (str5 != null) {
                linkedHashMap2.put("[JS] url", str5);
            }
            String str6 = a.f10751d;
            if (str6 != null) {
                linkedHashMap2.put("[JS] userAgent", str6);
            }
            String str7 = a.f10756i;
            if (str7 != null) {
                linkedHashMap2.put("[JS] file", str7);
            }
            long j = a.f10757j;
            if (j != 0) {
                linkedHashMap2.put("[JS] lineNumber", Long.toString(j));
            }
            linkedHashMap.putAll(linkedHashMap2);
            linkedHashMap.putAll(this.f10747e);
            linkedHashMap.put("Java Stack", this.f10746d);
            Thread thread = this.f10745c;
            if (a != null) {
                InnerApi.postH5CrashAsync(thread, a.f10753f, a.f10754g, a.f10755h, linkedHashMap);
                return;
            }
            return;
        }
        C4888x.m16983d("Same payload from js. Please check whether you've injected bugly.js more than one times.", new Object[0]);
    }
}
