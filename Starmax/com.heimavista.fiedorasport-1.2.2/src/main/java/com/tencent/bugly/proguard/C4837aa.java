package com.tencent.bugly.proguard;

import android.os.Handler;
import android.os.SystemClock;

/* renamed from: com.tencent.bugly.proguard.aa */
/* compiled from: BUGLY */
public final class C4837aa implements Runnable {

    /* renamed from: a */
    private final Handler f10787a;

    /* renamed from: b */
    private final String f10788b;

    /* renamed from: c */
    private long f10789c;

    /* renamed from: d */
    private final long f10790d;

    /* renamed from: e */
    private boolean f10791e = true;

    /* renamed from: f */
    private long f10792f;

    C4837aa(Handler handler, String str, long j) {
        this.f10787a = handler;
        this.f10788b = str;
        this.f10789c = j;
        this.f10790d = j;
    }

    /* renamed from: a */
    public final void mo26819a() {
        if (this.f10791e) {
            this.f10791e = false;
            this.f10792f = SystemClock.uptimeMillis();
            this.f10787a.postAtFrontOfQueue(this);
        }
    }

    /* renamed from: b */
    public final boolean mo26821b() {
        return !this.f10791e && SystemClock.uptimeMillis() > this.f10792f + this.f10789c;
    }

    /* renamed from: c */
    public final int mo26822c() {
        if (this.f10791e) {
            return 0;
        }
        return SystemClock.uptimeMillis() - this.f10792f < this.f10789c ? 1 : 3;
    }

    /* renamed from: d */
    public final Thread mo26823d() {
        return this.f10787a.getLooper().getThread();
    }

    /* renamed from: e */
    public final String mo26824e() {
        return this.f10788b;
    }

    /* renamed from: f */
    public final void mo26825f() {
        this.f10789c = this.f10790d;
    }

    public final void run() {
        this.f10791e = true;
        this.f10789c = this.f10790d;
    }

    /* renamed from: a */
    public final void mo26820a(long j) {
        this.f10789c = Long.MAX_VALUE;
    }
}
