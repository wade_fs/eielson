package com.tencent.bugly.proguard;

import android.util.Log;
import java.util.Locale;

/* renamed from: com.tencent.bugly.proguard.x */
/* compiled from: BUGLY */
public final class C4888x {

    /* renamed from: a */
    public static String f11043a = "CrashReport";

    /* renamed from: b */
    public static boolean f11044b = false;

    /* renamed from: c */
    private static String f11045c = "CrashReportInfo";

    /* renamed from: a */
    private static boolean m16975a(int i, String str, Object... objArr) {
        if (!f11044b) {
            return false;
        }
        if (str == null) {
            str = "null";
        } else if (!(objArr == null || objArr.length == 0)) {
            str = String.format(Locale.US, str, objArr);
        }
        if (i == 0) {
            Log.i(f11043a, str);
            return true;
        } else if (i == 1) {
            Log.d(f11043a, str);
            return true;
        } else if (i == 2) {
            Log.w(f11043a, str);
            return true;
        } else if (i == 3) {
            Log.e(f11043a, str);
            return true;
        } else if (i != 5) {
            return false;
        } else {
            Log.i(f11045c, str);
            return true;
        }
    }

    /* renamed from: b */
    public static boolean m16980b(String str, Object... objArr) {
        return m16975a(5, str, objArr);
    }

    /* renamed from: c */
    public static boolean m16982c(String str, Object... objArr) {
        return m16975a(1, str, objArr);
    }

    /* renamed from: d */
    public static boolean m16983d(String str, Object... objArr) {
        return m16975a(2, str, objArr);
    }

    /* renamed from: e */
    public static boolean m16984e(String str, Object... objArr) {
        return m16975a(3, str, objArr);
    }

    /* renamed from: b */
    public static boolean m16979b(Class cls, String str, Object... objArr) {
        return m16975a(1, String.format(Locale.US, "[%s] %s", cls.getSimpleName(), str), objArr);
    }

    /* renamed from: b */
    public static boolean m16981b(Throwable th) {
        if (!f11044b) {
            return false;
        }
        return m16975a(3, C4893z.m17013a(th), new Object[0]);
    }

    /* renamed from: a */
    public static boolean m16977a(String str, Object... objArr) {
        return m16975a(0, str, objArr);
    }

    /* renamed from: a */
    public static boolean m16976a(Class cls, String str, Object... objArr) {
        return m16975a(0, String.format(Locale.US, "[%s] %s", cls.getSimpleName(), str), objArr);
    }

    /* renamed from: a */
    public static boolean m16978a(Throwable th) {
        if (!f11044b) {
            return false;
        }
        return m16975a(2, C4893z.m17013a(th), new Object[0]);
    }
}
