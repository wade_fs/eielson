package com.tencent.bugly.crashreport;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.C4792b;
import com.tencent.bugly.CrashModule;
import com.tencent.bugly.crashreport.biz.C4802b;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver;
import com.tencent.bugly.crashreport.crash.C4824c;
import com.tencent.bugly.crashreport.crash.C4827d;
import com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler;
import com.tencent.bugly.crashreport.crash.p213h5.C4832b;
import com.tencent.bugly.crashreport.crash.p213h5.H5JavaScriptInterface;
import com.tencent.bugly.proguard.C4836a;
import com.tencent.bugly.proguard.C4877q;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.net.InetAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: BUGLY */
public class CrashReport {

    /* renamed from: a */
    private static Context f10417a;

    /* compiled from: BUGLY */
    public static class CrashHandleCallback extends BuglyStrategy.C4791a {
    }

    /* compiled from: BUGLY */
    public static class UserStrategy extends BuglyStrategy {

        /* renamed from: a */
        private CrashHandleCallback f10418a;

        public UserStrategy(Context context) {
        }

        public synchronized void setCrashHandleCallback(CrashHandleCallback crashHandleCallback) {
            this.f10418a = crashHandleCallback;
        }

        public synchronized CrashHandleCallback getCrashHandleCallback() {
            return this.f10418a;
        }
    }

    /* compiled from: BUGLY */
    public interface WebViewInterface {
        void addJavascriptInterface(H5JavaScriptInterface h5JavaScriptInterface, String str);

        CharSequence getContentDescription();

        String getUrl();

        void loadUrl(String str);

        void setJavaScriptEnabled(boolean z);
    }

    /* renamed from: com.tencent.bugly.crashreport.CrashReport$a */
    /* compiled from: BUGLY */
    static class C4793a implements WebViewInterface {

        /* renamed from: a */
        private /* synthetic */ WebView f10419a;

        C4793a(WebView webView) {
            this.f10419a = webView;
        }

        public final void addJavascriptInterface(H5JavaScriptInterface h5JavaScriptInterface, String str) {
            this.f10419a.addJavascriptInterface(h5JavaScriptInterface, str);
        }

        public final CharSequence getContentDescription() {
            return this.f10419a.getContentDescription();
        }

        public final String getUrl() {
            return this.f10419a.getUrl();
        }

        public final void loadUrl(String str) {
            this.f10419a.loadUrl(str);
        }

        public final void setJavaScriptEnabled(boolean z) {
            WebSettings settings = this.f10419a.getSettings();
            if (!settings.getJavaScriptEnabled()) {
                settings.setJavaScriptEnabled(true);
            }
        }
    }

    public static void closeBugly() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not close bugly because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else if (f10417a != null) {
            BuglyBroadcastReceiver instance = BuglyBroadcastReceiver.getInstance();
            if (instance != null) {
                instance.unregister(f10417a);
            }
            closeCrashReport();
            C4802b.m16459a(f10417a);
            C4886w a = C4886w.m16969a();
            if (a != null) {
                a.mo26936b();
            }
        }
    }

    public static void closeCrashReport() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not close crash report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4824c.m16656a().mo26761d();
        }
    }

    public static void closeNativeReport() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not close native report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4824c.m16656a().mo26763f();
        }
    }

    public static void enableBugly(boolean z) {
        C4792b.f10412a = z;
    }

    public static void enableObtainId(Context context, boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set DB name because bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "enableObtainId args context should not be null");
        } else {
            String str = C4888x.f11043a;
            Log.i(str, "Enable identification obtaining? " + z);
            C4806a.m16491a(context).mo26682b(z);
        }
    }

    public static Set<String> getAllUserDataKeys(Context context) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get all keys of user data because bugly is disable.");
            return new HashSet();
        } else if (context != null) {
            return C4806a.m16491a(context).mo26659E();
        } else {
            Log.e(C4888x.f11043a, "getAllUserDataKeys args context should not be null");
            return new HashSet();
        }
    }

    public static String getAppChannel() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get App channel because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4806a.m16491a(f10417a).f10534l;
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static String getAppID() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get App ID because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4806a.m16491a(f10417a).mo26689f();
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static String getAppVer() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get app version because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4806a.m16491a(f10417a).f10532j;
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static String getBuglyVersion(Context context) {
        if (context == null) {
            C4888x.m16983d("Please call with context.", new Object[0]);
            return "unknown";
        }
        C4806a.m16491a(context);
        return C4806a.m16493c();
    }

    public static Proxy getHttpProxy() {
        return C4836a.m16731b();
    }

    public static Map<String, String> getSdkExtraData() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get SDK extra data because bugly is disable.");
            return new HashMap();
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4806a.m16491a(f10417a).f10472A;
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return null;
        }
    }

    public static String getUserData(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get user data because bugly is disable.");
            return "unknown";
        } else if (context == null) {
            Log.e(C4888x.f11043a, "getUserDataValue args context should not be null");
            return "unknown";
        } else if (C4893z.m17024a(str)) {
            return null;
        } else {
            return C4806a.m16491a(context).mo26692g(str);
        }
    }

    public static int getUserDatasSize(Context context) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get size of user data because bugly is disable.");
            return -1;
        } else if (context != null) {
            return C4806a.m16491a(context).mo26658D();
        } else {
            Log.e(C4888x.f11043a, "getUserDatasSize args context should not be null");
            return -1;
        }
    }

    public static String getUserId() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get user ID because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4806a.m16491a(f10417a).mo26691g();
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static int getUserSceneTagId(Context context) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get user scene tag because bugly is disable.");
            return -1;
        } else if (context != null) {
            return C4806a.m16491a(context).mo26662H();
        } else {
            Log.e(C4888x.f11043a, "getUserSceneTagId args context should not be null");
            return -1;
        }
    }

    public static void initCrashReport(Context context) {
        f10417a = context;
        C4792b.m16436a(CrashModule.getInstance());
        C4792b.m16433a(context);
    }

    public static boolean isLastSessionCrash() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "The info 'isLastSessionCrash' is not accurate because bugly is disable.");
            return false;
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C4824c.m16656a().mo26759b();
        } else {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return false;
        }
    }

    public static void postCatchedException(Throwable th) {
        postCatchedException(th, Thread.currentThread(), false);
    }

    public static void postException(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not post crash caught because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4827d.m16681a(thread, i, str, str2, str3, map);
        }
    }

    private static void putSdkData(Context context, String str, String str2) {
        if (context != null && !C4893z.m17024a(str) && !C4893z.m17024a(str2)) {
            String replace = str.replace("[a-zA-Z[0-9]]+", "");
            if (replace.length() > 100) {
                Log.w(C4888x.f11043a, String.format("putSdkData key length over limit %d, will be cutted.", 50));
                replace = replace.substring(0, 50);
            }
            if (str2.length() > 500) {
                Log.w(C4888x.f11043a, String.format("putSdkData value length over limit %d, will be cutted!", Integer.valueOf((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION)));
                str2 = str2.substring(0, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
            }
            C4806a.m16491a(context).mo26684c(replace, str2);
            C4888x.m16980b(String.format("[param] putSdkData data: %s - %s", replace, str2), new Object[0]);
        }
    }

    public static void putUserData(Context context, String str, String str2) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not put user data because bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "putUserData args context should not be null");
        } else if (str == null) {
            str;
            C4888x.m16983d("putUserData args key should not be null or empty", new Object[0]);
        } else if (str2 == null) {
            str2;
            C4888x.m16983d("putUserData args value should not be null", new Object[0]);
        } else if (!str.matches("[a-zA-Z[0-9]]+")) {
            C4888x.m16983d("putUserData args key should match [a-zA-Z[0-9]]+  {" + str + "}", new Object[0]);
        } else {
            if (str2.length() > 200) {
                C4888x.m16983d("user data value length over limit %d, it will be cutted!", Integer.valueOf((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION));
                str2 = str2.substring(0, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
            }
            C4806a a = C4806a.m16491a(context);
            if (a.mo26659E().contains(str)) {
                NativeCrashHandler instance = NativeCrashHandler.getInstance();
                if (instance != null) {
                    instance.putKeyValueToNative(str, str2);
                }
                C4806a.m16491a(context).mo26681b(str, str2);
                C4888x.m16982c("replace KV %s %s", str, str2);
            } else if (a.mo26658D() >= 10) {
                C4888x.m16983d("user data size is over limit %d, it will be cutted!", 10);
            } else {
                if (str.length() > 50) {
                    C4888x.m16983d("user data key length over limit %d , will drop this new key %s", 50, str);
                    str = str.substring(0, 50);
                }
                NativeCrashHandler instance2 = NativeCrashHandler.getInstance();
                if (instance2 != null) {
                    instance2.putKeyValueToNative(str, str2);
                }
                C4806a.m16491a(context).mo26681b(str, str2);
                C4888x.m16980b("[param] set user data: %s - %s", str, str2);
            }
        }
    }

    public static String removeUserData(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not remove user data because bugly is disable.");
            return "unknown";
        } else if (context == null) {
            Log.e(C4888x.f11043a, "removeUserData args context should not be null");
            return "unknown";
        } else if (C4893z.m17024a(str)) {
            return null;
        } else {
            C4888x.m16980b("[param] remove user data: %s", str);
            return C4806a.m16491a(context).mo26690f(str);
        }
    }

    public static void setAppChannel(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App channel because Bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "setAppChannel args context should not be null");
        } else if (str == null) {
            Log.w(C4888x.f11043a, "App channel is null, will not set");
        } else {
            C4806a.m16491a(context).f10534l = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppChannel(str);
            }
        }
    }

    public static void setAppPackage(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App package because bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "setAppPackage args context should not be null");
        } else if (str == null) {
            Log.w(C4888x.f11043a, "App package is null, will not set");
        } else {
            C4806a.m16491a(context).f10525c = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppPackage(str);
            }
        }
    }

    public static void setAppVersion(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App version because bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "setAppVersion args context should not be null");
        } else if (str == null) {
            Log.w(C4888x.f11043a, "App version is null, will not set");
        } else {
            C4806a.m16491a(context).f10532j = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppVersion(str);
            }
        }
    }

    public static void setAuditEnable(Context context, boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App package because bugly is disable.");
        } else if (context == null) {
            Log.w(C4888x.f11043a, "setAppPackage args context should not be null");
        } else {
            String str = C4888x.f11043a;
            Log.i(str, "Set audit enable: " + z);
            C4806a.m16491a(context).f10473B = z;
        }
    }

    public static void setBuglyDbName(String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set DB name because bugly is disable.");
            return;
        }
        String str2 = C4888x.f11043a;
        Log.i(str2, "Set Bugly DB name: " + str);
        C4877q.f10977a = str;
    }

    public static void setContext(Context context) {
        f10417a = context;
    }

    public static void setCrashFilter(String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App package because bugly is disable.");
            return;
        }
        String str2 = C4888x.f11043a;
        Log.i(str2, "Set crash stack filter: " + str);
        C4824c.f10702n = str;
    }

    public static void setCrashRegularFilter(String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App package because bugly is disable.");
            return;
        }
        String str2 = C4888x.f11043a;
        Log.i(str2, "Set crash stack filter: " + str);
        C4824c.f10703o = str;
    }

    public static void setHandleNativeCrashInJava(boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set App package because bugly is disable.");
            return;
        }
        String str = C4888x.f11043a;
        Log.i(str, "Should handle native crash in Java profile after handled in native profile: " + z);
        NativeCrashHandler.setShouldHandleInJava(z);
    }

    public static void setHttpProxy(String str, int i) {
        C4836a.m16726a(str, i);
    }

    public static void setIsAppForeground(Context context, boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set 'isAppForeground' because bugly is disable.");
        } else if (context == null) {
            C4888x.m16983d("Context should not be null.", new Object[0]);
        } else {
            if (z) {
                C4888x.m16982c("App is in foreground.", new Object[0]);
            } else {
                C4888x.m16982c("App is in background.", new Object[0]);
            }
            C4806a.m16491a(context).mo26677a(z);
        }
    }

    public static void setIsDevelopmentDevice(Context context, boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set 'isDevelopmentDevice' because bugly is disable.");
        } else if (context == null) {
            C4888x.m16983d("Context should not be null.", new Object[0]);
        } else {
            if (z) {
                C4888x.m16982c("This is a development device.", new Object[0]);
            } else {
                C4888x.m16982c("This is not a development device.", new Object[0]);
            }
            C4806a.m16491a(context).f10547y = z;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean
     arg types: [android.webkit.WebView, boolean, int]
     candidates:
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean */
    public static boolean setJavascriptMonitor(WebView webView, boolean z) {
        return setJavascriptMonitor(webView, z, false);
    }

    public static void setSdkExtraData(Context context, String str, String str2) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not put SDK extra data because bugly is disable.");
        } else if (context != null && !C4893z.m17024a(str) && !C4893z.m17024a(str2)) {
            C4806a.m16491a(context).mo26676a(str, str2);
        }
    }

    public static void setServerUrl(String str) {
        if (C4893z.m17024a(str) || !C4893z.m17047c(str)) {
            Log.i(C4888x.f11043a, "URL is invalid.");
            return;
        }
        C4809a.m16591a(str);
        StrategyBean.f10555b = str;
        StrategyBean.f10556c = str;
    }

    public static void setSessionIntervalMills(long j) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set 'SessionIntervalMills' because bugly is disable.");
        } else {
            C4802b.m16458a(j);
        }
    }

    public static void setUserId(String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set user ID because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            setUserId(f10417a, str);
        }
    }

    public static void setUserSceneTag(Context context, int i) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set tag caught because bugly is disable.");
        } else if (context == null) {
            Log.e(C4888x.f11043a, "setTag args context should not be null");
        } else {
            if (i <= 0) {
                C4888x.m16983d("setTag args tagId should > 0", new Object[0]);
            }
            C4806a.m16491a(context).mo26674a(i);
            C4888x.m16980b("[param] set user scene tag: %d", Integer.valueOf(i));
        }
    }

    public static void startCrashReport() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not start crash report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4824c.m16656a().mo26760c();
        }
    }

    public static void testANRCrash() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not test ANR crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4888x.m16977a("start to create a anr crash for test!", new Object[0]);
            C4824c.m16656a().mo26767j();
        }
    }

    public static void testJavaCrash() {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not test Java crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4806a b = C4806a.m16492b();
            if (b != null) {
                b.mo26679b(24096);
            }
            throw new RuntimeException("This Crash create for Test! You can go to Bugly see more detail!");
        }
    }

    public static void testNativeCrash() {
        testNativeCrash(false, false, false);
    }

    public static void postCatchedException(Throwable th, Thread thread) {
        postCatchedException(th, thread, false);
    }

    public static void setHttpProxy(InetAddress inetAddress, int i) {
        C4836a.m16727a(inetAddress, i);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public static boolean setJavascriptMonitor(WebView webView, boolean z, boolean z2) {
        if (webView != null) {
            return setJavascriptMonitor(new C4793a(webView), z, z2);
        }
        Log.w(C4888x.f11043a, "WebView is null.");
        return false;
    }

    public static void testNativeCrash(boolean z, boolean z2, boolean z3) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not test native crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C4888x.m16977a("start to create a native crash for test!", new Object[0]);
            C4824c.m16656a().mo26758a(z, z2, z3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.c.a(java.lang.Thread, java.lang.Throwable, boolean, java.lang.String, byte[], boolean):void
     arg types: [java.lang.Thread, java.lang.Throwable, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], boolean]
     candidates:
      com.tencent.bugly.crashreport.crash.c.a(int, android.content.Context, boolean, com.tencent.bugly.BuglyStrategy$a, com.tencent.bugly.proguard.o, java.lang.String):com.tencent.bugly.crashreport.crash.c
      com.tencent.bugly.crashreport.crash.c.a(java.lang.Thread, java.lang.Throwable, boolean, java.lang.String, byte[], boolean):void */
    public static void postCatchedException(Throwable th, Thread thread, boolean z) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not post crash caught because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C4888x.f11043a, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else if (th == null) {
            C4888x.m16983d("throwable is null, just return", new Object[0]);
        } else {
            if (thread == null) {
                thread = Thread.currentThread();
            }
            C4824c.m16656a().mo26757a(thread, th, false, (String) null, (byte[]) null, z);
        }
    }

    public static void initCrashReport(Context context, UserStrategy userStrategy) {
        f10417a = context;
        C4792b.m16436a(CrashModule.getInstance());
        C4792b.m16434a(context, userStrategy);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean
     arg types: [com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, int]
     candidates:
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean */
    public static boolean setJavascriptMonitor(WebViewInterface webViewInterface, boolean z) {
        return setJavascriptMonitor(webViewInterface, z, false);
    }

    public static void postException(int i, String str, String str2, String str3, Map<String, String> map) {
        postException(Thread.currentThread(), i, str, str2, str3, map);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public static boolean setJavascriptMonitor(WebViewInterface webViewInterface, boolean z, boolean z2) {
        if (webViewInterface == null) {
            Log.w(C4888x.f11043a, "WebViewInterface is null.");
            return false;
        } else if (!CrashModule.getInstance().hasInitialized()) {
            C4888x.m16984e("CrashReport has not been initialed! please to call method 'initCrashReport' first!", new Object[0]);
            return false;
        } else {
            C4888x.m16977a("Set Javascript exception monitor of webview.", new Object[0]);
            if (!C4792b.f10412a) {
                Log.w(C4888x.f11043a, "Can not set JavaScript monitor because bugly is disable.");
                return false;
            }
            C4888x.m16982c("URL of webview is %s", webViewInterface.getUrl());
            if (z2 || Build.VERSION.SDK_INT >= 19) {
                C4888x.m16977a("Enable the javascript needed by webview monitor.", new Object[0]);
                webViewInterface.setJavaScriptEnabled(true);
                H5JavaScriptInterface instance = H5JavaScriptInterface.getInstance(webViewInterface);
                if (instance != null) {
                    C4888x.m16977a("Add a secure javascript interface to the webview.", new Object[0]);
                    webViewInterface.addJavascriptInterface(instance, "exceptionUploader");
                }
                if (z) {
                    C4888x.m16977a("Inject bugly.js(v%s) to the webview.", C4832b.m16693b());
                    String a = C4832b.m16692a();
                    if (a == null) {
                        C4888x.m16984e("Failed to inject Bugly.js.", C4832b.m16693b());
                        return false;
                    }
                    webViewInterface.loadUrl("javascript:" + a);
                }
                return true;
            }
            C4888x.m16984e("This interface is only available for Android 4.4 or later.", new Object[0]);
            return false;
        }
    }

    public static void setUserId(Context context, String str) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not set user ID because bugly is disable.");
        } else if (context == null) {
            Log.e(C4888x.f11043a, "Context should not be null when bugly has not been initialed!");
        } else if (str == null) {
            C4888x.m16983d("userId should not be null", new Object[0]);
        } else {
            if (str.length() > 100) {
                String substring = str.substring(0, 100);
                C4888x.m16983d("userId %s length is over limit %d substring to %s", str, 100, substring);
                str = substring;
            }
            if (!str.equals(C4806a.m16491a(context).mo26691g())) {
                C4806a.m16491a(context).mo26680b(str);
                C4888x.m16980b("[user] set userId : %s", str);
                NativeCrashHandler instance = NativeCrashHandler.getInstance();
                if (instance != null) {
                    instance.setNativeUserId(str);
                }
                if (CrashModule.getInstance().hasInitialized()) {
                    C4802b.m16457a();
                }
            }
        }
    }

    public static Map<String, String> getSdkExtraData(Context context) {
        if (!C4792b.f10412a) {
            Log.w(C4888x.f11043a, "Can not get SDK extra data because bugly is disable.");
            return new HashMap();
        } else if (context != null) {
            return C4806a.m16491a(context).f10472A;
        } else {
            C4888x.m16983d("Context should not be null.", new Object[0]);
            return null;
        }
    }

    public static void initCrashReport(Context context, String str, boolean z) {
        if (context != null) {
            f10417a = context;
            C4792b.m16436a(CrashModule.getInstance());
            C4792b.m16435a(context, str, z, null);
        }
    }

    public static void initCrashReport(Context context, String str, boolean z, UserStrategy userStrategy) {
        if (context != null) {
            f10417a = context;
            C4792b.m16436a(CrashModule.getInstance());
            C4792b.m16435a(context, str, z, userStrategy);
        }
    }
}
