package com.tencent.bugly.crashreport.biz;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.biz.C4796a;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C4886w;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;

/* renamed from: com.tencent.bugly.crashreport.biz.b */
/* compiled from: BUGLY */
public class C4802b {

    /* renamed from: a */
    public static C4796a f10452a = null;

    /* renamed from: b */
    private static boolean f10453b = false;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static int f10454c = 10;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static long f10455d = 300000;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static long f10456e = 30000;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static long f10457f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static int f10458g = 0;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static long f10459h = 0;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static long f10460i = 0;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static long f10461j = 0;

    /* renamed from: k */
    private static Application.ActivityLifecycleCallbacks f10462k = null;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public static Class<?> f10463l = null;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static boolean f10464m = true;

    /* renamed from: com.tencent.bugly.crashreport.biz.b$a */
    /* compiled from: BUGLY */
    static class C4803a implements Runnable {

        /* renamed from: P */
        private /* synthetic */ Context f10465P;

        /* renamed from: Q */
        private /* synthetic */ BuglyStrategy f10466Q;

        C4803a(Context context, BuglyStrategy buglyStrategy) {
            this.f10465P = context;
            this.f10466Q = buglyStrategy;
        }

        public final void run() {
            C4802b.m16467c(this.f10465P, this.f10466Q);
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.b$b */
    /* compiled from: BUGLY */
    static class C4804b implements Application.ActivityLifecycleCallbacks {
        C4804b() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            String name = activity != null ? activity.getClass().getName() : "unknown";
            if (C4802b.f10463l == null || C4802b.f10463l.getName().equals(name)) {
                C4888x.m16982c(">>> %s onCreated <<<", name);
                C4806a b = C4806a.m16492b();
                if (b != null) {
                    b.f10474C.add(C4802b.m16456a(name, "onCreated"));
                }
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            String name = activity != null ? activity.getClass().getName() : "unknown";
            if (C4802b.f10463l == null || C4802b.f10463l.getName().equals(name)) {
                C4888x.m16982c(">>> %s onDestroyed <<<", name);
                C4806a b = C4806a.m16492b();
                if (b != null) {
                    b.f10474C.add(C4802b.m16456a(name, "onDestroyed"));
                }
            }
        }

        public final void onActivityPaused(Activity activity) {
            String name = activity != null ? activity.getClass().getName() : "unknown";
            if (C4802b.f10463l == null || C4802b.f10463l.getName().equals(name)) {
                C4888x.m16982c(">>> %s onPaused <<<", name);
                C4806a b = C4806a.m16492b();
                if (b != null) {
                    b.f10474C.add(C4802b.m16456a(name, "onPaused"));
                    b.mo26677a(false);
                    b.f10540r = System.currentTimeMillis();
                    long j = b.f10540r;
                    b.f10541s = j - b.f10539q;
                    long unused = C4802b.f10459h = j;
                    if (b.f10541s < 0) {
                        b.f10541s = 0;
                    }
                    if (activity != null) {
                        b.f10538p = "background";
                    } else {
                        b.f10538p = "unknown";
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
         arg types: [int, int, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
        public final void onActivityResumed(Activity activity) {
            String name = activity != null ? activity.getClass().getName() : "unknown";
            if (C4802b.f10463l == null || C4802b.f10463l.getName().equals(name)) {
                C4888x.m16982c(">>> %s onResumed <<<", name);
                C4806a b = C4806a.m16492b();
                if (b != null) {
                    b.f10474C.add(C4802b.m16456a(name, "onResumed"));
                    b.mo26677a(true);
                    b.f10538p = name;
                    b.f10539q = System.currentTimeMillis();
                    b.f10542t = b.f10539q - C4802b.f10460i;
                    long d = b.f10539q - C4802b.f10459h;
                    if (d > (C4802b.f10457f > 0 ? C4802b.f10457f : C4802b.f10456e)) {
                        b.mo26685d();
                        C4802b.m16471g();
                        C4888x.m16977a("[session] launch app one times (app in background %d seconds and over %d seconds)", Long.valueOf(d / 1000), Long.valueOf(C4802b.f10456e / 1000));
                        if (C4802b.f10458g % C4802b.f10454c == 0) {
                            C4802b.f10452a.mo26635a(4, C4802b.f10464m, 0);
                            return;
                        }
                        C4802b.f10452a.mo26635a(4, false, 0L);
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - C4802b.f10461j > C4802b.f10455d) {
                            long unused = C4802b.f10461j = currentTimeMillis;
                            C4888x.m16977a("add a timer to upload hot start user info", new Object[0]);
                            if (C4802b.f10464m) {
                                C4886w.m16969a().mo26935a(new C4796a.C4799c(null, true), C4802b.f10455d);
                            }
                        }
                    }
                }
            }
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public final void onActivityStarted(Activity activity) {
        }

        public final void onActivityStopped(Activity activity) {
        }
    }

    /* renamed from: a */
    static /* synthetic */ String m16456a(String str, String str2) {
        return C4893z.m17008a() + "  " + str + "  " + str2 + "\n";
    }

    /* renamed from: g */
    static /* synthetic */ int m16471g() {
        int i = f10458g;
        f10458g = i + 1;
        return i;
    }

    /* renamed from: a */
    public static void m16460a(Context context, BuglyStrategy buglyStrategy) {
        long j;
        if (!f10453b) {
            f10464m = C4806a.m16491a(context).f10527e;
            f10452a = new C4796a(context, f10464m);
            f10453b = true;
            if (buglyStrategy != null) {
                f10463l = buglyStrategy.getUserInfoActivity();
                j = buglyStrategy.getAppReportDelay();
            } else {
                j = 0;
            }
            if (j <= 0) {
                m16467c(context, buglyStrategy);
            } else {
                C4886w.m16969a().mo26935a(new C4803a(context, buglyStrategy), j);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r14v11, types: [android.content.Context] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
      com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0068 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m16467c(android.content.Context r14, com.tencent.bugly.BuglyStrategy r15) {
        /*
            r0 = 0
            r1 = 1
            if (r15 == 0) goto L_0x000d
            boolean r2 = r15.recordUserInfoOnceADay()
            boolean r15 = r15.isEnableUserInfo()
            goto L_0x000f
        L_0x000d:
            r15 = 1
            r2 = 0
        L_0x000f:
            r3 = 0
            if (r2 == 0) goto L_0x006a
            com.tencent.bugly.crashreport.common.info.a r15 = com.tencent.bugly.crashreport.common.info.C4806a.m16491a(r14)
            java.lang.String r2 = r15.f10526d
            com.tencent.bugly.crashreport.biz.a r5 = com.tencent.bugly.crashreport.biz.C4802b.f10452a
            java.util.List r2 = r5.mo26633a(r2)
            if (r2 == 0) goto L_0x0065
            r5 = 0
        L_0x0022:
            int r6 = r2.size()
            if (r5 >= r6) goto L_0x0065
            java.lang.Object r6 = r2.get(r5)
            com.tencent.bugly.crashreport.biz.UserInfoBean r6 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r6
            java.lang.String r7 = r6.f10433n
            java.lang.String r8 = r15.f10532j
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0062
            int r7 = r6.f10421b
            if (r7 != r1) goto L_0x0062
            long r7 = com.tencent.bugly.proguard.C4893z.m17032b()
            int r9 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r9 <= 0) goto L_0x0065
            long r9 = r6.f10424e
            int r11 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r11 < 0) goto L_0x0062
            long r5 = r6.f10425f
            int r15 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r15 > 0) goto L_0x0060
            com.tencent.bugly.crashreport.biz.a r15 = com.tencent.bugly.crashreport.biz.C4802b.f10452a
            com.tencent.bugly.proguard.w r2 = com.tencent.bugly.proguard.C4886w.m16969a()
            if (r2 == 0) goto L_0x0060
            com.tencent.bugly.crashreport.biz.a$b r5 = new com.tencent.bugly.crashreport.biz.a$b
            r5.<init>()
            r2.mo26934a(r5)
        L_0x0060:
            r15 = 0
            goto L_0x0066
        L_0x0062:
            int r5 = r5 + 1
            goto L_0x0022
        L_0x0065:
            r15 = 1
        L_0x0066:
            if (r15 != 0) goto L_0x0069
            return
        L_0x0069:
            r15 = 0
        L_0x006a:
            com.tencent.bugly.crashreport.common.info.a r2 = com.tencent.bugly.crashreport.common.info.C4806a.m16492b()
            r5 = 0
            if (r2 == 0) goto L_0x00b0
            java.lang.Thread r6 = java.lang.Thread.currentThread()
            java.lang.StackTraceElement[] r6 = r6.getStackTrace()
            int r7 = r6.length
            r9 = r5
            r8 = 0
            r10 = 0
        L_0x007d:
            if (r8 >= r7) goto L_0x00a1
            r11 = r6[r8]
            java.lang.String r12 = r11.getMethodName()
            java.lang.String r13 = "onCreate"
            boolean r12 = r12.equals(r13)
            if (r12 == 0) goto L_0x0091
            java.lang.String r9 = r11.getClassName()
        L_0x0091:
            java.lang.String r11 = r11.getClassName()
            java.lang.String r12 = "android.app.Activity"
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x009e
            r10 = 1
        L_0x009e:
            int r8 = r8 + 1
            goto L_0x007d
        L_0x00a1:
            if (r9 == 0) goto L_0x00ac
            if (r10 == 0) goto L_0x00a9
            r2.mo26677a(r1)
            goto L_0x00ae
        L_0x00a9:
            java.lang.String r9 = "background"
            goto L_0x00ae
        L_0x00ac:
            java.lang.String r9 = "unknown"
        L_0x00ae:
            r2.f10538p = r9
        L_0x00b0:
            if (r15 == 0) goto L_0x00e4
            int r15 = android.os.Build.VERSION.SDK_INT
            r2 = 14
            if (r15 < r2) goto L_0x00e4
            android.content.Context r15 = r14.getApplicationContext()
            boolean r15 = r15 instanceof android.app.Application
            if (r15 == 0) goto L_0x00c7
            android.content.Context r14 = r14.getApplicationContext()
            r5 = r14
            android.app.Application r5 = (android.app.Application) r5
        L_0x00c7:
            if (r5 == 0) goto L_0x00e4
            android.app.Application$ActivityLifecycleCallbacks r14 = com.tencent.bugly.crashreport.biz.C4802b.f10462k     // Catch:{ Exception -> 0x00da }
            if (r14 != 0) goto L_0x00d4
            com.tencent.bugly.crashreport.biz.b$b r14 = new com.tencent.bugly.crashreport.biz.b$b     // Catch:{ Exception -> 0x00da }
            r14.<init>()     // Catch:{ Exception -> 0x00da }
            com.tencent.bugly.crashreport.biz.C4802b.f10462k = r14     // Catch:{ Exception -> 0x00da }
        L_0x00d4:
            android.app.Application$ActivityLifecycleCallbacks r14 = com.tencent.bugly.crashreport.biz.C4802b.f10462k     // Catch:{ Exception -> 0x00da }
            r5.registerActivityLifecycleCallbacks(r14)     // Catch:{ Exception -> 0x00da }
            goto L_0x00e4
        L_0x00da:
            r14 = move-exception
            boolean r15 = com.tencent.bugly.proguard.C4888x.m16978a(r14)
            if (r15 != 0) goto L_0x00e4
            r14.printStackTrace()
        L_0x00e4:
            boolean r14 = com.tencent.bugly.crashreport.biz.C4802b.f10464m
            if (r14 == 0) goto L_0x0110
            long r14 = java.lang.System.currentTimeMillis()
            com.tencent.bugly.crashreport.biz.C4802b.f10460i = r14
            com.tencent.bugly.crashreport.biz.a r14 = com.tencent.bugly.crashreport.biz.C4802b.f10452a
            r14.mo26635a(r1, r0, r3)
            java.lang.Object[] r14 = new java.lang.Object[r0]
            java.lang.String r15 = "[session] launch app, new start"
            com.tencent.bugly.proguard.C4888x.m16977a(r15, r14)
            com.tencent.bugly.crashreport.biz.a r14 = com.tencent.bugly.crashreport.biz.C4802b.f10452a
            r14.mo26634a()
            com.tencent.bugly.crashreport.biz.a r14 = com.tencent.bugly.crashreport.biz.C4802b.f10452a
            r0 = 21600000(0x1499700, double:1.0671818E-316)
            com.tencent.bugly.proguard.w r15 = com.tencent.bugly.proguard.C4886w.m16969a()
            com.tencent.bugly.crashreport.biz.a$e r2 = new com.tencent.bugly.crashreport.biz.a$e
            r2.<init>(r0)
            r15.mo26935a(r2, r0)
        L_0x0110:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C4802b.m16467c(android.content.Context, com.tencent.bugly.BuglyStrategy):void");
    }

    /* renamed from: a */
    public static void m16458a(long j) {
        if (j < 0) {
            j = C4809a.m16589a().mo26720c().f10570q;
        }
        f10457f = j;
    }

    /* renamed from: a */
    public static void m16461a(StrategyBean strategyBean, boolean z) {
        C4886w a;
        C4796a aVar = f10452a;
        if (!(aVar == null || z || (a = C4886w.m16969a()) == null)) {
            a.mo26934a(new C4796a.C4798b());
        }
        if (strategyBean != null) {
            long j = strategyBean.f10570q;
            if (j > 0) {
                f10456e = j;
            }
            int i = strategyBean.f10576w;
            if (i > 0) {
                f10454c = i;
            }
            long j2 = strategyBean.f10577x;
            if (j2 > 0) {
                f10455d = j2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
      com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
    /* renamed from: a */
    public static void m16457a() {
        C4796a aVar = f10452a;
        if (aVar != null) {
            aVar.mo26635a(2, false, 0L);
        }
    }

    /* JADX WARN: Type inference failed for: r3v5, types: [android.content.Context] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m16459a(android.content.Context r3) {
        /*
            boolean r0 = com.tencent.bugly.crashreport.biz.C4802b.f10453b
            if (r0 == 0) goto L_0x0036
            if (r3 != 0) goto L_0x0007
            goto L_0x0036
        L_0x0007:
            r0 = 0
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 14
            if (r1 < r2) goto L_0x0033
            android.content.Context r1 = r3.getApplicationContext()
            boolean r1 = r1 instanceof android.app.Application
            if (r1 == 0) goto L_0x001d
            android.content.Context r3 = r3.getApplicationContext()
            r0 = r3
            android.app.Application r0 = (android.app.Application) r0
        L_0x001d:
            if (r0 == 0) goto L_0x0033
            android.app.Application$ActivityLifecycleCallbacks r3 = com.tencent.bugly.crashreport.biz.C4802b.f10462k     // Catch:{ Exception -> 0x0029 }
            if (r3 == 0) goto L_0x0033
            android.app.Application$ActivityLifecycleCallbacks r3 = com.tencent.bugly.crashreport.biz.C4802b.f10462k     // Catch:{ Exception -> 0x0029 }
            r0.unregisterActivityLifecycleCallbacks(r3)     // Catch:{ Exception -> 0x0029 }
            goto L_0x0033
        L_0x0029:
            r3 = move-exception
            boolean r0 = com.tencent.bugly.proguard.C4888x.m16978a(r3)
            if (r0 != 0) goto L_0x0033
            r3.printStackTrace()
        L_0x0033:
            r3 = 0
            com.tencent.bugly.crashreport.biz.C4802b.f10453b = r3
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C4802b.m16459a(android.content.Context):void");
    }
}
