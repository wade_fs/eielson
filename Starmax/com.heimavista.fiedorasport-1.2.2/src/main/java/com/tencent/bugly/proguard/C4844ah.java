package com.tencent.bugly.proguard;

import com.facebook.appevents.AppEventsConstants;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.tencent.bugly.proguard.ah */
/* compiled from: BUGLY */
public final class C4844ah implements C4846aj {

    /* renamed from: a */
    private String f10796a = null;

    /* renamed from: a */
    public final byte[] mo26837a(byte[] bArr) {
        if (this.f10796a == null || bArr == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            stringBuffer.append(((int) b) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.f10796a.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, secretKeySpec, new IvParameterSpec(this.f10796a.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        for (byte b2 : doFinal) {
            stringBuffer2.append(((int) b2) + " ");
        }
        return doFinal;
    }

    /* renamed from: b */
    public final byte[] mo26838b(byte[] bArr) {
        if (this.f10796a == null || bArr == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            stringBuffer.append(((int) b) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.f10796a.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, secretKeySpec, new IvParameterSpec(this.f10796a.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        for (byte b2 : doFinal) {
            stringBuffer2.append(((int) b2) + " ");
        }
        return doFinal;
    }

    /* renamed from: a */
    public final void mo26836a(String str) {
        if (str != null) {
            for (int length = str.length(); length < 16; length++) {
                str = str + AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            this.f10796a = str.substring(0, 16);
        }
    }
}
