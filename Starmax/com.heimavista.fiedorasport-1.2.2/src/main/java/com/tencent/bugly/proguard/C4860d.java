package com.tencent.bugly.proguard;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.d */
/* compiled from: BUGLY */
public final class C4860d extends C4859c {

    /* renamed from: f */
    private static HashMap<String, byte[]> f10913f;

    /* renamed from: g */
    private static HashMap<String, HashMap<String, byte[]>> f10914g;

    /* renamed from: e */
    private C4862f f10915e = new C4862f();

    public C4860d() {
        this.f10915e.f10920a = 2;
    }

    /* renamed from: a */
    public final <T> void mo26816a(String str, Object obj) {
        if (!str.startsWith(".")) {
            super.mo26816a(str, obj);
            return;
        }
        throw new IllegalArgumentException("put name can not startwith . , now is " + str);
    }

    /* renamed from: b */
    public final void mo26848b(String str) {
        this.f10915e.f10922c = str;
    }

    /* renamed from: c */
    public final void mo26846c() {
        super.mo26846c();
        this.f10915e.f10920a = 3;
    }

    /* renamed from: b */
    public final void mo26847b(int i) {
        this.f10915e.f10921b = 1;
    }

    /* renamed from: c */
    public final void mo26849c(String str) {
        this.f10915e.f10923d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final byte[] mo26818a() {
        C4862f fVar = this.f10915e;
        if (fVar.f10920a != 2) {
            if (fVar.f10922c == null) {
                fVar.f10922c = "";
            }
            C4862f fVar2 = this.f10915e;
            if (fVar2.f10923d == null) {
                fVar2.f10923d = "";
            }
        } else if (fVar.f10922c.equals("")) {
            throw new IllegalArgumentException("servantName can not is null");
        } else if (this.f10915e.f10923d.equals("")) {
            throw new IllegalArgumentException("funcName can not is null");
        }
        C4867j jVar = new C4867j(0);
        jVar.mo26873a(this.f10784b);
        if (this.f10915e.f10920a == 2) {
            jVar.mo26882a((Map) this.f10783a, 0);
        } else {
            jVar.mo26882a((Map) super.f10910d, 0);
        }
        this.f10915e.f10924e = C4869l.m16876a(jVar.mo26874a());
        C4867j jVar2 = new C4867j(0);
        jVar2.mo26873a(this.f10784b);
        this.f10915e.mo26840a(jVar2);
        byte[] a = C4869l.m16876a(jVar2.mo26874a());
        int length = a.length + 4;
        ByteBuffer allocate = ByteBuffer.allocate(length);
        allocate.putInt(length).put(a).flip();
        return allocate.array();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* renamed from: a */
    public final void mo26817a(byte[] bArr) {
        if (bArr.length >= 4) {
            try {
                C4865i iVar = new C4865i(bArr, 4);
                iVar.mo26863a(this.f10784b);
                this.f10915e.mo26839a(iVar);
                if (this.f10915e.f10920a == 3) {
                    C4865i iVar2 = new C4865i(this.f10915e.f10924e);
                    iVar2.mo26863a(this.f10784b);
                    if (f10913f == null) {
                        HashMap<String, byte[]> hashMap = new HashMap<>();
                        f10913f = hashMap;
                        hashMap.put("", new byte[0]);
                    }
                    super.f10910d = iVar2.mo26867a((Map) f10913f, 0, false);
                    return;
                }
                C4865i iVar3 = new C4865i(this.f10915e.f10924e);
                iVar3.mo26863a(this.f10784b);
                if (f10914g == null) {
                    f10914g = new HashMap<>();
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("", new byte[0]);
                    f10914g.put("", hashMap2);
                }
                this.f10783a = iVar3.mo26867a((Map) f10914g, 0, false);
                new HashMap();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("decode package must include size head");
        }
    }
}
