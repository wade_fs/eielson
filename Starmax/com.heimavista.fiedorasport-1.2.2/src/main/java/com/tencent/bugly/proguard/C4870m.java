package com.tencent.bugly.proguard;

import java.io.Serializable;

/* renamed from: com.tencent.bugly.proguard.m */
/* compiled from: BUGLY */
public final class C4870m implements Serializable, Comparable<C4870m> {

    /* renamed from: a */
    public long f10938a;

    /* renamed from: b */
    public String f10939b;

    /* renamed from: c */
    public long f10940c;

    /* renamed from: d */
    public int f10941d;

    /* renamed from: e */
    public String f10942e;

    /* renamed from: f */
    public String f10943f;

    /* renamed from: g */
    public long f10944g;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return (int) (this.f10940c - ((C4870m) obj).f10940c);
    }
}
