package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.ap */
/* compiled from: BUGLY */
public final class C4852ap extends C4868k {

    /* renamed from: y */
    private static byte[] f10841y;

    /* renamed from: z */
    private static Map<String, String> f10842z = new HashMap();

    /* renamed from: a */
    public int f10843a = 0;

    /* renamed from: b */
    public String f10844b = "";

    /* renamed from: c */
    public String f10845c = "";

    /* renamed from: d */
    public String f10846d = "";

    /* renamed from: e */
    public String f10847e = "";

    /* renamed from: f */
    public String f10848f = "";

    /* renamed from: g */
    public int f10849g = 0;

    /* renamed from: h */
    public byte[] f10850h = null;

    /* renamed from: i */
    public String f10851i = "";

    /* renamed from: j */
    public String f10852j = "";

    /* renamed from: k */
    public Map<String, String> f10853k = null;

    /* renamed from: l */
    public String f10854l = "";

    /* renamed from: m */
    public long f10855m = 0;

    /* renamed from: n */
    public String f10856n = "";

    /* renamed from: o */
    public String f10857o = "";

    /* renamed from: p */
    public String f10858p = "";

    /* renamed from: q */
    public long f10859q = 0;

    /* renamed from: r */
    public String f10860r = "";

    /* renamed from: s */
    public String f10861s = "";

    /* renamed from: t */
    public String f10862t = "";

    /* renamed from: u */
    public String f10863u = "";

    /* renamed from: v */
    public String f10864v = "";

    /* renamed from: w */
    public String f10865w = "";

    /* renamed from: x */
    private String f10866x = "";

    static {
        byte[] bArr = new byte[1];
        f10841y = bArr;
        bArr[0] = 0;
        f10842z.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26876a(this.f10843a, 0);
        jVar.mo26880a(this.f10844b, 1);
        jVar.mo26880a(this.f10845c, 2);
        jVar.mo26880a(this.f10846d, 3);
        String str = this.f10847e;
        if (str != null) {
            jVar.mo26880a(str, 4);
        }
        jVar.mo26880a(this.f10848f, 5);
        jVar.mo26876a(this.f10849g, 6);
        jVar.mo26885a(this.f10850h, 7);
        String str2 = this.f10851i;
        if (str2 != null) {
            jVar.mo26880a(str2, 8);
        }
        String str3 = this.f10852j;
        if (str3 != null) {
            jVar.mo26880a(str3, 9);
        }
        Map<String, String> map = this.f10853k;
        if (map != null) {
            jVar.mo26882a((Map) map, 10);
        }
        String str4 = this.f10854l;
        if (str4 != null) {
            jVar.mo26880a(str4, 11);
        }
        jVar.mo26877a(this.f10855m, 12);
        String str5 = this.f10856n;
        if (str5 != null) {
            jVar.mo26880a(str5, 13);
        }
        String str6 = this.f10857o;
        if (str6 != null) {
            jVar.mo26880a(str6, 14);
        }
        String str7 = this.f10858p;
        if (str7 != null) {
            jVar.mo26880a(str7, 15);
        }
        jVar.mo26877a(this.f10859q, 16);
        String str8 = this.f10860r;
        if (str8 != null) {
            jVar.mo26880a(str8, 17);
        }
        String str9 = this.f10861s;
        if (str9 != null) {
            jVar.mo26880a(str9, 18);
        }
        String str10 = this.f10862t;
        if (str10 != null) {
            jVar.mo26880a(str10, 19);
        }
        String str11 = this.f10863u;
        if (str11 != null) {
            jVar.mo26880a(str11, 20);
        }
        String str12 = this.f10864v;
        if (str12 != null) {
            jVar.mo26880a(str12, 21);
        }
        String str13 = this.f10865w;
        if (str13 != null) {
            jVar.mo26880a(str13, 22);
        }
        String str14 = this.f10866x;
        if (str14 != null) {
            jVar.mo26880a(str14, 23);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10843a = iVar.mo26862a(this.f10843a, 0, true);
        this.f10844b = iVar.mo26871b(1, true);
        this.f10845c = iVar.mo26871b(2, true);
        this.f10846d = iVar.mo26871b(3, true);
        this.f10847e = iVar.mo26871b(4, false);
        this.f10848f = iVar.mo26871b(5, true);
        this.f10849g = iVar.mo26862a(this.f10849g, 6, true);
        this.f10850h = iVar.mo26872c(7, true);
        this.f10851i = iVar.mo26871b(8, false);
        this.f10852j = iVar.mo26871b(9, false);
        this.f10853k = (Map) iVar.mo26866a((Object) f10842z, 10, false);
        this.f10854l = iVar.mo26871b(11, false);
        this.f10855m = iVar.mo26864a(this.f10855m, 12, false);
        this.f10856n = iVar.mo26871b(13, false);
        this.f10857o = iVar.mo26871b(14, false);
        this.f10858p = iVar.mo26871b(15, false);
        this.f10859q = iVar.mo26864a(this.f10859q, 16, false);
        this.f10860r = iVar.mo26871b(17, false);
        this.f10861s = iVar.mo26871b(18, false);
        this.f10862t = iVar.mo26871b(19, false);
        this.f10863u = iVar.mo26871b(20, false);
        this.f10864v = iVar.mo26871b(21, false);
        this.f10865w = iVar.mo26871b(22, false);
        this.f10866x = iVar.mo26871b(23, false);
    }
}
