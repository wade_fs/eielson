package com.tencent.bugly.proguard;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C4806a;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutorService;

/* renamed from: com.tencent.bugly.proguard.y */
/* compiled from: BUGLY */
public final class C4889y {

    /* renamed from: a */
    public static boolean f11046a = true;

    /* renamed from: b */
    private static SimpleDateFormat f11047b = null;

    /* renamed from: c */
    private static int f11048c = 5120;

    /* renamed from: d */
    private static StringBuilder f11049d = null;

    /* renamed from: e */
    private static StringBuilder f11050e = null;

    /* renamed from: f */
    private static boolean f11051f = false;

    /* renamed from: g */
    private static C4890a f11052g = null;

    /* renamed from: h */
    private static String f11053h = null;

    /* renamed from: i */
    private static String f11054i = null;

    /* renamed from: j */
    private static Context f11055j = null;

    /* renamed from: k */
    private static String f11056k = null;

    /* renamed from: l */
    private static boolean f11057l = false;

    /* renamed from: m */
    private static boolean f11058m = false;

    /* renamed from: n */
    private static ExecutorService f11059n;

    /* renamed from: o */
    private static int f11060o;

    /* renamed from: p */
    private static final Object f11061p = new Object();

    /* renamed from: com.tencent.bugly.proguard.y$a */
    /* compiled from: BUGLY */
    public static class C4890a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f11062a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public File f11063b;

        /* renamed from: c */
        private String f11064c;

        /* renamed from: d */
        private long f11065d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public long f11066e = 30720;

        public C4890a(String str) {
            if (str != null && !str.equals("")) {
                this.f11064c = str;
                this.f11062a = m16997a();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m16997a() {
            try {
                this.f11063b = new File(this.f11064c);
                if (this.f11063b.exists() && !this.f11063b.delete()) {
                    this.f11062a = false;
                    return false;
                } else if (this.f11063b.createNewFile()) {
                    return true;
                } else {
                    this.f11062a = false;
                    return false;
                }
            } catch (Throwable th) {
                C4888x.m16978a(th);
                this.f11062a = false;
                return false;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[SYNTHETIC, Splitter:B:19:0x0036] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean mo26939a(java.lang.String r10) {
            /*
                r9 = this;
                boolean r0 = r9.f11062a
                r1 = 0
                if (r0 != 0) goto L_0x0006
                return r1
            L_0x0006:
                r0 = 0
                java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x002d }
                java.io.File r3 = r9.f11063b     // Catch:{ all -> 0x002d }
                r4 = 1
                r2.<init>(r3, r4)     // Catch:{ all -> 0x002d }
                java.lang.String r0 = "UTF-8"
                byte[] r10 = r10.getBytes(r0)     // Catch:{ all -> 0x002b }
                r2.write(r10)     // Catch:{ all -> 0x002b }
                r2.flush()     // Catch:{ all -> 0x002b }
                r2.close()     // Catch:{ all -> 0x002b }
                long r5 = r9.f11065d     // Catch:{ all -> 0x002b }
                int r10 = r10.length     // Catch:{ all -> 0x002b }
                long r7 = (long) r10     // Catch:{ all -> 0x002b }
                long r5 = r5 + r7
                r9.f11065d = r5     // Catch:{ all -> 0x002b }
                r9.f11062a = r4     // Catch:{ all -> 0x002b }
                r2.close()     // Catch:{ IOException -> 0x002a }
            L_0x002a:
                return r4
            L_0x002b:
                r10 = move-exception
                goto L_0x002f
            L_0x002d:
                r10 = move-exception
                r2 = r0
            L_0x002f:
                com.tencent.bugly.proguard.C4888x.m16978a(r10)     // Catch:{ all -> 0x003a }
                r9.f11062a = r1     // Catch:{ all -> 0x003a }
                if (r2 == 0) goto L_0x0039
                r2.close()     // Catch:{ IOException -> 0x0039 }
            L_0x0039:
                return r1
            L_0x003a:
                r10 = move-exception
                if (r2 == 0) goto L_0x0040
                r2.close()     // Catch:{ IOException -> 0x0040 }
            L_0x0040:
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4889y.C4890a.mo26939a(java.lang.String):boolean");
        }
    }

    /* renamed from: com.tencent.bugly.proguard.y$b */
    /* compiled from: BUGLY */
    static class C4891b implements Runnable {

        /* renamed from: P */
        private /* synthetic */ String f11067P;

        /* renamed from: Q */
        private /* synthetic */ String f11068Q;

        /* renamed from: R */
        private /* synthetic */ String f11069R;

        C4891b(String str, String str2, String str3) {
            this.f11067P = str;
            this.f11068Q = str2;
            this.f11069R = str3;
        }

        public final void run() {
            if (C4889y.m16994d(this.f11067P, this.f11068Q, this.f11069R)) {
            }
        }
    }

    /* renamed from: com.tencent.bugly.proguard.y$c */
    /* compiled from: BUGLY */
    static class C4892c implements Runnable {

        /* renamed from: P */
        private /* synthetic */ String f11070P;

        /* renamed from: Q */
        private /* synthetic */ String f11071Q;

        /* renamed from: R */
        private /* synthetic */ String f11072R;

        C4892c(String str, String str2, String str3) {
            this.f11070P = str;
            this.f11071Q = str2;
            this.f11072R = str3;
        }

        public final void run() {
            C4889y.m16995e(this.f11070P, this.f11071Q, this.f11072R);
        }
    }

    static {
        try {
            f11047b = new SimpleDateFormat("MM-dd HH:mm:ss");
        } catch (Throwable unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0071, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m16986a(android.content.Context r3) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.y> r0 = com.tencent.bugly.proguard.C4889y.class
            monitor-enter(r0)
            boolean r1 = com.tencent.bugly.proguard.C4889y.f11057l     // Catch:{ all -> 0x0072 }
            if (r1 != 0) goto L_0x0070
            if (r3 == 0) goto L_0x0070
            boolean r1 = com.tencent.bugly.proguard.C4889y.f11046a     // Catch:{ all -> 0x0072 }
            if (r1 != 0) goto L_0x000e
            goto L_0x0070
        L_0x000e:
            java.util.concurrent.ExecutorService r1 = java.util.concurrent.Executors.newSingleThreadExecutor()     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11059n = r1     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            r2 = 0
            r1.<init>(r2)     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11050e = r1     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            r1.<init>(r2)     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11049d = r1     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11055j = r3     // Catch:{ all -> 0x006b }
            com.tencent.bugly.crashreport.common.info.a r3 = com.tencent.bugly.crashreport.common.info.C4806a.m16491a(r3)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = r3.f10526d     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11053h = r1     // Catch:{ all -> 0x006b }
            r3.getClass()     // Catch:{ all -> 0x006b }
            java.lang.String r3 = ""
            com.tencent.bugly.proguard.C4889y.f11054i = r3     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            r3.<init>()     // Catch:{ all -> 0x006b }
            android.content.Context r1 = com.tencent.bugly.proguard.C4889y.f11055j     // Catch:{ all -> 0x006b }
            java.io.File r1 = r1.getFilesDir()     // Catch:{ all -> 0x006b }
            java.lang.String r1 = r1.getPath()     // Catch:{ all -> 0x006b }
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = "/buglylog_"
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = com.tencent.bugly.proguard.C4889y.f11053h     // Catch:{ all -> 0x006b }
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = "_"
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = com.tencent.bugly.proguard.C4889y.f11054i     // Catch:{ all -> 0x006b }
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r1 = ".txt"
            r3.append(r1)     // Catch:{ all -> 0x006b }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11056k = r3     // Catch:{ all -> 0x006b }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x006b }
            com.tencent.bugly.proguard.C4889y.f11060o = r3     // Catch:{ all -> 0x006b }
        L_0x006b:
            r3 = 1
            com.tencent.bugly.proguard.C4889y.f11057l = r3     // Catch:{ all -> 0x0072 }
            monitor-exit(r0)
            return
        L_0x0070:
            monitor-exit(r0)
            return
        L_0x0072:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4889y.m16986a(android.content.Context):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static boolean m16994d(String str, String str2, String str3) {
        try {
            C4806a b = C4806a.m16492b();
            if (b == null || b.f10475D == null) {
                return false;
            }
            return b.f10475D.appendLogToNative(str, str2, str3);
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:26|27|(1:29)(2:30|(1:34))|35|(1:37)|38|39|40|41) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x00dd */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m16995e(java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.y> r0 = com.tencent.bugly.proguard.C4889y.class
            monitor-enter(r0)
            int r1 = android.os.Process.myTid()     // Catch:{ all -> 0x00e3 }
            long r1 = (long) r1     // Catch:{ all -> 0x00e3 }
            java.lang.StringBuilder r3 = com.tencent.bugly.proguard.C4889y.f11049d     // Catch:{ all -> 0x00e3 }
            r4 = 0
            r3.setLength(r4)     // Catch:{ all -> 0x00e3 }
            int r3 = r9.length()     // Catch:{ all -> 0x00e3 }
            r5 = 1
            r6 = 30720(0x7800, float:4.3048E-41)
            if (r3 <= r6) goto L_0x0025
            int r3 = r9.length()     // Catch:{ all -> 0x00e3 }
            int r3 = r3 - r6
            int r6 = r9.length()     // Catch:{ all -> 0x00e3 }
            int r6 = r6 - r5
            java.lang.String r9 = r9.substring(r3, r6)     // Catch:{ all -> 0x00e3 }
        L_0x0025:
            java.util.Date r3 = new java.util.Date     // Catch:{ all -> 0x00e3 }
            r3.<init>()     // Catch:{ all -> 0x00e3 }
            java.text.SimpleDateFormat r6 = com.tencent.bugly.proguard.C4889y.f11047b     // Catch:{ all -> 0x00e3 }
            if (r6 == 0) goto L_0x0035
            java.text.SimpleDateFormat r6 = com.tencent.bugly.proguard.C4889y.f11047b     // Catch:{ all -> 0x00e3 }
            java.lang.String r3 = r6.format(r3)     // Catch:{ all -> 0x00e3 }
            goto L_0x0039
        L_0x0035:
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00e3 }
        L_0x0039:
            java.lang.StringBuilder r6 = com.tencent.bugly.proguard.C4889y.f11049d     // Catch:{ all -> 0x00e3 }
            r6.append(r3)     // Catch:{ all -> 0x00e3 }
            java.lang.String r3 = " "
            r6.append(r3)     // Catch:{ all -> 0x00e3 }
            int r3 = com.tencent.bugly.proguard.C4889y.f11060o     // Catch:{ all -> 0x00e3 }
            r6.append(r3)     // Catch:{ all -> 0x00e3 }
            java.lang.String r3 = " "
            r6.append(r3)     // Catch:{ all -> 0x00e3 }
            r6.append(r1)     // Catch:{ all -> 0x00e3 }
            java.lang.String r1 = " "
            r6.append(r1)     // Catch:{ all -> 0x00e3 }
            r6.append(r7)     // Catch:{ all -> 0x00e3 }
            java.lang.String r7 = " "
            r6.append(r7)     // Catch:{ all -> 0x00e3 }
            r6.append(r8)     // Catch:{ all -> 0x00e3 }
            java.lang.String r7 = ": "
            r6.append(r7)     // Catch:{ all -> 0x00e3 }
            r6.append(r9)     // Catch:{ all -> 0x00e3 }
            java.lang.String r7 = "\u0001\r\n"
            r6.append(r7)     // Catch:{ all -> 0x00e3 }
            java.lang.StringBuilder r7 = com.tencent.bugly.proguard.C4889y.f11049d     // Catch:{ all -> 0x00e3 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00e3 }
            java.lang.Object r8 = com.tencent.bugly.proguard.C4889y.f11061p     // Catch:{ all -> 0x00e3 }
            monitor-enter(r8)     // Catch:{ all -> 0x00e3 }
            java.lang.StringBuilder r9 = com.tencent.bugly.proguard.C4889y.f11050e     // Catch:{ all -> 0x00dd }
            r9.append(r7)     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r7 = com.tencent.bugly.proguard.C4889y.f11050e     // Catch:{ all -> 0x00dd }
            int r7 = r7.length()     // Catch:{ all -> 0x00dd }
            int r9 = com.tencent.bugly.proguard.C4889y.f11048c     // Catch:{ all -> 0x00dd }
            if (r7 > r9) goto L_0x0088
            monitor-exit(r8)     // Catch:{ all -> 0x00e0 }
            monitor-exit(r0)
            return
        L_0x0088:
            boolean r7 = com.tencent.bugly.proguard.C4889y.f11051f     // Catch:{ all -> 0x00dd }
            if (r7 == 0) goto L_0x008f
            monitor-exit(r8)     // Catch:{ all -> 0x00e0 }
            monitor-exit(r0)
            return
        L_0x008f:
            com.tencent.bugly.proguard.C4889y.f11051f = r5     // Catch:{ all -> 0x00dd }
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            if (r7 != 0) goto L_0x009f
            com.tencent.bugly.proguard.y$a r7 = new com.tencent.bugly.proguard.y$a     // Catch:{ all -> 0x00dd }
            java.lang.String r9 = com.tencent.bugly.proguard.C4889y.f11056k     // Catch:{ all -> 0x00dd }
            r7.<init>(r9)     // Catch:{ all -> 0x00dd }
            com.tencent.bugly.proguard.C4889y.f11052g = r7     // Catch:{ all -> 0x00dd }
            goto L_0x00c8
        L_0x009f:
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            java.io.File r7 = r7.f11063b     // Catch:{ all -> 0x00dd }
            if (r7 == 0) goto L_0x00c3
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            java.io.File r7 = r7.f11063b     // Catch:{ all -> 0x00dd }
            long r1 = r7.length()     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r7 = com.tencent.bugly.proguard.C4889y.f11050e     // Catch:{ all -> 0x00dd }
            int r7 = r7.length()     // Catch:{ all -> 0x00dd }
            long r5 = (long) r7     // Catch:{ all -> 0x00dd }
            long r1 = r1 + r5
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            long r5 = r7.f11066e     // Catch:{ all -> 0x00dd }
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00c8
        L_0x00c3:
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            boolean unused = r7.m16997a()     // Catch:{ all -> 0x00dd }
        L_0x00c8:
            com.tencent.bugly.proguard.y$a r7 = com.tencent.bugly.proguard.C4889y.f11052g     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r9 = com.tencent.bugly.proguard.C4889y.f11050e     // Catch:{ all -> 0x00dd }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00dd }
            boolean r7 = r7.mo26939a(r9)     // Catch:{ all -> 0x00dd }
            if (r7 == 0) goto L_0x00dd
            java.lang.StringBuilder r7 = com.tencent.bugly.proguard.C4889y.f11050e     // Catch:{ all -> 0x00dd }
            r7.setLength(r4)     // Catch:{ all -> 0x00dd }
            com.tencent.bugly.proguard.C4889y.f11051f = r4     // Catch:{ all -> 0x00dd }
        L_0x00dd:
            monitor-exit(r8)     // Catch:{ all -> 0x00e0 }
            monitor-exit(r0)
            return
        L_0x00e0:
            r7 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00e3 }
            throw r7     // Catch:{ all -> 0x00e3 }
        L_0x00e3:
            r7 = move-exception
            monitor-exit(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4889y.m16995e(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /* renamed from: b */
    private static String m16991b() {
        try {
            C4806a b = C4806a.m16492b();
            if (b == null || b.f10475D == null) {
                return null;
            }
            return b.f10475D.getLogFromNative();
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static void m16985a(int i) {
        synchronized (f11061p) {
            f11048c = i;
            if (i < 0) {
                f11048c = 0;
            } else if (i > 10240) {
                f11048c = 10240;
            }
        }
    }

    /* renamed from: a */
    public static void m16989a(boolean z) {
        C4888x.m16977a("[LogUtil] Whether can record user log into native: " + z, new Object[0]);
        f11058m = z;
    }

    /* renamed from: a */
    public static void m16988a(String str, String str2, Throwable th) {
        if (th != null) {
            String message = th.getMessage();
            if (message == null) {
                message = "";
            }
            m16987a(str, str2, message + 10 + C4893z.m17034b(th));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m16987a(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.y> r0 = com.tencent.bugly.proguard.C4889y.class
            monitor-enter(r0)
            boolean r1 = com.tencent.bugly.proguard.C4889y.f11057l     // Catch:{ all -> 0x002f }
            if (r1 == 0) goto L_0x002d
            boolean r1 = com.tencent.bugly.proguard.C4889y.f11046a     // Catch:{ all -> 0x002f }
            if (r1 != 0) goto L_0x000c
            goto L_0x002d
        L_0x000c:
            boolean r1 = com.tencent.bugly.proguard.C4889y.f11058m     // Catch:{ Exception -> 0x0027 }
            if (r1 == 0) goto L_0x001b
            java.util.concurrent.ExecutorService r1 = com.tencent.bugly.proguard.C4889y.f11059n     // Catch:{ Exception -> 0x0027 }
            com.tencent.bugly.proguard.y$b r2 = new com.tencent.bugly.proguard.y$b     // Catch:{ Exception -> 0x0027 }
            r2.<init>(r3, r4, r5)     // Catch:{ Exception -> 0x0027 }
            r1.execute(r2)     // Catch:{ Exception -> 0x0027 }
            goto L_0x002b
        L_0x001b:
            java.util.concurrent.ExecutorService r1 = com.tencent.bugly.proguard.C4889y.f11059n     // Catch:{ Exception -> 0x0027 }
            com.tencent.bugly.proguard.y$c r2 = new com.tencent.bugly.proguard.y$c     // Catch:{ Exception -> 0x0027 }
            r2.<init>(r3, r4, r5)     // Catch:{ Exception -> 0x0027 }
            r1.execute(r2)     // Catch:{ Exception -> 0x0027 }
            monitor-exit(r0)
            return
        L_0x0027:
            r3 = move-exception
            com.tencent.bugly.proguard.C4888x.m16981b(r3)     // Catch:{ all -> 0x002f }
        L_0x002b:
            monitor-exit(r0)
            return
        L_0x002d:
            monitor-exit(r0)
            return
        L_0x002f:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4889y.m16987a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.z.a(java.io.File, int, boolean):java.lang.String
     arg types: [java.io.File, int, int]
     candidates:
      com.tencent.bugly.proguard.z.a(android.content.Context, int, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.z.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.bugly.proguard.z.a(java.io.File, java.io.File, int):boolean
      com.tencent.bugly.proguard.z.a(int, byte[], byte[]):byte[]
      com.tencent.bugly.proguard.z.a(java.io.File, java.lang.String, java.lang.String):byte[]
      com.tencent.bugly.proguard.z.a(byte[], int, java.lang.String):byte[]
      com.tencent.bugly.proguard.z.a(java.io.File, int, boolean):java.lang.String */
    /* renamed from: a */
    public static byte[] m16990a() {
        if (!f11046a) {
            return null;
        }
        if (f11058m) {
            C4888x.m16977a("[LogUtil] Get user log from native.", new Object[0]);
            String b = m16991b();
            if (b != null) {
                C4888x.m16977a("[LogUtil] Got user log from native: %d bytes", Integer.valueOf(b.length()));
                return C4893z.m17028a((File) null, b, "BuglyNativeLog.txt");
            }
        }
        StringBuilder sb = new StringBuilder();
        synchronized (f11061p) {
            if (f11052g != null && f11052g.f11062a && f11052g.f11063b != null && f11052g.f11063b.length() > 0) {
                sb.append(C4893z.m17012a(f11052g.f11063b, 30720, true));
            }
            if (f11050e != null && f11050e.length() > 0) {
                sb.append(f11050e.toString());
            }
        }
        return C4893z.m17028a((File) null, sb.toString(), "BuglyLog.txt");
    }
}
