package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.as */
/* compiled from: BUGLY */
public final class C4855as extends C4868k implements Cloneable {

    /* renamed from: m */
    private static C4854ar f10879m = new C4854ar();

    /* renamed from: n */
    private static Map<String, String> f10880n = new HashMap();

    /* renamed from: o */
    private static /* synthetic */ boolean f10881o = (!C4855as.class.desiredAssertionStatus());

    /* renamed from: a */
    public boolean f10882a = true;

    /* renamed from: b */
    public boolean f10883b = true;

    /* renamed from: c */
    public boolean f10884c = true;

    /* renamed from: d */
    public String f10885d = "";

    /* renamed from: e */
    public String f10886e = "";

    /* renamed from: f */
    public C4854ar f10887f = null;

    /* renamed from: g */
    public Map<String, String> f10888g = null;

    /* renamed from: h */
    public long f10889h = 0;

    /* renamed from: i */
    public int f10890i = 0;

    /* renamed from: j */
    private String f10891j = "";

    /* renamed from: k */
    private String f10892k = "";

    /* renamed from: l */
    private int f10893l = 0;

    static {
        f10880n.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
     arg types: [com.tencent.bugly.proguard.ar, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26884a(this.f10882a, 0);
        jVar.mo26884a(this.f10883b, 1);
        jVar.mo26884a(this.f10884c, 2);
        String str = this.f10885d;
        if (str != null) {
            jVar.mo26880a(str, 3);
        }
        String str2 = this.f10886e;
        if (str2 != null) {
            jVar.mo26880a(str2, 4);
        }
        C4854ar arVar = this.f10887f;
        if (arVar != null) {
            jVar.mo26878a((C4868k) super, 5);
        }
        Map<String, String> map = this.f10888g;
        if (map != null) {
            jVar.mo26882a((Map) map, 6);
        }
        jVar.mo26877a(this.f10889h, 7);
        String str3 = this.f10891j;
        if (str3 != null) {
            jVar.mo26880a(str3, 8);
        }
        String str4 = this.f10892k;
        if (str4 != null) {
            jVar.mo26880a(str4, 9);
        }
        jVar.mo26876a(this.f10893l, 10);
        jVar.mo26876a(this.f10890i, 11);
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f10881o) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C4855as asVar = (C4855as) obj;
        if (!C4869l.m16875a(this.f10882a, asVar.f10882a) || !C4869l.m16875a(this.f10883b, asVar.f10883b) || !C4869l.m16875a(this.f10884c, asVar.f10884c) || !C4869l.m16874a(this.f10885d, asVar.f10885d) || !C4869l.m16874a(this.f10886e, asVar.f10886e) || !C4869l.m16874a(this.f10887f, asVar.f10887f) || !C4869l.m16874a(this.f10888g, asVar.f10888g) || !C4869l.m16873a(this.f10889h, asVar.f10889h) || !C4869l.m16874a(this.f10891j, asVar.f10891j) || !C4869l.m16874a(this.f10892k, asVar.f10892k) || !C4869l.m16872a(this.f10893l, asVar.f10893l) || !C4869l.m16872a(this.f10890i, asVar.f10890i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.i$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.i.a(int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
     arg types: [com.tencent.bugly.proguard.ar, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10882a = iVar.mo26870a(0, true);
        this.f10883b = iVar.mo26870a(1, true);
        this.f10884c = iVar.mo26870a(2, true);
        this.f10885d = iVar.mo26871b(3, false);
        this.f10886e = iVar.mo26871b(4, false);
        this.f10887f = (C4854ar) iVar.mo26865a((C4868k) f10879m, 5, false);
        this.f10888g = (Map) iVar.mo26866a((Object) f10880n, 6, false);
        this.f10889h = iVar.mo26864a(this.f10889h, 7, false);
        this.f10891j = iVar.mo26871b(8, false);
        this.f10892k = iVar.mo26871b(9, false);
        this.f10893l = iVar.mo26862a(this.f10893l, 10, false);
        this.f10890i = iVar.mo26862a(this.f10890i, 11, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.h.a(com.tencent.bugly.proguard.k, java.lang.String):com.tencent.bugly.proguard.h
     arg types: [com.tencent.bugly.proguard.ar, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.h.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(int, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(long, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(short, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(boolean, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(com.tencent.bugly.proguard.k, java.lang.String):com.tencent.bugly.proguard.h */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.h.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(int, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(long, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(com.tencent.bugly.proguard.k, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(short, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(boolean, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h */
    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
        C4864h hVar = new C4864h(sb, i);
        hVar.mo26859a(this.f10882a, "enable");
        hVar.mo26859a(this.f10883b, "enableUserInfo");
        hVar.mo26859a(this.f10884c, "enableQuery");
        hVar.mo26856a(this.f10885d, "url");
        hVar.mo26856a(this.f10886e, "expUrl");
        hVar.mo26855a((C4868k) this.f10887f, "security");
        hVar.mo26857a((Map) this.f10888g, "valueMap");
        hVar.mo26854a(this.f10889h, "strategylastUpdateTime");
        hVar.mo26856a(this.f10891j, "httpsUrl");
        hVar.mo26856a(this.f10892k, "httpsExpUrl");
        hVar.mo26853a(this.f10893l, "eventRecordCount");
        hVar.mo26853a(this.f10890i, "eventTimeInterval");
    }
}
