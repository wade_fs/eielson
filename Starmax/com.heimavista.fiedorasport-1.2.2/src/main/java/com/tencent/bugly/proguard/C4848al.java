package com.tencent.bugly.proguard;

import java.util.ArrayList;
import java.util.Collection;

/* renamed from: com.tencent.bugly.proguard.al */
/* compiled from: BUGLY */
public final class C4848al extends C4868k implements Cloneable {

    /* renamed from: c */
    private static ArrayList<String> f10803c;

    /* renamed from: a */
    private String f10804a = "";

    /* renamed from: b */
    private ArrayList<String> f10805b = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26880a(this.f10804a, 0);
        ArrayList<String> arrayList = this.f10805b;
        if (arrayList != null) {
            jVar.mo26881a((Collection) arrayList, 1);
        }
    }

    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10804a = iVar.mo26871b(0, true);
        if (f10803c == null) {
            f10803c = new ArrayList<>();
            f10803c.add("");
        }
        this.f10805b = (ArrayList) iVar.mo26866a((Object) f10803c, 1, false);
    }
}
