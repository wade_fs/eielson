package com.tencent.bugly.proguard;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import com.facebook.appevents.AppEventsConstants;
import com.tencent.bugly.crashreport.common.info.AppInfo;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.info.PlugInBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import p241io.jsonwebtoken.Header;

/* renamed from: com.tencent.bugly.proguard.z */
/* compiled from: BUGLY */
public class C4893z {

    /* renamed from: a */
    private static Map<String, String> f11073a = null;

    /* renamed from: b */
    private static boolean f11074b = false;

    /* renamed from: a */
    public static String m17013a(Throwable th) {
        if (th == null) {
            return "";
        }
        try {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            return stringWriter.getBuffer().toString();
        } catch (Throwable th2) {
            if (C4888x.m16978a(th2)) {
                return "fail";
            }
            th2.printStackTrace();
            return "fail";
        }
    }

    /* renamed from: b */
    private static byte[] m17044b(byte[] bArr, int i, String str) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        try {
            C4846aj a = C4836a.m16719a(i);
            if (a == null) {
                return null;
            }
            a.mo26836a(str);
            return a.mo26837a(bArr);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            C4888x.m16983d("encrytype %d %s", Integer.valueOf(i), str);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ad A[Catch:{ all -> 0x00c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b2 A[SYNTHETIC, Splitter:B:40:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00bc A[SYNTHETIC, Splitter:B:45:0x00bc] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.ArrayList<java.lang.String> m17046c(android.content.Context r5, java.lang.String r6) {
        /*
            boolean r5 = com.tencent.bugly.crashreport.common.info.AppInfo.m16486f(r5)
            if (r5 == 0) goto L_0x0016
            java.util.ArrayList r5 = new java.util.ArrayList
            java.lang.String r6 = "unknown(low memory)"
            java.lang.String[] r6 = new java.lang.String[]{r6}
            java.util.List r6 = java.util.Arrays.asList(r6)
            r5.<init>(r6)
            return r5
        L_0x0016:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r0 = 0
            java.lang.String r1 = "/system/bin/sh"
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x00a4 }
            r2.<init>(r1)     // Catch:{ all -> 0x00a4 }
            boolean r2 = r2.exists()     // Catch:{ all -> 0x00a4 }
            if (r2 == 0) goto L_0x0034
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x00a4 }
            r2.<init>(r1)     // Catch:{ all -> 0x00a4 }
            boolean r2 = r2.canExecute()     // Catch:{ all -> 0x00a4 }
            if (r2 != 0) goto L_0x0036
        L_0x0034:
            java.lang.String r1 = "sh"
        L_0x0036:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x00a4 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ all -> 0x00a4 }
            r4 = 0
            r3[r4] = r1     // Catch:{ all -> 0x00a4 }
            r1 = 1
            java.lang.String r4 = "-c"
            r3[r1] = r4     // Catch:{ all -> 0x00a4 }
            java.util.List r1 = java.util.Arrays.asList(r3)     // Catch:{ all -> 0x00a4 }
            r2.<init>(r1)     // Catch:{ all -> 0x00a4 }
            r2.add(r6)     // Catch:{ all -> 0x00a4 }
            java.lang.Runtime r6 = java.lang.Runtime.getRuntime()     // Catch:{ all -> 0x00a4 }
            r1 = 3
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ all -> 0x00a4 }
            java.lang.Object[] r1 = r2.toArray(r1)     // Catch:{ all -> 0x00a4 }
            java.lang.String[] r1 = (java.lang.String[]) r1     // Catch:{ all -> 0x00a4 }
            java.lang.Process r6 = r6.exec(r1)     // Catch:{ all -> 0x00a4 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x00a4 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x00a4 }
            java.io.InputStream r3 = r6.getInputStream()     // Catch:{ all -> 0x00a4 }
            r2.<init>(r3)     // Catch:{ all -> 0x00a4 }
            r1.<init>(r2)     // Catch:{ all -> 0x00a4 }
        L_0x006c:
            java.lang.String r2 = r1.readLine()     // Catch:{ all -> 0x00a1 }
            if (r2 == 0) goto L_0x0076
            r5.add(r2)     // Catch:{ all -> 0x00a1 }
            goto L_0x006c
        L_0x0076:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x00a1 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x00a1 }
            java.io.InputStream r6 = r6.getErrorStream()     // Catch:{ all -> 0x00a1 }
            r3.<init>(r6)     // Catch:{ all -> 0x00a1 }
            r2.<init>(r3)     // Catch:{ all -> 0x00a1 }
        L_0x0084:
            java.lang.String r6 = r2.readLine()     // Catch:{ all -> 0x009f }
            if (r6 == 0) goto L_0x008e
            r5.add(r6)     // Catch:{ all -> 0x009f }
            goto L_0x0084
        L_0x008e:
            r1.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0096
        L_0x0092:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0096:
            r2.close()     // Catch:{ IOException -> 0x009a }
            goto L_0x009e
        L_0x009a:
            r6 = move-exception
            r6.printStackTrace()
        L_0x009e:
            return r5
        L_0x009f:
            r5 = move-exception
            goto L_0x00a7
        L_0x00a1:
            r5 = move-exception
            r2 = r0
            goto L_0x00a7
        L_0x00a4:
            r5 = move-exception
            r1 = r0
            r2 = r1
        L_0x00a7:
            boolean r6 = com.tencent.bugly.proguard.C4888x.m16978a(r5)     // Catch:{ all -> 0x00c5 }
            if (r6 != 0) goto L_0x00b0
            r5.printStackTrace()     // Catch:{ all -> 0x00c5 }
        L_0x00b0:
            if (r1 == 0) goto L_0x00ba
            r1.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x00ba
        L_0x00b6:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00ba:
            if (r2 == 0) goto L_0x00c4
            r2.close()     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00c4
        L_0x00c0:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00c4:
            return r0
        L_0x00c5:
            r5 = move-exception
            if (r1 == 0) goto L_0x00d0
            r1.close()     // Catch:{ IOException -> 0x00cc }
            goto L_0x00d0
        L_0x00cc:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00d0:
            if (r2 == 0) goto L_0x00da
            r2.close()     // Catch:{ IOException -> 0x00d6 }
            goto L_0x00da
        L_0x00d6:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00da:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4893z.m17046c(android.content.Context, java.lang.String):java.util.ArrayList");
    }

    /* renamed from: a */
    public static String m17008a() {
        return m17009a(System.currentTimeMillis());
    }

    /* renamed from: a */
    public static String m17009a(long j) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date(j));
        } catch (Exception unused) {
            return new Date().toString();
        }
    }

    /* renamed from: b */
    public static byte[] m17042b(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(bArr.length);
        objArr[1] = i == 2 ? "Gzip" : Header.COMPRESSION_ALGORITHM;
        C4888x.m16982c("[Util] Unzip %d bytes data with type %s", objArr);
        try {
            C4841ae a = C4840ad.m16752a(i);
            if (a == null) {
                return null;
            }
            return a.mo26835b(bArr);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public static String m17014a(Date date) {
        if (date == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(date);
        } catch (Exception unused) {
            return new Date().toString();
        }
    }

    /* renamed from: b */
    public static byte[] m17043b(byte[] bArr, int i, int i2, String str) {
        try {
            return m17042b(m17044b(bArr, 1, str), 2);
        } catch (Exception e) {
            if (C4888x.m16978a(e)) {
                return null;
            }
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    private static byte[] m17031a(byte[] bArr, int i, String str) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        C4888x.m16982c("rqdp{  enD:} %d %d", Integer.valueOf(bArr.length), Integer.valueOf(i));
        try {
            C4846aj a = C4836a.m16719a(i);
            if (a == null) {
                return null;
            }
            a.mo26836a(str);
            return a.mo26838b(bArr);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: b */
    public static long m17032b() {
        try {
            return (((System.currentTimeMillis() + ((long) TimeZone.getDefault().getRawOffset())) / 86400000) * 86400000) - ((long) TimeZone.getDefault().getRawOffset());
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return -1;
            }
            th.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x006a A[Catch:{ all -> 0x007d }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006f A[SYNTHETIC, Splitter:B:28:0x006f] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] m17028a(java.io.File r5, java.lang.String r6, java.lang.String r7) {
        /*
            java.lang.String r5 = "rqdp{  ZF end}"
            r0 = 0
            if (r6 == 0) goto L_0x008e
            int r1 = r6.length()
            if (r1 != 0) goto L_0x000d
            goto L_0x008e
        L_0x000d:
            r1 = 0
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.String r3 = "rqdp{  ZF start}"
            com.tencent.bugly.proguard.C4888x.m16982c(r3, r2)
            java.lang.String r2 = "UTF-8"
            byte[] r6 = r6.getBytes(r2)     // Catch:{ all -> 0x0062 }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0062 }
            r2.<init>(r6)     // Catch:{ all -> 0x0062 }
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0062 }
            r6.<init>()     // Catch:{ all -> 0x0062 }
            java.util.zip.ZipOutputStream r3 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x0062 }
            r3.<init>(r6)     // Catch:{ all -> 0x0062 }
            r4 = 8
            r3.setMethod(r4)     // Catch:{ all -> 0x0060 }
            java.util.zip.ZipEntry r4 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0060 }
            r4.<init>(r7)     // Catch:{ all -> 0x0060 }
            r3.putNextEntry(r4)     // Catch:{ all -> 0x0060 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ all -> 0x0060 }
        L_0x003b:
            int r4 = r2.read(r7)     // Catch:{ all -> 0x0060 }
            if (r4 <= 0) goto L_0x0045
            r3.write(r7, r1, r4)     // Catch:{ all -> 0x0060 }
            goto L_0x003b
        L_0x0045:
            r3.closeEntry()     // Catch:{ all -> 0x0060 }
            r3.flush()     // Catch:{ all -> 0x0060 }
            r3.finish()     // Catch:{ all -> 0x0060 }
            byte[] r6 = r6.toByteArray()     // Catch:{ all -> 0x0060 }
            r3.close()     // Catch:{ IOException -> 0x0056 }
            goto L_0x005a
        L_0x0056:
            r7 = move-exception
            r7.printStackTrace()
        L_0x005a:
            java.lang.Object[] r7 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r7)
            return r6
        L_0x0060:
            r6 = move-exception
            goto L_0x0064
        L_0x0062:
            r6 = move-exception
            r3 = r0
        L_0x0064:
            boolean r7 = com.tencent.bugly.proguard.C4888x.m16978a(r6)     // Catch:{ all -> 0x007d }
            if (r7 != 0) goto L_0x006d
            r6.printStackTrace()     // Catch:{ all -> 0x007d }
        L_0x006d:
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ IOException -> 0x0073 }
            goto L_0x0077
        L_0x0073:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0077:
            java.lang.Object[] r6 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r6)
            return r0
        L_0x007d:
            r6 = move-exception
            if (r3 == 0) goto L_0x0088
            r3.close()     // Catch:{ IOException -> 0x0084 }
            goto L_0x0088
        L_0x0084:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0088:
            java.lang.Object[] r7 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r7)
            throw r6
        L_0x008e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4893z.m17028a(java.io.File, java.lang.String, java.lang.String):byte[]");
    }

    /* renamed from: b */
    public static String m17035b(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "NULL";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bArr);
            return m17015a(instance.digest());
        } catch (Throwable th) {
            if (C4888x.m16978a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public static void m17037b(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: b */
    public static void m17039b(String str) {
        if (str != null) {
            File file = new File(str);
            if (file.isFile() && file.exists() && file.canWrite()) {
                file.delete();
            }
        }
    }

    /* renamed from: c */
    public static byte[] m17048c(long j) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            return sb.toString().getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public static String m17034b(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.flush();
        return stringWriter.toString();
    }

    /* renamed from: c */
    public static long m17045c(byte[] bArr) {
        if (bArr == null) {
            return -1;
        }
        try {
            return Long.parseLong(new String(bArr, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* renamed from: c */
    public static boolean m17047c(String str) {
        if (str == null || str.trim().length() <= 0) {
            return false;
        }
        if (str.length() > 255) {
            C4888x.m16977a("URL(%s)'s length is larger than 255.", str);
            return false;
        } else if (str.toLowerCase().startsWith("http")) {
            return true;
        } else {
            C4888x.m16977a("URL(%s) is not start with \"http\".", str);
            return false;
        }
    }

    /* renamed from: b */
    public static void m17038b(Parcel parcel, Map<String, String> map) {
        if (map == null || map.size() <= 0) {
            parcel.writeBundle(null);
            return;
        }
        int size = map.size();
        ArrayList arrayList = new ArrayList(size);
        ArrayList arrayList2 = new ArrayList(size);
        for (Map.Entry entry : map.entrySet()) {
            arrayList.add(entry.getKey());
            arrayList2.add(entry.getValue());
        }
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("keys", arrayList);
        bundle.putStringArrayList("values", arrayList2);
        parcel.writeBundle(bundle);
    }

    /* renamed from: a */
    public static byte[] m17029a(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(bArr.length);
        objArr[1] = i == 2 ? "Gzip" : Header.COMPRESSION_ALGORITHM;
        C4888x.m16982c("[Util] Zip %d bytes data with type %s", objArr);
        try {
            C4841ae a = C4840ad.m16752a(i);
            if (a == null) {
                return null;
            }
            return a.mo26834a(bArr);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: b */
    public static Map<String, String> m17036b(Parcel parcel) {
        Bundle readBundle = parcel.readBundle();
        HashMap hashMap = null;
        if (readBundle == null) {
            return null;
        }
        ArrayList<String> stringArrayList = readBundle.getStringArrayList("keys");
        ArrayList<String> stringArrayList2 = readBundle.getStringArrayList("values");
        if (stringArrayList == null || stringArrayList2 == null || stringArrayList.size() != stringArrayList2.size()) {
            C4888x.m16984e("map parcel error!", new Object[0]);
        } else {
            hashMap = new HashMap(stringArrayList.size());
            for (int i = 0; i < stringArrayList.size(); i++) {
                hashMap.put(stringArrayList.get(i), stringArrayList2.get(i));
            }
        }
        return hashMap;
    }

    /* renamed from: a */
    public static byte[] m17030a(byte[] bArr, int i, int i2, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return m17031a(m17029a(bArr, 2), 1, str);
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public static String m17015a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() == 1) {
                stringBuffer.append(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            }
            stringBuffer.append(hexString);
        }
        return stringBuffer.toString().toUpperCase();
    }

    /* renamed from: b */
    public static byte[] m17041b(int i, byte[] bArr, byte[] bArr2) {
        try {
            PublicKey generatePublic = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bArr2));
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            instance.init(1, generatePublic);
            return instance.doFinal(bArr);
        } catch (Exception e) {
            if (C4888x.m16981b(e)) {
                return null;
            }
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00bc A[Catch:{ all -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c1 A[SYNTHETIC, Splitter:B:58:0x00c1] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00cb A[SYNTHETIC, Splitter:B:63:0x00cb] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m17022a(java.io.File r6, java.io.File r7, int r8) {
        /*
            java.lang.String r8 = "rqdp{  ZF end}"
            r0 = 0
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r2 = "rqdp{  ZF start}"
            com.tencent.bugly.proguard.C4888x.m16982c(r2, r1)
            if (r6 == 0) goto L_0x00fd
            if (r7 == 0) goto L_0x00fd
            boolean r1 = r6.equals(r7)
            if (r1 == 0) goto L_0x0016
            goto L_0x00fd
        L_0x0016:
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x00f5
            boolean r1 = r6.canRead()
            if (r1 != 0) goto L_0x0024
            goto L_0x00f5
        L_0x0024:
            java.io.File r1 = r7.getParentFile()     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x003b
            java.io.File r1 = r7.getParentFile()     // Catch:{ all -> 0x0045 }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x0045 }
            if (r1 != 0) goto L_0x003b
            java.io.File r1 = r7.getParentFile()     // Catch:{ all -> 0x0045 }
            r1.mkdirs()     // Catch:{ all -> 0x0045 }
        L_0x003b:
            boolean r1 = r7.exists()     // Catch:{ all -> 0x0045 }
            if (r1 != 0) goto L_0x004f
            r7.createNewFile()     // Catch:{ all -> 0x0045 }
            goto L_0x004f
        L_0x0045:
            r1 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r1)
            if (r2 != 0) goto L_0x004f
            r1.printStackTrace()
        L_0x004f:
            boolean r1 = r7.exists()
            if (r1 == 0) goto L_0x00f4
            boolean r1 = r7.canRead()
            if (r1 != 0) goto L_0x005d
            goto L_0x00f4
        L_0x005d:
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x00b3 }
            r2.<init>(r6)     // Catch:{ all -> 0x00b3 }
            java.util.zip.ZipOutputStream r3 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x00b0 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00b0 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ all -> 0x00b0 }
            r5.<init>(r7)     // Catch:{ all -> 0x00b0 }
            r4.<init>(r5)     // Catch:{ all -> 0x00b0 }
            r3.<init>(r4)     // Catch:{ all -> 0x00b0 }
            r7 = 8
            r3.setMethod(r7)     // Catch:{ all -> 0x00ae }
            java.util.zip.ZipEntry r7 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x00ae }
            java.lang.String r6 = r6.getName()     // Catch:{ all -> 0x00ae }
            r7.<init>(r6)     // Catch:{ all -> 0x00ae }
            r3.putNextEntry(r7)     // Catch:{ all -> 0x00ae }
            r6 = 5000(0x1388, float:7.006E-42)
            byte[] r6 = new byte[r6]     // Catch:{ all -> 0x00ae }
        L_0x0087:
            int r7 = r2.read(r6)     // Catch:{ all -> 0x00ae }
            if (r7 <= 0) goto L_0x0091
            r3.write(r6, r0, r7)     // Catch:{ all -> 0x00ae }
            goto L_0x0087
        L_0x0091:
            r3.flush()     // Catch:{ all -> 0x00ae }
            r3.closeEntry()     // Catch:{ all -> 0x00ae }
            r2.close()     // Catch:{ IOException -> 0x009b }
            goto L_0x009f
        L_0x009b:
            r6 = move-exception
            r6.printStackTrace()
        L_0x009f:
            r3.close()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x00a7
        L_0x00a3:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00a7:
            java.lang.Object[] r6 = new java.lang.Object[r0]
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r6)
            r6 = 1
            return r6
        L_0x00ae:
            r6 = move-exception
            goto L_0x00b6
        L_0x00b0:
            r6 = move-exception
            r3 = r1
            goto L_0x00b6
        L_0x00b3:
            r6 = move-exception
            r2 = r1
            r3 = r2
        L_0x00b6:
            boolean r7 = com.tencent.bugly.proguard.C4888x.m16978a(r6)     // Catch:{ all -> 0x00d9 }
            if (r7 != 0) goto L_0x00bf
            r6.printStackTrace()     // Catch:{ all -> 0x00d9 }
        L_0x00bf:
            if (r2 == 0) goto L_0x00c9
            r2.close()     // Catch:{ IOException -> 0x00c5 }
            goto L_0x00c9
        L_0x00c5:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00c9:
            if (r3 == 0) goto L_0x00d3
            r3.close()     // Catch:{ IOException -> 0x00cf }
            goto L_0x00d3
        L_0x00cf:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00d3:
            java.lang.Object[] r6 = new java.lang.Object[r0]
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r6)
            return r0
        L_0x00d9:
            r6 = move-exception
            if (r2 == 0) goto L_0x00e4
            r2.close()     // Catch:{ IOException -> 0x00e0 }
            goto L_0x00e4
        L_0x00e0:
            r7 = move-exception
            r7.printStackTrace()
        L_0x00e4:
            if (r3 == 0) goto L_0x00ee
            r3.close()     // Catch:{ IOException -> 0x00ea }
            goto L_0x00ee
        L_0x00ea:
            r7 = move-exception
            r7.printStackTrace()
        L_0x00ee:
            java.lang.Object[] r7 = new java.lang.Object[r0]
            com.tencent.bugly.proguard.C4888x.m16982c(r8, r7)
            throw r6
        L_0x00f4:
            return r0
        L_0x00f5:
            java.lang.Object[] r6 = new java.lang.Object[r0]
            java.lang.String r7 = "rqdp{  !sFile.exists() || !sFile.canRead(),pls check ,return!}"
            com.tencent.bugly.proguard.C4888x.m16983d(r7, r6)
            return r0
        L_0x00fd:
            java.lang.Object[] r6 = new java.lang.Object[r0]
            java.lang.String r7 = "rqdp{  err ZF 1R!}"
            com.tencent.bugly.proguard.C4888x.m16983d(r7, r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4893z.m17022a(java.io.File, java.io.File, int):boolean");
    }

    /* renamed from: b */
    public static boolean m17040b(Context context, String str) {
        C4888x.m16982c("[Util] Try to unlock file: %s (pid=%d | tid=%d)", str, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            File file = new File(context.getFilesDir() + File.separator + str);
            if (!file.exists()) {
                return true;
            }
            if (!file.delete()) {
                return false;
            }
            C4888x.m16982c("[Util] Successfully unlocked file: %s (pid=%d | tid=%d)", str, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            return true;
        } catch (Throwable th) {
            C4888x.m16978a(th);
            return false;
        }
    }

    /* renamed from: b */
    public static String m17033b(String str, String str2) {
        return (C4806a.m16492b() == null || C4806a.m16492b().f10476E == null) ? "" : C4806a.m16492b().f10476E.getString(str, str2);
    }

    /* renamed from: a */
    public static String m17011a(Context context, String str) {
        Class<C4893z> cls = C4893z.class;
        if (str == null || str.trim().equals("")) {
            return "";
        }
        if (f11073a == null) {
            f11073a = new HashMap();
            ArrayList<String> c = m17046c(context, "getprop");
            if (c != null && c.size() > 0) {
                C4888x.m16979b(cls, "Successfully get 'getprop' list.", new Object[0]);
                Pattern compile = Pattern.compile("\\[(.+)\\]: \\[(.*)\\]");
                for (String str2 : c) {
                    Matcher matcher = compile.matcher(str2);
                    if (matcher.find()) {
                        f11073a.put(matcher.group(1), matcher.group(2));
                    }
                }
                C4888x.m16979b(cls, "System properties number: %d.", Integer.valueOf(f11073a.size()));
            }
        }
        return f11073a.containsKey(str) ? f11073a.get(str) : "fail";
    }

    /* renamed from: a */
    public static boolean m17024a(String str) {
        return str == null || str.trim().length() <= 0;
    }

    /* renamed from: a */
    public static Context m17002a(Context context) {
        Context applicationContext;
        return (context == null || (applicationContext = context.getApplicationContext()) == null) ? context : applicationContext;
    }

    /* renamed from: a */
    public static void m17020a(Class<?> cls, String str, Object obj, Object obj2) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            declaredField.set(null, obj);
        } catch (Exception unused) {
        }
    }

    /* renamed from: a */
    public static Object m17006a(String str, String str2, Object obj, Class<?>[] clsArr, Object[] objArr) {
        try {
            Method declaredMethod = Class.forName(str).getDeclaredMethod(str2, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(null, objArr);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: a */
    public static void m17019a(Parcel parcel, Map<String, PlugInBean> map) {
        if (map == null || map.size() <= 0) {
            parcel.writeBundle(null);
            return;
        }
        int size = map.size();
        ArrayList arrayList = new ArrayList(size);
        ArrayList arrayList2 = new ArrayList(size);
        for (Map.Entry entry : map.entrySet()) {
            arrayList.add(entry.getKey());
            arrayList2.add(entry.getValue());
        }
        Bundle bundle = new Bundle();
        bundle.putInt("pluginNum", arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            bundle.putString("pluginKey" + i, (String) arrayList.get(i));
        }
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            bundle.putString("pluginVal" + i2 + "plugInId", ((PlugInBean) arrayList2.get(i2)).f10468a);
            bundle.putString("pluginVal" + i2 + "plugInUUID", ((PlugInBean) arrayList2.get(i2)).f10470c);
            bundle.putString("pluginVal" + i2 + "plugInVersion", ((PlugInBean) arrayList2.get(i2)).f10469b);
        }
        parcel.writeBundle(bundle);
    }

    /* renamed from: a */
    public static Map<String, PlugInBean> m17018a(Parcel parcel) {
        Bundle readBundle = parcel.readBundle();
        HashMap hashMap = null;
        if (readBundle == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        int intValue = ((Integer) readBundle.get("pluginNum")).intValue();
        for (int i = 0; i < intValue; i++) {
            arrayList.add(readBundle.getString("pluginKey" + i));
        }
        for (int i2 = 0; i2 < intValue; i2++) {
            String string = readBundle.getString("pluginVal" + i2 + "plugInId");
            String string2 = readBundle.getString("pluginVal" + i2 + "plugInUUID");
            arrayList2.add(new PlugInBean(string, readBundle.getString("pluginVal" + i2 + "plugInVersion"), string2));
        }
        if (arrayList.size() == arrayList2.size()) {
            hashMap = new HashMap(arrayList.size());
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                hashMap.put(arrayList.get(i3), PlugInBean.class.cast(arrayList2.get(i3)));
            }
        } else {
            C4888x.m16984e("map plugin parcel error!", new Object[0]);
        }
        return hashMap;
    }

    /* renamed from: a */
    public static byte[] m17027a(Parcelable parcelable) {
        Parcel obtain = Parcel.obtain();
        parcelable.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        return marshall;
    }

    /* renamed from: a */
    public static <T> T m17007a(byte[] bArr, Parcelable.Creator creator) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        try {
            T createFromParcel = creator.createFromParcel(obtain);
            if (obtain != null) {
                obtain.recycle();
            }
            return createFromParcel;
        } catch (Throwable th) {
            if (obtain != null) {
                obtain.recycle();
            }
            throw th;
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Process, java.lang.String] */
    /* renamed from: a */
    public static String m17010a(Context context, int i, String str) {
        String[] strArr;
        ? r0 = 0;
        if (!AppInfo.m16481a(context, "android.permission.READ_LOGS")) {
            C4888x.m16983d("no read_log permission!", new Object[0]);
            return r0;
        }
        if (str == null) {
            strArr = new String[]{"logcat", "-d", "-v", "threadtime"};
        } else {
            strArr = new String[]{"logcat", "-d", "-v", "threadtime", "-s", str};
        }
        StringBuilder sb = new StringBuilder();
        try {
            Process exec = Runtime.getRuntime().exec(strArr);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\n");
                if (i > 0 && sb.length() > i) {
                    sb.delete(0, sb.length() - i);
                }
            }
            String sb2 = sb.toString();
            if (exec != null) {
                try {
                    exec.getOutputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    exec.getInputStream().close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    exec.getErrorStream().close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            return sb2;
        } catch (Throwable th) {
            if (r0 != 0) {
                try {
                    r0.getOutputStream().close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                try {
                    r0.getInputStream().close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                try {
                    r0.getErrorStream().close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            throw th;
        }
    }

    /* renamed from: a */
    public static Map<String, String> m17017a(int i, boolean z) {
        HashMap hashMap = new HashMap(12);
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        if (allStackTraces == null) {
            return null;
        }
        Thread thread = Looper.getMainLooper().getThread();
        if (!allStackTraces.containsKey(thread)) {
            allStackTraces.put(thread, thread.getStackTrace());
        }
        Thread.currentThread().getId();
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : allStackTraces.entrySet()) {
            int i2 = 0;
            sb.setLength(0);
            if (!(entry.getValue() == null || ((StackTraceElement[]) entry.getValue()).length == 0)) {
                StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) entry.getValue();
                int length = stackTraceElementArr.length;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTraceElementArr[i2];
                    if (i > 0 && sb.length() >= i) {
                        sb.append("\n[Stack over limit size :" + i + " , has been cut!]");
                        break;
                    }
                    sb.append(stackTraceElement.toString());
                    sb.append("\n");
                    i2++;
                }
                hashMap.put(((Thread) entry.getKey()).getName() + "(" + ((Thread) entry.getKey()).getId() + ")", sb.toString());
            }
        }
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0035 A[SYNTHETIC, Splitter:B:20:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0055 A[SYNTHETIC, Splitter:B:27:0x0055] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized byte[] m17025a(int r6) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.z> r6 = com.tencent.bugly.proguard.C4893z.class
            monitor-enter(r6)
            r0 = 16
            r1 = 0
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.lang.String r5 = "/dev/urandom"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            r2.readFully(r0)     // Catch:{ Exception -> 0x0021 }
            r2.close()     // Catch:{ Exception -> 0x005b }
            monitor-exit(r6)
            return r0
        L_0x0021:
            r0 = move-exception
            goto L_0x0028
        L_0x0023:
            r0 = move-exception
            r2 = r1
            goto L_0x0053
        L_0x0026:
            r0 = move-exception
            r2 = r1
        L_0x0028:
            java.lang.String r3 = "Failed to read from /dev/urandom : %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0052 }
            r5 = 0
            r4[r5] = r0     // Catch:{ all -> 0x0052 }
            com.tencent.bugly.proguard.C4888x.m16984e(r3, r4)     // Catch:{ all -> 0x0052 }
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ Exception -> 0x005b }
        L_0x0038:
            java.lang.String r0 = "AES"
            javax.crypto.KeyGenerator r0 = javax.crypto.KeyGenerator.getInstance(r0)     // Catch:{ Exception -> 0x005b }
            r2 = 128(0x80, float:1.794E-43)
            java.security.SecureRandom r3 = new java.security.SecureRandom     // Catch:{ Exception -> 0x005b }
            r3.<init>()     // Catch:{ Exception -> 0x005b }
            r0.init(r2, r3)     // Catch:{ Exception -> 0x005b }
            javax.crypto.SecretKey r0 = r0.generateKey()     // Catch:{ Exception -> 0x005b }
            byte[] r0 = r0.getEncoded()     // Catch:{ Exception -> 0x005b }
            monitor-exit(r6)
            return r0
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ Exception -> 0x005b }
        L_0x0058:
            throw r0     // Catch:{ Exception -> 0x005b }
        L_0x0059:
            r0 = move-exception
            goto L_0x0067
        L_0x005b:
            r0 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16981b(r0)     // Catch:{ all -> 0x0059 }
            if (r2 != 0) goto L_0x0065
            r0.printStackTrace()     // Catch:{ all -> 0x0059 }
        L_0x0065:
            monitor-exit(r6)
            return r1
        L_0x0067:
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4893z.m17025a(int):byte[]");
    }

    @TargetApi(19)
    /* renamed from: a */
    public static byte[] m17026a(int i, byte[] bArr, byte[] bArr2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            if (Build.VERSION.SDK_INT >= 21) {
                if (!f11074b) {
                    instance.init(i, secretKeySpec, new GCMParameterSpec(instance.getBlockSize() << 3, bArr2));
                    return instance.doFinal(bArr);
                }
            }
            instance.init(i, secretKeySpec, new IvParameterSpec(bArr2));
            return instance.doFinal(bArr);
        } catch (InvalidAlgorithmParameterException e) {
            f11074b = true;
            throw e;
        } catch (Exception e2) {
            if (C4888x.m16981b(e2)) {
                return null;
            }
            e2.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static boolean m17021a(Context context, String str, long j) {
        C4888x.m16982c("[Util] Try to lock file:%s (pid=%d | tid=%d)", str, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            File file = new File(context.getFilesDir() + File.separator + str);
            if (file.exists()) {
                if (System.currentTimeMillis() - file.lastModified() < j) {
                    return false;
                }
                C4888x.m16982c("[Util] Lock file (%s) is expired, unlock it.", str);
                m17040b(context, str);
            }
            if (file.createNewFile()) {
                C4888x.m16982c("[Util] Successfully locked file: %s (pid=%d | tid=%d)", str, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                return true;
            }
            C4888x.m16982c("[Util] Failed to locked file: %s (pid=%d | tid=%d)", str, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            return false;
        } catch (Throwable th) {
            C4888x.m16978a(th);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0067 A[SYNTHETIC, Splitter:B:30:0x0067] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m17012a(java.io.File r5, int r6, boolean r7) {
        /*
            r0 = 0
            if (r5 == 0) goto L_0x007c
            boolean r1 = r5.exists()
            if (r1 == 0) goto L_0x007c
            boolean r1 = r5.canRead()
            if (r1 != 0) goto L_0x0011
            goto L_0x007c
        L_0x0011:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0060 }
            r1.<init>()     // Catch:{ all -> 0x0060 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0060 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0060 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x0060 }
            r4.<init>(r5)     // Catch:{ all -> 0x0060 }
            java.lang.String r5 = "utf-8"
            r3.<init>(r4, r5)     // Catch:{ all -> 0x0060 }
            r2.<init>(r3)     // Catch:{ all -> 0x0060 }
        L_0x0027:
            java.lang.String r5 = r2.readLine()     // Catch:{ all -> 0x005e }
            if (r5 == 0) goto L_0x0051
            r1.append(r5)     // Catch:{ all -> 0x005e }
            java.lang.String r5 = "\n"
            r1.append(r5)     // Catch:{ all -> 0x005e }
            if (r6 <= 0) goto L_0x0027
            int r5 = r1.length()     // Catch:{ all -> 0x005e }
            if (r5 <= r6) goto L_0x0027
            if (r7 == 0) goto L_0x0047
            int r5 = r1.length()     // Catch:{ all -> 0x005e }
            r1.delete(r6, r5)     // Catch:{ all -> 0x005e }
            goto L_0x0051
        L_0x0047:
            r5 = 0
            int r3 = r1.length()     // Catch:{ all -> 0x005e }
            int r3 = r3 - r6
            r1.delete(r5, r3)     // Catch:{ all -> 0x005e }
            goto L_0x0027
        L_0x0051:
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x005e }
            r2.close()     // Catch:{ Exception -> 0x0059 }
            goto L_0x005d
        L_0x0059:
            r6 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r6)
        L_0x005d:
            return r5
        L_0x005e:
            r5 = move-exception
            goto L_0x0062
        L_0x0060:
            r5 = move-exception
            r2 = r0
        L_0x0062:
            com.tencent.bugly.proguard.C4888x.m16978a(r5)     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x006f
            r2.close()     // Catch:{ Exception -> 0x006b }
            goto L_0x006f
        L_0x006b:
            r5 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r5)
        L_0x006f:
            return r0
        L_0x0070:
            r5 = move-exception
            if (r2 == 0) goto L_0x007b
            r2.close()     // Catch:{ Exception -> 0x0077 }
            goto L_0x007b
        L_0x0077:
            r6 = move-exception
            com.tencent.bugly.proguard.C4888x.m16978a(r6)
        L_0x007b:
            throw r5
        L_0x007c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4893z.m17012a(java.io.File, int, boolean):java.lang.String");
    }

    /* renamed from: a */
    private static BufferedReader m17004a(File file) {
        if (file != null && file.exists() && file.canRead()) {
            try {
                return new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"));
            } catch (Throwable th) {
                C4888x.m16978a(th);
            }
        }
        return null;
    }

    /* renamed from: a */
    public static BufferedReader m17005a(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            File file = new File(str, str2);
            if (file.exists()) {
                if (file.canRead()) {
                    return m17004a(file);
                }
            }
            return null;
        } catch (NullPointerException e) {
            C4888x.m16978a(e);
            return null;
        }
    }

    /* renamed from: a */
    public static Thread m17016a(Runnable runnable, String str) {
        try {
            Thread thread = new Thread(runnable);
            thread.setName(str);
            thread.start();
            return thread;
        } catch (Throwable th) {
            C4888x.m16984e("[Util] Failed to start a thread to execute task with message: %s", th.getMessage());
            return null;
        }
    }

    /* renamed from: a */
    public static boolean m17023a(Runnable runnable) {
        if (runnable == null) {
            return false;
        }
        C4886w a = C4886w.m16969a();
        if (a != null) {
            return a.mo26934a(runnable);
        }
        String[] split = runnable.getClass().getName().split("\\.");
        return m17016a(runnable, split[split.length - 1]) != null;
    }

    /* renamed from: a */
    public static SharedPreferences m17003a(String str, Context context) {
        if (context != null) {
            return context.getSharedPreferences(str, 0);
        }
        return null;
    }
}
