package com.tencent.bugly.crashreport.crash;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.C4792b;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.info.PlugInBean;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C4847ak;
import com.tencent.bugly.proguard.C4849am;
import com.tencent.bugly.proguard.C4850an;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4878r;
import com.tencent.bugly.proguard.C4880t;
import com.tencent.bugly.proguard.C4881u;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/* renamed from: com.tencent.bugly.crashreport.crash.b */
/* compiled from: BUGLY */
public final class C4822b {

    /* renamed from: a */
    private static int f10680a;

    /* renamed from: b */
    private Context f10681b;

    /* renamed from: c */
    private C4881u f10682c;

    /* renamed from: d */
    private C4875p f10683d;

    /* renamed from: e */
    private C4809a f10684e;

    /* renamed from: f */
    private C4874o f10685f;

    /* renamed from: g */
    private BuglyStrategy.C4791a f10686g;

    /* renamed from: com.tencent.bugly.crashreport.crash.b$a */
    /* compiled from: BUGLY */
    class C4823a implements C4880t {

        /* renamed from: a */
        private /* synthetic */ List f10687a;

        C4823a(List list) {
            this.f10687a = list;
        }

        /* renamed from: a */
        public final void mo26637a(boolean z) {
            C4822b.m16640a(z, this.f10687a);
        }
    }

    public C4822b(int i, Context context, C4881u uVar, C4875p pVar, C4809a aVar, BuglyStrategy.C4791a aVar2, C4874o oVar) {
        f10680a = i;
        this.f10681b = context;
        this.f10682c = uVar;
        this.f10683d = pVar;
        this.f10684e = aVar;
        this.f10686g = aVar2;
        this.f10685f = oVar;
    }

    /* renamed from: a */
    private static List<C4813a> m16638a(List<C4813a> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        for (C4813a aVar : list) {
            if (aVar.f10647d && aVar.f10645b <= currentTimeMillis - 86400000) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    /* renamed from: e */
    private static ContentValues m16646e(CrashDetailBean crashDetailBean) {
        if (crashDetailBean == null) {
            return null;
        }
        try {
            ContentValues contentValues = new ContentValues();
            if (crashDetailBean.f10618a > 0) {
                contentValues.put("_id", Long.valueOf(crashDetailBean.f10618a));
            }
            contentValues.put("_tm", Long.valueOf(crashDetailBean.f10635r));
            contentValues.put("_s1", crashDetailBean.f10638u);
            int i = 1;
            contentValues.put("_up", Integer.valueOf(crashDetailBean.f10621d ? 1 : 0));
            if (!crashDetailBean.f10627j) {
                i = 0;
            }
            contentValues.put("_me", Integer.valueOf(i));
            contentValues.put("_uc", Integer.valueOf(crashDetailBean.f10629l));
            contentValues.put("_dt", C4893z.m17027a(crashDetailBean));
            return contentValues;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: b */
    public final void mo26751b(CrashDetailBean crashDetailBean) {
        if (this.f10685f != null) {
            C4888x.m16982c("Calling 'onCrashHandleEnd' of RQD crash listener.", new Object[0]);
            int i = crashDetailBean.f10619b;
        }
    }

    /* renamed from: c */
    public final void mo26752c(CrashDetailBean crashDetailBean) {
        int i;
        Map<String, String> map;
        String str;
        HashMap hashMap;
        if (crashDetailBean != null) {
            if (this.f10686g != null || this.f10685f != null) {
                try {
                    C4888x.m16977a("[crash callback] start user's callback:onCrashHandleStart()", new Object[0]);
                    switch (crashDetailBean.f10619b) {
                        case 0:
                            i = 0;
                            break;
                        case 1:
                            i = 2;
                            break;
                        case 2:
                            i = 1;
                            break;
                        case 3:
                            i = 4;
                            break;
                        case 4:
                            i = 3;
                            break;
                        case 5:
                            i = 5;
                            break;
                        case 6:
                            i = 6;
                            break;
                        case 7:
                            i = 7;
                            break;
                        default:
                            return;
                    }
                    int i2 = crashDetailBean.f10619b;
                    String str2 = crashDetailBean.f10631n;
                    String str3 = crashDetailBean.f10633p;
                    String str4 = crashDetailBean.f10634q;
                    long j = crashDetailBean.f10635r;
                    byte[] bArr = null;
                    if (this.f10685f != null) {
                        C4888x.m16982c("Calling 'onCrashHandleStart' of RQD crash listener.", new Object[0]);
                        C4888x.m16982c("Calling 'getCrashExtraMessage' of RQD crash listener.", new Object[0]);
                        String b = this.f10685f.mo26894b();
                        if (b != null) {
                            hashMap = new HashMap(1);
                            hashMap.put("userData", b);
                        } else {
                            hashMap = null;
                        }
                        map = hashMap;
                    } else if (this.f10686g != null) {
                        C4888x.m16982c("Calling 'onCrashHandleStart' of Bugly crash listener.", new Object[0]);
                        map = this.f10686g.onCrashHandleStart(i, crashDetailBean.f10631n, crashDetailBean.f10632o, crashDetailBean.f10634q);
                    } else {
                        map = null;
                    }
                    if (map != null && map.size() > 0) {
                        crashDetailBean.f10609O = new LinkedHashMap(map.size());
                        for (Map.Entry entry : map.entrySet()) {
                            if (!C4893z.m17024a((String) entry.getKey())) {
                                String str5 = (String) entry.getKey();
                                if (str5.length() > 100) {
                                    str5 = str5.substring(0, 100);
                                    C4888x.m16983d("setted key length is over limit %d substring to %s", 100, str5);
                                }
                                if (C4893z.m17024a((String) entry.getValue()) || ((String) entry.getValue()).length() <= 30000) {
                                    str = ((String) entry.getValue());
                                } else {
                                    str = ((String) entry.getValue()).substring(((String) entry.getValue()).length() - BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH);
                                    C4888x.m16983d("setted %s value length is over limit %d substring", str5, Integer.valueOf((int) BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH));
                                }
                                crashDetailBean.f10609O.put(str5, str);
                                C4888x.m16977a("add setted key %s value size:%d", str5, Integer.valueOf(str.length()));
                            }
                        }
                    }
                    C4888x.m16977a("[crash callback] start user's callback:onCrashHandleStart2GetExtraDatas()", new Object[0]);
                    if (this.f10685f != null) {
                        C4888x.m16982c("Calling 'getCrashExtraData' of RQD crash listener.", new Object[0]);
                        bArr = this.f10685f.mo26893a();
                    } else if (this.f10686g != null) {
                        C4888x.m16982c("Calling 'onCrashHandleStart2GetExtraDatas' of Bugly crash listener.", new Object[0]);
                        bArr = this.f10686g.onCrashHandleStart2GetExtraDatas(i, crashDetailBean.f10631n, crashDetailBean.f10632o, crashDetailBean.f10634q);
                    }
                    crashDetailBean.f10614T = bArr;
                    if (bArr != null) {
                        if (bArr.length > 30000) {
                            C4888x.m16983d("extra bytes size %d is over limit %d will drop over part", Integer.valueOf(bArr.length), Integer.valueOf((int) BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH));
                            crashDetailBean.f10614T = Arrays.copyOf(bArr, (int) BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH);
                        }
                        C4888x.m16977a("add extra bytes %d ", Integer.valueOf(bArr.length));
                    }
                } catch (Throwable th) {
                    C4888x.m16983d("crash handle callback something wrong! %s", th.getClass().getName());
                    if (!C4888x.m16978a(th)) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o, boolean):long
     arg types: [java.lang.String, android.content.ContentValues, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o, boolean):long */
    /* renamed from: d */
    public final void mo26753d(CrashDetailBean crashDetailBean) {
        ContentValues e;
        if (crashDetailBean != null && (e = m16646e(crashDetailBean)) != null) {
            long a = C4875p.m16899a().mo26897a("t_cr", e, (C4874o) null, true);
            if (a >= 0) {
                C4888x.m16982c("insert %s success!", "t_cr");
                crashDetailBean.f10618a = a;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f9 A[Catch:{ all -> 0x0102 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00fe A[DONT_GENERATE] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean> m16643b(java.util.List<com.tencent.bugly.crashreport.crash.C4813a> r15) {
        /*
            r14 = this;
            r0 = 0
            if (r15 == 0) goto L_0x0109
            int r1 = r15.size()
            if (r1 != 0) goto L_0x000b
            goto L_0x0109
        L_0x000b:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "_id in "
            r1.append(r2)
            java.lang.String r3 = "("
            r1.append(r3)
            java.util.Iterator r15 = r15.iterator()
        L_0x001e:
            boolean r4 = r15.hasNext()
            java.lang.String r5 = ","
            if (r4 == 0) goto L_0x0035
            java.lang.Object r4 = r15.next()
            com.tencent.bugly.crashreport.crash.a r4 = (com.tencent.bugly.crashreport.crash.C4813a) r4
            long r6 = r4.f10644a
            r1.append(r6)
            r1.append(r5)
            goto L_0x001e
        L_0x0035:
            java.lang.String r15 = r1.toString()
            boolean r15 = r15.contains(r5)
            r4 = 0
            if (r15 == 0) goto L_0x004e
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            int r6 = r1.lastIndexOf(r5)
            java.lang.String r1 = r1.substring(r4, r6)
            r15.<init>(r1)
            goto L_0x004f
        L_0x004e:
            r15 = r1
        L_0x004f:
            java.lang.String r1 = ")"
            r15.append(r1)
            java.lang.String r9 = r15.toString()
            r15.setLength(r4)
            com.tencent.bugly.proguard.p r6 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00f1 }
            java.lang.String r7 = "t_cr"
            r8 = 0
            r10 = 0
            r11 = 0
            r12 = 1
            android.database.Cursor r6 = r6.mo26898a(r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x00f1 }
            if (r6 != 0) goto L_0x0071
            if (r6 == 0) goto L_0x0070
            r6.close()
        L_0x0070:
            return r0
        L_0x0071:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00ef }
            r7.<init>()     // Catch:{ all -> 0x00ef }
            r15.append(r2)     // Catch:{ all -> 0x00ef }
            r15.append(r3)     // Catch:{ all -> 0x00ef }
            r2 = 0
        L_0x007d:
            boolean r3 = r6.moveToNext()     // Catch:{ all -> 0x00ef }
            if (r3 == 0) goto L_0x00a8
            com.tencent.bugly.crashreport.crash.CrashDetailBean r3 = m16634a(r6)     // Catch:{ all -> 0x00ef }
            if (r3 == 0) goto L_0x008d
            r7.add(r3)     // Catch:{ all -> 0x00ef }
            goto L_0x007d
        L_0x008d:
            java.lang.String r3 = "_id"
            int r3 = r6.getColumnIndex(r3)     // Catch:{ all -> 0x00a0 }
            long r8 = r6.getLong(r3)     // Catch:{ all -> 0x00a0 }
            r15.append(r8)     // Catch:{ all -> 0x00a0 }
            r15.append(r5)     // Catch:{ all -> 0x00a0 }
            int r2 = r2 + 1
            goto L_0x007d
        L_0x00a0:
            java.lang.String r3 = "unknown id!"
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ef }
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r8)     // Catch:{ all -> 0x00ef }
            goto L_0x007d
        L_0x00a8:
            java.lang.String r3 = r15.toString()     // Catch:{ all -> 0x00ef }
            boolean r3 = r3.contains(r5)     // Catch:{ all -> 0x00ef }
            if (r3 == 0) goto L_0x00c0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ef }
            int r5 = r15.lastIndexOf(r5)     // Catch:{ all -> 0x00ef }
            java.lang.String r15 = r15.substring(r4, r5)     // Catch:{ all -> 0x00ef }
            r3.<init>(r15)     // Catch:{ all -> 0x00ef }
            r15 = r3
        L_0x00c0:
            r15.append(r1)     // Catch:{ all -> 0x00ef }
            java.lang.String r10 = r15.toString()     // Catch:{ all -> 0x00ef }
            if (r2 <= 0) goto L_0x00e9
            com.tencent.bugly.proguard.p r8 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00ef }
            java.lang.String r9 = "t_cr"
            r11 = 0
            r12 = 0
            r13 = 1
            int r15 = r8.mo26896a(r9, r10, r11, r12, r13)     // Catch:{ all -> 0x00ef }
            java.lang.String r1 = "deleted %s illegal data %d"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x00ef }
            java.lang.String r3 = "t_cr"
            r2[r4] = r3     // Catch:{ all -> 0x00ef }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x00ef }
            r3 = 1
            r2[r3] = r15     // Catch:{ all -> 0x00ef }
            com.tencent.bugly.proguard.C4888x.m16983d(r1, r2)     // Catch:{ all -> 0x00ef }
        L_0x00e9:
            if (r6 == 0) goto L_0x00ee
            r6.close()
        L_0x00ee:
            return r7
        L_0x00ef:
            r15 = move-exception
            goto L_0x00f3
        L_0x00f1:
            r15 = move-exception
            r6 = r0
        L_0x00f3:
            boolean r1 = com.tencent.bugly.proguard.C4888x.m16978a(r15)     // Catch:{ all -> 0x0102 }
            if (r1 != 0) goto L_0x00fc
            r15.printStackTrace()     // Catch:{ all -> 0x0102 }
        L_0x00fc:
            if (r6 == 0) goto L_0x0101
            r6.close()
        L_0x0101:
            return r0
        L_0x0102:
            r15 = move-exception
            if (r6 == 0) goto L_0x0108
            r6.close()
        L_0x0108:
            throw r15
        L_0x0109:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4822b.m16643b(java.util.List):java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* renamed from: d */
    private static void m16645d(List<CrashDetailBean> list) {
        if (list != null) {
            try {
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    for (CrashDetailBean crashDetailBean : list) {
                        sb.append(" or _id");
                        sb.append(" = ");
                        sb.append(crashDetailBean.f10618a);
                    }
                    String sb2 = sb.toString();
                    if (sb2.length() > 0) {
                        sb2 = sb2.substring(4);
                    }
                    sb.setLength(0);
                    C4888x.m16982c("deleted %s data %d", "t_cr", Integer.valueOf(C4875p.m16899a().mo26896a("t_cr", sb2, (String[]) null, (C4874o) null, true)));
                }
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    private CrashDetailBean m16635a(List<C4813a> list, CrashDetailBean crashDetailBean) {
        List<CrashDetailBean> b;
        String[] split;
        if (list == null || list.size() == 0) {
            return crashDetailBean;
        }
        CrashDetailBean crashDetailBean2 = null;
        ArrayList arrayList = new ArrayList(10);
        for (C4813a aVar : list) {
            if (aVar.f10648e) {
                arrayList.add(aVar);
            }
        }
        if (arrayList.size() > 0 && (b = m16643b(arrayList)) != null && b.size() > 0) {
            Collections.sort(b);
            CrashDetailBean crashDetailBean3 = null;
            for (int i = 0; i < b.size(); i++) {
                CrashDetailBean crashDetailBean4 = b.get(i);
                if (i == 0) {
                    crashDetailBean3 = crashDetailBean4;
                } else {
                    String str = crashDetailBean4.f10636s;
                    if (!(str == null || (split = str.split("\n")) == null)) {
                        for (String str2 : split) {
                            if (!crashDetailBean3.f10636s.contains(str2)) {
                                crashDetailBean3.f10637t++;
                                crashDetailBean3.f10636s += str2 + "\n";
                            }
                        }
                    }
                }
            }
            crashDetailBean2 = crashDetailBean3;
        }
        if (crashDetailBean2 == null) {
            crashDetailBean.f10627j = true;
            crashDetailBean.f10637t = 0;
            crashDetailBean.f10636s = "";
            crashDetailBean2 = crashDetailBean;
        }
        for (C4813a aVar2 : list) {
            if (!aVar2.f10648e && !aVar2.f10647d) {
                String str3 = crashDetailBean2.f10636s;
                StringBuilder sb = new StringBuilder();
                sb.append(aVar2.f10645b);
                if (!str3.contains(sb.toString())) {
                    crashDetailBean2.f10637t++;
                    crashDetailBean2.f10636s += aVar2.f10645b + "\n";
                }
            }
        }
        if (crashDetailBean2.f10635r != crashDetailBean.f10635r) {
            String str4 = crashDetailBean2.f10636s;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(crashDetailBean.f10635r);
            if (!str4.contains(sb2.toString())) {
                crashDetailBean2.f10637t++;
                crashDetailBean2.f10636s += crashDetailBean.f10635r + "\n";
            }
        }
        return crashDetailBean2;
    }

    /* renamed from: b */
    private static C4813a m16641b(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            C4813a aVar = new C4813a();
            aVar.f10644a = cursor.getLong(cursor.getColumnIndex("_id"));
            aVar.f10645b = cursor.getLong(cursor.getColumnIndex("_tm"));
            aVar.f10646c = cursor.getString(cursor.getColumnIndex("_s1"));
            boolean z = false;
            aVar.f10647d = cursor.getInt(cursor.getColumnIndex("_up")) == 1;
            if (cursor.getInt(cursor.getColumnIndex("_me")) == 1) {
                z = true;
            }
            aVar.f10648e = z;
            aVar.f10649f = cursor.getInt(cursor.getColumnIndex("_uc"));
            return aVar;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public final boolean mo26749a(CrashDetailBean crashDetailBean) {
        return mo26750a(crashDetailBean, -123456789);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* renamed from: a */
    public final boolean mo26750a(CrashDetailBean crashDetailBean, int i) {
        if (crashDetailBean == null) {
            return true;
        }
        String str = C4824c.f10702n;
        if (str != null && !str.isEmpty()) {
            C4888x.m16982c("Crash filter for crash stack is: %s", C4824c.f10702n);
            if (crashDetailBean.f10634q.contains(C4824c.f10702n)) {
                C4888x.m16983d("This crash contains the filter string set. It will not be record and upload.", new Object[0]);
                return true;
            }
        }
        String str2 = C4824c.f10703o;
        if (str2 != null && !str2.isEmpty()) {
            C4888x.m16982c("Crash regular filter for crash stack is: %s", C4824c.f10703o);
            if (Pattern.compile(C4824c.f10703o).matcher(crashDetailBean.f10634q).find()) {
                C4888x.m16983d("This crash matches the regular filter string set. It will not be record and upload.", new Object[0]);
                return true;
            }
        }
        int i2 = crashDetailBean.f10619b;
        String str3 = crashDetailBean.f10631n;
        String str4 = crashDetailBean.f10632o;
        String str5 = crashDetailBean.f10633p;
        String str6 = crashDetailBean.f10634q;
        long j = crashDetailBean.f10635r;
        String str7 = crashDetailBean.f10630m;
        String str8 = crashDetailBean.f10622e;
        String str9 = crashDetailBean.f10620c;
        String str10 = crashDetailBean.f10595A;
        String str11 = crashDetailBean.f10596B;
        if (this.f10685f != null) {
            C4888x.m16982c("Calling 'onCrashSaving' of RQD crash listener.", new Object[0]);
            if (!this.f10685f.mo26895c()) {
                C4888x.m16983d("Crash listener 'onCrashSaving' return 'false' thus will not handle this crash.", new Object[0]);
                return true;
            }
        }
        if (crashDetailBean.f10619b != 2) {
            C4878r rVar = new C4878r();
            rVar.f10982b = 1;
            rVar.f10983c = crashDetailBean.f10595A;
            rVar.f10984d = crashDetailBean.f10596B;
            rVar.f10985e = crashDetailBean.f10635r;
            this.f10683d.mo26905b(1);
            this.f10683d.mo26904a(rVar);
            C4888x.m16980b("[crash] a crash occur, handling...", new Object[0]);
        } else {
            C4888x.m16980b("[crash] a caught exception occur, handling...", new Object[0]);
        }
        List<C4813a> b = m16642b();
        ArrayList arrayList = null;
        if (b != null && b.size() > 0) {
            arrayList = new ArrayList(10);
            ArrayList<C4813a> arrayList2 = new ArrayList<>(10);
            arrayList.addAll(m16638a(b));
            b.removeAll(arrayList);
            if (((long) b.size()) > 20) {
                StringBuilder sb = new StringBuilder();
                sb.append("_id in ");
                sb.append("(");
                sb.append("SELECT _id");
                sb.append(" FROM t_cr");
                sb.append(" order by _id");
                sb.append(" limit 5");
                sb.append(")");
                String sb2 = sb.toString();
                sb.setLength(0);
                try {
                    C4888x.m16982c("deleted first record %s data %d", "t_cr", Integer.valueOf(C4875p.m16899a().mo26896a("t_cr", sb2, (String[]) null, (C4874o) null, true)));
                } catch (Throwable th) {
                    if (!C4888x.m16978a(th)) {
                        th.printStackTrace();
                    }
                }
            }
            if (!C4792b.f10414c && C4824c.f10692d) {
                boolean z = false;
                for (C4813a aVar : b) {
                    if (crashDetailBean.f10638u.equals(aVar.f10646c)) {
                        if (aVar.f10648e) {
                            z = true;
                        }
                        arrayList2.add(aVar);
                    }
                }
                if (z || arrayList2.size() >= C4824c.f10691c) {
                    C4888x.m16977a("same crash occur too much do merged!", new Object[0]);
                    CrashDetailBean a = m16635a(arrayList2, crashDetailBean);
                    for (C4813a aVar2 : arrayList2) {
                        if (aVar2.f10644a != a.f10618a) {
                            arrayList.add(aVar2);
                        }
                    }
                    mo26753d(a);
                    m16644c(arrayList);
                    C4888x.m16980b("[crash] save crash success. For this device crash many times, it will not upload crashes immediately", new Object[0]);
                    return true;
                }
            }
        }
        mo26753d(crashDetailBean);
        if (arrayList != null && !arrayList.isEmpty()) {
            m16644c(arrayList);
        }
        C4888x.m16980b("[crash] save crash success", new Object[0]);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c0 A[Catch:{ all -> 0x00c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c5 A[DONT_GENERATE] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.tencent.bugly.crashreport.crash.C4813a> m16642b() {
        /*
            r16 = this;
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            java.lang.String r3 = "_id"
            java.lang.String r4 = "_tm"
            java.lang.String r5 = "_s1"
            java.lang.String r6 = "_up"
            java.lang.String r7 = "_me"
            java.lang.String r8 = "_uc"
            java.lang.String[] r11 = new java.lang.String[]{r3, r4, r5, r6, r7, r8}     // Catch:{ all -> 0x00b8 }
            com.tencent.bugly.proguard.p r9 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00b8 }
            java.lang.String r10 = "t_cr"
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 1
            android.database.Cursor r3 = r9.mo26898a(r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x00b8 }
            if (r3 != 0) goto L_0x002c
            if (r3 == 0) goto L_0x002b
            r3.close()
        L_0x002b:
            return r2
        L_0x002c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b6 }
            r0.<init>()     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "_id in "
            r0.append(r2)     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "("
            r0.append(r2)     // Catch:{ all -> 0x00b6 }
            r2 = 0
            r4 = 0
        L_0x003d:
            boolean r5 = r3.moveToNext()     // Catch:{ all -> 0x00b6 }
            java.lang.String r6 = ","
            if (r5 == 0) goto L_0x006a
            com.tencent.bugly.crashreport.crash.a r5 = m16641b(r3)     // Catch:{ all -> 0x00b6 }
            if (r5 == 0) goto L_0x004f
            r1.add(r5)     // Catch:{ all -> 0x00b6 }
            goto L_0x003d
        L_0x004f:
            java.lang.String r5 = "_id"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ all -> 0x0062 }
            long r7 = r3.getLong(r5)     // Catch:{ all -> 0x0062 }
            r0.append(r7)     // Catch:{ all -> 0x0062 }
            r0.append(r6)     // Catch:{ all -> 0x0062 }
            int r4 = r4 + 1
            goto L_0x003d
        L_0x0062:
            java.lang.String r5 = "unknown id!"
            java.lang.Object[] r6 = new java.lang.Object[r2]     // Catch:{ all -> 0x00b6 }
            com.tencent.bugly.proguard.C4888x.m16983d(r5, r6)     // Catch:{ all -> 0x00b6 }
            goto L_0x003d
        L_0x006a:
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x00b6 }
            boolean r5 = r5.contains(r6)     // Catch:{ all -> 0x00b6 }
            if (r5 == 0) goto L_0x0082
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b6 }
            int r6 = r0.lastIndexOf(r6)     // Catch:{ all -> 0x00b6 }
            java.lang.String r0 = r0.substring(r2, r6)     // Catch:{ all -> 0x00b6 }
            r5.<init>(r0)     // Catch:{ all -> 0x00b6 }
            r0 = r5
        L_0x0082:
            java.lang.String r5 = ")"
            r0.append(r5)     // Catch:{ all -> 0x00b6 }
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x00b6 }
            r0.setLength(r2)     // Catch:{ all -> 0x00b6 }
            if (r4 <= 0) goto L_0x00b0
            com.tencent.bugly.proguard.p r6 = com.tencent.bugly.proguard.C4875p.m16899a()     // Catch:{ all -> 0x00b6 }
            java.lang.String r7 = "t_cr"
            r9 = 0
            r10 = 0
            r11 = 1
            int r0 = r6.mo26896a(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x00b6 }
            java.lang.String r4 = "deleted %s illegal data %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00b6 }
            java.lang.String r6 = "t_cr"
            r5[r2] = r6     // Catch:{ all -> 0x00b6 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00b6 }
            r2 = 1
            r5[r2] = r0     // Catch:{ all -> 0x00b6 }
            com.tencent.bugly.proguard.C4888x.m16983d(r4, r5)     // Catch:{ all -> 0x00b6 }
        L_0x00b0:
            if (r3 == 0) goto L_0x00b5
            r3.close()
        L_0x00b5:
            return r1
        L_0x00b6:
            r0 = move-exception
            goto L_0x00ba
        L_0x00b8:
            r0 = move-exception
            r3 = r2
        L_0x00ba:
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16978a(r0)     // Catch:{ all -> 0x00c9 }
            if (r2 != 0) goto L_0x00c3
            r0.printStackTrace()     // Catch:{ all -> 0x00c9 }
        L_0x00c3:
            if (r3 == 0) goto L_0x00c8
            r3.close()
        L_0x00c8:
            return r1
        L_0x00c9:
            r0 = move-exception
            if (r3 == 0) goto L_0x00cf
            r3.close()
        L_0x00cf:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4822b.m16642b():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int */
    /* renamed from: c */
    private static void m16644c(List<C4813a> list) {
        if (list != null && list.size() != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("_id in ");
            sb.append("(");
            for (C4813a aVar : list) {
                sb.append(aVar.f10644a);
                sb.append(",");
            }
            StringBuilder sb2 = new StringBuilder(sb.substring(0, sb.lastIndexOf(",")));
            sb2.append(")");
            String sb3 = sb2.toString();
            sb2.setLength(0);
            try {
                C4888x.m16982c("deleted %s data %d", "t_cr", Integer.valueOf(C4875p.m16899a().mo26896a("t_cr", sb3, (String[]) null, (C4874o) null, true)));
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    public final List<CrashDetailBean> mo26746a() {
        StrategyBean c = C4809a.m16589a().mo26720c();
        if (c == null) {
            C4888x.m16983d("have not synced remote!", new Object[0]);
            return null;
        } else if (!c.f10560g) {
            C4888x.m16983d("Crashreport remote closed, please check your APP ID correct and Version available, then uninstall and reinstall your app.", new Object[0]);
            C4888x.m16980b("[init] WARNING! Crashreport closed by server, please check your APP ID correct and Version available, then uninstall and reinstall your app.", new Object[0]);
            return null;
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            long b = C4893z.m17032b();
            List<C4813a> b2 = m16642b();
            C4888x.m16982c("Size of crash list loaded from DB: %s", Integer.valueOf(b2.size()));
            if (b2 == null || b2.size() <= 0) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(m16638a(b2));
            b2.removeAll(arrayList);
            Iterator<C4813a> it = b2.iterator();
            while (it.hasNext()) {
                C4813a next = it.next();
                long j = next.f10645b;
                if (j < b - C4824c.f10695g) {
                    it.remove();
                    arrayList.add(next);
                } else if (next.f10647d) {
                    if (j >= currentTimeMillis - 86400000) {
                        it.remove();
                    } else if (!next.f10648e) {
                        it.remove();
                        arrayList.add(next);
                    }
                } else if (((long) next.f10649f) >= 3 && j < currentTimeMillis - 86400000) {
                    it.remove();
                    arrayList.add(next);
                }
            }
            if (arrayList.size() > 0) {
                m16644c(arrayList);
            }
            ArrayList arrayList2 = new ArrayList();
            List<CrashDetailBean> b3 = m16643b(b2);
            if (b3 != null && b3.size() > 0) {
                String str = C4806a.m16492b().f10532j;
                Iterator<CrashDetailBean> it2 = b3.iterator();
                while (it2.hasNext()) {
                    CrashDetailBean next2 = it2.next();
                    if (!str.equals(next2.f10623f)) {
                        it2.remove();
                        arrayList2.add(next2);
                    }
                }
            }
            if (arrayList2.size() > 0) {
                m16645d(arrayList2);
            }
            return b3;
        }
    }

    /* renamed from: a */
    public final void mo26747a(CrashDetailBean crashDetailBean, long j, boolean z) {
        if (C4824c.f10700l) {
            C4888x.m16977a("try to upload right now", new Object[0]);
            ArrayList arrayList = new ArrayList();
            arrayList.add(crashDetailBean);
            mo26748a(arrayList, 3000, z, crashDetailBean.f10619b == 7, z);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a7 A[Catch:{ all -> 0x00e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00af A[Catch:{ all -> 0x00e9 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo26748a(java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean> r15, long r16, boolean r18, boolean r19, boolean r20) {
        /*
            r14 = this;
            r1 = r14
            r0 = r15
            android.content.Context r2 = r1.f10681b
            com.tencent.bugly.crashreport.common.info.a r2 = com.tencent.bugly.crashreport.common.info.C4806a.m16491a(r2)
            boolean r2 = r2.f10527e
            if (r2 != 0) goto L_0x000d
            return
        L_0x000d:
            com.tencent.bugly.proguard.u r2 = r1.f10682c
            if (r2 != 0) goto L_0x0012
            return
        L_0x0012:
            if (r20 != 0) goto L_0x001d
            int r3 = com.tencent.bugly.crashreport.crash.C4824c.f10689a
            boolean r2 = r2.mo26926b(r3)
            if (r2 != 0) goto L_0x001d
            return
        L_0x001d:
            com.tencent.bugly.crashreport.common.strategy.a r2 = r1.f10684e
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r2 = r2.mo26720c()
            boolean r3 = r2.f10560g
            r4 = 0
            if (r3 != 0) goto L_0x0037
            java.lang.Object[] r0 = new java.lang.Object[r4]
            java.lang.String r2 = "remote report is disable!"
            com.tencent.bugly.proguard.C4888x.m16983d(r2, r0)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            java.lang.String r2 = "[crash] server closed bugly in this app. please check your appid if is correct, and re-install it"
            com.tencent.bugly.proguard.C4888x.m16980b(r2, r0)
            return
        L_0x0037:
            if (r0 == 0) goto L_0x0101
            int r3 = r15.size()
            if (r3 != 0) goto L_0x0041
            goto L_0x0101
        L_0x0041:
            com.tencent.bugly.proguard.u r3 = r1.f10682c     // Catch:{ all -> 0x00e9 }
            boolean r3 = r3.f10992a     // Catch:{ all -> 0x00e9 }
            if (r3 == 0) goto L_0x004a
            java.lang.String r2 = r2.f10572s     // Catch:{ all -> 0x00e9 }
            goto L_0x004c
        L_0x004a:
            java.lang.String r2 = r2.f10573t     // Catch:{ all -> 0x00e9 }
        L_0x004c:
            r8 = r2
            com.tencent.bugly.proguard.u r2 = r1.f10682c     // Catch:{ all -> 0x00e9 }
            boolean r2 = r2.f10992a     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x0056
            java.lang.String r2 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f10556c     // Catch:{ all -> 0x00e9 }
            goto L_0x0058
        L_0x0056:
            java.lang.String r2 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f10554a     // Catch:{ all -> 0x00e9 }
        L_0x0058:
            r9 = r2
            com.tencent.bugly.proguard.u r2 = r1.f10682c     // Catch:{ all -> 0x00e9 }
            boolean r2 = r2.f10992a     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x0062
            r2 = 830(0x33e, float:1.163E-42)
            goto L_0x0064
        L_0x0062:
            r2 = 630(0x276, float:8.83E-43)
        L_0x0064:
            android.content.Context r3 = r1.f10681b     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.crashreport.common.info.a r5 = com.tencent.bugly.crashreport.common.info.C4806a.m16492b()     // Catch:{ all -> 0x00e9 }
            if (r3 == 0) goto L_0x009d
            if (r0 == 0) goto L_0x009d
            int r6 = r15.size()     // Catch:{ all -> 0x00e9 }
            if (r6 == 0) goto L_0x009d
            if (r5 != 0) goto L_0x0077
            goto L_0x009d
        L_0x0077:
            com.tencent.bugly.proguard.ao r6 = new com.tencent.bugly.proguard.ao     // Catch:{ all -> 0x00e9 }
            r6.<init>()     // Catch:{ all -> 0x00e9 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00e9 }
            r7.<init>()     // Catch:{ all -> 0x00e9 }
            r6.f10840a = r7     // Catch:{ all -> 0x00e9 }
            java.util.Iterator r7 = r15.iterator()     // Catch:{ all -> 0x00e9 }
        L_0x0087:
            boolean r10 = r7.hasNext()     // Catch:{ all -> 0x00e9 }
            if (r10 == 0) goto L_0x00a5
            java.lang.Object r10 = r7.next()     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.crashreport.crash.CrashDetailBean r10 = (com.tencent.bugly.crashreport.crash.CrashDetailBean) r10     // Catch:{ all -> 0x00e9 }
            java.util.ArrayList<com.tencent.bugly.proguard.an> r11 = r6.f10840a     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.an r10 = m16637a(r3, r10, r5)     // Catch:{ all -> 0x00e9 }
            r11.add(r10)     // Catch:{ all -> 0x00e9 }
            goto L_0x0087
        L_0x009d:
            java.lang.String r3 = "enEXPPkg args == null!"
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.C4888x.m16983d(r3, r5)     // Catch:{ all -> 0x00e9 }
            r6 = 0
        L_0x00a5:
            if (r6 != 0) goto L_0x00af
            java.lang.String r0 = "create eupPkg fail!"
            java.lang.Object[] r2 = new java.lang.Object[r4]     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r2)     // Catch:{ all -> 0x00e9 }
            return
        L_0x00af:
            byte[] r3 = com.tencent.bugly.proguard.C4836a.m16729a(r6)     // Catch:{ all -> 0x00e9 }
            if (r3 != 0) goto L_0x00bd
            java.lang.String r0 = "send encode fail!"
            java.lang.Object[] r2 = new java.lang.Object[r4]     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r2)     // Catch:{ all -> 0x00e9 }
            return
        L_0x00bd:
            android.content.Context r5 = r1.f10681b     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.ap r7 = com.tencent.bugly.proguard.C4836a.m16720a(r5, r2, r3)     // Catch:{ all -> 0x00e9 }
            if (r7 != 0) goto L_0x00cd
            java.lang.String r0 = "request package is null."
            java.lang.Object[] r2 = new java.lang.Object[r4]     // Catch:{ all -> 0x00e9 }
            com.tencent.bugly.proguard.C4888x.m16983d(r0, r2)     // Catch:{ all -> 0x00e9 }
            return
        L_0x00cd:
            com.tencent.bugly.crashreport.crash.b$a r10 = new com.tencent.bugly.crashreport.crash.b$a     // Catch:{ all -> 0x00e9 }
            r10.<init>(r15)     // Catch:{ all -> 0x00e9 }
            if (r18 == 0) goto L_0x00e0
            com.tencent.bugly.proguard.u r5 = r1.f10682c     // Catch:{ all -> 0x00e9 }
            int r6 = com.tencent.bugly.crashreport.crash.C4822b.f10680a     // Catch:{ all -> 0x00e9 }
            r11 = r16
            r13 = r19
            r5.mo26918a(r6, r7, r8, r9, r10, r11, r13)     // Catch:{ all -> 0x00e9 }
            goto L_0x0101
        L_0x00e0:
            com.tencent.bugly.proguard.u r5 = r1.f10682c     // Catch:{ all -> 0x00e9 }
            int r6 = com.tencent.bugly.crashreport.crash.C4822b.f10680a     // Catch:{ all -> 0x00e9 }
            r11 = 0
            r5.mo26919a(r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x00e9 }
            return
        L_0x00e9:
            r0 = move-exception
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = r0.toString()
            r2[r4] = r3
            java.lang.String r3 = "req cr error %s"
            com.tencent.bugly.proguard.C4888x.m16984e(r3, r2)
            boolean r2 = com.tencent.bugly.proguard.C4888x.m16981b(r0)
            if (r2 != 0) goto L_0x0101
            r0.printStackTrace()
        L_0x0101:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4822b.mo26748a(java.util.List, long, boolean, boolean, boolean):void");
    }

    /* renamed from: a */
    public static void m16640a(boolean z, List<CrashDetailBean> list) {
        if (list != null && list.size() > 0) {
            C4888x.m16982c("up finish update state %b", Boolean.valueOf(z));
            for (CrashDetailBean crashDetailBean : list) {
                C4888x.m16982c("pre uid:%s uc:%d re:%b me:%b", crashDetailBean.f10620c, Integer.valueOf(crashDetailBean.f10629l), Boolean.valueOf(crashDetailBean.f10621d), Boolean.valueOf(crashDetailBean.f10627j));
                crashDetailBean.f10629l++;
                crashDetailBean.f10621d = z;
                C4888x.m16982c("set uid:%s uc:%d re:%b me:%b", crashDetailBean.f10620c, Integer.valueOf(crashDetailBean.f10629l), Boolean.valueOf(crashDetailBean.f10621d), Boolean.valueOf(crashDetailBean.f10627j));
            }
            for (CrashDetailBean crashDetailBean2 : list) {
                C4824c.m16656a().mo26756a(crashDetailBean2);
            }
            C4888x.m16982c("update state size %d", Integer.valueOf(list.size()));
        }
        if (!z) {
            C4888x.m16980b("[crash] upload fail.", new Object[0]);
        }
    }

    /* renamed from: a */
    private static CrashDetailBean m16634a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            byte[] blob = cursor.getBlob(cursor.getColumnIndex("_dt"));
            if (blob == null) {
                return null;
            }
            long j = cursor.getLong(cursor.getColumnIndex("_id"));
            CrashDetailBean crashDetailBean = (CrashDetailBean) C4893z.m17007a(blob, CrashDetailBean.CREATOR);
            if (crashDetailBean != null) {
                crashDetailBean.f10618a = j;
            }
            return crashDetailBean;
        } catch (Throwable th) {
            if (!C4888x.m16978a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    private static C4850an m16637a(Context context, CrashDetailBean crashDetailBean, C4806a aVar) {
        C4849am a;
        C4849am a2;
        C4849am amVar;
        boolean z = false;
        if (context == null || crashDetailBean == null || aVar == null) {
            C4888x.m16983d("enExp args == null", new Object[0]);
            return null;
        }
        C4850an anVar = new C4850an();
        int i = crashDetailBean.f10619b;
        switch (i) {
            case 0:
                anVar.f10818a = crashDetailBean.f10627j ? "200" : "100";
                break;
            case 1:
                anVar.f10818a = crashDetailBean.f10627j ? "201" : "101";
                break;
            case 2:
                anVar.f10818a = crashDetailBean.f10627j ? "202" : "102";
                break;
            case 3:
                anVar.f10818a = crashDetailBean.f10627j ? "203" : "103";
                break;
            case 4:
                anVar.f10818a = crashDetailBean.f10627j ? "204" : "104";
                break;
            case 5:
                anVar.f10818a = crashDetailBean.f10627j ? "207" : "107";
                break;
            case 6:
                anVar.f10818a = crashDetailBean.f10627j ? "206" : "106";
                break;
            case 7:
                anVar.f10818a = crashDetailBean.f10627j ? "208" : "108";
                break;
            default:
                C4888x.m16984e("crash type error! %d", Integer.valueOf(i));
                break;
        }
        anVar.f10819b = crashDetailBean.f10635r;
        anVar.f10820c = crashDetailBean.f10631n;
        anVar.f10821d = crashDetailBean.f10632o;
        anVar.f10822e = crashDetailBean.f10633p;
        anVar.f10824g = crashDetailBean.f10634q;
        anVar.f10825h = crashDetailBean.f10643z;
        anVar.f10826i = crashDetailBean.f10620c;
        anVar.f10827j = null;
        anVar.f10829l = crashDetailBean.f10630m;
        anVar.f10830m = crashDetailBean.f10622e;
        anVar.f10823f = crashDetailBean.f10596B;
        anVar.f10837t = C4806a.m16492b().mo26694i();
        anVar.f10831n = null;
        Map<String, PlugInBean> map = crashDetailBean.f10626i;
        if (map != null && map.size() > 0) {
            anVar.f10832o = new ArrayList<>();
            for (Map.Entry entry : crashDetailBean.f10626i.entrySet()) {
                C4847ak akVar = new C4847ak();
                akVar.f10798a = ((PlugInBean) entry.getValue()).f10468a;
                akVar.f10800c = ((PlugInBean) entry.getValue()).f10470c;
                akVar.f10801d = ((PlugInBean) entry.getValue()).f10469b;
                akVar.f10799b = aVar.mo26703r();
                anVar.f10832o.add(akVar);
            }
        }
        Map<String, PlugInBean> map2 = crashDetailBean.f10625h;
        if (map2 != null && map2.size() > 0) {
            anVar.f10833p = new ArrayList<>();
            for (Map.Entry entry2 : crashDetailBean.f10625h.entrySet()) {
                C4847ak akVar2 = new C4847ak();
                akVar2.f10798a = ((PlugInBean) entry2.getValue()).f10468a;
                akVar2.f10800c = ((PlugInBean) entry2.getValue()).f10470c;
                akVar2.f10801d = ((PlugInBean) entry2.getValue()).f10469b;
                anVar.f10833p.add(akVar2);
            }
        }
        if (crashDetailBean.f10627j) {
            anVar.f10828k = crashDetailBean.f10637t;
            String str = crashDetailBean.f10636s;
            if (str != null && str.length() > 0) {
                if (anVar.f10834q == null) {
                    anVar.f10834q = new ArrayList<>();
                }
                try {
                    anVar.f10834q.add(new C4849am((byte) 1, "alltimes.txt", crashDetailBean.f10636s.getBytes("utf-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    anVar.f10834q = null;
                }
            }
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(anVar.f10828k);
            ArrayList<C4849am> arrayList = anVar.f10834q;
            objArr[1] = Integer.valueOf(arrayList != null ? arrayList.size() : 0);
            C4888x.m16982c("crashcount:%d sz:%d", objArr);
        }
        if (crashDetailBean.f10640w != null) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            try {
                anVar.f10834q.add(new C4849am((byte) 1, "log.txt", crashDetailBean.f10640w.getBytes("utf-8")));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                anVar.f10834q = null;
            }
        }
        if (crashDetailBean.f10641x != null) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            try {
                anVar.f10834q.add(new C4849am((byte) 1, "jniLog.txt", crashDetailBean.f10641x.getBytes("utf-8")));
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
                anVar.f10834q = null;
            }
        }
        if (!C4893z.m17024a(crashDetailBean.f10615U)) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            try {
                amVar = new C4849am((byte) 1, "crashInfos.txt", crashDetailBean.f10615U.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
                amVar = null;
            }
            if (amVar != null) {
                C4888x.m16982c("attach crash infos", new Object[0]);
                anVar.f10834q.add(amVar);
            }
        }
        if (crashDetailBean.f10616V != null) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            C4849am a3 = m16636a("backupRecord.zip", context, crashDetailBean.f10616V);
            if (a3 != null) {
                C4888x.m16982c("attach backup record", new Object[0]);
                anVar.f10834q.add(a3);
            }
        }
        byte[] bArr = crashDetailBean.f10642y;
        if (bArr != null && bArr.length > 0) {
            C4849am amVar2 = new C4849am((byte) 2, "buglylog.zip", bArr);
            C4888x.m16982c("attach user log", new Object[0]);
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            anVar.f10834q.add(amVar2);
        }
        if (crashDetailBean.f10619b == 3) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            C4888x.m16982c("crashBean.userDatas:%s", crashDetailBean.f10609O);
            Map<String, String> map3 = crashDetailBean.f10609O;
            if (map3 != null && map3.containsKey("BUGLY_CR_01")) {
                try {
                    if (!TextUtils.isEmpty(crashDetailBean.f10609O.get("BUGLY_CR_01"))) {
                        anVar.f10834q.add(new C4849am((byte) 1, "anrMessage.txt", crashDetailBean.f10609O.get("BUGLY_CR_01").getBytes("utf-8")));
                        C4888x.m16982c("attach anr message", new Object[0]);
                    }
                } catch (UnsupportedEncodingException e5) {
                    e5.printStackTrace();
                    anVar.f10834q = null;
                }
                crashDetailBean.f10609O.remove("BUGLY_CR_01");
            }
            String str2 = crashDetailBean.f10639v;
            if (!(str2 == null || (a2 = m16636a("trace.zip", context, str2)) == null)) {
                C4888x.m16982c("attach traces", new Object[0]);
                anVar.f10834q.add(a2);
            }
        }
        if (crashDetailBean.f10619b == 1) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            String str3 = crashDetailBean.f10639v;
            if (!(str3 == null || (a = m16636a("tomb.zip", context, str3)) == null)) {
                C4888x.m16982c("attach tombs", new Object[0]);
                anVar.f10834q.add(a);
            }
        }
        List<String> list = aVar.f10474C;
        if (list != null && !list.isEmpty()) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            StringBuilder sb = new StringBuilder();
            for (String str4 : aVar.f10474C) {
                sb.append(str4);
            }
            try {
                anVar.f10834q.add(new C4849am((byte) 1, "martianlog.txt", sb.toString().getBytes("utf-8")));
                C4888x.m16982c("attach pageTracingList", new Object[0]);
            } catch (UnsupportedEncodingException e6) {
                e6.printStackTrace();
            }
        }
        byte[] bArr2 = crashDetailBean.f10614T;
        if (bArr2 != null && bArr2.length > 0) {
            if (anVar.f10834q == null) {
                anVar.f10834q = new ArrayList<>();
            }
            anVar.f10834q.add(new C4849am((byte) 1, "userExtraByteData", crashDetailBean.f10614T));
            C4888x.m16982c("attach extraData", new Object[0]);
        }
        anVar.f10835r = new HashMap();
        Map<String, String> map4 = anVar.f10835r;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(crashDetailBean.f10597C);
        map4.put("A9", sb2.toString());
        Map<String, String> map5 = anVar.f10835r;
        StringBuilder sb3 = new StringBuilder();
        sb3.append(crashDetailBean.f10598D);
        map5.put("A11", sb3.toString());
        Map<String, String> map6 = anVar.f10835r;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(crashDetailBean.f10599E);
        map6.put("A10", sb4.toString());
        anVar.f10835r.put("A23", crashDetailBean.f10623f);
        anVar.f10835r.put("A7", aVar.f10528f);
        anVar.f10835r.put("A6", aVar.mo26704s());
        anVar.f10835r.put("A5", aVar.mo26703r());
        anVar.f10835r.put("A22", aVar.mo26693h());
        Map<String, String> map7 = anVar.f10835r;
        StringBuilder sb5 = new StringBuilder();
        sb5.append(crashDetailBean.f10601G);
        map7.put("A2", sb5.toString());
        Map<String, String> map8 = anVar.f10835r;
        StringBuilder sb6 = new StringBuilder();
        sb6.append(crashDetailBean.f10600F);
        map8.put("A1", sb6.toString());
        anVar.f10835r.put("A24", aVar.f10530h);
        Map<String, String> map9 = anVar.f10835r;
        StringBuilder sb7 = new StringBuilder();
        sb7.append(crashDetailBean.f10602H);
        map9.put("A17", sb7.toString());
        anVar.f10835r.put("A3", aVar.mo26696k());
        anVar.f10835r.put("A16", aVar.mo26698m());
        anVar.f10835r.put("A25", aVar.mo26699n());
        anVar.f10835r.put("A14", aVar.mo26697l());
        anVar.f10835r.put("A15", aVar.mo26708w());
        Map<String, String> map10 = anVar.f10835r;
        StringBuilder sb8 = new StringBuilder();
        sb8.append(aVar.mo26709x());
        map10.put("A13", sb8.toString());
        anVar.f10835r.put("A34", crashDetailBean.f10595A);
        if (aVar.f10546x != null) {
            anVar.f10835r.put("productIdentify", aVar.f10546x);
        }
        try {
            anVar.f10835r.put("A26", URLEncoder.encode(crashDetailBean.f10603I, "utf-8"));
        } catch (UnsupportedEncodingException e7) {
            e7.printStackTrace();
        }
        if (crashDetailBean.f10619b == 1) {
            anVar.f10835r.put("A27", crashDetailBean.f10605K);
            anVar.f10835r.put("A28", crashDetailBean.f10604J);
            Map<String, String> map11 = anVar.f10835r;
            StringBuilder sb9 = new StringBuilder();
            sb9.append(crashDetailBean.f10628k);
            map11.put("A29", sb9.toString());
        }
        anVar.f10835r.put("A30", crashDetailBean.f10606L);
        Map<String, String> map12 = anVar.f10835r;
        StringBuilder sb10 = new StringBuilder();
        sb10.append(crashDetailBean.f10607M);
        map12.put("A18", sb10.toString());
        Map<String, String> map13 = anVar.f10835r;
        StringBuilder sb11 = new StringBuilder();
        sb11.append(!crashDetailBean.f10608N);
        map13.put("A36", sb11.toString());
        Map<String, String> map14 = anVar.f10835r;
        StringBuilder sb12 = new StringBuilder();
        sb12.append(aVar.f10539q);
        map14.put("F02", sb12.toString());
        Map<String, String> map15 = anVar.f10835r;
        StringBuilder sb13 = new StringBuilder();
        sb13.append(aVar.f10540r);
        map15.put("F03", sb13.toString());
        anVar.f10835r.put("F04", aVar.mo26687e());
        Map<String, String> map16 = anVar.f10835r;
        StringBuilder sb14 = new StringBuilder();
        sb14.append(aVar.f10541s);
        map16.put("F05", sb14.toString());
        anVar.f10835r.put("F06", aVar.f10538p);
        anVar.f10835r.put("F08", aVar.f10544v);
        anVar.f10835r.put("F09", aVar.f10545w);
        Map<String, String> map17 = anVar.f10835r;
        StringBuilder sb15 = new StringBuilder();
        sb15.append(aVar.f10542t);
        map17.put("F10", sb15.toString());
        if (crashDetailBean.f10610P >= 0) {
            Map<String, String> map18 = anVar.f10835r;
            StringBuilder sb16 = new StringBuilder();
            sb16.append(crashDetailBean.f10610P);
            map18.put("C01", sb16.toString());
        }
        if (crashDetailBean.f10611Q >= 0) {
            Map<String, String> map19 = anVar.f10835r;
            StringBuilder sb17 = new StringBuilder();
            sb17.append(crashDetailBean.f10611Q);
            map19.put("C02", sb17.toString());
        }
        Map<String, String> map20 = crashDetailBean.f10612R;
        if (map20 != null && map20.size() > 0) {
            for (Map.Entry entry3 : crashDetailBean.f10612R.entrySet()) {
                anVar.f10835r.put("C03_" + ((String) entry3.getKey()), entry3.getValue());
            }
        }
        Map<String, String> map21 = crashDetailBean.f10613S;
        if (map21 != null && map21.size() > 0) {
            for (Map.Entry entry4 : crashDetailBean.f10613S.entrySet()) {
                anVar.f10835r.put("C04_" + ((String) entry4.getKey()), entry4.getValue());
            }
        }
        anVar.f10836s = null;
        Map<String, String> map22 = crashDetailBean.f10609O;
        if (map22 != null && map22.size() > 0) {
            anVar.f10836s = crashDetailBean.f10609O;
            C4888x.m16977a("setted message size %d", Integer.valueOf(anVar.f10836s.size()));
        }
        Object[] objArr2 = new Object[12];
        objArr2[0] = crashDetailBean.f10631n;
        objArr2[1] = crashDetailBean.f10620c;
        objArr2[2] = aVar.mo26687e();
        objArr2[3] = Long.valueOf((crashDetailBean.f10635r - crashDetailBean.f10607M) / 1000);
        objArr2[4] = Boolean.valueOf(crashDetailBean.f10628k);
        objArr2[5] = Boolean.valueOf(crashDetailBean.f10608N);
        objArr2[6] = Boolean.valueOf(crashDetailBean.f10627j);
        if (crashDetailBean.f10619b == 1) {
            z = true;
        }
        objArr2[7] = Boolean.valueOf(z);
        objArr2[8] = Integer.valueOf(crashDetailBean.f10637t);
        objArr2[9] = crashDetailBean.f10636s;
        objArr2[10] = Boolean.valueOf(crashDetailBean.f10621d);
        objArr2[11] = Integer.valueOf(anVar.f10835r.size());
        C4888x.m16982c("%s rid:%s sess:%s ls:%ds isR:%b isF:%b isM:%b isN:%b mc:%d ,%s ,isUp:%b ,vm:%d", objArr2);
        return anVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0090 A[Catch:{ all -> 0x00b2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0095 A[SYNTHETIC, Splitter:B:35:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a9 A[DONT_GENERATE] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tencent.bugly.proguard.C4849am m16636a(java.lang.String r6, android.content.Context r7, java.lang.String r8) {
        /*
            java.lang.String r0 = "del tmp"
            r1 = 0
            r2 = 0
            if (r8 == 0) goto L_0x00d2
            if (r7 != 0) goto L_0x000a
            goto L_0x00d2
        L_0x000a:
            r3 = 1
            java.lang.Object[] r4 = new java.lang.Object[r3]
            r4[r2] = r8
            java.lang.String r5 = "zip %s"
            com.tencent.bugly.proguard.C4888x.m16982c(r5, r4)
            java.io.File r4 = new java.io.File
            r4.<init>(r8)
            java.io.File r8 = new java.io.File
            java.io.File r7 = r7.getCacheDir()
            r8.<init>(r7, r6)
            r6 = 5000(0x1388, float:7.006E-42)
            boolean r6 = com.tencent.bugly.proguard.C4893z.m17022a(r4, r8, r6)
            if (r6 != 0) goto L_0x0032
            java.lang.Object[] r6 = new java.lang.Object[r2]
            java.lang.String r7 = "zip fail!"
            com.tencent.bugly.proguard.C4888x.m16983d(r7, r6)
            return r1
        L_0x0032:
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream
            r6.<init>()
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ all -> 0x0088 }
            r7.<init>(r8)     // Catch:{ all -> 0x0088 }
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]     // Catch:{ all -> 0x0086 }
        L_0x0040:
            int r5 = r7.read(r4)     // Catch:{ all -> 0x0086 }
            if (r5 <= 0) goto L_0x004d
            r6.write(r4, r2, r5)     // Catch:{ all -> 0x0086 }
            r6.flush()     // Catch:{ all -> 0x0086 }
            goto L_0x0040
        L_0x004d:
            byte[] r6 = r6.toByteArray()     // Catch:{ all -> 0x0086 }
            java.lang.String r4 = "read bytes :%d"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0086 }
            int r5 = r6.length     // Catch:{ all -> 0x0086 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0086 }
            r3[r2] = r5     // Catch:{ all -> 0x0086 }
            com.tencent.bugly.proguard.C4888x.m16982c(r4, r3)     // Catch:{ all -> 0x0086 }
            com.tencent.bugly.proguard.am r3 = new com.tencent.bugly.proguard.am     // Catch:{ all -> 0x0086 }
            r4 = 2
            java.lang.String r5 = r8.getName()     // Catch:{ all -> 0x0086 }
            r3.<init>(r4, r5, r6)     // Catch:{ all -> 0x0086 }
            r7.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0077
        L_0x006d:
            r6 = move-exception
            boolean r7 = com.tencent.bugly.proguard.C4888x.m16978a(r6)
            if (r7 != 0) goto L_0x0077
            r6.printStackTrace()
        L_0x0077:
            boolean r6 = r8.exists()
            if (r6 == 0) goto L_0x0085
            java.lang.Object[] r6 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r6)
            r8.delete()
        L_0x0085:
            return r3
        L_0x0086:
            r6 = move-exception
            goto L_0x008a
        L_0x0088:
            r6 = move-exception
            r7 = r1
        L_0x008a:
            boolean r3 = com.tencent.bugly.proguard.C4888x.m16978a(r6)     // Catch:{ all -> 0x00b2 }
            if (r3 != 0) goto L_0x0093
            r6.printStackTrace()     // Catch:{ all -> 0x00b2 }
        L_0x0093:
            if (r7 == 0) goto L_0x00a3
            r7.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x00a3
        L_0x0099:
            r6 = move-exception
            boolean r7 = com.tencent.bugly.proguard.C4888x.m16978a(r6)
            if (r7 != 0) goto L_0x00a3
            r6.printStackTrace()
        L_0x00a3:
            boolean r6 = r8.exists()
            if (r6 == 0) goto L_0x00b1
            java.lang.Object[] r6 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r6)
            r8.delete()
        L_0x00b1:
            return r1
        L_0x00b2:
            r6 = move-exception
            if (r7 == 0) goto L_0x00c3
            r7.close()     // Catch:{ IOException -> 0x00b9 }
            goto L_0x00c3
        L_0x00b9:
            r7 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C4888x.m16978a(r7)
            if (r1 != 0) goto L_0x00c3
            r7.printStackTrace()
        L_0x00c3:
            boolean r7 = r8.exists()
            if (r7 == 0) goto L_0x00d1
            java.lang.Object[] r7 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C4888x.m16982c(r0, r7)
            r8.delete()
        L_0x00d1:
            throw r6
        L_0x00d2:
            java.lang.Object[] r6 = new java.lang.Object[r2]
            java.lang.String r7 = "rqdp{  createZipAttachment sourcePath == null || context == null ,pls check}"
            com.tencent.bugly.proguard.C4888x.m16983d(r7, r6)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C4822b.m16636a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.am");
    }

    /* renamed from: a */
    public static void m16639a(String str, String str2, String str3, String str4, String str5, CrashDetailBean crashDetailBean) {
        String str6;
        C4806a b = C4806a.m16492b();
        if (b != null) {
            C4888x.m16984e("#++++++++++Record By Bugly++++++++++#", new Object[0]);
            C4888x.m16984e("# You can use Bugly(http:\\\\bugly.qq.com) to get more Crash Detail!", new Object[0]);
            C4888x.m16984e("# PKG NAME: %s", b.f10525c);
            C4888x.m16984e("# APP VER: %s", b.f10532j);
            C4888x.m16984e("# LAUNCH TIME: %s", C4893z.m17014a(new Date(C4806a.m16492b().f10498a)));
            C4888x.m16984e("# CRASH TYPE: %s", str);
            C4888x.m16984e("# CRASH TIME: %s", str2);
            C4888x.m16984e("# CRASH PROCESS: %s", str3);
            C4888x.m16984e("# CRASH THREAD: %s", str4);
            if (crashDetailBean != null) {
                C4888x.m16984e("# REPORT ID: %s", crashDetailBean.f10620c);
                Object[] objArr = new Object[2];
                objArr[0] = b.f10529g;
                objArr[1] = b.mo26709x().booleanValue() ? "ROOTED" : "UNROOT";
                C4888x.m16984e("# CRASH DEVICE: %s %s", objArr);
                C4888x.m16984e("# RUNTIME AVAIL RAM:%d ROM:%d SD:%d", Long.valueOf(crashDetailBean.f10597C), Long.valueOf(crashDetailBean.f10598D), Long.valueOf(crashDetailBean.f10599E));
                C4888x.m16984e("# RUNTIME TOTAL RAM:%d ROM:%d SD:%d", Long.valueOf(crashDetailBean.f10600F), Long.valueOf(crashDetailBean.f10601G), Long.valueOf(crashDetailBean.f10602H));
                if (!C4893z.m17024a(crashDetailBean.f10605K)) {
                    C4888x.m16984e("# EXCEPTION FIRED BY %s %s", crashDetailBean.f10605K, crashDetailBean.f10604J);
                } else if (crashDetailBean.f10619b == 3) {
                    Object[] objArr2 = new Object[1];
                    if (crashDetailBean.f10609O == null) {
                        str6 = "null";
                    } else {
                        str6 = crashDetailBean.f10609O.get("BUGLY_CR_01");
                    }
                    objArr2[0] = str6;
                    C4888x.m16984e("# EXCEPTION ANR MESSAGE:\n %s", objArr2);
                }
            }
            if (!C4893z.m17024a(str5)) {
                C4888x.m16984e("# CRASH STACK: ", new Object[0]);
                C4888x.m16984e(str5, new Object[0]);
            }
            C4888x.m16984e("#++++++++++++++++++++++++++++++++++++++++++#", new Object[0]);
        }
    }
}
