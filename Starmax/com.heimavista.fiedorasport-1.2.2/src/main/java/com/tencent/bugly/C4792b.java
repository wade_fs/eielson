package com.tencent.bugly;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.bugly.crashreport.biz.C4802b;
import com.tencent.bugly.crashreport.common.info.C4806a;
import com.tencent.bugly.crashreport.common.strategy.C4809a;
import com.tencent.bugly.proguard.C4871n;
import com.tencent.bugly.proguard.C4874o;
import com.tencent.bugly.proguard.C4875p;
import com.tencent.bugly.proguard.C4881u;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4889y;
import com.tencent.bugly.proguard.C4893z;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.b */
/* compiled from: BUGLY */
public final class C4792b {

    /* renamed from: a */
    public static boolean f10412a = true;

    /* renamed from: b */
    public static List<BUGLY> f10413b = new ArrayList();

    /* renamed from: c */
    public static boolean f10414c;

    /* renamed from: d */
    private static C4875p f10415d;

    /* renamed from: e */
    private static boolean f10416e;

    /* renamed from: a */
    private static boolean m16437a(C4806a aVar) {
        List<String> list = aVar.f10537o;
        aVar.getClass();
        return list != null && list.contains("bugly");
    }

    /* renamed from: a */
    public static synchronized void m16433a(Context context) {
        synchronized (C4792b.class) {
            m16434a(context, null);
        }
    }

    /* renamed from: a */
    public static synchronized void m16434a(Context context, BuglyStrategy buglyStrategy) {
        synchronized (C4792b.class) {
            if (f10416e) {
                C4888x.m16983d("[init] initial Multi-times, ignore this.", new Object[0]);
            } else if (context == null) {
                Log.w(C4888x.f11043a, "[init] context of init() is null, check it.");
            } else {
                C4806a a = C4806a.m16491a(context);
                if (m16437a(a)) {
                    f10412a = false;
                    return;
                }
                String f = a.mo26689f();
                if (f == null) {
                    Log.e(C4888x.f11043a, "[init] meta data of BUGLY_APPID in AndroidManifest.xml should be set.");
                } else {
                    m16435a(context, f, a.f10543u, buglyStrategy);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o):int
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, java.lang.String, byte[], com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.o, boolean):int
      com.tencent.bugly.proguard.p.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.o, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.p.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.o):long
      com.tencent.bugly.proguard.p.a(com.tencent.bugly.proguard.p, int, com.tencent.bugly.proguard.o):java.util.Map
      com.tencent.bugly.proguard.p.a(int, java.lang.String, com.tencent.bugly.proguard.o):boolean
      com.tencent.bugly.proguard.p.a(int, com.tencent.bugly.proguard.o, boolean):java.util.Map<java.lang.String, byte[]> */
    /* renamed from: a */
    public static synchronized void m16435a(Context context, String str, boolean z, BuglyStrategy buglyStrategy) {
        byte[] bArr;
        String str2 = str;
        boolean z2 = z;
        BuglyStrategy buglyStrategy2 = buglyStrategy;
        synchronized (C4792b.class) {
            if (f10416e) {
                C4888x.m16983d("[init] initial Multi-times, ignore this.", new Object[0]);
            } else if (context == null) {
                Log.w(C4888x.f11043a, "[init] context is null, check it.");
            } else if (str2 == null) {
                Log.e(C4888x.f11043a, "init arg 'crashReportAppID' should not be null!");
            } else {
                f10416e = true;
                if (z2) {
                    f10414c = true;
                    C4888x.f11044b = true;
                    C4888x.m16983d("Bugly debug模式开启，请在发布时把isDebug关闭。 -- Running in debug model for 'isDebug' is enabled. Please disable it when you release.", new Object[0]);
                    C4888x.m16984e("--------------------------------------------------------------------------------------------", new Object[0]);
                    C4888x.m16983d("Bugly debug模式将有以下行为特性 -- The following list shows the behaviour of debug model: ", new Object[0]);
                    C4888x.m16983d("[1] 输出详细的Bugly SDK的Log -- More detailed log of Bugly SDK will be output to logcat;", new Object[0]);
                    C4888x.m16983d("[2] 每一条Crash都会被立即上报 -- Every crash caught by Bugly will be uploaded immediately.", new Object[0]);
                    C4888x.m16983d("[3] 自定义日志将会在Logcat中输出 -- Custom log will be output to logcat.", new Object[0]);
                    C4888x.m16984e("--------------------------------------------------------------------------------------------", new Object[0]);
                    C4888x.m16980b("[init] Open debug mode of Bugly.", new Object[0]);
                }
                C4888x.m16977a("[init] Bugly version: v%s", "3.1.0");
                C4888x.m16977a(" crash report start initializing...", new Object[0]);
                C4888x.m16980b("[init] Bugly start initializing...", new Object[0]);
                C4888x.m16977a("[init] Bugly complete version: v%s", "3.1.0");
                Context a = C4893z.m17002a(context);
                C4806a a2 = C4806a.m16491a(a);
                a2.mo26705t();
                C4889y.m16986a(a);
                f10415d = C4875p.m16900a(a, f10413b);
                C4881u.m16933a(a);
                C4809a a3 = C4809a.m16590a(a, f10413b);
                C4871n a4 = C4871n.m16878a(a);
                if (m16437a(a2)) {
                    f10412a = false;
                    return;
                }
                a2.mo26675a(str2);
                C4888x.m16977a("[param] Set APP ID:%s", str2);
                if (buglyStrategy2 != null) {
                    String appVersion = buglyStrategy.getAppVersion();
                    if (!TextUtils.isEmpty(appVersion)) {
                        if (appVersion.length() > 100) {
                            String substring = appVersion.substring(0, 100);
                            C4888x.m16983d("appVersion %s length is over limit %d substring to %s", appVersion, 100, substring);
                            appVersion = substring;
                        }
                        a2.f10532j = appVersion;
                        C4888x.m16977a("[param] Set App version: %s", buglyStrategy.getAppVersion());
                    }
                    try {
                        if (buglyStrategy.isReplaceOldChannel()) {
                            String appChannel = buglyStrategy.getAppChannel();
                            if (!TextUtils.isEmpty(appChannel)) {
                                if (appChannel.length() > 100) {
                                    String substring2 = appChannel.substring(0, 100);
                                    C4888x.m16983d("appChannel %s length is over limit %d substring to %s", appChannel, 100, substring2);
                                    appChannel = substring2;
                                }
                                f10415d.mo26903a(556, "app_channel", appChannel.getBytes(), (C4874o) null, false);
                                a2.f10534l = appChannel;
                            }
                        } else {
                            Map<String, byte[]> a5 = f10415d.mo26900a(556, (C4874o) null, true);
                            if (!(a5 == null || (bArr = a5.get("app_channel")) == null)) {
                                a2.f10534l = new String(bArr);
                            }
                        }
                        C4888x.m16977a("[param] Set App channel: %s", a2.f10534l);
                    } catch (Exception e) {
                        if (f10414c) {
                            e.printStackTrace();
                        }
                    }
                    String appPackageName = buglyStrategy.getAppPackageName();
                    if (!TextUtils.isEmpty(appPackageName)) {
                        if (appPackageName.length() > 100) {
                            String substring3 = appPackageName.substring(0, 100);
                            C4888x.m16983d("appPackageName %s length is over limit %d substring to %s", appPackageName, 100, substring3);
                            appPackageName = substring3;
                        }
                        a2.f10525c = appPackageName;
                        C4888x.m16977a("[param] Set App package: %s", buglyStrategy.getAppPackageName());
                    }
                    String deviceID = buglyStrategy.getDeviceID();
                    if (deviceID != null) {
                        if (deviceID.length() > 100) {
                            String substring4 = deviceID.substring(0, 100);
                            C4888x.m16983d("deviceId %s length is over limit %d substring to %s", deviceID, 100, substring4);
                            deviceID = substring4;
                        }
                        a2.mo26683c(deviceID);
                        C4888x.m16977a("[param] Set device ID: %s", deviceID);
                    }
                    a2.f10527e = buglyStrategy.isUploadProcess();
                    C4889y.f11046a = buglyStrategy.isBuglyLogUpload();
                }
                C4802b.m16460a(a, buglyStrategy2);
                for (int i = 0; i < f10413b.size(); i++) {
                    try {
                        if (a4.mo26890a(f10413b.get(i).f10411id)) {
                            f10413b.get(i).init(a, z2, buglyStrategy2);
                        }
                    } catch (Throwable th) {
                        if (!C4888x.m16978a(th)) {
                            th.printStackTrace();
                        }
                    }
                }
                a3.mo26716a(buglyStrategy2 != null ? buglyStrategy.getAppReportDelay() : 0);
                C4888x.m16980b("[init] Bugly initialization finished.", new Object[0]);
            }
        }
    }

    /* renamed from: a */
    public static synchronized void m16436a(BUGLY aVar) {
        synchronized (C4792b.class) {
            if (!f10413b.contains(aVar)) {
                f10413b.add(aVar);
            }
        }
    }
}
