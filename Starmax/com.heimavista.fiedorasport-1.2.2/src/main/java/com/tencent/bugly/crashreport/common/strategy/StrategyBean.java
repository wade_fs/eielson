package com.tencent.bugly.crashreport.common.strategy;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.proguard.C4893z;
import java.util.Map;

/* compiled from: BUGLY */
public class StrategyBean implements Parcelable {
    public static final Parcelable.Creator<StrategyBean> CREATOR = new C4808a();

    /* renamed from: a */
    public static String f10554a = "http://rqd.uu.qq.com/rqd/sync";

    /* renamed from: b */
    public static String f10555b = "http://android.bugly.qq.com/rqd/async";

    /* renamed from: c */
    public static String f10556c = "http://android.bugly.qq.com/rqd/async";

    /* renamed from: d */
    public static String f10557d;

    /* renamed from: e */
    public long f10558e;

    /* renamed from: f */
    public long f10559f;

    /* renamed from: g */
    public boolean f10560g;

    /* renamed from: h */
    public boolean f10561h;

    /* renamed from: i */
    public boolean f10562i;

    /* renamed from: j */
    public boolean f10563j;

    /* renamed from: k */
    public boolean f10564k;

    /* renamed from: l */
    public boolean f10565l;

    /* renamed from: m */
    public boolean f10566m;

    /* renamed from: n */
    public boolean f10567n;

    /* renamed from: o */
    public boolean f10568o;

    /* renamed from: p */
    public long f10569p;

    /* renamed from: q */
    public long f10570q;

    /* renamed from: r */
    public String f10571r;

    /* renamed from: s */
    public String f10572s;

    /* renamed from: t */
    public String f10573t;

    /* renamed from: u */
    public String f10574u;

    /* renamed from: v */
    public Map<String, String> f10575v;

    /* renamed from: w */
    public int f10576w;

    /* renamed from: x */
    public long f10577x;

    /* renamed from: y */
    public long f10578y;

    /* renamed from: com.tencent.bugly.crashreport.common.strategy.StrategyBean$a */
    /* compiled from: BUGLY */
    static class C4808a implements Parcelable.Creator<StrategyBean> {
        C4808a() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new StrategyBean(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new StrategyBean[i];
        }
    }

    public StrategyBean() {
        this.f10558e = -1;
        this.f10559f = -1;
        this.f10560g = true;
        this.f10561h = true;
        this.f10562i = true;
        this.f10563j = true;
        this.f10564k = false;
        this.f10565l = true;
        this.f10566m = true;
        this.f10567n = true;
        this.f10568o = true;
        this.f10570q = 30000;
        this.f10571r = f10555b;
        this.f10572s = f10556c;
        this.f10573t = f10554a;
        this.f10576w = 10;
        this.f10577x = 300000;
        this.f10578y = -1;
        this.f10559f = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        sb.append("S(@L@L");
        sb.append("@)");
        f10557d = sb.toString();
        sb.setLength(0);
        sb.append("*^@K#K");
        sb.append("@!");
        this.f10574u = sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f10559f);
        parcel.writeByte(this.f10560g ? (byte) 1 : 0);
        parcel.writeByte(this.f10561h ? (byte) 1 : 0);
        parcel.writeByte(this.f10562i ? (byte) 1 : 0);
        parcel.writeString(this.f10571r);
        parcel.writeString(this.f10572s);
        parcel.writeString(this.f10574u);
        C4893z.m17038b(parcel, this.f10575v);
        parcel.writeByte(this.f10563j ? (byte) 1 : 0);
        parcel.writeByte(this.f10564k ? (byte) 1 : 0);
        parcel.writeByte(this.f10567n ? (byte) 1 : 0);
        parcel.writeByte(this.f10568o ? (byte) 1 : 0);
        parcel.writeLong(this.f10570q);
        parcel.writeByte(this.f10565l ? (byte) 1 : 0);
        parcel.writeByte(this.f10566m ? (byte) 1 : 0);
        parcel.writeLong(this.f10569p);
        parcel.writeInt(this.f10576w);
        parcel.writeLong(this.f10577x);
        parcel.writeLong(this.f10578y);
    }

    public StrategyBean(Parcel parcel) {
        this.f10558e = -1;
        this.f10559f = -1;
        boolean z = true;
        this.f10560g = true;
        this.f10561h = true;
        this.f10562i = true;
        this.f10563j = true;
        this.f10564k = false;
        this.f10565l = true;
        this.f10566m = true;
        this.f10567n = true;
        this.f10568o = true;
        this.f10570q = 30000;
        this.f10571r = f10555b;
        this.f10572s = f10556c;
        this.f10573t = f10554a;
        this.f10576w = 10;
        this.f10577x = 300000;
        this.f10578y = -1;
        try {
            f10557d = "S(@L@L" + "@)";
            this.f10559f = parcel.readLong();
            this.f10560g = parcel.readByte() == 1;
            this.f10561h = parcel.readByte() == 1;
            this.f10562i = parcel.readByte() == 1;
            this.f10571r = parcel.readString();
            this.f10572s = parcel.readString();
            this.f10574u = parcel.readString();
            this.f10575v = C4893z.m17036b(parcel);
            this.f10563j = parcel.readByte() == 1;
            this.f10564k = parcel.readByte() == 1;
            this.f10567n = parcel.readByte() == 1;
            this.f10568o = parcel.readByte() == 1;
            this.f10570q = parcel.readLong();
            this.f10565l = parcel.readByte() == 1;
            if (parcel.readByte() != 1) {
                z = false;
            }
            this.f10566m = z;
            this.f10569p = parcel.readLong();
            this.f10576w = parcel.readInt();
            this.f10577x = parcel.readLong();
            this.f10578y = parcel.readLong();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
