package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.ak */
/* compiled from: BUGLY */
public final class C4847ak extends C4868k implements Cloneable {

    /* renamed from: a */
    public String f10798a = "";

    /* renamed from: b */
    public String f10799b = "";

    /* renamed from: c */
    public String f10800c = "";

    /* renamed from: d */
    public String f10801d = "";

    /* renamed from: e */
    private String f10802e = "";

    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26880a(this.f10798a, 0);
        String str = this.f10799b;
        if (str != null) {
            jVar.mo26880a(str, 1);
        }
        String str2 = this.f10800c;
        if (str2 != null) {
            jVar.mo26880a(str2, 2);
        }
        String str3 = this.f10802e;
        if (str3 != null) {
            jVar.mo26880a(str3, 3);
        }
        String str4 = this.f10801d;
        if (str4 != null) {
            jVar.mo26880a(str4, 4);
        }
    }

    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
    }

    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10798a = iVar.mo26871b(0, true);
        this.f10799b = iVar.mo26871b(1, false);
        this.f10800c = iVar.mo26871b(2, false);
        this.f10802e = iVar.mo26871b(3, false);
        this.f10801d = iVar.mo26871b(4, false);
    }
}
