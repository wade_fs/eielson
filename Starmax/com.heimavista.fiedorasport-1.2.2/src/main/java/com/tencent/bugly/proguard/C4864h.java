package com.tencent.bugly.proguard;

import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.h */
/* compiled from: BUGLY */
public final class C4864h {

    /* renamed from: a */
    private StringBuilder f10930a;

    /* renamed from: b */
    private int f10931b = 0;

    public C4864h(StringBuilder sb, int i) {
        this.f10930a = sb;
        this.f10931b = i;
    }

    /* renamed from: a */
    private void m16816a(String str) {
        for (int i = 0; i < this.f10931b; i++) {
            this.f10930a.append(9);
        }
        if (str != null) {
            StringBuilder sb = this.f10930a;
            sb.append(str);
            sb.append(": ");
        }
    }

    /* renamed from: a */
    public final C4864h mo26859a(boolean z, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append(z ? 'T' : 'F');
        sb.append(10);
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26852a(byte b, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append((int) b);
        sb.append(10);
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26858a(short s, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append((int) s);
        sb.append(10);
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26853a(int i, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append(i);
        sb.append(10);
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26854a(long j, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append(j);
        sb.append(10);
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26856a(String str, String str2) {
        m16816a(str2);
        if (str == null) {
            this.f10930a.append("null\n");
        } else {
            StringBuilder sb = this.f10930a;
            sb.append(str);
            sb.append(10);
        }
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26860a(byte[] bArr, String str) {
        m16816a(str);
        if (bArr == null) {
            this.f10930a.append("null\n");
            return this;
        } else if (bArr.length == 0) {
            StringBuilder sb = this.f10930a;
            sb.append(bArr.length);
            sb.append(", []\n");
            return this;
        } else {
            StringBuilder sb2 = this.f10930a;
            sb2.append(bArr.length);
            sb2.append(", [\n");
            C4864h hVar = new C4864h(this.f10930a, this.f10931b + 1);
            for (byte b : bArr) {
                hVar.m16816a(null);
                StringBuilder sb3 = hVar.f10930a;
                sb3.append((int) b);
                sb3.append(10);
            }
            m16816a(null);
            StringBuilder sb4 = this.f10930a;
            sb4.append(']');
            sb4.append(10);
            return this;
        }
    }

    /* renamed from: a */
    public final <K, V> C4864h mo26857a(Map map, String str) {
        m16816a(str);
        if (map == null) {
            this.f10930a.append("null\n");
            return this;
        } else if (map.isEmpty()) {
            StringBuilder sb = this.f10930a;
            sb.append(map.size());
            sb.append(", {}\n");
            return this;
        } else {
            StringBuilder sb2 = this.f10930a;
            sb2.append(map.size());
            sb2.append(", {\n");
            C4864h hVar = new C4864h(this.f10930a, this.f10931b + 1);
            C4864h hVar2 = new C4864h(this.f10930a, this.f10931b + 2);
            for (Map.Entry entry : map.entrySet()) {
                hVar.m16816a(null);
                StringBuilder sb3 = hVar.f10930a;
                sb3.append('(');
                sb3.append(10);
                hVar2.m16814a(entry.getKey(), (String) null);
                hVar2.m16814a(entry.getValue(), (String) null);
                hVar.m16816a(null);
                StringBuilder sb4 = hVar.f10930a;
                sb4.append(')');
                sb4.append(10);
            }
            m16816a(null);
            StringBuilder sb5 = this.f10930a;
            sb5.append('}');
            sb5.append(10);
            return this;
        }
    }

    /* renamed from: a */
    private <T> C4864h m16815a(Object[] objArr, String str) {
        m16816a(str);
        if (objArr == null) {
            this.f10930a.append("null\n");
            return this;
        } else if (objArr.length == 0) {
            StringBuilder sb = this.f10930a;
            sb.append(objArr.length);
            sb.append(", []\n");
            return this;
        } else {
            StringBuilder sb2 = this.f10930a;
            sb2.append(objArr.length);
            sb2.append(", [\n");
            C4864h hVar = new C4864h(this.f10930a, this.f10931b + 1);
            for (Object obj : objArr) {
                hVar.m16814a(obj, (String) null);
            }
            m16816a(null);
            StringBuilder sb3 = this.f10930a;
            sb3.append(']');
            sb3.append(10);
            return this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.h.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.h
     arg types: [boolean[], java.lang.String]
     candidates:
      com.tencent.bugly.proguard.h.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(int, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(long, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(com.tencent.bugly.proguard.k, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(short, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(boolean, java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(byte[], java.lang.String):com.tencent.bugly.proguard.h
      com.tencent.bugly.proguard.h.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.h */
    /* renamed from: a */
    private <T> C4864h m16814a(Object obj, String str) {
        if (obj == null) {
            this.f10930a.append("null\n");
        } else if (obj instanceof Byte) {
            byte byteValue = ((Byte) obj).byteValue();
            m16816a(str);
            StringBuilder sb = this.f10930a;
            sb.append((int) byteValue);
            sb.append(10);
        } else if (obj instanceof Boolean) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            m16816a(str);
            StringBuilder sb2 = this.f10930a;
            sb2.append(booleanValue ? 'T' : 'F');
            sb2.append(10);
        } else if (obj instanceof Short) {
            short shortValue = ((Short) obj).shortValue();
            m16816a(str);
            StringBuilder sb3 = this.f10930a;
            sb3.append((int) shortValue);
            sb3.append(10);
        } else if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            m16816a(str);
            StringBuilder sb4 = this.f10930a;
            sb4.append(intValue);
            sb4.append(10);
        } else if (obj instanceof Long) {
            long longValue = ((Long) obj).longValue();
            m16816a(str);
            StringBuilder sb5 = this.f10930a;
            sb5.append(longValue);
            sb5.append(10);
        } else if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            m16816a(str);
            StringBuilder sb6 = this.f10930a;
            sb6.append(floatValue);
            sb6.append(10);
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            m16816a(str);
            StringBuilder sb7 = this.f10930a;
            sb7.append(doubleValue);
            sb7.append(10);
        } else if (obj instanceof String) {
            mo26856a((String) obj, str);
        } else if (obj instanceof Map) {
            mo26857a((Map) obj, str);
        } else if (obj instanceof List) {
            List list = (List) obj;
            if (list == null) {
                m16816a(str);
                this.f10930a.append("null\t");
            } else {
                m16815a(list.toArray(), str);
            }
        } else if (obj instanceof C4868k) {
            mo26855a((C4868k) obj, str);
        } else if (obj instanceof byte[]) {
            mo26860a((byte[]) obj, str);
        } else if (obj instanceof boolean[]) {
            m16814a((Object) ((boolean[]) obj), str);
        } else {
            int i = 0;
            if (obj instanceof short[]) {
                short[] sArr = (short[]) obj;
                m16816a(str);
                if (sArr == null) {
                    this.f10930a.append("null\n");
                } else if (sArr.length == 0) {
                    StringBuilder sb8 = this.f10930a;
                    sb8.append(sArr.length);
                    sb8.append(", []\n");
                } else {
                    StringBuilder sb9 = this.f10930a;
                    sb9.append(sArr.length);
                    sb9.append(", [\n");
                    C4864h hVar = new C4864h(this.f10930a, this.f10931b + 1);
                    int length = sArr.length;
                    while (i < length) {
                        short s = sArr[i];
                        hVar.m16816a(null);
                        StringBuilder sb10 = hVar.f10930a;
                        sb10.append((int) s);
                        sb10.append(10);
                        i++;
                    }
                    m16816a(null);
                    StringBuilder sb11 = this.f10930a;
                    sb11.append(']');
                    sb11.append(10);
                }
            } else if (obj instanceof int[]) {
                int[] iArr = (int[]) obj;
                m16816a(str);
                if (iArr == null) {
                    this.f10930a.append("null\n");
                } else if (iArr.length == 0) {
                    StringBuilder sb12 = this.f10930a;
                    sb12.append(iArr.length);
                    sb12.append(", []\n");
                } else {
                    StringBuilder sb13 = this.f10930a;
                    sb13.append(iArr.length);
                    sb13.append(", [\n");
                    C4864h hVar2 = new C4864h(this.f10930a, this.f10931b + 1);
                    int length2 = iArr.length;
                    while (i < length2) {
                        int i2 = iArr[i];
                        hVar2.m16816a(null);
                        StringBuilder sb14 = hVar2.f10930a;
                        sb14.append(i2);
                        sb14.append(10);
                        i++;
                    }
                    m16816a(null);
                    StringBuilder sb15 = this.f10930a;
                    sb15.append(']');
                    sb15.append(10);
                }
            } else if (obj instanceof long[]) {
                long[] jArr = (long[]) obj;
                m16816a(str);
                if (jArr == null) {
                    this.f10930a.append("null\n");
                } else if (jArr.length == 0) {
                    StringBuilder sb16 = this.f10930a;
                    sb16.append(jArr.length);
                    sb16.append(", []\n");
                } else {
                    StringBuilder sb17 = this.f10930a;
                    sb17.append(jArr.length);
                    sb17.append(", [\n");
                    C4864h hVar3 = new C4864h(this.f10930a, this.f10931b + 1);
                    int length3 = jArr.length;
                    while (i < length3) {
                        long j = jArr[i];
                        hVar3.m16816a(null);
                        StringBuilder sb18 = hVar3.f10930a;
                        sb18.append(j);
                        sb18.append(10);
                        i++;
                    }
                    m16816a(null);
                    StringBuilder sb19 = this.f10930a;
                    sb19.append(']');
                    sb19.append(10);
                }
            } else if (obj instanceof float[]) {
                float[] fArr = (float[]) obj;
                m16816a(str);
                if (fArr == null) {
                    this.f10930a.append("null\n");
                } else if (fArr.length == 0) {
                    StringBuilder sb20 = this.f10930a;
                    sb20.append(fArr.length);
                    sb20.append(", []\n");
                } else {
                    StringBuilder sb21 = this.f10930a;
                    sb21.append(fArr.length);
                    sb21.append(", [\n");
                    C4864h hVar4 = new C4864h(this.f10930a, this.f10931b + 1);
                    int length4 = fArr.length;
                    while (i < length4) {
                        float f = fArr[i];
                        hVar4.m16816a(null);
                        StringBuilder sb22 = hVar4.f10930a;
                        sb22.append(f);
                        sb22.append(10);
                        i++;
                    }
                    m16816a(null);
                    StringBuilder sb23 = this.f10930a;
                    sb23.append(']');
                    sb23.append(10);
                }
            } else if (obj instanceof double[]) {
                double[] dArr = (double[]) obj;
                m16816a(str);
                if (dArr == null) {
                    this.f10930a.append("null\n");
                } else if (dArr.length == 0) {
                    StringBuilder sb24 = this.f10930a;
                    sb24.append(dArr.length);
                    sb24.append(", []\n");
                } else {
                    StringBuilder sb25 = this.f10930a;
                    sb25.append(dArr.length);
                    sb25.append(", [\n");
                    C4864h hVar5 = new C4864h(this.f10930a, this.f10931b + 1);
                    int length5 = dArr.length;
                    while (i < length5) {
                        double d = dArr[i];
                        hVar5.m16816a(null);
                        StringBuilder sb26 = hVar5.f10930a;
                        sb26.append(d);
                        sb26.append(10);
                        i++;
                    }
                    m16816a(null);
                    StringBuilder sb27 = this.f10930a;
                    sb27.append(']');
                    sb27.append(10);
                }
            } else if (obj.getClass().isArray()) {
                m16815a((Object[]) obj, str);
            } else {
                throw new C4858b("write object error: unsupport type.");
            }
        }
        return this;
    }

    /* renamed from: a */
    public final C4864h mo26855a(C4868k kVar, String str) {
        m16816a(str);
        StringBuilder sb = this.f10930a;
        sb.append('{');
        sb.append(10);
        if (kVar == null) {
            StringBuilder sb2 = this.f10930a;
            sb2.append(9);
            sb2.append("null");
        } else {
            kVar.mo26841a(this.f10930a, this.f10931b + 1);
        }
        m16816a(null);
        StringBuilder sb3 = this.f10930a;
        sb3.append('}');
        sb3.append(10);
        return this;
    }
}
