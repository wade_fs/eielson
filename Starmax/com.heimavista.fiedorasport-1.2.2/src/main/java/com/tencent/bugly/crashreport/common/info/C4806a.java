package com.tencent.bugly.crashreport.common.info;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Process;
import com.facebook.internal.ServerProtocol;
import com.tencent.bugly.C4792b;
import com.tencent.bugly.crashreport.C4794a;
import com.tencent.bugly.proguard.C4888x;
import com.tencent.bugly.proguard.C4893z;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* renamed from: com.tencent.bugly.crashreport.common.info.a */
/* compiled from: BUGLY */
public final class C4806a {

    /* renamed from: af */
    private static C4806a f10471af;

    /* renamed from: A */
    public HashMap<String, String> f10472A = new HashMap<>();

    /* renamed from: B */
    public boolean f10473B = true;

    /* renamed from: C */
    public List<String> f10474C = new ArrayList();

    /* renamed from: D */
    public C4794a f10475D = null;

    /* renamed from: E */
    public SharedPreferences f10476E;

    /* renamed from: F */
    private final Context f10477F;

    /* renamed from: G */
    private String f10478G;

    /* renamed from: H */
    private String f10479H;

    /* renamed from: I */
    private String f10480I;

    /* renamed from: J */
    private String f10481J = "unknown";

    /* renamed from: K */
    private String f10482K = "unknown";

    /* renamed from: L */
    private String f10483L = "";

    /* renamed from: M */
    private String f10484M = null;

    /* renamed from: N */
    private String f10485N = null;

    /* renamed from: O */
    private String f10486O = null;

    /* renamed from: P */
    private String f10487P = null;

    /* renamed from: Q */
    private long f10488Q = -1;

    /* renamed from: R */
    private long f10489R = -1;

    /* renamed from: S */
    private long f10490S = -1;

    /* renamed from: T */
    private String f10491T = null;

    /* renamed from: U */
    private String f10492U = null;

    /* renamed from: V */
    private Map<String, PlugInBean> f10493V = null;

    /* renamed from: W */
    private boolean f10494W = true;

    /* renamed from: X */
    private String f10495X = null;

    /* renamed from: Y */
    private String f10496Y = null;

    /* renamed from: Z */
    private Boolean f10497Z = null;

    /* renamed from: a */
    public final long f10498a = System.currentTimeMillis();

    /* renamed from: aa */
    private String f10499aa = null;

    /* renamed from: ab */
    private String f10500ab = null;

    /* renamed from: ac */
    private String f10501ac = null;

    /* renamed from: ad */
    private Map<String, PlugInBean> f10502ad = null;

    /* renamed from: ae */
    private Map<String, PlugInBean> f10503ae = null;

    /* renamed from: ag */
    private int f10504ag = -1;

    /* renamed from: ah */
    private int f10505ah = -1;

    /* renamed from: ai */
    private Map<String, String> f10506ai = new HashMap();

    /* renamed from: aj */
    private Map<String, String> f10507aj = new HashMap();

    /* renamed from: ak */
    private Map<String, String> f10508ak = new HashMap();

    /* renamed from: al */
    private boolean f10509al = true;

    /* renamed from: am */
    private Boolean f10510am = null;

    /* renamed from: an */
    private Boolean f10511an = null;

    /* renamed from: ao */
    private String f10512ao = null;

    /* renamed from: ap */
    private String f10513ap = null;

    /* renamed from: aq */
    private String f10514aq = null;

    /* renamed from: ar */
    private String f10515ar = null;

    /* renamed from: as */
    private String f10516as = null;

    /* renamed from: at */
    private final Object f10517at = new Object();

    /* renamed from: au */
    private final Object f10518au = new Object();

    /* renamed from: av */
    private final Object f10519av = new Object();

    /* renamed from: aw */
    private final Object f10520aw = new Object();

    /* renamed from: ax */
    private final Object f10521ax = new Object();

    /* renamed from: ay */
    private final Object f10522ay = new Object();

    /* renamed from: az */
    private final Object f10523az = new Object();

    /* renamed from: b */
    public final byte f10524b;

    /* renamed from: c */
    public String f10525c;

    /* renamed from: d */
    public final String f10526d;

    /* renamed from: e */
    public boolean f10527e = true;

    /* renamed from: f */
    public final String f10528f;

    /* renamed from: g */
    public final String f10529g;

    /* renamed from: h */
    public final String f10530h;

    /* renamed from: i */
    public long f10531i;

    /* renamed from: j */
    public String f10532j = null;

    /* renamed from: k */
    public String f10533k = null;

    /* renamed from: l */
    public String f10534l = null;

    /* renamed from: m */
    public String f10535m = null;

    /* renamed from: n */
    public String f10536n = null;

    /* renamed from: o */
    public List<String> f10537o = null;

    /* renamed from: p */
    public String f10538p = "unknown";

    /* renamed from: q */
    public long f10539q = 0;

    /* renamed from: r */
    public long f10540r = 0;

    /* renamed from: s */
    public long f10541s = 0;

    /* renamed from: t */
    public long f10542t = 0;

    /* renamed from: u */
    public boolean f10543u = false;

    /* renamed from: v */
    public String f10544v = null;

    /* renamed from: w */
    public String f10545w = null;

    /* renamed from: x */
    public String f10546x = null;

    /* renamed from: y */
    public boolean f10547y = false;

    /* renamed from: z */
    public boolean f10548z = false;

    private C4806a(Context context) {
        this.f10477F = C4893z.m17002a(context);
        this.f10524b = 1;
        PackageInfo b = AppInfo.m16482b(context);
        if (b != null) {
            try {
                this.f10532j = b.versionName;
                this.f10544v = this.f10532j;
                this.f10545w = Integer.toString(b.versionCode);
            } catch (Throwable th) {
                if (!C4888x.m16978a(th)) {
                    th.printStackTrace();
                }
            }
        }
        this.f10525c = AppInfo.m16478a(context);
        this.f10526d = AppInfo.m16477a(Process.myPid());
        this.f10528f = C4807b.m16578o();
        this.f10529g = C4807b.m16551a();
        this.f10533k = AppInfo.m16483c(context);
        this.f10530h = "Android " + C4807b.m16554b() + ",level " + C4807b.m16556c();
        this.f10529g + ";" + this.f10530h;
        Map<String, String> d = AppInfo.m16484d(context);
        if (d != null) {
            try {
                this.f10537o = AppInfo.m16480a(d);
                String str = d.get("BUGLY_APPID");
                if (str != null) {
                    this.f10496Y = str;
                    mo26684c("APP_ID", this.f10496Y);
                }
                String str2 = d.get("BUGLY_APP_VERSION");
                if (str2 != null) {
                    this.f10532j = str2;
                }
                String str3 = d.get("BUGLY_APP_CHANNEL");
                if (str3 != null) {
                    this.f10534l = str3;
                }
                String str4 = d.get("BUGLY_ENABLE_DEBUG");
                if (str4 != null) {
                    this.f10543u = str4.equalsIgnoreCase(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                }
                String str5 = d.get("com.tencent.rdm.uuid");
                if (str5 != null) {
                    this.f10546x = str5;
                }
            } catch (Throwable th2) {
                if (!C4888x.m16978a(th2)) {
                    th2.printStackTrace();
                }
            }
        }
        try {
            if (!context.getDatabasePath("bugly_db_").exists()) {
                this.f10548z = true;
                C4888x.m16982c("App is first time to be installed on the device.", new Object[0]);
            }
        } catch (Throwable th3) {
            if (C4792b.f10414c) {
                th3.printStackTrace();
            }
        }
        this.f10476E = C4893z.m17003a("BUGLY_COMMON_VALUES", context);
        C4888x.m16982c("com info create end", new Object[0]);
    }

    /* renamed from: K */
    public static int m16490K() {
        return C4807b.m16556c();
    }

    /* renamed from: b */
    public static synchronized C4806a m16492b() {
        C4806a aVar;
        synchronized (C4806a.class) {
            aVar = f10471af;
        }
        return aVar;
    }

    /* renamed from: c */
    public static String m16493c() {
        return "3.1.0";
    }

    /* renamed from: A */
    public final String mo26655A() {
        if (this.f10501ac == null) {
            this.f10501ac = C4807b.m16564g();
            C4888x.m16977a("Hardware serial number: %s", this.f10501ac);
        }
        return this.f10501ac;
    }

    /* renamed from: B */
    public final Map<String, String> mo26656B() {
        synchronized (this.f10519av) {
            if (this.f10506ai.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f10506ai);
            return hashMap;
        }
    }

    /* renamed from: C */
    public final void mo26657C() {
        synchronized (this.f10519av) {
            this.f10506ai.clear();
        }
    }

    /* renamed from: D */
    public final int mo26658D() {
        int size;
        synchronized (this.f10519av) {
            size = this.f10506ai.size();
        }
        return size;
    }

    /* renamed from: E */
    public final Set<String> mo26659E() {
        Set<String> keySet;
        synchronized (this.f10519av) {
            keySet = this.f10506ai.keySet();
        }
        return keySet;
    }

    /* renamed from: F */
    public final Map<String, String> mo26660F() {
        synchronized (this.f10523az) {
            if (this.f10507aj.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f10507aj);
            return hashMap;
        }
    }

    /* renamed from: G */
    public final Map<String, String> mo26661G() {
        synchronized (this.f10520aw) {
            if (this.f10508ak.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f10508ak);
            return hashMap;
        }
    }

    /* renamed from: H */
    public final int mo26662H() {
        int i;
        synchronized (this.f10521ax) {
            i = this.f10504ag;
        }
        return i;
    }

    /* renamed from: I */
    public final int mo26663I() {
        return this.f10505ah;
    }

    /* renamed from: J */
    public final synchronized Map<String, PlugInBean> mo26664J() {
        return null;
    }

    /* renamed from: L */
    public final String mo26665L() {
        if (this.f10512ao == null) {
            this.f10512ao = C4807b.m16580q();
        }
        return this.f10512ao;
    }

    /* renamed from: M */
    public final String mo26666M() {
        if (this.f10513ap == null) {
            this.f10513ap = C4807b.m16563f(this.f10477F);
        }
        return this.f10513ap;
    }

    /* renamed from: N */
    public final String mo26667N() {
        if (this.f10514aq == null) {
            this.f10514aq = C4807b.m16565g(this.f10477F);
        }
        return this.f10514aq;
    }

    /* renamed from: O */
    public final String mo26668O() {
        return C4807b.m16581r();
    }

    /* renamed from: P */
    public final String mo26669P() {
        if (this.f10515ar == null) {
            this.f10515ar = C4807b.m16567h(this.f10477F);
        }
        return this.f10515ar;
    }

    /* renamed from: Q */
    public final long mo26670Q() {
        return C4807b.m16582s();
    }

    /* renamed from: R */
    public final boolean mo26671R() {
        if (this.f10510am == null) {
            this.f10510am = Boolean.valueOf(C4807b.m16569i(this.f10477F));
            C4888x.m16977a("Is it a virtual machine? " + this.f10510am, new Object[0]);
        }
        return this.f10510am.booleanValue();
    }

    /* renamed from: S */
    public final boolean mo26672S() {
        if (this.f10511an == null) {
            this.f10511an = Boolean.valueOf(C4807b.m16571j(this.f10477F));
            C4888x.m16977a("Does it has hook frame? " + this.f10511an, new Object[0]);
        }
        return this.f10511an.booleanValue();
    }

    /* renamed from: T */
    public final String mo26673T() {
        if (this.f10479H == null) {
            this.f10479H = AppInfo.m16487g(this.f10477F);
            C4888x.m16977a("Beacon channel " + this.f10479H, new Object[0]);
        }
        return this.f10479H;
    }

    /* renamed from: a */
    public final boolean mo26678a() {
        return this.f10509al;
    }

    /* renamed from: c */
    public final void mo26683c(String str) {
        this.f10480I = str;
        synchronized (this.f10523az) {
            this.f10507aj.put("E8", str);
        }
    }

    /* renamed from: d */
    public final void mo26685d() {
        synchronized (this.f10517at) {
            this.f10478G = UUID.randomUUID().toString();
        }
    }

    /* renamed from: e */
    public final String mo26687e() {
        String str;
        synchronized (this.f10517at) {
            if (this.f10478G == null) {
                synchronized (this.f10517at) {
                    this.f10478G = UUID.randomUUID().toString();
                }
            }
            str = this.f10478G;
        }
        return str;
    }

    /* renamed from: f */
    public final String mo26689f() {
        if (!C4893z.m17024a((String) null)) {
            return null;
        }
        return this.f10496Y;
    }

    /* renamed from: g */
    public final String mo26691g() {
        String str;
        synchronized (this.f10522ay) {
            str = this.f10481J;
        }
        return str;
    }

    /* renamed from: h */
    public final String mo26693h() {
        String str = this.f10480I;
        if (str != null) {
            return str;
        }
        this.f10480I = mo26696k() + "|" + mo26698m() + "|" + mo26699n();
        return this.f10480I;
    }

    /* renamed from: i */
    public final synchronized String mo26694i() {
        return this.f10482K;
    }

    /* renamed from: j */
    public final synchronized String mo26695j() {
        return this.f10483L;
    }

    /* renamed from: k */
    public final String mo26696k() {
        if (!this.f10494W) {
            return "";
        }
        if (this.f10484M == null) {
            this.f10484M = C4807b.m16558d();
        }
        return this.f10484M;
    }

    /* renamed from: l */
    public final String mo26697l() {
        if (!this.f10494W) {
            return "";
        }
        String str = this.f10485N;
        if (str == null || !str.contains(":")) {
            this.f10485N = C4807b.m16562f();
        }
        return this.f10485N;
    }

    /* renamed from: m */
    public final String mo26698m() {
        if (!this.f10494W) {
            return "";
        }
        if (this.f10486O == null) {
            this.f10486O = C4807b.m16560e();
        }
        return this.f10486O;
    }

    /* renamed from: n */
    public final String mo26699n() {
        if (!this.f10494W) {
            return "";
        }
        if (this.f10487P == null) {
            this.f10487P = C4807b.m16552a(this.f10477F);
        }
        return this.f10487P;
    }

    /* renamed from: o */
    public final long mo26700o() {
        if (this.f10488Q <= 0) {
            this.f10488Q = C4807b.m16566h();
        }
        return this.f10488Q;
    }

    /* renamed from: p */
    public final long mo26701p() {
        if (this.f10489R <= 0) {
            this.f10489R = C4807b.m16570j();
        }
        return this.f10489R;
    }

    /* renamed from: q */
    public final long mo26702q() {
        if (this.f10490S <= 0) {
            this.f10490S = C4807b.m16575l();
        }
        return this.f10490S;
    }

    /* renamed from: r */
    public final String mo26703r() {
        if (this.f10491T == null) {
            this.f10491T = C4807b.m16553a(this.f10477F, true);
        }
        return this.f10491T;
    }

    /* renamed from: s */
    public final String mo26704s() {
        if (this.f10492U == null) {
            this.f10492U = C4807b.m16561e(this.f10477F);
        }
        return this.f10492U;
    }

    /* renamed from: t */
    public final String mo26705t() {
        try {
            Map<String, ?> all = this.f10477F.getSharedPreferences("BuglySdkInfos", 0).getAll();
            if (!all.isEmpty()) {
                synchronized (this.f10518au) {
                    for (Map.Entry entry : all.entrySet()) {
                        try {
                            this.f10472A.put(entry.getKey(), entry.getValue().toString());
                        } catch (Throwable th) {
                            C4888x.m16978a(th);
                        }
                    }
                }
            }
        } catch (Throwable th2) {
            C4888x.m16978a(th2);
        }
        if (this.f10472A.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry2 : this.f10472A.entrySet()) {
            sb.append("[");
            sb.append((String) entry2.getKey());
            sb.append(",");
            sb.append((String) entry2.getValue());
            sb.append("] ");
        }
        mo26684c("SDK_INFO", sb.toString());
        return sb.toString();
    }

    /* renamed from: u */
    public final String mo26706u() {
        if (this.f10516as == null) {
            this.f10516as = AppInfo.m16485e(this.f10477F);
        }
        return this.f10516as;
    }

    /* renamed from: v */
    public final synchronized Map<String, PlugInBean> mo26707v() {
        return null;
    }

    /* renamed from: w */
    public final String mo26708w() {
        if (this.f10495X == null) {
            this.f10495X = C4807b.m16577n();
        }
        return this.f10495X;
    }

    /* renamed from: x */
    public final Boolean mo26709x() {
        if (this.f10497Z == null) {
            this.f10497Z = Boolean.valueOf(C4807b.m16579p());
        }
        return this.f10497Z;
    }

    /* renamed from: y */
    public final String mo26710y() {
        if (this.f10499aa == null) {
            this.f10499aa = C4807b.m16559d(this.f10477F);
            C4888x.m16977a("ROM ID: %s", this.f10499aa);
        }
        return this.f10499aa;
    }

    /* renamed from: z */
    public final String mo26711z() {
        if (this.f10500ab == null) {
            this.f10500ab = C4807b.m16555b(this.f10477F);
            C4888x.m16977a("SIM serial number: %s", this.f10500ab);
        }
        return this.f10500ab;
    }

    /* renamed from: a */
    public final void mo26677a(boolean z) {
        this.f10509al = z;
        C4794a aVar = this.f10475D;
        if (aVar != null) {
            aVar.setNativeIsAppForeground(z);
        }
    }

    /* renamed from: b */
    public final void mo26680b(String str) {
        synchronized (this.f10522ay) {
            if (str == null) {
                str = "10000";
            }
            this.f10481J = str;
        }
    }

    /* renamed from: f */
    public final String mo26690f(String str) {
        String remove;
        if (C4893z.m17024a(str)) {
            C4888x.m16983d("key should not be empty %s", str);
            return null;
        }
        synchronized (this.f10519av) {
            remove = this.f10506ai.remove(str);
        }
        return remove;
    }

    /* renamed from: d */
    public final synchronized void mo26686d(String str) {
        this.f10482K = str;
    }

    /* renamed from: g */
    public final String mo26692g(String str) {
        String str2;
        if (C4893z.m17024a(str)) {
            C4888x.m16983d("key should not be empty %s", str);
            return null;
        }
        synchronized (this.f10519av) {
            str2 = this.f10506ai.get(str);
        }
        return str2;
    }

    /* renamed from: a */
    public static synchronized C4806a m16491a(Context context) {
        C4806a aVar;
        synchronized (C4806a.class) {
            if (f10471af == null) {
                f10471af = new C4806a(context);
            }
            aVar = f10471af;
        }
        return aVar;
    }

    /* renamed from: b */
    public final void mo26682b(boolean z) {
        this.f10494W = z;
    }

    /* renamed from: c */
    public final void mo26684c(String str, String str2) {
        if (C4893z.m17024a(str) || C4893z.m17024a(str2)) {
            C4888x.m16983d("server key&value should not be empty %s %s", str, str2);
            return;
        }
        synchronized (this.f10520aw) {
            this.f10508ak.put(str, str2);
        }
    }

    /* renamed from: b */
    public final void mo26681b(String str, String str2) {
        if (C4893z.m17024a(str) || C4893z.m17024a(str2)) {
            C4888x.m16983d("key&value should not be empty %s %s", str, str2);
            return;
        }
        synchronized (this.f10519av) {
            this.f10506ai.put(str, str2);
        }
    }

    /* renamed from: e */
    public final synchronized void mo26688e(String str) {
        this.f10483L = str;
    }

    /* renamed from: a */
    public final void mo26675a(String str) {
        this.f10496Y = str;
        mo26684c("APP_ID", str);
    }

    /* renamed from: a */
    public final void mo26676a(String str, String str2) {
        if (str != null && str2 != null) {
            synchronized (this.f10518au) {
                this.f10472A.put(str, str2);
            }
        }
    }

    /* renamed from: b */
    public final void mo26679b(int i) {
        int i2 = this.f10505ah;
        if (i2 != 24096) {
            this.f10505ah = 24096;
            C4888x.m16977a("server scene tag %d changed to tag %d", Integer.valueOf(i2), Integer.valueOf(this.f10505ah));
        }
    }

    /* renamed from: a */
    public final void mo26674a(int i) {
        synchronized (this.f10521ax) {
            int i2 = this.f10504ag;
            if (i2 != i) {
                this.f10504ag = i;
                C4888x.m16977a("user scene tag %d changed to tag %d", Integer.valueOf(i2), Integer.valueOf(this.f10504ag));
            }
        }
    }
}
