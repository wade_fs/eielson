package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.at */
/* compiled from: BUGLY */
public final class C4856at extends C4868k {

    /* renamed from: i */
    private static Map<String, String> f10894i = new HashMap();

    /* renamed from: a */
    public long f10895a = 0;

    /* renamed from: b */
    public byte f10896b = 0;

    /* renamed from: c */
    public String f10897c = "";

    /* renamed from: d */
    public String f10898d = "";

    /* renamed from: e */
    public String f10899e = "";

    /* renamed from: f */
    public Map<String, String> f10900f = null;

    /* renamed from: g */
    public String f10901g = "";

    /* renamed from: h */
    public boolean f10902h = true;

    static {
        f10894i.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.j.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.j.a(byte, int):void
      com.tencent.bugly.proguard.j.a(int, int):void
      com.tencent.bugly.proguard.j.a(long, int):void
      com.tencent.bugly.proguard.j.a(com.tencent.bugly.proguard.k, int):void
      com.tencent.bugly.proguard.j.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.j.a(java.lang.String, int):void
      com.tencent.bugly.proguard.j.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.j.a(short, int):void
      com.tencent.bugly.proguard.j.a(boolean, int):void
      com.tencent.bugly.proguard.j.a(byte[], int):void
      com.tencent.bugly.proguard.j.a(java.util.Map, int):void */
    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26877a(this.f10895a, 0);
        jVar.mo26875a(this.f10896b, 1);
        String str = this.f10897c;
        if (str != null) {
            jVar.mo26880a(str, 2);
        }
        String str2 = this.f10898d;
        if (str2 != null) {
            jVar.mo26880a(str2, 3);
        }
        String str3 = this.f10899e;
        if (str3 != null) {
            jVar.mo26880a(str3, 4);
        }
        Map<String, String> map = this.f10900f;
        if (map != null) {
            jVar.mo26882a((Map) map, 5);
        }
        String str4 = this.f10901g;
        if (str4 != null) {
            jVar.mo26880a(str4, 6);
        }
        jVar.mo26884a(this.f10902h, 7);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.i$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.i.a(int, boolean):boolean */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10895a = iVar.mo26864a(this.f10895a, 0, true);
        this.f10896b = iVar.mo26861a(this.f10896b, 1, true);
        this.f10897c = iVar.mo26871b(2, false);
        this.f10898d = iVar.mo26871b(3, false);
        this.f10899e = iVar.mo26871b(4, false);
        this.f10900f = (Map) iVar.mo26866a((Object) f10894i, 5, false);
        this.f10901g = iVar.mo26871b(6, false);
        this.f10902h = iVar.mo26870a(7, false);
    }
}
