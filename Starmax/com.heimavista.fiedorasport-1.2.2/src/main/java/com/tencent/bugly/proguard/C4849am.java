package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.am */
/* compiled from: BUGLY */
public final class C4849am extends C4868k implements Cloneable {

    /* renamed from: d */
    private static byte[] f10806d;

    /* renamed from: a */
    private byte f10807a = 0;

    /* renamed from: b */
    private String f10808b = "";

    /* renamed from: c */
    private byte[] f10809c = null;

    public C4849am() {
    }

    /* renamed from: a */
    public final void mo26840a(C4867j jVar) {
        jVar.mo26875a(this.f10807a, 0);
        jVar.mo26880a(this.f10808b, 1);
        byte[] bArr = this.f10809c;
        if (bArr != null) {
            jVar.mo26885a(bArr, 2);
        }
    }

    /* renamed from: a */
    public final void mo26841a(StringBuilder sb, int i) {
    }

    public C4849am(byte b, String str, byte[] bArr) {
        this.f10807a = b;
        this.f10808b = str;
        this.f10809c = bArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte */
    /* renamed from: a */
    public final void mo26839a(C4865i iVar) {
        this.f10807a = iVar.mo26861a(this.f10807a, 0, true);
        this.f10808b = iVar.mo26871b(1, true);
        if (f10806d == null) {
            byte[] bArr = new byte[1];
            f10806d = bArr;
            bArr[0] = 0;
        }
        this.f10809c = iVar.mo26872c(2, false);
    }
}
