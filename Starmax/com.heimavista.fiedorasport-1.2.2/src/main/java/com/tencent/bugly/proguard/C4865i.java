package com.tencent.bugly.proguard;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.i */
/* compiled from: BUGLY */
public final class C4865i {

    /* renamed from: a */
    private ByteBuffer f10932a;

    /* renamed from: b */
    private String f10933b = "GBK";

    /* renamed from: com.tencent.bugly.proguard.i$a */
    /* compiled from: BUGLY */
    public static class C4866a {

        /* renamed from: a */
        public byte f10934a;

        /* renamed from: b */
        public int f10935b;
    }

    public C4865i() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* renamed from: d */
    private boolean[] m16835d(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    boolean[] zArr = new boolean[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        zArr[i2] = mo26861a((byte) 0, 0, true) != 0;
                    }
                    return zArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short */
    /* renamed from: e */
    private short[] m16836e(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    short[] sArr = new short[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        sArr[i2] = mo26868a(sArr[0], 0, true);
                    }
                    return sArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* renamed from: f */
    private int[] m16837f(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    int[] iArr = new int[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        iArr[i2] = mo26862a(iArr[0], 0, true);
                    }
                    return iArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* renamed from: g */
    private long[] m16838g(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    long[] jArr = new long[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        jArr[i2] = mo26864a(jArr[0], 0, true);
                    }
                    return jArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(float, int, boolean):float
     arg types: [float, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(float, int, boolean):float */
    /* renamed from: h */
    private float[] m16839h(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    float[] fArr = new float[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        fArr[i2] = m16827a(fArr[0], 0, true);
                    }
                    return fArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(double, int, boolean):double
     arg types: [double, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(double, int, boolean):double */
    /* renamed from: i */
    private double[] m16840i(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    double[] dArr = new double[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        dArr[i2] = m16826a(dArr[0], 0, true);
                    }
                    return dArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    public final void mo26869a(byte[] bArr) {
        ByteBuffer byteBuffer = this.f10932a;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
        this.f10932a = ByteBuffer.wrap(bArr);
    }

    /* renamed from: b */
    public final String mo26871b(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 6) {
                int i2 = this.f10932a.get();
                if (i2 < 0) {
                    i2 += 256;
                }
                byte[] bArr = new byte[i2];
                this.f10932a.get(bArr);
                try {
                    return new String(bArr, this.f10933b);
                } catch (UnsupportedEncodingException unused) {
                    return new String(bArr);
                }
            } else if (b == 7) {
                int i3 = this.f10932a.getInt();
                if (i3 > 104857600 || i3 < 0) {
                    throw new C4863g("String too long: " + i3);
                }
                byte[] bArr2 = new byte[i3];
                this.f10932a.get(bArr2);
                try {
                    return new String(bArr2, this.f10933b);
                } catch (UnsupportedEncodingException unused2) {
                    return new String(bArr2);
                }
            } else {
                throw new C4863g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte */
    /* renamed from: c */
    public final byte[] mo26872c(int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    byte[] bArr = new byte[a];
                    for (int i2 = 0; i2 < a; i2++) {
                        bArr[i2] = mo26861a(bArr[0], 0, true);
                    }
                    return bArr;
                }
                throw new C4863g("size invalid: " + a);
            } else if (b == 13) {
                C4866a aVar2 = new C4866a();
                m16828a(aVar2, this.f10932a);
                if (aVar2.f10934a == 0) {
                    int a2 = mo26862a(0, 0, true);
                    if (a2 >= 0) {
                        byte[] bArr2 = new byte[a2];
                        this.f10932a.get(bArr2);
                        return bArr2;
                    }
                    throw new C4863g("invalid size, tag: " + i + ", type: " + ((int) aVar.f10934a) + ", " + ((int) aVar2.f10934a) + ", size: " + a2);
                }
                throw new C4863g("type mismatch, tag: " + i + ", type: " + ((int) aVar.f10934a) + ", " + ((int) aVar2.f10934a));
            } else {
                throw new C4863g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    public C4865i(byte[] bArr) {
        this.f10932a = ByteBuffer.wrap(bArr);
    }

    /* renamed from: a */
    private static int m16828a(C4866a aVar, ByteBuffer byteBuffer) {
        byte b = byteBuffer.get();
        aVar.f10934a = (byte) (b & 15);
        aVar.f10935b = (b & 240) >> 4;
        if (aVar.f10935b != 15) {
            return 1;
        }
        aVar.f10935b = byteBuffer.get();
        return 2;
    }

    public C4865i(byte[] bArr, int i) {
        this.f10932a = ByteBuffer.wrap(bArr);
        this.f10932a.position(4);
    }

    /* renamed from: a */
    private boolean m16832a(int i) {
        try {
            C4866a aVar = new C4866a();
            while (true) {
                int a = m16828a(aVar, this.f10932a.duplicate());
                if (i <= aVar.f10935b) {
                    break;
                } else if (aVar.f10934a == 11) {
                    break;
                } else {
                    this.f10932a.position(this.f10932a.position() + a);
                    m16831a(aVar.f10934a);
                }
            }
            if (i == aVar.f10935b) {
                return true;
            }
            return false;
        } catch (C4863g | BufferUnderflowException unused) {
        }
    }

    /* renamed from: a */
    private void m16830a() {
        C4866a aVar = new C4866a();
        do {
            m16828a(aVar, this.f10932a);
            m16831a(aVar.f10934a);
        } while (aVar.f10934a != 11);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: b */
    private <T> T[] m16834b(T t, int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 9) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    T[] tArr = (Object[]) Array.newInstance(t.getClass(), a);
                    for (int i2 = 0; i2 < a; i2++) {
                        tArr[i2] = mo26866a((Object) t, 0, true);
                    }
                    return tArr;
                }
                throw new C4863g("size invalid: " + a);
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* renamed from: a */
    private void m16831a(byte b) {
        int i = 0;
        switch (b) {
            case 0:
                ByteBuffer byteBuffer = this.f10932a;
                byteBuffer.position(byteBuffer.position() + 1);
                return;
            case 1:
                ByteBuffer byteBuffer2 = this.f10932a;
                byteBuffer2.position(byteBuffer2.position() + 2);
                return;
            case 2:
                ByteBuffer byteBuffer3 = this.f10932a;
                byteBuffer3.position(byteBuffer3.position() + 4);
                return;
            case 3:
                ByteBuffer byteBuffer4 = this.f10932a;
                byteBuffer4.position(byteBuffer4.position() + 8);
                return;
            case 4:
                ByteBuffer byteBuffer5 = this.f10932a;
                byteBuffer5.position(byteBuffer5.position() + 4);
                return;
            case 5:
                ByteBuffer byteBuffer6 = this.f10932a;
                byteBuffer6.position(byteBuffer6.position() + 8);
                return;
            case 6:
                int i2 = this.f10932a.get();
                if (i2 < 0) {
                    i2 += 256;
                }
                ByteBuffer byteBuffer7 = this.f10932a;
                byteBuffer7.position(byteBuffer7.position() + i2);
                return;
            case 7:
                int i3 = this.f10932a.getInt();
                ByteBuffer byteBuffer8 = this.f10932a;
                byteBuffer8.position(byteBuffer8.position() + i3);
                return;
            case 8:
                int a = mo26862a(0, 0, true);
                while (i < (a << 1)) {
                    C4866a aVar = new C4866a();
                    m16828a(aVar, this.f10932a);
                    m16831a(aVar.f10934a);
                    i++;
                }
                return;
            case 9:
                int a2 = mo26862a(0, 0, true);
                while (i < a2) {
                    C4866a aVar2 = new C4866a();
                    m16828a(aVar2, this.f10932a);
                    m16831a(aVar2.f10934a);
                    i++;
                }
                return;
            case 10:
                m16830a();
                return;
            case 11:
            case 12:
                return;
            case 13:
                C4866a aVar3 = new C4866a();
                m16828a(aVar3, this.f10932a);
                if (aVar3.f10934a == 0) {
                    int a3 = mo26862a(0, 0, true);
                    ByteBuffer byteBuffer9 = this.f10932a;
                    byteBuffer9.position(byteBuffer9.position() + a3);
                    return;
                }
                throw new C4863g("skipField with invalid type, type value: " + ((int) b) + ", " + ((int) aVar3.f10934a));
            default:
                throw new C4863g("invalid type.");
        }
    }

    /* renamed from: a */
    public final boolean mo26870a(int i, boolean z) {
        return mo26861a((byte) 0, i, z) != 0;
    }

    /* renamed from: a */
    public final byte mo26861a(byte b, int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b2 = aVar.f10934a;
            if (b2 == 0) {
                return this.f10932a.get();
            }
            if (b2 == 12) {
                return 0;
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return b;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    public final short mo26868a(short s, int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 0) {
                return (short) this.f10932a.get();
            }
            if (b == 1) {
                return this.f10932a.getShort();
            }
            if (b == 12) {
                return 0;
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return s;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    public final int mo26862a(int i, int i2, boolean z) {
        if (m16832a(i2)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 0) {
                return this.f10932a.get();
            }
            if (b == 1) {
                return this.f10932a.getShort();
            }
            if (b == 2) {
                return this.f10932a.getInt();
            }
            if (b == 12) {
                return 0;
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return i;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    public final long mo26864a(long j, int i, boolean z) {
        int i2;
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 0) {
                i2 = this.f10932a.get();
            } else if (b == 1) {
                i2 = this.f10932a.getShort();
            } else if (b == 2) {
                i2 = this.f10932a.getInt();
            } else if (b == 3) {
                return this.f10932a.getLong();
            } else {
                if (b == 12) {
                    return 0;
                }
                throw new C4863g("type mismatch.");
            }
            return (long) i2;
        } else if (!z) {
            return j;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    private float m16827a(float f, int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 4) {
                return this.f10932a.getFloat();
            }
            if (b == 12) {
                return 0.0f;
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return f;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    private double m16826a(double d, int i, boolean z) {
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            byte b = aVar.f10934a;
            if (b == 4) {
                return (double) this.f10932a.getFloat();
            }
            if (b == 5) {
                return this.f10932a.getDouble();
            }
            if (b == 12) {
                return 0.0d;
            }
            throw new C4863g("type mismatch.");
        } else if (!z) {
            return d;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* renamed from: a */
    public final <K, V> HashMap<K, V> mo26867a(Map map, int i, boolean z) {
        return (HashMap) m16829a(new HashMap(), map, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    private <K, V> Map<K, V> m16829a(Map<K, V> map, Map<K, V> map2, int i, boolean z) {
        if (map2 == null || map2.isEmpty()) {
            return new HashMap();
        }
        Map.Entry next = map2.entrySet().iterator().next();
        Object key = next.getKey();
        Object value = next.getValue();
        if (m16832a(i)) {
            C4866a aVar = new C4866a();
            m16828a(aVar, this.f10932a);
            if (aVar.f10934a == 8) {
                int a = mo26862a(0, 0, true);
                if (a >= 0) {
                    for (int i2 = 0; i2 < a; i2++) {
                        map.put(mo26866a(key, 0, true), mo26866a(value, 1, true));
                    }
                } else {
                    throw new C4863g("size invalid: " + a);
                }
            } else {
                throw new C4863g("type mismatch.");
            }
        } else if (z) {
            throw new C4863g("require field not exist.");
        }
        return map;
    }

    /* renamed from: a */
    private <T> T[] m16833a(Object[] objArr, int i, boolean z) {
        if (objArr != null && objArr.length != 0) {
            return m16834b(objArr[0], i, z);
        }
        throw new C4863g("unable to get type of key and value.");
    }

    /* renamed from: a */
    public final C4868k mo26865a(C4868k kVar, int i, boolean z) {
        if (m16832a(i)) {
            try {
                C4868k kVar2 = (C4868k) kVar.getClass().newInstance();
                C4866a aVar = new C4866a();
                m16828a(aVar, this.f10932a);
                if (aVar.f10934a == 10) {
                    kVar2.mo26839a(this);
                    m16830a();
                    return kVar2;
                }
                throw new C4863g("type mismatch.");
            } catch (Exception e) {
                throw new C4863g(e.getMessage());
            }
        } else if (!z) {
            return null;
        } else {
            throw new C4863g("require field not exist.");
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [int] */
    /* JADX WARN: Type inference failed for: r1v3, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(short, int, boolean):short
     arg types: [int, int, boolean]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(long, int, boolean):long
     arg types: [int, int, boolean]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(float, int, boolean):float
     arg types: [int, int, boolean]
     candidates:
      com.tencent.bugly.proguard.i.a(double, int, boolean):double
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(float, int, boolean):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(double, int, boolean):double
     arg types: [int, int, boolean]
     candidates:
      com.tencent.bugly.proguard.i.a(float, int, boolean):float
      com.tencent.bugly.proguard.i.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.i.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.i.a(int, int, boolean):int
      com.tencent.bugly.proguard.i.a(long, int, boolean):long
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.k, int, boolean):com.tencent.bugly.proguard.k
      com.tencent.bugly.proguard.i.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.i.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.i.a(short, int, boolean):short
      com.tencent.bugly.proguard.i.a(double, int, boolean):double */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> java.lang.Object mo26866a(java.lang.Object r3, int r4, boolean r5) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof java.lang.Byte
            r1 = 0
            if (r0 == 0) goto L_0x000e
            byte r3 = r2.mo26861a(r1, r4, r5)
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            return r3
        L_0x000e:
            boolean r0 = r3 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x001e
            byte r3 = r2.mo26861a(r1, r4, r5)
            if (r3 == 0) goto L_0x0019
            r1 = 1
        L_0x0019:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
            return r3
        L_0x001e:
            boolean r0 = r3 instanceof java.lang.Short
            if (r0 == 0) goto L_0x002b
            short r3 = r2.mo26868a(r1, r4, r5)
            java.lang.Short r3 = java.lang.Short.valueOf(r3)
            return r3
        L_0x002b:
            boolean r0 = r3 instanceof java.lang.Integer
            if (r0 == 0) goto L_0x0038
            int r3 = r2.mo26862a(r1, r4, r5)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            return r3
        L_0x0038:
            boolean r0 = r3 instanceof java.lang.Long
            if (r0 == 0) goto L_0x0047
            r0 = 0
            long r3 = r2.mo26864a(r0, r4, r5)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            return r3
        L_0x0047:
            boolean r0 = r3 instanceof java.lang.Float
            if (r0 == 0) goto L_0x0055
            r3 = 0
            float r3 = r2.m16827a(r3, r4, r5)
            java.lang.Float r3 = java.lang.Float.valueOf(r3)
            return r3
        L_0x0055:
            boolean r0 = r3 instanceof java.lang.Double
            if (r0 == 0) goto L_0x0064
            r0 = 0
            double r3 = r2.m16826a(r0, r4, r5)
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            return r3
        L_0x0064:
            boolean r0 = r3 instanceof java.lang.String
            if (r0 == 0) goto L_0x0071
            java.lang.String r3 = r2.mo26871b(r4, r5)
            java.lang.String r3 = java.lang.String.valueOf(r3)
            return r3
        L_0x0071:
            boolean r0 = r3 instanceof java.util.Map
            if (r0 == 0) goto L_0x0083
            java.util.Map r3 = (java.util.Map) r3
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.util.Map r3 = r2.m16829a(r0, r3, r4, r5)
            java.util.HashMap r3 = (java.util.HashMap) r3
            return r3
        L_0x0083:
            boolean r0 = r3 instanceof java.util.List
            if (r0 == 0) goto L_0x00b5
            java.util.List r3 = (java.util.List) r3
            if (r3 == 0) goto L_0x00af
            boolean r0 = r3.isEmpty()
            if (r0 == 0) goto L_0x0092
            goto L_0x00af
        L_0x0092:
            java.lang.Object r3 = r3.get(r1)
            java.lang.Object[] r3 = r2.m16834b(r3, r4, r5)
            if (r3 != 0) goto L_0x009e
            r3 = 0
            return r3
        L_0x009e:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
        L_0x00a3:
            int r5 = r3.length
            if (r1 >= r5) goto L_0x00ae
            r5 = r3[r1]
            r4.add(r5)
            int r1 = r1 + 1
            goto L_0x00a3
        L_0x00ae:
            return r4
        L_0x00af:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            return r3
        L_0x00b5:
            boolean r0 = r3 instanceof com.tencent.bugly.proguard.C4868k
            if (r0 == 0) goto L_0x00c0
            com.tencent.bugly.proguard.k r3 = (com.tencent.bugly.proguard.C4868k) r3
            com.tencent.bugly.proguard.k r3 = r2.mo26865a(r3, r4, r5)
            return r3
        L_0x00c0:
            java.lang.Class r0 = r3.getClass()
            boolean r0 = r0.isArray()
            if (r0 == 0) goto L_0x0115
            boolean r0 = r3 instanceof byte[]
            if (r0 != 0) goto L_0x0110
            boolean r0 = r3 instanceof java.lang.Byte[]
            if (r0 == 0) goto L_0x00d3
            goto L_0x0110
        L_0x00d3:
            boolean r0 = r3 instanceof boolean[]
            if (r0 == 0) goto L_0x00dc
            boolean[] r3 = r2.m16835d(r4, r5)
            return r3
        L_0x00dc:
            boolean r0 = r3 instanceof short[]
            if (r0 == 0) goto L_0x00e5
            short[] r3 = r2.m16836e(r4, r5)
            return r3
        L_0x00e5:
            boolean r0 = r3 instanceof int[]
            if (r0 == 0) goto L_0x00ee
            int[] r3 = r2.m16837f(r4, r5)
            return r3
        L_0x00ee:
            boolean r0 = r3 instanceof long[]
            if (r0 == 0) goto L_0x00f7
            long[] r3 = r2.m16838g(r4, r5)
            return r3
        L_0x00f7:
            boolean r0 = r3 instanceof float[]
            if (r0 == 0) goto L_0x0100
            float[] r3 = r2.m16839h(r4, r5)
            return r3
        L_0x0100:
            boolean r0 = r3 instanceof double[]
            if (r0 == 0) goto L_0x0109
            double[] r3 = r2.m16840i(r4, r5)
            return r3
        L_0x0109:
            java.lang.Object[] r3 = (java.lang.Object[]) r3
            java.lang.Object[] r3 = r2.m16833a(r3, r4, r5)
            return r3
        L_0x0110:
            byte[] r3 = r2.mo26872c(r4, r5)
            return r3
        L_0x0115:
            com.tencent.bugly.proguard.g r3 = new com.tencent.bugly.proguard.g
            java.lang.String r4 = "read object error: unsupport type."
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C4865i.mo26866a(java.lang.Object, int, boolean):java.lang.Object");
    }

    /* renamed from: a */
    public final int mo26863a(String str) {
        this.f10933b = str;
        return 0;
    }
}
