package com.tencent.bugly.crashreport;

/* renamed from: com.tencent.bugly.crashreport.a */
/* compiled from: BUGLY */
public interface C4794a {
    boolean appendLogToNative(String str, String str2, String str3);

    String getLogFromNative();

    boolean setNativeIsAppForeground(boolean z);
}
