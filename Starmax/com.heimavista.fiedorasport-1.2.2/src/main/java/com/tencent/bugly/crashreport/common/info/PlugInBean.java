package com.tencent.bugly.crashreport.common.info;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: BUGLY */
public class PlugInBean implements Parcelable {
    public static final Parcelable.Creator<PlugInBean> CREATOR = new C4805a();

    /* renamed from: a */
    public final String f10468a;

    /* renamed from: b */
    public final String f10469b;

    /* renamed from: c */
    public final String f10470c;

    /* renamed from: com.tencent.bugly.crashreport.common.info.PlugInBean$a */
    /* compiled from: BUGLY */
    static class C4805a implements Parcelable.Creator<PlugInBean> {
        C4805a() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new PlugInBean(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new PlugInBean[i];
        }
    }

    public PlugInBean(String str, String str2, String str3) {
        this.f10468a = str;
        this.f10469b = str2;
        this.f10470c = str3;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "plid:" + this.f10468a + " plV:" + this.f10469b + " plUUID:" + this.f10470c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f10468a);
        parcel.writeString(this.f10469b);
        parcel.writeString(this.f10470c);
    }

    public PlugInBean(Parcel parcel) {
        this.f10468a = parcel.readString();
        this.f10469b = parcel.readString();
        this.f10470c = parcel.readString();
    }
}
