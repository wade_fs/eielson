package com.tencent.mmkv;

/* renamed from: com.tencent.mmkv.c */
public enum MMKVLogLevel {
    LevelDebug,
    LevelInfo,
    LevelWarning,
    LevelError,
    LevelNone
}
