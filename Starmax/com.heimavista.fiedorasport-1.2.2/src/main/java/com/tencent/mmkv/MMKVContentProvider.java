package com.tencent.mmkv;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MMKVContentProvider extends ContentProvider {

    /* renamed from: P */
    private static Uri f11123P;

    @Nullable
    /* renamed from: a */
    protected static Uri m17104a(Context context) {
        String b;
        Uri uri = f11123P;
        if (uri != null) {
            return uri;
        }
        if (context == null || (b = m17107b(context)) == null) {
            return null;
        }
        f11123P = Uri.parse("content://" + b);
        return f11123P;
    }

    /* renamed from: b */
    private static String m17107b(Context context) {
        ProviderInfo providerInfo;
        try {
            ComponentName componentName = new ComponentName(context, MMKVContentProvider.class.getName());
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (providerInfo = packageManager.getProviderInfo(componentName, 0)) == null) {
                return null;
            }
            return providerInfo.authority;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public Bundle call(@NonNull String str, @Nullable String str2, @Nullable Bundle bundle) {
        if (!str.equals("mmkvFromAshmemID") || bundle == null) {
            return null;
        }
        return m17105a(str2, bundle.getInt("KEY_SIZE"), bundle.getInt("KEY_MODE"), bundle.getString("KEY_CRYPT"));
    }

    public int delete(@NonNull Uri uri, @Nullable String str, @Nullable String[] strArr) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    @Nullable
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    public boolean onCreate() {
        String b;
        Context context = getContext();
        if (context == null || (b = m17107b(context)) == null) {
            return false;
        }
        if (f11123P != null) {
            return true;
        }
        f11123P = Uri.parse("content://" + b);
        return true;
    }

    @Nullable
    public Cursor query(@NonNull Uri uri, @Nullable String[] strArr, @Nullable String str, @Nullable String[] strArr2, @Nullable String str2) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String str, @Nullable String[] strArr) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    /* renamed from: a */
    private Bundle m17105a(String str, int i, int i2, String str2) {
        MMKV a = MMKV.m17078a(getContext(), str, i, i2, str2);
        if (a == null) {
            return null;
        }
        ParcelableMMKV parcelableMMKV = new ParcelableMMKV(a);
        Log.i("MMKV", str + " fd = " + a.ashmemFD() + ", meta fd = " + a.ashmemMetaFD());
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY", parcelableMMKV);
        return bundle;
    }

    /* renamed from: a */
    protected static String m17106a(Context context, int i) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null) {
            return "";
        }
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : activityManager.getRunningAppProcesses()) {
            if (runningAppProcessInfo.pid == i) {
                return runningAppProcessInfo.processName;
            }
        }
        return "";
    }
}
