package com.tencent.mmkv;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import java.io.IOException;

public final class ParcelableMMKV implements Parcelable {
    public static final Parcelable.Creator<ParcelableMMKV> CREATOR = new C4915a();

    /* renamed from: P */
    private String f11124P;

    /* renamed from: Q */
    private int f11125Q;

    /* renamed from: R */
    private int f11126R;

    /* renamed from: S */
    private String f11127S;

    /* renamed from: com.tencent.mmkv.ParcelableMMKV$a */
    static class C4915a implements Parcelable.Creator<ParcelableMMKV> {
        C4915a() {
        }

        public ParcelableMMKV createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(parcel);
            ParcelFileDescriptor parcelFileDescriptor2 = (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(parcel);
            String readString2 = parcel.readString();
            if (parcelFileDescriptor == null || parcelFileDescriptor2 == null) {
                return null;
            }
            return new ParcelableMMKV(readString, parcelFileDescriptor.detachFd(), parcelFileDescriptor2.detachFd(), readString2, null);
        }

        public ParcelableMMKV[] newArray(int i) {
            return new ParcelableMMKV[i];
        }
    }

    /* synthetic */ ParcelableMMKV(String str, int i, int i2, String str2, C4915a aVar) {
        this(str, i, i2, str2);
    }

    /* renamed from: a */
    public MMKV mo27087a() {
        int i;
        int i2 = this.f11125Q;
        if (i2 < 0 || (i = this.f11126R) < 0) {
            return null;
        }
        return MMKV.m17079a(this.f11124P, i2, i, this.f11127S);
    }

    public int describeContents() {
        return 1;
    }

    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(this.f11124P);
            ParcelFileDescriptor fromFd = ParcelFileDescriptor.fromFd(this.f11125Q);
            ParcelFileDescriptor fromFd2 = ParcelFileDescriptor.fromFd(this.f11126R);
            int i2 = i | 1;
            fromFd.writeToParcel(parcel, i2);
            fromFd2.writeToParcel(parcel, i2);
            if (this.f11127S != null) {
                parcel.writeString(this.f11127S);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ParcelableMMKV(MMKV mmkv) {
        this.f11125Q = -1;
        this.f11126R = -1;
        this.f11127S = null;
        this.f11124P = mmkv.mmapID();
        this.f11125Q = mmkv.ashmemFD();
        this.f11126R = mmkv.ashmemMetaFD();
        this.f11127S = mmkv.cryptKey();
    }

    private ParcelableMMKV(String str, int i, int i2, String str2) {
        this.f11125Q = -1;
        this.f11126R = -1;
        this.f11127S = null;
        this.f11124P = str;
        this.f11125Q = i;
        this.f11126R = i2;
        this.f11127S = str2;
    }
}
