package com.tencent.mmkv;

/* renamed from: com.tencent.mmkv.b */
public interface MMKVHandler {
    /* renamed from: a */
    MMKVRecoverStrategic mo27093a(String str);

    /* renamed from: a */
    void mo27094a(MMKVLogLevel cVar, String str, int i, String str2, String str3);

    /* renamed from: b */
    MMKVRecoverStrategic mo27095b(String str);
}
