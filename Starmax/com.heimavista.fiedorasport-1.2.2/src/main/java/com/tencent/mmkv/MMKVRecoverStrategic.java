package com.tencent.mmkv;

/* renamed from: com.tencent.mmkv.d */
public enum MMKVRecoverStrategic {
    OnErrorDiscard,
    OnErrorRecover
}
