package com.tencent.mmkv;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.util.Log;
import androidx.annotation.Nullable;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MMKV implements SharedPreferences, SharedPreferences.Editor {

    /* renamed from: a */
    private static EnumMap<MMKVRecoverStrategic, Integer> f11114a = new EnumMap<>(MMKVRecoverStrategic.class);

    /* renamed from: b */
    private static EnumMap<MMKVLogLevel, Integer> f11115b = new EnumMap<>(MMKVLogLevel.class);

    /* renamed from: c */
    private static MMKVLogLevel[] f11116c = {MMKVLogLevel.LevelDebug, MMKVLogLevel.LevelInfo, MMKVLogLevel.LevelWarning, MMKVLogLevel.LevelError, MMKVLogLevel.LevelNone};

    /* renamed from: d */
    private static String f11117d = null;

    /* renamed from: e */
    private static final HashMap<String, Parcelable.Creator<?>> f11118e = new HashMap<>();

    /* renamed from: f */
    private static MMKVHandler f11119f;

    /* renamed from: g */
    private static boolean f11120g = false;

    /* renamed from: h */
    private static MMKVContentChangeNotification f11121h;
    private long nativeHandle;

    /* renamed from: com.tencent.mmkv.MMKV$a */
    static /* synthetic */ class C4913a {

        /* renamed from: a */
        static final /* synthetic */ int[] f11122a = new int[MMKVLogLevel.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.tencent.mmkv.c[] r0 = com.tencent.mmkv.MMKVLogLevel.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tencent.mmkv.MMKV.C4913a.f11122a = r0
                int[] r0 = com.tencent.mmkv.MMKV.C4913a.f11122a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tencent.mmkv.c r1 = com.tencent.mmkv.MMKVLogLevel.LevelDebug     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tencent.mmkv.MMKV.C4913a.f11122a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tencent.mmkv.c r1 = com.tencent.mmkv.MMKVLogLevel.LevelInfo     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.tencent.mmkv.MMKV.C4913a.f11122a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.tencent.mmkv.c r1 = com.tencent.mmkv.MMKVLogLevel.LevelWarning     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.tencent.mmkv.MMKV.C4913a.f11122a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.tencent.mmkv.c r1 = com.tencent.mmkv.MMKVLogLevel.LevelError     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.tencent.mmkv.MMKV.C4913a.f11122a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.tencent.mmkv.c r1 = com.tencent.mmkv.MMKVLogLevel.LevelNone     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.mmkv.MMKV.C4913a.<clinit>():void");
        }
    }

    /* renamed from: com.tencent.mmkv.MMKV$b */
    public interface C4914b {
        /* renamed from: a */
        void mo27079a(String str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [com.tencent.mmkv.d, int]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(e.d.a.a.d, java.lang.Integer):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [com.tencent.mmkv.c, int]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(e.d.a.a.d, java.lang.Integer):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    static {
        f11114a.put((Enum) MMKVRecoverStrategic.OnErrorDiscard, (Object) 0);
        f11114a.put((Enum) MMKVRecoverStrategic.OnErrorRecover, (Object) 1);
        f11115b.put((Enum) MMKVLogLevel.LevelDebug, (Object) 0);
        f11115b.put((Enum) MMKVLogLevel.LevelInfo, (Object) 1);
        f11115b.put((Enum) MMKVLogLevel.LevelWarning, (Object) 2);
        f11115b.put((Enum) MMKVLogLevel.LevelError, (Object) 3);
        f11115b.put((Enum) MMKVLogLevel.LevelNone, (Object) 4);
    }

    private MMKV(long j) {
        this.nativeHandle = j;
    }

    /* renamed from: a */
    public static String m17080a(Context context) {
        String str = context.getFilesDir().getAbsolutePath() + "/mmkv";
        m17081a(str, (C4914b) null, MMKVLogLevel.LevelInfo);
        return str;
    }

    private native boolean containsKey(long j, String str);

    private native long count(long j);

    private static native long createNB(int i);

    private native boolean decodeBool(long j, String str, boolean z);

    private native byte[] decodeBytes(long j, String str);

    private native double decodeDouble(long j, String str, double d);

    private native float decodeFloat(long j, String str, float f);

    private native int decodeInt(long j, String str, int i);

    private native long decodeLong(long j, String str, long j2);

    private native String decodeString(long j, String str, String str2);

    private native String[] decodeStringSet(long j, String str);

    private static native void destroyNB(long j, int i);

    /* renamed from: e */
    public static MMKV m17083e(String str) {
        if (f11117d != null) {
            return new MMKV(getMMKVWithID(str, 1, null, null));
        }
        throw new IllegalStateException("You should Call MMKV.initialize() first.");
    }

    private native boolean encodeBool(long j, String str, boolean z);

    private native boolean encodeBytes(long j, String str, byte[] bArr);

    private native boolean encodeDouble(long j, String str, double d);

    private native boolean encodeFloat(long j, String str, float f);

    private native boolean encodeInt(long j, String str, int i);

    private native boolean encodeLong(long j, String str, long j2);

    private native boolean encodeSet(long j, String str, String[] strArr);

    private native boolean encodeString(long j, String str, String str2);

    private static native long getDefaultMMKV(int i, String str);

    private static native long getMMKVWithAshmemFD(String str, int i, int i2, String str2);

    private static native long getMMKVWithID(String str, int i, String str2, String str3);

    private static native long getMMKVWithIDAndSize(String str, int i, int i2, String str2);

    public static native boolean isFileValid(String str);

    private static native void jniInitialize(String str, int i);

    private static void mmkvLogImp(int i, String str, int i2, String str2, String str3) {
        MMKVHandler bVar = f11119f;
        if (bVar == null || !f11120g) {
            int i3 = C4913a.f11122a[f11116c[i].ordinal()];
            if (i3 == 1) {
                Log.d("MMKV", str3);
            } else if (i3 == 2) {
                Log.i("MMKV", str3);
            } else if (i3 == 3) {
                Log.w("MMKV", str3);
            } else if (i3 == 4) {
                Log.e("MMKV", str3);
            }
        } else {
            bVar.mo27094a(f11116c[i], str, i2, str2, str3);
        }
    }

    private static void onContentChangedByOuterProcess(String str) {
        MMKVContentChangeNotification aVar = f11121h;
        if (aVar != null) {
            aVar.mo27092a(str);
        }
    }

    public static native void onExit();

    private static int onMMKVCRCCheckFail(String str) {
        MMKVRecoverStrategic dVar = MMKVRecoverStrategic.OnErrorDiscard;
        MMKVHandler bVar = f11119f;
        if (bVar != null) {
            dVar = bVar.mo27095b(str);
        }
        MMKVLogLevel cVar = MMKVLogLevel.LevelInfo;
        m17082a(cVar, "Recover strategic for " + str + " is " + dVar);
        Integer num = f11114a.get(dVar);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    private static int onMMKVFileLengthError(String str) {
        MMKVRecoverStrategic dVar = MMKVRecoverStrategic.OnErrorDiscard;
        MMKVHandler bVar = f11119f;
        if (bVar != null) {
            dVar = bVar.mo27093a(str);
        }
        MMKVLogLevel cVar = MMKVLogLevel.LevelInfo;
        m17082a(cVar, "Recover strategic for " + str + " is " + dVar);
        Integer num = f11114a.get(dVar);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public static native int pageSize();

    private native void removeValueForKey(long j, String str);

    private static native void setLogLevel(int i);

    private static native void setLogReDirecting(boolean z);

    private static native void setWantsContentChangeNotify(boolean z);

    private native void sync(boolean z);

    private native long totalSize(long j);

    private native int valueSize(long j, String str, boolean z);

    private native int writeValueToNB(long j, String str, long j2, int i);

    public native String[] allKeys();

    public void apply() {
        sync(false);
    }

    public native int ashmemFD();

    public native int ashmemMetaFD();

    /* renamed from: b */
    public boolean mo27043b(String str, boolean z) {
        return encodeBool(this.nativeHandle, str, z);
    }

    /* renamed from: c */
    public String mo27044c(String str) {
        return decodeString(this.nativeHandle, str, null);
    }

    public native void checkContentChangedByOuterProcess();

    public native void checkReSetCryptKey(String str);

    public SharedPreferences.Editor clear() {
        clearAll();
        return this;
    }

    public native void clearAll();

    public native void clearMemoryCache();

    public native void close();

    public boolean commit() {
        sync(true);
        return true;
    }

    public boolean contains(String str) {
        return mo27031a(str);
    }

    public native String cryptKey();

    /* renamed from: d */
    public void mo27054d(String str) {
        removeValueForKey(this.nativeHandle, str);
    }

    public SharedPreferences.Editor edit() {
        return this;
    }

    public Map<String, ?> getAll() {
        throw new UnsupportedOperationException("use allKeys() instead, getAll() not implement because type-erasure inside mmkv");
    }

    public boolean getBoolean(String str, boolean z) {
        return decodeBool(this.nativeHandle, str, z);
    }

    public float getFloat(String str, float f) {
        return decodeFloat(this.nativeHandle, str, f);
    }

    public int getInt(String str, int i) {
        return decodeInt(this.nativeHandle, str, i);
    }

    public long getLong(String str, long j) {
        return decodeLong(this.nativeHandle, str, j);
    }

    @Nullable
    public String getString(String str, @Nullable String str2) {
        return decodeString(this.nativeHandle, str, str2);
    }

    @Nullable
    public Set<String> getStringSet(String str, @Nullable Set<String> set) {
        return mo27029a(str, set);
    }

    public native void lock();

    public native String mmapID();

    public SharedPreferences.Editor putBoolean(String str, boolean z) {
        encodeBool(this.nativeHandle, str, z);
        return this;
    }

    public SharedPreferences.Editor putFloat(String str, float f) {
        encodeFloat(this.nativeHandle, str, f);
        return this;
    }

    public SharedPreferences.Editor putInt(String str, int i) {
        encodeInt(this.nativeHandle, str, i);
        return this;
    }

    public SharedPreferences.Editor putLong(String str, long j) {
        encodeLong(this.nativeHandle, str, j);
        return this;
    }

    public SharedPreferences.Editor putString(String str, @Nullable String str2) {
        encodeString(this.nativeHandle, str, str2);
        return this;
    }

    public SharedPreferences.Editor putStringSet(String str, @Nullable Set<String> set) {
        mo27042b(str, set);
        return this;
    }

    public native boolean reKey(String str);

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    public SharedPreferences.Editor remove(String str) {
        mo27054d(str);
        return this;
    }

    public native void removeValuesForKeys(String[] strArr);

    public native void trim();

    public native boolean tryLock();

    public native void unlock();

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        throw new UnsupportedOperationException("Not implement in MMKV");
    }

    /* renamed from: b */
    public boolean mo27039b(String str, int i) {
        return encodeInt(this.nativeHandle, str, i);
    }

    /* renamed from: b */
    public int mo27038b(String str) {
        return decodeInt(this.nativeHandle, str, 0);
    }

    /* renamed from: a */
    public static String m17081a(String str, C4914b bVar, MMKVLogLevel cVar) {
        if (bVar != null) {
            bVar.mo27079a("mmkv");
        } else {
            System.loadLibrary("mmkv");
        }
        f11117d = str;
        jniInitialize(f11117d, m17076a(cVar));
        return str;
    }

    /* renamed from: b */
    public boolean mo27040b(String str, long j) {
        return encodeLong(this.nativeHandle, str, j);
    }

    /* renamed from: b */
    public boolean mo27041b(String str, String str2) {
        return encodeString(this.nativeHandle, str, str2);
    }

    /* renamed from: b */
    public boolean mo27042b(String str, Set<String> set) {
        return encodeSet(this.nativeHandle, str, (String[]) set.toArray(new String[0]));
    }

    /* renamed from: a */
    private static int m17076a(MMKVLogLevel cVar) {
        int i = C4913a.f11122a[cVar.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i != 2) {
            if (i == 3) {
                return 2;
            }
            if (i != 4) {
                return i != 5 ? 1 : 4;
            }
            return 3;
        }
    }

    @Nullable
    /* renamed from: a */
    public static MMKV m17078a(Context context, String str, int i, int i2, String str2) {
        if (f11117d != null) {
            String a = MMKVContentProvider.m17106a(context, Process.myPid());
            if (a == null || a.length() == 0) {
                m17082a(MMKVLogLevel.LevelError, "process name detect fail, try again later");
                return null;
            } else if (a.contains(":")) {
                Uri a2 = MMKVContentProvider.m17104a(context);
                if (a2 == null) {
                    m17082a(MMKVLogLevel.LevelError, "MMKVContentProvider has invalid authority");
                    return null;
                }
                MMKVLogLevel cVar = MMKVLogLevel.LevelInfo;
                m17082a(cVar, "getting parcelable mmkv in process, Uri = " + a2);
                Bundle bundle = new Bundle();
                bundle.putInt("KEY_SIZE", i);
                bundle.putInt("KEY_MODE", i2);
                if (str2 != null) {
                    bundle.putString("KEY_CRYPT", str2);
                }
                Bundle call = context.getContentResolver().call(a2, "mmkvFromAshmemID", str, bundle);
                if (call != null) {
                    call.setClassLoader(ParcelableMMKV.class.getClassLoader());
                    ParcelableMMKV parcelableMMKV = (ParcelableMMKV) call.getParcelable("KEY");
                    if (parcelableMMKV != null) {
                        MMKV a3 = parcelableMMKV.mo27087a();
                        if (a3 != null) {
                            MMKVLogLevel cVar2 = MMKVLogLevel.LevelInfo;
                            m17082a(cVar2, a3.mmapID() + " fd = " + a3.ashmemFD() + ", meta fd = " + a3.ashmemMetaFD());
                        }
                        return a3;
                    }
                }
                return null;
            } else {
                m17082a(MMKVLogLevel.LevelInfo, "getting mmkv in main process");
                return new MMKV(getMMKVWithIDAndSize(str, i, i2 | 8, str2));
            }
        } else {
            throw new IllegalStateException("You should Call MMKV.initialize() first.");
        }
    }

    /* renamed from: a */
    public static MMKV m17077a(int i, String str) {
        if (f11117d != null) {
            return new MMKV(getDefaultMMKV(i, str));
        }
        throw new IllegalStateException("You should Call MMKV.initialize() first.");
    }

    /* renamed from: a */
    public boolean mo27033a(String str, boolean z) {
        return decodeBool(this.nativeHandle, str, z);
    }

    /* renamed from: a */
    public int mo27024a(String str, int i) {
        return decodeInt(this.nativeHandle, str, i);
    }

    /* renamed from: a */
    public long mo27025a(String str, long j) {
        return decodeLong(this.nativeHandle, str, j);
    }

    /* renamed from: a */
    public String mo27028a(String str, String str2) {
        return decodeString(this.nativeHandle, str, str2);
    }

    /* renamed from: a */
    public Set<String> mo27029a(String str, Set<String> set) {
        return mo27030a(str, set, HashSet.class);
    }

    /* renamed from: a */
    public Set<String> mo27030a(String str, Set<String> set, Class<? extends Set> cls) {
        String[] decodeStringSet = decodeStringSet(this.nativeHandle, str);
        if (decodeStringSet == null) {
            return set;
        }
        try {
            Set<String> set2 = (Set) cls.newInstance();
            set2.addAll(Arrays.asList(decodeStringSet));
            return set2;
        } catch (IllegalAccessException | InstantiationException unused) {
            return set;
        }
    }

    /* renamed from: a */
    public boolean mo27032a(String str, Parcelable parcelable) {
        Parcel obtain = Parcel.obtain();
        parcelable.writeToParcel(obtain, parcelable.describeContents());
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        return encodeBytes(this.nativeHandle, str, marshall);
    }

    /* renamed from: a */
    public <T extends Parcelable> T mo27026a(String str, Class cls) {
        return mo27027a(str, cls, (Parcelable) null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: android.os.Parcelable$Creator} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T extends android.os.Parcelable> T mo27027a(java.lang.String r4, java.lang.Class r5, android.os.Parcelable r6) {
        /*
            r3 = this;
            if (r5 != 0) goto L_0x0003
            return r6
        L_0x0003:
            long r0 = r3.nativeHandle
            byte[] r4 = r3.decodeBytes(r0, r4)
            if (r4 != 0) goto L_0x000c
            return r6
        L_0x000c:
            android.os.Parcel r0 = android.os.Parcel.obtain()
            int r1 = r4.length
            r2 = 0
            r0.unmarshall(r4, r2, r1)
            r0.setDataPosition(r2)
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x0067 }
            java.util.HashMap<java.lang.String, android.os.Parcelable$Creator<?>> r1 = com.tencent.mmkv.MMKV.f11118e     // Catch:{ Exception -> 0x0067 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x0067 }
            java.util.HashMap<java.lang.String, android.os.Parcelable$Creator<?>> r2 = com.tencent.mmkv.MMKV.f11118e     // Catch:{ all -> 0x0062 }
            java.lang.Object r2 = r2.get(r4)     // Catch:{ all -> 0x0062 }
            android.os.Parcelable$Creator r2 = (android.os.Parcelable.Creator) r2     // Catch:{ all -> 0x0062 }
            if (r2 != 0) goto L_0x003e
            java.lang.String r2 = "CREATOR"
            java.lang.reflect.Field r5 = r5.getField(r2)     // Catch:{ all -> 0x0062 }
            r2 = 0
            java.lang.Object r5 = r5.get(r2)     // Catch:{ all -> 0x0062 }
            r2 = r5
            android.os.Parcelable$Creator r2 = (android.os.Parcelable.Creator) r2     // Catch:{ all -> 0x0062 }
            if (r2 == 0) goto L_0x003e
            java.util.HashMap<java.lang.String, android.os.Parcelable$Creator<?>> r5 = com.tencent.mmkv.MMKV.f11118e     // Catch:{ all -> 0x0062 }
            r5.put(r4, r2)     // Catch:{ all -> 0x0062 }
        L_0x003e:
            monitor-exit(r1)     // Catch:{ all -> 0x0062 }
            if (r2 == 0) goto L_0x004b
            java.lang.Object r4 = r2.createFromParcel(r0)     // Catch:{ Exception -> 0x0067 }
            android.os.Parcelable r4 = (android.os.Parcelable) r4     // Catch:{ Exception -> 0x0067 }
            r0.recycle()
            return r4
        L_0x004b:
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0067 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0067 }
            r1.<init>()     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = "Parcelable protocol requires a non-null static Parcelable.Creator object called CREATOR on class "
            r1.append(r2)     // Catch:{ Exception -> 0x0067 }
            r1.append(r4)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x0067 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0067 }
            throw r5     // Catch:{ Exception -> 0x0067 }
        L_0x0062:
            r4 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0062 }
            throw r4     // Catch:{ Exception -> 0x0067 }
        L_0x0065:
            r4 = move-exception
            goto L_0x0075
        L_0x0067:
            r4 = move-exception
            com.tencent.mmkv.c r5 = com.tencent.mmkv.MMKVLogLevel.LevelError     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0065 }
            m17082a(r5, r4)     // Catch:{ all -> 0x0065 }
            r0.recycle()
            return r6
        L_0x0075:
            r0.recycle()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mmkv.MMKV.mo27027a(java.lang.String, java.lang.Class, android.os.Parcelable):android.os.Parcelable");
    }

    /* renamed from: a */
    public boolean mo27031a(String str) {
        return containsKey(this.nativeHandle, str);
    }

    /* renamed from: a */
    public int mo27023a(SharedPreferences sharedPreferences) {
        Map<String, ?> all = sharedPreferences.getAll();
        if (all == null || all.size() <= 0) {
            return 0;
        }
        for (Map.Entry entry : all.entrySet()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (!(str == null || value == null)) {
                if (value instanceof Boolean) {
                    encodeBool(this.nativeHandle, str, ((Boolean) value).booleanValue());
                } else if (value instanceof Integer) {
                    encodeInt(this.nativeHandle, str, ((Integer) value).intValue());
                } else if (value instanceof Long) {
                    encodeLong(this.nativeHandle, str, ((Long) value).longValue());
                } else if (value instanceof Float) {
                    encodeFloat(this.nativeHandle, str, ((Float) value).floatValue());
                } else if (value instanceof Double) {
                    encodeDouble(this.nativeHandle, str, ((Double) value).doubleValue());
                } else if (value instanceof String) {
                    encodeString(this.nativeHandle, str, (String) value);
                } else if (value instanceof Set) {
                    mo27042b(str, (Set) value);
                } else {
                    MMKVLogLevel cVar = MMKVLogLevel.LevelError;
                    m17082a(cVar, "unknown type: " + value.getClass());
                }
            }
        }
        return all.size();
    }

    /* renamed from: a */
    public static MMKV m17079a(String str, int i, int i2, String str2) {
        return new MMKV(getMMKVWithAshmemFD(str, i, i2, str2));
    }

    /* renamed from: a */
    private static void m17082a(MMKVLogLevel cVar, String str) {
        int i;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StackTraceElement stackTraceElement = stackTrace[stackTrace.length - 1];
        Integer num = f11115b.get(cVar);
        if (num == null) {
            i = 0;
        } else {
            i = num.intValue();
        }
        mmkvLogImp(i, stackTraceElement.getFileName(), stackTraceElement.getLineNumber(), stackTraceElement.getMethodName(), str);
    }
}
