package com.github.mikephil.charting.data;

import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;

/* renamed from: com.github.mikephil.charting.data.j */
public class CombinedData extends BarLineScatterCandleBubbleData<IBarLineScatterCandleBubbleDataSet<? extends Entry>> {

    /* renamed from: j */
    private LineData f2526j;

    /* renamed from: k */
    private BarData f2527k;

    /* renamed from: l */
    private ScatterData f2528l;

    /* renamed from: m */
    private CandleData f2529m;

    /* renamed from: n */
    private BubbleData f2530n;

    /* renamed from: a */
    public void mo13376a() {
        if (this.f2525i == null) {
            this.f2525i = new ArrayList();
        }
        this.f2525i.clear();
        this.f2517a = -3.4028235E38f;
        this.f2518b = Float.MAX_VALUE;
        this.f2519c = -3.4028235E38f;
        this.f2520d = Float.MAX_VALUE;
        this.f2521e = -3.4028235E38f;
        this.f2522f = Float.MAX_VALUE;
        this.f2523g = -3.4028235E38f;
        this.f2524h = Float.MAX_VALUE;
        for (ChartData iVar : mo13396k()) {
            iVar.mo13376a();
            this.f2525i.addAll(iVar.mo13386c());
            if (iVar.mo13391h() > this.f2517a) {
                this.f2517a = iVar.mo13391h();
            }
            if (iVar.mo13392i() < this.f2518b) {
                this.f2518b = iVar.mo13392i();
            }
            if (iVar.mo13389f() > this.f2519c) {
                this.f2519c = iVar.mo13389f();
            }
            if (iVar.mo13390g() < this.f2520d) {
                this.f2520d = iVar.mo13390g();
            }
            float f = iVar.f2521e;
            if (f > this.f2521e) {
                this.f2521e = f;
            }
            float f2 = iVar.f2522f;
            if (f2 < this.f2522f) {
                this.f2522f = f2;
            }
            float f3 = iVar.f2523g;
            if (f3 > this.f2523g) {
                this.f2523g = f3;
            }
            float f4 = iVar.f2524h;
            if (f4 < this.f2524h) {
                this.f2524h = f4;
            }
        }
    }

    /* renamed from: b */
    public IBarLineScatterCandleBubbleDataSet<? extends Entry> mo13394b(Highlight dVar) {
        if (dVar.mo23244b() >= mo13396k().size()) {
            return null;
        }
        BarLineScatterCandleBubbleData c = mo13395c(dVar.mo23244b());
        if (dVar.mo23245c() >= c.mo13383b()) {
            return null;
        }
        return (IBarLineScatterCandleBubbleDataSet) c.mo13386c().get(dVar.mo23245c());
    }

    /* renamed from: c */
    public BarLineScatterCandleBubbleData mo13395c(int i) {
        return mo13396k().get(i);
    }

    /* renamed from: j */
    public void mo13393j() {
        LineData lVar = this.f2526j;
        if (lVar != null) {
            lVar.mo13393j();
        }
        BarData aVar = this.f2527k;
        if (aVar != null) {
            aVar.mo13393j();
        }
        CandleData hVar = this.f2529m;
        if (hVar != null) {
            hVar.mo13393j();
        }
        ScatterData qVar = this.f2528l;
        if (qVar != null) {
            qVar.mo13393j();
        }
        BubbleData gVar = this.f2530n;
        if (gVar != null) {
            gVar.mo13393j();
        }
        mo13376a();
    }

    /* renamed from: k */
    public List<BarLineScatterCandleBubbleData> mo13396k() {
        ArrayList arrayList = new ArrayList();
        LineData lVar = this.f2526j;
        if (lVar != null) {
            arrayList.add(lVar);
        }
        BarData aVar = this.f2527k;
        if (aVar != null) {
            arrayList.add(aVar);
        }
        ScatterData qVar = this.f2528l;
        if (qVar != null) {
            arrayList.add(qVar);
        }
        CandleData hVar = this.f2529m;
        if (hVar != null) {
            arrayList.add(hVar);
        }
        BubbleData gVar = this.f2530n;
        if (gVar != null) {
            arrayList.add(gVar);
        }
        return arrayList;
    }

    /* renamed from: l */
    public BarData mo13397l() {
        return this.f2527k;
    }

    /* renamed from: m */
    public BubbleData mo13398m() {
        return this.f2530n;
    }

    /* renamed from: n */
    public CandleData mo13399n() {
        return this.f2529m;
    }

    /* renamed from: o */
    public LineData mo13400o() {
        return this.f2526j;
    }

    /* renamed from: p */
    public ScatterData mo13401p() {
        return this.f2528l;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x003d  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.github.mikephil.charting.data.Entry mo13373a(p119e.p128c.p129a.p130a.p134d.Highlight r6) {
        /*
            r5 = this;
            int r0 = r6.mo23244b()
            java.util.List r1 = r5.mo13396k()
            int r1 = r1.size()
            r2 = 0
            if (r0 < r1) goto L_0x0010
            return r2
        L_0x0010:
            int r0 = r6.mo23244b()
            com.github.mikephil.charting.data.c r0 = r5.mo13395c(r0)
            int r1 = r6.mo23245c()
            int r3 = r0.mo13383b()
            if (r1 < r3) goto L_0x0023
            return r2
        L_0x0023:
            int r1 = r6.mo23245c()
            e.c.a.a.e.b.e r0 = r0.mo13374a(r1)
            float r1 = r6.mo23249g()
            java.util.List r0 = r0.mo13411b(r1)
            java.util.Iterator r0 = r0.iterator()
        L_0x0037:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005a
            java.lang.Object r1 = r0.next()
            com.github.mikephil.charting.data.Entry r1 = (com.github.mikephil.charting.data.Entry) r1
            float r3 = r1.mo13303c()
            float r4 = r6.mo23251i()
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 == 0) goto L_0x0059
            float r3 = r6.mo23251i()
            boolean r3 = java.lang.Float.isNaN(r3)
            if (r3 == 0) goto L_0x0037
        L_0x0059:
            return r1
        L_0x005a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.mikephil.charting.data.CombinedData.mo13373a(e.c.a.a.d.d):com.github.mikephil.charting.data.Entry");
    }
}
