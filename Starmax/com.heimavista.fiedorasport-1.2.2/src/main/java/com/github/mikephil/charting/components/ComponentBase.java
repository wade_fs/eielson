package com.github.mikephil.charting.components;

import android.graphics.Typeface;
import androidx.core.view.ViewCompat;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.components.b */
public abstract class ComponentBase {

    /* renamed from: a */
    protected boolean f2372a = true;

    /* renamed from: b */
    protected float f2373b = 5.0f;

    /* renamed from: c */
    protected float f2374c = 5.0f;

    /* renamed from: d */
    protected Typeface f2375d = null;

    /* renamed from: e */
    protected float f2376e = Utils.m11599a(10.0f);

    /* renamed from: f */
    protected int f2377f = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: a */
    public void mo13232a(Typeface typeface) {
        this.f2375d = typeface;
    }

    /* renamed from: b */
    public void mo13235b(float f) {
        this.f2373b = Utils.m11599a(f);
    }

    /* renamed from: c */
    public void mo13237c(float f) {
        this.f2374c = Utils.m11599a(f);
    }

    /* renamed from: d */
    public float mo13238d() {
        return this.f2373b;
    }

    /* renamed from: e */
    public float mo13239e() {
        return this.f2374c;
    }

    /* renamed from: f */
    public boolean mo13240f() {
        return this.f2372a;
    }

    /* renamed from: a */
    public void mo13230a(float f) {
        if (f > 24.0f) {
            f = 24.0f;
        }
        if (f < 6.0f) {
            f = 6.0f;
        }
        this.f2376e = Utils.m11599a(f);
    }

    /* renamed from: b */
    public float mo13234b() {
        return this.f2376e;
    }

    /* renamed from: c */
    public Typeface mo13236c() {
        return this.f2375d;
    }

    /* renamed from: a */
    public void mo13231a(int i) {
        this.f2377f = i;
    }

    /* renamed from: a */
    public int mo13229a() {
        return this.f2377f;
    }

    /* renamed from: a */
    public void mo13233a(boolean z) {
        this.f2372a = z;
    }
}
