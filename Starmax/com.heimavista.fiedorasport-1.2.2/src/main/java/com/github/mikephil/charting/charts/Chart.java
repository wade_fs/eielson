package com.github.mikephil.charting.charts;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.annotation.RequiresApi;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import java.util.ArrayList;
import java.util.Iterator;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p131a.Easing;
import p119e.p128c.p129a.p130a.p133c.DefaultValueFormatter;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.ChartHighlighter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p134d.IHighlighter;
import p119e.p128c.p129a.p130a.p135e.p136a.ChartInterface;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;
import p119e.p128c.p129a.p130a.p139g.OnChartGestureListener;
import p119e.p128c.p129a.p130a.p139g.OnChartValueSelectedListener;
import p119e.p128c.p129a.p130a.p141i.DataRenderer;
import p119e.p128c.p129a.p130a.p141i.LegendRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

public abstract class Chart<T extends ChartData<? extends IDataSet<? extends Entry>>> extends ViewGroup implements ChartInterface {

    /* renamed from: P */
    protected boolean f2263P = false;

    /* renamed from: Q */
    protected T f2264Q = null;

    /* renamed from: R */
    protected boolean f2265R = true;

    /* renamed from: S */
    private boolean f2266S = true;

    /* renamed from: T */
    private float f2267T = 0.9f;

    /* renamed from: U */
    protected DefaultValueFormatter f2268U = new DefaultValueFormatter(0);

    /* renamed from: V */
    protected Paint f2269V;

    /* renamed from: W */
    protected Paint f2270W;

    /* renamed from: a0 */
    protected XAxis f2271a0;

    /* renamed from: b0 */
    protected boolean f2272b0 = true;

    /* renamed from: c0 */
    protected Description f2273c0;

    /* renamed from: d0 */
    protected Legend f2274d0;

    /* renamed from: e0 */
    protected OnChartValueSelectedListener f2275e0;

    /* renamed from: f0 */
    protected ChartTouchListener f2276f0;

    /* renamed from: g0 */
    private String f2277g0 = "No chart data available.";

    /* renamed from: h0 */
    private OnChartGestureListener f2278h0;

    /* renamed from: i0 */
    protected LegendRenderer f2279i0;

    /* renamed from: j0 */
    protected DataRenderer f2280j0;

    /* renamed from: k0 */
    protected IHighlighter f2281k0;

    /* renamed from: l0 */
    protected ViewPortHandler f2282l0 = new ViewPortHandler();

    /* renamed from: m0 */
    protected ChartAnimator f2283m0;

    /* renamed from: n0 */
    private float f2284n0 = 0.0f;

    /* renamed from: o0 */
    private float f2285o0 = 0.0f;

    /* renamed from: p0 */
    private float f2286p0 = 0.0f;

    /* renamed from: q0 */
    private float f2287q0 = 0.0f;

    /* renamed from: r0 */
    private boolean f2288r0 = false;

    /* renamed from: s0 */
    protected Highlight[] f2289s0;

    /* renamed from: t0 */
    protected float f2290t0 = 0.0f;

    /* renamed from: u0 */
    protected boolean f2291u0 = true;

    /* renamed from: v0 */
    protected IMarker f2292v0;

    /* renamed from: w0 */
    protected ArrayList<Runnable> f2293w0 = new ArrayList<>();

    /* renamed from: x0 */
    private boolean f2294x0 = false;

    /* renamed from: com.github.mikephil.charting.charts.Chart$a */
    class C1644a implements ValueAnimator.AnimatorUpdateListener {
        C1644a() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            Chart.this.postInvalidate();
        }
    }

    public Chart(Context context) {
        super(context);
        mo12942h();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13025a(Canvas canvas) {
        float f;
        float f2;
        Description cVar = this.f2273c0;
        if (cVar != null && cVar.mo13240f()) {
            MPPointF g = this.f2273c0.mo13241g();
            this.f2269V.setTypeface(this.f2273c0.mo13236c());
            this.f2269V.setTextSize(this.f2273c0.mo13234b());
            this.f2269V.setColor(this.f2273c0.mo13229a());
            this.f2269V.setTextAlign(this.f2273c0.mo13243i());
            if (g == null) {
                f2 = (((float) getWidth()) - this.f2282l0.mo23488z()) - this.f2273c0.mo13238d();
                f = (((float) getHeight()) - this.f2282l0.mo23486x()) - this.f2273c0.mo13239e();
            } else {
                float f3 = g.f7123R;
                f = g.f7124S;
                f2 = f3;
            }
            canvas.drawText(this.f2273c0.mo13242h(), f2, f, this.f2269V);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13030b(float f, float f2) {
        float f3;
        T t = this.f2264Q;
        if (t == null || t.mo13387d() < 2) {
            f3 = Math.max(Math.abs(f), Math.abs(f2));
        } else {
            f3 = Math.abs(f2 - f);
        }
        this.f2268U.mo23218a(Utils.m11615b(f3));
    }

    /* renamed from: c */
    public Paint mo13033c(int i) {
        if (i == 7) {
            return this.f2270W;
        }
        if (i != 11) {
            return null;
        }
        return this.f2269V;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract void mo12961d();

    /* renamed from: e */
    public void mo13034e() {
        this.f2264Q = null;
        this.f2288r0 = false;
        this.f2289s0 = null;
        this.f2276f0.mo23311a((Highlight) null);
        invalidate();
    }

    /* renamed from: f */
    public void mo13035f() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    /* renamed from: g */
    public void mo13036g() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
    }

    public ChartAnimator getAnimator() {
        return this.f2283m0;
    }

    public MPPointF getCenter() {
        return MPPointF.m11567a(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    public MPPointF getCenterOfView() {
        return getCenter();
    }

    public MPPointF getCenterOffsets() {
        return this.f2282l0.mo23475m();
    }

    public Bitmap getChartBitmap() {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        Drawable background = getBackground();
        if (background != null) {
            background.draw(canvas);
        } else {
            canvas.drawColor(-1);
        }
        draw(canvas);
        return createBitmap;
    }

    public RectF getContentRect() {
        return this.f2282l0.mo23476n();
    }

    public T getData() {
        return this.f2264Q;
    }

    public ValueFormatter getDefaultValueFormatter() {
        return this.f2268U;
    }

    public Description getDescription() {
        return this.f2273c0;
    }

    public float getDragDecelerationFrictionCoef() {
        return this.f2267T;
    }

    public float getExtraBottomOffset() {
        return this.f2286p0;
    }

    public float getExtraLeftOffset() {
        return this.f2287q0;
    }

    public float getExtraRightOffset() {
        return this.f2285o0;
    }

    public float getExtraTopOffset() {
        return this.f2284n0;
    }

    public Highlight[] getHighlighted() {
        return this.f2289s0;
    }

    public IHighlighter getHighlighter() {
        return this.f2281k0;
    }

    public ArrayList<Runnable> getJobs() {
        return this.f2293w0;
    }

    public Legend getLegend() {
        return this.f2274d0;
    }

    public LegendRenderer getLegendRenderer() {
        return this.f2279i0;
    }

    public IMarker getMarker() {
        return this.f2292v0;
    }

    @Deprecated
    public IMarker getMarkerView() {
        return getMarker();
    }

    public float getMaxHighlightDistance() {
        return this.f2290t0;
    }

    public OnChartGestureListener getOnChartGestureListener() {
        return this.f2278h0;
    }

    public ChartTouchListener getOnTouchListener() {
        return this.f2276f0;
    }

    public DataRenderer getRenderer() {
        return this.f2280j0;
    }

    public ViewPortHandler getViewPortHandler() {
        return this.f2282l0;
    }

    public XAxis getXAxis() {
        return this.f2271a0;
    }

    public float getXChartMax() {
        return this.f2271a0.f2349G;
    }

    public float getXChartMin() {
        return this.f2271a0.f2350H;
    }

    public float getXRange() {
        return this.f2271a0.f2351I;
    }

    public float getYMax() {
        return this.f2264Q.mo13391h();
    }

    public float getYMin() {
        return this.f2264Q.mo13392i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        setWillNotDraw(false);
        this.f2283m0 = new ChartAnimator(new C1644a());
        Utils.m11605a(getContext());
        this.f2290t0 = Utils.m11599a(500.0f);
        this.f2273c0 = new Description();
        this.f2274d0 = new Legend();
        this.f2279i0 = new LegendRenderer(this.f2282l0, this.f2274d0);
        this.f2271a0 = new XAxis();
        this.f2269V = new Paint(1);
        this.f2270W = new Paint(1);
        this.f2270W.setColor(Color.rgb(247, (int) PsExtractor.PRIVATE_STREAM_1, 51));
        this.f2270W.setTextAlign(Paint.Align.CENTER);
        this.f2270W.setTextSize(Utils.m11599a(12.0f));
        if (this.f2263P) {
            Log.i("", "Chart.init()");
        }
    }

    /* renamed from: i */
    public boolean mo13068i() {
        return this.f2266S;
    }

    /* renamed from: j */
    public boolean mo13069j() {
        return this.f2291u0;
    }

    /* renamed from: k */
    public boolean mo13070k() {
        return this.f2265R;
    }

    /* renamed from: l */
    public boolean mo13071l() {
        return this.f2263P;
    }

    /* renamed from: m */
    public abstract void mo12978m();

    /* renamed from: n */
    public boolean mo13072n() {
        Highlight[] dVarArr = this.f2289s0;
        return (dVarArr == null || dVarArr.length <= 0 || dVarArr[0] == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f2294x0) {
            m3817a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.f2264Q == null) {
            if (!TextUtils.isEmpty(this.f2277g0)) {
                MPPointF center = getCenter();
                canvas.drawText(this.f2277g0, center.f7123R, center.f7124S, this.f2270W);
            }
        } else if (!this.f2288r0) {
            mo12961d();
            this.f2288r0 = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        for (int i5 = 0; i5 < getChildCount(); i5++) {
            getChildAt(i5).layout(i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int a = (int) Utils.m11599a(50.0f);
        setMeasuredDimension(Math.max(getSuggestedMinimumWidth(), ViewGroup.resolveSize(a, i)), Math.max(getSuggestedMinimumHeight(), ViewGroup.resolveSize(a, i2)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        if (this.f2263P) {
            Log.i("MPAndroidChart", "OnSizeChanged()");
        }
        if (i > 0 && i2 > 0 && i < 10000 && i2 < 10000) {
            if (this.f2263P) {
                Log.i("MPAndroidChart", "Setting chart dimens, width: " + i + ", height: " + i2);
            }
            this.f2282l0.mo23452b((float) i, (float) i2);
        } else if (this.f2263P) {
            Log.w("MPAndroidChart", "*Avoiding* setting chart dimens! width: " + i + ", height: " + i2);
        }
        mo12978m();
        Iterator<Runnable> it = this.f2293w0.iterator();
        while (it.hasNext()) {
            post(it.next());
        }
        this.f2293w0.clear();
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setData(T t) {
        this.f2264Q = t;
        this.f2288r0 = false;
        if (t != null) {
            mo13030b(t.mo13392i(), t.mo13391h());
            for (IDataSet eVar : this.f2264Q.mo13386c()) {
                if (eVar.mo13360n() || eVar.mo13356j() == this.f2268U) {
                    eVar.mo13339a(this.f2268U);
                }
            }
            mo12978m();
            if (this.f2263P) {
                Log.i("MPAndroidChart", "Data is set.");
            }
        }
    }

    public void setDescription(Description cVar) {
        this.f2273c0 = cVar;
    }

    public void setDragDecelerationEnabled(boolean z) {
        this.f2266S = z;
    }

    public void setDragDecelerationFrictionCoef(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        }
        if (f >= 1.0f) {
            f = 0.999f;
        }
        this.f2267T = f;
    }

    @Deprecated
    public void setDrawMarkerViews(boolean z) {
        setDrawMarkers(z);
    }

    public void setDrawMarkers(boolean z) {
        this.f2291u0 = z;
    }

    public void setExtraBottomOffset(float f) {
        this.f2286p0 = Utils.m11599a(f);
    }

    public void setExtraLeftOffset(float f) {
        this.f2287q0 = Utils.m11599a(f);
    }

    public void setExtraRightOffset(float f) {
        this.f2285o0 = Utils.m11599a(f);
    }

    public void setExtraTopOffset(float f) {
        this.f2284n0 = Utils.m11599a(f);
    }

    public void setHardwareAccelerationEnabled(boolean z) {
        if (z) {
            setLayerType(2, null);
        } else {
            setLayerType(1, null);
        }
    }

    public void setHighlightPerTapEnabled(boolean z) {
        this.f2265R = z;
    }

    public void setHighlighter(ChartHighlighter bVar) {
        this.f2281k0 = bVar;
    }

    /* access modifiers changed from: protected */
    public void setLastHighlighted(Highlight[] dVarArr) {
        if (dVarArr == null || dVarArr.length <= 0 || dVarArr[0] == null) {
            this.f2276f0.mo23311a((Highlight) null);
        } else {
            this.f2276f0.mo23311a(dVarArr[0]);
        }
    }

    public void setLogEnabled(boolean z) {
        this.f2263P = z;
    }

    public void setMarker(IMarker dVar) {
        this.f2292v0 = dVar;
    }

    @Deprecated
    public void setMarkerView(IMarker dVar) {
        setMarker(dVar);
    }

    public void setMaxHighlightDistance(float f) {
        this.f2290t0 = Utils.m11599a(f);
    }

    public void setNoDataText(String str) {
        this.f2277g0 = str;
    }

    public void setNoDataTextColor(int i) {
        this.f2270W.setColor(i);
    }

    public void setNoDataTextTypeface(Typeface typeface) {
        this.f2270W.setTypeface(typeface);
    }

    public void setOnChartGestureListener(OnChartGestureListener cVar) {
        this.f2278h0 = cVar;
    }

    public void setOnChartValueSelectedListener(OnChartValueSelectedListener dVar) {
        this.f2275e0 = dVar;
    }

    public void setOnTouchListener(ChartTouchListener bVar) {
        this.f2276f0 = bVar;
    }

    public void setRenderer(DataRenderer gVar) {
        if (gVar != null) {
            this.f2280j0 = gVar;
        }
    }

    public void setTouchEnabled(boolean z) {
        this.f2272b0 = z;
    }

    public void setUnbindEnabled(boolean z) {
        this.f2294x0 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13032b(Canvas canvas) {
        if (this.f2292v0 != null && mo13069j() && mo13072n()) {
            int i = 0;
            while (true) {
                Highlight[] dVarArr = this.f2289s0;
                if (i < dVarArr.length) {
                    Highlight dVar = dVarArr[i];
                    IDataSet a = this.f2264Q.mo13374a(dVar.mo23245c());
                    Entry a2 = this.f2264Q.mo13373a(this.f2289s0[i]);
                    int a3 = a.mo13405a(a2);
                    if (a2 != null && ((float) a3) <= ((float) a.mo13417t()) * this.f2283m0.mo23201a()) {
                        float[] a4 = mo13029a(dVar);
                        if (this.f2282l0.mo23451a(a4[0], a4[1])) {
                            this.f2292v0.mo13194a(a2, dVar);
                            this.f2292v0.mo13193a(canvas, a4[0], a4[1]);
                        }
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: a */
    public void mo13028a(Highlight[] dVarArr) {
        this.f2289s0 = dVarArr;
        setLastHighlighted(dVarArr);
        invalidate();
    }

    /* renamed from: a */
    public void mo13026a(Highlight dVar, boolean z) {
        Entry entry;
        if (dVar == null) {
            this.f2289s0 = null;
            entry = null;
        } else {
            if (this.f2263P) {
                Log.i("MPAndroidChart", "Highlighted: " + dVar.toString());
            }
            entry = this.f2264Q.mo13373a(dVar);
            if (entry == null) {
                this.f2289s0 = null;
                dVar = null;
            } else {
                this.f2289s0 = new Highlight[]{dVar};
            }
        }
        setLastHighlighted(this.f2289s0);
        if (z && this.f2275e0 != null) {
            if (!mo13072n()) {
                this.f2275e0.mo23322a();
            } else {
                this.f2275e0.mo23323a(entry, dVar);
            }
        }
        invalidate();
    }

    @RequiresApi(11)
    /* renamed from: b */
    public void mo13031b(int i) {
        this.f2283m0.mo23205b(i);
    }

    public Chart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo12942h();
    }

    /* renamed from: a */
    public Highlight mo12937a(float f, float f2) {
        if (this.f2264Q != null) {
            return getHighlighter().mo23231a(f, f2);
        }
        Log.e("MPAndroidChart", "Can't select by touch. No data set.");
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float[] mo13029a(Highlight dVar) {
        return new float[]{dVar.mo23246d(), dVar.mo23247e()};
    }

    @RequiresApi(11)
    /* renamed from: a */
    public void mo13024a(int i, Easing.C3843c0 c0Var) {
        this.f2283m0.mo23206b(i, c0Var);
    }

    @RequiresApi(11)
    /* renamed from: a */
    public void mo13023a(int i) {
        this.f2283m0.mo23202a(i);
    }

    /* renamed from: a */
    public void mo13022a(float f, float f2, float f3, float f4) {
        setExtraLeftOffset(f);
        setExtraTopOffset(f2);
        setExtraRightOffset(f3);
        setExtraBottomOffset(f4);
    }

    public Chart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo12942h();
    }

    /* renamed from: a */
    public void mo13027a(Runnable runnable) {
        if (this.f2282l0.mo23481s()) {
            post(runnable);
        } else {
            this.f2293w0.add(runnable);
        }
    }

    /* renamed from: a */
    private void m3817a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            int i = 0;
            while (true) {
                ViewGroup viewGroup = (ViewGroup) view;
                if (i < super.getChildCount()) {
                    m3817a(super.getChildAt(i));
                    i++;
                } else {
                    super.removeAllViews();
                    return;
                }
            }
        }
    }
}
