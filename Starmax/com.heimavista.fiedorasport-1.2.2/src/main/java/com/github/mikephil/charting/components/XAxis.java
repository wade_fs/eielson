package com.github.mikephil.charting.components;

import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.components.h */
public class XAxis extends AxisBase {

    /* renamed from: J */
    public int f2445J;

    /* renamed from: K */
    public int f2446K;

    /* renamed from: L */
    public int f2447L = 1;

    /* renamed from: M */
    public int f2448M = 1;

    /* renamed from: N */
    protected float f2449N = 0.0f;

    /* renamed from: O */
    private boolean f2450O = false;

    /* renamed from: P */
    private C1654a f2451P = C1654a.TOP;

    /* renamed from: com.github.mikephil.charting.components.h$a */
    /* compiled from: XAxis */
    public enum C1654a {
        TOP,
        BOTTOM,
        BOTH_SIDED,
        TOP_INSIDE,
        BOTTOM_INSIDE
    }

    public XAxis() {
        this.f2374c = Utils.m11599a(4.0f);
    }

    /* renamed from: A */
    public C1654a mo13282A() {
        return this.f2451P;
    }

    /* renamed from: B */
    public boolean mo13283B() {
        return this.f2450O;
    }

    /* renamed from: a */
    public void mo13284a(C1654a aVar) {
        this.f2451P = aVar;
    }

    /* renamed from: z */
    public float mo13285z() {
        return this.f2449N;
    }
}
