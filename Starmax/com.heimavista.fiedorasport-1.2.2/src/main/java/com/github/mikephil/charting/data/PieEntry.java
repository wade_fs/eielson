package com.github.mikephil.charting.data;

import android.annotation.SuppressLint;
import android.util.Log;

@SuppressLint({"ParcelCreator"})
public class PieEntry extends Entry {

    /* renamed from: T */
    private String f2486T;

    public PieEntry(float f, String str) {
        super(0.0f, f);
        this.f2486T = str;
    }

    @Deprecated
    /* renamed from: d */
    public float mo13315d() {
        Log.i("DEPRECATED", "Pie entries do not have x values");
        return super.mo13315d();
    }

    /* renamed from: e */
    public String mo13321e() {
        return this.f2486T;
    }
}
