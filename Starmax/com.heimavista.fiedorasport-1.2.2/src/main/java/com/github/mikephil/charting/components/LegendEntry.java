package com.github.mikephil.charting.components;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.components.Legend;

/* renamed from: com.github.mikephil.charting.components.f */
public class LegendEntry {

    /* renamed from: a */
    public String f2427a;

    /* renamed from: b */
    public Legend.C1649c f2428b = Legend.C1649c.f2411R;

    /* renamed from: c */
    public float f2429c = Float.NaN;

    /* renamed from: d */
    public float f2430d = Float.NaN;

    /* renamed from: e */
    public DashPathEffect f2431e = null;

    /* renamed from: f */
    public int f2432f = 1122867;

    public LegendEntry() {
    }

    public LegendEntry(String str, Legend.C1649c cVar, float f, float f2, DashPathEffect dashPathEffect, int i) {
        this.f2427a = str;
        this.f2428b = cVar;
        this.f2429c = f;
        this.f2430d = f2;
        this.f2431e = dashPathEffect;
        this.f2432f = i;
    }
}
