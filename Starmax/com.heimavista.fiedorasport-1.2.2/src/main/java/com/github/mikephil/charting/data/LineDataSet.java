package com.github.mikephil.charting.data;

/* renamed from: com.github.mikephil.charting.data.m */
public enum LineDataSet {
    LINEAR,
    STEPPED,
    CUBIC_BEZIER,
    HORIZONTAL_BEZIER
}
