package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;
import p119e.p128c.p129a.p130a.p139g.PieRadarChartTouchListener;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

public abstract class PieRadarChartBase<T extends ChartData<? extends IDataSet<? extends Entry>>> extends Chart<T> {

    /* renamed from: A0 */
    protected boolean f2323A0 = true;

    /* renamed from: B0 */
    protected float f2324B0 = 0.0f;

    /* renamed from: y0 */
    private float f2325y0 = 270.0f;

    /* renamed from: z0 */
    private float f2326z0 = 270.0f;

    /* renamed from: com.github.mikephil.charting.charts.PieRadarChartBase$a */
    static /* synthetic */ class C1646a {

        /* renamed from: a */
        static final /* synthetic */ int[] f2327a = new int[Legend.C1652f.values().length];

        /* renamed from: b */
        static final /* synthetic */ int[] f2328b = new int[Legend.C1650d.values().length];

        /* renamed from: c */
        static final /* synthetic */ int[] f2329c = new int[Legend.C1651e.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|(3:19|20|22)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x005a */
        static {
            /*
                com.github.mikephil.charting.components.e$e[] r0 = com.github.mikephil.charting.components.Legend.C1651e.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2329c = r0
                r0 = 1
                int[] r1 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2329c     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.components.e$e r2 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2329c     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.components.e$e r3 = com.github.mikephil.charting.components.Legend.C1651e.HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.github.mikephil.charting.components.e$d[] r2 = com.github.mikephil.charting.components.Legend.C1650d.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2328b = r2
                int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2328b     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.LEFT     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2328b     // Catch:{ NoSuchFieldError -> 0x003c }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.RIGHT     // Catch:{ NoSuchFieldError -> 0x003c }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2328b     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.CENTER     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r4 = 3
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                com.github.mikephil.charting.components.e$f[] r2 = com.github.mikephil.charting.components.Legend.C1652f.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2327a = r2
                int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2327a     // Catch:{ NoSuchFieldError -> 0x005a }
                com.github.mikephil.charting.components.e$f r3 = com.github.mikephil.charting.components.Legend.C1652f.TOP     // Catch:{ NoSuchFieldError -> 0x005a }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x005a }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x005a }
            L_0x005a:
                int[] r0 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2327a     // Catch:{ NoSuchFieldError -> 0x0064 }
                com.github.mikephil.charting.components.e$f r2 = com.github.mikephil.charting.components.Legend.C1652f.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0064 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0064 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0064 }
            L_0x0064:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.<clinit>():void");
        }
    }

    public PieRadarChartBase(Context context) {
        super(context);
    }

    /* renamed from: a */
    public abstract int mo13113a(float f);

    /* renamed from: a */
    public MPPointF mo13159a(MPPointF eVar, float f, float f2) {
        MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
        mo13160a(eVar, f, f2, a);
        return a;
    }

    /* renamed from: c */
    public float mo13161c(float f, float f2) {
        MPPointF centerOffsets = getCenterOffsets();
        float f3 = centerOffsets.f7123R;
        float f4 = f > f3 ? f - f3 : f3 - f;
        float f5 = centerOffsets.f7124S;
        float sqrt = (float) Math.sqrt(Math.pow((double) f4, 2.0d) + Math.pow((double) (f2 > f5 ? f2 - f5 : f5 - f2), 2.0d));
        MPPointF.m11570b(centerOffsets);
        return sqrt;
    }

    public void computeScroll() {
        ChartTouchListener bVar = super.f2276f0;
        if (bVar instanceof PieRadarChartTouchListener) {
            ((PieRadarChartTouchListener) bVar).mo23324a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x007d, code lost:
        if (r2 != 2) goto L_0x007f;
     */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo12961d() {
        /*
            r11 = this;
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            r1 = 0
            if (r0 == 0) goto L_0x0194
            boolean r0 = r0.mo13240f()
            if (r0 == 0) goto L_0x0194
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            boolean r0 = r0.mo13273y()
            if (r0 != 0) goto L_0x0194
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            float r0 = r0.f2402x
            e.c.a.a.j.j r2 = r11.f2282l0
            float r2 = r2.mo23473l()
            com.github.mikephil.charting.components.e r3 = r11.f2274d0
            float r3 = r3.mo13267s()
            float r2 = r2 * r3
            float r0 = java.lang.Math.min(r0, r2)
            int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2329c
            com.github.mikephil.charting.components.e r3 = r11.f2274d0
            com.github.mikephil.charting.components.e$e r3 = r3.mo13268t()
            int r3 = r3.ordinal()
            r2 = r2[r3]
            r3 = 2
            r4 = 1
            if (r2 == r4) goto L_0x008c
            if (r2 == r3) goto L_0x003e
            goto L_0x007f
        L_0x003e:
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            com.github.mikephil.charting.components.e$f r0 = r0.mo13270v()
            com.github.mikephil.charting.components.e$f r2 = com.github.mikephil.charting.components.Legend.C1652f.TOP
            if (r0 == r2) goto L_0x0052
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            com.github.mikephil.charting.components.e$f r0 = r0.mo13270v()
            com.github.mikephil.charting.components.e$f r2 = com.github.mikephil.charting.components.Legend.C1652f.BOTTOM
            if (r0 != r2) goto L_0x007f
        L_0x0052:
            float r0 = r11.getRequiredLegendOffset()
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            float r2 = r2.f2403y
            float r2 = r2 + r0
            e.c.a.a.j.j r0 = r11.f2282l0
            float r0 = r0.mo23471k()
            com.github.mikephil.charting.components.e r5 = r11.f2274d0
            float r5 = r5.mo13267s()
            float r0 = r0 * r5
            float r0 = java.lang.Math.min(r2, r0)
            int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2327a
            com.github.mikephil.charting.components.e r5 = r11.f2274d0
            com.github.mikephil.charting.components.e$f r5 = r5.mo13270v()
            int r5 = r5.ordinal()
            r2 = r2[r5]
            if (r2 == r4) goto L_0x0087
            if (r2 == r3) goto L_0x0084
        L_0x007f:
            r0 = 0
        L_0x0080:
            r2 = 0
        L_0x0081:
            r3 = 0
            goto L_0x017f
        L_0x0084:
            r2 = r0
            r0 = 0
            goto L_0x0081
        L_0x0087:
            r3 = r0
            r0 = 0
            r2 = 0
            goto L_0x017f
        L_0x008c:
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            com.github.mikephil.charting.components.e$d r2 = r2.mo13266r()
            com.github.mikephil.charting.components.e$d r5 = com.github.mikephil.charting.components.Legend.C1650d.LEFT
            if (r2 == r5) goto L_0x00a4
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            com.github.mikephil.charting.components.e$d r2 = r2.mo13266r()
            com.github.mikephil.charting.components.e$d r5 = com.github.mikephil.charting.components.Legend.C1650d.RIGHT
            if (r2 != r5) goto L_0x00a1
            goto L_0x00a4
        L_0x00a1:
            r0 = 0
            goto L_0x0122
        L_0x00a4:
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            com.github.mikephil.charting.components.e$f r2 = r2.mo13270v()
            com.github.mikephil.charting.components.e$f r5 = com.github.mikephil.charting.components.Legend.C1652f.CENTER
            if (r2 != r5) goto L_0x00b6
            r2 = 1095761920(0x41500000, float:13.0)
            float r2 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r2)
            float r0 = r0 + r2
            goto L_0x0122
        L_0x00b6:
            r2 = 1090519040(0x41000000, float:8.0)
            float r2 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r2)
            float r0 = r0 + r2
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            float r5 = r2.f2403y
            float r2 = r2.f2404z
            float r5 = r5 + r2
            e.c.a.a.j.e r2 = r11.getCenter()
            com.github.mikephil.charting.components.e r6 = r11.f2274d0
            com.github.mikephil.charting.components.e$d r6 = r6.mo13266r()
            com.github.mikephil.charting.components.e$d r7 = com.github.mikephil.charting.components.Legend.C1650d.RIGHT
            r8 = 1097859072(0x41700000, float:15.0)
            if (r6 != r7) goto L_0x00dc
            int r6 = r11.getWidth()
            float r6 = (float) r6
            float r6 = r6 - r0
            float r6 = r6 + r8
            goto L_0x00de
        L_0x00dc:
            float r6 = r0 - r8
        L_0x00de:
            float r5 = r5 + r8
            float r7 = r11.mo13161c(r6, r5)
            float r8 = r11.getRadius()
            float r6 = r11.mo13163d(r6, r5)
            e.c.a.a.j.e r6 = r11.mo13159a(r2, r8, r6)
            float r8 = r6.f7123R
            float r9 = r6.f7124S
            float r8 = r11.mo13161c(r8, r9)
            r9 = 1084227584(0x40a00000, float:5.0)
            float r9 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r9)
            float r10 = r2.f7124S
            int r5 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r5 < 0) goto L_0x0113
            int r5 = r11.getHeight()
            float r5 = (float) r5
            float r5 = r5 - r0
            int r10 = r11.getWidth()
            float r10 = (float) r10
            int r5 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r5 <= 0) goto L_0x0113
            goto L_0x011c
        L_0x0113:
            int r0 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r0 >= 0) goto L_0x011b
            float r8 = r8 - r7
            float r9 = r9 + r8
            r0 = r9
            goto L_0x011c
        L_0x011b:
            r0 = 0
        L_0x011c:
            p119e.p128c.p129a.p130a.p143j.MPPointF.m11570b(r2)
            p119e.p128c.p129a.p130a.p143j.MPPointF.m11570b(r6)
        L_0x0122:
            int[] r2 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2328b
            com.github.mikephil.charting.components.e r5 = r11.f2274d0
            com.github.mikephil.charting.components.e$d r5 = r5.mo13266r()
            int r5 = r5.ordinal()
            r2 = r2[r5]
            if (r2 == r4) goto L_0x017c
            if (r2 == r3) goto L_0x0080
            r0 = 3
            if (r2 == r0) goto L_0x0138
            goto L_0x014a
        L_0x0138:
            int[] r0 = com.github.mikephil.charting.charts.PieRadarChartBase.C1646a.f2327a
            com.github.mikephil.charting.components.e r2 = r11.f2274d0
            com.github.mikephil.charting.components.e$f r2 = r2.mo13270v()
            int r2 = r2.ordinal()
            r0 = r0[r2]
            if (r0 == r4) goto L_0x0164
            if (r0 == r3) goto L_0x014c
        L_0x014a:
            goto L_0x007f
        L_0x014c:
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            float r0 = r0.f2403y
            e.c.a.a.j.j r2 = r11.f2282l0
            float r2 = r2.mo23471k()
            com.github.mikephil.charting.components.e r3 = r11.f2274d0
            float r3 = r3.mo13267s()
            float r2 = r2 * r3
            float r0 = java.lang.Math.min(r0, r2)
            goto L_0x0084
        L_0x0164:
            com.github.mikephil.charting.components.e r0 = r11.f2274d0
            float r0 = r0.f2403y
            e.c.a.a.j.j r2 = r11.f2282l0
            float r2 = r2.mo23471k()
            com.github.mikephil.charting.components.e r3 = r11.f2274d0
            float r3 = r3.mo13267s()
            float r2 = r2 * r3
            float r0 = java.lang.Math.min(r0, r2)
            goto L_0x0087
        L_0x017c:
            r1 = r0
            goto L_0x007f
        L_0x017f:
            float r4 = r11.getRequiredBaseOffset()
            float r1 = r1 + r4
            float r4 = r11.getRequiredBaseOffset()
            float r0 = r0 + r4
            float r4 = r11.getRequiredBaseOffset()
            float r3 = r3 + r4
            float r4 = r11.getRequiredBaseOffset()
            float r2 = r2 + r4
            goto L_0x0197
        L_0x0194:
            r0 = 0
            r2 = 0
            r3 = 0
        L_0x0197:
            float r4 = r11.f2324B0
            float r4 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r4)
            boolean r5 = r11 instanceof com.github.mikephil.charting.charts.RadarChart
            if (r5 == 0) goto L_0x01b8
            com.github.mikephil.charting.components.h r5 = r11.getXAxis()
            boolean r6 = r5.mo13240f()
            if (r6 == 0) goto L_0x01b8
            boolean r6 = r5.mo13225v()
            if (r6 == 0) goto L_0x01b8
            int r5 = r5.f2447L
            float r5 = (float) r5
            float r4 = java.lang.Math.max(r4, r5)
        L_0x01b8:
            float r5 = r11.getExtraTopOffset()
            float r3 = r3 + r5
            float r5 = r11.getExtraRightOffset()
            float r0 = r0 + r5
            float r5 = r11.getExtraBottomOffset()
            float r2 = r2 + r5
            float r5 = r11.getExtraLeftOffset()
            float r1 = r1 + r5
            float r1 = java.lang.Math.max(r4, r1)
            float r3 = java.lang.Math.max(r4, r3)
            float r0 = java.lang.Math.max(r4, r0)
            float r5 = r11.getRequiredBaseOffset()
            float r2 = java.lang.Math.max(r5, r2)
            float r2 = java.lang.Math.max(r4, r2)
            e.c.a.a.j.j r4 = r11.f2282l0
            r4.mo23445a(r1, r3, r0, r2)
            boolean r4 = r11.f2263P
            if (r4 == 0) goto L_0x021b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "offsetLeft: "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r1 = ", offsetTop: "
            r4.append(r1)
            r4.append(r3)
            java.lang.String r1 = ", offsetRight: "
            r4.append(r1)
            r4.append(r0)
            java.lang.String r0 = ", offsetBottom: "
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            java.lang.String r1 = "MPAndroidChart"
            android.util.Log.i(r1, r0)
        L_0x021b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.mikephil.charting.charts.PieRadarChartBase.mo12961d():void");
    }

    public float getDiameter() {
        RectF n = super.f2282l0.mo23476n();
        n.left += getExtraLeftOffset();
        n.top += getExtraTopOffset();
        n.right -= getExtraRightOffset();
        n.bottom -= getExtraBottomOffset();
        return Math.min(n.width(), n.height());
    }

    public int getMaxVisibleCount() {
        return super.f2264Q.mo13387d();
    }

    public float getMinOffset() {
        return this.f2324B0;
    }

    public abstract float getRadius();

    public float getRawRotationAngle() {
        return this.f2326z0;
    }

    /* access modifiers changed from: protected */
    public abstract float getRequiredBaseOffset();

    /* access modifiers changed from: protected */
    public abstract float getRequiredLegendOffset();

    public float getRotationAngle() {
        return this.f2325y0;
    }

    public float getYChartMax() {
        return 0.0f;
    }

    public float getYChartMin() {
        return 0.0f;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        super.f2276f0 = new PieRadarChartTouchListener(this);
    }

    /* renamed from: m */
    public void mo12978m() {
        if (super.f2264Q != null) {
            mo13129o();
            if (super.f2274d0 != null) {
                super.f2279i0.mo23361a((ChartData<?>) super.f2264Q);
            }
            mo12961d();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public void mo13129o() {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ChartTouchListener bVar;
        if (!super.f2272b0 || (bVar = super.f2276f0) == null) {
            return super.onTouchEvent(motionEvent);
        }
        return bVar.onTouch(this, motionEvent);
    }

    /* renamed from: p */
    public boolean mo13171p() {
        return this.f2323A0;
    }

    public void setMinOffset(float f) {
        this.f2324B0 = f;
    }

    public void setRotationAngle(float f) {
        this.f2326z0 = f;
        this.f2325y0 = Utils.m11619c(this.f2326z0);
    }

    public void setRotationEnabled(boolean z) {
        this.f2323A0 = z;
    }

    /* renamed from: a */
    public void mo13160a(MPPointF eVar, float f, float f2, MPPointF eVar2) {
        double d = (double) f;
        double d2 = (double) f2;
        eVar2.f7123R = (float) (((double) eVar.f7123R) + (Math.cos(Math.toRadians(d2)) * d));
        eVar2.f7124S = (float) (((double) eVar.f7124S) + (d * Math.sin(Math.toRadians(d2))));
    }

    public PieRadarChartBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PieRadarChartBase(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: d */
    public float mo13163d(float f, float f2) {
        MPPointF centerOffsets = getCenterOffsets();
        double d = (double) (f - centerOffsets.f7123R);
        double d2 = (double) (f2 - centerOffsets.f7124S);
        float degrees = (float) Math.toDegrees(Math.acos(d2 / Math.sqrt((d * d) + (d2 * d2))));
        if (f > centerOffsets.f7123R) {
            degrees = 360.0f - degrees;
        }
        float f3 = degrees + 90.0f;
        if (f3 > 360.0f) {
            f3 -= 360.0f;
        }
        MPPointF.m11570b(centerOffsets);
        return f3;
    }
}
