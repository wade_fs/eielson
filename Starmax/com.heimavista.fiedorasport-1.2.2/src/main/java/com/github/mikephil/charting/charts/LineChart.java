package com.github.mikephil.charting.charts;

import android.content.Context;
import android.util.AttributeSet;
import com.github.mikephil.charting.data.LineData;
import p119e.p128c.p129a.p130a.p135e.p136a.LineDataProvider;
import p119e.p128c.p129a.p130a.p141i.DataRenderer;
import p119e.p128c.p129a.p130a.p141i.LineChartRenderer;

public class LineChart extends BarLineChartBase<LineData> implements LineDataProvider {
    public LineChart(Context context) {
        super(context);
    }

    public LineData getLineData() {
        return (LineData) this.f2264Q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2280j0 = new LineChartRenderer(this, this.f2283m0, this.f2282l0);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        DataRenderer gVar = this.f2280j0;
        if (gVar != null && (gVar instanceof LineChartRenderer)) {
            ((LineChartRenderer) gVar).mo23367b();
        }
        super.onDetachedFromWindow();
    }

    public LineChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LineChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
