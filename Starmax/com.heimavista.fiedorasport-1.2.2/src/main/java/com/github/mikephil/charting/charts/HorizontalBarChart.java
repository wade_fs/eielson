package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p134d.HorizontalBarHighlighter;
import p119e.p128c.p129a.p130a.p141i.HorizontalBarChartRenderer;
import p119e.p128c.p129a.p130a.p141i.XAxisRendererHorizontalBarChart;
import p119e.p128c.p129a.p130a.p141i.YAxisRendererHorizontalBarChart;
import p119e.p128c.p129a.p130a.p143j.HorizontalViewPortHandler;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.TransformerHorizontalBarChart;
import p119e.p128c.p129a.p130a.p143j.Utils;

public class HorizontalBarChart extends BarChart {

    /* renamed from: i1 */
    private RectF f2306i1 = new RectF();

    public HorizontalBarChart(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: D */
    public void mo12951D() {
        Transformer gVar = this.f2248U0;
        YAxis iVar = this.f2244Q0;
        float f = iVar.f2350H;
        float f2 = iVar.f2351I;
        XAxis hVar = this.f2271a0;
        gVar.mo23429a(f, f2, hVar.f2351I, hVar.f2350H);
        Transformer gVar2 = this.f2247T0;
        YAxis iVar2 = this.f2243P0;
        float f3 = iVar2.f2350H;
        float f4 = iVar2.f2351I;
        XAxis hVar2 = this.f2271a0;
        gVar2.mo23429a(f3, f4, hVar2.f2351I, hVar2.f2350H);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float[] mo13029a(Highlight dVar) {
        return new float[]{dVar.mo23247e(), dVar.mo23246d()};
    }

    /* renamed from: d */
    public void mo12961d() {
        mo12954a(this.f2306i1);
        RectF rectF = this.f2306i1;
        float f = rectF.left + 0.0f;
        float f2 = rectF.top + 0.0f;
        float f3 = rectF.right + 0.0f;
        float f4 = rectF.bottom + 0.0f;
        if (this.f2243P0.mo13297L()) {
            f2 += this.f2243P0.mo13298a(this.f2245R0.mo23334a());
        }
        if (this.f2244Q0.mo13297L()) {
            f4 += this.f2244Q0.mo13298a(this.f2246S0.mo23334a());
        }
        XAxis hVar = this.f2271a0;
        float f5 = (float) hVar.f2447L;
        if (hVar.mo13240f()) {
            if (this.f2271a0.mo13282A() == XAxis.C1654a.BOTTOM) {
                f += f5;
            } else {
                if (this.f2271a0.mo13282A() != XAxis.C1654a.TOP) {
                    if (this.f2271a0.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                        f += f5;
                    }
                }
                f3 += f5;
            }
        }
        float extraTopOffset = f2 + getExtraTopOffset();
        float extraRightOffset = f3 + getExtraRightOffset();
        float extraBottomOffset = f4 + getExtraBottomOffset();
        float extraLeftOffset = f + getExtraLeftOffset();
        float a = Utils.m11599a(this.f2240M0);
        this.f2282l0.mo23445a(Math.max(a, extraLeftOffset), Math.max(a, extraTopOffset), Math.max(a, extraRightOffset), Math.max(a, extraBottomOffset));
        if (this.f2263P) {
            Log.i("MPAndroidChart", "offsetLeft: " + extraLeftOffset + ", offsetTop: " + extraTopOffset + ", offsetRight: " + extraRightOffset + ", offsetBottom: " + extraBottomOffset);
            StringBuilder sb = new StringBuilder();
            sb.append("Content: ");
            sb.append(this.f2282l0.mo23476n().toString());
            Log.i("MPAndroidChart", sb.toString());
        }
        mo12950C();
        mo12951D();
    }

    public float getHighestVisibleX() {
        mo12952a(YAxis.C1655a.LEFT).mo23430a(this.f2282l0.mo23463g(), this.f2282l0.mo23467i(), this.f2256c1);
        return (float) Math.min((double) this.f2271a0.f2349G, this.f2256c1.f7121S);
    }

    public float getLowestVisibleX() {
        mo12952a(YAxis.C1655a.LEFT).mo23430a(this.f2282l0.mo23463g(), this.f2282l0.mo23459e(), this.f2255b1);
        return (float) Math.max((double) this.f2271a0.f2350H, this.f2255b1.f7121S);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        this.f2282l0 = new HorizontalViewPortHandler();
        super.mo12942h();
        this.f2247T0 = new TransformerHorizontalBarChart(this.f2282l0);
        this.f2248U0 = new TransformerHorizontalBarChart(this.f2282l0);
        this.f2280j0 = new HorizontalBarChartRenderer(this, this.f2283m0, this.f2282l0);
        setHighlighter(new HorizontalBarHighlighter(this));
        this.f2245R0 = new YAxisRendererHorizontalBarChart(this.f2282l0, this.f2243P0, this.f2247T0);
        this.f2246S0 = new YAxisRendererHorizontalBarChart(this.f2282l0, this.f2244Q0, this.f2248U0);
        this.f2249V0 = new XAxisRendererHorizontalBarChart(this.f2282l0, this.f2271a0, this.f2247T0, super);
    }

    public void setVisibleXRangeMaximum(float f) {
        this.f2282l0.mo23474l(this.f2271a0.f2351I / f);
    }

    public void setVisibleXRangeMinimum(float f) {
        this.f2282l0.mo23470j(this.f2271a0.f2351I / f);
    }

    /* renamed from: a */
    public Highlight mo12937a(float f, float f2) {
        if (this.f2264Q != null) {
            return getHighlighter().mo23231a(f2, f);
        }
        if (!this.f2263P) {
            return null;
        }
        Log.e("MPAndroidChart", "Can't select by touch. No data set.");
        return null;
    }

    public HorizontalBarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public HorizontalBarChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
