package com.github.mikephil.charting.data;

import com.github.mikephil.charting.data.Entry;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.github.mikephil.charting.data.k */
public abstract class DataSet<T extends Entry> extends BaseDataSet<T> {

    /* renamed from: s */
    protected List<T> f2531s = null;

    /* renamed from: t */
    protected float f2532t = -3.4028235E38f;

    /* renamed from: u */
    protected float f2533u = Float.MAX_VALUE;

    /* renamed from: v */
    protected float f2534v = -3.4028235E38f;

    /* renamed from: w */
    protected float f2535w = Float.MAX_VALUE;

    /* renamed from: com.github.mikephil.charting.data.k$a */
    /* compiled from: DataSet */
    public enum C1658a {
        UP,
        DOWN,
        CLOSEST
    }

    public DataSet(List<T> list, String str) {
        super(str);
        this.f2531s = list;
        if (this.f2531s == null) {
            this.f2531s = new ArrayList();
        }
        mo13402H0();
    }

    /* renamed from: H0 */
    public void mo13402H0() {
        List<T> list = this.f2531s;
        if (list != null && !list.isEmpty()) {
            this.f2532t = -3.4028235E38f;
            this.f2533u = Float.MAX_VALUE;
            this.f2534v = -3.4028235E38f;
            this.f2535w = Float.MAX_VALUE;
            for (T t : this.f2531s) {
                mo13325b(t);
            }
        }
    }

    /* renamed from: I0 */
    public String mo13403I0() {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder();
        sb.append("DataSet, label: ");
        sb.append(mo13352f() == null ? "" : mo13352f());
        sb.append(", entries: ");
        sb.append(this.f2531s.size());
        sb.append("\n");
        stringBuffer.append(sb.toString());
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public float mo13404a() {
        return this.f2535w;
    }

    /* renamed from: b */
    public void mo13412b(float f, float f2) {
        List<T> list = this.f2531s;
        if (list != null && !list.isEmpty()) {
            this.f2532t = -3.4028235E38f;
            this.f2533u = Float.MAX_VALUE;
            int b = mo13409b(f2, Float.NaN, C1658a.UP);
            for (int b2 = mo13409b(f, Float.NaN, C1658a.DOWN); b2 <= b; b2++) {
                mo13414d((Entry) this.f2531s.get(b2));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo13325b(Entry entry);

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo13413c(Entry entry) {
        if (entry.mo13315d() < this.f2535w) {
            this.f2535w = entry.mo13315d();
        }
        if (entry.mo13315d() > this.f2534v) {
            this.f2534v = entry.mo13315d();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo13414d(T t) {
        if (t.mo13303c() < this.f2533u) {
            this.f2533u = t.mo13303c();
        }
        if (t.mo13303c() > this.f2532t) {
            this.f2532t = t.mo13303c();
        }
    }

    /* renamed from: g */
    public float mo13415g() {
        return this.f2533u;
    }

    /* renamed from: q */
    public float mo13416q() {
        return this.f2534v;
    }

    /* renamed from: t */
    public int mo13417t() {
        return this.f2531s.size();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(mo13403I0());
        for (int i = 0; i < this.f2531s.size(); i++) {
            stringBuffer.append(((Entry) this.f2531s.get(i)).toString() + " ");
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public int mo13405a(Entry entry) {
        return this.f2531s.indexOf(entry);
    }

    /* renamed from: a */
    public T mo13407a(float f, float f2, C1658a aVar) {
        int b = mo13409b(f, f2, aVar);
        if (b > -1) {
            return (Entry) this.f2531s.get(b);
        }
        return null;
    }

    /* renamed from: a */
    public T mo13406a(float f, float f2) {
        return mo13407a(f, f2, C1658a.CLOSEST);
    }

    /* renamed from: b */
    public float mo13408b() {
        return this.f2532t;
    }

    /* renamed from: b */
    public T mo13410b(int i) {
        return (Entry) this.f2531s.get(i);
    }

    /* renamed from: b */
    public int mo13409b(float f, float f2, C1658a aVar) {
        int i;
        Entry entry;
        List<T> list = this.f2531s;
        if (list == null || list.isEmpty()) {
            return -1;
        }
        int i2 = 0;
        int size = this.f2531s.size() - 1;
        while (i2 < size) {
            int i3 = (i2 + size) / 2;
            float d = ((Entry) this.f2531s.get(i3)).mo13315d() - f;
            int i4 = i3 + 1;
            float abs = Math.abs(d);
            float abs2 = Math.abs(((Entry) this.f2531s.get(i4)).mo13315d() - f);
            if (abs2 >= abs) {
                if (abs >= abs2) {
                    double d2 = (double) d;
                    if (d2 < 0.0d) {
                        if (d2 >= 0.0d) {
                        }
                    }
                }
                size = i3;
            }
            i2 = i4;
        }
        if (size == -1) {
            return size;
        }
        float d3 = ((Entry) this.f2531s.get(size)).mo13315d();
        if (aVar == C1658a.UP) {
            if (d3 < f && size < this.f2531s.size() - 1) {
                size++;
            }
        } else if (aVar == C1658a.DOWN && d3 > f && size > 0) {
            size--;
        }
        if (Float.isNaN(f2)) {
            return size;
        }
        while (size > 0 && ((Entry) this.f2531s.get(size - 1)).mo13315d() == d3) {
            size--;
        }
        float c = ((Entry) this.f2531s.get(size)).mo13303c();
        loop2:
        while (true) {
            i = size;
            do {
                size++;
                if (size >= this.f2531s.size()) {
                    break loop2;
                }
                entry = (Entry) this.f2531s.get(size);
                if (entry.mo13315d() != d3) {
                    break loop2;
                }
            } while (Math.abs(entry.mo13303c() - f2) >= Math.abs(c - f2));
            c = f2;
        }
        return i;
    }

    /* renamed from: b */
    public List<T> mo13411b(float f) {
        ArrayList arrayList = new ArrayList();
        int size = this.f2531s.size() - 1;
        int i = 0;
        while (true) {
            if (i > size) {
                break;
            }
            int i2 = (size + i) / 2;
            Entry entry = (Entry) this.f2531s.get(i2);
            if (f == entry.mo13315d()) {
                while (i2 > 0 && ((Entry) this.f2531s.get(i2 - 1)).mo13315d() == f) {
                    i2--;
                }
                int size2 = this.f2531s.size();
                while (i2 < size2) {
                    Entry entry2 = (Entry) this.f2531s.get(i2);
                    if (entry2.mo13315d() != f) {
                        break;
                    }
                    arrayList.add(entry2);
                    i2++;
                }
            } else if (f > entry.mo13315d()) {
                i = i2 + 1;
            } else {
                size = i2 - 1;
            }
        }
        return arrayList;
    }
}
