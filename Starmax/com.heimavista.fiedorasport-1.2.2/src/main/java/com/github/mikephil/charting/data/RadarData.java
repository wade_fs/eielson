package com.github.mikephil.charting.data;

import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IRadarDataSet;

/* renamed from: com.github.mikephil.charting.data.p */
public class RadarData extends ChartData<IRadarDataSet> {
    /* renamed from: a */
    public Entry mo13373a(Highlight dVar) {
        return ((IRadarDataSet) mo13374a(dVar.mo23245c())).mo13410b((int) dVar.mo23249g());
    }
}
