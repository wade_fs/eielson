package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieEntry;
import java.util.List;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p134d.PieHighlighter;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;
import p119e.p128c.p129a.p130a.p141i.DataRenderer;
import p119e.p128c.p129a.p130a.p141i.PieChartRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

public class PieChart extends PieRadarChartBase<PieData> {

    /* renamed from: C0 */
    private RectF f2307C0 = new RectF();

    /* renamed from: D0 */
    private boolean f2308D0 = true;

    /* renamed from: E0 */
    private float[] f2309E0 = new float[1];

    /* renamed from: F0 */
    private float[] f2310F0 = new float[1];

    /* renamed from: G0 */
    private boolean f2311G0 = true;

    /* renamed from: H0 */
    private boolean f2312H0 = false;

    /* renamed from: I0 */
    private boolean f2313I0 = false;

    /* renamed from: J0 */
    private boolean f2314J0 = false;

    /* renamed from: K0 */
    private CharSequence f2315K0 = "";

    /* renamed from: L0 */
    private MPPointF f2316L0 = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: M0 */
    private float f2317M0 = 50.0f;

    /* renamed from: N0 */
    protected float f2318N0 = 55.0f;

    /* renamed from: O0 */
    private boolean f2319O0 = true;

    /* renamed from: P0 */
    private float f2320P0 = 100.0f;

    /* renamed from: Q0 */
    protected float f2321Q0 = 360.0f;

    /* renamed from: R0 */
    private float f2322R0 = 0.0f;

    public PieChart(Context context) {
        super(context);
    }

    /* renamed from: e */
    private float m3854e(float f, float f2) {
        return (f / f2) * this.f2321Q0;
    }

    /* renamed from: w */
    private void m3855w() {
        int d = ((PieData) this.f2264Q).mo13387d();
        if (this.f2309E0.length != d) {
            this.f2309E0 = new float[d];
        } else {
            for (int i = 0; i < d; i++) {
                this.f2309E0[i] = 0.0f;
            }
        }
        if (this.f2310F0.length != d) {
            this.f2310F0 = new float[d];
        } else {
            for (int i2 = 0; i2 < d; i2++) {
                this.f2310F0[i2] = 0.0f;
            }
        }
        float l = ((PieData) this.f2264Q).mo13420l();
        List c = ((PieData) this.f2264Q).mo13386c();
        float f = this.f2322R0;
        boolean z = f != 0.0f && ((float) d) * f <= this.f2321Q0;
        float[] fArr = new float[d];
        int i3 = 0;
        float f2 = 0.0f;
        float f3 = 0.0f;
        int i4 = 0;
        while (i3 < ((PieData) this.f2264Q).mo13383b()) {
            IPieDataSet iVar = (IPieDataSet) c.get(i3);
            float f4 = f2;
            for (int i5 = 0; i5 < iVar.mo13417t(); i5++) {
                float e = m3854e(Math.abs(((PieEntry) iVar.mo13410b(i5)).mo13303c()), l);
                if (z) {
                    float f5 = this.f2322R0;
                    float f6 = e - f5;
                    if (f6 <= 0.0f) {
                        fArr[i4] = f5;
                        f4 += -f6;
                    } else {
                        fArr[i4] = e;
                        f3 += f6;
                    }
                }
                float[] fArr2 = this.f2309E0;
                fArr2[i4] = e;
                if (i4 == 0) {
                    this.f2310F0[i4] = fArr2[i4];
                } else {
                    float[] fArr3 = this.f2310F0;
                    fArr3[i4] = fArr3[i4 - 1] + fArr2[i4];
                }
                i4++;
            }
            i3++;
            f2 = f4;
        }
        if (z) {
            for (int i6 = 0; i6 < d; i6++) {
                fArr[i6] = fArr[i6] - (((fArr[i6] - this.f2322R0) / f3) * f2);
                if (i6 == 0) {
                    this.f2310F0[0] = fArr[0];
                } else {
                    float[] fArr4 = this.f2310F0;
                    fArr4[i6] = fArr4[i6 - 1] + fArr[i6];
                }
            }
            this.f2309E0 = fArr;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float[] mo13029a(Highlight dVar) {
        MPPointF centerCircleBox = getCenterCircleBox();
        float radius = getRadius();
        float f = (radius / 10.0f) * 3.6f;
        if (mo13132s()) {
            f = (radius - ((radius / 100.0f) * getHoleRadius())) / 2.0f;
        }
        float f2 = radius - f;
        float rotationAngle = getRotationAngle();
        int g = (int) dVar.mo23249g();
        float f3 = this.f2309E0[g] / 2.0f;
        double d = (double) f2;
        float cos = (float) ((Math.cos(Math.toRadians((double) (((this.f2310F0[g] + rotationAngle) - f3) * this.f2283m0.mo23204b()))) * d) + ((double) centerCircleBox.f7123R));
        MPPointF.m11570b(centerCircleBox);
        return new float[]{cos, (float) ((d * Math.sin(Math.toRadians((double) (((rotationAngle + this.f2310F0[g]) - f3) * this.f2283m0.mo23204b())))) + ((double) centerCircleBox.f7124S))};
    }

    /* renamed from: d */
    public void mo12961d() {
        super.mo12961d();
        if (this.f2264Q != null) {
            float diameter = getDiameter() / 2.0f;
            MPPointF centerOffsets = getCenterOffsets();
            float F0 = ((PieData) this.f2264Q).mo13419k().mo13426F0();
            RectF rectF = this.f2307C0;
            float f = centerOffsets.f7123R;
            float f2 = centerOffsets.f7124S;
            rectF.set((f - diameter) + F0, (f2 - diameter) + F0, (f + diameter) - F0, (f2 + diameter) - F0);
            MPPointF.m11570b(centerOffsets);
        }
    }

    public float[] getAbsoluteAngles() {
        return this.f2310F0;
    }

    public MPPointF getCenterCircleBox() {
        return MPPointF.m11567a(this.f2307C0.centerX(), this.f2307C0.centerY());
    }

    public CharSequence getCenterText() {
        return this.f2315K0;
    }

    public MPPointF getCenterTextOffset() {
        MPPointF eVar = this.f2316L0;
        return MPPointF.m11567a(eVar.f7123R, eVar.f7124S);
    }

    public float getCenterTextRadiusPercent() {
        return this.f2320P0;
    }

    public RectF getCircleBox() {
        return this.f2307C0;
    }

    public float[] getDrawAngles() {
        return this.f2309E0;
    }

    public float getHoleRadius() {
        return this.f2317M0;
    }

    public float getMaxAngle() {
        return this.f2321Q0;
    }

    public float getMinAngleForSlices() {
        return this.f2322R0;
    }

    public float getRadius() {
        RectF rectF = this.f2307C0;
        if (rectF == null) {
            return 0.0f;
        }
        return Math.min(rectF.width() / 2.0f, this.f2307C0.height() / 2.0f);
    }

    /* access modifiers changed from: protected */
    public float getRequiredBaseOffset() {
        return 0.0f;
    }

    /* access modifiers changed from: protected */
    public float getRequiredLegendOffset() {
        return this.f2279i0.mo23357a().getTextSize() * 2.0f;
    }

    public float getTransparentCircleRadius() {
        return this.f2318N0;
    }

    @Deprecated
    public XAxis getXAxis() {
        throw new RuntimeException("PieChart has no XAxis");
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2280j0 = new PieChartRenderer(this, this.f2283m0, this.f2282l0);
        this.f2271a0 = null;
        this.f2281k0 = new PieHighlighter(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public void mo13129o() {
        m3855w();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        DataRenderer gVar = this.f2280j0;
        if (gVar != null && (gVar instanceof PieChartRenderer)) {
            ((PieChartRenderer) gVar).mo23388f();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f2264Q != null) {
            this.f2280j0.mo23339a(canvas);
            if (mo13072n()) {
                this.f2280j0.mo23342a(canvas, this.f2289s0);
            }
            this.f2280j0.mo23344b(canvas);
            this.f2280j0.mo23345c(canvas);
            this.f2279i0.mo23358a(canvas);
            mo13025a(canvas);
            mo13032b(canvas);
        }
    }

    /* renamed from: q */
    public boolean mo13130q() {
        return this.f2319O0;
    }

    /* renamed from: r */
    public boolean mo13131r() {
        return this.f2308D0;
    }

    /* renamed from: s */
    public boolean mo13132s() {
        return this.f2311G0;
    }

    public void setCenterText(CharSequence charSequence) {
        if (charSequence == null) {
            this.f2315K0 = "";
        } else {
            this.f2315K0 = charSequence;
        }
    }

    public void setCenterTextColor(int i) {
        ((PieChartRenderer) this.f2280j0).mo23382b().setColor(i);
    }

    public void setCenterTextRadiusPercent(float f) {
        this.f2320P0 = f;
    }

    public void setCenterTextSize(float f) {
        ((PieChartRenderer) this.f2280j0).mo23382b().setTextSize(Utils.m11599a(f));
    }

    public void setCenterTextSizePixels(float f) {
        ((PieChartRenderer) this.f2280j0).mo23382b().setTextSize(f);
    }

    public void setCenterTextTypeface(Typeface typeface) {
        ((PieChartRenderer) this.f2280j0).mo23382b().setTypeface(typeface);
    }

    public void setDrawCenterText(boolean z) {
        this.f2319O0 = z;
    }

    public void setDrawEntryLabels(boolean z) {
        this.f2308D0 = z;
    }

    public void setDrawHoleEnabled(boolean z) {
        this.f2311G0 = z;
    }

    public void setDrawRoundedSlices(boolean z) {
        this.f2314J0 = z;
    }

    @Deprecated
    public void setDrawSliceText(boolean z) {
        this.f2308D0 = z;
    }

    public void setDrawSlicesUnderHole(boolean z) {
        this.f2312H0 = z;
    }

    public void setEntryLabelColor(int i) {
        ((PieChartRenderer) this.f2280j0).mo23383c().setColor(i);
    }

    public void setEntryLabelTextSize(float f) {
        ((PieChartRenderer) this.f2280j0).mo23383c().setTextSize(Utils.m11599a(f));
    }

    public void setEntryLabelTypeface(Typeface typeface) {
        ((PieChartRenderer) this.f2280j0).mo23383c().setTypeface(typeface);
    }

    public void setHoleColor(int i) {
        ((PieChartRenderer) this.f2280j0).mo23384d().setColor(i);
    }

    public void setHoleRadius(float f) {
        this.f2317M0 = f;
    }

    public void setMaxAngle(float f) {
        if (f > 360.0f) {
            f = 360.0f;
        }
        if (f < 90.0f) {
            f = 90.0f;
        }
        this.f2321Q0 = f;
    }

    public void setMinAngleForSlices(float f) {
        float f2 = this.f2321Q0;
        if (f > f2 / 2.0f) {
            f = f2 / 2.0f;
        } else if (f < 0.0f) {
            f = 0.0f;
        }
        this.f2322R0 = f;
    }

    public void setTransparentCircleAlpha(int i) {
        ((PieChartRenderer) this.f2280j0).mo23386e().setAlpha(i);
    }

    public void setTransparentCircleColor(int i) {
        Paint e = ((PieChartRenderer) this.f2280j0).mo23386e();
        int alpha = e.getAlpha();
        e.setColor(i);
        e.setAlpha(alpha);
    }

    public void setTransparentCircleRadius(float f) {
        this.f2318N0 = f;
    }

    public void setUsePercentValues(boolean z) {
        this.f2313I0 = z;
    }

    /* renamed from: t */
    public boolean mo13156t() {
        return this.f2314J0;
    }

    /* renamed from: u */
    public boolean mo13157u() {
        return this.f2312H0;
    }

    /* renamed from: v */
    public boolean mo13158v() {
        return this.f2313I0;
    }

    /* renamed from: d */
    public boolean mo13114d(int i) {
        if (!mo13072n()) {
            return false;
        }
        int i2 = 0;
        while (true) {
            Highlight[] dVarArr = this.f2289s0;
            if (i2 >= dVarArr.length) {
                return false;
            }
            if (((int) dVarArr[i2].mo23249g()) == i) {
                return true;
            }
            i2++;
        }
    }

    /* renamed from: a */
    public int mo13113a(float f) {
        float c = Utils.m11619c(f - getRotationAngle());
        int i = 0;
        while (true) {
            float[] fArr = this.f2310F0;
            if (i >= fArr.length) {
                return -1;
            }
            if (fArr[i] > c) {
                return i;
            }
            i++;
        }
    }

    public PieChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PieChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
