package com.github.mikephil.charting.data;

import android.annotation.SuppressLint;

@SuppressLint({"ParcelCreator"})
public class BubbleEntry extends Entry {

    /* renamed from: T */
    private float f2480T;

    /* renamed from: e */
    public float mo13310e() {
        return this.f2480T;
    }
}
