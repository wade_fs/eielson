package com.github.mikephil.charting.components;

import android.graphics.Paint;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.components.i */
public class YAxis extends AxisBase {

    /* renamed from: J */
    private boolean f2458J = true;

    /* renamed from: K */
    private boolean f2459K = true;

    /* renamed from: L */
    protected boolean f2460L = false;

    /* renamed from: M */
    protected boolean f2461M = false;

    /* renamed from: N */
    protected int f2462N = -7829368;

    /* renamed from: O */
    protected float f2463O = 1.0f;

    /* renamed from: P */
    protected float f2464P = 10.0f;

    /* renamed from: Q */
    protected float f2465Q = 10.0f;

    /* renamed from: R */
    private C1656b f2466R = C1656b.OUTSIDE_CHART;

    /* renamed from: S */
    private C1655a f2467S;

    /* renamed from: T */
    protected float f2468T = 0.0f;

    /* renamed from: U */
    protected float f2469U = Float.POSITIVE_INFINITY;

    /* renamed from: com.github.mikephil.charting.components.i$a */
    /* compiled from: YAxis */
    public enum C1655a {
        LEFT,
        RIGHT
    }

    /* renamed from: com.github.mikephil.charting.components.i$b */
    /* compiled from: YAxis */
    public enum C1656b {
        OUTSIDE_CHART,
        INSIDE_CHART
    }

    public YAxis(C1655a aVar) {
        this.f2467S = aVar;
        this.f2374c = 0.0f;
    }

    /* renamed from: A */
    public C1656b mo13286A() {
        return this.f2466R;
    }

    /* renamed from: B */
    public float mo13287B() {
        return this.f2469U;
    }

    /* renamed from: C */
    public float mo13288C() {
        return this.f2468T;
    }

    /* renamed from: D */
    public float mo13289D() {
        return this.f2465Q;
    }

    /* renamed from: E */
    public float mo13290E() {
        return this.f2464P;
    }

    /* renamed from: F */
    public int mo13291F() {
        return this.f2462N;
    }

    /* renamed from: G */
    public float mo13292G() {
        return this.f2463O;
    }

    /* renamed from: H */
    public boolean mo13293H() {
        return this.f2458J;
    }

    /* renamed from: I */
    public boolean mo13294I() {
        return this.f2459K;
    }

    /* renamed from: J */
    public boolean mo13295J() {
        return this.f2461M;
    }

    /* renamed from: K */
    public boolean mo13296K() {
        return this.f2460L;
    }

    /* renamed from: L */
    public boolean mo13297L() {
        return mo13240f() && mo13225v() && mo13286A() == C1656b.OUTSIDE_CHART;
    }

    /* renamed from: a */
    public float mo13298a(Paint paint) {
        paint.setTextSize(this.f2376e);
        return ((float) Utils.m11602a(paint, mo13219p())) + (mo13239e() * 2.0f);
    }

    /* renamed from: b */
    public float mo13299b(Paint paint) {
        paint.setTextSize(this.f2376e);
        float c = ((float) Utils.m11621c(paint, mo13219p())) + (mo13238d() * 2.0f);
        float C = mo13288C();
        float B = mo13287B();
        if (C > 0.0f) {
            C = Utils.m11599a(C);
        }
        if (B > 0.0f && B != Float.POSITIVE_INFINITY) {
            B = Utils.m11599a(B);
        }
        if (((double) B) <= 0.0d) {
            B = c;
        }
        return Math.max(C, Math.min(c, B));
    }

    /* renamed from: h */
    public void mo13300h(float f) {
        this.f2465Q = f;
    }

    /* renamed from: i */
    public void mo13301i(float f) {
        this.f2464P = f;
    }

    /* renamed from: z */
    public C1655a mo13302z() {
        return this.f2467S;
    }

    /* renamed from: a */
    public void mo13199a(float f, float f2) {
        if (Math.abs(f2 - f) == 0.0f) {
            f2 += 1.0f;
            f -= 1.0f;
        }
        float abs = Math.abs(f2 - f);
        super.f2350H = super.f2347E ? super.f2350H : f - ((abs / 100.0f) * mo13289D());
        super.f2349G = super.f2348F ? super.f2349G : f2 + ((abs / 100.0f) * mo13290E());
        super.f2351I = Math.abs(super.f2350H - super.f2349G);
    }
}
