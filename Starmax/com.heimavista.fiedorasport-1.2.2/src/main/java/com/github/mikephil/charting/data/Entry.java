package com.github.mikephil.charting.data;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

public class Entry extends BaseEntry implements Parcelable {
    public static final Parcelable.Creator<Entry> CREATOR = new C1657a();

    /* renamed from: S */
    private float f2485S = 0.0f;

    /* renamed from: com.github.mikephil.charting.data.Entry$a */
    static class C1657a implements Parcelable.Creator<Entry> {
        C1657a() {
        }

        public Entry createFromParcel(Parcel parcel) {
            return new Entry(parcel);
        }

        public Entry[] newArray(int i) {
            return new Entry[i];
        }
    }

    public Entry() {
    }

    /* renamed from: d */
    public float mo13315d() {
        return this.f2485S;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "Entry, x: " + this.f2485S + " y: " + mo13303c();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f2485S);
        parcel.writeFloat(mo13303c());
        if (mo13368a() == null) {
            parcel.writeInt(0);
        } else if (mo13368a() instanceof Parcelable) {
            parcel.writeInt(1);
            parcel.writeParcelable((Parcelable) mo13368a(), i);
        } else {
            throw new ParcelFormatException("Cannot parcel an Entry with non-parcelable data");
        }
    }

    public Entry(float f, float f2) {
        super(f2);
        this.f2485S = f;
    }

    protected Entry(Parcel parcel) {
        this.f2485S = parcel.readFloat();
        mo13369a(parcel.readFloat());
        if (parcel.readInt() == 1) {
            mo13370a(parcel.readParcelable(Object.class.getClassLoader()));
        }
    }
}
