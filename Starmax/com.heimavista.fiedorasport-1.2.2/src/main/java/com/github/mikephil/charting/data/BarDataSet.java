package com.github.mikephil.charting.data;

import android.graphics.Color;
import androidx.core.view.ViewCompat;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;

/* renamed from: com.github.mikephil.charting.data.b */
public class BarDataSet extends BarLineScatterCandleBubbleDataSet<BarEntry> implements IBarDataSet {

    /* renamed from: A */
    private float f2488A = 0.0f;

    /* renamed from: B */
    private int f2489B = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: C */
    private int f2490C = 120;

    /* renamed from: D */
    private int f2491D = 0;

    /* renamed from: E */
    private String[] f2492E = {"Stack"};

    /* renamed from: y */
    private int f2493y = 1;

    /* renamed from: z */
    private int f2494z = Color.rgb(215, 215, 215);

    public BarDataSet(List<BarEntry> list, String str) {
        super(list, str);
        super.f2495x = Color.rgb(0, 0, 0);
        m4015c(list);
        m4014b(list);
    }

    /* renamed from: c */
    private void m4015c(List<BarEntry> list) {
        for (int i = 0; i < list.size(); i++) {
            float[] i2 = list.get(i).mo13308i();
            if (i2 != null && i2.length > this.f2493y) {
                this.f2493y = i2.length;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13325b(BarEntry barEntry) {
        if (barEntry != null && !Float.isNaN(barEntry.mo13303c())) {
            if (barEntry.mo13308i() == null) {
                if (barEntry.mo13303c() < this.f2533u) {
                    this.f2533u = barEntry.mo13303c();
                }
                if (barEntry.mo13303c() > this.f2532t) {
                    this.f2532t = barEntry.mo13303c();
                }
            } else {
                if ((-barEntry.mo13305f()) < this.f2533u) {
                    this.f2533u = -barEntry.mo13305f();
                }
                if (barEntry.mo13306g() > this.f2532t) {
                    this.f2532t = barEntry.mo13306g();
                }
            }
            mo13413c(barEntry);
        }
    }

    /* renamed from: h */
    public void mo13326h(int i) {
        this.f2490C = i;
    }

    /* renamed from: m0 */
    public int mo13327m0() {
        return this.f2489B;
    }

    /* renamed from: n0 */
    public float mo13328n0() {
        return this.f2488A;
    }

    /* renamed from: p0 */
    public int mo13329p0() {
        return this.f2494z;
    }

    /* renamed from: q0 */
    public int mo13330q0() {
        return this.f2493y;
    }

    /* renamed from: r0 */
    public int mo13331r0() {
        return this.f2490C;
    }

    /* renamed from: t0 */
    public boolean mo13332t0() {
        return this.f2493y > 1;
    }

    /* renamed from: u0 */
    public String[] mo13333u0() {
        return this.f2492E;
    }

    /* renamed from: b */
    private void m4014b(List<BarEntry> list) {
        this.f2491D = 0;
        for (int i = 0; i < list.size(); i++) {
            float[] i2 = list.get(i).mo13308i();
            if (i2 == null) {
                this.f2491D++;
            } else {
                this.f2491D += i2.length;
            }
        }
    }
}
