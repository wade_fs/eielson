package com.github.mikephil.charting.components;

import android.graphics.Paint;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.components.c */
public class Description extends ComponentBase {

    /* renamed from: g */
    private String f2378g = "Description Label";

    /* renamed from: h */
    private MPPointF f2379h;

    /* renamed from: i */
    private Paint.Align f2380i = Paint.Align.RIGHT;

    public Description() {
        super.f2376e = Utils.m11599a(8.0f);
    }

    /* renamed from: g */
    public MPPointF mo13241g() {
        return this.f2379h;
    }

    /* renamed from: h */
    public String mo13242h() {
        return this.f2378g;
    }

    /* renamed from: i */
    public Paint.Align mo13243i() {
        return this.f2380i;
    }
}
