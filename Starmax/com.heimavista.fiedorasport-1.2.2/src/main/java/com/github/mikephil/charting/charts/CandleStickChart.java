package com.github.mikephil.charting.charts;

import android.content.Context;
import android.util.AttributeSet;
import com.github.mikephil.charting.data.CandleData;
import p119e.p128c.p129a.p130a.p135e.p136a.CandleDataProvider;
import p119e.p128c.p129a.p130a.p141i.CandleStickChartRenderer;

public class CandleStickChart extends BarLineChartBase<CandleData> implements CandleDataProvider {
    public CandleStickChart(Context context) {
        super(context);
    }

    public CandleData getCandleData() {
        return (CandleData) this.f2264Q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2280j0 = new CandleStickChartRenderer(this, this.f2283m0, this.f2282l0);
        getXAxis().mo13210g(0.5f);
        getXAxis().mo13208f(0.5f);
    }

    public CandleStickChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CandleStickChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
