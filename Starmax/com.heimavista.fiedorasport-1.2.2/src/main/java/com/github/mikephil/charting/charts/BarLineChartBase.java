package com.github.mikephil.charting.charts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import androidx.core.view.ViewCompat;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import p119e.p128c.p129a.p130a.p134d.ChartHighlighter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.BarLineScatterCandleBubbleDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;
import p119e.p128c.p129a.p130a.p138f.MoveViewJob;
import p119e.p128c.p129a.p130a.p139g.BarLineChartTouchListener;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;
import p119e.p128c.p129a.p130a.p139g.OnDrawListener;
import p119e.p128c.p129a.p130a.p141i.DataRenderer;
import p119e.p128c.p129a.p130a.p141i.XAxisRenderer;
import p119e.p128c.p129a.p130a.p141i.YAxisRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

@SuppressLint({"RtlHardcoded"})
public abstract class BarLineChartBase<T extends BarLineScatterCandleBubbleData<? extends IBarLineScatterCandleBubbleDataSet<? extends Entry>>> extends Chart<T> implements BarLineScatterCandleBubbleDataProvider {

    /* renamed from: A0 */
    protected boolean f2228A0 = false;

    /* renamed from: B0 */
    protected boolean f2229B0 = true;

    /* renamed from: C0 */
    protected boolean f2230C0 = true;

    /* renamed from: D0 */
    private boolean f2231D0 = true;

    /* renamed from: E0 */
    private boolean f2232E0 = true;

    /* renamed from: F0 */
    private boolean f2233F0 = true;

    /* renamed from: G0 */
    private boolean f2234G0 = true;

    /* renamed from: H0 */
    protected Paint f2235H0;

    /* renamed from: I0 */
    protected Paint f2236I0;

    /* renamed from: J0 */
    protected boolean f2237J0 = false;

    /* renamed from: K0 */
    protected boolean f2238K0 = false;

    /* renamed from: L0 */
    protected boolean f2239L0 = false;

    /* renamed from: M0 */
    protected float f2240M0 = 15.0f;

    /* renamed from: N0 */
    protected boolean f2241N0 = false;

    /* renamed from: O0 */
    protected OnDrawListener f2242O0;

    /* renamed from: P0 */
    protected YAxis f2243P0;

    /* renamed from: Q0 */
    protected YAxis f2244Q0;

    /* renamed from: R0 */
    protected YAxisRenderer f2245R0;

    /* renamed from: S0 */
    protected YAxisRenderer f2246S0;

    /* renamed from: T0 */
    protected Transformer f2247T0;

    /* renamed from: U0 */
    protected Transformer f2248U0;

    /* renamed from: V0 */
    protected XAxisRenderer f2249V0;

    /* renamed from: W0 */
    private long f2250W0 = 0;

    /* renamed from: X0 */
    private long f2251X0 = 0;

    /* renamed from: Y0 */
    private RectF f2252Y0 = new RectF();

    /* renamed from: Z0 */
    protected Matrix f2253Z0 = new Matrix();

    /* renamed from: a1 */
    private boolean f2254a1;

    /* renamed from: b1 */
    protected MPPointD f2255b1;

    /* renamed from: c1 */
    protected MPPointD f2256c1;

    /* renamed from: d1 */
    protected float[] f2257d1;

    /* renamed from: y0 */
    protected int f2258y0 = 100;

    /* renamed from: z0 */
    protected boolean f2259z0 = false;

    /* renamed from: com.github.mikephil.charting.charts.BarLineChartBase$a */
    static /* synthetic */ class C1643a {

        /* renamed from: a */
        static final /* synthetic */ int[] f2260a = new int[Legend.C1652f.values().length];

        /* renamed from: b */
        static final /* synthetic */ int[] f2261b = new int[Legend.C1650d.values().length];

        /* renamed from: c */
        static final /* synthetic */ int[] f2262c = new int[Legend.C1651e.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|(3:19|20|22)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x005a */
        static {
            /*
                com.github.mikephil.charting.components.e$e[] r0 = com.github.mikephil.charting.components.Legend.C1651e.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2262c = r0
                r0 = 1
                int[] r1 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2262c     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.components.e$e r2 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2262c     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.components.e$e r3 = com.github.mikephil.charting.components.Legend.C1651e.HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.github.mikephil.charting.components.e$d[] r2 = com.github.mikephil.charting.components.Legend.C1650d.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2261b = r2
                int[] r2 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2261b     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.LEFT     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r2 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2261b     // Catch:{ NoSuchFieldError -> 0x003c }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.RIGHT     // Catch:{ NoSuchFieldError -> 0x003c }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                int[] r2 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2261b     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.CENTER     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r4 = 3
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                com.github.mikephil.charting.components.e$f[] r2 = com.github.mikephil.charting.components.Legend.C1652f.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2260a = r2
                int[] r2 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2260a     // Catch:{ NoSuchFieldError -> 0x005a }
                com.github.mikephil.charting.components.e$f r3 = com.github.mikephil.charting.components.Legend.C1652f.TOP     // Catch:{ NoSuchFieldError -> 0x005a }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x005a }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x005a }
            L_0x005a:
                int[] r0 = com.github.mikephil.charting.charts.BarLineChartBase.C1643a.f2260a     // Catch:{ NoSuchFieldError -> 0x0064 }
                com.github.mikephil.charting.components.e$f r2 = com.github.mikephil.charting.components.Legend.C1652f.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0064 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0064 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0064 }
            L_0x0064:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.github.mikephil.charting.charts.BarLineChartBase.C1643a.<clinit>():void");
        }
    }

    public BarLineChartBase(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new Matrix();
        this.f2254a1 = false;
        this.f2255b1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2256c1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2257d1 = new float[2];
    }

    /* renamed from: A */
    public boolean mo12948A() {
        return this.f2233F0;
    }

    /* renamed from: B */
    public boolean mo12949B() {
        return this.f2234G0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: C */
    public void mo12950C() {
        this.f2248U0.mo23434a(this.f2244Q0.mo13296K());
        this.f2247T0.mo23434a(this.f2243P0.mo13296K());
    }

    /* access modifiers changed from: protected */
    /* renamed from: D */
    public void mo12951D() {
        if (super.f2263P) {
            Log.i("MPAndroidChart", "Preparing Value-Px Matrix, xmin: " + super.f2271a0.f2350H + ", xmax: " + super.f2271a0.f2349G + ", xdelta: " + super.f2271a0.f2351I);
        }
        Transformer gVar = this.f2248U0;
        XAxis hVar = super.f2271a0;
        float f = hVar.f2350H;
        float f2 = hVar.f2351I;
        YAxis iVar = this.f2244Q0;
        gVar.mo23429a(f, f2, iVar.f2351I, iVar.f2350H);
        Transformer gVar2 = this.f2247T0;
        XAxis hVar2 = super.f2271a0;
        float f3 = hVar2.f2350H;
        float f4 = hVar2.f2351I;
        YAxis iVar2 = this.f2243P0;
        gVar2.mo23429a(f3, f4, iVar2.f2351I, iVar2.f2350H);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12954a(RectF rectF) {
        rectF.left = 0.0f;
        rectF.right = 0.0f;
        rectF.top = 0.0f;
        rectF.bottom = 0.0f;
        Legend eVar = super.f2274d0;
        if (eVar != null && eVar.mo13240f() && !super.f2274d0.mo13273y()) {
            int i = C1643a.f2262c[super.f2274d0.mo13268t().ordinal()];
            if (i == 1) {
                int i2 = C1643a.f2261b[super.f2274d0.mo13266r().ordinal()];
                if (i2 == 1) {
                    rectF.left += Math.min(super.f2274d0.f2402x, super.f2282l0.mo23473l() * super.f2274d0.mo13267s()) + super.f2274d0.mo13238d();
                } else if (i2 == 2) {
                    rectF.right += Math.min(super.f2274d0.f2402x, super.f2282l0.mo23473l() * super.f2274d0.mo13267s()) + super.f2274d0.mo13238d();
                } else if (i2 == 3) {
                    int i3 = C1643a.f2260a[super.f2274d0.mo13270v().ordinal()];
                    if (i3 == 1) {
                        rectF.top += Math.min(super.f2274d0.f2403y, super.f2282l0.mo23471k() * super.f2274d0.mo13267s()) + super.f2274d0.mo13239e();
                    } else if (i3 == 2) {
                        rectF.bottom += Math.min(super.f2274d0.f2403y, super.f2282l0.mo23471k() * super.f2274d0.mo13267s()) + super.f2274d0.mo13239e();
                    }
                }
            } else if (i == 2) {
                int i4 = C1643a.f2260a[super.f2274d0.mo13270v().ordinal()];
                if (i4 == 1) {
                    rectF.top += Math.min(super.f2274d0.f2403y, super.f2282l0.mo23471k() * super.f2274d0.mo13267s()) + super.f2274d0.mo13239e();
                } else if (i4 == 2) {
                    rectF.bottom += Math.min(super.f2274d0.f2403y, super.f2282l0.mo23471k() * super.f2274d0.mo13267s()) + super.f2274d0.mo13239e();
                }
            }
        }
    }

    /* renamed from: b */
    public void mo12955b(float f, float f2, float f3, float f4) {
        super.f2282l0.mo23446a(f, f2, f3, -f4, this.f2253Z0);
        super.f2282l0.mo23444a(this.f2253Z0, this, false);
        mo12961d();
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo12959c(Canvas canvas) {
        if (this.f2237J0) {
            canvas.drawRect(super.f2282l0.mo23476n(), this.f2235H0);
        }
        if (this.f2238K0) {
            canvas.drawRect(super.f2282l0.mo23476n(), this.f2236I0);
        }
    }

    public void computeScroll() {
        ChartTouchListener bVar = super.f2276f0;
        if (bVar instanceof BarLineChartTouchListener) {
            ((BarLineChartTouchListener) bVar).mo23303a();
        }
    }

    /* renamed from: d */
    public void mo12961d() {
        if (!this.f2254a1) {
            mo12954a(this.f2252Y0);
            RectF rectF = this.f2252Y0;
            float f = rectF.left + 0.0f;
            float f2 = rectF.top + 0.0f;
            float f3 = rectF.right + 0.0f;
            float f4 = rectF.bottom + 0.0f;
            if (this.f2243P0.mo13297L()) {
                f += this.f2243P0.mo13299b(this.f2245R0.mo23334a());
            }
            if (this.f2244Q0.mo13297L()) {
                f3 += this.f2244Q0.mo13299b(this.f2246S0.mo23334a());
            }
            if (super.f2271a0.mo13240f() && super.f2271a0.mo13225v()) {
                XAxis hVar = super.f2271a0;
                float e = ((float) hVar.f2448M) + hVar.mo13239e();
                if (super.f2271a0.mo13282A() == XAxis.C1654a.BOTTOM) {
                    f4 += e;
                } else {
                    if (super.f2271a0.mo13282A() != XAxis.C1654a.TOP) {
                        if (super.f2271a0.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                            f4 += e;
                        }
                    }
                    f2 += e;
                }
            }
            float extraTopOffset = f2 + getExtraTopOffset();
            float extraRightOffset = f3 + getExtraRightOffset();
            float extraBottomOffset = f4 + getExtraBottomOffset();
            float extraLeftOffset = f + getExtraLeftOffset();
            float a = Utils.m11599a(this.f2240M0);
            super.f2282l0.mo23445a(Math.max(a, extraLeftOffset), Math.max(a, extraTopOffset), Math.max(a, extraRightOffset), Math.max(a, extraBottomOffset));
            if (super.f2263P) {
                Log.i("MPAndroidChart", "offsetLeft: " + extraLeftOffset + ", offsetTop: " + extraTopOffset + ", offsetRight: " + extraRightOffset + ", offsetBottom: " + extraBottomOffset);
                StringBuilder sb = new StringBuilder();
                sb.append("Content: ");
                sb.append(super.f2282l0.mo23476n().toString());
                Log.i("MPAndroidChart", sb.toString());
            }
        }
        mo12950C();
        mo12951D();
    }

    public YAxis getAxisLeft() {
        return this.f2243P0;
    }

    public YAxis getAxisRight() {
        return this.f2244Q0;
    }

    public /* bridge */ /* synthetic */ BarLineScatterCandleBubbleData getData() {
        return (BarLineScatterCandleBubbleData) super.getData();
    }

    public OnDrawListener getDrawListener() {
        return this.f2242O0;
    }

    public float getHighestVisibleX() {
        mo12952a(YAxis.C1655a.LEFT).mo23430a(super.f2282l0.mo23465h(), super.f2282l0.mo23459e(), this.f2256c1);
        return (float) Math.min((double) super.f2271a0.f2349G, this.f2256c1.f7120R);
    }

    public float getLowestVisibleX() {
        mo12952a(YAxis.C1655a.LEFT).mo23430a(super.f2282l0.mo23463g(), super.f2282l0.mo23459e(), this.f2255b1);
        return (float) Math.max((double) super.f2271a0.f2350H, this.f2255b1.f7120R);
    }

    public int getMaxVisibleCount() {
        return this.f2258y0;
    }

    public float getMinOffset() {
        return this.f2240M0;
    }

    public YAxisRenderer getRendererLeftYAxis() {
        return this.f2245R0;
    }

    public YAxisRenderer getRendererRightYAxis() {
        return this.f2246S0;
    }

    public XAxisRenderer getRendererXAxis() {
        return this.f2249V0;
    }

    public float getScaleX() {
        ViewPortHandler jVar = super.f2282l0;
        if (jVar == null) {
            return 1.0f;
        }
        return jVar.mo23478p();
    }

    public float getScaleY() {
        ViewPortHandler jVar = super.f2282l0;
        if (jVar == null) {
            return 1.0f;
        }
        return jVar.mo23479q();
    }

    public float getVisibleXRange() {
        return Math.abs(getHighestVisibleX() - getLowestVisibleX());
    }

    public float getYChartMax() {
        return Math.max(this.f2243P0.f2349G, this.f2244Q0.f2349G);
    }

    public float getYChartMin() {
        return Math.min(this.f2243P0.f2350H, this.f2244Q0.f2350H);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2243P0 = new YAxis(YAxis.C1655a.LEFT);
        this.f2244Q0 = new YAxis(YAxis.C1655a.RIGHT);
        this.f2247T0 = new Transformer(super.f2282l0);
        this.f2248U0 = new Transformer(super.f2282l0);
        this.f2245R0 = new YAxisRenderer(super.f2282l0, this.f2243P0, this.f2247T0);
        this.f2246S0 = new YAxisRenderer(super.f2282l0, this.f2244Q0, this.f2248U0);
        this.f2249V0 = new XAxisRenderer(super.f2282l0, super.f2271a0, this.f2247T0);
        setHighlighter(new ChartHighlighter(this));
        super.f2276f0 = new BarLineChartTouchListener(this, super.f2282l0.mo23477o(), 3.0f);
        this.f2235H0 = new Paint();
        this.f2235H0.setStyle(Paint.Style.FILL);
        this.f2235H0.setColor(Color.rgb((int) PsExtractor.VIDEO_STREAM_MASK, (int) PsExtractor.VIDEO_STREAM_MASK, (int) PsExtractor.VIDEO_STREAM_MASK));
        this.f2236I0 = new Paint();
        this.f2236I0.setStyle(Paint.Style.STROKE);
        this.f2236I0.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.f2236I0.setStrokeWidth(Utils.m11599a(1.0f));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.a.a.i.q.a(float, float, boolean):void
     arg types: [float, float, int]
     candidates:
      e.c.a.a.i.q.a(android.graphics.Canvas, float, e.c.a.a.j.e):void
      e.c.a.a.i.q.a(android.graphics.Canvas, com.github.mikephil.charting.components.g, float[]):void
      e.c.a.a.i.q.a(float, float, boolean):void */
    /* renamed from: m */
    public void mo12978m() {
        if (super.f2264Q != null) {
            if (super.f2263P) {
                Log.i("MPAndroidChart", "Preparing...");
            }
            DataRenderer gVar = super.f2280j0;
            if (gVar != null) {
                gVar.mo23337a();
            }
            mo12943p();
            YAxisRenderer tVar = this.f2245R0;
            YAxis iVar = this.f2243P0;
            tVar.mo23336a(iVar.f2350H, iVar.f2349G, iVar.mo13296K());
            YAxisRenderer tVar2 = this.f2246S0;
            YAxis iVar2 = this.f2244Q0;
            tVar2.mo23336a(iVar2.f2350H, iVar2.f2349G, iVar2.mo13296K());
            XAxisRenderer qVar = this.f2249V0;
            XAxis hVar = super.f2271a0;
            qVar.mo23336a(hVar.f2350H, hVar.f2349G, false);
            if (super.f2274d0 != null) {
                super.f2279i0.mo23361a((ChartData<?>) super.f2264Q);
            }
            mo12961d();
        } else if (super.f2263P) {
            Log.i("MPAndroidChart", "Preparing... DATA NOT SET.");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public void mo12979o() {
        ((BarLineScatterCandleBubbleData) super.f2264Q).mo13378a(getLowestVisibleX(), getHighestVisibleX());
        super.f2271a0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13390g(), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13389f());
        if (this.f2243P0.mo13240f()) {
            this.f2243P0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13382b(YAxis.C1655a.LEFT), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13372a(YAxis.C1655a.LEFT));
        }
        if (this.f2244Q0.mo13240f()) {
            this.f2244Q0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13382b(YAxis.C1655a.RIGHT), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13372a(YAxis.C1655a.RIGHT));
        }
        mo12961d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.a.a.i.q.a(float, float, boolean):void
     arg types: [float, float, int]
     candidates:
      e.c.a.a.i.q.a(android.graphics.Canvas, float, e.c.a.a.j.e):void
      e.c.a.a.i.q.a(android.graphics.Canvas, com.github.mikephil.charting.components.g, float[]):void
      e.c.a.a.i.q.a(float, float, boolean):void */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (super.f2264Q != null) {
            long currentTimeMillis = System.currentTimeMillis();
            mo12959c(canvas);
            if (this.f2259z0) {
                mo12979o();
            }
            if (this.f2243P0.mo13240f()) {
                YAxisRenderer tVar = this.f2245R0;
                YAxis iVar = this.f2243P0;
                tVar.mo23336a(iVar.f2350H, iVar.f2349G, iVar.mo13296K());
            }
            if (this.f2244Q0.mo13240f()) {
                YAxisRenderer tVar2 = this.f2246S0;
                YAxis iVar2 = this.f2244Q0;
                tVar2.mo23336a(iVar2.f2350H, iVar2.f2349G, iVar2.mo13296K());
            }
            if (super.f2271a0.mo13240f()) {
                XAxisRenderer qVar = this.f2249V0;
                XAxis hVar = super.f2271a0;
                qVar.mo23336a(hVar.f2350H, hVar.f2349G, false);
            }
            this.f2249V0.mo23402b(canvas);
            this.f2245R0.mo23412c(canvas);
            this.f2246S0.mo23412c(canvas);
            if (super.f2271a0.mo13223t()) {
                this.f2249V0.mo23404c(canvas);
            }
            if (this.f2243P0.mo13223t()) {
                this.f2245R0.mo23414d(canvas);
            }
            if (this.f2244Q0.mo13223t()) {
                this.f2246S0.mo23414d(canvas);
            }
            if (super.f2271a0.mo13240f() && super.f2271a0.mo13226w()) {
                this.f2249V0.mo23406d(canvas);
            }
            if (this.f2243P0.mo13240f() && this.f2243P0.mo13226w()) {
                this.f2245R0.mo23415e(canvas);
            }
            if (this.f2244Q0.mo13240f() && this.f2244Q0.mo13226w()) {
                this.f2246S0.mo23415e(canvas);
            }
            int save = canvas.save();
            canvas.clipRect(super.f2282l0.mo23476n());
            super.f2280j0.mo23339a(canvas);
            if (!super.f2271a0.mo13223t()) {
                this.f2249V0.mo23404c(canvas);
            }
            if (!this.f2243P0.mo13223t()) {
                this.f2245R0.mo23414d(canvas);
            }
            if (!this.f2244Q0.mo13223t()) {
                this.f2246S0.mo23414d(canvas);
            }
            if (mo13072n()) {
                super.f2280j0.mo23342a(canvas, super.f2289s0);
            }
            canvas.restoreToCount(save);
            super.f2280j0.mo23344b(canvas);
            if (super.f2271a0.mo13240f() && !super.f2271a0.mo13226w()) {
                this.f2249V0.mo23406d(canvas);
            }
            if (this.f2243P0.mo13240f() && !this.f2243P0.mo13226w()) {
                this.f2245R0.mo23415e(canvas);
            }
            if (this.f2244Q0.mo13240f() && !this.f2244Q0.mo13226w()) {
                this.f2246S0.mo23415e(canvas);
            }
            this.f2249V0.mo23395a(canvas);
            this.f2245R0.mo23411b(canvas);
            this.f2246S0.mo23411b(canvas);
            if (mo12985s()) {
                int save2 = canvas.save();
                canvas.clipRect(super.f2282l0.mo23476n());
                super.f2280j0.mo23345c(canvas);
                canvas.restoreToCount(save2);
            } else {
                super.f2280j0.mo23345c(canvas);
            }
            super.f2279i0.mo23358a(canvas);
            mo13025a(canvas);
            mo13032b(canvas);
            if (super.f2263P) {
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                this.f2250W0 += currentTimeMillis2;
                this.f2251X0++;
                Log.i("MPAndroidChart", "Drawtime: " + currentTimeMillis2 + " ms, average: " + (this.f2250W0 / this.f2251X0) + " ms, cycles: " + this.f2251X0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        float[] fArr = this.f2257d1;
        fArr[1] = 0.0f;
        fArr[0] = 0.0f;
        if (this.f2241N0) {
            fArr[0] = super.f2282l0.mo23463g();
            this.f2257d1[1] = super.f2282l0.mo23467i();
            mo12952a(YAxis.C1655a.LEFT).mo23435a(this.f2257d1);
        }
        super.onSizeChanged(i, i2, i3, i4);
        if (this.f2241N0) {
            mo12952a(YAxis.C1655a.LEFT).mo23442b(this.f2257d1);
            super.f2282l0.mo23448a(this.f2257d1, this);
            return;
        }
        ViewPortHandler jVar = super.f2282l0;
        jVar.mo23444a(jVar.mo23477o(), this, true);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        ChartTouchListener bVar = super.f2276f0;
        if (bVar == null || super.f2264Q == null || !super.f2272b0) {
            return false;
        }
        return bVar.onTouch(this, motionEvent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public void mo12943p() {
        super.f2271a0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13390g(), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13389f());
        this.f2243P0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13382b(YAxis.C1655a.LEFT), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13372a(YAxis.C1655a.LEFT));
        this.f2244Q0.mo13199a(((BarLineScatterCandleBubbleData) super.f2264Q).mo13382b(YAxis.C1655a.RIGHT), ((BarLineScatterCandleBubbleData) super.f2264Q).mo13372a(YAxis.C1655a.RIGHT));
    }

    /* renamed from: q */
    public boolean mo12983q() {
        return super.f2282l0.mo23482t();
    }

    /* renamed from: r */
    public boolean mo12984r() {
        if (!this.f2243P0.mo13296K() && !this.f2244Q0.mo13296K()) {
            return false;
        }
        return true;
    }

    /* renamed from: s */
    public boolean mo12985s() {
        return this.f2239L0;
    }

    public void setAutoScaleMinMaxEnabled(boolean z) {
        this.f2259z0 = z;
    }

    public void setBorderColor(int i) {
        this.f2236I0.setColor(i);
    }

    public void setBorderWidth(float f) {
        this.f2236I0.setStrokeWidth(Utils.m11599a(f));
    }

    public void setClipValuesToContent(boolean z) {
        this.f2239L0 = z;
    }

    public void setDoubleTapToZoomEnabled(boolean z) {
        this.f2229B0 = z;
    }

    public void setDragEnabled(boolean z) {
        this.f2231D0 = z;
        this.f2232E0 = z;
    }

    public void setDragOffsetX(float f) {
        super.f2282l0.mo23464g(f);
    }

    public void setDragOffsetY(float f) {
        super.f2282l0.mo23466h(f);
    }

    public void setDragXEnabled(boolean z) {
        this.f2231D0 = z;
    }

    public void setDragYEnabled(boolean z) {
        this.f2232E0 = z;
    }

    public void setDrawBorders(boolean z) {
        this.f2238K0 = z;
    }

    public void setDrawGridBackground(boolean z) {
        this.f2237J0 = z;
    }

    public void setGridBackgroundColor(int i) {
        this.f2235H0.setColor(i);
    }

    public void setHighlightPerDragEnabled(boolean z) {
        this.f2230C0 = z;
    }

    public void setKeepPositionOnRotation(boolean z) {
        this.f2241N0 = z;
    }

    public void setMaxVisibleValueCount(int i) {
        this.f2258y0 = i;
    }

    public void setMinOffset(float f) {
        this.f2240M0 = f;
    }

    public void setOnDrawListener(OnDrawListener eVar) {
        this.f2242O0 = eVar;
    }

    public void setPinchZoom(boolean z) {
        this.f2228A0 = z;
    }

    public void setRendererLeftYAxis(YAxisRenderer tVar) {
        this.f2245R0 = tVar;
    }

    public void setRendererRightYAxis(YAxisRenderer tVar) {
        this.f2246S0 = tVar;
    }

    public void setScaleEnabled(boolean z) {
        this.f2233F0 = z;
        this.f2234G0 = z;
    }

    public void setScaleXEnabled(boolean z) {
        this.f2233F0 = z;
    }

    public void setScaleYEnabled(boolean z) {
        this.f2234G0 = z;
    }

    public void setVisibleXRangeMaximum(float f) {
        super.f2282l0.mo23472k(super.f2271a0.f2351I / f);
    }

    public void setVisibleXRangeMinimum(float f) {
        super.f2282l0.mo23468i(super.f2271a0.f2351I / f);
    }

    public void setXAxisRenderer(XAxisRenderer qVar) {
        this.f2249V0 = qVar;
    }

    /* renamed from: t */
    public boolean mo13013t() {
        return this.f2229B0;
    }

    /* renamed from: u */
    public boolean mo13014u() {
        return this.f2231D0 || this.f2232E0;
    }

    /* renamed from: v */
    public boolean mo13015v() {
        return this.f2231D0;
    }

    /* renamed from: w */
    public boolean mo13016w() {
        return this.f2232E0;
    }

    /* renamed from: x */
    public boolean mo13017x() {
        return super.f2282l0.mo23483u();
    }

    /* renamed from: y */
    public boolean mo13018y() {
        return this.f2230C0;
    }

    /* renamed from: z */
    public boolean mo13019z() {
        return this.f2228A0;
    }

    /* renamed from: b */
    public boolean mo12956b(YAxis.C1655a aVar) {
        return mo12957c(aVar).mo13296K();
    }

    /* renamed from: c */
    public IBarLineScatterCandleBubbleDataSet mo12958c(float f, float f2) {
        Highlight a = mo12937a(f, f2);
        if (a != null) {
            return (IBarLineScatterCandleBubbleDataSet) ((BarLineScatterCandleBubbleData) super.f2264Q).mo13374a(a.mo23245c());
        }
        return null;
    }

    /* renamed from: c */
    public YAxis mo12957c(YAxis.C1655a aVar) {
        if (aVar == YAxis.C1655a.LEFT) {
            return this.f2243P0;
        }
        return this.f2244Q0;
    }

    public BarLineChartBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new Matrix();
        this.f2254a1 = false;
        this.f2255b1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2256c1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2257d1 = new float[2];
    }

    /* renamed from: a */
    public Transformer mo12952a(YAxis.C1655a aVar) {
        if (aVar == YAxis.C1655a.LEFT) {
            return this.f2247T0;
        }
        return this.f2248U0;
    }

    /* renamed from: a */
    public void mo12953a(float f) {
        mo13027a(MoveViewJob.m11363a(super.f2282l0, f, 0.0f, mo12952a(YAxis.C1655a.LEFT), this));
    }

    public BarLineChartBase(Context context) {
        super(context);
        new Matrix();
        this.f2254a1 = false;
        this.f2255b1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2256c1 = MPPointD.m11564a(0.0d, 0.0d);
        this.f2257d1 = new float[2];
    }
}
