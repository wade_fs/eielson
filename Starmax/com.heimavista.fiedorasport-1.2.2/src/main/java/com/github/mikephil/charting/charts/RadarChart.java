package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.RadarData;
import p119e.p128c.p129a.p130a.p134d.RadarHighlighter;
import p119e.p128c.p129a.p130a.p135e.p137b.IRadarDataSet;
import p119e.p128c.p129a.p130a.p141i.RadarChartRenderer;
import p119e.p128c.p129a.p130a.p141i.XAxisRendererRadarChart;
import p119e.p128c.p129a.p130a.p141i.YAxisRendererRadarChart;
import p119e.p128c.p129a.p130a.p143j.Utils;

public class RadarChart extends PieRadarChartBase<RadarData> {

    /* renamed from: C0 */
    private float f2330C0 = 2.5f;

    /* renamed from: D0 */
    private float f2331D0 = 1.5f;

    /* renamed from: E0 */
    private int f2332E0 = Color.rgb(122, 122, 122);

    /* renamed from: F0 */
    private int f2333F0 = Color.rgb(122, 122, 122);

    /* renamed from: G0 */
    private int f2334G0 = 150;

    /* renamed from: H0 */
    private boolean f2335H0 = true;

    /* renamed from: I0 */
    private int f2336I0 = 0;

    /* renamed from: J0 */
    private YAxis f2337J0;

    /* renamed from: K0 */
    protected YAxisRendererRadarChart f2338K0;

    /* renamed from: L0 */
    protected XAxisRendererRadarChart f2339L0;

    public RadarChart(Context context) {
        super(context);
    }

    /* renamed from: a */
    public int mo13113a(float f) {
        float c = Utils.m11619c(f - getRotationAngle());
        float sliceAngle = getSliceAngle();
        int t = ((IRadarDataSet) ((RadarData) this.f2264Q).mo13388e()).mo13417t();
        int i = 0;
        while (i < t) {
            int i2 = i + 1;
            if ((((float) i2) * sliceAngle) - (sliceAngle / 2.0f) > c) {
                return i;
            }
            i = i2;
        }
        return 0;
    }

    public float getFactor() {
        RectF n = this.f2282l0.mo23476n();
        return Math.min(n.width() / 2.0f, n.height() / 2.0f) / this.f2337J0.f2351I;
    }

    public float getRadius() {
        RectF n = this.f2282l0.mo23476n();
        return Math.min(n.width() / 2.0f, n.height() / 2.0f);
    }

    /* access modifiers changed from: protected */
    public float getRequiredBaseOffset() {
        if (!this.f2271a0.mo13240f() || !this.f2271a0.mo13225v()) {
            return Utils.m11599a(10.0f);
        }
        return (float) this.f2271a0.f2447L;
    }

    /* access modifiers changed from: protected */
    public float getRequiredLegendOffset() {
        return this.f2279i0.mo23357a().getTextSize() * 4.0f;
    }

    public int getSkipWebLineCount() {
        return this.f2336I0;
    }

    public float getSliceAngle() {
        return 360.0f / ((float) ((IRadarDataSet) ((RadarData) this.f2264Q).mo13388e()).mo13417t());
    }

    public int getWebAlpha() {
        return this.f2334G0;
    }

    public int getWebColor() {
        return this.f2332E0;
    }

    public int getWebColorInner() {
        return this.f2333F0;
    }

    public float getWebLineWidth() {
        return this.f2330C0;
    }

    public float getWebLineWidthInner() {
        return this.f2331D0;
    }

    public YAxis getYAxis() {
        return this.f2337J0;
    }

    public float getYChartMax() {
        return this.f2337J0.f2349G;
    }

    public float getYChartMin() {
        return this.f2337J0.f2350H;
    }

    public float getYRange() {
        return this.f2337J0.f2351I;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2337J0 = new YAxis(YAxis.C1655a.LEFT);
        this.f2330C0 = Utils.m11599a(1.5f);
        this.f2331D0 = Utils.m11599a(0.75f);
        this.f2280j0 = new RadarChartRenderer(this, this.f2283m0, this.f2282l0);
        this.f2338K0 = new YAxisRendererRadarChart(this.f2282l0, this.f2337J0, this);
        this.f2339L0 = new XAxisRendererRadarChart(this.f2282l0, this.f2271a0, this);
        this.f2281k0 = new RadarHighlighter(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.a.a.i.q.a(float, float, boolean):void
     arg types: [float, float, int]
     candidates:
      e.c.a.a.i.q.a(android.graphics.Canvas, float, e.c.a.a.j.e):void
      e.c.a.a.i.q.a(android.graphics.Canvas, com.github.mikephil.charting.components.g, float[]):void
      e.c.a.a.i.q.a(float, float, boolean):void */
    /* renamed from: m */
    public void mo12978m() {
        if (this.f2264Q != null) {
            mo13129o();
            YAxisRendererRadarChart vVar = this.f2338K0;
            YAxis iVar = this.f2337J0;
            vVar.mo23336a(iVar.f2350H, iVar.f2349G, iVar.mo13296K());
            XAxisRendererRadarChart sVar = this.f2339L0;
            XAxis hVar = this.f2271a0;
            sVar.mo23336a(hVar.f2350H, hVar.f2349G, false);
            Legend eVar = this.f2274d0;
            if (eVar != null && !eVar.mo13274z()) {
                this.f2279i0.mo23361a((ChartData<?>) this.f2264Q);
            }
            mo12961d();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public void mo13129o() {
        super.mo13129o();
        this.f2337J0.mo13199a(((RadarData) this.f2264Q).mo13382b(YAxis.C1655a.LEFT), ((RadarData) this.f2264Q).mo13372a(YAxis.C1655a.LEFT));
        this.f2271a0.mo13199a(0.0f, (float) ((IRadarDataSet) ((RadarData) this.f2264Q).mo13388e()).mo13417t());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.a.a.i.q.a(float, float, boolean):void
     arg types: [float, float, int]
     candidates:
      e.c.a.a.i.q.a(android.graphics.Canvas, float, e.c.a.a.j.e):void
      e.c.a.a.i.q.a(android.graphics.Canvas, com.github.mikephil.charting.components.g, float[]):void
      e.c.a.a.i.q.a(float, float, boolean):void */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f2264Q != null) {
            if (this.f2271a0.mo13240f()) {
                XAxisRendererRadarChart sVar = this.f2339L0;
                XAxis hVar = this.f2271a0;
                sVar.mo23336a(hVar.f2350H, hVar.f2349G, false);
            }
            this.f2339L0.mo23395a(canvas);
            if (this.f2335H0) {
                this.f2280j0.mo23344b(canvas);
            }
            if (this.f2337J0.mo13240f() && this.f2337J0.mo13226w()) {
                this.f2338K0.mo23415e(canvas);
            }
            this.f2280j0.mo23339a(canvas);
            if (mo13072n()) {
                this.f2280j0.mo23342a(canvas, this.f2289s0);
            }
            if (this.f2337J0.mo13240f() && !this.f2337J0.mo13226w()) {
                this.f2338K0.mo23415e(canvas);
            }
            this.f2338K0.mo23411b(canvas);
            this.f2280j0.mo23345c(canvas);
            this.f2279i0.mo23358a(canvas);
            mo13025a(canvas);
            mo13032b(canvas);
        }
    }

    public void setDrawWeb(boolean z) {
        this.f2335H0 = z;
    }

    public void setSkipWebLineCount(int i) {
        this.f2336I0 = Math.max(0, i);
    }

    public void setWebAlpha(int i) {
        this.f2334G0 = i;
    }

    public void setWebColor(int i) {
        this.f2332E0 = i;
    }

    public void setWebColorInner(int i) {
        this.f2333F0 = i;
    }

    public void setWebLineWidth(float f) {
        this.f2330C0 = Utils.m11599a(f);
    }

    public void setWebLineWidthInner(float f) {
        this.f2331D0 = Utils.m11599a(f);
    }

    public RadarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RadarChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
