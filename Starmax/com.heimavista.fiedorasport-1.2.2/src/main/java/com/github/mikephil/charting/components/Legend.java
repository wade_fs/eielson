package com.github.mikephil.charting.components;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p143j.FSize;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: com.github.mikephil.charting.components.e */
public class Legend extends ComponentBase {

    /* renamed from: A */
    private boolean f2381A = false;

    /* renamed from: B */
    private List<FSize> f2382B = new ArrayList(16);

    /* renamed from: C */
    private List<Boolean> f2383C = new ArrayList(16);

    /* renamed from: D */
    private List<FSize> f2384D = new ArrayList(16);

    /* renamed from: g */
    private LegendEntry[] f2385g = new LegendEntry[0];

    /* renamed from: h */
    private LegendEntry[] f2386h;

    /* renamed from: i */
    private boolean f2387i = false;

    /* renamed from: j */
    private C1650d f2388j = C1650d.LEFT;

    /* renamed from: k */
    private C1652f f2389k = C1652f.BOTTOM;

    /* renamed from: l */
    private C1651e f2390l = C1651e.HORIZONTAL;

    /* renamed from: m */
    private boolean f2391m = false;

    /* renamed from: n */
    private C1648b f2392n = C1648b.LEFT_TO_RIGHT;

    /* renamed from: o */
    private C1649c f2393o = C1649c.SQUARE;

    /* renamed from: p */
    private float f2394p = 8.0f;

    /* renamed from: q */
    private float f2395q = 3.0f;

    /* renamed from: r */
    private DashPathEffect f2396r = null;

    /* renamed from: s */
    private float f2397s = 6.0f;

    /* renamed from: t */
    private float f2398t = 0.0f;

    /* renamed from: u */
    private float f2399u = 5.0f;

    /* renamed from: v */
    private float f2400v = 3.0f;

    /* renamed from: w */
    private float f2401w = 0.95f;

    /* renamed from: x */
    public float f2402x = 0.0f;

    /* renamed from: y */
    public float f2403y = 0.0f;

    /* renamed from: z */
    public float f2404z = 0.0f;

    /* renamed from: com.github.mikephil.charting.components.e$a */
    /* compiled from: Legend */
    static /* synthetic */ class C1647a {

        /* renamed from: a */
        static final /* synthetic */ int[] f2405a = new int[C1651e.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.github.mikephil.charting.components.e$e[] r0 = com.github.mikephil.charting.components.Legend.C1651e.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.github.mikephil.charting.components.Legend.C1647a.f2405a = r0
                int[] r0 = com.github.mikephil.charting.components.Legend.C1647a.f2405a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.components.e$e r1 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.github.mikephil.charting.components.Legend.C1647a.f2405a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.components.e$e r1 = com.github.mikephil.charting.components.Legend.C1651e.HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.github.mikephil.charting.components.Legend.C1647a.<clinit>():void");
        }
    }

    /* renamed from: com.github.mikephil.charting.components.e$b */
    /* compiled from: Legend */
    public enum C1648b {
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT
    }

    /* renamed from: com.github.mikephil.charting.components.e$c */
    /* compiled from: Legend */
    public enum C1649c {
        NONE,
        EMPTY,
        f2411R,
        SQUARE,
        CIRCLE,
        LINE
    }

    /* renamed from: com.github.mikephil.charting.components.e$d */
    /* compiled from: Legend */
    public enum C1650d {
        LEFT,
        CENTER,
        RIGHT
    }

    /* renamed from: com.github.mikephil.charting.components.e$e */
    /* compiled from: Legend */
    public enum C1651e {
        HORIZONTAL,
        VERTICAL
    }

    /* renamed from: com.github.mikephil.charting.components.e$f */
    /* compiled from: Legend */
    public enum C1652f {
        TOP,
        CENTER,
        BOTTOM
    }

    public Legend() {
        super.f2376e = Utils.m11599a(10.0f);
        super.f2373b = Utils.m11599a(5.0f);
        super.f2374c = Utils.m11599a(3.0f);
    }

    /* renamed from: a */
    public void mo13249a(List<LegendEntry> list) {
        this.f2385g = (LegendEntry[]) list.toArray(new LegendEntry[list.size()]);
    }

    /* renamed from: b */
    public float mo13250b(Paint paint) {
        float a = Utils.m11599a(this.f2399u);
        LegendEntry[] fVarArr = this.f2385g;
        float f = 0.0f;
        float f2 = 0.0f;
        for (LegendEntry fVar : fVarArr) {
            float a2 = Utils.m11599a(Float.isNaN(fVar.f2429c) ? this.f2394p : fVar.f2429c);
            if (a2 > f2) {
                f2 = a2;
            }
            String str = fVar.f2427a;
            if (str != null) {
                float c = (float) Utils.m11621c(paint, str);
                if (c > f) {
                    f = c;
                }
            }
        }
        return f + f2 + a;
    }

    /* renamed from: d */
    public void mo13252d(float f) {
        this.f2394p = f;
    }

    /* renamed from: e */
    public void mo13253e(float f) {
        this.f2397s = f;
    }

    /* renamed from: f */
    public void mo13254f(float f) {
        this.f2398t = f;
    }

    /* renamed from: g */
    public List<Boolean> mo13255g() {
        return this.f2383C;
    }

    /* renamed from: h */
    public List<FSize> mo13256h() {
        return this.f2382B;
    }

    /* renamed from: i */
    public List<FSize> mo13257i() {
        return this.f2384D;
    }

    /* renamed from: j */
    public C1648b mo13258j() {
        return this.f2392n;
    }

    /* renamed from: k */
    public LegendEntry[] mo13259k() {
        return this.f2385g;
    }

    /* renamed from: l */
    public LegendEntry[] mo13260l() {
        return this.f2386h;
    }

    /* renamed from: m */
    public C1649c mo13261m() {
        return this.f2393o;
    }

    /* renamed from: n */
    public DashPathEffect mo13262n() {
        return this.f2396r;
    }

    /* renamed from: o */
    public float mo13263o() {
        return this.f2395q;
    }

    /* renamed from: p */
    public float mo13264p() {
        return this.f2394p;
    }

    /* renamed from: q */
    public float mo13265q() {
        return this.f2399u;
    }

    /* renamed from: r */
    public C1650d mo13266r() {
        return this.f2388j;
    }

    /* renamed from: s */
    public float mo13267s() {
        return this.f2401w;
    }

    /* renamed from: t */
    public C1651e mo13268t() {
        return this.f2390l;
    }

    /* renamed from: u */
    public float mo13269u() {
        return this.f2400v;
    }

    /* renamed from: v */
    public C1652f mo13270v() {
        return this.f2389k;
    }

    /* renamed from: w */
    public float mo13271w() {
        return this.f2397s;
    }

    /* renamed from: x */
    public float mo13272x() {
        return this.f2398t;
    }

    /* renamed from: y */
    public boolean mo13273y() {
        return this.f2391m;
    }

    /* renamed from: z */
    public boolean mo13274z() {
        return this.f2387i;
    }

    /* renamed from: a */
    public float mo13244a(Paint paint) {
        float f = 0.0f;
        for (LegendEntry fVar : this.f2385g) {
            String str = fVar.f2427a;
            if (str != null) {
                float a = (float) Utils.m11602a(paint, str);
                if (a > f) {
                    f = a;
                }
            }
        }
        return f;
    }

    /* renamed from: a */
    public void mo13246a(C1650d dVar) {
        this.f2388j = dVar;
    }

    /* renamed from: a */
    public void mo13248a(C1652f fVar) {
        this.f2389k = fVar;
    }

    /* renamed from: a */
    public void mo13247a(C1651e eVar) {
        this.f2390l = eVar;
    }

    /* renamed from: a */
    public void mo13245a(Paint paint, ViewPortHandler jVar) {
        float f;
        int i;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        Paint paint2 = paint;
        float a = Utils.m11599a(this.f2394p);
        float a2 = Utils.m11599a(this.f2400v);
        float a3 = Utils.m11599a(this.f2399u);
        float a4 = Utils.m11599a(this.f2397s);
        float a5 = Utils.m11599a(this.f2398t);
        boolean z = this.f2381A;
        LegendEntry[] fVarArr = this.f2385g;
        int length = fVarArr.length;
        mo13250b(paint);
        this.f2404z = mo13244a(paint);
        int i2 = C1647a.f2405a[this.f2390l.ordinal()];
        if (i2 == 1) {
            float f7 = a;
            float f8 = a2;
            LegendEntry[] fVarArr2 = fVarArr;
            float a6 = Utils.m11600a(paint);
            float f9 = 0.0f;
            float f10 = 0.0f;
            boolean z2 = false;
            float f11 = 0.0f;
            for (int i3 = 0; i3 < length; i3++) {
                LegendEntry fVar = fVarArr2[i3];
                boolean z3 = fVar.f2428b != C1649c.NONE;
                if (Float.isNaN(fVar.f2429c)) {
                    f = f7;
                } else {
                    f = Utils.m11599a(fVar.f2429c);
                }
                String str = fVar.f2427a;
                if (!z2) {
                    f11 = 0.0f;
                }
                if (z3) {
                    if (z2) {
                        f11 += f8;
                    }
                    f11 += f;
                }
                if (str != null) {
                    if (z3 && !z2) {
                        f11 += a3;
                    } else if (z2) {
                        f9 = Math.max(f9, f11);
                        f10 += a6 + a5;
                        z2 = false;
                        f11 = 0.0f;
                    }
                    f11 += (float) Utils.m11621c(paint2, str);
                    if (i3 < length - 1) {
                        f10 += a6 + a5;
                    }
                } else {
                    f11 += f;
                    if (i3 < length - 1) {
                        f11 += f8;
                    }
                    z2 = true;
                }
                f9 = Math.max(f9, f11);
            }
            this.f2402x = f9;
            this.f2403y = f10;
        } else if (i2 == 2) {
            float a7 = Utils.m11600a(paint);
            float b = Utils.m11613b(paint) + a5;
            float j = jVar.mo23469j() * this.f2401w;
            this.f2383C.clear();
            this.f2382B.clear();
            this.f2384D.clear();
            int i4 = 0;
            int i5 = -1;
            float f12 = 0.0f;
            float f13 = 0.0f;
            float f14 = 0.0f;
            while (i4 < length) {
                LegendEntry fVar2 = fVarArr[i4];
                float f15 = a;
                boolean z4 = fVar2.f2428b != C1649c.NONE;
                if (Float.isNaN(fVar2.f2429c)) {
                    f2 = f15;
                } else {
                    f2 = Utils.m11599a(fVar2.f2429c);
                }
                String str2 = fVar2.f2427a;
                float f16 = a4;
                LegendEntry[] fVarArr3 = fVarArr;
                this.f2383C.add(false);
                float f17 = i5 == -1 ? 0.0f : f13 + a2;
                if (str2 != null) {
                    f3 = a2;
                    this.f2382B.add(Utils.m11618b(paint2, str2));
                    f4 = f17 + (z4 ? a3 + f2 : 0.0f) + this.f2382B.get(i4).f7117R;
                } else {
                    f3 = a2;
                    float f18 = f2;
                    this.f2382B.add(FSize.m11561a(0.0f, 0.0f));
                    if (!z4) {
                        f18 = 0.0f;
                    }
                    f4 = f17 + f18;
                    if (i5 == -1) {
                        i5 = i4;
                    }
                }
                if (str2 != null || i4 == length - 1) {
                    float f19 = f14;
                    int i6 = (f19 > 0.0f ? 1 : (f19 == 0.0f ? 0 : -1));
                    float f20 = i6 == 0 ? 0.0f : f16;
                    if (!z || i6 == 0 || j - f19 >= f20 + f4) {
                        f5 = f12;
                        f6 = f19 + f20 + f4;
                    } else {
                        this.f2384D.add(FSize.m11561a(f19, a7));
                        float max = Math.max(f12, f19);
                        this.f2383C.set(i5 > -1 ? i5 : i4, true);
                        f6 = f4;
                        f5 = max;
                    }
                    if (i4 == length - 1) {
                        this.f2384D.add(FSize.m11561a(f6, a7));
                        f14 = f6;
                        f12 = Math.max(f5, f6);
                    } else {
                        f14 = f6;
                        f12 = f5;
                    }
                }
                if (str2 != null) {
                    i5 = -1;
                }
                i4++;
                a2 = f3;
                a = f15;
                fVarArr = fVarArr3;
                f13 = f4;
                a4 = f16;
            }
            this.f2402x = f12;
            float size = a7 * ((float) this.f2384D.size());
            if (this.f2384D.size() == 0) {
                i = 0;
            } else {
                i = this.f2384D.size() - 1;
            }
            this.f2403y = size + (b * ((float) i));
        }
        this.f2403y += super.f2374c;
        this.f2402x += super.f2373b;
    }

    /* renamed from: b */
    public void mo13251b(boolean z) {
        this.f2391m = z;
    }
}
