package com.github.mikephil.charting.data;

import android.graphics.Typeface;
import com.github.mikephil.charting.components.YAxis;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;

/* renamed from: com.github.mikephil.charting.data.i */
public abstract class ChartData<T extends IDataSet<? extends Entry>> {

    /* renamed from: a */
    protected float f2517a;

    /* renamed from: b */
    protected float f2518b;

    /* renamed from: c */
    protected float f2519c;

    /* renamed from: d */
    protected float f2520d;

    /* renamed from: e */
    protected float f2521e;

    /* renamed from: f */
    protected float f2522f;

    /* renamed from: g */
    protected float f2523g;

    /* renamed from: h */
    protected float f2524h;

    /* renamed from: i */
    protected List<T> f2525i;

    public ChartData() {
        this.f2517a = -3.4028235E38f;
        this.f2518b = Float.MAX_VALUE;
        this.f2519c = -3.4028235E38f;
        this.f2520d = Float.MAX_VALUE;
        this.f2521e = -3.4028235E38f;
        this.f2522f = Float.MAX_VALUE;
        this.f2523g = -3.4028235E38f;
        this.f2524h = Float.MAX_VALUE;
        this.f2525i = new ArrayList();
    }

    /* renamed from: a */
    private List<T> m4064a(IDataSet[] eVarArr) {
        ArrayList arrayList = new ArrayList();
        for (IDataSet eVar : eVarArr) {
            arrayList.add(eVar);
        }
        return arrayList;
    }

    /* renamed from: b */
    public int mo13383b() {
        List<T> list = this.f2525i;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    /* renamed from: c */
    public List<T> mo13386c() {
        return this.f2525i;
    }

    /* renamed from: d */
    public int mo13387d() {
        int i = 0;
        for (T t : this.f2525i) {
            i += t.mo13417t();
        }
        return i;
    }

    /* renamed from: e */
    public T mo13388e() {
        List<T> list = this.f2525i;
        if (list == null || list.isEmpty()) {
            return null;
        }
        T t = (IDataSet) this.f2525i.get(0);
        for (T t2 : this.f2525i) {
            if (t2.mo13417t() > t.mo13417t()) {
                t = t2;
            }
        }
        return t;
    }

    /* renamed from: f */
    public float mo13389f() {
        return this.f2519c;
    }

    /* renamed from: g */
    public float mo13390g() {
        return this.f2520d;
    }

    /* renamed from: h */
    public float mo13391h() {
        return this.f2517a;
    }

    /* renamed from: i */
    public float mo13392i() {
        return this.f2518b;
    }

    /* renamed from: j */
    public void mo13393j() {
        mo13376a();
    }

    /* renamed from: b */
    public float mo13382b(YAxis.C1655a aVar) {
        if (aVar == YAxis.C1655a.LEFT) {
            float f = this.f2522f;
            return f == Float.MAX_VALUE ? this.f2524h : f;
        }
        float f2 = this.f2524h;
        return f2 == Float.MAX_VALUE ? this.f2522f : f2;
    }

    /* renamed from: a */
    public void mo13378a(float f, float f2) {
        for (T t : this.f2525i) {
            t.mo13412b(f, f2);
        }
        mo13376a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13376a() {
        List<T> list = this.f2525i;
        if (list != null) {
            this.f2517a = -3.4028235E38f;
            this.f2518b = Float.MAX_VALUE;
            this.f2519c = -3.4028235E38f;
            this.f2520d = Float.MAX_VALUE;
            for (T t : list) {
                mo13381a(t);
            }
            this.f2521e = -3.4028235E38f;
            this.f2522f = Float.MAX_VALUE;
            this.f2523g = -3.4028235E38f;
            this.f2524h = Float.MAX_VALUE;
            IDataSet a = mo13375a(this.f2525i);
            if (a != null) {
                this.f2521e = a.mo13408b();
                this.f2522f = a.mo13415g();
                for (T t2 : this.f2525i) {
                    if (t2.mo13364s() == YAxis.C1655a.LEFT) {
                        if (t2.mo13415g() < this.f2522f) {
                            this.f2522f = t2.mo13415g();
                        }
                        if (t2.mo13408b() > this.f2521e) {
                            this.f2521e = t2.mo13408b();
                        }
                    }
                }
            }
            IDataSet b = mo13384b(this.f2525i);
            if (b != null) {
                this.f2523g = b.mo13408b();
                this.f2524h = b.mo13415g();
                for (T t3 : this.f2525i) {
                    if (t3.mo13364s() == YAxis.C1655a.RIGHT) {
                        if (t3.mo13415g() < this.f2524h) {
                            this.f2524h = t3.mo13415g();
                        }
                        if (t3.mo13408b() > this.f2523g) {
                            this.f2523g = t3.mo13408b();
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public T mo13384b(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            T t = (IDataSet) it.next();
            if (t.mo13364s() == YAxis.C1655a.RIGHT) {
                return t;
            }
        }
        return null;
    }

    /* renamed from: b */
    public void mo13385b(int i) {
        for (T t : this.f2525i) {
            t.mo13337a(i);
        }
    }

    public ChartData(T... tArr) {
        this.f2517a = -3.4028235E38f;
        this.f2518b = Float.MAX_VALUE;
        this.f2519c = -3.4028235E38f;
        this.f2520d = Float.MAX_VALUE;
        this.f2521e = -3.4028235E38f;
        this.f2522f = Float.MAX_VALUE;
        this.f2523g = -3.4028235E38f;
        this.f2524h = Float.MAX_VALUE;
        this.f2525i = m4064a(tArr);
        mo13393j();
    }

    public ChartData(List<T> list) {
        this.f2517a = -3.4028235E38f;
        this.f2518b = Float.MAX_VALUE;
        this.f2519c = -3.4028235E38f;
        this.f2520d = Float.MAX_VALUE;
        this.f2521e = -3.4028235E38f;
        this.f2522f = Float.MAX_VALUE;
        this.f2523g = -3.4028235E38f;
        this.f2524h = Float.MAX_VALUE;
        this.f2525i = list;
        mo13393j();
    }

    /* renamed from: a */
    public float mo13372a(YAxis.C1655a aVar) {
        if (aVar == YAxis.C1655a.LEFT) {
            float f = this.f2521e;
            return f == -3.4028235E38f ? this.f2523g : f;
        }
        float f2 = this.f2523g;
        return f2 == -3.4028235E38f ? this.f2521e : f2;
    }

    /* renamed from: a */
    public Entry mo13373a(Highlight dVar) {
        if (dVar.mo23245c() >= this.f2525i.size()) {
            return null;
        }
        return ((IDataSet) this.f2525i.get(dVar.mo23245c())).mo13406a(dVar.mo23249g(), dVar.mo23251i());
    }

    /* renamed from: a */
    public T mo13374a(int i) {
        List<T> list = this.f2525i;
        if (list == null || i < 0 || i >= list.size()) {
            return null;
        }
        return (IDataSet) this.f2525i.get(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13381a(IDataSet eVar) {
        if (this.f2517a < eVar.mo13408b()) {
            this.f2517a = eVar.mo13408b();
        }
        if (this.f2518b > eVar.mo13415g()) {
            this.f2518b = eVar.mo13415g();
        }
        if (this.f2519c < eVar.mo13416q()) {
            this.f2519c = eVar.mo13416q();
        }
        if (this.f2520d > eVar.mo13404a()) {
            this.f2520d = eVar.mo13404a();
        }
        if (eVar.mo13364s() == YAxis.C1655a.LEFT) {
            if (this.f2521e < eVar.mo13408b()) {
                this.f2521e = eVar.mo13408b();
            }
            if (this.f2522f > eVar.mo13415g()) {
                this.f2522f = eVar.mo13415g();
                return;
            }
            return;
        }
        if (this.f2523g < eVar.mo13408b()) {
            this.f2523g = eVar.mo13408b();
        }
        if (this.f2524h > eVar.mo13415g()) {
            this.f2524h = eVar.mo13415g();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public T mo13375a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            T t = (IDataSet) it.next();
            if (t.mo13364s() == YAxis.C1655a.LEFT) {
                return t;
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo13380a(ValueFormatter gVar) {
        if (gVar != null) {
            for (T t : this.f2525i) {
                t.mo13339a(gVar);
            }
        }
    }

    /* renamed from: a */
    public void mo13379a(Typeface typeface) {
        for (T t : this.f2525i) {
            t.mo13338a(typeface);
        }
    }

    /* renamed from: a */
    public void mo13377a(float f) {
        for (T t : this.f2525i) {
            t.mo13336a(f);
        }
    }
}
