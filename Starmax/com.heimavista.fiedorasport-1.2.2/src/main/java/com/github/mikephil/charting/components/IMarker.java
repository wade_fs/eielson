package com.github.mikephil.charting.components;

import android.graphics.Canvas;
import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p134d.Highlight;

/* renamed from: com.github.mikephil.charting.components.d */
public interface IMarker {
    /* renamed from: a */
    void mo13193a(Canvas canvas, float f, float f2);

    /* renamed from: a */
    void mo13194a(Entry entry, Highlight dVar);
}
