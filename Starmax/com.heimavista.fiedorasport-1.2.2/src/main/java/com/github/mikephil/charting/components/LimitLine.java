package com.github.mikephil.charting.components;

import android.graphics.DashPathEffect;
import android.graphics.Paint;

/* renamed from: com.github.mikephil.charting.components.g */
public class LimitLine extends ComponentBase {

    /* renamed from: g */
    private float f2433g;

    /* renamed from: h */
    private float f2434h;

    /* renamed from: i */
    private int f2435i;

    /* renamed from: j */
    private Paint.Style f2436j;

    /* renamed from: k */
    private String f2437k;

    /* renamed from: l */
    private DashPathEffect f2438l;

    /* renamed from: m */
    private C1653a f2439m;

    /* renamed from: com.github.mikephil.charting.components.g$a */
    /* compiled from: LimitLine */
    public enum C1653a {
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM
    }

    /* renamed from: g */
    public DashPathEffect mo13275g() {
        return this.f2438l;
    }

    /* renamed from: h */
    public String mo13276h() {
        return this.f2437k;
    }

    /* renamed from: i */
    public C1653a mo13277i() {
        return this.f2439m;
    }

    /* renamed from: j */
    public float mo13278j() {
        return this.f2433g;
    }

    /* renamed from: k */
    public int mo13279k() {
        return this.f2435i;
    }

    /* renamed from: l */
    public float mo13280l() {
        return this.f2434h;
    }

    /* renamed from: m */
    public Paint.Style mo13281m() {
        return this.f2436j;
    }
}
