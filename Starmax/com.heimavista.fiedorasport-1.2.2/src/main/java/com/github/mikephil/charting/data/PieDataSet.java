package com.github.mikephil.charting.data;

import androidx.core.view.ViewCompat;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.data.o */
public class PieDataSet extends DataSet<PieEntry> implements IPieDataSet {

    /* renamed from: A */
    private C1659a f2545A;

    /* renamed from: B */
    private C1659a f2546B;

    /* renamed from: C */
    private boolean f2547C;

    /* renamed from: D */
    private int f2548D;

    /* renamed from: E */
    private float f2549E;

    /* renamed from: F */
    private float f2550F;

    /* renamed from: G */
    private float f2551G;

    /* renamed from: H */
    private float f2552H;

    /* renamed from: I */
    private boolean f2553I;

    /* renamed from: x */
    private float f2554x = 0.0f;

    /* renamed from: y */
    private boolean f2555y;

    /* renamed from: z */
    private float f2556z = 18.0f;

    /* renamed from: com.github.mikephil.charting.data.o$a */
    /* compiled from: PieDataSet */
    public enum C1659a {
        INSIDE_SLICE,
        OUTSIDE_SLICE
    }

    public PieDataSet(List<PieEntry> list, String str) {
        super(list, str);
        C1659a aVar = C1659a.INSIDE_SLICE;
        this.f2545A = aVar;
        this.f2546B = aVar;
        this.f2547C = false;
        this.f2548D = ViewCompat.MEASURED_STATE_MASK;
        this.f2549E = 1.0f;
        this.f2550F = 75.0f;
        this.f2551G = 0.3f;
        this.f2552H = 0.4f;
        this.f2553I = true;
    }

    /* renamed from: A0 */
    public C1659a mo13421A0() {
        return this.f2545A;
    }

    /* renamed from: B0 */
    public C1659a mo13422B0() {
        return this.f2546B;
    }

    /* renamed from: C0 */
    public boolean mo13423C0() {
        return this.f2553I;
    }

    /* renamed from: D0 */
    public float mo13424D0() {
        return this.f2552H;
    }

    /* renamed from: E0 */
    public boolean mo13425E0() {
        return this.f2547C;
    }

    /* renamed from: F0 */
    public float mo13426F0() {
        return this.f2556z;
    }

    /* renamed from: G0 */
    public float mo13427G0() {
        return this.f2550F;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13325b(PieEntry pieEntry) {
        if (pieEntry != null) {
            mo13414d(pieEntry);
        }
    }

    /* renamed from: c */
    public void mo13429c(float f) {
        this.f2556z = Utils.m11599a(f);
    }

    /* renamed from: d */
    public void mo13430d(float f) {
        if (f > 20.0f) {
            f = 20.0f;
        }
        if (f < 0.0f) {
            f = 0.0f;
        }
        this.f2554x = Utils.m11599a(f);
    }

    /* renamed from: v0 */
    public float mo13432v0() {
        return this.f2554x;
    }

    /* renamed from: w0 */
    public boolean mo13433w0() {
        return this.f2555y;
    }

    /* renamed from: x0 */
    public int mo13434x0() {
        return this.f2548D;
    }

    /* renamed from: y0 */
    public float mo13435y0() {
        return this.f2549E;
    }

    /* renamed from: z0 */
    public float mo13436z0() {
        return this.f2551G;
    }

    /* renamed from: d */
    public void mo13431d(boolean z) {
        this.f2547C = z;
    }
}
