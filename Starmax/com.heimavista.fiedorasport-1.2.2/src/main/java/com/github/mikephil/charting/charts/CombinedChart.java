package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BubbleData;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.ScatterData;
import p119e.p128c.p129a.p130a.p134d.CombinedHighlighter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.CombinedDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;
import p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer;

public class CombinedChart extends BarLineChartBase<CombinedData> implements CombinedDataProvider {

    /* renamed from: e1 */
    private boolean f2296e1 = true;

    /* renamed from: f1 */
    protected boolean f2297f1 = false;

    /* renamed from: g1 */
    private boolean f2298g1 = false;

    /* renamed from: h1 */
    protected C1645a[] f2299h1;

    /* renamed from: com.github.mikephil.charting.charts.CombinedChart$a */
    public enum C1645a {
        BAR,
        BUBBLE,
        LINE,
        CANDLE,
        SCATTER
    }

    public CombinedChart(Context context) {
        super(context);
    }

    /* renamed from: a */
    public Highlight mo12937a(float f, float f2) {
        if (this.f2264Q == null) {
            Log.e("MPAndroidChart", "Can't select by touch. No data set.");
            return null;
        }
        Highlight a = getHighlighter().mo23231a(f, f2);
        return (a == null || !mo12940c()) ? a : new Highlight(a.mo23249g(), a.mo23251i(), a.mo23250h(), a.mo23252j(), a.mo23245c(), -1, a.mo23240a());
    }

    /* renamed from: b */
    public boolean mo12939b() {
        return this.f2296e1;
    }

    /* renamed from: c */
    public boolean mo12940c() {
        return this.f2297f1;
    }

    public BarData getBarData() {
        T t = this.f2264Q;
        if (t == null) {
            return null;
        }
        return ((CombinedData) t).mo13397l();
    }

    public BubbleData getBubbleData() {
        T t = this.f2264Q;
        if (t == null) {
            return null;
        }
        return ((CombinedData) t).mo13398m();
    }

    public CandleData getCandleData() {
        T t = this.f2264Q;
        if (t == null) {
            return null;
        }
        return ((CombinedData) t).mo13399n();
    }

    public CombinedData getCombinedData() {
        return (CombinedData) this.f2264Q;
    }

    public C1645a[] getDrawOrder() {
        return this.f2299h1;
    }

    public LineData getLineData() {
        T t = this.f2264Q;
        if (t == null) {
            return null;
        }
        return ((CombinedData) t).mo13400o();
    }

    public ScatterData getScatterData() {
        T t = this.f2264Q;
        if (t == null) {
            return null;
        }
        return ((CombinedData) t).mo13401p();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2299h1 = new C1645a[]{C1645a.BAR, C1645a.BUBBLE, C1645a.LINE, C1645a.CANDLE, C1645a.SCATTER};
        setHighlighter(new CombinedHighlighter(this, this));
        setHighlightFullBarEnabled(true);
        this.f2280j0 = new CombinedChartRenderer(this, this.f2283m0, this.f2282l0);
    }

    public void setDrawBarShadow(boolean z) {
        this.f2298g1 = z;
    }

    public void setDrawOrder(C1645a[] aVarArr) {
        if (aVarArr != null && aVarArr.length > 0) {
            this.f2299h1 = aVarArr;
        }
    }

    public void setDrawValueAboveBar(boolean z) {
        this.f2296e1 = z;
    }

    public void setHighlightFullBarEnabled(boolean z) {
        this.f2297f1 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13032b(Canvas canvas) {
        if (this.f2292v0 != null && mo13069j() && mo13072n()) {
            int i = 0;
            while (true) {
                Highlight[] dVarArr = this.f2289s0;
                if (i < dVarArr.length) {
                    Highlight dVar = dVarArr[i];
                    IBarLineScatterCandleBubbleDataSet<? extends Entry> b = ((CombinedData) this.f2264Q).mo13394b(dVar);
                    Entry a = ((CombinedData) this.f2264Q).mo13373a(dVar);
                    if (a != null && ((float) b.mo13405a(a)) <= ((float) b.mo13417t()) * this.f2283m0.mo23201a()) {
                        float[] a2 = mo13029a(dVar);
                        if (this.f2282l0.mo23451a(a2[0], a2[1])) {
                            this.f2292v0.mo13194a(a, dVar);
                            this.f2292v0.mo13193a(canvas, a2[0], a2[1]);
                        }
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void setData(CombinedData jVar) {
        super.setData((ChartData) jVar);
        setHighlighter(new CombinedHighlighter(this, this));
        ((CombinedChartRenderer) this.f2280j0).mo23354b();
        this.f2280j0.mo23337a();
    }

    public CombinedChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: a */
    public boolean mo12938a() {
        return this.f2298g1;
    }

    public CombinedChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
