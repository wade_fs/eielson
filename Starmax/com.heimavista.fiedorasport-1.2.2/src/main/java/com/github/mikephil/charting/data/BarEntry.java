package com.github.mikephil.charting.data;

import android.annotation.SuppressLint;
import p119e.p128c.p129a.p130a.p134d.Range;

@SuppressLint({"ParcelCreator"})
public class BarEntry extends Entry {

    /* renamed from: T */
    private float[] f2476T;

    /* renamed from: U */
    private Range[] f2477U;

    /* renamed from: V */
    private float f2478V;

    /* renamed from: W */
    private float f2479W;

    public BarEntry(float f, float f2) {
        super(f, f2);
    }

    /* renamed from: a */
    private static float m3993a(float[] fArr) {
        float f = 0.0f;
        if (fArr == null) {
            return 0.0f;
        }
        for (float f2 : fArr) {
            f += f2;
        }
        return f;
    }

    /* renamed from: k */
    private void m3994k() {
        float[] fArr = this.f2476T;
        if (fArr == null) {
            this.f2478V = 0.0f;
            this.f2479W = 0.0f;
            return;
        }
        float f = 0.0f;
        float f2 = 0.0f;
        for (float f3 : fArr) {
            if (f3 <= 0.0f) {
                f += Math.abs(f3);
            } else {
                f2 += f3;
            }
        }
        this.f2478V = f;
        this.f2479W = f2;
    }

    /* renamed from: c */
    public float mo13303c() {
        return super.mo13303c();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo13304e() {
        float[] i = mo13308i();
        if (i != null && i.length != 0) {
            this.f2477U = new Range[i.length];
            int i2 = 0;
            float f = -mo13305f();
            float f2 = 0.0f;
            while (true) {
                Range[] jVarArr = this.f2477U;
                if (i2 < jVarArr.length) {
                    float f3 = i[i2];
                    if (f3 < 0.0f) {
                        float f4 = f - f3;
                        jVarArr[i2] = new Range(f, f4);
                        f = f4;
                    } else {
                        float f5 = f3 + f2;
                        jVarArr[i2] = new Range(f2, f5);
                        f2 = f5;
                    }
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: f */
    public float mo13305f() {
        return this.f2478V;
    }

    /* renamed from: g */
    public float mo13306g() {
        return this.f2479W;
    }

    /* renamed from: h */
    public Range[] mo13307h() {
        return this.f2477U;
    }

    /* renamed from: i */
    public float[] mo13308i() {
        return this.f2476T;
    }

    /* renamed from: j */
    public boolean mo13309j() {
        return this.f2476T != null;
    }

    public BarEntry(float f, float[] fArr) {
        super(f, m3993a(fArr));
        this.f2476T = fArr;
        m3994k();
        mo13304e();
    }
}
