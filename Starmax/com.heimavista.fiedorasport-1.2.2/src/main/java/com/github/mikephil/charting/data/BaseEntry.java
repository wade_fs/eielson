package com.github.mikephil.charting.data;

import android.graphics.drawable.Drawable;

/* renamed from: com.github.mikephil.charting.data.f */
public abstract class BaseEntry {

    /* renamed from: P */
    private float f2514P = 0.0f;

    /* renamed from: Q */
    private Object f2515Q = null;

    /* renamed from: R */
    private Drawable f2516R = null;

    public BaseEntry() {
    }

    /* renamed from: a */
    public void mo13369a(float f) {
        this.f2514P = f;
    }

    /* renamed from: b */
    public Drawable mo13371b() {
        return this.f2516R;
    }

    /* renamed from: c */
    public float mo13303c() {
        return this.f2514P;
    }

    /* renamed from: a */
    public Object mo13368a() {
        return this.f2515Q;
    }

    /* renamed from: a */
    public void mo13370a(Object obj) {
        this.f2515Q = obj;
    }

    public BaseEntry(float f) {
        this.f2514P = f;
    }
}
