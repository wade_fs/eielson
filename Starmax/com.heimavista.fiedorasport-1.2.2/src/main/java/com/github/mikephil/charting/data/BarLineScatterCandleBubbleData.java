package com.github.mikephil.charting.data;

import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;

/* renamed from: com.github.mikephil.charting.data.c */
public abstract class BarLineScatterCandleBubbleData<T extends IBarLineScatterCandleBubbleDataSet<? extends Entry>> extends ChartData<T> {
    public BarLineScatterCandleBubbleData() {
    }

    public BarLineScatterCandleBubbleData(List<T> list) {
        super(list);
    }
}
