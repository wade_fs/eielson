package com.github.mikephil.charting.components;

import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import java.lang.ref.WeakReference;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p143j.MPPointF;

public class MarkerView extends RelativeLayout implements IMarker {

    /* renamed from: P */
    private MPPointF f2340P = new MPPointF();

    /* renamed from: Q */
    private MPPointF f2341Q = new MPPointF();

    /* renamed from: R */
    private WeakReference<Chart> f2342R;

    public MarkerView(Context context, int i) {
        super(context);
        setupLayoutResource(i);
    }

    private void setupLayoutResource(int i) {
        View inflate = LayoutInflater.from(getContext()).inflate(i, this);
        inflate.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        inflate.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
    }

    /* renamed from: a */
    public MPPointF mo13192a(float f, float f2) {
        MPPointF offset = getOffset();
        MPPointF eVar = this.f2341Q;
        eVar.f7123R = offset.f7123R;
        eVar.f7124S = offset.f7124S;
        Chart chartView = getChartView();
        float width = (float) getWidth();
        float height = (float) getHeight();
        MPPointF eVar2 = this.f2341Q;
        float f3 = eVar2.f7123R;
        if (f + f3 < 0.0f) {
            eVar2.f7123R = -f;
        } else if (chartView != null && f + width + f3 > ((float) chartView.getWidth())) {
            this.f2341Q.f7123R = (((float) chartView.getWidth()) - f) - width;
        }
        MPPointF eVar3 = this.f2341Q;
        float f4 = eVar3.f7124S;
        if (f2 + f4 < 0.0f) {
            eVar3.f7124S = -f2;
        } else if (chartView != null && f2 + height + f4 > ((float) chartView.getHeight())) {
            this.f2341Q.f7124S = (((float) chartView.getHeight()) - f2) - height;
        }
        return this.f2341Q;
    }

    public Chart getChartView() {
        WeakReference<Chart> weakReference = this.f2342R;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    public MPPointF getOffset() {
        return this.f2340P;
    }

    public void setChartView(Chart chart) {
        this.f2342R = new WeakReference<>(chart);
    }

    public void setOffset(MPPointF eVar) {
        this.f2340P = eVar;
        if (this.f2340P == null) {
            this.f2340P = new MPPointF();
        }
    }

    /* renamed from: a */
    public void mo13194a(Entry entry, Highlight dVar) {
        measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
    }

    /* renamed from: a */
    public void mo13193a(Canvas canvas, float f, float f2) {
        MPPointF a = mo13192a(f, f2);
        int save = canvas.save();
        canvas.translate(f + a.f7123R, f2 + a.f7124S);
        draw(canvas);
        canvas.restoreToCount(save);
    }
}
