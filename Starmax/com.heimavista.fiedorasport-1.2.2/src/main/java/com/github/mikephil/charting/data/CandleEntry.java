package com.github.mikephil.charting.data;

import android.annotation.SuppressLint;

@SuppressLint({"ParcelCreator"})
public class CandleEntry extends Entry {

    /* renamed from: T */
    private float f2481T;

    /* renamed from: U */
    private float f2482U;

    /* renamed from: V */
    private float f2483V;

    /* renamed from: W */
    private float f2484W;

    /* renamed from: c */
    public float mo13303c() {
        return super.mo13303c();
    }

    /* renamed from: e */
    public float mo13311e() {
        return this.f2483V;
    }

    /* renamed from: f */
    public float mo13312f() {
        return this.f2481T;
    }

    /* renamed from: g */
    public float mo13313g() {
        return this.f2482U;
    }

    /* renamed from: h */
    public float mo13314h() {
        return this.f2484W;
    }
}
