package com.github.mikephil.charting.data;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import androidx.core.view.ViewCompat;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p140h.GradientColor;
import p119e.p128c.p129a.p130a.p143j.ColorTemplate;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.data.e */
public abstract class BaseDataSet<T extends Entry> implements IDataSet<T> {

    /* renamed from: a */
    protected List<Integer> f2496a;

    /* renamed from: b */
    protected GradientColor f2497b;

    /* renamed from: c */
    protected List<GradientColor> f2498c;

    /* renamed from: d */
    protected List<Integer> f2499d;

    /* renamed from: e */
    private String f2500e;

    /* renamed from: f */
    protected YAxis.C1655a f2501f;

    /* renamed from: g */
    protected boolean f2502g;

    /* renamed from: h */
    protected transient ValueFormatter f2503h;

    /* renamed from: i */
    protected Typeface f2504i;

    /* renamed from: j */
    private Legend.C1649c f2505j;

    /* renamed from: k */
    private float f2506k;

    /* renamed from: l */
    private float f2507l;

    /* renamed from: m */
    private DashPathEffect f2508m;

    /* renamed from: n */
    protected boolean f2509n;

    /* renamed from: o */
    protected boolean f2510o;

    /* renamed from: p */
    protected MPPointF f2511p;

    /* renamed from: q */
    protected float f2512q;

    /* renamed from: r */
    protected boolean f2513r;

    public BaseDataSet() {
        this.f2496a = null;
        this.f2497b = null;
        this.f2498c = null;
        this.f2499d = null;
        this.f2500e = "DataSet";
        this.f2501f = YAxis.C1655a.LEFT;
        this.f2502g = true;
        this.f2505j = Legend.C1649c.f2411R;
        this.f2506k = Float.NaN;
        this.f2507l = Float.NaN;
        this.f2508m = null;
        this.f2509n = true;
        this.f2510o = true;
        this.f2511p = new MPPointF();
        this.f2512q = 17.0f;
        this.f2513r = true;
        this.f2496a = new ArrayList();
        this.f2499d = new ArrayList();
        this.f2496a.add(Integer.valueOf(Color.rgb(140, 234, 255)));
        this.f2499d.add(Integer.valueOf((int) ViewCompat.MEASURED_STATE_MASK));
    }

    /* renamed from: a */
    public void mo13341a(List<Integer> list) {
        this.f2496a = list;
    }

    /* renamed from: b */
    public void mo13344b(boolean z) {
        this.f2509n = z;
    }

    /* renamed from: c */
    public int mo13345c(int i) {
        List<Integer> list = this.f2496a;
        return list.get(i % list.size()).intValue();
    }

    /* renamed from: d */
    public int mo13348d(int i) {
        List<Integer> list = this.f2499d;
        return list.get(i % list.size()).intValue();
    }

    /* renamed from: e */
    public GradientColor mo13351e(int i) {
        List<GradientColor> list = this.f2498c;
        return list.get(i % list.size());
    }

    /* renamed from: f */
    public String mo13352f() {
        return this.f2500e;
    }

    /* renamed from: h */
    public GradientColor mo13353h() {
        return this.f2497b;
    }

    /* renamed from: i */
    public float mo13354i() {
        return this.f2512q;
    }

    public boolean isVisible() {
        return this.f2513r;
    }

    /* renamed from: j */
    public ValueFormatter mo13356j() {
        if (mo13360n()) {
            return Utils.m11616b();
        }
        return this.f2503h;
    }

    /* renamed from: k */
    public float mo13357k() {
        return this.f2507l;
    }

    /* renamed from: l */
    public float mo13358l() {
        return this.f2506k;
    }

    /* renamed from: m */
    public Typeface mo13359m() {
        return this.f2504i;
    }

    /* renamed from: n */
    public boolean mo13360n() {
        return this.f2503h == null;
    }

    /* renamed from: o */
    public List<Integer> mo13361o() {
        return this.f2496a;
    }

    /* renamed from: p */
    public List<GradientColor> mo13362p() {
        return this.f2498c;
    }

    /* renamed from: r */
    public boolean mo13363r() {
        return this.f2509n;
    }

    /* renamed from: s */
    public YAxis.C1655a mo13364s() {
        return this.f2501f;
    }

    /* renamed from: u */
    public MPPointF mo13365u() {
        return this.f2511p;
    }

    /* renamed from: v */
    public int mo13366v() {
        return this.f2496a.get(0).intValue();
    }

    /* renamed from: w */
    public boolean mo13367w() {
        return this.f2502g;
    }

    /* renamed from: a */
    public void mo13343a(int... iArr) {
        this.f2496a = ColorTemplate.m11560a(iArr);
    }

    /* renamed from: c */
    public void mo13347c(boolean z) {
        this.f2502g = z;
    }

    /* renamed from: d */
    public boolean mo13349d() {
        return this.f2510o;
    }

    /* renamed from: e */
    public Legend.C1649c mo13350e() {
        return this.f2505j;
    }

    /* renamed from: a */
    public void mo13339a(ValueFormatter gVar) {
        if (gVar != null) {
            this.f2503h = gVar;
        }
    }

    /* renamed from: c */
    public DashPathEffect mo13346c() {
        return this.f2508m;
    }

    /* renamed from: a */
    public void mo13337a(int i) {
        this.f2499d.clear();
        this.f2499d.add(Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo13338a(Typeface typeface) {
        this.f2504i = typeface;
    }

    /* renamed from: a */
    public void mo13336a(float f) {
        this.f2512q = Utils.m11599a(f);
    }

    /* renamed from: a */
    public void mo13342a(boolean z) {
        this.f2510o = z;
    }

    /* renamed from: a */
    public void mo13340a(MPPointF eVar) {
        MPPointF eVar2 = this.f2511p;
        eVar2.f7123R = eVar.f7123R;
        eVar2.f7124S = eVar.f7124S;
    }

    public BaseDataSet(String str) {
        this();
        this.f2500e = str;
    }
}
