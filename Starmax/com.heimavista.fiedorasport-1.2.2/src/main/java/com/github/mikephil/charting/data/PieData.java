package com.github.mikephil.charting.data;

import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;

/* renamed from: com.github.mikephil.charting.data.n */
public class PieData extends ChartData<IPieDataSet> {
    public PieData() {
    }

    /* renamed from: k */
    public IPieDataSet mo13419k() {
        return (IPieDataSet) super.f2525i.get(0);
    }

    /* renamed from: l */
    public float mo13420l() {
        float f = 0.0f;
        for (int i = 0; i < mo13419k().mo13417t(); i++) {
            f += ((PieEntry) mo13419k().mo13410b(i)).mo13303c();
        }
        return f;
    }

    public PieData(IPieDataSet iVar) {
        super(iVar);
    }

    /* renamed from: a */
    public IPieDataSet mo13374a(int i) {
        if (i == 0) {
            return mo13419k();
        }
        return null;
    }

    /* renamed from: a */
    public Entry mo13373a(Highlight dVar) {
        return mo13419k().mo13410b((int) dVar.mo23249g());
    }
}
