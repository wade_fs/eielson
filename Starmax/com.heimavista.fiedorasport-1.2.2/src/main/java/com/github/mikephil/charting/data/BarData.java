package com.github.mikephil.charting.data;

import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;

/* renamed from: com.github.mikephil.charting.data.a */
public class BarData extends BarLineScatterCandleBubbleData<IBarDataSet> {

    /* renamed from: j */
    private float f2487j = 0.85f;

    public BarData() {
    }

    /* renamed from: b */
    public void mo13322b(float f) {
        this.f2487j = f;
    }

    /* renamed from: k */
    public float mo13323k() {
        return this.f2487j;
    }

    public BarData(List<IBarDataSet> list) {
        super(list);
    }
}
