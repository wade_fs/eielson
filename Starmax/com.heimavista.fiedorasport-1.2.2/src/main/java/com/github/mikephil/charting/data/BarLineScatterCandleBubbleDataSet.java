package com.github.mikephil.charting.data;

import android.graphics.Color;
import com.github.mikephil.charting.data.Entry;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;

/* renamed from: com.github.mikephil.charting.data.d */
public abstract class BarLineScatterCandleBubbleDataSet<T extends Entry> extends DataSet<T> implements IBarLineScatterCandleBubbleDataSet<T> {

    /* renamed from: x */
    protected int f2495x = Color.rgb(255, 187, 115);

    public BarLineScatterCandleBubbleDataSet(List<T> list, String str) {
        super(list, str);
    }

    /* renamed from: g */
    public void mo13334g(int i) {
        this.f2495x = i;
    }

    /* renamed from: x */
    public int mo13335x() {
        return this.f2495x;
    }
}
