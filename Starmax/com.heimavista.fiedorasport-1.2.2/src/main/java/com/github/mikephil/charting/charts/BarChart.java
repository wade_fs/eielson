package com.github.mikephil.charting.charts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import p119e.p128c.p129a.p130a.p134d.BarHighlighter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p141i.BarChartRenderer;

public class BarChart extends BarLineChartBase<BarData> implements BarDataProvider {

    /* renamed from: e1 */
    protected boolean f2224e1 = false;

    /* renamed from: f1 */
    private boolean f2225f1 = true;

    /* renamed from: g1 */
    private boolean f2226g1 = false;

    /* renamed from: h1 */
    private boolean f2227h1 = false;

    public BarChart(Context context) {
        super(context);
    }

    /* renamed from: a */
    public Highlight mo12937a(float f, float f2) {
        if (this.f2264Q == null) {
            Log.e("MPAndroidChart", "Can't select by touch. No data set.");
            return null;
        }
        Highlight a = getHighlighter().mo23231a(f, f2);
        return (a == null || !mo12940c()) ? a : new Highlight(a.mo23249g(), a.mo23251i(), a.mo23250h(), a.mo23252j(), a.mo23245c(), -1, a.mo23240a());
    }

    /* renamed from: b */
    public boolean mo12939b() {
        return this.f2225f1;
    }

    /* renamed from: c */
    public boolean mo12940c() {
        return this.f2224e1;
    }

    public BarData getBarData() {
        return (BarData) this.f2264Q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo12942h() {
        super.mo12942h();
        this.f2280j0 = new BarChartRenderer(this, this.f2283m0, this.f2282l0);
        setHighlighter(new BarHighlighter(this));
        getXAxis().mo13210g(0.5f);
        getXAxis().mo13208f(0.5f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public void mo12943p() {
        if (this.f2227h1) {
            this.f2271a0.mo13199a(((BarData) this.f2264Q).mo13390g() - (((BarData) this.f2264Q).mo13323k() / 2.0f), ((BarData) this.f2264Q).mo13389f() + (((BarData) this.f2264Q).mo13323k() / 2.0f));
        } else {
            this.f2271a0.mo13199a(((BarData) this.f2264Q).mo13390g(), ((BarData) this.f2264Q).mo13389f());
        }
        super.f2243P0.mo13199a(((BarData) this.f2264Q).mo13382b(YAxis.C1655a.LEFT), ((BarData) this.f2264Q).mo13372a(YAxis.C1655a.LEFT));
        super.f2244Q0.mo13199a(((BarData) this.f2264Q).mo13382b(YAxis.C1655a.RIGHT), ((BarData) this.f2264Q).mo13372a(YAxis.C1655a.RIGHT));
    }

    public void setDrawBarShadow(boolean z) {
        this.f2226g1 = z;
    }

    public void setDrawValueAboveBar(boolean z) {
        this.f2225f1 = z;
    }

    public void setFitBars(boolean z) {
        this.f2227h1 = z;
    }

    public void setHighlightFullBarEnabled(boolean z) {
        this.f2224e1 = z;
    }

    public BarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: a */
    public boolean mo12938a() {
        return this.f2226g1;
    }

    public BarChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
