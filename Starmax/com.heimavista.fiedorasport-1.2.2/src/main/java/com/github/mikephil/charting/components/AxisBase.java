package com.github.mikephil.charting.components;

import android.graphics.DashPathEffect;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.DefaultAxisValueFormatter;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: com.github.mikephil.charting.components.a */
public abstract class AxisBase extends ComponentBase {

    /* renamed from: A */
    protected boolean f2343A = false;

    /* renamed from: B */
    protected boolean f2344B = true;

    /* renamed from: C */
    protected float f2345C = 0.0f;

    /* renamed from: D */
    protected float f2346D = 0.0f;

    /* renamed from: E */
    protected boolean f2347E = false;

    /* renamed from: F */
    protected boolean f2348F = false;

    /* renamed from: G */
    public float f2349G = 0.0f;

    /* renamed from: H */
    public float f2350H = 0.0f;

    /* renamed from: I */
    public float f2351I = 0.0f;

    /* renamed from: g */
    protected ValueFormatter f2352g;

    /* renamed from: h */
    private int f2353h = -7829368;

    /* renamed from: i */
    private float f2354i = 1.0f;

    /* renamed from: j */
    private int f2355j = -7829368;

    /* renamed from: k */
    private float f2356k = 1.0f;

    /* renamed from: l */
    public float[] f2357l = new float[0];

    /* renamed from: m */
    public float[] f2358m = new float[0];

    /* renamed from: n */
    public int f2359n;

    /* renamed from: o */
    public int f2360o;

    /* renamed from: p */
    private int f2361p = 6;

    /* renamed from: q */
    protected float f2362q = 1.0f;

    /* renamed from: r */
    protected boolean f2363r = false;

    /* renamed from: s */
    protected boolean f2364s = false;

    /* renamed from: t */
    protected boolean f2365t = true;

    /* renamed from: u */
    protected boolean f2366u = true;

    /* renamed from: v */
    protected boolean f2367v = true;

    /* renamed from: w */
    protected boolean f2368w = false;

    /* renamed from: x */
    private DashPathEffect f2369x = null;

    /* renamed from: y */
    private DashPathEffect f2370y = null;

    /* renamed from: z */
    protected List<LimitLine> f2371z;

    public AxisBase() {
        super.f2376e = Utils.m11599a(10.0f);
        super.f2373b = Utils.m11599a(5.0f);
        super.f2374c = Utils.m11599a(5.0f);
        this.f2371z = new ArrayList();
    }

    /* renamed from: a */
    public void mo13200a(ValueFormatter gVar) {
        if (gVar == null) {
            this.f2352g = new DefaultAxisValueFormatter(this.f2360o);
        } else {
            this.f2352g = gVar;
        }
    }

    /* renamed from: b */
    public void mo13202b(boolean z) {
        this.f2366u = z;
    }

    /* renamed from: c */
    public void mo13204c(boolean z) {
        this.f2365t = z;
    }

    /* renamed from: d */
    public void mo13206d(boolean z) {
        this.f2363r = z;
    }

    /* renamed from: e */
    public void mo13207e(float f) {
        this.f2362q = f;
        this.f2363r = true;
    }

    /* renamed from: f */
    public void mo13208f(float f) {
        this.f2346D = f;
    }

    /* renamed from: g */
    public int mo13209g() {
        return this.f2355j;
    }

    /* renamed from: h */
    public DashPathEffect mo13211h() {
        return this.f2369x;
    }

    /* renamed from: i */
    public float mo13212i() {
        return this.f2356k;
    }

    /* renamed from: j */
    public float mo13213j() {
        return this.f2362q;
    }

    /* renamed from: k */
    public int mo13214k() {
        return this.f2353h;
    }

    /* renamed from: l */
    public DashPathEffect mo13215l() {
        return this.f2370y;
    }

    /* renamed from: m */
    public float mo13216m() {
        return this.f2354i;
    }

    /* renamed from: n */
    public int mo13217n() {
        return this.f2361p;
    }

    /* renamed from: o */
    public List<LimitLine> mo13218o() {
        return this.f2371z;
    }

    /* renamed from: p */
    public String mo13219p() {
        String str = "";
        for (int i = 0; i < this.f2357l.length; i++) {
            String b = mo13201b(i);
            if (b != null && str.length() < b.length()) {
                str = b;
            }
        }
        return str;
    }

    /* renamed from: q */
    public ValueFormatter mo13220q() {
        ValueFormatter gVar = this.f2352g;
        if (gVar == null || ((gVar instanceof DefaultAxisValueFormatter) && ((DefaultAxisValueFormatter) gVar).mo23216a() != this.f2360o)) {
            this.f2352g = new DefaultAxisValueFormatter(this.f2360o);
        }
        return this.f2352g;
    }

    /* renamed from: r */
    public boolean mo13221r() {
        return this.f2368w && this.f2359n > 0;
    }

    /* renamed from: s */
    public boolean mo13222s() {
        return this.f2366u;
    }

    /* renamed from: t */
    public boolean mo13223t() {
        return this.f2344B;
    }

    /* renamed from: u */
    public boolean mo13224u() {
        return this.f2365t;
    }

    /* renamed from: v */
    public boolean mo13225v() {
        return this.f2367v;
    }

    /* renamed from: w */
    public boolean mo13226w() {
        return this.f2343A;
    }

    /* renamed from: x */
    public boolean mo13227x() {
        return this.f2364s;
    }

    /* renamed from: y */
    public boolean mo13228y() {
        return this.f2363r;
    }

    /* renamed from: b */
    public String mo13201b(int i) {
        return (i < 0 || i >= this.f2357l.length) ? "" : mo13220q().mo23221a(this.f2357l[i], this);
    }

    /* renamed from: c */
    public void mo13203c(int i) {
        if (i > 25) {
            i = 25;
        }
        if (i < 2) {
            i = 2;
        }
        this.f2361p = i;
        this.f2364s = false;
    }

    /* renamed from: d */
    public void mo13205d(float f) {
        this.f2347E = true;
        this.f2350H = f;
        this.f2351I = Math.abs(this.f2349G - f);
    }

    /* renamed from: g */
    public void mo13210g(float f) {
        this.f2345C = f;
    }

    /* renamed from: a */
    public void mo13199a(float f, float f2) {
        float f3 = this.f2347E ? this.f2350H : f - this.f2345C;
        float f4 = this.f2348F ? this.f2349G : f2 + this.f2346D;
        if (Math.abs(f4 - f3) == 0.0f) {
            f4 += 1.0f;
            f3 -= 1.0f;
        }
        this.f2350H = f3;
        this.f2349G = f4;
        this.f2351I = Math.abs(f4 - f3);
    }
}
