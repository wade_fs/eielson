package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.C0232Px;
import com.squareup.picasso.Picasso;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: com.squareup.picasso.w */
/* compiled from: Request */
public final class C4750w {

    /* renamed from: u */
    private static final long f10063u = TimeUnit.SECONDS.toNanos(5);

    /* renamed from: a */
    int f10064a;

    /* renamed from: b */
    long f10065b;

    /* renamed from: c */
    int f10066c;

    /* renamed from: d */
    public final Uri f10067d;

    /* renamed from: e */
    public final int f10068e;

    /* renamed from: f */
    public final String f10069f;

    /* renamed from: g */
    public final List<C4723c0> f10070g;

    /* renamed from: h */
    public final int f10071h;

    /* renamed from: i */
    public final int f10072i;

    /* renamed from: j */
    public final boolean f10073j;

    /* renamed from: k */
    public final int f10074k;

    /* renamed from: l */
    public final boolean f10075l;

    /* renamed from: m */
    public final boolean f10076m;

    /* renamed from: n */
    public final float f10077n;

    /* renamed from: o */
    public final float f10078o;

    /* renamed from: p */
    public final float f10079p;

    /* renamed from: q */
    public final boolean f10080q;

    /* renamed from: r */
    public final boolean f10081r;

    /* renamed from: s */
    public final Bitmap.Config f10082s;

    /* renamed from: t */
    public final Picasso.C4746f f10083t;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo26332a() {
        Uri uri = this.f10067d;
        if (uri != null) {
            return String.valueOf(uri.getPath());
        }
        return Integer.toHexString(this.f10068e);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo26333b() {
        return this.f10070g != null;
    }

    /* renamed from: c */
    public boolean mo26334c() {
        return (this.f10071h == 0 && this.f10072i == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public String mo26335d() {
        long nanoTime = System.nanoTime() - this.f10065b;
        if (nanoTime > f10063u) {
            return mo26338g() + '+' + TimeUnit.NANOSECONDS.toSeconds(nanoTime) + 's';
        }
        return mo26338g() + '+' + TimeUnit.NANOSECONDS.toMillis(nanoTime) + "ms";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public boolean mo26336e() {
        return mo26334c() || this.f10077n != 0.0f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public boolean mo26337f() {
        return mo26336e() || mo26333b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public String mo26338g() {
        return "[R" + this.f10064a + ']';
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Request{");
        int i = this.f10068e;
        if (i > 0) {
            sb.append(i);
        } else {
            sb.append(this.f10067d);
        }
        List<C4723c0> list = this.f10070g;
        if (list != null && !list.isEmpty()) {
            for (C4723c0 c0Var : this.f10070g) {
                sb.append(' ');
                sb.append(c0Var.mo26252a());
            }
        }
        if (this.f10069f != null) {
            sb.append(" stableKey(");
            sb.append(this.f10069f);
            sb.append(')');
        }
        if (this.f10071h > 0) {
            sb.append(" resize(");
            sb.append(this.f10071h);
            sb.append(',');
            sb.append(this.f10072i);
            sb.append(')');
        }
        if (this.f10073j) {
            sb.append(" centerCrop");
        }
        if (this.f10075l) {
            sb.append(" centerInside");
        }
        if (this.f10077n != 0.0f) {
            sb.append(" rotation(");
            sb.append(this.f10077n);
            if (this.f10080q) {
                sb.append(" @ ");
                sb.append(this.f10078o);
                sb.append(',');
                sb.append(this.f10079p);
            }
            sb.append(')');
        }
        if (this.f10081r) {
            sb.append(" purgeable");
        }
        if (this.f10082s != null) {
            sb.append(' ');
            sb.append(this.f10082s);
        }
        sb.append('}');
        return sb.toString();
    }

    private C4750w(Uri uri, int i, String str, List<C4723c0> list, int i2, int i3, boolean z, boolean z2, int i4, boolean z3, float f, float f2, float f3, boolean z4, boolean z5, Bitmap.Config config, Picasso.C4746f fVar) {
        this.f10067d = uri;
        this.f10068e = i;
        this.f10069f = str;
        if (list == null) {
            this.f10070g = null;
        } else {
            this.f10070g = Collections.unmodifiableList(list);
        }
        this.f10071h = i2;
        this.f10072i = i3;
        this.f10073j = z;
        this.f10075l = z2;
        this.f10074k = i4;
        this.f10076m = z3;
        this.f10077n = f;
        this.f10078o = f2;
        this.f10079p = f3;
        this.f10080q = z4;
        this.f10081r = z5;
        this.f10082s = config;
        this.f10083t = fVar;
    }

    /* renamed from: com.squareup.picasso.w$b */
    /* compiled from: Request */
    public static final class C4752b {

        /* renamed from: a */
        private Uri f10084a;

        /* renamed from: b */
        private int f10085b;

        /* renamed from: c */
        private String f10086c;

        /* renamed from: d */
        private int f10087d;

        /* renamed from: e */
        private int f10088e;

        /* renamed from: f */
        private boolean f10089f;

        /* renamed from: g */
        private int f10090g;

        /* renamed from: h */
        private boolean f10091h;

        /* renamed from: i */
        private boolean f10092i;

        /* renamed from: j */
        private float f10093j;

        /* renamed from: k */
        private float f10094k;

        /* renamed from: l */
        private float f10095l;

        /* renamed from: m */
        private boolean f10096m;

        /* renamed from: n */
        private boolean f10097n;

        /* renamed from: o */
        private List<C4723c0> f10098o;

        /* renamed from: p */
        private Bitmap.Config f10099p;

        /* renamed from: q */
        private Picasso.C4746f f10100q;

        C4752b(Uri uri, int i, Bitmap.Config config) {
            this.f10084a = uri;
            this.f10085b = i;
            this.f10099p = config;
        }

        /* renamed from: a */
        public C4752b mo26340a(@C0232Px int i, @C0232Px int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Width must be positive number or 0.");
            } else if (i2 < 0) {
                throw new IllegalArgumentException("Height must be positive number or 0.");
            } else if (i2 == 0 && i == 0) {
                throw new IllegalArgumentException("At least one dimension has to be positive number.");
            } else {
                this.f10087d = i;
                this.f10088e = i2;
                return this;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public boolean mo26342b() {
            return (this.f10084a == null && this.f10085b == 0) ? false : true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public boolean mo26343c() {
            return (this.f10087d == 0 && this.f10088e == 0) ? false : true;
        }

        /* renamed from: a */
        public C4750w mo26341a() {
            if (this.f10091h && this.f10089f) {
                throw new IllegalStateException("Center crop and center inside can not be used together.");
            } else if (this.f10089f && this.f10087d == 0 && this.f10088e == 0) {
                throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
            } else if (this.f10091h && this.f10087d == 0 && this.f10088e == 0) {
                throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
            } else {
                if (this.f10100q == null) {
                    this.f10100q = Picasso.C4746f.NORMAL;
                }
                C4750w wVar = r2;
                C4750w wVar2 = new C4750w(this.f10084a, this.f10085b, this.f10086c, this.f10098o, this.f10087d, this.f10088e, this.f10089f, this.f10091h, this.f10090g, this.f10092i, this.f10093j, this.f10094k, this.f10095l, this.f10096m, this.f10097n, this.f10099p, this.f10100q);
                return wVar;
            }
        }
    }
}
