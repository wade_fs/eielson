package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.InputStream;
import p244j.Okio;

/* renamed from: com.squareup.picasso.f */
class ContactsPhotoRequestHandler extends RequestHandler {

    /* renamed from: b */
    private static final UriMatcher f9956b = new UriMatcher(-1);

    /* renamed from: a */
    private final Context f9957a;

    static {
        f9956b.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        f9956b.addURI("com.android.contacts", "contacts/lookup/*", 1);
        f9956b.addURI("com.android.contacts", "contacts/#/photo", 2);
        f9956b.addURI("com.android.contacts", "contacts/#", 3);
        f9956b.addURI("com.android.contacts", "display_photo/#", 4);
    }

    ContactsPhotoRequestHandler(Context context) {
        this.f9957a = context;
    }

    /* renamed from: c */
    private InputStream m15736c(C4750w wVar) {
        ContentResolver contentResolver = this.f9957a.getContentResolver();
        Uri uri = wVar.f10067d;
        int match = f9956b.match(uri);
        if (match != 1) {
            if (match != 2) {
                if (match != 3) {
                    if (match != 4) {
                        throw new IllegalStateException("Invalid uri: " + uri);
                    }
                }
            }
            return contentResolver.openInputStream(uri);
        }
        uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (uri == null) {
            return null;
        }
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        Uri uri = wVar.f10067d;
        return "content".equals(uri.getScheme()) && ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && f9956b.match(wVar.f10067d) != -1;
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        InputStream c = m15736c(wVar);
        if (c == null) {
            return null;
        }
        return new RequestHandler.C4753a(Okio.m18218a(c), Picasso.C4745e.DISK);
    }
}
