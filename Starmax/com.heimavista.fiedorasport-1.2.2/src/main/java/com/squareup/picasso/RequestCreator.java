package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.TypedValue;
import android.widget.ImageView;
import androidx.annotation.DrawableRes;
import com.squareup.picasso.C4750w;
import com.squareup.picasso.Picasso;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.squareup.picasso.x */
public class RequestCreator {

    /* renamed from: m */
    private static final AtomicInteger f10101m = new AtomicInteger();

    /* renamed from: a */
    private final Picasso f10102a;

    /* renamed from: b */
    private final C4750w.C4752b f10103b;

    /* renamed from: c */
    private boolean f10104c;

    /* renamed from: d */
    private boolean f10105d;

    /* renamed from: e */
    private boolean f10106e = true;

    /* renamed from: f */
    private int f10107f;

    /* renamed from: g */
    private int f10108g;

    /* renamed from: h */
    private int f10109h;

    /* renamed from: i */
    private int f10110i;

    /* renamed from: j */
    private Drawable f10111j;

    /* renamed from: k */
    private Drawable f10112k;

    /* renamed from: l */
    private Object f10113l;

    RequestCreator(Picasso tVar, Uri uri, int i) {
        if (!tVar.f10030o) {
            this.f10102a = tVar;
            this.f10103b = new C4750w.C4752b(uri, i, tVar.f10027l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* renamed from: c */
    private Drawable m15834c() {
        int i = this.f10107f;
        if (i == 0) {
            return this.f10111j;
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            return this.f10102a.f10020e.getDrawable(i);
        }
        if (i2 >= 16) {
            return this.f10102a.f10020e.getResources().getDrawable(this.f10107f);
        }
        TypedValue typedValue = new TypedValue();
        this.f10102a.f10020e.getResources().getValue(this.f10107f, typedValue, true);
        return this.f10102a.f10020e.getResources().getDrawable(typedValue.resourceId);
    }

    /* renamed from: a */
    public RequestCreator mo26345a(@DrawableRes int i) {
        if (!this.f10106e) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.f10111j == null) {
            this.f10107f = i;
            return this;
        } else {
            throw new IllegalStateException("Placeholder image already set.");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public RequestCreator mo26349b() {
        this.f10105d = false;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public RequestCreator mo26344a() {
        this.f10113l = null;
        return this;
    }

    /* renamed from: a */
    public RequestCreator mo26346a(int i, int i2) {
        this.f10103b.mo26340a(i, i2);
        return this;
    }

    /* renamed from: a */
    public void mo26347a(ImageView imageView) {
        mo26348a(imageView, (Callback) null);
    }

    /* renamed from: a */
    public void mo26348a(ImageView imageView, Callback eVar) {
        Bitmap a;
        ImageView imageView2 = imageView;
        Callback eVar2 = eVar;
        long nanoTime = System.nanoTime();
        C4726d0.m15727a();
        if (imageView2 == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.f10103b.mo26342b()) {
            this.f10102a.mo26311a(imageView2);
            if (this.f10106e) {
                PicassoDrawable.m15818a(imageView2, m15834c());
            }
        } else {
            if (this.f10105d) {
                if (!this.f10103b.mo26343c()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width == 0 || height == 0) {
                        if (this.f10106e) {
                            PicassoDrawable.m15818a(imageView2, m15834c());
                        }
                        this.f10102a.mo26312a(imageView2, new DeferredRequestCreator(this, imageView2, eVar2));
                        return;
                    }
                    this.f10103b.mo26340a(width, height);
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            C4750w a2 = m15833a(nanoTime);
            String a3 = C4726d0.m15725a(a2);
            if (!MemoryPolicy.m15786a(this.f10109h) || (a = this.f10102a.mo26307a(a3)) == null) {
                if (this.f10106e) {
                    PicassoDrawable.m15818a(imageView2, m15834c());
                }
                this.f10102a.mo26313a((Action) new ImageViewAction(this.f10102a, imageView, a2, this.f10109h, this.f10110i, this.f10108g, this.f10112k, a3, this.f10113l, eVar, this.f10104c));
                return;
            }
            this.f10102a.mo26311a(imageView2);
            Picasso tVar = this.f10102a;
            PicassoDrawable.m15817a(imageView, tVar.f10020e, a, Picasso.C4745e.MEMORY, this.f10104c, tVar.f10028m);
            if (this.f10102a.f10029n) {
                String g = a2.mo26338g();
                C4726d0.m15730a("Main", "completed", g, "from " + Picasso.C4745e.MEMORY);
            }
            if (eVar2 != null) {
                eVar.onSuccess();
            }
        }
    }

    /* renamed from: a */
    private C4750w m15833a(long j) {
        int andIncrement = f10101m.getAndIncrement();
        C4750w a = this.f10103b.mo26341a();
        a.f10064a = andIncrement;
        a.f10065b = j;
        boolean z = this.f10102a.f10029n;
        if (z) {
            C4726d0.m15730a("Main", "created", a.mo26338g(), a.toString());
        }
        this.f10102a.mo26308a(a);
        if (a != a) {
            a.f10064a = andIncrement;
            a.f10065b = j;
            if (z) {
                String d = a.mo26335d();
                C4726d0.m15730a("Main", "changed", d, "into " + a);
            }
        }
        return a;
    }
}
