package com.squareup.picasso;

/* renamed from: com.squareup.picasso.e */
public interface Callback {
    void onError(Exception exc);

    void onSuccess();
}
