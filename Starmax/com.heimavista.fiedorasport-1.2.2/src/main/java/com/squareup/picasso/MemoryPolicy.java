package com.squareup.picasso;

/* renamed from: com.squareup.picasso.p */
public enum MemoryPolicy {
    NO_CACHE(1),
    NO_STORE(2);
    

    /* renamed from: P */
    final int f10003P;

    private MemoryPolicy(int i) {
        this.f10003P = i;
    }

    /* renamed from: a */
    static boolean m15786a(int i) {
        return (i & NO_CACHE.f10003P) == 0;
    }

    /* renamed from: b */
    static boolean m15787b(int i) {
        return (i & NO_STORE.f10003P) == 0;
    }
}
