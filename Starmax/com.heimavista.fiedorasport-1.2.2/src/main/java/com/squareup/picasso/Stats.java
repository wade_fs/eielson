package com.squareup.picasso;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

/* renamed from: com.squareup.picasso.a0 */
class Stats {

    /* renamed from: a */
    final HandlerThread f9891a = new HandlerThread("Picasso-Stats", 10);

    /* renamed from: b */
    final C4724d f9892b;

    /* renamed from: c */
    final Handler f9893c;

    /* renamed from: d */
    long f9894d;

    /* renamed from: e */
    long f9895e;

    /* renamed from: f */
    long f9896f;

    /* renamed from: g */
    long f9897g;

    /* renamed from: h */
    long f9898h;

    /* renamed from: i */
    long f9899i;

    /* renamed from: j */
    long f9900j;

    /* renamed from: k */
    long f9901k;

    /* renamed from: l */
    int f9902l;

    /* renamed from: m */
    int f9903m;

    /* renamed from: n */
    int f9904n;

    /* renamed from: com.squareup.picasso.a0$a */
    /* compiled from: Stats */
    private static class C4715a extends Handler {

        /* renamed from: a */
        private final Stats f9905a;

        /* renamed from: com.squareup.picasso.a0$a$a */
        /* compiled from: Stats */
        class C4716a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Message f9906P;

            C4716a(C4715a aVar, Message message) {
                this.f9906P = message;
            }

            public void run() {
                throw new AssertionError("Unhandled stats message." + this.f9906P.what);
            }
        }

        C4715a(Looper looper, Stats a0Var) {
            super(looper);
            this.f9905a = a0Var;
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                this.f9905a.mo26220d();
            } else if (i == 1) {
                this.f9905a.mo26221e();
            } else if (i == 2) {
                this.f9905a.mo26216b((long) message.arg1);
            } else if (i == 3) {
                this.f9905a.mo26219c((long) message.arg1);
            } else if (i != 4) {
                Picasso.f10014p.post(new C4716a(this, message));
            } else {
                this.f9905a.mo26214a((Long) message.obj);
            }
        }
    }

    Stats(C4724d dVar) {
        this.f9892b = dVar;
        this.f9891a.start();
        C4726d0.m15728a(this.f9891a.getLooper());
        this.f9893c = new C4715a(this.f9891a.getLooper(), this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26213a(Bitmap bitmap) {
        m15666a(bitmap, 2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26217b(Bitmap bitmap) {
        m15666a(bitmap, 3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo26218c() {
        this.f9893c.sendEmptyMessage(1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo26220d() {
        this.f9894d++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public void mo26221e() {
        this.f9895e++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26212a(long j) {
        Handler handler = this.f9893c;
        handler.sendMessage(handler.obtainMessage(4, Long.valueOf(j)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26215b() {
        this.f9893c.sendEmptyMessage(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo26219c(long j) {
        this.f9904n++;
        this.f9898h += j;
        this.f9901k = m15665a(this.f9903m, this.f9898h);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26214a(Long l) {
        this.f9902l++;
        this.f9896f += l.longValue();
        this.f9899i = m15665a(this.f9902l, this.f9896f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26216b(long j) {
        this.f9903m++;
        this.f9897g += j;
        this.f9900j = m15665a(this.f9903m, this.f9897g);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public StatsSnapshot mo26211a() {
        return new StatsSnapshot(this.f9892b.mo26253a(), this.f9892b.size(), this.f9894d, this.f9895e, this.f9896f, this.f9897g, this.f9898h, this.f9899i, this.f9900j, this.f9901k, this.f9902l, this.f9903m, this.f9904n, System.currentTimeMillis());
    }

    /* renamed from: a */
    private void m15666a(Bitmap bitmap, int i) {
        int a = C4726d0.m15718a(bitmap);
        Handler handler = this.f9893c;
        handler.sendMessage(handler.obtainMessage(i, a, 0));
    }

    /* renamed from: a */
    private static long m15665a(int i, long j) {
        return j / ((long) i);
    }
}
