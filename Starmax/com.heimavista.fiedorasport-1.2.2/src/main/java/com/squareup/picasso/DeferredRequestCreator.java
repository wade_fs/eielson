package com.squareup.picasso;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import androidx.annotation.VisibleForTesting;
import java.lang.ref.WeakReference;

/* renamed from: com.squareup.picasso.h */
class DeferredRequestCreator implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {

    /* renamed from: P */
    private final RequestCreator f9959P;
    @VisibleForTesting

    /* renamed from: Q */
    final WeakReference<ImageView> f9960Q;
    @VisibleForTesting

    /* renamed from: R */
    Callback f9961R;

    DeferredRequestCreator(RequestCreator xVar, ImageView imageView, Callback eVar) {
        this.f9959P = xVar;
        this.f9960Q = new WeakReference<>(imageView);
        this.f9961R = eVar;
        imageView.addOnAttachStateChangeListener(this);
        if (imageView.getWindowToken() != null) {
            onViewAttachedToWindow(imageView);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26263a() {
        this.f9959P.mo26344a();
        this.f9961R = null;
        ImageView imageView = this.f9960Q.get();
        if (imageView != null) {
            this.f9960Q.clear();
            imageView.removeOnAttachStateChangeListener(this);
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    public boolean onPreDraw() {
        ImageView imageView = this.f9960Q.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        if (width > 0 && height > 0) {
            imageView.removeOnAttachStateChangeListener(this);
            viewTreeObserver.removeOnPreDrawListener(this);
            this.f9960Q.clear();
            RequestCreator xVar = this.f9959P;
            xVar.mo26349b();
            xVar.mo26346a(width, height);
            xVar.mo26348a(imageView, this.f9961R);
        }
        return true;
    }

    public void onViewAttachedToWindow(View view) {
        view.getViewTreeObserver().addOnPreDrawListener(this);
    }

    public void onViewDetachedFromWindow(View view) {
        view.getViewTreeObserver().removeOnPreDrawListener(this);
    }
}
