package com.squareup.picasso;

/* renamed from: com.squareup.picasso.q */
public enum NetworkPolicy {
    NO_CACHE(1),
    NO_STORE(2),
    OFFLINE(4);
    

    /* renamed from: P */
    final int f10008P;

    private NetworkPolicy(int i) {
        this.f10008P = i;
    }

    /* renamed from: a */
    public static boolean m15788a(int i) {
        return (i & OFFLINE.f10008P) != 0;
    }

    /* renamed from: b */
    public static boolean m15789b(int i) {
        return (i & NO_CACHE.f10008P) == 0;
    }

    /* renamed from: c */
    public static boolean m15790c(int i) {
        return (i & NO_STORE.f10008P) == 0;
    }
}
