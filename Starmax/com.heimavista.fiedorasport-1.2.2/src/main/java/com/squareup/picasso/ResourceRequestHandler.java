package com.squareup.picasso;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;

/* renamed from: com.squareup.picasso.z */
class ResourceRequestHandler extends RequestHandler {

    /* renamed from: a */
    private final Context f10118a;

    ResourceRequestHandler(Context context) {
        this.f10118a = context;
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        if (wVar.f10068e != 0) {
            return true;
        }
        return "android.resource".equals(wVar.f10067d.getScheme());
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        Resources a = C4726d0.m15720a(this.f10118a, wVar);
        return new RequestHandler.C4753a(m15854a(a, C4726d0.m15717a(a, wVar), wVar), Picasso.C4745e.DISK);
    }

    /* renamed from: a */
    private static Bitmap m15854a(Resources resources, int i, C4750w wVar) {
        BitmapFactory.Options b = RequestHandler.m15844b(wVar);
        if (RequestHandler.m15843a(b)) {
            BitmapFactory.decodeResource(resources, i, b);
            RequestHandler.m15842a(wVar.f10071h, wVar.f10072i, b, wVar);
        }
        return BitmapFactory.decodeResource(resources, i, b);
    }
}
