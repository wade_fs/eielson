package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.squareup.picasso.m */
/* compiled from: LruCache */
public final class C4734m implements C4724d {

    /* renamed from: a */
    final LruCache<String, C4736b> f9982a;

    /* renamed from: com.squareup.picasso.m$a */
    /* compiled from: LruCache */
    class C4735a extends LruCache<String, C4736b> {
        C4735a(C4734m mVar, int i) {
            super(i);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int sizeOf(String str, C4736b bVar) {
            return bVar.f9984b;
        }
    }

    /* renamed from: com.squareup.picasso.m$b */
    /* compiled from: LruCache */
    static final class C4736b {

        /* renamed from: a */
        final Bitmap f9983a;

        /* renamed from: b */
        final int f9984b;

        C4736b(Bitmap bitmap, int i) {
            this.f9983a = bitmap;
            this.f9984b = i;
        }
    }

    public C4734m(@NonNull Context context) {
        this(C4726d0.m15716a(context));
    }

    /* renamed from: a */
    public void mo26254a(@NonNull String str, @NonNull Bitmap bitmap) {
        if (str == null || bitmap == null) {
            throw new NullPointerException("key == null || bitmap == null");
        }
        int a = C4726d0.m15718a(bitmap);
        if (a > mo26253a()) {
            this.f9982a.remove(str);
        } else {
            this.f9982a.put(str, new C4736b(bitmap, a));
        }
    }

    @Nullable
    public Bitmap get(@NonNull String str) {
        C4736b bVar = this.f9982a.get(str);
        if (bVar != null) {
            return bVar.f9983a;
        }
        return null;
    }

    public int size() {
        return this.f9982a.size();
    }

    public C4734m(int i) {
        this.f9982a = new C4735a(this, i);
    }

    /* renamed from: a */
    public int mo26253a() {
        return this.f9982a.maxSize();
    }
}
