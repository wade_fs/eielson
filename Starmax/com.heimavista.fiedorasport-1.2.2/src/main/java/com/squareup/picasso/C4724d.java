package com.squareup.picasso;

import android.graphics.Bitmap;

/* renamed from: com.squareup.picasso.d */
/* compiled from: Cache */
public interface C4724d {

    /* renamed from: com.squareup.picasso.d$a */
    /* compiled from: Cache */
    static class C4725a implements C4724d {
        C4725a() {
        }

        /* renamed from: a */
        public int mo26253a() {
            return 0;
        }

        /* renamed from: a */
        public void mo26254a(String str, Bitmap bitmap) {
        }

        public Bitmap get(String str) {
            return null;
        }

        public int size() {
            return 0;
        }
    }

    static {
        new C4725a();
    }

    /* renamed from: a */
    int mo26253a();

    /* renamed from: a */
    void mo26254a(String str, Bitmap bitmap);

    Bitmap get(String str);

    int size();
}
