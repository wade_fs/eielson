package com.squareup.picasso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.internal.view.SupportMenu;
import com.squareup.picasso.Action;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

/* renamed from: com.squareup.picasso.t */
public class Picasso {

    /* renamed from: p */
    static final Handler f10014p = new C4740a(Looper.getMainLooper());
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: q */
    static volatile Picasso f10015q = null;

    /* renamed from: a */
    private final C4744d f10016a;

    /* renamed from: b */
    private final C4747g f10017b;

    /* renamed from: c */
    private final C4742c f10018c;

    /* renamed from: d */
    private final List<RequestHandler> f10019d;

    /* renamed from: e */
    final Context f10020e;

    /* renamed from: f */
    final Dispatcher f10021f;

    /* renamed from: g */
    final C4724d f10022g;

    /* renamed from: h */
    final Stats f10023h;

    /* renamed from: i */
    final Map<Object, Action> f10024i;

    /* renamed from: j */
    final Map<ImageView, DeferredRequestCreator> f10025j;

    /* renamed from: k */
    final ReferenceQueue<Object> f10026k;

    /* renamed from: l */
    final Bitmap.Config f10027l;

    /* renamed from: m */
    boolean f10028m;

    /* renamed from: n */
    volatile boolean f10029n;

    /* renamed from: o */
    boolean f10030o;

    /* renamed from: com.squareup.picasso.t$a */
    /* compiled from: Picasso */
    static class C4740a extends Handler {
        C4740a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i != 3) {
                int i2 = 0;
                if (i == 8) {
                    List list = (List) message.obj;
                    int size = list.size();
                    while (i2 < size) {
                        BitmapHunter cVar = (BitmapHunter) list.get(i2);
                        cVar.f9930Q.mo26314a(cVar);
                        i2++;
                    }
                } else if (i == 13) {
                    List list2 = (List) message.obj;
                    int size2 = list2.size();
                    while (i2 < size2) {
                        Action aVar = (Action) list2.get(i2);
                        aVar.f9878a.mo26316b(aVar);
                        i2++;
                    }
                } else {
                    throw new AssertionError("Unknown handler message received: " + message.what);
                }
            } else {
                Action aVar2 = (Action) message.obj;
                if (aVar2.mo26204e().f10029n) {
                    C4726d0.m15730a("Main", "canceled", aVar2.f9879b.mo26335d(), "target got garbage collected");
                }
                aVar2.f9878a.mo26315a(aVar2.mo26208i());
            }
        }
    }

    /* renamed from: com.squareup.picasso.t$b */
    /* compiled from: Picasso */
    public static class C4741b {

        /* renamed from: a */
        private final Context f10031a;

        /* renamed from: b */
        private Downloader f10032b;

        /* renamed from: c */
        private ExecutorService f10033c;

        /* renamed from: d */
        private C4724d f10034d;

        /* renamed from: e */
        private C4744d f10035e;

        /* renamed from: f */
        private C4747g f10036f;

        /* renamed from: g */
        private List<RequestHandler> f10037g;

        /* renamed from: h */
        private Bitmap.Config f10038h;

        /* renamed from: i */
        private boolean f10039i;

        /* renamed from: j */
        private boolean f10040j;

        public C4741b(@NonNull Context context) {
            if (context != null) {
                this.f10031a = context.getApplicationContext();
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        /* renamed from: a */
        public Picasso mo26319a() {
            Context context = this.f10031a;
            if (this.f10032b == null) {
                this.f10032b = new OkHttp3Downloader(context);
            }
            if (this.f10034d == null) {
                this.f10034d = new C4734m(context);
            }
            if (this.f10033c == null) {
                this.f10033c = new PicassoExecutorService();
            }
            if (this.f10036f == null) {
                this.f10036f = C4747g.f10053a;
            }
            Stats a0Var = new Stats(this.f10034d);
            Context context2 = context;
            return new Picasso(context2, new Dispatcher(context2, this.f10033c, Picasso.f10014p, this.f10032b, this.f10034d, a0Var), this.f10034d, this.f10035e, this.f10036f, this.f10037g, a0Var, this.f10038h, this.f10039i, this.f10040j);
        }
    }

    /* renamed from: com.squareup.picasso.t$c */
    /* compiled from: Picasso */
    private static class C4742c extends Thread {

        /* renamed from: P */
        private final ReferenceQueue<Object> f10041P;

        /* renamed from: Q */
        private final Handler f10042Q;

        /* renamed from: com.squareup.picasso.t$c$a */
        /* compiled from: Picasso */
        class C4743a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Exception f10043P;

            C4743a(C4742c cVar, Exception exc) {
                this.f10043P = exc;
            }

            public void run() {
                throw new RuntimeException(this.f10043P);
            }
        }

        C4742c(ReferenceQueue<Object> referenceQueue, Handler handler) {
            this.f10041P = referenceQueue;
            this.f10042Q = handler;
            setDaemon(true);
            setName("Picasso-refQueue");
        }

        public void run() {
            Process.setThreadPriority(10);
            while (true) {
                try {
                    Action.C4714a aVar = (Action.C4714a) this.f10041P.remove(1000);
                    Message obtainMessage = this.f10042Q.obtainMessage();
                    if (aVar != null) {
                        obtainMessage.what = 3;
                        obtainMessage.obj = aVar.f9890a;
                        this.f10042Q.sendMessage(obtainMessage);
                    } else {
                        obtainMessage.recycle();
                    }
                } catch (InterruptedException unused) {
                    return;
                } catch (Exception e) {
                    this.f10042Q.post(new C4743a(this, e));
                    return;
                }
            }
        }
    }

    /* renamed from: com.squareup.picasso.t$d */
    /* compiled from: Picasso */
    public interface C4744d {
        /* renamed from: a */
        void mo26322a(Picasso tVar, Uri uri, Exception exc);
    }

    /* renamed from: com.squareup.picasso.t$e */
    /* compiled from: Picasso */
    public enum C4745e {
        MEMORY(-16711936),
        DISK(-16776961),
        NETWORK(SupportMenu.CATEGORY_MASK);
        

        /* renamed from: P */
        final int f10048P;

        private C4745e(int i) {
            this.f10048P = i;
        }
    }

    /* renamed from: com.squareup.picasso.t$f */
    /* compiled from: Picasso */
    public enum C4746f {
        LOW,
        NORMAL,
        HIGH
    }

    /* renamed from: com.squareup.picasso.t$g */
    /* compiled from: Picasso */
    public interface C4747g {

        /* renamed from: a */
        public static final C4747g f10053a = new C4748a();

        /* renamed from: com.squareup.picasso.t$g$a */
        /* compiled from: Picasso */
        static class C4748a implements C4747g {
            C4748a() {
            }

            /* renamed from: a */
            public C4750w mo26323a(C4750w wVar) {
                return wVar;
            }
        }

        /* renamed from: a */
        C4750w mo26323a(C4750w wVar);
    }

    Picasso(Context context, Dispatcher iVar, C4724d dVar, C4744d dVar2, C4747g gVar, List<RequestHandler> list, Stats a0Var, Bitmap.Config config, boolean z, boolean z2) {
        this.f10020e = context;
        this.f10021f = iVar;
        this.f10022g = dVar;
        this.f10016a = dVar2;
        this.f10017b = gVar;
        this.f10027l = config;
        ArrayList arrayList = new ArrayList((list != null ? list.size() : 0) + 7);
        arrayList.add(new ResourceRequestHandler(context));
        if (list != null) {
            arrayList.addAll(list);
        }
        arrayList.add(new ContactsPhotoRequestHandler(context));
        arrayList.add(new MediaStoreRequestHandler(context));
        arrayList.add(new ContentStreamRequestHandler(context));
        arrayList.add(new AssetRequestHandler(context));
        arrayList.add(new FileRequestHandler(context));
        arrayList.add(new NetworkRequestHandler(iVar.f9965d, a0Var));
        this.f10019d = Collections.unmodifiableList(arrayList);
        this.f10023h = a0Var;
        this.f10024i = new WeakHashMap();
        this.f10025j = new WeakHashMap();
        this.f10028m = z;
        this.f10029n = z2;
        this.f10026k = new ReferenceQueue<>();
        this.f10018c = new C4742c(this.f10026k, f10014p);
        this.f10018c.start();
    }

    /* renamed from: a */
    public void mo26311a(@NonNull ImageView imageView) {
        if (imageView != null) {
            mo26315a((Object) imageView);
            return;
        }
        throw new IllegalArgumentException("view cannot be null.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26316b(Action aVar) {
        Bitmap a = MemoryPolicy.m15786a(aVar.f9882e) ? mo26307a(aVar.mo26201b()) : null;
        if (a != null) {
            m15798a(a, C4745e.MEMORY, aVar, null);
            if (this.f10029n) {
                String d = aVar.f9879b.mo26335d();
                C4726d0.m15730a("Main", "completed", d, "from " + C4745e.MEMORY);
                return;
            }
            return;
        }
        mo26313a(aVar);
        if (this.f10029n) {
            C4726d0.m15729a("Main", "resumed", aVar.f9879b.mo26335d());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo26317c(Action aVar) {
        this.f10021f.mo26276b(aVar);
    }

    /* renamed from: a */
    public RequestCreator mo26309a(@Nullable Uri uri) {
        return new RequestCreator(this, uri, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public List<RequestHandler> mo26310a() {
        return this.f10019d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C4750w mo26308a(C4750w wVar) {
        this.f10017b.mo26323a(wVar);
        if (wVar != null) {
            return wVar;
        }
        throw new IllegalStateException("Request transformer " + this.f10017b.getClass().getCanonicalName() + " returned null for " + wVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26312a(ImageView imageView, DeferredRequestCreator hVar) {
        if (this.f10025j.containsKey(imageView)) {
            mo26315a((Object) imageView);
        }
        this.f10025j.put(imageView, hVar);
    }

    /* renamed from: b */
    public static Picasso m15799b() {
        if (f10015q == null) {
            synchronized (Picasso.class) {
                if (f10015q == null) {
                    if (PicassoProvider.f9877P != null) {
                        f10015q = new C4741b(PicassoProvider.f9877P).mo26319a();
                    } else {
                        throw new IllegalStateException("context == null");
                    }
                }
            }
        }
        return f10015q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26313a(Action aVar) {
        Object i = aVar.mo26208i();
        if (!(i == null || this.f10024i.get(i) == aVar)) {
            mo26315a(i);
            this.f10024i.put(i, aVar);
        }
        mo26317c(aVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bitmap mo26307a(String str) {
        Bitmap bitmap = this.f10022g.get(str);
        if (bitmap != null) {
            this.f10023h.mo26215b();
        } else {
            this.f10023h.mo26218c();
        }
        return bitmap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26314a(BitmapHunter cVar) {
        Action b = cVar.mo26231b();
        List<Action> c = cVar.mo26233c();
        boolean z = true;
        boolean z2 = c != null && !c.isEmpty();
        if (b == null && !z2) {
            z = false;
        }
        if (z) {
            Uri uri = cVar.mo26234d().f10067d;
            Exception e = cVar.mo26235e();
            Bitmap k = cVar.mo26241k();
            C4745e g = cVar.mo26237g();
            if (b != null) {
                m15798a(k, g, b, e);
            }
            if (z2) {
                int size = c.size();
                for (int i = 0; i < size; i++) {
                    m15798a(k, g, c.get(i), e);
                }
            }
            C4744d dVar = this.f10016a;
            if (dVar != null && e != null) {
                dVar.mo26322a(this, uri, e);
            }
        }
    }

    /* renamed from: a */
    private void m15798a(Bitmap bitmap, C4745e eVar, Action aVar, Exception exc) {
        if (!aVar.mo26209j()) {
            if (!aVar.mo26210k()) {
                this.f10024i.remove(aVar.mo26208i());
            }
            if (bitmap == null) {
                aVar.mo26200a(exc);
                if (this.f10029n) {
                    C4726d0.m15730a("Main", "errored", aVar.f9879b.mo26335d(), exc.getMessage());
                }
            } else if (eVar != null) {
                aVar.mo26199a(bitmap, eVar);
                if (this.f10029n) {
                    String d = aVar.f9879b.mo26335d();
                    C4726d0.m15730a("Main", "completed", d, "from " + eVar);
                }
            } else {
                throw new AssertionError("LoadedFrom cannot be null.");
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26315a(Object obj) {
        DeferredRequestCreator remove;
        C4726d0.m15727a();
        Action remove2 = this.f10024i.remove(obj);
        if (remove2 != null) {
            remove2.mo26198a();
            this.f10021f.mo26269a(remove2);
        }
        if ((obj instanceof ImageView) && (remove = this.f10025j.remove((ImageView) obj)) != null) {
            remove.mo26263a();
        }
    }
}
