package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import p244j.Okio;

/* renamed from: com.squareup.picasso.o */
class MediaStoreRequestHandler extends ContentStreamRequestHandler {

    /* renamed from: b */
    private static final String[] f9992b = {"orientation"};

    /* renamed from: com.squareup.picasso.o$a */
    /* compiled from: MediaStoreRequestHandler */
    enum C4737a {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);
        

        /* renamed from: P */
        final int f9997P;

        /* renamed from: Q */
        final int f9998Q;

        /* renamed from: R */
        final int f9999R;

        private C4737a(int i, int i2, int i3) {
            this.f9997P = i;
            this.f9998Q = i2;
            this.f9999R = i3;
        }
    }

    MediaStoreRequestHandler(Context context) {
        super(context);
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        Uri uri = wVar.f10067d;
        return "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        Bitmap bitmap;
        C4750w wVar2 = wVar;
        ContentResolver contentResolver = super.f9958a.getContentResolver();
        int a = m15782a(contentResolver, wVar2.f10067d);
        String type = contentResolver.getType(wVar2.f10067d);
        boolean z = type != null && type.startsWith("video/");
        if (wVar.mo26334c()) {
            C4737a a2 = m15783a(wVar2.f10071h, wVar2.f10072i);
            if (!z && a2 == C4737a.FULL) {
                return new RequestHandler.C4753a(null, Okio.m18218a(mo26262c(wVar)), Picasso.C4745e.DISK, a);
            }
            long parseId = ContentUris.parseId(wVar2.f10067d);
            BitmapFactory.Options b = RequestHandler.m15844b(wVar);
            b.inJustDecodeBounds = true;
            BitmapFactory.Options options = b;
            RequestHandler.m15841a(wVar2.f10071h, wVar2.f10072i, a2.f9998Q, a2.f9999R, b, wVar);
            if (z) {
                bitmap = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, a2 == C4737a.FULL ? 1 : a2.f9997P, options);
            } else {
                bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, a2.f9997P, options);
            }
            if (bitmap != null) {
                return new RequestHandler.C4753a(bitmap, null, Picasso.C4745e.DISK, a);
            }
        }
        return new RequestHandler.C4753a(null, Okio.m18218a(mo26262c(wVar)), Picasso.C4745e.DISK, a);
    }

    /* renamed from: a */
    static C4737a m15783a(int i, int i2) {
        C4737a aVar = C4737a.MICRO;
        if (i <= aVar.f9998Q && i2 <= aVar.f9999R) {
            return aVar;
        }
        C4737a aVar2 = C4737a.MINI;
        if (i > aVar2.f9998Q || i2 > aVar2.f9999R) {
            return C4737a.FULL;
        }
        return aVar2;
    }

    /* renamed from: a */
    static int m15782a(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = null;
        try {
            Cursor query = contentResolver.query(uri, f9992b, null, null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    if (query != null) {
                        query.close();
                    }
                    return i;
                }
            }
            if (query != null) {
                query.close();
            }
            return 0;
        } catch (RuntimeException unused) {
            if (cursor != null) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
