package com.squareup.picasso;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.squareup.picasso.n */
final class MarkableInputStream extends InputStream {

    /* renamed from: P */
    private final InputStream f9985P;

    /* renamed from: Q */
    private long f9986Q;

    /* renamed from: R */
    private long f9987R;

    /* renamed from: S */
    private long f9988S;

    /* renamed from: T */
    private long f9989T;

    /* renamed from: U */
    private boolean f9990U;

    /* renamed from: V */
    private int f9991V;

    MarkableInputStream(InputStream inputStream) {
        this(super, 4096);
    }

    /* renamed from: h */
    private void m15778h(long j) {
        try {
            if (this.f9987R >= this.f9986Q || this.f9986Q > this.f9988S) {
                this.f9987R = this.f9986Q;
                this.f9985P.mark((int) (j - this.f9986Q));
            } else {
                this.f9985P.reset();
                this.f9985P.mark((int) (j - this.f9987R));
                m15777a(this.f9987R, this.f9986Q);
            }
            this.f9988S = j;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to mark: " + e);
        }
    }

    /* renamed from: a */
    public long mo26292a(int i) {
        long j = this.f9986Q + ((long) i);
        if (this.f9988S < j) {
            m15778h(j);
        }
        return this.f9986Q;
    }

    public int available() {
        return this.f9985P.available();
    }

    public void close() {
        this.f9985P.close();
    }

    /* renamed from: g */
    public void mo26296g(long j) {
        if (this.f9986Q > this.f9988S || j < this.f9987R) {
            throw new IOException("Cannot reset");
        }
        this.f9985P.reset();
        m15777a(this.f9987R, j);
        this.f9986Q = j;
    }

    public void mark(int i) {
        this.f9989T = mo26292a(i);
    }

    public boolean markSupported() {
        return this.f9985P.markSupported();
    }

    public int read() {
        if (!this.f9990U) {
            long j = this.f9988S;
            if (this.f9986Q + 1 > j) {
                m15778h(j + ((long) this.f9991V));
            }
        }
        int read = this.f9985P.read();
        if (read != -1) {
            this.f9986Q++;
        }
        return read;
    }

    public void reset() {
        mo26296g(this.f9989T);
    }

    public long skip(long j) {
        if (!this.f9990U) {
            long j2 = this.f9986Q;
            if (j2 + j > this.f9988S) {
                m15778h(j2 + j + ((long) this.f9991V));
            }
        }
        long skip = this.f9985P.skip(j);
        this.f9986Q += skip;
        return skip;
    }

    MarkableInputStream(InputStream inputStream, int i) {
        this(super, i, 1024);
    }

    private MarkableInputStream(InputStream inputStream, int i, int i2) {
        this.f9989T = -1;
        this.f9990U = true;
        this.f9991V = -1;
        this.f9985P = !super.markSupported() ? new BufferedInputStream(super, i) : inputStream;
        this.f9991V = i2;
    }

    /* renamed from: a */
    public void mo26293a(boolean z) {
        this.f9990U = z;
    }

    public int read(byte[] bArr) {
        if (!this.f9990U) {
            long j = this.f9986Q;
            if (((long) bArr.length) + j > this.f9988S) {
                m15778h(j + ((long) bArr.length) + ((long) this.f9991V));
            }
        }
        int read = this.f9985P.read(bArr);
        if (read != -1) {
            this.f9986Q += (long) read;
        }
        return read;
    }

    /* renamed from: a */
    private void m15777a(long j, long j2) {
        while (j < j2) {
            long skip = this.f9985P.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        if (!this.f9990U) {
            long j = this.f9986Q;
            long j2 = (long) i2;
            if (j + j2 > this.f9988S) {
                m15778h(j + j2 + ((long) this.f9991V));
            }
        }
        int read = this.f9985P.read(bArr, i, i2);
        if (read != -1) {
            this.f9986Q += (long) read;
        }
        return read;
    }
}
