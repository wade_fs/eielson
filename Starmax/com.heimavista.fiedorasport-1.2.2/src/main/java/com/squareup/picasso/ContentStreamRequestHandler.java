package com.squareup.picasso;

import android.content.Context;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.InputStream;
import p244j.Okio;

/* renamed from: com.squareup.picasso.g */
class ContentStreamRequestHandler extends RequestHandler {

    /* renamed from: a */
    final Context f9958a;

    ContentStreamRequestHandler(Context context) {
        this.f9958a = context;
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        return "content".equals(wVar.f10067d.getScheme());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public InputStream mo26262c(C4750w wVar) {
        return this.f9958a.getContentResolver().openInputStream(wVar.f10067d);
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        return new RequestHandler.C4753a(Okio.m18218a(mo26262c(wVar)), Picasso.C4745e.DISK);
    }
}
