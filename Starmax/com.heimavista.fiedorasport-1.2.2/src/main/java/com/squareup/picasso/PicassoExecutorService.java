package com.squareup.picasso;

import android.net.NetworkInfo;
import com.squareup.picasso.C4726d0;
import com.squareup.picasso.Picasso;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.squareup.picasso.v */
class PicassoExecutorService extends ThreadPoolExecutor {

    /* renamed from: com.squareup.picasso.v$a */
    /* compiled from: PicassoExecutorService */
    private static final class C4749a extends FutureTask<BitmapHunter> implements Comparable<C4749a> {

        /* renamed from: P */
        private final BitmapHunter f10062P;

        C4749a(BitmapHunter cVar) {
            super(cVar, null);
            this.f10062P = cVar;
        }

        /* renamed from: a */
        public int compareTo(C4749a aVar) {
            Picasso.C4746f j = this.f10062P.mo26240j();
            Picasso.C4746f j2 = aVar.f10062P.mo26240j();
            return j == j2 ? this.f10062P.f9929P - aVar.f10062P.f9929P : j2.ordinal() - j.ordinal();
        }
    }

    PicassoExecutorService() {
        super(3, 3, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new C4726d0.C4729c());
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26328a(NetworkInfo networkInfo) {
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            m15819a(3);
            return;
        }
        int type = networkInfo.getType();
        if (type == 0) {
            int subtype = networkInfo.getSubtype();
            switch (subtype) {
                case 1:
                case 2:
                    m15819a(1);
                    return;
                case 3:
                case 4:
                case 5:
                case 6:
                    break;
                default:
                    switch (subtype) {
                        case 12:
                            break;
                        case 13:
                        case 14:
                        case 15:
                            m15819a(3);
                            return;
                        default:
                            m15819a(3);
                            return;
                    }
            }
            m15819a(2);
        } else if (type == 1 || type == 6 || type == 9) {
            m15819a(4);
        } else {
            m15819a(3);
        }
    }

    public Future<?> submit(Runnable runnable) {
        C4749a aVar = new C4749a((BitmapHunter) runnable);
        execute(aVar);
        return aVar;
    }

    /* renamed from: a */
    private void m15819a(int i) {
        setCorePoolSize(i);
        setMaximumPoolSize(i);
    }
}
