package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

/* renamed from: com.squareup.picasso.u */
final class PicassoDrawable extends BitmapDrawable {

    /* renamed from: h */
    private static final Paint f10054h = new Paint();

    /* renamed from: a */
    private final boolean f10055a;

    /* renamed from: b */
    private final float f10056b;

    /* renamed from: c */
    private final Picasso.C4745e f10057c;

    /* renamed from: d */
    Drawable f10058d;

    /* renamed from: e */
    long f10059e;

    /* renamed from: f */
    boolean f10060f;

    /* renamed from: g */
    int f10061g = 255;

    PicassoDrawable(Context context, Bitmap bitmap, Drawable drawable, Picasso.C4745e eVar, boolean z, boolean z2) {
        super(context.getResources(), bitmap);
        this.f10055a = z2;
        this.f10056b = context.getResources().getDisplayMetrics().density;
        this.f10057c = eVar;
        if (eVar != Picasso.C4745e.MEMORY && !z) {
            this.f10058d = drawable;
            this.f10060f = true;
            this.f10059e = SystemClock.uptimeMillis();
        }
    }

    /* renamed from: a */
    static void m15817a(ImageView imageView, Context context, Bitmap bitmap, Picasso.C4745e eVar, boolean z, boolean z2) {
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).stop();
        }
        imageView.setImageDrawable(new PicassoDrawable(context, bitmap, drawable, eVar, z, z2));
    }

    public void draw(Canvas canvas) {
        if (!this.f10060f) {
            super.draw(canvas);
        } else {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.f10059e)) / 200.0f;
            if (uptimeMillis >= 1.0f) {
                this.f10060f = false;
                this.f10058d = null;
                super.draw(canvas);
            } else {
                Drawable drawable = this.f10058d;
                if (drawable != null) {
                    drawable.draw(canvas);
                }
                super.setAlpha((int) (((float) this.f10061g) * uptimeMillis));
                super.draw(canvas);
                super.setAlpha(this.f10061g);
            }
        }
        if (this.f10055a) {
            m15816a(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f10058d;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        super.onBoundsChange(rect);
    }

    public void setAlpha(int i) {
        this.f10061g = i;
        Drawable drawable = this.f10058d;
        if (drawable != null) {
            drawable.setAlpha(i);
        }
        super.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.f10058d;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        }
        super.setColorFilter(colorFilter);
    }

    /* renamed from: a */
    static void m15818a(ImageView imageView, Drawable drawable) {
        imageView.setImageDrawable(drawable);
        if (imageView.getDrawable() instanceof Animatable) {
            ((Animatable) imageView.getDrawable()).start();
        }
    }

    /* renamed from: a */
    private void m15816a(Canvas canvas) {
        f10054h.setColor(-1);
        canvas.drawPath(m15815a(0, 0, (int) (this.f10056b * 16.0f)), f10054h);
        f10054h.setColor(this.f10057c.f10048P);
        canvas.drawPath(m15815a(0, 0, (int) (this.f10056b * 15.0f)), f10054h);
    }

    /* renamed from: a */
    private static Path m15815a(int i, int i2, int i3) {
        Path path = new Path();
        float f = (float) i;
        float f2 = (float) i2;
        path.moveTo(f, f2);
        path.lineTo((float) (i + i3), f2);
        path.lineTo(f, (float) (i2 + i3));
        return path;
    }
}
