package com.squareup.picasso;

import android.net.NetworkInfo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.IOException;
import p231i.C5023y;
import p231i.CacheControl;
import p231i.Response;
import p231i.ResponseBody;

/* renamed from: com.squareup.picasso.r */
class NetworkRequestHandler extends RequestHandler {

    /* renamed from: a */
    private final Downloader f10009a;

    /* renamed from: b */
    private final Stats f10010b;

    /* renamed from: com.squareup.picasso.r$a */
    /* compiled from: NetworkRequestHandler */
    static class C4738a extends IOException {
        C4738a(String str) {
            super(str);
        }
    }

    /* renamed from: com.squareup.picasso.r$b */
    /* compiled from: NetworkRequestHandler */
    static final class C4739b extends IOException {

        /* renamed from: P */
        final int f10011P;

        /* renamed from: Q */
        final int f10012Q;

        C4739b(int i, int i2) {
            super("HTTP " + i);
            this.f10011P = i;
            this.f10012Q = i2;
        }
    }

    NetworkRequestHandler(Downloader jVar, Stats a0Var) {
        this.f10009a = jVar;
        this.f10010b = a0Var;
    }

    /* renamed from: b */
    private static C5023y m15791b(C4750w wVar, int i) {
        CacheControl dVar;
        if (i == 0) {
            dVar = null;
        } else if (NetworkPolicy.m15788a(i)) {
            dVar = CacheControl.f11272n;
        } else {
            CacheControl.C4948a aVar = new CacheControl.C4948a();
            if (!NetworkPolicy.m15789b(i)) {
                aVar.mo27391b();
            }
            if (!NetworkPolicy.m15790c(i)) {
                aVar.mo27392c();
            }
            dVar = aVar.mo27390a();
        }
        C5023y.C5024a aVar2 = new C5023y.C5024a();
        aVar2.mo27836b(wVar.f10067d.toString());
        if (dVar != null) {
            aVar2.mo27829a(dVar);
        }
        return aVar2.mo27835a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo26304a() {
        return 2;
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        String scheme = wVar.f10067d.getScheme();
        return "http".equals(scheme) || "https".equals(scheme);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo26306b() {
        return true;
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        Response a = this.f10009a.mo26289a(m15791b(wVar, i));
        ResponseBody a2 = a.mo27315a();
        if (a.mo27324t()) {
            Picasso.C4745e eVar = a.mo27319c() == null ? Picasso.C4745e.NETWORK : Picasso.C4745e.DISK;
            if (eVar == Picasso.C4745e.DISK && a2.mo27347a() == 0) {
                a2.close();
                throw new C4738a("Received response with 0 content-length header.");
            }
            if (eVar == Picasso.C4745e.NETWORK && a2.mo27347a() > 0) {
                this.f10010b.mo26212a(a2.mo27347a());
            }
            return new RequestHandler.C4753a(a2.mo27348b(), eVar);
        }
        a2.close();
        throw new C4739b(a.mo27321d(), wVar.f10066c);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo26305a(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }
}
