package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import android.os.Build;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import p244j.BufferedSource;
import p244j.C5049s;
import p244j.Okio;

/* renamed from: com.squareup.picasso.c */
class BitmapHunter implements Runnable {

    /* renamed from: i0 */
    private static final Object f9925i0 = new Object();

    /* renamed from: j0 */
    private static final ThreadLocal<StringBuilder> f9926j0 = new C4717a();

    /* renamed from: k0 */
    private static final AtomicInteger f9927k0 = new AtomicInteger();

    /* renamed from: l0 */
    private static final RequestHandler f9928l0 = new C4718b();

    /* renamed from: P */
    final int f9929P = f9927k0.incrementAndGet();

    /* renamed from: Q */
    final Picasso f9930Q;

    /* renamed from: R */
    final Dispatcher f9931R;

    /* renamed from: S */
    final C4724d f9932S;

    /* renamed from: T */
    final Stats f9933T;

    /* renamed from: U */
    final String f9934U;

    /* renamed from: V */
    final C4750w f9935V;

    /* renamed from: W */
    final int f9936W;

    /* renamed from: X */
    int f9937X;

    /* renamed from: Y */
    final RequestHandler f9938Y;

    /* renamed from: Z */
    Action f9939Z;

    /* renamed from: a0 */
    List<Action> f9940a0;

    /* renamed from: b0 */
    Bitmap f9941b0;

    /* renamed from: c0 */
    Future<?> f9942c0;

    /* renamed from: d0 */
    Picasso.C4745e f9943d0;

    /* renamed from: e0 */
    Exception f9944e0;

    /* renamed from: f0 */
    int f9945f0;

    /* renamed from: g0 */
    int f9946g0;

    /* renamed from: h0 */
    Picasso.C4746f f9947h0;

    /* renamed from: com.squareup.picasso.c$a */
    /* compiled from: BitmapHunter */
    static class C4717a extends ThreadLocal<StringBuilder> {
        C4717a() {
        }

        /* access modifiers changed from: protected */
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    /* renamed from: com.squareup.picasso.c$b */
    /* compiled from: BitmapHunter */
    static class C4718b extends RequestHandler {
        C4718b() {
        }

        /* renamed from: a */
        public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
            throw new IllegalStateException("Unrecognized type of request: " + wVar);
        }

        /* renamed from: a */
        public boolean mo26225a(C4750w wVar) {
            return true;
        }
    }

    /* renamed from: com.squareup.picasso.c$c */
    /* compiled from: BitmapHunter */
    static class C4719c implements Runnable {

        /* renamed from: P */
        final /* synthetic */ C4723c0 f9948P;

        /* renamed from: Q */
        final /* synthetic */ RuntimeException f9949Q;

        C4719c(C4723c0 c0Var, RuntimeException runtimeException) {
            this.f9948P = c0Var;
            this.f9949Q = runtimeException;
        }

        public void run() {
            throw new RuntimeException("Transformation " + this.f9948P.mo26252a() + " crashed with exception.", this.f9949Q);
        }
    }

    /* renamed from: com.squareup.picasso.c$d */
    /* compiled from: BitmapHunter */
    static class C4720d implements Runnable {

        /* renamed from: P */
        final /* synthetic */ StringBuilder f9950P;

        C4720d(StringBuilder sb) {
            this.f9950P = sb;
        }

        public void run() {
            throw new NullPointerException(this.f9950P.toString());
        }
    }

    /* renamed from: com.squareup.picasso.c$e */
    /* compiled from: BitmapHunter */
    static class C4721e implements Runnable {

        /* renamed from: P */
        final /* synthetic */ C4723c0 f9951P;

        C4721e(C4723c0 c0Var) {
            this.f9951P = c0Var;
        }

        public void run() {
            throw new IllegalStateException("Transformation " + this.f9951P.mo26252a() + " returned input Bitmap but recycled it.");
        }
    }

    /* renamed from: com.squareup.picasso.c$f */
    /* compiled from: BitmapHunter */
    static class C4722f implements Runnable {

        /* renamed from: P */
        final /* synthetic */ C4723c0 f9952P;

        C4722f(C4723c0 c0Var) {
            this.f9952P = c0Var;
        }

        public void run() {
            throw new IllegalStateException("Transformation " + this.f9952P.mo26252a() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    BitmapHunter(Picasso tVar, Dispatcher iVar, C4724d dVar, Stats a0Var, Action aVar, RequestHandler yVar) {
        this.f9930Q = tVar;
        this.f9931R = iVar;
        this.f9932S = dVar;
        this.f9933T = a0Var;
        this.f9939Z = aVar;
        this.f9934U = aVar.mo26201b();
        this.f9935V = aVar.mo26206g();
        this.f9947h0 = aVar.mo26205f();
        this.f9936W = aVar.mo26202c();
        this.f9937X = aVar.mo26203d();
        this.f9938Y = yVar;
        this.f9946g0 = yVar.mo26304a();
    }

    /* renamed from: a */
    static int m15682a(int i) {
        switch (i) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return 270;
            default:
                return 0;
        }
    }

    /* renamed from: a */
    static Bitmap m15684a(C5049s sVar, C4750w wVar) {
        BufferedSource a = Okio.m18212a(sVar);
        boolean a2 = C4726d0.m15731a(a);
        boolean z = wVar.f10081r && Build.VERSION.SDK_INT < 21;
        BitmapFactory.Options b = RequestHandler.m15844b(wVar);
        boolean a3 = RequestHandler.m15843a(b);
        if (a2 || z) {
            byte[] i = a.mo28061i();
            if (a3) {
                BitmapFactory.decodeByteArray(i, 0, i.length, b);
                RequestHandler.m15842a(wVar.f10071h, wVar.f10072i, b, wVar);
            }
            return BitmapFactory.decodeByteArray(i, 0, i.length, b);
        }
        InputStream q = a.mo28070q();
        if (a3) {
            MarkableInputStream nVar = new MarkableInputStream(q);
            nVar.mo26293a(false);
            long a4 = nVar.mo26292a(1024);
            BitmapFactory.decodeStream(nVar, null, b);
            RequestHandler.m15842a(wVar.f10071h, wVar.f10072i, b, wVar);
            nVar.mo26296g(a4);
            nVar.mo26293a(true);
            q = nVar;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(q, null, b);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    /* renamed from: a */
    private static boolean m15688a(boolean z, int i, int i2, int i3, int i4) {
        return !z || (i3 != 0 && i > i3) || (i4 != 0 && i2 > i4);
    }

    /* renamed from: b */
    static int m15689b(int i) {
        return (i == 2 || i == 7 || i == 4 || i == 5) ? -1 : 1;
    }

    /* renamed from: o */
    private Picasso.C4746f m15690o() {
        Picasso.C4746f fVar = Picasso.C4746f.LOW;
        List<Action> list = this.f9940a0;
        boolean z = true;
        boolean z2 = list != null && !list.isEmpty();
        if (this.f9939Z == null && !z2) {
            z = false;
        }
        if (!z) {
            return fVar;
        }
        Action aVar = this.f9939Z;
        if (aVar != null) {
            fVar = aVar.mo26205f();
        }
        if (z2) {
            int size = this.f9940a0.size();
            for (int i = 0; i < size; i++) {
                Picasso.C4746f f = this.f9940a0.get(i).mo26205f();
                if (f.ordinal() > fVar.ordinal()) {
                    fVar = f;
                }
            }
        }
        return fVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26232b(Action aVar) {
        boolean z;
        if (this.f9939Z == aVar) {
            this.f9939Z = null;
            z = true;
        } else {
            List<Action> list = this.f9940a0;
            z = list != null ? list.remove(aVar) : false;
        }
        if (z && aVar.mo26205f() == this.f9947h0) {
            this.f9947h0 = m15690o();
        }
        if (this.f9930Q.f10029n) {
            C4726d0.m15730a("Hunter", "removed", aVar.f9879b.mo26335d(), C4726d0.m15724a(this, "from "));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public List<Action> mo26233c() {
        return this.f9940a0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public C4750w mo26234d() {
        return this.f9935V;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public Exception mo26235e() {
        return this.f9944e0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public String mo26236f() {
        return this.f9934U;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public Picasso.C4745e mo26237g() {
        return this.f9943d0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public int mo26238h() {
        return this.f9936W;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public Picasso mo26239i() {
        return this.f9930Q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public Picasso.C4746f mo26240j() {
        return this.f9947h0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public Bitmap mo26241k() {
        return this.f9941b0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public Bitmap mo26242l() {
        Bitmap bitmap;
        if (MemoryPolicy.m15786a(this.f9936W)) {
            bitmap = this.f9932S.get(this.f9934U);
            if (bitmap != null) {
                this.f9933T.mo26215b();
                this.f9943d0 = Picasso.C4745e.MEMORY;
                if (this.f9930Q.f10029n) {
                    C4726d0.m15730a("Hunter", "decoded", this.f9935V.mo26335d(), "from cache");
                }
                return bitmap;
            }
        } else {
            bitmap = null;
        }
        this.f9937X = this.f9946g0 == 0 ? NetworkPolicy.OFFLINE.f10008P : this.f9937X;
        RequestHandler.C4753a a = this.f9938Y.mo26224a(this.f9935V, this.f9937X);
        if (a != null) {
            this.f9943d0 = a.mo26352c();
            this.f9945f0 = a.mo26351b();
            bitmap = a.mo26350a();
            if (bitmap == null) {
                C5049s d = a.mo26353d();
                try {
                    bitmap = m15684a(d, this.f9935V);
                } finally {
                    try {
                        d.close();
                    } catch (IOException unused) {
                    }
                }
            }
        }
        if (bitmap != null) {
            if (this.f9930Q.f10029n) {
                C4726d0.m15729a("Hunter", "decoded", this.f9935V.mo26335d());
            }
            this.f9933T.mo26213a(bitmap);
            if (this.f9935V.mo26337f() || this.f9945f0 != 0) {
                synchronized (f9925i0) {
                    if (this.f9935V.mo26336e() || this.f9945f0 != 0) {
                        bitmap = m15683a(this.f9935V, bitmap, this.f9945f0);
                        if (this.f9930Q.f10029n) {
                            C4726d0.m15729a("Hunter", "transformed", this.f9935V.mo26335d());
                        }
                    }
                    if (this.f9935V.mo26333b()) {
                        bitmap = m15685a(this.f9935V.f10070g, bitmap);
                        if (this.f9930Q.f10029n) {
                            C4726d0.m15730a("Hunter", "transformed", this.f9935V.mo26335d(), "from custom transformations");
                        }
                    }
                }
                if (bitmap != null) {
                    this.f9933T.mo26217b(bitmap);
                }
            }
        }
        return bitmap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public boolean mo26243m() {
        Future<?> future = this.f9942c0;
        return future != null && future.isCancelled();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: n */
    public boolean mo26244n() {
        return this.f9938Y.mo26306b();
    }

    public void run() {
        try {
            m15687a(this.f9935V);
            if (this.f9930Q.f10029n) {
                C4726d0.m15729a("Hunter", "executing", C4726d0.m15723a(this));
            }
            this.f9941b0 = mo26242l();
            if (this.f9941b0 == null) {
                this.f9931R.mo26277b(this);
            } else {
                this.f9931R.mo26271a(this);
            }
        } catch (NetworkRequestHandler.C4739b e) {
            if (!NetworkPolicy.m15788a(e.f10012Q) || e.f10011P != 504) {
                this.f9944e0 = e;
            }
            this.f9931R.mo26277b(this);
        } catch (IOException e2) {
            this.f9944e0 = e2;
            this.f9931R.mo26281c(this);
        } catch (OutOfMemoryError e3) {
            StringWriter stringWriter = new StringWriter();
            this.f9933T.mo26211a().mo26226a(new PrintWriter(stringWriter));
            this.f9944e0 = new RuntimeException(stringWriter.toString(), e3);
            this.f9931R.mo26277b(this);
        } catch (Exception e4) {
            this.f9944e0 = e4;
            this.f9931R.mo26277b(this);
        } catch (Throwable th) {
            Thread.currentThread().setName("Picasso-Idle");
            throw th;
        }
        Thread.currentThread().setName("Picasso-Idle");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Action mo26231b() {
        return this.f9939Z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26228a(Action aVar) {
        boolean z = this.f9930Q.f10029n;
        C4750w wVar = aVar.f9879b;
        if (this.f9939Z == null) {
            this.f9939Z = aVar;
            if (z) {
                List<Action> list = this.f9940a0;
                if (list == null || list.isEmpty()) {
                    C4726d0.m15730a("Hunter", "joined", wVar.mo26335d(), "to empty hunter");
                } else {
                    C4726d0.m15730a("Hunter", "joined", wVar.mo26335d(), C4726d0.m15724a(this, "to "));
                }
            }
        } else {
            if (this.f9940a0 == null) {
                this.f9940a0 = new ArrayList(3);
            }
            this.f9940a0.add(aVar);
            if (z) {
                C4726d0.m15730a("Hunter", "joined", wVar.mo26335d(), C4726d0.m15724a(this, "to "));
            }
            Picasso.C4746f f = aVar.mo26205f();
            if (f.ordinal() > this.f9947h0.ordinal()) {
                this.f9947h0 = f;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo26229a() {
        Future<?> future;
        if (this.f9939Z != null) {
            return false;
        }
        List<Action> list = this.f9940a0;
        if ((list == null || list.isEmpty()) && (future = this.f9942c0) != null && future.cancel(false)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo26230a(boolean z, NetworkInfo networkInfo) {
        if (!(this.f9946g0 > 0)) {
            return false;
        }
        this.f9946g0--;
        return this.f9938Y.mo26305a(z, networkInfo);
    }

    /* renamed from: a */
    static void m15687a(C4750w wVar) {
        String a = wVar.mo26332a();
        StringBuilder sb = f9926j0.get();
        sb.ensureCapacity(a.length() + 8);
        sb.replace(8, sb.length(), a);
        Thread.currentThread().setName(sb.toString());
    }

    /* renamed from: a */
    static BitmapHunter m15686a(Picasso tVar, Dispatcher iVar, C4724d dVar, Stats a0Var, Action aVar) {
        C4750w g = aVar.mo26206g();
        List<RequestHandler> a = tVar.mo26310a();
        int size = a.size();
        for (int i = 0; i < size; i++) {
            RequestHandler yVar = a.get(i);
            if (yVar.mo26225a(g)) {
                return new BitmapHunter(tVar, iVar, dVar, a0Var, aVar, yVar);
            }
        }
        return new BitmapHunter(tVar, iVar, dVar, a0Var, aVar, f9928l0);
    }

    /* renamed from: a */
    static Bitmap m15685a(List<C4723c0> list, Bitmap bitmap) {
        int size = list.size();
        int i = 0;
        while (i < size) {
            C4723c0 c0Var = list.get(i);
            try {
                Bitmap a = c0Var.mo26251a(bitmap);
                if (a == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(c0Var.mo26252a());
                    sb.append(" returned null after ");
                    sb.append(i);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (C4723c0 c0Var2 : list) {
                        sb.append(c0Var2.mo26252a());
                        sb.append(10);
                    }
                    Picasso.f10014p.post(new C4720d(sb));
                    return null;
                } else if (a == bitmap && bitmap.isRecycled()) {
                    Picasso.f10014p.post(new C4721e(c0Var));
                    return null;
                } else if (a == bitmap || bitmap.isRecycled()) {
                    i++;
                    bitmap = a;
                } else {
                    Picasso.f10014p.post(new C4722f(c0Var));
                    return null;
                }
            } catch (RuntimeException e) {
                Picasso.f10014p.post(new C4719c(c0Var, e));
                return null;
            }
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0267  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.graphics.Bitmap m15683a(com.squareup.picasso.C4750w r26, android.graphics.Bitmap r27, int r28) {
        /*
            r0 = r26
            int r1 = r27.getWidth()
            int r2 = r27.getHeight()
            boolean r3 = r0.f10076m
            android.graphics.Matrix r9 = new android.graphics.Matrix
            r9.<init>()
            boolean r4 = r26.mo26336e()
            if (r4 != 0) goto L_0x001f
            if (r28 == 0) goto L_0x001a
            goto L_0x001f
        L_0x001a:
            r3 = r1
            r5 = r2
            r0 = r9
            goto L_0x0253
        L_0x001f:
            int r4 = r0.f10071h
            int r6 = r0.f10072i
            float r7 = r0.f10077n
            r8 = 0
            int r8 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r8 == 0) goto L_0x0144
            double r10 = (double) r7
            double r12 = java.lang.Math.toRadians(r10)
            double r12 = java.lang.Math.cos(r12)
            double r10 = java.lang.Math.toRadians(r10)
            double r10 = java.lang.Math.sin(r10)
            boolean r4 = r0.f10080q
            if (r4 == 0) goto L_0x00d5
            float r4 = r0.f10078o
            float r6 = r0.f10079p
            r9.setRotate(r7, r4, r6)
            float r4 = r0.f10078o
            double r6 = (double) r4
            r14 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r14 = r14 - r12
            double r6 = r6 * r14
            float r8 = r0.f10079p
            r16 = r2
            r17 = r3
            double r2 = (double) r8
            double r2 = r2 * r10
            double r6 = r6 + r2
            double r2 = (double) r8
            double r2 = r2 * r14
            double r14 = (double) r4
            double r14 = r14 * r10
            double r2 = r2 - r14
            int r4 = r0.f10071h
            double r14 = (double) r4
            double r14 = r14 * r12
            double r14 = r14 + r6
            r18 = r6
            double r5 = (double) r4
            double r5 = r5 * r10
            double r5 = r5 + r2
            r20 = r9
            double r8 = (double) r4
            double r8 = r8 * r12
            double r7 = r18 + r8
            int r9 = r0.f10072i
            r22 = r1
            double r0 = (double) r9
            double r0 = r0 * r10
            double r7 = r7 - r0
            double r0 = (double) r4
            double r0 = r0 * r10
            double r0 = r0 + r2
            r23 = r5
            double r4 = (double) r9
            double r4 = r4 * r12
            double r0 = r0 + r4
            double r4 = (double) r9
            double r4 = r4 * r10
            double r4 = r18 - r4
            double r9 = (double) r9
            double r9 = r9 * r12
            double r9 = r9 + r2
            r11 = r18
            r18 = r9
            double r9 = java.lang.Math.max(r11, r14)
            double r9 = java.lang.Math.max(r7, r9)
            double r9 = java.lang.Math.max(r4, r9)
            double r11 = java.lang.Math.min(r11, r14)
            double r6 = java.lang.Math.min(r7, r11)
            double r4 = java.lang.Math.min(r4, r6)
            r6 = r23
            double r11 = java.lang.Math.max(r2, r6)
            double r11 = java.lang.Math.max(r0, r11)
            r13 = r18
            double r11 = java.lang.Math.max(r13, r11)
            double r2 = java.lang.Math.min(r2, r6)
            double r0 = java.lang.Math.min(r0, r2)
            double r0 = java.lang.Math.min(r13, r0)
            double r9 = r9 - r4
            double r2 = java.lang.Math.floor(r9)
            int r4 = (int) r2
            double r11 = r11 - r0
            double r0 = java.lang.Math.floor(r11)
            int r6 = (int) r0
            r0 = r20
            goto L_0x014b
        L_0x00d5:
            r22 = r1
            r16 = r2
            r17 = r3
            r0 = r9
            r0.setRotate(r7)
            r1 = r26
            int r2 = r1.f10071h
            double r3 = (double) r2
            double r3 = r3 * r12
            double r5 = (double) r2
            double r5 = r5 * r10
            double r7 = (double) r2
            double r7 = r7 * r12
            int r9 = r1.f10072i
            double r14 = (double) r9
            double r14 = r14 * r10
            double r7 = r7 - r14
            double r14 = (double) r2
            double r14 = r14 * r10
            double r1 = (double) r9
            double r1 = r1 * r12
            double r14 = r14 + r1
            double r1 = (double) r9
            double r1 = r1 * r10
            double r1 = -r1
            double r9 = (double) r9
            double r9 = r9 * r12
            r11 = 0
            r18 = r9
            double r9 = java.lang.Math.max(r11, r3)
            double r9 = java.lang.Math.max(r7, r9)
            double r9 = java.lang.Math.max(r1, r9)
            double r3 = java.lang.Math.min(r11, r3)
            double r3 = java.lang.Math.min(r7, r3)
            double r1 = java.lang.Math.min(r1, r3)
            double r3 = java.lang.Math.max(r11, r5)
            double r3 = java.lang.Math.max(r14, r3)
            r7 = r18
            double r3 = java.lang.Math.max(r7, r3)
            double r5 = java.lang.Math.min(r11, r5)
            double r5 = java.lang.Math.min(r14, r5)
            double r5 = java.lang.Math.min(r7, r5)
            double r9 = r9 - r1
            double r1 = java.lang.Math.floor(r9)
            int r1 = (int) r1
            double r3 = r3 - r5
            double r2 = java.lang.Math.floor(r3)
            int r6 = (int) r2
            r4 = r1
            goto L_0x014b
        L_0x0144:
            r22 = r1
            r16 = r2
            r17 = r3
            r0 = r9
        L_0x014b:
            if (r28 == 0) goto L_0x0171
            int r1 = m15682a(r28)
            int r2 = m15689b(r28)
            if (r1 == 0) goto L_0x0168
            float r3 = (float) r1
            r0.preRotate(r3)
            r3 = 90
            if (r1 == r3) goto L_0x0163
            r3 = 270(0x10e, float:3.78E-43)
            if (r1 != r3) goto L_0x0168
        L_0x0163:
            r25 = r6
            r6 = r4
            r4 = r25
        L_0x0168:
            r1 = 1
            if (r2 == r1) goto L_0x0171
            float r1 = (float) r2
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.postScale(r1, r2)
        L_0x0171:
            r1 = r26
            boolean r2 = r1.f10073j
            if (r2 == 0) goto L_0x0208
            if (r4 == 0) goto L_0x0181
            float r2 = (float) r4
            r3 = r22
            float r5 = (float) r3
            float r2 = r2 / r5
            r5 = r16
            goto L_0x0188
        L_0x0181:
            r3 = r22
            float r2 = (float) r6
            r5 = r16
            float r7 = (float) r5
            float r2 = r2 / r7
        L_0x0188:
            if (r6 == 0) goto L_0x018d
            float r7 = (float) r6
            float r8 = (float) r5
            goto L_0x018f
        L_0x018d:
            float r7 = (float) r4
            float r8 = (float) r3
        L_0x018f:
            float r7 = r7 / r8
            int r8 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r8 <= 0) goto L_0x01bf
            float r8 = (float) r5
            float r7 = r7 / r2
            float r8 = r8 * r7
            double r7 = (double) r8
            double r7 = java.lang.Math.ceil(r7)
            int r7 = (int) r7
            int r1 = r1.f10074k
            r8 = r1 & 48
            r9 = 48
            if (r8 != r9) goto L_0x01a8
            r1 = 0
            goto L_0x01b4
        L_0x01a8:
            r8 = 80
            r1 = r1 & r8
            if (r1 != r8) goto L_0x01b0
            int r1 = r5 - r7
            goto L_0x01b4
        L_0x01b0:
            int r1 = r5 - r7
            int r1 = r1 / 2
        L_0x01b4:
            float r8 = (float) r6
            float r9 = (float) r7
            float r8 = r8 / r9
            r9 = r3
            r10 = r7
            r21 = 0
            r7 = r2
            r2 = r17
            goto L_0x01f9
        L_0x01bf:
            int r8 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r8 >= 0) goto L_0x01f1
            float r8 = (float) r3
            float r2 = r2 / r7
            float r8 = r8 * r2
            double r8 = (double) r8
            double r8 = java.lang.Math.ceil(r8)
            int r2 = (int) r8
            int r1 = r1.f10074k
            r8 = r1 & 3
            r9 = 3
            if (r8 != r9) goto L_0x01d6
            r1 = 0
            goto L_0x01e1
        L_0x01d6:
            r8 = 5
            r1 = r1 & r8
            if (r1 != r8) goto L_0x01dd
            int r1 = r3 - r2
            goto L_0x01e1
        L_0x01dd:
            int r1 = r3 - r2
            int r1 = r1 / 2
        L_0x01e1:
            float r8 = (float) r4
            float r9 = (float) r2
            float r8 = r8 / r9
            r21 = r1
            r9 = r2
            r10 = r5
            r2 = r17
            r1 = 0
            r25 = r8
            r8 = r7
            r7 = r25
            goto L_0x01f9
        L_0x01f1:
            r9 = r3
            r10 = r5
            r8 = r7
            r2 = r17
            r1 = 0
            r21 = 0
        L_0x01f9:
            boolean r2 = m15688a(r2, r3, r5, r4, r6)
            if (r2 == 0) goto L_0x0202
            r0.preScale(r7, r8)
        L_0x0202:
            r6 = r1
            r7 = r9
            r8 = r10
            r5 = r21
            goto L_0x0257
        L_0x0208:
            r5 = r16
            r2 = r17
            r3 = r22
            boolean r1 = r1.f10075l
            if (r1 == 0) goto L_0x0232
            if (r4 == 0) goto L_0x0217
            float r1 = (float) r4
            float r7 = (float) r3
            goto L_0x0219
        L_0x0217:
            float r1 = (float) r6
            float r7 = (float) r5
        L_0x0219:
            float r1 = r1 / r7
            if (r6 == 0) goto L_0x021f
            float r7 = (float) r6
            float r8 = (float) r5
            goto L_0x0221
        L_0x021f:
            float r7 = (float) r4
            float r8 = (float) r3
        L_0x0221:
            float r7 = r7 / r8
            int r8 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r8 >= 0) goto L_0x0227
            goto L_0x0228
        L_0x0227:
            r1 = r7
        L_0x0228:
            boolean r2 = m15688a(r2, r3, r5, r4, r6)
            if (r2 == 0) goto L_0x0253
            r0.preScale(r1, r1)
            goto L_0x0253
        L_0x0232:
            if (r4 != 0) goto L_0x0236
            if (r6 == 0) goto L_0x0253
        L_0x0236:
            if (r4 != r3) goto L_0x023a
            if (r6 == r5) goto L_0x0253
        L_0x023a:
            if (r4 == 0) goto L_0x023f
            float r1 = (float) r4
            float r7 = (float) r3
            goto L_0x0241
        L_0x023f:
            float r1 = (float) r6
            float r7 = (float) r5
        L_0x0241:
            float r1 = r1 / r7
            if (r6 == 0) goto L_0x0247
            float r7 = (float) r6
            float r8 = (float) r5
            goto L_0x0249
        L_0x0247:
            float r7 = (float) r4
            float r8 = (float) r3
        L_0x0249:
            float r7 = r7 / r8
            boolean r2 = m15688a(r2, r3, r5, r4, r6)
            if (r2 == 0) goto L_0x0253
            r0.preScale(r1, r7)
        L_0x0253:
            r7 = r3
            r8 = r5
            r5 = 0
            r6 = 0
        L_0x0257:
            r10 = 1
            r4 = r27
            r9 = r0
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r4, r5, r6, r7, r8, r9, r10)
            r1 = r27
            if (r0 == r1) goto L_0x0267
            r27.recycle()
            goto L_0x0268
        L_0x0267:
            r0 = r1
        L_0x0268:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.picasso.BitmapHunter.m15683a(com.squareup.picasso.w, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }
}
