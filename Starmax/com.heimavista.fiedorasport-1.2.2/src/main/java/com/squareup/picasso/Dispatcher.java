package com.squareup.picasso;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.facebook.internal.ServerProtocol;
import com.squareup.picasso.NetworkRequestHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

/* renamed from: com.squareup.picasso.i */
class Dispatcher {

    /* renamed from: a */
    final C4732b f9962a = new C4732b();

    /* renamed from: b */
    final Context f9963b;

    /* renamed from: c */
    final ExecutorService f9964c;

    /* renamed from: d */
    final Downloader f9965d;

    /* renamed from: e */
    final Map<String, BitmapHunter> f9966e;

    /* renamed from: f */
    final Map<Object, Action> f9967f;

    /* renamed from: g */
    final Map<Object, Action> f9968g;

    /* renamed from: h */
    final Set<Object> f9969h;

    /* renamed from: i */
    final Handler f9970i;

    /* renamed from: j */
    final Handler f9971j;

    /* renamed from: k */
    final C4724d f9972k;

    /* renamed from: l */
    final Stats f9973l;

    /* renamed from: m */
    final List<BitmapHunter> f9974m;

    /* renamed from: n */
    final C4733c f9975n;

    /* renamed from: o */
    final boolean f9976o;

    /* renamed from: p */
    boolean f9977p;

    /* renamed from: com.squareup.picasso.i$a */
    /* compiled from: Dispatcher */
    private static class C4730a extends Handler {

        /* renamed from: a */
        private final Dispatcher f9978a;

        /* renamed from: com.squareup.picasso.i$a$a */
        /* compiled from: Dispatcher */
        class C4731a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Message f9979P;

            C4731a(C4730a aVar, Message message) {
                this.f9979P = message;
            }

            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.f9979P.what);
            }
        }

        C4730a(Looper looper, Dispatcher iVar) {
            super(looper);
            this.f9978a = iVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void
         arg types: [com.squareup.picasso.c, int]
         candidates:
          com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void
          com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void */
        public void handleMessage(Message message) {
            boolean z = false;
            switch (message.what) {
                case 1:
                    this.f9978a.mo26282d((Action) message.obj);
                    return;
                case 2:
                    this.f9978a.mo26280c((Action) message.obj);
                    return;
                case 3:
                case 8:
                default:
                    Picasso.f10014p.post(new C4731a(this, message));
                    return;
                case 4:
                    this.f9978a.mo26283d((BitmapHunter) message.obj);
                    return;
                case 5:
                    this.f9978a.mo26284e((BitmapHunter) message.obj);
                    return;
                case 6:
                    this.f9978a.mo26272a((BitmapHunter) message.obj, false);
                    return;
                case 7:
                    this.f9978a.mo26267a();
                    return;
                case 9:
                    this.f9978a.mo26275b((NetworkInfo) message.obj);
                    return;
                case 10:
                    Dispatcher iVar = this.f9978a;
                    if (message.arg1 == 1) {
                        z = true;
                    }
                    iVar.mo26279b(z);
                    return;
                case 11:
                    this.f9978a.mo26273a(message.obj);
                    return;
                case 12:
                    this.f9978a.mo26278b(message.obj);
                    return;
            }
        }
    }

    /* renamed from: com.squareup.picasso.i$b */
    /* compiled from: Dispatcher */
    static class C4732b extends HandlerThread {
        C4732b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    /* renamed from: com.squareup.picasso.i$c */
    /* compiled from: Dispatcher */
    static class C4733c extends BroadcastReceiver {

        /* renamed from: a */
        private final Dispatcher f9980a;

        C4733c(Dispatcher iVar) {
            this.f9980a = iVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo26287a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.f9980a.f9976o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.f9980a.f9963b.registerReceiver(super, intentFilter);
        }

        @SuppressLint({"MissingPermission"})
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra(ServerProtocol.DIALOG_PARAM_STATE)) {
                        this.f9980a.mo26274a(intent.getBooleanExtra(ServerProtocol.DIALOG_PARAM_STATE, false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.f9980a.mo26268a(((ConnectivityManager) C4726d0.m15721a(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    Dispatcher(Context context, ExecutorService executorService, Handler handler, Downloader jVar, C4724d dVar, Stats a0Var) {
        this.f9962a.start();
        C4726d0.m15728a(this.f9962a.getLooper());
        this.f9963b = context;
        this.f9964c = executorService;
        this.f9966e = new LinkedHashMap();
        this.f9967f = new WeakHashMap();
        this.f9968g = new WeakHashMap();
        this.f9969h = new LinkedHashSet();
        this.f9970i = new C4730a(this.f9962a.getLooper(), this);
        this.f9965d = jVar;
        this.f9971j = handler;
        this.f9972k = dVar;
        this.f9973l = a0Var;
        this.f9974m = new ArrayList(4);
        this.f9977p = C4726d0.m15735c(this.f9963b);
        this.f9976o = C4726d0.m15734b(context, "android.permission.ACCESS_NETWORK_STATE");
        this.f9975n = new C4733c(this);
        this.f9975n.mo26287a();
    }

    /* renamed from: f */
    private void m15746f(BitmapHunter cVar) {
        if (!cVar.mo26243m()) {
            Bitmap bitmap = cVar.f9941b0;
            if (bitmap != null) {
                bitmap.prepareToDraw();
            }
            this.f9974m.add(cVar);
            if (!this.f9970i.hasMessages(7)) {
                this.f9970i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    /* renamed from: g */
    private void m15747g(BitmapHunter cVar) {
        Action b = cVar.mo26231b();
        if (b != null) {
            m15745e(b);
        }
        List<Action> c = cVar.mo26233c();
        if (c != null) {
            int size = c.size();
            for (int i = 0; i < size; i++) {
                m15745e(c.get(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26269a(Action aVar) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(2, aVar));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26276b(Action aVar) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(1, aVar));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo26281c(BitmapHunter cVar) {
        Handler handler = this.f9970i;
        handler.sendMessageDelayed(handler.obtainMessage(5, cVar), 500);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void
     arg types: [com.squareup.picasso.a, int]
     candidates:
      com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void
      com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo26282d(Action aVar) {
        mo26270a(aVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void
     arg types: [com.squareup.picasso.c, int]
     candidates:
      com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void
      com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void */
    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    /* renamed from: e */
    public void mo26284e(BitmapHunter cVar) {
        if (!cVar.mo26243m()) {
            boolean z = false;
            if (this.f9964c.isShutdown()) {
                mo26272a(cVar, false);
                return;
            }
            NetworkInfo networkInfo = null;
            if (this.f9976o) {
                networkInfo = ((ConnectivityManager) C4726d0.m15721a(this.f9963b, "connectivity")).getActiveNetworkInfo();
            }
            if (cVar.mo26230a(this.f9977p, networkInfo)) {
                if (cVar.mo26239i().f10029n) {
                    C4726d0.m15729a("Dispatcher", "retrying", C4726d0.m15723a(cVar));
                }
                if (cVar.mo26235e() instanceof NetworkRequestHandler.C4738a) {
                    cVar.f9937X |= NetworkPolicy.NO_CACHE.f10008P;
                }
                cVar.f9942c0 = this.f9964c.submit(cVar);
                return;
            }
            if (this.f9976o && cVar.mo26244n()) {
                z = true;
            }
            mo26272a(cVar, z);
            if (z) {
                m15747g(cVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26271a(BitmapHunter cVar) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(4, cVar));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26277b(BitmapHunter cVar) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(6, cVar));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo26280c(Action aVar) {
        String b = aVar.mo26201b();
        BitmapHunter cVar = this.f9966e.get(b);
        if (cVar != null) {
            cVar.mo26232b(aVar);
            if (cVar.mo26229a()) {
                this.f9966e.remove(b);
                if (aVar.mo26204e().f10029n) {
                    C4726d0.m15729a("Dispatcher", "canceled", aVar.mo26206g().mo26335d());
                }
            }
        }
        if (this.f9969h.contains(aVar.mo26207h())) {
            this.f9968g.remove(aVar.mo26208i());
            if (aVar.mo26204e().f10029n) {
                C4726d0.m15730a("Dispatcher", "canceled", aVar.mo26206g().mo26335d(), "because paused request got canceled");
            }
        }
        Action remove = this.f9967f.remove(aVar.mo26208i());
        if (remove != null && remove.mo26204e().f10029n) {
            C4726d0.m15730a("Dispatcher", "canceled", remove.mo26206g().mo26335d(), "from replaying");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo26283d(BitmapHunter cVar) {
        if (MemoryPolicy.m15787b(cVar.mo26238h())) {
            this.f9972k.mo26254a(cVar.mo26236f(), cVar.mo26241k());
        }
        this.f9966e.remove(cVar.mo26236f());
        m15746f(cVar);
        if (cVar.mo26239i().f10029n) {
            C4726d0.m15730a("Dispatcher", "batched", C4726d0.m15723a(cVar), "for completion");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26268a(NetworkInfo networkInfo) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26278b(Object obj) {
        if (this.f9969h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<Action> it = this.f9968g.values().iterator();
            while (it.hasNext()) {
                Action next = it.next();
                if (next.mo26207h().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.f9971j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26274a(boolean z) {
        Handler handler = this.f9970i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26270a(Action aVar, boolean z) {
        if (this.f9969h.contains(aVar.mo26207h())) {
            this.f9968g.put(aVar.mo26208i(), aVar);
            if (aVar.mo26204e().f10029n) {
                String d = aVar.f9879b.mo26335d();
                C4726d0.m15730a("Dispatcher", "paused", d, "because tag '" + aVar.mo26207h() + "' is paused");
                return;
            }
            return;
        }
        BitmapHunter cVar = this.f9966e.get(aVar.mo26201b());
        if (cVar != null) {
            cVar.mo26228a(aVar);
        } else if (!this.f9964c.isShutdown()) {
            BitmapHunter a = BitmapHunter.m15686a(aVar.mo26204e(), this, this.f9972k, this.f9973l, aVar);
            a.f9942c0 = this.f9964c.submit(a);
            this.f9966e.put(aVar.mo26201b(), a);
            if (z) {
                this.f9967f.remove(aVar.mo26208i());
            }
            if (aVar.mo26204e().f10029n) {
                C4726d0.m15729a("Dispatcher", "enqueued", aVar.f9879b.mo26335d());
            }
        } else if (aVar.mo26204e().f10029n) {
            C4726d0.m15730a("Dispatcher", "ignored", aVar.f9879b.mo26335d(), "because shut down");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26279b(boolean z) {
        this.f9977p = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo26275b(NetworkInfo networkInfo) {
        ExecutorService executorService = this.f9964c;
        if (executorService instanceof PicassoExecutorService) {
            ((PicassoExecutorService) executorService).mo26328a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            m15744b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void
     arg types: [com.squareup.picasso.a, int]
     candidates:
      com.squareup.picasso.i.a(com.squareup.picasso.c, boolean):void
      com.squareup.picasso.i.a(com.squareup.picasso.a, boolean):void */
    /* renamed from: b */
    private void m15744b() {
        if (!this.f9967f.isEmpty()) {
            Iterator<Action> it = this.f9967f.values().iterator();
            while (it.hasNext()) {
                Action next = it.next();
                it.remove();
                if (next.mo26204e().f10029n) {
                    C4726d0.m15729a("Dispatcher", "replaying", next.mo26206g().mo26335d());
                }
                mo26270a(next, false);
            }
        }
    }

    /* renamed from: e */
    private void m15745e(Action aVar) {
        Object i = aVar.mo26208i();
        if (i != null) {
            aVar.f9888k = true;
            this.f9967f.put(i, aVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26273a(Object obj) {
        if (this.f9969h.add(obj)) {
            Iterator<BitmapHunter> it = this.f9966e.values().iterator();
            while (it.hasNext()) {
                BitmapHunter next = it.next();
                boolean z = next.mo26239i().f10029n;
                Action b = next.mo26231b();
                List<Action> c = next.mo26233c();
                boolean z2 = c != null && !c.isEmpty();
                if (b != null || z2) {
                    if (b != null && b.mo26207h().equals(obj)) {
                        next.mo26232b(b);
                        this.f9968g.put(b.mo26208i(), b);
                        if (z) {
                            C4726d0.m15730a("Dispatcher", "paused", b.f9879b.mo26335d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = c.size() - 1; size >= 0; size--) {
                            Action aVar = c.get(size);
                            if (aVar.mo26207h().equals(obj)) {
                                next.mo26232b(aVar);
                                this.f9968g.put(aVar.mo26208i(), aVar);
                                if (z) {
                                    C4726d0.m15730a("Dispatcher", "paused", aVar.f9879b.mo26335d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.mo26229a()) {
                        it.remove();
                        if (z) {
                            C4726d0.m15730a("Dispatcher", "canceled", C4726d0.m15723a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26267a() {
        ArrayList arrayList = new ArrayList(this.f9974m);
        this.f9974m.clear();
        Handler handler = this.f9971j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        m15743a((List<BitmapHunter>) arrayList);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26272a(BitmapHunter cVar, boolean z) {
        if (cVar.mo26239i().f10029n) {
            String a = C4726d0.m15723a(cVar);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            C4726d0.m15730a("Dispatcher", "batched", a, sb.toString());
        }
        this.f9966e.remove(cVar.mo26236f());
        m15746f(cVar);
    }

    /* renamed from: a */
    private void m15743a(List<BitmapHunter> list) {
        if (list != null && !list.isEmpty() && list.get(0).mo26239i().f10029n) {
            StringBuilder sb = new StringBuilder();
            for (BitmapHunter cVar : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(C4726d0.m15723a(cVar));
            }
            C4726d0.m15729a("Dispatcher", "delivered", sb.toString());
        }
    }
}
