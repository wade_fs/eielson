package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: com.squareup.picasso.a */
abstract class Action<T> {

    /* renamed from: a */
    final Picasso f9878a;

    /* renamed from: b */
    final C4750w f9879b;

    /* renamed from: c */
    final WeakReference<T> f9880c;

    /* renamed from: d */
    final boolean f9881d;

    /* renamed from: e */
    final int f9882e;

    /* renamed from: f */
    final int f9883f;

    /* renamed from: g */
    final int f9884g;

    /* renamed from: h */
    final Drawable f9885h;

    /* renamed from: i */
    final String f9886i;

    /* renamed from: j */
    final Object f9887j;

    /* renamed from: k */
    boolean f9888k;

    /* renamed from: l */
    boolean f9889l;

    /* renamed from: com.squareup.picasso.a$a */
    /* compiled from: Action */
    static class C4714a<M> extends WeakReference<M> {

        /* renamed from: a */
        final Action f9890a;

        C4714a(Action aVar, M m, ReferenceQueue<? super M> referenceQueue) {
            super(m, referenceQueue);
            this.f9890a = aVar;
        }
    }

    Action(Picasso tVar, T t, C4750w wVar, int i, int i2, int i3, Drawable drawable, String str, Object obj, boolean z) {
        C4714a aVar;
        this.f9878a = tVar;
        this.f9879b = wVar;
        if (t == null) {
            aVar = null;
        } else {
            aVar = new C4714a(this, t, tVar.f10026k);
        }
        this.f9880c = aVar;
        this.f9882e = i;
        this.f9883f = i2;
        this.f9881d = z;
        this.f9884g = i3;
        this.f9885h = drawable;
        this.f9886i = str;
        this.f9887j = obj == null ? this : obj;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26198a() {
        this.f9889l = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo26199a(Bitmap bitmap, Picasso.C4745e eVar);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo26200a(Exception exc);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public String mo26201b() {
        return this.f9886i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo26202c() {
        return this.f9882e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo26203d() {
        return this.f9883f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public Picasso mo26204e() {
        return this.f9878a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public Picasso.C4746f mo26205f() {
        return this.f9879b.f10083t;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public C4750w mo26206g() {
        return this.f9879b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public Object mo26207h() {
        return this.f9887j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public T mo26208i() {
        WeakReference<T> weakReference = this.f9880c;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public boolean mo26209j() {
        return this.f9889l;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public boolean mo26210k() {
        return this.f9888k;
    }
}
