package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.squareup.picasso.Picasso;
import p244j.C5049s;

/* renamed from: com.squareup.picasso.y */
public abstract class RequestHandler {

    /* renamed from: com.squareup.picasso.y$a */
    /* compiled from: RequestHandler */
    public static final class C4753a {

        /* renamed from: a */
        private final Picasso.C4745e f10114a;

        /* renamed from: b */
        private final Bitmap f10115b;

        /* renamed from: c */
        private final C5049s f10116c;

        /* renamed from: d */
        private final int f10117d;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public C4753a(@NonNull Bitmap bitmap, @NonNull Picasso.C4745e eVar) {
            this(bitmap, null, eVar, 0);
            C4726d0.m15722a(bitmap, "bitmap == null");
        }

        @Nullable
        /* renamed from: a */
        public Bitmap mo26350a() {
            return this.f10115b;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public int mo26351b() {
            return this.f10117d;
        }

        @NonNull
        /* renamed from: c */
        public Picasso.C4745e mo26352c() {
            return this.f10114a;
        }

        @Nullable
        /* renamed from: d */
        public C5049s mo26353d() {
            return this.f10116c;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public C4753a(@NonNull C5049s sVar, @NonNull Picasso.C4745e eVar) {
            this(null, sVar, eVar, 0);
            C4726d0.m15722a(sVar, "source == null");
        }

        C4753a(@Nullable Bitmap bitmap, @Nullable C5049s sVar, @NonNull Picasso.C4745e eVar, int i) {
            if ((bitmap != null) != (sVar == null ? false : true)) {
                this.f10115b = bitmap;
                this.f10116c = sVar;
                C4726d0.m15722a(eVar, "loadedFrom == null");
                this.f10114a = eVar;
                this.f10117d = i;
                return;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    static boolean m15843a(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    /* renamed from: b */
    static BitmapFactory.Options m15844b(C4750w wVar) {
        boolean c = wVar.mo26334c();
        boolean z = wVar.f10082s != null;
        BitmapFactory.Options options = null;
        if (c || z || wVar.f10081r) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = c;
            boolean z2 = wVar.f10081r;
            options.inInputShareable = z2;
            options.inPurgeable = z2;
            if (z) {
                options.inPreferredConfig = wVar.f10082s;
            }
        }
        return options;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo26304a() {
        return 0;
    }

    @Nullable
    /* renamed from: a */
    public abstract C4753a mo26224a(C4750w wVar, int i);

    /* renamed from: a */
    public abstract boolean mo26225a(C4750w wVar);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo26305a(boolean z, NetworkInfo networkInfo) {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo26306b() {
        return false;
    }

    /* renamed from: a */
    static void m15842a(int i, int i2, BitmapFactory.Options options, C4750w wVar) {
        m15841a(i, i2, options.outWidth, options.outHeight, options, wVar);
    }

    /* renamed from: a */
    static void m15841a(int i, int i2, int i3, int i4, BitmapFactory.Options options, C4750w wVar) {
        int i5;
        double floor;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                floor = Math.floor((double) (((float) i3) / ((float) i)));
            } else if (i == 0) {
                floor = Math.floor((double) (((float) i4) / ((float) i2)));
            } else {
                int floor2 = (int) Math.floor((double) (((float) i4) / ((float) i2)));
                int floor3 = (int) Math.floor((double) (((float) i3) / ((float) i)));
                i5 = wVar.f10075l ? Math.max(floor2, floor3) : Math.min(floor2, floor3);
            }
            i5 = (int) floor;
        } else {
            i5 = 1;
        }
        options.inSampleSize = i5;
        options.inJustDecodeBounds = false;
    }
}
