package com.squareup.picasso;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import p244j.BufferedSource;
import p244j.ByteString;

/* renamed from: com.squareup.picasso.d0 */
/* compiled from: Utils */
final class C4726d0 {

    /* renamed from: a */
    static final StringBuilder f9953a = new StringBuilder();

    /* renamed from: b */
    private static final ByteString f9954b = ByteString.m18170d("RIFF");

    /* renamed from: c */
    private static final ByteString f9955c = ByteString.m18170d("WEBP");

    /* renamed from: com.squareup.picasso.d0$a */
    /* compiled from: Utils */
    static class C4727a extends Handler {
        C4727a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            sendMessageDelayed(obtainMessage(), 1000);
        }
    }

    /* renamed from: com.squareup.picasso.d0$b */
    /* compiled from: Utils */
    private static class C4728b extends Thread {
        C4728b(Runnable runnable) {
            super(runnable);
        }

        public void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    /* renamed from: com.squareup.picasso.d0$c */
    /* compiled from: Utils */
    static class C4729c implements ThreadFactory {
        C4729c() {
        }

        public Thread newThread(Runnable runnable) {
            return new C4728b(runnable);
        }
    }

    /* renamed from: a */
    static int m15718a(Bitmap bitmap) {
        int allocationByteCount = Build.VERSION.SDK_INT >= 19 ? bitmap.getAllocationByteCount() : bitmap.getByteCount();
        if (allocationByteCount >= 0) {
            return allocationByteCount;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    /* renamed from: b */
    static boolean m15733b() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /* renamed from: c */
    static boolean m15735c(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        try {
            if (Build.VERSION.SDK_INT < 17) {
                if (Settings.System.getInt(contentResolver, "airplane_mode_on", 0) != 0) {
                    return true;
                }
                return false;
            } else if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0) {
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException | SecurityException unused) {
            return false;
        }
    }

    /* renamed from: b */
    static File m15732b(Context context) {
        File file = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    /* renamed from: a */
    static <T> T m15722a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: a */
    static void m15727a() {
        if (!m15733b()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    /* renamed from: b */
    static boolean m15734b(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    /* renamed from: a */
    static String m15723a(BitmapHunter cVar) {
        return m15724a(cVar, "");
    }

    /* renamed from: a */
    static String m15724a(BitmapHunter cVar, String str) {
        StringBuilder sb = new StringBuilder(str);
        Action b = cVar.mo26231b();
        if (b != null) {
            sb.append(b.f9879b.mo26335d());
        }
        List<Action> c = cVar.mo26233c();
        if (c != null) {
            int size = c.size();
            for (int i = 0; i < size; i++) {
                if (i > 0 || b != null) {
                    sb.append(", ");
                }
                sb.append(c.get(i).f9879b.mo26335d());
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    static void m15729a(String str, String str2, String str3) {
        m15730a(str, str2, str3, "");
    }

    /* renamed from: a */
    static void m15730a(String str, String str2, String str3, String str4) {
        Log.d("Picasso", String.format("%1$-11s %2$-12s %3$s %4$s", str, str2, str3, str4));
    }

    /* renamed from: a */
    static String m15725a(C4750w wVar) {
        String a = m15726a(wVar, f9953a);
        f9953a.setLength(0);
        return a;
    }

    /* renamed from: a */
    static String m15726a(C4750w wVar, StringBuilder sb) {
        String str = wVar.f10069f;
        if (str != null) {
            sb.ensureCapacity(str.length() + 50);
            sb.append(wVar.f10069f);
        } else {
            Uri uri = wVar.f10067d;
            if (uri != null) {
                String uri2 = uri.toString();
                sb.ensureCapacity(uri2.length() + 50);
                sb.append(uri2);
            } else {
                sb.ensureCapacity(50);
                sb.append(wVar.f10068e);
            }
        }
        sb.append(10);
        if (wVar.f10077n != 0.0f) {
            sb.append("rotation:");
            sb.append(wVar.f10077n);
            if (wVar.f10080q) {
                sb.append('@');
                sb.append(wVar.f10078o);
                sb.append('x');
                sb.append(wVar.f10079p);
            }
            sb.append(10);
        }
        if (wVar.mo26334c()) {
            sb.append("resize:");
            sb.append(wVar.f10071h);
            sb.append('x');
            sb.append(wVar.f10072i);
            sb.append(10);
        }
        if (wVar.f10073j) {
            sb.append("centerCrop:");
            sb.append(wVar.f10074k);
            sb.append(10);
        } else if (wVar.f10075l) {
            sb.append("centerInside");
            sb.append(10);
        }
        List<C4723c0> list = wVar.f10070g;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                sb.append(wVar.f10070g.get(i).mo26252a());
                sb.append(10);
            }
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    @TargetApi(18)
    /* renamed from: a */
    static long m15719a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j = ((Build.VERSION.SDK_INT < 18 ? (long) statFs.getBlockCount() : statFs.getBlockCountLong()) * (Build.VERSION.SDK_INT < 18 ? (long) statFs.getBlockSize() : statFs.getBlockSizeLong())) / 50;
        } catch (IllegalArgumentException unused) {
            j = 5242880;
        }
        return Math.max(Math.min(j, 52428800L), 5242880L);
    }

    /* renamed from: a */
    static int m15716a(Context context) {
        ActivityManager activityManager = (ActivityManager) m15721a(context, "activity");
        return (int) ((((long) ((context.getApplicationInfo().flags & 1048576) != 0 ? activityManager.getLargeMemoryClass() : activityManager.getMemoryClass())) * 1048576) / 7);
    }

    /* renamed from: a */
    static <T> T m15721a(Context context, String str) {
        return context.getSystemService(str);
    }

    /* renamed from: a */
    static boolean m15731a(BufferedSource eVar) {
        return eVar.mo28040a(0, f9954b) && eVar.mo28040a(8, f9955c);
    }

    /* renamed from: a */
    static int m15717a(Resources resources, C4750w wVar) {
        Uri uri;
        if (wVar.f10068e != 0 || (uri = wVar.f10067d) == null) {
            return wVar.f10068e;
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            List<String> pathSegments = wVar.f10067d.getPathSegments();
            if (pathSegments == null || pathSegments.isEmpty()) {
                throw new FileNotFoundException("No path segments: " + wVar.f10067d);
            } else if (pathSegments.size() == 1) {
                try {
                    return Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException unused) {
                    throw new FileNotFoundException("Last path segment is not a resource ID: " + wVar.f10067d);
                }
            } else if (pathSegments.size() == 2) {
                return resources.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + wVar.f10067d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + wVar.f10067d);
        }
    }

    /* renamed from: a */
    static Resources m15720a(Context context, C4750w wVar) {
        Uri uri;
        if (wVar.f10068e != 0 || (uri = wVar.f10067d) == null) {
            return context.getResources();
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            try {
                return context.getPackageManager().getResourcesForApplication(authority);
            } catch (PackageManager.NameNotFoundException unused) {
                throw new FileNotFoundException("Unable to obtain resources for package: " + wVar.f10067d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + wVar.f10067d);
        }
    }

    /* renamed from: a */
    static void m15728a(Looper looper) {
        C4727a aVar = new C4727a(looper);
        aVar.sendMessageDelayed(aVar.obtainMessage(), 1000);
    }
}
