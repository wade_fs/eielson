package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

/* renamed from: com.squareup.picasso.l */
class ImageViewAction extends Action<ImageView> {

    /* renamed from: m */
    Callback f9981m;

    ImageViewAction(Picasso tVar, ImageView imageView, C4750w wVar, int i, int i2, int i3, Drawable drawable, String str, Object obj, Callback eVar, boolean z) {
        super(tVar, imageView, wVar, i, i2, i3, drawable, str, obj, z);
        this.f9981m = eVar;
    }

    /* renamed from: a */
    public void mo26199a(Bitmap bitmap, Picasso.C4745e eVar) {
        if (bitmap != null) {
            ImageView imageView = (ImageView) super.f9880c.get();
            if (imageView != null) {
                Picasso tVar = super.f9878a;
                Bitmap bitmap2 = bitmap;
                Picasso.C4745e eVar2 = eVar;
                PicassoDrawable.m15817a(imageView, tVar.f10020e, bitmap2, eVar2, super.f9881d, tVar.f10028m);
                Callback eVar3 = this.f9981m;
                if (eVar3 != null) {
                    eVar3.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    /* renamed from: a */
    public void mo26200a(Exception exc) {
        ImageView imageView = (ImageView) super.f9880c.get();
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof Animatable) {
                ((Animatable) drawable).stop();
            }
            int i = super.f9884g;
            if (i != 0) {
                imageView.setImageResource(i);
            } else {
                Drawable drawable2 = super.f9885h;
                if (drawable2 != null) {
                    imageView.setImageDrawable(drawable2);
                }
            }
            Callback eVar = this.f9981m;
            if (eVar != null) {
                eVar.onError(exc);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo26198a() {
        super.mo26198a();
        if (this.f9981m != null) {
            this.f9981m = null;
        }
    }
}
