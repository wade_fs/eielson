package com.squareup.picasso;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.io.File;
import p231i.C5023y;
import p231i.Call;
import p231i.OkHttpClient;
import p231i.Response;

/* renamed from: com.squareup.picasso.s */
public final class OkHttp3Downloader implements Downloader {
    @VisibleForTesting

    /* renamed from: a */
    final Call.C4949a f10013a;

    public OkHttp3Downloader(Context context) {
        this(C4726d0.m15732b(context));
    }

    @NonNull
    /* renamed from: a */
    public Response mo26289a(@NonNull C5023y yVar) {
        return this.f10013a.mo27396a(yVar).mo27395s();
    }

    public OkHttp3Downloader(File file) {
        this(file, C4726d0.m15719a(file));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OkHttp3Downloader(java.io.File r3, long r4) {
        /*
            r2 = this;
            i.v$b r0 = new i.v$b
            r0.<init>()
            i.c r1 = new i.c
            r1.<init>(r3, r4)
            r0.mo27809a(r1)
            i.v r3 = r0.mo27810a()
            r2.<init>(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.picasso.OkHttp3Downloader.<init>(java.io.File, long):void");
    }

    public OkHttp3Downloader(OkHttpClient vVar) {
        this.f10013a = vVar;
        vVar.mo27786b();
    }
}
