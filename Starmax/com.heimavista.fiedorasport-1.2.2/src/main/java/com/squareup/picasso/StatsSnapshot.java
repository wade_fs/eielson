package com.squareup.picasso;

import java.io.PrintWriter;

/* renamed from: com.squareup.picasso.b0 */
public class StatsSnapshot {

    /* renamed from: a */
    public final int f9911a;

    /* renamed from: b */
    public final int f9912b;

    /* renamed from: c */
    public final long f9913c;

    /* renamed from: d */
    public final long f9914d;

    /* renamed from: e */
    public final long f9915e;

    /* renamed from: f */
    public final long f9916f;

    /* renamed from: g */
    public final long f9917g;

    /* renamed from: h */
    public final long f9918h;

    /* renamed from: i */
    public final long f9919i;

    /* renamed from: j */
    public final long f9920j;

    /* renamed from: k */
    public final int f9921k;

    /* renamed from: l */
    public final int f9922l;

    /* renamed from: m */
    public final int f9923m;

    /* renamed from: n */
    public final long f9924n;

    public StatsSnapshot(int i, int i2, long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, int i3, int i4, int i5, long j9) {
        this.f9911a = i;
        this.f9912b = i2;
        this.f9913c = j;
        this.f9914d = j2;
        this.f9915e = j3;
        this.f9916f = j4;
        this.f9917g = j5;
        this.f9918h = j6;
        this.f9919i = j7;
        this.f9920j = j8;
        this.f9921k = i3;
        this.f9922l = i4;
        this.f9923m = i5;
        this.f9924n = j9;
    }

    /* renamed from: a */
    public void mo26226a(PrintWriter printWriter) {
        printWriter.println("===============BEGIN PICASSO STATS ===============");
        printWriter.println("Memory Cache Stats");
        printWriter.print("  Max Cache Size: ");
        printWriter.println(this.f9911a);
        printWriter.print("  Cache Size: ");
        printWriter.println(this.f9912b);
        printWriter.print("  Cache % Full: ");
        printWriter.println((int) Math.ceil((double) ((((float) this.f9912b) / ((float) this.f9911a)) * 100.0f)));
        printWriter.print("  Cache Hits: ");
        printWriter.println(this.f9913c);
        printWriter.print("  Cache Misses: ");
        printWriter.println(this.f9914d);
        printWriter.println("Network Stats");
        printWriter.print("  Download Count: ");
        printWriter.println(this.f9921k);
        printWriter.print("  Total Download Size: ");
        printWriter.println(this.f9915e);
        printWriter.print("  Average Download Size: ");
        printWriter.println(this.f9918h);
        printWriter.println("Bitmap Stats");
        printWriter.print("  Total Bitmaps Decoded: ");
        printWriter.println(this.f9922l);
        printWriter.print("  Total Bitmap Size: ");
        printWriter.println(this.f9916f);
        printWriter.print("  Total Transformed Bitmaps: ");
        printWriter.println(this.f9923m);
        printWriter.print("  Total Transformed Bitmap Size: ");
        printWriter.println(this.f9917g);
        printWriter.print("  Average Bitmap Size: ");
        printWriter.println(this.f9919i);
        printWriter.print("  Average Transformed Bitmap Size: ");
        printWriter.println(this.f9920j);
        printWriter.println("===============END PICASSO STATS ===============");
        printWriter.flush();
    }

    public String toString() {
        return "StatsSnapshot{maxSize=" + this.f9911a + ", size=" + this.f9912b + ", cacheHits=" + this.f9913c + ", cacheMisses=" + this.f9914d + ", downloadCount=" + this.f9921k + ", totalDownloadSize=" + this.f9915e + ", averageDownloadSize=" + this.f9918h + ", totalOriginalBitmapSize=" + this.f9916f + ", totalTransformedBitmapSize=" + this.f9917g + ", averageOriginalBitmapSize=" + this.f9919i + ", averageTransformedBitmapSize=" + this.f9920j + ", originalBitmapCount=" + this.f9922l + ", transformedBitmapCount=" + this.f9923m + ", timeStamp=" + this.f9924n + '}';
    }
}
