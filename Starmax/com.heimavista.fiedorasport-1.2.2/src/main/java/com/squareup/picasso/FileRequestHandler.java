package com.squareup.picasso;

import android.content.Context;
import android.net.Uri;
import androidx.exifinterface.media.ExifInterface;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import p244j.Okio;

/* renamed from: com.squareup.picasso.k */
class FileRequestHandler extends ContentStreamRequestHandler {
    FileRequestHandler(Context context) {
        super(context);
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        return "file".equals(wVar.f10067d.getScheme());
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        return new RequestHandler.C4753a(null, Okio.m18218a(mo26262c(wVar)), Picasso.C4745e.DISK, m15768a(wVar.f10067d));
    }

    /* renamed from: a */
    static int m15768a(Uri uri) {
        return new ExifInterface(uri.getPath()).getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
    }
}
