package com.squareup.picasso;

import androidx.annotation.NonNull;
import p231i.C5023y;
import p231i.Response;

/* renamed from: com.squareup.picasso.j */
public interface Downloader {
    @NonNull
    /* renamed from: a */
    Response mo26289a(@NonNull C5023y yVar);
}
