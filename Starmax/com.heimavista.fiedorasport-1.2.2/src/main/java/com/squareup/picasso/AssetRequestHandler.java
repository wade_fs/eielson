package com.squareup.picasso;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import p244j.Okio;

/* renamed from: com.squareup.picasso.b */
class AssetRequestHandler extends RequestHandler {

    /* renamed from: d */
    private static final int f9907d = 22;

    /* renamed from: a */
    private final Context f9908a;

    /* renamed from: b */
    private final Object f9909b = new Object();

    /* renamed from: c */
    private AssetManager f9910c;

    AssetRequestHandler(Context context) {
        this.f9908a = context;
    }

    /* renamed from: c */
    static String m15678c(C4750w wVar) {
        return wVar.f10067d.toString().substring(f9907d);
    }

    /* renamed from: a */
    public boolean mo26225a(C4750w wVar) {
        Uri uri = wVar.f10067d;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public RequestHandler.C4753a mo26224a(C4750w wVar, int i) {
        if (this.f9910c == null) {
            synchronized (this.f9909b) {
                if (this.f9910c == null) {
                    this.f9910c = this.f9908a.getAssets();
                }
            }
        }
        return new RequestHandler.C4753a(Okio.m18218a(this.f9910c.open(m15678c(wVar))), Picasso.C4745e.DISK);
    }
}
