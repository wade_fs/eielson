package com.heimavista.thirdpart.api.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Handler;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.AlertDialog;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.Platform;
import com.heimavista.thirdpart.api.dialog.AuthDialog;
import com.tencent.mmkv.MMKV;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TwitterAuthDialog extends AuthDialog {
    public AuthDialog.AuthListener authListener;
    public Context context;
    public Handler handler = new Handler();
    public RequestToken m_requestToken;
    public Twitter twitter;
    public String twitterAppKey;
    public String twitterAppSecret;

    public TwitterAuthDialog(Context context2, String str, String str2, AuthDialog.AuthListener authListener2) {
        super(context2, 16973840);
        this.context = context2;
        this.twitterAppKey = str;
        this.twitterAppSecret = str2;
        this.authListener = authListener2;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ void m15225a() {
        this.twitter = new TwitterFactory().getInstance();
        this.twitter.setOAuthConsumer(this.twitterAppKey, this.twitterAppSecret);
        try {
            this.m_requestToken = this.twitter.getOAuthRequestToken();
            StringBuilder sb = new StringBuilder();
            sb.append(this.m_requestToken.getAuthenticationURL());
            sb.append("&force_login=true");
            this.handler.post(new C4588d(this, sb.toString()));
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public WebViewClient getWebViewClient() {
        return new WebViewClient() {
            /* class com.heimavista.thirdpart.api.dialog.TwitterAuthDialog.C45841 */

            /* access modifiers changed from: private */
            /* renamed from: a */
            public /* synthetic */ void m15233a(String str, WebView webView) {
                try {
                    AccessToken oAuthAccessToken = TwitterAuthDialog.this.twitter.getOAuthAccessToken(TwitterAuthDialog.this.m_requestToken, str);
                    MMKV a = MMKV.m17077a(1, Platform.TWITTER.name());
                    StringBuilder sb = new StringBuilder();
                    sb.append(Platform.TWITTER.name());
                    sb.append("_token");
                    a.mo27041b(sb.toString(), oAuthAccessToken.getToken());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(Platform.TWITTER.name());
                    sb2.append("_secret");
                    a.mo27041b(sb2.toString(), oAuthAccessToken.getTokenSecret());
                    TwitterAuthDialog.this.handler.post(new C4586b(this, webView));
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }

            /* renamed from: b */
            public static /* synthetic */ void m15234b(SslErrorHandler sslErrorHandler, DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sslErrorHandler.cancel();
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                TwitterAuthDialog.this.hideSpinner();
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                String str2;
                if (str.contains("oauth_verifier=")) {
                    String[] split = str.split("\\?")[1].split("&");
                    if (split[0].startsWith("oauth_verifier")) {
                        str2 = split[0].split("=")[1];
                    } else {
                        str2 = split[1].startsWith("oauth_verifier") ? split[1].split("=")[1] : "";
                    }
                    new Thread(new C4587c(this, str2, webView)).start();
                    return;
                }
                TwitterAuthDialog.this.showSpinner();
                super.onPageStarted(webView, str, bitmap);
            }

            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                new AlertDialog.Builder(TwitterAuthDialog.this.context).setMessage(C4574R.string.web_ssl_warn).setPositiveButton(17039379, new C4591g(sslErrorHandler)).setNegativeButton(17039360, new C4590f(sslErrorHandler)).setCancelable(false).create().show();
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }

            /* access modifiers changed from: private */
            /* renamed from: a */
            public /* synthetic */ void m15230a(WebView webView) {
                webView.stopLoading();
                TwitterAuthDialog.this.dismiss();
                TwitterAuthDialog.this.authListener.onComplete(null);
            }

            /* renamed from: a */
            public static /* synthetic */ void m15229a(SslErrorHandler sslErrorHandler, DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sslErrorHandler.proceed();
            }
        };
    }

    public void loadContent() {
        new Thread(new C4589e(this)).start();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ void m15228a(String str) {
        getWebView().loadUrl(str);
    }
}
