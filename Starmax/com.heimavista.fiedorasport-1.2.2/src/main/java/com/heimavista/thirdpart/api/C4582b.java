package com.heimavista.thirdpart.api;

import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.api.dialog.AuthDialog;
import java.util.Map;

/* renamed from: com.heimavista.thirdpart.api.b */
/* compiled from: lambda */
public final /* synthetic */ class C4582b implements AuthDialog.AuthListener {

    /* renamed from: a */
    private final /* synthetic */ TwitterApi f9351a;

    /* renamed from: b */
    private final /* synthetic */ IThirdPartyLogin.Callback f9352b;

    public /* synthetic */ C4582b(TwitterApi twitterApi, IThirdPartyLogin.Callback callback) {
        this.f9351a = twitterApi;
        this.f9352b = callback;
    }

    public final void onComplete(Map map) {
        this.f9351a.m15220a(this.f9352b, map);
    }
}
