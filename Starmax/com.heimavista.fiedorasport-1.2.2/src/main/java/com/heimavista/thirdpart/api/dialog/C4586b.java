package com.heimavista.thirdpart.api.dialog;

import android.webkit.WebView;
import com.heimavista.thirdpart.api.dialog.TwitterAuthDialog;

/* renamed from: com.heimavista.thirdpart.api.dialog.b */
/* compiled from: lambda */
public final /* synthetic */ class C4586b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ TwitterAuthDialog.C45841 f9355P;

    /* renamed from: Q */
    private final /* synthetic */ WebView f9356Q;

    public /* synthetic */ C4586b(TwitterAuthDialog.C45841 r1, WebView webView) {
        this.f9355P = r1;
        this.f9356Q = webView;
    }

    public final void run() {
        this.f9355P.m15230a(this.f9356Q);
    }
}
