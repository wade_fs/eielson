package com.heimavista.thirdpart.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.tencent.p214mm.opensdk.modelbase.BaseReq;
import com.tencent.p214mm.opensdk.modelbase.BaseResp;
import com.tencent.p214mm.opensdk.modelmsg.SendAuth;
import com.tencent.p214mm.opensdk.modelmsg.ShowMessageFromWX;
import com.tencent.p214mm.opensdk.modelmsg.WXAppExtendObject;
import com.tencent.p214mm.opensdk.openapi.IWXAPI;
import com.tencent.p214mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.p214mm.opensdk.openapi.WXAPIFactory;
import java.util.Locale;
import org.json.JSONObject;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    public IWXAPI api;
    public String appKey;
    public String appSecret;

    private void getProfileInfo(String str, final IThirdPartyLogin.Callback callback) {
        String format = String.format(Locale.US, "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%1s&secret=%2s&code=%3s&grant_type=authorization_code", this.appKey, this.appSecret, str);
        LogUtil.m20121i("url:" + format);
        C5217x.http().get(new RequestParams(format), new Callback.CommonCallback<JSONObject>() {
            /* class com.heimavista.thirdpart.wxapi.WXEntryActivity.C45921 */

            public void onCancelled(Callback.CancelledException cancelledException) {
            }

            public void onError(Throwable th, boolean z) {
            }

            public void onFinished() {
            }

            public void onSuccess(JSONObject jSONObject) {
                LogUtil.m20121i("result:" + jSONObject);
                String optString = jSONObject.optString("access_token");
                String optString2 = jSONObject.optString("openid");
                String format = String.format(Locale.US, "https://api.weixin.qq.com/sns/userinfo?access_token=%1s&openid=%2s", optString, optString2);
                LogUtil.m20121i("url:" + format);
                C5217x.http().get(new RequestParams(format), new Callback.CommonCallback<JSONObject>() {
                    /* class com.heimavista.thirdpart.wxapi.WXEntryActivity.C45921.C45931 */

                    public void onCancelled(Callback.CancelledException cancelledException) {
                        IThirdPartyLogin.Callback callback = callback;
                        if (callback != null) {
                            callback.onCancel();
                        }
                    }

                    public void onError(Throwable th, boolean z) {
                        IThirdPartyLogin.Callback callback = callback;
                        if (callback != null) {
                            callback.onError(th.getMessage());
                        }
                    }

                    public void onFinished() {
                    }

                    public void onSuccess(JSONObject jSONObject) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(callback == null);
                        sb.append("result:");
                        sb.append(jSONObject);
                        LogUtil.m20121i(sb.toString());
                        String optString = jSONObject.optString("openid");
                        String optString2 = jSONObject.optString("nickname");
                        int optInt = jSONObject.optInt("sex");
                        String optString3 = jSONObject.optString("headimgurl");
                        String optString4 = jSONObject.optString("unionid");
                        MemberProfile memberProfile = new MemberProfile(Platform.WECHAT, optString, optString2);
                        memberProfile.setGender(optInt == 1 ? "M" : optInt == 2 ? "F" : "");
                        memberProfile.setPhotoUrl(optString3);
                        memberProfile.setUnionId(optString4);
                        MemberProfile.saveProfile(memberProfile);
                        IThirdPartyLogin.Callback callback = callback;
                        if (callback != null) {
                            callback.onComplete(memberProfile);
                        }
                    }
                });
            }
        });
    }

    private void handleFromWXReq(ShowMessageFromWX.Req req) {
        WXAppExtendObject wXAppExtendObject = (WXAppExtendObject) req.message.mediaObject;
        finish();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.api.handleIntent(intent, this);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.appKey = getString(C4574R.string.wechat_appKey);
        this.appSecret = getString(C4574R.string.wechat_appSecret);
        this.api = WXAPIFactory.createWXAPI(this, this.appKey, false);
        this.api.registerApp(this.appKey);
        try {
            if (!this.api.handleIntent(getIntent(), this)) {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.api.handleIntent(intent, this);
    }

    public void onReq(BaseReq baseReq) {
        if (baseReq.getType() == 4 && (baseReq instanceof ShowMessageFromWX.Req)) {
            handleFromWXReq((ShowMessageFromWX.Req) baseReq);
        }
    }

    public void onResp(BaseResp baseResp) {
        if (baseResp instanceof SendAuth.Resp) {
            SendAuth.Resp resp = (SendAuth.Resp) baseResp;
            IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(Platform.WECHAT);
            int i = baseResp.errCode;
            if (i != -4) {
                if (i != -2) {
                    if (i == 0) {
                        getProfileInfo(resp.code, loginCallback);
                    }
                } else if (loginCallback != null) {
                    loginCallback.onCancel();
                }
            } else if (loginCallback != null) {
                loginCallback.onError("ERR_AUTH_DENIED");
            }
            LoginCallbackManager.getInstance().removeCallback(resp.state);
            finish();
        }
    }
}
