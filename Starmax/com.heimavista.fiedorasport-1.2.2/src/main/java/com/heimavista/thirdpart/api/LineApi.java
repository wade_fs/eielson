package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;
import java.util.ArrayList;

public class LineApi {
    public static final int LINE_LOGIN_REQUEST_CODE = 550;
    public static final Platform platformLine = Platform.LINE;
    public Activity activity;
    public LineApiClient lineApiClient;

    /* renamed from: com.heimavista.thirdpart.api.LineApi$1 */
    static /* synthetic */ class C45771 {
        public static final /* synthetic */ int[] $SwitchMap$com$linecorp$linesdk$LineApiResponseCode = new int[LineApiResponseCode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.linecorp.linesdk.LineApiResponseCode[] r0 = com.linecorp.linesdk.LineApiResponseCode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.thirdpart.api.LineApi.C45771.$SwitchMap$com$linecorp$linesdk$LineApiResponseCode = r0
                int[] r0 = com.heimavista.thirdpart.api.LineApi.C45771.$SwitchMap$com$linecorp$linesdk$LineApiResponseCode     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.linecorp.linesdk.LineApiResponseCode r1 = com.linecorp.linesdk.LineApiResponseCode.SUCCESS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.thirdpart.api.LineApi.C45771.$SwitchMap$com$linecorp$linesdk$LineApiResponseCode     // Catch:{ NoSuchFieldError -> 0x001f }
                com.linecorp.linesdk.LineApiResponseCode r1 = com.linecorp.linesdk.LineApiResponseCode.CANCEL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.thirdpart.api.LineApi.C45771.<clinit>():void");
        }
    }

    public LineApi(@NonNull Activity activity2) {
        this.activity = activity2;
    }

    public void login() {
        Intent intent;
        IThirdPartyLogin.Callback logoutCallback = LoginCallbackManager.getInstance().getLogoutCallback(platformLine);
        try {
            String[] stringArray = this.activity.getResources().getStringArray(C4574R.array.line_scopes);
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (String str : stringArray) {
                if (str.equalsIgnoreCase("PROFILE")) {
                    arrayList.add(Scope.PROFILE);
                }
                if (str.equalsIgnoreCase("FRIEND")) {
                    arrayList.add(Scope.FRIEND);
                }
                if (str.equalsIgnoreCase("GROUP")) {
                    arrayList.add(Scope.GROUP);
                }
                if (str.equalsIgnoreCase("MESSAGE")) {
                    arrayList.add(Scope.MESSAGE);
                }
                if (str.equalsIgnoreCase("OPENID_CONNECT")) {
                    arrayList.add(Scope.OPENID_CONNECT);
                }
                if (str.equalsIgnoreCase("OC_EMAIL")) {
                    arrayList.add(Scope.OC_EMAIL);
                }
                if (str.equalsIgnoreCase("OC_PHONE_NUMBER")) {
                    arrayList.add(Scope.OC_PHONE_NUMBER);
                }
                if (str.equalsIgnoreCase("OC_GENDER")) {
                    arrayList.add(Scope.OC_GENDER);
                }
                if (str.equalsIgnoreCase("OC_BIRTHDATE")) {
                    arrayList.add(Scope.OC_BIRTHDATE);
                }
                if (str.equalsIgnoreCase("OC_ADDRESS")) {
                    arrayList.add(Scope.OC_ADDRESS);
                }
                if (str.equalsIgnoreCase("OC_REAL_NAME")) {
                    arrayList.add(Scope.OC_REAL_NAME);
                }
                if (str.equalsIgnoreCase("ONE_TIME_SHARE")) {
                    arrayList.add(Scope.ONE_TIME_SHARE);
                }
            }
            LineAuthenticationParams build = new LineAuthenticationParams.Builder().scopes(arrayList).build();
            try {
                if (this.activity.getPackageManager().getApplicationInfo(BuildConfig.LINE_APP_PACKAGE_NAME, 0) != null) {
                    z = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
            if (z) {
                intent = LineLoginApi.getLoginIntent(this.activity, this.activity.getString(C4574R.string.line_channel_id), build);
            } else {
                intent = LineLoginApi.getLoginIntentWithoutLineAppAuth(this.activity, this.activity.getString(C4574R.string.line_channel_id), build);
            }
            this.activity.startActivityForResult(intent, LINE_LOGIN_REQUEST_CODE);
        } catch (Exception e) {
            logoutCallback.onError(e.getMessage());
        }
    }

    public void logout() {
        if (this.lineApiClient == null) {
            Activity activity2 = this.activity;
            this.lineApiClient = new LineApiClientBuilder(activity2, activity2.getString(C4574R.string.line_channel_id)).build();
        }
        this.lineApiClient.logout();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 550 && i2 == -1) {
            IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(platformLine);
            LineLoginResult loginResultFromIntent = LineLoginApi.getLoginResultFromIntent(intent);
            int i3 = C45771.$SwitchMap$com$linecorp$linesdk$LineApiResponseCode[loginResultFromIntent.getResponseCode().ordinal()];
            if (i3 == 1) {
                LineProfile lineProfile = loginResultFromIntent.getLineProfile();
                if (lineProfile != null) {
                    MemberProfile memberProfile = new MemberProfile(Platform.LINE, lineProfile.getUserId(), lineProfile.getDisplayName());
                    Uri pictureUrl = lineProfile.getPictureUrl();
                    if (pictureUrl != null) {
                        memberProfile.setPhotoUrl(pictureUrl.toString());
                    }
                    MemberProfile.saveProfile(memberProfile);
                    if (loginCallback != null) {
                        loginCallback.onComplete(memberProfile);
                    }
                }
            } else if (i3 != 2) {
                if (loginCallback != null) {
                    loginCallback.onError(loginResultFromIntent.getErrorData().toString());
                }
            } else if (loginCallback != null) {
                loginCallback.onCancel();
            }
        }
    }
}
