package com.heimavista.thirdpart.api.dialog;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: com.heimavista.thirdpart.api.dialog.a */
/* compiled from: lambda */
public final /* synthetic */ class C4585a implements DialogInterface.OnKeyListener {

    /* renamed from: P */
    private final /* synthetic */ AuthDialog f9354P;

    public /* synthetic */ C4585a(AuthDialog authDialog) {
        this.f9354P = authDialog;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        return this.f9354P.m15223a(dialogInterface, i, keyEvent);
    }
}
