package com.heimavista.thirdpart;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.api.FacebookApi;
import com.heimavista.thirdpart.api.GoogleApi;
import com.heimavista.thirdpart.api.LineApi;
import com.heimavista.thirdpart.api.SinaApi;
import com.heimavista.thirdpart.api.TwitterApi;
import com.heimavista.thirdpart.api.WeChatApi;
import com.tencent.mmkv.MMKV;
import org.xutils.common.util.LogUtil;

public class ThirdPartyLoginOriginalImpl implements IThirdPartyLogin {
    public static final String TAG = "thirdPartLogin ";
    public FacebookApi facebookApi;
    public GoogleApi googleApi;
    public LineApi lineApi;
    public Activity mActivity;
    public Platform platform;
    public SinaApi sinaApi;
    public TwitterApi twitterApi;
    public WeChatApi weChatApi;

    /* renamed from: com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl$1 */
    static /* synthetic */ class C45751 {
        public static final /* synthetic */ int[] $SwitchMap$com$heimavista$thirdpart$Platform = new int[Platform.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.heimavista.thirdpart.Platform[] r0 = com.heimavista.thirdpart.Platform.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform = r0
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.WECHAT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x001f }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.FACEBOOK     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x002a }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.GOOGLE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.LINE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.TWITTER     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.$SwitchMap$com$heimavista$thirdpart$Platform     // Catch:{ NoSuchFieldError -> 0x004b }
                com.heimavista.thirdpart.Platform r1 = com.heimavista.thirdpart.Platform.SINA     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.thirdpart.ThirdPartyLoginOriginalImpl.C45751.<clinit>():void");
        }
    }

    public ThirdPartyLoginOriginalImpl(Activity activity) {
        this.mActivity = activity;
        MMKV.m17080a(activity);
    }

    public void login(@NonNull Platform platform2, @NonNull IThirdPartyLogin.Callback callback) {
        this.platform = platform2;
        LoginCallbackManager instance = LoginCallbackManager.getInstance();
        instance.setCallback(platform2.name() + LoginCallbackManager.SUFFIX_LOGIN, callback);
        LogUtil.m20121i(TAG + platform2.name() + " start login");
        callback.onStart();
        int ordinal = platform2.ordinal();
        if (ordinal == 0) {
            callback.onStart();
            if (this.weChatApi == null) {
                this.weChatApi = new WeChatApi(this.mActivity);
            }
            this.weChatApi.login();
        } else if (ordinal == 1) {
            if (this.facebookApi == null) {
                this.facebookApi = new FacebookApi(this.mActivity);
            }
            this.facebookApi.login();
        } else if (ordinal == 2) {
            if (this.googleApi == null) {
                this.googleApi = new GoogleApi(this.mActivity);
            }
            this.googleApi.login();
        } else if (ordinal == 3) {
            if (this.lineApi == null) {
                this.lineApi = new LineApi(this.mActivity);
            }
            this.lineApi.login();
        } else if (ordinal == 4) {
            if (this.sinaApi == null) {
                this.sinaApi = new SinaApi(this.mActivity);
            }
            this.sinaApi.login();
        } else if (ordinal == 5) {
            if (this.twitterApi == null) {
                this.twitterApi = new TwitterApi(this.mActivity);
            }
            this.twitterApi.login();
        }
    }

    public void logout(@NonNull Platform platform2, @NonNull IThirdPartyLogin.Callback callback) {
        this.platform = platform2;
        LogUtil.m20121i(TAG + platform2.name() + " logout");
        LoginCallbackManager instance = LoginCallbackManager.getInstance();
        instance.setCallback(platform2.name() + LoginCallbackManager.SUFFIX_LOGOUT, callback);
        MemberProfile.clearProfile(platform2);
        int ordinal = platform2.ordinal();
        if (ordinal == 0) {
            return;
        }
        if (ordinal == 1) {
            if (this.facebookApi == null) {
                this.facebookApi = new FacebookApi(this.mActivity);
            }
            this.facebookApi.logout();
        } else if (ordinal == 2) {
            if (this.googleApi == null) {
                this.googleApi = new GoogleApi(this.mActivity);
            }
            this.googleApi.logout();
        } else if (ordinal == 3) {
            if (this.lineApi == null) {
                this.lineApi = new LineApi(this.mActivity);
            }
            this.lineApi.logout();
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        int ordinal;
        LineApi lineApi2;
        Platform platform2 = this.platform;
        if (platform2 != null && (ordinal = platform2.ordinal()) != 0) {
            if (ordinal == 1) {
                FacebookApi facebookApi2 = this.facebookApi;
                if (facebookApi2 != null) {
                    facebookApi2.onActivityResult(i, i2, intent);
                }
            } else if (ordinal == 2) {
                GoogleApi googleApi2 = this.googleApi;
                if (googleApi2 != null) {
                    googleApi2.onActivityResult(i, i2, intent);
                }
            } else if (ordinal == 3 && (lineApi2 = this.lineApi) != null) {
                lineApi2.onActivityResult(i, i2, intent);
            }
        }
    }
}
