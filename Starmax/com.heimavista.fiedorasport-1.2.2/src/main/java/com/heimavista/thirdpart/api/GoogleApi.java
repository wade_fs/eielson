package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.google.android.gms.auth.api.signin.C1976a;
import com.google.android.gms.auth.api.signin.C1978c;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.api.C2030b;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import org.xutils.common.util.LogUtil;
import p119e.p144d.p145a.p157c.p167e.C4065h;

public class GoogleApi {
    public static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;
    public static final int REQUEST_CODE_SIGN_IN = 1;
    public static final String TAG = "GoogleApi";
    public static final Platform platformGoogle = Platform.GOOGLE;
    public Activity activity;
    public C1978c googleSignInClient;

    public GoogleApi(@NonNull Activity activity2) {
        this.activity = activity2;
        initGoogleSignInOptions();
    }

    /* renamed from: a */
    public static /* synthetic */ void m15219a(IThirdPartyLogin.Callback callback, C4065h hVar) {
        if (callback != null) {
            callback.onComplete(null);
        }
    }

    private void initGoogleSignInOptions() {
        GoogleSignInOptions.C1975a aVar = new GoogleSignInOptions.C1975a(GoogleSignInOptions.f3094d0);
        aVar.mo16409c();
        aVar.mo16408b();
        aVar.mo16410d();
        this.googleSignInClient = C1976a.m4547a(this.activity, aVar.mo16407a());
    }

    private boolean isGooglePlayServicesAvailable(IThirdPartyLogin.Callback callback) {
        int c = C2167b.m5251a().mo16831c(this.activity);
        if (c == 0) {
            return true;
        }
        if (C2167b.m5251a().mo16833c(c)) {
            C2167b.m5251a().mo16821a(this.activity, c, 2).show();
        } else {
            new AlertDialog.Builder(this.activity).setMessage(this.activity.getString(C4574R.string.not_support_google)).setCancelable(true).create().show();
        }
        if (callback == null) {
            return false;
        }
        callback.onError(this.activity.getString(C4574R.string.not_support_google));
        return false;
    }

    public void login() {
        if (isGooglePlayServicesAvailable(LoginCallbackManager.getInstance().getLoginCallback(platformGoogle))) {
            if (this.googleSignInClient == null) {
                initGoogleSignInOptions();
            }
            this.activity.startActivityForResult(this.googleSignInClient.mo16415i(), 1);
        }
    }

    public void logout() {
        IThirdPartyLogin.Callback logoutCallback = LoginCallbackManager.getInstance().getLogoutCallback(platformGoogle);
        if (isGooglePlayServicesAvailable(logoutCallback)) {
            if (this.googleSignInClient == null) {
                initGoogleSignInOptions();
            }
            this.googleSignInClient.mo16416j().mo23694a(this.activity, new C4581a(logoutCallback));
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(platformGoogle);
        if (i == 1) {
            try {
                GoogleSignInAccount a = C1976a.m4548a(intent).mo23705a(C2030b.class);
                if (a != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("GoogleApi onSuccess account:");
                    sb.append(a.mo16383D());
                    LogUtil.m20121i(sb.toString());
                    MemberProfile memberProfile = new MemberProfile(Platform.GOOGLE, a.mo16392x(), a.mo16385d());
                    Uri z = a.mo16394z();
                    if (z != null) {
                        memberProfile.setPhotoUrl(z.toString());
                    }
                    memberProfile.setEmail(a.mo16388u());
                    memberProfile.setToken(a.mo16393y());
                    MemberProfile.saveProfile(memberProfile);
                    if (loginCallback != null) {
                        loginCallback.onComplete(memberProfile);
                    }
                }
            } catch (C2030b e) {
                if (loginCallback != null) {
                    loginCallback.onError(e.getMessage());
                }
            }
        }
    }
}
