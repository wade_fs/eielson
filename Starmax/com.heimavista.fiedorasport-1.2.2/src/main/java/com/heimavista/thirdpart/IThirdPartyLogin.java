package com.heimavista.thirdpart;

import android.content.Intent;
import androidx.annotation.NonNull;

public interface IThirdPartyLogin {

    public interface Callback {
        void onCancel();

        void onComplete(MemberProfile memberProfile);

        void onError(String str);

        void onStart();
    }

    void login(@NonNull Platform platform, @NonNull Callback callback);

    void logout(@NonNull Platform platform, @NonNull Callback callback);

    void onActivityResult(int i, int i2, Intent intent);
}
