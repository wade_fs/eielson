package com.heimavista.thirdpart;

import com.heimavista.thirdpart.IThirdPartyLogin;
import java.util.HashMap;
import java.util.Map;

public class LoginCallbackManager {
    public static final String SUFFIX_LOGIN = "_loginCallback";
    public static final String SUFFIX_LOGOUT = "_logoutCallback";
    public static LoginCallbackManager sInstance;
    public Map<String, IThirdPartyLogin.Callback> mCallbackMap = new HashMap();

    public static LoginCallbackManager getInstance() {
        LoginCallbackManager loginCallbackManager;
        Class<LoginCallbackManager> cls = LoginCallbackManager.class;
        synchronized (cls) {
            if (sInstance == null) {
                synchronized (cls) {
                    if (sInstance == null) {
                        sInstance = new LoginCallbackManager();
                    }
                }
            }
            loginCallbackManager = sInstance;
        }
        return loginCallbackManager;
    }

    public IThirdPartyLogin.Callback getCallback(String str) {
        return this.mCallbackMap.get(str);
    }

    public IThirdPartyLogin.Callback getLoginCallback(Platform platform) {
        return getCallback(platform.name() + SUFFIX_LOGIN);
    }

    public IThirdPartyLogin.Callback getLogoutCallback(Platform platform) {
        return getCallback(platform.name() + SUFFIX_LOGOUT);
    }

    public void removeCallback(String str) {
        this.mCallbackMap.remove(str);
    }

    public IThirdPartyLogin.Callback setCallback(String str, IThirdPartyLogin.Callback callback) {
        return this.mCallbackMap.put(str, callback);
    }
}
