package com.heimavista.thirdpart;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.tencent.mmkv.MMKV;

public class MemberProfile {
    public String email;
    public String gender;

    /* renamed from: id */
    public String f9349id;
    public long lastModifyTime;
    public String name;
    public String phone;
    public String photoUrl;
    public Platform platform;
    public String token;
    public String unionId;

    public MemberProfile(Platform platform2, String str, String str2) {
        this.f9349id = str;
        this.name = str2;
        this.platform = platform2;
        this.lastModifyTime = System.currentTimeMillis() / 1000;
    }

    public static void clearProfile(@NonNull Platform... platformArr) {
        for (Platform platform2 : platformArr) {
            MMKV mmkv = getMMKV(platform2);
            mmkv.remove(platform2 + "_id");
            mmkv.remove(platform2 + "_name");
            mmkv.remove(platform2 + "_email");
            mmkv.remove(platform2 + "_gender");
            mmkv.remove(platform2 + "_photoUrl");
            mmkv.remove(platform2 + "_unionId");
            mmkv.remove(platform2 + "_token");
        }
    }

    public static MMKV getMMKV(Platform platform2) {
        return MMKV.m17077a(1, platform2.name());
    }

    public static MemberProfile getProfile(@NonNull Platform platform2) {
        MMKV a = MMKV.m17077a(1, platform2.name());
        String c = a.mo27044c(platform2 + "_id");
        if (TextUtils.isEmpty(c)) {
            return null;
        }
        String c2 = a.mo27044c(platform2 + "_name");
        String c3 = a.mo27044c(platform2 + "_email");
        String c4 = a.mo27044c(platform2 + "_phone");
        String c5 = a.mo27044c(platform2 + "_gender");
        String c6 = a.mo27044c(platform2 + "_photoUrl");
        String c7 = a.mo27044c(platform2 + "_unionId");
        String c8 = a.mo27044c(platform2 + "_token");
        return new MemberProfile(c, c2, c3, c4, c5, c6, c7, c8, a.getLong(platform2 + "_lastModifyTime", 0), platform2);
    }

    public static void saveProfile(@NonNull MemberProfile memberProfile) {
        Platform platform2 = memberProfile.platform;
        MMKV mmkv = getMMKV(platform2);
        mmkv.mo27041b(platform2 + "_id", memberProfile.f9349id);
        mmkv.mo27041b(platform2 + "_name", memberProfile.name);
        mmkv.mo27041b(platform2 + "_email", memberProfile.email);
        mmkv.mo27041b(platform2 + "_gender", memberProfile.gender);
        mmkv.mo27041b(platform2 + "_photoUrl", memberProfile.photoUrl);
        mmkv.mo27041b(platform2 + "_unionId", memberProfile.unionId);
        mmkv.mo27041b(platform2 + "_token", memberProfile.token);
    }

    public String getEmail() {
        return this.email;
    }

    public String getGender() {
        return this.gender;
    }

    public String getId() {
        return this.f9349id;
    }

    public long getLastModifyTime() {
        return this.lastModifyTime;
    }

    public String getName() {
        return this.name;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public Platform getPlatform() {
        return this.platform;
    }

    public String getToken() {
        return this.token;
    }

    public String getUnionId() {
        return this.unionId;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setId(String str) {
        this.f9349id = str;
    }

    public void setLastModifyTime(long j) {
        this.lastModifyTime = j;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPhone(String str) {
        this.phone = str;
    }

    public void setPhotoUrl(String str) {
        this.photoUrl = str;
    }

    public void setPlatform(Platform platform2) {
        this.platform = platform2;
    }

    public void setToken(String str) {
        this.token = str;
    }

    public void setUnionId(String str) {
        this.unionId = str;
    }

    @NonNull
    public String toString() {
        return "MemberProfile{id='" + this.f9349id + '\'' + ", name='" + this.name + '\'' + ", email='" + this.email + '\'' + ", phone='" + this.phone + '\'' + ", gender='" + this.gender + '\'' + ", photoUrl='" + this.photoUrl + '\'' + ", unionId='" + this.unionId + '\'' + ", token='" + this.token + '\'' + ", lastModifyTime=" + this.lastModifyTime + ", platform=" + this.platform + '}';
    }

    public MemberProfile(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, long j, Platform platform2) {
        this.f9349id = str;
        this.name = str2;
        this.email = str3;
        this.phone = str4;
        this.gender = str5;
        this.photoUrl = str6;
        this.unionId = str7;
        this.token = str8;
        this.platform = platform2;
    }
}
