package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareDialog;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import java.io.File;
import java.util.Arrays;
import org.json.JSONObject;
import org.xutils.common.util.LogUtil;

public class FacebookApi {
    public static final String TAG = "FacebookApi";
    public static final Platform platformFacebook = Platform.FACEBOOK;
    public Activity activity;
    public CallbackManager callbackManager = CallbackManager.Factory.create();
    public FacebookCallback<Sharer.Result> facebookCallback;
    public ShareDialog shareDialog;

    public FacebookApi(@NonNull Activity activity2) {
        this.activity = activity2;
    }

    private void initFacebookShare() {
        this.shareDialog = new ShareDialog(this.activity);
        this.shareDialog.registerCallback(this.callbackManager, this.facebookCallback);
    }

    public void login() {
        final IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(platformFacebook);
        LoginManager.getInstance().registerCallback(this.callbackManager, new FacebookCallback<LoginResult>() {
            /* class com.heimavista.thirdpart.api.FacebookApi.C45761 */

            /* renamed from: a */
            public static /* synthetic */ void m15218a(IThirdPartyLogin.Callback callback, JSONObject jSONObject, GraphResponse graphResponse) {
                LogUtil.m20121i("FacebookApi data:" + jSONObject);
                if (jSONObject != null) {
                    String optString = jSONObject.optString("id");
                    MemberProfile memberProfile = new MemberProfile(Platform.FACEBOOK, optString, jSONObject.optString("name"));
                    Uri profilePictureUri = ImageRequest.getProfilePictureUri(optString, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                    if (profilePictureUri != null) {
                        memberProfile.setPhotoUrl(profilePictureUri.toString());
                    }
                    MemberProfile.saveProfile(memberProfile);
                    if (callback != null) {
                        callback.onComplete(memberProfile);
                    }
                } else if (graphResponse.getConnection() != null && callback != null) {
                    callback.onError(graphResponse.getError().getErrorMessage());
                }
            }

            public void onCancel() {
                LogUtil.m20121i("FacebookApi onCancel");
                IThirdPartyLogin.Callback callback = loginCallback;
                if (callback != null) {
                    callback.onCancel();
                }
            }

            public void onError(FacebookException facebookException) {
                LogUtil.m20121i("FacebookApi onError " + facebookException.getMessage());
                IThirdPartyLogin.Callback callback = loginCallback;
                if (callback != null) {
                    callback.onError(facebookException.getMessage());
                }
            }

            public void onSuccess(LoginResult loginResult) {
                LogUtil.m20121i("FacebookApi onSuccess");
                GraphRequest newMeRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new C4583c(loginCallback));
                Bundle bundle = new Bundle();
                bundle.putString(GraphRequest.FIELDS_PARAM, FacebookApi.this.activity.getString(C4574R.string.facebook_fields));
                newMeRequest.setParameters(bundle);
                newMeRequest.executeAsync();
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(this.activity, Arrays.asList(this.activity.getResources().getStringArray(C4574R.array.facebook_permissions)));
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        CallbackManager callbackManager2 = this.callbackManager;
        if (callbackManager2 != null) {
            callbackManager2.onActivityResult(i, i2, intent);
        }
    }

    public void setFacebookCallback(FacebookCallback<Sharer.Result> facebookCallback2) {
        this.facebookCallback = facebookCallback2;
    }

    public void shareLink2FB(String str, String str2) {
        initFacebookShare();
        if (ShareDialog.canShow((Class<? extends ShareContent>) ShareLinkContent.class)) {
            this.shareDialog.show(((ShareLinkContent.Builder) new ShareLinkContent.Builder().setQuote(str).setContentUrl(TextUtils.isEmpty(str2) ? null : Uri.parse(str2))).build());
        }
    }

    public void sharePhoto2FB(String str, Bitmap bitmap) {
        initFacebookShare();
        if (ShareDialog.canShow((Class<? extends ShareContent>) SharePhotoContent.class)) {
            this.shareDialog.show(new SharePhotoContent.Builder().addPhoto(new SharePhoto.Builder().setCaption(str).setBitmap(bitmap).build()).build());
        }
    }

    public void shareVideo2FB(String str, String str2, String str3, String str4) {
        File file = new File(str4);
        if (file.exists()) {
            initFacebookShare();
            if (ShareDialog.canShow((Class<? extends ShareContent>) ShareVideoContent.class)) {
                this.shareDialog.show(((ShareVideoContent.Builder) new ShareVideoContent.Builder().setContentTitle(str).setContentDescription(str2).setContentUrl(TextUtils.isEmpty(str3) ? null : Uri.parse(str3))).setVideo(new ShareVideo.Builder().setLocalUrl(Uri.fromFile(file)).build()).build());
            }
        }
    }
}
