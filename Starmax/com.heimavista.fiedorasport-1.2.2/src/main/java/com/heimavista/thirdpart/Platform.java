package com.heimavista.thirdpart;

public enum Platform {
    WECHAT,
    FACEBOOK,
    GOOGLE,
    LINE,
    SINA,
    TWITTER
}
