package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.heimavista.thirdpart.C4574R;
import com.tencent.p214mm.opensdk.modelmsg.SendAuth;
import com.tencent.p214mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.p214mm.opensdk.modelmsg.WXImageObject;
import com.tencent.p214mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.p214mm.opensdk.modelmsg.WXTextObject;
import com.tencent.p214mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.p214mm.opensdk.openapi.IWXAPI;
import com.tencent.p214mm.opensdk.openapi.WXAPIFactory;
import java.io.ByteArrayOutputStream;

public class WeChatApi {
    public Activity activity;
    public IWXAPI wxApi;

    public WeChatApi(@NonNull Activity activity2) {
        this.activity = activity2;
    }

    private byte[] bmpToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        bitmap.recycle();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    private String buildTransaction(String str) {
        if (str == null) {
            return String.valueOf(System.currentTimeMillis());
        }
        return str + System.currentTimeMillis();
    }

    private int calculateInSampleSize(BitmapFactory.Options options) {
        int i = options.outHeight;
        int i2 = options.outWidth;
        int i3 = 1;
        while (true) {
            if (i <= 96 && i2 <= 64) {
                return i3;
            }
            i >>= 1;
            i2 >>= 1;
            i3 <<= 1;
        }
    }

    private Bitmap getBitmap(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(str, options);
    }

    private void initWxApi() {
        if (this.wxApi == null) {
            String string = this.activity.getString(C4574R.string.wechat_appKey);
            this.wxApi = WXAPIFactory.createWXAPI(this.activity, string, true);
            this.wxApi.registerApp(string);
        }
    }

    public void login() {
        initWxApi();
        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "wechat_sdk_login";
        this.wxApi.sendReq(req);
    }

    public boolean shareImage2Wx(boolean z, String str) {
        initWxApi();
        WXImageObject wXImageObject = new WXImageObject();
        wXImageObject.imagePath = str;
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.mediaObject = wXImageObject;
        wXMediaMessage.thumbData = bmpToByteArray(getBitmap(str));
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("img");
        req.message = wXMediaMessage;
        req.scene = z ? 1 : 0;
        return this.wxApi.sendReq(req);
    }

    public boolean shareText2Wx(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        initWxApi();
        WXTextObject wXTextObject = new WXTextObject();
        wXTextObject.text = str;
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.mediaObject = wXTextObject;
        wXMediaMessage.description = str;
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("text");
        req.message = wXMediaMessage;
        req.scene = 0;
        return this.wxApi.sendReq(req);
    }

    public boolean shareWebPage2Wx(boolean z, String str, String str2, String str3, String str4) {
        initWxApi();
        WXWebpageObject wXWebpageObject = new WXWebpageObject();
        wXWebpageObject.webpageUrl = str;
        WXMediaMessage wXMediaMessage = new WXMediaMessage(wXWebpageObject);
        wXMediaMessage.title = str2;
        wXMediaMessage.description = str3;
        wXMediaMessage.thumbData = bmpToByteArray(getBitmap(str4));
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = wXMediaMessage;
        req.scene = z ? 1 : 0;
        return this.wxApi.sendReq(req);
    }
}
