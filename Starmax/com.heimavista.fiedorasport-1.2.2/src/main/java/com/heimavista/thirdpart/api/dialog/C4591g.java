package com.heimavista.thirdpart.api.dialog;

import android.content.DialogInterface;
import android.webkit.SslErrorHandler;
import com.heimavista.thirdpart.api.dialog.TwitterAuthDialog;

/* renamed from: com.heimavista.thirdpart.api.dialog.g */
/* compiled from: lambda */
public final /* synthetic */ class C4591g implements DialogInterface.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ SslErrorHandler f9364P;

    public /* synthetic */ C4591g(SslErrorHandler sslErrorHandler) {
        this.f9364P = sslErrorHandler;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        TwitterAuthDialog.C45841.m15229a(this.f9364P, dialogInterface, i);
    }
}
