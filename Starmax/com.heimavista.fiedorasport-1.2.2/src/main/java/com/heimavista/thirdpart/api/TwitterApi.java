package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.heimavista.thirdpart.api.dialog.TwitterAuthDialog;
import com.tencent.mmkv.MMKV;
import java.io.File;
import java.util.Map;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterApi {
    public static Platform platformTwitter = Platform.TWITTER;
    public Activity activity;

    public TwitterApi(@NonNull Activity activity2) {
        this.activity = activity2;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ void m15220a(IThirdPartyLogin.Callback callback, Map map) {
        User twitterUserInfo = getTwitterUserInfo(this.activity);
        if (twitterUserInfo != null) {
            MemberProfile memberProfile = new MemberProfile(Platform.TWITTER, String.valueOf(twitterUserInfo.getId()), twitterUserInfo.getName());
            memberProfile.setPhotoUrl(twitterUserInfo.getProfileImageURL());
            MemberProfile.saveProfile(memberProfile);
            if (callback != null) {
                callback.onComplete(memberProfile);
            }
        }
    }

    public static MMKV getMMKV() {
        return MMKV.m17077a(1, Platform.TWITTER.name());
    }

    public User getTwitterUserInfo(Context context) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(context.getString(C4574R.string.twitter_appKey));
        configurationBuilder.setOAuthConsumerSecret(context.getString(C4574R.string.twitter_appSecret));
        MMKV mmkv = getMMKV();
        configurationBuilder.setOAuthAccessToken(mmkv.mo27044c(Platform.TWITTER.name() + "_token"));
        MMKV mmkv2 = getMMKV();
        configurationBuilder.setOAuthAccessTokenSecret(mmkv2.mo27044c(Platform.TWITTER.name() + "_secret"));
        Twitter instance = new TwitterFactory(configurationBuilder.build()).getInstance();
        try {
            return instance.showUser(instance.getId());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void login() {
        IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(platformTwitter);
        MemberProfile profile = MemberProfile.getProfile(platformTwitter);
        if (profile == null) {
            Activity activity2 = this.activity;
            new TwitterAuthDialog(activity2, activity2.getString(C4574R.string.twitter_appKey), this.activity.getString(C4574R.string.twitter_appSecret), new C4582b(this, loginCallback)).show();
        } else if (loginCallback != null) {
            loginCallback.onComplete(profile);
        }
    }

    public boolean shareTwitter(String str, String str2) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(this.activity.getString(C4574R.string.twitter_appKey));
        configurationBuilder.setOAuthConsumerSecret(this.activity.getString(C4574R.string.twitter_appSecret));
        MMKV mmkv = getMMKV();
        configurationBuilder.setOAuthAccessToken(mmkv.mo27044c(Platform.TWITTER.name() + "_token"));
        MMKV mmkv2 = getMMKV();
        configurationBuilder.setOAuthAccessTokenSecret(mmkv2.mo27044c(Platform.TWITTER.name() + "_secret"));
        Twitter instance = new TwitterFactory(configurationBuilder.build()).getInstance();
        try {
            StatusUpdate statusUpdate = new StatusUpdate(str);
            if (!TextUtils.isEmpty(str2)) {
                statusUpdate.media(new File(str2));
            }
            if (instance.updateStatus(statusUpdate) != null) {
                return true;
            }
            return false;
        } catch (TwitterException e) {
            e.printStackTrace();
            return false;
        }
    }
}
