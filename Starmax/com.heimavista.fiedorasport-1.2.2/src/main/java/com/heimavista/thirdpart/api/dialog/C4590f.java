package com.heimavista.thirdpart.api.dialog;

import android.content.DialogInterface;
import android.webkit.SslErrorHandler;
import com.heimavista.thirdpart.api.dialog.TwitterAuthDialog;

/* renamed from: com.heimavista.thirdpart.api.dialog.f */
/* compiled from: lambda */
public final /* synthetic */ class C4590f implements DialogInterface.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ SslErrorHandler f9363P;

    public /* synthetic */ C4590f(SslErrorHandler sslErrorHandler) {
        this.f9363P = sslErrorHandler;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        TwitterAuthDialog.C45841.m15234b(this.f9363P, dialogInterface, i);
    }
}
