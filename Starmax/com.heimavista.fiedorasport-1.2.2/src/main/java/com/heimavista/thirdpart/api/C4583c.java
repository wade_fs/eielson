package com.heimavista.thirdpart.api;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.api.FacebookApi;
import org.json.JSONObject;

/* renamed from: com.heimavista.thirdpart.api.c */
/* compiled from: lambda */
public final /* synthetic */ class C4583c implements GraphRequest.GraphJSONObjectCallback {

    /* renamed from: a */
    private final /* synthetic */ IThirdPartyLogin.Callback f9353a;

    public /* synthetic */ C4583c(IThirdPartyLogin.Callback callback) {
        this.f9353a = callback;
    }

    public final void onCompleted(JSONObject jSONObject, GraphResponse graphResponse) {
        FacebookApi.C45761.m15218a(this.f9353a, jSONObject, graphResponse);
    }
}
