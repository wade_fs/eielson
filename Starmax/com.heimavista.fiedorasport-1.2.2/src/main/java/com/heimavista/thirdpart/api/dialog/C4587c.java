package com.heimavista.thirdpart.api.dialog;

import android.webkit.WebView;
import com.heimavista.thirdpart.api.dialog.TwitterAuthDialog;

/* renamed from: com.heimavista.thirdpart.api.dialog.c */
/* compiled from: lambda */
public final /* synthetic */ class C4587c implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ TwitterAuthDialog.C45841 f9357P;

    /* renamed from: Q */
    private final /* synthetic */ String f9358Q;

    /* renamed from: R */
    private final /* synthetic */ WebView f9359R;

    public /* synthetic */ C4587c(TwitterAuthDialog.C45841 r1, String str, WebView webView) {
        this.f9357P = r1;
        this.f9358Q = str;
        this.f9359R = webView;
    }

    public final void run() {
        this.f9357P.m15233a(this.f9358Q, this.f9359R);
    }
}
