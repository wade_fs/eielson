package com.heimavista.thirdpart;

import android.app.Activity;

public class ThirdPartyLoginFactory {
    public static IThirdPartyLogin create(Activity activity) {
        return new ThirdPartyLoginOriginalImpl(activity);
    }
}
