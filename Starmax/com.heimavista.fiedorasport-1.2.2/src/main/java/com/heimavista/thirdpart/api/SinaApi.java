package com.heimavista.thirdpart.api;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.facebook.share.internal.ShareConstants;
import com.heimavista.thirdpart.C4574R;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.LoginCallbackManager;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.utils.Utility;
import java.io.File;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;

public class SinaApi {
    public static final Platform platformSina = Platform.SINA;
    public Activity activity;

    public SinaApi(@NonNull Activity activity2) {
        this.activity = activity2;
    }

    public static RequestParams getSinaUserParams(String str, String str2) {
        RequestParams requestParams = new RequestParams("https://api.weibo.com/2/users/show.json");
        requestParams.addBodyParameter("access_token", str);
        requestParams.addBodyParameter("uid", str2);
        return requestParams;
    }

    public void login() {
        final IThirdPartyLogin.Callback loginCallback = LoginCallbackManager.getInstance().getLoginCallback(platformSina);
        MemberProfile profile = MemberProfile.getProfile(platformSina);
        if (profile == null) {
            Activity activity2 = this.activity;
            new SsoHandler(this.activity, new AuthInfo(activity2, activity2.getString(C4574R.string.sina_appKey), this.activity.getString(C4574R.string.sina_redirectUrl), "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write")).authorize(new WeiboAuthListener() {
                /* class com.heimavista.thirdpart.api.SinaApi.C45781 */

                public void onCancel() {
                    IThirdPartyLogin.Callback callback = loginCallback;
                    if (callback != null) {
                        callback.onCancel();
                    }
                }

                public void onComplete(Bundle bundle) {
                    Oauth2AccessToken parseAccessToken = Oauth2AccessToken.parseAccessToken(bundle);
                    if (parseAccessToken == null || !parseAccessToken.isSessionValid()) {
                        IThirdPartyLogin.Callback callback = loginCallback;
                        if (callback != null) {
                            callback.onError("session invalid");
                            return;
                        }
                        return;
                    }
                    MemberProfile memberProfile = new MemberProfile(Platform.SINA, parseAccessToken.getUid(), "");
                    memberProfile.setToken(parseAccessToken.getToken());
                    memberProfile.setLastModifyTime(parseAccessToken.getExpiresTime());
                    memberProfile.setPhone(parseAccessToken.getPhoneNum());
                    IThirdPartyLogin.Callback callback2 = loginCallback;
                    if (callback2 != null) {
                        callback2.onComplete(memberProfile);
                    }
                }

                public void onWeiboException(WeiboException weiboException) {
                    loginCallback.onError(weiboException.getMessage());
                }
            });
        } else if (loginCallback != null) {
            loginCallback.onComplete(profile);
        }
    }

    public void shareImage(String str) {
        Activity activity2 = this.activity;
        final IWeiboShareAPI createWeiboAPI = WeiboShareSDK.createWeiboAPI(activity2, activity2.getString(C4574R.string.sina_appKey));
        createWeiboAPI.registerApp();
        C5217x.image().loadFile(str, ImageOptions.DEFAULT, new Callback.CacheCallback<File>() {
            /* class com.heimavista.thirdpart.api.SinaApi.C45792 */

            public boolean onCache(File file) {
                return false;
            }

            public void onCancelled(Callback.CancelledException cancelledException) {
            }

            public void onError(Throwable th, boolean z) {
            }

            public void onFinished() {
            }

            public void onSuccess(File file) {
                if (createWeiboAPI.isWeiboAppSupportAPI()) {
                    Bitmap decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath());
                    WeiboMessage weiboMessage = new WeiboMessage();
                    ImageObject imageObject = new ImageObject();
                    imageObject.setImageObject(new BitmapDrawable(SinaApi.this.activity.getResources(), decodeFile).getBitmap());
                    weiboMessage.mediaObject = imageObject;
                    SendMessageToWeiboRequest sendMessageToWeiboRequest = new SendMessageToWeiboRequest();
                    sendMessageToWeiboRequest.transaction = String.valueOf(System.currentTimeMillis());
                    sendMessageToWeiboRequest.message = weiboMessage;
                    createWeiboAPI.sendRequest(SinaApi.this.activity, sendMessageToWeiboRequest);
                    return;
                }
                RequestParams requestParams = new RequestParams("https://upload.api.weibo.com/2/statuses/upload.json");
                requestParams.addBodyParameter(ShareConstants.FEED_SOURCE_PARAM, SinaApi.this.activity.getString(C4574R.string.sina_appKey));
                requestParams.addBodyParameter("access_token", SinaApi.this.activity.getString(C4574R.string.sina_appSecret));
                requestParams.addBodyParameter("pic", file);
                C5217x.http().post(requestParams, null);
            }
        });
    }

    public void shareWebPage2Sina(String str, String str2, String str3, String str4) {
        Activity activity2 = this.activity;
        final IWeiboShareAPI createWeiboAPI = WeiboShareSDK.createWeiboAPI(activity2, activity2.getString(C4574R.string.sina_appKey));
        createWeiboAPI.registerApp();
        if (TextUtils.isEmpty(str4)) {
            shareWebPage2Sina(str, str2, str3, null, createWeiboAPI);
            return;
        }
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        C5217x.image().loadFile(str4, ImageOptions.DEFAULT, new Callback.CacheCallback<File>() {
            /* class com.heimavista.thirdpart.api.SinaApi.C45803 */

            public boolean onCache(File file) {
                return false;
            }

            public void onCancelled(Callback.CancelledException cancelledException) {
            }

            public void onError(Throwable th, boolean z) {
            }

            public void onFinished() {
            }

            public void onSuccess(File file) {
                SinaApi.this.shareWebPage2Sina(str5, str6, str7, file, createWeiboAPI);
            }
        });
    }

    public void shareWebPage2Sina(String str, String str2, String str3, File file, IWeiboShareAPI iWeiboShareAPI) {
        if (iWeiboShareAPI.isWeiboAppSupportAPI()) {
            WeiboMessage weiboMessage = new WeiboMessage();
            WebpageObject webpageObject = new WebpageObject();
            webpageObject.identify = Utility.generateGUID();
            webpageObject.title = str;
            webpageObject.description = str2;
            if (file != null) {
                webpageObject.setThumbImage(BitmapFactory.decodeFile(file.getAbsolutePath()));
            }
            webpageObject.actionUrl = str3;
            webpageObject.defaultText = str2;
            weiboMessage.mediaObject = webpageObject;
            SendMessageToWeiboRequest sendMessageToWeiboRequest = new SendMessageToWeiboRequest();
            sendMessageToWeiboRequest.transaction = String.valueOf(System.currentTimeMillis());
            sendMessageToWeiboRequest.message = weiboMessage;
            iWeiboShareAPI.sendRequest(this.activity, sendMessageToWeiboRequest);
            return;
        }
        RequestParams requestParams = new RequestParams("https://upload.api.weibo.com/2/statuses/upload.json");
        requestParams.addBodyParameter(ShareConstants.FEED_SOURCE_PARAM, this.activity.getString(C4574R.string.sina_appKey));
        requestParams.addBodyParameter("access_token", this.activity.getString(C4574R.string.sina_appSecret));
        requestParams.addBodyParameter("status", str2);
        if (file != null) {
            requestParams.addBodyParameter("pic", file);
        }
        C5217x.http().post(requestParams, null);
    }
}
