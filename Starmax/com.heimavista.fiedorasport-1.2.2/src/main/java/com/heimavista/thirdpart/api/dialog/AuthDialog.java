package com.heimavista.thirdpart.api.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import java.util.Map;
import java.util.Objects;

public class AuthDialog extends Dialog {
    public RelativeLayout mContent;
    public ProgressDialog mSpinner;
    public WebView mWebView;

    public interface AuthListener {
        void onComplete(Map<String, Object> map);
    }

    public AuthDialog(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ boolean m15223a(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        onBack();
        return false;
    }

    private void setUpWebView() {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        this.mWebView = new WebView(getContext());
        this.mWebView.setWebViewClient(getWebViewClient());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        this.mContent.setBackgroundColor(0);
        try {
            int i = (int) (getContext().getResources().getDisplayMetrics().density * 10.0f);
            layoutParams2.leftMargin = i;
            layoutParams2.topMargin = i;
            layoutParams2.rightMargin = i;
            layoutParams2.bottomMargin = i;
        } catch (Exception e) {
            e.printStackTrace();
        }
        relativeLayout.setBackgroundColor(Color.parseColor("#88888888"));
        relativeLayout.addView(this.mWebView, layoutParams2);
        relativeLayout.setGravity(17);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = 30;
        layoutParams.bottomMargin = 10;
        this.mContent.addView(relativeLayout, layoutParams);
        loadContent();
    }

    public WebView getWebView() {
        return this.mWebView;
    }

    public WebViewClient getWebViewClient() {
        return null;
    }

    public void hideSpinner() {
        if (this.mSpinner.isShowing()) {
            this.mSpinner.dismiss();
        }
    }

    public void loadContent() {
    }

    public void onBack() {
        try {
            this.mSpinner.dismiss();
            if (this.mWebView != null) {
                this.mWebView.stopLoading();
                this.mWebView.destroy();
            }
        } catch (Exception unused) {
        }
        dismiss();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mSpinner = new ProgressDialog(getContext());
        this.mSpinner.requestWindowFeature(1);
        this.mSpinner.setMessage("Loading...");
        this.mSpinner.setOnKeyListener(new C4585a(this));
        requestWindowFeature(1);
        ((Window) Objects.requireNonNull(getWindow())).setFeatureDrawableAlpha(0, 0);
        this.mContent = new RelativeLayout(getContext());
        setUpWebView();
        addContentView(this.mContent, new ViewGroup.LayoutParams(-1, -1));
    }

    public void showSpinner() {
        this.mSpinner.show();
    }
}
