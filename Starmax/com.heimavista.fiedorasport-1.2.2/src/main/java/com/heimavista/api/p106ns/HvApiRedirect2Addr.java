package com.heimavista.api.p106ns;

import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;
import p119e.p189e.p219h.HttpRequest;

/* renamed from: com.heimavista.api.ns.HvApiRedirect2Addr */
public class HvApiRedirect2Addr extends HvApiBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "ns";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "redirect2Addr";
    }

    public String getUrl() {
        return HttpRequest.m17164a(genHttpRequestConfig(), genRequestParams(null));
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }

    public HttpCancelable request(HttpParams cVar) {
        return null;
    }

    public HvApiResponse requestSync(HttpParams cVar) {
        return null;
    }
}
