package com.heimavista.api.base;

import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;
import p119e.p189e.p219h.HttpRequest;
import p119e.p189e.p219h.HttpRequestConfig;

public class HvApiGetMedia extends HvApiBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "base";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getMedia";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public String getUrl(String str) {
        HttpRequestConfig genHttpRequestConfig = genHttpRequestConfig();
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("mediaId", (Object) str);
        return HttpRequest.m17164a(genHttpRequestConfig, genRequestParams(cVar));
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }

    public HttpCancelable request(HttpParams cVar) {
        return null;
    }

    public HvApiResponse requestSync(HttpParams cVar) {
        return null;
    }
}
