package com.heimavista.api.band;

import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetHeartData extends HvApiGetDataBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getHeartData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.i.d.a(java.lang.String, java.lang.String, long, int, boolean, int):void
     arg types: [java.lang.String, java.lang.String, long, int, int, int]
     candidates:
      e.e.d.a.a(java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer):java.util.List<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.i.d.a(java.lang.String, java.lang.String, long, int, boolean, int):void */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                HeartRateDataDb.m13152a(MemberControl.m17125s().mo27187l(), jSONArray.optString(a2.indexOf("timezone")), jSONArray.optLong(a2.indexOf("timestamp")), jSONArray.optInt(a2.indexOf("heartrate")), true, 2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
