package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p195j.C4209e;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetStepData extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getStepData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                StepInfo stepInfo = new StepInfo();
                stepInfo.f6242P = cVar.mo27198a("userNbr", "");
                stepInfo.f6243Q = jSONArray.optLong(a2.indexOf("timestamp"));
                stepInfo.f6244R = jSONArray.optString(a2.indexOf("timezone"));
                stepInfo.f6247U = jSONArray.optInt(a2.indexOf("cal"));
                stepInfo.f6246T = jSONArray.optInt(a2.indexOf("distance"));
                stepInfo.f6248V = jSONArray.optInt(a2.indexOf("stepTime"));
                stepInfo.f6245S = jSONArray.optInt(a2.indexOf("step"));
                C4209e.m13331a(stepInfo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (fVar.mo25323a("isNext", (Integer) 0).intValue() == 1) {
            requestSync(cVar.mo27198a("userNbr", ""));
        }
    }

    public HttpCancelable request(String str) {
        return super.request(getHttpParamsWithTick(str));
    }

    public HvApiResponse requestSync(String str) {
        return super.requestSync(getHttpParamsWithTick(str));
    }
}
