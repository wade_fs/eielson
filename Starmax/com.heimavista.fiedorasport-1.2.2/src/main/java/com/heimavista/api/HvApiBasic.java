package com.heimavista.api;

import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiBasic extends HvApiBase {
    public static final String ACTION_OTHER_BIND = (HvApiBasic.class.getName() + ".ACTION_OTHER_BIND");

    public long getTick() {
        return TickManager.m10523b(getTickKey());
    }

    /* access modifiers changed from: protected */
    public String getTickKey() {
        if (isNeedLogin()) {
            return MemberControl.m17125s().mo27187l() + "_" + getFun() + "_" + getOp();
        }
        return getFun() + "_" + getOp();
    }

    /* access modifiers changed from: protected */
    public void onApiCallback(HttpParams cVar, HttpParams cVar2, int i, String str, ParamJsonData fVar) {
        super.onApiCallback(cVar, cVar2, i, str, fVar);
        if (i == 901) {
            FSManager.m10323r().mo22829b();
            Bundle bundle = new Bundle();
            bundle.putString(NotificationCompat.CATEGORY_MESSAGE, str);
            HvApp.m13010c().mo24158a(ACTION_OTHER_BIND, bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        LogUtils.m1139c("op=" + getOp() + " onApiFailed " + str);
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.lang.String, long]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        LogUtils.m1139c("op=" + getOp() + " onApiSuccess");
        if (fVar.mo25331a("lastTime")) {
            TickManager.m10526c(getTickKey(), fVar.mo25324a("lastTime", (Long) 0L).longValue());
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
