package com.heimavista.api;

import com.facebook.internal.ServerProtocol;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiStart extends HvApi {
    /* access modifiers changed from: protected */
    public String getAppId() {
        return HvAppConfig.m13024a().mo24166a("app", "id");
    }

    /* access modifiers changed from: protected */
    public String getAppSecret() {
        return HvAppConfig.m13024a().mo24166a("app", "secret");
    }

    /* access modifiers changed from: protected */
    public HttpParams getBasicParams() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String getUri() {
        return HvAppConfig.m13024a().mo24166a("app", "url");
    }

    /* access modifiers changed from: protected */
    public String getVersion() {
        return HvAppConfig.m13024a().mo24166a("app", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
    }

    /* access modifiers changed from: protected */
    public void onApiCancel(HttpParams cVar, HttpParams cVar2, String str) {
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
    }

    /* access modifiers changed from: protected */
    public void onApiNewInform() {
    }
}
