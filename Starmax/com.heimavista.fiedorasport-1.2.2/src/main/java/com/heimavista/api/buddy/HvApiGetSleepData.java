package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p195j.C4208d;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSleepData extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSleepData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SleepInfo cVar3 = new SleepInfo();
                cVar3.f6262a = cVar.mo27198a("userNbr", "");
                cVar3.f6263b = jSONArray.optLong(a2.indexOf("timestamp"));
                cVar3.f6264c = jSONArray.optString(a2.indexOf("timezone"));
                cVar3.f6265d = jSONArray.optInt(a2.indexOf("type"));
                cVar3.f6266e = jSONArray.optLong(a2.indexOf(TtmlNode.START));
                cVar3.f6267f = jSONArray.optLong(a2.indexOf(TtmlNode.END));
                cVar3.f6268g = jSONArray.optInt(a2.indexOf("sleep"));
                C4208d.m13322a(cVar3);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (fVar.mo25323a("isNext", (Integer) 0).intValue() == 1) {
            requestSync(cVar.mo27198a("userNbr", ""));
        }
    }

    public HttpCancelable request(String str) {
        return super.request(getHttpParamsWithTick(str));
    }

    public HvApiResponse requestSync(String str) {
        return super.requestSync(getHttpParamsWithTick(str));
    }
}
