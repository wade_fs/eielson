package com.heimavista.api.buddy;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiReqBuddyById extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "reqBuddyById";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, OnResultListener<ParamJsonData> dVar) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("userId", (Object) str);
        return super.request(cVar, dVar);
    }
}
