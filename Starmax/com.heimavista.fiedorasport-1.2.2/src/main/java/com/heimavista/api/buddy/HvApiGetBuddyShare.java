package com.heimavista.api.buddy;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p195j.BuddyShareDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBuddyShare extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBuddyShare";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        BuddyShareDb.m13289t();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                BuddyShareDb i = BuddyShareDb.m13288i(jSONArray.optString(a2.indexOf("userNbr")));
                i.mo24317d(jSONArray.optInt(a2.indexOf("isShareStep")));
                i.mo24316c(jSONArray.optInt(a2.indexOf("isShareSleep")));
                i.mo24314a(jSONArray.optInt(a2.indexOf("isShareHeart")));
                i.mo24315b(jSONArray.optInt(a2.indexOf("isShareLoc")));
                i.mo24181l();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        return super.request(new HttpParams(), dVar);
    }
}
