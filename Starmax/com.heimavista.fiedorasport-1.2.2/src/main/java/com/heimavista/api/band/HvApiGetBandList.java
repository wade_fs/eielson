package com.heimavista.api.band;

import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p193d.p194i.BandInfoDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBandList extends HvApiBasic {
    private OnResultListener<ParamJsonData> listener;
    private int requestTimes = 0;

    /* access modifiers changed from: protected */
    public String getFun() {
        return "band";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBandList";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        if (this.requestTimes < 3) {
            request(this.listener);
        }
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("BandList", "[]"), true).mo25327a();
        int size = a.size();
        for (int i = 0; i < size; i++) {
            try {
                JSONObject jSONObject = new JSONObject(String.valueOf(a.get(i)));
                BandInfoDb.m13129c(jSONObject);
                if (i == size - 1) {
                    String optString = jSONObject.optString("bandId");
                    String optString2 = jSONObject.optString("deviceName");
                    FSManager.m10323r().mo22827a(optString);
                    FSManager.m10323r().mo22830b(optString2);
                    FSManager.m10323r().mo22834d(jSONObject.optString("deviceMacAddress"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.requestTimes = 0;
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        this.requestTimes++;
        this.listener = dVar;
        return super.request(null, dVar);
    }
}
