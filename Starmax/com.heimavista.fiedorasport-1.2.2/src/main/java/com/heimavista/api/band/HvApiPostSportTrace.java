package com.heimavista.api.band;

import android.location.Location;
import androidx.annotation.NonNull;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.band.SportTrace;
import com.heimavista.entity.band.SportTraceInfo;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.p199j.p200m.GPSConverterUtils;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SportTraceDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostSportTrace extends HvApiBaseBand {
    private OnResultListener<ParamJsonData> listener;
    private int requestTimes = 0;
    private String sportId;
    private List<SportTrace> sportTraces;

    public HvApiPostSportTrace(String str, @NonNull List<SportTrace> list, OnResultListener<ParamJsonData> dVar) {
        this.sportId = str;
        this.sportTraces = list;
        this.listener = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getHttpParams() {
        JSONArray jSONArray = new JSONArray();
        int size = this.sportTraces.size();
        for (int i = 0; i < size; i++) {
            if (i == 0) {
                SportTrace eVar = this.sportTraces.get(i);
                if (eVar.mo22572c()) {
                    Location a = eVar.mo22569a();
                    jSONArray.put(getItem(eVar.mo22571b(), a.getTime() / 1000, a.getLatitude(), a.getLongitude(), 0, 0));
                }
            } else {
                SportTrace eVar2 = this.sportTraces.get(i - 1);
                SportTrace eVar3 = this.sportTraces.get(i);
                Location a2 = eVar2.mo22569a();
                Location a3 = eVar3.mo22569a();
                long time = a3.getTime() - a2.getTime();
                double d = 0.0d;
                if (eVar2.mo22572c() && eVar3.mo22572c()) {
                    d = (double) GPSConverterUtils.m13467a(a3.getLatitude(), a3.getLongitude(), a2.getLatitude(), a2.getLongitude());
                }
                double longitude = a3.getLongitude();
                jSONArray.put(getItem(eVar3.mo22571b(), a3.getTime() / 1000, a3.getLatitude(), longitude, (int) (time / 1000), (int) d));
            }
        }
        JSONArray jSONArray2 = new JSONArray();
        jSONArray2.put("type");
        jSONArray2.put("timestamp");
        jSONArray2.put("la");
        jSONArray2.put("lo");
        jSONArray2.put("duration");
        jSONArray2.put("distance");
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("sportId", (Object) this.sportId);
        httpParamsWithBandId.mo27196a("Dict", (Object) jSONArray2.toString());
        httpParamsWithBandId.mo27196a("Data", (Object) jSONArray.toString());
        return httpParamsWithBandId;
    }

    private JSONArray getItem(int i, long j, double d, double d2, int i2, int i3) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(i);
        jSONArray.put(j);
        try {
            jSONArray.put(d);
            jSONArray.put(d2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jSONArray.put(i2);
        jSONArray.put(i3);
        return jSONArray;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postSportTrace";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
        int i = this.requestTimes;
        if (i < 3) {
            this.requestTimes = i + 1;
            request();
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        C4222l.m13459a(cVar.toString(), "FsLog", "sport.log");
        JSONArray optJSONArray = cVar.mo27203c().optJSONArray("Data");
        if (optJSONArray != null) {
            String l = MemberControl.m17125s().mo27187l();
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                SportTraceInfo fVar2 = new SportTraceInfo();
                fVar2.f6289a = l;
                fVar2.f6290b = this.sportId;
                fVar2.f6291c = optJSONArray.optInt(0);
                fVar2.f6292d = optJSONArray.optLong(1);
                fVar2.f6293e = optJSONArray.optDouble(2);
                fVar2.f6294f = optJSONArray.optDouble(3);
                fVar2.f6295g = optJSONArray.optInt(4);
                fVar2.f6296h = optJSONArray.optInt(5);
                fVar2.f6297i = true;
                SportTraceDataDb.m13208a(fVar2);
            }
        }
        this.requestTimes = 0;
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request() {
        return super.request(getHttpParams(), this.listener);
    }

    public HvApiResponse requestSync() {
        return super.requestSync(getHttpParams());
    }
}
