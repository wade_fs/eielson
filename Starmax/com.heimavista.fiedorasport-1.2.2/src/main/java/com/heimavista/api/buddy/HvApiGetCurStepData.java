package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONObject;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetCurStepData extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getCurStepData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        JSONObject optJSONObject = fVar.mo25334d().optJSONObject("Data");
        if (optJSONObject != null) {
            StepInfo stepInfo = new StepInfo();
            stepInfo.f6242P = cVar.mo27198a("userNbr", "");
            stepInfo.f6243Q = optJSONObject.optLong("timestamp");
            stepInfo.f6244R = optJSONObject.optString("timezone");
            stepInfo.f6245S = optJSONObject.optInt("step");
            stepInfo.f6246T = optJSONObject.optInt("distance");
            stepInfo.f6247U = optJSONObject.optInt("cal");
            CurStepDataDb.m13135a(stepInfo);
            TickManager.m10526c(getTickKey(), optJSONObject.optLong("timestamp"));
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(String str) {
        return super.request(getHttpParamsWithUser(str));
    }

    public HvApiResponse requestSync(String str) {
        return super.requestSync(getHttpParamsWithUser(str));
    }
}
