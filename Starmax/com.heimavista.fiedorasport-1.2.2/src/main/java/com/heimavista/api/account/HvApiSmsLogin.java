package com.heimavista.api.account;

import android.text.TextUtils;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.MapUtil;
import com.heimavista.utils.ParamJsonData;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSmsLogin extends HvApiBase {
    private OnResultListener<Void> listener;

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"memMobile"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "smsLogin";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(MemberControl.f11154f);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = fVar.mo25325a("loginInfo", "");
        if (TextUtils.isEmpty(a)) {
            onApiFailed(cVar, cVar2, "loginInfo is empty", fVar);
            return;
        }
        String decrypt = decrypt(a, str);
        Map<String, Object> c = new ParamJsonData(fVar.mo25325a("MemInfo", "{}"), true).mo25333c();
        String[] f = MemberControl.m17125s().mo27181f();
        for (String str2 : f) {
            String a2 = MapUtil.m15243a(c, str2, (String) null);
            if (!TextUtils.isEmpty(a2)) {
                c.put(str2, decrypt(a2, str));
            }
        }
        MemberControl.m17125s().mo27177c(decrypt);
        MemberControl.m17125s().mo27179d(MapUtil.m15245a(c).toString());
        MemberControl.m17125s().mo27175b(fVar.mo25325a("ExtraInfo", "{}"));
        HvApp.m13010c().mo24157a(MemberControl.f11153e);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, String str2) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memMobile", (Object) str);
        cVar.mo27196a("smsCode", (Object) str2);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, String str2, OnResultListener<Void> dVar) {
        this.listener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memMobile", (Object) str);
        cVar.mo27196a("smsCode", (Object) str2);
        return request(cVar);
    }
}
