package com.heimavista.api.band;

import com.heimavista.entity.band.StepInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetStepData extends HvApiGetDataBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getStepData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                StepInfo stepInfo = new StepInfo();
                stepInfo.f6242P = MemberControl.m17125s().mo27187l();
                stepInfo.f6243Q = jSONArray.optLong(a2.indexOf("timestamp"));
                stepInfo.f6244R = jSONArray.optString(a2.indexOf("timezone"));
                stepInfo.f6247U = jSONArray.optInt(a2.indexOf("cal"));
                stepInfo.f6246T = jSONArray.optInt(a2.indexOf("distance"));
                stepInfo.f6248V = jSONArray.optInt(a2.indexOf("stepTime"));
                stepInfo.f6245S = jSONArray.optInt(a2.indexOf("step"));
                stepInfo.f6250X = true;
                StepDataDb.m13218a(stepInfo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
