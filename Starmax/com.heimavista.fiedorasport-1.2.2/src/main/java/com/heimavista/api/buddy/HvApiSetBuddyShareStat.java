package com.heimavista.api.buddy;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSetBuddyShareStat extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "setBuddyShareStat";
    }

    public HttpCancelable request(String str, boolean z, boolean z2, boolean z3, boolean z4, OnResultListener<ParamJsonData> dVar) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("isShareStep", Integer.valueOf(z ? 1 : 0));
        httpParamsWithUser.mo27196a("isShareSleep", Integer.valueOf(z2 ? 1 : 0));
        httpParamsWithUser.mo27196a("isShareHeart", Integer.valueOf(z3 ? 1 : 0));
        httpParamsWithUser.mo27196a("isShareLoc", Integer.valueOf(z4 ? 1 : 0));
        return super.request(httpParamsWithUser, dVar);
    }
}
