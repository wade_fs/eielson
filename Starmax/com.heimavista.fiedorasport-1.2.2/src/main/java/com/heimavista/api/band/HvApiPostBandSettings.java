package com.heimavista.api.band;

import android.text.TextUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.utils.ParamJsonData;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostBandSettings extends HvApiBaseBand {
    private OnResultListener<ParamJsonData> listener;

    public HvApiPostBandSettings(OnResultListener<ParamJsonData> dVar) {
        this.listener = dVar;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postBandSettings";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(int i, boolean z) {
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("enable", z ? 1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request(i, jSONObject.toString());
    }

    public HttpCancelable requestPostAlarms() {
        JSONArray jSONArray = new JSONArray();
        ArrayList arrayList = new ArrayList();
        int i = FSManager.m10323r().mo22847q() ? 5 : 3;
        for (int i2 = 0; i2 < i; i2++) {
            AlarmInfoItem a = BandDataManager.m10547a(i2);
            arrayList.add(a);
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("id", a.mo26354a());
                jSONObject.put("type", a.mo26373i());
                jSONObject.put("hour", a.mo26377k());
                jSONObject.put("minute", a.mo26378l());
                jSONObject.put("monday", a.mo26362d());
                jSONObject.put("tuesday", a.mo26371h());
                jSONObject.put("wedensday", a.mo26375j());
                jSONObject.put("thursday", a.mo26371h());
                jSONObject.put("friday", a.mo26360c());
                jSONObject.put("saturday", a.mo26365e());
                jSONObject.put("sunday", a.mo26367f());
                jSONObject.put("once", a.mo26379m() ? 1 : 0);
                jSONObject.put("content", a.mo26358b());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jSONArray.put(jSONObject);
        }
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22883a(arrayList);
        }
        return request(4, jSONArray.toString());
    }

    public HttpCancelable requestPostDeviceProfile() {
        DeviceProfile b = FSManager.m10323r().mo22847q() ? BandDataManager.m10556b() : null;
        if (FSManager.m10323r().mo22846p()) {
            PeriodTimeInfo b2 = BandDataManager.m10555b("QuickView");
            PeriodTimeInfo b3 = BandDataManager.m10555b("DoNotDisturb");
            b = new DeviceProfile(b2.mo22593g(), false, b3.mo22593g(), b3.mo22585c(), b3.mo22580a(), b3.mo22587d(), b3.mo22583b());
        }
        if (b == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            int i = 1;
            jSONObject.put("riseHand", b.mo26407e() ? 1 : 0);
            jSONObject.put("vibrate", b.mo26409g() ? 1 : 0);
            if (!b.mo26408f()) {
                i = 0;
            }
            jSONObject.put("noDisturb", i);
            jSONObject.put("startHour", b.mo26401c());
            jSONObject.put("startMinte", b.mo26404d());
            jSONObject.put("endHour", b.mo26395a());
            jSONObject.put("endMinute", b.mo26398b());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request(7, jSONObject.toString());
    }

    public HttpCancelable requestPostNotification() {
        JSONObject jSONObject = new JSONObject();
        try {
            RemindMsgInfo e = BandDataManager.m10563e();
            int i = 1;
            jSONObject.put(ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE, e.mo22616i() ? 1 : 0);
            jSONObject.put("qq", e.mo22608e() ? 1 : 0);
            jSONObject.put("facebook", e.mo22601b() ? 1 : 0);
            jSONObject.put("skype", e.mo22610f() ? 1 : 0);
            jSONObject.put("twitter", e.mo22614h() ? 1 : 0);
            jSONObject.put("whatsapp", e.mo22616i() ? 1 : 0);
            if (!e.mo22603c()) {
                i = 0;
            }
            jSONObject.put("line", i);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return request(3, jSONObject.toString());
    }

    public HttpCancelable requestPostSedentary() {
        JSONObject jSONObject = new JSONObject();
        try {
            PeriodTimeInfo b = BandDataManager.m10555b("Sedentary");
            jSONObject.put("time", b.mo22590e());
            jSONObject.put("startHour", b.mo22585c());
            jSONObject.put("startMinute", b.mo22587d());
            jSONObject.put("endHour", b.mo22580a());
            jSONObject.put("endMinute", b.mo22583b());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request(5, jSONObject.toString());
    }

    public HttpCancelable requestPostStepGoal(int i) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("stepGoal", i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request(6, jSONObject.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(int i, String str) {
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("type", Integer.valueOf(i));
        httpParamsWithBandId.mo27196a("Data", (Object) str);
        return super.request(httpParamsWithBandId, this.listener);
    }
}
