package com.heimavista.api.sport;

import com.heimavista.api.HvApiBasic;
import com.heimavista.api.HvApiResponse;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p196k.SportLikeUserDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiMyLikeList extends HvApiBasic {
    private HttpParams getHttpParams() {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("lastTime", Long.valueOf(TickManager.m10523b(getTickKey())));
        return cVar;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "sport";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "myLikeList";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SportLikeUserDb.m13372a(jSONArray.optString(a2.indexOf("sportId")), MemberControl.m17125s().mo27187l(), jSONArray.optLong(a2.indexOf("timestamp")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request() {
        return super.request(getHttpParams());
    }

    public HvApiResponse requestSync() {
        return super.requestSync(getHttpParams());
    }
}
