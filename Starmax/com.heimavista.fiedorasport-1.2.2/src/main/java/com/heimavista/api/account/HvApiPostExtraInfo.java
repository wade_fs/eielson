package com.heimavista.api.account;

import com.heimavista.api.HvApiBase;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONObject;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostExtraInfo extends HvApiBase {
    private OnResultListener<Void> listener;

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postExtraInfo";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        MemberControl.m17125s().mo27175b(cVar.mo27201b("Data").toString());
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable postExtraInfo(JSONObject jSONObject, OnResultListener<Void> dVar) {
        if (jSONObject == null) {
            return null;
        }
        this.listener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("Data", (Object) jSONObject.toString());
        return super.request(cVar);
    }
}
