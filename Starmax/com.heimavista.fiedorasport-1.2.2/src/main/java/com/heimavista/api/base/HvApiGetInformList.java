package com.heimavista.api.base;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.facebook.internal.NativeProtocol;
import com.heimavista.api.HvApi;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import java.util.Map;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetInformList extends HvApiBase {
    private static boolean isRunning = false;
    private OnResultListener<String> onResultListener;
    private C3656a progressCallback;

    /* renamed from: com.heimavista.api.base.HvApiGetInformList$a */
    public interface C3656a {
        /* renamed from: a */
        void mo22483a(int i);

        /* renamed from: a */
        void mo22484a(boolean z, HttpParams cVar, HvApiResponse... cVarArr);
    }

    /* renamed from: a */
    static /* synthetic */ void m10011a(C3656a aVar) {
        HvApiGetInformList hvApiGetInformList = new HvApiGetInformList();
        hvApiGetInformList.setProgressCallback(aVar);
        hvApiGetInformList.requestSync();
        isRunning = false;
    }

    private int getTick() {
        return SPUtils.m1243c("BaseGetInformList").mo9869a("tick", 0);
    }

    private HttpCancelable request(OnResultListener<String> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("tick", Integer.valueOf(getTick()));
        return request(cVar);
    }

    private void requestFinished(HttpParams cVar, ParamJsonData fVar) {
        C3656a aVar = this.progressCallback;
        if (aVar != null) {
            aVar.mo22483a(100);
            HvApiResponse cVar2 = new HvApiResponse();
            cVar2.mo22508a(fVar);
            this.progressCallback.mo22484a(true, cVar, cVar2);
        }
    }

    public static void requestSingle() {
        requestSingle(null);
    }

    private HvApiResponse requestSync() {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("tick", Integer.valueOf(getTick()));
        return requestSync(cVar);
    }

    public static boolean runInform(String str, String str2, Map<String, Object> map) {
        try {
            String str3 = "com.heimavista.api." + str + ".HvApi" + StringUtils.m941a(str2);
            LogUtils.m1139c(str3);
            HvApi aVar = (HvApi) Class.forName(str3).newInstance();
            aVar.setHandleNewInform(false);
            HttpParams cVar = new HttpParams();
            cVar.mo27197a(map);
            if (aVar.requestSync(cVar).mo22515d() == 1) {
                return true;
            }
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (InstantiationException e2) {
            e2.printStackTrace();
            return false;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    private void saveTick(int i) {
        SPUtils.m1243c("BaseGetInformList").mo9881b("tick", i);
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "base";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getInformList";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        if (this.progressCallback != null) {
            HvApiResponse cVar3 = new HvApiResponse();
            cVar3.mo22509a(str);
            this.progressCallback.mo22484a(false, cVar, cVar3);
            return;
        }
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        int intValue = fVar.mo25323a("tick", (Integer) 0).intValue();
        boolean z = true;
        LogUtils.m1139c(fVar.mo25325a("Rows", "[]"));
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        int size = a.size();
        if (size == 0) {
            requestFinished(cVar, fVar);
        } else {
            int i = 0;
            for (Object obj : a) {
                ParamJsonData fVar2 = new ParamJsonData(String.valueOf(obj));
                if (!runInform(fVar2.mo25325a("fun", (String) null), fVar2.mo25325a("op", (String) null), fVar2.mo25329a(NativeProtocol.WEB_DIALOG_PARAMS))) {
                    z = false;
                }
                C3656a aVar = this.progressCallback;
                if (aVar != null) {
                    i++;
                    int i2 = (i * 100) / size;
                    aVar.mo22483a(i2);
                    if (i2 == 100) {
                        requestFinished(cVar, fVar);
                    }
                }
            }
        }
        if (z) {
            saveTick(intValue);
        }
        OnResultListener<String> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(fVar.mo25332b());
        }
    }

    public void setProgressCallback(C3656a aVar) {
        this.progressCallback = aVar;
    }

    public static void requestSingle(C3656a aVar) {
        if (!isRunning) {
            isRunning = true;
            new Thread(new C3657a(aVar)).start();
        }
    }
}
