package com.heimavista.api.band;

import android.text.TextUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostSportData extends HvApiBaseBand {
    public static final String actionFail;
    public static final String actionSuccess;
    private OnResultListener<ParamJsonData> listener;
    private SportInfo sportData;

    static {
        Class<HvApiPostSportData> cls = HvApiPostSportData.class;
        actionSuccess = cls.getName() + ".success";
        actionFail = cls.getName() + ".fail";
    }

    public HvApiPostSportData(SportInfo dVar) {
        this.sportData = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getPostParams() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", this.sportData.f6273d);
            jSONObject.put("timezone", this.sportData.f6271b);
            jSONObject.put(TtmlNode.START, this.sportData.f6274e);
            jSONObject.put(TtmlNode.END, this.sportData.f6275f);
            jSONObject.put("duration", this.sportData.f6276g);
            jSONObject.put("distance", this.sportData.f6277h);
            jSONObject.put("cal", this.sportData.f6278i);
            jSONObject.put("avgHeartRate", this.sportData.f6279j);
            jSONObject.put("minHeartRate", this.sportData.f6280k);
            jSONObject.put("maxHeartRate", this.sportData.f6281l);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Data", (Object) jSONObject.toString());
        return httpParamsWithBandId;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postSportData";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
        HvApp.m13010c().mo24157a(actionFail);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        String a = fVar.mo25325a("sportId", "");
        if (!TextUtils.isEmpty(a)) {
            SportInfo dVar = this.sportData;
            dVar.f6272c = a;
            SportDataDb.m13191a(dVar);
            HvApp.m13010c().mo24157a(actionSuccess);
        }
    }

    public HttpCancelable request() {
        return super.request(getPostParams(), this.listener);
    }

    public HvApiPostSportData(SportInfo dVar, OnResultListener<ParamJsonData> dVar2) {
        this.sportData = dVar;
        this.listener = dVar2;
    }
}
