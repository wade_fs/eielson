package com.heimavista.api.sport;

import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p193d.p196k.SportLikeUserDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetLikeCnt extends HvApiBasic {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getHttpParams(String str) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("lastTime", Long.valueOf(getTick()));
        cVar.mo27196a("userNbr", (Object) str);
        return cVar;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "sport";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getLikeCnt";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        LogUtils.m1139c("op=" + getOp() + " onApiSuccess");
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                String a3 = cVar.mo27198a("userNbr", "");
                String optString = jSONArray.optString(a2.indexOf("sportId"));
                int optInt = jSONArray.optInt(a2.indexOf("cnt"));
                long optLong = jSONArray.optLong(a2.indexOf("timestamp"));
                SportDataDb.m13192a(a3, optString, optInt, optLong);
                SportLikeUserDb.m13372a(optString, MemberControl.m17125s().mo27187l(), optLong);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(String str) {
        return super.request(getHttpParams(str));
    }

    public HttpCancelable request(String str, OnResultListener<ParamJsonData> dVar) {
        return super.request(getHttpParams(str), dVar);
    }
}
