package com.heimavista.api.buddy;

import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSetRemark extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "setRemark";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("userNbr", "");
        String a2 = cVar.mo27198a("remark", "");
        LogUtils.m1139c("op=setRemark " + a + " " + a2);
        BuddyListDb.m13248b(a, a2);
        UserInfoDb.m13342b(a, a2);
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, String str2, OnResultListener<ParamJsonData> dVar) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("remark", (Object) str2);
        return super.request(httpParamsWithUser, dVar);
    }
}
