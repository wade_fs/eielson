package com.heimavista.api.account;

import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.MapUtil;
import com.heimavista.utils.ParamJsonData;
import java.io.File;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiUpdProfile extends HvApiBase {
    public static final String ACTION_FAILED;
    public static final String ACTION_SUCCESS;
    private OnResultListener<Void> listener;

    static {
        Class<HvApiUpdProfile> cls = HvApiUpdProfile.class;
        ACTION_SUCCESS = cls.getName() + ".SUCCESS";
        ACTION_FAILED = cls.getName() + ".FAILED";
    }

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"memName", "memRealName"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "updProfile";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(ACTION_FAILED);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        Map<String, Object> c = new ParamJsonData(fVar.mo25325a("MemInfo", "{}"), true).mo25333c();
        String[] f = MemberControl.m17125s().mo27181f();
        for (String str2 : f) {
            String a = MapUtil.m15243a(c, str2, (String) null);
            if (!TextUtils.isEmpty(a)) {
                c.put(str2, decrypt(a, str));
            }
        }
        MemberControl.m17125s().mo27179d(MapUtil.m15245a(c).toString());
        HvApp.m13010c().mo24157a(ACTION_SUCCESS);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    public void setListener(OnResultListener<Void> dVar) {
        this.listener = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable updMemBirthYm(String str) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memBirthYm", (Object) str);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable updMemGender(String str) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memGender", (Object) str);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable updMemName(String str) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memName", (Object) str);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable updMemRealName(String str) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memRealName", (Object) str);
        return request(cVar);
    }

    public HttpCancelable updPhoto(File file) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_PHOTO, file);
        return request(cVar);
    }
}
