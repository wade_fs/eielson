package com.heimavista.api.start;

import androidx.annotation.NonNull;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.HvApiStart;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiRegister extends HvApiStart {
    public static final String DEV_TYPE_ANDROID = "android";
    public static final String DEV_TYPE_ANDROID_FCM = "android_fcm";
    public static final String DEV_TYPE_ANDROID_JPUSH = "android_jpush";
    public static final String DEV_TYPE_IOS = "ios";
    private OnResultListener<String> onResultListener;

    /* renamed from: com.heimavista.api.start.HvApiRegister$a */
    class C3660a implements OnResultListener<String> {

        /* renamed from: a */
        final /* synthetic */ C3661b f6221a;

        /* renamed from: b */
        final /* synthetic */ String f6222b;

        /* renamed from: c */
        final /* synthetic */ String f6223c;

        C3660a(C3661b bVar, String str, String str2) {
            this.f6221a = bVar;
            this.f6222b = str;
            this.f6223c = str2;
        }

        /* renamed from: a */
        public void mo22380a(String str) {
            this.f6221a.f6225a = HvApiRegister.this.request(str, this.f6222b, this.f6223c);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            HvApiRegister.this.onApiFailed(cVar, cVar2, str, fVar);
        }
    }

    /* renamed from: com.heimavista.api.start.HvApiRegister$b */
    private static class C3661b implements HttpCancelable {

        /* renamed from: a */
        HttpCancelable f6225a;

        private C3661b() {
        }

        public void cancel() {
            this.f6225a.cancel();
        }

        /* synthetic */ C3661b(C3660a aVar) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"devCode"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return TtmlNode.START;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "register";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<String> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = fVar.mo25325a("devNbr", "");
        HvApiBase.saveDevNbr(a);
        OnResultListener<String> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("devCode", (Object) HvApp.m13010c().mo24154a());
        cVar.mo27196a("devType", (Object) str2);
        cVar.mo27196a("apnToken", (Object) str3);
        cVar.mo27196a("token", (Object) str);
        return request(cVar);
    }

    public HvApiResponse requestSync() {
        HvApiResponse requestSync = new HvApiToken().requestSync();
        if (requestSync.mo22515d() != 1) {
            return requestSync;
        }
        return requestSync(requestSync.mo22506a().mo25325a("token", ""), "android", "");
    }

    public HttpCancelable requestWithListener(OnResultListener<String> dVar) {
        return requestWithListener("android", "", dVar);
    }

    public HttpCancelable requestWithListener(String str, String str2, OnResultListener<String> dVar) {
        this.onResultListener = dVar;
        C3661b bVar = new C3661b(null);
        bVar.f6225a = new HvApiToken().requestWithListener(new C3660a(bVar, str, str2));
        return bVar;
    }

    public HvApiResponse requestSync(String str, String str2) {
        HvApiResponse requestSync = new HvApiToken().requestSync();
        if (requestSync.mo22515d() != 1) {
            return requestSync;
        }
        return requestSync(requestSync.mo22506a().mo25325a("token", ""), str, str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HvApiResponse requestSync(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("devCode", (Object) HvApp.m13010c().mo24154a());
        cVar.mo27196a("devType", (Object) str2);
        cVar.mo27196a("apnToken", (Object) str3);
        cVar.mo27196a("token", (Object) str);
        return requestSync(cVar);
    }
}
