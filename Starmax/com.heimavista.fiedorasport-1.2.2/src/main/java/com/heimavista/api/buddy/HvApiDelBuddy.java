package com.heimavista.api.buddy;

import android.text.TextUtils;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpParams;

public class HvApiDelBuddy extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "delBuddy";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("userNbr", "");
        if (!TextUtils.isEmpty(a)) {
            BuddyListDb.m13249k(a);
            new UserInfoDb().mo24331k(a);
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
