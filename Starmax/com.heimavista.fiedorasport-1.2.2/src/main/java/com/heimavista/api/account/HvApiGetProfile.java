package com.heimavista.api.account;

import android.text.TextUtils;
import com.heimavista.api.HvApiBase;
import com.heimavista.utils.MapUtil;
import com.heimavista.utils.ParamJsonData;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetProfile extends HvApiBase {
    public static final String ACTION_FAILED = (HvApiGetProfile.class.getName() + ".FAILED");

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getProfile";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(ACTION_FAILED);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        Map<String, Object> c = new ParamJsonData(fVar.mo25325a("MemInfo", "{}"), true).mo25333c();
        String[] f = MemberControl.m17125s().mo27181f();
        for (String str2 : f) {
            String a = MapUtil.m15243a(c, str2, (String) null);
            if (!TextUtils.isEmpty(a)) {
                c.put(str2, decrypt(a, str));
            }
        }
        MemberControl.m17125s().mo27179d(MapUtil.m15245a(c).toString());
        MemberControl.m17125s().mo27175b(fVar.mo25325a("ExtraInfo", "{}"));
        HvApp.m13010c().mo24157a(MemberControl.f11155g);
    }

    public HttpCancelable request() {
        return request(null);
    }
}
