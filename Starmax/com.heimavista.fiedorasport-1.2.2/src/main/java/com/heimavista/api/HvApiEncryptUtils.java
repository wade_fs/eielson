package com.heimavista.api;

import android.text.TextUtils;
import android.util.Base64;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.utils.RandomUtils;
import java.util.Arrays;

/* renamed from: com.heimavista.api.b */
public class HvApiEncryptUtils {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* renamed from: a */
    public static String m10000a(String str, String str2) {
        String str3;
        String str4;
        if (str2.length() >= 8) {
            str3 = str2.substring(0, 8);
        } else {
            str3 = String.format("%-8s", str2).replace(' ', '0');
        }
        byte[] decode = Base64.decode(str, 2);
        try {
            str4 = new String(EncryptUtils.m1069b(decode, str2.getBytes(), "DES/CBC/PKCS5Padding", str3.getBytes()));
        } catch (Exception unused) {
            str4 = str;
        }
        LogUtils.m1139c("decryptDES", "data=" + str, "key=" + str2, "iv=" + str3, "bytes=" + Arrays.toString(decode), "result=" + str4);
        return str4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* renamed from: b */
    public static String m10003b(String str, String str2) {
        String str3;
        if (str2.length() >= 8) {
            str3 = str2.substring(0, 8);
        } else {
            str3 = String.format("%-8s", str2).replace(' ', '0');
        }
        byte[] e = EncryptUtils.m1074e(str.getBytes(), str2.getBytes(), "DES/CBC/PKCS5Padding", str3.getBytes());
        String encodeToString = Base64.encodeToString(e, 2);
        LogUtils.m1139c("encryptDES", "data=" + str, "key=" + str2, "iv=" + str3, "bytes=" + Arrays.toString(e), "base64Encode=" + encodeToString);
        return encodeToString;
    }

    /* renamed from: a */
    private static String m9998a() {
        String a = SPUtils.m1243c("company").mo9872a("name", "");
        if (!TextUtils.isEmpty(a)) {
            return a;
        }
        String b = RandomUtils.m15266b(16);
        SPUtils.m1243c("company").mo9883b("name", b);
        return b;
    }

    /* renamed from: b */
    private static String m10001b() {
        String a = SPUtils.m1243c("company").mo9872a("desc", "");
        if (!TextUtils.isEmpty(a)) {
            return a;
        }
        String b = RandomUtils.m15266b(16);
        SPUtils.m1243c("company").mo9883b("desc", b);
        return b;
    }

    /* renamed from: a */
    public static String m9999a(String str) {
        return (str == null || TextUtils.isEmpty(str)) ? "" : new String(EncryptUtils.m1063a(str, m9998a().getBytes(), "AES/CBC/PKCS5Padding", m10001b().getBytes()));
    }

    /* renamed from: b */
    public static String m10002b(String str) {
        return (str == null || TextUtils.isEmpty(str)) ? "" : EncryptUtils.m1073d(str.getBytes(), m9998a().getBytes(), "AES/CBC/PKCS5Padding", m10001b().getBytes());
    }
}
