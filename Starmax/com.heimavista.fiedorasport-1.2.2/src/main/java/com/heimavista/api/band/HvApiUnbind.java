package com.heimavista.api.band;

import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiUnbind extends HvApiBaseBand {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "unbind";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("deviceMacAddress", (Object) FSManager.m10323r().mo22842l());
        return super.request(httpParamsWithBandId, dVar);
    }
}
