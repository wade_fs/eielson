package com.heimavista.api.buddy;

import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiPostCurStepData;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiAcceptBuddy extends HvApiBaseBuddy {
    private boolean accept = false;

    /* access modifiers changed from: protected */
    public String getOp() {
        return "acceptBuddy";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        if (this.accept) {
            CurStepDataDb k = CurStepDataDb.m13136k(MemberControl.m17125s().mo27187l());
            if (k.mo24250x() > 0) {
                new HvApiPostCurStepData().request(k.mo24232o(), k.mo24250x(), k.mo24249w(), k.mo24248v());
            }
            LocManager.m10493g().mo22958c();
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, boolean z, OnResultListener<ParamJsonData> dVar) {
        this.accept = z;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("userNbr", (Object) str);
        cVar.mo27196a("buddyStat", Integer.valueOf(z ? 1 : 0));
        return super.request(cVar, dVar);
    }
}
