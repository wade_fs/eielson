package com.heimavista.api.buddy;

import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.utils.ParamJsonData;
import java.util.Calendar;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostLoc extends HvApiBaseBuddy {
    private Location location;

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postLoc";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (HvApp.m13010c().mo24159b()) {
            C4222l.m13459a("postLoc success:" + this.location.toString() + " speed=" + this.location.getSpeed(), "FsLog", "loc.log");
        }
        FSManager.m10323r().mo22835e().mo27032a("loc_data", this.location);
    }

    public HttpCancelable request(Location location2) {
        PeriodTimeInfo b = BandDataManager.m10555b("Location");
        if (b.mo22593g() && BuddyListDb.m13245F() != 0) {
            if (BandDataManager.m10564f() <= 0 || (!(b.mo22590e() == 1 || b.mo22590e() == 2) || Calendar.getInstance().getTimeInMillis() <= BandDataManager.m10564f())) {
                this.location = location2;
                HttpParams cVar = new HttpParams();
                cVar.mo27196a("la", Double.valueOf(location2.getLatitude()));
                cVar.mo27196a("lo", Double.valueOf(location2.getLongitude()));
                cVar.mo27196a("speed", Double.valueOf(((double) location2.getSpeed()) * 3.6d));
                Intent registerReceiver = HvApp.m13010c().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                if (registerReceiver != null) {
                    int intExtra = registerReceiver.getIntExtra("level", 0);
                    int intExtra2 = registerReceiver.getIntExtra("status", 0);
                    cVar.mo27196a("battery", Integer.valueOf(intExtra));
                    cVar.mo27196a("batteryStat", Integer.valueOf((intExtra2 == 2 || intExtra2 == 5) ? 1 : 0));
                    LogUtils.m1139c("op=postLoc " + cVar.mo27203c());
                    return super.request(cVar);
                }
            } else {
                b.mo22582a(false);
                BandDataManager.m10550a(b);
                return null;
            }
        }
        return null;
    }
}
