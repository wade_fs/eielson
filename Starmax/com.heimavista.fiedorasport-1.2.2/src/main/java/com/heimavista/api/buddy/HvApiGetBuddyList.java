package com.heimavista.api.buddy;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBuddyList extends HvApiBaseBuddy {
    /* access modifiers changed from: private */
    public static boolean isRunning = false;

    /* renamed from: com.heimavista.api.buddy.HvApiGetBuddyList$a */
    class C3658a extends ThreadUtils.C0877e<List<HvApiResponse>> {

        /* renamed from: U */
        final /* synthetic */ List f6212U;

        C3658a(List list) {
            this.f6212U = list;
        }

        /* renamed from: e */
        private void m10014e() {
            if (HvApiGetBuddyList.this.getListener() != null) {
                HvApiGetBuddyList.this.getListener().mo22380a(null);
            }
            boolean unused = HvApiGetBuddyList.isRunning = false;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo22495a((List<HvApiResponse>) ((List) obj));
        }

        /* renamed from: c */
        public void mo9799c() {
            m10014e();
        }

        /* renamed from: a */
        public void mo22495a(List<HvApiResponse> list) {
            m10014e();
        }

        /* renamed from: b */
        public List<HvApiResponse> mo9798b() {
            ArrayList arrayList = new ArrayList();
            for (C3659b bVar : this.f6212U) {
                arrayList.add(HvApiGetBuddyList.this.runByOp(bVar.mo22496a(), bVar.mo22497b()));
            }
            return arrayList;
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
            m10014e();
        }
    }

    /* renamed from: com.heimavista.api.buddy.HvApiGetBuddyList$b */
    class C3659b {

        /* renamed from: a */
        String f6214a;

        /* renamed from: b */
        String f6215b;

        C3659b(HvApiGetBuddyList hvApiGetBuddyList, String str, String str2, String str3) {
            this.f6214a = str;
            this.f6215b = str3;
        }

        /* renamed from: a */
        public String mo22496a() {
            return this.f6215b;
        }

        /* renamed from: b */
        public String mo22497b() {
            return this.f6214a;
        }
    }

    private void requestSingle(List<C3659b> list) {
        if (list == null || list.isEmpty()) {
            if (getListener() != null) {
                getListener().mo22380a(null);
            }
        } else if (!isRunning) {
            isRunning = true;
            ThreadUtils.m959b(new C3658a(list));
        }
    }

    /* access modifiers changed from: private */
    public HvApiResponse runByOp(String str, String str2) {
        try {
            HvApiBaseBuddy hvApiBaseBuddy = (HvApiBaseBuddy) Class.forName("com.heimavista.api." + getFun() + ".HvApi" + StringUtils.m941a(str)).newInstance();
            hvApiBaseBuddy.setHandleNewInform(false);
            super.setUserNbr(str2);
            return super.requestSync(str2);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (InstantiationException e2) {
            e2.printStackTrace();
            return null;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBuddyList";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        ArrayList arrayList;
        Iterator<Object> it;
        JSONArray jSONArray;
        String str2;
        String str3;
        ParamJsonData fVar2 = fVar;
        List<Object> a = new ParamJsonData(fVar2.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar2.mo25325a("Dict", "[]"), true).mo25327a();
        Iterator<Object> it2 = a.iterator();
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        while (it2.hasNext()) {
            Object next = it2.next();
            if (arrayList2 == null) {
                arrayList2 = new ArrayList();
            }
            ArrayList arrayList4 = arrayList2;
            BuddyListDb aVar = new BuddyListDb();
            String l = MemberControl.m17125s().mo27187l();
            aVar.mo24300h(l);
            try {
                JSONArray jSONArray2 = new JSONArray(String.valueOf(next));
                String optString = jSONArray2.optString(a2.indexOf("userNbr"));
                BuddyListDb l2 = BuddyListDb.m13250l(optString);
                if (!l.equals(optString)) {
                    if (!TextUtils.isEmpty(optString)) {
                        int optInt = jSONArray2.optInt(a2.indexOf("userStat"));
                        long optLong = jSONArray2.optLong(a2.indexOf("timestamp"));
                        long optLong2 = jSONArray2.optLong(a2.indexOf("userTimestamp"));
                        String str4 = optString;
                        long optLong3 = jSONArray2.optLong(a2.indexOf("curStepTimestamp"));
                        long j = optLong;
                        long optLong4 = jSONArray2.optLong(a2.indexOf("stepTimestamp"));
                        long optLong5 = jSONArray2.optLong(a2.indexOf("sleepTimestamp"));
                        long optLong6 = jSONArray2.optLong(a2.indexOf("heartTimestamp"));
                        long optLong7 = jSONArray2.optLong(a2.indexOf("sportTimestamp"));
                        long optLong8 = jSONArray2.optLong(a2.indexOf("sportLikeTimestamp"));
                        String optString2 = jSONArray2.optString(a2.indexOf("remark"));
                        it = it2;
                        if (optInt == 9) {
                            try {
                                if (l2.mo24313z() < optLong2) {
                                    arrayList = arrayList3;
                                    try {
                                        str2 = optString2;
                                        jSONArray = jSONArray2;
                                        str3 = str4;
                                        arrayList4.add(new C3659b(this, str3, getFun(), "getUserInfo"));
                                    } catch (JSONException e) {
                                        e = e;
                                        LogUtils.m1139c("mady buddy " + e.getMessage());
                                        arrayList3 = arrayList;
                                        arrayList2 = arrayList4;
                                        it2 = it;
                                    }
                                } else {
                                    arrayList = arrayList3;
                                    jSONArray = jSONArray2;
                                    str2 = optString2;
                                    str3 = str4;
                                }
                                if (l2.mo24232o() < optLong3) {
                                    arrayList4.add(new C3659b(this, str3, getFun(), "getCurStepData"));
                                }
                                if (l2.mo24309v() < optLong4) {
                                    arrayList4.add(new C3659b(this, str3, getFun(), "getStepData"));
                                }
                                if (l2.mo24307t() < optLong5) {
                                    arrayList4.add(new C3659b(this, str3, getFun(), "getSleepData"));
                                }
                                if (l2.mo24303p() < optLong6) {
                                    arrayList4.add(new C3659b(this, str3, getFun(), "getHeartData"));
                                }
                                if (l2.mo24308u() < optLong7) {
                                    arrayList4.add(new C3659b(this, str3, getFun(), "getSportData"));
                                }
                            } catch (JSONException e2) {
                                e = e2;
                                arrayList = arrayList3;
                                LogUtils.m1139c("mady buddy " + e.getMessage());
                                arrayList3 = arrayList;
                                arrayList2 = arrayList4;
                                it2 = it;
                            }
                        } else {
                            arrayList = arrayList3;
                            jSONArray = jSONArray2;
                            str2 = optString2;
                            str3 = str4;
                        }
                        aVar.mo24302j(str3);
                        aVar.mo24295e(optInt);
                        aVar.mo24298g(j);
                        aVar.mo24299h(optLong2);
                        aVar.mo24288a(optLong3);
                        aVar.mo24297f(optLong4);
                        aVar.mo24292c(optLong5);
                        aVar.mo24290b(optLong6);
                        aVar.mo24296e(optLong7);
                        aVar.mo24294d(optLong8);
                        JSONArray jSONArray3 = jSONArray;
                        aVar.mo24293d(jSONArray3.optInt(a2.indexOf("isShareStep")));
                        aVar.mo24291c(jSONArray3.optInt(a2.indexOf("isShareSleep")));
                        aVar.mo24287a(jSONArray3.optInt(a2.indexOf("isShareHeart")));
                        aVar.mo24289b(jSONArray3.optInt(a2.indexOf("isShareLoc")));
                        aVar.mo24301i(str2);
                        arrayList3 = arrayList == null ? new ArrayList() : arrayList;
                        arrayList3.add(aVar);
                        arrayList2 = arrayList4;
                        it2 = it;
                    }
                }
                arrayList = arrayList3;
                arrayList2 = arrayList4;
                it2 = it2;
                arrayList3 = arrayList;
            } catch (JSONException e3) {
                e = e3;
                it = it2;
                arrayList = arrayList3;
                LogUtils.m1139c("mady buddy " + e.getMessage());
                arrayList3 = arrayList;
                arrayList2 = arrayList4;
                it2 = it;
            }
        }
        ArrayList<BuddyListDb> arrayList5 = arrayList3;
        BuddyListDb.m13244E();
        if (arrayList5 != null) {
            for (BuddyListDb aVar2 : arrayList5) {
                aVar2.mo24181l();
            }
        }
        requestSingle(arrayList2);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        return super.request((HttpParams) null, dVar);
    }
}
