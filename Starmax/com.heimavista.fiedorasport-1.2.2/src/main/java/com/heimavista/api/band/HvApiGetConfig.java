package com.heimavista.api.band;

import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONObject;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetConfig extends HvApiBasic {
    public static final String ACTION_SUCCESS = (HvApiGetConfig.class.getName() + ".ACTION_SUCCESS");

    /* access modifiers changed from: protected */
    public String getFun() {
        return "band";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getConfig";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        JSONObject optJSONObject = fVar.mo25334d().optJSONObject("BandConfig");
        if (optJSONObject != null) {
            BandConfig.m10064m().mo22556a(optJSONObject.toString());
        }
        HvApp.m13010c().mo24157a(ACTION_SUCCESS);
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        return super.request(new HttpParams(), dVar);
    }
}
