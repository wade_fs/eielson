package com.heimavista.api.account;

import com.heimavista.api.HvApiBase;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSignout extends HvApiBase {
    public static final String ACTION_FAILED = (HvApiSignout.class.getName() + ".FAILED");

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "signout";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(ACTION_FAILED);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        MemberControl.m17125s().mo27193r();
    }

    public HttpCancelable request() {
        return request(null);
    }
}
