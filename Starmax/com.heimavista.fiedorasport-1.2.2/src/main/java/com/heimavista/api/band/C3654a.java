package com.heimavista.api.band;

import com.heimavista.api.band.HvApiGetBandDataTask;
import java.util.List;

/* renamed from: com.heimavista.api.band.a */
/* compiled from: lambda */
public final /* synthetic */ class C3654a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ HvApiGetBandDataTask f6207P;

    /* renamed from: Q */
    private final /* synthetic */ List f6208Q;

    /* renamed from: R */
    private final /* synthetic */ HvApiGetBandDataTask.C3655a f6209R;

    public /* synthetic */ C3654a(HvApiGetBandDataTask bVar, List list, HvApiGetBandDataTask.C3655a aVar) {
        this.f6207P = bVar;
        this.f6208Q = list;
        this.f6209R = aVar;
    }

    public final void run() {
        this.f6207P.mo22478a(this.f6208Q, this.f6209R);
    }
}
