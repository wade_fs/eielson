package com.heimavista.api.account;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.MapUtil;
import com.heimavista.utils.ParamJsonData;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSocialRegister extends HvApiBase {
    private OnResultListener<Void> listener;

    /* renamed from: com.heimavista.api.account.HvApiSocialRegister$a */
    public static class C3652a {

        /* renamed from: a */
        private HttpParams f6205a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        public C3652a(@NonNull String str, @NonNull String str2) {
            HttpParams cVar = new HttpParams();
            cVar.mo27196a("memUserid", (Object) str);
            cVar.mo27196a("memName", (Object) str2);
            this.f6205a = cVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: a */
        public C3652a mo22427a(String str) {
            this.f6205a.mo27196a("memGender", (Object) str);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: b */
        public C3652a mo22429b(String str) {
            this.f6205a.mo27196a("memPhotoUrl", (Object) str);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: c */
        public C3652a mo22430c(String str) {
            this.f6205a.mo27196a("unionId", (Object) str);
            return this;
        }

        /* renamed from: a */
        public HttpParams mo22428a() {
            return this.f6205a;
        }
    }

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"memUserid", "memName", "unionId"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "socialRegister";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(MemberControl.f11154f);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = fVar.mo25325a("loginInfo", "");
        if (TextUtils.isEmpty(a)) {
            onApiFailed(cVar, cVar2, "loginInfo is empty", fVar);
            return;
        }
        String decrypt = decrypt(a, str);
        Map<String, Object> c = new ParamJsonData(fVar.mo25325a("MemInfo", "{}"), true).mo25333c();
        String[] f = MemberControl.m17125s().mo27181f();
        for (String str2 : f) {
            String a2 = MapUtil.m15243a(c, str2, (String) null);
            if (!TextUtils.isEmpty(a2)) {
                c.put(str2, decrypt(a2, str));
            }
        }
        MemberControl.m17125s().mo27177c(decrypt);
        MemberControl.m17125s().mo27179d(MapUtil.m15245a(c).toString());
        MemberControl.m17125s().mo27175b(fVar.mo25325a("ExtraInfo", "{}"));
        HvApp.m13010c().mo24157a(MemberControl.f11153e);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    public HttpCancelable request(C3652a aVar) {
        return super.request(aVar.mo22428a());
    }

    public HvApiResponse requestSync(C3652a aVar) {
        return super.requestSync(aVar.mo22428a());
    }

    public HttpCancelable request(C3652a aVar, OnResultListener<Void> dVar) {
        this.listener = dVar;
        return super.request(aVar.mo22428a());
    }

    public HvApiResponse requestSync(C3652a aVar, OnResultListener<Void> dVar) {
        this.listener = dVar;
        return super.requestSync(aVar.mo22428a());
    }
}
