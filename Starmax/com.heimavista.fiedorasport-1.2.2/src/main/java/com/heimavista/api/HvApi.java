package com.heimavista.api;

import android.text.TextUtils;
import androidx.core.app.NotificationCompat;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.facebook.internal.ServerProtocol;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.utils.RandomUtils;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCallback;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;
import p119e.p189e.p219h.HttpRequest;
import p119e.p189e.p219h.HttpRequestConfig;
import p119e.p189e.p219h.HttpResponse;

/* renamed from: com.heimavista.api.a */
public abstract class HvApi {
    public static final int METHOD_GET = 1;
    public static final int METHOD_POST = 0;
    public static final int RET_CODE_FAILED = 0;
    public static final int RET_CODE_LOGOUT = 999;
    public static final int RET_CODE_SUCCESS = 1;
    private boolean isHandleNewInform = true;

    /* renamed from: com.heimavista.api.a$a */
    /* compiled from: HvApi */
    class C3650a implements HttpCallback {

        /* renamed from: a */
        final /* synthetic */ HttpParams f6201a;

        /* renamed from: b */
        final /* synthetic */ HttpParams f6202b;

        C3650a(HttpParams cVar, HttpParams cVar2) {
            this.f6201a = cVar;
            this.f6202b = cVar2;
        }

        /* renamed from: a */
        public void mo22403a(String str) {
            HvApi.this.handleResponse(this.f6201a, this.f6202b, false, str);
        }

        /* renamed from: b */
        public void mo22404b(String str) {
            HvApi.this.onApiCancel(this.f6201a, this.f6202b, str);
        }

        public void onError(String str) {
            HvApi.this.onApiFailed(this.f6201a, this.f6202b, str, null);
        }

        public void onFinished() {
        }
    }

    /* access modifiers changed from: protected */
    public String createSign(HttpParams cVar) {
        TreeMap treeMap = new TreeMap(cVar.mo27202b());
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : treeMap.entrySet()) {
            String trim = ((String) entry.getKey()).trim();
            Object value = entry.getValue();
            if (!(value instanceof File) && !"sign".equals(trim)) {
                String trim2 = String.valueOf(value).trim();
                if (!TextUtils.isEmpty(trim2)) {
                    sb.append(trim);
                    sb.append("=");
                    sb.append(trim2);
                    sb.append("&");
                }
            }
        }
        sb.append("key=");
        sb.append(getAppSecret());
        String sb2 = sb.toString();
        String lowerCase = EncryptUtils.m1061a(sb2).toLowerCase();
        LogUtils.m1139c("src:" + sb2, "sign:" + lowerCase);
        return lowerCase;
    }

    /* access modifiers changed from: protected */
    public String decrypt(String str, String str2) {
        return HvApiEncryptUtils.m10000a(str, getAppSecret() + str2);
    }

    /* access modifiers changed from: protected */
    public String encrypt(String str, String str2) {
        return HvApiEncryptUtils.m10003b(str, getAppSecret() + str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public void encryptParams(HttpParams cVar) {
        String[] encryptKeys;
        String a = cVar.mo27198a("nonce", (String) null);
        if (a != null && (encryptKeys = getEncryptKeys()) != null) {
            for (String str : encryptKeys) {
                if (cVar.mo27200a(str)) {
                    cVar.mo27196a(str, (Object) encrypt(cVar.mo27198a(str, ""), a));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public HttpCallback genHttpCallback(HttpParams cVar, HttpParams cVar2) {
        return new C3650a(cVar, cVar2);
    }

    /* access modifiers changed from: protected */
    public HttpRequestConfig genHttpRequestConfig() {
        HttpRequestConfig.C4928b bVar = new HttpRequestConfig.C4928b();
        bVar.mo27211a(getUri());
        return bVar.mo27212a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public HttpParams genRequestParams(HttpParams cVar) {
        HttpParams cVar2 = new HttpParams();
        cVar2.mo27196a("appid", (Object) getAppId());
        cVar2.mo27196a("fun", (Object) getFun());
        cVar2.mo27196a("op", (Object) getOp());
        cVar2.mo27196a(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, (Object) getVersion());
        cVar2.mo27196a("sdkv", (Object) getSdkVersion());
        cVar2.mo27196a("nonce", (Object) RandomUtils.m15266b(10));
        cVar2.mo27195a(getBasicParams());
        cVar2.mo27195a(cVar);
        encryptParams(cVar2);
        cVar2.mo27196a("sign", (Object) createSign(cVar2));
        return cVar2;
    }

    /* access modifiers changed from: protected */
    public abstract String getAppId();

    /* access modifiers changed from: protected */
    public abstract String getAppSecret();

    /* access modifiers changed from: protected */
    public abstract HttpParams getBasicParams();

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract String getFun();

    /* access modifiers changed from: protected */
    public int getMethod() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public abstract String getOp();

    /* access modifiers changed from: protected */
    public String getSdkVersion() {
        return AppUtils.m946c();
    }

    /* access modifiers changed from: protected */
    public abstract String getUri();

    /* access modifiers changed from: protected */
    public abstract String getVersion();

    /* access modifiers changed from: protected */
    public HvApiResponse handleResponse(HttpParams cVar, HttpParams cVar2, boolean z, String str) {
        LogUtils.m1139c("handleResponse:" + str);
        HvApiResponse parseResponse = parseResponse(cVar, cVar2, z, str);
        int d = parseResponse.mo22515d();
        if (d == 0) {
            onApiFailed(cVar, cVar2, parseResponse.mo22511b(), null);
        } else if (d == 1) {
            onApiSuccess(cVar, cVar2, parseResponse.mo22506a(), parseResponse.mo22513c());
        } else if (d != 999) {
            onApiCallback(cVar, cVar2, parseResponse.mo22515d(), parseResponse.mo22511b(), parseResponse.mo22506a());
        } else {
            onApiLogout(cVar, cVar2, parseResponse.mo22506a());
        }
        if (this.isHandleNewInform && parseResponse.mo22516e()) {
            onApiNewInform();
        }
        return parseResponse;
    }

    public boolean isHandleNewInform() {
        return this.isHandleNewInform;
    }

    /* access modifiers changed from: protected */
    public void onApiCallback(HttpParams cVar, HttpParams cVar2, int i, String str, ParamJsonData fVar) {
    }

    /* access modifiers changed from: protected */
    public abstract void onApiCancel(HttpParams cVar, HttpParams cVar2, String str);

    /* access modifiers changed from: protected */
    public abstract void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar);

    /* access modifiers changed from: protected */
    public void onApiLogout(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar) {
        MemberControl.m17125s().mo27173a(fVar.mo25325a(NotificationCompat.CATEGORY_MESSAGE, ""));
    }

    /* access modifiers changed from: protected */
    public abstract void onApiNewInform();

    /* access modifiers changed from: protected */
    public abstract void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    public HvApiResponse parseResponse(HttpParams cVar, HttpParams cVar2, boolean z, String str) {
        HvApiResponse cVar3 = new HvApiResponse();
        cVar3.mo22514c(str);
        boolean z2 = false;
        if (z) {
            cVar3.mo22507a(0);
            cVar3.mo22509a("request error");
            return cVar3;
        }
        ParamJsonData fVar = new ParamJsonData(str, true);
        cVar3.mo22508a(fVar);
        cVar3.mo22509a(fVar.mo25325a(NotificationCompat.CATEGORY_MESSAGE, ""));
        String a = cVar2.mo27198a("nonce", (String) null);
        String a2 = fVar.mo25325a("nonce", (String) null);
        cVar3.mo22512b(a2);
        if (a != null) {
            if (!a.equals(a2)) {
                cVar3.mo22507a(0);
                cVar3.mo22509a("nonce error");
                return cVar3;
            }
            String a3 = fVar.mo25325a("sign", (String) null);
            if (a3 == null) {
                cVar3.mo22507a(0);
                cVar3.mo22509a("sign is null");
                return cVar3;
            }
            HttpParams cVar4 = new HttpParams();
            cVar4.mo27197a(fVar.mo25333c());
            if (!a3.equals(createSign(cVar4))) {
                cVar3.mo22507a(0);
                cVar3.mo22509a("sign is not match");
                return cVar3;
            }
        }
        cVar3.mo22507a(fVar.mo25323a("retCode", (Integer) 0).intValue());
        if (fVar.mo25323a("isNewInform", (Integer) 0).intValue() == 1) {
            z2 = true;
        }
        cVar3.mo22510a(z2);
        return cVar3;
    }

    public HttpCancelable request(HttpParams cVar) {
        HttpParams genRequestParams = genRequestParams(cVar);
        if (getMethod() == 1) {
            return new HttpRequest(genHttpRequestConfig()).mo27204a(genRequestParams, genHttpCallback(cVar, genRequestParams));
        }
        return new HttpRequest(genHttpRequestConfig()).mo27207b(genRequestParams, genHttpCallback(cVar, genRequestParams));
    }

    public HvApiResponse requestSync(HttpParams cVar) {
        HttpResponse hVar;
        HttpParams genRequestParams = genRequestParams(cVar);
        if (getMethod() == 1) {
            hVar = new HttpRequest(genHttpRequestConfig()).mo27205a(genRequestParams);
        } else {
            hVar = new HttpRequest(genHttpRequestConfig()).mo27208b(genRequestParams);
        }
        return handleResponse(cVar, genRequestParams, hVar.mo27219a(), hVar.mo27220b());
    }

    public void setHandleNewInform(boolean z) {
        this.isHandleNewInform = z;
    }
}
