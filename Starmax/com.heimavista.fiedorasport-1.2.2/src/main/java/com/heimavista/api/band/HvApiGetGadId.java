package com.heimavista.api.band;

import android.text.TextUtils;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetGadId extends HvApiBasic {
    private static final String KEY_EXPIRE = "GetGadId_expires_";

    private static long getExpires(String str) {
        return TickManager.m10520a(KEY_EXPIRE + str, 0);
    }

    public static boolean isExpire(String str) {
        return getExpires(str) < System.currentTimeMillis() / 1000;
    }

    private static void saveExpires(String str, long j) {
        TickManager.m10526c(KEY_EXPIRE + str, j);
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "band";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public void getGadId(String str, OnResultListener<ParamJsonData> dVar) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("userNbr", (Object) str);
        super.request(cVar, dVar);
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getGadId";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String str2 = "";
        if (fVar.mo25334d().has("gadId")) {
            str2 = fVar.mo25325a("gadId", str2);
        }
        int i = 0;
        if (fVar.mo25334d().has("expires")) {
            i = fVar.mo25323a("expires", (Integer) 0).intValue();
        }
        if (!TextUtils.isEmpty(str2)) {
            saveExpires(str2, ((long) i) + (System.currentTimeMillis() / 1000));
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, OnResultListener<ParamJsonData> dVar) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("userNbr", (Object) str);
        return super.request(cVar, dVar);
    }
}
