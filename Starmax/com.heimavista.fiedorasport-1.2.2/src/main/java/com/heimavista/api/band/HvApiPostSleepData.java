package com.heimavista.api.band;

import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONArray;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostSleepData extends HvApiBaseBand {
    private long endTimestamp;
    private long startTimestamp;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getRequestParams() {
        JSONArray v = new SleepDataDb().mo24265v();
        if (v == null) {
            return null;
        }
        this.startTimestamp = v.optJSONArray(0).optLong(3);
        this.endTimestamp = v.optJSONArray(v.length() - 1).optLong(4);
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("timestamp");
        jSONArray.put("timezone");
        jSONArray.put("type");
        jSONArray.put(TtmlNode.START);
        jSONArray.put(TtmlNode.END);
        jSONArray.put("sleep");
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Dict", (Object) jSONArray.toString());
        httpParamsWithBandId.mo27196a("Data", (Object) v.toString());
        return httpParamsWithBandId;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postSleepData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        new SleepDataDb().mo24262a(this.startTimestamp, this.endTimestamp);
    }

    public HttpCancelable request() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.request(requestParams);
    }

    public HvApiResponse requestSync() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.requestSync(requestParams);
    }
}
