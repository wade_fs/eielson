package com.heimavista.api.account;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.MapUtil;
import com.heimavista.utils.ParamJsonData;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiBindSocial extends HvApiBase {
    public static final String ACTION_FAILED;
    public static final String ACTION_SUCCESS;
    private OnResultListener<Void> listener;

    /* renamed from: com.heimavista.api.account.HvApiBindSocial$a */
    public static class C3651a {

        /* renamed from: a */
        private HttpParams f6204a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        public C3651a(@NonNull String str, @NonNull String str2) {
            HttpParams cVar = new HttpParams();
            cVar.mo27196a("memUserid", (Object) str);
            cVar.mo27196a("memName", (Object) str2);
            this.f6204a = cVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: a */
        public C3651a mo22413a(String str) {
            this.f6204a.mo27196a("memGender", (Object) str);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: b */
        public C3651a mo22415b(String str) {
            this.f6204a.mo27196a("memPhotoUrl", (Object) str);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: c */
        public C3651a mo22416c(String str) {
            this.f6204a.mo27196a("unionId", (Object) str);
            return this;
        }

        /* renamed from: a */
        public HttpParams mo22414a() {
            return this.f6204a;
        }
    }

    static {
        Class<HvApiBindSocial> cls = HvApiBindSocial.class;
        ACTION_SUCCESS = cls.getName() + ".SUCCESS";
        ACTION_FAILED = cls.getName() + ".FAILED";
    }

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"memUserid", "memName", "unionId"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "bindSocial";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        HvApp.m13010c().mo24157a(ACTION_FAILED);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        Map<String, Object> c = new ParamJsonData(fVar.mo25325a("MemInfo", "{}"), true).mo25333c();
        String[] f = MemberControl.m17125s().mo27181f();
        for (String str2 : f) {
            String a = MapUtil.m15243a(c, str2, (String) null);
            if (!TextUtils.isEmpty(a)) {
                c.put(str2, decrypt(a, str));
            }
        }
        String a2 = fVar.mo25325a("loginInfo", "");
        if (!TextUtils.isEmpty(a2)) {
            MemberControl.m17125s().mo27177c(decrypt(a2, str));
        }
        MemberControl.m17125s().mo27179d(MapUtil.m15245a(c).toString());
        HvApp.m13010c().mo24157a(ACTION_SUCCESS);
        OnResultListener<Void> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    public HttpCancelable request(C3651a aVar) {
        return super.request(aVar.mo22414a());
    }

    public HvApiResponse requestSync(C3651a aVar) {
        return super.requestSync(aVar.mo22414a());
    }

    public HttpCancelable request(C3651a aVar, OnResultListener<Void> dVar) {
        this.listener = dVar;
        return super.request(aVar.mo22414a());
    }

    public HvApiResponse requestSync(C3651a aVar, OnResultListener<Void> dVar) {
        this.listener = dVar;
        return super.requestSync(aVar.mo22414a());
    }
}
