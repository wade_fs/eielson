package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetHeartData extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getHeartData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.i.d.a(java.lang.String, java.lang.String, long, int, boolean, int):void
     arg types: [java.lang.String, java.lang.String, long, int, int, int]
     candidates:
      e.e.d.a.a(java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer):java.util.List<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.i.d.a(java.lang.String, java.lang.String, long, int, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        HttpParams cVar3 = cVar;
        ParamJsonData fVar2 = fVar;
        List<Object> a = new ParamJsonData(fVar2.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar2.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                HeartRateDataDb.m13152a(cVar3.mo27198a("userNbr", ""), jSONArray.optString(a2.indexOf("timezone")), jSONArray.optLong(a2.indexOf("timestamp")), jSONArray.optInt(a2.indexOf("heartrate")), true, 2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (fVar2.mo25323a("isNext", (Integer) 0).intValue() == 1) {
            requestSync(cVar3.mo27198a("userNbr", ""));
        }
    }

    public HttpCancelable request(String str) {
        return super.request(getHttpParamsWithTick(str));
    }

    public HvApiResponse requestSync(String str) {
        return super.requestSync(getHttpParamsWithTick(str));
    }
}
