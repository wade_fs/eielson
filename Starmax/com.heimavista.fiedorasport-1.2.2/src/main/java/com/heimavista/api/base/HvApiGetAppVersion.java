package com.heimavista.api.base;

import android.os.Bundle;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.OnResultListener;
import com.heimavista.info.VersionInfo;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetAppVersion extends HvApiBase {
    public static final String ACTION_FAILED;
    public static final String ACTION_SUCCESS;
    public static final String ACTION_SUCCESS_NEW;
    public static final String ACTION_SUCCESS_NEW_FORCE;
    private OnResultListener<VersionInfo> onResultListener;

    static {
        Class<HvApiGetAppVersion> cls = HvApiGetAppVersion.class;
        ACTION_SUCCESS = cls.getName() + ".SUCCESS";
        ACTION_SUCCESS_NEW = cls.getName() + ".SUCCESS_NEW";
        ACTION_SUCCESS_NEW_FORCE = cls.getName() + ".SUCCESS_NEW_FORCE";
        ACTION_FAILED = cls.getName() + ".FAILED";
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "base";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getAppVersion";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<VersionInfo> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        } else {
            HvApp.m13010c().mo24157a(ACTION_FAILED);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String str2;
        VersionInfo versionInfo = (VersionInfo) GsonUtils.m1100a(fVar.mo25325a("VerInfo", "{}"), VersionInfo.class);
        OnResultListener<VersionInfo> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(versionInfo);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable(VersionInfo.class.getCanonicalName(), versionInfo);
        if (AppUtils.m944b() >= versionInfo.f9274P) {
            str2 = ACTION_SUCCESS;
        } else if (versionInfo.f9277S == 1) {
            str2 = ACTION_SUCCESS_NEW_FORCE;
        } else {
            str2 = ACTION_SUCCESS_NEW;
        }
        HvApp.m13010c().mo24158a(str2, bundle);
    }

    public HttpCancelable request() {
        return requestWithListener(null);
    }

    public HttpCancelable requestWithListener(OnResultListener<VersionInfo> dVar) {
        this.onResultListener = dVar;
        return request(null);
    }
}
