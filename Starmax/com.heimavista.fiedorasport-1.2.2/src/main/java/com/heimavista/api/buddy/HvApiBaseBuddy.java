package com.heimavista.api.buddy;

import com.heimavista.api.HvApiBasic;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiBaseBuddy extends HvApiBasic {
    private String userNbr;

    /* access modifiers changed from: protected */
    public String getFun() {
        return "buddy";
    }

    /* access modifiers changed from: protected */
    public HttpParams getHttpParamsWithTick(String str) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("lastTime", Long.valueOf(TickManager.m10523b(getTickKey())));
        return httpParamsWithUser;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public HttpParams getHttpParamsWithUser(String str) {
        setUserNbr(str);
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("userNbr", (Object) str);
        return cVar;
    }

    /* access modifiers changed from: protected */
    public String getTickKey() {
        return this.userNbr + "_" + super.getTickKey();
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    public HttpCancelable request(String str) {
        return request(str, (OnResultListener<ParamJsonData>) null);
    }

    public HvApiResponse requestSync(String str) {
        setUserNbr(str);
        return super.requestSync(getHttpParamsWithUser(str));
    }

    public void setUserNbr(String str) {
        this.userNbr = str;
    }

    public HttpCancelable request(String str, OnResultListener<ParamJsonData> dVar) {
        return super.request(getHttpParamsWithUser(str), dVar);
    }
}
