package com.heimavista.api.band;

import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBandInfo extends HvApiBaseBand {
    private OnResultListener<ParamJsonData> listener;
    private int requestTimes = 0;

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBandInfo";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        if (this.requestTimes < 3) {
            request(this.listener);
        }
        super.onApiFailed(cVar, cVar2, str, fVar);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        JSONObject optJSONObject = fVar.mo25334d().optJSONObject("BandInfo");
        if (optJSONObject != null) {
            FSManager.m10323r().mo22827a(optJSONObject.optString("bandId"));
            FSManager.m10323r().mo22830b(optJSONObject.optString("deviceName"));
            FSManager.m10323r().mo22834d(optJSONObject.optString("deviceMacAddress"));
        }
        this.requestTimes = 0;
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        this.requestTimes++;
        this.listener = dVar;
        return super.request(null, dVar);
    }
}
