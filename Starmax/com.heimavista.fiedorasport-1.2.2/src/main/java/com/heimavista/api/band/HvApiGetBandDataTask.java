package com.heimavista.api.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.StringUtils;
import com.heimavista.entity.band.TickType;
import com.heimavista.fiedorasport.p109h.TickManager;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.heimavista.api.band.b */
public class HvApiGetBandDataTask {

    /* renamed from: a */
    private static boolean f6210a = false;

    /* renamed from: com.heimavista.api.band.b$a */
    /* compiled from: HvApiGetBandDataTask */
    public interface C3655a {
        /* renamed from: a */
        void mo22479a(int i);
    }

    /* renamed from: a */
    public void mo22477a(@NonNull C3655a aVar) {
        if (!f6210a) {
            f6210a = true;
            new Thread(new C3654a(this, Arrays.asList("getSleepData", "getStepData", "getStepDetail", "getHeartData", "getSportData"), aVar)).start();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22478a(List list, C3655a aVar) {
        Iterator it = list.iterator();
        boolean z = false;
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            int a = m10007a((String) it.next());
            if (a == 901 || a == 999) {
                aVar.mo22479a(100);
                z = true;
            } else {
                i++;
                int size = (i * 100) / list.size();
                boolean z2 = size == 100;
                aVar.mo22479a(size);
                LogUtils.m1139c("progress " + i + "/" + list.size());
                z = z2;
            }
        }
        aVar.mo22479a(100);
        z = true;
        if (z) {
            TickManager.m10524b(TickType.DATA_REQUEST, System.currentTimeMillis());
        }
        f6210a = false;
    }

    /* renamed from: a */
    private int m10007a(String str) {
        try {
            HvApiGetDataBase hvApiGetDataBase = (HvApiGetDataBase) Class.forName("com.heimavista.api.band.HvApi" + StringUtils.m941a(str)).newInstance();
            hvApiGetDataBase.setHandleNewInform(false);
            return hvApiGetDataBase.requestSync().mo22515d();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        } catch (InstantiationException e2) {
            e2.printStackTrace();
            return 0;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            return 0;
        }
    }
}
