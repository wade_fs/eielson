package com.heimavista.api;

import android.os.Bundle;
import android.text.TextUtils;
import com.blankj.utilcode.util.SPUtils;
import com.facebook.internal.ServerProtocol;
import com.heimavista.api.base.HvApiGetInformList;
import com.heimavista.api.start.HvApiRegister;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiBase extends HvApi {
    public static String ACTION_REQUEST_FAILED = (HvApp.m13010c().getPackageName() + ".HvApi.ACTION_HTTP_REQUEST_FAILED");
    private OnResultListener<ParamJsonData> listener;

    /* renamed from: com.heimavista.api.HvApiBase$a */
    class C3648a implements OnResultListener<String> {

        /* renamed from: a */
        final /* synthetic */ C3649b f6197a;

        /* renamed from: b */
        final /* synthetic */ HttpParams f6198b;

        C3648a(C3649b bVar, HttpParams cVar) {
            this.f6197a = bVar;
            this.f6198b = cVar;
        }

        /* renamed from: a */
        public void mo22380a(String str) {
            this.f6197a.f6200a = HvApiBase.super.request(this.f6198b);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            HvApiBase.this.onApiRegisterFailed(cVar, cVar2, str, fVar);
        }
    }

    /* renamed from: com.heimavista.api.HvApiBase$b */
    private static class C3649b implements HttpCancelable {

        /* renamed from: a */
        HttpCancelable f6200a;

        private C3649b() {
        }

        public void cancel() {
            this.f6200a.cancel();
        }

        /* synthetic */ C3649b(C3648a aVar) {
            this();
        }
    }

    public static String getDevNbr() {
        return HvApiEncryptUtils.m9999a(SPUtils.m1243c("HvApiBase").mo9872a("devNbr", ""));
    }

    public static void saveDevNbr(String str) {
        SPUtils.m1243c("HvApiBase").mo9883b("devNbr", HvApiEncryptUtils.m10002b(str));
    }

    /* access modifiers changed from: protected */
    public String getAppId() {
        return HvAppConfig.m13024a().mo24166a("app", "id");
    }

    /* access modifiers changed from: protected */
    public String getAppSecret() {
        return HvAppConfig.m13024a().mo24166a("app", "secret");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public HttpParams getBasicParams() {
        HttpParams cVar = new HttpParams();
        if (isNeedLogin() && MemberControl.m17125s().mo27192q()) {
            cVar.mo27196a("loginInfo", (Object) MemberControl.m17125s().mo27174b());
        }
        cVar.mo27196a("devNbr", (Object) getDevNbr());
        return cVar;
    }

    public OnResultListener<ParamJsonData> getListener() {
        return this.listener;
    }

    /* access modifiers changed from: protected */
    public String getUri() {
        return HvAppConfig.m13024a().mo24166a("app", "url");
    }

    /* access modifiers changed from: protected */
    public String getVersion() {
        return HvAppConfig.m13024a().mo24166a("app", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
    }

    /* access modifiers changed from: protected */
    public abstract boolean isNeedLogin();

    /* access modifiers changed from: protected */
    public void onApiCancel(HttpParams cVar, HttpParams cVar2, String str) {
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<ParamJsonData> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("errMsg", str);
        HvApp.m13010c().mo24158a(ACTION_REQUEST_FAILED, bundle);
    }

    /* access modifiers changed from: protected */
    public void onApiNewInform() {
        HvApiGetInformList.requestSingle();
    }

    /* access modifiers changed from: protected */
    public void onApiRegisterFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        onApiFailed(cVar, cVar2, str, fVar);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        OnResultListener<ParamJsonData> dVar = this.listener;
        if (dVar != null) {
            dVar.mo22380a(fVar);
        }
    }

    public HttpCancelable request(HttpParams cVar) {
        if (!TextUtils.isEmpty(getDevNbr())) {
            return super.request(cVar);
        }
        C3649b bVar = new C3649b(null);
        bVar.f6200a = new HvApiRegister().requestWithListener(new C3648a(bVar, cVar));
        return bVar;
    }

    public HvApiResponse requestSync(HttpParams cVar) {
        if (!TextUtils.isEmpty(getDevNbr())) {
            return super.requestSync(cVar);
        }
        HvApiResponse requestSync = new HvApiRegister().requestSync();
        return requestSync.mo22515d() == 1 ? super.requestSync(cVar) : requestSync;
    }

    public HttpCancelable request(HttpParams cVar, OnResultListener<ParamJsonData> dVar) {
        this.listener = dVar;
        return request(cVar);
    }
}
