package com.heimavista.api.band;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.api.HvApiResponse;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONArray;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostStepData extends HvApiBaseBand {
    private long endTimestamp;
    private int lastStep = 0;
    private long startTimestamp;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getRequestParams() {
        JSONArray v = new StepDataDb().mo24278v();
        if (v == null) {
            return null;
        }
        this.startTimestamp = v.optJSONArray(0).optLong(0);
        this.endTimestamp = v.optJSONArray(v.length() - 1).optLong(0);
        this.lastStep = v.optJSONArray(v.length() - 1).optInt(5);
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("timestamp");
        jSONArray.put("timezone");
        jSONArray.put("cal");
        jSONArray.put("distance");
        jSONArray.put("stepTime");
        jSONArray.put("step");
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Dict", (Object) jSONArray.toString());
        httpParamsWithBandId.mo27196a("Data", (Object) v.toString());
        return httpParamsWithBandId;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postStepData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        new StepDataDb().mo24274a(this.startTimestamp, this.endTimestamp);
        LogUtils.m1139c("madyStep " + TimeUtils.m1009b(this.startTimestamp * 1000) + " " + TimeUtils.m1009b(this.endTimestamp * 1000) + " " + this.lastStep);
        int i = this.lastStep;
        if (i > 0) {
            BandDataManager.m10549a(this.endTimestamp, i);
        }
    }

    public HttpCancelable request() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.request(requestParams);
    }

    public HvApiResponse requestSync() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.requestSync(requestParams);
    }
}
