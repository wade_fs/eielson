package com.heimavista.api.start;

import com.heimavista.api.HvApiResponse;
import com.heimavista.api.HvApiStart;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiToken extends HvApiStart {
    private OnResultListener<String> onTokenListener;

    /* access modifiers changed from: protected */
    public String getFun() {
        return TtmlNode.START;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "token";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<String> dVar = this.onTokenListener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        if (this.onTokenListener != null) {
            this.onTokenListener.mo22380a(fVar.mo25325a("token", (String) null));
        }
    }

    public HvApiResponse requestSync() {
        return requestSync(null);
    }

    public HttpCancelable requestWithListener(OnResultListener<String> dVar) {
        this.onTokenListener = dVar;
        return request(null);
    }
}
