package com.heimavista.api.buddy;

import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p195j.LocDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBuddyLoc extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBuddyLoc";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        ParamJsonData fVar2 = fVar;
        List<Object> a = new ParamJsonData(fVar2.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar2.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                LocDb.m13306a(MemberControl.m17125s().mo27187l(), jSONArray.optString(a2.indexOf("userNbr")), jSONArray.optDouble(a2.indexOf("la")), jSONArray.optDouble(a2.indexOf("lo")), (float) jSONArray.optDouble(a2.indexOf("speed")), jSONArray.optInt(a2.indexOf("timestamp")), jSONArray.optInt(a2.indexOf("battery")), jSONArray.optInt(a2.indexOf("batteryStat")), jSONArray.optInt(a2.indexOf("locStat")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
