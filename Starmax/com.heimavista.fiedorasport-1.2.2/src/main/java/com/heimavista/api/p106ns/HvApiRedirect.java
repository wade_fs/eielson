package com.heimavista.api.p106ns;

import android.text.TextUtils;
import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;
import p119e.p189e.p219h.HttpRequest;

/* renamed from: com.heimavista.api.ns.HvApiRedirect */
public class HvApiRedirect extends HvApiBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "ns";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "redirect";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public String getUrl(String str) {
        HttpParams cVar = new HttpParams();
        if (!TextUtils.isEmpty(str)) {
            cVar.mo27196a("redirectUrl", (Object) str);
        }
        return HttpRequest.m17164a(genHttpRequestConfig(), genRequestParams(cVar));
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }

    public HttpCancelable request(HttpParams cVar) {
        return null;
    }

    public HvApiResponse requestSync(HttpParams cVar) {
        return null;
    }
}
