package com.heimavista.api.band;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.io.File;
import p119e.p189e.p219h.HttpParams;

public class HvApiFeedback extends HvApiBaseBand {

    /* renamed from: com.heimavista.api.band.HvApiFeedback$a */
    public static class C3653a {

        /* renamed from: a */
        HttpParams f6206a = new HttpParams();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
         arg types: [java.lang.String, java.lang.String]
         candidates:
          e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
          e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
        /* renamed from: a */
        public C3653a mo22443a(String str) {
            this.f6206a.mo27196a("content", (Object) str);
            return this;
        }

        /* renamed from: a */
        public C3653a mo22442a(File file) {
            this.f6206a.mo27196a("Image", file);
            return this;
        }

        /* renamed from: a */
        public HttpParams mo22444a() {
            return this.f6206a;
        }
    }

    public void feedback(OnResultListener<ParamJsonData> dVar, HttpParams cVar) {
        request(cVar, dVar);
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "feedback";
    }
}
