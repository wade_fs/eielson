package com.heimavista.api.band;

import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiBind extends HvApiBaseBand {
    private String deviceMacAddress;
    private String deviceName;

    public HvApiBind(String str, String str2) {
        this.deviceName = str;
        this.deviceMacAddress = str2;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "bind";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String str2 = "";
        if (fVar.mo25331a("bandId")) {
            str2 = fVar.mo25325a("bandId", str2);
        } else if (fVar.mo25331a("bindId")) {
            str2 = fVar.mo25325a("bindId", str2);
        }
        if (!TextUtils.isEmpty(str2)) {
            FSManager.m10323r().mo22827a(str2);
            FSManager.m10323r().mo22830b(this.deviceName);
            FSManager.m10323r().mo22834d(this.deviceMacAddress);
            new HvApiGetBandSettings().request();
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(boolean z, OnResultListener<ParamJsonData> dVar) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("deviceName", (Object) this.deviceName);
        cVar.mo27196a("deviceMacAddress", (Object) this.deviceMacAddress);
        cVar.mo27196a("isForce", Integer.valueOf(z ? 1 : 0));
        if (HvAppConfig.m13024a().mo24166a("app", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION).compareTo("1.7.0") >= 0) {
            boolean equalsIgnoreCase = this.deviceName.equalsIgnoreCase("F15");
            cVar.mo27196a("deviceType", (Object) (equalsIgnoreCase ? "watch" : "band"));
            cVar.mo27196a("sdkType", (Object) (equalsIgnoreCase ? "watch01" : "band01"));
        }
        return super.request(cVar, dVar);
    }
}
