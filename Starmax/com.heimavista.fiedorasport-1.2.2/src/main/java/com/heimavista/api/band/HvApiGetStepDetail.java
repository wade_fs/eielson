package com.heimavista.api.band;

import com.heimavista.entity.band.StepDetailInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.StepDetailDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetStepDetail extends HvApiGetDataBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getStepDetail";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                StepDetailInfo gVar = new StepDetailInfo();
                gVar.f6298a = MemberControl.m17125s().mo27187l();
                gVar.f6299b = jSONArray.optLong(a2.indexOf("timestamp"));
                gVar.f6300c = jSONArray.optString(a2.indexOf("timezone"));
                gVar.f6301d = jSONArray.optInt(a2.indexOf("type"));
                gVar.f6302e = jSONArray.optLong(a2.indexOf(TtmlNode.START));
                gVar.f6303f = jSONArray.optLong(a2.indexOf(TtmlNode.END));
                gVar.f6307j = jSONArray.optInt(a2.indexOf("step"));
                gVar.f6305h = jSONArray.optInt(a2.indexOf("distance"));
                gVar.f6304g = jSONArray.optInt(a2.indexOf("cal"));
                gVar.f6306i = jSONArray.optInt(a2.indexOf("stepTime"));
                gVar.f6308k = true;
                new StepDetailDb().mo24281a(gVar);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
