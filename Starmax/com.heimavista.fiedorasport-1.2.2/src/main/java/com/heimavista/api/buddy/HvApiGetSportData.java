package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.api.sport.HvApiGetLikeCnt;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSportData extends HvApiBaseBuddy {
    private static boolean onRequesting = false;

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSportData";
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
        onRequesting = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        List<Object> a3 = new ParamJsonData(fVar.mo25325a("Deleted", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SportInfo dVar = new SportInfo();
                dVar.f6270a = cVar.mo27198a("userNbr", "");
                dVar.f6272c = jSONArray.optString(a2.indexOf("sportId"));
                dVar.f6271b = jSONArray.optString(a2.indexOf("timezone"));
                dVar.f6273d = jSONArray.optInt(a2.indexOf("type"));
                dVar.f6274e = jSONArray.optLong(a2.indexOf(TtmlNode.START));
                dVar.f6275f = jSONArray.optLong(a2.indexOf(TtmlNode.END));
                dVar.f6276g = jSONArray.optInt(a2.indexOf("duration"));
                dVar.f6277h = jSONArray.optInt(a2.indexOf("distance"));
                dVar.f6278i = jSONArray.optInt(a2.indexOf("cal"));
                dVar.f6279j = jSONArray.optInt(a2.indexOf("avgHeartRate"));
                dVar.f6281l = jSONArray.optInt(a2.indexOf("maxHeartRate"));
                dVar.f6280k = jSONArray.optInt(a2.indexOf("minHeartRate"));
                SportDataDb.m13191a(dVar);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        new SportDataDb().mo24267a(cVar.mo27198a("userNbr", ""), a3);
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (fVar.mo25323a("isNext", (Integer) 0).intValue() == 1) {
            requestSync(cVar.mo27198a("userNbr", ""));
        }
        new HvApiGetLikeCnt().request(cVar.mo27198a("userNbr", ""));
        onRequesting = false;
    }

    public HttpCancelable request(String str) {
        if (onRequesting) {
            return null;
        }
        onRequesting = true;
        return super.request(getHttpParamsWithTick(str));
    }

    public HvApiResponse requestSync(String str) {
        if (onRequesting) {
            return null;
        }
        onRequesting = true;
        return super.requestSync(getHttpParamsWithTick(str));
    }
}
