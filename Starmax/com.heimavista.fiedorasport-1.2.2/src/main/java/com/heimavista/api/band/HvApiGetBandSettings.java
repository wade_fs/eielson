package com.heimavista.api.band;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.utils.ParamJsonData;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetBandSettings extends HvApiBaseBand {
    private void saveAlarm(JSONArray jSONArray) {
        JSONArray jSONArray2 = jSONArray;
        if (jSONArray2 != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray2.optJSONObject(i);
                BandDataManager.m10552a(new AlarmInfoItem((long) optJSONObject.optInt("id"), optJSONObject.optInt("type"), optJSONObject.optInt("hour"), optJSONObject.optInt("minute"), optJSONObject.optInt("monday"), optJSONObject.optInt("tuesday"), optJSONObject.optInt("wedensday"), optJSONObject.optInt("thursday"), optJSONObject.optInt("friday"), optJSONObject.optInt("saturday"), optJSONObject.optInt("sunday"), optJSONObject.optString("content"), optJSONObject.optInt("once") == 1));
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getBandSettings";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        int i = 1;
        for (Object obj : new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a()) {
            try {
                JSONObject jSONObject = new JSONObject(String.valueOf(obj));
                int optInt = jSONObject.optInt("type", -1);
                if (optInt == 4) {
                    try {
                    } catch (JSONException e) {
                        e = e;
                        e.printStackTrace();
                        i = 1;
                    }
                    try {
                        saveAlarm(jSONObject.optJSONArray("Data"));
                    } catch (JSONException e2) {
                        e = e2;
                        e.printStackTrace();
                        i = 1;
                    }
                } else {
                    JSONObject optJSONObject = jSONObject.optJSONObject("Data");
                    if (!(optJSONObject == null || optInt == 0)) {
                        boolean z = false;
                        if (optInt == i) {
                            RemindMsgInfo e3 = BandDataManager.m10563e();
                            if (optJSONObject.optInt("enable") == 1) {
                                z = true;
                            }
                            e3.mo22604d(z);
                            BandDataManager.m10551a(e3);
                            i = 1;
                        } else if (optInt == 2) {
                            RemindMsgInfo e4 = BandDataManager.m10563e();
                            if (optJSONObject.optInt("enable") == 1) {
                                z = true;
                            }
                            e4.mo22611g(z);
                            BandDataManager.m10551a(e4);
                        } else if (optInt == 3) {
                            RemindMsgInfo e5 = BandDataManager.m10563e();
                            e5.mo22600b(optJSONObject.optInt("facebook") == 1);
                            e5.mo22602c(optJSONObject.optInt("line") == 1);
                            e5.mo22607e(optJSONObject.optInt("qq") == 1);
                            e5.mo22615i(optJSONObject.optInt(ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE) == 1);
                            e5.mo22609f(optJSONObject.optInt("skype") == 1);
                            e5.mo22613h(optJSONObject.optInt("twitter") == 1);
                            if (optJSONObject.optInt("whatsapp") == 1) {
                                z = true;
                            }
                            e5.mo22617j(z);
                            BandDataManager.m10551a(e5);
                        } else if (optInt == 5) {
                            int optInt2 = optJSONObject.optInt("time", 0);
                            BandDataManager.m10550a(new PeriodTimeInfo("Sedentary", optInt2 > 0, optJSONObject.optInt("startHour", 9), optJSONObject.optInt("startMinute", 0), optJSONObject.optInt("endHour", 0), optJSONObject.optInt("endMinute", 0), optInt2));
                        } else if (optInt == 6) {
                            BandDataManager.m10560c(optJSONObject.optInt("stepGoal", 8000));
                        } else if (optInt == 7) {
                            boolean z2 = optJSONObject.optInt("riseHand") == i;
                            boolean z3 = optJSONObject.optInt("riseHand") == i;
                            boolean z4 = optJSONObject.optInt("riseHand") == i;
                            int optInt3 = optJSONObject.optInt("startHour");
                            int optInt4 = optJSONObject.optInt("endHour");
                            int optInt5 = optJSONObject.optInt("startMinute");
                            int optInt6 = optJSONObject.optInt("endMinute");
                            BandDataManager.m10553a(new DeviceProfile(z2, z3, z4, optInt3, optInt4, optInt5, optInt6));
                            PeriodTimeInfo periodTimeInfo = new PeriodTimeInfo("DoNotDisturb", z4, optInt3, optInt5, optInt4, optInt6, 0);
                            BandDataManager.m10550a(periodTimeInfo);
                            PeriodTimeInfo periodTimeInfo2 = r14;
                            PeriodTimeInfo periodTimeInfo3 = new PeriodTimeInfo("QuickView", z2, optInt4, optInt6, optInt3, optInt5, 0);
                            if (!z4) {
                                periodTimeInfo2.mo22586c(0);
                                periodTimeInfo2.mo22588d(0);
                                periodTimeInfo2.mo22581a(0);
                                periodTimeInfo2.mo22584b(0);
                            }
                            try {
                                Object[] objArr = new Object[1];
                                objArr[0] = "mady " + periodTimeInfo.toString();
                                LogUtils.m1139c(objArr);
                                Object[] objArr2 = new Object[1];
                                objArr2[0] = "mady " + periodTimeInfo2.toString();
                                LogUtils.m1139c(objArr2);
                                BandDataManager.m10550a(periodTimeInfo);
                                BandDataManager.m10550a(periodTimeInfo2);
                            } catch (JSONException e6) {
                                e = e6;
                            }
                        }
                    }
                }
            } catch (JSONException e7) {
                e = e7;
                e.printStackTrace();
                i = 1;
            }
            i = 1;
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request() {
        return request((OnResultListener<ParamJsonData>) null);
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            return null;
        }
        return super.request(getHttpParamsWithBandId(), dVar);
    }
}
