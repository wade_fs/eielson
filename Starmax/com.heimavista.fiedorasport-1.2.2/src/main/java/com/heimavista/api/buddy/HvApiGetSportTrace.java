package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.band.SportTraceInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SportTraceDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSportTrace extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSportTrace";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SportTraceInfo fVar2 = new SportTraceInfo();
                fVar2.f6289a = cVar.mo27198a("userNbr", "");
                fVar2.f6290b = cVar.mo27198a("sportId", "");
                fVar2.f6291c = jSONArray.optInt(a2.indexOf("type"));
                fVar2.f6292d = jSONArray.optLong(a2.indexOf("timestamp"));
                fVar2.f6293e = jSONArray.optDouble(a2.indexOf("la"));
                fVar2.f6294f = jSONArray.optDouble(a2.indexOf("lo"));
                fVar2.f6295g = jSONArray.optInt(a2.indexOf("duration"));
                fVar2.f6296h = jSONArray.optInt(a2.indexOf("distance"));
                fVar2.f6297i = true;
                SportTraceDataDb.m13208a(fVar2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, String str2) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("sportId", (Object) str2);
        return super.request(httpParamsWithUser);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    public HvApiResponse requestSync(String str, String str2) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("sportId", (Object) str2);
        HvApiResponse requestSync = requestSync(httpParamsWithUser);
        if (requestSync.mo22515d() == 1) {
            boolean z = false;
            if (requestSync.mo22506a().mo25323a("isNext", (Integer) 0).intValue() == 1) {
                z = true;
            }
            if (z) {
                return requestSync(str2);
            }
        }
        return super.requestSync(httpParamsWithUser);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, String str2, OnResultListener<ParamJsonData> dVar) {
        HttpParams httpParamsWithUser = getHttpParamsWithUser(str);
        httpParamsWithUser.mo27196a("sportId", (Object) str2);
        return super.request(httpParamsWithUser, dVar);
    }
}
