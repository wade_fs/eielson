package com.heimavista.api.account;

import com.heimavista.api.HvApiBase;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSmsCode extends HvApiBase {
    private OnResultListener<Void> onResultListener;

    /* access modifiers changed from: protected */
    public String[] getEncryptKeys() {
        return new String[]{"memMobile"};
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "account";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSmsCode";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<Void> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        OnResultListener<Void> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(String str, OnResultListener<Void> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("memMobile", (Object) str);
        return request(cVar);
    }
}
