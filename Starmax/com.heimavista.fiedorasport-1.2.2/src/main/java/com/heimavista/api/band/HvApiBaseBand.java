package com.heimavista.api.band;

import com.heimavista.api.HvApiBasic;
import com.heimavista.fiedorasport.FSManager;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiBaseBand extends HvApiBasic {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "band";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public HttpParams getHttpParamsWithBandId() {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("bandId", (Object) FSManager.m10323r().mo22836f());
        return cVar;
    }

    /* access modifiers changed from: protected */
    public HttpParams getHttpParamsWithTick() {
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("lastTime", Long.valueOf(getTick()));
        return httpParamsWithBandId;
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }
}
