package com.heimavista.api;

import com.heimavista.utils.ParamJsonData;

/* renamed from: com.heimavista.api.c */
public class HvApiResponse {

    /* renamed from: a */
    private int f6216a;

    /* renamed from: b */
    private String f6217b;

    /* renamed from: c */
    private String f6218c;

    /* renamed from: d */
    private ParamJsonData f6219d;

    /* renamed from: e */
    private boolean f6220e;

    /* renamed from: a */
    public void mo22507a(int i) {
        this.f6216a = i;
    }

    /* renamed from: b */
    public void mo22512b(String str) {
        this.f6217b = str;
    }

    /* renamed from: c */
    public String mo22513c() {
        return this.f6217b;
    }

    /* renamed from: c */
    public void mo22514c(String str) {
    }

    /* renamed from: d */
    public int mo22515d() {
        return this.f6216a;
    }

    /* renamed from: e */
    public boolean mo22516e() {
        return this.f6220e;
    }

    /* renamed from: a */
    public void mo22509a(String str) {
        this.f6218c = str;
    }

    /* renamed from: b */
    public String mo22511b() {
        return this.f6218c;
    }

    /* renamed from: a */
    public ParamJsonData mo22506a() {
        return this.f6219d;
    }

    /* renamed from: a */
    public void mo22508a(ParamJsonData fVar) {
        this.f6219d = fVar;
    }

    /* renamed from: a */
    public void mo22510a(boolean z) {
        this.f6220e = z;
    }
}
