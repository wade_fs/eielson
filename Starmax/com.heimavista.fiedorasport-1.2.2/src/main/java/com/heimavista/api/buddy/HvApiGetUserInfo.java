package com.heimavista.api.buddy;

import com.heimavista.api.HvApiResponse;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetUserInfo extends HvApiBaseBuddy {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getUserInfo";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        try {
            UserInfoDb c = UserInfoDb.m13343c(new JSONObject(fVar.mo25325a("UserInfo", "{}")));
            if (c != null) {
                c.mo24181l();
                TickManager.m10526c(getTickKey(), c.mo24232o());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public UserInfoDb requestGetUserSync(String str) {
        HvApiResponse requestSync = super.requestSync(str);
        if (requestSync.mo22515d() != 1) {
            return null;
        }
        try {
            return UserInfoDb.m13343c(new JSONObject(requestSync.mo22506a().mo25325a("UserInfo", "{}")));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
