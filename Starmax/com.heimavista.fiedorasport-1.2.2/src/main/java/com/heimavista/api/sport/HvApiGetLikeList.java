package com.heimavista.api.sport;

import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p193d.p196k.SportLikeUserDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetLikeList extends HvApiBasic {
    public static final String actionFail;
    public static final String actionNewLikes;
    public static final String actionSuccess;

    static {
        Class<HvApiGetLikeList> cls = HvApiGetLikeList.class;
        actionSuccess = cls.getName() + ".success";
        actionFail = cls.getName() + ".fail";
        actionNewLikes = cls.getName() + ".newLikes";
    }

    private HttpParams getHttpParams() {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("lastTime", Long.valueOf(getTick()));
        return cVar;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "sport";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getLikeList";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        super.onApiFailed(cVar, cVar2, str, fVar);
        HvApp.m13010c().mo24157a(actionFail);
        LogUtils.m1139c("op=" + getOp() + " onApiFailed " + str);
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        LogUtils.m1139c("op=" + getOp() + " onApiSuccess");
        List<Object> a = new ParamJsonData(fVar.mo25325a("CntRows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("CntDict", "[]"), true).mo25327a();
        List<Object> a3 = new ParamJsonData(fVar.mo25325a("UserRows", "[]"), true).mo25327a();
        List<Object> a4 = new ParamJsonData(fVar.mo25325a("UserDict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SportDataDb.m13192a(MemberControl.m17125s().mo27187l(), jSONArray.optString(a2.indexOf("sportId")), jSONArray.optInt(a2.indexOf("cnt")), jSONArray.optLong(a2.indexOf("timestamp")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (Object obj2 : a3) {
            try {
                JSONArray jSONArray2 = new JSONArray(String.valueOf(obj2));
                SportLikeUserDb.m13372a(jSONArray2.optString(a4.indexOf("sportId")), jSONArray2.optString(a4.indexOf("userNbr")), jSONArray2.optLong(a4.indexOf("timestamp")));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        if (!a3.isEmpty()) {
            HvApp.m13010c().mo24157a(actionNewLikes);
        } else {
            HvApp.m13010c().mo24157a(actionSuccess);
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }

    public HttpCancelable request() {
        return request((OnResultListener<ParamJsonData>) null);
    }

    public HvApiResponse requestSync() {
        return super.requestSync(getHttpParams());
    }

    public HttpCancelable request(OnResultListener<ParamJsonData> dVar) {
        return super.request(getHttpParams(), dVar);
    }
}
