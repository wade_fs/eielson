package com.heimavista.api.band;

import com.heimavista.api.HvApiResponse;
import com.heimavista.utils.ParamJsonData;
import org.json.JSONArray;
import p119e.p189e.p193d.p194i.StepDetailDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostStepDetail extends HvApiBaseBand {
    private long endTimestamp;
    private long startTimestamp;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    private HttpParams getRequestParams() {
        JSONArray v = new StepDetailDb().mo24282v();
        if (v == null) {
            return null;
        }
        this.startTimestamp = v.optJSONArray(0).optLong(0);
        this.endTimestamp = v.optJSONArray(v.length() - 1).optLong(0);
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("timestamp");
        jSONArray.put("timezone");
        jSONArray.put("type");
        jSONArray.put(TtmlNode.START);
        jSONArray.put(TtmlNode.END);
        jSONArray.put("step");
        jSONArray.put("distance");
        jSONArray.put("cal");
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Dict", (Object) jSONArray.toString());
        httpParamsWithBandId.mo27196a("Data", (Object) v.toString());
        return httpParamsWithBandId;
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "postStepDetail";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        new StepDetailDb().mo24280a(this.startTimestamp, this.endTimestamp);
    }

    public HttpCancelable request() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.request(requestParams);
    }

    public HvApiResponse requestSync() {
        HttpParams requestParams = getRequestParams();
        if (requestParams == null) {
            return null;
        }
        return super.requestSync(requestParams);
    }
}
