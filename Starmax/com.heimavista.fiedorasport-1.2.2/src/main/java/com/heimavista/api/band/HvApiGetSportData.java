package com.heimavista.api.band;

import com.heimavista.entity.band.SportInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSportData extends HvApiGetDataBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSportData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SportInfo dVar = new SportInfo();
                dVar.f6270a = MemberControl.m17125s().mo27187l();
                dVar.f6272c = jSONArray.optString(a2.indexOf("sportId"));
                dVar.f6271b = jSONArray.optString(a2.indexOf("timezone"));
                dVar.f6273d = jSONArray.optInt(a2.indexOf("type"));
                dVar.f6274e = jSONArray.optLong(a2.indexOf(TtmlNode.START));
                dVar.f6275f = jSONArray.optLong(a2.indexOf(TtmlNode.END));
                dVar.f6276g = jSONArray.optInt(a2.indexOf("duration"));
                dVar.f6277h = jSONArray.optInt(a2.indexOf("distance"));
                dVar.f6278i = jSONArray.optInt(a2.indexOf("cal"));
                dVar.f6279j = jSONArray.optInt(a2.indexOf("avgHeartRate"));
                dVar.f6281l = jSONArray.optInt(a2.indexOf("maxHeartRate"));
                dVar.f6280k = jSONArray.optInt(a2.indexOf("minHeartRate"));
                dVar.f6284o = true;
                SportDataDb.m13191a(dVar);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
