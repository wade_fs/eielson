package com.heimavista.api.band;

import com.heimavista.fiedorasport.p199j.TimeUtil;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostCurStepData extends HvApiBaseBand {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "postCurStepData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(long j, int i, int i2, int i3) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("timestamp", j);
            jSONObject.put("timezone", TimeUtil.m13442a());
            jSONObject.put("step", i);
            jSONObject.put("distance", i2);
            jSONObject.put("cal", i3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Data", (Object) jSONObject.toString());
        return super.request(httpParamsWithBandId);
    }
}
