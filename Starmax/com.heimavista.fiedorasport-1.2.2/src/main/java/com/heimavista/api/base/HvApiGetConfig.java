package com.heimavista.api.base;

import com.heimavista.api.HvApiBase;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetConfig extends HvApiBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "base";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getConfig";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }
}
