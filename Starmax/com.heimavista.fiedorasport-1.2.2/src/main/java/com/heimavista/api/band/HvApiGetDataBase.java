package com.heimavista.api.band;

import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiGetDataBase extends HvApiBaseBand {
    /* access modifiers changed from: protected */
    public HttpParams getHttpParamsWithBandId() {
        return new HttpParams();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        super.onApiSuccess(cVar, cVar2, fVar, str);
        if (fVar.mo25323a("isNext", (Integer) 0).intValue() == 1) {
            requestSync();
        }
        TickManager.m10525b(getTickKey(), System.currentTimeMillis());
    }

    public HttpCancelable request() {
        return super.request(getHttpParamsWithTick());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002a, code lost:
        if (r4 < 20) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0036, code lost:
        if ((java.lang.System.currentTimeMillis() - r0) <= 600000) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.heimavista.api.HvApiResponse requestSync() {
        /*
            r7 = this;
            java.lang.String r0 = r7.getTickKey()
            long r0 = com.heimavista.fiedorasport.p109h.TickManager.m10519a(r0)
            r2 = 0
            r3 = 1
            r4 = 0
            int r6 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r6 == 0) goto L_0x0038
            boolean r4 = com.blankj.utilcode.util.TimeUtils.m1007a(r0)
            if (r4 != 0) goto L_0x0017
            goto L_0x0038
        L_0x0017:
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            r4.setTimeInMillis(r0)
            r5 = 11
            int r4 = r4.get(r5)
            r5 = 10
            if (r4 <= r5) goto L_0x002c
            r5 = 20
            if (r4 < r5) goto L_0x0039
        L_0x002c:
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r0
            r0 = 600000(0x927c0, double:2.964394E-318)
            int r6 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r6 <= 0) goto L_0x0039
        L_0x0038:
            r2 = 1
        L_0x0039:
            if (r2 == 0) goto L_0x0044
            e.e.h.c r0 = r7.getHttpParamsWithTick()
            com.heimavista.api.c r0 = super.requestSync(r0)
            return r0
        L_0x0044:
            com.heimavista.api.c r0 = new com.heimavista.api.c
            r0.<init>()
            r0.mo22507a(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.api.band.HvApiGetDataBase.requestSync():com.heimavista.api.c");
    }
}
