package com.heimavista.api.band;

import org.json.JSONArray;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiPostHeartData extends HvApiBaseBand {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "postHeartData";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request() {
        JSONArray u = HeartRateDataDb.m13159u();
        if (u == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("timestamp");
        jSONArray.put("timezone");
        jSONArray.put("heartrate");
        HttpParams httpParamsWithBandId = getHttpParamsWithBandId();
        httpParamsWithBandId.mo27196a("Dict", (Object) jSONArray.toString());
        httpParamsWithBandId.mo27196a("Data", (Object) u.toString());
        return super.request(httpParamsWithBandId);
    }
}
