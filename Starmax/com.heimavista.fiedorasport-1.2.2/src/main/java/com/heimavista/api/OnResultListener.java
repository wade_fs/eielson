package com.heimavista.api;

import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.api.d */
public interface OnResultListener<T> {
    /* renamed from: a */
    void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar);

    /* renamed from: a */
    void mo22380a(Object obj);
}
