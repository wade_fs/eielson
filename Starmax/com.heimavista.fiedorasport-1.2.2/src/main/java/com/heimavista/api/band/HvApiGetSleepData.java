package com.heimavista.api.band;

import com.heimavista.entity.band.SleepInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetSleepData extends HvApiGetDataBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "getSleepData";
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        List<Object> a2 = new ParamJsonData(fVar.mo25325a("Dict", "[]"), true).mo25327a();
        for (Object obj : a) {
            try {
                JSONArray jSONArray = new JSONArray(String.valueOf(obj));
                SleepInfo cVar3 = new SleepInfo();
                cVar3.f6262a = MemberControl.m17125s().mo27187l();
                cVar3.f6263b = jSONArray.optLong(a2.indexOf("timestamp"));
                cVar3.f6264c = jSONArray.optString(a2.indexOf("timezone"));
                cVar3.f6265d = jSONArray.optInt(a2.indexOf("type"));
                cVar3.f6266e = jSONArray.optLong(a2.indexOf(TtmlNode.START));
                cVar3.f6267f = jSONArray.optLong(a2.indexOf(TtmlNode.END));
                cVar3.f6268g = jSONArray.optInt(a2.indexOf("sleep"));
                cVar3.f6269h = true;
                SleepDataDb.m13178a(cVar3);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onApiSuccess(cVar, cVar2, fVar, str);
    }
}
