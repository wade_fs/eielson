package com.heimavista.api.band;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiSetShareStat extends HvApiBaseBand {
    /* access modifiers changed from: protected */
    public String getOp() {
        return "setShareStat";
    }

    public HttpCancelable setShareLoc(boolean z, OnResultListener<ParamJsonData> dVar) {
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("isShareLoc", Integer.valueOf(z ? 1 : 0));
        return super.request(cVar, dVar);
    }
}
