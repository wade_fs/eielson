package com.heimavista.zbar;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.exoplayer2.util.MimeTypes;
import com.yanzhenjie.zbar.camera.CameraPreview;
import com.yanzhenjie.zbar.camera.ScanCallback;
import com.zbar.lib.R$id;
import com.zbar.lib.R$layout;
import com.zbar.lib.R$raw;
import java.io.IOException;

public class CaptureActivity extends AppCompatActivity implements ScanCallback {

    /* renamed from: P */
    private MediaPlayer f9506P;

    /* renamed from: Q */
    private boolean f9507Q;

    /* renamed from: R */
    private boolean f9508R;

    /* renamed from: S */
    private CameraPreview f9509S;

    /* renamed from: com.heimavista.zbar.CaptureActivity$a */
    class C4633a implements MediaPlayer.OnCompletionListener {
        C4633a(CaptureActivity captureActivity) {
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    }

    /* renamed from: a */
    private boolean m15445a() {
        if (Build.VERSION.SDK_INT < 23 || checkSelfPermission("android.permission.CAMERA") == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: b */
    private void m15446b() {
        if (this.f9507Q && this.f9506P == null) {
            setVolumeControlStream(3);
            this.f9506P = new MediaPlayer();
            this.f9506P.setAudioStreamType(3);
            this.f9506P.setOnCompletionListener(new C4633a(this));
            AssetFileDescriptor openRawResourceFd = getResources().openRawResourceFd(R$raw.beep);
            try {
                this.f9506P.setDataSource(openRawResourceFd.getFileDescriptor(), openRawResourceFd.getStartOffset(), openRawResourceFd.getLength());
                openRawResourceFd.close();
                this.f9506P.setVolume(0.5f, 0.5f);
                this.f9506P.prepare();
            } catch (IOException unused) {
                this.f9506P = null;
            }
        }
    }

    /* renamed from: c */
    private void m15447c() {
        Vibrator vibrator;
        MediaPlayer mediaPlayer;
        if (this.f9507Q && (mediaPlayer = this.f9506P) != null) {
            mediaPlayer.start();
        }
        if (this.f9508R && (vibrator = (Vibrator) getSystemService("vibrator")) != null) {
            vibrator.vibrate(200);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.activity_qr_scan);
        if (!m15445a()) {
            finish();
        }
        this.f9509S = (CameraPreview) findViewById(R$id.capture_preview);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 2, 0.0f, 2, 0.9f);
        translateAnimation.setDuration(1500);
        translateAnimation.setRepeatCount(-1);
        translateAnimation.setRepeatMode(2);
        translateAnimation.setInterpolator(new LinearInterpolator());
        ((ImageView) findViewById(R$id.capture_scan_line)).setAnimation(translateAnimation);
        this.f9509S.setScanCallback(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.f9509S.stop();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f9509S.start();
        this.f9507Q = true;
        AudioManager audioManager = (AudioManager) getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            if (audioManager.getRingerMode() != 2) {
                this.f9507Q = false;
            }
            m15446b();
            this.f9508R = true;
        }
    }

    public void onScanResult(String str) {
        this.f9509S.stop();
        m15447c();
        Intent intent = new Intent();
        intent.putExtra("content", str);
        setResult(-1, intent);
        finish();
    }
}
