package com.heimavista.zbar;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.HashMap;
import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.WriterException;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p186o.QRCodeWriter;
import p119e.p144d.p176c.p186o.p187b.ErrorCorrectionLevel;

/* renamed from: com.heimavista.zbar.a */
public class QRCodeUtils {
    /* renamed from: a */
    public static Bitmap m15449a(String str, int i) {
        return m15450a(str, i, null);
    }

    /* renamed from: a */
    public static Bitmap m15450a(String str, int i, Bitmap bitmap) {
        HashMap hashMap = new HashMap();
        hashMap.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.f7931T);
        hashMap.put(EncodeHintType.MARGIN, 1);
        return m15451a(str, i, bitmap, hashMap);
    }

    /* renamed from: a */
    public static Bitmap m15451a(String str, int i, Bitmap bitmap, Map<EncodeHintType, ?> map) {
        try {
            BitMatrix a = new QRCodeWriter().mo23990a(str, BarcodeFormat.QR_CODE, i, i, map);
            int[] iArr = new int[(i * i)];
            for (int i2 = 0; i2 < i; i2++) {
                for (int i3 = 0; i3 < i; i3++) {
                    if (a.mo24032a(i3, i2)) {
                        iArr[(i2 * i) + i3] = -16777216;
                    } else {
                        iArr[(i2 * i) + i3] = -1;
                    }
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
            createBitmap.setPixels(iArr, 0, i, 0, 0, i, i);
            return bitmap != null ? m15448a(createBitmap, bitmap) : createBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    private static Bitmap m15448a(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap2 == null) {
            return bitmap;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int width2 = bitmap2.getWidth();
        int height2 = bitmap2.getHeight();
        if (width == 0 || height == 0) {
            return null;
        }
        if (width2 == 0 || height2 == 0) {
            return bitmap;
        }
        float f = ((((float) width) * 1.0f) / 6.0f) / ((float) width2);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        try {
            Canvas canvas = new Canvas(createBitmap);
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
            canvas.scale(f, f, (float) (width / 2), (float) (height / 2));
            canvas.drawBitmap(bitmap2, (float) ((width - width2) / 2), (float) ((height - height2) / 2), (Paint) null);
            canvas.save();
            canvas.restore();
            return createBitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
