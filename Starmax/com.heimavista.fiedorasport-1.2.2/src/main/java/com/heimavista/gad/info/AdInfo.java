package com.heimavista.gad.info;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

public class AdInfo implements Parcelable {
    public static final Parcelable.Creator<AdInfo> CREATOR = new C4540a();

    /* renamed from: P */
    public int f9169P;

    /* renamed from: Q */
    public String f9170Q;

    /* renamed from: R */
    public String f9171R;

    /* renamed from: S */
    public String f9172S;

    /* renamed from: T */
    public int f9173T;

    /* renamed from: U */
    public String f9174U;

    /* renamed from: V */
    public String f9175V;

    /* renamed from: W */
    public ResInfo f9176W;

    /* renamed from: com.heimavista.gad.info.AdInfo$a */
    static class C4540a implements Parcelable.Creator<AdInfo> {
        C4540a() {
        }

        public AdInfo createFromParcel(Parcel parcel) {
            return new AdInfo(parcel);
        }

        public AdInfo[] newArray(int i) {
            return new AdInfo[i];
        }
    }

    public AdInfo() {
    }

    /* renamed from: a */
    public static AdInfo m14877a(String str) {
        AdInfo adInfo = new AdInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            adInfo.f9169P = jSONObject.optInt("seq", 0);
            adInfo.f9170Q = jSONObject.optString("pos", "");
            adInfo.f9171R = jSONObject.optString("acId", "");
            adInfo.f9172S = jSONObject.optString("adId", "");
            adInfo.f9173T = jSONObject.optInt("isAd", 0);
            adInfo.f9174U = jSONObject.optString(TtmlNode.START, "");
            adInfo.f9175V = jSONObject.optString(TtmlNode.END, "");
            adInfo.f9176W = ResInfo.m14879a(jSONObject.optString("ResInfo", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adInfo;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof AdInfo) || ((AdInfo) obj).f9169P != this.f9169P) {
            return false;
        }
        return true;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f9169P);
        parcel.writeString(this.f9170Q);
        parcel.writeString(this.f9171R);
        parcel.writeString(this.f9172S);
        parcel.writeInt(this.f9173T);
        parcel.writeString(this.f9174U);
        parcel.writeString(this.f9175V);
        parcel.writeString(this.f9176W.mo24982b());
    }

    protected AdInfo(Parcel parcel) {
        this.f9169P = parcel.readInt();
        this.f9170Q = parcel.readString();
        this.f9171R = parcel.readString();
        this.f9172S = parcel.readString();
        this.f9173T = parcel.readInt();
        this.f9174U = parcel.readString();
        this.f9175V = parcel.readString();
        this.f9176W = ResInfo.m14879a(parcel.readString());
    }
}
