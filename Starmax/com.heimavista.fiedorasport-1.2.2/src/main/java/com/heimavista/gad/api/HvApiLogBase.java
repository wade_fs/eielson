package com.heimavista.gad.api;

import com.heimavista.gad.HvGad;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiLogBase extends HvApiGadBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "log";
    }

    public HttpParams getLogParams(HttpParams cVar) {
        return genRequestParams(cVar);
    }

    /* access modifiers changed from: protected */
    public String getUri() {
        return HvGad.m14915i().mo25011c().f9254b;
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }
}
