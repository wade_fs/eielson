package com.heimavista.gad.gui;

import android.view.View;
import com.heimavista.gad.info.AdInfo;

/* renamed from: com.heimavista.gad.gui.h */
/* compiled from: lambda */
public final /* synthetic */ class C4527h implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ResBaseFragment f9149P;

    /* renamed from: Q */
    private final /* synthetic */ AdInfo f9150Q;

    public /* synthetic */ C4527h(ResBaseFragment resBaseFragment, AdInfo adInfo) {
        this.f9149P = resBaseFragment;
        this.f9150Q = adInfo;
    }

    public final void onClick(View view) {
        this.f9149P.mo24948a(this.f9150Q, view);
    }
}
