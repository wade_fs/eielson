package com.heimavista.gad.gui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.R$string;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.utils.RandomUtils;
import java.io.File;
import p019cn.jzvd.JZDataSource;
import p019cn.jzvd.Jzvd;
import p019cn.jzvd.media.JZMediaExo;
import p119e.p189e.p217f.GAManage;

public class Ex2ResVideoFragment extends Ex2ResBaseFragment {

    /* renamed from: X */
    private JzvdStdGad f9103X;

    /* renamed from: a */
    static /* synthetic */ boolean m14787a(View view, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 1) {
            return Jzvd.backPress();
        }
        return false;
    }

    /* renamed from: c */
    private void m14788c() {
        Ex2AdInfo ex2AdInfo = super.f9094P;
        if (ex2AdInfo != null && ex2AdInfo.f9184W != null) {
            Jzvd.releaseAllVideos();
            JzvdStdGad jzvdStdGad = this.f9103X;
            if (jzvdStdGad != null) {
                long logDuration = jzvdStdGad.getLogDuration();
                int i = super.f9094P.f9184W.f9191V;
                if (logDuration > ((long) i)) {
                    logDuration = (long) i;
                }
                HvGadLogManage d = HvGadLogManage.m14968d();
                LogInfo dVar = new LogInfo();
                dVar.mo24993a(super.f9094P);
                dVar.mo24991a(logDuration);
                d.mo25033b(dVar);
                GAManage a = GAManage.m17122a();
                String string = getString(R$string.hv_gad_ga_cg_view);
                Ex2AdInfo ex2AdInfo2 = super.f9094P;
                a.mo27171a(string, ex2AdInfo2.f9178Q, ex2AdInfo2.f9180S, Long.valueOf(logDuration));
            }
        }
    }

    /* renamed from: d */
    private void m14789d() {
        JzvdStdGad jzvdStdGad = this.f9103X;
        if (jzvdStdGad != null) {
            jzvdStdGad.startVideo();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_video, viewGroup, false);
        String[] a = super.f9094P.f9184W.mo24981a();
        String a2 = ResInfo.m14881a(getContext(), super.f9094P.f9184W.f9185P, a[RandomUtils.m15262a(a.length - 1)]);
        Context context = getContext();
        ResInfo resInfo = super.f9094P.f9184W;
        String a3 = ResInfo.m14881a(context, resInfo.f9185P, resInfo.f9192W);
        int[] b = mo24927b(inflate);
        JZDataSource jZDataSource = new JZDataSource(a2);
        jZDataSource.looping = true;
        jZDataSource.mute = true;
        this.f9103X = (JzvdStdGad) inflate.findViewById(R$id.videoplayer);
        ViewGroup.LayoutParams layoutParams = this.f9103X.getLayoutParams();
        layoutParams.width = b[0];
        layoutParams.height = b[1];
        ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) this.f9103X.getParent()).getLayoutParams();
        layoutParams2.width = b[0];
        layoutParams2.height = b[1];
        this.f9103X.setUp(jZDataSource, 0, JZMediaExo.class);
        Jzvd.setVideoImageDisplayType(2);
        this.f9103X.thumbImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.f9103X.setOnClickUiToggleListener(new C4531l(this));
        this.f9103X.thumbImageView.setImageURI(Uri.fromFile(new File(a3)));
        mo24923a(inflate);
        return inflate;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (isVisible()) {
            m14789d();
        } else {
            m14788c();
        }
    }

    public void onPause() {
        super.onPause();
        m14788c();
    }

    public void onResume() {
        super.onResume();
        m14789d();
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.setFocusable(true);
            view.requestFocus();
            view.setOnKeyListener(C4522c.f9143P);
        }
    }
}
