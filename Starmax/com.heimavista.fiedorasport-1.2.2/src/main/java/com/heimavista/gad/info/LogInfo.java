package com.heimavista.gad.info;

import android.os.Build;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.NetworkUtils;
import com.heimavista.gad.HvGad;
import java.util.Locale;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.info.d */
public class LogInfo {

    /* renamed from: a */
    private String f9218a;

    /* renamed from: b */
    private String f9219b;

    /* renamed from: c */
    private String f9220c;

    /* renamed from: d */
    private String f9221d;

    /* renamed from: e */
    private Integer f9222e;

    /* renamed from: f */
    private String f9223f;

    /* renamed from: g */
    private Long f9224g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, int]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* renamed from: c */
    private static HttpParams m14898c() {
        int i;
        NetworkUtils.C0853a b = NetworkUtils.m857b();
        if (NetworkUtils.C0853a.NETWORK_WIFI.equals(b)) {
            i = 1;
        } else if (NetworkUtils.C0853a.NETWORK_2G.equals(b)) {
            i = 2;
        } else if (NetworkUtils.C0853a.NETWORK_3G.equals(b)) {
            i = 3;
        } else {
            i = NetworkUtils.C0853a.NETWORK_4G.equals(b) ? 4 : 0;
        }
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("os", (Object) 2);
        cVar.mo27196a("osv", (Object) Build.VERSION.RELEASE);
        cVar.mo27196a("lang", (Object) Locale.getDefault().getLanguage());
        cVar.mo27196a("brand", (Object) Build.BRAND);
        cVar.mo27196a("model", (Object) Build.MODEL);
        cVar.mo27196a("conn", Integer.valueOf(i));
        return cVar;
    }

    /* renamed from: a */
    public LogInfo mo24992a(AdInfo adInfo) {
        this.f9218a = adInfo.f9171R;
        this.f9219b = adInfo.f9176W.f9185P;
        this.f9220c = adInfo.f9172S;
        this.f9221d = adInfo.f9170Q;
        return this;
    }

    /* renamed from: b */
    public JSONObject mo24996b() {
        return mo24995a().mo27203c();
    }

    @NonNull
    public String toString() {
        return mo24996b().toString();
    }

    /* renamed from: a */
    public LogInfo mo24994a(ExAdInfo bVar) {
        this.f9218a = bVar.f9200a;
        this.f9219b = bVar.f9208i.f9209a;
        this.f9220c = bVar.f9204e;
        this.f9221d = bVar.f9202c;
        this.f9222e = 1;
        this.f9223f = bVar.f9203d;
        return this;
    }

    /* renamed from: a */
    public LogInfo mo24993a(Ex2AdInfo ex2AdInfo) {
        this.f9218a = ex2AdInfo.f9179R;
        this.f9219b = ex2AdInfo.f9184W.f9185P;
        this.f9220c = ex2AdInfo.f9180S;
        this.f9221d = ex2AdInfo.f9178Q;
        return this;
    }

    /* renamed from: a */
    public LogInfo mo24991a(long j) {
        this.f9224g = Long.valueOf(j);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* renamed from: a */
    public HttpParams mo24995a() {
        HttpParams c = m14898c();
        String str = this.f9218a;
        if (str != null) {
            c.mo27196a("acId", (Object) str);
        }
        String str2 = this.f9219b;
        if (str2 != null) {
            c.mo27196a("resId", (Object) str2);
        }
        String str3 = this.f9220c;
        if (str3 != null) {
            c.mo27196a("adId", (Object) str3);
        }
        String str4 = this.f9221d;
        if (str4 != null) {
            c.mo27196a("pos", (Object) str4);
        }
        Integer num = this.f9222e;
        if (num != null) {
            c.mo27196a("ex", num);
        }
        String str5 = this.f9223f;
        if (str5 != null) {
            c.mo27196a("exAcId", (Object) str5);
        }
        Long l = this.f9224g;
        if (l != null) {
            c.mo27196a("duration", l);
        }
        if (HvGad.m14915i().mo25014d().mo25023a()) {
            c.mo27196a("userNbr", (Object) HvGad.m14915i().mo25014d().mo25024b());
        }
        return c;
    }
}
