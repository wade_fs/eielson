package com.heimavista.gad.api.gad;

import com.heimavista.api.OnResultListener;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.info.ExAdInfo;
import com.heimavista.gad.p202u.ExRes;
import com.heimavista.gad.p202u.Like;
import com.heimavista.gad.p202u.Res;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiAddLike extends HvApiGadBase {
    private OnResultListener<Integer> onResultListener;

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "addLike";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        OnResultListener<Integer> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22379a(cVar, cVar2, str, fVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("resId", "");
        Like fVar2 = new Like();
        fVar2.mo25133h(cVar.mo27198a("acId", ""));
        fVar2.mo25135j(a);
        Res hVar = new Res();
        hVar.mo25145h(a);
        if (hVar.mo24202e()) {
            hVar.mo24206j();
            fVar2.mo25134i(hVar.mo25147p());
        } else {
            ExRes eVar = new ExRes();
            eVar.mo25129h(a);
            eVar.mo24206j();
            fVar2.mo25134i(hVar.mo25147p());
        }
        fVar2.mo24181l();
        OnResultListener<Integer> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(fVar.mo25323a("likeCnt", (Integer) 0));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(AdInfo adInfo, OnResultListener<Integer> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("resId", (Object) adInfo.f9176W.f9185P);
        cVar.mo27196a("acId", (Object) adInfo.f9171R);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(ExAdInfo bVar, OnResultListener<Integer> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("resId", (Object) bVar.f9208i.f9209a);
        cVar.mo27196a("acId", (Object) bVar.f9203d);
        return request(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable request(Ex2AdInfo ex2AdInfo, OnResultListener<Integer> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("resId", (Object) ex2AdInfo.f9184W.f9185P);
        cVar.mo27196a("acId", (Object) ex2AdInfo.f9179R);
        return request(cVar);
    }
}
