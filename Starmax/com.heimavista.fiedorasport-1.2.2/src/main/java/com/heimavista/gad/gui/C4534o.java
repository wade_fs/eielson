package com.heimavista.gad.gui;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.gui.o */
/* compiled from: ExAdFragment */
class C4534o implements OnResultListener<Integer> {

    /* renamed from: a */
    final /* synthetic */ ExAdFragment f9157a;

    C4534o(ExAdFragment exAdFragment) {
        this.f9157a = exAdFragment;
    }

    /* renamed from: a */
    public void mo22380a(Integer num) {
        int unused = this.f9157a.f9116b0 = num.intValue();
        this.f9157a.m14814h();
        this.f9157a.f9110V.setEnabled(true);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f9157a.m14812g();
        this.f9157a.m14818j();
        this.f9157a.f9110V.setEnabled(true);
    }
}
