package com.heimavista.gad.info;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class Ex2AdInfo implements Parcelable {
    public static final Parcelable.Creator<AdInfo> CREATOR = new C4541a();

    /* renamed from: P */
    public int f9177P;

    /* renamed from: Q */
    public String f9178Q;

    /* renamed from: R */
    public String f9179R;

    /* renamed from: S */
    public String f9180S;

    /* renamed from: T */
    public int f9181T;

    /* renamed from: U */
    public String f9182U;

    /* renamed from: V */
    public String f9183V;

    /* renamed from: W */
    public ResInfo f9184W;

    /* renamed from: com.heimavista.gad.info.Ex2AdInfo$a */
    static class C4541a implements Parcelable.Creator<AdInfo> {
        C4541a() {
        }

        public AdInfo createFromParcel(Parcel parcel) {
            return new AdInfo(parcel);
        }

        public AdInfo[] newArray(int i) {
            return new AdInfo[i];
        }
    }

    /* renamed from: a */
    public static Ex2AdInfo m14878a(String str) {
        Ex2AdInfo ex2AdInfo = new Ex2AdInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            ex2AdInfo.f9177P = jSONObject.optInt("seq", 0);
            ex2AdInfo.f9178Q = jSONObject.optString("pos", "");
            ex2AdInfo.f9179R = jSONObject.optString("acId", "");
            ex2AdInfo.f9180S = jSONObject.optString("adId", "");
            ex2AdInfo.f9181T = jSONObject.optInt("isAd", 0);
            ex2AdInfo.f9182U = jSONObject.optString(TtmlNode.START, "");
            ex2AdInfo.f9183V = jSONObject.optString(TtmlNode.END, "");
            ex2AdInfo.f9184W = ResInfo.m14879a(jSONObject.optString("ResInfo", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ex2AdInfo;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f9177P);
        parcel.writeString(this.f9178Q);
        parcel.writeString(this.f9179R);
        parcel.writeString(this.f9180S);
        parcel.writeInt(this.f9181T);
        parcel.writeString(this.f9182U);
        parcel.writeString(this.f9183V);
        parcel.writeString(this.f9184W.mo24982b());
    }
}
