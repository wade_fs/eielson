package com.heimavista.gad.api.gad;

import com.heimavista.gad.HvGad;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Like;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetLikeList extends HvApiGadBase {
    private static boolean isRequest;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, int):int
      com.blankj.utilcode.util.w.a(java.lang.String, long):long
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String):java.lang.String
      com.blankj.utilcode.util.w.a(java.lang.String, boolean):boolean */
    public static void requestOnce() {
        if (!isRequest) {
            isRequest = true;
            if (!HvGad.m14915i().mo25015e().mo9879a("hasGetLikeList", false)) {
                new HvApiGetLikeList().request(null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getLikeList";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        isRequest = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.w.b(java.lang.String, int):void
      com.blankj.utilcode.util.w.b(java.lang.String, long):void
      com.blankj.utilcode.util.w.b(java.lang.String, java.lang.String):void
      com.blankj.utilcode.util.w.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        List<Object> a = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
        new Like().mo24182n();
        for (Object obj : a) {
            ParamJsonData fVar2 = new ParamJsonData(String.valueOf(obj), true);
            String a2 = fVar2.mo25325a("acId", "");
            String a3 = fVar2.mo25325a("ResInfo", "{}");
            ResInfo a4 = ResInfo.m14879a(a3);
            Like fVar3 = new Like();
            fVar3.mo25133h(a2);
            fVar3.mo25135j(a4.f9185P);
            fVar3.mo25134i(a3);
            fVar3.mo24181l();
        }
        HvGad.m14915i().mo25015e().mo9884b("hasGetLikeList", true);
    }
}
