package com.heimavista.gad;

import com.heimavista.gad.api.gad.HvApiGetServiceUrl;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.gad.p */
public abstract class HvGadDelegateSimple implements HvGadDelegate {
    /* renamed from: a */
    public boolean mo25023a() {
        return MemberControl.m17125s().mo27192q();
    }

    /* renamed from: b */
    public String mo25024b() {
        return MemberControl.m17125s().mo27187l();
    }

    /* renamed from: c */
    public HvGadConfig mo25025c() {
        return new HvApiGetServiceUrl().requestSync();
    }
}
