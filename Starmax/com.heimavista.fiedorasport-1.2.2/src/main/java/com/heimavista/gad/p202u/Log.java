package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.HvGad;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.g */
public class Log extends DbObjectBase {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.w.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.blankj.utilcode.util.w.a(java.lang.String, int):int
      com.blankj.utilcode.util.w.a(java.lang.String, java.lang.String):java.lang.String
      com.blankj.utilcode.util.w.a(java.lang.String, boolean):boolean
      com.blankj.utilcode.util.w.a(java.lang.String, long):long */
    /* renamed from: q */
    public static synchronized long m15108q() {
        long j;
        synchronized (Log.class) {
            long a = HvGad.m14915i().mo25015e().mo9870a("LogSeq", 0L);
            if (a == Long.MAX_VALUE) {
                a = 0;
            }
            j = a + 1;
            HvGad.m14915i().mo25015e().mo9882b("LogSeq", j);
        }
        return j;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "gad_log_mstr";
    }

    /* renamed from: a */
    public void mo25137a(long j) {
        mo24194a("gad_log_seq", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.g.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_log_seq", OrmColumnType.LONG, (Object) 0);
        mo24173a("gad_log_type", OrmColumnType.STRING, "");
        mo24173a("gad_log_info", OrmColumnType.STRING, "");
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_log_seq"};
    }

    /* renamed from: i */
    public void mo25139i(String str) {
        mo24195a("gad_log_type", str);
    }

    /* renamed from: o */
    public String mo24232o() {
        return mo24203f("gad_log_info");
    }

    /* renamed from: p */
    public String mo25140p() {
        return mo24203f("gad_log_type");
    }

    /* renamed from: a */
    public List<Log> mo25136a(int i) {
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : mo24183g().mo24216a(this, null, null, null, "gad_log_seq asc", 0, Integer.valueOf(i))) {
            Log gVar = new Log();
            gVar.mo24196a(map);
            arrayList.add(gVar);
        }
        return arrayList;
    }

    /* renamed from: h */
    public void mo25138h(String str) {
        mo24195a("gad_log_info", str);
    }

    /* renamed from: b */
    public static long m15107b(String str, String str2) {
        Log gVar = new Log();
        gVar.mo25137a(m15108q());
        gVar.mo25139i(str);
        gVar.mo25138h(str2);
        return gVar.mo24181l();
    }
}
