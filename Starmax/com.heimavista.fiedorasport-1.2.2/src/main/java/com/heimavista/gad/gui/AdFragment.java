package com.heimavista.gad.gui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadDownloadManage;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Ad;
import com.heimavista.gad.p202u.Res;
import java.util.ArrayList;
import java.util.List;

public class AdFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public String f9076P;

    /* renamed from: Q */
    private boolean f9077Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public AdInfo f9078R;

    /* renamed from: S */
    private HvGadViewConfig f9079S;

    /* renamed from: T */
    private LocalBroadcastManager f9080T;

    /* renamed from: U */
    private BroadcastReceiver f9081U;

    /* renamed from: V */
    private long f9082V;

    /* renamed from: W */
    private View f9083W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public boolean f9084X = false;

    /* renamed from: com.heimavista.gad.gui.AdFragment$a */
    class C4510a extends BroadcastReceiver {
        C4510a() {
        }

        public void onReceive(Context context, Intent intent) {
            AdInfo adInfo;
            if (HvGadDownloadManage.f9257g.equals(intent.getAction())) {
                if (AdFragment.this.f9078R == null) {
                    String stringExtra = intent.getStringExtra("resId");
                    if (!TextUtils.isEmpty(stringExtra) && new Ad().mo25057a(HvGad.m14915i().mo25000a(), AdFragment.this.f9076P, false, stringExtra).size() > 0) {
                        AdFragment.this.m14753f();
                    }
                }
            } else if (HvGad.f9227h.equals(intent.getAction())) {
                AdFragment.this.m14753f();
            } else if (ResBaseFragment.f9128Y.equals(intent.getAction()) && (adInfo = (AdInfo) intent.getParcelableExtra(AdInfo.class.getCanonicalName())) != null && adInfo.equals(AdFragment.this.f9078R)) {
                boolean unused = AdFragment.this.f9084X = true;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.gad.u.b.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.b>
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.heimavista.gad.u.b.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.heimavista.gad.u.b.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.b> */
    /* renamed from: d */
    private AdInfo m14751d() {
        List<Ad> a = new Ad().mo25056a(HvGad.m14915i().mo25000a(), this.f9076P, (Boolean) false);
        if (a.size() == 0) {
            a = new Ad().mo25056a(HvGad.m14915i().mo25000a(), this.f9076P, (Boolean) true);
        }
        ArrayList arrayList = new ArrayList();
        SPUtils e = HvGad.m14915i().mo25015e();
        String a2 = e.mo9871a("last_ad_id_" + this.f9076P);
        AdInfo adInfo = null;
        for (Ad bVar : a) {
            AdInfo q = bVar.mo25071q();
            ResInfo resInfo = q.f9176W;
            if (!resInfo.mo24980a(getContext())) {
                Res hVar = new Res();
                hVar.mo25145h(resInfo.f9185P);
                hVar.mo25143a(false);
                hVar.mo24208m();
                HvGadDownloadManage.m14960g().mo25028c();
            } else if (q.f9172S.equals(a2)) {
                adInfo = q;
            } else {
                arrayList.add(q);
            }
        }
        if (arrayList.size() == 0 && adInfo != null) {
            arrayList.add(adInfo);
        }
        if (arrayList.size() > 0) {
            return (AdInfo) arrayList.get((int) (System.currentTimeMillis() % ((long) arrayList.size())));
        }
        return null;
    }

    /* renamed from: e */
    private void m14752e() {
        if (this.f9080T != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(HvGadDownloadManage.f9257g);
            intentFilter.addAction(HvGad.f9227h);
            intentFilter.addAction(ResBaseFragment.f9128Y);
            this.f9080T.registerReceiver(this.f9081U, intentFilter);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m14753f() {
        /*
            r6 = this;
            com.heimavista.gad.info.AdInfo r0 = r6.m14751d()
            r6.f9078R = r0
            r0 = 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r2 = 0
            java.lang.String r3 = "resetAdInfo"
            r1[r2] = r3
            com.heimavista.gad.info.AdInfo r3 = r6.f9078R
            r4 = 1
            r1[r4] = r3
            com.blankj.utilcode.util.LogUtils.m1123a(r1)
            com.heimavista.gad.info.AdInfo r1 = r6.f9078R
            r3 = 0
            if (r1 == 0) goto L_0x006c
            com.heimavista.gad.info.ResInfo r1 = r1.f9176W
            if (r1 == 0) goto L_0x006c
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "resetResInfo"
            r0[r2] = r5
            r0[r4] = r1
            com.blankj.utilcode.util.LogUtils.m1123a(r0)
            com.heimavista.gad.info.AdInfo r0 = r6.f9078R
            com.heimavista.gad.info.ResInfo r0 = r0.f9176W
            java.lang.String r0 = r0.f9186Q
            java.lang.String r1 = "image"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004b
            com.heimavista.gad.gui.ResImageFragment r0 = new com.heimavista.gad.gui.ResImageFragment
            r0.<init>()
            java.lang.String r1 = r6.f9076P
            com.heimavista.gad.HvGadViewConfig r2 = r6.f9079S
            com.heimavista.gad.info.AdInfo r4 = r6.f9078R
            android.os.Bundle r1 = com.heimavista.gad.gui.ResBaseFragment.m14833a(r1, r2, r4)
            r0.setArguments(r1)
            goto L_0x006d
        L_0x004b:
            com.heimavista.gad.info.AdInfo r0 = r6.f9078R
            com.heimavista.gad.info.ResInfo r0 = r0.f9176W
            java.lang.String r0 = r0.f9186Q
            java.lang.String r1 = "video"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x006c
            com.heimavista.gad.gui.ResVideoFragment r0 = new com.heimavista.gad.gui.ResVideoFragment
            r0.<init>()
            java.lang.String r1 = r6.f9076P
            com.heimavista.gad.HvGadViewConfig r2 = r6.f9079S
            com.heimavista.gad.info.AdInfo r4 = r6.f9078R
            android.os.Bundle r1 = com.heimavista.gad.gui.ResBaseFragment.m14833a(r1, r2, r4)
            r0.setArguments(r1)
            goto L_0x006d
        L_0x006c:
            r0 = r3
        L_0x006d:
            if (r0 != 0) goto L_0x0086
            r6.f9078R = r3
            boolean r1 = r6.f9077Q
            if (r1 != 0) goto L_0x00b2
            com.heimavista.gad.gui.ResDefFragment r0 = new com.heimavista.gad.gui.ResDefFragment
            r0.<init>()
            java.lang.String r1 = r6.f9076P
            com.heimavista.gad.HvGadViewConfig r2 = r6.f9079S
            android.os.Bundle r1 = com.heimavista.gad.gui.ResBaseFragment.m14833a(r1, r2, r3)
            r0.setArguments(r1)
            goto L_0x00b2
        L_0x0086:
            com.heimavista.gad.info.AdInfo r1 = r6.f9078R
            if (r1 == 0) goto L_0x00b2
            long r1 = java.lang.System.currentTimeMillis()
            r6.f9082V = r1
            com.heimavista.gad.l r1 = com.heimavista.gad.HvGad.m14915i()
            com.blankj.utilcode.util.w r1 = r1.mo25015e()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "last_ad_id_"
            r2.append(r3)
            java.lang.String r3 = r6.f9076P
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.heimavista.gad.info.AdInfo r3 = r6.f9078R
            java.lang.String r3 = r3.f9172S
            r1.mo9883b(r2, r3)
        L_0x00b2:
            if (r0 == 0) goto L_0x00c5
            androidx.fragment.app.FragmentManager r1 = r6.getChildFragmentManager()
            androidx.fragment.app.FragmentTransaction r1 = r1.beginTransaction()
            int r2 = com.heimavista.gad.R$id.ll_ad
            androidx.fragment.app.FragmentTransaction r0 = r1.replace(r2, r0)
            r0.commit()
        L_0x00c5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.gad.gui.AdFragment.m14753f():void");
    }

    /* renamed from: g */
    private void m14754g() {
        LocalBroadcastManager localBroadcastManager = this.f9080T;
        if (localBroadcastManager != null) {
            localBroadcastManager.unregisterReceiver(this.f9081U);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9080T = LocalBroadcastManager.getInstance(context);
        this.f9081U = new C4510a();
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f9076P = arguments.getString("pos", "");
            this.f9077Q = arguments.getBoolean("isEx", false);
            this.f9079S = (HvGadViewConfig) arguments.getParcelable(HvGadViewConfig.class.getCanonicalName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        if (this.f9083W == null) {
            this.f9083W = layoutInflater.inflate(R$layout.hv_gad_ad, viewGroup, false);
        }
        return this.f9083W;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (isVisible()) {
            m14749c();
        } else {
            m14748b();
        }
    }

    public void onPause() {
        super.onPause();
        m14748b();
    }

    public void onResume() {
        super.onResume();
        m14749c();
    }

    /* renamed from: b */
    private void m14748b() {
        m14754g();
    }

    /* renamed from: c */
    private void m14749c() {
        HvGadDownloadManage.m14960g().mo25027b();
        m14752e();
        if (this.f9084X) {
            this.f9084X = false;
            this.f9082V = System.currentTimeMillis();
        } else if (this.f9078R == null || System.currentTimeMillis() - this.f9082V >= 30000) {
            m14753f();
        }
    }

    /* renamed from: a */
    public static Bundle m14744a(String str, HvGadViewConfig hvGadViewConfig, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putString("pos", str);
        bundle.putBoolean("isEx", z);
        bundle.putParcelable(HvGadViewConfig.class.getCanonicalName(), hvGadViewConfig);
        return bundle;
    }
}
