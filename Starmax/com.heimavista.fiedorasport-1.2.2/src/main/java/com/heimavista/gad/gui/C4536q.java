package com.heimavista.gad.gui;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.gui.q */
/* compiled from: ResBaseFragment */
class C4536q implements OnResultListener<Integer> {

    /* renamed from: a */
    final /* synthetic */ ResBaseFragment f9159a;

    C4536q(ResBaseFragment resBaseFragment) {
        this.f9159a = resBaseFragment;
    }

    /* renamed from: a */
    public void mo22380a(Integer num) {
        int unused = this.f9159a.f9137X = num.intValue();
        this.f9159a.m14840d();
        this.f9159a.f9132S.setEnabled(true);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f9159a.m14838c();
        this.f9159a.m14842e();
        this.f9159a.f9132S.setEnabled(true);
    }
}
