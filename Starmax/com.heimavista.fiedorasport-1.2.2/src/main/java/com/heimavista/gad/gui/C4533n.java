package com.heimavista.gad.gui;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.gui.n */
/* compiled from: Ex2ResBaseFragment */
class C4533n implements OnResultListener<Integer> {

    /* renamed from: a */
    final /* synthetic */ Ex2ResBaseFragment f9156a;

    C4533n(Ex2ResBaseFragment ex2ResBaseFragment) {
        this.f9156a = ex2ResBaseFragment;
    }

    /* renamed from: a */
    public void mo22380a(Integer num) {
        int unused = this.f9156a.f9101W = num.intValue();
        this.f9156a.m14774d();
        this.f9156a.f9096R.setEnabled(true);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f9156a.m14772c();
        this.f9156a.m14776e();
        this.f9156a.f9096R.setEnabled(true);
    }
}
