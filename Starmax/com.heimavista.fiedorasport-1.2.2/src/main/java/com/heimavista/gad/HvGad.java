package com.heimavista.gad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.heimavista.api.HvApiEncryptUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.gad.api.gad.HvApiGetAcInfo;
import com.heimavista.gad.api.gad.HvApiGetByAcId;
import com.heimavista.gad.api.gad.HvApiGetEx2Ad;
import com.heimavista.gad.api.gad.HvApiGetExAd;
import com.heimavista.gad.api.gad.HvApiGetLikeList;
import com.heimavista.gad.api.mac.HvApiGetByMacId;
import com.heimavista.gad.api.mac.HvApiRegisterMacId;
import com.heimavista.gad.info.AcInfo;
import com.heimavista.gad.info.ExResInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Ac;
import com.heimavista.gad.p202u.ExRes;
import com.heimavista.gad.p202u.Res;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.l */
public class HvGad {

    /* renamed from: h */
    public static final String f9227h = (HvGad.class.getName() + ".RESET");

    /* renamed from: a */
    private Handler f9228a;

    /* renamed from: b */
    private HvGadDelegate f9229b;

    /* renamed from: c */
    private HvGadConfig f9230c;

    /* renamed from: d */
    private AcInfo f9231d;

    /* renamed from: e */
    private boolean f9232e;

    /* renamed from: f */
    private boolean f9233f;

    /* renamed from: g */
    private HvGadViewManage f9234g;

    /* renamed from: com.heimavista.gad.l$a */
    /* compiled from: HvGad */
    class C4545a implements OnResultListener<AcInfo> {

        /* renamed from: a */
        final /* synthetic */ String f9235a;

        /* renamed from: b */
        final /* synthetic */ Context f9236b;

        /* renamed from: c */
        final /* synthetic */ OnGadStartListener f9237c;

        /* renamed from: d */
        final /* synthetic */ boolean f9238d;

        C4545a(String str, Context context, OnGadStartListener tVar, boolean z) {
            this.f9235a = str;
            this.f9236b = context;
            this.f9237c = tVar;
            this.f9238d = z;
        }

        /* renamed from: a */
        public void mo22380a(AcInfo aVar) {
            HvGad.this.m14911a(this.f9235a, aVar);
            HvGad.this.mo25003a(this.f9236b, this.f9237c, this.f9238d);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            OnGadStartListener tVar = this.f9237c;
            if (tVar != null) {
                tVar.onError(str);
            }
            ToastUtils.m1029a(str);
        }
    }

    /* renamed from: com.heimavista.gad.l$b */
    /* compiled from: HvGad */
    class C4546b implements OnResultListener<AcInfo> {

        /* renamed from: a */
        final /* synthetic */ String f9240a;

        /* renamed from: b */
        final /* synthetic */ OnGadStartListener f9241b;

        /* renamed from: c */
        final /* synthetic */ Context f9242c;

        C4546b(String str, OnGadStartListener tVar, Context context) {
            this.f9240a = str;
            this.f9241b = tVar;
            this.f9242c = context;
        }

        /* renamed from: a */
        public void mo22380a(AcInfo aVar) {
            if (aVar == null || !aVar.mo24987a()) {
                HvGad.this.m14912b(this.f9242c, this.f9241b);
                return;
            }
            HvGad.this.m14911a(this.f9240a, aVar);
            HvGad.this.mo25018h();
            OnGadStartListener tVar = this.f9241b;
            if (tVar != null) {
                tVar.onSuccess();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            OnGadStartListener tVar = this.f9241b;
            if (tVar != null) {
                tVar.onError(str);
            }
            ToastUtils.m1029a(str);
        }
    }

    /* renamed from: com.heimavista.gad.l$c */
    /* compiled from: HvGad */
    private static class C4547c {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static HvGad f9244a = new HvGad(null);
    }

    /* synthetic */ HvGad(C4545a aVar) {
        this();
    }

    /* renamed from: i */
    public static HvGad m14915i() {
        return C4547c.f9244a;
    }

    /* renamed from: j */
    private String m14916j() {
        return mo25015e().mo9872a("macId", "");
    }

    /* renamed from: b */
    public AcInfo mo25008b() {
        if (this.f9231d == null) {
            Ac aVar = new Ac();
            aVar.mo25048j(mo25000a());
            if (aVar.mo24202e()) {
                aVar.mo24206j();
                this.f9231d = aVar.mo24232o();
            }
        }
        return this.f9231d;
    }

    /* renamed from: c */
    public synchronized HvGadConfig mo25011c() {
        if (this.f9230c == null) {
            this.f9230c = new HvGadConfig(mo25015e().mo9872a("serviceUrl", ""), mo25015e().mo9872a("logUrl", ""), HvApiEncryptUtils.m9999a(mo25015e().mo9872a("appDevNbr", "")), mo25015e().mo9872a("prodId", ""));
        }
        return this.f9230c;
    }

    /* renamed from: d */
    public HvGadDelegate mo25014d() {
        return this.f9229b;
    }

    /* renamed from: e */
    public SPUtils mo25015e() {
        return SPUtils.m1243c("HvGad");
    }

    /* renamed from: f */
    public HvGadViewManage mo25016f() {
        if (this.f9234g == null) {
            this.f9234g = new HvGadViewManage();
        }
        return this.f9234g;
    }

    /* renamed from: g */
    public void mo25017g() {
        HvGadDownloadManage.m14960g().mo25029d();
        mo25015e().mo9880b("macId");
        mo25015e().mo9880b("acId");
        this.f9231d = null;
        HvApp.m13010c().mo24157a(f9227h);
    }

    /* renamed from: h */
    public void mo25018h() {
        if (!this.f9232e && NetworkUtils.m858c()) {
            this.f9232e = true;
            String a = mo25000a();
            if (TextUtils.isEmpty(a)) {
                this.f9232e = false;
            } else if (HvApiGetByAcId.isExpire(a) || HvApiGetExAd.isExpire(a)) {
                new Thread(new C4504b(this, a)).start();
            } else {
                HvGadDownloadManage.m14960g().mo25028c();
                this.f9232e = false;
            }
        }
    }

    private HvGad() {
        this.f9228a = new Handler(Looper.getMainLooper());
    }

    /* renamed from: d */
    private String m14913d(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        SPUtils e = mo25015e();
        return e.mo9872a("acId_" + str, "");
    }

    /* renamed from: e */
    private HvGad m14914e(String str) {
        if (!TextUtils.isEmpty(m14916j())) {
            mo25017g();
        }
        mo25015e().mo9883b("macId", str);
        return this;
    }

    /* renamed from: a */
    private void m14908a(HvGadConfig nVar) {
        mo25015e().mo9883b("serviceUrl", nVar.f9253a);
        mo25015e().mo9883b("logUrl", nVar.f9254b);
        mo25015e().mo9883b("appDevNbr", HvApiEncryptUtils.m10002b(nVar.f9255c));
        mo25015e().mo9883b("prodId", nVar.f9256d);
        this.f9230c = nVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo25003a(Context context, OnGadStartListener tVar, boolean z) {
        if (mo25011c().mo25022a()) {
            HvApiGetLikeList.requestOnce();
            String j = m14916j();
            if (!TextUtils.isEmpty(j)) {
                String d = m14913d(j);
                if (!TextUtils.isEmpty(d)) {
                    AcInfo b = mo25008b();
                    if (b == null || ((long) b.f9199f) < System.currentTimeMillis() / 1000) {
                        new HvApiGetAcInfo().requestWithListener(d, new C4545a(j, context, tVar, z));
                        return;
                    }
                    mo25018h();
                    if (tVar != null) {
                        tVar.onSuccess();
                        return;
                    }
                    return;
                }
                new HvApiGetByMacId().requestWithListener(j, new C4546b(j, tVar, context));
                return;
            }
            LogUtils.m1131b("HvGad start MacId error");
            tVar.onError("HvGad start MacId error");
        } else if (this.f9229b != null) {
            new Thread(new C4506d(this, context, tVar, z)).start();
        } else {
            ToastUtils.m1029a("HvGad start init error");
            if (tVar != null) {
                tVar.onError("HvGad start init error");
            }
        }
    }

    /* renamed from: a */
    public String mo25000a() {
        return m14913d(m14916j());
    }

    /* renamed from: b */
    public /* synthetic */ void mo25009b(Context context, OnGadStartListener tVar, boolean z) {
        HvGadConfig c = this.f9229b.mo25025c();
        if (c == null || !c.mo25022a()) {
            this.f9228a.post(new C4538h(tVar));
            return;
        }
        m14908a(c);
        this.f9228a.post(new C4508f(this, context, tVar, z));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14911a(String str, AcInfo aVar) {
        SPUtils e = mo25015e();
        e.mo9883b("acId_" + str, aVar.f9194a);
        this.f9231d = aVar;
    }

    /* renamed from: a */
    public void mo25001a(Context context, HvGadDelegate oVar) {
        this.f9229b = oVar;
        if (!TextUtils.isEmpty(m14916j())) {
            mo25002a(context, (OnGadStartListener) null);
            HvGadLogManage.m14968d().mo25032b();
        } else if (!mo25011c().mo25022a()) {
            new Thread(new C4509g(this, oVar)).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14912b(Context context, OnGadStartListener tVar) {
        LogUtils.m1123a("showAcIdInputDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R$style.HvGadDialog);
        View inflate = LayoutInflater.from(context).inflate(R$layout.hv_gad_dialog_input_acid, (ViewGroup) null, false);
        EditText editText = (EditText) inflate.findViewById(16908291);
        editText.setImeOptions(2);
        ImageView imageView = (ImageView) inflate.findViewById(R$id.ivClose);
        Button button = (Button) inflate.findViewById(16908313);
        builder.setView(inflate);
        builder.setCancelable(false);
        builder.setOnCancelListener(new C4507e(tVar));
        AlertDialog create = builder.create();
        imageView.setOnClickListener(new C4505c(create));
        button.setOnClickListener(new C4539i(this, editText, imageView, button, context, tVar, create));
        create.show();
    }

    /* renamed from: a */
    public /* synthetic */ void mo25006a(HvGadDelegate oVar) {
        HvGadConfig c = oVar.mo25025c();
        if (c != null && c.mo25022a()) {
            m14908a(c);
        }
    }

    /* renamed from: a */
    public void mo25004a(Context context, String str, OnGadStartListener tVar) {
        m14914e(str);
        mo25002a(context, tVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo25002a(Context context, OnGadStartListener tVar) {
        mo25003a(context, tVar, false);
    }

    /* renamed from: a */
    static /* synthetic */ void m14909a(OnGadStartListener tVar) {
        if (tVar != null) {
            tVar.onError("HvGad start init error");
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m14910a(OnGadStartListener tVar, DialogInterface dialogInterface) {
        if (tVar != null) {
            tVar.onCancel();
        }
    }

    /* renamed from: c */
    public void mo25013c(String str) {
        if (!this.f9233f && NetworkUtils.m858c()) {
            this.f9233f = true;
            if (TextUtils.isEmpty(str)) {
                this.f9233f = false;
            } else if (HvApiGetEx2Ad.isExpire(str)) {
                new Thread(new C4503a(this, str)).start();
            } else {
                HvGadDownloadManage.m14960g().mo25028c();
                this.f9233f = false;
            }
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo25010b(String str) {
        if (new HvApiGetEx2Ad().checkExpiresSync(str)) {
            new Res().mo25141a(System.currentTimeMillis() - 2592000000L);
            ResInfo.m14882b(HvApp.m13010c());
        }
        HvGadDownloadManage.m14960g().mo25028c();
        this.f9233f = false;
    }

    /* renamed from: a */
    public /* synthetic */ void mo25005a(EditText editText, ImageView imageView, Button button, Context context, OnGadStartListener tVar, AlertDialog alertDialog, View view) {
        String obj = editText.getText().toString();
        if (!TextUtils.isEmpty(obj)) {
            String j = m14916j();
            imageView.setEnabled(false);
            editText.setEnabled(false);
            Button button2 = button;
            button2.setEnabled(false);
            new HvApiRegisterMacId().requestWithListener(j, obj, new C4548m(this, j, context, tVar, alertDialog, imageView, editText, button2));
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo25007a(String str) {
        if (new HvApiGetByAcId().checkExpiresSync(str)) {
            new Res().mo25141a(System.currentTimeMillis() - 2592000000L);
            ResInfo.m14882b(HvApp.m13010c());
        }
        AcInfo b = mo25008b();
        if (b == null || !b.f9198e || new HvApiGetExAd().checkExpiresSync(str)) {
            new ExRes().mo25125a(System.currentTimeMillis() - 604800000);
            ExResInfo.m14893b(HvApp.m13010c());
        }
        HvGadDownloadManage.m14960g().mo25028c();
        this.f9232e = false;
    }
}
