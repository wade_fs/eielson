package com.heimavista.gad;

/* renamed from: com.heimavista.gad.t */
public interface OnGadStartListener {
    void onCancel();

    void onError(String str);

    void onSuccess();
}
