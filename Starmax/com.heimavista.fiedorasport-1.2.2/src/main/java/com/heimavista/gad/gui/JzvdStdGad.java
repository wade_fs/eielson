package com.heimavista.gad.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.heimavista.gad.R$drawable;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;
import p019cn.jzvd.JZMediaInterface;
import p019cn.jzvd.JzvdStd;

public class JzvdStdGad extends JzvdStd {

    /* renamed from: P */
    public ImageView f9123P;

    /* renamed from: Q */
    private C4517a f9124Q;

    /* renamed from: R */
    private C4518b f9125R;

    /* renamed from: S */
    private boolean f9126S = true;

    /* renamed from: T */
    private long f9127T;

    /* renamed from: com.heimavista.gad.gui.JzvdStdGad$a */
    public interface C4517a {
        /* renamed from: a */
        void mo24945a();
    }

    /* renamed from: com.heimavista.gad.gui.JzvdStdGad$b */
    public interface C4518b {
        void onError();
    }

    public JzvdStdGad(Context context) {
        super(context);
    }

    /* renamed from: a */
    private void m14830a() {
        if (this.f9126S) {
            JZMediaInterface jZMediaInterface = this.mediaInterface;
            if (jZMediaInterface != null) {
                jZMediaInterface.setVolume(0.0f, 0.0f);
            }
            this.f9123P.setImageResource(R$drawable.hv_gad_volume_close);
            return;
        }
        JZMediaInterface jZMediaInterface2 = this.mediaInterface;
        if (jZMediaInterface2 != null) {
            jZMediaInterface2.setVolume(1.0f, 1.0f);
        }
        this.f9123P.setImageResource(R$drawable.hv_gad_volume_open);
    }

    public int getLayoutId() {
        return R$layout.hv_gad_std;
    }

    public long getLogDuration() {
        return (System.currentTimeMillis() - this.f9127T) / 1000;
    }

    public void gotoScreenFullscreen() {
        super.gotoScreenFullscreen();
    }

    public void init(Context context) {
        super.init(context);
        this.f9123P = (ImageView) findViewById(R$id.iv_volume);
        this.f9123P.setOnClickListener(this);
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.iv_volume) {
            this.f9126S = !this.f9126S;
            m14830a();
        }
    }

    public void onClickUiToggle() {
        C4517a aVar = this.f9124Q;
        if (aVar != null) {
            aVar.mo24945a();
        }
    }

    public void onPrepared() {
        super.onPrepared();
        this.f9127T = System.currentTimeMillis();
        if (this.screen == 1) {
            this.mediaInterface.setVolume(1.0f, 1.0f);
        } else if (this.f9126S) {
            this.mediaInterface.setVolume(0.0f, 0.0f);
        } else {
            this.mediaInterface.setVolume(1.0f, 1.0f);
        }
    }

    public void onStateError() {
        super.onStateError();
        C4518b bVar = this.f9125R;
        if (bVar != null) {
            bVar.onError();
        }
    }

    public void setOnClickUiToggleListener(C4517a aVar) {
        this.f9124Q = aVar;
    }

    public void setOnStatErrorListener(C4518b bVar) {
        this.f9125R = bVar;
    }

    public void setScreenFullscreen() {
        super.setScreenFullscreen();
        JZMediaInterface jZMediaInterface = this.mediaInterface;
        if (jZMediaInterface != null) {
            jZMediaInterface.setVolume(1.0f, 1.0f);
        }
    }

    public void setScreenNormal() {
        super.setScreenNormal();
        m14830a();
    }

    public JzvdStdGad(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
