package com.heimavista.gad.gui;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;

public class ResDefFragment extends ResBaseFragment {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_default, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(R$id.image);
        int a = HvGad.m14915i().mo25014d().mo22826a(super.f9129P);
        if (a > 0) {
            imageView.setImageResource(a);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), a, options);
            mo24949a(inflate, options.outWidth, options.outHeight);
        } else {
            inflate.getLayoutParams().width = 0;
            inflate.getLayoutParams().height = 0;
        }
        return inflate;
    }
}
