package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.info.AcInfo;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.a */
public class Ac extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_ac_mstr";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        while (i < i2) {
            if (i == 1) {
                sQLiteDatabase.execSQL("alter table gad_ac_mstr add gad_ac_is_ex integer default 1");
                i = 2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_ac_name", OrmColumnType.STRING, "");
        mo24173a("gad_ac_desc", OrmColumnType.STRING, "");
        mo24173a("gad_ac_icon_url", OrmColumnType.STRING, "");
        mo24173a("gad_ac_is_ex", OrmColumnType.INT, "");
        mo24173a("gad_ac_expires", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_ac_id"};
    }

    /* renamed from: i */
    public void mo25047i(String str) {
        mo24195a("gad_ac_icon_url", str);
    }

    /* renamed from: j */
    public void mo25048j(String str) {
        mo24195a("gad_ac_id", str);
    }

    /* renamed from: k */
    public void mo25049k(String str) {
        mo24195a("gad_ac_name", str);
    }

    /* renamed from: o */
    public AcInfo mo24232o() {
        AcInfo aVar = new AcInfo();
        aVar.f9194a = mo25053s();
        aVar.f9195b = mo25055u();
        aVar.f9196c = mo25050p();
        aVar.f9197d = mo25052r();
        aVar.f9198e = mo25054t();
        aVar.f9199f = mo25051q();
        return aVar;
    }

    /* renamed from: p */
    public String mo25050p() {
        return mo24203f("gad_ac_desc");
    }

    /* renamed from: q */
    public int mo25051q() {
        return mo24200d("gad_ac_expires").intValue();
    }

    /* renamed from: r */
    public String mo25052r() {
        return mo24203f("gad_ac_icon_url");
    }

    /* renamed from: s */
    public String mo25053s() {
        return mo24203f("gad_ac_id");
    }

    /* renamed from: t */
    public boolean mo25054t() {
        return mo24189a("gad_ac_is_ex").booleanValue();
    }

    /* renamed from: u */
    public String mo25055u() {
        return mo24203f("gad_ac_name");
    }

    /* renamed from: a */
    public void mo25045a(boolean z) {
        mo24190a("gad_ac_is_ex", Boolean.valueOf(z));
    }

    /* renamed from: h */
    public void mo25046h(String str) {
        mo24195a("gad_ac_desc", str);
    }

    /* renamed from: a */
    public void mo25043a(int i) {
        mo24193a("gad_ac_expires", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo25044a(AcInfo aVar) {
        mo25048j(aVar.f9194a);
        mo25049k(aVar.f9195b);
        mo25046h(aVar.f9196c);
        mo25047i(aVar.f9197d);
        mo25045a(aVar.f9198e);
        mo25043a(aVar.f9199f);
    }
}
