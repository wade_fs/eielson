package com.heimavista.gad;

import android.os.Bundle;
import android.webkit.URLUtil;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.heimavista.gad.info.AcInfo;
import com.heimavista.gad.info.ExResInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Ad;
import com.heimavista.gad.p202u.ExAd;
import com.heimavista.gad.p202u.ExRes;
import com.heimavista.gad.p202u.Res;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import org.xutils.C5217x;
import org.xutils.http.RequestParams;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.gad.q */
public class HvGadDownloadManage {

    /* renamed from: g */
    public static final String f9257g;

    /* renamed from: h */
    public static final String f9258h;

    /* renamed from: a */
    private boolean f9259a;

    /* renamed from: b */
    private boolean f9260b;

    /* renamed from: c */
    private boolean f9261c;

    /* renamed from: d */
    private boolean f9262d;

    /* renamed from: e */
    private int f9263e;

    /* renamed from: f */
    private Runnable f9264f;

    /* renamed from: com.heimavista.gad.q$b */
    /* compiled from: HvGadDownloadManage */
    private static class C4550b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static HvGadDownloadManage f9265a = new HvGadDownloadManage();
    }

    static {
        Class<HvGadDownloadManage> cls = HvGadDownloadManage.class;
        f9257g = cls.getName() + ".RES_SUCCESS";
        f9258h = cls.getName() + ".EXAD_SUCCESS";
    }

    /* renamed from: e */
    private void m14958e() {
        AcInfo b = HvGad.m14915i().mo25008b();
        if (b != null && b.f9198e) {
            int i = 3;
            while (true) {
                List<ExAd> h = new ExAd().mo25106h(HvGad.m14915i().mo25000a());
                LogUtils.m1123a(h);
                if (h.size() == 0) {
                    break;
                } else if (i <= 0) {
                    this.f9262d = true;
                    break;
                } else {
                    i--;
                    for (ExAd dVar : h) {
                        LogUtils.m1123a(dVar.mo25121v());
                        ExRes eVar = new ExRes();
                        eVar.mo25129h(dVar.mo25121v());
                        eVar.mo24206j();
                        ExResInfo q = eVar.mo25132q();
                        String[] a = q.mo24989a();
                        boolean z = false;
                        for (String str : a) {
                            if (!m14961h()) {
                                break;
                            }
                            if (!m14957a(q, str)) {
                                z = true;
                            }
                        }
                        if (!z) {
                            if (!m14961h()) {
                                break;
                            } else if (m14957a(q, q.f9216h)) {
                                Bundle bundle = new Bundle();
                                bundle.putString("pos", dVar.mo25120u());
                                HvApp.m13010c().mo24158a(f9258h, bundle);
                                eVar.mo25127a(true);
                                eVar.mo24181l();
                            }
                        }
                    }
                }
            }
        }
        new ExAd().mo24232o();
    }

    /* renamed from: f */
    private void m14959f() {
        while (true) {
            List<String> r = new Res().mo25149r();
            if (r.size() == 0) {
                break;
            }
            this.f9263e--;
            if (this.f9263e <= 0) {
                this.f9261c = true;
                break;
            }
            for (String str : r) {
                Res hVar = new Res();
                hVar.mo25145h(str);
                hVar.mo24206j();
                ResInfo q = hVar.mo25148q();
                String[] a = q.mo24981a();
                boolean z = false;
                for (String str2 : a) {
                    if (!m14961h()) {
                        break;
                    }
                    if (!m14956a(q, str2)) {
                        z = true;
                    }
                }
                if (!z) {
                    if (!m14961h()) {
                        break;
                    } else if (m14956a(q, q.f9192W)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("resId", str);
                        HvApp.m13010c().mo24158a(f9257g, bundle);
                        hVar.mo25143a(true);
                        hVar.mo24181l();
                    }
                }
            }
        }
        new Ad().mo24232o();
    }

    /* renamed from: g */
    public static HvGadDownloadManage m14960g() {
        return C4550b.f9265a;
    }

    /* renamed from: h */
    private boolean m14961h() {
        return !this.f9259a && NetworkUtils.m858c();
    }

    /* renamed from: a */
    public /* synthetic */ void mo25026a() {
        if (m14961h()) {
            m14959f();
            m14958e();
        }
        this.f9259a = true;
        this.f9260b = false;
    }

    /* renamed from: b */
    public void mo25027b() {
        if ((this.f9261c || this.f9262d) && !this.f9260b) {
            mo25028c();
        }
    }

    /* renamed from: c */
    public void mo25028c() {
        this.f9259a = false;
        this.f9263e = 3;
        if (!this.f9260b && m14961h()) {
            this.f9260b = true;
            this.f9261c = false;
            this.f9262d = false;
            new Thread(this.f9264f).start();
        }
    }

    /* renamed from: d */
    public void mo25029d() {
        this.f9259a = true;
    }

    private HvGadDownloadManage() {
        this.f9259a = false;
        this.f9260b = false;
        this.f9261c = false;
        this.f9262d = false;
        this.f9264f = new C4543j(this);
    }

    /* renamed from: a */
    private int m14955a(String str) {
        int i;
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.setConnectTimeout(15000);
            openConnection.addRequestProperty("Accept-Encoding", "identity");
            openConnection.connect();
            i = openConnection.getContentLength();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            i = 0;
            LogUtils.m1123a(str, Integer.valueOf(i));
            return i;
        } catch (IOException e2) {
            e2.printStackTrace();
            i = 0;
            LogUtils.m1123a(str, Integer.valueOf(i));
            return i;
        }
        LogUtils.m1123a(str, Integer.valueOf(i));
        return i;
    }

    /* renamed from: a */
    private int m14954a(int i) {
        int i2 = ((i / 1024) / 60) * 1000;
        LogUtils.m1123a("size", Integer.valueOf(i), "from", Integer.valueOf(i2));
        if (i2 < 15000) {
            i2 = 15000;
        } else if (i2 > 180000) {
            i2 = 180000;
        }
        LogUtils.m1123a("to", Integer.valueOf(i2));
        return i2;
    }

    /* renamed from: a */
    private boolean m14956a(ResInfo resInfo, String str) {
        if (!URLUtil.isNetworkUrl(str)) {
            return true;
        }
        File file = new File(ResInfo.m14881a(HvApp.m13010c(), resInfo.f9185P, str));
        if (file.isFile()) {
            return true;
        }
        int a = m14955a(str);
        if (a <= 0) {
            return false;
        }
        RequestParams requestParams = new RequestParams(str);
        requestParams.setReadTimeout(m14954a(a));
        requestParams.setMaxRetryCount(0);
        String str2 = file.getAbsolutePath() + ".det";
        requestParams.setSaveFilePath(str2);
        try {
            C5217x.http().getSync(requestParams, File.class);
            File file2 = new File(str2);
            if (!file2.isFile()) {
                return false;
            }
            long length = file2.length();
            LogUtils.m1123a(str, Long.valueOf(length), file2, Long.valueOf(file2.length()));
            if (length != ((long) a)) {
                file2.deleteOnExit();
                return false;
            } else if (!file2.renameTo(file)) {
                return false;
            } else {
                return file.isFile();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* renamed from: a */
    private boolean m14957a(ExResInfo cVar, String str) {
        if (!URLUtil.isNetworkUrl(str)) {
            return true;
        }
        File file = new File(ExResInfo.m14892a(HvApp.m13010c(), cVar.f9209a, str));
        if (file.isFile()) {
            return true;
        }
        int a = m14955a(str);
        if (a <= 0) {
            return false;
        }
        RequestParams requestParams = new RequestParams(str);
        requestParams.setReadTimeout(m14954a(a));
        requestParams.setMaxRetryCount(0);
        String str2 = file.getAbsolutePath() + ".det";
        requestParams.setSaveFilePath(str2);
        try {
            C5217x.http().getSync(requestParams, File.class);
            File file2 = new File(str2);
            if (!file2.isFile()) {
                return false;
            }
            long length = file2.length();
            LogUtils.m1123a(str, Long.valueOf(length), file2, Long.valueOf(file2.length()));
            if (length != ((long) a)) {
                LogUtils.m1123a(str, Integer.valueOf(a), file2, Long.valueOf(file2.length()));
                file2.deleteOnExit();
                return false;
            } else if (!file2.renameTo(file)) {
                return false;
            } else {
                return file.isFile();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }
}
