package com.heimavista.gad.gui;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.gad.R$drawable;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$string;
import com.heimavista.gad.api.gad.HvApiAddLike;
import com.heimavista.gad.api.gad.HvApiDelLike;
import com.heimavista.gad.api.gad.HvApiGetLikeCnt;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Like;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p217f.GAManage;
import p119e.p189e.p219h.HttpParams;

public abstract class Ex2ResBaseFragment extends Fragment {

    /* renamed from: P */
    protected Ex2AdInfo f9094P;

    /* renamed from: Q */
    protected HvGadViewConfig f9095Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public ImageView f9096R;

    /* renamed from: S */
    private TextView f9097S;

    /* renamed from: T */
    private boolean f9098T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public boolean f9099U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public boolean f9100V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public int f9101W;

    /* renamed from: com.heimavista.gad.gui.Ex2ResBaseFragment$a */
    class C4512a implements OnResultListener<Integer> {
        C4512a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.Ex2ResBaseFragment, int]
         candidates:
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, int):int
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.info.Ex2AdInfo, android.view.View):void
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22380a(Integer num) {
            boolean unused = Ex2ResBaseFragment.this.f9100V = false;
            boolean unused2 = Ex2ResBaseFragment.this.f9099U = true;
            int unused3 = Ex2ResBaseFragment.this.f9101W = num.intValue();
            Ex2ResBaseFragment.this.m14774d();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.Ex2ResBaseFragment, int]
         candidates:
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, int):int
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.info.Ex2AdInfo, android.view.View):void
          com.heimavista.gad.gui.Ex2ResBaseFragment.a(com.heimavista.gad.gui.Ex2ResBaseFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            boolean unused = Ex2ResBaseFragment.this.f9100V = false;
            boolean unused2 = Ex2ResBaseFragment.this.f9099U = false;
            int unused3 = Ex2ResBaseFragment.this.f9101W = 0;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m14776e() {
        if (!this.f9100V) {
            this.f9100V = true;
            new HvApiGetLikeCnt().request(this.f9094P, new C4512a());
        }
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            arguments.getString("pos", "");
            this.f9094P = (Ex2AdInfo) arguments.getParcelable(Ex2AdInfo.class.getCanonicalName());
            this.f9095Q = (HvGadViewConfig) arguments.getParcelable(HvGadViewConfig.class.getCanonicalName());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m14772c() {
        Like fVar = new Like();
        fVar.mo25133h(this.f9094P.f9179R);
        fVar.mo25135j(this.f9094P.f9184W.f9185P);
        this.f9098T = fVar.mo24202e();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m14774d() {
        int i = 8;
        if (this.f9098T) {
            this.f9096R.setImageResource(R$drawable.hv_gad_like_selected);
            TextView textView = this.f9097S;
            if (this.f9099U && this.f9101W > 0) {
                i = 0;
            }
            textView.setVisibility(i);
            if (!this.f9099U) {
                m14776e();
            }
        } else {
            this.f9096R.setImageResource(R$drawable.hv_gad_like_unselected);
            this.f9097S.setVisibility(8);
        }
        this.f9097S.setText(String.valueOf(this.f9101W));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public int[] mo24927b(View view) {
        return mo24925a(view, 0, 0);
    }

    /* renamed from: a */
    public static Bundle m14767a(String str, HvGadViewConfig hvGadViewConfig, Ex2AdInfo ex2AdInfo) {
        Bundle bundle = new Bundle();
        bundle.putString("pos", str);
        bundle.putParcelable(Ex2AdInfo.class.getCanonicalName(), ex2AdInfo);
        bundle.putParcelable(HvGadViewConfig.class.getCanonicalName(), hvGadViewConfig);
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24926b() {
        ResInfo resInfo;
        Ex2AdInfo ex2AdInfo = this.f9094P;
        if (ex2AdInfo != null && (resInfo = ex2AdInfo.f9184W) != null && URLUtil.isNetworkUrl(resInfo.f9188S)) {
            HvGadLogManage d = HvGadLogManage.m14968d();
            LogInfo dVar = new LogInfo();
            dVar.mo24993a(this.f9094P);
            d.mo25031a(dVar);
            GAManage a = GAManage.m17122a();
            String string = getString(R$string.hv_gad_ga_cg_click);
            Ex2AdInfo ex2AdInfo2 = this.f9094P;
            a.mo27171a(string, ex2AdInfo2.f9178Q, ex2AdInfo2.f9180S, null);
            startActivity(GadWebActivity.m14827a(getContext(), this.f9094P.f9184W.f9188S));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int[] mo24925a(View view, int i, int i2) {
        int b = this.f9095Q.mo24890b();
        int a = this.f9095Q.mo24889a();
        if (b <= 0 || a <= 0) {
            if (b <= 0 && a <= 0) {
                b = ScreenUtils.m1263a();
            }
            Ex2AdInfo ex2AdInfo = this.f9094P;
            int i3 = ex2AdInfo != null ? ex2AdInfo.f9184W.f9189T : 0;
            Ex2AdInfo ex2AdInfo2 = this.f9094P;
            int i4 = ex2AdInfo2 != null ? ex2AdInfo2.f9184W.f9190U : 0;
            if (b > 0) {
                if (i3 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i > 0) {
                    a = (i2 * b) / i;
                }
            } else {
                if (i4 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i2 > 0) {
                    b = (i * a) / i2;
                }
            }
        }
        if (b > 0 && a <= 0) {
            a = (b * 9) / 16;
        } else if (b <= 0 && a > 0) {
            b = (a * 16) / 9;
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = b;
            layoutParams.height = a;
        }
        return new int[]{b, a};
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24923a(View view) {
        Ex2AdInfo ex2AdInfo = this.f9094P;
        m14772c();
        this.f9096R = (ImageView) view.findViewById(R$id.iv_like);
        this.f9097S = (TextView) view.findViewById(R$id.tv_like);
        this.f9096R.setOnClickListener(new C4520a(this, ex2AdInfo));
        if (this.f9098T) {
            m14776e();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24924a(Ex2AdInfo ex2AdInfo, View view) {
        this.f9096R.setEnabled(false);
        this.f9098T = !this.f9098T;
        if (this.f9098T) {
            this.f9101W++;
            new HvApiAddLike().request(ex2AdInfo, new C4532m(this));
        } else {
            this.f9101W--;
            new HvApiDelLike().request(ex2AdInfo, new C4533n(this));
        }
        m14774d();
    }
}
