package com.heimavista.gad.api.gad;

import com.heimavista.api.OnResultListener;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.AcInfo;
import com.heimavista.gad.p202u.Ac;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetAcInfo extends HvApiGadBase {
    private OnResultListener<AcInfo> onResultListener;

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getAcInfo";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        AcInfo a = AcInfo.m14887a(fVar.mo25325a("AcInfo", "{}"));
        a.f9199f = (int) ((System.currentTimeMillis() / 1000) + ((long) a.f9199f));
        Ac aVar = new Ac();
        aVar.mo25044a(a);
        aVar.mo24181l();
        OnResultListener<AcInfo> dVar = this.onResultListener;
        if (dVar != null) {
            dVar.mo22380a(a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public HttpCancelable requestWithListener(String str, OnResultListener<AcInfo> dVar) {
        this.onResultListener = dVar;
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("acId", (Object) str);
        return request(cVar);
    }
}
