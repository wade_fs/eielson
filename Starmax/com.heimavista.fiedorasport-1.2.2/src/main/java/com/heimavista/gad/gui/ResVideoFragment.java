package com.heimavista.gad.gui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.R$string;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.utils.RandomUtils;
import java.io.File;
import p019cn.jzvd.JZDataSource;
import p019cn.jzvd.Jzvd;
import p019cn.jzvd.media.JZMediaExo;
import p119e.p189e.p217f.GAManage;

public class ResVideoFragment extends ResBaseFragment {

    /* renamed from: Z */
    private JzvdStdGad f9139Z;

    /* renamed from: a */
    static /* synthetic */ boolean m14853a(View view, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 1) {
            return Jzvd.backPress();
        }
        return false;
    }

    /* renamed from: c */
    private void m14854c() {
        AdInfo adInfo = super.f9130Q;
        if (adInfo != null && adInfo.f9176W != null) {
            Jzvd.releaseAllVideos();
            JzvdStdGad jzvdStdGad = this.f9139Z;
            if (jzvdStdGad != null) {
                long logDuration = jzvdStdGad.getLogDuration();
                int i = super.f9130Q.f9176W.f9191V;
                if (logDuration > ((long) i)) {
                    logDuration = (long) i;
                }
                HvGadLogManage d = HvGadLogManage.m14968d();
                LogInfo dVar = new LogInfo();
                dVar.mo24992a(super.f9130Q);
                dVar.mo24991a(logDuration);
                d.mo25033b(dVar);
                GAManage a = GAManage.m17122a();
                String string = getString(R$string.hv_gad_ga_cg_view);
                AdInfo adInfo2 = super.f9130Q;
                a.mo27171a(string, adInfo2.f9170Q, adInfo2.f9172S, Long.valueOf(logDuration));
            }
        }
    }

    /* renamed from: d */
    private void m14855d() {
        JzvdStdGad jzvdStdGad = this.f9139Z;
        if (jzvdStdGad != null) {
            jzvdStdGad.startVideo();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_video, viewGroup, false);
        String[] a = super.f9130Q.f9176W.mo24981a();
        String a2 = ResInfo.m14881a(getContext(), super.f9130Q.f9176W.f9185P, a[RandomUtils.m15262a(a.length - 1)]);
        Context context = getContext();
        ResInfo resInfo = super.f9130Q.f9176W;
        String a3 = ResInfo.m14881a(context, resInfo.f9185P, resInfo.f9192W);
        int[] b = mo24951b(inflate);
        JZDataSource jZDataSource = new JZDataSource(a2);
        jZDataSource.looping = true;
        jZDataSource.mute = true;
        this.f9139Z = (JzvdStdGad) inflate.findViewById(R$id.videoplayer);
        ViewGroup.LayoutParams layoutParams = this.f9139Z.getLayoutParams();
        layoutParams.width = b[0];
        layoutParams.height = b[1];
        ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) this.f9139Z.getParent()).getLayoutParams();
        layoutParams2.width = b[0];
        layoutParams2.height = b[1];
        this.f9139Z.setUp(jZDataSource, 0, JZMediaExo.class);
        Jzvd.setVideoImageDisplayType(2);
        this.f9139Z.thumbImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.f9139Z.setOnClickUiToggleListener(new C4530k(this));
        this.f9139Z.thumbImageView.setImageURI(Uri.fromFile(new File(a3)));
        mo24947a(inflate);
        return inflate;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (isVisible()) {
            m14855d();
        } else {
            m14854c();
        }
    }

    public void onPause() {
        super.onPause();
        m14854c();
    }

    public void onResume() {
        super.onResume();
        m14855d();
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.setFocusable(true);
            view.requestFocus();
            view.setOnKeyListener(C4529j.f9152P);
        }
    }
}
