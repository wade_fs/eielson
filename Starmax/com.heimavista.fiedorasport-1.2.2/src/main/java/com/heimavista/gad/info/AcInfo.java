package com.heimavista.gad.info;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.heimavista.gad.info.a */
public class AcInfo {

    /* renamed from: a */
    public String f9194a;

    /* renamed from: b */
    public String f9195b;

    /* renamed from: c */
    public String f9196c;

    /* renamed from: d */
    public String f9197d;

    /* renamed from: e */
    public boolean f9198e;

    /* renamed from: f */
    public int f9199f;

    /* renamed from: a */
    public boolean mo24987a() {
        LogUtils.m1123a(this.f9194a);
        return true ^ TextUtils.isEmpty(this.f9194a);
    }

    /* renamed from: a */
    public static AcInfo m14887a(String str) {
        boolean z = true;
        LogUtils.m1123a(str);
        AcInfo aVar = new AcInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            aVar.f9194a = jSONObject.optString("acId", "");
            aVar.f9195b = jSONObject.optString("acName", "");
            aVar.f9196c = jSONObject.optString("acDesc", "");
            aVar.f9197d = jSONObject.optString("iconUrl", "");
            if (jSONObject.optInt("isEx", 0) != 1) {
                z = false;
            }
            aVar.f9198e = z;
            aVar.f9199f = jSONObject.optInt("expires", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return aVar;
    }
}
