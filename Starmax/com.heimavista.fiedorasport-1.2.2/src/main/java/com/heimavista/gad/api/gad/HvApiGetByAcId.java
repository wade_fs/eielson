package com.heimavista.gad.api.gad;

import com.blankj.utilcode.util.SPUtils;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.p202u.Ad;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetByAcId extends HvApiGadBase {
    private static final String KEY_PRE_EXPIRES = "GetByAcId_Expires_";
    private static final String KEY_PRE_TICK = "GetByAcId_Tick_";

    private static int getExpires(String str) {
        SPUtils e = HvGad.m14915i().mo25015e();
        return e.mo9869a(KEY_PRE_EXPIRES + str, 0);
    }

    private static int getTick(String str) {
        SPUtils e = HvGad.m14915i().mo25015e();
        return e.mo9869a(KEY_PRE_TICK + str, 0);
    }

    public static boolean isExpire(String str) {
        return ((long) getExpires(str)) < System.currentTimeMillis() / 1000;
    }

    private static void saveExpires(String str, int i) {
        SPUtils e = HvGad.m14915i().mo25015e();
        e.mo9881b(KEY_PRE_EXPIRES + str, i);
    }

    private static void saveTick(String str, int i) {
        SPUtils e = HvGad.m14915i().mo25015e();
        e.mo9881b(KEY_PRE_TICK + str, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public boolean checkExpiresSync(String str) {
        if (!isExpire(str)) {
            return true;
        }
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("acId", (Object) str);
        cVar.mo27196a("tick", Integer.valueOf(getTick(str)));
        if (requestSync(cVar).mo22515d() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getByAcId";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("acId", "");
        if (fVar.mo25334d().has("Rows")) {
            List<Object> a2 = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
            Ad bVar = new Ad();
            bVar.mo25062b(true);
            bVar.mo25069n(a);
            for (Object obj : a2) {
                bVar.mo25059a(AdInfo.m14877a(String.valueOf(obj)));
            }
        }
        if (fVar.mo25334d().has("tick")) {
            saveTick(a, fVar.mo25323a("tick", (Integer) 0).intValue());
        }
        if (fVar.mo25334d().has("expires")) {
            saveExpires(a, (int) (((long) fVar.mo25323a("expires", (Integer) 0).intValue()) + (System.currentTimeMillis() / 1000)));
        }
    }
}
