package com.heimavista.gad;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.gad.info.AcInfo;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.m */
/* compiled from: HvGad */
class C4548m implements OnResultListener<AcInfo> {

    /* renamed from: a */
    final /* synthetic */ String f9245a;

    /* renamed from: b */
    final /* synthetic */ Context f9246b;

    /* renamed from: c */
    final /* synthetic */ OnGadStartListener f9247c;

    /* renamed from: d */
    final /* synthetic */ AlertDialog f9248d;

    /* renamed from: e */
    final /* synthetic */ ImageView f9249e;

    /* renamed from: f */
    final /* synthetic */ EditText f9250f;

    /* renamed from: g */
    final /* synthetic */ Button f9251g;

    /* renamed from: h */
    final /* synthetic */ HvGad f9252h;

    C4548m(HvGad lVar, String str, Context context, OnGadStartListener tVar, AlertDialog alertDialog, ImageView imageView, EditText editText, Button button) {
        this.f9252h = lVar;
        this.f9245a = str;
        this.f9246b = context;
        this.f9247c = tVar;
        this.f9248d = alertDialog;
        this.f9249e = imageView;
        this.f9250f = editText;
        this.f9251g = button;
    }

    /* renamed from: a */
    public void mo22380a(AcInfo aVar) {
        Object[] objArr = new Object[1];
        objArr[0] = aVar != null ? Boolean.valueOf(aVar.mo24987a()) : "null";
        LogUtils.m1123a(objArr);
        if (aVar == null || !aVar.mo24987a()) {
            this.f9249e.setEnabled(true);
            this.f9250f.setEnabled(true);
            this.f9251g.setEnabled(true);
            return;
        }
        this.f9252h.m14911a(this.f9245a, aVar);
        this.f9252h.mo25003a(this.f9246b, this.f9247c, true);
        this.f9248d.dismiss();
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        ToastUtils.m1029a(str);
        this.f9249e.setEnabled(true);
        this.f9250f.setEnabled(true);
        this.f9251g.setEnabled(true);
    }
}
