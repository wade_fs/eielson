package com.heimavista.gad;

import android.content.Context;

/* renamed from: com.heimavista.gad.d */
/* compiled from: lambda */
public final /* synthetic */ class C4506d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ HvGad f9065P;

    /* renamed from: Q */
    private final /* synthetic */ Context f9066Q;

    /* renamed from: R */
    private final /* synthetic */ OnGadStartListener f9067R;

    /* renamed from: S */
    private final /* synthetic */ boolean f9068S;

    public /* synthetic */ C4506d(HvGad lVar, Context context, OnGadStartListener tVar, boolean z) {
        this.f9065P = lVar;
        this.f9066Q = context;
        this.f9067R = tVar;
        this.f9068S = z;
    }

    public final void run() {
        this.f9065P.mo25009b(this.f9066Q, this.f9067R, this.f9068S);
    }
}
