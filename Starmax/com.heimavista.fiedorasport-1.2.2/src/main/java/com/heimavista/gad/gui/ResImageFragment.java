package com.heimavista.gad.gui;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.info.ResInfo;
import java.io.File;

public class ResImageFragment extends ResBaseFragment {
    /* renamed from: c */
    public /* synthetic */ void mo24953c(View view) {
        mo24950b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_image, viewGroup, false);
        mo24951b(inflate);
        String[] a = super.f9130Q.f9176W.mo24981a();
        ((ImageView) inflate.findViewById(R$id.image)).setImageURI(Uri.fromFile(new File(ResInfo.m14881a(getContext(), super.f9130Q.f9176W.f9185P, a[(int) (System.currentTimeMillis() % ((long) a.length))]))));
        inflate.setOnClickListener(new C4528i(this));
        mo24947a(inflate);
        return inflate;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (!isVisible()) {
            m14851c();
        }
    }

    public void onPause() {
        super.onPause();
        m14851c();
    }

    /* renamed from: c */
    private void m14851c() {
        HvGadLogManage d = HvGadLogManage.m14968d();
        LogInfo dVar = new LogInfo();
        dVar.mo24992a(super.f9130Q);
        d.mo25033b(dVar);
    }
}
