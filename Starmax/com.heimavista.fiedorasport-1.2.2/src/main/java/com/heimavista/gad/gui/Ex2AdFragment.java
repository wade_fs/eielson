package com.heimavista.gad.gui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadDownloadManage;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.api.gad.HvApiGetEx2Ad;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Ex2Ad;
import com.heimavista.gad.p202u.Res;
import java.util.ArrayList;
import java.util.List;

public class Ex2AdFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public String f9086P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public String f9087Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public Ex2AdInfo f9088R;

    /* renamed from: S */
    private HvGadViewConfig f9089S;

    /* renamed from: T */
    private LocalBroadcastManager f9090T;

    /* renamed from: U */
    private BroadcastReceiver f9091U;

    /* renamed from: V */
    private View f9092V;

    /* renamed from: com.heimavista.gad.gui.Ex2AdFragment$a */
    class C4511a extends BroadcastReceiver {
        C4511a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (HvGadDownloadManage.f9257g.equals(intent.getAction())) {
                if (Ex2AdFragment.this.f9088R == null) {
                    String stringExtra = intent.getStringExtra("resId");
                    if (!TextUtils.isEmpty(stringExtra) && new Ex2Ad().mo25080a(Ex2AdFragment.this.f9086P, Ex2AdFragment.this.f9087Q, false, stringExtra).size() > 0) {
                        Ex2AdFragment.this.m14764f();
                    }
                }
            } else if (HvApiGetEx2Ad.ACTION_NEW.equals(intent.getAction()) && Ex2AdFragment.this.f9088R == null && !TextUtils.isEmpty(Ex2AdFragment.this.f9086P) && Ex2AdFragment.this.f9086P.equals(intent.getStringExtra("acId"))) {
                Ex2AdFragment.this.m14764f();
            }
        }
    }

    /* renamed from: e */
    private void m14763e() {
        if (this.f9090T != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(HvGadDownloadManage.f9257g);
            intentFilter.addAction(HvGad.f9227h);
            intentFilter.addAction(HvApiGetEx2Ad.ACTION_NEW);
            this.f9090T.registerReceiver(this.f9091U, intentFilter);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m14764f() {
        /*
            r6 = this;
            com.heimavista.gad.info.Ex2AdInfo r0 = r6.m14761d()
            r6.f9088R = r0
            r0 = 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r2 = 0
            java.lang.String r3 = "resetAdInfo"
            r1[r2] = r3
            com.heimavista.gad.info.Ex2AdInfo r3 = r6.f9088R
            r4 = 1
            r1[r4] = r3
            com.blankj.utilcode.util.LogUtils.m1123a(r1)
            com.heimavista.gad.info.Ex2AdInfo r1 = r6.f9088R
            r3 = 0
            if (r1 == 0) goto L_0x006c
            com.heimavista.gad.info.ResInfo r1 = r1.f9184W
            if (r1 == 0) goto L_0x006c
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "resetResInfo"
            r0[r2] = r5
            r0[r4] = r1
            com.blankj.utilcode.util.LogUtils.m1123a(r0)
            com.heimavista.gad.info.Ex2AdInfo r0 = r6.f9088R
            com.heimavista.gad.info.ResInfo r0 = r0.f9184W
            java.lang.String r0 = r0.f9186Q
            java.lang.String r1 = "image"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004b
            com.heimavista.gad.gui.Ex2ResImageFragment r0 = new com.heimavista.gad.gui.Ex2ResImageFragment
            r0.<init>()
            java.lang.String r1 = r6.f9087Q
            com.heimavista.gad.HvGadViewConfig r2 = r6.f9089S
            com.heimavista.gad.info.Ex2AdInfo r4 = r6.f9088R
            android.os.Bundle r1 = com.heimavista.gad.gui.Ex2ResBaseFragment.m14767a(r1, r2, r4)
            r0.setArguments(r1)
            goto L_0x006d
        L_0x004b:
            com.heimavista.gad.info.Ex2AdInfo r0 = r6.f9088R
            com.heimavista.gad.info.ResInfo r0 = r0.f9184W
            java.lang.String r0 = r0.f9186Q
            java.lang.String r1 = "video"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x006c
            com.heimavista.gad.gui.Ex2ResVideoFragment r0 = new com.heimavista.gad.gui.Ex2ResVideoFragment
            r0.<init>()
            java.lang.String r1 = r6.f9087Q
            com.heimavista.gad.HvGadViewConfig r2 = r6.f9089S
            com.heimavista.gad.info.Ex2AdInfo r4 = r6.f9088R
            android.os.Bundle r1 = com.heimavista.gad.gui.Ex2ResBaseFragment.m14767a(r1, r2, r4)
            r0.setArguments(r1)
            goto L_0x006d
        L_0x006c:
            r0 = r3
        L_0x006d:
            if (r0 != 0) goto L_0x0072
            r6.f9088R = r3
            goto L_0x0098
        L_0x0072:
            com.heimavista.gad.info.Ex2AdInfo r1 = r6.f9088R
            if (r1 == 0) goto L_0x0098
            com.heimavista.gad.l r1 = com.heimavista.gad.HvGad.m14915i()
            com.blankj.utilcode.util.w r1 = r1.mo25015e()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "last_ex2ad_id_"
            r2.append(r3)
            java.lang.String r3 = r6.f9087Q
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.heimavista.gad.info.Ex2AdInfo r3 = r6.f9088R
            java.lang.String r3 = r3.f9180S
            r1.mo9883b(r2, r3)
        L_0x0098:
            if (r0 == 0) goto L_0x00ab
            androidx.fragment.app.FragmentManager r1 = r6.getChildFragmentManager()
            androidx.fragment.app.FragmentTransaction r1 = r1.beginTransaction()
            int r2 = com.heimavista.gad.R$id.ll_ad
            androidx.fragment.app.FragmentTransaction r0 = r1.replace(r2, r0)
            r0.commit()
        L_0x00ab:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.gad.gui.Ex2AdFragment.m14764f():void");
    }

    /* renamed from: g */
    private void m14765g() {
        LocalBroadcastManager localBroadcastManager = this.f9090T;
        if (localBroadcastManager != null) {
            localBroadcastManager.unregisterReceiver(this.f9091U);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9090T = LocalBroadcastManager.getInstance(context);
        this.f9091U = new C4511a();
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f9086P = arguments.getString("acId", "");
            this.f9087Q = arguments.getString("pos", "");
            this.f9089S = (HvGadViewConfig) arguments.getParcelable(HvGadViewConfig.class.getCanonicalName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        if (this.f9092V == null) {
            this.f9092V = layoutInflater.inflate(R$layout.hv_gad_ad, viewGroup, false);
        }
        return this.f9092V;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (isVisible()) {
            m14760c();
        } else {
            m14758b();
        }
    }

    public void onPause() {
        super.onPause();
        m14758b();
    }

    public void onResume() {
        super.onResume();
        m14760c();
    }

    /* renamed from: a */
    public static Bundle m14755a(String str, String str2, HvGadViewConfig hvGadViewConfig) {
        Bundle bundle = new Bundle();
        bundle.putString("acId", str);
        bundle.putString("pos", str2);
        bundle.putParcelable(HvGadViewConfig.class.getCanonicalName(), hvGadViewConfig);
        return bundle;
    }

    /* renamed from: b */
    private void m14758b() {
        m14765g();
    }

    /* renamed from: c */
    private void m14760c() {
        m14763e();
        if (this.f9088R == null) {
            m14764f();
        }
        HvGad.m14915i().mo25013c(this.f9086P);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.gad.u.c.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.c>
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.heimavista.gad.u.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.heimavista.gad.u.c.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.c> */
    /* renamed from: d */
    private Ex2AdInfo m14761d() {
        List<Ex2Ad> a = new Ex2Ad().mo25079a(this.f9086P, this.f9087Q, (Boolean) false);
        if (a.size() == 0) {
            a = new Ex2Ad().mo25079a(this.f9086P, this.f9087Q, (Boolean) true);
        }
        ArrayList arrayList = new ArrayList();
        SPUtils e = HvGad.m14915i().mo25015e();
        String a2 = e.mo9871a("last_ex2ad_id_" + this.f9087Q);
        Ex2AdInfo ex2AdInfo = null;
        for (Ex2Ad cVar : a) {
            Ex2AdInfo p = cVar.mo25092p();
            ResInfo resInfo = p.f9184W;
            if (!resInfo.mo24980a(getContext())) {
                Res hVar = new Res();
                hVar.mo25145h(resInfo.f9185P);
                hVar.mo25143a(false);
                hVar.mo24208m();
                HvGadDownloadManage.m14960g().mo25028c();
            } else if (p.f9180S.equals(a2)) {
                ex2AdInfo = p;
            } else {
                arrayList.add(p);
            }
        }
        if (arrayList.size() == 0 && ex2AdInfo != null) {
            arrayList.add(ex2AdInfo);
        }
        if (arrayList.size() > 0) {
            return (Ex2AdInfo) arrayList.get((int) (System.currentTimeMillis() % ((long) arrayList.size())));
        }
        return null;
    }
}
