package com.heimavista.gad;

import android.graphics.drawable.Drawable;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import com.heimavista.gad.gui.AdFragment;
import com.heimavista.gad.gui.Ex2AdFragment;
import com.heimavista.gad.gui.ExAdFragment;
import com.heimavista.gad.info.AcInfo;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.image.ImageOptions;

/* renamed from: com.heimavista.gad.s */
public class HvGadViewManage {

    /* renamed from: com.heimavista.gad.s$a */
    /* compiled from: HvGadViewManage */
    class C4553a implements Callback.CommonCallback<Drawable> {

        /* renamed from: a */
        final /* synthetic */ ImageView f9269a;

        /* renamed from: b */
        final /* synthetic */ int f9270b;

        /* renamed from: c */
        final /* synthetic */ int f9271c;

        C4553a(HvGadViewManage sVar, ImageView imageView, int i, int i2) {
            this.f9269a = imageView;
            this.f9270b = i;
            this.f9271c = i2;
        }

        public void onCancelled(Callback.CancelledException cancelledException) {
        }

        public void onError(Throwable th, boolean z) {
            this.f9269a.setImageResource(this.f9270b);
            this.f9269a.setVisibility(this.f9271c);
        }

        public void onFinished() {
        }

        public void onSuccess(Drawable drawable) {
            this.f9269a.setImageDrawable(drawable);
            this.f9269a.setVisibility(0);
        }
    }

    /* renamed from: a */
    public void mo25036a(ImageView imageView, int i, int i2) {
        AcInfo b = HvGad.m14915i().mo25008b();
        if (b == null || !URLUtil.isNetworkUrl(b.f9197d)) {
            imageView.setImageResource(i);
            imageView.setVisibility(i2);
            return;
        }
        C5217x.image().loadDrawable(b.f9197d, ImageOptions.DEFAULT, new C4553a(this, imageView, i, i2));
    }

    /* renamed from: b */
    public Fragment mo25037b(String str, HvGadViewConfig hvGadViewConfig) {
        AcInfo b = HvGad.m14915i().mo25008b();
        if (b == null) {
            return new Fragment();
        }
        if (b.f9198e) {
            ExAdFragment exAdFragment = new ExAdFragment();
            exAdFragment.setArguments(ExAdFragment.m14791a(str, hvGadViewConfig));
            return exAdFragment;
        }
        AdFragment adFragment = new AdFragment();
        adFragment.setArguments(AdFragment.m14744a(str, hvGadViewConfig, true));
        return adFragment;
    }

    /* renamed from: a */
    public Fragment mo25034a(String str, HvGadViewConfig hvGadViewConfig) {
        AdFragment adFragment = new AdFragment();
        adFragment.setArguments(AdFragment.m14744a(str, hvGadViewConfig, false));
        return adFragment;
    }

    /* renamed from: a */
    public Fragment mo25035a(String str, String str2, HvGadViewConfig hvGadViewConfig) {
        Ex2AdFragment ex2AdFragment = new Ex2AdFragment();
        ex2AdFragment.setArguments(Ex2AdFragment.m14755a(str, str2, hvGadViewConfig));
        return ex2AdFragment;
    }
}
