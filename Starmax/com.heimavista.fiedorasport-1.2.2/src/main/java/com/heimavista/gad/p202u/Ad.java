package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.utils.DateUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.IOrmTable;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.b */
public class Ad extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_ad_mstr";
    }

    /* renamed from: a */
    public void mo25058a(int i) {
        mo24193a("gad_ad_seq", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.b.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.b>
      com.heimavista.gad.u.b.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_ad_seq", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_ad_pos", OrmColumnType.STRING, "");
        mo24173a("gad_ad_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_ad_id", OrmColumnType.STRING, "");
        mo24173a("gad_ad_is_ad", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_ad_start", OrmColumnType.STRING, "");
        mo24173a("gad_ad_end", OrmColumnType.STRING, "");
        mo24173a("gad_ad_res_id", OrmColumnType.STRING, "");
        mo24173a("gad_ad_wait_del", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_ad_seq"};
    }

    /* renamed from: i */
    public void mo25064i(String str) {
        mo24195a("gad_ad_end", str);
    }

    /* renamed from: j */
    public void mo25065j(String str) {
        mo24195a("gad_ad_id", str);
    }

    /* renamed from: k */
    public void mo25066k(String str) {
        mo24195a("gad_ad_pos", str);
    }

    /* renamed from: l */
    public void mo25067l(String str) {
        mo24195a("gad_ad_res_id", str);
    }

    /* renamed from: m */
    public void mo25068m(String str) {
        mo24195a("gad_ad_start", str);
    }

    /* renamed from: n */
    public void mo25069n(String str) {
        mo24188a("gad_ad_ac_id=?", new String[]{str});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.gad.u.b.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.b>
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.heimavista.gad.u.b.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.heimavista.gad.u.b.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.b> */
    /* renamed from: o */
    public void mo24232o() {
        boolean z;
        for (Ad bVar : mo25061b(mo25070p(), null, true)) {
            String[] split = bVar.mo25074t().split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = true;
                    break;
                }
                String str = split[i];
                if (mo25061b(mo25070p(), str, false).size() != 0 && mo25056a(mo25070p(), str, (Boolean) false).size() == 0) {
                    z = false;
                    break;
                }
                i++;
            }
            if (z) {
                mo24178c("gad_ad_ac_id=? and gad_ad_seq=" + bVar.mo25076v(), new String[]{mo25070p()});
            }
        }
    }

    /* renamed from: p */
    public String mo25070p() {
        return mo24203f("gad_ad_ac_id");
    }

    /* renamed from: q */
    public AdInfo mo25071q() {
        AdInfo adInfo = new AdInfo();
        adInfo.f9169P = mo25076v();
        adInfo.f9170Q = mo25074t();
        adInfo.f9171R = mo25070p();
        adInfo.f9172S = mo25073s();
        adInfo.f9173T = mo25078x() ? 1 : 0;
        adInfo.f9174U = mo25077w();
        adInfo.f9175V = mo25072r();
        Res hVar = new Res();
        hVar.mo25145h(mo25075u());
        hVar.mo24206j();
        adInfo.f9176W = hVar.mo25148q();
        return adInfo;
    }

    /* renamed from: r */
    public String mo25072r() {
        return mo24203f("gad_ad_end");
    }

    /* renamed from: s */
    public String mo25073s() {
        return mo24203f("gad_ad_id");
    }

    /* renamed from: t */
    public String mo25074t() {
        return mo24203f("gad_ad_pos");
    }

    /* renamed from: u */
    public String mo25075u() {
        return mo24203f("gad_ad_res_id");
    }

    /* renamed from: v */
    public int mo25076v() {
        return mo24200d("gad_ad_seq").intValue();
    }

    /* renamed from: w */
    public String mo25077w() {
        return mo24203f("gad_ad_start");
    }

    /* renamed from: x */
    public boolean mo25078x() {
        return mo24200d("gad_ad_is_ad").intValue() == 1;
    }

    /* renamed from: a */
    public void mo25060a(boolean z) {
        mo24193a("gad_ad_is_ad", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: h */
    public void mo25063h(String str) {
        mo24195a("gad_ad_ac_id", str);
    }

    /* renamed from: a */
    public void mo25059a(AdInfo adInfo) {
        mo25058a(adInfo.f9169P);
        mo25066k(adInfo.f9170Q);
        mo25063h(adInfo.f9171R);
        mo25065j(adInfo.f9172S);
        boolean z = true;
        if (adInfo.f9173T != 1) {
            z = false;
        }
        mo25060a(z);
        mo25068m(adInfo.f9174U);
        mo25064i(adInfo.f9175V);
        mo25062b(false);
        ResInfo resInfo = adInfo.f9176W;
        new Res().mo25142a(resInfo);
        mo25067l(resInfo.f9185P);
        mo24181l();
    }

    /* renamed from: b */
    public void mo25062b(boolean z) {
        mo24193a("gad_ad_wait_del", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: b */
    public List<Ad> mo25061b(String str, String str2, Boolean bool) {
        String[] strArr;
        ArrayList arrayList = new ArrayList();
        String str3 = "gad_ad_ac_id=? ";
        if (str2 != null) {
            str3 = str3 + " and (gad_ad_pos=? or gad_ad_pos like ? or gad_ad_pos like ? or gad_ad_pos like ?)";
            strArr = new String[]{str, str2};
        } else {
            strArr = new String[]{str};
        }
        String[] strArr2 = strArr;
        if (bool != null) {
            str3 = str3 + " and gad_ad_wait_del=" + (bool.booleanValue() ? 1 : 0);
        }
        Iterator<Map<String, Object>> it = mo24183g().mo24215a(this, null, str3, strArr2, null).iterator();
        while (it.hasNext()) {
            Ad bVar = new Ad();
            bVar.mo24196a(it.next());
            arrayList.add(bVar);
        }
        return arrayList;
    }

    /* renamed from: a */
    public List<Ad> mo25056a(String str, String str2, Boolean bool) {
        return mo25057a(str, str2, bool, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c[], int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: a */
    public List<Ad> mo25057a(String str, String str2, Boolean bool, String str3) {
        String[] strArr;
        String str4 = str2;
        ArrayList arrayList = new ArrayList();
        String str5 = "gad_ad_res_id=gad_res_id and gad_ad_ac_id=? and gad_res_download=1 and (gad_ad_pos=? or gad_ad_pos like ? or gad_ad_pos like ? or gad_ad_pos like ?) and gad_ad_start<=? and gad_ad_end>=?";
        if (bool != null) {
            str5 = str5 + " and gad_ad_wait_del=" + (bool.booleanValue() ? 1 : 0);
        }
        if (str3 != null) {
            str5 = str5 + " and gad_ad_res_id=?";
        }
        String str6 = str5;
        String a = DateUtil.m15237a(System.currentTimeMillis(), "yyyy-MM-dd");
        if (str3 != null) {
            strArr = new String[]{str, str4, str4 + ",%", "%," + str4, "%," + str4 + ",%", a, a, str3};
        } else {
            strArr = new String[]{str, str4, str4 + ",%", "%," + str4, "%," + str4 + ",%", a, a};
        }
        Iterator<Map<String, Object>> it = mo24183g().mo24218a(new IOrmTable[]{this, new Res()}, false, new String[]{mo24185a() + ".*"}, str6, strArr, (String) null, (String) null, "gad_ad_pos asc", (Integer) null, (Integer) null).iterator();
        while (it.hasNext()) {
            Ad bVar = new Ad();
            bVar.mo24196a(it.next());
            arrayList.add(bVar);
        }
        return arrayList;
    }
}
