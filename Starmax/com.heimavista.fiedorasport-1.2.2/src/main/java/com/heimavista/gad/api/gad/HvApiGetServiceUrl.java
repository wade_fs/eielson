package com.heimavista.gad.api.gad;

import com.heimavista.api.HvApiBase;
import com.heimavista.api.HvApiResponse;
import com.heimavista.gad.HvGadConfig;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetServiceUrl extends HvApiBase {
    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getServiceUrl";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedLogin() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
    }

    public HvGadConfig requestSync() {
        HvApiResponse requestSync = super.requestSync(null);
        if (requestSync.mo22515d() == 1) {
            return new HvGadConfig(requestSync.mo22506a().mo25325a("serviceUrl", ""), requestSync.mo22506a().mo25325a("logUrl", ""), requestSync.mo22506a().mo25325a("appDevNbr", ""), requestSync.mo22506a().mo25325a("prodId", ""));
        }
        return null;
    }
}
