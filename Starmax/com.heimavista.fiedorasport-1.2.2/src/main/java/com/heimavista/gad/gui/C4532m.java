package com.heimavista.gad.gui;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.gui.m */
/* compiled from: Ex2ResBaseFragment */
class C4532m implements OnResultListener<Integer> {

    /* renamed from: a */
    final /* synthetic */ Ex2ResBaseFragment f9155a;

    C4532m(Ex2ResBaseFragment ex2ResBaseFragment) {
        this.f9155a = ex2ResBaseFragment;
    }

    /* renamed from: a */
    public void mo22380a(Integer num) {
        int unused = this.f9155a.f9101W = num.intValue();
        this.f9155a.m14774d();
        this.f9155a.f9096R.setEnabled(true);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f9155a.m14772c();
        this.f9155a.m14776e();
        this.f9155a.f9096R.setEnabled(true);
    }
}
