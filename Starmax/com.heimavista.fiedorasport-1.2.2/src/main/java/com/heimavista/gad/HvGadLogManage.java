package com.heimavista.gad;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.heimavista.gad.api.log.HvApiClick;
import com.heimavista.gad.api.log.HvApiView;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.p202u.Log;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import p119e.p189e.p219h.HttpParams;
import p119e.p189e.p219h.HttpRequest;
import p119e.p189e.p219h.HttpRequestConfig;
import p119e.p189e.p219h.HttpResponse;

/* renamed from: com.heimavista.gad.r */
public class HvGadLogManage {

    /* renamed from: a */
    private boolean f9266a;

    /* renamed from: b */
    private long f9267b;

    /* renamed from: com.heimavista.gad.r$b */
    /* compiled from: HvGadLogManage */
    private static class C4552b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static HvGadLogManage f9268a = new HvGadLogManage();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: c */
    private void m14967c() {
        JSONObject jSONObject;
        while (m14969e()) {
            List<Log> a = new Log().mo25136a(100);
            if (a.size() != 0) {
                JSONArray jSONArray = new JSONArray();
                for (Log gVar : a) {
                    ParamJsonData fVar = new ParamJsonData(gVar.mo24232o(), true);
                    HttpParams cVar = new HttpParams();
                    cVar.mo27197a(fVar.mo25333c());
                    if ("click".equals(gVar.mo25140p())) {
                        jSONObject = new HvApiClick().getLogParams(cVar).mo27203c();
                    } else {
                        jSONObject = new HvApiView().getLogParams(cVar).mo27203c();
                    }
                    jSONArray.put(jSONObject);
                }
                String jSONArray2 = jSONArray.toString();
                HttpRequestConfig.C4928b bVar = new HttpRequestConfig.C4928b();
                bVar.mo27211a(HvGad.m14915i().mo25011c().f9254b);
                HttpResponse a2 = new HttpRequest(bVar.mo27212a()).mo27206a(jSONArray2);
                LogUtils.m1123a(Boolean.valueOf(a2.mo27219a()), a2.mo27220b());
                if (!a2.mo27219a() && new ParamJsonData(a2.mo27220b(), true).mo25323a("retCode", (Integer) 0).intValue() == 1) {
                    for (Log gVar2 : a) {
                        gVar2.mo24199d();
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: d */
    public static HvGadLogManage m14968d() {
        return C4552b.f9268a;
    }

    /* renamed from: e */
    private boolean m14969e() {
        return !TextUtils.isEmpty(HvGad.m14915i().mo25011c().f9254b) && NetworkUtils.m858c();
    }

    /* renamed from: a */
    public void mo25031a(LogInfo dVar) {
        Log.m15107b("click", dVar.toString());
        mo25032b();
    }

    /* renamed from: b */
    public void mo25033b(LogInfo dVar) {
        Log.m15107b(ViewHierarchyConstants.VIEW_KEY, dVar.toString());
        mo25032b();
    }

    private HvGadLogManage() {
    }

    /* renamed from: a */
    public /* synthetic */ void mo25030a() {
        m14967c();
        this.f9266a = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo25032b() {
        if (m14969e() && System.currentTimeMillis() - this.f9267b >= DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS) {
            this.f9267b = System.currentTimeMillis();
            if (!this.f9266a) {
                this.f9266a = true;
                new Thread(new C4544k(this)).start();
            }
        }
    }
}
