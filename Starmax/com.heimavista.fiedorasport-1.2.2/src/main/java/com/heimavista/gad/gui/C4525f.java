package com.heimavista.gad.gui;

import android.view.View;
import com.heimavista.gad.info.ExAdInfo;

/* renamed from: com.heimavista.gad.gui.f */
/* compiled from: lambda */
public final /* synthetic */ class C4525f implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ExAdFragment f9146P;

    /* renamed from: Q */
    private final /* synthetic */ ExAdInfo f9147Q;

    public /* synthetic */ C4525f(ExAdFragment exAdFragment, ExAdInfo bVar) {
        this.f9146P = exAdFragment;
        this.f9147Q = bVar;
    }

    public final void onClick(View view) {
        this.f9146P.mo24931a(this.f9147Q, view);
    }
}
