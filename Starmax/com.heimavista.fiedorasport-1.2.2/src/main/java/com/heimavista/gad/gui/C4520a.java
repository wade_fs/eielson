package com.heimavista.gad.gui;

import android.view.View;
import com.heimavista.gad.info.Ex2AdInfo;

/* renamed from: com.heimavista.gad.gui.a */
/* compiled from: lambda */
public final /* synthetic */ class C4520a implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ Ex2ResBaseFragment f9140P;

    /* renamed from: Q */
    private final /* synthetic */ Ex2AdInfo f9141Q;

    public /* synthetic */ C4520a(Ex2ResBaseFragment ex2ResBaseFragment, Ex2AdInfo ex2AdInfo) {
        this.f9140P = ex2ResBaseFragment;
        this.f9141Q = ex2AdInfo;
    }

    public final void onClick(View view) {
        this.f9140P.mo24924a(this.f9141Q, view);
    }
}
