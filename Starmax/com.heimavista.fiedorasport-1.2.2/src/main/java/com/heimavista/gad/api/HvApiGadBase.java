package com.heimavista.gad.api;

import com.facebook.internal.ServerProtocol;
import com.heimavista.api.HvApi;
import com.heimavista.gad.HvGad;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p219h.HttpParams;

public abstract class HvApiGadBase extends HvApi {
    public static String getAppDevNbr() {
        return HvGad.m14915i().mo25011c().f9255c;
    }

    public static String getProdId() {
        return HvGad.m14915i().mo25011c().f9256d;
    }

    /* access modifiers changed from: protected */
    public String getAppId() {
        return HvAppConfig.m13024a().mo24166a("gad", "id");
    }

    /* access modifiers changed from: protected */
    public String getAppSecret() {
        return HvAppConfig.m13024a().mo24166a("gad", "secret");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* access modifiers changed from: protected */
    public HttpParams getBasicParams() {
        HttpParams cVar = new HttpParams();
        if (isNeedUserNbr() && HvGad.m14915i().mo25014d().mo25023a()) {
            cVar.mo27196a("userNbr", (Object) HvGad.m14915i().mo25014d().mo25024b());
        }
        cVar.mo27196a("prodId", (Object) getProdId());
        cVar.mo27196a("appDevNbr", (Object) getAppDevNbr());
        return cVar;
    }

    /* access modifiers changed from: protected */
    public String getSdkVersion() {
        return "1.0.0";
    }

    /* access modifiers changed from: protected */
    public String getUri() {
        return HvGad.m14915i().mo25011c().f9253a;
    }

    /* access modifiers changed from: protected */
    public String getVersion() {
        return HvAppConfig.m13024a().mo24166a("gad", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
    }

    /* access modifiers changed from: protected */
    public abstract boolean isNeedUserNbr();

    /* access modifiers changed from: protected */
    public void onApiCancel(HttpParams cVar, HttpParams cVar2, String str) {
    }

    /* access modifiers changed from: protected */
    public void onApiFailed(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
    }

    /* access modifiers changed from: protected */
    public void onApiNewInform() {
    }
}
