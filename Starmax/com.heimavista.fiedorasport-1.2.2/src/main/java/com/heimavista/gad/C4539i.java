package com.heimavista.gad;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

/* renamed from: com.heimavista.gad.i */
/* compiled from: lambda */
public final /* synthetic */ class C4539i implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ HvGad f9162P;

    /* renamed from: Q */
    private final /* synthetic */ EditText f9163Q;

    /* renamed from: R */
    private final /* synthetic */ ImageView f9164R;

    /* renamed from: S */
    private final /* synthetic */ Button f9165S;

    /* renamed from: T */
    private final /* synthetic */ Context f9166T;

    /* renamed from: U */
    private final /* synthetic */ OnGadStartListener f9167U;

    /* renamed from: V */
    private final /* synthetic */ AlertDialog f9168V;

    public /* synthetic */ C4539i(HvGad lVar, EditText editText, ImageView imageView, Button button, Context context, OnGadStartListener tVar, AlertDialog alertDialog) {
        this.f9162P = lVar;
        this.f9163Q = editText;
        this.f9164R = imageView;
        this.f9165S = button;
        this.f9166T = context;
        this.f9167U = tVar;
        this.f9168V = alertDialog;
    }

    public final void onClick(View view) {
        this.f9162P.mo25005a(this.f9163Q, this.f9164R, this.f9165S, this.f9166T, this.f9167U, this.f9168V, view);
    }
}
