package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.f */
public class Like extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_like_mstr";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_like_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_like_res_id", OrmColumnType.STRING, "");
        mo24173a("gad_like_res_info", OrmColumnType.STRING, "");
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_like_ac_id", "gad_like_res_id"};
    }

    /* renamed from: i */
    public void mo25134i(String str) {
        mo24195a("gad_like_res_info", str);
    }

    /* renamed from: j */
    public void mo25135j(String str) {
        mo24195a("gad_like_res_id", str);
    }

    /* renamed from: h */
    public void mo25133h(String str) {
        mo24195a("gad_like_ac_id", str);
    }
}
