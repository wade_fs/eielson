package com.heimavista.gad.api.log;

import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.heimavista.gad.api.HvApiLogBase;

public class HvApiView extends HvApiLogBase {
    /* access modifiers changed from: protected */
    public String getOp() {
        return ViewHierarchyConstants.VIEW_KEY;
    }
}
