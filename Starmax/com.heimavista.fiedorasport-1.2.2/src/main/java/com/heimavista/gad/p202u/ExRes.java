package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.info.ExResInfo;
import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.e */
public class ExRes extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_exres_mstr";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* renamed from: a */
    public void mo25126a(ExResInfo cVar) {
        mo25129h(cVar.f9209a);
        mo25130i(cVar.mo24990b());
        mo25127a(false);
        mo25128b(System.currentTimeMillis());
        mo24181l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.e.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_exres_id", OrmColumnType.STRING, "");
        mo24173a("gad_exres_info", OrmColumnType.STRING, "");
        mo24173a("gad_exres_download", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_exres_time", OrmColumnType.LONG, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_exres_id"};
    }

    /* renamed from: i */
    public void mo25130i(String str) {
        mo24195a("gad_exres_info", str);
    }

    /* renamed from: o */
    public List<String> mo24232o() {
        ArrayList arrayList = new ArrayList();
        Iterator<Map<String, Object>> it = mo24183g().mo24215a(this, new String[]{"gad_exres_id"}, null, null, null).iterator();
        while (it.hasNext()) {
            arrayList.add(MapUtil.m15243a(it.next(), "gad_exres_id", ""));
        }
        return arrayList;
    }

    /* renamed from: p */
    public String mo25131p() {
        return mo24203f("gad_exres_info");
    }

    /* renamed from: q */
    public ExResInfo mo25132q() {
        return ExResInfo.m14890a(mo25131p());
    }

    /* renamed from: h */
    public void mo25129h(String str) {
        mo24195a("gad_exres_id", str);
    }

    /* renamed from: b */
    public void mo25128b(long j) {
        mo24194a("gad_exres_time", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo25127a(boolean z) {
        mo24193a("gad_exres_download", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: a */
    public int mo25125a(long j) {
        ExAd dVar = new ExAd();
        mo24183g().mo24219a(dVar);
        return mo24178c("gad_exres_time<? and gad_exres_id not in(select gad_exad_res_id from " + dVar.mo24185a() + ")", new String[]{String.valueOf(j)});
    }
}
