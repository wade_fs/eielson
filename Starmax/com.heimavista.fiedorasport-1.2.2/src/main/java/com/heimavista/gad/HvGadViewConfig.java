package com.heimavista.gad;

import android.os.Parcel;
import android.os.Parcelable;

public class HvGadViewConfig implements Parcelable {
    public static final Parcelable.Creator<HvGadViewConfig> CREATOR = new C4501a();
    /* access modifiers changed from: private */

    /* renamed from: P */
    public int f9055P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public int f9056Q;

    /* renamed from: com.heimavista.gad.HvGadViewConfig$a */
    static class C4501a implements Parcelable.Creator<HvGadViewConfig> {
        C4501a() {
        }

        public HvGadViewConfig createFromParcel(Parcel parcel) {
            return new HvGadViewConfig(parcel);
        }

        public HvGadViewConfig[] newArray(int i) {
            return new HvGadViewConfig[i];
        }
    }

    /* renamed from: com.heimavista.gad.HvGadViewConfig$b */
    public static class C4502b {

        /* renamed from: a */
        private HvGadViewConfig f9057a = new HvGadViewConfig();

        public C4502b(int i, int i2) {
            int unused = this.f9057a.f9055P = i;
            int unused2 = this.f9057a.f9056Q = i2;
        }

        /* renamed from: a */
        public HvGadViewConfig mo24895a() {
            return this.f9057a;
        }
    }

    protected HvGadViewConfig() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f9055P);
        parcel.writeInt(this.f9056Q);
    }

    protected HvGadViewConfig(Parcel parcel) {
        this.f9055P = parcel.readInt();
        this.f9056Q = parcel.readInt();
    }

    /* renamed from: a */
    public int mo24889a() {
        return this.f9056Q;
    }

    /* renamed from: b */
    public int mo24890b() {
        return this.f9055P;
    }
}
