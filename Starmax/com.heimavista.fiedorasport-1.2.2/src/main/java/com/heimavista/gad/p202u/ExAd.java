package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.gad.info.ExAdInfo;
import com.heimavista.gad.info.ExResInfo;
import com.heimavista.utils.DateUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.IOrmTable;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p193d.OrmDB;

/* renamed from: com.heimavista.gad.u.d */
public class ExAd extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_exad_mstr";
    }

    /* renamed from: a */
    public void mo25101a(int i) {
        mo24193a("gad_exad_seq", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.d.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.d>
      com.heimavista.gad.u.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_exad_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_exad_seq", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_exad_pos", OrmColumnType.STRING, "");
        mo24173a("gad_exad_ex_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_exad_id", OrmColumnType.STRING, "");
        mo24173a("gad_exad_is_ad", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_exad_start", OrmColumnType.STRING, "");
        mo24173a("gad_exad_end", OrmColumnType.STRING, "");
        mo24173a("gad_exad_res_id", OrmColumnType.STRING, "");
        mo24173a("gad_exad_wait_del", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_exad_seq"};
    }

    /* renamed from: i */
    public void mo25107i(String str) {
        mo24195a("gad_exad_ac_id", str);
    }

    /* renamed from: j */
    public void mo25108j(String str) {
        mo24195a("gad_exad_end", str);
    }

    /* renamed from: k */
    public void mo25109k(String str) {
        mo24195a("gad_exad_ex_ac_id", str);
    }

    /* renamed from: l */
    public void mo25110l(String str) {
        mo24195a("gad_exad_id", str);
    }

    /* renamed from: m */
    public void mo25111m(String str) {
        mo24195a("gad_exad_pos", str);
    }

    /* renamed from: n */
    public void mo25112n(String str) {
        mo24195a("gad_exad_res_id", str);
    }

    /* renamed from: o */
    public void mo25113o(String str) {
        mo24195a("gad_exad_start", str);
    }

    /* renamed from: p */
    public String mo25114p() {
        return mo24203f("gad_exad_ac_id");
    }

    /* renamed from: q */
    public ExAdInfo mo25116q() {
        ExAdInfo bVar = new ExAdInfo();
        bVar.f9201b = mo25122w();
        bVar.f9202c = mo25120u();
        bVar.f9200a = mo25114p();
        bVar.f9203d = mo25118s();
        bVar.f9204e = mo25119t();
        bVar.f9205f = mo25124y() ? 1 : 0;
        bVar.f9206g = mo25123x();
        bVar.f9207h = mo25117r();
        ExRes eVar = new ExRes();
        eVar.mo25129h(mo25121v());
        eVar.mo24206j();
        bVar.f9208i = eVar.mo25132q();
        return bVar;
    }

    /* renamed from: r */
    public String mo25117r() {
        return mo24203f("gad_exad_end");
    }

    /* renamed from: s */
    public String mo25118s() {
        return mo24203f("gad_exad_ex_ac_id");
    }

    /* renamed from: t */
    public String mo25119t() {
        return mo24203f("gad_exad_id");
    }

    /* renamed from: u */
    public String mo25120u() {
        return mo24203f("gad_exad_pos");
    }

    /* renamed from: v */
    public String mo25121v() {
        return mo24203f("gad_exad_res_id");
    }

    /* renamed from: w */
    public int mo25122w() {
        return mo24200d("gad_exad_seq").intValue();
    }

    /* renamed from: x */
    public String mo25123x() {
        return mo24203f("gad_exad_start");
    }

    /* renamed from: y */
    public boolean mo25124y() {
        return mo24200d("gad_exad_is_ad").intValue() == 1;
    }

    /* renamed from: a */
    public void mo25103a(boolean z) {
        mo24193a("gad_exad_is_ad", Integer.valueOf(z ? 1 : 0));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c[], int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: h */
    public List<ExAd> mo25106h(String str) {
        ArrayList arrayList = new ArrayList();
        LogUtils.m1123a("gad_exad_res_id=gad_exres_id and gad_exad_ac_id=? and gad_exres_download=0 and gad_exad_wait_del=0", str);
        OrmDB g = mo24183g();
        IOrmTable[] cVarArr = {this, new ExRes()};
        Iterator<Map<String, Object>> it = g.mo24218a(cVarArr, false, new String[]{mo24185a() + ".*"}, "gad_exad_res_id=gad_exres_id and gad_exad_ac_id=? and gad_exres_download=0 and gad_exad_wait_del=0", new String[]{str}, (String) null, (String) null, "gad_exad_pos asc", (Integer) null, (Integer) null).iterator();
        while (it.hasNext()) {
            ExAd dVar = new ExAd();
            dVar.mo24196a(it.next());
            arrayList.add(dVar);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.gad.u.d.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.d>
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.heimavista.gad.u.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.heimavista.gad.u.d.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.d> */
    /* renamed from: o */
    public void mo24232o() {
        boolean z;
        for (ExAd dVar : mo25104b(mo25114p(), null, true)) {
            String[] split = dVar.mo25120u().split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = true;
                    break;
                }
                String str = split[i];
                if (mo25104b(mo25114p(), str, false).size() != 0 && mo25100a(mo25114p(), str, (Boolean) false).size() == 0) {
                    z = false;
                    break;
                }
                i++;
            }
            if (z) {
                mo24178c("gad_exad_ac_id=? and gad_exad_seq=" + dVar.mo25122w(), new String[]{mo25114p()});
            }
        }
    }

    /* renamed from: p */
    public void mo25115p(String str) {
        mo24188a("gad_exad_ac_id=?", new String[]{str});
    }

    /* renamed from: a */
    public void mo25102a(ExAdInfo bVar) {
        mo25101a(bVar.f9201b);
        mo25111m(bVar.f9202c);
        mo25107i(bVar.f9200a);
        mo25109k(bVar.f9203d);
        mo25110l(bVar.f9204e);
        boolean z = true;
        if (bVar.f9205f != 1) {
            z = false;
        }
        mo25103a(z);
        mo25113o(bVar.f9206g);
        mo25108j(bVar.f9207h);
        mo25105b(false);
        ExResInfo cVar = bVar.f9208i;
        new ExRes().mo25126a(cVar);
        mo25112n(cVar.f9209a);
        mo24181l();
    }

    /* renamed from: b */
    public void mo25105b(boolean z) {
        mo24193a("gad_exad_wait_del", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: b */
    public List<ExAd> mo25104b(String str, String str2, Boolean bool) {
        String[] strArr;
        ArrayList arrayList = new ArrayList();
        String str3 = "gad_exad_ac_id=? ";
        if (str2 != null) {
            str3 = str3 + " and (gad_exad_pos=? or gad_exad_pos like ? or gad_exad_pos like ? or gad_exad_pos like ?)";
            strArr = new String[]{str, str2};
        } else {
            strArr = new String[]{str};
        }
        String[] strArr2 = strArr;
        if (bool != null) {
            str3 = str3 + " and gad_exad_wait_del=" + (bool.booleanValue() ? 1 : 0);
        }
        Iterator<Map<String, Object>> it = mo24183g().mo24215a(this, null, str3, strArr2, null).iterator();
        while (it.hasNext()) {
            ExAd dVar = new ExAd();
            dVar.mo24196a(it.next());
            arrayList.add(dVar);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c[], int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: a */
    public List<ExAd> mo25100a(String str, String str2, Boolean bool) {
        String str3 = str2;
        ArrayList arrayList = new ArrayList();
        String str4 = "gad_exad_res_id=gad_exres_id and gad_exad_ac_id=? and gad_exres_download=1 and (gad_exad_pos=? or gad_exad_pos like ? or gad_exad_pos like ? or gad_exad_pos like ?) and gad_exad_start<=? and gad_exad_end>=?";
        if (bool != null) {
            str4 = str4 + " and gad_exad_wait_del=" + (bool.booleanValue() ? 1 : 0);
        }
        String str5 = str4;
        String a = DateUtil.m15237a(System.currentTimeMillis(), "yyyy-MM-dd");
        String[] strArr = {str, str3, str3 + ",%", "%," + str3, "%," + str3 + ",%", a, a};
        OrmDB g = mo24183g();
        IOrmTable[] cVarArr = {this, new ExRes()};
        StringBuilder sb = new StringBuilder();
        sb.append(mo24185a());
        sb.append(".*");
        Iterator<Map<String, Object>> it = g.mo24218a(cVarArr, false, new String[]{sb.toString()}, str5, strArr, (String) null, (String) null, "gad_exad_pos asc", (Integer) null, (Integer) null).iterator();
        while (it.hasNext()) {
            ExAd dVar = new ExAd();
            dVar.mo24196a(it.next());
            arrayList.add(dVar);
        }
        return arrayList;
    }
}
