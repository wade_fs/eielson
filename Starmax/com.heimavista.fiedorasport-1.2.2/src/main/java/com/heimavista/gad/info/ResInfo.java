package com.heimavista.gad.info;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.blankj.utilcode.util.FileUtils;
import com.heimavista.gad.p202u.Res;
import java.io.File;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class ResInfo implements Parcelable {
    public static final Parcelable.Creator<ResInfo> CREATOR = new C4542a();

    /* renamed from: P */
    public String f9185P;

    /* renamed from: Q */
    public String f9186Q;

    /* renamed from: R */
    public String f9187R;

    /* renamed from: S */
    public String f9188S;

    /* renamed from: T */
    public int f9189T;

    /* renamed from: U */
    public int f9190U;

    /* renamed from: V */
    public int f9191V;

    /* renamed from: W */
    public String f9192W;

    /* renamed from: X */
    public int f9193X;

    /* renamed from: com.heimavista.gad.info.ResInfo$a */
    static class C4542a implements Parcelable.Creator<ResInfo> {
        C4542a() {
        }

        public ResInfo createFromParcel(Parcel parcel) {
            return new ResInfo(parcel);
        }

        public ResInfo[] newArray(int i) {
            return new ResInfo[i];
        }
    }

    public ResInfo() {
    }

    /* renamed from: a */
    public static ResInfo m14879a(String str) {
        ResInfo resInfo = new ResInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            resInfo.f9185P = jSONObject.optString("resId", "");
            resInfo.f9186Q = jSONObject.optString("type", "");
            resInfo.f9187R = jSONObject.optString("url", "");
            resInfo.f9188S = jSONObject.optString("link", "");
            resInfo.f9189T = jSONObject.optInt("w", 0);
            resInfo.f9190U = jSONObject.optInt("h", 0);
            resInfo.f9191V = jSONObject.optInt("duration", 0);
            resInfo.f9192W = jSONObject.optString("thumbUrl", "");
            resInfo.f9193X = jSONObject.optInt("isAuto", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resInfo;
    }

    /* renamed from: c */
    public static String m14883c(Context context) {
        File externalFilesDir = context.getExternalFilesDir("HvGadRes");
        if (externalFilesDir == null) {
            return null;
        }
        return externalFilesDir.getAbsolutePath();
    }

    /* renamed from: b */
    public String mo24982b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("resId", this.f9185P);
            jSONObject.put("type", this.f9186Q);
            jSONObject.put("url", this.f9187R);
            jSONObject.put("link", this.f9188S);
            jSONObject.put("w", this.f9189T);
            jSONObject.put("h", this.f9190U);
            jSONObject.put("duration", this.f9191V);
            jSONObject.put("thumbUrl", this.f9192W);
            jSONObject.put("isAuto", this.f9193X);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f9185P);
        parcel.writeString(this.f9186Q);
        parcel.writeString(this.f9187R);
        parcel.writeString(this.f9188S);
        parcel.writeInt(this.f9189T);
        parcel.writeInt(this.f9190U);
        parcel.writeInt(this.f9191V);
        parcel.writeString(this.f9192W);
        parcel.writeInt(this.f9193X);
    }

    protected ResInfo(Parcel parcel) {
        this.f9185P = parcel.readString();
        this.f9186Q = parcel.readString();
        this.f9187R = parcel.readString();
        this.f9188S = parcel.readString();
        this.f9189T = parcel.readInt();
        this.f9190U = parcel.readInt();
        this.f9191V = parcel.readInt();
        this.f9192W = parcel.readString();
        this.f9193X = parcel.readInt();
    }

    /* renamed from: b */
    public static void m14882b(Context context) {
        List<String> o = new Res().mo24232o();
        String c = m14883c(context);
        if (new File(c).isDirectory()) {
            for (File file : FileUtils.m1098c(c)) {
                if (file.isDirectory() && !o.contains(file.getName())) {
                    file.deleteOnExit();
                }
            }
        }
    }

    /* renamed from: a */
    public String[] mo24981a() {
        String str = this.f9187R;
        if (str != null) {
            return str.split(",");
        }
        return null;
    }

    /* renamed from: a */
    public static String m14880a(Context context, String str) {
        String c = m14883c(context);
        if (TextUtils.isEmpty(c)) {
            return null;
        }
        return c + File.separator + str;
    }

    /* renamed from: a */
    public static String m14881a(Context context, String str, String str2) {
        String a = m14880a(context, str);
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        return a + File.separator + str2.substring(str2.lastIndexOf("/") + 1);
    }

    /* renamed from: a */
    public boolean mo24980a(Context context) {
        String[] a = mo24981a();
        for (String str : a) {
            if (URLUtil.isNetworkUrl(str)) {
                String a2 = m14881a(context, this.f9185P, str);
                if (TextUtils.isEmpty(a2) || !new File(a2).isFile()) {
                    return false;
                }
            }
        }
        if (!URLUtil.isNetworkUrl(this.f9192W)) {
            return true;
        }
        String a3 = m14881a(context, this.f9185P, this.f9192W);
        if (TextUtils.isEmpty(a3) || !new File(a3).isFile()) {
            return false;
        }
        return true;
    }
}
