package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.h */
public class Res extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_res_mstr";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* renamed from: a */
    public void mo25142a(ResInfo resInfo) {
        mo25145h(resInfo.f9185P);
        mo25146i(resInfo.mo24982b());
        mo25143a(false);
        mo25144b(System.currentTimeMillis());
        mo24181l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.h.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_res_id", OrmColumnType.STRING, "");
        mo24173a("gad_res_info", OrmColumnType.STRING, "");
        mo24173a("gad_res_download", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_res_time", OrmColumnType.LONG, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_res_id"};
    }

    /* renamed from: i */
    public void mo25146i(String str) {
        mo24195a("gad_res_info", str);
    }

    /* renamed from: o */
    public List<String> mo24232o() {
        ArrayList arrayList = new ArrayList();
        Iterator<Map<String, Object>> it = mo24183g().mo24215a(this, new String[]{"gad_res_id"}, null, null, null).iterator();
        while (it.hasNext()) {
            arrayList.add(MapUtil.m15243a(it.next(), "gad_res_id", ""));
        }
        return arrayList;
    }

    /* renamed from: p */
    public String mo25147p() {
        return mo24203f("gad_res_info");
    }

    /* renamed from: q */
    public ResInfo mo25148q() {
        return ResInfo.m14879a(mo25147p());
    }

    /* renamed from: r */
    public List<String> mo25149r() {
        ArrayList arrayList = new ArrayList();
        Iterator<Map<String, Object>> it = mo24183g().mo24215a(this, new String[]{"gad_res_id"}, "gad_res_download=0", null, null).iterator();
        while (it.hasNext()) {
            arrayList.add(MapUtil.m15243a(it.next(), "gad_res_id", ""));
        }
        return arrayList;
    }

    /* renamed from: h */
    public void mo25145h(String str) {
        mo24195a("gad_res_id", str);
    }

    /* renamed from: b */
    public void mo25144b(long j) {
        mo24194a("gad_res_time", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo25143a(boolean z) {
        mo24193a("gad_res_download", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: a */
    public int mo25141a(long j) {
        Ad bVar = new Ad();
        mo24183g().mo24219a(bVar);
        return mo24178c("gad_res_time<? and gad_res_id not in(select gad_ad_res_id from " + bVar.mo24185a() + ")", new String[]{String.valueOf(j)});
    }
}
