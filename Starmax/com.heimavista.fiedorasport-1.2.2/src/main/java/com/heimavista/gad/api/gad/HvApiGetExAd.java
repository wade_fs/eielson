package com.heimavista.gad.api.gad;

import android.os.Build;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.ExAdInfo;
import com.heimavista.gad.p202u.ExAd;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetExAd extends HvApiGadBase {
    private static final String KEY_PRE_EXPIRES = "GetByExAd_Expires_";

    private static int getExpires(String str) {
        SPUtils e = HvGad.m14915i().mo25015e();
        return e.mo9869a(KEY_PRE_EXPIRES + str, 0);
    }

    public static boolean isExpire(String str) {
        return ((long) getExpires(str)) < System.currentTimeMillis() / 1000;
    }

    private static void saveExpires(String str, int i) {
        SPUtils e = HvGad.m14915i().mo25015e();
        e.mo9881b(KEY_PRE_EXPIRES + str, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, int]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public boolean checkExpiresSync(String str) {
        if (!isExpire(str)) {
            return true;
        }
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("acId", (Object) str);
        cVar.mo27196a("os", (Object) 2);
        cVar.mo27196a("osv", (Object) Build.VERSION.RELEASE);
        cVar.mo27196a("lang", (Object) Locale.getDefault().getLanguage());
        cVar.mo27196a("brand", (Object) Build.BRAND);
        cVar.mo27196a("model", (Object) Build.MODEL);
        if (requestSync(cVar).mo22515d() != 1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getExAd";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("acId", "");
        if (fVar.mo25334d().has("Rows")) {
            List<Object> a2 = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
            ExAd dVar = new ExAd();
            dVar.mo25105b(true);
            dVar.mo25115p(a);
            for (Object obj : a2) {
                dVar.mo25102a(ExAdInfo.m14889a(a, String.valueOf(obj)));
            }
        }
        if (fVar.mo25334d().has("expires")) {
            saveExpires(a, (int) (((long) fVar.mo25323a("expires", (Integer) 0).intValue()) + (System.currentTimeMillis() / 1000)));
        }
    }
}
