package com.heimavista.gad.api.gad;

import android.os.Bundle;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.api.HvApiGadBase;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.p202u.Ex2Ad;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpParams;

public class HvApiGetEx2Ad extends HvApiGadBase {
    public static final String ACTION_NEW = (HvApiGetEx2Ad.class.getName() + ".NEW");
    private static final String KEY_PRE_EXPIRES = "GetEx2Ad_Expires_";
    private static final String KEY_PRE_TICK = "GetEx2Ad_Tick_";

    private static int getExpires(String str) {
        SPUtils e = HvGad.m14915i().mo25015e();
        return e.mo9869a(KEY_PRE_EXPIRES + str, 0);
    }

    private static int getTick(String str) {
        SPUtils e = HvGad.m14915i().mo25015e();
        return e.mo9869a(KEY_PRE_TICK + str, 0);
    }

    public static boolean isExpire(String str) {
        return ((long) getExpires(str)) < System.currentTimeMillis() / 1000;
    }

    private static void saveExpires(String str, int i) {
        SPUtils e = HvGad.m14915i().mo25015e();
        e.mo9881b(KEY_PRE_EXPIRES + str, i);
    }

    private static void saveTick(String str, int i) {
        SPUtils e = HvGad.m14915i().mo25015e();
        e.mo9881b(KEY_PRE_TICK + str, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      e.e.h.c.a(java.lang.String, java.lang.String):java.lang.String
      e.e.h.c.a(java.lang.String, java.lang.Object):e.e.h.c */
    public boolean checkExpiresSync(String str) {
        if (!isExpire(str)) {
            return true;
        }
        HttpParams cVar = new HttpParams();
        cVar.mo27196a("acId", (Object) str);
        cVar.mo27196a("tick", Integer.valueOf(getTick(str)));
        if (requestSync(cVar).mo22515d() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public String getFun() {
        return "gad";
    }

    /* access modifiers changed from: protected */
    public String getOp() {
        return "getEx2Ad";
    }

    /* access modifiers changed from: protected */
    public boolean isNeedUserNbr() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* access modifiers changed from: protected */
    public void onApiSuccess(HttpParams cVar, HttpParams cVar2, ParamJsonData fVar, String str) {
        String a = cVar.mo27198a("acId", "");
        if (fVar.mo25334d().has("Rows")) {
            List<Object> a2 = new ParamJsonData(fVar.mo25325a("Rows", "[]"), true).mo25327a();
            Ex2Ad cVar3 = new Ex2Ad();
            cVar3.mo25084b(true);
            cVar3.mo25091n(a);
            for (Object obj : a2) {
                cVar3.mo25082a(Ex2AdInfo.m14878a(String.valueOf(obj)));
            }
            Bundle bundle = new Bundle();
            bundle.putString("acId", a);
            HvApp.m13010c().mo24158a(ACTION_NEW, bundle);
        }
        if (fVar.mo25334d().has("tick")) {
            saveTick(a, fVar.mo25323a("tick", (Integer) 0).intValue());
        }
        if (fVar.mo25334d().has("expires")) {
            saveExpires(a, (int) (((long) fVar.mo25323a("expires", (Integer) 0).intValue()) + (System.currentTimeMillis() / 1000)));
        }
    }
}
