package com.heimavista.gad.info;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.heimavista.gad.info.b */
public class ExAdInfo {

    /* renamed from: a */
    public String f9200a;

    /* renamed from: b */
    public int f9201b;

    /* renamed from: c */
    public String f9202c;

    /* renamed from: d */
    public String f9203d;

    /* renamed from: e */
    public String f9204e;

    /* renamed from: f */
    public int f9205f;

    /* renamed from: g */
    public String f9206g;

    /* renamed from: h */
    public String f9207h;

    /* renamed from: i */
    public ExResInfo f9208i;

    /* renamed from: a */
    public static ExAdInfo m14889a(String str, String str2) {
        ExAdInfo bVar = new ExAdInfo();
        bVar.f9200a = str;
        try {
            JSONObject jSONObject = new JSONObject(str2);
            bVar.f9201b = jSONObject.optInt("seq", 0);
            bVar.f9202c = jSONObject.optString("pos", "");
            bVar.f9203d = jSONObject.optString("acId", "");
            bVar.f9204e = jSONObject.optString("adId", "");
            bVar.f9205f = jSONObject.optInt("isAd", 0);
            bVar.f9206g = jSONObject.optString(TtmlNode.START, "");
            bVar.f9207h = jSONObject.optString(TtmlNode.END, "");
            bVar.f9208i = ExResInfo.m14890a(jSONObject.optString("ResInfo", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bVar;
    }
}
