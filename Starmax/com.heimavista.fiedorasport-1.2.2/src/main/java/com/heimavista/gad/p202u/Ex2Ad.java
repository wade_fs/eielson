package com.heimavista.gad.p202u;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.gad.info.Ex2AdInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.utils.DateUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.IOrmTable;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: com.heimavista.gad.u.c */
public class Ex2Ad extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "gad_ex2ad_mstr";
    }

    /* renamed from: a */
    public void mo25081a(int i) {
        mo24193a("gad_ex2ad_seq", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      com.heimavista.gad.u.c.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.c>
      com.heimavista.gad.u.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24173a("gad_ex2ad_seq", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_ex2ad_pos", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_ac_id", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_id", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_is_ad", OrmColumnType.INT, (Object) 0);
        mo24173a("gad_ex2ad_start", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_end", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_res_id", OrmColumnType.STRING, "");
        mo24173a("gad_ex2ad_wait_del", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"gad_ex2ad_seq"};
    }

    /* renamed from: i */
    public void mo25086i(String str) {
        mo24195a("gad_ex2ad_end", str);
    }

    /* renamed from: j */
    public void mo25087j(String str) {
        mo24195a("gad_ex2ad_id", str);
    }

    /* renamed from: k */
    public void mo25088k(String str) {
        mo24195a("gad_ex2ad_pos", str);
    }

    /* renamed from: l */
    public void mo25089l(String str) {
        mo24195a("gad_ex2ad_res_id", str);
    }

    /* renamed from: m */
    public void mo25090m(String str) {
        mo24195a("gad_ex2ad_start", str);
    }

    /* renamed from: n */
    public void mo25091n(String str) {
        mo24188a("gad_ex2ad_ac_id=?", new String[]{str});
    }

    /* renamed from: o */
    public String mo24232o() {
        return mo24203f("gad_ex2ad_ac_id");
    }

    /* renamed from: p */
    public Ex2AdInfo mo25092p() {
        Ex2AdInfo ex2AdInfo = new Ex2AdInfo();
        ex2AdInfo.f9177P = mo25097u();
        ex2AdInfo.f9178Q = mo25095s();
        ex2AdInfo.f9179R = mo24232o();
        ex2AdInfo.f9180S = mo25094r();
        ex2AdInfo.f9181T = mo25099w() ? 1 : 0;
        ex2AdInfo.f9182U = mo25098v();
        ex2AdInfo.f9183V = mo25093q();
        Res hVar = new Res();
        hVar.mo25145h(mo25096t());
        hVar.mo24206j();
        ex2AdInfo.f9184W = hVar.mo25148q();
        return ex2AdInfo;
    }

    /* renamed from: q */
    public String mo25093q() {
        return mo24203f("gad_ex2ad_end");
    }

    /* renamed from: r */
    public String mo25094r() {
        return mo24203f("gad_ex2ad_id");
    }

    /* renamed from: s */
    public String mo25095s() {
        return mo24203f("gad_ex2ad_pos");
    }

    /* renamed from: t */
    public String mo25096t() {
        return mo24203f("gad_ex2ad_res_id");
    }

    /* renamed from: u */
    public int mo25097u() {
        return mo24200d("gad_ex2ad_seq").intValue();
    }

    /* renamed from: v */
    public String mo25098v() {
        return mo24203f("gad_ex2ad_start");
    }

    /* renamed from: w */
    public boolean mo25099w() {
        return mo24200d("gad_ex2ad_is_ad").intValue() == 1;
    }

    /* renamed from: a */
    public void mo25083a(boolean z) {
        mo24193a("gad_ex2ad_is_ad", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: h */
    public void mo25085h(String str) {
        mo24195a("gad_ex2ad_ac_id", str);
    }

    /* renamed from: a */
    public void mo25082a(Ex2AdInfo ex2AdInfo) {
        mo25081a(ex2AdInfo.f9177P);
        mo25088k(ex2AdInfo.f9178Q);
        mo25085h(ex2AdInfo.f9179R);
        mo25087j(ex2AdInfo.f9180S);
        boolean z = true;
        if (ex2AdInfo.f9181T != 1) {
            z = false;
        }
        mo25083a(z);
        mo25090m(ex2AdInfo.f9182U);
        mo25086i(ex2AdInfo.f9183V);
        mo25084b(false);
        ResInfo resInfo = ex2AdInfo.f9184W;
        new Res().mo25142a(resInfo);
        mo25089l(resInfo.f9185P);
        mo24181l();
    }

    /* renamed from: b */
    public void mo25084b(boolean z) {
        mo24193a("gad_ex2ad_wait_del", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: a */
    public List<Ex2Ad> mo25079a(String str, String str2, Boolean bool) {
        return mo25080a(str, str2, bool, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c[], int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: a */
    public List<Ex2Ad> mo25080a(String str, String str2, Boolean bool, String str3) {
        String[] strArr;
        String str4 = str2;
        ArrayList arrayList = new ArrayList();
        String str5 = "gad_ex2ad_res_id=gad_res_id and gad_ex2ad_ac_id=? and gad_res_download=1 and (gad_ex2ad_pos=? or gad_ex2ad_pos like ? or gad_ex2ad_pos like ? or gad_ex2ad_pos like ?) and gad_ex2ad_start<=? and gad_ex2ad_end>=?";
        if (bool != null) {
            str5 = str5 + " and gad_ex2ad_wait_del=" + (bool.booleanValue() ? 1 : 0);
        }
        if (str3 != null) {
            str5 = str5 + " and gad_ex2ad_res_id=?";
        }
        String str6 = str5;
        String a = DateUtil.m15237a(System.currentTimeMillis(), "yyyy-MM-dd");
        if (str3 != null) {
            strArr = new String[]{str, str4, str4 + ",%", "%," + str4, "%," + str4 + ",%", a, a, str3};
        } else {
            strArr = new String[]{str, str4, str4 + ",%", "%," + str4, "%," + str4 + ",%", a, a};
        }
        Iterator<Map<String, Object>> it = mo24183g().mo24218a(new IOrmTable[]{this, new Res()}, false, new String[]{mo24185a() + ".*"}, str6, strArr, (String) null, (String) null, "gad_ex2ad_pos asc", (Integer) null, (Integer) null).iterator();
        while (it.hasNext()) {
            Ex2Ad cVar = new Ex2Ad();
            cVar.mo24196a(it.next());
            arrayList.add(cVar);
        }
        return arrayList;
    }
}
