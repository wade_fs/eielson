package com.heimavista.gad.info;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.blankj.utilcode.util.FileUtils;
import com.heimavista.gad.p202u.ExRes;
import java.io.File;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.heimavista.gad.info.c */
public class ExResInfo {

    /* renamed from: a */
    public String f9209a;

    /* renamed from: b */
    public String f9210b;

    /* renamed from: c */
    public String f9211c;

    /* renamed from: d */
    public String f9212d;

    /* renamed from: e */
    public int f9213e;

    /* renamed from: f */
    public int f9214f;

    /* renamed from: g */
    public int f9215g;

    /* renamed from: h */
    public String f9216h;

    /* renamed from: i */
    public int f9217i;

    /* renamed from: a */
    public static ExResInfo m14890a(String str) {
        ExResInfo cVar = new ExResInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            cVar.f9209a = jSONObject.optString("resId", "");
            cVar.f9210b = jSONObject.optString("type", "");
            cVar.f9211c = jSONObject.optString("url", "");
            cVar.f9212d = jSONObject.optString("link", "");
            cVar.f9213e = jSONObject.optInt("w", 0);
            cVar.f9214f = jSONObject.optInt("h", 0);
            cVar.f9215g = jSONObject.optInt("duration", 0);
            cVar.f9216h = jSONObject.optString("thumbUrl", "");
            cVar.f9217i = jSONObject.optInt("isAuto", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cVar;
    }

    /* renamed from: c */
    public static String m14894c(Context context) {
        File externalFilesDir = context.getExternalFilesDir("HvGadExRes");
        if (externalFilesDir == null) {
            return null;
        }
        return externalFilesDir.getAbsolutePath();
    }

    /* renamed from: b */
    public String mo24990b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("resId", this.f9209a);
            jSONObject.put("type", this.f9210b);
            jSONObject.put("url", this.f9211c);
            jSONObject.put("link", this.f9212d);
            jSONObject.put("w", this.f9213e);
            jSONObject.put("h", this.f9214f);
            jSONObject.put("duration", this.f9215g);
            jSONObject.put("thumbUrl", this.f9216h);
            jSONObject.put("isAuto", this.f9217i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    /* renamed from: b */
    public static void m14893b(Context context) {
        List<String> o = new ExRes().mo24232o();
        String c = m14894c(context);
        if (new File(c).isDirectory()) {
            for (File file : FileUtils.m1098c(c)) {
                if (file.isDirectory() && !o.contains(file.getName())) {
                    file.deleteOnExit();
                }
            }
        }
    }

    /* renamed from: a */
    public String[] mo24989a() {
        return this.f9211c.split(",");
    }

    /* renamed from: a */
    public static String m14891a(Context context, String str) {
        String c = m14894c(context);
        if (TextUtils.isEmpty(c)) {
            return null;
        }
        return c + File.separator + str;
    }

    /* renamed from: a */
    public static String m14892a(Context context, String str, String str2) {
        String a = m14891a(context, str);
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        return a + File.separator + str2.substring(str2.lastIndexOf("/") + 1);
    }

    /* renamed from: a */
    public boolean mo24988a(Context context) {
        String[] a = mo24989a();
        for (String str : a) {
            if (URLUtil.isNetworkUrl(str)) {
                String a2 = m14892a(context, this.f9209a, str);
                if (TextUtils.isEmpty(a2) || !new File(a2).isFile()) {
                    return false;
                }
            }
        }
        if (!URLUtil.isNetworkUrl(this.f9216h)) {
            return true;
        }
        String a3 = m14892a(context, this.f9209a, this.f9216h);
        if (TextUtils.isEmpty(a3) || !new File(a3).isFile()) {
            return false;
        }
        return true;
    }
}
