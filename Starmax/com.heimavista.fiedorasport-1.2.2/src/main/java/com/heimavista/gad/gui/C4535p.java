package com.heimavista.gad.gui;

import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.gad.gui.p */
/* compiled from: ExAdFragment */
class C4535p implements OnResultListener<Integer> {

    /* renamed from: a */
    final /* synthetic */ ExAdFragment f9158a;

    C4535p(ExAdFragment exAdFragment) {
        this.f9158a = exAdFragment;
    }

    /* renamed from: a */
    public void mo22380a(Integer num) {
        int unused = this.f9158a.f9116b0 = num.intValue();
        this.f9158a.m14814h();
        this.f9158a.f9110V.setEnabled(true);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f9158a.m14812g();
        this.f9158a.m14818j();
        this.f9158a.f9110V.setEnabled(true);
    }
}
