package com.heimavista.gad.gui;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.gad.R$drawable;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$string;
import com.heimavista.gad.api.gad.HvApiAddLike;
import com.heimavista.gad.api.gad.HvApiDelLike;
import com.heimavista.gad.api.gad.HvApiGetLikeCnt;
import com.heimavista.gad.info.AdInfo;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.info.ResInfo;
import com.heimavista.gad.p202u.Like;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p217f.GAManage;
import p119e.p189e.p219h.HttpParams;

public abstract class ResBaseFragment extends Fragment {

    /* renamed from: Y */
    public static final String f9128Y = (ResBaseFragment.class.getName() + ".AD.CLICK");

    /* renamed from: P */
    protected String f9129P;

    /* renamed from: Q */
    protected AdInfo f9130Q;

    /* renamed from: R */
    protected HvGadViewConfig f9131R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public ImageView f9132S;

    /* renamed from: T */
    private TextView f9133T;

    /* renamed from: U */
    private boolean f9134U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public boolean f9135V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public boolean f9136W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public int f9137X;

    /* renamed from: com.heimavista.gad.gui.ResBaseFragment$a */
    class C4519a implements OnResultListener<Integer> {
        C4519a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ResBaseFragment, int]
         candidates:
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, int):int
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.info.AdInfo, android.view.View):void
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22380a(Integer num) {
            boolean unused = ResBaseFragment.this.f9136W = false;
            boolean unused2 = ResBaseFragment.this.f9135V = true;
            int unused3 = ResBaseFragment.this.f9137X = num.intValue();
            ResBaseFragment.this.m14840d();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ResBaseFragment, int]
         candidates:
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, int):int
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.info.AdInfo, android.view.View):void
          com.heimavista.gad.gui.ResBaseFragment.a(com.heimavista.gad.gui.ResBaseFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            boolean unused = ResBaseFragment.this.f9136W = false;
            boolean unused2 = ResBaseFragment.this.f9135V = false;
            int unused3 = ResBaseFragment.this.f9137X = 0;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m14842e() {
        if (!this.f9136W) {
            this.f9136W = true;
            new HvApiGetLikeCnt().request(this.f9130Q, new C4519a());
        }
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f9129P = arguments.getString("pos", "");
            this.f9130Q = (AdInfo) arguments.getParcelable(AdInfo.class.getCanonicalName());
            this.f9131R = (HvGadViewConfig) arguments.getParcelable(HvGadViewConfig.class.getCanonicalName());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m14838c() {
        Like fVar = new Like();
        fVar.mo25133h(this.f9130Q.f9171R);
        fVar.mo25135j(this.f9130Q.f9176W.f9185P);
        this.f9134U = fVar.mo24202e();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m14840d() {
        int i = 8;
        if (this.f9134U) {
            this.f9132S.setImageResource(R$drawable.hv_gad_like_selected);
            TextView textView = this.f9133T;
            if (this.f9135V && this.f9137X > 0) {
                i = 0;
            }
            textView.setVisibility(i);
            if (!this.f9135V) {
                m14842e();
            }
        } else {
            this.f9132S.setImageResource(R$drawable.hv_gad_like_unselected);
            this.f9133T.setVisibility(8);
        }
        this.f9133T.setText(String.valueOf(this.f9137X));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public int[] mo24951b(View view) {
        return mo24949a(view, 0, 0);
    }

    /* renamed from: a */
    public static Bundle m14833a(String str, HvGadViewConfig hvGadViewConfig, AdInfo adInfo) {
        Bundle bundle = new Bundle();
        bundle.putString("pos", str);
        bundle.putParcelable(AdInfo.class.getCanonicalName(), adInfo);
        bundle.putParcelable(HvGadViewConfig.class.getCanonicalName(), hvGadViewConfig);
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24950b() {
        ResInfo resInfo;
        AdInfo adInfo = this.f9130Q;
        if (adInfo != null && (resInfo = adInfo.f9176W) != null && URLUtil.isNetworkUrl(resInfo.f9188S)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(AdInfo.class.getCanonicalName(), this.f9130Q);
            HvApp.m13010c().mo24158a(f9128Y, bundle);
            HvGadLogManage d = HvGadLogManage.m14968d();
            LogInfo dVar = new LogInfo();
            dVar.mo24992a(this.f9130Q);
            d.mo25031a(dVar);
            GAManage a = GAManage.m17122a();
            String string = getString(R$string.hv_gad_ga_cg_click);
            AdInfo adInfo2 = this.f9130Q;
            a.mo27171a(string, adInfo2.f9170Q, adInfo2.f9172S, null);
            startActivity(GadWebActivity.m14827a(getContext(), this.f9130Q.f9176W.f9188S));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int[] mo24949a(View view, int i, int i2) {
        int b = this.f9131R.mo24890b();
        int a = this.f9131R.mo24889a();
        if (b <= 0 || a <= 0) {
            if (b <= 0 && a <= 0) {
                b = ScreenUtils.m1263a();
            }
            AdInfo adInfo = this.f9130Q;
            int i3 = adInfo != null ? adInfo.f9176W.f9189T : 0;
            AdInfo adInfo2 = this.f9130Q;
            int i4 = adInfo2 != null ? adInfo2.f9176W.f9190U : 0;
            if (b > 0) {
                if (i3 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i > 0) {
                    a = (i2 * b) / i;
                }
            } else {
                if (i4 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i2 > 0) {
                    b = (i * a) / i2;
                }
            }
        }
        if (b > 0 && a <= 0) {
            a = (b * 9) / 16;
        } else if (b <= 0 && a > 0) {
            b = (a * 16) / 9;
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = b;
            layoutParams.height = a;
        }
        return new int[]{b, a};
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24947a(View view) {
        AdInfo adInfo = this.f9130Q;
        m14838c();
        this.f9132S = (ImageView) view.findViewById(R$id.iv_like);
        this.f9133T = (TextView) view.findViewById(R$id.tv_like);
        this.f9132S.setOnClickListener(new C4527h(this, adInfo));
        if (this.f9134U) {
            m14842e();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24948a(AdInfo adInfo, View view) {
        this.f9132S.setEnabled(false);
        this.f9134U = !this.f9134U;
        if (this.f9134U) {
            this.f9137X++;
            new HvApiAddLike().request(adInfo, new C4536q(this));
        } else {
            this.f9137X--;
            new HvApiDelLike().request(adInfo, new C4537r(this));
        }
        m14840d();
    }
}
