package com.heimavista.gad;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;

/* renamed from: com.heimavista.gad.n */
public class HvGadConfig {

    /* renamed from: a */
    public String f9253a;

    /* renamed from: b */
    public String f9254b;

    /* renamed from: c */
    public String f9255c;

    /* renamed from: d */
    public String f9256d;

    public HvGadConfig(String str, String str2, String str3, String str4) {
        this.f9253a = str;
        this.f9254b = str2;
        this.f9255c = str3;
        this.f9256d = str4;
    }

    /* renamed from: a */
    public boolean mo25022a() {
        LogUtils.m1123a(this.f9253a, this.f9255c, this.f9256d);
        if (TextUtils.isEmpty(this.f9253a) || TextUtils.isEmpty(this.f9255c) || TextUtils.isEmpty(this.f9256d)) {
            return false;
        }
        return true;
    }
}
