package com.heimavista.gad.gui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadDownloadManage;
import com.heimavista.gad.HvGadLogManage;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.gad.R$drawable;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;
import com.heimavista.gad.R$string;
import com.heimavista.gad.api.gad.HvApiAddLike;
import com.heimavista.gad.api.gad.HvApiDelLike;
import com.heimavista.gad.api.gad.HvApiGetLikeCnt;
import com.heimavista.gad.info.ExAdInfo;
import com.heimavista.gad.info.ExResInfo;
import com.heimavista.gad.info.LogInfo;
import com.heimavista.gad.p202u.ExAd;
import com.heimavista.gad.p202u.ExRes;
import com.heimavista.gad.p202u.Like;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.utils.RandomUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import p019cn.jzvd.JZDataSource;
import p019cn.jzvd.Jzvd;
import p019cn.jzvd.media.JZMediaExo;
import p119e.p189e.p217f.GAManage;
import p119e.p189e.p219h.HttpParams;

public class ExAdFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public String f9104P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public ExAdInfo f9105Q;

    /* renamed from: R */
    private HvGadViewConfig f9106R;

    /* renamed from: S */
    private LocalBroadcastManager f9107S;

    /* renamed from: T */
    private BroadcastReceiver f9108T;

    /* renamed from: U */
    private FrameLayout f9109U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public ImageView f9110V;

    /* renamed from: W */
    private TextView f9111W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public JzvdStdGad f9112X;

    /* renamed from: Y */
    private boolean f9113Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public boolean f9114Z;
    /* access modifiers changed from: private */

    /* renamed from: a0 */
    public boolean f9115a0;
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public int f9116b0;

    /* renamed from: com.heimavista.gad.gui.ExAdFragment$a */
    class C4513a extends BroadcastReceiver {
        C4513a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (HvGadDownloadManage.f9258h.equals(intent.getAction())) {
                if (ExAdFragment.this.f9105Q == null && Arrays.asList(intent.getStringExtra("pos").split(",")).contains(ExAdFragment.this.f9104P)) {
                    ExAdFragment.this.m14811f();
                    if (ExAdFragment.this.f9105Q != null && "video".equals(ExAdFragment.this.f9105Q.f9208i.f9210b) && ExAdFragment.this.f9112X != null) {
                        ExAdFragment.this.f9112X.startVideo();
                    }
                }
            } else if (HvGad.f9227h.equals(intent.getAction())) {
                ExAdFragment.this.m14820k();
            }
        }
    }

    /* renamed from: com.heimavista.gad.gui.ExAdFragment$b */
    class C4514b implements OnResultListener<Integer> {
        C4514b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ExAdFragment, int]
         candidates:
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, int):int
          com.heimavista.gad.gui.ExAdFragment.a(java.lang.String, com.heimavista.gad.HvGadViewConfig):android.os.Bundle
          com.heimavista.gad.gui.ExAdFragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.info.b, android.view.View):void
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.gui.ExAdFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ExAdFragment, int]
         candidates:
          com.heimavista.gad.gui.ExAdFragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
          com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.info.b, android.view.View):void
          com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.gui.ExAdFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22380a(Integer num) {
            boolean unused = ExAdFragment.this.f9115a0 = false;
            boolean unused2 = ExAdFragment.this.f9114Z = true;
            int unused3 = ExAdFragment.this.f9116b0 = num.intValue();
            ExAdFragment.this.m14814h();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ExAdFragment, int]
         candidates:
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, int):int
          com.heimavista.gad.gui.ExAdFragment.a(java.lang.String, com.heimavista.gad.HvGadViewConfig):android.os.Bundle
          com.heimavista.gad.gui.ExAdFragment.a(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.info.b, android.view.View):void
          com.heimavista.gad.gui.ExAdFragment.a(com.heimavista.gad.gui.ExAdFragment, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.gui.ExAdFragment, boolean):boolean
         arg types: [com.heimavista.gad.gui.ExAdFragment, int]
         candidates:
          com.heimavista.gad.gui.ExAdFragment.b(android.view.LayoutInflater, android.view.ViewGroup):android.view.View
          com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.info.b, android.view.View):void
          com.heimavista.gad.gui.ExAdFragment.b(com.heimavista.gad.gui.ExAdFragment, boolean):boolean */
        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            boolean unused = ExAdFragment.this.f9115a0 = false;
            boolean unused2 = ExAdFragment.this.f9114Z = false;
            int unused3 = ExAdFragment.this.f9116b0 = 0;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m14818j() {
        if (!this.f9115a0) {
            this.f9115a0 = true;
            new HvApiGetLikeCnt().request(this.f9105Q, new C4514b());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m14820k() {
        this.f9105Q = null;
        Jzvd.releaseAllVideos();
        this.f9112X = null;
        this.f9115a0 = false;
        this.f9113Y = false;
        this.f9116b0 = 0;
        this.f9111W = null;
        this.f9110V = null;
        m14811f();
    }

    /* renamed from: l */
    private void m14821l() {
        LocalBroadcastManager localBroadcastManager = this.f9107S;
        if (localBroadcastManager != null) {
            localBroadcastManager.unregisterReceiver(this.f9108T);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9107S = LocalBroadcastManager.getInstance(context);
        this.f9108T = new C4513a();
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f9104P = arguments.getString("pos", "");
            this.f9106R = (HvGadViewConfig) arguments.getParcelable(HvGadViewConfig.class.getCanonicalName());
        }
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        this.f9109U = new FrameLayout(layoutInflater.getContext());
        return this.f9109U;
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        if (isVisible()) {
            m14806d();
        } else {
            m14804c();
        }
    }

    public void onPause() {
        super.onPause();
        m14804c();
    }

    public void onResume() {
        super.onResume();
        m14806d();
        this.f9109U.setFocusableInTouchMode(true);
        this.f9109U.setFocusable(true);
        this.f9109U.requestFocus();
        this.f9109U.setOnKeyListener(C4524e.f9145P);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: c */
    private View m14803c(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_video, viewGroup, false);
        String[] a = this.f9105Q.f9208i.mo24989a();
        String a2 = ExResInfo.m14892a(getContext(), this.f9105Q.f9208i.f9209a, a[RandomUtils.m15262a(a.length - 1)]);
        Context context = getContext();
        ExResInfo cVar = this.f9105Q.f9208i;
        String a3 = ExResInfo.m14892a(context, cVar.f9209a, cVar.f9216h);
        int[] b = m14802b(inflate);
        JZDataSource jZDataSource = new JZDataSource(a2);
        jZDataSource.looping = true;
        jZDataSource.mute = true;
        this.f9112X = (JzvdStdGad) inflate.findViewById(R$id.videoplayer);
        ViewGroup.LayoutParams layoutParams = this.f9112X.getLayoutParams();
        layoutParams.width = b[0];
        layoutParams.height = b[1];
        ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) this.f9112X.getParent()).getLayoutParams();
        layoutParams2.width = b[0];
        layoutParams2.height = b[1];
        this.f9112X.setUp(jZDataSource, 0, JZMediaExo.class);
        Jzvd.setVideoImageDisplayType(2);
        this.f9112X.thumbImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.f9112X.setOnClickUiToggleListener(new C4523d(this));
        this.f9112X.thumbImageView.setImageURI(Uri.fromFile(new File(a3)));
        return inflate;
    }

    /* renamed from: d */
    private void m14806d() {
        JzvdStdGad jzvdStdGad;
        HvGadDownloadManage.m14960g().mo25027b();
        m14817i();
        ExAdInfo bVar = this.f9105Q;
        if (bVar == null || bVar.f9208i == null) {
            if (this.f9109U.getChildCount() == 0) {
                m14811f();
                ExAdInfo bVar2 = this.f9105Q;
                if (bVar2 == null || bVar2.f9208i == null) {
                    return;
                }
            } else if (m14808e() != null) {
                m14811f();
            } else {
                return;
            }
        }
        if (this.f9105Q != null && !HvGad.m14915i().mo25000a().equals(this.f9105Q.f9200a)) {
            m14820k();
        }
        ExAdInfo bVar3 = this.f9105Q;
        if (bVar3 != null && "video".equals(bVar3.f9208i.f9210b) && (jzvdStdGad = this.f9112X) != null) {
            jzvdStdGad.startVideo();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.gad.u.d.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.d>
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.heimavista.gad.u.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.heimavista.gad.u.d.a(java.lang.String, java.lang.String, java.lang.Boolean):java.util.List<com.heimavista.gad.u.d> */
    /* renamed from: e */
    private ExAdInfo m14808e() {
        List<ExAd> a = new ExAd().mo25100a(HvGad.m14915i().mo25000a(), this.f9104P, (Boolean) false);
        if (a.size() == 0) {
            a = new ExAd().mo25100a(HvGad.m14915i().mo25000a(), this.f9104P, (Boolean) true);
        }
        LogUtils.m1123a(a);
        ArrayList arrayList = new ArrayList();
        for (ExAd dVar : a) {
            LogUtils.m1123a(dVar.mo25116q());
            ExAdInfo q = dVar.mo25116q();
            ExResInfo cVar = q.f9208i;
            if (!cVar.mo24988a(getContext())) {
                ExRes eVar = new ExRes();
                eVar.mo25129h(cVar.f9209a);
                eVar.mo25127a(false);
                eVar.mo24208m();
                HvGadDownloadManage.m14960g().mo25028c();
            } else {
                arrayList.add(q);
            }
        }
        if (arrayList.size() > 0) {
            return (ExAdInfo) arrayList.get((int) (System.currentTimeMillis() % ((long) arrayList.size())));
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040  */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m14811f() {
        /*
            r5 = this;
            com.heimavista.gad.info.b r0 = r5.m14808e()
            android.content.Context r1 = r5.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r1)
            r2 = 0
            if (r0 == 0) goto L_0x003d
            r5.f9105Q = r0
            com.heimavista.gad.info.c r3 = r0.f9208i
            java.lang.String r3 = r3.f9210b
            java.lang.String r4 = "image"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0027
            android.widget.FrameLayout r3 = r5.f9109U
            android.view.View r3 = r5.m14797b(r1, r3)
            r5.m14800b(r0, r3)
            goto L_0x003e
        L_0x0027:
            com.heimavista.gad.info.c r3 = r0.f9208i
            java.lang.String r3 = r3.f9210b
            java.lang.String r4 = "video"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x003d
            android.widget.FrameLayout r3 = r5.f9109U
            android.view.View r3 = r5.m14803c(r1, r3)
            r5.m14800b(r0, r3)
            goto L_0x003e
        L_0x003d:
            r3 = r2
        L_0x003e:
            if (r3 != 0) goto L_0x0047
            android.widget.FrameLayout r0 = r5.f9109U
            android.view.View r3 = r5.m14792a(r1, r0)
            r0 = r2
        L_0x0047:
            r5.f9105Q = r0
            android.widget.FrameLayout r0 = r5.f9109U
            r0.removeAllViews()
            android.widget.FrameLayout r0 = r5.f9109U
            r0.addView(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.gad.gui.ExAdFragment.m14811f():void");
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m14812g() {
        Like fVar = new Like();
        fVar.mo25133h(this.f9105Q.f9203d);
        fVar.mo25135j(this.f9105Q.f9208i.f9209a);
        this.f9113Y = fVar.mo24202e();
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m14814h() {
        int i = 8;
        if (this.f9113Y) {
            this.f9110V.setImageResource(R$drawable.hv_gad_like_selected);
            TextView textView = this.f9111W;
            if (this.f9114Z && this.f9116b0 > 0) {
                i = 0;
            }
            textView.setVisibility(i);
            if (!this.f9114Z) {
                m14818j();
            }
        } else {
            this.f9110V.setImageResource(R$drawable.hv_gad_like_unselected);
            this.f9111W.setVisibility(8);
        }
        this.f9111W.setText(String.valueOf(this.f9116b0));
    }

    /* renamed from: i */
    private void m14817i() {
        if (this.f9107S != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(HvGadDownloadManage.f9258h);
            intentFilter.addAction(HvGad.f9227h);
            this.f9107S.registerReceiver(this.f9108T, intentFilter);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: b */
    private View m14797b(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_image, viewGroup, false);
        m14802b(inflate);
        String[] a = this.f9105Q.f9208i.mo24989a();
        ((ImageView) inflate.findViewById(R$id.image)).setImageURI(Uri.fromFile(new File(ExResInfo.m14892a(getContext(), this.f9105Q.f9208i.f9209a, a[(int) (System.currentTimeMillis() % ((long) a.length))]))));
        inflate.setOnClickListener(new C4526g(this));
        return inflate;
    }

    /* renamed from: a */
    public static Bundle m14791a(String str, HvGadViewConfig hvGadViewConfig) {
        Bundle bundle = new Bundle();
        bundle.putString("pos", str);
        bundle.putParcelable(HvGadViewConfig.class.getCanonicalName(), hvGadViewConfig);
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    private View m14792a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R$layout.hv_gad_default, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(R$id.image);
        int a = HvGad.m14915i().mo25014d().mo22826a(this.f9104P);
        if (a > 0) {
            imageView.setImageResource(a);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), a, options);
            m14796a(inflate, options.outWidth, options.outHeight);
        } else {
            inflate.getLayoutParams().width = 0;
            inflate.getLayoutParams().height = 0;
        }
        return inflate;
    }

    /* renamed from: b */
    private int[] m14802b(View view) {
        return m14796a(view, 0, 0);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14799b() {
        if (URLUtil.isNetworkUrl(this.f9105Q.f9208i.f9212d)) {
            HvGadLogManage d = HvGadLogManage.m14968d();
            LogInfo dVar = new LogInfo();
            dVar.mo24994a(this.f9105Q);
            d.mo25031a(dVar);
            GAManage a = GAManage.m17122a();
            String string = getString(R$string.hv_gad_ga_cg_click);
            ExAdInfo bVar = this.f9105Q;
            a.mo27171a(string, bVar.f9202c, bVar.f9204e, null);
            startActivity(GadWebActivity.m14827a(getContext(), this.f9105Q.f9208i.f9212d));
        }
    }

    /* renamed from: b */
    private void m14800b(ExAdInfo bVar, View view) {
        m14812g();
        this.f9110V = (ImageView) view.findViewById(R$id.iv_like);
        this.f9111W = (TextView) view.findViewById(R$id.tv_like);
        this.f9110V.setOnClickListener(new C4525f(this, bVar));
        if (this.f9113Y) {
            m14818j();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24930a(View view) {
        m14799b();
    }

    /* renamed from: a */
    private int[] m14796a(View view, int i, int i2) {
        int b = this.f9106R.mo24890b();
        int a = this.f9106R.mo24889a();
        if (b <= 0 || a <= 0) {
            if (b <= 0 && a <= 0) {
                b = ScreenUtils.m1263a();
            }
            ExAdInfo bVar = this.f9105Q;
            int i3 = bVar != null ? bVar.f9208i.f9213e : 0;
            ExAdInfo bVar2 = this.f9105Q;
            int i4 = bVar2 != null ? bVar2.f9208i.f9214f : 0;
            if (b > 0) {
                if (i3 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i > 0) {
                    a = (i2 * b) / i;
                }
            } else {
                if (i4 > 0) {
                    i = i3;
                    i2 = i4;
                }
                if (i2 > 0) {
                    b = (i * a) / i2;
                }
            }
        }
        if (b > 0 && a <= 0) {
            a = (b * 9) / 16;
        } else if (b <= 0 && a > 0) {
            b = (a * 16) / 9;
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = b;
            layoutParams.height = a;
        }
        return new int[]{b, a};
    }

    /* renamed from: c */
    private void m14804c() {
        ExResInfo cVar;
        m14821l();
        ExAdInfo bVar = this.f9105Q;
        if (bVar != null && (cVar = bVar.f9208i) != null) {
            if ("video".equals(cVar.f9210b)) {
                Jzvd.releaseAllVideos();
                JzvdStdGad jzvdStdGad = this.f9112X;
                if (jzvdStdGad != null) {
                    long logDuration = jzvdStdGad.getLogDuration();
                    int i = this.f9105Q.f9208i.f9215g;
                    if (logDuration > ((long) i)) {
                        logDuration = (long) i;
                    }
                    HvGadLogManage d = HvGadLogManage.m14968d();
                    LogInfo dVar = new LogInfo();
                    dVar.mo24994a(this.f9105Q);
                    dVar.mo24991a(logDuration);
                    d.mo25033b(dVar);
                    GAManage a = GAManage.m17122a();
                    String string = getString(R$string.hv_gad_ga_cg_view);
                    ExAdInfo bVar2 = this.f9105Q;
                    a.mo27171a(string, bVar2.f9202c, bVar2.f9204e, Long.valueOf(logDuration));
                    return;
                }
                return;
            }
            HvGadLogManage d2 = HvGadLogManage.m14968d();
            LogInfo dVar2 = new LogInfo();
            dVar2.mo24994a(this.f9105Q);
            d2.mo25033b(dVar2);
        }
    }

    /* renamed from: a */
    static /* synthetic */ boolean m14794a(View view, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 1) {
            return Jzvd.backPress();
        }
        return false;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24931a(ExAdInfo bVar, View view) {
        this.f9110V.setEnabled(false);
        this.f9113Y = !this.f9113Y;
        if (this.f9113Y) {
            this.f9116b0++;
            new HvApiAddLike().request(bVar, new C4534o(this));
        } else {
            this.f9116b0--;
            new HvApiDelLike().request(bVar, new C4535p(this));
        }
        m14814h();
    }
}
