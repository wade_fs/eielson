package com.heimavista.gad.gui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import androidx.annotation.Nullable;
import com.heimavista.gad.R$drawable;
import com.heimavista.gad.R$id;
import com.heimavista.gad.R$layout;

public class GadWebActivity extends Activity {

    /* renamed from: P */
    private WebView f9119P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public ProgressBar f9120Q;

    /* renamed from: com.heimavista.gad.gui.GadWebActivity$a */
    class C4515a extends WebChromeClient {
        C4515a() {
        }

        public void onProgressChanged(WebView webView, int i) {
            GadWebActivity.this.f9120Q.setProgress(i);
            super.onProgressChanged(webView, i);
        }

        public void onReceivedTitle(WebView webView, String str) {
            super.onReceivedTitle(webView, str);
            if (!TextUtils.isEmpty(str) && !URLUtil.isNetworkUrl(str) && GadWebActivity.this.getActionBar() != null) {
                GadWebActivity.this.getActionBar().setTitle(str);
            }
        }
    }

    /* renamed from: com.heimavista.gad.gui.GadWebActivity$b */
    class C4516b extends WebViewClient {
        C4516b() {
        }

        public void onPageFinished(WebView webView, String str) {
            GadWebActivity.this.f9120Q.setVisibility(8);
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            GadWebActivity.this.f9120Q.setVisibility(0);
            super.onPageStarted(webView, str, bitmap);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!str.startsWith("scheme:")) {
                return false;
            }
            GadWebActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return false;
        }
    }

    public void onBackPressed() {
        if (this.f9119P.canGoBack()) {
            this.f9119P.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.hv_gad_web);
        if (getActionBar() != null) {
            getActionBar().setTitle("");
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayUseLogoEnabled(false);
            getActionBar().setHomeAsUpIndicator(R$drawable.hv_gad_close);
        }
        this.f9120Q = (ProgressBar) findViewById(R$id.progressBar);
        this.f9119P = (WebView) findViewById(R$id.webview);
        m14829a();
        this.f9119P.loadUrl(getIntent().getStringExtra("url"));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    /* renamed from: a */
    public static Intent m14827a(Context context, String str) {
        Intent intent = new Intent(context, GadWebActivity.class);
        intent.putExtra("url", str);
        return intent;
    }

    /* renamed from: a */
    private void m14829a() {
        WebSettings settings = this.f9119P.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setCacheMode(-1);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setGeolocationEnabled(true);
        settings.setBuiltInZoomControls(false);
        this.f9119P.setHorizontalScrollBarEnabled(false);
        this.f9119P.setVerticalScrollbarOverlay(true);
        this.f9119P.setScrollBarStyle(33554432);
        this.f9119P.setWebChromeClient(new C4515a());
        this.f9119P.setWebViewClient(new C4516b());
    }
}
