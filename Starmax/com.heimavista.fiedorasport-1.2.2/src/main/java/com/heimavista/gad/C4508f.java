package com.heimavista.gad;

import android.content.Context;

/* renamed from: com.heimavista.gad.f */
/* compiled from: lambda */
public final /* synthetic */ class C4508f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ HvGad f9070P;

    /* renamed from: Q */
    private final /* synthetic */ Context f9071Q;

    /* renamed from: R */
    private final /* synthetic */ OnGadStartListener f9072R;

    /* renamed from: S */
    private final /* synthetic */ boolean f9073S;

    public /* synthetic */ C4508f(HvGad lVar, Context context, OnGadStartListener tVar, boolean z) {
        this.f9070P = lVar;
        this.f9071Q = context;
        this.f9072R = tVar;
        this.f9073S = z;
    }

    public final void run() {
        this.f9070P.mo25003a(this.f9071Q, this.f9072R, this.f9073S);
    }
}
