package com.heimavista.gad;

import android.content.DialogInterface;

/* renamed from: com.heimavista.gad.e */
/* compiled from: lambda */
public final /* synthetic */ class C4507e implements DialogInterface.OnCancelListener {

    /* renamed from: P */
    private final /* synthetic */ OnGadStartListener f9069P;

    public /* synthetic */ C4507e(OnGadStartListener tVar) {
        this.f9069P = tVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        HvGad.m14910a(this.f9069P, dialogInterface);
    }
}
