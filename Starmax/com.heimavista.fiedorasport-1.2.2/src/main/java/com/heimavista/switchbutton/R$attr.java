package com.heimavista.switchbutton;

public final class R$attr {
    public static final int sb_background = 2130969319;
    public static final int sb_border_width = 2130969320;
    public static final int sb_button_color = 2130969321;
    public static final int sb_checked = 2130969322;
    public static final int sb_checked_color = 2130969323;
    public static final int sb_checkline_color = 2130969324;
    public static final int sb_checkline_width = 2130969325;
    public static final int sb_effect_duration = 2130969326;
    public static final int sb_enable_effect = 2130969327;
    public static final int sb_shadow_color = 2130969328;
    public static final int sb_shadow_effect = 2130969329;
    public static final int sb_shadow_offset = 2130969330;
    public static final int sb_shadow_radius = 2130969331;
    public static final int sb_show_indicator = 2130969332;
    public static final int sb_uncheck_color = 2130969333;
    public static final int sb_uncheckcircle_color = 2130969334;
    public static final int sb_uncheckcircle_radius = 2130969335;
    public static final int sb_uncheckcircle_width = 2130969336;

    private R$attr() {
    }
}
