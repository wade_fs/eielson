package com.heimavista.switchbutton;

public final class R$styleable {
    public static final int[] SwitchButton = {2130969319, 2130969320, 2130969321, 2130969322, 2130969323, 2130969324, 2130969325, 2130969326, 2130969327, 2130969328, 2130969329, 2130969330, 2130969331, 2130969332, 2130969333, 2130969334, 2130969335, 2130969336};
    public static final int SwitchButton_sb_background = 0;
    public static final int SwitchButton_sb_border_width = 1;
    public static final int SwitchButton_sb_button_color = 2;
    public static final int SwitchButton_sb_checked = 3;
    public static final int SwitchButton_sb_checked_color = 4;
    public static final int SwitchButton_sb_checkline_color = 5;
    public static final int SwitchButton_sb_checkline_width = 6;
    public static final int SwitchButton_sb_effect_duration = 7;
    public static final int SwitchButton_sb_enable_effect = 8;
    public static final int SwitchButton_sb_shadow_color = 9;
    public static final int SwitchButton_sb_shadow_effect = 10;
    public static final int SwitchButton_sb_shadow_offset = 11;
    public static final int SwitchButton_sb_shadow_radius = 12;
    public static final int SwitchButton_sb_show_indicator = 13;
    public static final int SwitchButton_sb_uncheck_color = 14;
    public static final int SwitchButton_sb_uncheckcircle_color = 15;
    public static final int SwitchButton_sb_uncheckcircle_radius = 16;
    public static final int SwitchButton_sb_uncheckcircle_width = 17;

    private R$styleable() {
    }
}
