package com.heimavista.apn;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.heimavista.utils.ParamJsonData;

public class PushMsg implements Parcelable {
    public static final Parcelable.Creator<PushMsg> CREATOR = new C3662a();

    /* renamed from: P */
    public String f6226P;

    /* renamed from: Q */
    public String f6227Q;

    /* renamed from: R */
    public String f6228R;

    /* renamed from: S */
    public String f6229S;

    /* renamed from: T */
    public String f6230T;

    /* renamed from: U */
    public ParamJsonData f6231U;

    /* renamed from: com.heimavista.apn.PushMsg$a */
    static class C3662a implements Parcelable.Creator<PushMsg> {
        C3662a() {
        }

        public PushMsg createFromParcel(Parcel parcel) {
            return new PushMsg(parcel);
        }

        public PushMsg[] newArray(int i) {
            return new PushMsg[i];
        }
    }

    public PushMsg() {
    }

    public int describeContents() {
        return 0;
    }

    @NonNull
    public String toString() {
        return "{category=" + this.f6226P + ",title=" + this.f6227Q + ",body=" + this.f6228R + ",icon=" + this.f6229S + ",sound=" + this.f6230T + ",custom=" + this.f6231U.mo25332b() + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f6226P);
        parcel.writeString(this.f6227Q);
        parcel.writeString(this.f6228R);
        parcel.writeString(this.f6229S);
        parcel.writeString(this.f6230T);
        parcel.writeString(this.f6231U.mo25332b());
    }

    protected PushMsg(Parcel parcel) {
        this.f6226P = parcel.readString();
        this.f6227Q = parcel.readString();
        this.f6228R = parcel.readString();
        this.f6229S = parcel.readString();
        this.f6230T = parcel.readString();
        this.f6231U = new ParamJsonData(parcel.readString(), true);
    }
}
