package com.heimavista.apn;

import com.blankj.utilcode.util.LogUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.heimavista.utils.ParamJsonData;
import java.util.Map;

public class HvFcmService extends FirebaseMessagingService {
    /* renamed from: a */
    public void mo22312a(RemoteMessage remoteMessage) {
        super.mo22312a(remoteMessage);
        Map<String, String> c = remoteMessage.mo22318c();
        LogUtils.m1139c("HvFcm", "onMessageReceived", remoteMessage.mo22318c().toString());
        PushMsg pushMsg = new PushMsg();
        pushMsg.f6226P = c.get("category");
        pushMsg.f6227Q = c.get("title");
        pushMsg.f6228R = c.get(TtmlNode.TAG_BODY);
        pushMsg.f6229S = c.get("icon");
        pushMsg.f6230T = c.get("sound");
        pushMsg.f6231U = new ParamJsonData(c.get("custom"), true);
        ApnDelegate.m10041a().mo22543a(this, pushMsg);
    }
}
