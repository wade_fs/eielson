package com.heimavista.apn;

import android.content.Context;
import android.text.TextUtils;
import cn.jpush.android.api.JPushInterface;
import com.blankj.utilcode.util.LogUtils;
import com.google.android.gms.common.C2167b;
import com.google.firebase.iid.C3560a;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.heimavista.api.start.HvApiRegister;
import com.heimavista.utils.ClassUtil;
import java.util.HashMap;
import java.util.Map;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.apn.d */
public class ApnManage {

    /* renamed from: a */
    private Map<String, PushHandler> f6240a;

    /* renamed from: com.heimavista.apn.d$a */
    /* compiled from: ApnManage */
    class C3667a extends HashMap<String, PushHandler> {
        C3667a(ApnManage dVar) {
            put("default", new PushHandlerDefault());
        }
    }

    /* renamed from: com.heimavista.apn.d$b */
    /* compiled from: ApnManage */
    private static final class C3668b {

        /* renamed from: a */
        static ApnManage f6241a = new ApnManage(null);
    }

    /* synthetic */ ApnManage(C3667a aVar) {
        this();
    }

    /* renamed from: a */
    public static ApnManage m10050a() {
        return C3668b.f6241a;
    }

    /* renamed from: b */
    private static boolean m10053b() {
        return ClassUtil.m15236a("com.google.firebase.messaging.FirebaseMessagingService");
    }

    /* renamed from: c */
    private static boolean m10055c() {
        return ClassUtil.m15236a("cn.jpush.android.api.JPushInterface");
    }

    private ApnManage() {
        this.f6240a = new C3667a(this);
        this.f6240a.put("common", new PushHandlerDefault());
    }

    /* renamed from: b */
    private static boolean m10054b(Context context) {
        if (C2167b.m5251a().mo16831c(context) != 0 || !m10053b()) {
            return false;
        }
        FirebaseMessaging.m9904a().mo22307a(true);
        C4065h<C3560a> b = FirebaseInstanceId.m9728j().mo22203b();
        b.mo23697a(new C3664b(context));
        b.mo23696a(new C3663a(context));
        return true;
    }

    /* renamed from: c */
    private static boolean m10056c(Context context) {
        if (!m10055c()) {
            return false;
        }
        JPushInterface.setDebugMode(HvApp.m13010c().mo24159b());
        JPushInterface.init(context);
        String registrationID = JPushInterface.getRegistrationID(context);
        LogUtils.m1139c("startJPush", "registerId", registrationID);
        if (TextUtils.isEmpty(registrationID)) {
            return true;
        }
        ApnDelegate.m10041a().mo22544a(context, HvApiRegister.DEV_TYPE_ANDROID_JPUSH, registrationID);
        return true;
    }

    /* renamed from: a */
    public void mo22547a(Context context) {
        if (!m10054b(context)) {
            m10056c(context);
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10051a(Context context, C3560a aVar) {
        LogUtils.m1139c("HvFcm", "onSuccess", aVar.mo22216a());
        ApnDelegate.m10041a().mo22544a(context, HvApiRegister.DEV_TYPE_ANDROID_FCM, aVar.mo22216a());
    }

    /* renamed from: a */
    static /* synthetic */ void m10052a(Context context, Exception exc) {
        LogUtils.m1139c("HvFcm", "onFailure", exc.getMessage());
        FirebaseMessaging.m9904a().mo22307a(false);
        m10056c(context);
    }

    /* renamed from: a */
    public void mo22548a(String str, PushHandler eVar) {
        this.f6240a.put(str, eVar);
    }

    /* renamed from: a */
    public PushHandler mo22546a(String str) {
        if (this.f6240a.containsKey(str)) {
            return this.f6240a.get(str);
        }
        return null;
    }
}
