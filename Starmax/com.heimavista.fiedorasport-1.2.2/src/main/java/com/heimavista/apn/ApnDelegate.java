package com.heimavista.apn;

import android.content.Context;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.start.HvApiRegister;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.apn.c */
public class ApnDelegate {

    /* renamed from: com.heimavista.apn.c$a */
    /* compiled from: ApnDelegate */
    class C3665a implements OnResultListener<String> {

        /* renamed from: a */
        final /* synthetic */ String f6234a;

        /* renamed from: b */
        final /* synthetic */ String f6235b;

        /* renamed from: c */
        final /* synthetic */ int f6236c;

        /* renamed from: d */
        final /* synthetic */ Context f6237d;

        C3665a(String str, String str2, int i, Context context) {
            this.f6234a = str;
            this.f6235b = str2;
            this.f6236c = i;
            this.f6237d = context;
        }

        /* renamed from: a */
        public void mo22380a(String str) {
            SPUtils.m1243c("hvApn").mo9883b("devType", this.f6234a);
            SPUtils.m1243c("hvApn").mo9883b("apnToken", this.f6235b);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            ApnDelegate.this.m10042a(this.f6237d, this.f6234a, this.f6235b, this.f6236c - 1);
        }
    }

    /* renamed from: com.heimavista.apn.c$b */
    /* compiled from: ApnDelegate */
    private static final class C3666b {

        /* renamed from: a */
        static ApnDelegate f6239a = new ApnDelegate(null);
    }

    /* synthetic */ ApnDelegate(C3665a aVar) {
        this();
    }

    private ApnDelegate() {
    }

    /* renamed from: a */
    public static ApnDelegate m10041a() {
        return C3666b.f6239a;
    }

    /* renamed from: a */
    public void mo22544a(Context context, String str, String str2) {
        m10042a(context, str, str2, 3);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10042a(Context context, String str, String str2, int i) {
        if (i > 0 && !m10044a(str, str2)) {
            new HvApiRegister().requestWithListener(str, str2, new C3665a(str, str2, i, context));
        }
    }

    /* renamed from: a */
    private boolean m10044a(String str, String str2) {
        return SPUtils.m1243c("hvApn").mo9872a("devType", "").equalsIgnoreCase(str) && SPUtils.m1243c("hvApn").mo9872a("apnToken", "").equalsIgnoreCase(str2);
    }

    /* renamed from: a */
    public void mo22543a(Context context, PushMsg pushMsg) {
        String str = pushMsg.f6226P;
        if (TextUtils.isEmpty(str)) {
            str = "default";
        }
        PushHandler a = ApnManage.m10050a().mo22546a(str);
        LogUtils.m1123a("handler", "category=" + pushMsg.f6226P + " " + str, a, pushMsg);
        if (a != null) {
            a.mo22549a(context, pushMsg);
        }
    }
}
