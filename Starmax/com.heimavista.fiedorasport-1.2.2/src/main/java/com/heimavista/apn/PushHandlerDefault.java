package com.heimavista.apn;

import android.content.Context;
import p119e.p189e.p191b.HvNotify;

/* renamed from: com.heimavista.apn.f */
public class PushHandlerDefault implements PushHandler {
    /* renamed from: a */
    public void mo22549a(Context context, PushMsg pushMsg) {
        HvNotify b = HvNotify.m13018b();
        String str = pushMsg.f6226P;
        String str2 = pushMsg.f6227Q;
        b.mo24162a(context, str, str, 3, 0, str2, pushMsg.f6228R, str2, null);
    }
}
