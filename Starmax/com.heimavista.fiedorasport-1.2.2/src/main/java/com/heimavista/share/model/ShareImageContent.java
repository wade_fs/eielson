package com.heimavista.share.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ShareImageContent extends ShareContent {
    public static final Parcelable.Creator<ShareLinkContent> CREATOR = new C4571a();

    /* renamed from: P */
    private String f9338P;

    /* renamed from: Q */
    private String f9339Q;

    /* renamed from: com.heimavista.share.model.ShareImageContent$a */
    static class C4571a implements Parcelable.Creator<ShareLinkContent> {
        C4571a() {
        }

        public ShareLinkContent createFromParcel(Parcel parcel) {
            return new ShareLinkContent(parcel);
        }

        public ShareLinkContent[] newArray(int i) {
            return new ShareLinkContent[i];
        }
    }

    /* renamed from: a */
    public String mo25215a() {
        return this.f9338P;
    }

    /* renamed from: b */
    public String mo25216b() {
        return this.f9339Q;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f9338P);
        parcel.writeString(this.f9339Q);
    }
}
