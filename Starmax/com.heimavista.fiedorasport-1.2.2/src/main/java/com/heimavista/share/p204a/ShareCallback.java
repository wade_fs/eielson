package com.heimavista.share.p204a;

/* renamed from: com.heimavista.share.a.a */
public interface ShareCallback {
    void onCancel();

    void onError(String str);

    void onSuccess();
}
