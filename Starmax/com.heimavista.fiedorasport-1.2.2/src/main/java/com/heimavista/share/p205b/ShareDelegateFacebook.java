package com.heimavista.share.p205b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import com.blankj.utilcode.util.AppUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.heimavista.share.R$string;
import com.heimavista.share.model.ShareImageContent;
import com.heimavista.share.model.ShareLinkContent;
import com.heimavista.share.p204a.ShareCallback;

/* renamed from: com.heimavista.share.b.c */
public class ShareDelegateFacebook extends ShareDelegate {

    /* renamed from: a */
    private CallbackManager f9335a;

    /* renamed from: com.heimavista.share.b.c$a */
    /* compiled from: ShareDelegateFacebook */
    class C4569a implements FacebookCallback<Sharer.Result> {

        /* renamed from: a */
        final /* synthetic */ ShareCallback f9336a;

        C4569a(ShareDelegateFacebook cVar, ShareCallback aVar) {
            this.f9336a = aVar;
        }

        /* renamed from: a */
        public void onSuccess(Sharer.Result result) {
            this.f9336a.onSuccess();
        }

        public void onCancel() {
            this.f9336a.onCancel();
        }

        public void onError(FacebookException facebookException) {
            this.f9336a.onError(facebookException.toString());
        }
    }

    /* renamed from: com.heimavista.share.b.c$b */
    /* compiled from: ShareDelegateFacebook */
    class C4570b implements FacebookCallback<Sharer.Result> {

        /* renamed from: a */
        final /* synthetic */ ShareCallback f9337a;

        C4570b(ShareDelegateFacebook cVar, ShareCallback aVar) {
            this.f9337a = aVar;
        }

        /* renamed from: a */
        public void onSuccess(Sharer.Result result) {
            this.f9337a.onSuccess();
        }

        public void onCancel() {
            this.f9337a.onCancel();
        }

        public void onError(FacebookException facebookException) {
            this.f9337a.onError(facebookException.toString());
        }
    }

    /* renamed from: c */
    private boolean m15192c() {
        return AppUtils.m947c("com.facebook.katana");
    }

    /* renamed from: a */
    public void mo25207a() {
        this.f9335a = CallbackManager.Factory.create();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo25211a(Activity activity, ShareLinkContent shareLinkContent, ShareCallback aVar) {
        if (!m15192c() || !ShareDialog.canShow((Class<? extends ShareContent>) com.facebook.share.model.ShareLinkContent.class)) {
            m15190a(activity, aVar);
            return;
        }
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(this.f9335a, new C4569a(this, aVar));
        ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
        if (shareLinkContent.mo25222a() != null) {
            builder.setContentUrl(Uri.parse(shareLinkContent.mo25222a()));
        }
        shareDialog.show(builder.build());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo25210a(Activity activity, ShareImageContent shareImageContent, ShareCallback aVar) {
        if (!m15192c() || !ShareDialog.canShow((Class<? extends ShareContent>) SharePhotoContent.class)) {
            m15190a(activity, aVar);
            return;
        }
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(this.f9335a, new C4570b(this, aVar));
        SharePhoto.Builder builder = new SharePhoto.Builder();
        if (shareImageContent.mo25216b() != null) {
            builder.setImageUrl(Uri.parse(shareImageContent.mo25216b()));
        } else if (shareImageContent.mo25215a() != null) {
            builder.setBitmap(BitmapFactory.decodeFile(shareImageContent.mo25215a()));
        } else {
            aVar.onError("image's url or path are all null");
        }
        shareDialog.show(new SharePhotoContent.Builder().addPhoto(builder.build()).build());
    }

    /* renamed from: a */
    private void m15190a(Context context, ShareCallback aVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R$string.hv_share_facebook_uninstalled);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.setOnDismissListener(new C4568a(aVar));
        builder.show();
    }

    /* renamed from: a */
    public void mo25208a(int i, int i2, Intent intent) {
        this.f9335a.onActivityResult(i, i2, intent);
    }
}
