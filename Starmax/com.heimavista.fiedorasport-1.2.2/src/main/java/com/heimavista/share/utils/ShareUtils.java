package com.heimavista.share.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.heimavista.share.model.ShareContent;
import com.heimavista.share.p204a.ShareCallback;
import com.heimavista.share.p205b.ShareDelegate;
import com.heimavista.share.p205b.ShareDelegateFacebook;
import com.heimavista.utils.StatusBarUtils;
import java.util.ArrayList;
import java.util.Arrays;

public class ShareUtils {

    /* renamed from: a */
    private ArrayList<String> f9344a = new ArrayList<>();

    /* renamed from: b */
    private ShareCallback f9345b;

    public static class ShareActivity extends Activity {

        /* renamed from: Q */
        private static ShareCallback f9346Q;

        /* renamed from: P */
        private ShareDelegate f9347P;

        /* renamed from: com.heimavista.share.utils.ShareUtils$ShareActivity$a */
        class C4573a implements ShareCallback {
            C4573a() {
            }

            public void onCancel() {
                ShareActivity.this.m15211a();
            }

            public void onError(String str) {
                ShareActivity.this.m15215a(str);
            }

            public void onSuccess() {
                ShareActivity.this.m15216b();
            }
        }

        /* access modifiers changed from: protected */
        public void onActivityResult(int i, int i2, Intent intent) {
            super.onActivityResult(i, i2, intent);
            ShareDelegate bVar = this.f9347P;
            if (bVar != null) {
                bVar.mo25208a(i, i2, intent);
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            getWindow().addFlags(262160);
            StatusBarUtils.m15268a(getWindow());
            super.onCreate(bundle);
            String stringExtra = getIntent().getStringExtra("type");
            ShareContent shareContent = (ShareContent) getIntent().getParcelableExtra("content");
            if (((stringExtra.hashCode() == 497130182 && stringExtra.equals("facebook")) ? (char) 0 : 65535) == 0) {
                this.f9347P = new ShareDelegateFacebook();
            }
            ShareDelegate bVar = this.f9347P;
            if (bVar == null) {
                m15215a("type " + stringExtra + " is not support");
                return;
            }
            bVar.mo25207a();
            this.f9347P.mo25209a(super, shareContent, new C4573a());
        }

        /* access modifiers changed from: protected */
        public void onDestroy() {
            super.onDestroy();
            ShareDelegate bVar = this.f9347P;
            if (bVar != null) {
                bVar.mo25212b();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m15216b() {
            ShareCallback aVar = f9346Q;
            if (aVar != null) {
                aVar.onSuccess();
                f9346Q = null;
            }
            finish();
        }

        /* renamed from: a */
        public static void m15212a(Context context, String str, ShareContent shareContent, ShareCallback aVar) {
            f9346Q = aVar;
            Intent intent = new Intent(context, ShareActivity.class);
            intent.putExtra("type", str);
            intent.putExtra("content", shareContent);
            context.startActivity(intent);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m15211a() {
            ShareCallback aVar = f9346Q;
            if (aVar != null) {
                aVar.onCancel();
                f9346Q = null;
            }
            finish();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m15215a(String str) {
            ShareCallback aVar = f9346Q;
            if (aVar != null) {
                aVar.onError(str);
                f9346Q = null;
            }
            finish();
        }
    }

    private ShareUtils(String... strArr) {
        this.f9344a.addAll(Arrays.asList(strArr));
    }

    /* renamed from: a */
    public static ShareUtils m15206a(String... strArr) {
        return new ShareUtils(strArr);
    }

    /* renamed from: a */
    public ShareUtils mo25230a(ShareCallback aVar) {
        this.f9345b = aVar;
        return this;
    }

    /* renamed from: a */
    public void mo25231a(Context context, ShareContent shareContent) {
        if (shareContent == null) {
            m15208a("content is not empty");
        } else if (this.f9344a.size() > 1) {
            m15208a("types multi is not ready");
        } else if (this.f9344a.size() == 1) {
            m15207a(context, this.f9344a.get(0), shareContent);
        } else {
            m15208a("types is empty");
        }
    }

    /* renamed from: a */
    private void m15207a(Context context, String str, ShareContent shareContent) {
        if (shareContent == null) {
            m15208a("content is null");
        } else {
            ShareActivity.m15212a(context, str, shareContent, this.f9345b);
        }
    }

    /* renamed from: a */
    private void m15208a(String str) {
        ShareCallback aVar = this.f9345b;
        if (aVar != null) {
            aVar.onError(str);
            this.f9345b = null;
        }
    }
}
