package com.heimavista.share.p205b;

import android.app.Activity;
import android.content.Intent;
import com.heimavista.share.model.ShareContent;
import com.heimavista.share.model.ShareImageContent;
import com.heimavista.share.model.ShareLinkContent;
import com.heimavista.share.p204a.ShareCallback;

/* renamed from: com.heimavista.share.b.b */
public abstract class ShareDelegate {
    /* renamed from: a */
    public abstract void mo25207a();

    /* renamed from: a */
    public abstract void mo25208a(int i, int i2, Intent intent);

    /* renamed from: a */
    public void mo25209a(Activity activity, ShareContent shareContent, ShareCallback aVar) {
        if (shareContent instanceof ShareLinkContent) {
            mo25211a(activity, (ShareLinkContent) shareContent, aVar);
        } else if (shareContent instanceof ShareImageContent) {
            mo25210a(activity, (ShareImageContent) shareContent, aVar);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo25210a(Activity activity, ShareImageContent shareImageContent, ShareCallback aVar);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo25211a(Activity activity, ShareLinkContent shareLinkContent, ShareCallback aVar);

    /* renamed from: b */
    public void mo25212b() {
    }
}
