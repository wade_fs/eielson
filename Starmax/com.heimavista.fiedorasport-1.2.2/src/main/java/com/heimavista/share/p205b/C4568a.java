package com.heimavista.share.p205b;

import android.content.DialogInterface;
import com.heimavista.share.p204a.ShareCallback;

/* renamed from: com.heimavista.share.b.a */
/* compiled from: lambda */
public final /* synthetic */ class C4568a implements DialogInterface.OnDismissListener {

    /* renamed from: P */
    private final /* synthetic */ ShareCallback f9334P;

    public /* synthetic */ C4568a(ShareCallback aVar) {
        this.f9334P = aVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f9334P.onError("facebook uninstalled");
    }
}
