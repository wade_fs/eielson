package com.heimavista.share.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ShareLinkContent extends ShareContent {
    public static final Parcelable.Creator<ShareLinkContent> CREATOR = new C4572a();

    /* renamed from: P */
    private String f9340P;

    /* renamed from: Q */
    private String f9341Q;

    /* renamed from: R */
    private String f9342R;

    /* renamed from: S */
    private String f9343S;

    /* renamed from: com.heimavista.share.model.ShareLinkContent$a */
    static class C4572a implements Parcelable.Creator<ShareLinkContent> {
        C4572a() {
        }

        public ShareLinkContent createFromParcel(Parcel parcel) {
            return new ShareLinkContent(parcel);
        }

        public ShareLinkContent[] newArray(int i) {
            return new ShareLinkContent[i];
        }
    }

    public ShareLinkContent() {
    }

    /* renamed from: a */
    public String mo25222a() {
        return this.f9340P;
    }

    /* renamed from: b */
    public ShareLinkContent mo25223b(String str) {
        this.f9340P = str;
        return this;
    }

    /* renamed from: c */
    public ShareLinkContent mo25224c(String str) {
        this.f9342R = str;
        return this;
    }

    /* renamed from: d */
    public ShareLinkContent mo25225d(String str) {
        this.f9341Q = str;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f9340P);
        parcel.writeString(this.f9341Q);
        parcel.writeString(this.f9342R);
        parcel.writeString(this.f9343S);
    }

    protected ShareLinkContent(Parcel parcel) {
        this.f9340P = parcel.readString();
        this.f9341Q = parcel.readString();
        this.f9342R = parcel.readString();
        this.f9343S = parcel.readString();
    }

    /* renamed from: a */
    public ShareLinkContent mo25221a(String str) {
        this.f9343S = str;
        return this;
    }
}
