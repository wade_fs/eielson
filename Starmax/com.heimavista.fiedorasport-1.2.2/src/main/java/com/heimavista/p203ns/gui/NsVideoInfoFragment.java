package com.heimavista.p203ns.gui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.p203ns.object.NsVideo;
import java.util.ArrayList;

/* renamed from: com.heimavista.ns.gui.NsVideoInfoFragment */
public class NsVideoInfoFragment extends Fragment {

    /* renamed from: P */
    private ArrayList<NsVideo> f9292P;

    /* renamed from: Q */
    private int f9293Q;

    /* renamed from: R */
    private View f9294R;

    /* renamed from: S */
    private TextView f9295S;

    /* renamed from: T */
    private TextView f9296T;

    /* renamed from: U */
    private ListView f9297U;

    /* renamed from: V */
    private C4558a f9298V;

    /* renamed from: com.heimavista.ns.gui.NsVideoInfoFragment$a */
    public interface C4558a {
        /* renamed from: a */
        void mo25166a(ArrayList<NsVideo> arrayList, int i);
    }

    /* renamed from: b */
    private void m15156b() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f9292P = arguments.getParcelableArrayList(NsVideo.class.getCanonicalName());
            this.f9293Q = arguments.getInt("index", 0);
        }
    }

    /* renamed from: c */
    private void m15157c() {
        this.f9297U.setOnItemClickListener(new C4563d(this));
    }

    /* renamed from: d */
    private void m15158d() {
        this.f9295S = (TextView) this.f9294R.findViewById(R$id.tvTitle);
        this.f9296T = (TextView) this.f9294R.findViewById(R$id.tvDesc);
        this.f9297U = (ListView) this.f9294R.findViewById(16908298);
        this.f9297U.setAdapter((ListAdapter) new NsVideoAdapter(this.f9294R.getContext(), this.f9292P));
    }

    /* renamed from: e */
    private void m15159e() {
        NsVideo nsVideo = this.f9292P.get(this.f9293Q);
        String str = "";
        this.f9295S.setText(nsVideo.mo25199e() != null ? nsVideo.mo25199e() : str);
        TextView textView = this.f9296T;
        if (nsVideo.mo25194a() != null) {
            str = nsVideo.mo25194a();
        }
        textView.setText(str);
    }

    /* renamed from: a */
    public /* synthetic */ void mo25167a(AdapterView adapterView, View view, int i, long j) {
        this.f9298V.mo25166a(this.f9292P, i);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9298V = (C4558a) context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        if (this.f9294R == null) {
            this.f9294R = layoutInflater.inflate(R$layout.hv_ns_video_info, viewGroup, false);
            m15156b();
            m15158d();
            m15157c();
            m15159e();
        }
        return this.f9294R;
    }
}
