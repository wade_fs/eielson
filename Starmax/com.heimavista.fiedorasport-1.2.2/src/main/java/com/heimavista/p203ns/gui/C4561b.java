package com.heimavista.p203ns.gui;

import com.heimavista.api.base.HvApiGetInformList;
import com.heimavista.utils.ParamJsonData;

/* renamed from: com.heimavista.ns.gui.b */
/* compiled from: lambda */
public final /* synthetic */ class C4561b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f9311P;

    /* renamed from: Q */
    private final /* synthetic */ String f9312Q;

    /* renamed from: R */
    private final /* synthetic */ String f9313R;

    public /* synthetic */ C4561b(String str, String str2, String str3) {
        this.f9311P = str;
        this.f9312Q = str2;
        this.f9313R = str3;
    }

    public final void run() {
        HvApiGetInformList.runInform(this.f9311P, this.f9312Q, new ParamJsonData(this.f9313R, true).mo25333c());
    }
}
