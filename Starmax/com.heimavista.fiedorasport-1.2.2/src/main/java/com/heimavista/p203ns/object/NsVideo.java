package com.heimavista.p203ns.object;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.heimavista.ns.object.NsVideo */
public class NsVideo implements Parcelable {
    public static final Parcelable.Creator<NsVideo> CREATOR = new C4567a();

    /* renamed from: P */
    private String f9326P;

    /* renamed from: Q */
    private String f9327Q;

    /* renamed from: R */
    private String f9328R;

    /* renamed from: S */
    private String f9329S;

    /* renamed from: T */
    private int f9330T;

    /* renamed from: U */
    private int f9331U;

    /* renamed from: V */
    private long f9332V;

    /* renamed from: W */
    private boolean f9333W;

    /* renamed from: com.heimavista.ns.object.NsVideo$a */
    static class C4567a implements Parcelable.Creator<NsVideo> {
        C4567a() {
        }

        public NsVideo createFromParcel(Parcel parcel) {
            return new NsVideo(parcel);
        }

        public NsVideo[] newArray(int i) {
            return new NsVideo[i];
        }
    }

    public NsVideo(String str, String str2, String str3, String str4, int i, int i2, long j, boolean z) {
        this.f9326P = str;
        this.f9327Q = str2;
        this.f9328R = str3;
        this.f9329S = str4;
        this.f9330T = i;
        this.f9331U = i2;
        this.f9332V = j;
        this.f9333W = z;
    }

    /* renamed from: a */
    public String mo25194a() {
        return this.f9327Q;
    }

    /* renamed from: b */
    public long mo25195b() {
        return this.f9332V;
    }

    /* renamed from: c */
    public int mo25196c() {
        return this.f9331U;
    }

    /* renamed from: d */
    public String mo25197d() {
        return this.f9328R;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public String mo25199e() {
        return this.f9326P;
    }

    /* renamed from: f */
    public String mo25200f() {
        return this.f9329S;
    }

    /* renamed from: g */
    public int mo25201g() {
        return this.f9330T;
    }

    /* renamed from: h */
    public boolean mo25202h() {
        return this.f9333W;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f9326P);
        parcel.writeString(this.f9327Q);
        parcel.writeString(this.f9328R);
        parcel.writeString(this.f9329S);
        parcel.writeInt(this.f9330T);
        parcel.writeInt(this.f9331U);
        parcel.writeLong(this.f9332V);
        parcel.writeByte(this.f9333W ? (byte) 1 : 0);
    }

    protected NsVideo(Parcel parcel) {
        this.f9326P = parcel.readString();
        this.f9327Q = parcel.readString();
        this.f9328R = parcel.readString();
        this.f9329S = parcel.readString();
        this.f9330T = parcel.readInt();
        this.f9331U = parcel.readInt();
        this.f9332V = parcel.readLong();
        this.f9333W = parcel.readByte() != 0;
    }
}
