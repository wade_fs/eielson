package com.heimavista.p203ns.gui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.p203ns.gui.NsVideoInfoFragment;
import com.heimavista.p203ns.object.NsVideo;
import java.util.ArrayList;

/* renamed from: com.heimavista.ns.gui.NsVideoActivity */
public class NsVideoActivity extends FragmentActivity implements NsVideoInfoFragment.C4558a {

    /* renamed from: P */
    private ArrayList<NsVideo> f9290P;

    /* renamed from: Q */
    private int f9291Q;

    /* renamed from: a */
    public static Intent m15149a(Context context, ArrayList<NsVideo> arrayList, int i) {
        Intent intent = new Intent(context, NsVideoActivity.class);
        intent.putParcelableArrayListExtra(NsVideo.class.getCanonicalName(), arrayList);
        intent.putExtra("index", i);
        return intent;
    }

    /* renamed from: b */
    private void m15151b() {
        findViewById(R$id.ivBack).setOnClickListener(new C4562c(this));
    }

    /* renamed from: c */
    private void m15152c() {
        NsVideoInfoFragment nsVideoInfoFragment = new NsVideoInfoFragment();
        nsVideoInfoFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().replace(R$id.ll_fragment_info, nsVideoInfoFragment, "ns_video_info").commit();
    }

    /* renamed from: d */
    private void m15153d() {
        NsVideoJzFragment nsVideoJzFragment = new NsVideoJzFragment();
        nsVideoJzFragment.setArguments(NsVideoJzFragment.m15162a(this.f9290P.get(this.f9291Q)));
        getSupportFragmentManager().beginTransaction().replace(R$id.ll_fragment_video_player, nsVideoJzFragment, "ns_video_jz").commit();
    }

    public void onBackPressed() {
        boolean z = false;
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof NsVideoJzFragment) {
                z |= ((NsVideoJzFragment) fragment).mo25168b();
            }
        }
        if (!z) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.hv_ns_video);
        m15150a();
        m15151b();
        if (bundle == null) {
            m15153d();
            m15152c();
        }
    }

    /* renamed from: a */
    private void m15150a() {
        this.f9290P = getIntent().getParcelableArrayListExtra(NsVideo.class.getCanonicalName());
        this.f9291Q = getIntent().getIntExtra("index", 0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo25165a(View view) {
        finish();
    }

    /* renamed from: a */
    public void mo25166a(ArrayList<NsVideo> arrayList, int i) {
        startActivity(m15149a(this, arrayList, i));
        finish();
        overridePendingTransition(0, 0);
    }
}
