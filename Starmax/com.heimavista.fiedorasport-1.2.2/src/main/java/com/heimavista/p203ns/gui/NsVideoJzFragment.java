package com.heimavista.p203ns.gui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.p203ns.NsManage;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.p203ns.object.NsVideo;
import org.xutils.C5217x;
import p019cn.jzvd.JZDataSource;
import p019cn.jzvd.Jzvd;
import p019cn.jzvd.JzvdStd;
import p019cn.jzvd.media.JZMediaExo;

/* renamed from: com.heimavista.ns.gui.NsVideoJzFragment */
public class NsVideoJzFragment extends Fragment {

    /* renamed from: P */
    private NsVideo f9299P;

    /* renamed from: Q */
    private ColorDrawable f9300Q;

    /* renamed from: R */
    private JzvdStd f9301R;

    /* renamed from: S */
    private View f9302S;

    /* renamed from: T */
    private int f9303T;

    /* renamed from: a */
    public static Bundle m15162a(NsVideo nsVideo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(NsVideo.class.getCanonicalName(), nsVideo);
        return bundle;
    }

    /* renamed from: c */
    private void m15163c() {
        this.f9301R = (JzvdStd) this.f9302S.findViewById(R$id.videoplayer);
    }

    /* renamed from: d */
    private void m15164d() {
        Jzvd.releaseAllVideos();
        int i = this.f9303T;
        int c = (this.f9299P.mo25196c() * i) / this.f9299P.mo25201g();
        ViewGroup.LayoutParams layoutParams = this.f9301R.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = c;
        ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) this.f9301R.getParent()).getLayoutParams();
        layoutParams2.width = i;
        layoutParams2.height = c;
        String f = this.f9299P.mo25200f();
        if (this.f9299P.mo25202h()) {
            f = NsManage.m15135d().mo25158c().mo23164a(f);
        }
        this.f9301R.setUp(new JZDataSource(f), 0, JZMediaExo.class);
        if (this.f9299P.mo25197d() == null) {
            this.f9301R.thumbImageView.setImageDrawable(this.f9300Q);
        } else {
            C5217x.image().bind(this.f9301R.thumbImageView, this.f9299P.mo25197d());
        }
        this.f9301R.startVideo();
    }

    /* renamed from: b */
    public boolean mo25168b() {
        return Jzvd.backPress();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9303T = ScreenUtils.m1263a();
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        this.f9299P = (NsVideo) getArguments().getParcelable(NsVideo.class.getCanonicalName());
        this.f9300Q = new ColorDrawable(ViewCompat.MEASURED_STATE_MASK);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        if (this.f9302S == null) {
            this.f9302S = layoutInflater.inflate(R$layout.hv_ns_video_jz, viewGroup, false);
            m15163c();
        }
        return this.f9302S;
    }

    public void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }

    public void onResume() {
        super.onResume();
        m15164d();
    }
}
