package com.heimavista.p203ns.gui;

import android.view.View;
import android.widget.AdapterView;

/* renamed from: com.heimavista.ns.gui.d */
/* compiled from: lambda */
public final /* synthetic */ class C4563d implements AdapterView.OnItemClickListener {

    /* renamed from: P */
    private final /* synthetic */ NsVideoInfoFragment f9315P;

    public /* synthetic */ C4563d(NsVideoInfoFragment nsVideoInfoFragment) {
        this.f9315P = nsVideoInfoFragment;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.f9315P.mo25167a(adapterView, view, i, j);
    }
}
