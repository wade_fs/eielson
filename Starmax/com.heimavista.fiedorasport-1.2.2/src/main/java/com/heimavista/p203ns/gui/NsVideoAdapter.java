package com.heimavista.p203ns.gui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.view.ViewCompat;
import com.facebook.appevents.AppEventsConstants;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.p203ns.object.NsVideo;
import java.util.ArrayList;
import java.util.List;
import org.xutils.C5217x;

/* renamed from: com.heimavista.ns.gui.f */
public class NsVideoAdapter extends BaseAdapter {

    /* renamed from: P */
    private Context f9319P;

    /* renamed from: Q */
    private List<NsVideo> f9320Q = new ArrayList();

    /* renamed from: R */
    private ColorDrawable f9321R;

    /* renamed from: com.heimavista.ns.gui.f$b */
    /* compiled from: NsVideoAdapter */
    private class C4566b {

        /* renamed from: a */
        ImageView f9322a;

        /* renamed from: b */
        TextView f9323b;

        /* renamed from: c */
        TextView f9324c;

        /* renamed from: d */
        TextView f9325d;

        private C4566b(NsVideoAdapter fVar) {
        }
    }

    public NsVideoAdapter(Context context, List<NsVideo> list) {
        this.f9319P = context;
        if (list != null) {
            this.f9320Q.addAll(list);
        }
        this.f9321R = new ColorDrawable(ViewCompat.MEASURED_STATE_MASK);
    }

    public int getCount() {
        return this.f9320Q.size();
    }

    public Object getItem(int i) {
        return this.f9320Q.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        C4566b bVar;
        String str;
        String str2;
        String str3;
        if (view == null) {
            view = LayoutInflater.from(this.f9319P).inflate(R$layout.hv_ns_video_item, viewGroup, false);
            bVar = new C4566b();
            bVar.f9322a = (ImageView) view.findViewById(16908294);
            bVar.f9323b = (TextView) view.findViewById(16908310);
            bVar.f9324c = (TextView) view.findViewById(16908290);
            bVar.f9325d = (TextView) view.findViewById(R$id.tvDuration);
            view.setTag(bVar);
        } else {
            bVar = (C4566b) view.getTag();
        }
        NsVideo nsVideo = (NsVideo) getItem(i);
        bVar.f9323b.setText(nsVideo.mo25199e() != null ? nsVideo.mo25199e() : "");
        TextView textView = bVar.f9324c;
        if (nsVideo.mo25194a() != null) {
            str = nsVideo.mo25194a();
        } else {
            str = "";
        }
        textView.setText(str);
        if (nsVideo.mo25197d() == null) {
            bVar.f9322a.setImageDrawable(this.f9321R);
        } else {
            C5217x.image().bind(bVar.f9322a, nsVideo.mo25197d());
        }
        if (nsVideo.mo25195b() > 0) {
            bVar.f9325d.setVisibility(0);
        } else {
            bVar.f9325d.setVisibility(8);
        }
        long b = nsVideo.mo25195b() % 60;
        long b2 = nsVideo.mo25195b() / 60;
        if (b2 > 9) {
            str2 = "" + b2;
        } else {
            str2 = "" + AppEventsConstants.EVENT_PARAM_VALUE_NO + b2;
        }
        String str4 = str2 + ":";
        if (b > 9) {
            str3 = str4 + b;
        } else {
            str3 = str4 + AppEventsConstants.EVENT_PARAM_VALUE_NO + b;
        }
        bVar.f9325d.setText(str3);
        return view;
    }
}
