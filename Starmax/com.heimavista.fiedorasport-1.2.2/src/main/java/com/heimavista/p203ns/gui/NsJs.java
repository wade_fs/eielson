package com.heimavista.p203ns.gui;

import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import com.blankj.utilcode.util.LogUtils;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.heimavista.p203ns.object.NsVideo;
import com.heimavista.share.model.ShareLinkContent;
import com.heimavista.share.p204a.ShareCallback;
import com.heimavista.share.utils.ShareUtils;
import com.heimavista.utils.StatusBarUtils;
import com.heimavista.widget.JsInterface;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p217f.GAManage;

/* renamed from: com.heimavista.ns.gui.e */
public class NsJs extends JsInterface {

    /* renamed from: e */
    public static final String f9316e = (NsJs.class.getName() + ".notifyNsHomeRefresh");

    /* renamed from: d */
    private String f9317d;

    /* renamed from: com.heimavista.ns.gui.e$a */
    /* compiled from: NsJs */
    class C4564a implements ShareCallback {
        C4564a() {
        }

        public void onCancel() {
            NsJs.this.mo25178c();
        }

        public void onError(String str) {
            NsJs.this.mo25178c();
        }

        public void onSuccess() {
            NsJs.this.mo25179d();
        }
    }

    /* renamed from: b */
    private void m15172b(String str, JSONObject jSONObject) {
        if (mo25480b() != null) {
            mo25476a().runOnUiThread(new C4560a(this, str, jSONObject));
        }
    }

    @JavascriptInterface
    public void backConfig(int i) {
        if (i < 0) {
            mo25480b().clearHistory();
        }
    }

    /* renamed from: c */
    public void mo25178c() {
        LogUtils.m1123a("onShareFailed:" + this.f9317d);
        if (this.f9317d != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("stat", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            m15172b(this.f9317d, jSONObject);
        }
    }

    /* renamed from: d */
    public void mo25179d() {
        LogUtils.m1123a("onShareSuccess:" + this.f9317d);
        if (this.f9317d != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("stat", 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            m15172b(this.f9317d, jSONObject);
        }
    }

    @JavascriptInterface
    public int getStatusBarHeight() {
        return StatusBarUtils.m15267a(mo25476a().getResources());
    }

    @JavascriptInterface
    public void login(String str) {
    }

    @JavascriptInterface
    public void notifyNsHomeRefresh() {
        HvApp.m13010c().mo24157a(f9316e);
    }

    @JavascriptInterface
    public void open(String str) {
        mo25476a().startActivity(NsWebActivity.m15166a(mo25476a(), str));
    }

    @JavascriptInterface
    public void playVideo(String str, int i) {
        String str2 = str;
        int i2 = 0;
        LogUtils.m1139c(str2);
        if (!TextUtils.isEmpty(str)) {
            try {
                ArrayList arrayList = new ArrayList();
                JSONArray jSONArray = new JSONArray(str2);
                int length = jSONArray.length();
                int i3 = 0;
                while (i3 < length) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i3);
                    String optString = jSONObject.optString("title", null);
                    String optString2 = jSONObject.optString("desc", null);
                    String optString3 = jSONObject.optString("preview", null);
                    String optString4 = jSONObject.optString("url", null);
                    int optInt = jSONObject.optInt(ViewHierarchyConstants.DIMENSION_WIDTH_KEY, i2);
                    int optInt2 = jSONObject.optInt(ViewHierarchyConstants.DIMENSION_HEIGHT_KEY, i2);
                    ArrayList arrayList2 = arrayList;
                    long optLong = jSONObject.optLong("duration", 0);
                    boolean z = jSONObject.optInt("cache", i2) == 1;
                    NsVideo nsVideo = r7;
                    NsVideo nsVideo2 = new NsVideo(optString, optString2, optString3, optString4, optInt, optInt2, optLong, z);
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(nsVideo);
                    i3++;
                    arrayList = arrayList3;
                    i2 = 0;
                }
                mo25476a().startActivity(NsVideoActivity.m15149a(mo25476a(), arrayList, i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @JavascriptInterface
    public void sendInform(String str, String str2, String str3) {
        new Thread(new C4561b(str, str2, str3)).start();
    }

    @JavascriptInterface
    public void share(String str, String str2, String str3, String str4, String str5, String str6) {
        LogUtils.m1139c(str, str2, str3, str4, str5, str6);
        this.f9317d = str6;
        ShareLinkContent shareLinkContent = new ShareLinkContent();
        shareLinkContent.mo25223b(str5).mo25221a(str4).mo25225d(str2).mo25224c(str3);
        ShareUtils a = ShareUtils.m15206a("facebook");
        a.mo25230a(new C4564a());
        a.mo25231a(mo25476a(), shareLinkContent);
        GAManage.m17122a().mo27171a("分享", str, "fb", null);
    }

    /* renamed from: a */
    public /* synthetic */ void mo25176a(String str, JSONObject jSONObject) {
        WebView b = mo25480b();
        b.loadUrl("javascript:" + str + "(" + jSONObject + ")");
    }
}
