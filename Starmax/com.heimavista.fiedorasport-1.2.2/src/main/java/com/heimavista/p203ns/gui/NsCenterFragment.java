package com.heimavista.p203ns.gui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.blankj.utilcode.util.NetworkUtils;
import com.heimavista.p203ns.NsManage;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.widget.HvWebView;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.ns.gui.NsCenterFragment */
public class NsCenterFragment extends Fragment {

    /* renamed from: P */
    private View f9283P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public HvWebView f9284Q;

    /* renamed from: R */
    private BroadcastReceiver f9285R;

    /* renamed from: S */
    private NsCenterConfig f9286S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public boolean f9287T = false;

    /* renamed from: com.heimavista.ns.gui.NsCenterFragment$NsCenterConfig */
    public static class NsCenterConfig implements Parcelable {
        public static final Parcelable.Creator<NsCenterConfig> CREATOR = new C4556a();

        /* renamed from: P */
        boolean f9288P;

        /* renamed from: com.heimavista.ns.gui.NsCenterFragment$NsCenterConfig$a */
        static class C4556a implements Parcelable.Creator<NsCenterConfig> {
            C4556a() {
            }

            public NsCenterConfig createFromParcel(Parcel parcel) {
                return new NsCenterConfig(parcel);
            }

            public NsCenterConfig[] newArray(int i) {
                return new NsCenterConfig[i];
            }
        }

        public NsCenterConfig() {
        }

        /* renamed from: a */
        public NsCenterConfig mo25159a(boolean z) {
            this.f9288P = z;
            return this;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeByte(this.f9288P ? (byte) 1 : 0);
        }

        protected NsCenterConfig(Parcel parcel) {
            this.f9288P = parcel.readByte() != 0;
        }
    }

    /* renamed from: com.heimavista.ns.gui.NsCenterFragment$a */
    class C4557a extends BroadcastReceiver {
        C4557a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (MemberControl.f11153e.equals(intent.getAction()) || MemberControl.f11156h.equals(intent.getAction()) || MemberControl.f11155g.equals(intent.getAction())) {
                boolean unused = NsCenterFragment.this.f9287T = false;
                NsCenterFragment.this.m15147b();
            } else if (NsJs.f9316e.equals(intent.getAction())) {
                NsCenterFragment.this.f9284Q.reload();
            }
        }
    }

    private NsCenterFragment() {
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f9286S = (NsCenterConfig) getArguments().getParcelable(NsCenterConfig.class.getCanonicalName());
        NsManage.m15135d().mo25156a(context);
        this.f9285R = new C4557a();
        HvApp.m13010c().mo24156a(this.f9285R, MemberControl.f11153e, MemberControl.f11156h, MemberControl.f11155g, NsJs.f9316e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        if (this.f9283P == null) {
            this.f9283P = layoutInflater.inflate(R$layout.hv_ns_center, viewGroup, false);
            this.f9284Q = (HvWebView) this.f9283P.findViewById(R$id.webview);
            NsJs eVar = new NsJs();
            eVar.mo25479a(super);
            eVar.mo25478a(this.f9284Q);
            this.f9284Q.setJsInterface(eVar);
            if (this.f9286S.f9288P) {
                WebSettings settings = this.f9284Q.getSettings();
                settings.setUserAgentString(settings.getUserAgentString() + " FiedoraApp");
            }
        }
        return this.f9283P;
    }

    public void onDestroy() {
        super.onDestroy();
        HvApp.m13010c().mo24155a(this.f9285R);
    }

    public void onPause() {
        super.onPause();
        if (this.f9287T) {
            this.f9284Q.onPause();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.f9287T) {
            this.f9284Q.mo25338b();
        } else {
            m15147b();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m15147b() {
        if (this.f9287T) {
            return;
        }
        if (!NetworkUtils.m858c()) {
            this.f9284Q.setVisibility(8);
            return;
        }
        this.f9287T = true;
        this.f9284Q.setVisibility(0);
        this.f9284Q.loadUrl(NsManage.m15135d().mo25157b());
    }

    /* renamed from: a */
    public static NsCenterFragment m15143a(NsCenterConfig nsCenterConfig) {
        NsCenterFragment nsCenterFragment = new NsCenterFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(NsCenterConfig.class.getCanonicalName(), nsCenterConfig);
        super.setArguments(bundle);
        return nsCenterFragment;
    }
}
