package com.heimavista.p203ns;

import android.content.Context;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.p106ns.HvApiRedirect;
import com.heimavista.api.p106ns.HvApiRedirect2Addr;
import p119e.p124b.p125a.HttpProxyCacheServer;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.ns.a */
public class NsManage {

    /* renamed from: com.heimavista.ns.a$a */
    /* compiled from: NsManage */
    private static class C4555a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final NsManage f9281a = new NsManage();
        /* access modifiers changed from: private */

        /* renamed from: b */
        public static final HttpProxyCacheServer f9282b = new HttpProxyCacheServer(HvApp.m13010c());
    }

    /* renamed from: d */
    public static NsManage m15135d() {
        return C4555a.f9281a;
    }

    /* renamed from: a */
    public String mo25154a() {
        String url = new HvApiRedirect2Addr().getUrl();
        LogUtils.m1139c(url);
        return url;
    }

    /* renamed from: b */
    public String mo25157b() {
        return mo25155a("");
    }

    /* renamed from: c */
    public HttpProxyCacheServer mo25158c() {
        return C4555a.f9282b;
    }

    /* renamed from: a */
    public String mo25155a(String str) {
        String url = new HvApiRedirect().getUrl(str);
        LogUtils.m1139c(str, url);
        return url;
    }

    /* renamed from: a */
    public void mo25156a(Context context) {
        if (!MemberControl.m17125s().mo27192q()) {
            CookieSyncManager.createInstance(context);
            CookieManager instance = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT >= 21) {
                instance.removeSessionCookies(null);
                instance.removeAllCookies(null);
                instance.flush();
                return;
            }
            CookieSyncManager.getInstance().startSync();
            instance.removeAllCookie();
        }
    }
}
