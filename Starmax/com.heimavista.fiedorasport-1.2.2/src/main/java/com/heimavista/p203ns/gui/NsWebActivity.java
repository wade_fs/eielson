package com.heimavista.p203ns.gui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.widget.ProgressBar;
import androidx.annotation.Nullable;
import com.heimavista.p203ns.NsManage;
import com.heimavista.p203ns.R$drawable;
import com.heimavista.p203ns.R$id;
import com.heimavista.p203ns.R$layout;
import com.heimavista.widget.HvWebView;

/* renamed from: com.heimavista.ns.gui.NsWebActivity */
public class NsWebActivity extends Activity {

    /* renamed from: P */
    private HvWebView f9304P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public ProgressBar f9305Q;

    /* renamed from: R */
    private NsJs f9306R;

    /* renamed from: com.heimavista.ns.gui.NsWebActivity$a */
    class C4559a implements HvWebView.C4599d {
        C4559a() {
        }

        /* renamed from: a */
        public void mo24581a(int i) {
            NsWebActivity.this.f9305Q.setProgress(i);
        }

        public void onFinished() {
            NsWebActivity.this.f9305Q.setVisibility(8);
        }

        public void onStart() {
            NsWebActivity.this.f9305Q.setVisibility(0);
        }

        /* renamed from: a */
        public void mo24582a(String str) {
            if (!TextUtils.isEmpty(str) && !URLUtil.isNetworkUrl(str) && NsWebActivity.this.getActionBar() != null) {
                NsWebActivity.this.getActionBar().setTitle(str);
            }
        }
    }

    public void onBackPressed() {
        if (this.f9304P.canGoBack()) {
            this.f9304P.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        NsManage.m15135d().mo25156a(this);
        setContentView(R$layout.hv_ns_web);
        if (getActionBar() != null) {
            getActionBar().setTitle("");
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayUseLogoEnabled(false);
            getActionBar().setHomeAsUpIndicator(R$drawable.hv_ns_close);
        }
        this.f9305Q = (ProgressBar) findViewById(R$id.progressBar);
        this.f9304P = (HvWebView) findViewById(R$id.webview);
        m15168a();
        this.f9304P.loadUrl(getIntent().getStringExtra("url"));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    /* renamed from: a */
    public static Intent m15166a(Context context, String str) {
        Intent intent = new Intent(context, NsWebActivity.class);
        intent.putExtra("url", str);
        return intent;
    }

    /* renamed from: a */
    private void m15168a() {
        this.f9304P.setOnProgressListener(new C4559a());
        this.f9306R = new NsJs();
        this.f9306R.mo25477a(super);
        this.f9304P.setJsInterface(this.f9306R);
        WebSettings settings = this.f9304P.getSettings();
        settings.setUserAgentString(settings.getUserAgentString() + " FiedoraApp");
    }
}
