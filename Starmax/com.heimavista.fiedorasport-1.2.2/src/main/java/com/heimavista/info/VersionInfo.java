package com.heimavista.info;

import android.os.Parcel;
import android.os.Parcelable;

public class VersionInfo implements Parcelable {
    public static final Parcelable.Creator<VersionInfo> CREATOR = new C4554a();

    /* renamed from: P */
    public int f9274P;

    /* renamed from: Q */
    public String f9275Q;

    /* renamed from: R */
    public String f9276R;

    /* renamed from: S */
    public int f9277S;

    /* renamed from: T */
    public String f9278T;

    /* renamed from: U */
    public String f9279U;

    /* renamed from: com.heimavista.info.VersionInfo$a */
    static class C4554a implements Parcelable.Creator<VersionInfo> {
        C4554a() {
        }

        public VersionInfo createFromParcel(Parcel parcel) {
            return new VersionInfo(parcel);
        }

        public VersionInfo[] newArray(int i) {
            return new VersionInfo[i];
        }
    }

    protected VersionInfo(Parcel parcel) {
        this.f9274P = parcel.readInt();
        this.f9275Q = parcel.readString();
        this.f9276R = parcel.readString();
        this.f9277S = parcel.readInt();
        this.f9278T = parcel.readString();
        this.f9279U = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f9274P);
        parcel.writeString(this.f9275Q);
        parcel.writeString(this.f9276R);
        parcel.writeInt(this.f9277S);
        parcel.writeString(this.f9278T);
        parcel.writeString(this.f9279U);
    }
}
