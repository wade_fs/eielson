package com.heimavista.utils;

import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/* renamed from: com.heimavista.utils.d */
public class ListUtil {
    /* renamed from: a */
    public static JSONArray m15238a(List<Object> list) {
        JSONArray jSONArray = new JSONArray();
        if (list != null) {
            for (Object obj : list) {
                if (obj instanceof Map) {
                    jSONArray.put(MapUtil.m15245a((Map) obj));
                } else if (obj instanceof List) {
                    jSONArray.put(m15238a((List) obj));
                } else {
                    jSONArray.put(obj);
                }
            }
        }
        return jSONArray;
    }
}
