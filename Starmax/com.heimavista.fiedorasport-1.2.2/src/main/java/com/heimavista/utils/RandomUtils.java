package com.heimavista.utils;

import android.text.TextUtils;
import java.util.Random;

/* renamed from: com.heimavista.utils.g */
public class RandomUtils {
    /* renamed from: a */
    public static String m15264a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return m15265a(str.toCharArray(), i);
    }

    /* renamed from: b */
    public static String m15266b(int i) {
        return m15264a("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", i);
    }

    /* renamed from: a */
    public static String m15265a(char[] cArr, int i) {
        if (cArr == null || cArr.length == 0 || i < 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(i);
        Random random = new Random();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    /* renamed from: a */
    public static int m15262a(int i) {
        return m15263a(0, i);
    }

    /* renamed from: a */
    public static int m15263a(int i, int i2) {
        if (i > i2) {
            return 0;
        }
        return i == i2 ? i : i + new Random().nextInt(i2 - i);
    }
}
