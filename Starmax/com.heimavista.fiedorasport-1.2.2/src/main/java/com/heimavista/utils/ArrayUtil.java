package com.heimavista.utils;

/* renamed from: com.heimavista.utils.a */
public class ArrayUtil {
    /* renamed from: a */
    public static <T> String m15235a(T[] tArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tArr.length; i++) {
            sb.append((Object) tArr[i]);
            if (i < tArr.length - 1) {
                sb.append(str);
            }
        }
        return sb.toString();
    }
}
