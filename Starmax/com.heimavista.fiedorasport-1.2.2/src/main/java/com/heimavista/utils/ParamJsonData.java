package com.heimavista.utils;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.heimavista.utils.f */
public class ParamJsonData {

    /* renamed from: a */
    private Map<String, Object> f9365a = new HashMap();

    /* renamed from: b */
    private List<Object> f9366b = new ArrayList();

    /* renamed from: c */
    private JSONObject f9367c;

    /* renamed from: d */
    private boolean f9368d = false;

    /* renamed from: e */
    private String f9369e;

    public ParamJsonData(String str, boolean z) {
        if (!TextUtils.isEmpty(str)) {
            mo25330a(z);
            m15247b(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002e, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r0 = new org.json.JSONArray(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0021, code lost:
        if (r1.f9368d != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        m15248b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        r1.f9366b = m15246a(r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001a */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m15247b(java.lang.String r2) {
        /*
            r1 = this;
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x001a }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x001a }
            r1.f9367c = r0     // Catch:{ JSONException -> 0x001a }
            boolean r0 = r1.f9368d     // Catch:{ JSONException -> 0x001a }
            if (r0 == 0) goto L_0x0011
            org.json.JSONObject r0 = r1.f9367c     // Catch:{ JSONException -> 0x001a }
            r1.m15249b(r0)     // Catch:{ JSONException -> 0x001a }
            goto L_0x0032
        L_0x0011:
            org.json.JSONObject r0 = r1.f9367c     // Catch:{ JSONException -> 0x001a }
            java.util.Map r0 = r1.mo25328a(r0)     // Catch:{ JSONException -> 0x001a }
            r1.f9365a = r0     // Catch:{ JSONException -> 0x001a }
            goto L_0x0032
        L_0x001a:
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x002e }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x002e }
            boolean r2 = r1.f9368d     // Catch:{ JSONException -> 0x002e }
            if (r2 == 0) goto L_0x0027
            r1.m15248b(r0)     // Catch:{ JSONException -> 0x002e }
            goto L_0x0032
        L_0x0027:
            java.util.List r2 = r1.m15246a(r0)     // Catch:{ JSONException -> 0x002e }
            r1.f9366b = r2     // Catch:{ JSONException -> 0x002e }
            goto L_0x0032
        L_0x002e:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0032:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.utils.ParamJsonData.m15247b(java.lang.String):void");
    }

    /* renamed from: a */
    public void mo25330a(boolean z) {
        this.f9368d = z;
    }

    /* renamed from: c */
    public Map<String, Object> mo25333c() {
        return this.f9365a;
    }

    /* renamed from: d */
    public JSONObject mo25334d() {
        return this.f9367c;
    }

    /* renamed from: a */
    public List<Object> mo25327a() {
        return this.f9366b;
    }

    /* renamed from: a */
    public boolean mo25331a(String str) {
        return this.f9365a.containsKey(str);
    }

    /* renamed from: a */
    public Map<String, Object> mo25329a(String... strArr) {
        Map<String, Object> map = this.f9365a;
        int length = strArr.length;
        int i = 0;
        while (true) {
            int i2 = length - 1;
            if (i >= i2) {
                return MapUtil.m15244a(map, strArr[i2]);
            }
            map = MapUtil.m15244a(map, strArr[i]);
            if (map == null) {
                return null;
            }
            i++;
        }
    }

    public ParamJsonData(String str) {
        if (!TextUtils.isEmpty(str)) {
            m15247b(str);
        }
    }

    /* renamed from: a */
    public String mo25325a(String str, String str2) {
        return MapUtil.m15243a(this.f9365a, str, str2);
    }

    /* renamed from: a */
    public String mo25326a(String[] strArr, String str) {
        Map<String, Object> map = this.f9365a;
        int length = strArr.length;
        int i = 0;
        while (true) {
            int i2 = length - 1;
            if (i >= i2) {
                return MapUtil.m15243a(map, strArr[i2], str);
            }
            map = MapUtil.m15244a(map, strArr[i]);
            if (map == null) {
                return str;
            }
            i++;
        }
    }

    /* renamed from: b */
    private void m15249b(JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                this.f9365a.put(next, jSONObject.get(next));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public Integer mo25323a(String str, Integer num) {
        return MapUtil.m15241a(this.f9365a, str, num);
    }

    /* renamed from: a */
    public Long mo25324a(String str, Long l) {
        return MapUtil.m15242a(this.f9365a, str, l);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:11|12|13|14|(3:15|16|26)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x004a */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map<java.lang.String, java.lang.Object> mo25328a(org.json.JSONObject r5) {
        /*
            r4 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.util.Iterator r1 = r5.keys()
        L_0x0009:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0057
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = r4.f9369e
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x0032
            java.lang.String r3 = r4.f9369e
            boolean r3 = r2.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0032
            java.lang.String r3 = r5.getString(r2)     // Catch:{ JSONException -> 0x002d }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x002d }
            goto L_0x0009
        L_0x002d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0009
        L_0x0032:
            org.json.JSONArray r3 = r5.getJSONArray(r2)     // Catch:{ JSONException -> 0x003e }
            java.util.List r3 = r4.m15246a(r3)     // Catch:{ JSONException -> 0x003e }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x003e }
            goto L_0x0009
        L_0x003e:
            org.json.JSONObject r3 = r5.getJSONObject(r2)     // Catch:{ JSONException -> 0x004a }
            java.util.Map r3 = r4.mo25328a(r3)     // Catch:{ JSONException -> 0x004a }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x004a }
            goto L_0x0009
        L_0x004a:
            java.lang.String r3 = r5.getString(r2)     // Catch:{ JSONException -> 0x0052 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x0052 }
            goto L_0x0009
        L_0x0052:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0009
        L_0x0057:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.utils.ParamJsonData.mo25328a(org.json.JSONObject):java.util.Map");
    }

    /* renamed from: b */
    private void m15248b(JSONArray jSONArray) {
        try {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                this.f9366b.add(jSONArray.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: b */
    public String mo25332b() {
        JSONObject jSONObject = this.f9367c;
        return jSONObject != null ? jSONObject.toString() : "";
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:3|4|5|6|(3:7|8|17)|11|1) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0018 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0024 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<java.lang.Object> m15246a(org.json.JSONArray r4) {
        /*
            r3 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
        L_0x0006:
            int r2 = r4.length()
            if (r1 >= r2) goto L_0x0033
            org.json.JSONArray r2 = r4.getJSONArray(r1)     // Catch:{ JSONException -> 0x0018 }
            java.util.List r2 = r3.m15246a(r2)     // Catch:{ JSONException -> 0x0018 }
            r0.add(r2)     // Catch:{ JSONException -> 0x0018 }
            goto L_0x0030
        L_0x0018:
            org.json.JSONObject r2 = r4.getJSONObject(r1)     // Catch:{ JSONException -> 0x0024 }
            java.util.Map r2 = r3.mo25328a(r2)     // Catch:{ JSONException -> 0x0024 }
            r0.add(r2)     // Catch:{ JSONException -> 0x0024 }
            goto L_0x0030
        L_0x0024:
            java.lang.String r2 = r4.getString(r1)     // Catch:{ JSONException -> 0x002c }
            r0.add(r2)     // Catch:{ JSONException -> 0x002c }
            goto L_0x0030
        L_0x002c:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0030:
            int r1 = r1 + 1
            goto L_0x0006
        L_0x0033:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.utils.ParamJsonData.m15246a(org.json.JSONArray):java.util.List");
    }
}
