package com.heimavista.utils;

import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* renamed from: com.heimavista.utils.c */
public class DateUtil {
    /* renamed from: a */
    public static String m15237a(long j, String str) {
        Date date = new Date(j);
        if (TextUtils.isEmpty(str)) {
            str = "yyyy-MM-dd HH:mm:ss";
        }
        return new SimpleDateFormat(str, Locale.getDefault()).format(date);
    }
}
