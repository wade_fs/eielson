package com.heimavista.utils;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.heimavista.utils.e */
public class MapUtil {
    /* renamed from: a */
    public static String m15243a(Map<String, Object> map, String str, String str2) {
        try {
            if (map.containsKey(str)) {
                String valueOf = String.valueOf(map.get(str));
                if (!TextUtils.isEmpty(valueOf)) {
                    return valueOf;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }

    /* renamed from: a */
    public static Integer m15241a(Map<String, Object> map, String str, Integer num) {
        try {
            if (map.containsKey(str)) {
                String valueOf = String.valueOf(map.get(str));
                if (!TextUtils.isEmpty(valueOf)) {
                    return Integer.valueOf(valueOf);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    /* renamed from: a */
    public static Long m15242a(Map<String, Object> map, String str, Long l) {
        try {
            if (map.containsKey(str)) {
                String valueOf = String.valueOf(map.get(str));
                if (!TextUtils.isEmpty(valueOf)) {
                    return Long.valueOf(valueOf);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }

    /* renamed from: a */
    public static Float m15240a(Map<String, Object> map, String str, Float f) {
        try {
            if (map.containsKey(str)) {
                String valueOf = String.valueOf(map.get(str));
                if (!TextUtils.isEmpty(valueOf)) {
                    return Float.valueOf(valueOf);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    /* renamed from: a */
    public static Double m15239a(Map<String, Object> map, String str, Double d) {
        try {
            if (map.containsKey(str)) {
                String valueOf = String.valueOf(map.get(str));
                if (!TextUtils.isEmpty(valueOf)) {
                    return Double.valueOf(valueOf);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    /* renamed from: a */
    public static Map<String, Object> m15244a(Map<String, Object> map, String str) {
        HashMap hashMap = new HashMap();
        try {
            String[] split = str.split("\\.");
            for (int i = 0; i < split.length; i++) {
                if (i == split.length - 1) {
                    return (map == null || !map.containsKey(split[i])) ? hashMap : (Map) map.get(split[i]);
                }
                if (map == null || !map.containsKey(split[i])) {
                    return hashMap;
                }
                map = (Map) map.get(split[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    /* renamed from: a */
    public static JSONObject m15245a(Map<String, Object> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Map) {
                    try {
                        jSONObject.put(str, m15245a((Map) value));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (value instanceof List) {
                    try {
                        jSONObject.put(str, ListUtil.m15238a((List) value));
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    try {
                        jSONObject.put(str, value);
                    } catch (JSONException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        return jSONObject;
    }
}
