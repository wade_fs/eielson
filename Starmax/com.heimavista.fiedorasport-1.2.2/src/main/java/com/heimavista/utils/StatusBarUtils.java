package com.heimavista.utils;

import android.content.res.Resources;
import android.os.Build;
import android.view.Window;

/* renamed from: com.heimavista.utils.h */
public class StatusBarUtils {
    /* renamed from: a */
    public static void m15268a(Window window) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            window.clearFlags(67108864);
            window.getDecorView().setSystemUiVisibility(1280);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(0);
        } else if (i >= 19) {
            window.addFlags(67108864);
            window.addFlags(134217728);
        }
    }

    /* renamed from: a */
    public static int m15267a(Resources resources) {
        int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }
}
