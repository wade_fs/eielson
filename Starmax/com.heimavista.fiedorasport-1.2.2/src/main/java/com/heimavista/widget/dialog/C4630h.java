package com.heimavista.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import java.util.List;

/* renamed from: com.heimavista.widget.dialog.h */
/* compiled from: MenuDialog */
final class C4630h<T> extends BaseAdapter {

    /* renamed from: P */
    private List<T> f9491P;

    /* renamed from: Q */
    private Context f9492Q;

    /* renamed from: com.heimavista.widget.dialog.h$a */
    /* compiled from: MenuDialog */
    final class C4631a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final TextView f9493a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final View f9494b;

        C4631a(C4630h hVar, View view) {
            this.f9493a = (TextView) view.findViewById(R$id.dialog_content);
            this.f9494b = view.findViewById(R$id.dialog_divider);
        }
    }

    /* renamed from: a */
    public void mo25459a(List<T> list) {
        this.f9491P = list;
        notifyDataSetChanged();
    }

    public int getCount() {
        return mo25458a();
    }

    public T getItem(int i) {
        return this.f9491P.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        C4631a aVar;
        if (view == null) {
            view = LayoutInflater.from(this.f9492Q).inflate(R$layout.basic_dialog_item_menu, viewGroup, false);
            aVar = new C4631a(this, view);
            view.setTag(aVar);
        } else {
            aVar = (C4631a) view.getTag();
        }
        m15420a(aVar, i);
        return view;
    }

    private C4630h(Context context) {
        this.f9492Q = context;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo25458a() {
        List<T> list = this.f9491P;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    /* renamed from: a */
    private void m15420a(@NonNull C4631a aVar, int i) {
        aVar.f9493a.setText(this.f9491P.get(i).toString());
        if (i == 0) {
            if (mo25458a() == 1) {
                aVar.f9494b.setVisibility(8);
            } else {
                aVar.f9494b.setVisibility(0);
            }
        } else if (i == mo25458a() - 1) {
            aVar.f9494b.setVisibility(8);
        } else {
            aVar.f9494b.setVisibility(0);
        }
    }
}
