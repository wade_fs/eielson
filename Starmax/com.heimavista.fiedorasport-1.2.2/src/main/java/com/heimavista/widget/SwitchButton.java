package com.heimavista.widget;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.Checkable;
import com.heimavista.switchbutton.R$style;
import com.heimavista.switchbutton.R$styleable;

public class SwitchButton extends View implements Checkable {

    /* renamed from: R0 */
    private static final int f9387R0 = m15310b(58.0f);

    /* renamed from: S0 */
    private static final int f9388S0 = m15310b(36.0f);
    /* access modifiers changed from: private */

    /* renamed from: A0 */
    public C4604e f9389A0;

    /* renamed from: B0 */
    private RectF f9390B0 = new RectF();
    /* access modifiers changed from: private */

    /* renamed from: C0 */
    public int f9391C0 = 0;

    /* renamed from: D0 */
    private ValueAnimator f9392D0;
    /* access modifiers changed from: private */

    /* renamed from: E0 */
    public final ArgbEvaluator f9393E0 = new ArgbEvaluator();
    /* access modifiers changed from: private */

    /* renamed from: F0 */
    public boolean f9394F0;

    /* renamed from: G0 */
    private boolean f9395G0;

    /* renamed from: H0 */
    private boolean f9396H0;

    /* renamed from: I0 */
    private boolean f9397I0;

    /* renamed from: J0 */
    private boolean f9398J0 = false;

    /* renamed from: K0 */
    private boolean f9399K0 = false;

    /* renamed from: L0 */
    private boolean f9400L0 = false;

    /* renamed from: M0 */
    private C4603d f9401M0;

    /* renamed from: N0 */
    private long f9402N0;

    /* renamed from: O0 */
    private Runnable f9403O0 = new C4600a();

    /* renamed from: P */
    private int f9404P;

    /* renamed from: P0 */
    private ValueAnimator.AnimatorUpdateListener f9405P0 = new C4601b();

    /* renamed from: Q */
    private boolean f9406Q = false;

    /* renamed from: Q0 */
    private Animator.AnimatorListener f9407Q0 = new C4602c();

    /* renamed from: R */
    private float f9408R;

    /* renamed from: S */
    private float f9409S;

    /* renamed from: T */
    private int f9410T;

    /* renamed from: U */
    private int f9411U;

    /* renamed from: V */
    private int f9412V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public float f9413W;

    /* renamed from: a0 */
    private float f9414a0;

    /* renamed from: b0 */
    private float f9415b0;

    /* renamed from: c0 */
    private float f9416c0;

    /* renamed from: d0 */
    private float f9417d0;

    /* renamed from: e0 */
    private float f9418e0;

    /* renamed from: f0 */
    private float f9419f0;

    /* renamed from: g0 */
    private float f9420g0;

    /* renamed from: h0 */
    private int f9421h0;
    /* access modifiers changed from: private */

    /* renamed from: i0 */
    public int f9422i0;
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public int f9423j0;

    /* renamed from: k0 */
    private int f9424k0;
    /* access modifiers changed from: private */

    /* renamed from: l0 */
    public int f9425l0;

    /* renamed from: m0 */
    private int f9426m0;

    /* renamed from: n0 */
    private float f9427n0;

    /* renamed from: o0 */
    private int f9428o0;

    /* renamed from: p0 */
    private int f9429p0;

    /* renamed from: q0 */
    private float f9430q0;

    /* renamed from: r0 */
    private float f9431r0;

    /* renamed from: s0 */
    private float f9432s0;

    /* renamed from: t0 */
    private float f9433t0;
    /* access modifiers changed from: private */

    /* renamed from: u0 */
    public float f9434u0;
    /* access modifiers changed from: private */

    /* renamed from: v0 */
    public float f9435v0;

    /* renamed from: w0 */
    private Paint f9436w0;

    /* renamed from: x0 */
    private Paint f9437x0;
    /* access modifiers changed from: private */

    /* renamed from: y0 */
    public C4604e f9438y0;
    /* access modifiers changed from: private */

    /* renamed from: z0 */
    public C4604e f9439z0;

    /* renamed from: com.heimavista.widget.SwitchButton$a */
    class C4600a implements Runnable {
        C4600a() {
        }

        public void run() {
            if (!SwitchButton.this.m15319d()) {
                SwitchButton.this.m15324g();
            }
        }
    }

    /* renamed from: com.heimavista.widget.SwitchButton$b */
    class C4601b implements ValueAnimator.AnimatorUpdateListener {
        C4601b() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            int i = SwitchButton.this.f9391C0;
            if (i == 1 || i == 3 || i == 4) {
                SwitchButton.this.f9438y0.f9445c = ((Integer) SwitchButton.this.f9393E0.evaluate(floatValue, Integer.valueOf(SwitchButton.this.f9439z0.f9445c), Integer.valueOf(SwitchButton.this.f9389A0.f9445c))).intValue();
                SwitchButton.this.f9438y0.f9446d = SwitchButton.this.f9439z0.f9446d + ((SwitchButton.this.f9389A0.f9446d - SwitchButton.this.f9439z0.f9446d) * floatValue);
                if (SwitchButton.this.f9391C0 != 1) {
                    SwitchButton.this.f9438y0.f9443a = SwitchButton.this.f9439z0.f9443a + ((SwitchButton.this.f9389A0.f9443a - SwitchButton.this.f9439z0.f9443a) * floatValue);
                }
                SwitchButton.this.f9438y0.f9444b = ((Integer) SwitchButton.this.f9393E0.evaluate(floatValue, Integer.valueOf(SwitchButton.this.f9439z0.f9444b), Integer.valueOf(SwitchButton.this.f9389A0.f9444b))).intValue();
            } else if (i == 5) {
                SwitchButton.this.f9438y0.f9443a = SwitchButton.this.f9439z0.f9443a + ((SwitchButton.this.f9389A0.f9443a - SwitchButton.this.f9439z0.f9443a) * floatValue);
                float n = (SwitchButton.this.f9438y0.f9443a - SwitchButton.this.f9434u0) / (SwitchButton.this.f9435v0 - SwitchButton.this.f9434u0);
                SwitchButton.this.f9438y0.f9444b = ((Integer) SwitchButton.this.f9393E0.evaluate(n, Integer.valueOf(SwitchButton.this.f9422i0), Integer.valueOf(SwitchButton.this.f9423j0))).intValue();
                SwitchButton.this.f9438y0.f9446d = SwitchButton.this.f9413W * n;
                SwitchButton.this.f9438y0.f9445c = ((Integer) SwitchButton.this.f9393E0.evaluate(n, 0, Integer.valueOf(SwitchButton.this.f9425l0))).intValue();
            }
            SwitchButton.this.postInvalidate();
        }
    }

    /* renamed from: com.heimavista.widget.SwitchButton$c */
    class C4602c implements Animator.AnimatorListener {
        C4602c() {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            int i = SwitchButton.this.f9391C0;
            if (i == 1) {
                int unused = SwitchButton.this.f9391C0 = 2;
                SwitchButton.this.f9438y0.f9445c = 0;
                SwitchButton.this.f9438y0.f9446d = SwitchButton.this.f9413W;
                SwitchButton.this.postInvalidate();
            } else if (i == 2) {
            } else {
                if (i == 3) {
                    int unused2 = SwitchButton.this.f9391C0 = 0;
                    SwitchButton.this.postInvalidate();
                } else if (i == 4) {
                    int unused3 = SwitchButton.this.f9391C0 = 0;
                    SwitchButton.this.postInvalidate();
                    SwitchButton.this.m15301a();
                } else if (i == 5) {
                    SwitchButton switchButton = SwitchButton.this;
                    boolean unused4 = switchButton.f9394F0 = !switchButton.f9394F0;
                    int unused5 = SwitchButton.this.f9391C0 = 0;
                    SwitchButton.this.postInvalidate();
                    SwitchButton.this.m15301a();
                }
            }
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
        }
    }

    /* renamed from: com.heimavista.widget.SwitchButton$d */
    public interface C4603d {
        /* renamed from: a */
        void mo24507a(SwitchButton switchButton, boolean z);
    }

    /* renamed from: com.heimavista.widget.SwitchButton$e */
    private static class C4604e {

        /* renamed from: a */
        float f9443a;

        /* renamed from: b */
        int f9444b;

        /* renamed from: c */
        int f9445c;

        /* renamed from: d */
        float f9446d;

        C4604e() {
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m15340a(C4604e eVar) {
            this.f9443a = eVar.f9443a;
            this.f9444b = eVar.f9444b;
            this.f9445c = eVar.f9445c;
            this.f9446d = eVar.f9446d;
        }
    }

    public SwitchButton(Context context) {
        super(context);
        m15302a(context, (AttributeSet) null);
    }

    private void setCheckedViewState(C4604e eVar) {
        eVar.f9446d = this.f9413W;
        eVar.f9444b = this.f9423j0;
        eVar.f9445c = this.f9425l0;
        eVar.f9443a = this.f9435v0;
    }

    private void setUncheckViewState(C4604e eVar) {
        eVar.f9446d = 0.0f;
        eVar.f9444b = this.f9422i0;
        eVar.f9445c = 0;
        eVar.f9443a = this.f9434u0;
    }

    public boolean isChecked() {
        return this.f9394F0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.f9437x0.setStrokeWidth((float) this.f9424k0);
        this.f9437x0.setStyle(Paint.Style.FILL);
        this.f9437x0.setColor(this.f9421h0);
        Canvas canvas2 = canvas;
        m15305a(canvas2, this.f9416c0, this.f9417d0, this.f9418e0, this.f9419f0, this.f9413W, this.f9437x0);
        this.f9437x0.setStyle(Paint.Style.STROKE);
        this.f9437x0.setColor(this.f9422i0);
        m15305a(canvas2, this.f9416c0, this.f9417d0, this.f9418e0, this.f9419f0, this.f9413W, this.f9437x0);
        if (this.f9397I0) {
            m15314b(canvas);
        }
        float f = this.f9438y0.f9446d * 0.5f;
        this.f9437x0.setStyle(Paint.Style.STROKE);
        this.f9437x0.setColor(this.f9438y0.f9444b);
        this.f9437x0.setStrokeWidth(((float) this.f9424k0) + (f * 2.0f));
        Canvas canvas3 = canvas;
        m15305a(canvas3, this.f9416c0 + f, this.f9417d0 + f, this.f9418e0 - f, this.f9419f0 - f, this.f9413W, this.f9437x0);
        this.f9437x0.setStyle(Paint.Style.FILL);
        this.f9437x0.setStrokeWidth(1.0f);
        float f2 = this.f9416c0;
        float f3 = this.f9417d0;
        float f4 = this.f9413W;
        m15304a(canvas3, f2, f3, f2 + (f4 * 2.0f), f3 + (f4 * 2.0f), 90.0f, 180.0f, this.f9437x0);
        float f5 = this.f9416c0;
        float f6 = this.f9413W;
        float f7 = this.f9417d0;
        canvas.drawRect(f5 + f6, f7, this.f9438y0.f9443a, f7 + (f6 * 2.0f), this.f9437x0);
        if (this.f9397I0) {
            mo25361a(canvas);
        }
        m15303a(canvas, this.f9438y0.f9443a, this.f9420g0);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 0 || mode == Integer.MIN_VALUE) {
            i = View.MeasureSpec.makeMeasureSpec(f9387R0, 1073741824);
        }
        if (mode2 == 0 || mode2 == Integer.MIN_VALUE) {
            i2 = View.MeasureSpec.makeMeasureSpec(f9388S0, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        float max = (float) Math.max(this.f9410T + this.f9411U, this.f9424k0);
        float f = ((float) i2) - max;
        this.f9415b0 = f - max;
        this.f9413W = this.f9415b0 * 0.5f;
        float f2 = this.f9413W;
        this.f9414a0 = f2 - ((float) this.f9424k0);
        this.f9416c0 = max;
        this.f9417d0 = max;
        this.f9418e0 = ((float) i) - max;
        this.f9419f0 = f;
        float f3 = this.f9416c0;
        float f4 = this.f9418e0;
        this.f9420g0 = (this.f9417d0 + this.f9419f0) * 0.5f;
        this.f9434u0 = f3 + f2;
        this.f9435v0 = f4 - f2;
        if (isChecked()) {
            setCheckedViewState(this.f9438y0);
        } else {
            setUncheckViewState(this.f9438y0);
        }
        this.f9399K0 = true;
        postInvalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (!isEnabled()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        float x = motionEvent.getX() - this.f9408R;
        float y = motionEvent.getY() - this.f9409S;
        if (actionMasked == 0) {
            this.f9408R = motionEvent.getX();
            this.f9409S = motionEvent.getY();
            setPressed(true);
            this.f9398J0 = true;
            this.f9402N0 = System.currentTimeMillis();
            removeCallbacks(this.f9403O0);
            postDelayed(this.f9403O0, 100);
        } else if (actionMasked == 1) {
            this.f9398J0 = false;
            removeCallbacks(this.f9403O0);
            if (System.currentTimeMillis() - this.f9402N0 <= 300) {
                toggle();
            } else if (m15317c()) {
                if (Math.max(0.0f, Math.min(1.0f, motionEvent.getX() / ((float) getWidth()))) > 0.5f) {
                    z = true;
                }
                if (z == isChecked()) {
                    m15322f();
                } else {
                    this.f9394F0 = z;
                    m15326h();
                }
            } else if (m15321e()) {
                m15322f();
            }
        } else if (actionMasked == 2) {
            float x2 = motionEvent.getX();
            if (!this.f9406Q && (Math.abs(x) > ((float) (this.f9404P / 2)) || Math.abs(y) > ((float) (this.f9404P / 2)))) {
                if (y == 0.0f || Math.abs(x) > Math.abs(y)) {
                    m15313b();
                    if (m15321e()) {
                        float max = Math.max(0.0f, Math.min(1.0f, x2 / ((float) getWidth())));
                        C4604e eVar = this.f9438y0;
                        float f = this.f9434u0;
                        eVar.f9443a = f + ((this.f9435v0 - f) * max);
                    } else if (m15317c()) {
                        float max2 = Math.max(0.0f, Math.min(1.0f, x2 / ((float) getWidth())));
                        C4604e eVar2 = this.f9438y0;
                        float f2 = this.f9434u0;
                        eVar2.f9443a = f2 + ((this.f9435v0 - f2) * max2);
                        eVar2.f9444b = ((Integer) this.f9393E0.evaluate(max2, Integer.valueOf(this.f9422i0), Integer.valueOf(this.f9423j0))).intValue();
                        postInvalidate();
                    }
                } else if (Math.abs(y) > Math.abs(x)) {
                    return false;
                }
            }
        } else if (actionMasked == 3) {
            this.f9398J0 = false;
            removeCallbacks(this.f9403O0);
            if (m15321e() || m15317c()) {
                m15322f();
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.widget.SwitchButton.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.heimavista.widget.SwitchButton.a(com.heimavista.widget.SwitchButton, int):int
      com.heimavista.widget.SwitchButton.a(android.content.Context, android.util.AttributeSet):void
      com.heimavista.widget.SwitchButton.a(com.heimavista.widget.SwitchButton, boolean):boolean
      com.heimavista.widget.SwitchButton.a(boolean, boolean):void */
    public void setChecked(boolean z) {
        if (z == isChecked()) {
            postInvalidate();
        } else {
            m15306a(this.f9395G0, false);
        }
    }

    public void setEnableEffect(boolean z) {
        this.f9395G0 = z;
    }

    public void setOnCheckedChangeListener(C4603d dVar) {
        this.f9401M0 = dVar;
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
    }

    public final void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
    }

    public final void setPadding(int i, int i2, int i3, int i4) {
        super.setPadding(0, 0, 0, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, float, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void setShadowEffect(boolean z) {
        if (this.f9396H0 != z) {
            this.f9396H0 = z;
            if (this.f9396H0) {
                this.f9436w0.setShadowLayer((float) this.f9410T, 0.0f, (float) this.f9411U, this.f9412V);
            } else {
                this.f9436w0.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
            }
        }
    }

    public void toggle() {
        mo25364a(true);
    }

    /* renamed from: b */
    private void m15314b(Canvas canvas) {
        mo25363a(canvas, this.f9428o0, (float) this.f9429p0, this.f9418e0 - this.f9430q0, this.f9420g0, this.f9431r0, this.f9437x0);
    }

    /* renamed from: c */
    private boolean m15317c() {
        return this.f9391C0 == 2;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public boolean m15319d() {
        return this.f9391C0 != 0;
    }

    /* renamed from: e */
    private boolean m15321e() {
        int i = this.f9391C0;
        return i == 1 || i == 3;
    }

    /* renamed from: f */
    private void m15322f() {
        if (m15317c() || m15321e()) {
            if (this.f9392D0.isRunning()) {
                this.f9392D0.cancel();
            }
            this.f9391C0 = 3;
            this.f9439z0.m15340a(this.f9438y0);
            if (isChecked()) {
                setCheckedViewState(this.f9389A0);
            } else {
                setUncheckViewState(this.f9389A0);
            }
            this.f9392D0.start();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m15324g() {
        if (!m15319d() && this.f9398J0) {
            if (this.f9392D0.isRunning()) {
                this.f9392D0.cancel();
            }
            this.f9391C0 = 1;
            this.f9439z0.m15340a(this.f9438y0);
            this.f9389A0.m15340a(this.f9438y0);
            if (isChecked()) {
                C4604e eVar = this.f9389A0;
                int i = this.f9423j0;
                eVar.f9444b = i;
                eVar.f9443a = this.f9435v0;
                eVar.f9445c = i;
            } else {
                C4604e eVar2 = this.f9389A0;
                eVar2.f9444b = this.f9422i0;
                eVar2.f9443a = this.f9434u0;
                eVar2.f9446d = this.f9413W;
            }
            this.f9392D0.start();
        }
    }

    /* renamed from: h */
    private void m15326h() {
        if (this.f9392D0.isRunning()) {
            this.f9392D0.cancel();
        }
        this.f9391C0 = 4;
        this.f9439z0.m15340a(this.f9438y0);
        if (isChecked()) {
            setCheckedViewState(this.f9389A0);
        } else {
            setUncheckViewState(this.f9389A0);
        }
        this.f9392D0.start();
    }

    /* renamed from: b */
    private void m15313b() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        this.f9398J0 = true;
    }

    /* renamed from: c */
    private static int m15315c(TypedArray typedArray, int i, int i2) {
        return typedArray == null ? i2 : typedArray.getDimensionPixelOffset(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.widget.SwitchButton.a(android.content.res.TypedArray, int, boolean):boolean
     arg types: [android.content.res.TypedArray, int, int]
     candidates:
      com.heimavista.widget.SwitchButton.a(android.content.res.TypedArray, int, float):float
      com.heimavista.widget.SwitchButton.a(android.content.res.TypedArray, int, int):int
      com.heimavista.widget.SwitchButton.a(android.graphics.Canvas, float, float):void
      com.heimavista.widget.SwitchButton.a(android.content.res.TypedArray, int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, float, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* renamed from: a */
    private void m15302a(Context context, AttributeSet attributeSet) {
        this.f9404P = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        ViewConfiguration.getPressedStateDuration();
        ViewConfiguration.getTapTimeout();
        TypedArray obtainStyledAttributes = attributeSet != null ? context.obtainStyledAttributes(attributeSet, R$styleable.SwitchButton, 0, R$style.SwitchButtonStyle) : null;
        this.f9396H0 = m15307a(obtainStyledAttributes, R$styleable.SwitchButton_sb_shadow_effect, true);
        this.f9428o0 = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_uncheckcircle_color, -5592406);
        this.f9429p0 = m15315c(obtainStyledAttributes, R$styleable.SwitchButton_sb_uncheckcircle_width, m15310b(1.5f));
        this.f9430q0 = m15297a(10.0f);
        this.f9431r0 = m15298a(obtainStyledAttributes, R$styleable.SwitchButton_sb_uncheckcircle_radius, m15297a(4.0f));
        this.f9432s0 = m15297a(4.0f);
        this.f9433t0 = m15297a(4.0f);
        this.f9410T = m15315c(obtainStyledAttributes, R$styleable.SwitchButton_sb_shadow_radius, m15310b(2.5f));
        this.f9411U = m15315c(obtainStyledAttributes, R$styleable.SwitchButton_sb_shadow_offset, m15310b(1.5f));
        this.f9412V = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_shadow_color, 855638016);
        this.f9422i0 = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_uncheck_color, -2236963);
        this.f9423j0 = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_checked_color, -11414681);
        this.f9424k0 = m15315c(obtainStyledAttributes, R$styleable.SwitchButton_sb_border_width, m15310b(1.0f));
        this.f9425l0 = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_checkline_color, -1);
        this.f9426m0 = m15315c(obtainStyledAttributes, R$styleable.SwitchButton_sb_checkline_width, m15310b(1.0f));
        this.f9427n0 = m15297a(6.0f);
        int a = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_button_color, -1);
        int b = m15311b(obtainStyledAttributes, R$styleable.SwitchButton_sb_effect_duration, 300);
        this.f9394F0 = m15307a(obtainStyledAttributes, R$styleable.SwitchButton_sb_checked, false);
        this.f9397I0 = m15307a(obtainStyledAttributes, R$styleable.SwitchButton_sb_show_indicator, true);
        this.f9421h0 = m15299a(obtainStyledAttributes, R$styleable.SwitchButton_sb_background, -1);
        this.f9395G0 = m15307a(obtainStyledAttributes, R$styleable.SwitchButton_sb_enable_effect, true);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
        this.f9437x0 = new Paint(1);
        this.f9436w0 = new Paint(1);
        this.f9436w0.setColor(a);
        if (this.f9396H0) {
            this.f9436w0.setShadowLayer((float) this.f9410T, 0.0f, (float) this.f9411U, this.f9412V);
        }
        this.f9438y0 = new C4604e();
        this.f9439z0 = new C4604e();
        this.f9389A0 = new C4604e();
        this.f9392D0 = ValueAnimator.ofFloat(0.0f, 1.0f);
        this.f9392D0.setDuration((long) b);
        this.f9392D0.setRepeatCount(0);
        this.f9392D0.addUpdateListener(this.f9405P0);
        this.f9392D0.addListener(this.f9407Q0);
        super.setClickable(true);
        setPadding(0, 0, 0, 0);
        if (Build.VERSION.SDK_INT >= 11) {
            setLayerType(1, null);
        }
    }

    /* renamed from: b */
    private static int m15310b(float f) {
        return (int) m15297a(f);
    }

    /* renamed from: b */
    private static int m15311b(TypedArray typedArray, int i, int i2) {
        return typedArray == null ? i2 : typedArray.getInt(i, i2);
    }

    public SwitchButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m15302a(context, attributeSet);
    }

    public SwitchButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m15302a(context, attributeSet);
    }

    @TargetApi(21)
    public SwitchButton(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m15302a(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo25361a(Canvas canvas) {
        int i = this.f9438y0.f9445c;
        float f = (float) this.f9426m0;
        float f2 = this.f9416c0;
        float f3 = this.f9413W;
        float f4 = (f2 + f3) - this.f9432s0;
        float f5 = this.f9420g0;
        float f6 = this.f9427n0;
        float f7 = f5 - f6;
        mo25362a(canvas, i, f, f4, f7, (f2 + f3) - this.f9433t0, f5 + f6, this.f9437x0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo25362a(Canvas canvas, int i, float f, float f2, float f3, float f4, float f5, Paint paint) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(i);
        paint.setStrokeWidth(f);
        canvas.drawLine(f2, f3, f4, f5, paint);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo25363a(Canvas canvas, int i, float f, float f2, float f3, float f4, Paint paint) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(i);
        paint.setStrokeWidth(f);
        canvas.drawCircle(f2, f3, f4, paint);
    }

    /* renamed from: a */
    private void m15304a(Canvas canvas, float f, float f2, float f3, float f4, float f5, float f6, Paint paint) {
        if (Build.VERSION.SDK_INT >= 21) {
            canvas.drawArc(f, f2, f3, f4, f5, f6, true, paint);
            return;
        }
        this.f9390B0.set(f, f2, f3, f4);
        canvas.drawArc(this.f9390B0, f5, f6, true, paint);
    }

    /* renamed from: a */
    private void m15305a(Canvas canvas, float f, float f2, float f3, float f4, float f5, Paint paint) {
        if (Build.VERSION.SDK_INT >= 21) {
            canvas.drawRoundRect(f, f2, f3, f4, f5, f5, paint);
            return;
        }
        this.f9390B0.set(f, f2, f3, f4);
        canvas.drawRoundRect(this.f9390B0, f5, f5, paint);
    }

    /* renamed from: a */
    private void m15303a(Canvas canvas, float f, float f2) {
        canvas.drawCircle(f, f2, this.f9414a0, this.f9436w0);
        this.f9437x0.setStyle(Paint.Style.STROKE);
        this.f9437x0.setStrokeWidth(1.0f);
        this.f9437x0.setColor(-2236963);
        canvas.drawCircle(f, f2, this.f9414a0, this.f9437x0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.widget.SwitchButton.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.heimavista.widget.SwitchButton.a(com.heimavista.widget.SwitchButton, int):int
      com.heimavista.widget.SwitchButton.a(android.content.Context, android.util.AttributeSet):void
      com.heimavista.widget.SwitchButton.a(com.heimavista.widget.SwitchButton, boolean):boolean
      com.heimavista.widget.SwitchButton.a(boolean, boolean):void */
    /* renamed from: a */
    public void mo25364a(boolean z) {
        m15306a(z, true);
    }

    /* renamed from: a */
    private void m15306a(boolean z, boolean z2) {
        if (isEnabled()) {
            if (this.f9400L0) {
                throw new RuntimeException("should NOT switch the state in method: [onCheckedChanged]!");
            } else if (!this.f9399K0) {
                this.f9394F0 = !this.f9394F0;
                if (z2) {
                    m15301a();
                }
            } else {
                if (this.f9392D0.isRunning()) {
                    this.f9392D0.cancel();
                }
                if (!this.f9395G0 || !z) {
                    this.f9394F0 = !this.f9394F0;
                    if (isChecked()) {
                        setCheckedViewState(this.f9438y0);
                    } else {
                        setUncheckViewState(this.f9438y0);
                    }
                    postInvalidate();
                    if (z2) {
                        m15301a();
                        return;
                    }
                    return;
                }
                this.f9391C0 = 5;
                this.f9439z0.m15340a(this.f9438y0);
                if (isChecked()) {
                    setUncheckViewState(this.f9389A0);
                } else {
                    setCheckedViewState(this.f9389A0);
                }
                this.f9392D0.start();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m15301a() {
        C4603d dVar = this.f9401M0;
        if (dVar != null) {
            this.f9400L0 = true;
            dVar.mo24507a(this, isChecked());
        }
        this.f9400L0 = false;
    }

    /* renamed from: a */
    private static float m15297a(float f) {
        return TypedValue.applyDimension(1, f, Resources.getSystem().getDisplayMetrics());
    }

    /* renamed from: a */
    private static float m15298a(TypedArray typedArray, int i, float f) {
        return typedArray == null ? f : typedArray.getDimension(i, f);
    }

    /* renamed from: a */
    private static int m15299a(TypedArray typedArray, int i, int i2) {
        return typedArray == null ? i2 : typedArray.getColor(i, i2);
    }

    /* renamed from: a */
    private static boolean m15307a(TypedArray typedArray, int i, boolean z) {
        return typedArray == null ? z : typedArray.getBoolean(i, z);
    }
}
