package com.heimavista.widget.dialog;

public final class R$attr {
    public static final int actionBarDivider = 2130968577;
    public static final int actionBarItemBackground = 2130968578;
    public static final int actionBarPopupTheme = 2130968579;
    public static final int actionBarSize = 2130968580;
    public static final int actionBarSplitStyle = 2130968581;
    public static final int actionBarStyle = 2130968582;
    public static final int actionBarTabBarStyle = 2130968583;
    public static final int actionBarTabStyle = 2130968584;
    public static final int actionBarTabTextStyle = 2130968585;
    public static final int actionBarTheme = 2130968586;
    public static final int actionBarWidgetTheme = 2130968587;
    public static final int actionButtonStyle = 2130968588;
    public static final int actionDropDownStyle = 2130968589;
    public static final int actionLayout = 2130968590;
    public static final int actionMenuTextAppearance = 2130968591;
    public static final int actionMenuTextColor = 2130968592;
    public static final int actionModeBackground = 2130968593;
    public static final int actionModeCloseButtonStyle = 2130968594;
    public static final int actionModeCloseDrawable = 2130968595;
    public static final int actionModeCopyDrawable = 2130968596;
    public static final int actionModeCutDrawable = 2130968597;
    public static final int actionModeFindDrawable = 2130968598;
    public static final int actionModePasteDrawable = 2130968599;
    public static final int actionModePopupWindowStyle = 2130968600;
    public static final int actionModeSelectAllDrawable = 2130968601;
    public static final int actionModeShareDrawable = 2130968602;
    public static final int actionModeSplitBackground = 2130968603;
    public static final int actionModeStyle = 2130968604;
    public static final int actionModeWebSearchDrawable = 2130968605;
    public static final int actionOverflowButtonStyle = 2130968606;
    public static final int actionOverflowMenuStyle = 2130968607;
    public static final int actionProviderClass = 2130968608;
    public static final int actionViewClass = 2130968610;
    public static final int activityChooserViewStyle = 2130968611;
    public static final int alertDialogButtonGroupStyle = 2130968614;
    public static final int alertDialogCenterButtons = 2130968615;
    public static final int alertDialogStyle = 2130968616;
    public static final int alertDialogTheme = 2130968617;
    public static final int allowStacking = 2130968618;
    public static final int alpha = 2130968619;
    public static final int alphabeticModifiers = 2130968620;
    public static final int arrowHeadLength = 2130968628;
    public static final int arrowShaftLength = 2130968629;
    public static final int autoCompleteTextViewStyle = 2130968631;
    public static final int autoSizeMaxTextSize = 2130968632;
    public static final int autoSizeMinTextSize = 2130968633;
    public static final int autoSizePresetSizes = 2130968634;
    public static final int autoSizeStepGranularity = 2130968635;
    public static final int autoSizeTextType = 2130968636;
    public static final int background = 2130968639;
    public static final int backgroundSplit = 2130968646;
    public static final int backgroundStacked = 2130968647;
    public static final int backgroundTint = 2130968648;
    public static final int backgroundTintMode = 2130968649;
    public static final int barLength = 2130968653;
    public static final int borderlessButtonStyle = 2130968674;
    public static final int buttonBarButtonStyle = 2130968695;
    public static final int buttonBarNegativeButtonStyle = 2130968696;
    public static final int buttonBarNeutralButtonStyle = 2130968697;
    public static final int buttonBarPositiveButtonStyle = 2130968698;
    public static final int buttonBarStyle = 2130968699;
    public static final int buttonCompat = 2130968700;
    public static final int buttonGravity = 2130968701;
    public static final int buttonIconDimen = 2130968702;
    public static final int buttonPanelSideLayout = 2130968703;
    public static final int buttonStyle = 2130968705;
    public static final int buttonStyleSmall = 2130968706;
    public static final int buttonTint = 2130968707;
    public static final int buttonTintMode = 2130968708;
    public static final int cardBackgroundColor = 2130968716;
    public static final int cardCornerRadius = 2130968717;
    public static final int cardElevation = 2130968718;
    public static final int cardMaxElevation = 2130968720;
    public static final int cardPreventCornerOverlap = 2130968721;
    public static final int cardUseCompatPadding = 2130968722;
    public static final int cardViewStyle = 2130968723;
    public static final int checkboxStyle = 2130968725;
    public static final int checkedTextViewStyle = 2130968732;
    public static final int closeIcon = 2130968756;
    public static final int closeItemLayout = 2130968763;
    public static final int collapseContentDescription = 2130968764;
    public static final int collapseIcon = 2130968765;
    public static final int color = 2130968768;
    public static final int colorAccent = 2130968769;
    public static final int colorBackgroundFloating = 2130968770;
    public static final int colorButtonNormal = 2130968771;
    public static final int colorControlActivated = 2130968772;
    public static final int colorControlHighlight = 2130968773;
    public static final int colorControlNormal = 2130968774;
    public static final int colorError = 2130968775;
    public static final int colorPrimary = 2130968782;
    public static final int colorPrimaryDark = 2130968783;
    public static final int colorSwitchThumbNormal = 2130968790;
    public static final int commitIcon = 2130968803;
    public static final int contentDescription = 2130968810;
    public static final int contentInsetEnd = 2130968811;
    public static final int contentInsetEndWithActions = 2130968812;
    public static final int contentInsetLeft = 2130968813;
    public static final int contentInsetRight = 2130968814;
    public static final int contentInsetStart = 2130968815;
    public static final int contentInsetStartWithNavigation = 2130968816;
    public static final int contentPadding = 2130968817;
    public static final int contentPaddingBottom = 2130968818;
    public static final int contentPaddingLeft = 2130968819;
    public static final int contentPaddingRight = 2130968820;
    public static final int contentPaddingTop = 2130968821;
    public static final int controlBackground = 2130968824;
    public static final int customNavigationLayout = 2130968853;
    public static final int defaultQueryHint = 2130968861;
    public static final int dialogCornerRadius = 2130968867;
    public static final int dialogPreferredPadding = 2130968868;
    public static final int dialogTheme = 2130968869;
    public static final int displayOptions = 2130968872;
    public static final int divider = 2130968874;
    public static final int dividerHorizontal = 2130968875;
    public static final int dividerPadding = 2130968876;
    public static final int dividerVertical = 2130968877;
    public static final int drawableBottomCompat = 2130968887;
    public static final int drawableEndCompat = 2130968888;
    public static final int drawableLeftCompat = 2130968889;
    public static final int drawableRightCompat = 2130968890;
    public static final int drawableSize = 2130968891;
    public static final int drawableStartCompat = 2130968892;
    public static final int drawableTint = 2130968893;
    public static final int drawableTintMode = 2130968894;
    public static final int drawableTopCompat = 2130968895;
    public static final int drawerArrowStyle = 2130968896;
    public static final int dropDownListViewStyle = 2130968897;
    public static final int dropdownListPreferredItemHeight = 2130968898;
    public static final int editTextBackground = 2130968905;
    public static final int editTextColor = 2130968906;
    public static final int editTextStyle = 2130968907;
    public static final int elevation = 2130968908;
    public static final int expandActivityOverflowButtonDrawable = 2130968927;
    public static final int firstBaselineToTopHeight = 2130968951;
    public static final int font = 2130968972;
    public static final int fontFamily = 2130968973;
    public static final int fontProviderAuthority = 2130968974;
    public static final int fontProviderCerts = 2130968975;
    public static final int fontProviderFetchStrategy = 2130968976;
    public static final int fontProviderFetchTimeout = 2130968977;
    public static final int fontProviderPackage = 2130968978;
    public static final int fontProviderQuery = 2130968979;
    public static final int fontStyle = 2130968980;
    public static final int fontVariationSettings = 2130968981;
    public static final int fontWeight = 2130968982;
    public static final int gapBetweenBars = 2130968985;
    public static final int goIcon = 2130968986;
    public static final int height = 2130968990;
    public static final int hideOnContentScroll = 2130968996;
    public static final int homeAsUpIndicator = 2130969004;
    public static final int homeLayout = 2130969005;
    public static final int icon = 2130969008;
    public static final int iconTint = 2130969014;
    public static final int iconTintMode = 2130969015;
    public static final int iconifiedByDefault = 2130969016;
    public static final int imageButtonStyle = 2130969019;
    public static final int indeterminateProgressStyle = 2130969020;
    public static final int initialActivityCount = 2130969021;
    public static final int isLightTheme = 2130969027;
    public static final int itemPadding = 2130969041;
    public static final int lastBaselineToBottomHeight = 2130969066;
    public static final int layout = 2130969071;
    public static final int lineHeight = 2130969145;
    public static final int listChoiceBackgroundIndicator = 2130969147;
    public static final int listChoiceIndicatorMultipleAnimated = 2130969148;
    public static final int listChoiceIndicatorSingleAnimated = 2130969149;
    public static final int listDividerAlertDialog = 2130969150;
    public static final int listItemLayout = 2130969151;
    public static final int listLayout = 2130969152;
    public static final int listMenuViewStyle = 2130969153;
    public static final int listPopupWindowStyle = 2130969154;
    public static final int listPreferredItemHeight = 2130969155;
    public static final int listPreferredItemHeightLarge = 2130969156;
    public static final int listPreferredItemHeightSmall = 2130969157;
    public static final int listPreferredItemPaddingEnd = 2130969158;
    public static final int listPreferredItemPaddingLeft = 2130969159;
    public static final int listPreferredItemPaddingRight = 2130969160;
    public static final int listPreferredItemPaddingStart = 2130969161;
    public static final int logo = 2130969170;
    public static final int logoDescription = 2130969171;
    public static final int maxButtonHeight = 2130969195;
    public static final int measureWithLargestChild = 2130969202;
    public static final int menu = 2130969203;
    public static final int multiChoiceItemLayout = 2130969228;
    public static final int navigationContentDescription = 2130969235;
    public static final int navigationIcon = 2130969236;
    public static final int navigationMode = 2130969237;
    public static final int numericModifiers = 2130969240;
    public static final int overlapAnchor = 2130969247;
    public static final int paddingBottomNoButtons = 2130969249;
    public static final int paddingEnd = 2130969251;
    public static final int paddingStart = 2130969254;
    public static final int paddingTopNoTitle = 2130969255;
    public static final int panelBackground = 2130969256;
    public static final int panelMenuListTheme = 2130969257;
    public static final int panelMenuListWidth = 2130969258;
    public static final int popupMenuStyle = 2130969282;
    public static final int popupTheme = 2130969283;
    public static final int popupWindowStyle = 2130969284;
    public static final int preserveIconSpacing = 2130969288;
    public static final int progressBarPadding = 2130969291;
    public static final int progressBarStyle = 2130969292;
    public static final int queryBackground = 2130969293;
    public static final int queryHint = 2130969294;
    public static final int radioButtonStyle = 2130969295;
    public static final int ratingBarStyle = 2130969298;
    public static final int ratingBarStyleIndicator = 2130969299;
    public static final int ratingBarStyleSmall = 2130969300;
    public static final int searchHintIcon = 2130969346;
    public static final int searchIcon = 2130969347;
    public static final int searchViewStyle = 2130969348;
    public static final int seekBarStyle = 2130969349;
    public static final int selectableItemBackground = 2130969350;
    public static final int selectableItemBackgroundBorderless = 2130969351;
    public static final int showAsAction = 2130969358;
    public static final int showDividers = 2130969359;
    public static final int showText = 2130969362;
    public static final int showTitle = 2130969363;
    public static final int singleChoiceItemLayout = 2130969369;
    public static final int spinBars = 2130969378;
    public static final int spinnerDropDownItemStyle = 2130969379;
    public static final int spinnerStyle = 2130969380;
    public static final int splitTrack = 2130969381;
    public static final int srcCompat = 2130969383;
    public static final int state_above_anchor = 2130969457;
    public static final int subMenuArrow = 2130969468;
    public static final int submitBackground = 2130969469;
    public static final int subtitle = 2130969470;
    public static final int subtitleTextAppearance = 2130969471;
    public static final int subtitleTextColor = 2130969472;
    public static final int subtitleTextStyle = 2130969473;
    public static final int suggestionRowLayout = 2130969477;
    public static final int switchMinWidth = 2130969479;
    public static final int switchPadding = 2130969480;
    public static final int switchStyle = 2130969481;
    public static final int switchTextAppearance = 2130969482;
    public static final int textAllCaps = 2130969514;
    public static final int textAppearanceLargePopupMenu = 2130969525;
    public static final int textAppearanceListItem = 2130969527;
    public static final int textAppearanceListItemSecondary = 2130969528;
    public static final int textAppearanceListItemSmall = 2130969529;
    public static final int textAppearancePopupMenuHeader = 2130969531;
    public static final int textAppearanceSearchResultSubtitle = 2130969532;
    public static final int textAppearanceSearchResultTitle = 2130969533;
    public static final int textAppearanceSmallPopupMenu = 2130969534;
    public static final int textColorAlertDialogListItem = 2130969538;
    public static final int textColorSearchUrl = 2130969539;
    public static final int textLocale = 2130969543;
    public static final int theme = 2130969547;
    public static final int thickness = 2130969549;
    public static final int thumbTextPadding = 2130969553;
    public static final int thumbTint = 2130969554;
    public static final int thumbTintMode = 2130969555;
    public static final int tickMark = 2130969559;
    public static final int tickMarkTint = 2130969560;
    public static final int tickMarkTintMode = 2130969561;
    public static final int tint = 2130969562;
    public static final int tintMode = 2130969563;
    public static final int title = 2130969564;
    public static final int titleMargin = 2130969566;
    public static final int titleMarginBottom = 2130969567;
    public static final int titleMarginEnd = 2130969568;
    public static final int titleMarginStart = 2130969569;
    public static final int titleMarginTop = 2130969570;
    public static final int titleMargins = 2130969571;
    public static final int titleTextAppearance = 2130969572;
    public static final int titleTextColor = 2130969573;
    public static final int titleTextStyle = 2130969574;
    public static final int toolbarNavigationButtonStyle = 2130969614;
    public static final int toolbarStyle = 2130969615;
    public static final int tooltipForegroundColor = 2130969616;
    public static final int tooltipFrameBackground = 2130969617;
    public static final int tooltipText = 2130969619;
    public static final int track = 2130969627;
    public static final int trackTint = 2130969632;
    public static final int trackTintMode = 2130969633;
    public static final int ttcIndex = 2130969641;
    public static final int viewInflaterClass = 2130969658;
    public static final int voiceIcon = 2130969660;
    public static final int windowActionBar = 2130969667;
    public static final int windowActionBarOverlay = 2130969668;
    public static final int windowActionModeOverlay = 2130969669;
    public static final int windowFixedHeightMajor = 2130969670;
    public static final int windowFixedHeightMinor = 2130969671;
    public static final int windowFixedWidthMajor = 2130969672;
    public static final int windowFixedWidthMinor = 2130969673;
    public static final int windowMinWidthMajor = 2130969674;
    public static final int windowMinWidthMinor = 2130969675;
    public static final int windowNoTitle = 2130969676;

    private R$attr() {
    }
}
