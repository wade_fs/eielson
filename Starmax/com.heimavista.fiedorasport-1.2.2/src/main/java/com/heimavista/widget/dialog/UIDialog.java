package com.heimavista.widget.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.UIDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.widget.dialog.k */
public class UIDialog<B extends UIDialog> extends BaseDialog.C4610b<B> {

    /* renamed from: g0 */
    private boolean f9497g0 = true;

    /* renamed from: h0 */
    private final ViewGroup f9498h0;

    /* renamed from: i0 */
    private final TextView f9499i0;

    /* renamed from: j0 */
    private final View f9500j0;

    /* renamed from: k0 */
    private final View f9501k0;

    /* renamed from: l0 */
    private final View f9502l0;

    public UIDialog(Context context) {
        super(context);
        mo25419c(R$layout.basic_dialog_ui);
        mo25416b(AnimAction.f7973c);
        mo25422d(17);
        mo25427g((context.getResources().getDisplayMetrics().widthPixels * 7) / 10);
        this.f9498h0 = (ViewGroup) findViewById(R$id.dialog_container);
        this.f9499i0 = (TextView) findViewById(R$id.dialog_title);
        this.f9500j0 = findViewById(R$id.dialog_negative_btn);
        this.f9501k0 = findViewById(R$id.dialog_divider);
        this.f9502l0 = findViewById(R$id.dialog_positive_btn);
        mo24144a(R$id.dialog_negative_btn, R$id.dialog_positive_btn);
    }

    /* renamed from: a */
    public B mo25466a(CharSequence charSequence) {
        boolean isEmpty = TextUtils.isEmpty(charSequence);
        View view = this.f9500j0;
        int i = 8;
        if (view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setText(charSequence);
            }
            this.f9500j0.setVisibility(isEmpty ? 8 : 0);
        }
        View view2 = this.f9501k0;
        if (view2 != null) {
            if (!isEmpty) {
                i = 0;
            }
            view2.setVisibility(i);
        }
        return this;
    }

    /* renamed from: b */
    public B mo25467b(View view) {
        this.f9498h0.addView(view, 1);
        return this;
    }

    /* renamed from: c */
    public B mo25469c(CharSequence charSequence) {
        this.f9499i0.setText(charSequence);
        this.f9499i0.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        return this;
    }

    /* renamed from: h */
    public B mo25470h(@StringRes int i) {
        mo25466a(mo24148a(i));
        return this;
    }

    /* renamed from: i */
    public B mo25473i(@StringRes int i) {
        mo25468b(mo24148a(i));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: j */
    public B mo25474j(@LayoutRes int i) {
        mo25467b(LayoutInflater.from(getContext()).inflate(i, this.f9498h0, false));
        return this;
    }

    /* renamed from: k */
    public B mo25475k(@StringRes int i) {
        mo25469c(mo24148a(i));
        return this;
    }

    /* renamed from: b */
    public B mo25468b(CharSequence charSequence) {
        View view = this.f9502l0;
        if (view instanceof TextView) {
            ((TextView) view).setText(charSequence);
        }
        return this;
    }

    /* renamed from: h */
    public void mo25471h() {
        if (this.f9497g0) {
            mo25420c();
        }
    }

    /* renamed from: i */
    public B mo25472i() {
        View view = this.f9500j0;
        if (view != null) {
            view.setVisibility(8);
        }
        View view2 = this.f9501k0;
        if (view2 != null) {
            view2.setVisibility(8);
        }
        return this;
    }
}
