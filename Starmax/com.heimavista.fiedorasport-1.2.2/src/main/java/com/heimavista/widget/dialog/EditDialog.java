package com.heimavista.widget.dialog;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import androidx.annotation.StringRes;
import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.widget.dialog.c */
public final class EditDialog extends UIDialog<EditDialog> implements BaseDialog.C4620l, BaseDialog.C4618j {

    /* renamed from: m0 */
    private C4627d f9484m0;

    /* renamed from: n0 */
    private final EditText f9485n0 = ((EditText) findViewById(R$id.dialog_edit));

    public EditDialog(Context context) {
        super(context);
        mo25474j(R$layout.basic_edit_dialog);
        mo25412a((BaseDialog.C4620l) this);
        mo25411a((BaseDialog.C4618j) this);
    }

    /* renamed from: a */
    public EditDialog mo25448a(C4627d dVar) {
        this.f9484m0 = dVar;
        return this;
    }

    /* renamed from: b */
    public void mo25431b(BaseDialog baseDialog) {
        mo25415a(new C4626b(this), 500);
    }

    /* renamed from: d */
    public EditDialog mo25449d(CharSequence charSequence) {
        this.f9485n0.setText(charSequence);
        int length = this.f9485n0.getText().toString().length();
        if (length > 0) {
            this.f9485n0.requestFocus();
            this.f9485n0.setSelection(length);
        }
        return this;
    }

    /* renamed from: e */
    public EditDialog mo25450e(CharSequence charSequence) {
        this.f9485n0.setHint(charSequence);
        return this;
    }

    /* renamed from: j */
    public /* synthetic */ void mo25451j() {
        ((InputMethodManager) mo24147a(InputMethodManager.class)).showSoftInput(this.f9485n0, 0);
    }

    /* renamed from: l */
    public EditDialog mo25452l(@StringRes int i) {
        mo25450e(mo24148a(i));
        return this;
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R$id.dialog_positive_btn) {
            mo25471h();
            C4627d dVar = this.f9484m0;
            if (dVar != null) {
                dVar.mo24478a(this.f9485n0.getText().toString());
            }
        } else if (id == R$id.dialog_negative_btn) {
            mo25471h();
        }
    }

    /* renamed from: a */
    public void mo22806a(BaseDialog baseDialog) {
        ((InputMethodManager) mo24147a(InputMethodManager.class)).hideSoftInputFromWindow(this.f9485n0.getWindowToken(), 0);
    }
}
