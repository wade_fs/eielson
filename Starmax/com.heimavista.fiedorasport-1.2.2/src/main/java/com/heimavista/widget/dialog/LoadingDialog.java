package com.heimavista.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.heimavista.widget.dialog.BaseDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.widget.dialog.e */
public final class LoadingDialog {

    /* renamed from: com.heimavista.widget.dialog.e$a */
    /* compiled from: LoadingDialog */
    public interface C4628a {
        void onDismiss(DialogInterface dialogInterface);
    }

    /* renamed from: a */
    public static BaseDialog.C4610b m15413a(Context context, @NonNull CharSequence charSequence) {
        BaseDialog.C4610b bVar = new BaseDialog.C4610b(context);
        bVar.mo25419c(R$layout.basic_loading_dialog);
        bVar.mo25416b(AnimAction.f7978h);
        bVar.mo25425f(R$style.LoadingDialogStyle);
        bVar.mo25409a(R$id.dialog_content, charSequence);
        bVar.mo25408a(R$id.dialog_content, TextUtils.isEmpty(charSequence) ? 8 : 0);
        return bVar.mo25422d(17);
    }
}
