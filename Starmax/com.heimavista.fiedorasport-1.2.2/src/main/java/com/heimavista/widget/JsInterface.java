package com.heimavista.widget;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import androidx.fragment.app.Fragment;

/* renamed from: com.heimavista.widget.e */
public class JsInterface {

    /* renamed from: a */
    private WebView f9503a;

    /* renamed from: b */
    private Activity f9504b;

    /* renamed from: c */
    private Fragment f9505c;

    /* renamed from: a */
    public void mo25478a(WebView webView) {
        this.f9503a = webView;
    }

    /* renamed from: b */
    public WebView mo25480b() {
        return this.f9503a;
    }

    @JavascriptInterface
    public void close() {
        this.f9504b.finish();
    }

    /* renamed from: a */
    public Activity mo25476a() {
        Activity activity = this.f9504b;
        return activity != null ? activity : this.f9505c.getActivity();
    }

    /* renamed from: a */
    public void mo25477a(Activity activity) {
        this.f9504b = activity;
    }

    /* renamed from: a */
    public void mo25479a(Fragment fragment) {
        this.f9505c = fragment;
    }
}
