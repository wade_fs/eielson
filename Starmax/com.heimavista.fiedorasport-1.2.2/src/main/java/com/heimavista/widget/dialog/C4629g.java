package com.heimavista.widget.dialog;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.heimavista.widget.dialog.BaseDialog;
import java.util.Arrays;
import java.util.List;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.widget.dialog.g */
/* compiled from: MenuDialog */
public final class C4629g extends BaseDialog.C4610b<C4629g> implements View.OnClickListener, AdapterView.OnItemClickListener {

    /* renamed from: g0 */
    private C4632i f9486g0;

    /* renamed from: h0 */
    private boolean f9487h0 = true;

    /* renamed from: i0 */
    private final ListView f9488i0;

    /* renamed from: j0 */
    private final C4630h f9489j0;

    /* renamed from: k0 */
    private final TextView f9490k0;

    public C4629g(FragmentActivity fragmentActivity) {
        super((Activity) fragmentActivity);
        mo25419c(R$layout.basic_menu_dialog);
        mo25416b(AnimAction.f7975e);
        mo25427g((fragmentActivity.getResources().getDisplayMetrics().widthPixels * 4) / 5);
        this.f9488i0 = (ListView) findViewById(R$id.dialog_list);
        this.f9490k0 = (TextView) findViewById(R$id.dialog_negative_btn);
        this.f9489j0 = new C4630h(getContext());
        this.f9488i0.setOnItemClickListener(this);
        this.f9488i0.setAdapter((ListAdapter) this.f9489j0);
        mo24144a(R$id.dialog_negative_btn);
    }

    /* renamed from: a */
    public C4629g mo25456a(String... strArr) {
        mo25455a(Arrays.asList(strArr));
        return this;
    }

    public void onClick(View view) {
        C4632i iVar;
        if (this.f9487h0) {
            mo25420c();
        }
        if (view == this.f9490k0 && (iVar = this.f9486g0) != null) {
            iVar.mo24375a(mo25424e());
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.f9487h0) {
            mo25420c();
        }
        C4632i iVar = this.f9486g0;
        if (iVar != null) {
            iVar.mo24376a(mo25424e(), i, this.f9489j0.getItem(i));
        }
    }

    /* renamed from: a */
    public C4629g mo25455a(List list) {
        this.f9489j0.mo25459a(list);
        return this;
    }

    /* renamed from: d */
    public C4629g mo25422d(int i) {
        if (i == 16 || i == 17) {
            mo25454a((CharSequence) null);
            mo25416b(AnimAction.f7972b);
        }
        super.mo25422d(i);
        return this;
    }

    /* renamed from: a */
    public C4629g mo25454a(CharSequence charSequence) {
        this.f9490k0.setText(charSequence);
        return this;
    }

    /* renamed from: a */
    public C4629g mo25453a(C4632i iVar) {
        this.f9486g0 = iVar;
        return this;
    }
}
