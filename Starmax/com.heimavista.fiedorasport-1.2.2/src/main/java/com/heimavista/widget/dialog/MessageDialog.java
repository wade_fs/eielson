package com.heimavista.widget.dialog;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.widget.dialog.j */
public final class MessageDialog extends UIDialog<MessageDialog> {

    /* renamed from: m0 */
    private BaseDialog.C4621m f9495m0;

    /* renamed from: n0 */
    private final TextView f9496n0 = ((TextView) findViewById(R$id.dialog_content));

    public MessageDialog(Context context) {
        super(context);
        mo25474j(R$layout.basic_message_dialog);
    }

    /* renamed from: a */
    public MessageDialog mo25464a(BaseDialog.C4621m mVar) {
        this.f9495m0 = mVar;
        return this;
    }

    /* renamed from: b */
    public BaseDialog mo25418b() {
        if (!"".equals(this.f9496n0.getText().toString())) {
            return super.mo25418b();
        }
        throw new IllegalArgumentException("Dialog message not null");
    }

    /* renamed from: d */
    public MessageDialog mo25465d(CharSequence charSequence) {
        this.f9496n0.setText(charSequence);
        return this;
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R$id.dialog_positive_btn) {
            mo25471h();
            BaseDialog.C4621m mVar = this.f9495m0;
            if (mVar != null) {
                mVar.mo23034a(true);
            }
        } else if (id == R$id.dialog_negative_btn) {
            mo25471h();
            BaseDialog.C4621m mVar2 = this.f9495m0;
            if (mVar2 != null) {
                mVar2.mo23034a(false);
            }
        }
    }
}
