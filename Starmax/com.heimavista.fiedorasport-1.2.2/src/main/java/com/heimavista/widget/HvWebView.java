package com.heimavista.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ZoomButtonsController;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.heimavista.utils.StatusBarUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class HvWebView extends WebView {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Context f9370P;

    /* renamed from: Q */
    private boolean f9371Q = false;

    /* renamed from: R */
    private boolean f9372R;

    /* renamed from: S */
    private ViewPager f9373S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public ValueCallback<Uri> f9374T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public ValueCallback<Uri[]> f9375U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public C4599d f9376V;

    /* renamed from: W */
    WebChromeClient f9377W = new C4595a();

    /* renamed from: a0 */
    private WebViewClient f9378a0 = new C4597b();
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public WebFileChooseActivity.C4594a f9379b0 = new C4598c();

    /* renamed from: com.heimavista.widget.HvWebView$a */
    class C4595a extends WebChromeClient {

        /* renamed from: com.heimavista.widget.HvWebView$a$a */
        class C4596a implements PermissionUtils.C0922e {

            /* renamed from: a */
            final /* synthetic */ ValueCallback f9383a;

            C4596a(ValueCallback valueCallback) {
                this.f9383a = valueCallback;
            }

            /* renamed from: a */
            public void mo9866a() {
                ValueCallback unused = HvWebView.this.f9375U = this.f9383a;
                WebFileChooseActivity.m15285a(HvWebView.this.f9370P, HvWebView.this.f9379b0);
            }

            /* renamed from: b */
            public void mo9867b() {
            }
        }

        C4595a() {
        }

        public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
            quotaUpdater.updateQuota(j2 * 2);
        }

        public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
            callback.invoke(str, true, false);
            super.onGeolocationPermissionsShowPrompt(str, callback);
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HvWebView.this.f9370P);
            builder.setTitle("Alert");
            builder.setMessage(str2);
            builder.setPositiveButton(17039370, new C4606b(jsResult));
            builder.setCancelable(false);
            builder.create();
            builder.show();
            return true;
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HvWebView.this.f9370P);
            builder.setTitle("Confirm");
            builder.setMessage(str2);
            builder.setPositiveButton(17039370, new C4605a(jsResult));
            builder.setNeutralButton(17039360, new C4607c(jsResult));
            builder.setCancelable(false);
            builder.create();
            builder.show();
            return true;
        }

        public void onProgressChanged(WebView webView, int i) {
            super.onProgressChanged(webView, i);
            if (HvWebView.this.f9376V != null) {
                HvWebView.this.f9376V.mo24581a(i);
            }
        }

        public void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
            quotaUpdater.updateQuota(j * 2);
        }

        public void onReceivedTitle(WebView webView, String str) {
            super.onReceivedTitle(webView, str);
            if (HvWebView.this.f9376V != null) {
                HvWebView.this.f9376V.mo24582a(str);
            }
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            LogUtils.m1131b("view:" + view + ",callback:" + customViewCallback);
        }

        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            if (PermissionUtils.m1193a("android.permission-group.CAMERA")) {
                ValueCallback unused = HvWebView.this.f9375U = valueCallback;
                WebFileChooseActivity.m15285a(HvWebView.this.f9370P, HvWebView.this.f9379b0);
                return true;
            }
            PermissionUtils b = PermissionUtils.m1196b("android.permission-group.CAMERA");
            b.mo9858a(new C4596a(valueCallback));
            b.mo9859a();
            return true;
        }
    }

    /* renamed from: com.heimavista.widget.HvWebView$b */
    class C4597b extends WebViewClient {
        C4597b() {
        }

        public void onPageFinished(WebView webView, String str) {
            if (HvWebView.this.f9376V != null) {
                HvWebView.this.f9376V.onFinished();
            }
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            if (HvWebView.this.f9376V != null) {
                HvWebView.this.f9376V.onStart();
            }
            super.onPageStarted(webView, str, bitmap);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!str.startsWith("scheme:")) {
                return false;
            }
            HvWebView.this.f9370P.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return false;
        }
    }

    /* renamed from: com.heimavista.widget.HvWebView$c */
    class C4598c implements WebFileChooseActivity.C4594a {
        C4598c() {
        }

        /* renamed from: a */
        public void mo25348a(Uri uri) {
            if (HvWebView.this.f9374T != null || HvWebView.this.f9375U != null) {
                Uri[] uriArr = null;
                if (HvWebView.this.f9374T != null) {
                    HvWebView.this.f9374T.onReceiveValue(uri);
                    ValueCallback unused = HvWebView.this.f9374T = null;
                }
                if (HvWebView.this.f9375U != null) {
                    ValueCallback e = HvWebView.this.f9375U;
                    if (uri != null) {
                        uriArr = new Uri[]{uri};
                    }
                    e.onReceiveValue(uriArr);
                }
            }
        }
    }

    /* renamed from: com.heimavista.widget.HvWebView$d */
    public interface C4599d {
        /* renamed from: a */
        void mo24581a(int i);

        /* renamed from: a */
        void mo24582a(String str);

        void onFinished();

        void onStart();
    }

    public HvWebView(Context context) {
        super(context);
        this.f9370P = context;
        m15276d();
        m15274c();
    }

    private ViewPager getViewPagerParent() {
        ViewParent parent = getParent();
        while (!(parent instanceof ViewPager)) {
            parent = parent.getParent();
            if (parent == null) {
                return null;
            }
        }
        return (ViewPager) parent;
    }

    public int getScrollHeight() {
        return computeVerticalScrollRange();
    }

    public int getScrollWidth() {
        return computeHorizontalScrollRange();
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.onOverScrolled(i, i2, z, z2);
        this.f9371Q = z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && !this.f9372R) {
            this.f9373S = getViewPagerParent();
        }
        if (this.f9373S != null) {
            int action = motionEvent.getAction();
            if (action == 0) {
                this.f9371Q = false;
                this.f9373S.requestDisallowInterceptTouchEvent(true);
            } else if (action != 2) {
                this.f9373S.requestDisallowInterceptTouchEvent(false);
                this.f9372R = false;
                this.f9373S = null;
            } else {
                this.f9373S.requestDisallowInterceptTouchEvent(true ^ this.f9371Q);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setJsInterface(JsInterface eVar) {
        addJavascriptInterface(eVar, "JsInterface");
    }

    public void setOnProgressListener(C4599d dVar) {
        this.f9376V = dVar;
    }

    public void setZoomControlGone(View view) {
        try {
            Field declaredField = WebView.class.getDeclaredField("mZoomButtonsController");
            declaredField.setAccessible(true);
            ZoomButtonsController zoomButtonsController = new ZoomButtonsController(view);
            zoomButtonsController.getZoomControls().setVisibility(8);
            try {
                declaredField.set(view, zoomButtonsController);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            }
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (NoSuchFieldException e4) {
            e4.printStackTrace();
        }
    }

    public static class WebFileChooseActivity extends Activity {

        /* renamed from: Q */
        public static C4594a f9380Q;

        /* renamed from: P */
        private String f9381P;

        /* renamed from: com.heimavista.widget.HvWebView$WebFileChooseActivity$a */
        public interface C4594a {
            /* renamed from: a */
            void mo25348a(Uri uri);
        }

        /* renamed from: a */
        public static void m15285a(Context context, C4594a aVar) {
            f9380Q = aVar;
            context.startActivity(new Intent(context, WebFileChooseActivity.class));
        }

        /* renamed from: b */
        private Intent m15287b() {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.addCategory("android.intent.category.OPENABLE");
            intent.setType("*/*");
            Intent a = m15283a(m15282a());
            a.putExtra("android.intent.extra.INTENT", intent);
            return a;
        }

        /* access modifiers changed from: protected */
        public void onActivityResult(int i, int i2, Intent intent) {
            super.onActivityResult(i, i2, intent);
            if (i == 1) {
                Uri a = m15284a(i2, intent, (intent == null || i2 != -1) ? null : intent.getData());
                C4594a aVar = f9380Q;
                if (aVar != null) {
                    aVar.mo25348a(a);
                    f9380Q = null;
                }
                finish();
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(@Nullable Bundle bundle) {
            getWindow().addFlags(262160);
            StatusBarUtils.m15268a(getWindow());
            super.onCreate(bundle);
            startActivityForResult(m15287b(), 1);
        }

        /* renamed from: a */
        private Uri m15284a(int i, Intent intent, Uri uri) {
            if (uri == null && intent == null && i == -1) {
                File file = new File(this.f9381P);
                if (!file.exists()) {
                    return uri;
                }
                Uri fromFile = Uri.fromFile(file);
                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", fromFile));
                return fromFile;
            } else if (uri == null) {
                return uri;
            } else {
                try {
                    InputStream openInputStream = getContentResolver().openInputStream(uri);
                    if (openInputStream == null) {
                        return uri;
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(this.f9381P);
                    m15286a(openInputStream, fileOutputStream);
                    openInputStream.close();
                    fileOutputStream.close();
                    return Uri.fromFile(new File(this.f9381P));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /* renamed from: a */
        private void m15286a(InputStream inputStream, OutputStream outputStream) {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (-1 != read) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        }

        /* renamed from: a */
        private Intent m15283a(Intent... intentArr) {
            Intent intent = new Intent("android.intent.action.CHOOSER");
            intent.putExtra("android.intent.extra.INITIAL_INTENTS", intentArr);
            intent.putExtra("android.intent.extra.TITLE", "Browser");
            return intent;
        }

        /* renamed from: a */
        private Intent m15282a() {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File externalCacheDir = getExternalCacheDir();
            File file = new File(externalCacheDir.getAbsolutePath() + File.separator + "browser-photos");
            file.mkdirs();
            this.f9381P = file.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg";
            intent.putExtra("output", Uri.fromFile(new File(this.f9381P)));
            return intent;
        }
    }

    /* renamed from: c */
    private void m15274c() {
        setHorizontalScrollbarOverlay(true);
        setVerticalScrollbarOverlay(true);
        requestFocusFromTouch();
        setAlwaysDrawnWithCacheEnabled(true);
        setWebChromeClient(this.f9377W);
        setWebViewClient(this.f9378a0);
        setDownloadListener(new C4608d(this));
    }

    /* renamed from: d */
    private void m15276d() {
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setCacheMode(-1);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setGeolocationEnabled(true);
        settings.setBuiltInZoomControls(false);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(true);
        setScrollBarStyle(33554432);
        mo25335a();
        settings.setUserAgentString(settings.getUserAgentString() + " HvApp");
    }

    /* renamed from: a */
    public void mo25335a() {
        getSettings().setSupportZoom(true);
        getSettings().setBuiltInZoomControls(true);
        getSettings().setDisplayZoomControls(false);
    }

    /* renamed from: b */
    public void mo25338b() {
        resumeTimers();
        mo25336a("onResume");
    }

    /* renamed from: a */
    public /* synthetic */ void mo25337a(String str, String str2, String str3, String str4, long j) {
        if (!URLUtil.isNetworkUrl(str)) {
            str = "http://" + str;
        }
        this.f9370P.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public HvWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f9370P = context;
        m15276d();
        m15274c();
    }

    /* renamed from: a */
    public void mo25336a(String str) {
        try {
            WebView.class.getMethod(str, new Class[0]).invoke(this, new Object[0]);
        } catch (NoSuchMethodException unused) {
            LogUtils.m1139c("No such method: " + str);
        } catch (IllegalAccessException unused2) {
            LogUtils.m1139c("Illegal Access: " + str);
        } catch (InvocationTargetException unused3) {
            LogUtils.m1139c("Invocation Target Exception: " + str);
        }
    }
}
