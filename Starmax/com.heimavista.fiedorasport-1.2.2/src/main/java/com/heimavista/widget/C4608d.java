package com.heimavista.widget;

import android.webkit.DownloadListener;

/* renamed from: com.heimavista.widget.d */
/* compiled from: lambda */
public final /* synthetic */ class C4608d implements DownloadListener {

    /* renamed from: a */
    private final /* synthetic */ HvWebView f9450a;

    public /* synthetic */ C4608d(HvWebView hvWebView) {
        this.f9450a = hvWebView;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f9450a.mo25337a(str, str2, str3, str4, j);
    }
}
