package com.heimavista.widget.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AppCompatDialog;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p190a.AnimAction;
import p119e.p189e.p190a.C4198c;
import p119e.p189e.p190a.C4199e;
import p119e.p189e.p190a.C4200g;
import p119e.p189e.p190a.ClickAction;
import p119e.p189e.p190a.ContextAction;
import p119e.p189e.p190a.HandlerAction;

public class BaseDialog extends AppCompatDialog implements C4199e, C4200g, C4198c, DialogInterface.OnShowListener, DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {

    /* renamed from: P */
    private final C4615g<BaseDialog> f9451P;

    /* renamed from: Q */
    private List<C4620l> f9452Q;

    /* renamed from: R */
    private List<C4616h> f9453R;

    /* renamed from: S */
    private List<C4618j> f9454S;

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$b */
    public static class C4610b<B extends C4610b> implements C4199e, C4198c {

        /* renamed from: P */
        private final Context f9455P;

        /* renamed from: Q */
        private BaseDialog f9456Q;

        /* renamed from: R */
        private View f9457R;

        /* renamed from: S */
        private int f9458S;

        /* renamed from: T */
        private int f9459T;

        /* renamed from: U */
        private int f9460U;

        /* renamed from: V */
        private int f9461V;

        /* renamed from: W */
        private int f9462W;

        /* renamed from: X */
        private boolean f9463X;

        /* renamed from: Y */
        private float f9464Y;

        /* renamed from: Z */
        private boolean f9465Z;

        /* renamed from: a0 */
        private boolean f9466a0;

        /* renamed from: b0 */
        private List<C4620l> f9467b0;

        /* renamed from: c0 */
        private List<C4616h> f9468c0;

        /* renamed from: d0 */
        private List<C4618j> f9469d0;

        /* renamed from: e0 */
        private C4619k f9470e0;

        /* renamed from: f0 */
        private SparseArray<C4617i> f9471f0;

        public C4610b(Activity activity) {
            this((Context) activity);
        }

        /* renamed from: a */
        public /* synthetic */ Resources mo24146a() {
            return ContextAction.m12998a(this);
        }

        /* renamed from: a */
        public B mo25410a(View view) {
            this.f9457R = view;
            if (mo25426f()) {
                this.f9456Q.setContentView(view);
            } else {
                View view2 = this.f9457R;
                if (view2 != null) {
                    ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                    if (layoutParams != null && this.f9461V == -2 && this.f9462W == -2) {
                        mo25427g(layoutParams.width);
                        mo25423e(layoutParams.height);
                    }
                    if (this.f9460U == 0) {
                        if (layoutParams instanceof FrameLayout.LayoutParams) {
                            mo25422d(((FrameLayout.LayoutParams) layoutParams).gravity);
                        } else if (layoutParams instanceof LinearLayout.LayoutParams) {
                            mo25422d(((LinearLayout.LayoutParams) layoutParams).gravity);
                        } else {
                            mo25422d(17);
                        }
                    }
                }
            }
            return this;
        }

        /* renamed from: a */
        public /* synthetic */ <S> S mo24147a(@NonNull Class cls) {
            return ContextAction.m12999a(this, cls);
        }

        /* renamed from: a */
        public /* synthetic */ String mo24148a(@StringRes int i) {
            return ContextAction.m13000a(this, i);
        }

        /* renamed from: a */
        public /* synthetic */ void mo24144a(@IdRes int... iArr) {
            ClickAction.m12996a(this, iArr);
        }

        /* renamed from: b */
        public B mo25417b(boolean z) {
            this.f9466a0 = z;
            if (mo25426f() && this.f9465Z) {
                this.f9456Q.setCanceledOnTouchOutside(z);
            }
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.widget.FrameLayout, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* renamed from: c */
        public B mo25419c(@LayoutRes int i) {
            mo25410a(LayoutInflater.from(this.f9455P).inflate(i, (ViewGroup) new FrameLayout(this.f9455P), false));
            return this;
        }

        /* renamed from: d */
        public B mo25422d(int i) {
            if (Build.VERSION.SDK_INT >= 17) {
                i = Gravity.getAbsoluteGravity(i, mo24146a().getConfiguration().getLayoutDirection());
            }
            this.f9460U = i;
            if (mo25426f()) {
                this.f9456Q.mo25392b(i);
            }
            return this;
        }

        /* renamed from: e */
        public B mo25423e(int i) {
            this.f9462W = i;
            if (mo25426f()) {
                this.f9456Q.mo25396c(i);
            } else {
                View view = this.f9457R;
                ViewGroup.LayoutParams layoutParams = view != null ? view.getLayoutParams() : null;
                if (layoutParams != null) {
                    layoutParams.height = i;
                    this.f9457R.setLayoutParams(layoutParams);
                }
            }
            return this;
        }

        /* renamed from: f */
        public B mo25425f(@StyleRes int i) {
            if (!mo25426f()) {
                this.f9458S = i;
                return this;
            }
            throw new IllegalStateException("are you ok?");
        }

        public <V extends View> V findViewById(@IdRes int i) {
            View view = this.f9457R;
            if (view != null) {
                return view.findViewById(i);
            }
            throw new IllegalStateException("are you ok?");
        }

        /* renamed from: g */
        public B mo25427g(int i) {
            this.f9461V = i;
            if (mo25426f()) {
                this.f9456Q.mo25397d(i);
            } else {
                View view = this.f9457R;
                ViewGroup.LayoutParams layoutParams = view != null ? view.getLayoutParams() : null;
                if (layoutParams != null) {
                    layoutParams.width = i;
                    this.f9457R.setLayoutParams(layoutParams);
                }
            }
            return this;
        }

        public Context getContext() {
            return this.f9455P;
        }

        public /* synthetic */ void onClick(View view) {
            ClickAction.m12995a(this, view);
        }

        public C4610b(Context context) {
            this.f9458S = R$style.BaseDialogStyle;
            this.f9459T = 0;
            this.f9460U = 0;
            this.f9461V = -2;
            this.f9462W = -2;
            this.f9463X = true;
            this.f9464Y = 0.5f;
            this.f9465Z = true;
            this.f9466a0 = true;
            this.f9455P = context;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void mo25420c() {
            BaseDialog baseDialog = this.f9456Q;
            if (baseDialog != null) {
                baseDialog.dismiss();
            }
        }

        /* renamed from: b */
        public B mo25416b(@StyleRes int i) {
            this.f9459T = i;
            if (mo25426f()) {
                this.f9456Q.mo25399e(i);
            }
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean mo25426f() {
            return this.f9456Q != null;
        }

        /* JADX WARNING: Removed duplicated region for block: B:5:0x0009  */
        /* renamed from: d */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.app.Activity mo25421d() {
            /*
                r3 = this;
                android.content.Context r0 = r3.f9455P
            L_0x0002:
                boolean r1 = r0 instanceof android.app.Activity
                if (r1 == 0) goto L_0x0009
                android.app.Activity r0 = (android.app.Activity) r0
                return r0
            L_0x0009:
                boolean r1 = r0 instanceof android.content.ContextWrapper
                r2 = 0
                if (r1 == 0) goto L_0x0016
                android.content.ContextWrapper r0 = (android.content.ContextWrapper) r0
                android.content.Context r0 = r0.getBaseContext()
                if (r0 != 0) goto L_0x0002
            L_0x0016:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.widget.dialog.BaseDialog.C4610b.mo25421d():android.app.Activity");
        }

        @SuppressLint({"RtlHardcoded"})
        /* renamed from: b */
        public BaseDialog mo25418b() {
            if (this.f9457R != null) {
                if (this.f9460U == 0) {
                    this.f9460U = 17;
                }
                if (this.f9459T == 0) {
                    int i = this.f9460U;
                    if (i == 3) {
                        this.f9459T = AnimAction.f7976f;
                    } else if (i == 5) {
                        this.f9459T = AnimAction.f7977g;
                    } else if (i == 48) {
                        this.f9459T = AnimAction.f7974d;
                    } else if (i != 80) {
                        this.f9459T = AnimAction.f7971a;
                    } else {
                        this.f9459T = AnimAction.f7975e;
                    }
                }
                this.f9456Q = mo25414a(this.f9455P, this.f9458S);
                this.f9456Q.setContentView(this.f9457R);
                this.f9456Q.setCancelable(this.f9465Z);
                if (this.f9465Z) {
                    this.f9456Q.setCanceledOnTouchOutside(this.f9466a0);
                }
                Window window = this.f9456Q.getWindow();
                if (window != null) {
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.width = this.f9461V;
                    attributes.height = this.f9462W;
                    attributes.gravity = this.f9460U;
                    attributes.windowAnimations = this.f9459T;
                    window.setAttributes(attributes);
                    if (this.f9463X) {
                        window.addFlags(2);
                        window.setDimAmount(this.f9464Y);
                    } else {
                        window.clearFlags(2);
                    }
                }
                List<C4620l> list = this.f9467b0;
                if (list != null) {
                    this.f9456Q.m15347c(list);
                }
                List<C4616h> list2 = this.f9468c0;
                if (list2 != null) {
                    this.f9456Q.m15343a(list2);
                }
                List<C4618j> list3 = this.f9469d0;
                if (list3 != null) {
                    this.f9456Q.m15345b(list3);
                }
                C4619k kVar = this.f9470e0;
                if (kVar != null) {
                    this.f9456Q.mo25390a(kVar);
                }
                int i2 = 0;
                while (true) {
                    SparseArray<C4617i> sparseArray = this.f9471f0;
                    if (sparseArray == null || i2 >= sparseArray.size()) {
                        Activity d = mo25421d();
                    } else {
                        this.f9457R.findViewById(this.f9471f0.keyAt(i2)).setOnClickListener(new C4624p(this.f9471f0.valueAt(i2)));
                        i2++;
                    }
                }
                Activity d2 = mo25421d();
                if (d2 != null) {
                    C4612d.m15392b(d2, this.f9456Q);
                }
                return this.f9456Q;
            }
            throw new IllegalArgumentException("Dialog layout cannot be empty");
        }

        @Nullable
        /* renamed from: e */
        public BaseDialog mo25424e() {
            return this.f9456Q;
        }

        /* access modifiers changed from: protected */
        /* renamed from: g */
        public boolean mo25428g() {
            return mo25426f() && this.f9456Q.isShowing();
        }

        /* renamed from: a */
        public B mo25413a(boolean z) {
            this.f9465Z = z;
            if (mo25426f()) {
                this.f9456Q.setCancelable(z);
            }
            return this;
        }

        /* renamed from: a */
        public B mo25412a(@NonNull C4620l lVar) {
            if (mo25426f()) {
                this.f9456Q.mo25391a(lVar);
            } else {
                if (this.f9467b0 == null) {
                    this.f9467b0 = new ArrayList();
                }
                this.f9467b0.add(lVar);
            }
            return this;
        }

        /* renamed from: a */
        public B mo25411a(@NonNull C4618j jVar) {
            if (mo25426f()) {
                this.f9456Q.mo25389a(jVar);
            } else {
                if (this.f9469d0 == null) {
                    this.f9469d0 = new ArrayList();
                }
                this.f9469d0.add(jVar);
            }
            return this;
        }

        /* renamed from: a */
        public B mo25409a(@IdRes int i, CharSequence charSequence) {
            ((TextView) findViewById(i)).setText(charSequence);
            return this;
        }

        /* renamed from: a */
        public B mo25408a(@IdRes int i, int i2) {
            findViewById(i).setVisibility(i2);
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public BaseDialog mo25414a(Context context, @StyleRes int i) {
            return new BaseDialog(context, i);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo25415a(Runnable runnable, long j) {
            if (mo25428g()) {
                this.f9456Q.postDelayed(runnable, j);
            } else {
                mo25412a(new C4623o(runnable, j));
            }
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$c */
    private static final class C4611c extends SoftReference<DialogInterface.OnCancelListener> implements C4616h {
        /* renamed from: a */
        public void mo25429a(BaseDialog baseDialog) {
            if (get() != null) {
                ((DialogInterface.OnCancelListener) get()).onCancel(baseDialog);
            }
        }

        private C4611c(DialogInterface.OnCancelListener onCancelListener) {
            super(onCancelListener);
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$d */
    private static final class C4612d implements Application.ActivityLifecycleCallbacks, C4620l, C4618j {

        /* renamed from: P */
        private BaseDialog f9472P;

        /* renamed from: Q */
        private Activity f9473Q;

        /* renamed from: R */
        private int f9474R;

        private C4612d(Activity activity, BaseDialog baseDialog) {
            this.f9473Q = activity;
            baseDialog.mo25391a((C4620l) this);
            baseDialog.mo25389a((C4618j) this);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static void m15392b(Activity activity, BaseDialog baseDialog) {
            new C4612d(activity, baseDialog);
        }

        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
        }

        public void onActivityDestroyed(@NonNull Activity activity) {
            if (this.f9473Q == activity) {
                BaseDialog baseDialog = this.f9472P;
                if (baseDialog != null) {
                    baseDialog.mo25394b((C4620l) null);
                    this.f9472P.mo25393b((C4618j) null);
                    if (this.f9472P.isShowing()) {
                        this.f9472P.dismiss();
                    }
                    this.f9472P = null;
                }
                this.f9473Q.getApplication().unregisterActivityLifecycleCallbacks(this);
                this.f9473Q = null;
            }
        }

        public void onActivityPaused(@NonNull Activity activity) {
        }

        public void onActivityResumed(@NonNull Activity activity) {
            BaseDialog baseDialog = this.f9472P;
            if (baseDialog != null && baseDialog.isShowing()) {
                this.f9472P.post(new C4625a(this));
            }
        }

        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {
        }

        public void onActivityStarted(@NonNull Activity activity) {
        }

        public void onActivityStopped(@NonNull Activity activity) {
            BaseDialog baseDialog = this.f9472P;
            if (baseDialog != null && baseDialog.isShowing()) {
                this.f9474R = this.f9472P.mo25395c();
                this.f9472P.mo25399e(0);
            }
        }

        /* renamed from: a */
        public /* synthetic */ void mo25430a() {
            this.f9472P.mo25399e(this.f9474R);
        }

        /* renamed from: b */
        public void mo25431b(BaseDialog baseDialog) {
            this.f9472P = baseDialog;
            this.f9473Q.getApplication().registerActivityLifecycleCallbacks(this);
        }

        /* renamed from: a */
        public void mo22806a(BaseDialog baseDialog) {
            this.f9472P = null;
            this.f9473Q.getApplication().unregisterActivityLifecycleCallbacks(this);
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$e */
    private static final class C4613e extends SoftReference<DialogInterface.OnDismissListener> implements C4618j {
        /* renamed from: a */
        public void mo22806a(BaseDialog baseDialog) {
            if (get() != null) {
                ((DialogInterface.OnDismissListener) get()).onDismiss(baseDialog);
            }
        }

        private C4613e(DialogInterface.OnDismissListener onDismissListener) {
            super(onDismissListener);
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$f */
    private static final class C4614f implements DialogInterface.OnKeyListener {

        /* renamed from: P */
        private final C4619k f9475P;

        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            C4619k kVar = this.f9475P;
            if (kVar == null || !(dialogInterface instanceof BaseDialog)) {
                return false;
            }
            kVar.mo25444a((BaseDialog) dialogInterface, keyEvent);
            return false;
        }

        private C4614f(C4619k kVar) {
            this.f9475P = kVar;
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$g */
    private static final class C4615g<T extends DialogInterface.OnShowListener & DialogInterface.OnCancelListener & DialogInterface.OnDismissListener> extends WeakReference<T> implements DialogInterface.OnShowListener, DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {
        public void onCancel(DialogInterface dialogInterface) {
            if (get() != null) {
                ((DialogInterface.OnCancelListener) ((DialogInterface.OnShowListener) get())).onCancel(dialogInterface);
            }
        }

        public void onDismiss(DialogInterface dialogInterface) {
            if (get() != null) {
                ((DialogInterface.OnDismissListener) ((DialogInterface.OnShowListener) get())).onDismiss(dialogInterface);
            }
        }

        public void onShow(DialogInterface dialogInterface) {
            if (get() != null) {
                ((DialogInterface.OnShowListener) get()).onShow(dialogInterface);
            }
        }

        private C4615g(T t) {
            super(t);
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$h */
    public interface C4616h {
        /* renamed from: a */
        void mo25429a(BaseDialog baseDialog);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$i */
    public interface C4617i<V extends View> {
        /* renamed from: a */
        void mo25443a(BaseDialog baseDialog, V v);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$j */
    public interface C4618j {
        /* renamed from: a */
        void mo22806a(BaseDialog baseDialog);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$k */
    public interface C4619k {
        /* renamed from: a */
        boolean mo25444a(BaseDialog baseDialog, KeyEvent keyEvent);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$l */
    public interface C4620l {
        /* renamed from: b */
        void mo25431b(BaseDialog baseDialog);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$m */
    public interface C4621m {
        /* renamed from: a */
        void mo23034a(boolean z);
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$n */
    private static final class C4622n extends SoftReference<DialogInterface.OnShowListener> implements C4620l {
        /* renamed from: b */
        public void mo25431b(BaseDialog baseDialog) {
            if (get() != null) {
                ((DialogInterface.OnShowListener) get()).onShow(baseDialog);
            }
        }

        private C4622n(DialogInterface.OnShowListener onShowListener) {
            super(onShowListener);
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$o */
    private static final class C4623o implements C4620l {

        /* renamed from: P */
        private final Runnable f9476P;

        /* renamed from: Q */
        private final long f9477Q;

        /* renamed from: b */
        public void mo25431b(BaseDialog baseDialog) {
            if (this.f9476P != null) {
                baseDialog.mo25394b(this);
                baseDialog.postDelayed(this.f9476P, this.f9477Q);
            }
        }

        private C4623o(Runnable runnable, long j) {
            this.f9476P = runnable;
            this.f9477Q = j;
        }
    }

    /* renamed from: com.heimavista.widget.dialog.BaseDialog$p */
    private static final class C4624p implements View.OnClickListener {

        /* renamed from: P */
        private final BaseDialog f9478P;

        /* renamed from: Q */
        private final C4617i f9479Q;

        public final void onClick(View view) {
            this.f9479Q.mo25443a(this.f9478P, view);
        }

        private C4624p(BaseDialog baseDialog, C4617i iVar) {
            this.f9478P = baseDialog;
            this.f9479Q = iVar;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseDialog(Context context, int i) {
        super(context, i <= 0 ? R$style.BaseDialogStyle : i);
        this.f9451P = new C4615g<>(this);
    }

    /* renamed from: a */
    public /* synthetic */ Resources mo24146a() {
        return ContextAction.m12998a(this);
    }

    /* renamed from: a */
    public /* synthetic */ <S> S mo24147a(@NonNull Class cls) {
        return ContextAction.m12999a(this, cls);
    }

    /* renamed from: a */
    public /* synthetic */ String mo24148a(@StringRes int i) {
        return ContextAction.m13000a(this, i);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24144a(@IdRes int... iArr) {
        ClickAction.m12996a(this, iArr);
    }

    /* renamed from: a */
    public /* synthetic */ boolean mo24150a(Runnable runnable, long j) {
        return HandlerAction.m13006a(this, runnable, j);
    }

    /* renamed from: b */
    public /* synthetic */ void mo24151b() {
        HandlerAction.m13004a(this);
    }

    /* renamed from: d */
    public void mo25397d(int i) {
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.width = i;
            window.setAttributes(attributes);
        }
    }

    public void dismiss() {
        mo24151b();
        super.dismiss();
    }

    /* renamed from: e */
    public void mo25399e(@StyleRes int i) {
        Window window = getWindow();
        if (window != null) {
            window.setWindowAnimations(i);
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
        List<C4616h> list = this.f9453R;
        if (list != null) {
            for (C4616h hVar : list) {
                hVar.mo25429a(this);
            }
        }
    }

    public /* synthetic */ void onClick(View view) {
        ClickAction.m12995a(this, view);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        List<C4618j> list = this.f9454S;
        if (list != null) {
            for (C4618j jVar : list) {
                jVar.mo22806a(this);
            }
        }
    }

    public void onShow(DialogInterface dialogInterface) {
        List<C4620l> list = this.f9452Q;
        if (list != null) {
            for (C4620l lVar : list) {
                lVar.mo25431b(this);
            }
        }
    }

    public /* synthetic */ boolean post(Runnable runnable) {
        return HandlerAction.m13005a(this, runnable);
    }

    public /* synthetic */ boolean postDelayed(Runnable runnable, long j) {
        return HandlerAction.m13007b(this, runnable, j);
    }

    @Deprecated
    public void setOnCancelListener(@Nullable DialogInterface.OnCancelListener onCancelListener) {
        if (onCancelListener != null) {
            mo25388a(new C4611c(onCancelListener));
        }
    }

    @Deprecated
    public void setOnDismissListener(@Nullable DialogInterface.OnDismissListener onDismissListener) {
        if (onDismissListener != null) {
            mo25389a(new C4613e(onDismissListener));
        }
    }

    @Deprecated
    public void setOnKeyListener(@Nullable DialogInterface.OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }

    @Deprecated
    public void setOnShowListener(@Nullable DialogInterface.OnShowListener onShowListener) {
        if (onShowListener != null) {
            mo25391a(new C4622n(onShowListener));
        }
    }

    /* renamed from: a */
    public void mo25390a(@Nullable C4619k kVar) {
        super.setOnKeyListener(new C4614f(kVar));
    }

    /* renamed from: b */
    public void mo25392b(int i) {
        Window window = getWindow();
        if (window != null) {
            window.setGravity(i);
        }
    }

    /* renamed from: c */
    public void mo25396c(int i) {
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.height = i;
            window.setAttributes(attributes);
        }
    }

    /* renamed from: a */
    public void mo25391a(@Nullable C4620l lVar) {
        if (this.f9452Q == null) {
            this.f9452Q = new ArrayList();
            super.setOnShowListener(this.f9451P);
        }
        this.f9452Q.add(lVar);
    }

    /* renamed from: b */
    public void mo25394b(@Nullable C4620l lVar) {
        List<C4620l> list = this.f9452Q;
        if (list != null) {
            list.remove(lVar);
        }
    }

    /* renamed from: b */
    public void mo25393b(@Nullable C4618j jVar) {
        List<C4618j> list = this.f9454S;
        if (list != null) {
            list.remove(jVar);
        }
    }

    /* renamed from: c */
    public int mo25395c() {
        Window window = getWindow();
        if (window != null) {
            return window.getAttributes().windowAnimations;
        }
        return 0;
    }

    /* renamed from: a */
    public void mo25388a(@Nullable C4616h hVar) {
        if (this.f9453R == null) {
            this.f9453R = new ArrayList();
            super.setOnCancelListener(this.f9451P);
        }
        this.f9453R.add(hVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m15345b(@Nullable List<C4618j> list) {
        super.setOnDismissListener(this.f9451P);
        this.f9454S = list;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m15347c(@Nullable List<C4620l> list) {
        super.setOnShowListener(this.f9451P);
        this.f9452Q = list;
    }

    /* renamed from: a */
    public void mo25389a(@Nullable C4618j jVar) {
        if (this.f9454S == null) {
            this.f9454S = new ArrayList();
            super.setOnDismissListener(this.f9451P);
        }
        this.f9454S.add(jVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m15343a(@Nullable List<C4616h> list) {
        super.setOnCancelListener(this.f9451P);
        this.f9453R = list;
    }
}
