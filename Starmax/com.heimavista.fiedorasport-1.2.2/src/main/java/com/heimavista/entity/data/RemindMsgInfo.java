package com.heimavista.entity.data;

import android.os.Parcel;
import android.os.Parcelable;

public class RemindMsgInfo implements Parcelable {
    public static final Parcelable.Creator<RemindMsgInfo> CREATOR = new C3674a();

    /* renamed from: P */
    private boolean f6332P;

    /* renamed from: Q */
    private boolean f6333Q;

    /* renamed from: R */
    private boolean f6334R;

    /* renamed from: S */
    private boolean f6335S;

    /* renamed from: T */
    private boolean f6336T;

    /* renamed from: U */
    private boolean f6337U;

    /* renamed from: V */
    private boolean f6338V;

    /* renamed from: W */
    private boolean f6339W;

    /* renamed from: X */
    private boolean f6340X;

    /* renamed from: Y */
    private boolean f6341Y;

    /* renamed from: Z */
    private boolean f6342Z;

    /* renamed from: com.heimavista.entity.data.RemindMsgInfo$a */
    static class C3674a implements Parcelable.Creator<RemindMsgInfo> {
        C3674a() {
        }

        public RemindMsgInfo createFromParcel(Parcel parcel) {
            return new RemindMsgInfo(parcel);
        }

        public RemindMsgInfo[] newArray(int i) {
            return new RemindMsgInfo[i];
        }
    }

    public RemindMsgInfo() {
        this.f6332P = true;
        this.f6333Q = true;
        this.f6334R = true;
        this.f6335S = true;
        this.f6336T = true;
        this.f6337U = true;
        this.f6338V = true;
        this.f6339W = true;
        this.f6340X = true;
        this.f6341Y = true;
        this.f6342Z = false;
    }

    /* renamed from: a */
    public boolean mo22599a() {
        return this.f6342Z;
    }

    /* renamed from: b */
    public boolean mo22601b() {
        return this.f6334R;
    }

    /* renamed from: c */
    public boolean mo22603c() {
        return this.f6335S;
    }

    /* renamed from: d */
    public boolean mo22605d() {
        return this.f6332P;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public boolean mo22608e() {
        return this.f6336T;
    }

    /* renamed from: f */
    public boolean mo22610f() {
        return this.f6338V;
    }

    /* renamed from: g */
    public boolean mo22612g() {
        return this.f6333Q;
    }

    /* renamed from: h */
    public boolean mo22614h() {
        return this.f6339W;
    }

    /* renamed from: i */
    public boolean mo22616i() {
        return this.f6337U;
    }

    /* renamed from: j */
    public boolean mo22618j() {
        return this.f6340X;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.f6332P ? (byte) 1 : 0);
        parcel.writeByte(this.f6333Q ? (byte) 1 : 0);
        parcel.writeByte(this.f6334R ? (byte) 1 : 0);
        parcel.writeByte(this.f6335S ? (byte) 1 : 0);
        parcel.writeByte(this.f6336T ? (byte) 1 : 0);
        parcel.writeByte(this.f6337U ? (byte) 1 : 0);
        parcel.writeByte(this.f6338V ? (byte) 1 : 0);
        parcel.writeByte(this.f6339W ? (byte) 1 : 0);
        parcel.writeByte(this.f6340X ? (byte) 1 : 0);
        parcel.writeByte(this.f6341Y ? (byte) 1 : 0);
        parcel.writeByte(this.f6342Z ? (byte) 1 : 0);
    }

    /* renamed from: a */
    public RemindMsgInfo mo22598a(boolean z) {
        this.f6342Z = z;
        return this;
    }

    /* renamed from: b */
    public void mo22600b(boolean z) {
        this.f6334R = z;
    }

    /* renamed from: c */
    public void mo22602c(boolean z) {
        this.f6335S = z;
    }

    /* renamed from: d */
    public void mo22604d(boolean z) {
        this.f6332P = z;
    }

    /* renamed from: e */
    public void mo22607e(boolean z) {
        this.f6336T = z;
    }

    /* renamed from: f */
    public void mo22609f(boolean z) {
        this.f6338V = z;
    }

    /* renamed from: g */
    public void mo22611g(boolean z) {
        this.f6333Q = z;
    }

    /* renamed from: h */
    public void mo22613h(boolean z) {
        this.f6339W = z;
    }

    /* renamed from: i */
    public void mo22615i(boolean z) {
        this.f6337U = z;
    }

    /* renamed from: j */
    public void mo22617j(boolean z) {
        this.f6340X = z;
    }

    protected RemindMsgInfo(Parcel parcel) {
        boolean z = true;
        this.f6332P = parcel.readByte() != 0;
        this.f6333Q = parcel.readByte() != 0;
        this.f6334R = parcel.readByte() != 0;
        this.f6335S = parcel.readByte() != 0;
        this.f6336T = parcel.readByte() != 0;
        this.f6337U = parcel.readByte() != 0;
        this.f6338V = parcel.readByte() != 0;
        this.f6339W = parcel.readByte() != 0;
        this.f6340X = parcel.readByte() != 0;
        this.f6341Y = parcel.readByte() != 0;
        this.f6342Z = parcel.readByte() == 0 ? false : z;
    }
}
