package com.heimavista.entity.band;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;

public class StepInfo implements Parcelable {
    public static final Parcelable.Creator<StepInfo> CREATOR = new C3669a();

    /* renamed from: P */
    public String f6242P;

    /* renamed from: Q */
    public long f6243Q;

    /* renamed from: R */
    public String f6244R;

    /* renamed from: S */
    public int f6245S;

    /* renamed from: T */
    public int f6246T;

    /* renamed from: U */
    public int f6247U;

    /* renamed from: V */
    public int f6248V;

    /* renamed from: W */
    public int f6249W;

    /* renamed from: X */
    public boolean f6250X;

    /* renamed from: com.heimavista.entity.band.StepInfo$a */
    static class C3669a implements Parcelable.Creator<StepInfo> {
        C3669a() {
        }

        public StepInfo createFromParcel(Parcel parcel) {
            return new StepInfo(parcel);
        }

        public StepInfo[] newArray(int i) {
            return new StepInfo[i];
        }
    }

    public StepInfo() {
    }

    public int describeContents() {
        return 0;
    }

    @NonNull
    public String toString() {
        return "StepData{userNbr='" + this.f6242P + '\'' + ", " + TimeUtils.m1009b(this.f6243Q * 1000) + ", cal=" + this.f6247U + ", distance=" + this.f6246T + ", stepTime=" + this.f6248V + ", step=" + this.f6245S + ", dayCount=" + this.f6249W + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f6242P);
        parcel.writeLong(this.f6243Q);
        parcel.writeString(this.f6244R);
        parcel.writeInt(this.f6245S);
        parcel.writeInt(this.f6246T);
        parcel.writeInt(this.f6247U);
        parcel.writeInt(this.f6248V);
        parcel.writeInt(this.f6249W);
        parcel.writeByte(this.f6250X ? (byte) 1 : 0);
    }

    public StepInfo(String str, long j, String str2, int i, int i2, int i3) {
        this.f6242P = str;
        this.f6243Q = j;
        this.f6244R = str2;
        this.f6245S = i;
        this.f6246T = i2;
        this.f6247U = i3;
    }

    protected StepInfo(Parcel parcel) {
        this.f6242P = parcel.readString();
        this.f6243Q = parcel.readLong();
        this.f6244R = parcel.readString();
        this.f6245S = parcel.readInt();
        this.f6246T = parcel.readInt();
        this.f6247U = parcel.readInt();
        this.f6248V = parcel.readInt();
        this.f6249W = parcel.readInt();
        this.f6250X = parcel.readByte() != 0;
    }
}
