package com.heimavista.entity.data;

import java.util.Comparator;

/* renamed from: com.heimavista.entity.data.c */
public class ComparatorBleDeviceItem implements Comparator<BleDeviceItem> {
    /* renamed from: a */
    public int compare(BleDeviceItem bVar, BleDeviceItem bVar2) {
        int c = bVar.mo22627c();
        int c2 = bVar2.mo22627c();
        int i = c < c2 ? 1 : 0;
        if (c > c2) {
            return -1;
        }
        return i;
    }
}
