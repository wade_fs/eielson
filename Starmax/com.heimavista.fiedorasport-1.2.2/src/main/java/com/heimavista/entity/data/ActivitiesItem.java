package com.heimavista.entity.data;

import java.util.List;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.entity.data.a */
public class ActivitiesItem {

    /* renamed from: a */
    public int f6343a;

    /* renamed from: b */
    public boolean f6344b;

    /* renamed from: c */
    public String f6345c;

    /* renamed from: d */
    public List<UserInfoDb> f6346d;

    /* renamed from: e */
    public long f6347e;

    /* renamed from: f */
    public String f6348f;
}
