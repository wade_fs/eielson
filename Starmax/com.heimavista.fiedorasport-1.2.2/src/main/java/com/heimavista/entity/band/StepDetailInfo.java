package com.heimavista.entity.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;

/* renamed from: com.heimavista.entity.band.g */
public class StepDetailInfo {

    /* renamed from: a */
    public String f6298a;

    /* renamed from: b */
    public long f6299b;

    /* renamed from: c */
    public String f6300c;

    /* renamed from: d */
    public int f6301d;

    /* renamed from: e */
    public long f6302e;

    /* renamed from: f */
    public long f6303f;

    /* renamed from: g */
    public int f6304g;

    /* renamed from: h */
    public int f6305h;

    /* renamed from: i */
    public int f6306i;

    /* renamed from: j */
    public int f6307j;

    /* renamed from: k */
    public boolean f6308k;

    /* renamed from: a */
    private String m10084a() {
        int i = this.f6301d;
        if (i == 1) {
            return "1 minor";
        }
        if (i == 2) {
            return "2 slow";
        }
        if (i != 3) {
            return i != 4 ? "" : "4 run";
        }
        return "3 fast";
    }

    @NonNull
    public String toString() {
        return "StepDetail{userNbr='" + this.f6298a + '\'' + ", step=" + this.f6307j + ", " + this.f6300c + ", " + this.f6302e + "~" + this.f6303f + ", " + TimeUtils.m1009b(this.f6302e * 1000) + "~" + TimeUtils.m1009b(this.f6303f * 1000) + ", cal=" + this.f6304g + ", distance=" + this.f6305h + ", stepTime=" + this.f6306i + " " + m10084a() + ", sync=" + this.f6308k + '}';
    }
}
