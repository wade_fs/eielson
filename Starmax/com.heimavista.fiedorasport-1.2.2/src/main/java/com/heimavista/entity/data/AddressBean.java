package com.heimavista.entity.data;

import android.os.Parcel;
import android.os.Parcelable;

public class AddressBean implements Parcelable {
    public static final Parcelable.Creator<AddressBean> CREATOR = new C3672a();

    /* renamed from: P */
    private double f6320P;

    /* renamed from: Q */
    private double f6321Q;

    /* renamed from: R */
    private String f6322R;

    /* renamed from: S */
    private String f6323S;

    /* renamed from: T */
    private String f6324T;

    /* renamed from: com.heimavista.entity.data.AddressBean$a */
    static class C3672a implements Parcelable.Creator<AddressBean> {
        C3672a() {
        }

        public AddressBean createFromParcel(Parcel parcel) {
            return new AddressBean(parcel);
        }

        public AddressBean[] newArray(int i) {
            return new AddressBean[i];
        }
    }

    protected AddressBean(Parcel parcel) {
        this.f6320P = parcel.readDouble();
        this.f6321Q = parcel.readDouble();
        this.f6322R = parcel.readString();
        this.f6323S = parcel.readString();
        this.f6324T = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "name:" + this.f6322R + " \naddress:" + this.f6323S + " \nplace_id:" + this.f6324T + " \naddress:" + this.f6323S + " \nlat,lng:(" + this.f6320P + "," + this.f6321Q + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.f6320P);
        parcel.writeDouble(this.f6321Q);
        parcel.writeString(this.f6322R);
        parcel.writeString(this.f6323S);
        parcel.writeString(this.f6324T);
    }
}
