package com.heimavista.entity.data;

import android.os.Parcel;
import android.os.Parcelable;

public class PeriodTimeInfo implements Parcelable {
    public static final Parcelable.Creator<PeriodTimeInfo> CREATOR = new C3673a();

    /* renamed from: P */
    private String f6325P;

    /* renamed from: Q */
    private boolean f6326Q;

    /* renamed from: R */
    private int f6327R;

    /* renamed from: S */
    private int f6328S;

    /* renamed from: T */
    private int f6329T;

    /* renamed from: U */
    private int f6330U;

    /* renamed from: V */
    private int f6331V;

    /* renamed from: com.heimavista.entity.data.PeriodTimeInfo$a */
    static class C3673a implements Parcelable.Creator<PeriodTimeInfo> {
        C3673a() {
        }

        public PeriodTimeInfo createFromParcel(Parcel parcel) {
            return new PeriodTimeInfo(parcel);
        }

        public PeriodTimeInfo[] newArray(int i) {
            return new PeriodTimeInfo[i];
        }
    }

    public PeriodTimeInfo() {
    }

    /* renamed from: h */
    public static PeriodTimeInfo m10085h() {
        return new PeriodTimeInfo("AutoHeart", false, 0, 0, 0, 0, 60);
    }

    /* renamed from: i */
    public static PeriodTimeInfo m10086i() {
        return new PeriodTimeInfo("DoNotDisturb", false, 22, 0, 8, 0, 0);
    }

    /* renamed from: j */
    public static PeriodTimeInfo m10087j() {
        return new PeriodTimeInfo("Location", false, 8, 0, 22, 0, 30);
    }

    /* renamed from: k */
    public static PeriodTimeInfo m10088k() {
        return new PeriodTimeInfo("QuickView", false, 8, 0, 22, 0, 0);
    }

    /* renamed from: l */
    public static PeriodTimeInfo m10089l() {
        return new PeriodTimeInfo("Sedentary", false, 9, 0, 20, 0, 20);
    }

    /* renamed from: a */
    public void mo22582a(boolean z) {
        this.f6326Q = z;
    }

    /* renamed from: b */
    public int mo22583b() {
        return this.f6330U;
    }

    /* renamed from: c */
    public int mo22585c() {
        return this.f6327R;
    }

    /* renamed from: d */
    public int mo22587d() {
        return this.f6328S;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public int mo22590e() {
        return this.f6331V;
    }

    /* renamed from: f */
    public String mo22592f() {
        return this.f6325P;
    }

    /* renamed from: g */
    public boolean mo22593g() {
        return this.f6326Q;
    }

    public String toString() {
        return "PeriodTimeInfo{type='" + this.f6325P + '\'' + ", enable=" + this.f6326Q + ", " + this.f6327R + ":" + this.f6328S + "~" + this.f6329T + ":" + this.f6330U + ", timeInterval=" + this.f6331V + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f6325P);
        parcel.writeByte(this.f6326Q ? (byte) 1 : 0);
        parcel.writeInt(this.f6327R);
        parcel.writeInt(this.f6328S);
        parcel.writeInt(this.f6329T);
        parcel.writeInt(this.f6330U);
        parcel.writeInt(this.f6331V);
    }

    public PeriodTimeInfo(String str, boolean z, int i, int i2, int i3, int i4, int i5) {
        this.f6325P = str;
        this.f6326Q = z;
        this.f6327R = i;
        this.f6328S = i2;
        this.f6329T = i3;
        this.f6330U = i4;
        this.f6331V = i5;
    }

    /* renamed from: a */
    public int mo22580a() {
        return this.f6329T;
    }

    /* renamed from: b */
    public void mo22584b(int i) {
        this.f6330U = i;
    }

    /* renamed from: c */
    public void mo22586c(int i) {
        this.f6327R = i;
    }

    /* renamed from: d */
    public void mo22588d(int i) {
        this.f6328S = i;
    }

    /* renamed from: e */
    public void mo22591e(int i) {
        this.f6331V = i;
    }

    /* renamed from: a */
    public void mo22581a(int i) {
        this.f6329T = i;
    }

    public PeriodTimeInfo(Parcel parcel) {
        this.f6325P = parcel.readString();
        this.f6326Q = parcel.readByte() != 0;
        this.f6327R = parcel.readInt();
        this.f6328S = parcel.readInt();
        this.f6329T = parcel.readInt();
        this.f6330U = parcel.readInt();
        this.f6331V = parcel.readInt();
    }
}
