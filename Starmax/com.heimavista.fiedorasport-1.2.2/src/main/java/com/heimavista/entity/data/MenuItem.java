package com.heimavista.entity.data;

import android.view.View;

/* renamed from: com.heimavista.entity.data.e */
public class MenuItem {

    /* renamed from: a */
    private int f6356a;

    /* renamed from: b */
    private int f6357b;

    /* renamed from: c */
    private String f6358c;

    /* renamed from: d */
    private String f6359d;

    /* renamed from: e */
    private boolean f6360e;

    /* renamed from: f */
    private boolean f6361f;

    /* renamed from: g */
    private boolean f6362g;

    /* renamed from: h */
    private View f6363h;

    /* renamed from: com.heimavista.entity.data.e$b */
    /* compiled from: MenuItem */
    public static class C3676b {

        /* renamed from: a */
        private MenuItem f6364a = new MenuItem();

        /* renamed from: a */
        public C3676b mo22657a(int i) {
            this.f6364a.mo22642a(i);
            return this;
        }

        /* renamed from: b */
        public C3676b mo22663b(int i) {
            this.f6364a.mo22647b(i);
            return this;
        }

        /* renamed from: a */
        public C3676b mo22659a(String str) {
            this.f6364a.mo22644a(str);
            return this;
        }

        /* renamed from: b */
        public C3676b mo22664b(String str) {
            this.f6364a.mo22648b(str);
            return this;
        }

        /* renamed from: a */
        public C3676b mo22660a(boolean z) {
            this.f6364a.mo22645a(z);
            return this;
        }

        /* renamed from: b */
        public C3676b mo22665b(boolean z) {
            this.f6364a.mo22649b(z);
            return this;
        }

        /* renamed from: a */
        public C3676b mo22658a(View view) {
            this.f6364a.mo22643a(view);
            return this;
        }

        /* renamed from: b */
        public C3676b mo22662b() {
            this.f6364a.mo22651c(false);
            return this;
        }

        /* renamed from: a */
        public MenuItem mo22661a() {
            return this.f6364a;
        }
    }

    /* renamed from: a */
    public void mo22645a(boolean z) {
        this.f6360e = z;
    }

    /* renamed from: b */
    public String mo22646b() {
        return this.f6359d;
    }

    /* renamed from: c */
    public int mo22650c() {
        return this.f6357b;
    }

    /* renamed from: d */
    public String mo22652d() {
        return this.f6358c;
    }

    /* renamed from: e */
    public int mo22653e() {
        return this.f6356a;
    }

    /* renamed from: f */
    public boolean mo22654f() {
        return this.f6360e;
    }

    /* renamed from: g */
    public boolean mo22655g() {
        return this.f6361f;
    }

    /* renamed from: h */
    public boolean mo22656h() {
        return this.f6362g;
    }

    private MenuItem() {
        this.f6357b = 0;
        this.f6360e = false;
        this.f6361f = true;
        this.f6362g = true;
        this.f6356a = 7;
    }

    /* renamed from: a */
    public void mo22644a(String str) {
        this.f6359d = str;
    }

    /* renamed from: b */
    public void mo22647b(int i) {
        this.f6356a = i;
    }

    /* renamed from: c */
    public void mo22651c(boolean z) {
        this.f6362g = z;
    }

    /* renamed from: a */
    public void mo22642a(int i) {
        this.f6357b = i;
    }

    /* renamed from: b */
    public void mo22648b(String str) {
        this.f6358c = str;
    }

    /* renamed from: a */
    public View mo22641a() {
        return this.f6363h;
    }

    /* renamed from: b */
    public void mo22649b(boolean z) {
        this.f6361f = z;
    }

    /* renamed from: a */
    public void mo22643a(View view) {
        this.f6363h = view;
    }
}
