package com.heimavista.entity.data;

/* renamed from: com.heimavista.entity.data.f */
public class SelectFriendBean {

    /* renamed from: a */
    private int f6365a;

    /* renamed from: b */
    private String f6366b;

    /* renamed from: c */
    private int f6367c;

    /* renamed from: d */
    private boolean f6368d;

    public SelectFriendBean(int i, String str, boolean z) {
        this.f6365a = i;
        this.f6366b = str;
        this.f6368d = z;
    }

    /* renamed from: a */
    public int mo22666a() {
        return this.f6367c;
    }

    /* renamed from: b */
    public int mo22667b() {
        return this.f6365a;
    }

    /* renamed from: c */
    public String mo22668c() {
        return this.f6366b;
    }

    /* renamed from: d */
    public boolean mo22669d() {
        return this.f6368d;
    }

    public SelectFriendBean(int i, String str, int i2) {
        this.f6365a = i;
        this.f6366b = str;
        this.f6367c = i2;
    }
}
