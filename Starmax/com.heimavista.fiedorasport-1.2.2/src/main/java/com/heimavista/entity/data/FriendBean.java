package com.heimavista.entity.data;

import androidx.annotation.NonNull;
import p119e.p189e.p193d.p195j.BuddyListDb;

/* renamed from: com.heimavista.entity.data.d */
public class FriendBean implements Comparable<FriendBean> {

    /* renamed from: P */
    private int f6352P;

    /* renamed from: Q */
    private String f6353Q;

    /* renamed from: R */
    private BuddyListDb f6354R;

    /* renamed from: S */
    private long f6355S;

    /* renamed from: a */
    public FriendBean mo22632a(int i) {
        this.f6352P = i;
        return this;
    }

    /* renamed from: b */
    public long mo22637b() {
        return this.f6355S;
    }

    /* renamed from: c */
    public int mo22638c() {
        return this.f6352P;
    }

    /* renamed from: d */
    public String mo22640d() {
        return this.f6353Q;
    }

    /* renamed from: a */
    public FriendBean mo22635a(String str) {
        this.f6353Q = str;
        return this;
    }

    /* renamed from: a */
    public FriendBean mo22633a(long j) {
        this.f6355S = j;
        return this;
    }

    /* renamed from: a */
    public FriendBean mo22634a(BuddyListDb aVar) {
        this.f6354R = aVar;
        return this;
    }

    /* renamed from: a */
    public BuddyListDb mo22636a() {
        return this.f6354R;
    }

    /* renamed from: a */
    public int compareTo(@NonNull FriendBean dVar) {
        long j;
        long j2;
        if (dVar.mo22636a() == null || this.f6354R == null) {
            j2 = dVar.mo22637b();
            j = this.f6355S;
        } else {
            j2 = dVar.mo22636a().mo24304q();
            j = this.f6354R.mo24304q();
        }
        return (int) (j2 - j);
    }
}
