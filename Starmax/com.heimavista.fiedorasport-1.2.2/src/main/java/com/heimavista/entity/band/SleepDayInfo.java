package com.heimavista.entity.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;
import java.util.List;

/* renamed from: com.heimavista.entity.band.b */
public class SleepDayInfo {

    /* renamed from: a */
    public long f6252a;

    /* renamed from: b */
    public long f6253b;

    /* renamed from: c */
    public int f6254c;

    /* renamed from: d */
    public int f6255d;

    /* renamed from: e */
    public int f6256e;

    /* renamed from: f */
    public int f6257f;

    /* renamed from: g */
    public int f6258g;

    /* renamed from: h */
    public int f6259h;

    /* renamed from: i */
    public int f6260i;

    /* renamed from: j */
    public List<SleepInfo> f6261j;

    @NonNull
    public String toString() {
        Object obj;
        StringBuilder sb = new StringBuilder();
        sb.append("DaySleepData{, ");
        sb.append(TimeUtils.m1009b(this.f6252a * 1000));
        sb.append("~");
        sb.append(TimeUtils.m1009b(this.f6253b * 1000));
        sb.append(", deep=");
        sb.append(this.f6254c);
        sb.append(", light=");
        sb.append(this.f6255d);
        sb.append(", awake=");
        sb.append(this.f6256e);
        sb.append(", total=");
        sb.append(this.f6257f);
        sb.append(", sleep=");
        sb.append(this.f6258g);
        sb.append(", awakeTime=");
        sb.append(this.f6259h);
        sb.append(", dayCount=");
        sb.append(this.f6260i);
        sb.append(", sleepDataList=");
        List<SleepInfo> list = this.f6261j;
        if (list == null) {
            obj = "";
        } else {
            obj = Integer.valueOf(list.size());
        }
        sb.append(obj);
        sb.append('}');
        return sb.toString();
    }
}
