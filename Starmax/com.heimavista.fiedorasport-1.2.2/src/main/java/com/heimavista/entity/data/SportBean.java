package com.heimavista.entity.data;

import com.heimavista.entity.band.SportInfo;

/* renamed from: com.heimavista.entity.data.g */
public class SportBean implements Comparable<SportBean> {

    /* renamed from: P */
    private int f6369P;

    /* renamed from: Q */
    private String f6370Q;

    /* renamed from: R */
    private long f6371R;

    /* renamed from: S */
    private SportInfo f6372S;

    public SportBean(int i, SportInfo dVar) {
        this.f6369P = i;
        this.f6372S = dVar;
    }

    /* renamed from: a */
    public String mo22671a() {
        return this.f6370Q;
    }

    /* renamed from: b */
    public SportInfo mo22672b() {
        return this.f6372S;
    }

    /* renamed from: c */
    public long mo22673c() {
        return this.f6371R;
    }

    /* renamed from: d */
    public int mo22675d() {
        return this.f6369P;
    }

    /* renamed from: a */
    public int compareTo(SportBean gVar) {
        return (int) (gVar.f6371R - this.f6371R);
    }

    public SportBean(int i, long j, SportInfo dVar) {
        this.f6369P = i;
        this.f6371R = j;
        this.f6372S = dVar;
    }

    public SportBean(int i, String str, long j, SportInfo dVar) {
        this.f6369P = i;
        this.f6370Q = str;
        this.f6371R = j;
        this.f6372S = dVar;
    }
}
