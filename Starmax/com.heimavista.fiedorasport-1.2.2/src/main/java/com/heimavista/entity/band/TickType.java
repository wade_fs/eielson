package com.heimavista.entity.band;

/* renamed from: com.heimavista.entity.band.h */
public enum TickType {
    STEP,
    SLEEP,
    HEART,
    SPORT,
    LOGIN,
    PAUSE,
    BUDDY_LIST,
    LIKE_CNT,
    NONE,
    DATA_REQUEST
}
