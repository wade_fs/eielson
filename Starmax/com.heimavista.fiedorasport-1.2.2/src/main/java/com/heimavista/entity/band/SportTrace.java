package com.heimavista.entity.band;

import android.location.Location;

/* renamed from: com.heimavista.entity.band.e */
public class SportTrace {

    /* renamed from: a */
    private int f6286a;

    /* renamed from: b */
    private boolean f6287b;

    /* renamed from: c */
    private Location f6288c;

    public SportTrace(int i, boolean z, Location location) {
        this.f6287b = z;
        this.f6288c = location;
    }

    /* renamed from: a */
    public void mo22570a(boolean z) {
        this.f6287b = z;
    }

    /* renamed from: b */
    public int mo22571b() {
        return this.f6286a;
    }

    /* renamed from: c */
    public boolean mo22572c() {
        return this.f6287b;
    }

    /* renamed from: a */
    public Location mo22569a() {
        return this.f6288c;
    }
}
