package com.heimavista.entity.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.entity.band.c */
public class SleepInfo {

    /* renamed from: a */
    public String f6262a;

    /* renamed from: b */
    public long f6263b;

    /* renamed from: c */
    public String f6264c;

    /* renamed from: d */
    public int f6265d;

    /* renamed from: e */
    public long f6266e;

    /* renamed from: f */
    public long f6267f;

    /* renamed from: g */
    public int f6268g;

    /* renamed from: h */
    public boolean f6269h;

    public SleepInfo() {
    }

    /* renamed from: a */
    private String m10077a() {
        int i = this.f6265d;
        if (i == 1) {
            return "1 deep ";
        }
        if (i == 2) {
            return "2 light";
        }
        return i == 3 ? "3 awake" : "";
    }

    @NonNull
    public String toString() {
        return "SleepData{userNbr='" + this.f6262a + '\'' + ", " + this.f6263b + ", " + this.f6264c + ", " + m10077a() + ", " + this.f6266e + "~" + this.f6267f + ", " + TimeUtils.m1009b(this.f6266e * 1000) + "~" + TimeUtils.m1009b(this.f6267f * 1000) + ", sleep=" + this.f6268g + '}';
    }

    public SleepInfo(long j, int i, int i2) {
        this.f6262a = MemberControl.m17125s().mo27187l();
        this.f6263b = j;
        this.f6264c = TimeUtil.m13442a();
        this.f6266e = j;
        this.f6267f = j;
        this.f6265d = i;
        this.f6268g = i2;
    }
}
