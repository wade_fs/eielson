package com.heimavista.entity.band;

import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.facebook.internal.ServerProtocol;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.entity.band.a */
public class BandConfig {

    /* renamed from: com.heimavista.entity.band.a$b */
    /* compiled from: BandConfig */
    private static class C3671b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static BandConfig f6251a = new BandConfig();
    }

    /* renamed from: k */
    private JSONObject m10062k() {
        try {
            return new JSONObject(m10063l().mo9872a("BandConfig", "{}"));
        } catch (JSONException unused) {
            return new JSONObject();
        }
    }

    /* renamed from: l */
    private SPUtils m10063l() {
        return SPUtils.m1243c("BandConfig-" + MemberControl.m17125s().mo27187l());
    }

    /* renamed from: m */
    public static BandConfig m10064m() {
        return C3671b.f6251a;
    }

    /* renamed from: a */
    public void mo22556a(String str) {
        m10063l().mo9883b("BandConfig", str);
    }

    /* renamed from: b */
    public ArrayList<String> mo22557b() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (HvAppConfig.m13024a().mo24166a("app", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION).compareTo("1.7.0") >= 0) {
            JSONObject optJSONObject = m10062k().optJSONObject("BtSdkPrefix");
            if (optJSONObject != null) {
                JSONArray optJSONArray = optJSONObject.optJSONArray("band01");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    int length = optJSONArray.length();
                    for (int i = 0; i < length; i++) {
                        arrayList.add(optJSONArray.optString(i));
                    }
                }
                JSONArray optJSONArray2 = optJSONObject.optJSONArray("watch01");
                if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                    int length2 = optJSONArray2.length();
                    for (int i2 = 0; i2 < length2; i2++) {
                        arrayList.add(optJSONArray2.optString(i2));
                    }
                }
            }
        } else {
            JSONArray optJSONArray3 = m10062k().optJSONArray("BtPrefix");
            if (optJSONArray3 != null && optJSONArray3.length() > 0) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    arrayList.add(optJSONArray3.optString(i3));
                }
            }
            arrayList.add("F15");
        }
        if (!arrayList.contains("FS")) {
            arrayList.add("FS");
        }
        if (!arrayList.contains("F15")) {
            arrayList.add("F15");
        }
        LogUtils.m1139c("mady btPrefix:" + arrayList);
        return arrayList;
    }

    /* renamed from: c */
    public ArrayList<String> mo22558c() {
        JSONArray optJSONArray;
        ArrayList<String> arrayList = new ArrayList<>();
        JSONObject optJSONObject = m10062k().optJSONObject("Ad");
        if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("DisabledPos")) == null || optJSONArray.length() <= 0)) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                arrayList.add(optJSONArray.optString(i));
            }
        }
        return arrayList;
    }

    /* renamed from: d */
    public boolean mo22559d() {
        return TextUtils.isEmpty(m10063l().mo9872a("BandConfig", ""));
    }

    /* renamed from: e */
    public boolean mo22560e() {
        JSONObject optJSONObject = m10062k().optJSONObject("Activity");
        if (optJSONObject == null || optJSONObject.optInt("enabled", 0) != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: f */
    public boolean mo22561f() {
        JSONObject optJSONObject = m10062k().optJSONObject("Ad");
        if (optJSONObject == null || optJSONObject.optInt("enabled", 0) != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: g */
    public boolean mo22562g() {
        JSONObject optJSONObject = m10062k().optJSONObject("Buddy");
        if (optJSONObject == null || optJSONObject.optInt("enabled", 0) != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: h */
    public boolean mo22563h() {
        JSONObject optJSONObject = m10062k().optJSONObject("Exp");
        if (optJSONObject == null || optJSONObject.optInt("enabled", 0) != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: i */
    public boolean mo22564i() {
        JSONObject optJSONObject = m10062k().optJSONObject("Buddy");
        if (optJSONObject == null || optJSONObject.optInt("sportEnabled", 0) != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: j */
    public boolean mo22565j() {
        JSONObject optJSONObject = m10062k().optJSONObject("Buddy");
        if (optJSONObject == null || optJSONObject.optInt("timelineEnabled", 0) != 1) {
            return false;
        }
        return true;
    }

    private BandConfig() {
    }

    /* renamed from: a */
    public void mo22555a() {
        m10063l().mo9873a();
    }
}
