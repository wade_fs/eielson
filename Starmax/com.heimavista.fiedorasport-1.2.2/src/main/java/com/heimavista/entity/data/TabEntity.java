package com.heimavista.entity.data;

import com.flyco.tablayout.listener.CustomTabEntity;

/* renamed from: com.heimavista.entity.data.h */
public class TabEntity implements CustomTabEntity {

    /* renamed from: a */
    private String f6373a;

    /* renamed from: b */
    private int f6374b;

    /* renamed from: c */
    private int f6375c;

    public TabEntity(String str, int i, int i2) {
        this.f6373a = str;
        this.f6374b = i;
        this.f6375c = i2;
    }

    public int getTabSelectedIcon() {
        return this.f6374b;
    }

    public String getTabTitle() {
        return this.f6373a;
    }

    public int getTabUnselectedIcon() {
        return this.f6375c;
    }
}
