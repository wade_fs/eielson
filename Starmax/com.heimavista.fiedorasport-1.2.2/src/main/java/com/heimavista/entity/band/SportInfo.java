package com.heimavista.entity.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;
import java.util.List;

/* renamed from: com.heimavista.entity.band.d */
public class SportInfo {

    /* renamed from: a */
    public String f6270a;

    /* renamed from: b */
    public String f6271b;

    /* renamed from: c */
    public String f6272c;

    /* renamed from: d */
    public int f6273d;

    /* renamed from: e */
    public long f6274e;

    /* renamed from: f */
    public long f6275f;

    /* renamed from: g */
    public int f6276g;

    /* renamed from: h */
    public int f6277h;

    /* renamed from: i */
    public int f6278i;

    /* renamed from: j */
    public int f6279j;

    /* renamed from: k */
    public int f6280k;

    /* renamed from: l */
    public int f6281l;

    /* renamed from: m */
    public int f6282m;

    /* renamed from: n */
    public long f6283n;

    /* renamed from: o */
    public boolean f6284o;

    /* renamed from: p */
    public List<Integer> f6285p;

    /* renamed from: a */
    private String m10078a() {
        int i = this.f6273d;
        if (i == 0) {
            return "0run";
        }
        return i == 1 ? "1walk" : "2bike";
    }

    @NonNull
    public String toString() {
        return "SportData{userNbr='" + this.f6270a + '\'' + ", " + this.f6271b + " " + m10078a() + ", sportId='" + this.f6272c + '\'' + ", " + this.f6274e + "~" + this.f6275f + ", " + TimeUtils.m1009b(this.f6274e * 1000) + "~" + TimeUtils.m1009b(this.f6275f * 1000) + ", duration=" + this.f6276g + ", distance=" + this.f6277h + ", cal=" + this.f6278i + ", avgHeart=" + this.f6279j + ", minHeart=" + this.f6280k + ", maxHeart=" + this.f6281l + ", likesTimestamp=" + this.f6283n + ", cnt=" + this.f6282m + ", sync=" + this.f6284o + '}';
    }
}
