package com.heimavista.entity.data;

import androidx.annotation.NonNull;

/* renamed from: com.heimavista.entity.data.b */
public class BleDeviceItem {

    /* renamed from: a */
    private String f6349a;

    /* renamed from: b */
    private String f6350b;

    /* renamed from: c */
    private int f6351c;

    public BleDeviceItem() {
    }

    /* renamed from: a */
    public String mo22622a() {
        return this.f6350b;
    }

    /* renamed from: b */
    public String mo22625b() {
        return this.f6349a;
    }

    /* renamed from: c */
    public int mo22627c() {
        return this.f6351c;
    }

    @NonNull
    public String toString() {
        return "「" + this.f6349a + "」" + this.f6350b;
    }

    public BleDeviceItem(String str, String str2, int i) {
        mo22626b(str);
        mo22624a(str2);
        mo22623a(i);
    }

    /* renamed from: a */
    public void mo22624a(String str) {
        this.f6350b = str;
    }

    /* renamed from: b */
    public void mo22626b(String str) {
        this.f6349a = str;
    }

    /* renamed from: a */
    public void mo22623a(int i) {
        this.f6351c = i;
    }
}
