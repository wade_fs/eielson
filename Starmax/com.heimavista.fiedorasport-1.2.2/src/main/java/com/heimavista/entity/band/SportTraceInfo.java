package com.heimavista.entity.band;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.TimeUtils;

/* renamed from: com.heimavista.entity.band.f */
public class SportTraceInfo {

    /* renamed from: a */
    public String f6289a;

    /* renamed from: b */
    public String f6290b;

    /* renamed from: c */
    public int f6291c;

    /* renamed from: d */
    public long f6292d;

    /* renamed from: e */
    public double f6293e;

    /* renamed from: f */
    public double f6294f;

    /* renamed from: g */
    public int f6295g;

    /* renamed from: h */
    public int f6296h;

    /* renamed from: i */
    public boolean f6297i;

    /* renamed from: a */
    private String m10083a() {
        return this.f6291c == 0 ? "0break" : "1running";
    }

    @NonNull
    public String toString() {
        return "SportTraceData{userNbr='" + this.f6289a + '\'' + ", sportId='" + this.f6290b + '\'' + ", " + m10083a() + '\'' + ", " + this.f6292d + ", " + TimeUtils.m1009b(this.f6292d * 1000) + " [" + this.f6293e + "," + this.f6294f + "], duration=" + this.f6295g + ", distance=" + this.f6296h + '}';
    }
}
