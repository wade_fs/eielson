package com.heimavista.fiedorasport.p111ui.fragment.map;

import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.C2885e;
import com.google.android.gms.maps.GoogleMapOptions;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.map.a */
/* compiled from: lambda */
public final /* synthetic */ class C4489a implements C2885e {

    /* renamed from: a */
    private final /* synthetic */ MapFragment f8895a;

    /* renamed from: b */
    private final /* synthetic */ GoogleMapOptions f8896b;

    public /* synthetic */ C4489a(MapFragment mapFragment, GoogleMapOptions googleMapOptions) {
        this.f8895a = mapFragment;
        this.f8896b = googleMapOptions;
    }

    /* renamed from: a */
    public final void mo18411a(C2880c cVar) {
        this.f8895a.mo24727a(this.f8896b, cVar);
    }
}
