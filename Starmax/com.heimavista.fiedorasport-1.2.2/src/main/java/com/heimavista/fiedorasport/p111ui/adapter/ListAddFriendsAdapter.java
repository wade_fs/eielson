package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.widget.CommonListItemView;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListAddFriendsAdapter */
public class ListAddFriendsAdapter extends BaseRecyclerViewAdapter<C4398a, ViewHolder> {

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListAddFriendsAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        CommonListItemView f8578R = ((CommonListItemView) findViewById(R$id.listItemView));

        ViewHolder(ListAddFriendsAdapter listAddFriendsAdapter, ViewGroup viewGroup, int i) {
            super(listAddFriendsAdapter, viewGroup, i);
            ImageView imageView = (ImageView) findViewById(16908294);
            TextView textView = (TextView) findViewById(16908308);
            TextView textView2 = (TextView) findViewById(16908309);
            Button button = (Button) findViewById(16908313);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListAddFriendsAdapter$a */
    public static class C4398a {

        /* renamed from: a */
        public int f8579a = 0;

        /* renamed from: b */
        public int f8580b;

        /* renamed from: c */
        public String f8581c;

        /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListAddFriendsAdapter$a$a */
        public static class C4399a {

            /* renamed from: a */
            private C4398a f8582a = new C4398a();

            /* renamed from: a */
            public C4399a mo24611a(int i) {
                this.f8582a.f8580b = i;
                return this;
            }

            /* renamed from: b */
            public C4399a mo24614b(int i) {
                this.f8582a.f8579a = i;
                return this;
            }

            /* renamed from: a */
            public C4399a mo24612a(String str) {
                this.f8582a.f8581c = str;
                return this;
            }

            /* renamed from: a */
            public C4398a mo24613a() {
                return this.f8582a;
            }
        }
    }

    public ListAddFriendsAdapter(Context context) {
        super(context);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        C4398a aVar = (C4398a) getItem(i);
        int i2 = aVar.f8579a;
        if (i2 == 1) {
            viewHolder.f8578R.setAccessoryType(0);
            viewHolder.f8578R.setImageResource(aVar.f8580b);
            viewHolder.f8578R.setText(aVar.f8581c);
        } else if (i2 == 2) {
            viewHolder.f8578R.setAccessoryType(0);
            viewHolder.f8578R.setText(aVar.f8581c);
            viewHolder.f8578R.getTextView().setTextSize(16.0f);
            viewHolder.itemView.setEnabled(false);
        }
    }

    public int getItemViewType(int i) {
        return ((C4398a) getItem(i)).f8579a;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this, viewGroup, R$layout.layout_list_item);
    }
}
