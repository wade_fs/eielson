package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.view.View;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.q */
/* compiled from: lambda */
public final /* synthetic */ class C4326q implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ SettingNotificationActivity f8322P;

    public /* synthetic */ C4326q(SettingNotificationActivity settingNotificationActivity) {
        this.f8322P = settingNotificationActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8322P.mo24483a(view, (MenuItem) obj, i);
    }
}
