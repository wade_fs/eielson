package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.data.ActivitiesItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.widget.p201a.BitmapLoader;
import com.heimavista.fiedorasport.widget.p201a.GroupAvatar;
import com.heimavista.fiedorasport.widget.p201a.GroupAvatarHelper;
import com.heimavista.fiedorasport.widget.p201a.OnGroupLoaded;
import java.util.ArrayList;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListActivitiesAdapter */
public class ListActivitiesAdapter extends BaseRecyclerViewAdapter<ActivitiesItem, ViewHolder> {

    /* renamed from: i */
    private final int f8567i = ConvertUtils.m1055a(200.0f);

    /* renamed from: j */
    private final int f8568j = ConvertUtils.m1055a(4.0f);

    /* renamed from: k */
    private final int f8569k = R$mipmap.ic_launcher;

    /* renamed from: l */
    private final int f8570l = Color.parseColor("#CCFFFF");

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListActivitiesAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        ImageView f8571R = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: S */
        TextView f8572S = ((TextView) findViewById(R$id.tvTitle));

        /* renamed from: T */
        TextView f8573T = ((TextView) findViewById(R$id.tvContent));

        /* renamed from: U */
        TextView f8574U = ((TextView) findViewById(R$id.tvTime));

        /* renamed from: V */
        ImageView f8575V = ((ImageView) findViewById(R$id.ivDot));

        ViewHolder(ListActivitiesAdapter listActivitiesAdapter, ViewGroup viewGroup, int i) {
            super(listActivitiesAdapter, viewGroup, i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListActivitiesAdapter$a */
    class C4396a implements BitmapLoader {

        /* renamed from: a */
        final /* synthetic */ Context f8576a;

        C4396a(ListActivitiesAdapter listActivitiesAdapter, Context context) {
            this.f8576a = context;
        }

        /* renamed from: a */
        public Bitmap mo24607a(String str) {
            return AvatarUtils.m13388a(this.f8576a, str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListActivitiesAdapter$b */
    class C4397b implements OnGroupLoaded {

        /* renamed from: a */
        final /* synthetic */ ViewHolder f8577a;

        C4397b(ListActivitiesAdapter listActivitiesAdapter, ViewHolder viewHolder) {
            this.f8577a = viewHolder;
        }

        /* renamed from: a */
        public void mo24608a(GroupAvatar bVar) {
            this.f8577a.f8571R.setImageBitmap(bVar.mo24828a());
        }

        public void onError() {
        }
    }

    public ListActivitiesAdapter(Context context) {
        super(context);
        GroupAvatarHelper.m14691a().mo24830a(context, new C4396a(this, context));
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ActivitiesItem aVar = (ActivitiesItem) getItem(i);
        if (aVar.f6343a > 0) {
            viewHolder.f8572S.setText(aVar.f6345c + "(" + aVar.f6346d.size() + ")");
            viewHolder.f8573T.setText(aVar.f6348f);
            viewHolder.f8574U.setText(TimeUtils.m998a(aVar.f6347e, "MM-dd HH:mm"));
            viewHolder.f8575V.setVisibility(aVar.f6344b ? 8 : 0);
            ArrayList arrayList = new ArrayList();
            for (UserInfoDb fVar : aVar.f6346d) {
                if (arrayList.size() == 9) {
                    break;
                }
                arrayList.add(fVar.mo24336v());
            }
            GroupAvatarHelper.m14691a().mo24832a(arrayList, this.f8567i, this.f8568j, this.f8570l, this.f8569k, new C4397b(this, viewHolder));
        }
    }

    public int getItemViewType(int i) {
        return ((ActivitiesItem) getItem(i)).f6343a;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new ViewHolder(this, viewGroup, R$layout.layout_empty);
        }
        return new ViewHolder(this, viewGroup, R$layout.layout_list_item_activities);
    }
}
