package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;
import org.xutils.C5217x;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListNotificationAdapter */
public class ListNotificationAdapter extends BaseRecyclerViewAdapter<MenuItem, C4407a> {

    /* renamed from: i */
    private LayoutInflater f8626i;

    /* renamed from: j */
    private BaseActivity f8627j;

    /* renamed from: k */
    private RemindMsgInfo f8628k;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListNotificationAdapter$a */
    class C4407a extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        CommonListItemView f8629R = ((CommonListItemView) findViewById(R$id.listItemView));

        C4407a(ListNotificationAdapter listNotificationAdapter, View view) {
            super(view);
            TextView textView = (TextView) findViewById(16908308);
        }
    }

    public ListNotificationAdapter(BaseActivity baseActivity, RemindMsgInfo remindMsgInfo) {
        super(baseActivity);
        this.f8627j = baseActivity;
        this.f8626i = LayoutInflater.from(baseActivity);
        this.f8628k = remindMsgInfo;
    }

    /* renamed from: g */
    private HvApiPostBandSettings m14348g() {
        this.f8627j.mo22712a(false);
        C5217x.task().postDelayed(new C4425o(this), 500);
        return new HvApiPostBandSettings(null);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4407a aVar, int i) {
        MenuItem eVar = (MenuItem) getItem(i);
        aVar.f8629R.setAccessoryType(2);
        aVar.f8629R.setImageResource(eVar.mo22650c());
        aVar.f8629R.setText(eVar.mo22652d());
        SwitchButton switchButton = aVar.f8629R.getSwitch();
        switchButton.setChecked(eVar.mo22654f());
        switchButton.setEnabled(eVar.mo22655g());
        switchButton.setOnCheckedChangeListener(new C4426p(this, eVar));
    }

    /* renamed from: f */
    public /* synthetic */ void mo24650f() {
        BaseActivity baseActivity = this.f8627j;
        if (baseActivity != null && !baseActivity.isFinishing()) {
            this.f8627j.mo22717b();
        }
    }

    public int getItemViewType(int i) {
        return ((MenuItem) getItem(i)).mo22653e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4407a onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new C4407a(this, this.f8626i.inflate(R$layout.layout_list_item, viewGroup, false));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24648a(MenuItem eVar, SwitchButton switchButton, boolean z) {
        String d = eVar.mo22652d();
        if (d.equals(mo22781a(R$string.facebook_message))) {
            m14347a("fb", z);
            this.f8628k.mo22600b(z);
        }
        if (d.equals(mo22781a(R$string.line_message))) {
            m14347a("line", z);
            this.f8628k.mo22602c(z);
        }
        if (d.equals(mo22781a(R$string.qq_message))) {
            m14347a("qq", z);
            this.f8628k.mo22607e(z);
        }
        if (d.equals(mo22781a(R$string.wechat_message))) {
            m14347a("微信", z);
            this.f8628k.mo22615i(z);
        }
        if (d.equals(mo22781a(R$string.skype_message))) {
            m14347a("skype", z);
            this.f8628k.mo22609f(z);
        }
        if (d.equals(mo22781a(R$string.twitter_message))) {
            m14347a("twitter", z);
            this.f8628k.mo22613h(z);
        }
        if (d.equals(mo22781a(R$string.whatsapp_message))) {
            m14347a("whatsapp", z);
            this.f8628k.mo22617j(z);
        }
        BandDataManager.m10551a(this.f8628k);
        m14348g().requestPostNotification();
    }

    /* renamed from: a */
    private void m14347a(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(z ? "開啟" : "關閉");
        Constant.m10311a(sb.toString());
    }
}
