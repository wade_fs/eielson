package com.heimavista.fiedorasport.p199j;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.location.LocationManagerCompat;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.FileIOUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.google.android.gms.common.C2167b;
import com.heimavista.fiedorasport.FSManager;
import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;
import org.xutils.C5217x;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.j.l */
/* compiled from: Utils */
public class C4222l {

    /* renamed from: com.heimavista.fiedorasport.j.l$a */
    /* compiled from: Utils */
    static class C4223a extends CountDownTimer {

        /* renamed from: a */
        final /* synthetic */ TextView f8049a;

        /* renamed from: b */
        final /* synthetic */ String f8050b;

        /* renamed from: c */
        final /* synthetic */ String f8051c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4223a(long j, long j2, TextView textView, String str, String str2) {
            super(j, j2);
            this.f8049a = textView;
            this.f8050b = str;
            this.f8051c = str2;
        }

        /* renamed from: a */
        static /* synthetic */ void m13466a(TextView textView, long j, String str) {
            textView.setEnabled(false);
            textView.setText((j / 1000) + str);
        }

        public void onFinish() {
            this.f8049a.setEnabled(true);
            this.f8049a.setText(this.f8051c);
        }

        public void onTick(long j) {
            C5217x.task().post(new C4213a(this.f8049a, j, this.f8050b));
        }
    }

    /* renamed from: a */
    public static int m13451a(int i) {
        return (i * 100) / m13460b();
    }

    /* renamed from: b */
    public static int m13460b() {
        return 61;
    }

    /* renamed from: b */
    public static int m13461b(int i) {
        int n = FSManager.m10323r().mo22844n();
        double h = ((double) (FSManager.m10323r().mo22838h() * ((float) i) * ((float) m13460b()))) * 0.01d;
        if (n == 0) {
            n = 60;
        }
        return (int) ((h * ((double) n)) / 1000.0d);
    }

    /* renamed from: c */
    public static int m13463c(int i) {
        return (i * m13460b()) / 100;
    }

    /* renamed from: d */
    public static boolean m13465d() {
        return PermissionUtils.m1193a(new String[]{"android.permission.ACCESS_FINE_LOCATION"}) || PermissionUtils.m1193a(new String[]{"android.permission.ACCESS_COARSE_LOCATION"});
    }

    /* renamed from: a */
    public static void m13457a(ImageView imageView, int i) {
        Drawable wrap = DrawableCompat.wrap(imageView.getDrawable());
        DrawableCompat.setTintList(wrap, imageView.getContext().getResources().getColorStateList(i));
        imageView.setImageDrawable(wrap);
    }

    /* renamed from: c */
    public static boolean m13464c() {
        LocationManager locationManager = (LocationManager) HvApp.m13010c().getSystemService("location");
        if (locationManager != null) {
            return LocationManagerCompat.isLocationEnabled(locationManager);
        }
        return false;
    }

    /* renamed from: b */
    public static boolean m13462b(Context context) {
        return C2167b.m5251a().mo16831c(context) == 0;
    }

    /* renamed from: a */
    public static boolean m13458a(CharSequence charSequence) {
        int length = charSequence.length();
        return (length == 9 && charSequence.toString().startsWith("9")) || (length == 10 && charSequence.toString().startsWith("09"));
    }

    /* renamed from: a */
    public static void m13456a(EditText editText, int i) {
        C4214b bVar = C4214b.f8022P;
        if (i > 0) {
            editText.setFilters(new InputFilter[]{bVar, new InputFilter.LengthFilter(i)});
            return;
        }
        editText.setFilters(new InputFilter[]{bVar});
    }

    /* renamed from: a */
    static /* synthetic */ CharSequence m13453a(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (Pattern.compile("[`~!@#$%^&*()+=|{}:;',\\[\\].<>/?！￥…（）—【】‘；：\"”“’。，、？]").matcher(charSequence.toString()).find()) {
            return "";
        }
        return null;
    }

    /* renamed from: a */
    public static void m13455a(Context context) {
        try {
            Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
            declaredMethod.setAccessible(true);
            Object invoke = declaredMethod.invoke((TelephonyManager) context.getSystemService("phone"), null);
            Method method = invoke.getClass().getMethod("endCall", new Class[0]);
            method.setAccessible(true);
            method.invoke(invoke, new Object[0]);
        } catch (Exception unused) {
            LogUtils.m1131b("end call error");
        }
    }

    /* renamed from: a */
    public static String m13454a() {
        String str;
        if (SDCardUtils.m1242b()) {
            str = SDCardUtils.m1241a();
        } else {
            str = HvApp.m13010c().getExternalCacheDir() != null ? HvApp.m13010c().getExternalCacheDir().getAbsolutePath() : "";
        }
        return str + File.separator + AppUtils.m943a() + File.separator;
    }

    /* renamed from: a */
    public static CountDownTimer m13452a(TextView textView, int i, String str, String str2) {
        return new C4223a((long) (i * 1000), 1000, textView, str2, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.j.a(java.lang.String, byte[], boolean):boolean
     arg types: [java.lang.String, byte[], int]
     candidates:
      com.blankj.utilcode.util.j.a(java.io.File, java.lang.String, boolean):boolean
      com.blankj.utilcode.util.j.a(java.lang.String, byte[], boolean):boolean */
    /* renamed from: a */
    public static boolean m13459a(String str, String str2, String str3) {
        File externalFilesDir = HvApp.m13010c().getExternalFilesDir(str2);
        if (externalFilesDir == null) {
            return false;
        }
        Calendar instance = Calendar.getInstance();
        String str4 = instance.get(1) + "-" + (instance.get(2) + 1) + "-" + instance.get(5) + "-" + str3;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return FileIOUtils.m1081a(externalFilesDir.getPath() + File.separator + str4, (TimeUtils.m1000a(simpleDateFormat) + ">>" + str + "\r\n").getBytes(), true);
    }
}
