package com.heimavista.fiedorasport.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.widget.ImageView;

@SuppressLint({"AppCompatCustomView"})
public class MaskableImageView extends ImageView {
    public MaskableImageView(Context context) {
        super(context);
    }

    /* renamed from: a */
    private void m14678a(float f) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setScale(f, f, f, 1.0f);
        ColorMatrixColorFilter colorMatrixColorFilter = new ColorMatrixColorFilter(colorMatrix);
        setColorFilter(colorMatrixColorFilter);
        getDrawable().setColorFilter(colorMatrixColorFilter);
    }

    public void setPressed(boolean z) {
        m14678a(z ? 0.8f : 1.0f);
        super.setPressed(z);
    }

    public MaskableImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MaskableImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
