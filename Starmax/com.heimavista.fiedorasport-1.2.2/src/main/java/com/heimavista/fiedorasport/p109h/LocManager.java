package com.heimavista.fiedorasport.p109h;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.location.C2826b;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.C2837g;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.heimavista.api.buddy.HvApiPostLoc;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.utils.ClassUtil;
import java.util.concurrent.TimeUnit;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p195j.BuddyListDb;

/* renamed from: com.heimavista.fiedorasport.h.b0 */
public class LocManager {

    /* renamed from: a */
    private boolean f6517a;

    /* renamed from: b */
    private boolean f6518b;

    /* renamed from: c */
    private ThreadUtils.C0877e<Void> f6519c;

    /* renamed from: d */
    private boolean f6520d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C2826b f6521e;

    /* renamed from: f */
    private C2833e f6522f;

    /* renamed from: g */
    private AMapLocationClient f6523g;

    /* renamed from: h */
    private AMapLocationListener f6524h;

    /* renamed from: i */
    private LocationManager f6525i;

    /* renamed from: j */
    private LocationListener f6526j;

    /* renamed from: k */
    private Context f6527k;

    /* renamed from: com.heimavista.fiedorasport.h.b0$a */
    /* compiled from: LocManager */
    class C3708a extends C2833e {
        C3708a() {
        }

        /* renamed from: a */
        public void mo18255a(LocationResult locationResult) {
            super.mo18255a(locationResult);
            LocManager.this.m10488a("onLocationResult");
            if (locationResult == null || locationResult.mo18234c() == null) {
                LocManager.this.m10497k();
            } else {
                LocManager.this.m10484a(locationResult.mo18234c());
            }
            LocManager.this.f6521e.mo18253a(super);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.b0$b */
    /* compiled from: LocManager */
    class C3709b implements AMapLocationListener {
        C3709b(LocManager b0Var) {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.b0$c */
    /* compiled from: LocManager */
    class C3710c implements LocationListener {
        C3710c(LocManager b0Var) {
        }

        public void onLocationChanged(Location location) {
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.b0$d */
    /* compiled from: LocManager */
    class C3711d extends ThreadUtils.C0877e<Void> {
        C3711d() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: b */
        public Void mo9798b() {
            return null;
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(Void voidR) {
            HvApp.m13010c().mo24157a("LocManager.action_location_callback");
            LocManager.this.mo22955a();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.b0$e */
    /* compiled from: LocManager */
    private static class C3712e {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static LocManager f6530a = new LocManager(null);
    }

    /* synthetic */ LocManager(C3708a aVar) {
        this();
    }

    /* renamed from: d */
    private void m10490d() {
        ThreadUtils.C0877e<Void> eVar = this.f6519c;
        if (eVar != null) {
            ThreadUtils.m952a(eVar);
        }
    }

    /* renamed from: e */
    private void m10491e() {
        if (this.f6519c == null) {
            this.f6519c = new C3711d();
        }
        ThreadUtils.m960b(this.f6519c, 20, TimeUnit.SECONDS);
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: f */
    private synchronized String m10492f() {
        String str = null;
        if (this.f6525i == null) {
            return null;
        }
        Location location = null;
        for (String str2 : this.f6525i.getAllProviders()) {
            Location lastKnownLocation = this.f6525i.getLastKnownLocation(str2);
            if (lastKnownLocation != null) {
                if (location == null || Float.compare(lastKnownLocation.getAccuracy(), location.getAccuracy()) >= 0) {
                    str = str2;
                    location = lastKnownLocation;
                }
            }
        }
        return str;
    }

    /* renamed from: g */
    public static LocManager m10493g() {
        return C3712e.f6530a;
    }

    /* renamed from: h */
    public static boolean m10494h() {
        return ClassUtil.m15236a("com.amap.api.location.AMapLocationClient");
    }

    /* renamed from: i */
    private void m10495i() {
        this.f6520d = true;
        Activity b = ActivityUtils.m926b();
        if (b == null) {
            this.f6527k = HvApp.m13010c();
        } else {
            this.f6527k = b;
            m10488a("curAct " + b.getClass().getName());
        }
        if (FSManager.m10325t()) {
            m10498l();
        } else if (FSManager.m10324s()) {
            m10496j();
        } else {
            m10497k();
        }
        m10491e();
    }

    /* renamed from: j */
    private void m10496j() {
        m10488a("startAmapLocation");
        this.f6523g = new AMapLocationClient(this.f6527k);
        AMapLocationClientOption aMapLocationClientOption = new AMapLocationClientOption();
        this.f6524h = new C3709b(this);
        this.f6523g.setLocationListener(this.f6524h);
        aMapLocationClientOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        aMapLocationClientOption.setOnceLocation(true);
        this.f6523g.setLocationOption(aMapLocationClientOption);
        this.f6523g.startLocation();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"MissingPermission"})
    /* renamed from: k */
    public void m10497k() {
        Location lastKnownLocation;
        m10488a("startDefaultLocation");
        this.f6525i = (LocationManager) this.f6527k.getSystemService("location");
        if (this.f6525i != null) {
            this.f6526j = new C3710c(this);
            for (String str : this.f6525i.getAllProviders()) {
                this.f6525i.requestLocationUpdates(str, (long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, 0.0f, this.f6526j);
            }
            while (!this.f6518b) {
                String f = m10492f();
                if (!(f == null || (lastKnownLocation = this.f6525i.getLastKnownLocation(f)) == null)) {
                    m10484a(lastKnownLocation);
                }
            }
        }
    }

    /* renamed from: l */
    private void m10498l() {
        m10488a("startGoogleLocation");
        if (this.f6521e == null) {
            this.f6521e = C2837g.m7977a(this.f6527k);
        }
        if (this.f6522f == null) {
            this.f6522f = new C3708a();
        }
        LocationRequest d = LocationRequest.m7950d();
        d.mo18231i(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        d.mo18229h(1000);
        d.mo18227d(100);
        d.mo18225a(0.0f);
        this.f6521e.mo18252a(d, this.f6522f, Looper.getMainLooper()).mo23696a(new C3733v(this));
    }

    /* renamed from: c */
    public void mo22958c() {
        this.f6517a = true;
        if (!this.f6520d && BandDataManager.m10555b("Location").mo22593g() && BuddyListDb.m13245F() != 0) {
            m10495i();
        }
    }

    private LocManager() {
        this.f6517a = false;
        this.f6518b = false;
        this.f6520d = false;
    }

    /* renamed from: b */
    public void mo22957b() {
        if (!this.f6520d) {
            this.f6517a = false;
            m10495i();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22956a(Exception exc) {
        m10488a("onFailure:" + exc.getMessage());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10484a(@NonNull Location location) {
        m10490d();
        m10488a("LastKnownLocation --> " + location.toString() + " speed=" + location.getSpeed());
        if (this.f6517a) {
            if (HvApp.m13010c().mo24159b()) {
                C4222l.m13459a("getLoc success:" + location.toString() + " speed=" + location.getSpeed(), "FsLog", "loc.log");
            }
            new HvApiPostLoc().request(location);
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable("location", location);
        HvApp.m13010c().mo24158a("LocManager.action_location_callback", bundle);
        this.f6518b = true;
        this.f6520d = false;
    }

    /* renamed from: a */
    public void mo22955a() {
        LocationListener locationListener;
        C2833e eVar;
        AMapLocationClient aMapLocationClient = this.f6523g;
        if (aMapLocationClient != null) {
            aMapLocationClient.stopLocation();
            AMapLocationListener aMapLocationListener = this.f6524h;
            if (aMapLocationListener != null) {
                this.f6523g.unRegisterLocationListener(aMapLocationListener);
            }
            this.f6523g.onDestroy();
            this.f6523g = null;
        }
        C2826b bVar = this.f6521e;
        if (!(bVar == null || (eVar = this.f6522f) == null)) {
            bVar.mo18253a(eVar);
            this.f6521e = null;
        }
        LocationManager locationManager = this.f6525i;
        if (!(locationManager == null || (locationListener = this.f6526j) == null)) {
            locationManager.removeUpdates(locationListener);
            this.f6525i = null;
        }
        ThreadUtils.C0877e<Void> eVar2 = this.f6519c;
        if (eVar2 != null) {
            ThreadUtils.m952a(eVar2);
        }
        this.f6518b = false;
        this.f6520d = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10488a(String str) {
        Object[] objArr = new Object[1];
        StringBuilder sb = new StringBuilder();
        sb.append("madyLoc>>>LocManager ");
        sb.append(this.f6517a ? "post " : "get ");
        sb.append(str);
        objArr[0] = sb.toString();
        LogUtils.m1139c(objArr);
    }
}
