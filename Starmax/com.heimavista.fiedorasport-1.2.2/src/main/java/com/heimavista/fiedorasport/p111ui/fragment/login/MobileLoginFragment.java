package com.heimavista.fiedorasport.p111ui.fragment.login;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.blankj.utilcode.util.RegexUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiGetSmsCode;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.login.LoginActivity;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.login.MobileLoginFragment */
public class MobileLoginFragment extends BaseFragment<LoginActivity> {

    /* renamed from: W */
    private EditText f8876W;

    /* renamed from: X */
    private EditText f8877X;
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public TextView f8878Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public CountDownTimer f8879Z;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.login.MobileLoginFragment$a */
    class C4483a implements OnResultListener<Void> {
        C4483a() {
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            MobileLoginFragment.this.mo22768b();
            MobileLoginFragment mobileLoginFragment = MobileLoginFragment.this;
            mobileLoginFragment.mo22761a(mobileLoginFragment.getString(R$string.send_code_success));
            MobileLoginFragment.this.f8879Z.start();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            MobileLoginFragment.this.mo22768b();
            MobileLoginFragment.this.mo22761a(str);
            MobileLoginFragment.this.f8878Y.setEnabled(true);
        }
    }

    /* renamed from: i */
    private void m14622i() {
        if (((LoginActivity) mo22771c()).mo22741l()) {
            if (RegexUtils.m1235a(this.f8876W.getText()) || C4222l.m13458a(this.f8876W.getText())) {
                mo22776h();
                if (this.f8879Z == null) {
                    this.f8879Z = C4222l.m13452a(this.f8878Y, 60, getString(R$string.send_code), getString(R$string.code_time_text));
                }
                this.f8878Y.setEnabled(false);
                new HvApiGetSmsCode().request(this.f8876W.getText().toString(), new C4483a());
                return;
            }
            mo22761a(getString(R$string.err_mobile));
        }
    }

    /* renamed from: j */
    public static MobileLoginFragment m14623j() {
        Bundle bundle = new Bundle();
        MobileLoginFragment mobileLoginFragment = new MobileLoginFragment();
        mobileLoginFragment.setArguments(bundle);
        return mobileLoginFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fragment_login_mobile;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.btnLogin) {
            ((LoginActivity) mo22771c()).mo24445a(this.f8876W, this.f8877X);
        } else if (id == R$id.tvCode) {
            m14622i();
        } else if (id == R$id.btnGoogleLogin) {
            ((LoginActivity) mo22771c()).mo23014v();
        } else if (id == R$id.btnFacebookLogin) {
            ((LoginActivity) mo22771c()).mo23013u();
        } else if (id == R$id.btnLineLogin) {
            ((LoginActivity) mo22771c()).mo23015w();
        } else if (id == R$id.btnWechatLogin) {
            ((LoginActivity) mo22771c()).mo24450z();
        }
    }

    public void onDestroy() {
        CountDownTimer countDownTimer = this.f8879Z;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        this.f8876W = (EditText) view.findViewById(R$id.etAccount);
        this.f8877X = (EditText) view.findViewById(R$id.etCode);
        this.f8878Y = (TextView) view.findViewById(R$id.tvCode);
        String lowerCase = HvAppConfig.m13024a().mo24165a("fs-thirdPart").toLowerCase();
        if (!lowerCase.contains("fb")) {
            view.findViewById(R$id.btnFacebookLogin).setVisibility(8);
        }
        if (!lowerCase.contains("line")) {
            view.findViewById(R$id.btnLineLogin).setVisibility(8);
        }
        if (!lowerCase.contains("wx")) {
            view.findViewById(R$id.btnWechatLogin).setVisibility(8);
        }
        if (!lowerCase.contains("gp")) {
            view.findViewById(R$id.btnGoogleLogin).setVisibility(8);
        }
        mo22770b(R$id.btnLogin, R$id.tvCode, R$id.btnGoogleLogin, R$id.btnFacebookLogin, R$id.btnLineLogin, R$id.btnWechatLogin);
    }
}
