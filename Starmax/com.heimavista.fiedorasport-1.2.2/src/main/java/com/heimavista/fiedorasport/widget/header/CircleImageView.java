package com.heimavista.fiedorasport.widget.header;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.widget.ImageView;

@SuppressLint({"ViewConstructor"})
public class CircleImageView extends ImageView {

    /* renamed from: P */
    int f9000P;

    /* renamed from: com.heimavista.fiedorasport.widget.header.CircleImageView$a */
    protected class C4496a extends OvalShape {

        /* renamed from: P */
        protected RadialGradient f9001P;

        /* renamed from: Q */
        protected Paint f9002Q = new Paint();

        protected C4496a(int i) {
            CircleImageView.this.f9000P = i;
            mo24844a((int) super.rect().width());
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo24844a(int i) {
            float f = ((float) i) / 2.0f;
            this.f9001P = new RadialGradient(f, f, (float) CircleImageView.this.f9000P, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.f9002Q.setShader(this.f9001P);
        }

        public void draw(Canvas canvas, Paint paint) {
            CircleImageView circleImageView = CircleImageView.this;
            float width = ((float) circleImageView.getWidth()) / 2.0f;
            float height = ((float) circleImageView.getHeight()) / 2.0f;
            canvas.drawCircle(width, height, width, this.f9002Q);
            canvas.drawCircle(width, height, width - ((float) CircleImageView.this.f9000P), paint);
        }

        /* access modifiers changed from: protected */
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            mo24844a((int) f);
        }
    }

    public CircleImageView(Context context, int i) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f = getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (0.0f * f);
        this.f9000P = (int) (3.5f * f);
        if (Build.VERSION.SDK_INT >= 21) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            setElevation(f * 4.0f);
        } else {
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(new C4496a(this.f9000P));
            setLayerType(1, shapeDrawable2.getPaint());
            shapeDrawable2.getPaint().setShadowLayer((float) this.f9000P, (float) i3, (float) i2, 503316480);
            int i4 = this.f9000P;
            setPadding(i4, i4, i4, i4);
            shapeDrawable = shapeDrawable2;
        }
        shapeDrawable.getPaint().setColor(i);
        if (Build.VERSION.SDK_INT >= 16) {
            setBackground(shapeDrawable);
        } else {
            setBackgroundDrawable(shapeDrawable);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (Build.VERSION.SDK_INT < 21) {
            super.setMeasuredDimension(getMeasuredWidth() + (this.f9000P * 2), getMeasuredHeight() + (this.f9000P * 2));
        }
    }
}
