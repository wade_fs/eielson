package com.heimavista.fiedorasport.p199j;

import android.text.TextUtils;
import android.util.ArrayMap;
import com.tencent.mmkv.MMKV;

/* renamed from: com.heimavista.fiedorasport.j.h */
public final class MMKVUtils {

    /* renamed from: a */
    private static MMKV f8042a;

    /* renamed from: b */
    private static ArrayMap<String, MMKV> f8043b = new ArrayMap<>();

    /* renamed from: a */
    public static MMKV m13424a(String str) {
        if (TextUtils.isEmpty(str)) {
            return f8042a;
        }
        if (f8043b.containsKey(str)) {
            return f8043b.get(str);
        }
        MMKV e = MMKV.m17083e(str);
        f8043b.put(str, e);
        return e;
    }
}
