package com.heimavista.fiedorasport.p111ui.fragment.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.TextureMapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.google.android.gms.maps.C2878a;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.model.MarkerOptions;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.p199j.C4222l;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
public class MapFragment extends Fragment {

    /* renamed from: P */
    private TextureMapView f8887P;

    /* renamed from: Q */
    private AMap f8888Q;

    /* renamed from: R */
    private MapView f8889R;

    /* renamed from: S */
    private C2880c f8890S;

    /* renamed from: T */
    private boolean f8891T = true;

    /* renamed from: U */
    private boolean f8892U = false;

    /* renamed from: V */
    private float f8893V = 15.0f;

    /* renamed from: W */
    private C4488a f8894W;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.map.MapFragment$a */
    public interface C4488a<T> {
        /* renamed from: a */
        void mo24442a(T t);
    }

    /* renamed from: a */
    public static MapFragment m14638a(boolean z, boolean z2) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("useGMap", z);
        bundle.putBoolean("myLocationEnable", z2);
        if (z) {
            GoogleMapOptions googleMapOptions = new GoogleMapOptions();
            googleMapOptions.mo18317d(1).mo18326k(true).mo18327l(true).mo18314b(false).mo18320e(false).mo18321f(true).mo18323h(false);
            bundle.putParcelable("MapOptions", googleMapOptions);
        } else {
            AMapOptions aMapOptions = new AMapOptions();
            aMapOptions.mapType(1).zoomControlsEnabled(true).zoomGesturesEnabled(true).compassEnabled(false).scaleControlsEnabled(true).rotateGesturesEnabled(false).scrollGesturesEnabled(true).tiltGesturesEnabled(false);
            AMapLocation lastKnownLocation = new AMapLocationClient(HvApp.m13010c()).getLastKnownLocation();
            if (lastKnownLocation != null) {
                aMapOptions.camera(CameraPosition.builder().target(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())).build());
            }
            bundle.putParcelable("MapOptions", aMapOptions);
        }
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        return mapFragment;
    }

    /* renamed from: b */
    public void mo24730b() {
        C2880c cVar = this.f8890S;
        if (cVar != null) {
            cVar.mo18401a();
        }
        AMap aMap = this.f8888Q;
        if (aMap != null) {
            aMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        AMapOptions aMapOptions;
        GoogleMapOptions googleMapOptions = null;
        if (getArguments() != null) {
            this.f8891T = getArguments().getBoolean("useGMap", true);
            this.f8892U = getArguments().getBoolean("myLocationEnable", false);
            if (this.f8891T) {
                googleMapOptions = (GoogleMapOptions) getArguments().getParcelable("MapOptions");
                aMapOptions = null;
            } else {
                aMapOptions = (AMapOptions) getArguments().getParcelable("MapOptions");
            }
        } else {
            aMapOptions = null;
        }
        if (this.f8891T) {
            View inflate = layoutInflater.inflate(R$layout.layout_gmap, viewGroup, false);
            this.f8889R = (MapView) inflate.findViewById(R$id.gMapView);
            this.f8889R.mo18363a(bundle);
            this.f8889R.mo18364a(new C4489a(this, googleMapOptions));
            return inflate;
        }
        View inflate2 = layoutInflater.inflate(R$layout.layout_amap, viewGroup, false);
        this.f8887P = inflate2.findViewById(R$id.aMapView);
        this.f8887P.onCreate(bundle);
        this.f8888Q = this.f8887P.getMap();
        AMap aMap = this.f8888Q;
        if (aMap != null) {
            aMap.moveCamera(CameraUpdateFactory.zoomTo(this.f8893V));
            if (this.f8892U) {
                if (aMapOptions != null) {
                    this.f8888Q.getUiSettings().setZoomControlsEnabled(aMapOptions.getZoomControlsEnabled());
                    this.f8888Q.getUiSettings().setCompassEnabled(aMapOptions.getCompassEnabled());
                    if (aMapOptions.getCamera() != null) {
                        this.f8888Q.moveCamera(CameraUpdateFactory.newCameraPosition(aMapOptions.getCamera()));
                    }
                }
                this.f8888Q.getUiSettings().setMyLocationButtonEnabled(true);
                this.f8888Q.setMyLocationEnabled(true);
            }
        }
        C4488a aVar = this.f8894W;
        if (aVar == null) {
            return inflate2;
        }
        aVar.mo24442a(this.f8888Q);
        return inflate2;
    }

    public void onDestroy() {
        TextureMapView textureMapView = this.f8887P;
        if (textureMapView != null) {
            textureMapView.onDestroy();
        }
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18362a();
        }
        super.onDestroy();
    }

    public void onLowMemory() {
        TextureMapView textureMapView = this.f8887P;
        if (textureMapView != null) {
            textureMapView.onLowMemory();
        }
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18365b();
        }
        super.onLowMemory();
    }

    public void onPause() {
        super.onPause();
        TextureMapView textureMapView = this.f8887P;
        if (textureMapView != null) {
            textureMapView.onPause();
        }
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18367c();
        }
    }

    public void onResume() {
        super.onResume();
        TextureMapView textureMapView = this.f8887P;
        if (textureMapView != null) {
            textureMapView.onResume();
        }
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18368d();
        }
    }

    public void onSaveInstanceState(@NonNull Bundle bundle) {
        TextureMapView textureMapView = this.f8887P;
        if (textureMapView != null) {
            textureMapView.onSaveInstanceState(bundle);
        }
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18366b(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    public void onStart() {
        super.onStart();
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18369e();
        }
    }

    public void onStop() {
        MapView mapView = this.f8889R;
        if (mapView != null) {
            mapView.mo18370f();
        }
        super.onStop();
    }

    public void setArguments(Bundle bundle) {
        try {
            super.setArguments(bundle);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: a */
    public <T> void mo24729a(C4488a aVar) {
        this.f8894W = aVar;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24727a(GoogleMapOptions googleMapOptions, C2880c cVar) {
        this.f8890S = cVar;
        cVar.mo18402a(C2879b.m8104a(this.f8893V));
        if (googleMapOptions != null) {
            cVar.mo18407b().mo18415c(googleMapOptions.mo18334y().booleanValue());
            cVar.mo18407b().mo18413a(googleMapOptions.mo18319d().booleanValue());
            if (googleMapOptions.mo18316c() != null) {
                cVar.mo18402a(C2879b.m8105a(googleMapOptions.mo18316c()));
            }
        }
        if (this.f8892U && C4222l.m13465d()) {
            cVar.mo18406a(true);
            cVar.mo18407b().mo18414b(true);
        }
        C4488a aVar = this.f8894W;
        if (aVar != null) {
            aVar.mo24442a(cVar);
        }
    }

    /* renamed from: a */
    public void mo24728a(C2878a aVar) {
        C2880c cVar = this.f8890S;
        if (cVar != null) {
            cVar.mo18402a(aVar);
        }
    }

    /* renamed from: a */
    public void mo24726a(CameraUpdate cameraUpdate) {
        AMap aMap = this.f8888Q;
        if (aMap != null) {
            aMap.moveCamera(cameraUpdate);
        }
    }

    /* renamed from: a */
    public C2932c mo24725a(MarkerOptions markerOptions) {
        C2880c cVar = this.f8890S;
        if (cVar != null) {
            return cVar.mo18399a(markerOptions);
        }
        return null;
    }

    /* renamed from: a */
    public Marker mo24724a(com.amap.api.maps.model.MarkerOptions markerOptions) {
        AMap aMap = this.f8888Q;
        if (aMap != null) {
            return aMap.addMarker(markerOptions);
        }
        return null;
    }
}
