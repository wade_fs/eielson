package com.heimavista.fiedorasport.p108g;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.widget.ImageView;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.widget.dialog.UIDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.fiedorasport.g.f */
public final class HeartRateDialog extends UIDialog<HeartRateDialog> {
    public HeartRateDialog(Context context) {
        super(context);
        mo25419c(R$layout.dialog_measure_heartrate);
        mo25416b(AnimAction.f7973c);
        mo25422d(17);
        mo25427g((ScreenUtils.m1265c() * 7) / 10);
        ImageView imageView = (ImageView) findViewById(R$id.ivHeartRate);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(imageView, "scaleX", 0.8f, 1.2f);
        ofFloat.setRepeatCount(-1);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(imageView, "scaleY", 0.8f, 1.2f);
        ofFloat2.setRepeatCount(-1);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ofFloat).with(ofFloat2);
        animatorSet.setDuration(1000L);
        animatorSet.start();
    }
}
