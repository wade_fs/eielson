package com.heimavista.fiedorasport.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.viewpager.widget.ViewPager;

public class NoScrollViewPager extends ViewPager {

    /* renamed from: P */
    private boolean f8958P;

    public NoScrollViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.f8958P) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f8958P) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void setCurrentItem(int i) {
        super.setCurrentItem(i, false);
    }

    public void setScroll(boolean z) {
        this.f8958P = z;
    }

    public NoScrollViewPager(Context context) {
        super(context);
    }
}
