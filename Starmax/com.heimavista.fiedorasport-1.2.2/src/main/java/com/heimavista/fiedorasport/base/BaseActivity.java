package com.heimavista.fiedorasport.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import com.blankj.swipepanel.SwipePanel;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.account.HvApiSignout;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.band.TickType;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.BleDataCallback;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p110i.Toast;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.login.LoginActivity;
import com.heimavista.fiedorasport.p111ui.activity.sport.SportActivity;
import com.heimavista.fiedorasport.p199j.AlarmMediaPlayer;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.service.JYBleService;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.LoadingDialog;
import com.heimavista.widget.dialog.MessageDialog;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;

public abstract class BaseActivity extends AppCompatActivity implements C3734y, View.OnClickListener {

    /* renamed from: a0 */
    private static final Handler f6406a0 = new Handler(Looper.getMainLooper());

    /* renamed from: P */
    private BroadcastReceiver f6407P;

    /* renamed from: Q */
    private boolean f6408Q = false;

    /* renamed from: R */
    private TextView f6409R;

    /* renamed from: S */
    private C3680b f6410S;

    /* renamed from: T */
    private int f6411T;

    /* renamed from: U */
    private String f6412U;

    /* renamed from: V */
    private long f6413V;

    /* renamed from: W */
    private Toast f6414W;

    /* renamed from: X */
    private BaseDialog f6415X;

    /* renamed from: Y */
    private int f6416Y = 0;

    /* renamed from: Z */
    private BaseDialog f6417Z;

    /* renamed from: com.heimavista.fiedorasport.base.BaseActivity$a */
    class C3679a extends BroadcastReceiver {
        C3679a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, ?[OBJECT, ARRAY]]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (MemberControl.f11153e.equals(action)) {
                BaseActivity.this.mo22747p();
            }
            if (MemberControl.f11154f.equals(action)) {
                BaseActivity.this.mo22748q();
            }
            if (MemberControl.f11156h.equals(action)) {
                Constant.m10311a("登出成功");
                BaseActivity.this.mo22722b(intent.hasExtra("errMsg") ? intent.getStringExtra("errMsg") : "");
            }
            if (HvApiSignout.ACTION_FAILED.equals(action)) {
                Constant.m10311a("登出失敗");
                BaseActivity.this.mo22717b();
                BaseActivity baseActivity = BaseActivity.this;
                baseActivity.mo22730c(baseActivity.getString(R$string.logout_failed));
            }
            if (MemberControl.f11155g.equals(action)) {
                BaseActivity.this.mo22733e();
            }
            if (HvApiBasic.ACTION_OTHER_BIND.equals(action)) {
                BaseActivity.this.mo22717b();
                String stringExtra = intent.getStringExtra(NotificationCompat.CATEGORY_MESSAGE);
                if (!TextUtils.isEmpty(stringExtra)) {
                    BaseActivity baseActivity2 = BaseActivity.this;
                    baseActivity2.mo22707a(baseActivity2.getString(R$string.warn), stringExtra, BaseActivity.this.getString(R$string.confirm), true, false, (BaseDialog.C4621m) null);
                }
            }
            BaseActivity.this.mo22691a(context, intent);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseActivity$b */
    public interface C3680b {
        void onActivityResult(int i, @Nullable Intent intent);
    }

    /* renamed from: u */
    private BroadcastReceiver mo23013u() {
        if (this.f6407P == null) {
            this.f6407P = new C3679a();
        }
        return this.f6407P;
    }

    /* renamed from: v */
    private void mo23014v() {
        if (mo22736h() != null) {
            SwipePanel swipePanel = new SwipePanel(this);
            swipePanel.setLeftDrawable(R$mipmap.ic_arrow_left);
            swipePanel.setLeftSwipeColor(17170445);
            swipePanel.setLeftEdgeSize(SizeUtils.m1272a(60.0f));
            swipePanel.setLeftEnabled(true);
            swipePanel.mo9685a(mo22736h());
            swipePanel.setOnFullSwipeListener(new C3689b(this, swipePanel));
        }
    }

    /* renamed from: w */
    private void mo23015w() {
        List<String> g = mo22735g();
        if (!g.isEmpty()) {
            int size = g.size();
            String[] strArr = new String[size];
            for (int i = 0; i < size; i++) {
                strArr[i] = g.get(i);
            }
            HvApp.m13010c().mo24156a(mo23013u(), strArr);
        }
    }

    /* renamed from: a */
    public void mo22682a(int i, int i2) {
    }

    /* renamed from: a */
    public void mo22683a(int i, int i2, long j) {
    }

    /* renamed from: a */
    public void mo22684a(int i, String str, int i2, int i3) {
    }

    /* renamed from: a */
    public void mo22685a(@IdRes int i, String str, String str2) {
        if (BandConfig.m10064m().mo22561f() && !BandConfig.m10064m().mo22558c().contains(str) && ((FrameLayout) findViewById(i)) != null) {
            getSupportFragmentManager().beginTransaction().replace(i, HvGad.m14915i().mo25016f().mo25037b(str, new HvGadViewConfig.C4502b(ScreenUtils.m1265c(), -2).mo24895a()), str2).commit();
        }
    }

    /* renamed from: a */
    public void mo22686a(int i, String str, String str2, String str3, int i2) {
    }

    /* renamed from: a */
    public /* synthetic */ void mo22687a(long j, int i) {
        BleDataCallback.m10569a(this, j, i);
    }

    /* renamed from: a */
    public void mo22688a(long j, int i, int i2) {
    }

    /* renamed from: a */
    public void mo22689a(long j, int i, int i2, int i3) {
    }

    /* renamed from: a */
    public void mo22690a(long j, int i, int i2, int i3, int i4) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo22697a(Bundle bundle);

    /* renamed from: a */
    public void mo22705a(String str) {
    }

    /* renamed from: a */
    public void mo22706a(String str, String str2, int i) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        if (Build.VERSION.SDK_INT > 24) {
            Configuration configuration = context.getResources().getConfiguration();
            configuration.fontScale = (((float) (BandDataManager.m10559c() - 1)) * 0.15f) + 1.0f;
            super.attachBaseContext(context.createConfigurationContext(configuration));
            return;
        }
        super.attachBaseContext(context);
    }

    /* renamed from: b */
    public void mo22718b(int i) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo22720b(Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22723b(int... iArr) {
        for (int i : iArr) {
            if (findViewById(i) != null) {
                mo22714a(findViewById(i));
            }
        }
    }

    /* renamed from: c */
    public <A extends BaseActivity> A mo22726c() {
        return this;
    }

    /* renamed from: c */
    public /* synthetic */ void mo22727c(int i) {
        BleDataCallback.m10568a(this, i);
    }

    /* renamed from: c */
    public void mo22729c(Class<? extends Activity> cls) {
        mo22703a(cls, ActivityOptionsCompat.makeSceneTransitionAnimation(this, new Pair[0]));
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22731d() {
        return R$mipmap.ic_back;
    }

    /* renamed from: e */
    public void mo22733e() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract int mo22734f();

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public List<String> mo22735g() {
        ArrayList arrayList = new ArrayList();
        if (mo22715a()) {
            arrayList.add(MemberControl.f11153e);
            arrayList.add(MemberControl.f11156h);
            arrayList.add(HvApiSignout.ACTION_FAILED);
            arrayList.add(MemberControl.f11155g);
            arrayList.add(MemberControl.f11154f);
            arrayList.add(HvApiBasic.ACTION_OTHER_BIND);
        }
        return arrayList;
    }

    public Resources getResources() {
        Resources resources = super.getResources();
        if (Build.VERSION.SDK_INT <= 24) {
            Configuration configuration = resources.getConfiguration();
            configuration.fontScale = (((float) (BandDataManager.m10559c() - 1)) * 0.15f) + 1.0f;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return resources;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return null;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager;
        if (view != null && (inputMethodManager = (InputMethodManager) getSystemService("input_method")) != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public int mo22738i() {
        return R$color.white;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return "";
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    /* renamed from: k */
    public int mo22740k() {
        return 0;
    }

    /* renamed from: l */
    public boolean mo22741l() {
        boolean c = NetworkUtils.m858c();
        if (!c) {
            mo22732d(R$string.net_err);
        }
        return c;
    }

    /* renamed from: m */
    public void mo22742m() {
        InputMethodManager inputMethodManager;
        View currentFocus = getCurrentFocus();
        if (currentFocus != null && (inputMethodManager = (InputMethodManager) getSystemService("input_method")) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    /* renamed from: n */
    public void mo22743n() {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R$layout.layout_fs_basic_action_bar, (ViewGroup) null);
        this.f6409R = (TextView) linearLayout.findViewById(R$id.tvScreenTitle);
        if (!TextUtils.isEmpty(mo22739j())) {
            this.f6409R.setText(mo22739j());
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            int d = mo22731d();
            if (d != 0) {
                supportActionBar.setHomeAsUpIndicator(d);
            }
            int i = mo22738i();
            if (i > 0) {
                supportActionBar.setBackgroundDrawable(getResources().getDrawable(i));
            }
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayUseLogoEnabled(false);
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setCustomView(linearLayout, new ActionBar.LayoutParams(-2, -2, 17));
            if (Build.VERSION.SDK_INT >= 21) {
                supportActionBar.setElevation(0.0f);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        C3680b bVar = this.f6410S;
        if (bVar == null || this.f6411T != i) {
            super.onActivityResult(i, i2, intent);
            return;
        }
        bVar.onActivityResult(i2, intent);
        this.f6410S = null;
    }

    public void onClick(View view) {
        if (view.getId() == R$id.ivBack) {
            onBackPressed();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void
     arg types: [com.heimavista.fiedorasport.base.BaseActivity, int]
     candidates:
      com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View
      com.blankj.utilcode.util.d.a(android.content.Context, int):android.view.View
      com.blankj.utilcode.util.d.a(android.view.Window, boolean):void
      com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View
     arg types: [com.heimavista.fiedorasport.base.BaseActivity, int]
     candidates:
      com.blankj.utilcode.util.d.a(android.content.Context, int):android.view.View
      com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void
      com.blankj.utilcode.util.d.a(android.view.Window, boolean):void
      com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        mo22750s();
        super.onCreate(bundle);
        int f = mo22734f();
        setContentView(f);
        LayoutInflater.from(this).inflate(f, (ViewGroup) null);
        BarUtils.m987a((Activity) this, true);
        if (mo22744o()) {
            BarUtils.m982a((Activity) this, getResources().getColor(17170445));
            View findViewById = findViewById(R$id.topLayoutContainer);
            if (findViewById != null) {
                findViewById.setPadding(0, BarUtils.m981a(), 0, 0);
            }
            ImageView imageView = (ImageView) findViewById(R$id.ivBack);
            if (imageView != null && (imageView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
                ((ViewGroup.MarginLayoutParams) imageView.getLayoutParams()).topMargin = BarUtils.m981a();
            }
        }
        mo23014v();
        mo22720b(bundle);
        mo22723b(R$id.ivBack);
        mo22697a(bundle);
        mo22743n();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22890b(getClass().getCanonicalName());
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10628d(getClass().getCanonicalName());
        }
        mo22717b();
        super.onDestroy();
        f6406a0.removeCallbacksAndMessages(this);
        mo22742m();
        if (isFinishing()) {
            setContentView(new View(this));
            System.gc();
            System.runFinalization();
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        TickManager.m10524b(TickType.PAUSE, System.currentTimeMillis());
        if (this.f6407P != null) {
            HvApp.m13010c().mo24155a(this.f6407P);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22880a(getClass().getCanonicalName(), this);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10606a(getClass().getCanonicalName(), this);
        }
        mo23015w();
        JYSDKManager.f6481z = this instanceof SportActivity;
        if (this.f6408Q) {
            AlarmMediaPlayer.m13385a();
            this.f6408Q = false;
        }
        if (mo22749r() && MemberControl.m17125s().mo27192q()) {
            long timeInMillis = Calendar.getInstance().getTimeInMillis();
            if (timeInMillis - TickManager.m10517a(TickType.PAUSE) > 1800000) {
                TickManager.m10524b(TickType.PAUSE, timeInMillis);
                finish();
                mo22702a(HomeActivity.class);
            }
        }
    }

    /* renamed from: p */
    public void mo22747p() {
    }

    /* renamed from: q */
    public void mo22748q() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public void mo22750s() {
        if (mo22740k() > 0 && Build.VERSION.SDK_INT >= 21) {
            getWindow().requestFeature(12);
            getWindow().setEnterTransition(TransitionInflater.from(this).inflateTransition(mo22740k()));
        }
    }

    public void startActivityForResult(Intent intent, int i, @Nullable Bundle bundle) {
        if (mo22724b(intent)) {
            super.startActivityForResult(intent, i, bundle);
        }
    }

    /* renamed from: t */
    public void mo22751t() {
        mo22712a(true);
    }

    /* renamed from: d */
    public void mo22732d(int i) {
        String str;
        try {
            str = getString(i);
        } catch (Exception unused) {
            str = String.valueOf(i);
        }
        mo22730c(str);
    }

    /* renamed from: c */
    public void mo22728c(Intent intent) {
        mo22694a(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this, new Pair[0]));
    }

    /* renamed from: b */
    public void mo22722b(String str) {
        MemberProfile.clearProfile(Platform.FACEBOOK, Platform.GOOGLE, Platform.LINE, Platform.WECHAT);
        FSManager.m10323r().mo22829b();
        FSManager.m10323r().mo22832c("");
        CRPBleManager.m10647r();
        JYSDKManager.m10388i().mo22884a(true);
        JYSDKManager.m10388i().mo22898f();
        JYSDKManager.m10388i().mo22900h();
        if (ServiceUtils.m1268a(JYBleService.class)) {
            ServiceUtils.m1271c(JYBleService.class);
        }
        LogUtils.m1139c("logout " + getClass().getSimpleName());
        if (!(this instanceof LoginActivity)) {
            ActivityUtils.m923a(HomeActivity.class);
            Intent intent = new Intent(this, LoginActivity.class);
            if (!TextUtils.isEmpty(str)) {
                intent.putExtra("logoutMsg", str);
            }
            mo22692a(intent);
        }
    }

    /* renamed from: c */
    public void mo22730c(String str) {
        if (!TextUtils.isEmpty(str)) {
            Toast bVar = this.f6414W;
            if (bVar == null) {
                this.f6414W = Toast.m10684a(this, str, 0);
            } else {
                bVar.mo22977a(str);
                this.f6414W.mo22975a(0);
            }
            if (!isFinishing()) {
                this.f6414W.mo22974a();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22714a(View... viewArr) {
        ClickUtils.m1045a(viewArr, this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22691a(Context context, Intent intent) {
        LogUtils.m1139c("mady>>>" + getClass().getSimpleName() + " onReceive:" + intent.getAction());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22701a(CharSequence charSequence) {
        TextView textView = (TextView) findViewById(R$id.toolbar_title);
        if (textView != null) {
            textView.setText(charSequence);
        }
        TextView textView2 = this.f6409R;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public MenuItem mo22680a(Menu menu, int i, String str, int i2, Drawable drawable, View.OnClickListener onClickListener) {
        View inflate = getLayoutInflater().inflate(R$layout.layout_menu_text, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R$id.tvMenu);
        if (textView != null) {
            textView.setText(str);
            textView.setTextColor(i2);
            if (drawable != null) {
                textView.setBackground(drawable);
            }
        }
        MenuItem add = menu.add(0, i, 0, str);
        add.setShowAsAction(1);
        add.setActionView(inflate);
        inflate.setOnClickListener(onClickListener);
        return add;
    }

    /* renamed from: b */
    public final boolean mo22725b(Runnable runnable, long j) {
        if (j < 0) {
            j = 0;
        }
        return mo22716a(runnable, SystemClock.uptimeMillis() + j);
    }

    /* renamed from: b */
    public void mo22721b(Class<? extends Activity> cls) {
        mo22692a(new Intent(this, cls));
    }

    /* renamed from: b */
    public void mo22719b(Intent intent, C3680b bVar) {
        mo22695a(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this, new Pair[0]), bVar);
    }

    /* renamed from: a */
    public final boolean mo22716a(Runnable runnable, long j) {
        return f6406a0.postAtTime(runnable, this, j);
    }

    /* renamed from: a */
    public void mo22702a(Class<? extends Activity> cls) {
        startActivity(new Intent(this, cls));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo22724b(Intent intent) {
        String str;
        boolean z = true;
        if (intent.getComponent() != null) {
            str = intent.getComponent().getClassName();
        } else {
            if (intent.getAction() != null) {
                str = intent.getAction();
            }
            return z;
        }
        if (str.equals(this.f6412U) && this.f6413V >= SystemClock.uptimeMillis() - 500) {
            z = false;
        }
        this.f6412U = str;
        this.f6413V = SystemClock.uptimeMillis();
        return z;
    }

    /* renamed from: a */
    public void mo22703a(Class<? extends Activity> cls, ActivityOptionsCompat activityOptionsCompat) {
        mo22694a(new Intent(this, cls), activityOptionsCompat);
    }

    /* renamed from: a */
    public void mo22694a(Intent intent, ActivityOptionsCompat activityOptionsCompat) {
        ContextCompat.startActivity(this, intent, activityOptionsCompat.toBundle());
    }

    /* renamed from: a */
    public void mo22692a(Intent intent) {
        startActivity(intent);
        finish();
    }

    /* renamed from: a */
    public void mo22704a(Class<? extends Activity> cls, C3680b bVar) {
        mo22693a(new Intent(this, cls), (Bundle) null, bVar);
    }

    /* renamed from: a */
    public void mo22696a(Intent intent, C3680b bVar) {
        mo22693a(intent, (Bundle) null, bVar);
    }

    /* renamed from: a */
    public void mo22695a(Intent intent, ActivityOptionsCompat activityOptionsCompat, C3680b bVar) {
        if (this.f6410S == null) {
            this.f6410S = bVar;
            this.f6411T = new Random().nextInt(255);
            ActivityCompat.startActivityForResult(this, intent, this.f6411T, activityOptionsCompat.toBundle());
        }
    }

    /* renamed from: b */
    public void mo22717b() {
        BaseDialog baseDialog;
        int i = this.f6416Y;
        if (i > 0) {
            this.f6416Y = i - 1;
        }
        if (this.f6416Y == 0 && (baseDialog = this.f6415X) != null && baseDialog.isShowing() && !isFinishing()) {
            this.f6415X.dismiss();
            this.f6415X = null;
        }
    }

    /* renamed from: a */
    public void mo22693a(Intent intent, @Nullable Bundle bundle, C3680b bVar) {
        if (this.f6410S == null) {
            this.f6410S = bVar;
            this.f6411T = new Random().nextInt(255);
            startActivityForResult(intent, this.f6411T, bundle);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void
     arg types: [java.lang.String, int, boolean, boolean, ?[OBJECT, ARRAY]]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(boolean, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.e$a):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void */
    /* renamed from: a */
    public void mo22712a(boolean z) {
        mo22711a("", false, z, z, (LoadingDialog.C4628a) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void
     arg types: [java.lang.String, int, boolean, boolean, ?[OBJECT, ARRAY]]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(boolean, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.e$a):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void */
    /* renamed from: a */
    public void mo22709a(String str, boolean z) {
        mo22711a(str, true, z, z, (LoadingDialog.C4628a) null);
    }

    /* renamed from: a */
    public void mo22711a(String str, boolean z, boolean z2, boolean z3, LoadingDialog.C4628a aVar) {
        this.f6416Y++;
        mo22725b(new C3691d(this, z, str, z2, z3, aVar), 300);
    }

    /* renamed from: a */
    public /* synthetic */ void mo22713a(boolean z, String str, boolean z2, boolean z3, LoadingDialog.C4628a aVar) {
        if (this.f6416Y > 0) {
            if (this.f6415X == null) {
                if (!z) {
                    str = "";
                }
                BaseDialog.C4610b a = LoadingDialog.m15413a(this, str);
                a.mo25413a(z2);
                this.f6415X = a.mo25418b();
                this.f6415X.setCanceledOnTouchOutside(z3);
                this.f6415X.mo25389a(new C3690c(this, aVar));
            }
            if (!this.f6415X.isShowing() && !isFinishing()) {
                this.f6415X.show();
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22700a(LoadingDialog.C4628a aVar, BaseDialog baseDialog) {
        if (aVar != null) {
            aVar.onDismiss(baseDialog);
        }
        this.f6415X = null;
    }

    /* renamed from: a */
    public void mo22710a(String str, boolean z, BaseDialog.C4621m mVar) {
        mo22708a(str, getString(R$string.confirm), z, mVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, boolean, int, com.heimavista.widget.dialog.BaseDialog$m]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* renamed from: a */
    public void mo22708a(String str, String str2, boolean z, BaseDialog.C4621m mVar) {
        mo22707a(getString(R$string.warn), str, str2, z, false, mVar);
    }

    /* renamed from: a */
    public void mo22707a(String str, String str2, String str3, boolean z, boolean z2, BaseDialog.C4621m mVar) {
        if (this.f6417Z == null) {
            MessageDialog jVar = new MessageDialog(this);
            jVar.mo25469c(str);
            MessageDialog jVar2 = jVar;
            jVar2.mo25465d(str2);
            jVar2.mo25470h(17039360);
            MessageDialog jVar3 = jVar2;
            jVar3.mo25468b(str3);
            MessageDialog jVar4 = jVar3;
            jVar4.mo25464a(mVar);
            jVar4.mo25427g((ScreenUtils.m1265c() * 7) / 10);
            MessageDialog jVar5 = jVar4;
            jVar5.mo25413a(z2);
            MessageDialog jVar6 = jVar5;
            jVar6.mo25411a(new C3688a(this));
            MessageDialog jVar7 = jVar6;
            if (z) {
                jVar7.mo25472i();
            }
            this.f6417Z = jVar7.mo25418b();
        }
        if (!isFinishing()) {
            this.f6417Z.show();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22699a(BaseDialog baseDialog) {
        this.f6417Z = null;
    }

    /* renamed from: a */
    public /* synthetic */ void mo22698a(SwipePanel swipePanel, int i) {
        swipePanel.mo9683a(i);
        onBackPressed();
    }

    /* renamed from: a */
    public void mo22681a(int i) {
        if (i == 1) {
            this.f6408Q = true;
            AlarmMediaPlayer.m13386a(this);
        }
        if (i == 4) {
            C4222l.m13455a(this);
        }
    }
}
