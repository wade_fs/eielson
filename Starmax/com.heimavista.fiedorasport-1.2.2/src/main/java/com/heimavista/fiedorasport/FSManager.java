package com.heimavista.fiedorasport;

import android.text.TextUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.service.JYBleService;
import com.heimavista.gad.HvGad;
import com.tencent.mmkv.MMKV;
import java.util.Calendar;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.e */
public class FSManager {

    /* renamed from: a */
    private MMKV f6457a;

    /* renamed from: com.heimavista.fiedorasport.e$b */
    /* compiled from: FSManager */
    private static class C3696b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static FSManager f6458a = new FSManager();
    }

    /* renamed from: r */
    public static FSManager m10323r() {
        return C3696b.f6458a;
    }

    /* renamed from: s */
    public static boolean m10324s() {
        return HvAppConfig.m13024a().mo24164a("mapType", 0).intValue() == 1;
    }

    /* renamed from: t */
    public static boolean m10325t() {
        return HvAppConfig.m13024a().mo24164a("mapType", 0).intValue() == 0;
    }

    /* renamed from: a */
    public boolean mo22828a() {
        MMKV e = MMKV.m17083e("app_config");
        if (e.mo27024a("versionCode", 0) >= 34) {
            return false;
        }
        e.mo27039b("versionCode", Math.max(AppUtils.m944b(), 34));
        return true;
    }

    /* renamed from: b */
    public void mo22830b(String str) {
        mo22835e().putString(m10322e("deviceName"), str);
    }

    /* renamed from: c */
    public void mo22832c(String str) {
        SPUtils.m1243c("member_data").mo9883b("memLoginType", str);
    }

    /* renamed from: d */
    public int mo22833d() {
        String g = mo22837g();
        if (TextUtils.isEmpty(g) || !g.contains("-")) {
            return 25;
        }
        try {
            Calendar instance = Calendar.getInstance();
            return instance.get(1) - Integer.parseInt(g.substring(0, g.indexOf("-")));
        } catch (NumberFormatException unused) {
            return 25;
        }
    }

    /* renamed from: e */
    public MMKV mo22835e() {
        if (this.f6457a == null) {
            this.f6457a = MMKV.m17083e("band_data");
        }
        return this.f6457a;
    }

    /* renamed from: f */
    public String mo22836f() {
        return mo22835e().getString(m10322e("bandId"), "");
    }

    /* renamed from: g */
    public String mo22837g() {
        String valueOf = String.valueOf(MemberControl.m17125s().mo27176c());
        if (TextUtils.isEmpty(valueOf) || valueOf.length() < 5) {
            return "";
        }
        String substring = valueOf.substring(0, 4);
        String substring2 = valueOf.substring(4);
        return substring + "-" + substring2;
    }

    /* renamed from: h */
    public float mo22838h() {
        float f = mo22835e().getFloat(m10322e("calCoefficient"), 0.598f);
        if (f == 0.0f) {
            return 0.598f;
        }
        return f;
    }

    /* renamed from: i */
    public String mo22839i() {
        return mo22835e().getString(m10322e("deviceName"), "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: j */
    public int mo22840j() {
        return MemberControl.m17125s().mo27172a().mo25323a(ViewHierarchyConstants.DIMENSION_HEIGHT_KEY, (Integer) 0).intValue();
    }

    /* renamed from: k */
    public String mo22841k() {
        return SPUtils.m1243c("member_data").mo9872a("memLoginType", "");
    }

    /* renamed from: l */
    public String mo22842l() {
        return mo22835e().getString(m10322e("macAddress"), "");
    }

    /* renamed from: m */
    public String mo22843m() {
        return MemberControl.m17125s().mo27172a().mo25325a("userId", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: n */
    public int mo22844n() {
        return MemberControl.m17125s().mo27172a().mo25323a("weight", (Integer) 0).intValue();
    }

    /* renamed from: o */
    public boolean mo22845o() {
        return !TextUtils.isEmpty(mo22841k()) && !"mobile".equals(mo22841k());
    }

    /* renamed from: p */
    public boolean mo22846p() {
        return "F15".equalsIgnoreCase(mo22839i());
    }

    /* renamed from: q */
    public boolean mo22847q() {
        return !mo22846p();
    }

    private FSManager() {
    }

    /* renamed from: b */
    public void mo22829b() {
        SPUtils.m1243c("crp").mo9873a();
        SPUtils.m1243c(AppUtils.m943a()).mo9873a();
        HvGad.m14915i().mo25017g();
        ServiceUtils.m1271c(JYBleService.class);
        mo22835e().clearAll();
    }

    /* renamed from: c */
    public void mo22831c() {
        String i = mo22839i();
        String l = mo22842l();
        if (mo22846p() && !TextUtils.isEmpty(l) && !CRPBleManager.m10639j()) {
            CRPBleManager.m10615b(mo22842l());
        }
        if (mo22847q() && !TextUtils.isEmpty(l) && !JYSDKManager.m10388i().mo22897e()) {
            JYSDKManager.m10388i().mo22882a(i, l, FSManager.class.getSimpleName());
        }
        LogUtils.m1139c("madyConnectBle " + i + " " + l + " crp=" + mo22846p() + " jyou=" + mo22847q() + " " + JYSDKManager.m10388i().mo22897e());
    }

    /* renamed from: e */
    private String m10322e(String str) {
        return MemberControl.m17125s().mo27187l() + "_" + str;
    }

    /* renamed from: a */
    public void mo22827a(String str) {
        mo22835e().putString(m10322e("bandId"), str);
    }

    /* renamed from: d */
    public void mo22834d(String str) {
        mo22835e().putString(m10322e("macAddress"), str);
    }
}
