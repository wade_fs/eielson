package com.heimavista.fiedorasport.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.service.JYBleService;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            boolean p = FSManager.m10323r().mo22846p();
            boolean q = FSManager.m10323r().mo22847q();
            String f = FSManager.m10323r().mo22836f();
            if (!TextUtils.isEmpty(f)) {
                if (FSManager.m10323r().mo22846p() && !CRPBleManager.m10639j()) {
                    CRPBleManager.m10645p();
                }
                if (FSManager.m10323r().mo22847q()) {
                    Class<JYBleService> cls = JYBleService.class;
                    if (!ServiceUtils.m1268a(cls)) {
                        Intent intent2 = new Intent(context, cls);
                        if (Build.VERSION.SDK_INT >= 26) {
                            context.startForegroundService(intent2);
                        } else {
                            context.startService(intent2);
                        }
                    }
                }
                LogUtils.m1139c("BootReceiver>>>", "crp=" + p, "jy=" + q, "id=" + f, "name=" + FSManager.m10323r().mo22839i(), "mac=" + FSManager.m10323r().mo22842l());
            }
        }
    }
}
