package com.heimavista.fiedorasport.widget.header;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import com.heimavista.fiedorasport.lib.R$styleable;
import com.scwang.smartrefresh.layout.internal.InternalAbstract;
import com.scwang.smartrefresh.layout.p206a.RefreshHeader;
import com.scwang.smartrefresh.layout.p206a.RefreshInternal;
import com.scwang.smartrefresh.layout.p206a.RefreshKernel;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import com.scwang.smartrefresh.layout.p207b.RefreshState;
import com.scwang.smartrefresh.layout.p207b.SpinnerStyle;
import com.scwang.smartrefresh.layout.p209d.SmartUtil;

public class MaterialHeader extends InternalAbstract implements RefreshHeader {

    /* renamed from: S */
    protected boolean f9004S;

    /* renamed from: T */
    protected int f9005T;

    /* renamed from: U */
    protected ImageView f9006U;

    /* renamed from: V */
    protected MaterialProgressDrawable f9007V;

    /* renamed from: W */
    protected int f9008W;

    /* renamed from: a0 */
    protected int f9009a0;

    /* renamed from: b0 */
    protected Path f9010b0;

    /* renamed from: c0 */
    protected Paint f9011c0;

    /* renamed from: d0 */
    protected RefreshState f9012d0;

    /* renamed from: e0 */
    protected boolean f9013e0;

    /* renamed from: f0 */
    protected boolean f9014f0;

    /* renamed from: com.heimavista.fiedorasport.widget.header.MaterialHeader$a */
    static /* synthetic */ class C4497a {

        /* renamed from: a */
        static final /* synthetic */ int[] f9015a = new int[RefreshState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.scwang.smartrefresh.layout.b.b[] r0 = com.scwang.smartrefresh.layout.p207b.RefreshState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.f9015a = r0
                int[] r0 = com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.f9015a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.f9015a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.PullDownToRefresh     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.f9015a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.ReleaseToRefresh     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.f9015a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.scwang.smartrefresh.layout.b.b r1 = com.scwang.smartrefresh.layout.p207b.RefreshState.Refreshing     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.widget.header.MaterialHeader.C4497a.<clinit>():void");
        }
    }

    public MaterialHeader(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i
     arg types: [com.heimavista.fiedorasport.widget.header.MaterialHeader, int]
     candidates:
      com.scwang.smartrefresh.layout.a.i.a(int, boolean):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, int):com.scwang.smartrefresh.layout.a.i
      com.scwang.smartrefresh.layout.a.i.a(com.scwang.smartrefresh.layout.a.h, boolean):com.scwang.smartrefresh.layout.a.i */
    /* renamed from: a */
    public void mo24848a(@NonNull RefreshKernel iVar, int i, int i2) {
        if (!this.f9013e0) {
            iVar.mo26106a((RefreshInternal) this, false);
        }
        if (isInEditMode()) {
            int i3 = i / 2;
            this.f9009a0 = i3;
            this.f9008W = i3;
        }
    }

    /* renamed from: b */
    public void mo24851b(@NonNull RefreshLayout jVar, int i, int i2) {
        this.f9007V.start();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.f9013e0) {
            this.f9010b0.reset();
            this.f9010b0.lineTo(0.0f, (float) this.f9009a0);
            this.f9010b0.quadTo(((float) getMeasuredWidth()) / 2.0f, ((float) this.f9009a0) + (((float) this.f9008W) * 1.9f), (float) getMeasuredWidth(), (float) this.f9009a0);
            this.f9010b0.lineTo((float) getMeasuredWidth(), 0.0f);
            canvas.drawPath(this.f9010b0, this.f9011c0);
        }
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        if (getChildCount() != 0) {
            ImageView imageView = this.f9006U;
            int measuredWidth = getMeasuredWidth();
            int measuredWidth2 = imageView.getMeasuredWidth();
            int measuredHeight = imageView.getMeasuredHeight();
            if (!isInEditMode() || (i5 = this.f9009a0) <= 0) {
                int i6 = measuredWidth / 2;
                int i7 = measuredWidth2 / 2;
                imageView.layout(i6 - i7, -measuredHeight, i6 + i7, 0);
                return;
            }
            int i8 = i5 - (measuredHeight / 2);
            int i9 = measuredWidth / 2;
            int i10 = measuredWidth2 / 2;
            imageView.layout(i9 - i10, i8, i9 + i10, measuredHeight + i8);
            this.f9007V.mo24861a(true);
            this.f9007V.mo24858a(0.0f, 0.8f);
            this.f9007V.mo24857a(1.0f);
            imageView.setAlpha(1.0f);
            imageView.setVisibility(0);
        }
    }

    public void onMeasure(int i, int i2) {
        super.setMeasuredDimension(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2));
        this.f9006U.measure(View.MeasureSpec.makeMeasureSpec(this.f9005T, 1073741824), View.MeasureSpec.makeMeasureSpec(this.f9005T, 1073741824));
    }

    @Deprecated
    public void setPrimaryColors(@ColorInt int... iArr) {
        if (iArr.length > 0) {
            this.f9011c0.setColor(iArr[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public MaterialHeader(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.f9013e0 = false;
        this.f9014f0 = true;
        super.f9853Q = SpinnerStyle.f9746f;
        setMinimumHeight(SmartUtil.m15574a(100.0f));
        this.f9007V = new MaterialProgressDrawable(this);
        this.f9007V.mo24860a(-328966);
        this.f9007V.setAlpha(255);
        this.f9007V.mo24862a(-16737844, -48060, -10053376, -5609780, -30720);
        this.f9006U = new CircleImageView(context, -328966);
        this.f9006U.setImageDrawable(this.f9007V);
        this.f9006U.setAlpha(0.0f);
        addView(this.f9006U);
        this.f9005T = (int) (getResources().getDisplayMetrics().density * 40.0f);
        this.f9010b0 = new Path();
        this.f9011c0 = new Paint();
        this.f9011c0.setAntiAlias(true);
        this.f9011c0.setStyle(Paint.Style.FILL);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.MaterialHeader);
        this.f9013e0 = obtainStyledAttributes.getBoolean(R$styleable.MaterialHeader_mhShowBezierWave, this.f9013e0);
        this.f9014f0 = obtainStyledAttributes.getBoolean(R$styleable.MaterialHeader_mhScrollableWhenRefreshing, this.f9014f0);
        this.f9011c0.setColor(obtainStyledAttributes.getColor(R$styleable.MaterialHeader_mhPrimaryColor, -15614977));
        if (obtainStyledAttributes.hasValue(R$styleable.MaterialHeader_mhShadowRadius)) {
            int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.MaterialHeader_mhShadowRadius, 0);
            this.f9011c0.setShadowLayer((float) dimensionPixelOffset, 0.0f, 0.0f, obtainStyledAttributes.getColor(R$styleable.MaterialHeader_mhShadowColor, ViewCompat.MEASURED_STATE_MASK));
            setLayerType(1, null);
        }
        int color = obtainStyledAttributes.getColor(R$styleable.MaterialHeader_mhProgressColor, 0);
        if (color != 0) {
            this.f9007V.mo24862a(color);
        }
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* renamed from: a */
    public void mo24850a(boolean z, float f, int i, int i2, int i3) {
        if (this.f9012d0 != RefreshState.Refreshing) {
            if (this.f9013e0) {
                this.f9009a0 = Math.min(i, i2);
                this.f9008W = Math.max(0, i - i2);
                postInvalidate();
            }
            if (z || (!this.f9007V.isRunning() && !this.f9004S)) {
                if (this.f9012d0 != RefreshState.Refreshing) {
                    float f2 = (float) i2;
                    float max = (((float) Math.max(((double) Math.min(1.0f, Math.abs((((float) i) * 1.0f) / f2))) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
                    double max2 = (double) (Math.max(0.0f, Math.min((float) (Math.abs(i) - i2), f2 * 2.0f) / f2) / 4.0f);
                    this.f9007V.mo24861a(true);
                    this.f9007V.mo24858a(0.0f, Math.min(0.8f, max * 0.8f));
                    this.f9007V.mo24857a(Math.min(1.0f, max));
                    this.f9007V.mo24863b((((max * 0.4f) - 16.0f) + (((float) (max2 - Math.pow(max2, 2.0d))) * 2.0f * 2.0f)) * 0.5f);
                }
                ImageView imageView = this.f9006U;
                float f3 = (float) i;
                imageView.setTranslationY(Math.min(f3, (f3 / 2.0f) + (((float) this.f9005T) / 2.0f)));
                imageView.setAlpha(Math.min(1.0f, (f3 * 4.0f) / ((float) this.f9005T)));
            }
        }
    }

    /* renamed from: a */
    public void mo24849a(@NonNull RefreshLayout jVar, @NonNull RefreshState bVar, @NonNull RefreshState bVar2) {
        ImageView imageView = this.f9006U;
        this.f9012d0 = bVar2;
        int i = C4497a.f9015a[bVar2.ordinal()];
        if (i == 1) {
            return;
        }
        if (i == 2) {
            this.f9004S = false;
            imageView.setVisibility(0);
            imageView.setTranslationY(0.0f);
            imageView.setScaleX(1.0f);
            imageView.setScaleY(1.0f);
        }
    }

    /* renamed from: a */
    public int mo24847a(@NonNull RefreshLayout jVar, boolean z) {
        ImageView imageView = this.f9006U;
        this.f9007V.stop();
        imageView.animate().scaleX(0.0f).scaleY(0.0f);
        this.f9004S = true;
        return 0;
    }
}
