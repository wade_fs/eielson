package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.View;
import com.heimavista.entity.data.SelectFriendBean;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.v */
/* compiled from: lambda */
public final /* synthetic */ class C4258v implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ SelectFriendsActivity f8138P;

    public /* synthetic */ C4258v(SelectFriendsActivity selectFriendsActivity) {
        this.f8138P = selectFriendsActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8138P.mo24406a(view, (SelectFriendBean) obj, i);
    }
}
