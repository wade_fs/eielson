package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.SportBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.l */
/* compiled from: lambda */
public final /* synthetic */ class C4422l implements View.OnLongClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListDynamicsAdapter f8682P;

    /* renamed from: Q */
    private final /* synthetic */ int f8683Q;

    /* renamed from: R */
    private final /* synthetic */ SportBean f8684R;

    public /* synthetic */ C4422l(ListDynamicsAdapter listDynamicsAdapter, int i, SportBean gVar) {
        this.f8682P = listDynamicsAdapter;
        this.f8683Q = i;
        this.f8684R = gVar;
    }

    public final boolean onLongClick(View view) {
        return this.f8682P.mo24639d(this.f8683Q, this.f8684R, view);
    }
}
