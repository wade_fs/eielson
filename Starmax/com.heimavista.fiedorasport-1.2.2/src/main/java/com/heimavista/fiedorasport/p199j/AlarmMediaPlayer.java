package com.heimavista.fiedorasport.p199j;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import com.blankj.utilcode.util.VibrateUtils;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.util.MimeTypes;
import java.io.IOException;

/* renamed from: com.heimavista.fiedorasport.j.c */
public class AlarmMediaPlayer {

    /* renamed from: a */
    private static MediaPlayer f8023a;

    /* renamed from: a */
    public static void m13386a(Context context) {
        if (f8023a == null) {
            VibrateUtils.m1058a(new long[]{0, 1000, 1000, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS, 1000}, 1);
            AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            RingtoneManager ringtoneManager = new RingtoneManager(context);
            ringtoneManager.setType(7);
            ringtoneManager.getCursor();
            Uri ringtoneUri = ringtoneManager.getRingtoneUri(7);
            audioManager.setStreamVolume(3, 7, 0);
            audioManager.setRingerMode(2);
            try {
                f8023a = new MediaPlayer();
                f8023a.setDataSource(context, ringtoneUri);
                f8023a.setAudioStreamType(3);
                f8023a.setLooping(true);
                f8023a.prepare();
                f8023a.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public static void m13385a() {
        VibrateUtils.m1057a();
        MediaPlayer mediaPlayer = f8023a;
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            f8023a.stop();
            f8023a.release();
            f8023a = null;
        }
    }
}
