package com.heimavista.fiedorasport.p111ui.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.PermissionUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiBindSocial;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.adapter.ListMenuAdapter;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.heimavista.thirdpart.ThirdPartyLoginFactory;
import com.heimavista.utils.ParamJsonData;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.BindSocialActivity */
public class BindSocialActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public ListMenuAdapter f8519b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public IThirdPartyLogin f8520c0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.BindSocialActivity$a */
    class C4376a implements PermissionUtils.C0922e {
        C4376a() {
        }

        /* renamed from: a */
        public void mo9866a() {
            BindSocialActivity.this.f8520c0.login(Platform.GOOGLE, BindSocialActivity.this.m14169d("gp#"));
        }

        /* renamed from: b */
        public void mo9867b() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.BindSocialActivity$b */
    class C4377b implements IThirdPartyLogin.Callback {

        /* renamed from: a */
        final /* synthetic */ String f8522a;

        C4377b(String str) {
            this.f8522a = str;
        }

        public void onCancel() {
            BindSocialActivity bindSocialActivity = BindSocialActivity.this;
            bindSocialActivity.mo22730c(bindSocialActivity.getString(17039360));
        }

        public void onComplete(MemberProfile memberProfile) {
            BindSocialActivity.this.m14165a(this.f8522a, memberProfile);
        }

        public void onError(String str) {
            BindSocialActivity bindSocialActivity = BindSocialActivity.this;
            bindSocialActivity.mo22730c(BindSocialActivity.this.getString(R$string.login_failed) + "\n" + str);
        }

        public void onStart() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.BindSocialActivity$c */
    class C4378c implements OnResultListener<Void> {

        /* renamed from: a */
        final /* synthetic */ String f8524a;

        C4378c(String str) {
            this.f8524a = str;
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            Constant.m10311a(String.format("第三方%s綁定成功", this.f8524a));
            BindSocialActivity.this.mo22717b();
            BindSocialActivity.this.f8519b0.mo22792b(BindSocialActivity.this.m14170u());
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            Constant.m10311a(String.format("第三方%s綁定失敗", this.f8524a));
            BindSocialActivity.this.mo22717b();
            if (TextUtils.isEmpty(str)) {
                str = BindSocialActivity.this.getString(R$string.err_bind_social);
            }
            BindSocialActivity.this.mo22730c(str);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public IThirdPartyLogin.Callback m14169d(String str) {
        return new C4377b(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public List<MenuItem> m14170u() {
        ArrayList arrayList = new ArrayList();
        boolean p = MemberControl.m17125s().mo27191p();
        boolean m = MemberControl.m17125s().mo27188m();
        boolean n = MemberControl.m17125s().mo27189n();
        boolean o = MemberControl.m17125s().mo27190o();
        String lowerCase = HvAppConfig.m13024a().mo24165a("fs-thirdPart").toLowerCase();
        if (lowerCase.contains("wx")) {
            MenuItem.C3676b bVar = new MenuItem.C3676b();
            bVar.mo22663b(3);
            bVar.mo22657a(R$mipmap.icon_wechat);
            bVar.mo22664b(getString(R$string.wechat));
            bVar.mo22658a(m14166b(p));
            arrayList.add(bVar.mo22661a());
        }
        if (lowerCase.contains("fb")) {
            MenuItem.C3676b bVar2 = new MenuItem.C3676b();
            bVar2.mo22663b(3);
            bVar2.mo22657a(R$mipmap.icon_fb);
            bVar2.mo22664b(getString(R$string.facebook));
            bVar2.mo22658a(m14166b(m));
            arrayList.add(bVar2.mo22661a());
        }
        if (lowerCase.contains("gp")) {
            MenuItem.C3676b bVar3 = new MenuItem.C3676b();
            bVar3.mo22663b(3);
            bVar3.mo22657a(R$mipmap.icon_google);
            bVar3.mo22664b(getString(R$string.google));
            bVar3.mo22658a(m14166b(n));
            arrayList.add(bVar3.mo22661a());
        }
        if (lowerCase.contains("line")) {
            MenuItem.C3676b bVar4 = new MenuItem.C3676b();
            bVar4.mo22663b(3);
            bVar4.mo22657a(R$mipmap.icon_line_btn_base);
            bVar4.mo22664b(getString(R$string.line));
            bVar4.mo22658a(m14166b(o));
            arrayList.add(bVar4.mo22661a());
        }
        return arrayList;
    }

    /* renamed from: v */
    private void m14171v() {
        if (!C4222l.m13462b(this)) {
            mo22730c(getString(R$string.un_support_google));
            return;
        }
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.CONTACTS");
        b.mo9858a(new C4376a());
        b.mo9859a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return true;
    }

    /* renamed from: e */
    public void mo22733e() {
        super.mo22733e();
        this.f8519b0.mo22792b((List) m14170u());
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.member_profile);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.f8520c0.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8519b0 = new ListMenuAdapter(super);
        this.f8519b0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4388a(this));
        this.f8519b0.mo22792b((List) m14170u());
        ((RecyclerView) findViewById(R$id.recyclerView)).setAdapter(this.f8519b0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24575a(View view, MenuItem eVar, int i) {
        if (eVar.mo22650c() == R$mipmap.icon_wechat) {
            if (!MemberControl.m17125s().mo27191p()) {
                this.f8520c0.login(Platform.WECHAT, m14169d("wx#"));
            }
        } else if (eVar.mo22650c() == R$mipmap.icon_fb) {
            if (!MemberControl.m17125s().mo27188m()) {
                this.f8520c0.login(Platform.FACEBOOK, m14169d("fb#"));
            }
        } else if (eVar.mo22650c() == R$mipmap.icon_google) {
            if (!MemberControl.m17125s().mo27189n()) {
                m14171v();
            }
        } else if (eVar.mo22650c() == R$mipmap.icon_line_btn_base && !MemberControl.m17125s().mo27190o()) {
            this.f8520c0.login(Platform.LINE, m14169d("line#"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: b */
    private View m14166b(boolean z) {
        View inflate = LayoutInflater.from(this).inflate(R$layout.layout_accessory_social, (ViewGroup) null, false);
        TextView textView = (TextView) inflate.findViewById(R$id.tvStatus);
        textView.setTextColor(getResources().getColor(z ? R$color.font_color_hint : R$color.blue));
        textView.setText(z ? R$string.has_bind_social : R$string.unbind_social);
        return inflate;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        if (this.f8520c0 == null) {
            this.f8520c0 = ThirdPartyLoginFactory.create(this);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14165a(String str, MemberProfile memberProfile) {
        if (mo22741l()) {
            mo22751t();
            HvApiBindSocial.C3651a aVar = new HvApiBindSocial.C3651a(memberProfile.f9349id, memberProfile.name);
            if (!TextUtils.isEmpty(memberProfile.gender)) {
                aVar.mo22413a(memberProfile.gender);
            }
            if (!TextUtils.isEmpty(memberProfile.photoUrl)) {
                aVar.mo22415b(memberProfile.photoUrl);
            }
            if (!TextUtils.isEmpty(memberProfile.unionId)) {
                aVar.mo22416c(memberProfile.unionId);
            }
            new HvApiBindSocial().request(aVar, new C4378c(str.equals("fb#") ? "fb" : str.equals("gp#") ? "google" : str.equals("line#") ? "line" : ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE));
        }
    }
}
