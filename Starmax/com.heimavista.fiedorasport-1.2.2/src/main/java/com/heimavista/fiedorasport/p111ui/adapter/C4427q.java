package com.heimavista.fiedorasport.p111ui.adapter;

import com.heimavista.entity.data.MenuItem;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.q */
/* compiled from: lambda */
public final /* synthetic */ class C4427q implements SwitchButton.C4603d {

    /* renamed from: a */
    private final /* synthetic */ ListShareOptionAdapter f8693a;

    /* renamed from: b */
    private final /* synthetic */ MenuItem f8694b;

    /* renamed from: c */
    private final /* synthetic */ int f8695c;

    public /* synthetic */ C4427q(ListShareOptionAdapter listShareOptionAdapter, MenuItem eVar, int i) {
        this.f8693a = listShareOptionAdapter;
        this.f8694b = eVar;
        this.f8695c = i;
    }

    /* renamed from: a */
    public final void mo24507a(SwitchButton switchButton, boolean z) {
        this.f8693a.mo24652a(this.f8694b, this.f8695c, switchButton, z);
    }
}
