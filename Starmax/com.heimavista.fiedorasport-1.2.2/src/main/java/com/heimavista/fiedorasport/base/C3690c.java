package com.heimavista.fiedorasport.base;

import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.LoadingDialog;

/* renamed from: com.heimavista.fiedorasport.base.c */
/* compiled from: lambda */
public final /* synthetic */ class C3690c implements BaseDialog.C4618j {

    /* renamed from: P */
    private final /* synthetic */ BaseActivity f6444P;

    /* renamed from: Q */
    private final /* synthetic */ LoadingDialog.C4628a f6445Q;

    public /* synthetic */ C3690c(BaseActivity baseActivity, LoadingDialog.C4628a aVar) {
        this.f6444P = baseActivity;
        this.f6445Q = aVar;
    }

    /* renamed from: a */
    public final void mo22806a(BaseDialog baseDialog) {
        this.f6444P.mo22700a(this.f6445Q, baseDialog);
    }
}
