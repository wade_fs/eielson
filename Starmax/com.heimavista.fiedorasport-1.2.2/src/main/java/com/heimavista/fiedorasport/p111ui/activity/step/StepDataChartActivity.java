package com.heimavista.fiedorasport.p111ui.activity.step;

import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.blankj.utilcode.util.SpanUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.widget.MyMarkerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p139g.OnChartValueSelectedListener;
import p119e.p189e.p197e.p198a.ChartStepData;

/* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataChartActivity */
public class StepDataChartActivity extends BaseActivity {

    /* renamed from: b0 */
    private BarChart f8507b0;

    /* renamed from: c0 */
    private TextView f8508c0;

    /* renamed from: d0 */
    private TextView f8509d0;

    /* renamed from: e0 */
    private TextView f8510e0;

    /* renamed from: f0 */
    private TextView f8511f0;
    /* access modifiers changed from: private */

    /* renamed from: g0 */
    public List<ChartStepData> f8512g0 = new ArrayList();

    /* renamed from: h0 */
    private List<ChartStepData> f8513h0 = new ArrayList();

    /* renamed from: i0 */
    private List<ChartStepData> f8514i0 = new ArrayList();

    /* renamed from: j0 */
    private List<ChartStepData> f8515j0 = new ArrayList();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataChartActivity$a */
    class C4373a extends ValueFormatter {
        C4373a() {
        }

        /* renamed from: a */
        public String mo23221a(float f, AxisBase aVar) {
            int i = (int) f;
            if (i < StepDataChartActivity.this.f8512g0.size()) {
                return ((ChartStepData) StepDataChartActivity.this.f8512g0.get(i)).f8015b;
            }
            return super.mo23221a(f, aVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01be  */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m14148e(int r15) {
        /*
            r14 = this;
            int r0 = com.heimavista.fiedorasport.lib.R$id.rb1
            if (r15 != r0) goto L_0x0007
            r0 = 30
            goto L_0x0009
        L_0x0007:
            r0 = 12
        L_0x0009:
            int r1 = com.heimavista.fiedorasport.lib.R$id.rb2
            r2 = 6
            if (r15 != r1) goto L_0x000f
            r0 = 6
        L_0x000f:
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.util.Locale r3 = java.util.Locale.getDefault()
            java.lang.String r4 = "M.d"
            r1.<init>(r4, r3)
            java.text.SimpleDateFormat r3 = new java.text.SimpleDateFormat
            java.util.Locale r4 = java.util.Locale.getDefault()
            java.lang.String r5 = "M月d日"
            r3.<init>(r5, r4)
            java.text.SimpleDateFormat r4 = new java.text.SimpleDateFormat
            java.util.Locale r5 = java.util.Locale.getDefault()
            java.lang.String r6 = "M月"
            r4.<init>(r6, r5)
            int r5 = com.heimavista.fiedorasport.lib.R$id.rb1
            r6 = 2
            r7 = 1
            if (r15 != r5) goto L_0x009c
            java.util.List<e.e.e.a.b> r5 = r14.f8513h0
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x009c
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            int r5 = 1 - r0
            r4.add(r2, r5)
            r5 = r0
        L_0x0048:
            if (r5 <= 0) goto L_0x01d5
            e.e.e.a.b r8 = new e.e.e.a.b
            r8.<init>()
            int r9 = r5 + -1
            com.heimavista.entity.band.StepInfo r9 = p119e.p189e.p193d.p194i.StepDataDb.m13223f(r9)
            r8.f8014a = r9
            int r10 = r0 - r5
            float r10 = (float) r10
            r8.f8018e = r10
            int r9 = r9.f6245S
            float r9 = (float) r9
            r8.f8017d = r9
            if (r5 != r7) goto L_0x006a
            int r9 = com.heimavista.fiedorasport.lib.R$string.today
        L_0x0065:
            java.lang.String r9 = r14.getString(r9)
            goto L_0x0077
        L_0x006a:
            if (r5 != r6) goto L_0x006f
            int r9 = com.heimavista.fiedorasport.lib.R$string.yesterday
            goto L_0x0065
        L_0x006f:
            java.util.Date r9 = r4.getTime()
            java.lang.String r9 = r1.format(r9)
        L_0x0077:
            r8.f8015b = r9
            if (r5 != r7) goto L_0x0082
            int r9 = com.heimavista.fiedorasport.lib.R$string.today
        L_0x007d:
            java.lang.String r9 = r14.getString(r9)
            goto L_0x008f
        L_0x0082:
            if (r5 != r6) goto L_0x0087
            int r9 = com.heimavista.fiedorasport.lib.R$string.yesterday
            goto L_0x007d
        L_0x0087:
            java.util.Date r9 = r4.getTime()
            java.lang.String r9 = r3.format(r9)
        L_0x008f:
            r8.f8016c = r9
            java.util.List<e.e.e.a.b> r9 = r14.f8513h0
            r9.add(r8)
            r4.add(r2, r7)
            int r5 = r5 + -1
            goto L_0x0048
        L_0x009c:
            int r5 = com.heimavista.fiedorasport.lib.R$id.rb2
            if (r15 != r5) goto L_0x0168
            java.util.List<e.e.e.a.b> r5 = r14.f8514i0
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x0168
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r4.setFirstDayOfWeek(r6)
            int r8 = r4.getFirstDayOfWeek()
            r9 = 7
            r4.set(r9, r8)
            int r8 = 1 - r0
            r10 = 3
            r4.add(r10, r8)
            r5.setFirstDayOfWeek(r6)
            int r11 = r5.getFirstDayOfWeek()
            r5.set(r9, r11)
            r5.add(r10, r8)
            r8 = r0
        L_0x00cf:
            if (r8 <= 0) goto L_0x01d5
            if (r8 != r0) goto L_0x00d6
            r5.add(r9, r2)
        L_0x00d6:
            e.e.e.a.b r11 = new e.e.e.a.b
            r11.<init>()
            int r12 = r8 + -1
            com.heimavista.entity.band.StepInfo r12 = p119e.p189e.p193d.p194i.StepDataDb.m13224g(r12)
            r11.f8014a = r12
            int r13 = r0 - r8
            float r13 = (float) r13
            r11.f8018e = r13
            int r12 = r12.f6245S
            float r12 = (float) r12
            r11.f8017d = r12
            if (r8 != r7) goto L_0x00f6
            int r12 = com.heimavista.fiedorasport.lib.R$string.current_week
        L_0x00f1:
            java.lang.String r12 = r14.getString(r12)
            goto L_0x011f
        L_0x00f6:
            if (r8 != r6) goto L_0x00fb
            int r12 = com.heimavista.fiedorasport.lib.R$string.last_week
            goto L_0x00f1
        L_0x00fb:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.util.Date r13 = r4.getTime()
            java.lang.String r13 = r1.format(r13)
            r12.append(r13)
            java.lang.String r13 = "-"
            r12.append(r13)
            java.util.Date r13 = r5.getTime()
            java.lang.String r13 = r1.format(r13)
            r12.append(r13)
            java.lang.String r12 = r12.toString()
        L_0x011f:
            r11.f8015b = r12
            if (r8 != r7) goto L_0x012a
            int r12 = com.heimavista.fiedorasport.lib.R$string.current_week
        L_0x0125:
            java.lang.String r12 = r14.getString(r12)
            goto L_0x0157
        L_0x012a:
            if (r8 != r6) goto L_0x012f
            int r12 = com.heimavista.fiedorasport.lib.R$string.last_week
            goto L_0x0125
        L_0x012f:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.util.Date r13 = r4.getTime()
            java.lang.String r13 = r3.format(r13)
            r12.append(r13)
            int r13 = com.heimavista.fiedorasport.lib.R$string.f6601to
            java.lang.String r13 = r14.getString(r13)
            r12.append(r13)
            java.util.Date r13 = r5.getTime()
            java.lang.String r13 = r3.format(r13)
            r12.append(r13)
            java.lang.String r12 = r12.toString()
        L_0x0157:
            r11.f8016c = r12
            java.util.List<e.e.e.a.b> r12 = r14.f8514i0
            r12.add(r11)
            r4.add(r10, r7)
            r5.add(r10, r7)
            int r8 = r8 + -1
            goto L_0x00cf
        L_0x0168:
            int r1 = com.heimavista.fiedorasport.lib.R$id.rb3
            if (r15 != r1) goto L_0x01d5
            java.util.List<e.e.e.a.b> r1 = r14.f8515j0
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x01d5
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            int r2 = -r0
            r1.add(r6, r2)
            r2 = r0
        L_0x017d:
            if (r2 <= 0) goto L_0x01d5
            r1.add(r6, r7)
            r3 = 5
            r1.set(r3, r7)
            e.e.e.a.b r3 = new e.e.e.a.b
            r3.<init>()
            int r5 = r2 + -1
            com.heimavista.entity.band.StepInfo r5 = p119e.p189e.p193d.p194i.StepDataDb.m13222e(r5)
            r3.f8014a = r5
            int r8 = r0 - r2
            float r8 = (float) r8
            r3.f8018e = r8
            int r5 = r5.f6245S
            float r5 = (float) r5
            r3.f8017d = r5
            if (r2 != r7) goto L_0x01a6
            int r5 = com.heimavista.fiedorasport.lib.R$string.current_month
        L_0x01a1:
            java.lang.String r5 = r14.getString(r5)
            goto L_0x01b3
        L_0x01a6:
            if (r2 != r6) goto L_0x01ab
            int r5 = com.heimavista.fiedorasport.lib.R$string.last_month
            goto L_0x01a1
        L_0x01ab:
            java.util.Date r5 = r1.getTime()
            java.lang.String r5 = r4.format(r5)
        L_0x01b3:
            r3.f8015b = r5
            if (r2 != r7) goto L_0x01be
            int r5 = com.heimavista.fiedorasport.lib.R$string.current_month
        L_0x01b9:
            java.lang.String r5 = r14.getString(r5)
            goto L_0x01cb
        L_0x01be:
            if (r2 != r6) goto L_0x01c3
            int r5 = com.heimavista.fiedorasport.lib.R$string.last_month
            goto L_0x01b9
        L_0x01c3:
            java.util.Date r5 = r1.getTime()
            java.lang.String r5 = r4.format(r5)
        L_0x01cb:
            r3.f8016c = r5
            java.util.List<e.e.e.a.b> r5 = r14.f8515j0
            r5.add(r3)
            int r2 = r2 + -1
            goto L_0x017d
        L_0x01d5:
            r14.m14149f(r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p111ui.activity.step.StepDataChartActivity.m14148e(int):void");
    }

    /* renamed from: u */
    private void m14150u() {
        this.f8507b0.setBorderWidth(1.0f);
        this.f8507b0.mo13022a(0.0f, 12.0f, 0.0f, 12.0f);
        this.f8507b0.getAxisRight().mo13233a(false);
        this.f8507b0.getAxisLeft().mo13233a(false);
        this.f8507b0.setScaleEnabled(false);
        this.f8507b0.setDragEnabled(true);
        this.f8507b0.setTouchEnabled(true);
        this.f8507b0.getDescription().mo13233a(false);
        this.f8507b0.setNoDataText(getString(R$string.no_sleep_data));
        this.f8507b0.setNoDataTextColor(getResources().getColor(R$color.font_color));
        this.f8507b0.getLegend().mo13233a(false);
        this.f8507b0.setDrawValueAboveBar(false);
        MyMarkerView myMarkerView = new MyMarkerView(this, R$layout.custom_marker_view);
        myMarkerView.setChartView(this.f8507b0);
        this.f8507b0.setMarker(myMarkerView);
        XAxis xAxis = this.f8507b0.getXAxis();
        xAxis.mo13284a(XAxis.C1654a.BOTTOM);
        xAxis.mo13232a(Typeface.DEFAULT);
        xAxis.mo13204c(false);
        xAxis.mo13202b(false);
        xAxis.mo13231a(-1);
        xAxis.mo13230a(10.0f);
        xAxis.mo13206d(true);
        xAxis.mo13207e(1.0f);
        xAxis.mo13205d(-0.5f);
        xAxis.mo13200a(new C4373a());
        this.f8507b0.setOnChartValueSelectedListener(new C4374b());
    }

    /* renamed from: v */
    private void m14151v() {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        for (int i = 0; i < this.f8512g0.size(); i++) {
            ChartStepData bVar = this.f8512g0.get(i);
            if (bVar.f8014a.f6245S > 0) {
                z = true;
            }
            arrayList.add(new BarEntry(bVar.f8018e, bVar.f8017d));
        }
        if (!z) {
            this.f8507b0.getDescription().mo13233a(true);
        } else {
            this.f8507b0.getDescription().mo13233a(false);
            BarDataSet bVar2 = new BarDataSet(arrayList, "");
            bVar2.mo13342a(false);
            bVar2.mo13344b(false);
            bVar2.mo13343a(getResources().getColor(R$color.stepBarChart));
            bVar2.mo13334g(getResources().getColor(R$color.stepBarChart_selected));
            bVar2.mo13326h(PsExtractor.VIDEO_STREAM_MASK);
            bVar2.mo13347c(true);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(bVar2);
            BarData aVar = new BarData(arrayList2);
            aVar.mo13377a(10.0f);
            aVar.mo13379a(Typeface.DEFAULT);
            if (arrayList.size() < 10) {
                aVar.mo13322b(0.5f);
            } else {
                aVar.mo13322b(0.9f);
            }
            this.f8507b0.setData(aVar);
            this.f8507b0.mo13031b(1000);
        }
        this.f8507b0.invalidate();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8507b0 = (BarChart) findViewById(R$id.barChart);
        this.f8508c0 = (TextView) findViewById(R$id.tvWalk);
        this.f8509d0 = (TextView) findViewById(R$id.tvStart);
        this.f8510e0 = (TextView) findViewById(R$id.tvCenter);
        this.f8511f0 = (TextView) findViewById(R$id.tvEnd);
        ((RadioGroup) findViewById(R$id.rbLabel)).setOnCheckedChangeListener(new C4375a(this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_chart_step;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f8513h0.clear();
        this.f8514i0.clear();
        this.f8515j0.clear();
        this.f8512g0.clear();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public void mo22750s() {
        super.mo22750s();
    }

    /* renamed from: f */
    private void m14149f(int i) {
        if (i == R$id.rb1) {
            this.f8512g0 = this.f8513h0;
        } else if (i == R$id.rb2) {
            this.f8512g0 = this.f8514i0;
        } else {
            this.f8512g0 = this.f8515j0;
        }
        List<ChartStepData> list = this.f8512g0;
        m14145a(list.get(list.size() - 1).f8014a);
        int size = this.f8512g0.size();
        this.f8507b0.getXAxis().mo13203c(Math.min(12, size));
        Matrix matrix = new Matrix();
        matrix.postScale(((float) size) / 12.0f, 1.0f);
        this.f8507b0.getViewPortHandler().mo23444a(matrix, this.f8507b0, false);
        this.f8507b0.mo13023a(1000);
        this.f8507b0.mo12953a((float) (size - 1));
        m14151v();
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataChartActivity$b */
    class C4374b implements OnChartValueSelectedListener {
        C4374b() {
        }

        /* renamed from: a */
        public void mo23323a(Entry entry, Highlight dVar) {
            int d = (int) entry.mo13315d();
            StepDataChartActivity stepDataChartActivity = StepDataChartActivity.this;
            stepDataChartActivity.m14145a(((ChartStepData) stepDataChartActivity.f8512g0.get(d)).f8014a);
            StepDataChartActivity stepDataChartActivity2 = StepDataChartActivity.this;
            stepDataChartActivity2.mo22701a(((ChartStepData) stepDataChartActivity2.f8512g0.get(d)).f8016c);
        }

        /* renamed from: a */
        public void mo23322a() {
            StepDataChartActivity stepDataChartActivity = StepDataChartActivity.this;
            stepDataChartActivity.m14145a(((ChartStepData) stepDataChartActivity.f8512g0.get(StepDataChartActivity.this.f8512g0.size() - 1)).f8014a);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24573a(RadioGroup radioGroup, int i) {
        this.f8507b0.mo13034e();
        if (i == R$id.rb1) {
            Constant.m10311a("點擊日報表");
        } else if (i == R$id.rb2) {
            Constant.m10311a("點擊週報表");
        } else if (i == R$id.rb3) {
            Constant.m10311a("點擊月報表");
        }
        m14148e(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2003", "exad_step_inside");
        m14150u();
        m14148e(R$id.rb1);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14145a(StepInfo stepInfo) {
        String str;
        SpanUtils a = SpanUtils.m865a(this.f8508c0);
        a.mo9735a(getString(stepInfo.f6249W == 1 ? R$string.day_step : R$string.day_step_avg));
        a.mo9735a(" ");
        int i = stepInfo.f6249W;
        if (i == 0) {
            str = "--";
        } else {
            str = String.valueOf(stepInfo.f6245S / i);
        }
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_step));
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8509d0);
        a2.mo9735a(String.format(Locale.getDefault(), "%.2f", Double.valueOf(((double) stepInfo.f6246T) / 1000.0d)));
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a2.mo9738c();
        a2.mo9735a(getString(R$string.data_unit_mill));
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8510e0);
        a3.mo9735a(String.valueOf(stepInfo.f6247U));
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9738c();
        a3.mo9735a(getString(R$string.data_unit_cal));
        a3.mo9736b();
        int i2 = stepInfo.f6248V;
        SpanUtils a4 = SpanUtils.m865a(this.f8511f0);
        a4.mo9735a(String.valueOf(i2 / 60));
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_hour));
        a4.mo9735a(String.valueOf(i2 % 60));
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_min));
        a4.mo9736b();
        List<ChartStepData> list = this.f8512g0;
        mo22701a((CharSequence) list.get(list.size() - 1).f8016c);
    }
}
