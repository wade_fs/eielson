package com.heimavista.fiedorasport.p111ui.activity.location;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;
import androidx.core.view.InputDeviceCompat;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.PolylineOptions;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.C2931b;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.model.MarkerOptions;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.activity.location.LocationActivity */
public class LocationActivity extends BaseActivity {

    /* renamed from: b0 */
    private boolean f8168b0 = false;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public AMap f8169c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public C2880c f8170d0;

    /* renamed from: e0 */
    private TableLayout f8171e0;

    /* renamed from: f0 */
    private TextView f8172f0;

    /* renamed from: g0 */
    private TextView f8173g0;

    /* renamed from: h0 */
    private TextView f8174h0;

    /* renamed from: i0 */
    private TextView f8175i0;

    /* renamed from: j0 */
    private TextView f8176j0;

    /* renamed from: k0 */
    private TextView f8177k0;

    /* renamed from: l0 */
    private TextView f8178l0;

    /* renamed from: m0 */
    private TextView f8179m0;

    /* renamed from: n0 */
    private C2932c f8180n0;

    /* renamed from: o0 */
    private Marker f8181o0;

    /* renamed from: p0 */
    private String f8182p0 = "";

    /* renamed from: q0 */
    private int f8183q0;
    /* access modifiers changed from: private */

    /* renamed from: r0 */
    public String f8184r0 = "SunMoonLake.json";

    /* renamed from: s0 */
    private JSONArray f8185s0 = null;
    /* access modifiers changed from: private */

    /* renamed from: t0 */
    public int f8186t0 = 20;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.location.LocationActivity$a */
    class C4272a implements C4632i<String> {
        C4272a() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            if (i == 0 && !"SunMoonLake.json".equals(LocationActivity.this.f8184r0)) {
                String unused = LocationActivity.this.f8184r0 = "SunMoonLake.json";
                LocationActivity.this.m13680y();
            } else if (i == 1 && "SunMoonLake.json".equals(LocationActivity.this.f8184r0)) {
                String unused2 = LocationActivity.this.f8184r0 = "matchLine.json";
                LocationActivity.this.m13680y();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.location.LocationActivity$b */
    class C4273b extends ThreadUtils.C0877e<JSONObject> {
        C4273b() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(JSONObject jSONObject) {
            LogUtils.m1139c("result:" + jSONObject);
            List list = (List) jSONObject.opt("list");
            List list2 = (List) jSONObject.opt("matchList");
            LocationActivity.this.m13670a(jSONObject);
            if (LocationActivity.this.f8169c0 != null) {
                if (list2 != null) {
                    LocationActivity.this.f8169c0.addPolyline(new PolylineOptions().addAll(list2).color(-7829368).geodesic(true).width((float) LocationActivity.this.f8186t0).lineJoinType(PolylineOptions.LineJoinType.LineJoinRound));
                }
                if (list != null && list.size() > 0) {
                    LocationActivity.this.f8169c0.moveCamera(CameraUpdateFactory.newLatLngZoom((LatLng) list.get(list.size() - 1), 14.0f));
                    LocationActivity.this.m13665a((LatLng) list.get(list.size() - 1));
                    LocationActivity.this.f8169c0.addPolyline(new PolylineOptions().addAll(list).color((int) InputDeviceCompat.SOURCE_ANY).geodesic(true).lineJoinType(PolylineOptions.LineJoinType.LineJoinRound));
                }
            }
            LocationActivity.this.mo22717b();
        }

        /* renamed from: b */
        public JSONObject mo9798b() {
            return LocationActivity.this.m13677v();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.location.LocationActivity$c */
    class C4274c extends ThreadUtils.C0877e<JSONObject> {
        C4274c() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(JSONObject jSONObject) {
            LocationActivity.this.m13670a(jSONObject);
            if (LocationActivity.this.f8170d0 != null) {
                List list = (List) jSONObject.opt("list");
                List list2 = (List) jSONObject.opt("matchList");
                if (list2 != null) {
                    LocationActivity.this.f8170d0.mo18400a(new com.google.android.gms.maps.model.PolylineOptions().mo18578c(list2).mo18580d(-7829368).mo18576a(true).mo18581e(2));
                }
                if (list != null && list.size() > 0) {
                    LocationActivity.this.f8170d0.mo18402a(C2879b.m8107a((com.google.android.gms.maps.model.LatLng) list.get(list.size() - 1), 14.0f));
                    LocationActivity.this.m13666a((com.google.android.gms.maps.model.LatLng) list.get(list.size() - 1));
                    LocationActivity.this.f8170d0.mo18400a(new com.google.android.gms.maps.model.PolylineOptions().mo18578c(list).mo18580d(InputDeviceCompat.SOURCE_ANY).mo18576a(true).mo18574a((float) LocationActivity.this.f8186t0).mo18581e(2));
                }
            }
            LocationActivity.this.mo22717b();
        }

        /* renamed from: b */
        public JSONObject mo9798b() {
            return LocationActivity.this.m13677v();
        }
    }

    /* renamed from: u */
    private void m13676u() {
        JSONArray jSONArray = this.f8185s0;
        if (jSONArray != null && jSONArray.length() != 0) {
            mo22751t();
            if (this.f8168b0) {
                C2880c cVar = this.f8170d0;
                if (cVar != null) {
                    cVar.mo18401a();
                    this.f8180n0 = null;
                    com.google.android.gms.maps.model.LatLng latLng = new com.google.android.gms.maps.model.LatLng(this.f8185s0.optJSONObject(0).optDouble("lat"), this.f8185s0.optJSONObject(0).optDouble("lng"));
                    this.f8170d0.mo18402a(C2879b.m8107a(latLng, 16.0f));
                    this.f8170d0.mo18399a(new MarkerOptions().mo18546a(C2931b.m8342a()).mo18545a(latLng));
                }
                m13679x();
                return;
            }
            AMap aMap = this.f8169c0;
            if (aMap != null) {
                aMap.setMyLocationEnabled(false);
                this.f8169c0.getUiSettings().setMyLocationButtonEnabled(false);
                this.f8169c0.getUiSettings().setZoomControlsEnabled(false);
                this.f8169c0.clear();
                this.f8181o0 = null;
                LatLng latLng2 = new LatLng(this.f8185s0.optJSONObject(0).optDouble("lat"), this.f8185s0.optJSONObject(0).optDouble("lng"));
                this.f8169c0.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng2, 16.0f));
                this.f8169c0.addMarker(new com.amap.api.maps.model.MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng2));
            }
            m13678w();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v */
    public JSONObject m13677v() {
        String str;
        int i;
        double d;
        StepInfo stepInfo;
        String str2;
        JSONObject jSONObject = new JSONObject();
        int length = this.f8185s0.length();
        int i2 = 0;
        double d2 = 0.0d;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        while (true) {
            str = "distance";
            if (i2 >= length) {
                break;
            }
            JSONObject optJSONObject = this.f8185s0.optJSONObject(i2);
            if (this.f8168b0) {
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList();
                }
                arrayList2.add(new com.google.android.gms.maps.model.LatLng(optJSONObject.optDouble("lat"), optJSONObject.optDouble("lng")));
            } else {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(new LatLng(optJSONObject.optDouble("lat"), optJSONObject.optDouble("lng")));
            }
            d2 += optJSONObject.optDouble(str);
            i2++;
        }
        jSONObject.put("matchMiles", d2);
        if (this.f8168b0) {
            arrayList = arrayList2;
        }
        jSONObject.put("matchList", arrayList);
        StepInfo w = new StepDataDb().mo24279w();
        double d3 = ((double) (w.f6245S * this.f8183q0)) * 0.01d;
        jSONObject.put("stepMiles", d3);
        double d4 = d3 / d2;
        if (d4 > 0.0d) {
            i = (int) d4;
            d3 -= d2 * ((double) i);
        } else {
            i = 0;
        }
        int length2 = this.f8185s0.length();
        int i3 = 0;
        int i4 = 0;
        ArrayList arrayList3 = null;
        ArrayList arrayList4 = null;
        while (i3 < length2) {
            JSONObject optJSONObject2 = this.f8185s0.optJSONObject(i3);
            int i5 = length2;
            i4 = (int) (((double) i4) + optJSONObject2.optDouble(str));
            if (((double) i4) > d3) {
                break;
            }
            if (this.f8168b0) {
                if (arrayList4 == null) {
                    arrayList4 = new ArrayList();
                }
                ArrayList arrayList5 = arrayList4;
                stepInfo = w;
                d = d3;
                str2 = str;
                arrayList5.add(new com.google.android.gms.maps.model.LatLng(optJSONObject2.optDouble("lat"), optJSONObject2.optDouble("lng")));
                arrayList4 = arrayList5;
            } else {
                stepInfo = w;
                d = d3;
                str2 = str;
                if (arrayList3 == null) {
                    arrayList3 = new ArrayList();
                }
                arrayList3.add(new LatLng(optJSONObject2.optDouble("lat"), optJSONObject2.optDouble("lng")));
            }
            i3++;
            str = str2;
            length2 = i5;
            w = stepInfo;
            d3 = d;
        }
        jSONObject.put("laps", i);
        jSONObject.put("currentMiles", d3);
        jSONObject.put("stepToday", StepDataDb.m13223f(0).f6245S);
        StepInfo stepInfo2 = w;
        jSONObject.put("stepTotal", stepInfo2.f6245S);
        jSONObject.put("days", stepInfo2.f6249W);
        if (this.f8168b0) {
            arrayList3 = arrayList4;
        }
        jSONObject.put("list", arrayList3);
        return jSONObject;
    }

    /* renamed from: w */
    private void m13678w() {
        ThreadUtils.m963c(new C4273b());
    }

    /* renamed from: x */
    private void m13679x() {
        ThreadUtils.m963c(new C4274c());
    }

    /* access modifiers changed from: private */
    /* renamed from: y */
    public void m13680y() {
        try {
            JSONObject jSONObject = new JSONObject(ResourceUtils.m1240b(this.f8184r0));
            this.f8182p0 = jSONObject.optString("name");
            this.f8185s0 = jSONObject.optJSONArray("points");
            mo22701a((CharSequence) this.f8182p0);
            m13676u();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: z */
    private void m13681z() {
        C4629g gVar = new C4629g(this);
        gVar.mo25456a("日月潭環湖拉力賽", "東部拉力賽");
        gVar.mo25453a(new C4272a());
        gVar.mo25418b().show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f8168b0 = FSManager.m10325t();
        MapFragment a = MapFragment.m14638a(this.f8168b0, false);
        if (this.f8168b0) {
            a.mo24729a(new C4275a(this));
        } else {
            a.mo24729a(new C4279e(this));
        }
        getSupportFragmentManager().beginTransaction().replace(R$id.mapFragment, a, "mapFragment").commit();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mo22680a(menu, 16908331, getString(R$string.setting), getResources().getColor(R$color.font_color), (Drawable) null, new C4278d(this));
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8183q0 = BandDataManager.m10566h().mo26419d();
        this.f8171e0 = (TableLayout) findViewById(R$id.tableLayout);
        this.f8172f0 = (TextView) findViewById(R$id.tvMatchName);
        this.f8173g0 = (TextView) findViewById(R$id.tvMatchProcess);
        this.f8174h0 = (TextView) findViewById(R$id.tvStepToday);
        this.f8175i0 = (TextView) findViewById(R$id.tvStepSum);
        this.f8176j0 = (TextView) findViewById(R$id.tvCurrentProcess);
        this.f8177k0 = (TextView) findViewById(R$id.tvStepTodayDistance);
        this.f8178l0 = (TextView) findViewById(R$id.tvStepSumDistance);
        this.f8179m0 = (TextView) findViewById(R$id.tvMatchDistance);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_location;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24436a(C2880c cVar) {
        this.f8170d0 = cVar;
        m13680y();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24434a(AMap aMap) {
        this.f8169c0 = aMap;
        m13680y();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24433a(View view) {
        m13681z();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13670a(JSONObject jSONObject) {
        String str;
        int optInt = jSONObject.optInt("days");
        int optInt2 = jSONObject.optInt("laps");
        int optInt3 = jSONObject.optInt("stepToday");
        int optInt4 = jSONObject.optInt("stepTotal");
        double optDouble = jSONObject.optDouble("matchMiles");
        double optDouble2 = jSONObject.optDouble("currentMiles");
        SpanUtils a = SpanUtils.m865a(this.f8172f0);
        a.mo9735a("累計");
        a.mo9735a(String.valueOf(optInt));
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a.mo9735a("天");
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8173g0);
        a2.mo9735a("當前進度");
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8174h0);
        a3.mo9735a(String.valueOf(optInt3));
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a3.mo9735a("步");
        a3.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8175i0);
        a4.mo9735a(String.valueOf(optInt4));
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a4.mo9735a("步");
        a4.mo9736b();
        SpanUtils a5 = SpanUtils.m865a(this.f8176j0);
        if (optInt2 == 0) {
            str = "";
        } else {
            str = "第" + (optInt2 + 1) + "圈 ";
        }
        a5.mo9735a(str);
        a5.mo9735a(String.format(Locale.getDefault(), "%.2f", Double.valueOf(optDouble2 / optDouble)));
        a5.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a5.mo9735a("%");
        a5.mo9736b();
        SpanUtils a6 = SpanUtils.m865a(this.f8177k0);
        a6.mo9735a("≈");
        a6.mo9735a(String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) ((optInt3 * this.f8183q0) / 100)) / 1000.0f)));
        a6.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_15));
        a6.mo9735a("km");
        a6.mo9736b();
        SpanUtils a7 = SpanUtils.m865a(this.f8178l0);
        a7.mo9735a("≈");
        a7.mo9735a(String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) ((optInt4 * this.f8183q0) / 100)) / 1000.0f)));
        a7.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_15));
        a7.mo9735a("km");
        a7.mo9736b();
        SpanUtils a8 = SpanUtils.m865a(this.f8179m0);
        a8.mo9735a("全程");
        a8.mo9735a(String.format(Locale.getDefault(), "%.2f", Double.valueOf(optDouble / 1000.0d)));
        a8.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_15));
        a8.mo9735a("km");
        a8.mo9736b();
        this.f8171e0.setVisibility(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13666a(com.google.android.gms.maps.model.LatLng latLng) {
        if (this.f8170d0 != null) {
            C2932c cVar = this.f8180n0;
            if (cVar != null) {
                cVar.mo18627a(latLng);
            } else {
                AvatarUtils.m13392a(this, MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4276b(this, latLng));
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24437a(com.google.android.gms.maps.model.LatLng latLng, Bitmap bitmap) {
        this.f8180n0 = this.f8170d0.mo18399a(new MarkerOptions().mo18545a(latLng).mo18546a(C2931b.m8343a(bitmap)));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13665a(LatLng latLng) {
        if (this.f8169c0 != null) {
            Marker marker = this.f8181o0;
            if (marker != null) {
                marker.setPosition(latLng);
            } else {
                AvatarUtils.m13392a(this, MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4277c(this, latLng));
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24435a(LatLng latLng, Bitmap bitmap) {
        this.f8181o0 = this.f8169c0.addMarker(new com.amap.api.maps.model.MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
    }
}
