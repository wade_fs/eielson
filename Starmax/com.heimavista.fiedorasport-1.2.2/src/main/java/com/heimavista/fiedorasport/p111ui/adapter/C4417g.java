package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.p111ui.adapter.ListBuddyDynamicAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.g */
/* compiled from: lambda */
public final /* synthetic */ class C4417g implements View.OnLongClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8666P;

    /* renamed from: Q */
    private final /* synthetic */ ListBuddyDynamicAdapter.ViewHolder f8667Q;

    /* renamed from: R */
    private final /* synthetic */ int f8668R;

    /* renamed from: S */
    private final /* synthetic */ FriendBean f8669S;

    public /* synthetic */ C4417g(ListBuddyDynamicAdapter listBuddyDynamicAdapter, ListBuddyDynamicAdapter.ViewHolder viewHolder, int i, FriendBean dVar) {
        this.f8666P = listBuddyDynamicAdapter;
        this.f8667Q = viewHolder;
        this.f8668R = i;
        this.f8669S = dVar;
    }

    public final boolean onLongClick(View view) {
        return this.f8666P.mo24618a(this.f8667Q, this.f8668R, this.f8669S, view);
    }
}
