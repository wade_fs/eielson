package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.amap.api.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.q */
/* compiled from: lambda */
public final /* synthetic */ class C4458q implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ BuddyLocFragment f8786a;

    /* renamed from: b */
    private final /* synthetic */ String f8787b;

    /* renamed from: c */
    private final /* synthetic */ LatLng f8788c;

    public /* synthetic */ C4458q(BuddyLocFragment buddyLocFragment, String str, LatLng latLng) {
        this.f8786a = buddyLocFragment;
        this.f8787b = str;
        this.f8788c = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8786a.mo24697b(this.f8787b, this.f8788c, bitmap);
    }
}
