package com.heimavista.fiedorasport.service;

import android.os.IBinder;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.blankj.utilcode.util.ServiceUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.heimavista.fiedorasport.service.LocUpdForegroundService;

public class LocUpdForegroundServiceA extends LocUpdForegroundService {

    /* renamed from: R */
    private AMapLocationClient f6624R = null;

    /* renamed from: S */
    private AMapLocationClientOption f6625S = null;

    /* renamed from: T */
    private AMapLocationListener f6626T;

    /* renamed from: com.heimavista.fiedorasport.service.LocUpdForegroundServiceA$a */
    class C3748a extends LocUpdForegroundService.C3747a<LocUpdForegroundServiceA> {
        C3748a() {
            super(LocUpdForegroundServiceA.this);
        }

        /* renamed from: a */
        public LocUpdForegroundServiceA mo23005a() {
            return LocUpdForegroundServiceA.this;
        }
    }

    /* renamed from: g */
    private AMapLocationClientOption m10711g() {
        AMapLocationClientOption aMapLocationClientOption = new AMapLocationClientOption();
        aMapLocationClientOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        aMapLocationClientOption.setGpsFirst(false);
        aMapLocationClientOption.setHttpTimeOut(20000);
        aMapLocationClientOption.setInterval((long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        aMapLocationClientOption.setNeedAddress(true);
        aMapLocationClientOption.setOnceLocation(false);
        aMapLocationClientOption.setOnceLocationLatest(false);
        AMapLocationClientOption.setLocationProtocol(AMapLocationClientOption.AMapLocationProtocol.HTTP);
        aMapLocationClientOption.setSensorEnable(false);
        aMapLocationClientOption.setWifiScan(true);
        aMapLocationClientOption.setLocationCacheEnable(true);
        return aMapLocationClientOption;
    }

    /* renamed from: a */
    public void mo22992a() {
        AMapLocationClient aMapLocationClient = this.f6624R;
        if (aMapLocationClient != null) {
            aMapLocationClient.onDestroy();
            this.f6626T = null;
            this.f6624R = null;
            this.f6625S = null;
        }
        super.f6623Q = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public IBinder mo22995b() {
        return new C3748a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo22996c() {
        this.f6624R = new AMapLocationClient(getApplicationContext());
        this.f6625S = m10711g();
        this.f6626T = new C3751a(this);
        this.f6624R.setLocationOption(this.f6625S);
        this.f6624R.setLocationListener(this.f6626T);
    }

    /* renamed from: e */
    public void mo22998e() {
        mo22994a("Requesting location updates");
        ServiceUtils.m1270b(LocUpdForegroundServiceA.class);
        this.f6624R.startLocation();
        super.f6623Q = true;
    }

    /* renamed from: f */
    public void mo22999f() {
        this.f6624R.stopLocation();
        super.f6623Q = false;
        stopSelf();
    }
}
