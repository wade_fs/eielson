package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.entity.data.SelectFriendBean;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListSelectFriendAdapter */
public class ListSelectFriendAdapter extends BaseRecyclerViewAdapter<SelectFriendBean, ViewHolder> {

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListSelectFriendAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        ImageView f8630R = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: S */
        TextView f8631S = ((TextView) findViewById(R$id.tvUserName));

        /* renamed from: T */
        CheckBox f8632T = ((CheckBox) findViewById(R$id.f6592cb));

        /* renamed from: U */
        CommonListItemView f8633U = ((CommonListItemView) findViewById(R$id.listItemView));

        ViewHolder(ListSelectFriendAdapter listSelectFriendAdapter, ViewGroup viewGroup, int i) {
            super(listSelectFriendAdapter, viewGroup, i);
        }
    }

    public ListSelectFriendAdapter(Context context) {
        super(context);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        SelectFriendBean fVar = (SelectFriendBean) getItem(i);
        if (fVar.mo22667b() == 2) {
            viewHolder.f8633U.setImageResource(fVar.mo22666a());
            viewHolder.f8633U.setText(fVar.mo22668c());
        } else if (fVar.mo22667b() == 1) {
            viewHolder.f8632T.setChecked(fVar.mo22669d());
            UserInfoDb.m13339a(mo22788b(), fVar.mo22668c(), viewHolder.f8631S, viewHolder.f8630R);
        }
    }

    public int getItemViewType(int i) {
        return ((SelectFriendBean) getItem(i)).mo22667b();
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == 2) {
            return new ViewHolder(this, viewGroup, R$layout.layout_list_item);
        }
        if (i == 0) {
            return new ViewHolder(this, viewGroup, R$layout.layout_list_item_buddy_add);
        }
        return new ViewHolder(this, viewGroup, R$layout.layout_list_item_select_friend);
    }
}
