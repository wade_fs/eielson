package com.heimavista.fiedorasport.p109h;

import com.heimavista.entity.band.TickType;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p199j.MMKVUtils;
import com.tencent.mmkv.MMKV;
import p119e.p189e.p193d.TickDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.h.d0 */
public class TickManager {

    /* renamed from: com.heimavista.fiedorasport.h.d0$a */
    /* compiled from: TickManager */
    static /* synthetic */ class C3715a {

        /* renamed from: a */
        static final /* synthetic */ int[] f6533a = new int[TickType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.heimavista.entity.band.h[] r0 = com.heimavista.entity.band.TickType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a = r0
                int[] r0 = com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.heimavista.entity.band.h r1 = com.heimavista.entity.band.TickType.STEP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.heimavista.entity.band.h r1 = com.heimavista.entity.band.TickType.SLEEP     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.heimavista.entity.band.h r1 = com.heimavista.entity.band.TickType.HEART     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.heimavista.entity.band.h r1 = com.heimavista.entity.band.TickType.SPORT     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.heimavista.fiedorasport.p109h.TickManager.C3715a.f6533a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.heimavista.entity.band.h r1 = com.heimavista.entity.band.TickType.PAUSE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p109h.TickManager.C3715a.<clinit>():void");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mmkv.MMKV.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.mmkv.MMKV.a(int, java.lang.String):com.tencent.mmkv.MMKV
      com.tencent.mmkv.MMKV.a(com.tencent.mmkv.c, java.lang.String):void
      com.tencent.mmkv.MMKV.a(java.lang.String, int):int
      com.tencent.mmkv.MMKV.a(java.lang.String, long):long
      com.tencent.mmkv.MMKV.a(java.lang.String, java.lang.Class):T
      com.tencent.mmkv.MMKV.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.mmkv.MMKV.a(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.tencent.mmkv.MMKV.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.mmkv.MMKV.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mmkv.MMKV.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.mmkv.MMKV.b(java.lang.String, int):boolean
      com.tencent.mmkv.MMKV.b(java.lang.String, long):boolean
      com.tencent.mmkv.MMKV.b(java.lang.String, java.lang.String):boolean
      com.tencent.mmkv.MMKV.b(java.lang.String, java.util.Set<java.lang.String>):boolean
      com.tencent.mmkv.MMKV.b(java.lang.String, boolean):boolean */
    /* renamed from: a */
    public static void m10521a() {
        MMKV a = MMKVUtils.m13424a("fiedorasport_tick_data");
        TickDb hVar = new TickDb();
        if (hVar.mo24207k()) {
            a.clearAll();
            for (TickDb hVar2 : TickDb.m13105r()) {
                a.mo27040b(hVar2.mo24234q() + "_" + hVar2.mo24232o(), hVar2.mo24233p());
            }
            hVar.mo24180g(hVar.mo24185a());
        }
        if (a.mo27033a("TickType", false)) {
            TickType[] hVarArr = {TickType.STEP, TickType.SLEEP, TickType.HEART, TickType.SPORT, TickType.LOGIN, TickType.PAUSE, TickType.BUDDY_LIST, TickType.LIKE_CNT, TickType.NONE};
            MMKV e = FSManager.m10323r().mo22835e();
            for (TickType hVar3 : hVarArr) {
                m10524b(hVar3, e.getLong("lastTimestamp" + hVar3, 0));
            }
            a.mo27043b("TickType", true);
        }
    }

    /* renamed from: b */
    public static void m10525b(String str, long j) {
        m10526c(str + "_loc", j);
    }

    /* renamed from: c */
    public static void m10526c(String str, long j) {
        MMKVUtils.m13424a("fiedorasport_tick_data").mo27040b(str, j);
    }

    /* renamed from: b */
    public static void m10524b(TickType hVar, long j) {
        m10526c(MemberControl.m17125s().mo27187l() + "_" + hVar, j);
    }

    /* renamed from: b */
    public static long m10523b(String str) {
        return m10520a(str, 0);
    }

    /* renamed from: b */
    public static long m10522b(TickType hVar) {
        return m10518a(hVar, 0);
    }

    /* renamed from: a */
    public static long m10519a(String str) {
        return m10523b(str + "_loc");
    }

    /* renamed from: a */
    public static long m10520a(String str, long j) {
        return MMKVUtils.m13424a("fiedorasport_tick_data").mo27025a(str, j);
    }

    /* renamed from: a */
    public static long m10518a(TickType hVar, long j) {
        return m10520a(MemberControl.m17125s().mo27187l() + "_" + hVar, j);
    }

    /* renamed from: a */
    public static long m10517a(TickType hVar) {
        long j;
        long b = m10522b(hVar);
        int i = C3715a.f6533a[hVar.ordinal()];
        if (i == 1) {
            j = Math.max(StepDataDb.m13225x(), b);
        } else if (i == 2) {
            j = Math.max(SleepDataDb.m13183w(), b);
        } else if (i == 3) {
            j = Math.max(HeartRateDataDb.m13158t(), b);
        } else if (i == 4) {
            j = Math.max(SportDataDb.m13195r(), b);
        } else if (i != 5) {
            return b;
        } else {
            return m10518a(hVar, System.currentTimeMillis());
        }
        return Math.max(m10522b(TickType.LOGIN), j);
    }
}
