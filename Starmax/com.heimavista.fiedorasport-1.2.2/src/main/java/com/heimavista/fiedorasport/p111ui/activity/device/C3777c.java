package com.heimavista.fiedorasport.p111ui.activity.device;

import android.content.DialogInterface;
import com.heimavista.widget.dialog.LoadingDialog;

/* renamed from: com.heimavista.fiedorasport.ui.activity.device.c */
/* compiled from: lambda */
public final /* synthetic */ class C3777c implements LoadingDialog.C4628a {

    /* renamed from: a */
    private final /* synthetic */ DeviceManagerActivity f6717a;

    public /* synthetic */ C3777c(DeviceManagerActivity deviceManagerActivity) {
        this.f6717a = deviceManagerActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f6717a.mo23035a(dialogInterface);
    }
}
