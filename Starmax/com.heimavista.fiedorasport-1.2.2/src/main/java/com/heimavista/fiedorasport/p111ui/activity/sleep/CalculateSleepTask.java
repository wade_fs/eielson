package com.heimavista.fiedorasport.p111ui.activity.sleep;

import android.os.AsyncTask;
import java.util.List;
import p119e.p189e.p197e.p198a.ChartSleepData;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.b */
class CalculateSleepTask extends AsyncTask<Void, Void, List<ChartSleepData>> {

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.b$a */
    /* compiled from: CalculateSleepTask */
    public enum C4344a {
        DAY,
        DAYS,
        WEEKS,
        MONTHS
    }
}
