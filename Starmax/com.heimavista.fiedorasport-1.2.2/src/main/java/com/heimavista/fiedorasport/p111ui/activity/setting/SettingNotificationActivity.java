package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p111ui.adapter.ListNotificationAdapter;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.dialog.BaseDialog;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingNotificationActivity */
public class SettingNotificationActivity extends BaseActivity {

    /* renamed from: b0 */
    private ConstraintLayout f8261b0;

    /* renamed from: c0 */
    private CommonListItemView f8262c0;

    /* renamed from: d0 */
    private RecyclerView f8263d0;

    /* renamed from: e0 */
    private ListNotificationAdapter f8264e0;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* renamed from: u */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m13837u() {
        /*
            r6 = this;
            java.lang.String r0 = r6.getPackageName()
            android.content.ContentResolver r1 = r6.getContentResolver()
            java.lang.String r2 = "enabled_notification_listeners"
            java.lang.String r1 = android.provider.Settings.Secure.getString(r1, r2)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            r3 = 0
            if (r2 != 0) goto L_0x0036
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)
            int r2 = r1.length
            r4 = 0
        L_0x001d:
            if (r4 >= r2) goto L_0x0036
            r5 = r1[r4]
            android.content.ComponentName r5 = android.content.ComponentName.unflattenFromString(r5)
            if (r5 == 0) goto L_0x0033
            java.lang.String r5 = r5.getPackageName()
            boolean r5 = android.text.TextUtils.equals(r0, r5)
            if (r5 == 0) goto L_0x0033
            r0 = 1
            goto L_0x0037
        L_0x0033:
            int r4 = r4 + 1
            goto L_0x001d
        L_0x0036:
            r0 = 0
        L_0x0037:
            r1 = 8
            if (r0 == 0) goto L_0x0046
            androidx.constraintlayout.widget.ConstraintLayout r2 = r6.f8261b0
            r2.setVisibility(r1)
            com.heimavista.fiedorasport.widget.CommonListItemView r1 = r6.f8262c0
            r1.setVisibility(r3)
            goto L_0x0050
        L_0x0046:
            androidx.constraintlayout.widget.ConstraintLayout r2 = r6.f8261b0
            r2.setVisibility(r3)
            com.heimavista.fiedorasport.widget.CommonListItemView r2 = r6.f8262c0
            r2.setVisibility(r1)
        L_0x0050:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p111ui.activity.setting.SettingNotificationActivity.m13837u():boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        boolean u = m13837u();
        ArrayList arrayList = new ArrayList();
        RemindMsgInfo e = BandDataManager.m10563e();
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(2);
        bVar.mo22664b(getString(R$string.facebook_message));
        boolean z = true;
        bVar.mo22660a(u && e.mo22601b());
        bVar.mo22665b(u);
        arrayList.add(bVar.mo22661a());
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(2);
        bVar2.mo22664b(getString(R$string.line_message));
        bVar2.mo22660a(u && e.mo22603c());
        bVar2.mo22665b(u);
        arrayList.add(bVar2.mo22661a());
        MenuItem.C3676b bVar3 = new MenuItem.C3676b();
        bVar3.mo22663b(2);
        bVar3.mo22664b(getString(R$string.qq_message));
        bVar3.mo22660a(u && e.mo22608e());
        bVar3.mo22665b(u);
        arrayList.add(bVar3.mo22661a());
        MenuItem.C3676b bVar4 = new MenuItem.C3676b();
        bVar4.mo22663b(2);
        bVar4.mo22664b(getString(R$string.wechat_message));
        bVar4.mo22660a(u && e.mo22616i());
        bVar4.mo22665b(u);
        arrayList.add(bVar4.mo22661a());
        MenuItem.C3676b bVar5 = new MenuItem.C3676b();
        bVar5.mo22663b(2);
        bVar5.mo22664b(getString(R$string.skype_message));
        bVar5.mo22660a(u && e.mo22610f());
        bVar5.mo22665b(u);
        arrayList.add(bVar5.mo22661a());
        MenuItem.C3676b bVar6 = new MenuItem.C3676b();
        bVar6.mo22663b(2);
        bVar6.mo22664b(getString(R$string.twitter_message));
        bVar6.mo22660a(u && e.mo22614h());
        bVar6.mo22665b(u);
        arrayList.add(bVar6.mo22661a());
        MenuItem.C3676b bVar7 = new MenuItem.C3676b();
        bVar7.mo22663b(2);
        bVar7.mo22664b(getString(R$string.whatsapp_message));
        if (!u || !e.mo22618j()) {
            z = false;
        }
        bVar7.mo22660a(z);
        bVar7.mo22665b(u);
        bVar7.mo22662b();
        arrayList.add(bVar7.mo22661a());
        this.f8264e0 = new ListNotificationAdapter(super, e);
        this.f8264e0.mo22792b((List) arrayList);
        this.f8264e0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4326q(this));
        this.f8263d0.setAdapter(this.f8264e0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8261b0 = (ConstraintLayout) findViewById(R$id.layoutNotification);
        this.f8262c0 = (CommonListItemView) findViewById(R$id.listItemView);
        this.f8263d0 = (RecyclerView) findViewById(R$id.recyclerView);
        mo22714a(this.f8261b0, this.f8262c0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_notification;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.remind_notification);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100) {
            this.f8264e0.mo22782a();
            this.f8264e0 = null;
            mo22697a((Bundle) null);
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.layoutNotification || view.getId() == R$id.listItemView) {
            startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), 100);
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24484b(boolean z) {
        if (z) {
            startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), 100);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.setting.p]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* renamed from: a */
    public /* synthetic */ void mo24483a(View view, MenuItem eVar, int i) {
        if (eVar.mo22653e() != 2) {
            return;
        }
        if (m13837u()) {
            ((CommonListItemView) view.findViewById(R$id.listItemView)).getSwitch().toggle();
            return;
        }
        mo22708a(getString(R$string.notification_permission_denied) + "\n" + getString(R$string.notification_tips), getString(R$string.goto_open), false, (BaseDialog.C4621m) new C4325p(this));
    }
}
