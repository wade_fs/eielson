package com.heimavista.fiedorasport.p111ui.activity.sleep;

import com.heimavista.fiedorasport.base.BaseActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.BaseSleepDataActivity */
public abstract class BaseSleepDataActivity extends BaseActivity {

    /* renamed from: b0 */
    private CalculateSleepTask f8337b0;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        CalculateSleepTask bVar = this.f8337b0;
        if (bVar != null) {
            if (!bVar.isCancelled()) {
                this.f8337b0.cancel(true);
            }
            this.f8337b0 = null;
        }
        super.onDestroy();
    }
}
