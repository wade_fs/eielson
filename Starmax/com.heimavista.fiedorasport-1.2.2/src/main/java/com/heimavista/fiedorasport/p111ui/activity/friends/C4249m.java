package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.app.AlertDialog;
import android.view.View;
import com.heimavista.fiedorasport.p108g.C3701h;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.m */
/* compiled from: lambda */
public final /* synthetic */ class C4249m implements C3701h {

    /* renamed from: a */
    private final /* synthetic */ ListAddFriendsActivity f8125a;

    public /* synthetic */ C4249m(ListAddFriendsActivity listAddFriendsActivity) {
        this.f8125a = listAddFriendsActivity;
    }

    /* renamed from: a */
    public final void mo22862a(View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        this.f8125a.mo24385a(view, alertDialog, z, charSequence);
    }
}
