package com.heimavista.fiedorasport.p108g;

import android.content.Context;
import android.view.View;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.UIDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.fiedorasport.g.e */
public final class DisconnectDialog extends UIDialog<DisconnectDialog> {

    /* renamed from: m0 */
    private BaseDialog.C4621m f6467m0;

    public DisconnectDialog(Context context) {
        super(context);
        mo25419c(R$layout.dialog_disconnect_tip);
        mo25416b(AnimAction.f7973c);
        mo25422d(17);
        mo25427g((ScreenUtils.m1265c() * 7) / 10);
        mo24144a(R$id.dialog_negative_btn, R$id.dialog_positive_btn);
    }

    /* renamed from: a */
    public DisconnectDialog mo22852a(BaseDialog.C4621m mVar) {
        this.f6467m0 = mVar;
        return this;
    }

    public void onClick(View view) {
        if (view.getId() == R$id.dialog_positive_btn) {
            mo25471h();
            BaseDialog.C4621m mVar = this.f6467m0;
            if (mVar != null) {
                mVar.mo23034a(true);
            }
        } else if (view.getId() == R$id.dialog_negative_btn) {
            mo25471h();
            BaseDialog.C4621m mVar2 = this.f6467m0;
            if (mVar2 != null) {
                mVar2.mo23034a(false);
            }
        }
    }
}
