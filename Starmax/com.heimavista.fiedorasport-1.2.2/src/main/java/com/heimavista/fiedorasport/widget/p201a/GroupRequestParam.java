package com.heimavista.fiedorasport.widget.p201a;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.widget.a.d */
public class GroupRequestParam implements Serializable {

    /* renamed from: P */
    private List<String> f8995P;

    /* renamed from: Q */
    private int f8996Q;

    /* renamed from: R */
    private int f8997R;

    /* renamed from: S */
    private int f8998S;

    /* renamed from: T */
    private Bitmap f8999T;

    public GroupRequestParam() {
    }

    /* renamed from: a */
    public int mo24838a() {
        return this.f8998S;
    }

    /* renamed from: e */
    public int mo24839e() {
        return this.f8997R;
    }

    /* renamed from: f */
    public Bitmap mo24840f() {
        return this.f8999T;
    }

    /* renamed from: g */
    public int mo24841g() {
        return this.f8996Q;
    }

    /* renamed from: h */
    public List<String> mo24842h() {
        return this.f8995P;
    }

    public GroupRequestParam(List<String> list, int i, int i2, int i3, Bitmap bitmap) {
        this.f8995P = list;
        this.f8996Q = i;
        this.f8997R = i2;
        this.f8998S = i3;
        this.f8999T = bitmap;
    }
}
