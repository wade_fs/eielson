package com.heimavista.fiedorasport.p111ui.activity.device;

import android.text.TextUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.device.l */
/* compiled from: DeviceManagerActivity */
class C3786l implements OnResultListener<ParamJsonData> {

    /* renamed from: a */
    final /* synthetic */ DeviceManagerActivity f6728a;

    C3786l(DeviceManagerActivity deviceManagerActivity) {
        this.f6728a = deviceManagerActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity.a(com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, boolean):boolean
     arg types: [com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, int]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity.a(com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity.b(com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, boolean):void
     arg types: [com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, int]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.b(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.b(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity.b(com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity, boolean):void */
    /* renamed from: a */
    public void mo22380a(ParamJsonData fVar) {
        boolean unused = this.f6728a.f6677i0 = true;
        Constant.m10311a("解除綁定成功");
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22884a(true);
            JYSDKManager.m10388i().mo22898f();
            JYSDKManager.m10388i().mo22900h();
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10647r();
        }
        this.f6728a.m10812c(false);
        FSManager.m10323r().mo22829b();
        this.f6728a.mo22717b();
        this.f6728a.m10812c(false);
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        Constant.m10311a("解除綁定失敗");
        this.f6728a.mo22717b();
        if (TextUtils.isEmpty(str)) {
            str = this.f6728a.getString(R$string.unbind_device_err);
        }
        this.f6728a.mo22730c(str);
    }
}
