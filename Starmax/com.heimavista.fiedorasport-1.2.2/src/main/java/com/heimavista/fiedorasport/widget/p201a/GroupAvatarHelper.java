package com.heimavista.fiedorasport.widget.p201a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@SuppressLint({"ALL"})
/* renamed from: com.heimavista.fiedorasport.widget.a.c */
public class GroupAvatarHelper {

    /* renamed from: a */
    private BitmapLoader f8989a;

    /* renamed from: b */
    private WeakReference<Context> f8990b;

    /* renamed from: c */
    private Bitmap f8991c;

    /* renamed from: com.heimavista.fiedorasport.widget.a.c$b */
    /* compiled from: GroupAvatarHelper */
    private static final class C4495b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final GroupAvatarHelper f8994a = new GroupAvatarHelper(null);
    }

    /* synthetic */ GroupAvatarHelper(C4494a aVar) {
        this();
    }

    /* renamed from: a */
    public static GroupAvatarHelper m14691a() {
        return C4495b.f8994a;
    }

    private GroupAvatarHelper() {
    }

    /* renamed from: a */
    public void mo24830a(Context context, BitmapLoader aVar) {
        mo24831a(context, aVar, null);
    }

    /* renamed from: com.heimavista.fiedorasport.widget.a.c$a */
    /* compiled from: GroupAvatarHelper */
    class C4494a extends AsyncTask<GroupRequestParam, Void, GroupAvatar> {

        /* renamed from: a */
        final /* synthetic */ OnGroupLoaded f8992a;

        C4494a(OnGroupLoaded eVar) {
            this.f8992a = eVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public GroupAvatar doInBackground(GroupRequestParam... dVarArr) {
            if (dVarArr == null || dVarArr.length != 1) {
                return null;
            }
            GroupRequestParam dVar = dVarArr[0];
            return GroupAvatarHelper.this.mo24829a(dVar.mo24842h(), dVar.mo24841g(), dVar.mo24839e(), dVar.mo24838a(), dVar.mo24840f());
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(GroupAvatar bVar) {
            super.onPostExecute(bVar);
            OnGroupLoaded eVar = this.f8992a;
            if (eVar != null) {
                if (bVar == null) {
                    eVar.onError();
                } else {
                    eVar.mo24608a(bVar);
                }
            }
        }
    }

    /* renamed from: a */
    public void mo24831a(Context context, BitmapLoader aVar, Bitmap bitmap) {
        this.f8989a = aVar;
        this.f8990b = new WeakReference<>(context);
        this.f8991c = bitmap;
    }

    /* renamed from: a */
    public void mo24832a(List<String> list, int i, int i2, int i3, int i4, OnGroupLoaded eVar) {
        WeakReference<Context> weakReference = this.f8990b;
        mo24833a(list, i, i2, i3, (weakReference == null || weakReference.get() == null) ? null : BitmapFactory.decodeResource(this.f8990b.get().getResources(), i4), eVar);
    }

    /* renamed from: a */
    public void mo24833a(List<String> list, int i, int i2, int i3, Bitmap bitmap, OnGroupLoaded eVar) {
        GroupRequestParam dVar = new GroupRequestParam(list, i, i2, i3, bitmap);
        new C4494a(eVar).execute(dVar);
    }

    /* renamed from: a */
    public GroupAvatar mo24829a(List<String> list, int i, int i2, int i3, Bitmap bitmap) {
        int i4;
        int i5;
        int i6 = i;
        Bitmap bitmap2 = bitmap;
        if (list == null || list.isEmpty()) {
            return new GroupAvatar(this.f8991c);
        }
        if (this.f8989a == null) {
            LogUtils.m1131b("getGroupAvatar>>>", "Please setup WeChatBitmapLoader before generate group avatar，call the config()");
            return new GroupAvatar(bitmap2);
        }
        int size = list.size();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i7 = 0; i7 < size; i7++) {
            String str = list.get(i7);
            Bitmap bitmap3 = null;
            if (!TextUtils.isEmpty(str)) {
                try {
                    bitmap3 = this.f8989a.mo24607a(str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (bitmap3 != null) {
                    arrayList2.add(str);
                }
                if (bitmap3 == null) {
                    bitmap3 = bitmap2;
                }
                if (bitmap3 != null) {
                    arrayList.add(bitmap3);
                }
            } else if (bitmap2 != null) {
                arrayList.add(bitmap2);
            }
        }
        int size2 = arrayList.size();
        if (size2 == 0) {
            return new GroupAvatar(this.f8991c);
        }
        if (size2 == 1) {
            i4 = i6;
        } else {
            if (size2 <= 4) {
                i5 = (i6 - (i2 * 3)) / 2;
            } else {
                i5 = (i6 - (i2 * 4)) / 3;
            }
            i4 = i5;
        }
        m14694a(arrayList, i4);
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (size2 > 1) {
            Paint paint = new Paint();
            paint.setColor(i3);
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            float f = (float) i6;
            canvas.drawRoundRect(new RectF(0.0f, 0.0f, f, f), 30.0f, 30.0f, paint);
        }
        m14692a(i, i2, i4, arrayList, canvas);
        m14693a(arrayList);
        return new GroupAvatar(createBitmap, arrayList2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0088 A[ADDED_TO_REGION] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m14692a(int r18, int r19, int r20, java.util.List<android.graphics.Bitmap> r21, android.graphics.Canvas r22) {
        /*
            r17 = this;
            int r0 = r21.size()
            r1 = 0
            r2 = 6
            r3 = 5
            r4 = 1
            r5 = 2
            if (r0 != r4) goto L_0x000d
            r6 = 0
            goto L_0x001f
        L_0x000d:
            if (r0 != r5) goto L_0x0013
            int r6 = r18 - r20
            int r6 = r6 / r5
            goto L_0x001f
        L_0x0013:
            if (r0 < r3) goto L_0x001d
            if (r0 > r2) goto L_0x001d
            int r6 = r20 + r19
            int r6 = r6 / r5
            int r6 = r6 + r19
            goto L_0x001f
        L_0x001d:
            r6 = r19
        L_0x001f:
            r7 = 7
            r8 = 8
            r9 = 3
            if (r0 != r4) goto L_0x0027
            r10 = 0
            goto L_0x003b
        L_0x0027:
            if (r0 == r9) goto L_0x0038
            if (r0 != r7) goto L_0x002c
            goto L_0x0038
        L_0x002c:
            if (r0 == r3) goto L_0x0034
            if (r0 != r8) goto L_0x0031
            goto L_0x0034
        L_0x0031:
            r10 = r19
            goto L_0x003b
        L_0x0034:
            int r10 = r20 + r19
            int r10 = r10 / r5
            goto L_0x003b
        L_0x0038:
            int r10 = r18 - r20
            int r10 = r10 / r5
        L_0x003b:
            if (r1 >= r0) goto L_0x0102
            r11 = r21
            java.lang.Object r12 = r11.get(r1)
            android.graphics.Bitmap r12 = (android.graphics.Bitmap) r12
            r13 = 9
            r14 = 4
            if (r0 > r5) goto L_0x004b
            goto L_0x006d
        L_0x004b:
            if (r0 > r2) goto L_0x0061
            if (r0 != r9) goto L_0x0051
            if (r1 < r4) goto L_0x006d
        L_0x0051:
            if (r0 == r14) goto L_0x0055
            if (r0 != r3) goto L_0x0057
        L_0x0055:
            if (r1 < r5) goto L_0x006d
        L_0x0057:
            if (r0 != r2) goto L_0x005c
            if (r1 >= r9) goto L_0x005c
            goto L_0x006d
        L_0x005c:
            int r15 = r6 + r19
            int r15 = r15 + r20
            goto L_0x0086
        L_0x0061:
            if (r0 != r7) goto L_0x0065
            if (r1 < r4) goto L_0x006d
        L_0x0065:
            if (r0 != r8) goto L_0x0069
            if (r1 < r5) goto L_0x006d
        L_0x0069:
            if (r0 != r13) goto L_0x006f
            if (r1 >= r9) goto L_0x006f
        L_0x006d:
            r15 = r6
            goto L_0x0086
        L_0x006f:
            if (r0 != r7) goto L_0x0073
            if (r1 < r14) goto L_0x007b
        L_0x0073:
            if (r0 != r8) goto L_0x0077
            if (r1 < r3) goto L_0x007b
        L_0x0077:
            if (r0 != r13) goto L_0x007f
            if (r1 >= r2) goto L_0x007f
        L_0x007b:
            int r15 = r19 + r20
        L_0x007d:
            int r15 = r15 + r6
            goto L_0x0086
        L_0x007f:
            int r15 = r19 * 2
            int r16 = r20 * 2
            int r15 = r15 + r16
            goto L_0x007d
        L_0x0086:
            if (r0 == r4) goto L_0x0090
            if (r0 == r9) goto L_0x0090
            if (r0 == r7) goto L_0x0090
            if (r0 == r3) goto L_0x0090
            if (r0 != r8) goto L_0x0095
        L_0x0090:
            if (r1 != 0) goto L_0x0095
            r13 = r10
            goto L_0x00f5
        L_0x0095:
            if (r0 == r3) goto L_0x0099
            if (r0 != r8) goto L_0x00a0
        L_0x0099:
            if (r1 != r4) goto L_0x00a0
            int r13 = r10 + r19
        L_0x009d:
            int r13 = r13 + r20
            goto L_0x00f5
        L_0x00a0:
            if (r0 == r4) goto L_0x00f3
            if (r0 != r5) goto L_0x00a6
            if (r1 < r4) goto L_0x00f3
        L_0x00a6:
            if (r0 != r9) goto L_0x00aa
            if (r1 == r4) goto L_0x00f3
        L_0x00aa:
            if (r0 != r14) goto L_0x00b0
            int r16 = r1 % 2
            if (r16 == 0) goto L_0x00f3
        L_0x00b0:
            if (r0 != r3) goto L_0x00b4
            if (r1 == r5) goto L_0x00f3
        L_0x00b4:
            if (r0 == r2) goto L_0x00b8
            if (r0 != r13) goto L_0x00bc
        L_0x00b8:
            int r16 = r1 % 3
            if (r16 == 0) goto L_0x00f3
        L_0x00bc:
            if (r0 != r7) goto L_0x00c4
            int r16 = r1 + -1
            int r16 = r16 % 3
            if (r16 == 0) goto L_0x00f3
        L_0x00c4:
            if (r0 != r8) goto L_0x00cd
            int r16 = r1 + -2
            int r16 = r16 % 3
            if (r16 != 0) goto L_0x00cd
            goto L_0x00f3
        L_0x00cd:
            if (r0 <= r14) goto L_0x00f0
            if (r0 != r3) goto L_0x00d3
            if (r1 == r9) goto L_0x00f0
        L_0x00d3:
            if (r0 == r2) goto L_0x00d7
            if (r0 != r13) goto L_0x00dc
        L_0x00d7:
            int r13 = r1 + -1
            int r13 = r13 % r9
            if (r13 == 0) goto L_0x00f0
        L_0x00dc:
            if (r0 != r7) goto L_0x00e3
            int r13 = r1 + -2
            int r13 = r13 % r9
            if (r13 == 0) goto L_0x00f0
        L_0x00e3:
            if (r0 != r8) goto L_0x00ea
            int r13 = r1 % 3
            if (r13 != 0) goto L_0x00ea
            goto L_0x00f0
        L_0x00ea:
            int r13 = r19 * 3
            int r14 = r20 * 2
            int r13 = r13 + r14
            goto L_0x00f5
        L_0x00f0:
            int r13 = r19 * 2
            goto L_0x009d
        L_0x00f3:
            r13 = r19
        L_0x00f5:
            float r13 = (float) r13
            float r14 = (float) r15
            r15 = 0
            r2 = r22
            r2.drawBitmap(r12, r13, r14, r15)
            int r1 = r1 + 1
            r2 = 6
            goto L_0x003b
        L_0x0102:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.widget.p201a.GroupAvatarHelper.m14692a(int, int, int, java.util.List, android.graphics.Canvas):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* renamed from: a */
    private static void m14694a(List<Bitmap> list, int i) {
        int i2;
        float f;
        int i3;
        int i4;
        int size = list.size();
        for (int i5 = 0; i5 < size; i5++) {
            Bitmap bitmap = list.get(i5);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width <= height) {
                f = (((float) i) * 1.0f) / ((float) width);
                i2 = width;
                i3 = (height - width) / 2;
                i4 = 0;
            } else {
                f = (((float) i) * 1.0f) / ((float) height);
                i2 = height;
                i3 = 0;
                i4 = (width - height) / 2;
            }
            Matrix matrix = new Matrix();
            matrix.preScale(f, f);
            list.set(i5, Bitmap.createBitmap(bitmap, i4, i3, i2, i2, matrix, false));
        }
    }

    /* renamed from: a */
    private static void m14693a(List<Bitmap> list) {
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null && !list.get(i).isRecycled()) {
                    list.get(i).recycle();
                }
            }
        }
    }
}
