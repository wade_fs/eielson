package com.heimavista.fiedorasport.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.ConvertUtils;
import com.heimavista.fiedorasport.lib.R$attr;
import com.heimavista.fiedorasport.lib.R$styleable;

public class LoadingView extends View {

    /* renamed from: P */
    private int f8950P;

    /* renamed from: Q */
    private int f8951Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public int f8952R;

    /* renamed from: S */
    private ValueAnimator f8953S;

    /* renamed from: T */
    private Paint f8954T;

    /* renamed from: U */
    private ValueAnimator.AnimatorUpdateListener f8955U;

    /* renamed from: com.heimavista.fiedorasport.widget.LoadingView$a */
    class C4491a implements ValueAnimator.AnimatorUpdateListener {
        C4491a() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int unused = LoadingView.this.f8952R = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            LoadingView.this.invalidate();
        }
    }

    public LoadingView(Context context) {
        this(context, null);
    }

    /* renamed from: c */
    private void m14675c() {
        this.f8954T = new Paint();
        this.f8954T.setColor(this.f8951Q);
        this.f8954T.setAntiAlias(true);
        this.f8954T.setStrokeCap(Paint.Cap.ROUND);
    }

    /* renamed from: b */
    public void mo24777b() {
        ValueAnimator valueAnimator = this.f8953S;
        if (valueAnimator != null) {
            valueAnimator.removeUpdateListener(this.f8955U);
            this.f8953S.removeAllUpdateListeners();
            this.f8953S.cancel();
            this.f8953S = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mo24776a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mo24777b();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int saveLayer = canvas.saveLayer(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), null, 31);
        m14674a(canvas, this.f8952R * 30);
        canvas.restoreToCount(saveLayer);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = this.f8950P;
        setMeasuredDimension(i3, i3);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(super, i);
        if (i == 0) {
            mo24776a();
        } else {
            mo24777b();
        }
    }

    public void setColor(int i) {
        this.f8951Q = i;
        this.f8954T.setColor(i);
        invalidate();
    }

    public void setSize(int i) {
        this.f8950P = i;
        requestLayout();
    }

    public LoadingView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.LoadingStyle);
    }

    /* renamed from: a */
    public void mo24776a() {
        ValueAnimator valueAnimator = this.f8953S;
        if (valueAnimator == null) {
            this.f8953S = ValueAnimator.ofInt(0, 11);
            this.f8953S.addUpdateListener(this.f8955U);
            this.f8953S.setDuration(600L);
            this.f8953S.setRepeatMode(1);
            this.f8953S.setRepeatCount(-1);
            this.f8953S.setInterpolator(new LinearInterpolator());
            this.f8953S.start();
        } else if (!valueAnimator.isStarted()) {
            this.f8953S.start();
        }
    }

    public LoadingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f8952R = 0;
        this.f8955U = new C4491a();
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R$styleable.LoadingView, i, 0);
        this.f8950P = obtainStyledAttributes.getDimensionPixelSize(R$styleable.LoadingView_loading_view_size, ConvertUtils.m1055a(32.0f));
        this.f8951Q = obtainStyledAttributes.getInt(R$styleable.LoadingView_android_color, -1);
        obtainStyledAttributes.recycle();
        m14675c();
    }

    /* renamed from: a */
    private void m14674a(Canvas canvas, int i) {
        int i2 = this.f8950P;
        int i3 = i2 / 12;
        int i4 = i2 / 6;
        this.f8954T.setStrokeWidth((float) i3);
        int i5 = this.f8950P;
        canvas.rotate((float) i, (float) (i5 / 2), (float) (i5 / 2));
        int i6 = this.f8950P;
        canvas.translate((float) (i6 / 2), (float) (i6 / 2));
        int i7 = 0;
        while (i7 < 12) {
            canvas.rotate(30.0f);
            i7++;
            this.f8954T.setAlpha((int) ((((float) i7) * 255.0f) / 12.0f));
            int i8 = i3 / 2;
            canvas.translate(0.0f, (float) (((-this.f8950P) / 2) + i8));
            canvas.drawLine(0.0f, 0.0f, 0.0f, (float) i4, this.f8954T);
            canvas.translate(0.0f, (float) ((this.f8950P / 2) - i8));
        }
    }
}
