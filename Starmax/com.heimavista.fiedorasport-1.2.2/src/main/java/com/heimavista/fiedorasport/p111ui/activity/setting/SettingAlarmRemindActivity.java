package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p111ui.activity.setting.AlarmAdapter;
import com.heimavista.widget.SwitchButton;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAlarmRemindActivity */
public class SettingAlarmRemindActivity extends BaseActivity {

    /* renamed from: b0 */
    private RecyclerView f8253b0;

    /* renamed from: c0 */
    private AlarmAdapter f8254c0;

    /* renamed from: d0 */
    private ArrayList<AlarmInfoItem> f8255d0 = new ArrayList<>();

    /* renamed from: e0 */
    private int f8256e0 = 3;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8254c0 = new AlarmAdapter(this);
        if (FSManager.m10323r().mo22847q()) {
            this.f8256e0 = 5;
        } else if (FSManager.m10323r().mo22846p()) {
            this.f8256e0 = 3;
        }
        for (int i = 0; i < this.f8256e0; i++) {
            this.f8255d0.add(BandDataManager.m10547a(i));
        }
        this.f8254c0.mo22792b((List) this.f8255d0);
        this.f8254c0.mo24463a((AlarmAdapter.C4291a) new C4295a());
        this.f8253b0.setAdapter(this.f8254c0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8253b0 = (RecyclerView) findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_alarm_remind;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.remind_alarm);
    }

    public void onBackPressed() {
        new HvApiPostBandSettings(null).requestPostAlarms();
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f8254c0.mo22782a();
        for (int i = 0; i < this.f8256e0; i++) {
            this.f8255d0.add(BandDataManager.m10547a(i));
        }
        this.f8254c0.mo22792b((List) this.f8255d0);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAlarmRemindActivity$a */
    class C4295a implements AlarmAdapter.C4291a {
        C4295a() {
        }

        /* renamed from: a */
        public void mo24467a(SwitchButton switchButton, int i, boolean z) {
            AlarmInfoItem a = BandDataManager.m10547a(i);
            if (!z) {
                a.mo26370g(0);
            } else if (a.mo26377k() >= 20 || a.mo26377k() <= 2) {
                a.mo26370g(1);
            } else {
                a.mo26370g(2);
            }
            Constant.m10311a(String.format(z ? "設置鬧鐘%d開啟" : "設置鬧鐘%d關閉", Integer.valueOf(i + 1)));
            BandDataManager.m10552a(a);
            if (FSManager.m10323r().mo22846p()) {
                CRPBleManager.m10603a(a);
            }
        }

        /* renamed from: a */
        public void mo24466a(View view, int i, AlarmInfoItem alarmInfoItem) {
            Intent intent = new Intent(SettingAlarmRemindActivity.this, SettingAlarmActivity.class);
            intent.putExtra("alarmId", i);
            SettingAlarmRemindActivity.this.startActivity(intent);
        }
    }
}
