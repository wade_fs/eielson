package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.amap.api.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.o */
/* compiled from: lambda */
public final /* synthetic */ class C4456o implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ BuddyLocFragment f8782a;

    /* renamed from: b */
    private final /* synthetic */ String f8783b;

    /* renamed from: c */
    private final /* synthetic */ LatLng f8784c;

    public /* synthetic */ C4456o(BuddyLocFragment buddyLocFragment, String str, LatLng latLng) {
        this.f8782a = buddyLocFragment;
        this.f8783b = str;
        this.f8784c = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8782a.mo24694a(this.f8783b, this.f8784c, bitmap);
    }
}
