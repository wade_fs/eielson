package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.SportBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.k */
/* compiled from: lambda */
public final /* synthetic */ class C4421k implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListDynamicsAdapter f8679P;

    /* renamed from: Q */
    private final /* synthetic */ int f8680Q;

    /* renamed from: R */
    private final /* synthetic */ SportBean f8681R;

    public /* synthetic */ C4421k(ListDynamicsAdapter listDynamicsAdapter, int i, SportBean gVar) {
        this.f8679P = listDynamicsAdapter;
        this.f8680Q = i;
        this.f8681R = gVar;
    }

    public final void onClick(View view) {
        this.f8679P.mo24640e(this.f8680Q, this.f8681R, view);
    }
}
