package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import com.blankj.utilcode.util.BarUtils;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.zbar.CaptureActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.MyCaptureActivity */
public class MyCaptureActivity extends CaptureActivity {
    /* renamed from: a */
    private void m13558a() {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R$layout.layout_fs_basic_action_bar, (ViewGroup) null);
        ((TextView) linearLayout.findViewById(R$id.tvScreenTitle)).setText(R$string.qr_scan);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(R$mipmap.ic_back);
            supportActionBar.setBackgroundDrawable(getResources().getDrawable(R$color.white));
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayUseLogoEnabled(false);
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setCustomView(linearLayout, new ActionBar.LayoutParams(-2, -2, 17));
            if (Build.VERSION.SDK_INT >= 21) {
                supportActionBar.setElevation(0.0f);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void
     arg types: [com.heimavista.fiedorasport.ui.activity.friends.MyCaptureActivity, int]
     candidates:
      com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View
      com.blankj.utilcode.util.d.a(android.content.Context, int):android.view.View
      com.blankj.utilcode.util.d.a(android.view.Window, boolean):void
      com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void */
    public void onCreate(Bundle bundle) {
        BarUtils.m987a((Activity) this, true);
        super.onCreate(bundle);
        m13558a();
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
