package com.heimavista.fiedorasport.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.heimavista.fiedorasport.lib.R$id;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p143j.MPPointF;

@SuppressLint({"ViewConstructor"})
public class MyMarkerView extends MarkerView {

    /* renamed from: S */
    private final TextView f8957S = ((TextView) findViewById(R$id.tvContent));

    public MyMarkerView(Context context, int i) {
        super(context, i);
    }

    /* renamed from: a */
    public void mo13194a(Entry entry, Highlight dVar) {
        if (entry instanceof CandleEntry) {
            this.f8957S.setText(String.valueOf(((CandleEntry) entry).mo13312f()));
        } else {
            this.f8957S.setText(String.valueOf((int) entry.mo13303c()));
        }
        super.mo13194a(entry, dVar);
    }

    public MPPointF getOffset() {
        return new MPPointF((float) (-(getWidth() / 2)), (float) (-getHeight()));
    }
}
