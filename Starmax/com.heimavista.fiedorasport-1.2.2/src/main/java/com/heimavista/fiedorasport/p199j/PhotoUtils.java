package com.heimavista.fiedorasport.p199j;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import com.blankj.utilcode.util.UriUtils;
import com.facebook.internal.ServerProtocol;
import java.io.File;
import java.io.IOException;

/* renamed from: com.heimavista.fiedorasport.j.i */
public class PhotoUtils {

    /* renamed from: a */
    private String f8044a = "";

    /* renamed from: b */
    private Uri f8045b;

    /* renamed from: c */
    private Uri f8046c;

    /* renamed from: d */
    public static String m13425d(Context context) {
        File file;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "cameraPic");
        } else {
            file = new File(context.getFilesDir().getPath() + File.separator + "cameraPic");
        }
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, System.currentTimeMillis() + ".png");
        try {
            if (file2.exists()) {
                file2.delete();
            } else {
                file2.createNewFile();
            }
            return file2.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: a */
    public String mo24359a() {
        Uri uri = this.f8046c;
        return uri == null ? "" : uri.getPath();
    }

    /* renamed from: b */
    public Uri mo24364b(Context context) {
        File file;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "cameraPic" + File.separator + "cropPic");
        } else {
            file = new File(context.getFilesDir().getPath() + File.separator + "cameraPic" + File.separator + "cropPic");
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".png");
        try {
            if (file2.exists()) {
                file2.delete();
            } else {
                file2.createNewFile();
            }
            return Uri.fromFile(file2);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: c */
    public Uri mo24367c(Context context) {
        File file = new File(mo24361a(this.f8044a, context));
        if (Build.VERSION.SDK_INT >= 24) {
            return UriUtils.m1051a(file);
        }
        return Uri.fromFile(file);
    }

    /* renamed from: a */
    public Uri mo24358a(Context context) {
        File file;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "cameraPic");
        } else {
            file = new File(context.getFilesDir().getPath() + File.separator + "cameraPic");
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file, System.currentTimeMillis() + ".png");
        Uri uri = null;
        try {
            if (file2.exists()) {
                file2.delete();
            } else {
                file2.createNewFile();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                uri = UriUtils.m1051a(file2);
            } else {
                uri = Uri.fromFile(file2);
            }
            this.f8044a = file2.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uri;
    }

    /* renamed from: b */
    public void mo24366b(Activity activity, int i) {
        activity.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x005f A[SYNTHETIC, Splitter:B:30:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006b A[SYNTHETIC, Splitter:B:37:0x006b] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String mo24365b(java.lang.String r5, android.content.Context r6) {
        /*
            r4 = this;
            android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeFile(r5)
            if (r5 != 0) goto L_0x0009
            java.lang.String r5 = ""
            return r5
        L_0x0009:
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.JPEG
            r2 = 100
            r5.compress(r1, r2, r0)
            r1 = 90
        L_0x0017:
            byte[] r2 = r0.toByteArray()
            int r2 = r2.length
            int r2 = r2 / 1024
            r3 = 300(0x12c, float:4.2E-43)
            if (r2 <= r3) goto L_0x0036
            r0.reset()
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.JPEG
            r5.compress(r2, r1, r0)
            r2 = 10
            if (r1 <= r2) goto L_0x0031
            int r1 = r1 + -10
            goto L_0x0033
        L_0x0031:
            int r1 = r1 + -5
        L_0x0033:
            r2 = 5
            if (r1 != r2) goto L_0x0017
        L_0x0036:
            java.lang.String r5 = m13425d(r6)
            r6 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0058, all -> 0x0055 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0058, all -> 0x0055 }
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x0053 }
            r1.write(r0)     // Catch:{ Exception -> 0x0053 }
            r1.close()     // Catch:{ Exception -> 0x0053 }
            r1.close()     // Catch:{ Exception -> 0x004e }
            goto L_0x0052
        L_0x004e:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0052:
            return r5
        L_0x0053:
            r5 = move-exception
            goto L_0x005a
        L_0x0055:
            r5 = move-exception
            r1 = r6
            goto L_0x0069
        L_0x0058:
            r5 = move-exception
            r1 = r6
        L_0x005a:
            r5.printStackTrace()     // Catch:{ all -> 0x0068 }
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ Exception -> 0x0063 }
            goto L_0x0067
        L_0x0063:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0067:
            return r6
        L_0x0068:
            r5 = move-exception
        L_0x0069:
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ Exception -> 0x006f }
            goto L_0x0073
        L_0x006f:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0073:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p199j.PhotoUtils.mo24365b(java.lang.String, android.content.Context):java.lang.String");
    }

    /* renamed from: a */
    public void mo24362a(Activity activity, int i) {
        try {
            this.f8045b = mo24358a(activity);
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.putExtra("output", this.f8045b);
            activity.startActivityForResult(intent, i);
        } catch (Exception unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* renamed from: a */
    public void mo24363a(Activity activity, Uri uri, int i, int i2, int i3) {
        this.f8046c = mo24364b(activity);
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= 24) {
            intent.addFlags(1);
        }
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("aspectX", i);
        intent.putExtra("aspectY", i2);
        intent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        intent.putExtra("outputX", i);
        intent.putExtra("outputY", i2);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", false);
        intent.putExtra("output", this.f8046c);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        activity.startActivityForResult(intent, i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002b A[SYNTHETIC, Splitter:B:20:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0033 A[Catch:{ Exception -> 0x002f }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003e A[SYNTHETIC, Splitter:B:30:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0046 A[Catch:{ Exception -> 0x0042 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String mo24360a(android.graphics.Bitmap r5, android.content.Context r6) {
        /*
            r4 = this;
            java.lang.String r6 = m13425d(r6)
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x001f }
            r3 = 100
            r5.compress(r2, r3, r1)     // Catch:{ Exception -> 0x001f }
            r1.close()     // Catch:{ Exception -> 0x001a }
            if (r5 == 0) goto L_0x001e
            r5.recycle()     // Catch:{ Exception -> 0x001a }
            goto L_0x001e
        L_0x001a:
            r5 = move-exception
            r5.printStackTrace()
        L_0x001e:
            return r6
        L_0x001f:
            r6 = move-exception
            goto L_0x0026
        L_0x0021:
            r6 = move-exception
            r1 = r0
            goto L_0x003c
        L_0x0024:
            r6 = move-exception
            r1 = r0
        L_0x0026:
            r6.printStackTrace()     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ Exception -> 0x002f }
            goto L_0x0031
        L_0x002f:
            r5 = move-exception
            goto L_0x0037
        L_0x0031:
            if (r5 == 0) goto L_0x003a
            r5.recycle()     // Catch:{ Exception -> 0x002f }
            goto L_0x003a
        L_0x0037:
            r5.printStackTrace()
        L_0x003a:
            return r0
        L_0x003b:
            r6 = move-exception
        L_0x003c:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0042 }
            goto L_0x0044
        L_0x0042:
            r5 = move-exception
            goto L_0x004a
        L_0x0044:
            if (r5 == 0) goto L_0x004d
            r5.recycle()     // Catch:{ Exception -> 0x0042 }
            goto L_0x004d
        L_0x004a:
            r5.printStackTrace()
        L_0x004d:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p199j.PhotoUtils.mo24360a(android.graphics.Bitmap, android.content.Context):java.lang.String");
    }

    /* renamed from: a */
    public String mo24361a(String str, Context context) {
        return mo24360a(mo24356a(mo24355a(str), mo24357a(str, 2)), context);
    }

    /* renamed from: a */
    public int mo24355a(String str) {
        try {
            int attributeInt = new ExifInterface(str).getAttributeInt(androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION, 1);
            if (attributeInt == 3) {
                return 180;
            }
            if (attributeInt == 6) {
                return 90;
            }
            if (attributeInt != 8) {
                return 0;
            }
            return 270;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* renamed from: a */
    public Bitmap mo24356a(int i, Bitmap bitmap) {
        Bitmap bitmap2;
        Matrix matrix = new Matrix();
        matrix.postRotate((float) i);
        try {
            bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (OutOfMemoryError unused) {
            bitmap2 = null;
        }
        if (bitmap2 == null) {
            bitmap2 = bitmap;
        }
        if (bitmap != bitmap2) {
            bitmap.recycle();
        }
        return bitmap2;
    }

    /* renamed from: a */
    public Bitmap mo24357a(String str, int i) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inJustDecodeBounds = false;
        if (options.outHeight > 0 && options.outWidth > 0) {
            while (true) {
                if (options.outHeight / i <= 1080 && options.outWidth / i <= 1080) {
                    break;
                }
                i++;
            }
        } else {
            i <<= 1;
        }
        options.inSampleSize = i;
        try {
            return BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError unused) {
            options.inSampleSize *= 2;
            return BitmapFactory.decodeFile(str, options);
        }
    }
}
