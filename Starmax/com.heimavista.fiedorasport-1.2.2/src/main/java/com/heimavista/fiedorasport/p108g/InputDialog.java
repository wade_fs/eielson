package com.heimavista.fiedorasport.p108g;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$style;

/* renamed from: com.heimavista.fiedorasport.g.g */
public class InputDialog {

    /* renamed from: a */
    private Context f6468a;

    /* renamed from: b */
    private int f6469b = 1;

    /* renamed from: c */
    private String f6470c;

    /* renamed from: d */
    private String f6471d;

    /* renamed from: e */
    private String f6472e;

    /* renamed from: f */
    private String f6473f;

    /* renamed from: g */
    private C3701h f6474g;

    /* renamed from: h */
    private DialogInterface.OnDismissListener f6475h;

    public InputDialog(Context context) {
        this.f6468a = context;
    }

    /* renamed from: a */
    public InputDialog mo22854a(int i) {
        this.f6469b = i;
        return this;
    }

    /* renamed from: b */
    public InputDialog mo22861b(String str) {
        this.f6470c = str;
        return this;
    }

    /* renamed from: a */
    public InputDialog mo22857a(String str) {
        this.f6471d = str;
        return this;
    }

    /* renamed from: a */
    public InputDialog mo22856a(C3701h hVar) {
        this.f6474g = hVar;
        return this;
    }

    /* renamed from: a */
    public InputDialog mo22855a(DialogInterface.OnDismissListener onDismissListener) {
        this.f6475h = onDismissListener;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public void mo22858a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f6468a, R$style.HvGadDialog);
        View inflate = LayoutInflater.from(this.f6468a).inflate(R$layout.hv_gad_dialog_input_acid, (ViewGroup) null, false);
        TextView textView = (TextView) inflate.findViewById(16908310);
        if (!TextUtils.isEmpty(this.f6470c)) {
            textView.setText(this.f6470c);
        }
        EditText editText = (EditText) inflate.findViewById(16908291);
        editText.setImeOptions(2);
        editText.setInputType(this.f6469b);
        if (!TextUtils.isEmpty(this.f6472e)) {
            editText.setText(this.f6472e);
        }
        if (!TextUtils.isEmpty(this.f6471d)) {
            editText.setHint(this.f6471d);
        }
        ImageView imageView = (ImageView) inflate.findViewById(R$id.ivClose);
        Button button = (Button) inflate.findViewById(16908313);
        if (!TextUtils.isEmpty(this.f6473f)) {
            button.setText(this.f6473f);
        }
        builder.setView(inflate);
        builder.setCancelable(false);
        builder.setOnCancelListener(C3700d.f6466P);
        builder.setOnDismissListener(this.f6475h);
        AlertDialog create = builder.create();
        imageView.setOnClickListener(new C3697a(this, create));
        button.setOnClickListener(new C3698b(this, create, editText));
        create.show();
    }

    /* renamed from: a */
    public /* synthetic */ void mo22859a(AlertDialog alertDialog, View view) {
        C3701h hVar = this.f6474g;
        if (hVar != null) {
            hVar.mo22862a(view, alertDialog, false, null);
        } else {
            alertDialog.cancel();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22860a(AlertDialog alertDialog, EditText editText, View view) {
        C3701h hVar = this.f6474g;
        if (hVar != null) {
            hVar.mo22862a(view, alertDialog, true, editText.getText());
        } else {
            alertDialog.cancel();
        }
    }
}
