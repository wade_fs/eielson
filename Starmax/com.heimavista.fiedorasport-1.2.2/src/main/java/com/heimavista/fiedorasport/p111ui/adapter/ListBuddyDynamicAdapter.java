package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.heimavista.api.buddy.HvApiGetUserInfo;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.C4208d;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter */
public class ListBuddyDynamicAdapter extends BaseRecyclerViewAdapter<FriendBean, ViewHolder> {

    /* renamed from: i */
    private C4401b f8583i;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        View f8584R = findViewById(R$id.addFriend);

        /* renamed from: S */
        View f8585S = findViewById(R$id.layoutShareLoc);

        /* renamed from: T */
        View f8586T = findViewById(R$id.layoutAction);

        /* renamed from: U */
        View f8587U = findViewById(R$id.layoutDynamic);

        /* renamed from: V */
        ImageView f8588V = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: W */
        TextView f8589W = ((TextView) findViewById(R$id.tvUserName));

        /* renamed from: X */
        TextView f8590X = ((TextView) findViewById(R$id.tvTip));

        /* renamed from: Y */
        TextView f8591Y = ((TextView) findViewById(R$id.tvDate));

        /* renamed from: Z */
        TextView f8592Z = ((TextView) findViewById(R$id.tvWalkStep));

        /* renamed from: a0 */
        TextView f8593a0 = ((TextView) findViewById(R$id.tvSleepTime));

        /* renamed from: b0 */
        TextView f8594b0 = ((TextView) findViewById(R$id.tvHeartRate));

        /* renamed from: c0 */
        Button f8595c0 = ((Button) findViewById(R$id.btnRefuse));

        /* renamed from: d0 */
        Button f8596d0 = ((Button) findViewById(R$id.btnAccept));

        ViewHolder(ListBuddyDynamicAdapter listBuddyDynamicAdapter, ViewGroup viewGroup, int i) {
            super(listBuddyDynamicAdapter, viewGroup, i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter$a */
    class C4400a extends ThreadUtils.C0877e<UserInfoDb> {

        /* renamed from: U */
        final /* synthetic */ String f8597U;

        /* renamed from: V */
        final /* synthetic */ ViewHolder f8598V;

        C4400a(String str, ViewHolder viewHolder) {
            this.f8597U = str;
            this.f8598V = viewHolder;
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(UserInfoDb fVar) {
            if (fVar != null) {
                ListBuddyDynamicAdapter.this.m14307a(this.f8597U, this.f8598V);
            }
        }

        /* renamed from: b */
        public UserInfoDb m14322b() {
            return new HvApiGetUserInfo().requestGetUserSync(this.f8597U);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter$b */
    public interface C4401b {
        /* renamed from: a */
        void mo24626a(int i, FriendBean dVar);

        /* renamed from: a */
        void mo24627a(boolean z, int i, FriendBean dVar);

        /* renamed from: a */
        boolean mo24628a(View view, int i, FriendBean dVar);

        /* renamed from: b */
        void mo24629b(int i, FriendBean dVar);

        /* renamed from: c */
        void mo24630c(int i, FriendBean dVar);
    }

    public ListBuddyDynamicAdapter(Context context) {
        super(context);
    }

    /* renamed from: b */
    public /* synthetic */ void mo24619b(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24630c(i, dVar);
        }
    }

    /* renamed from: c */
    public /* synthetic */ void mo24620c(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24626a(i, dVar);
        }
    }

    /* renamed from: d */
    public /* synthetic */ void mo24621d(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24626a(i, dVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(boolean, int, com.heimavista.entity.data.d):void
     arg types: [int, int, com.heimavista.entity.data.d]
     candidates:
      com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(android.view.View, int, com.heimavista.entity.data.d):boolean
      com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(boolean, int, com.heimavista.entity.data.d):void */
    /* renamed from: e */
    public /* synthetic */ void mo24622e(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24627a(true, i, dVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(boolean, int, com.heimavista.entity.data.d):void
     arg types: [int, int, com.heimavista.entity.data.d]
     candidates:
      com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(android.view.View, int, com.heimavista.entity.data.d):boolean
      com.heimavista.fiedorasport.ui.adapter.ListBuddyDynamicAdapter.b.a(boolean, int, com.heimavista.entity.data.d):void */
    /* renamed from: f */
    public /* synthetic */ void mo24623f(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24627a(false, i, dVar);
        }
    }

    /* renamed from: g */
    public /* synthetic */ void mo24624g(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24629b(i, dVar);
        }
    }

    public int getItemViewType(int i) {
        return ((FriendBean) getItem(i)).mo22638c();
    }

    /* renamed from: a */
    public void mo24617a(C4401b bVar) {
        this.f8583i = bVar;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int i2;
        if (i == 0) {
            i2 = R$layout.layout_empty_dynamics;
        } else if (i == 1) {
            i2 = R$layout.layout_list_item_buddy_tips;
        } else if (i == 4) {
            i2 = R$layout.layout_list_item_buddy_add;
        } else if (i != 9) {
            i2 = R$layout.layout_list_item_buddy_dynamic;
        } else {
            i2 = R$layout.layout_list_item_buddy_build;
        }
        return new ViewHolder(this, viewGroup, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.i.d.a(java.lang.String, boolean):e.e.d.i.d
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.d.a(java.lang.String, int):e.e.d.i.d
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.i.d.a(java.lang.String, boolean):e.e.d.i.d */
    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int i2;
        String str;
        String str2;
        String str3;
        FriendBean dVar = (FriendBean) getItem(i);
        String d = dVar.mo22640d();
        long b = dVar.mo22637b();
        int c = dVar.mo22638c();
        TextView textView = viewHolder.f8591Y;
        if (textView != null) {
            textView.setText(TimeUtil.m13443a(mo22788b(), b * 1000));
        }
        int i3 = 0;
        if (c == 0) {
            viewHolder.itemView.setEnabled(false);
            viewHolder.f8584R.setOnClickListener(new C4415e(this, i, dVar));
        } else if (c == 1) {
            BuddyListDb l = BuddyListDb.m13250l(d);
            if (l == null || TextUtils.isEmpty(l.mo24306s())) {
                m14307a(d, viewHolder);
                return;
            }
            String s = l.mo24306s();
            String string = mo22788b().getString(R$string.send_invite_msg, s);
            int indexOf = string.indexOf(s);
            SpanUtils a = SpanUtils.m865a(viewHolder.f8590X);
            a.mo9735a(string.substring(0, indexOf));
            a.mo9735a(s);
            a.mo9737b(mo22796e().getColor(R$color.blue));
            a.mo9735a(string.substring(indexOf + s.length()));
            a.mo9736b();
        } else if (c == 2) {
            viewHolder.f8586T.setVisibility(0);
            viewHolder.f8587U.setVisibility(8);
            viewHolder.f8590X.setText(R$string.invite_friend_tips);
            UserInfoDb.m13339a(mo22788b(), d, viewHolder.f8589W, viewHolder.f8588V);
            viewHolder.f8596d0.setOnClickListener(new C4412b(this, i, dVar));
            viewHolder.f8595c0.setOnClickListener(new C4416f(this, i, dVar));
        } else if (c == 3) {
            viewHolder.itemView.setOnClickListener(new C4413c(this, i, dVar));
            viewHolder.itemView.setOnLongClickListener(new C4417g(this, viewHolder, i, dVar));
            viewHolder.f8586T.setVisibility(8);
            viewHolder.f8587U.setVisibility(0);
            viewHolder.f8590X.setText(R$string.update_health_data);
            UserInfoDb.m13339a(mo22788b(), d, viewHolder.f8589W, viewHolder.f8588V);
            BuddyListDb a2 = dVar.mo22636a();
            String str4 = "--";
            if (a2.mo24286D()) {
                int x = CurStepDataDb.m13136k(d).mo24250x();
                SpanUtils a3 = SpanUtils.m865a(viewHolder.f8592Z);
                if (x == 0) {
                    str3 = str4;
                } else {
                    str3 = String.valueOf(x);
                }
                a3.mo9735a(str3);
                a3.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_17));
                a3.mo9738c();
                a3.mo9735a(mo22781a(R$string.data_unit_step));
                a3.mo9736b();
            } else {
                viewHolder.f8592Z.setText(R$string.not_shared);
            }
            if (a2.mo24285C()) {
                SleepDayInfo a4 = new C4208d().mo24329a(d, 0);
                if (a4 == null) {
                    i2 = 0;
                } else {
                    i2 = a4.f6258g;
                }
                SpanUtils a5 = SpanUtils.m865a(viewHolder.f8593a0);
                if (i2 == 0) {
                    str = str4;
                } else {
                    str = String.valueOf(i2 / 60);
                }
                a5.mo9735a(str);
                a5.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_17));
                a5.mo9738c();
                a5.mo9735a(mo22781a(R$string.data_unit_hour));
                if (i2 == 0) {
                    str2 = str4;
                } else {
                    str2 = String.valueOf(i2 % 60);
                }
                a5.mo9735a(str2);
                a5.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_17));
                a5.mo9738c();
                a5.mo9735a(mo22781a(R$string.data_unit_min));
                a5.mo9736b();
            } else {
                viewHolder.f8593a0.setText(R$string.not_shared);
            }
            if (a2.mo24283A()) {
                HeartRateDataDb a6 = HeartRateDataDb.m13149a(d, false);
                if (a6 != null) {
                    i3 = a6.mo24232o();
                }
                SpanUtils a7 = SpanUtils.m865a(viewHolder.f8594b0);
                if (i3 != 0) {
                    str4 = String.valueOf(i3);
                }
                a7.mo9735a(str4);
                a7.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_17));
                a7.mo9738c();
                a7.mo9735a(mo22781a(R$string.data_unit_rate));
                a7.mo9736b();
                return;
            }
            viewHolder.f8594b0.setText(R$string.not_shared);
        } else if (c == 4) {
            viewHolder.f8584R.setOnClickListener(new C4414d(this, i, dVar));
            View view = viewHolder.f8585S;
            if (view != null) {
                view.setOnClickListener(new C4411a(this, i, dVar));
            }
        } else if (c == 9) {
            UserInfoDb.m13339a(mo22788b(), d, viewHolder.f8589W, viewHolder.f8588V);
            View view2 = viewHolder.f8585S;
            if (view2 != null) {
                view2.setOnClickListener(new C4418h(this, i, dVar));
            }
            if (dVar.mo22636a().mo24305r() == 1) {
                viewHolder.f8590X.setText(R$string.friend_build);
            } else if (dVar.mo22636a().mo24305r() == 2) {
                viewHolder.f8590X.setText(R$string.friend_in_build);
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24615a(int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            bVar.mo24630c(i, dVar);
        }
    }

    /* renamed from: a */
    public /* synthetic */ boolean mo24618a(ViewHolder viewHolder, int i, FriendBean dVar, View view) {
        C4401b bVar = this.f8583i;
        if (bVar != null) {
            return bVar.mo24628a(viewHolder.itemView, i, dVar);
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14307a(String str, ViewHolder viewHolder) {
        UserInfoDb p = UserInfoDb.m13344p(str);
        if (!TextUtils.isEmpty(p.mo24240q())) {
            BuddyListDb l = BuddyListDb.m13250l(str);
            if (l != null) {
                l.mo24301i(p.mo24337w());
                l.mo24181l();
            }
            String w = p.mo24337w();
            String string = mo22788b().getString(R$string.send_invite_msg, w);
            int indexOf = string.indexOf(w);
            SpanUtils a = SpanUtils.m865a(viewHolder.f8590X);
            a.mo9735a(string.substring(0, indexOf));
            a.mo9735a(w);
            a.mo9737b(mo22796e().getColor(R$color.blue));
            a.mo9738c();
            a.mo9735a(string.substring(indexOf + w.length()));
            a.mo9736b();
        } else if (NetworkUtils.m858c()) {
            ThreadUtils.m959b(new C4400a(str, viewHolder));
        }
    }
}
