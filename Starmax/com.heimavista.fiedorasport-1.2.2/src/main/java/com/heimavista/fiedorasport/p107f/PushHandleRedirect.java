package com.heimavista.fiedorasport.p107f;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.apn.PushHandler;
import com.heimavista.apn.PushMsg;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.sport.SportDetailActivity;
import org.json.JSONObject;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p191b.HvNotify;

/* renamed from: com.heimavista.fiedorasport.f.b */
public class PushHandleRedirect implements PushHandler {
    /* renamed from: a */
    public void mo22549a(Context context, PushMsg pushMsg) {
        int i;
        Context context2 = context;
        PushMsg pushMsg2 = pushMsg;
        LogUtils.m1139c("madyPushMsg>>>" + pushMsg.toString());
        JSONObject d = pushMsg2.f6231U.mo25334d();
        Intent intent = new Intent();
        intent.setClass(context2, HomeActivity.class);
        intent.putExtra("pushMsg", pushMsg2);
        JSONObject optJSONObject = d.optJSONObject("ext");
        LogUtils.m1139c("madyPushMsg>>> ext=" + optJSONObject);
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("pageId");
            if ("buddyList".equalsIgnoreCase(optString)) {
                HvApp.m13010c().mo24157a("fiedoraSport.ACTION_NOTIFY_BUDDY");
                i = 10001;
            } else if ("expCenter".equalsIgnoreCase(optString)) {
                i = 10002;
            } else if ("sportDetail".equalsIgnoreCase(optString)) {
                JSONObject optJSONObject2 = optJSONObject.optJSONObject("param");
                if (optJSONObject2 != null) {
                    intent.setClass(context2, SportDetailActivity.class);
                    intent.putExtra("userNbr", optJSONObject2.optString("userNbr"));
                    intent.putExtra("sportId", optJSONObject2.optString("sportId"));
                }
                i = 10003;
            }
            if (!TextUtils.isEmpty(pushMsg2.f6228R) && !TextUtils.equals("null", pushMsg2.f6228R)) {
                if (TextUtils.equals("null", pushMsg2.f6227Q)) {
                    pushMsg2.f6227Q = context2.getString(R$string.app_name);
                }
                HvNotify.m13018b().mo24163a(context, "channel_fs_push", context2.getString(R$string.channel_push), 3, pushMsg2.f6227Q, R$mipmap.ic_push, BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon), PendingIntent.getActivity(context2, i, intent, 134217728), pushMsg2.f6228R, pushMsg2.f6227Q, 0, i);
                return;
            }
        }
        i = 1;
        if (!TextUtils.isEmpty(pushMsg2.f6228R)) {
        }
    }
}
