package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.c */
/* compiled from: lambda */
public final /* synthetic */ class C4358c implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ SportActivity f8476a;

    /* renamed from: b */
    private final /* synthetic */ LatLng f8477b;

    public /* synthetic */ C4358c(SportActivity sportActivity, LatLng latLng) {
        this.f8476a = sportActivity;
        this.f8477b = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8476a.mo24531a(this.f8477b, bitmap);
    }
}
