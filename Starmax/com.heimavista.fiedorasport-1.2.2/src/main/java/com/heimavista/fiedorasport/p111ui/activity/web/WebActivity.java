package com.heimavista.fiedorasport.p111ui.activity.web;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.widget.ProgressBar;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.widget.HvWebView;

/* renamed from: com.heimavista.fiedorasport.ui.activity.web.WebActivity */
public class WebActivity extends BaseActivity {

    /* renamed from: b0 */
    private HvWebView f8564b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public ProgressBar f8565c0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.web.WebActivity$a */
    class C4395a implements HvWebView.C4599d {
        C4395a() {
        }

        /* renamed from: a */
        public void mo24581a(int i) {
            WebActivity.this.f8565c0.setProgress(i);
        }

        public void onFinished() {
            WebActivity.this.f8565c0.setVisibility(8);
        }

        public void onStart() {
            WebActivity.this.f8565c0.setVisibility(0);
        }

        /* renamed from: a */
        public void mo24582a(String str) {
            if (!TextUtils.isEmpty(str) && !URLUtil.isNetworkUrl(str)) {
                WebActivity.this.mo22701a(str);
            }
        }
    }

    /* renamed from: u */
    private void m14291u() {
        WebSettings settings = this.f8564b0.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setCacheMode(-1);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setGeolocationEnabled(true);
        settings.setBuiltInZoomControls(true);
        this.f8564b0.setHorizontalScrollBarEnabled(false);
        this.f8564b0.setVerticalScrollbarOverlay(true);
        this.f8564b0.setScrollBarStyle(33554432);
        this.f8564b0.setOnProgressListener(new C4395a());
        WebSettings settings2 = this.f8564b0.getSettings();
        settings2.setUserAgentString(settings2.getUserAgentString() + " FiedoraApp");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8565c0 = (ProgressBar) findViewById(R$id.progressBar);
        this.f8564b0 = (HvWebView) findViewById(R$id.webview);
        m14291u();
        this.f8564b0.loadUrl(getIntent().getStringExtra("url"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_web;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getIntent().getStringExtra("title");
    }

    public void onBackPressed() {
        if (this.f8564b0.canGoBack()) {
            this.f8564b0.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* renamed from: a */
    public static Intent m14288a(Context context, String str, String str2) {
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("title", str);
        intent.putExtra("url", str2);
        return intent;
    }
}
