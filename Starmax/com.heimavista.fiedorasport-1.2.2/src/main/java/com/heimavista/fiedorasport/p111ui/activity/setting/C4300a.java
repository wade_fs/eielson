package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.view.View;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.a */
/* compiled from: lambda */
public final /* synthetic */ class C4300a implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ AlarmAdapter f8289P;

    /* renamed from: Q */
    private final /* synthetic */ int f8290Q;

    /* renamed from: R */
    private final /* synthetic */ AlarmInfoItem f8291R;

    public /* synthetic */ C4300a(AlarmAdapter alarmAdapter, int i, AlarmInfoItem alarmInfoItem) {
        this.f8289P = alarmAdapter;
        this.f8290Q = i;
        this.f8291R = alarmInfoItem;
    }

    public final void onClick(View view) {
        this.f8289P.mo24462a(this.f8290Q, this.f8291R, view);
    }
}
