package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.data.SportBean;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.C4222l;
import java.util.Locale;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p193d.p196k.SportLikeUserDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListDynamicsAdapter */
public class ListDynamicsAdapter extends BaseRecyclerViewAdapter<SportBean, ViewHolder> {

    /* renamed from: i */
    private C4404a f8609i;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListDynamicsAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        TextView f8610R = ((TextView) findViewById(R$id.tvDate));

        /* renamed from: S */
        TextView f8611S = ((TextView) findViewById(R$id.tvUserName));

        /* renamed from: T */
        TextView f8612T = ((TextView) findViewById(R$id.tvStatus));

        /* renamed from: U */
        TextView f8613U = ((TextView) findViewById(R$id.tvLike));

        /* renamed from: V */
        ImageView f8614V = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: W */
        ImageView f8615W = ((ImageView) findViewById(R$id.ivLike));

        /* renamed from: X */
        ImageView f8616X = ((ImageView) findViewById(R$id.ivSportType));

        /* renamed from: Y */
        LinearLayout f8617Y = ((LinearLayout) findViewById(R$id.addFriend));

        /* renamed from: Z */
        TextView f8618Z = ((TextView) findViewById(R$id.tvSportStep));

        /* renamed from: a0 */
        TextView f8619a0 = ((TextView) findViewById(R$id.tvSportDistance));

        /* renamed from: b0 */
        TextView f8620b0 = ((TextView) findViewById(R$id.tvSportDuration));

        /* renamed from: c0 */
        TextView f8621c0 = ((TextView) findViewById(R$id.tvSportAvgHeartRate));

        ViewHolder(ListDynamicsAdapter listDynamicsAdapter, ViewGroup viewGroup, int i) {
            super(listDynamicsAdapter, viewGroup, i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListDynamicsAdapter$a */
    public interface C4404a {
        /* renamed from: a */
        void mo24641a(int i, SportBean gVar);

        /* renamed from: b */
        void mo24642b(int i, SportBean gVar);

        /* renamed from: c */
        void mo24643c(int i, SportBean gVar);

        /* renamed from: d */
        void mo24644d(int i, SportBean gVar);
    }

    public ListDynamicsAdapter(Context context) {
        super(context);
    }

    /* renamed from: a */
    public void mo24636a(C4404a aVar) {
        this.f8609i = aVar;
    }

    /* renamed from: b */
    public /* synthetic */ void mo24637b(int i, SportBean gVar, View view) {
        this.f8609i.mo24641a(i, gVar);
    }

    /* renamed from: c */
    public /* synthetic */ void mo24638c(int i, SportBean gVar, View view) {
        this.f8609i.mo24641a(i, gVar);
    }

    /* renamed from: d */
    public /* synthetic */ boolean mo24639d(int i, SportBean gVar, View view) {
        this.f8609i.mo24642b(i, gVar);
        return false;
    }

    /* renamed from: e */
    public /* synthetic */ void mo24640e(int i, SportBean gVar, View view) {
        C4404a aVar = this.f8609i;
        if (aVar != null) {
            aVar.mo24643c(i, gVar);
        }
    }

    public int getItemViewType(int i) {
        return ((SportBean) getItem(i)).mo22675d();
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String str;
        String str2;
        SportBean gVar = (SportBean) getItem(i);
        int d = gVar.mo22675d();
        if (d == 0) {
            viewHolder.itemView.setEnabled(false);
            viewHolder.f8617Y.setOnClickListener(new C4423m(this, i, gVar));
        } else if (d == 1) {
            viewHolder.f8610R.setText(TimeUtils.m998a(gVar.mo22673c() * 1000, "M月d日 HH:mm"));
            UserInfoDb.m13339a(mo22788b(), gVar.mo22671a(), viewHolder.f8611S, viewHolder.f8614V);
            if (this.f8609i != null) {
                viewHolder.itemView.setOnClickListener(new C4420j(this, i, gVar));
            }
        } else if (d == 2 || d == 3) {
            if (this.f8609i != null) {
                viewHolder.itemView.setOnClickListener(new C4419i(this, i, gVar));
                viewHolder.itemView.setOnLongClickListener(new C4422l(this, i, gVar));
            }
            SportInfo b = gVar.mo22672b();
            if (b != null) {
                UserInfoDb.m13339a(mo22788b(), b.f6270a, viewHolder.f8611S, viewHolder.f8614V);
                viewHolder.f8610R.setText(TimeUtils.m998a(b.f6274e * 1000, "M月d日 HH:mm"));
                boolean k = SportLikeUserDb.m13374k(b.f6272c);
                if (!k) {
                    viewHolder.f8615W.setOnClickListener(new C4421k(this, i, gVar));
                }
                viewHolder.f8613U.setText(String.valueOf(b.f6282m));
                viewHolder.f8615W.setImageResource(k ? R$mipmap.icon_like_selected : R$mipmap.icon_like_unselected);
                int i2 = b.f6273d;
                int i3 = b.f6277h;
                String str3 = "--";
                if (i2 == 0 || i2 == 1) {
                    viewHolder.f8612T.setText(mo22781a(i2 == 0 ? R$string.update_sport_run : R$string.update_sport_walk));
                    viewHolder.f8616X.setImageResource(i2 == 0 ? R$mipmap.icon_sports_run : R$mipmap.icon_sports_walk);
                    int a = C4222l.m13451a(i3);
                    SpanUtils a2 = SpanUtils.m865a(viewHolder.f8618Z);
                    if (a == 0) {
                        str2 = str3;
                    } else {
                        str2 = String.valueOf(a);
                    }
                    a2.mo9735a(str2);
                    a2.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                    a2.mo9735a(mo22781a(R$string.data_unit_step));
                    a2.mo9736b();
                } else if (i2 == 2) {
                    viewHolder.f8612T.setText(mo22781a(R$string.update_sport_bike));
                    viewHolder.f8616X.setImageResource(R$mipmap.icon_sports_bike);
                }
                SpanUtils a3 = SpanUtils.m865a(viewHolder.f8619a0);
                if (i3 == 0) {
                    str = str3;
                } else {
                    str = String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) i3) / 1000.0f));
                }
                a3.mo9735a(str);
                a3.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a3.mo9735a(mo22781a(R$string.data_unit_mill));
                a3.mo9736b();
                int i4 = b.f6276g;
                SpanUtils a4 = SpanUtils.m865a(viewHolder.f8620b0);
                int i5 = i4 / 3600;
                if (i5 > 0) {
                    a4.mo9735a(String.valueOf(i5));
                    a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                    a4.mo9735a(mo22781a(R$string.data_unit_hour));
                }
                a4.mo9735a(String.valueOf((i4 / 60) % 60));
                a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a4.mo9735a(mo22781a(R$string.data_unit_min));
                a4.mo9735a(String.valueOf(i4 % 60));
                a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a4.mo9735a(mo22781a(R$string.data_unit_second));
                a4.mo9736b();
                SpanUtils a5 = SpanUtils.m865a(viewHolder.f8621c0);
                int i6 = b.f6279j;
                if (i6 != 0) {
                    str3 = String.valueOf(i6);
                }
                a5.mo9735a(str3);
                a5.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a5.mo9735a(mo22781a(R$string.data_unit_rate));
                a5.mo9736b();
            }
        }
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int i2;
        if (i == 0) {
            i2 = R$layout.layout_empty_dynamics;
        } else if (i == 1) {
            i2 = R$layout.layout_list_item_dynamics_like;
        } else if (i == 2) {
            i2 = R$layout.layout_list_item_dynamics_sport;
        } else {
            i2 = R$layout.layout_list_item_dynamics_sport_cycling;
        }
        return new ViewHolder(this, viewGroup, i2);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24634a(int i, SportBean gVar, View view) {
        C4404a aVar = this.f8609i;
        if (aVar != null) {
            aVar.mo24644d(i, gVar);
        }
    }
}
