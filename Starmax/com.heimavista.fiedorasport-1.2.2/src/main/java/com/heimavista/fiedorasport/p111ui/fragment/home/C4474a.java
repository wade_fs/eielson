package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.graphics.Bitmap;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.a */
/* compiled from: lambda */
public final /* synthetic */ class C4474a implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ MainHomeFragment f8864a;

    /* renamed from: b */
    private final /* synthetic */ double f8865b;

    /* renamed from: c */
    private final /* synthetic */ double f8866c;

    public /* synthetic */ C4474a(MainHomeFragment mainHomeFragment, double d, double d2) {
        this.f8864a = mainHomeFragment;
        this.f8865b = d;
        this.f8866c = d2;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8864a.mo24712a(this.f8865b, this.f8866c, bitmap);
    }
}
