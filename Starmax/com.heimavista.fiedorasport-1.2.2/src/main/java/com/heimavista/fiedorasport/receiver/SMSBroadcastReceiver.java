package com.heimavista.fiedorasport.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;

public class SMSBroadcastReceiver extends BroadcastReceiver {
    /* renamed from: a */
    private void m10692a(Context context, String str, String str2) {
        if (FSManager.m10323r().mo22846p() && CRPBleManager.m10639j()) {
            CRPBleManager.m10605a(str + ":" + str2, 0);
        }
        if (FSManager.m10323r().mo22847q() && JYSDKManager.m10388i().mo22897e()) {
            JYSDKManager i = JYSDKManager.m10388i();
            i.mo22879a((System.currentTimeMillis() / 1000) + "", 1, str, str2);
        }
    }

    public void onReceive(Context context, Intent intent) {
        Object[] objArr;
        boolean g = BandDataManager.m10563e().mo22612g();
        LogUtils.m1139c("SMSBroadcastReceiver onReceive remind=" + g);
        if (g && intent != null && intent.getExtras() != null && (objArr = (Object[]) intent.getExtras().get("pdus")) != null) {
            for (Object obj : objArr) {
                SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) obj);
                String originatingAddress = createFromPdu.getOriginatingAddress();
                String messageBody = createFromPdu.getMessageBody();
                m10692a(context, originatingAddress, messageBody);
                LogUtils.m1139c("SMSBroadcastReceiver", "mobile=" + originatingAddress, "content=" + messageBody);
            }
        }
    }
}
