package com.heimavista.fiedorasport.lib;

public final class R$xml {
    public static final int accessibibity_service_message_config = 2132017152;
    public static final int network_security_config = 2132017153;
    public static final int provider_paths = 2132017154;
    public static final int standalone_badge = 2132017155;
    public static final int standalone_badge_gravity_bottom_end = 2132017156;
    public static final int standalone_badge_gravity_bottom_start = 2132017157;
    public static final int standalone_badge_gravity_top_start = 2132017158;
    public static final int standalone_badge_offset = 2132017159;
    public static final int util_code_provider_paths = 2132017160;

    private R$xml() {
    }
}
