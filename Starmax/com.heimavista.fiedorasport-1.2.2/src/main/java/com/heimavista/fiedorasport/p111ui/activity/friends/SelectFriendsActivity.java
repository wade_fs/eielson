package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.heimavista.api.HvApiEncryptUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiReqBuddyByNbr;
import com.heimavista.entity.data.SelectFriendBean;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$drawable;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.adapter.ListSelectFriendAdapter;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SelectFriendsActivity */
public class SelectFriendsActivity extends BaseActivity {

    /* renamed from: b0 */
    private RecyclerView f8105b0;

    /* renamed from: c0 */
    private ListSelectFriendAdapter f8106c0;

    /* renamed from: d0 */
    private List<SelectFriendBean> f8107d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public ArrayList<String> f8108e0;

    /* renamed from: f0 */
    private String f8109f0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SelectFriendsActivity$a */
    class C4235a implements View.OnClickListener {
        C4235a() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putStringArrayListExtra("userNbrList", SelectFriendsActivity.this.f8108e0);
            SelectFriendsActivity.this.setResult(-1, intent);
            SelectFriendsActivity.this.onBackPressed();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SelectFriendsActivity$b */
    class C4236b implements OnResultListener<ParamJsonData> {
        C4236b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, com.heimavista.fiedorasport.ui.activity.friends.t]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            SelectFriendsActivity.this.mo22717b();
            SelectFriendsActivity selectFriendsActivity = SelectFriendsActivity.this;
            selectFriendsActivity.mo22707a("", selectFriendsActivity.getString(R$string.invite_tips), SelectFriendsActivity.this.getString(R$string.f6600ok), true, false, (BaseDialog.C4621m) new C4256t(this));
        }

        /* renamed from: a */
        public /* synthetic */ void mo24412a(boolean z) {
            if (z) {
                SelectFriendsActivity.this.finish();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            SelectFriendsActivity.this.mo22717b();
            SelectFriendsActivity.this.mo22730c(str);
        }
    }

    /* renamed from: d */
    private void m13590d(String str) {
        if (mo22741l()) {
            mo22751t();
            new HvApiReqBuddyByNbr().request(str, new C4236b());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.friends.u]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* renamed from: e */
    private void m13591e(String str) {
        mo22708a(getString(R$string.invite_request_tips, new Object[]{this.f8109f0}), getString(R$string.invite), false, (BaseDialog.C4621m) new C4257u(this, str));
    }

    /* renamed from: u */
    private void m13592u() {
        mo22704a(MyCaptureActivity.class, new C4259w(this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8105b0 = (RecyclerView) findViewById(R$id.recyclerView);
        this.f8106c0 = new ListSelectFriendAdapter(this);
        this.f8106c0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4260x(this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common_with_divider;
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    /* renamed from: k */
    public int mo22740k() {
        return 17760263;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mo22680a(menu, 16908331, getString(R$string.add), -1, getResources().getDrawable(R$drawable.selector_blue), new C4235a());
        return super.onCreateOptionsMenu(menu);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24405a(int i, Intent intent) {
        if (intent != null) {
            String stringExtra = intent.getStringExtra("content");
            LogUtils.m1139c("qrContent:" + stringExtra);
            try {
                String a = HvApiEncryptUtils.m10000a(stringExtra, "fiedoraSport");
                LogUtils.m1139c("qrContent decryptDES:" + a);
                JSONObject jSONObject = new JSONObject(a);
                if (!jSONObject.has("userNbr") || !jSONObject.has("userName")) {
                    SnackbarUtils a2 = SnackbarUtils.m927a(this.f8105b0);
                    a2.mo9783a(getString(R$string.err_qr_code));
                    a2.mo9782a(0);
                    a2.mo9787b();
                    return;
                }
                this.f8109f0 = jSONObject.getString("userName");
                m13591e(jSONObject.getString("userNbr"));
            } catch (Exception unused) {
                SnackbarUtils a3 = SnackbarUtils.m927a(this.f8105b0);
                a3.mo9783a(getString(R$string.err_qr_code));
                a3.mo9782a(0);
                a3.mo9787b();
            }
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24407b(View view, SelectFriendBean fVar, int i) {
        if (i == 0) {
            List c = this.f8106c0.mo22794c();
            if (c != null && !c.isEmpty()) {
                if (c.size() == 1) {
                    m13589b(true);
                } else if (((SelectFriendBean) c.get(1)).mo22667b() == 1) {
                    m13589b(true);
                } else {
                    m13589b(false);
                }
            }
        } else {
            String c2 = fVar.mo22668c();
            if (fVar.mo22667b() != 2) {
                CheckBox checkBox = (CheckBox) view.findViewById(R$id.f6592cb);
                checkBox.toggle();
                if (checkBox.isChecked()) {
                    ArrayList<String> arrayList = this.f8108e0;
                    if (arrayList == null) {
                        this.f8108e0 = new ArrayList<>();
                        this.f8108e0.add(c2);
                    } else if (!arrayList.contains(c2)) {
                        this.f8108e0.add(c2);
                    }
                } else {
                    ArrayList<String> arrayList2 = this.f8108e0;
                    if (arrayList2 != null) {
                        arrayList2.remove(c2);
                    }
                }
            } else if (getString(R$string.add_friends_by_scan).equals(c2)) {
                m13592u();
            } else if (getString(R$string.activities_qr_code).equals(c2)) {
                mo22702a(QrCodeActivity.class);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.entity.data.f.<init>(int, java.lang.String, boolean):void
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.heimavista.entity.data.f.<init>(int, java.lang.String, int):void
      com.heimavista.entity.data.f.<init>(int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.entity.data.f.<init>(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      com.heimavista.entity.data.f.<init>(int, java.lang.String, int):void
      com.heimavista.entity.data.f.<init>(int, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8106c0.mo22785a(new SelectFriendBean(0, (String) null, false));
        for (BuddyListDb aVar : BuddyListDb.m13246G()) {
            if (aVar.mo24312y() == 9) {
                this.f8106c0.mo22785a(new SelectFriendBean(1, aVar.mo24311x(), true));
            }
        }
        this.f8106c0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4258v(this));
        this.f8105b0.setAdapter(this.f8106c0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24406a(View view, SelectFriendBean fVar, int i) {
        if (fVar.mo22667b() == 0) {
            Constant.m10311a("添加好友點擊");
            mo22729c(ListAddFriendsActivity.class);
            return;
        }
        ((CheckBox) view.findViewById(R$id.f6592cb)).toggle();
    }

    /* renamed from: b */
    public /* synthetic */ void mo24408b(String str, boolean z) {
        if (z) {
            m13590d(str);
        }
    }

    /* renamed from: b */
    private void m13589b(boolean z) {
        if (this.f8107d0 == null) {
            this.f8107d0 = new ArrayList();
            this.f8107d0.add(new SelectFriendBean(2, getString(R$string.add_friends_by_scan), R$mipmap.ic_friends_scan2));
            this.f8107d0.add(new SelectFriendBean(2, getString(R$string.activities_qr_code), R$mipmap.ic_friends_qrcode));
        }
        if (z) {
            this.f8106c0.mo22787a(this.f8107d0, 1);
        } else {
            this.f8106c0.mo22793b(this.f8107d0, 1);
        }
    }
}
