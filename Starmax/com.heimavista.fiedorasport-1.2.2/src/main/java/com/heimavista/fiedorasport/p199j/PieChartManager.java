package com.heimavista.fiedorasport.p199j;

import android.graphics.Typeface;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.Easing;
import p119e.p128c.p129a.p130a.p133c.PercentFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p143j.MPPointF;

/* renamed from: com.heimavista.fiedorasport.j.j */
public class PieChartManager {

    /* renamed from: a */
    public PieChart f8047a;

    /* renamed from: b */
    private int f8048b;

    public PieChartManager(PieChart pieChart, int i, CharSequence charSequence) {
        this.f8047a = pieChart;
        this.f8048b = i;
        m13439a(charSequence);
    }

    /* renamed from: a */
    private void m13439a(CharSequence charSequence) {
        this.f8047a.setUsePercentValues(true);
        this.f8047a.getDescription().mo13233a(false);
        this.f8047a.mo13022a(5.0f, 5.0f, 5.0f, 5.0f);
        this.f8047a.setDragDecelerationFrictionCoef(0.95f);
        this.f8047a.setCenterText(charSequence);
        this.f8047a.setCenterTextSize(16.0f);
        this.f8047a.setHoleColor(-1);
        this.f8047a.setTransparentCircleColor(-1);
        this.f8047a.setTransparentCircleAlpha(110);
        this.f8047a.setDrawCenterText(true);
        this.f8047a.setRotationAngle(180.0f);
        this.f8047a.setRotationEnabled(false);
        this.f8047a.setHighlightPerTapEnabled(true);
        this.f8047a.mo13024a(this.f8048b, Easing.f6926b);
        Legend legend = this.f8047a.getLegend();
        legend.mo13248a(Legend.C1652f.CENTER);
        legend.mo13246a(Legend.C1650d.RIGHT);
        legend.mo13247a(Legend.C1651e.VERTICAL);
        legend.mo13251b(false);
        legend.mo13252d(14.0f);
        legend.mo13253e(5.0f);
        legend.mo13254f(10.0f);
        legend.mo13235b(0.0f);
        legend.mo13237c(0.0f);
        legend.mo13230a(14.0f);
        this.f8047a.setDrawEntryLabels(false);
        this.f8047a.setEntryLabelColor(-1);
        this.f8047a.setEntryLabelTextSize(12.0f);
        this.f8047a.setEntryLabelTypeface(Typeface.DEFAULT_BOLD);
    }

    /* renamed from: b */
    public void mo24369b(List<PieEntry> list, List<Integer> list2, String str) {
        PieDataSet oVar = new PieDataSet(list, str);
        oVar.mo13341a(list2);
        oVar.mo13430d(2.0f);
        oVar.mo13429c(0.0f);
        oVar.mo13340a(new MPPointF(10.0f, 10.0f));
        oVar.mo13344b(false);
        oVar.mo13431d(false);
        PieData nVar = new PieData(oVar);
        nVar.mo13380a(new PercentFormatter());
        nVar.mo13377a(14.0f);
        nVar.mo13385b(-1);
        this.f8047a.setData(nVar);
        this.f8047a.mo13028a((Highlight[]) null);
        this.f8047a.invalidate();
    }

    /* renamed from: a */
    public void mo24368a(List<PieEntry> list, List<Integer> list2, String str) {
        this.f8047a.setDrawHoleEnabled(true);
        this.f8047a.setHoleRadius(50.0f);
        this.f8047a.setTransparentCircleRadius(50.0f);
        mo24369b(list, list2, str);
    }
}
