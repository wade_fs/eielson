package com.heimavista.fiedorasport.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.StringRes;
import com.blankj.utilcode.util.ConvertUtils;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$style;
import com.heimavista.fiedorasport.lib.R$styleable;
import com.heimavista.widget.SwitchButton;

public class CommonListItemView extends RelativeLayout {

    /* renamed from: P */
    private int f8903P;

    /* renamed from: Q */
    protected ImageView f8904Q;

    /* renamed from: R */
    private ViewGroup f8905R;

    /* renamed from: S */
    private int f8906S;

    /* renamed from: T */
    protected TextView f8907T;

    /* renamed from: U */
    protected TextView f8908U;

    /* renamed from: V */
    protected ImageView f8909V;

    /* renamed from: W */
    protected SwitchButton f8910W;

    public CommonListItemView(Context context) {
        this(context, null);
    }

    private ImageView getAccessoryImageView() {
        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(getAccessoryLayoutParams());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        return imageView;
    }

    private ViewGroup.LayoutParams getAccessoryLayoutParams() {
        return new ViewGroup.LayoutParams(-2, -2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.heimavista.fiedorasport.widget.CommonListItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24737a(Context context, AttributeSet attributeSet, int i) {
        LayoutInflater.from(context).inflate(R$layout.common_list_item, (ViewGroup) this, true);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.CommonListItemView, i, 0);
        int i2 = obtainStyledAttributes.getInt(R$styleable.CommonListItemView_list_accessoryType, 0);
        int color = obtainStyledAttributes.getColor(R$styleable.CommonListItemView_list_titleColor, getResources().getColor(R$color.font_color));
        int color2 = obtainStyledAttributes.getColor(R$styleable.CommonListItemView_list_detailColor, getResources().getColor(R$color.font_color_hint));
        Drawable drawable = obtainStyledAttributes.getDrawable(R$styleable.CommonListItemView_src);
        String string = obtainStyledAttributes.getString(R$styleable.CommonListItemView_text);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R$styleable.CommonListItemView_list_titleTextSize, ConvertUtils.m1056b(15.0f));
        int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(R$styleable.CommonListItemView_list_detailTextSize, ConvertUtils.m1056b(13.0f));
        this.f8906S = obtainStyledAttributes.getResourceId(R$styleable.CommonListItemView_list_chevronRes, R$mipmap.ic_arrow);
        obtainStyledAttributes.recycle();
        this.f8904Q = (ImageView) findViewById(R$id.group_list_item_imageView);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R$id.group_list_item_textContainer);
        this.f8907T = (TextView) findViewById(R$id.group_list_item_textView);
        this.f8907T.setTextColor(color);
        this.f8908U = (TextView) findViewById(R$id.group_list_item_detailTextView);
        this.f8909V = (ImageView) findViewById(R$id.group_list_item_detailImage);
        this.f8908U.setTextColor(color2);
        this.f8905R = (ViewGroup) findViewById(R$id.group_list_item_accessoryView);
        this.f8907T.setTextSize(0, (float) dimensionPixelSize);
        this.f8908U.setTextSize(0, (float) dimensionPixelSize2);
        setText(string);
        setImageDrawable(drawable);
        setAccessoryType(i2);
    }

    public ViewGroup getAccessoryContainerView() {
        return this.f8905R;
    }

    public int getAccessoryType() {
        return this.f8903P;
    }

    public ImageView getDetailImageView() {
        return this.f8909V;
    }

    public CharSequence getDetailText() {
        return this.f8908U.getText();
    }

    public TextView getDetailTextView() {
        return this.f8908U;
    }

    public SwitchButton getSwitch() {
        return this.f8910W;
    }

    public CharSequence getText() {
        return this.f8907T.getText();
    }

    public TextView getTextView() {
        return this.f8907T;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.heimavista.fiedorasport.widget.CommonListItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setAccessoryType(int i) {
        this.f8905R.removeAllViews();
        this.f8903P = i;
        if (i == 0) {
            this.f8905R.setVisibility(8);
        } else if (i == 1) {
            ImageView accessoryImageView = getAccessoryImageView();
            accessoryImageView.setImageResource(this.f8906S);
            this.f8905R.addView(accessoryImageView);
            this.f8905R.setVisibility(0);
        } else if (i == 2) {
            if (this.f8910W == null) {
                this.f8910W = (SwitchButton) LayoutInflater.from(getContext()).inflate(R$layout.common_switchbutton, (ViewGroup) this, false);
                this.f8910W.setLayoutParams(new RelativeLayout.LayoutParams(getContext().getResources().getDimensionPixelSize(R$dimen.switch_button_width), getContext().getResources().getDimensionPixelSize(R$dimen.switch_button_height)));
            }
            this.f8905R.addView(this.f8910W);
            this.f8905R.setVisibility(0);
        } else if (i == 3) {
            this.f8905R.setVisibility(0);
        }
    }

    public void setDetailText(CharSequence charSequence) {
        this.f8908U.setText(charSequence);
        this.f8909V.requestLayout();
        if (TextUtils.isEmpty(charSequence)) {
            this.f8908U.setVisibility(8);
        } else {
            this.f8908U.setVisibility(0);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        if (drawable == null) {
            this.f8904Q.setVisibility(8);
            return;
        }
        this.f8904Q.setImageDrawable(drawable);
        this.f8904Q.setVisibility(0);
    }

    public void setImageResource(int i) {
        if (i > 0) {
            this.f8904Q.setImageResource(i);
            this.f8904Q.setVisibility(0);
            return;
        }
        this.f8904Q.setVisibility(8);
    }

    public void setText(@StringRes int i) {
        this.f8907T.setText(i);
        this.f8907T.setVisibility(0);
    }

    public CommonListItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$style.CommonListItemViewStyle);
    }

    public CommonListItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo24737a(context, attributeSet, i);
    }

    public void setText(CharSequence charSequence) {
        this.f8907T.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            this.f8907T.setVisibility(8);
        } else {
            this.f8907T.setVisibility(0);
        }
    }

    /* renamed from: a */
    public void mo24738a(View view) {
        if (this.f8903P == 3) {
            this.f8905R.addView(view);
        }
    }
}
