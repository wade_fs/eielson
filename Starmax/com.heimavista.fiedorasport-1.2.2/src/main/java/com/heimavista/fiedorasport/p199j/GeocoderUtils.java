package com.heimavista.fiedorasport.p199j;

import android.location.Address;
import android.location.Geocoder;
import com.blankj.utilcode.util.ThreadUtils;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.j.f */
public class GeocoderUtils {

    /* renamed from: com.heimavista.fiedorasport.j.f$a */
    /* compiled from: GeocoderUtils */
    static class C4220a extends ThreadUtils.C0877e<List<Address>> {

        /* renamed from: U */
        final /* synthetic */ double f8038U;

        /* renamed from: V */
        final /* synthetic */ double f8039V;

        /* renamed from: W */
        final /* synthetic */ C4221b f8040W;

        C4220a(double d, double d2, C4221b bVar) {
            this.f8038U = d;
            this.f8039V = d2;
            this.f8040W = bVar;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24352a((List<Address>) ((List) obj));
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24352a(List<Address> list) {
            C4221b bVar;
            if (list != null && (bVar = this.f8040W) != null) {
                bVar.mo24354a(list);
            }
        }

        /* renamed from: b */
        public List<Address> mo9798b() {
            return new Geocoder(HvApp.m13010c(), Locale.getDefault()).getFromLocation(this.f8038U, this.f8039V, 5);
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
            C4221b bVar = this.f8040W;
            if (bVar != null) {
                bVar.mo24353a(th);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.j.f$b */
    /* compiled from: GeocoderUtils */
    public interface C4221b {
        /* renamed from: a */
        void mo24353a(Throwable th);

        /* renamed from: a */
        void mo24354a(List<Address> list);
    }

    /* renamed from: a */
    public static void m13412a(double d, double d2, C4221b bVar) {
        ThreadUtils.m959b(new C4220a(d, d2, bVar));
    }
}
