package com.heimavista.fiedorasport.p108g;

import android.content.Context;
import android.view.View;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.UIDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.fiedorasport.g.i */
public class SportTipDialog extends UIDialog<SportTipDialog> {

    /* renamed from: m0 */
    BaseDialog.C4621m f6476m0;

    public SportTipDialog(Context context) {
        super(context);
        mo25419c(R$layout.dialog_sport_stop);
        mo25416b(AnimAction.f7973c);
        mo25422d(17);
        mo25427g((ScreenUtils.m1265c() * 7) / 10);
        mo25475k(R$string.stop);
        mo25473i(R$string.stop);
        mo25417b(false);
        mo25413a(false);
        mo24144a(R$id.dialog_positive_btn, R$id.dialog_negative_btn);
    }

    /* renamed from: a */
    public SportTipDialog mo22863a(BaseDialog.C4621m mVar) {
        this.f6476m0 = mVar;
        return this;
    }

    /* renamed from: d */
    public SportTipDialog mo22864d(CharSequence charSequence) {
        mo25409a(R$id.dialog_content, charSequence);
        return this;
    }

    public void onClick(View view) {
        if (view.getId() == R$id.dialog_positive_btn) {
            mo25471h();
            BaseDialog.C4621m mVar = this.f6476m0;
            if (mVar != null) {
                mVar.mo23034a(true);
            }
        } else if (view.getId() == R$id.dialog_negative_btn) {
            mo25471h();
            BaseDialog.C4621m mVar2 = this.f6476m0;
            if (mVar2 != null) {
                mVar2.mo23034a(false);
            }
        }
    }
}
