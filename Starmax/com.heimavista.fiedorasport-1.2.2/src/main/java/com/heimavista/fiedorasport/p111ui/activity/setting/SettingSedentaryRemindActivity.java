package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.os.Bundle;
import android.view.View;
import com.blankj.utilcode.util.SnackbarUtils;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.PickerManager;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;
import java.util.Locale;
import p112d.p113a.p114a.p116b.NumberPicker;
import p112d.p113a.p114a.p116b.TimePicker;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingSedentaryRemindActivity */
public class SettingSedentaryRemindActivity extends BaseActivity implements C3734y.C3735a {

    /* renamed from: b0 */
    private CommonListItemView f8270b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public CommonListItemView f8271c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public CommonListItemView f8272d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public CommonListItemView f8273e0;
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public PeriodTimeInfo f8274f0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingSedentaryRemindActivity$a */
    class C4297a extends NumberPicker.C3811a {
        C4297a() {
        }

        /* renamed from: b */
        public void mo23098b(int i, Number number) {
            Constant.m10311a("久坐提醒設置值");
            SettingSedentaryRemindActivity.this.f8274f0.mo22591e(number.intValue());
            CommonListItemView b = SettingSedentaryRemindActivity.this.f8271c0;
            b.setDetailText(number.intValue() + SettingSedentaryRemindActivity.this.getString(R$string.remind_time_unit));
            SettingSedentaryRemindActivity.this.m13867u();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingSedentaryRemindActivity$b */
    class C4298b extends NumberPicker.C3811a {

        /* renamed from: a */
        final /* synthetic */ boolean f8276a;

        C4298b(boolean z) {
            this.f8276a = z;
        }

        /* renamed from: b */
        public void mo23098b(int i, Number number) {
            int intValue = number.intValue();
            if (this.f8276a) {
                SettingSedentaryRemindActivity.this.f8274f0.mo22586c(intValue);
                SettingSedentaryRemindActivity.this.f8272d0.setDetailText(SettingSedentaryRemindActivity.this.m13862b(intValue, 0));
            } else if (intValue < SettingSedentaryRemindActivity.this.f8274f0.mo22585c()) {
                SnackbarUtils a = SnackbarUtils.m927a(SettingSedentaryRemindActivity.this.findViewById(R$id.rootView));
                a.mo9783a(SettingSedentaryRemindActivity.this.getString(R$string.err_time_setting));
                a.mo9782a(0);
                a.mo9787b();
                return;
            } else {
                SettingSedentaryRemindActivity.this.f8274f0.mo22581a(intValue);
                SettingSedentaryRemindActivity.this.f8273e0.setDetailText(SettingSedentaryRemindActivity.this.m13862b(intValue, 0));
            }
            SettingSedentaryRemindActivity.this.m13867u();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public void m13867u() {
        if (FSManager.m10323r().mo22847q()) {
            BandDataManager.m10550a(this.f8274f0);
            JYSDKManager.m10388i().mo22874a(this.f8274f0);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10622c(this.f8274f0);
        }
    }

    /* renamed from: v */
    private void m13868v() {
        NumberPicker a = PickerManager.m10513a(this, getString(R$string.remind_time_unit), 20, 120, 5);
        int e = (this.f8274f0.mo22590e() / 5) - 4;
        if (e < 0) {
            e = 0;
        }
        a.mo23095k(e);
        a.mo23094a((NumberPicker.C3811a) new C4297a());
        a.mo23129f();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_sedentary_remind;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.remind_sedentary);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22875a(this);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10602a(this);
        }
        this.f8274f0 = BandDataManager.m10555b("Sedentary");
        this.f8270b0 = (CommonListItemView) findViewById(R$id.livLock);
        this.f8271c0 = (CommonListItemView) findViewById(R$id.livTime);
        this.f8272d0 = (CommonListItemView) findViewById(R$id.livStart);
        this.f8273e0 = (CommonListItemView) findViewById(R$id.livEnd);
        this.f8270b0.getSwitch().setOnCheckedChangeListener(new C4330u(this));
        this.f8270b0.setOnClickListener(new C4334y(this));
        this.f8271c0.setOnClickListener(new C4333x(this));
        this.f8272d0.setOnClickListener(new C4332w(this));
        this.f8273e0.setOnClickListener(new C4331v(this));
    }

    /* renamed from: c */
    public /* synthetic */ void mo24493c(View view) {
        m13863b(true);
    }

    /* renamed from: d */
    public /* synthetic */ void mo24494d(View view) {
        m13863b(false);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24490a(SwitchButton switchButton, boolean z) {
        this.f8274f0.mo22582a(z);
        m13867u();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24489a(View view) {
        this.f8270b0.getSwitch().toggle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8270b0.getSwitch().setChecked(this.f8274f0.mo22593g());
        CommonListItemView commonListItemView = this.f8271c0;
        commonListItemView.setDetailText(this.f8274f0.mo22590e() + getString(R$string.remind_time_unit));
        this.f8272d0.setDetailText(m13862b(this.f8274f0.mo22585c(), this.f8274f0.mo22587d()));
        this.f8273e0.setDetailText(m13862b(this.f8274f0.mo22580a(), this.f8274f0.mo22583b()));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24491a(boolean z, String str, String str2) {
        try {
            int parseInt = Integer.parseInt(str);
            int parseInt2 = Integer.parseInt(str2);
            if (z) {
                this.f8274f0.mo22586c(parseInt);
                this.f8274f0.mo22588d(parseInt2);
                this.f8272d0.setDetailText(m13862b(parseInt, parseInt2));
            } else {
                if (parseInt >= this.f8274f0.mo22585c()) {
                    if (parseInt != this.f8274f0.mo22585c() || parseInt2 >= this.f8274f0.mo22587d()) {
                        this.f8274f0.mo22581a(parseInt);
                        this.f8274f0.mo22584b(parseInt2);
                        this.f8273e0.setDetailText(m13862b(parseInt, parseInt2));
                    }
                }
                SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
                a.mo9783a(getString(R$string.err_time_setting));
                a.mo9782a(0);
                a.mo9787b();
                return;
            }
            m13867u();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24492b(View view) {
        m13868v();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public String m13862b(int i, int i2) {
        return String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(i), Integer.valueOf(i2));
    }

    /* renamed from: b */
    private void m13863b(boolean z) {
        if (FSManager.m10323r().mo22847q()) {
            TimePicker a = PickerManager.m10514a(this, z ? this.f8274f0.mo22585c() : this.f8274f0.mo22580a(), z ? this.f8274f0.mo22587d() : this.f8274f0.mo22583b());
            a.mo23107a(new C4335z(this, z));
            a.mo23129f();
        } else if (FSManager.m10323r().mo22846p()) {
            NumberPicker a2 = PickerManager.m10513a(this, getString(R$string.data_unit_hour), 0, 23, 1);
            PeriodTimeInfo periodTimeInfo = this.f8274f0;
            a2.mo23095k(z ? periodTimeInfo.mo22585c() : periodTimeInfo.mo22580a());
            a2.mo23094a((NumberPicker.C3811a) new C4298b(z));
            a2.mo23129f();
        }
    }

    /* renamed from: a */
    public void mo22964a(C3734y.C3736b bVar, boolean z) {
        if (bVar == C3734y.C3736b.sedentary && mo22741l()) {
            new HvApiPostBandSettings(null).requestPostSedentary();
        }
    }
}
