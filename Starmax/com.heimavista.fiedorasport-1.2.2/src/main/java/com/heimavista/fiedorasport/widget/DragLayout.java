package com.heimavista.fiedorasport.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.lib.R$styleable;

public class DragLayout extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public int f8919P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public boolean f8920Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public ViewDragHelper f8921R;

    /* renamed from: S */
    private int f8922S;

    /* renamed from: T */
    private int f8923T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public View f8924U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public View f8925V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public int f8926W;
    /* access modifiers changed from: private */

    /* renamed from: a0 */
    public int f8927a0;
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public int f8928b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public int f8929c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public int f8930d0;

    /* renamed from: e0 */
    private int f8931e0;
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public int f8932f0;

    /* renamed from: com.heimavista.fiedorasport.widget.DragLayout$a */
    class C4490a extends ViewDragHelper.Callback {

        /* renamed from: a */
        private int f8933a;

        /* renamed from: b */
        int f8934b = 0;

        /* renamed from: c */
        int f8935c = 0;

        C4490a() {
        }

        public int clampViewPositionVertical(@NonNull View view, int i, int i2) {
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("clampViewPositionVertical child:" + view.getClass().getCanonicalName() + " top=" + i + " dy=" + i2);
            return i;
        }

        public int getViewHorizontalDragRange(@NonNull View view) {
            return super.getViewHorizontalDragRange(view);
        }

        public int getViewVerticalDragRange(@NonNull View view) {
            return DragLayout.this.getMeasuredHeight() - view.getMeasuredHeight();
        }

        public void onEdgeDragStarted(int i, int i2) {
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("onEdgeDragStarted edgeFlags=" + i);
            super.onEdgeDragStarted(i, i2);
            DragLayout.this.f8921R.captureChildView(DragLayout.this.f8924U, i2);
        }

        public void onEdgeTouched(int i, int i2) {
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("onEdgeTouched edgeFlags=" + i);
            super.onEdgeTouched(i, i2);
        }

        public void onViewDragStateChanged(int i) {
            this.f8933a = i;
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("onViewDragStateChanged state=" + i);
            super.onViewDragStateChanged(i);
            if (i == 0 || i == 2) {
                this.f8934b = 0;
            }
        }

        public void onViewPositionChanged(@NonNull View view, int i, int i2, int i3, int i4) {
            super.onViewPositionChanged(view, i, i2, i3, i4);
            if (this.f8933a == 1 && this.f8934b <= 0) {
                this.f8934b = i2;
            }
            this.f8935c = i2;
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("onViewPositionChanged top=" + i2 + " cur=" + this.f8935c + " pre=" + this.f8934b + " " + (this.f8935c - this.f8934b));
            DragLayout.this.f8925V.layout(0, DragLayout.this.f8924U.getBottom(), DragLayout.this.f8930d0, DragLayout.this.f8929c0);
            int unused = DragLayout.this.f8932f0 = i2;
        }

        public void onViewReleased(@NonNull View view, float f, float f2) {
            super.onViewReleased(view, f, f2);
            int i = this.f8935c - this.f8934b;
            int abs = Math.abs(i);
            int g = (DragLayout.this.f8929c0 - DragLayout.this.f8927a0) / 2;
            int g2 = (DragLayout.this.f8929c0 - DragLayout.this.f8926W) / 2;
            DragLayout dragLayout = DragLayout.this;
            dragLayout.m14658a("onViewReleased yvel=" + f2 + " dy=" + i + " open:" + Math.max(g, g2) + " top=" + DragLayout.this.f8919P);
            if (abs > 200) {
                if (i > 0) {
                    if (DragLayout.this.f8919P != DragLayout.this.f8926W) {
                        DragLayout dragLayout2 = DragLayout.this;
                        int unused = dragLayout2.f8919P = dragLayout2.f8929c0 - DragLayout.this.f8928b0;
                    } else if (abs > Math.max(g, g2)) {
                        DragLayout dragLayout3 = DragLayout.this;
                        int unused2 = dragLayout3.f8919P = dragLayout3.f8929c0 - DragLayout.this.f8928b0;
                    } else {
                        DragLayout dragLayout4 = DragLayout.this;
                        int unused3 = dragLayout4.f8919P = dragLayout4.f8929c0 - DragLayout.this.f8927a0;
                    }
                } else if (DragLayout.this.f8919P != DragLayout.this.f8929c0 - DragLayout.this.f8928b0) {
                    DragLayout dragLayout5 = DragLayout.this;
                    int unused4 = dragLayout5.f8919P = dragLayout5.f8926W;
                } else if (abs > Math.max(g, g2)) {
                    DragLayout dragLayout6 = DragLayout.this;
                    int unused5 = dragLayout6.f8919P = dragLayout6.f8926W;
                } else {
                    DragLayout dragLayout7 = DragLayout.this;
                    int unused6 = dragLayout7.f8919P = dragLayout7.f8929c0 - DragLayout.this.f8927a0;
                }
            } else if (Math.abs(f2) > 2000.0f) {
                if (f2 > 0.0f) {
                    if (DragLayout.this.f8919P == DragLayout.this.f8926W) {
                        DragLayout dragLayout8 = DragLayout.this;
                        int unused7 = dragLayout8.f8919P = dragLayout8.f8929c0 - DragLayout.this.f8927a0;
                    } else {
                        DragLayout dragLayout9 = DragLayout.this;
                        int unused8 = dragLayout9.f8919P = dragLayout9.f8929c0 - DragLayout.this.f8928b0;
                    }
                } else if (DragLayout.this.f8919P == DragLayout.this.f8929c0 - DragLayout.this.f8928b0) {
                    DragLayout dragLayout10 = DragLayout.this;
                    int unused9 = dragLayout10.f8919P = dragLayout10.f8929c0 - DragLayout.this.f8927a0;
                } else {
                    DragLayout dragLayout11 = DragLayout.this;
                    int unused10 = dragLayout11.f8919P = dragLayout11.f8926W;
                }
            }
            DragLayout.this.f8921R.settleCapturedViewAt(0, DragLayout.this.f8919P);
            DragLayout.this.invalidate();
        }

        public boolean tryCaptureView(@NonNull View view, int i) {
            return DragLayout.this.f8920Q && view == DragLayout.this.f8924U;
        }
    }

    public DragLayout(Context context) {
        this(context, null);
    }

    private ViewDragHelper.Callback getDragHelperCallback() {
        return new C4490a();
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.f8921R.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.f8924U = findViewById(this.f8922S);
        this.f8925V = findViewById(this.f8923T);
        this.f8919P = this.f8927a0;
        m14655a();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f8921R.shouldInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        View view = this.f8924U;
        int i5 = this.f8932f0;
        view.layout(0, i5, this.f8930d0, this.f8931e0 + i5);
        this.f8925V.layout(0, this.f8932f0 + this.f8931e0, this.f8930d0, this.f8929c0);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        StringBuilder sb = new StringBuilder();
        sb.append("onMeasure ");
        sb.append(i);
        sb.append(" ");
        sb.append(i2);
        sb.append(" ");
        sb.append(this.f8924U == null);
        m14658a(sb.toString());
        this.f8929c0 = View.MeasureSpec.getSize(i2);
        this.f8930d0 = View.MeasureSpec.getSize(i);
        View view = this.f8924U;
        this.f8931e0 = View.MeasureSpec.getSize(LinearLayout.getChildMeasureSpec(i2, 0, view == null ? 10 : view.getLayoutParams().height));
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.f8921R.processTouchEvent(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public void setCloseOffset(int i) {
        this.f8928b0 = i;
    }

    public void setDraggable(boolean z) {
        this.f8920Q = z;
    }

    public void setOpenOffset(int i) {
        this.f8927a0 = i;
    }

    public void setTopOffset(int i) {
        this.f8926W = i;
    }

    public DragLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DragLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f8919P = 0;
        this.f8920Q = true;
        this.f8926W = 0;
        this.f8927a0 = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        this.f8928b0 = 20;
        m14656a(context, attributeSet);
    }

    /* renamed from: a */
    private void m14656a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.DragLayout);
            this.f8922S = obtainStyledAttributes.getResourceId(R$styleable.DragLayout_dragHandleViewId, 0);
            this.f8923T = obtainStyledAttributes.getResourceId(R$styleable.DragLayout_dragContentViewId, 0);
            this.f8926W = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.DragLayout_dragTopOffset, this.f8926W);
            this.f8927a0 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.DragLayout_dragOpenOffset, this.f8927a0);
            this.f8928b0 = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.DragLayout_dratCloseOffset, this.f8928b0);
            this.f8920Q = obtainStyledAttributes.getBoolean(R$styleable.DragLayout_draggable, this.f8920Q);
            obtainStyledAttributes.recycle();
        }
        this.f8921R = ViewDragHelper.create(this, getDragHelperCallback());
        this.f8921R.setEdgeTrackingEnabled(8);
    }

    /* renamed from: a */
    private void m14655a() {
        if (this.f8921R.smoothSlideViewTo(this.f8924U, 0, this.f8919P)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14658a(String str) {
        LogUtils.m1131b("android dragLayout " + str);
    }
}
