package com.heimavista.fiedorasport.p111ui.activity.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserProfileActivity */
public class UserProfileActivity extends UserCommonActivity {
    /* access modifiers changed from: protected */
    /* renamed from: B */
    public String mo24603B() {
        return getString(R$string.member_profile);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R$id.recyclerView);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.setMargins(getResources().getDimensionPixelSize(R$dimen.list_item_padding_start), getResources().getDimensionPixelSize(R$dimen.list_item_padding_start), getResources().getDimensionPixelSize(R$dimen.list_item_padding_start), 0);
        recyclerView.setLayoutParams(layoutParams);
        mo24586a(recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return mo24603B();
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public List<MenuItem> mo23013u() {
        String str;
        String str2;
        mo24592e(FSManager.m10323r().mo22840j());
        mo24594f(FSManager.m10323r().mo22844n());
        mo24593e(FSManager.m10323r().mo22837g());
        mo24595f(MemberControl.m17125s().mo27178d());
        ArrayList arrayList = new ArrayList();
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(6);
        bVar.mo22664b(getString(R$string.member_avatar));
        bVar.mo22659a(MemberControl.m17125s().mo27183h());
        arrayList.add(bVar.mo22661a());
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(1);
        bVar2.mo22664b(getString(R$string.member_nickname));
        bVar2.mo22659a(MemberControl.m17125s().mo27182g());
        arrayList.add(bVar2.mo22661a());
        MenuItem.C3676b bVar3 = new MenuItem.C3676b();
        bVar3.mo22663b(0);
        bVar3.mo22664b("ID");
        bVar3.mo22659a(FSManager.m10323r().mo22843m());
        arrayList.add(bVar3.mo22661a());
        MenuItem.C3676b bVar4 = new MenuItem.C3676b();
        bVar4.mo22663b(1);
        bVar4.mo22664b(getString(R$string.member_name));
        bVar4.mo22659a(MemberControl.m17125s().mo27184i());
        arrayList.add(bVar4.mo22661a());
        String j = MemberControl.m17125s().mo27185j();
        if (!FSManager.m10323r().mo22845o() || !TextUtils.isEmpty(j)) {
            MenuItem.C3676b bVar5 = new MenuItem.C3676b();
            bVar5.mo22663b(1);
            bVar5.mo22664b(getString(R$string.member_phone));
            bVar5.mo22659a(j);
            arrayList.add(bVar5.mo22661a());
        }
        MenuItem.C3676b bVar6 = new MenuItem.C3676b();
        bVar6.mo22663b(1);
        bVar6.mo22664b(getString(R$string.member_address));
        bVar6.mo22662b();
        arrayList.add(bVar6.mo22661a());
        arrayList.add(new MenuItem.C3676b().mo22661a());
        MenuItem.C3676b bVar7 = new MenuItem.C3676b();
        bVar7.mo22663b(1);
        bVar7.mo22664b(getString(R$string.member_gender));
        bVar7.mo22659a(mo24597y().equals("M") ? "男" : mo24597y().equals("F") ? "女" : "");
        arrayList.add(bVar7.mo22661a());
        MenuItem.C3676b bVar8 = new MenuItem.C3676b();
        bVar8.mo22663b(1);
        bVar8.mo22664b(getString(R$string.member_birth));
        bVar8.mo22659a(mo24596x());
        arrayList.add(bVar8.mo22661a());
        MenuItem.C3676b bVar9 = new MenuItem.C3676b();
        bVar9.mo22663b(1);
        bVar9.mo22664b(getString(R$string.member_height));
        if (mo23014v() == 0) {
            str = getString(R$string.select_first);
        } else {
            str = mo23014v() + "cm";
        }
        bVar9.mo22659a(str);
        arrayList.add(bVar9.mo22661a());
        MenuItem.C3676b bVar10 = new MenuItem.C3676b();
        bVar10.mo22663b(1);
        bVar10.mo22664b(getString(R$string.member_weight));
        if (mo24598z() == 0) {
            str2 = getString(R$string.select_first);
        } else {
            str2 = mo24598z() + "KG";
        }
        bVar10.mo22659a(str2);
        bVar10.mo22662b();
        arrayList.add(bVar10.mo22661a());
        if (!FSManager.m10323r().mo22845o()) {
            arrayList.add(new MenuItem.C3676b().mo22661a());
            MenuItem.C3676b bVar11 = new MenuItem.C3676b();
            bVar11.mo22663b(1);
            bVar11.mo22664b(getString(R$string.bind_social));
            bVar11.mo22662b();
            arrayList.add(bVar11.mo22661a());
        }
        arrayList.add(new MenuItem.C3676b().mo22661a());
        MenuItem.C3676b bVar12 = new MenuItem.C3676b();
        bVar12.mo22663b(5);
        bVar12.mo22664b(getString(R$string.exit));
        bVar12.mo22662b();
        arrayList.add(bVar12.mo22661a());
        arrayList.add(new MenuItem.C3676b().mo22661a());
        return arrayList;
    }
}
