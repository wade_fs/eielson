package com.heimavista.fiedorasport.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.heimavista.fiedorasport.lib.R$styleable;

public class DashView extends View {

    /* renamed from: P */
    private float f8911P;

    /* renamed from: Q */
    private float f8912Q;

    /* renamed from: R */
    private float f8913R;

    /* renamed from: S */
    private int f8914S;

    /* renamed from: T */
    private int f8915T;

    /* renamed from: U */
    private Paint f8916U;

    /* renamed from: V */
    private int f8917V;

    /* renamed from: W */
    private int f8918W;

    public DashView(Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public void mo24754a(Canvas canvas) {
        canvas.save();
        float[] fArr = {0.0f, 0.0f, this.f8913R, 0.0f};
        canvas.translate(0.0f, this.f8912Q / 2.0f);
        float f = 0.0f;
        while (f <= ((float) this.f8917V)) {
            canvas.drawLines(fArr, this.f8916U);
            canvas.translate(this.f8913R + this.f8911P, 0.0f);
            f += this.f8913R + this.f8911P;
        }
        canvas.restore();
    }

    /* renamed from: b */
    public void mo24755b(Canvas canvas) {
        canvas.save();
        float[] fArr = {0.0f, 0.0f, 0.0f, this.f8913R};
        canvas.translate(this.f8912Q / 2.0f, 0.0f);
        float f = 0.0f;
        while (f <= ((float) this.f8918W)) {
            canvas.drawLines(fArr, this.f8916U);
            canvas.translate(0.0f, this.f8913R + this.f8911P);
            f += this.f8913R + this.f8911P;
        }
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f8915T != 1) {
            mo24754a(canvas);
        } else {
            mo24755b(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.f8917V = (View.MeasureSpec.getSize(i) - getPaddingLeft()) - getPaddingRight();
        this.f8918W = View.MeasureSpec.getSize((i2 - getPaddingTop()) - getPaddingBottom());
        if (this.f8915T == 0) {
            setMeasuredDimension(this.f8917V, (int) this.f8912Q);
        } else {
            setMeasuredDimension((int) this.f8912Q, this.f8918W);
        }
    }

    public DashView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f8916U = new Paint(1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.DashView);
        this.f8911P = obtainStyledAttributes.getDimension(R$styleable.DashView_dv_dashGap, 100.0f);
        this.f8912Q = obtainStyledAttributes.getDimension(R$styleable.DashView_dv_dashHeight, 10.0f);
        this.f8913R = obtainStyledAttributes.getDimension(R$styleable.DashView_dv_dashWidth, 100.0f);
        this.f8914S = obtainStyledAttributes.getColor(R$styleable.DashView_dv_color, 10395294);
        this.f8915T = obtainStyledAttributes.getInteger(R$styleable.DashView_dv_orientation, 0);
        this.f8916U.setColor(this.f8914S);
        this.f8916U.setStrokeWidth(this.f8912Q);
        obtainStyledAttributes.recycle();
    }
}
