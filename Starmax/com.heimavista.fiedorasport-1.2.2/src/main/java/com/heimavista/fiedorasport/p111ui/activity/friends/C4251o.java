package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.content.Intent;
import com.heimavista.fiedorasport.base.BaseActivity;
import p119e.p189e.p193d.p195j.BuddyShareDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.o */
/* compiled from: lambda */
public final /* synthetic */ class C4251o implements BaseActivity.C3680b {

    /* renamed from: a */
    private final /* synthetic */ ListBuddyShareActivity f8127a;

    /* renamed from: b */
    private final /* synthetic */ int f8128b;

    /* renamed from: c */
    private final /* synthetic */ BuddyShareDb f8129c;

    public /* synthetic */ C4251o(ListBuddyShareActivity listBuddyShareActivity, int i, BuddyShareDb bVar) {
        this.f8127a = listBuddyShareActivity;
        this.f8128b = i;
        this.f8129c = bVar;
    }

    public final void onActivityResult(int i, Intent intent) {
        this.f8127a.mo24392a(this.f8128b, this.f8129c, i, intent);
    }
}
