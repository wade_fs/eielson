package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiAcceptBuddy;
import com.heimavista.api.buddy.HvApiDelBuddy;
import com.heimavista.api.buddy.HvApiGetBuddyList;
import com.heimavista.api.buddy.HvApiSetRemark;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.InputDialog;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.FriendDataActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.ListAddFriendsActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.ListBuddyShareActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingShareOptionsActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListBuddyDynamicAdapter;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment */
public class BuddyDynamicFragment extends BaseFragment<HomeActivity> {
    /* access modifiers changed from: private */

    /* renamed from: W */
    public SmartRefreshLayout f8698W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public ListBuddyDynamicAdapter f8699X;

    /* renamed from: Y */
    private List<FriendBean> f8700Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public int f8701Z;

    /* renamed from: a0 */
    private HttpCancelable f8702a0;

    /* renamed from: b0 */
    private HttpCancelable f8703b0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment$b */
    class C4430b implements OnResultListener<ParamJsonData> {
        C4430b() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            if (BuddyDynamicFragment.this.f8698W != null) {
                BuddyDynamicFragment.this.f8698W.mo26062d();
            }
            if (BuddyDynamicFragment.this.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
                BuddyDynamicFragment.this.m14391k();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            if (BuddyDynamicFragment.this.f8698W != null) {
                BuddyDynamicFragment.this.f8698W.mo26062d();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment$c */
    class C4431c implements C4632i<String> {

        /* renamed from: a */
        final /* synthetic */ FriendBean f8709a;

        /* renamed from: b */
        final /* synthetic */ View f8710b;

        /* renamed from: c */
        final /* synthetic */ int f8711c;

        C4431c(FriendBean dVar, View view, int i) {
            this.f8709a = dVar;
            this.f8710b = view;
            this.f8711c = i;
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, int, com.heimavista.fiedorasport.ui.fragment.friend.e]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment.a(android.view.View, int, com.heimavista.entity.data.d):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, android.os.Bundle, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            String str2;
            if (i == 0) {
                Intent intent = new Intent(BuddyDynamicFragment.this.getContext(), SettingShareOptionsActivity.class);
                UserInfoDb p = UserInfoDb.m13344p(this.f8709a.mo22640d());
                if (p == null) {
                    str2 = "";
                } else {
                    str2 = p.mo24338x();
                }
                intent.putExtra("userName", str2);
                intent.putExtra("userNbr", this.f8709a.mo22640d());
                BuddyDynamicFragment.this.startActivity(intent);
            } else if (i == 1) {
                BuddyDynamicFragment.this.m14383a(this.f8710b, this.f8711c, this.f8709a);
            } else {
                BuddyDynamicFragment buddyDynamicFragment = BuddyDynamicFragment.this;
                buddyDynamicFragment.mo22764a(buddyDynamicFragment.getString(R$string.del_friend_tips), false, (BaseDialog.C4621m) new C4446e(this, this.f8709a));
            }
        }

        /* renamed from: a */
        public /* synthetic */ void mo24685a(FriendBean dVar, boolean z) {
            if (z) {
                Constant.m10311a("刪除好友");
                BuddyDynamicFragment.this.mo22776h();
                new HvApiDelBuddy().request(dVar.mo22640d(), new C4465x(this, dVar));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m14391k() {
        List<FriendBean> list = this.f8700Y;
        if (list == null) {
            this.f8700Y = new ArrayList();
        } else {
            list.clear();
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List<BuddyListDb> G = BuddyListDb.m13246G();
        this.f8699X.mo22782a();
        for (BuddyListDb aVar : G) {
            String x = aVar.mo24311x();
            long w = aVar.mo24310w();
            LogUtils.m1139c("BuddyListDb userNbr=" + x + " userStat=" + aVar.mo24312y() + " time=" + TimeUtils.m1009b(1000 * w));
            int y = aVar.mo24312y();
            if (y == 1) {
                FriendBean dVar = new FriendBean();
                dVar.mo22634a(aVar);
                dVar.mo22635a(x);
                dVar.mo22633a(w);
                dVar.mo22632a(1);
                arrayList.add(dVar);
            } else if (y == 2) {
                FriendBean dVar2 = new FriendBean();
                dVar2.mo22634a(aVar);
                dVar2.mo22635a(x);
                dVar2.mo22633a(w);
                dVar2.mo22632a(2);
                arrayList.add(dVar2);
            } else if (y == 9) {
                if (aVar.mo24305r() == 1 || aVar.mo24305r() == 2) {
                    FriendBean dVar3 = new FriendBean();
                    dVar3.mo22634a(aVar);
                    dVar3.mo22635a(x);
                    dVar3.mo22633a(w);
                    dVar3.mo22632a(9);
                    arrayList.add(dVar3);
                }
                FriendBean dVar4 = new FriendBean();
                dVar4.mo22635a(x);
                dVar4.mo22634a(aVar);
                dVar4.mo22632a(3);
                dVar4.mo22633a(aVar.mo24304q());
                arrayList2.add(dVar4);
            }
        }
        if (!arrayList.isEmpty() || !arrayList2.isEmpty()) {
            ListBuddyDynamicAdapter listBuddyDynamicAdapter = this.f8699X;
            FriendBean dVar5 = new FriendBean();
            dVar5.mo22632a(4);
            listBuddyDynamicAdapter.mo22785a(dVar5);
        } else if (this.f8699X.getItemCount() == 0) {
            ListBuddyDynamicAdapter listBuddyDynamicAdapter2 = this.f8699X;
            FriendBean dVar6 = new FriendBean();
            dVar6.mo22632a(0);
            listBuddyDynamicAdapter2.mo22785a(dVar6);
        } else {
            ListBuddyDynamicAdapter listBuddyDynamicAdapter3 = this.f8699X;
            FriendBean dVar7 = new FriendBean();
            dVar7.mo22632a(0);
            listBuddyDynamicAdapter3.mo22790b(0, dVar7);
        }
        Collections.sort(arrayList);
        Collections.sort(arrayList2);
        this.f8700Y.addAll(arrayList);
        this.f8700Y.addAll(arrayList2);
        if (!this.f8700Y.isEmpty()) {
            this.f8699X.mo22786a((List) this.f8700Y);
        }
    }

    /* renamed from: l */
    public static BuddyDynamicFragment m14392l() {
        Bundle bundle = new Bundle();
        BuddyDynamicFragment buddyDynamicFragment = new BuddyDynamicFragment();
        buddyDynamicFragment.setArguments(bundle);
        return buddyDynamicFragment;
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public void m14393m() {
        Constant.m10311a("添加好友點擊");
        mo22769b(ListAddFriendsActivity.class);
    }

    /* renamed from: n */
    private void m14394n() {
        if (!isVisible()) {
            mo24681j();
        } else {
            this.f8702a0 = new HvApiGetBuddyList().request(new C4430b());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        m14391k();
    }

    /* renamed from: i */
    public void mo24680i() {
        SmartRefreshLayout smartRefreshLayout = this.f8698W;
        if (smartRefreshLayout != null) {
            smartRefreshLayout.mo26056b();
        }
    }

    /* renamed from: j */
    public void mo24681j() {
        SmartRefreshLayout smartRefreshLayout = this.f8698W;
        if (smartRefreshLayout != null) {
            smartRefreshLayout.mo26062d();
        }
        HttpCancelable bVar = this.f8702a0;
        if (bVar != null) {
            bVar.cancel();
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.addFriend) {
            m14393m();
        }
    }

    public void onPause() {
        mo24681j();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        mo24680i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_buddy_dynamic;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14385b(View view, int i, FriendBean dVar) {
        String[] strArr = {getString(R$string.setting_share_permission), getString(R$string.friend_label), getString(R$string.del_friend)};
        C4629g gVar = new C4629g(mo22771c());
        gVar.mo25456a(strArr);
        gVar.mo25417b(true);
        C4629g gVar2 = gVar;
        gVar2.mo25453a(new C4431c(dVar, view, i));
        gVar2.mo25418b().show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        this.f8698W = (SmartRefreshLayout) view.findViewById(R$id.refreshLayout);
        this.f8698W.mo26048a(new C4448g(this));
        this.f8699X = new ListBuddyDynamicAdapter(getContext());
        this.f8699X.mo24617a((ListBuddyDynamicAdapter.C4401b) new C4428a());
        ((RecyclerView) view.findViewById(R$id.recyclerView)).setAdapter(this.f8699X);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment$a */
    class C4428a implements ListBuddyDynamicAdapter.C4401b {

        /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyDynamicFragment$a$a */
        class C4429a implements OnResultListener<ParamJsonData> {

            /* renamed from: a */
            final /* synthetic */ FriendBean f8705a;

            /* renamed from: b */
            final /* synthetic */ boolean f8706b;

            C4429a(FriendBean dVar, boolean z) {
                this.f8705a = dVar;
                this.f8706b = z;
            }

            /* renamed from: a */
            public void mo22380a(ParamJsonData fVar) {
                String str;
                BuddyDynamicFragment.this.mo22768b();
                BuddyListDb.m13249k(this.f8705a.mo22640d());
                BuddyDynamicFragment.this.f8699X.mo22791b((FriendBean) BuddyDynamicFragment.this.f8699X.getItem(BuddyDynamicFragment.this.f8701Z));
                if (this.f8706b) {
                    Intent intent = new Intent(BuddyDynamicFragment.this.getContext(), SettingShareOptionsActivity.class);
                    UserInfoDb p = UserInfoDb.m13344p(this.f8705a.mo22640d());
                    if (p == null) {
                        str = "";
                    } else {
                        str = p.mo24338x();
                    }
                    intent.putExtra("userName", str);
                    intent.putExtra("userNbr", this.f8705a.mo22640d());
                    BuddyDynamicFragment.this.startActivity(intent);
                } else if (BuddyListDb.m13246G().isEmpty()) {
                    ListBuddyDynamicAdapter a = BuddyDynamicFragment.this.f8699X;
                    FriendBean dVar = new FriendBean();
                    dVar.mo22632a(0);
                    a.mo22790b(0, dVar);
                }
            }

            /* renamed from: a */
            public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
                BuddyDynamicFragment.this.mo22768b();
            }
        }

        C4428a() {
        }

        /* renamed from: a */
        public void mo24626a(int i, FriendBean dVar) {
            String str;
            if (dVar.mo22636a() == null || dVar.mo22636a().mo24305r() >= 9) {
                Constant.m10311a("分享設定點擊");
                BuddyDynamicFragment.this.mo22769b(ListBuddyShareActivity.class);
                return;
            }
            BuddyDynamicFragment.this.f8699X.mo22789b(i);
            Intent intent = new Intent(BuddyDynamicFragment.this.getContext(), SettingShareOptionsActivity.class);
            UserInfoDb p = UserInfoDb.m13344p(dVar.mo22640d());
            if (p == null) {
                str = "";
            } else {
                str = p.mo24338x();
            }
            intent.putExtra("userName", str);
            intent.putExtra("userNbr", dVar.mo22640d());
            BuddyDynamicFragment.this.startActivity(intent);
        }

        /* renamed from: b */
        public void mo24629b(int i, FriendBean dVar) {
            Constant.m10311a("朋友標籤點擊");
            Intent intent = new Intent(BuddyDynamicFragment.this.getContext(), FriendDataActivity.class);
            intent.putExtra("userNbr", dVar.mo22640d());
            BuddyDynamicFragment.this.mo22756a(intent, new C4445d(this, i, dVar));
        }

        /* renamed from: c */
        public void mo24630c(int i, FriendBean dVar) {
            BuddyDynamicFragment.this.m14393m();
        }

        /* renamed from: a */
        public void mo24627a(boolean z, int i, FriendBean dVar) {
            BuddyDynamicFragment.this.mo22766a(false);
            int unused = BuddyDynamicFragment.this.f8701Z = i;
            Constant.m10311a(z ? "接受好友請求" : "拒絕好友請求");
            new HvApiAcceptBuddy().request(dVar.mo22640d(), z, new C4429a(dVar, z));
        }

        /* renamed from: a */
        public /* synthetic */ void mo24682a(int i, FriendBean dVar, int i2, Intent intent) {
            if (i2 == -1 && intent != null) {
                if (intent.getBooleanExtra("actionDelete", false)) {
                    BuddyDynamicFragment.this.f8699X.mo22789b(i);
                } else if (intent.hasExtra("remark")) {
                    String stringExtra = intent.getStringExtra("remark");
                    if (!TextUtils.isEmpty(stringExtra) && dVar.mo22636a() != null) {
                        dVar.mo22636a().mo24301i(stringExtra);
                        BuddyDynamicFragment.this.f8699X.notifyItemChanged(i, dVar);
                    }
                }
            }
        }

        /* renamed from: a */
        public boolean mo24628a(View view, int i, FriendBean dVar) {
            BuddyDynamicFragment.this.m14385b(view, i, dVar);
            return false;
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24679a(RefreshLayout jVar) {
        m14394n();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14383a(View view, int i, FriendBean dVar) {
        InputDialog gVar = new InputDialog(mo22771c());
        gVar.mo22854a(1);
        gVar.mo22861b(getString(R$string.friend_label));
        gVar.mo22857a(getString(R$string.input_label));
        gVar.mo22856a(new C4449h(this, dVar, i));
        gVar.mo22855a(new C4447f(this, view));
        gVar.mo22858a();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24678a(FriendBean dVar, int i, View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        if (!z) {
            HttpCancelable bVar = this.f8703b0;
            if (bVar != null) {
                bVar.cancel();
                this.f8703b0 = null;
            }
            alertDialog.cancel();
        } else if (!TextUtils.isEmpty(charSequence)) {
            Constant.m10311a("備註好友");
            view.setEnabled(false);
            Constant.m10311a("備註好友");
            mo22757a(view);
            this.f8703b0 = new HvApiSetRemark().request(dVar.mo22640d(), charSequence.toString(), new C4466y(this, alertDialog, dVar, charSequence, i));
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24677a(View view, DialogInterface dialogInterface) {
        mo22757a(view);
    }
}
