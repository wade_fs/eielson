package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.heimavista.api.HvApiEncryptUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiReqBuddyByNbr;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.InputDialog;
import com.heimavista.fiedorasport.p111ui.adapter.ListAddFriendsAdapter;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import org.json.JSONObject;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListAddFriendsActivity */
public class ListAddFriendsActivity extends BaseActivity {

    /* renamed from: b0 */
    private ListAddFriendsAdapter f8086b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public RecyclerView f8087c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public String f8088d0 = "";

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListAddFriendsActivity$a */
    class C4229a implements PermissionUtils.C0922e {
        C4229a() {
        }

        /* renamed from: a */
        public void mo9866a() {
            ListAddFriendsActivity.this.mo22704a(MyCaptureActivity.class, new C4244h(this));
        }

        /* renamed from: b */
        public void mo9867b() {
        }

        /* renamed from: a */
        public /* synthetic */ void mo24390a(int i, Intent intent) {
            if (intent != null) {
                String stringExtra = intent.getStringExtra("content");
                LogUtils.m1139c("qrContent:" + stringExtra);
                try {
                    String a = HvApiEncryptUtils.m10000a(stringExtra, "fiedoraSport");
                    LogUtils.m1139c("qrContent decryptDES:" + a);
                    JSONObject jSONObject = new JSONObject(a);
                    if (!jSONObject.has("userNbr") || !jSONObject.has("userName")) {
                        SnackbarUtils a2 = SnackbarUtils.m927a(ListAddFriendsActivity.this.f8087c0);
                        a2.mo9783a(ListAddFriendsActivity.this.getString(R$string.err_qr_code));
                        a2.mo9782a(0);
                        a2.mo9787b();
                        return;
                    }
                    String unused = ListAddFriendsActivity.this.f8088d0 = jSONObject.getString("userName");
                    ListAddFriendsActivity.this.m13522e(jSONObject.getString("userNbr"));
                } catch (Exception unused2) {
                    SnackbarUtils a3 = SnackbarUtils.m927a(ListAddFriendsActivity.this.f8087c0);
                    a3.mo9783a(ListAddFriendsActivity.this.getString(R$string.err_qr_code));
                    a3.mo9782a(0);
                    a3.mo9787b();
                }
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListAddFriendsActivity$b */
    class C4230b implements OnResultListener<ParamJsonData> {
        C4230b() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            ListAddFriendsActivity.this.mo22717b();
            ListAddFriendsActivity listAddFriendsActivity = ListAddFriendsActivity.this;
            listAddFriendsActivity.mo22730c(listAddFriendsActivity.getString(R$string.invite_tips));
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            ListAddFriendsActivity.this.mo22717b();
            ListAddFriendsActivity.this.mo22730c(str);
        }
    }

    /* renamed from: d */
    private void m13521d(String str) {
        if (mo22741l()) {
            mo22751t();
            new HvApiReqBuddyByNbr().request(str, new C4230b());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, com.heimavista.fiedorasport.ui.activity.friends.n]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.friends.i]
     candidates:
      com.heimavista.fiedorasport.ui.activity.friends.ListAddFriendsActivity.a(android.view.View, android.app.AlertDialog, boolean, java.lang.CharSequence):void
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m13522e(String str) {
        BuddyListDb l = BuddyListDb.m13250l(str);
        if (l != null) {
            if (l.mo24312y() == 1) {
                mo22707a("", getString(R$string.invite_tips), getString(R$string.f6600ok), true, false, (BaseDialog.C4621m) new C4250n(this));
                return;
            } else if (l.mo24312y() == 9) {
                mo22730c(getString(R$string.has_build));
                return;
            }
        }
        mo22708a(getString(R$string.invite_request_tips, new Object[]{this.f8088d0}), getString(R$string.invite), false, (BaseDialog.C4621m) new C4245i(this, str));
    }

    /* renamed from: u */
    private void m13523u() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.CAMERA");
        b.mo9858a(new C4229a());
        b.mo9859a();
    }

    /* renamed from: v */
    private void m13524v() {
        InputDialog gVar = new InputDialog(this);
        gVar.mo22854a(1);
        gVar.mo22861b(getString(R$string.activity_name));
        gVar.mo22857a(getString(R$string.activity_hint));
        gVar.mo22856a(new C4249m(this));
        gVar.mo22855a(new C4247k(this));
        gVar.mo22858a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22731d() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common_with_divider;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.add_friends);
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    /* renamed from: k */
    public int mo22740k() {
        return 17760262;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 16908331, 0, "").setIcon(R$mipmap.icon_close).setShowAsAction(1);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() != 16908331) {
            return false;
        }
        onBackPressed();
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8087c0 = (RecyclerView) findViewById(R$id.recyclerView);
        this.f8086b0 = new ListAddFriendsAdapter(this);
        this.f8086b0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4248l(this));
        this.f8087c0.setAdapter(this.f8086b0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24386a(View view, ListAddFriendsAdapter.C4398a aVar, int i) {
        if (aVar.f8579a != 1) {
            return;
        }
        if (getString(R$string.add_friends_by_share).equals(aVar.f8581c)) {
            Constant.m10311a("分享添加好友點擊");
            mo22730c(aVar.f8581c);
        } else if (getString(R$string.add_friends_by_userid).equals(aVar.f8581c)) {
            Constant.m10311a("ID搜尋點擊");
            mo22702a(SearchBuddyActivity.class);
        } else if (getString(R$string.add_friends_by_scan).equals(aVar.f8581c)) {
            Constant.m10311a("掃一掃點擊");
            m13523u();
        } else if (getString(R$string.add_friends_by_qr).equals(aVar.f8581c)) {
            Constant.m10311a("我的二維碼點擊");
            mo22702a(QrCodeActivity.class);
        } else if (getString(R$string.create_new_activities).equals(aVar.f8581c)) {
            m13524v();
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24388b(boolean z) {
        if (z) {
            finish();
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24387b(String str, boolean z) {
        if (z) {
            Constant.m10311a("掃一掃添加好友");
            m13521d(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        ListAddFriendsAdapter listAddFriendsAdapter = this.f8086b0;
        ListAddFriendsAdapter.C4398a.C4399a aVar = new ListAddFriendsAdapter.C4398a.C4399a();
        aVar.mo24614b(1);
        aVar.mo24611a(R$mipmap.ic_friends_mobile);
        aVar.mo24612a(getString(R$string.add_friends_by_userid));
        listAddFriendsAdapter.mo22785a(aVar.mo24613a());
        ListAddFriendsAdapter listAddFriendsAdapter2 = this.f8086b0;
        ListAddFriendsAdapter.C4398a.C4399a aVar2 = new ListAddFriendsAdapter.C4398a.C4399a();
        aVar2.mo24614b(1);
        aVar2.mo24611a(R$mipmap.ic_friends_scan2);
        aVar2.mo24612a(getString(R$string.add_friends_by_scan));
        listAddFriendsAdapter2.mo22785a(aVar2.mo24613a());
        ListAddFriendsAdapter listAddFriendsAdapter3 = this.f8086b0;
        ListAddFriendsAdapter.C4398a.C4399a aVar3 = new ListAddFriendsAdapter.C4398a.C4399a();
        aVar3.mo24614b(1);
        aVar3.mo24611a(R$mipmap.ic_friends_qrcode);
        aVar3.mo24612a(getString(R$string.add_friends_by_qr));
        listAddFriendsAdapter3.mo22785a(aVar3.mo24613a());
    }

    /* renamed from: a */
    public /* synthetic */ void mo24385a(View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        if (!z || TextUtils.isEmpty(charSequence)) {
            alertDialog.cancel();
        } else {
            mo22719b(new Intent(this, SelectFriendsActivity.class), C4246j.f8122a);
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m13519a(int i, Intent intent) {
        if (intent != null && intent.hasExtra("userNbrList")) {
            intent.getStringArrayListExtra("userNbrList");
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24384a(DialogInterface dialogInterface) {
        hideSoftKeyboard(this.f8087c0);
    }
}
