package com.heimavista.fiedorasport.p199j;

import android.content.Context;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.fiedorasport.lib.R$string;
import java.util.Calendar;
import java.util.TimeZone;

/* renamed from: com.heimavista.fiedorasport.j.k */
public class TimeUtil {
    /* renamed from: a */
    public static Calendar m13445a(int i) {
        Calendar d = m13448d(i);
        d.add(6, 1);
        d.add(14, -1);
        return d;
    }

    /* renamed from: b */
    public static Calendar m13446b(int i) {
        Calendar e = m13449e(i);
        e.add(2, 1);
        e.add(14, -1);
        return e;
    }

    /* renamed from: c */
    public static Calendar m13447c(int i) {
        Calendar f = m13450f(i);
        f.add(3, 1);
        f.add(14, -1);
        return f;
    }

    /* renamed from: d */
    public static Calendar m13448d(int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        instance.add(6, i);
        return instance;
    }

    /* renamed from: e */
    public static Calendar m13449e(int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        instance.set(5, 1);
        instance.add(2, i);
        return instance;
    }

    /* renamed from: f */
    public static Calendar m13450f(int i) {
        Calendar instance = Calendar.getInstance();
        instance.setFirstDayOfWeek(2);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        instance.set(7, instance.getFirstDayOfWeek());
        instance.add(3, i);
        return instance;
    }

    /* renamed from: a */
    public static String m13442a() {
        String str;
        int rawOffset = TimeZone.getDefault().getRawOffset();
        if (rawOffset < 0) {
            rawOffset = -rawOffset;
            str = "-";
        } else {
            str = "+";
        }
        return str + (((rawOffset / 1000) / 60) / 60);
    }

    /* renamed from: a */
    public static String m13443a(Context context, long j) {
        return m13444a(context, j, "yyyy-MM-dd");
    }

    /* renamed from: a */
    public static String m13444a(Context context, long j, String str) {
        long currentTimeMillis = (System.currentTimeMillis() - j) / 1000;
        if (j < 0) {
            return "";
        }
        if (currentTimeMillis < 60) {
            return context.getString(R$string.just_now);
        }
        if (currentTimeMillis < 3600) {
            return (currentTimeMillis / 60) + context.getString(R$string.before_minutes);
        } else if (currentTimeMillis < 86400) {
            return (currentTimeMillis / 3600) + context.getString(R$string.before_hours);
        } else if (currentTimeMillis >= 864000) {
            return TimeUtils.m998a(j, str);
        } else {
            return (currentTimeMillis / 86400) + context.getString(R$string.before_days);
        }
    }
}
