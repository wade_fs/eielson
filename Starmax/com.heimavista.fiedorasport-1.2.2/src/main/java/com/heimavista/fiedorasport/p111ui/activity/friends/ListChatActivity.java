package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListChatActivity */
public class ListChatActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        mo22701a((CharSequence) getIntent().getStringExtra("title"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22731d() {
        return R$mipmap.icon_close;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_chat;
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    /* renamed from: k */
    public int mo22740k() {
        return 17760262;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 16908331, 0, "").setIcon(R$mipmap.icon_add).setShowAsAction(1);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() == 16908331) {
            Constant.m10311a("添加好友點擊");
            mo22729c(ListAddFriendsActivity.class);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
