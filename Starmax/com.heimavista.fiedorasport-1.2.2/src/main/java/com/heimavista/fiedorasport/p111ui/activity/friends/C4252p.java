package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.View;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import p119e.p189e.p193d.p195j.BuddyShareDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.p */
/* compiled from: lambda */
public final /* synthetic */ class C4252p implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyShareActivity f8130P;

    public /* synthetic */ C4252p(ListBuddyShareActivity listBuddyShareActivity) {
        this.f8130P = listBuddyShareActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8130P.mo24393a(view, (BuddyShareDb) obj, i);
    }
}
