package com.heimavista.fiedorasport.p111ui.activity.sleep;

import android.widget.RadioGroup;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.a */
/* compiled from: lambda */
public final /* synthetic */ class C4343a implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a */
    private final /* synthetic */ SleepDataChartActivity f8386a;

    public /* synthetic */ C4343a(SleepDataChartActivity sleepDataChartActivity) {
        this.f8386a = sleepDataChartActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.f8386a.mo24523a(radioGroup, i);
    }
}
