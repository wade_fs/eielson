package com.heimavista.fiedorasport.widget.p201a;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.widget.a.b */
public class GroupAvatar implements Serializable {

    /* renamed from: P */
    private Bitmap f8988P;

    public GroupAvatar() {
    }

    /* renamed from: a */
    public Bitmap mo24828a() {
        return this.f8988P;
    }

    public GroupAvatar(Bitmap bitmap) {
        this.f8988P = bitmap;
    }

    public GroupAvatar(Bitmap bitmap, List<String> list) {
        this.f8988P = bitmap;
    }
}
