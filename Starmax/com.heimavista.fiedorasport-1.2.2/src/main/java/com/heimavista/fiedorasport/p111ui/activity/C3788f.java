package com.heimavista.fiedorasport.p111ui.activity;

import com.heimavista.api.HvApiResponse;
import com.heimavista.fiedorasport.p111ui.activity.MainActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.f */
/* compiled from: lambda */
public final /* synthetic */ class C3788f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MainActivity.C3760c f6730P;

    /* renamed from: Q */
    private final /* synthetic */ boolean f6731Q;

    /* renamed from: R */
    private final /* synthetic */ HvApiResponse[] f6732R;

    public /* synthetic */ C3788f(MainActivity.C3760c cVar, boolean z, HvApiResponse[] cVarArr) {
        this.f6730P = cVar;
        this.f6731Q = z;
        this.f6732R = cVarArr;
    }

    public final void run() {
        this.f6730P.mo23028a(this.f6731Q, this.f6732R);
    }
}
