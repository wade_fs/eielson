package com.heimavista.fiedorasport.p199j;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p031c.BitmapTransformation;
import com.bumptech.glide.load.p030p.p031c.TransformationUtils;
import java.security.MessageDigest;

/* renamed from: com.heimavista.fiedorasport.j.g */
public class GlideRoundImage extends BitmapTransformation {

    /* renamed from: b */
    private static float f8041b;

    public GlideRoundImage(Context context, int i) {
        f8041b = Resources.getSystem().getDisplayMetrics().density * ((float) i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap mo10342a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        return m13421a(eVar, TransformationUtils.m2212a(eVar, bitmap, i, i2));
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
    }

    /* renamed from: a */
    private static Bitmap m13421a(BitmapPool eVar, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap a = eVar.mo10060a(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(a);
        Paint paint = new Paint();
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
        paint.setAntiAlias(true);
        RectF rectF = new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        float f = f8041b;
        canvas.drawRoundRect(rectF, f, f, paint);
        return a;
    }
}
