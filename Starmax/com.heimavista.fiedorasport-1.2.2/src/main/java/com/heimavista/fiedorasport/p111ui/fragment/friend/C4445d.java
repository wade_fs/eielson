package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.content.Intent;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddyDynamicFragment;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.d */
/* compiled from: lambda */
public final /* synthetic */ class C4445d implements BaseActivity.C3680b {

    /* renamed from: a */
    private final /* synthetic */ BuddyDynamicFragment.C4428a f8762a;

    /* renamed from: b */
    private final /* synthetic */ int f8763b;

    /* renamed from: c */
    private final /* synthetic */ FriendBean f8764c;

    public /* synthetic */ C4445d(BuddyDynamicFragment.C4428a aVar, int i, FriendBean dVar) {
        this.f8762a = aVar;
        this.f8763b = i;
        this.f8764c = dVar;
    }

    public final void onActivityResult(int i, Intent intent) {
        this.f8762a.mo24682a(this.f8763b, this.f8764c, i, intent);
    }
}
