package com.heimavista.fiedorasport;

/* renamed from: com.heimavista.fiedorasport.R$raw */
public final class C5234R$raw {
    public static final int amu_ballon_gx_prefix = 2131755008;
    public static final int amu_basic_folder = 2131755009;
    public static final int amu_basic_placemark = 2131755010;
    public static final int amu_cdata = 2131755011;
    public static final int amu_default_balloon = 2131755012;
    public static final int amu_document_nest = 2131755013;
    public static final int amu_draw_order_ground_overlay = 2131755014;
    public static final int amu_extended_data = 2131755015;
    public static final int amu_ground_overlay = 2131755016;
    public static final int amu_ground_overlay_color = 2131755017;
    public static final int amu_inline_style = 2131755018;
    public static final int amu_multigeometry_placemarks = 2131755019;
    public static final int amu_multiple_placemarks = 2131755020;
    public static final int amu_nested_folders = 2131755021;
    public static final int amu_nested_multigeometry = 2131755022;
    public static final int amu_poly_style_boolean_alpha = 2131755023;
    public static final int amu_poly_style_boolean_numeric = 2131755024;
    public static final int amu_unknwown_folder = 2131755025;
    public static final int amu_unsupported = 2131755026;
    public static final int amu_visibility_ground_overlay = 2131755027;
    public static final int beep = 2131755028;
    public static final int cactus = 2131755029;
    public static final int keep = 2131755030;
}
