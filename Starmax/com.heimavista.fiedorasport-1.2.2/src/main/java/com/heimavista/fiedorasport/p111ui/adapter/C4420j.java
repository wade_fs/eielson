package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.SportBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.j */
/* compiled from: lambda */
public final /* synthetic */ class C4420j implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListDynamicsAdapter f8676P;

    /* renamed from: Q */
    private final /* synthetic */ int f8677Q;

    /* renamed from: R */
    private final /* synthetic */ SportBean f8678R;

    public /* synthetic */ C4420j(ListDynamicsAdapter listDynamicsAdapter, int i, SportBean gVar) {
        this.f8676P = listDynamicsAdapter;
        this.f8677Q = i;
        this.f8678R = gVar;
    }

    public final void onClick(View view) {
        this.f8676P.mo24637b(this.f8677Q, this.f8678R, view);
    }
}
