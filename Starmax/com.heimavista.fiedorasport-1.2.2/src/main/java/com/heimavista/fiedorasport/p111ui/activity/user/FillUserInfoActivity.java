package com.heimavista.fiedorasport.p111ui.activity.user;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingTargetStepActivity;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.FillUserInfoActivity */
public class FillUserInfoActivity extends UserCommonActivity {

    /* renamed from: l0 */
    private ImageView f8532l0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        AvatarUtils.m13393a(this.f8532l0, MemberControl.m17125s().mo27183h(), R$mipmap.login_user, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        mo22723b(R$id.ivAvatar, R$id.tvAvatar, R$id.btnAction);
        this.f8532l0 = (ImageView) findViewById(R$id.ivAvatar);
        mo24586a((RecyclerView) findViewById(R$id.recyclerView));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_user_info;
    }

    public void onBackPressed() {
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.tvAvatar || id == R$id.ivAvatar) {
            mo24585A();
        } else if (id != R$id.btnAction) {
        } else {
            if (mo23014v() == 0) {
                mo22730c(getString(R$string.member_height_empty));
            } else if (mo24598z() == 0) {
                mo22730c(getString(R$string.member_weight_empty));
            } else {
                startActivity(SettingTargetStepActivity.m13893b(false));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public List<MenuItem> mo23013u() {
        String str;
        String str2;
        String d = MemberControl.m17125s().mo27178d();
        mo24595f(d);
        ArrayList arrayList = new ArrayList();
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(1);
        bVar.mo22664b(getString(R$string.member_nickname));
        bVar.mo22659a(MemberControl.m17125s().mo27182g());
        arrayList.add(bVar.mo22661a());
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(1);
        bVar2.mo22664b(getString(R$string.member_name));
        bVar2.mo22659a(MemberControl.m17125s().mo27184i());
        arrayList.add(bVar2.mo22661a());
        MenuItem.C3676b bVar3 = new MenuItem.C3676b();
        bVar3.mo22663b(1);
        bVar3.mo22664b(getString(R$string.member_gender));
        bVar3.mo22659a(d.equals("M") ? "男" : d.equals("F") ? "女" : "");
        arrayList.add(bVar3.mo22661a());
        MenuItem.C3676b bVar4 = new MenuItem.C3676b();
        bVar4.mo22663b(1);
        bVar4.mo22664b(getString(R$string.member_birth));
        bVar4.mo22659a(FSManager.m10323r().mo22837g());
        arrayList.add(bVar4.mo22661a());
        MenuItem.C3676b bVar5 = new MenuItem.C3676b();
        bVar5.mo22663b(1);
        bVar5.mo22664b(getString(R$string.member_height));
        if (mo23014v() == 0) {
            str = getString(R$string.select_first);
        } else {
            str = mo23014v() + "cm";
        }
        bVar5.mo22659a(str);
        arrayList.add(bVar5.mo22661a());
        MenuItem.C3676b bVar6 = new MenuItem.C3676b();
        bVar6.mo22663b(1);
        bVar6.mo22664b(getString(R$string.member_weight));
        if (mo24598z() == 0) {
            str2 = getString(R$string.select_first);
        } else {
            str2 = mo24598z() + "KG";
        }
        bVar6.mo22659a(str2);
        bVar6.mo22662b();
        arrayList.add(bVar6.mo22661a());
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: w */
    public ImageView mo23015w() {
        if (this.f8532l0 == null) {
            this.f8532l0 = (ImageView) findViewById(R$id.ivAvatar);
        }
        return this.f8532l0;
    }
}
