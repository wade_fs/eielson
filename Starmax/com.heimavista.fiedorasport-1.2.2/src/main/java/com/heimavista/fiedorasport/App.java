package com.heimavista.fiedorasport;

import com.tencent.bugly.crashreport.CrashReport;
import javax.net.ssl.SSLSession;
import org.xutils.C5217x;

public class App extends FSApp {
    /* renamed from: b */
    static /* synthetic */ boolean m10173b(String str, SSLSession sSLSession) {
        return true;
    }

    /* renamed from: a */
    public /* synthetic */ boolean mo22676a(String str, SSLSession sSLSession) {
        return "com.heimavista.fiedorasport".equals(getPackageName());
    }

    public void onCreate() {
        super.onCreate();
        if (!mo24159b()) {
            CrashReport.initCrashReport(getApplicationContext(), "c3fdf8e5bc", false);
            C5217x.Ext.setDefaultHostnameVerifier(new C3677a(this));
            return;
        }
        C5217x.Ext.setDefaultHostnameVerifier(C3678b.f6405a);
    }
}
