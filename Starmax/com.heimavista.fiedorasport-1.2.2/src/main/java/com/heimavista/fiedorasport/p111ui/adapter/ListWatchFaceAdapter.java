package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.blankj.utilcode.util.ConvertUtils;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListWatchFaceAdapter */
public class ListWatchFaceAdapter extends BaseRecyclerViewAdapter<C4410a, ViewHolder> {

    /* renamed from: i */
    private View.OnClickListener f8637i;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListWatchFaceAdapter$ViewHolder */
    public class ViewHolder extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        CheckBox f8638R = ((CheckBox) findViewById(R$id.checkbox));

        /* renamed from: S */
        ImageView f8639S = ((ImageView) findViewById(R$id.ivWatchFace));

        /* renamed from: T */
        TextView f8640T = ((TextView) findViewById(R$id.tvTimeTopContent));

        /* renamed from: U */
        TextView f8641U = ((TextView) findViewById(R$id.tvTime));

        /* renamed from: V */
        TextView f8642V = ((TextView) findViewById(R$id.tvTimeBottomContent));

        /* renamed from: W */
        TextView f8643W = ((TextView) findViewById(R$id.tvWatchFaceName));

        /* renamed from: X */
        TextView f8644X = ((TextView) findViewById(R$id.tvEdit));

        ViewHolder(ListWatchFaceAdapter listWatchFaceAdapter, ViewGroup viewGroup, int i) {
            super(listWatchFaceAdapter, viewGroup, i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListWatchFaceAdapter$a */
    public static class C4410a {

        /* renamed from: a */
        boolean f8645a;

        /* renamed from: b */
        byte f8646b;

        /* renamed from: c */
        C1178n f8647c;

        public C4410a(boolean z, byte b) {
            this.f8645a = z;
            this.f8646b = b;
        }

        /* renamed from: a */
        public void mo24658a(C1178n nVar) {
            this.f8647c = nVar;
        }

        /* renamed from: b */
        public byte mo24660b() {
            return this.f8646b;
        }

        /* renamed from: a */
        public C1178n mo24657a() {
            return this.f8647c;
        }

        /* renamed from: a */
        public void mo24659a(boolean z) {
            this.f8645a = z;
        }
    }

    public ListWatchFaceAdapter(Context context) {
        super(context);
    }

    /* renamed from: c */
    private String m14359c(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? "" : "30958" : "208 BPM" : "10H36M" : "SAT 0928";
    }

    /* renamed from: a */
    public void mo24655a(View.OnClickListener onClickListener) {
        this.f8637i = onClickListener;
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        C4410a aVar = (C4410a) getItem(i);
        viewHolder.f8638R.setChecked(aVar.f8645a);
        boolean z = aVar.mo24660b() == 1;
        int i2 = 8;
        viewHolder.f8644X.setVisibility(z ? 0 : 8);
        viewHolder.f8640T.setVisibility(z ? 0 : 8);
        viewHolder.f8641U.setVisibility(z ? 0 : 8);
        TextView textView = viewHolder.f8642V;
        if (z) {
            i2 = 0;
        }
        textView.setVisibility(i2);
        byte b = aVar.mo24660b();
        if (b == 1) {
            String i3 = BandDataManager.m10567i();
            if (TextUtils.isEmpty(i3)) {
                viewHolder.f8639S.setImageResource(R$mipmap.icon_watch_face01);
            } else {
                viewHolder.f8639S.setImageBitmap(BitmapFactory.decodeFile(i3));
            }
            m14358a(aVar.mo24657a(), viewHolder.f8640T, viewHolder.f8641U, viewHolder.f8642V);
            View.OnClickListener onClickListener = this.f8637i;
            if (onClickListener != null) {
                viewHolder.f8644X.setOnClickListener(onClickListener);
            }
        } else if (b == 2) {
            viewHolder.f8639S.setImageResource(R$mipmap.icon_watch_face02);
        } else if (b == 3) {
            viewHolder.f8639S.setImageResource(R$mipmap.icon_watch_face03);
        }
        viewHolder.f8643W.setText(mo22788b().getString(R$string.watch_name, Byte.valueOf(aVar.mo24660b())));
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this, viewGroup, R$layout.layout_list_item_watch_face);
    }

    /* renamed from: a */
    private void m14358a(C1178n nVar, TextView textView, TextView textView2, TextView textView3) {
        if (nVar != null) {
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(-2, -2);
            ConstraintLayout.LayoutParams layoutParams2 = new ConstraintLayout.LayoutParams(-2, -2);
            layoutParams2.rightMargin = ConvertUtils.m1055a(5.0f);
            ConstraintLayout.LayoutParams layoutParams3 = new ConstraintLayout.LayoutParams(-2, -2);
            int d = nVar.mo10868d();
            if (d == 0) {
                layoutParams.topToTop = 0;
                layoutParams.endToEnd = R$id.tvTime;
                layoutParams2.topToBottom = R$id.tvTimeTopContent;
                layoutParams2.endToEnd = 0;
                int i = R$id.tvTime;
                layoutParams3.topToBottom = i;
                layoutParams3.endToEnd = i;
            } else if (d == 1) {
                int i2 = R$id.tvTime;
                layoutParams.bottomToTop = i2;
                layoutParams.endToEnd = i2;
                layoutParams2.bottomToTop = R$id.tvTimeBottomContent;
                layoutParams2.endToEnd = 0;
                layoutParams3.bottomToBottom = 0;
                layoutParams3.endToEnd = R$id.tvTime;
            }
            textView.setLayoutParams(layoutParams);
            textView2.setLayoutParams(layoutParams2);
            textView3.setLayoutParams(layoutParams3);
            textView.setText(m14359c(nVar.mo10870e()));
            textView3.setText(m14359c(nVar.mo10866c()));
            int b = nVar.mo10864b();
            textView2.setTextColor(b);
            textView.setTextColor(b);
            textView3.setTextColor(b);
        }
    }
}
