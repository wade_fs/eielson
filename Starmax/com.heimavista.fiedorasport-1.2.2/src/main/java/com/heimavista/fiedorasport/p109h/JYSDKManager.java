package com.heimavista.fiedorasport.p109h;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.api.band.HvApiPostCurStepData;
import com.heimavista.api.band.HvApiPostSportData;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.band.StepDetailInfo;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.entity.band.TickType;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.fiedorasport.service.JYBleService;
import com.heimavista.gad.HvGad;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.BleClientOption;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import com.sxr.sdk.ble.keepfit.aidl.IRemoteService;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import com.sxr.sdk.ble.keepfit.aidl.UserProfile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p193d.p194i.StepDetailDb;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;

/* renamed from: com.heimavista.fiedorasport.h.a0 */
public class JYSDKManager {

    /* renamed from: y */
    private static JYSDKManager f6480y = null;

    /* renamed from: z */
    public static boolean f6481z = false;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public HashMap<String, C3734y> f6482a = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C3734y.C3735a f6483b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f6484c = false;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public IRemoteService f6485d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public int f6486e = 0;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f6487f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public int f6488g = 0;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public int f6489h = 0;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public long f6490i = 0;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public long f6491j = 0;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public long f6492k = 0;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f6493l = false;

    /* renamed from: m */
    private ServiceConnection f6494m = new C3704a();

    /* renamed from: n */
    private IServiceCallback f6495n = new C3705b();

    /* renamed from: o */
    private String f6496o = null;

    /* renamed from: p */
    private String f6497p = null;

    /* renamed from: q */
    private int f6498q = 0;

    /* renamed from: r */
    private double f6499r = 0.0d;

    /* renamed from: s */
    private int f6500s = 0;

    /* renamed from: t */
    private int f6501t = 0;

    /* renamed from: u */
    private boolean f6502u = false;

    /* renamed from: v */
    private long f6503v = 0;

    /* renamed from: w */
    private SleepInfo f6504w;

    /* renamed from: x */
    private StepDetailInfo f6505x;

    /* renamed from: com.heimavista.fiedorasport.h.a0$a */
    /* compiled from: JYSDKManager */
    class C3704a implements ServiceConnection {
        C3704a() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            JYSDKManager.this.mo22878a("onServiceConnected");
            IRemoteService unused = JYSDKManager.this.f6485d = IRemoteService.C4760a.m15967a(iBinder);
            JYSDKManager.this.m10391j();
            JYSDKManager.this.mo22887a(HvApp.m13010c().mo24159b(), "/jyou/log/", "blue.log");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean
         arg types: [com.heimavista.fiedorasport.h.a0, int]
         candidates:
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, int):int
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, long):long
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, com.sxr.sdk.ble.keepfit.aidl.a):com.sxr.sdk.ble.keepfit.aidl.a
          com.heimavista.fiedorasport.h.a0.a(long, int):void
          com.heimavista.fiedorasport.h.a0.a(int, int):void
          com.heimavista.fiedorasport.h.a0.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
          com.heimavista.fiedorasport.h.a0.a(java.lang.String, java.lang.String):void
          com.heimavista.fiedorasport.h.a0.a(boolean, int):void
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean */
        public void onServiceDisconnected(ComponentName componentName) {
            JYSDKManager.this.mo22878a("onServiceDisconnected");
            IRemoteService unused = JYSDKManager.this.f6485d = (IRemoteService) null;
            boolean unused2 = JYSDKManager.this.f6484c = false;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.a0$b */
    /* compiled from: JYSDKManager */
    class C3705b extends IServiceCallback.C4762a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public HttpCancelable f6507a;

        /* renamed from: b */
        private long f6508b = 0;

        /* renamed from: c */
        private CountDownTimer f6509c;

        /* renamed from: com.heimavista.fiedorasport.h.a0$b$a */
        /* compiled from: JYSDKManager */
        class C3706a extends CountDownTimer {

            /* renamed from: a */
            final /* synthetic */ long f6511a;

            /* renamed from: b */
            final /* synthetic */ int f6512b;

            /* renamed from: c */
            final /* synthetic */ int f6513c;

            /* renamed from: d */
            final /* synthetic */ int f6514d;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C3706a(long j, long j2, long j3, int i, int i2, int i3) {
                super(j, j2);
                this.f6511a = j3;
                this.f6512b = i;
                this.f6513c = i2;
                this.f6514d = i3;
            }

            public void onFinish() {
                HttpCancelable unused = C3705b.this.f6507a = new HvApiPostCurStepData().request(this.f6511a, this.f6512b, this.f6513c, this.f6514d);
            }

            public void onTick(long j) {
            }
        }

        C3705b() {
        }

        /* renamed from: A */
        public void mo22903A(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetReminderText result=" + i);
        }

        /* renamed from: B */
        public void mo22904B(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetUserInfo " + i);
        }

        /* renamed from: D */
        public void mo22905D(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetLanguage " + i);
        }

        /* renamed from: E */
        public void mo22906E(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetNotify " + i);
        }

        /* renamed from: F */
        public void mo22907F(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onAuthSdkResult: " + i);
            if (!JYSDKManager.this.f6493l) {
                String a = HvGad.m14915i().mo25015e().mo9872a("macId", "");
                String l = FSManager.m10323r().mo22842l();
                if (!TextUtils.isEmpty(l) && l.equals(a) && !TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
                    JYSDKManager.this.mo22881a(FSManager.m10323r().mo22839i(), l);
                }
            }
        }

        /* renamed from: H */
        public void mo22908H(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetSleepTime result=" + i);
        }

        /* renamed from: a */
        public void mo22922a(String str, byte[] bArr) {
        }

        /* renamed from: a */
        public void mo22923a(String str, byte[] bArr, int i) {
        }

        /* renamed from: b */
        public void mo22928b(int i, long j, int i2, int i3) {
            String a = TimeUtils.m1001a(new Date(1000 * j));
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetSenserData: result: " + i + ",timestamp:" + j + ",time:" + a + ",heartrate:" + i2 + ",sleepstate:" + i3);
            if (j > 0 && i2 > 0) {
                HeartRateDataDb.m13151a(MemberControl.m17125s().mo27187l(), j, i2, JYSDKManager.f6481z ? 3 : 1);
                for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                    ((C3734y) entry.getValue()).mo22688a(j, i2, i3);
                }
            }
        }

        /* renamed from: c */
        public void mo22932c(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceInfo " + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.deviceInfo;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: d */
        public void mo22934d(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetAlarm " + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.alarms;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: e */
        public void mo22936e(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceCode " + i);
        }

        /* renamed from: f */
        public void mo22937f(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetIdleTime result=" + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.sedentary;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: h */
        public void mo22938h(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("setAutoHeartMode " + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.autoHeart;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: j */
        public void mo22939j(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetHourFormat " + i);
        }

        /* renamed from: k */
        public void mo22940k(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSendVibrationSignal result=" + i);
        }

        /* renamed from: m */
        public void mo22941m(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceHeartRateArea " + i);
        }

        /* renamed from: o */
        public void mo22942o(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceName result=" + i);
        }

        /* renamed from: p */
        public void mo22943p(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetReminder result=" + i);
        }

        /* renamed from: q */
        public void mo22944q(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceMode " + i);
        }

        /* renamed from: r */
        public void mo22945r(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetPhontMode result=" + i);
        }

        /* renamed from: s */
        public void mo22946s(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetAntiLost " + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.antiLost;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: t */
        public void mo22947t(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetDeviceTime " + i);
        }

        /* renamed from: u */
        public void mo22948u(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onAuthDeviceResult " + i);
        }

        /* renamed from: v */
        public void mo22949v(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetGoalStep " + i);
            if (JYSDKManager.this.f6483b != null) {
                C3734y.C3735a h = JYSDKManager.this.f6483b;
                C3734y.C3736b bVar = C3734y.C3736b.goalStep;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                h.mo22964a(bVar, z);
            }
        }

        /* renamed from: x */
        public void mo22950x(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetReminder rssi=" + i);
        }

        /* renamed from: y */
        public void mo22951y(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSendWeather " + i);
        }

        /* renamed from: z */
        public void mo22952z(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onConnectStateChanged state=" + i);
            if (i != 2) {
                BandDataManager.m10557b(0);
            }
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22718b(i);
            }
        }

        /* renamed from: a */
        public void mo22921a(String str, String str2, int i) {
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                JYSDKManager a0Var = JYSDKManager.this;
                a0Var.mo22878a("scanDevice onScanCallback: [" + str + "][" + str2 + "][" + i + "]");
                for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                    ((C3734y) entry.getValue()).mo22706a(str, str2, i);
                }
            }
        }

        /* renamed from: c */
        public void mo22933c(int i, int i2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetOtaUpdate: step " + i + " progress " + i2);
        }

        /* renamed from: d */
        public void mo22935d(int i, int i2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSensorStateChange type=" + i + " state=" + i2);
        }

        /* renamed from: a */
        public void mo22910a(int i, int i2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetDeviceBattery battery=" + i + ", state=" + i2);
            BandDataManager.m10557b(i);
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22682a(i, i2);
            }
        }

        /* renamed from: b */
        public void mo22929b(int i, String str, int i2, int i3) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onReadCurrentSportData: mode=" + i + ", time=" + str + ", step=" + i2 + "， cal=" + i3);
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22684a(i, str, i2, i3);
            }
        }

        /* renamed from: b */
        public void mo22925b(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetBloodPressureMode " + i);
        }

        /* renamed from: a */
        public void mo22917a(int i, String str, String str2, String str3, int i2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetDeviceInfo version=" + i + ", deviceMacAddress=" + str + ", vendorCode=" + str2 + ", productCode=" + str3 + ", crcResult=" + i2);
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22686a(i, str, str2, str3, i2);
            }
        }

        /* renamed from: b */
        public void mo22927b(int i, int i2, int i3, int i4, int i5) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onReceiveSensorData: result:" + i + " , " + i2 + " , " + i3 + " , " + i4 + " , " + i5);
        }

        /* renamed from: b */
        public void mo22931b(boolean z, String str, String str2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetOtaInfo: isUpdate " + z + " info " + str + " path " + str2);
        }

        /* renamed from: b */
        public void mo22926b(int i, int i2) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onSetEcgMode result=" + i + ", state=" + i2);
        }

        /* renamed from: a */
        public void mo22914a(int i, long j, int i2, int i3, int i4, int i5, int i6, int i7) {
            int i8 = i;
            long j2 = j;
            int i9 = i2;
            int i10 = i3;
            int i11 = i4;
            if (i8 == 0) {
                JYSDKManager a0Var = JYSDKManager.this;
                a0Var.mo22878a("onGetCurSportData " + i8 + "step  " + j2 + " step=" + i9 + ", distance=" + i10 + ", cal=" + i11 + ", cursleeptime=" + i5);
                StepInfo stepInfo = new StepInfo(MemberControl.m17125s().mo27187l(), j, TimeUtil.m13442a(), i2, i3, i4);
                for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                    ((C3734y) entry.getValue()).mo22690a(j, i2, i3, i4, i5);
                }
                if (BuddyListDb.m13245F() > 0 && StepDataDb.m13223f(0).f6245S != i9) {
                    if (j2 - this.f6508b > 30) {
                        HttpCancelable bVar = this.f6507a;
                        if (bVar != null) {
                            bVar.cancel();
                        }
                        this.f6507a = new HvApiPostCurStepData().request(j, i2, i3, i4);
                        this.f6508b = j2;
                        CountDownTimer countDownTimer = this.f6509c;
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                            this.f6509c = null;
                        }
                    } else {
                        CountDownTimer countDownTimer2 = this.f6509c;
                        if (countDownTimer2 != null) {
                            countDownTimer2.cancel();
                            this.f6509c = null;
                        }
                        C3706a aVar = r0;
                        C3706a aVar2 = new C3706a(30000, 1000, j, i2, i3, i4);
                        this.f6509c = aVar;
                        this.f6509c.start();
                    }
                }
                if (StepDataDb.m13223f(0).f6245S < i9) {
                    StepDataDb hVar = new StepDataDb();
                    hVar.mo24237i(TimeUtil.m13442a());
                    hVar.mo24238j(MemberControl.m17125s().mo27187l());
                    hVar.mo24235a(j2);
                    hVar.mo24276c(i9);
                    hVar.mo24275b(i10);
                    hVar.mo24273a(i4);
                    hVar.mo24181l();
                    CurStepDataDb.m13135a(stepInfo);
                    return;
                }
                return;
            }
            JYSDKManager a0Var2 = JYSDKManager.this;
            a0Var2.mo22878a("onGetCurSportData " + i8 + "sport " + j2 + " step=" + i9 + ", totalrunningtime=" + i6 + ", steptime=" + i7);
            StepDataDb.m13219a(MemberControl.m17125s().mo27187l(), JYSDKManager.this.f6486e, j2);
            for (Map.Entry entry2 : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry2.getValue()).mo22689a(j, i2, i6, i7);
            }
        }

        /* renamed from: b */
        public void mo22930b(int i, int[] iArr) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetEcgHistoryData id=" + i + ", values=" + Arrays.toString(iArr));
        }

        /* renamed from: a */
        public void mo22913a(int i, long j, int i2, int i3) {
            JYSDKManager a0Var = JYSDKManager.this;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetDataByDay ");
            sb.append(i == 1 ? "sport" : i == 2 ? "sleep" : "heart");
            sb.append(" ");
            sb.append(j);
            sb.append(" ");
            sb.append(i == 3 ? i3 : i2);
            a0Var.mo22878a(sb.toString());
            if (j <= 0) {
                return;
            }
            if (i == 1) {
                JYSDKManager a0Var2 = JYSDKManager.this;
                int unused = a0Var2.f6487f = a0Var2.f6487f + i2;
                if (i2 > 0) {
                    JYSDKManager.m10394l(JYSDKManager.this);
                }
            } else if (i == 2) {
                JYSDKManager.m10399o(JYSDKManager.this);
                if (j > JYSDKManager.this.f6491j) {
                    long unused2 = JYSDKManager.this.f6491j = j;
                    JYSDKManager.this.m10372a(j, i2);
                }
            } else if (i == 3 && i3 > 0) {
                if (j > JYSDKManager.this.f6492k) {
                    long unused3 = JYSDKManager.this.f6492k = j;
                    HeartRateDataDb.m13151a(MemberControl.m17125s().mo27187l(), j, i3, 0);
                }
                JYSDKManager.m10382d(JYSDKManager.this);
            }
        }

        /* renamed from: a */
        public void mo22912a(int i, long j) {
            LogUtils.m1131b("onGetDataByDayEnd " + j);
            if (TimeUtils.m1007a(1000 * j)) {
                if (JYSDKManager.this.f6487f > 0 || JYSDKManager.this.f6488g > 0) {
                    JYSDKManager.this.m10393k();
                    JYSDKManager.this.m10395l();
                    HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onGetSleepDataFinish");
                }
                if (JYSDKManager.this.f6489h > 0) {
                    HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onGetHeartDataFinish");
                }
            }
            if (JYSDKManager.this.f6486e > 0 && JYSDKManager.this.f6487f > 0 && j > 0) {
                LogUtils.m1131b("onGetDataByDayEnd stepData step=" + JYSDKManager.this.f6487f + " stepTime=" + JYSDKManager.this.f6486e + " timestamp=" + j);
                StepDataDb.m13220a(MemberControl.m17125s().mo27187l(), j, JYSDKManager.this.f6486e, JYSDKManager.this.f6487f);
                int unused = JYSDKManager.this.f6486e = 0;
                int unused2 = JYSDKManager.this.f6487f = 0;
                TickManager.m10524b(TickType.STEP, JYSDKManager.this.f6490i);
            }
            if (JYSDKManager.this.f6488g > 0 && j > 0) {
                LogUtils.m1131b("onGetDataByDayEnd sleepData sleepCount=" + JYSDKManager.this.f6488g + " timestamp=" + j);
                int unused3 = JYSDKManager.this.f6488g = 0;
                TickManager.m10524b(TickType.SLEEP, JYSDKManager.this.f6491j);
            }
            if (JYSDKManager.this.f6489h > 0) {
                LogUtils.m1131b("onGetDataByDayEnd heartData heartCount=" + JYSDKManager.this.f6489h + " timestamp=" + j);
                int unused4 = JYSDKManager.this.f6489h = 0;
                TickManager.m10524b(TickType.HEART, JYSDKManager.this.f6492k);
            }
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22683a(i, JYSDKManager.this.f6489h > 0 ? 2 : 1, j);
            }
        }

        /* renamed from: a */
        public void mo22916a(int i, String str, int i2, int i3) {
            JYSDKManager.this.m10371a(i, str, i2, i3);
        }

        /* renamed from: a */
        public void mo22915a(int i, String str) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetDeviceTime " + i + " " + str);
        }

        /* renamed from: a */
        public void mo22909a(int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetDeviceAction: type=" + i + " 说明：1手机震动和发出铃声 2手机拍照 4挂断电话 5手环向APP请求天气数据");
            for (Map.Entry entry : JYSDKManager.this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22681a(i);
            }
        }

        /* renamed from: a */
        public void mo22919a(int i, boolean[] zArr) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetBandFunction: " + i + " " + zArr.length + ", " + Arrays.toString(zArr));
        }

        /* renamed from: a */
        public void mo22924a(byte[] bArr) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetDeviceCode: bytes " + JYSDKManager.this.mo22870a(bArr));
        }

        /* renamed from: a */
        public void mo22918a(int i, int[] iArr) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetEcgValue state=" + i + ", values=" + Arrays.toString(iArr));
        }

        /* renamed from: a */
        public void mo22920a(long j, int i) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetEcgHistory timestamp=" + j + ", number=" + i);
        }

        /* renamed from: a */
        public void mo22911a(int i, int i2, long j) {
            JYSDKManager a0Var = JYSDKManager.this;
            a0Var.mo22878a("onGetEcgStartEnd id=" + i + ", state=" + i2 + ", timestamp=" + j);
        }
    }

    private JYSDKManager() {
    }

    /* renamed from: d */
    static /* synthetic */ int m10382d(JYSDKManager a0Var) {
        int i = a0Var.f6489h;
        a0Var.f6489h = i + 1;
        return i;
    }

    /* renamed from: l */
    static /* synthetic */ int m10394l(JYSDKManager a0Var) {
        int i = a0Var.f6486e;
        a0Var.f6486e = i + 1;
        return i;
    }

    /* renamed from: o */
    static /* synthetic */ int m10399o(JYSDKManager a0Var) {
        int i = a0Var.f6488g;
        a0Var.f6488g = i + 1;
        return i;
    }

    /* renamed from: i */
    public static JYSDKManager m10388i() {
        if (f6480y == null) {
            f6480y = new JYSDKManager();
        }
        return f6480y;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m10391j() {
        if (this.f6485d != null) {
            try {
                mo22878a("registerCallback");
                this.f6485d.mo26464b(this.f6495n);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m10393k() {
        SleepInfo cVar = this.f6504w;
        if (cVar != null) {
            SleepDataDb.m13178a(cVar);
            this.f6504w = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public void m10395l() {
        int i;
        StepDetailInfo gVar = this.f6505x;
        if (gVar != null && (i = gVar.f6306i) != 0) {
            int i2 = gVar.f6307j / i;
            if (i2 < 50) {
                gVar.f6301d = 1;
            } else if (i2 < 120) {
                gVar.f6301d = 2;
            } else if (i2 < 140) {
                gVar.f6301d = 3;
            } else {
                gVar.f6301d = 4;
            }
            StepDetailInfo gVar2 = this.f6505x;
            gVar2.f6304g = C4222l.m13461b(gVar2.f6307j);
            StepDetailInfo gVar3 = this.f6505x;
            gVar3.f6305h = C4222l.m13463c(gVar3.f6307j);
            new StepDetailDb().mo24281a(this.f6505x);
            this.f6505x = null;
        }
    }

    /* renamed from: m */
    private void m10397m() {
        if (this.f6485d != null) {
            try {
                mo22878a("unregisterCallback");
                this.f6485d.mo26459a(this.f6495n);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: e */
    public void mo22896e(boolean z) {
        this.f6493l = z;
    }

    /* renamed from: f */
    public void mo22898f() {
        HashMap<String, C3734y> hashMap = this.f6482a;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.f6483b = null;
    }

    /* renamed from: g */
    public void mo22899g() {
        if (this.f6485d != null) {
            UserProfile h = BandDataManager.m10566h();
            DeviceProfile b = BandDataManager.m10556b();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 5; i++) {
                arrayList.add(BandDataManager.m10547a(i));
            }
            try {
                this.f6485d.mo26450a(new BleClientOption(h, b, arrayList));
                this.f6485d.mo26442B();
                this.f6485d.mo26480s();
                this.f6485d.mo26477o();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            mo22889b(BandDataManager.m10565g());
            mo22893c(BandDataManager.m10563e().mo22599a());
            m10388i().mo22874a(BandDataManager.m10555b("Sedentary"));
        }
    }

    /* renamed from: h */
    public void mo22900h() {
        try {
            m10397m();
            mo22878a("unbindService");
            ServiceUtils.m1266a(this.f6494m);
        } catch (Exception e) {
            mo22878a(e.getMessage());
        }
        this.f6484c = false;
    }

    /* renamed from: c */
    public void mo22892c() {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26441A();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: d */
    public void mo22894d() {
        this.f6490i = TickManager.m10517a(TickType.STEP);
        this.f6491j = TickManager.m10517a(TickType.SLEEP);
        this.f6492k = TickManager.m10517a(TickType.HEART);
    }

    /* renamed from: e */
    public boolean mo22897e() {
        boolean z;
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                z = aVar.mo26476n();
            } catch (RemoteException e) {
                mo22878a(e.getMessage());
            }
            mo22878a("isConnect=" + z);
            return z;
        }
        z = false;
        mo22878a("isConnect=" + z);
        return z;
    }

    /* renamed from: b */
    public void mo22890b(String str) {
        HashMap<String, C3734y> hashMap = this.f6482a;
        if (hashMap != null) {
            hashMap.remove(str);
        }
        this.f6483b = null;
    }

    /* renamed from: c */
    public void mo22893c(boolean z) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                int f = aVar.mo26469f(z);
                mo22878a("setAntiLost enable=" + z + " " + f);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: d */
    public void mo22895d(boolean z) {
        this.f6484c = z;
    }

    /* renamed from: b */
    public void mo22891b(boolean z) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                int h = aVar.mo26472h(z);
                mo22878a("scanDevice enable=" + z + " " + h);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22880a(String str, C3734y yVar) {
        if (this.f6482a == null) {
            this.f6482a = new HashMap<>();
        }
        if (!this.f6482a.containsKey(str)) {
            this.f6482a.put(str, yVar);
        }
    }

    /* renamed from: b */
    public synchronized void mo22888b() {
        if (this.f6485d != null) {
            try {
                this.f6485d.mo26485x();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return;
    }

    /* renamed from: a */
    public void mo22875a(C3734y.C3735a aVar) {
        this.f6483b = aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo22870a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() == 1) {
                hexString = '0' + hexString;
            }
            sb.append(hexString.toUpperCase());
            sb.append(" ");
        }
        return sb.toString();
    }

    /* renamed from: b */
    public void mo22889b(int i) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null && i > 0) {
            try {
                int l = aVar.mo26474l(i);
                mo22878a("setGoalStep step=" + i + " " + l);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22871a() {
        mo22878a("bindService " + this.f6484c);
        if (!this.f6484c) {
            ServiceUtils.m1267a(JYBleService.class, this.f6494m, 1);
            this.f6484c = true;
        }
    }

    /* renamed from: a */
    public void mo22876a(BleClientOption bleClientOption) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26450a(bleClientOption);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22881a(String str, String str2) {
        mo22882a(str, str2, "jyoutManager");
    }

    /* renamed from: a */
    public void mo22882a(String str, String str2, String str3) {
        Constant.m10311a("手環連接");
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26452a(str, str2.toUpperCase());
                mo22878a("connectBt deviceName=" + str + "  macAddress=" + str2 + " " + str3);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22884a(boolean z) {
        mo22878a("callDisconnectBt");
        Constant.m10311a("手環斷開");
        if (this.f6485d != null) {
            try {
                mo22878a("disconnectBt enable=" + z);
                this.f6485d.mo26465b(z);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22874a(PeriodTimeInfo periodTimeInfo) {
        if (this.f6485d != null) {
            try {
                int e = periodTimeInfo.mo22590e();
                if (!periodTimeInfo.mo22593g()) {
                    e = 1440;
                }
                this.f6485d.mo26447a(e * 60, periodTimeInfo.mo22585c(), periodTimeInfo.mo22587d(), periodTimeInfo.mo22580a(), periodTimeInfo.mo22583b());
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22883a(ArrayList<AlarmInfoItem> arrayList) {
        if (this.f6485d != null) {
            try {
                StringBuilder sb = new StringBuilder("callSetAlarm \n");
                Iterator<AlarmInfoItem> it = arrayList.iterator();
                while (it.hasNext()) {
                    AlarmInfoItem next = it.next();
                    sb.append("alarmInfo_");
                    sb.append(next.mo26354a());
                    sb.append(" ");
                    sb.append(next.mo26358b());
                    sb.append(" single=");
                    sb.append(next.mo26379m());
                    sb.append(" type=");
                    sb.append(next.mo26373i());
                    sb.append(" ");
                    sb.append(next.mo26377k());
                    sb.append(":");
                    sb.append(next.mo26378l());
                    sb.append(" week=");
                    sb.append(next.mo26362d());
                    sb.append(next.mo26371h());
                    sb.append(next.mo26375j());
                    sb.append(next.mo26369g());
                    sb.append(next.mo26360c());
                    sb.append(next.mo26365e());
                    sb.append(next.mo26367f());
                    sb.append("\n");
                }
                mo22878a(sb.toString());
                this.f6485d.mo26450a(new BleClientOption(null, null, arrayList));
                this.f6485d.mo26477o();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22879a(String str, int i, String str2, String str3) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                boolean a = aVar.mo26460a(str, i, str2, str3);
                mo22878a("setNotify id=" + str + " title=" + str2 + " content=" + str3 + " b=" + a);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22885a(boolean z, int i) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                int a = aVar.mo26454a(z, i);
                mo22878a("setHeartRateMode enable=" + z + " time=" + i + " " + a);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22886a(boolean z, int i, int i2, int i3, int i4, int i5, int i6) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26456a(z, i, i2, i3, i4, i5, i6);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22877a(DeviceProfile deviceProfile) {
        if (this.f6485d != null) {
            try {
                mo22878a("callSetDeviceInfo " + deviceProfile.mo26407e() + " " + deviceProfile.mo26409g() + " " + deviceProfile.mo26408f() + " " + deviceProfile.mo26401c() + ":" + deviceProfile.mo26404d() + "~" + deviceProfile.mo26395a() + ":" + deviceProfile.mo26398b());
                mo22876a(new BleClientOption(null, deviceProfile, null));
                this.f6485d.mo26480s();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22873a(int i, int i2) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26467e(i, i2);
                mo22878a("getDataByDay type=" + i + ", day=" + i2);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22872a(int i) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                aVar.mo26470g(i);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo22887a(boolean z, String str, String str2) {
        IRemoteService aVar = this.f6485d;
        if (aVar != null) {
            try {
                int a = aVar.mo26457a(z, str, str2);
                mo22878a("openSDKLog enable=" + z + " filePath=" + str + " fileName=" + str2 + " " + a);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10372a(long j, int i) {
        int i2 = i == 0 ? 3 : i < 80 ? 2 : 1;
        SleepInfo cVar = this.f6504w;
        if (cVar == null) {
            this.f6504w = new SleepInfo(j, i2, 1);
        } else if (j - cVar.f6267f == 60 && i2 == cVar.f6265d) {
            cVar.f6267f = j;
            cVar.f6268g++;
        } else {
            m10393k();
            this.f6504w = new SleepInfo(j, i2, 1);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10371a(int i, String str, int i2, int i3) {
        String str2;
        int i4;
        long j;
        int i5 = i;
        String str3 = str;
        int i6 = i2;
        int i7 = i3;
        mo22878a("onGetMultipleSportData: time=" + str3 + ", flag=" + i5 + ", mode=" + i6 + ",value=" + i7);
        if (i5 == 2) {
            for (Map.Entry<String, C3734y> entry : this.f6482a.entrySet()) {
                ((C3734y) entry.getValue()).mo22705a(str3);
            }
        }
        if (i6 > 0 && i7 > 0) {
            this.f6498q++;
            if (this.f6496o == null) {
                this.f6496o = str3;
                this.f6501t = i6;
            }
            this.f6497p = str3;
        }
        if (i6 == 1) {
            this.f6499r += (double) (((float) (i7 * 36)) / 1000.0f);
            this.f6500s += i7;
        } else if (i6 == 2) {
            this.f6499r += (double) (((float) (i7 * 49)) / 1000.0f);
            this.f6500s += i7;
        } else if (i6 != 4) {
            this.f6499r += (double) (((float) i7) / 100.0f);
        } else {
            this.f6499r += (double) (((float) (i7 * 45)) / 1000.0f);
            this.f6500s += i7;
        }
        if (i6 == 0 && (str2 = this.f6496o) != null) {
            long a = TimeUtils.m996a(str2, "yyyy-MM-dd HH:mm:ss") / 1000;
            long a2 = TimeUtils.m996a(this.f6497p, "yyyy-MM-dd HH:mm:ss") / 1000;
            mo22878a("onGetMultipleSportData " + (this.f6503v - a2));
            if (this.f6503v - a2 < 0) {
                int c = C4222l.m13463c(this.f6500s);
                if (this.f6498q >= 3) {
                    i4 = c;
                    j = a2;
                    m10370a(this.f6501t, c, a, a2);
                } else {
                    i4 = c;
                    j = a2;
                }
                mo22878a("onGetMultipleSportDataEND start=" + this.f6496o + ", end=" + this.f6497p + ", distance=" + i4 + ", mode=" + this.f6501t + ", cal=" + this.f6499r + ", step=" + this.f6500s);
                this.f6503v = j;
                this.f6496o = null;
                this.f6497p = null;
                this.f6498q = 0;
                this.f6499r = 0.0d;
                this.f6500s = 0;
            }
        }
        this.f6501t = i6;
        if (TimeUtils.m1010b(str)) {
            if (TimeUtils.m1008b().compareTo(str3) <= 0 && !this.f6502u) {
                TickManager.m10524b(TickType.SPORT, TimeUtils.m1013c(str) / 1000);
                this.f6502u = true;
            }
            if (i5 == 2) {
                this.f6502u = false;
            }
        }
    }

    /* renamed from: a */
    private void m10370a(int i, int i2, long j, long j2) {
        SportInfo a = new HeartRateDataDb().mo24251a(MemberControl.m17125s().mo27187l(), j, j2);
        if (a.f6274e == 0) {
            a.f6274e = j;
        }
        if (a.f6275f == 0) {
            a.f6275f = j2;
        }
        a.f6276g = (int) (a.f6275f - a.f6274e);
        mo22878a("onGetMultipleSportData " + j + "~" + j2 + " duration:" + (a.f6275f - a.f6274e));
        a.f6277h = i2;
        a.f6278i = (int) this.f6499r;
        a.f6270a = MemberControl.m17125s().mo27187l();
        a.f6271b = TimeUtil.m13442a();
        if (i == 1) {
            a.f6273d = 1;
        } else if (i == 2) {
            a.f6273d = 0;
        } else if (i == 3) {
            a.f6273d = 2;
        }
        new HvApiPostSportData(a).request();
    }

    /* renamed from: a */
    public void mo22878a(String str) {
        LogUtils.m1139c("JYLog JY " + str);
    }
}
