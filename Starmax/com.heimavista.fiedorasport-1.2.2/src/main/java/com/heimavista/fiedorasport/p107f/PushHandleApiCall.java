package com.heimavista.fiedorasport.p107f;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.heimavista.apn.PushHandler;
import com.heimavista.apn.PushMsg;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import org.json.JSONObject;
import p119e.p189e.p191b.HvNotify;

/* renamed from: com.heimavista.fiedorasport.f.a */
public class PushHandleApiCall implements PushHandler {
    /* renamed from: a */
    public void mo22549a(Context context, PushMsg pushMsg) {
        Context context2 = context;
        PushMsg pushMsg2 = pushMsg;
        LogUtils.m1139c("madyPushMsg>>>" + pushMsg.toString());
        JSONObject optJSONObject = pushMsg2.f6231U.mo25334d().optJSONObject("ext");
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("Fun");
            String optString2 = optJSONObject.optString("op");
            if (optString.equalsIgnoreCase("buddy") && optString2.equalsIgnoreCase("postLoc")) {
                LocManager.m10493g().mo22958c();
            }
        }
        if (!TextUtils.isEmpty(pushMsg2.f6228R) && !TextUtils.equals("null", pushMsg2.f6228R)) {
            if (TextUtils.equals("null", pushMsg2.f6227Q)) {
                pushMsg2.f6227Q = context2.getString(R$string.app_name);
            }
            HvNotify.m13018b().mo24163a(context, "channel_fs_push", context2.getString(R$string.channel_push), 3, pushMsg2.f6227Q, R$mipmap.ic_push, BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon), PendingIntent.getActivity(context2, 10005, new Intent(context2, HomeActivity.class), 134217728), pushMsg2.f6228R, pushMsg2.f6227Q, DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS, 10005);
        }
    }
}
