package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.view.View;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.h */
/* compiled from: lambda */
public final /* synthetic */ class C4481h implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ MainMyFragment f8874P;

    public /* synthetic */ C4481h(MainMyFragment mainMyFragment) {
        this.f8874P = mainMyFragment;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8874P.mo24717a(view, (MenuItem) obj, i);
    }
}
