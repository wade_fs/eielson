package com.heimavista.fiedorasport.p111ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.widget.ProgressBar;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.p203ns.NsManage;
import com.heimavista.p203ns.gui.NsJs;
import com.heimavista.widget.HvWebView;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity */
public class NsAddressActivity extends BaseActivity {

    /* renamed from: b0 */
    private HvWebView f8533b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public ProgressBar f8534c0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity$a */
    class C4381a implements HvWebView.C4599d {
        C4381a() {
        }

        /* renamed from: a */
        public void mo24581a(int i) {
            NsAddressActivity.this.f8534c0.setProgress(i);
        }

        public void onFinished() {
            NsAddressActivity.this.f8534c0.setVisibility(8);
        }

        public void onStart() {
            NsAddressActivity.this.f8534c0.setVisibility(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity.a(com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity, java.lang.CharSequence):void
         arg types: [com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity, java.lang.String]
         candidates:
          com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity.a(android.content.Context, java.lang.String):android.content.Intent
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity.a(com.heimavista.fiedorasport.ui.activity.user.NsAddressActivity, java.lang.CharSequence):void */
        /* renamed from: a */
        public void mo24582a(String str) {
            if (!TextUtils.isEmpty(str) && !URLUtil.isNetworkUrl(str)) {
                NsAddressActivity.this.mo22701a((CharSequence) str);
            }
        }
    }

    /* renamed from: u */
    private void m14210u() {
        this.f8533b0.setOnProgressListener(new C4381a());
        NsJs eVar = new NsJs();
        eVar.mo25477a(this);
        this.f8533b0.setJsInterface(eVar);
        WebSettings settings = this.f8533b0.getSettings();
        settings.setUserAgentString(settings.getUserAgentString() + " FiedoraApp");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8534c0 = (ProgressBar) findViewById(R$id.progressBar);
        this.f8533b0 = (HvWebView) findViewById(R$id.webview);
        m14210u();
        this.f8533b0.loadUrl(getIntent().getStringExtra("url"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        NsManage.m15135d().mo25156a(this);
        return R$layout.fs_activity_web;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.member_address);
    }

    public void onBackPressed() {
        if (this.f8533b0.canGoBack()) {
            this.f8533b0.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* renamed from: a */
    public static Intent m14207a(Context context, String str) {
        Intent intent = new Intent(context, NsAddressActivity.class);
        intent.putExtra("url", str);
        return intent;
    }
}
