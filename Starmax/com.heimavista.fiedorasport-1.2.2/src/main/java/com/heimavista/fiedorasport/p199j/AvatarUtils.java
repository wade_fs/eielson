package com.heimavista.fiedorasport.p199j;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.p041l.CustomTarget;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.p040q.p042m.Transition;
import com.heimavista.fiedorasport.lib.R$mipmap;
import java.util.concurrent.ExecutionException;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.j.d */
public class AvatarUtils {

    /* renamed from: com.heimavista.fiedorasport.j.d$a */
    /* compiled from: AvatarUtils */
    static class C4215a extends CustomTarget<Bitmap> {

        /* renamed from: S */
        final /* synthetic */ int f8024S;

        /* renamed from: T */
        final /* synthetic */ C4219e f8025T;

        /* renamed from: U */
        final /* synthetic */ UserInfoDb f8026U;

        C4215a(int i, C4219e eVar, UserInfoDb fVar) {
            this.f8024S = i;
            this.f8025T = eVar;
            this.f8026U = fVar;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo10453a(@NonNull Object obj, @Nullable Transition bVar) {
            mo24347a((Bitmap) obj, (Transition<? super Bitmap>) bVar);
        }

        /* renamed from: c */
        public void mo10646c(@Nullable Drawable drawable) {
        }

        /* renamed from: a */
        public void mo24347a(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> bVar) {
            int i;
            if (this.f8024S > 0 && bitmap.getWidth() != (i = this.f8024S)) {
                bitmap = ImageUtils.m1101a(bitmap, i, i);
            }
            AvatarUtils.m13397b(this.f8025T, bitmap, this.f8026U.mo24240q(), this.f8026U.mo24337w());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.j.d$b */
    /* compiled from: AvatarUtils */
    static class C4216b implements RequestListener<Bitmap> {

        /* renamed from: P */
        final /* synthetic */ int f8027P;

        /* renamed from: Q */
        final /* synthetic */ Context f8028Q;

        /* renamed from: R */
        final /* synthetic */ C4219e f8029R;

        /* renamed from: S */
        final /* synthetic */ UserInfoDb f8030S;

        C4216b(int i, Context context, C4219e eVar, UserInfoDb fVar) {
            this.f8027P = i;
            this.f8028Q = context;
            this.f8029R = eVar;
            this.f8030S = fVar;
        }

        /* renamed from: a */
        public boolean mo24348a(Bitmap bitmap, Object obj, Target<Bitmap> iVar, DataSource aVar, boolean z) {
            return false;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo10643a(Object obj, Object obj2, Target iVar, DataSource aVar, boolean z) {
            return mo24348a((Bitmap) obj, obj2, (Target<Bitmap>) iVar, aVar, z);
        }

        /* renamed from: a */
        public boolean mo10642a(@Nullable GlideException qVar, Object obj, Target<Bitmap> iVar, boolean z) {
            int i;
            BitmapFactory.Options options = new BitmapFactory.Options();
            int i2 = this.f8027P;
            if (i2 > 0) {
                options.outHeight = i2;
                options.outWidth = i2;
            }
            Bitmap decodeResource = BitmapFactory.decodeResource(this.f8028Q.getResources(), R$mipmap.login_user, options);
            if (this.f8027P > 0 && decodeResource.getWidth() != (i = this.f8027P)) {
                decodeResource = ImageUtils.m1101a(decodeResource, i, i);
            }
            AvatarUtils.m13397b(this.f8029R, decodeResource, this.f8030S.mo24240q(), this.f8030S.mo24337w());
            return false;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.j.d$c */
    /* compiled from: AvatarUtils */
    static class C4217c extends CustomTarget<Bitmap> {

        /* renamed from: S */
        final /* synthetic */ int f8031S;

        /* renamed from: T */
        final /* synthetic */ int f8032T;

        /* renamed from: U */
        final /* synthetic */ C4219e f8033U;

        /* renamed from: V */
        final /* synthetic */ int f8034V;

        C4217c(int i, int i2, C4219e eVar, int i3) {
            this.f8031S = i;
            this.f8032T = i2;
            this.f8033U = eVar;
            this.f8034V = i3;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo10453a(@NonNull Object obj, @Nullable Transition bVar) {
            mo24349a((Bitmap) obj, (Transition<? super Bitmap>) bVar);
        }

        /* renamed from: c */
        public void mo10646c(@Nullable Drawable drawable) {
        }

        /* renamed from: a */
        public void mo24349a(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> bVar) {
            int i;
            Bitmap a = AvatarUtils.m13396b(this.f8031S, this.f8032T, bitmap);
            if (this.f8033U != null && a != null) {
                if (this.f8034V > 0 && a.getWidth() != (i = this.f8034V)) {
                    a = ImageUtils.m1101a(a, i, i);
                }
                this.f8033U.mo24351a(a);
                a.recycle();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.j.d$d */
    /* compiled from: AvatarUtils */
    static class C4218d implements RequestListener<Bitmap> {

        /* renamed from: P */
        final /* synthetic */ int f8035P;

        /* renamed from: Q */
        final /* synthetic */ Context f8036Q;

        /* renamed from: R */
        final /* synthetic */ C4219e f8037R;

        C4218d(int i, Context context, C4219e eVar) {
            this.f8035P = i;
            this.f8036Q = context;
            this.f8037R = eVar;
        }

        /* renamed from: a */
        public boolean mo24350a(Bitmap bitmap, Object obj, Target<Bitmap> iVar, DataSource aVar, boolean z) {
            return false;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo10643a(Object obj, Object obj2, Target iVar, DataSource aVar, boolean z) {
            return mo24350a((Bitmap) obj, obj2, (Target<Bitmap>) iVar, aVar, z);
        }

        /* renamed from: a */
        public boolean mo10642a(@Nullable GlideException qVar, Object obj, Target<Bitmap> iVar, boolean z) {
            int i;
            BitmapFactory.Options options = new BitmapFactory.Options();
            int i2 = this.f8035P;
            if (i2 > 0) {
                options.outHeight = i2;
                options.outWidth = i2;
            }
            Bitmap decodeResource = BitmapFactory.decodeResource(this.f8036Q.getResources(), R$mipmap.login_user, options);
            if (this.f8037R == null || decodeResource == null) {
                return false;
            }
            if (this.f8035P > 0 && decodeResource.getWidth() != (i = this.f8035P)) {
                decodeResource = ImageUtils.m1101a(decodeResource, i, i);
            }
            this.f8037R.mo24351a(decodeResource);
            decodeResource.recycle();
            return false;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.j.d$e */
    /* compiled from: AvatarUtils */
    public interface C4219e {
        /* renamed from: a */
        void mo24351a(Bitmap bitmap);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m13397b(C4219e eVar, Bitmap bitmap, String str, String str2) {
        int i;
        int parseColor = Color.parseColor("#33CC00");
        int parseColor2 = Color.parseColor("#33FF00");
        if (MemberControl.m17125s().mo27187l().equals(str)) {
            i = Color.parseColor("#00ccff");
            parseColor = Color.parseColor("#3399ff");
            parseColor2 = Color.parseColor("#33ccff");
        } else {
            i = -1;
        }
        Bitmap a = m13389a(str2, i, parseColor, parseColor2, bitmap);
        if (eVar != null && a != null) {
            eVar.mo24351a(a);
        }
    }

    /* renamed from: a */
    public static void m13390a(Context context, int i, UserInfoDb fVar, C4219e eVar) {
        if (i <= 0) {
            i = 30;
        }
        int a = ConvertUtils.m1055a((float) i);
        RequestBuilder<Bitmap> d = Glide.m1282e(context).mo9950d();
        d.mo9935a(fVar.mo24336v());
        RequestBuilder jVar = (RequestBuilder) ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) d.mo10593b()).mo10594b(a)).mo10598c(R$mipmap.login_user)).mo10580a(R$mipmap.login_user);
        jVar.mo9939b((RequestListener) new C4216b(a, context, eVar, fVar));
        jVar.mo9936a((Target) new C4215a(a, eVar, fVar));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static Bitmap m13396b(int i, int i2, Bitmap bitmap) {
        if (i < 0 || i > 255) {
            i = 255;
        }
        int width = bitmap.getWidth();
        int i3 = width + 8;
        Bitmap createBitmap = Bitmap.createBitmap(i3, i3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setDither(true);
        float f = (float) 2;
        paint.setStrokeWidth(f);
        paint.setColor(-1);
        canvas.drawCircle(((float) createBitmap.getWidth()) / 2.0f, ((float) createBitmap.getWidth()) / 2.0f, ((float) (width + 2)) / 2.0f, paint);
        paint.setAlpha(i);
        float f2 = (float) 4;
        canvas.drawBitmap(bitmap, f2, f2, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(i2);
        canvas.drawCircle(((float) createBitmap.getWidth()) / 2.0f, ((float) createBitmap.getWidth()) / 2.0f, (((float) width) / 2.0f) + (f * 1.5f), paint);
        return createBitmap;
    }

    /* renamed from: a */
    public static void m13392a(Context context, String str, int i, C4219e eVar) {
        m13391a(context, str, 30, i, 255, eVar);
    }

    /* renamed from: a */
    public static void m13391a(Context context, String str, int i, int i2, int i3, C4219e eVar) {
        if (i <= 0) {
            i = 20;
        }
        int a = ConvertUtils.m1055a((float) i);
        RequestBuilder<Bitmap> d = Glide.m1282e(context).mo9950d();
        d.mo9935a(str);
        RequestBuilder jVar = (RequestBuilder) ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) d.mo10593b()).mo10594b(a)).mo10598c(R$mipmap.login_user)).mo10580a(R$mipmap.login_user);
        jVar.mo9939b((RequestListener) new C4218d(a, context, eVar));
        jVar.mo9936a((Target) new C4217c(i3, i2, eVar, a));
    }

    /* renamed from: a */
    public static Bitmap m13389a(String str, int i, int i2, int i3, Bitmap bitmap) {
        String str2 = str;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int a = ConvertUtils.m1055a(2.0f);
        int a2 = ConvertUtils.m1055a(4.0f);
        int a3 = ConvertUtils.m1055a(6.0f);
        int a4 = ConvertUtils.m1055a(1.0f);
        TextPaint textPaint = new TextPaint(1);
        textPaint.setColor(-1);
        textPaint.setDither(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize((float) ConvertUtils.m1055a(12.0f));
        Paint.FontMetricsInt fontMetricsInt = textPaint.getFontMetricsInt();
        int i4 = fontMetricsInt.bottom - fontMetricsInt.top;
        Rect rect = new Rect();
        textPaint.getTextBounds(str2, 0, str.length(), rect);
        int i5 = rect.right - rect.left;
        int max = Math.max(width, i5) + (a3 * 2);
        int i6 = height + a2;
        int i7 = i4 + i6 + (a4 * 2);
        Bitmap createBitmap = Bitmap.createBitmap(max, i7, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStyle(Paint.Style.FILL);
        float f = (float) a;
        paint.setStrokeWidth(f);
        float f2 = f / 4.0f;
        float f3 = ((float) i6) + f2;
        float f4 = (float) max;
        RectF rectF = new RectF(f2, f3, f4 - f2, ((float) i7) - f2);
        float f5 = f3 + ((rectF.bottom - rectF.top) / 2.0f) + ((float) fontMetricsInt.bottom);
        paint.setColor(i2);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(rectF, 5.0f, 5.0f, paint);
        float f6 = f / 2.0f;
        paint.setStrokeWidth(f6);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(i3);
        canvas.drawRoundRect(rectF, 5.0f, 5.0f, paint);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText(str2, ((float) (max - i5)) / 2.0f, f5, textPaint);
        canvas.drawBitmap(bitmap, ((float) (max - width)) / 2.0f, f6, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(f);
        paint.setColor(i);
        float f7 = ((float) width) / 2.0f;
        canvas.drawCircle(f4 / 2.0f, f6 + f7, f7, paint);
        return createBitmap;
    }

    /* renamed from: a */
    public static void m13393a(ImageView imageView, String str, int i, boolean z) {
        m13394a(imageView, str, i, z, 0);
    }

    /* JADX WARN: Type inference failed for: r4v3, types: [com.bumptech.glide.q.a] */
    /* JADX WARN: Type inference failed for: r4v4, types: [com.bumptech.glide.q.a] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m13394a(android.widget.ImageView r1, java.lang.String r2, int r3, boolean r4, int r5) {
        /*
            com.bumptech.glide.q.h r0 = new com.bumptech.glide.q.h
            r0.<init>()
            com.bumptech.glide.q.a r0 = r0.mo10598c(r3)
            com.bumptech.glide.q.h r0 = (com.bumptech.glide.p040q.RequestOptions) r0
            com.bumptech.glide.q.a r0 = r0.mo10580a(r3)
            com.bumptech.glide.q.h r0 = (com.bumptech.glide.p040q.RequestOptions) r0
            if (r4 == 0) goto L_0x001a
            com.bumptech.glide.q.a r4 = r0.mo10593b()
            r0 = r4
            com.bumptech.glide.q.h r0 = (com.bumptech.glide.p040q.RequestOptions) r0
        L_0x001a:
            if (r5 <= 0) goto L_0x0023
            com.bumptech.glide.q.a r4 = r0.mo10594b(r5)
            r0 = r4
            com.bumptech.glide.q.h r0 = (com.bumptech.glide.p040q.RequestOptions) r0
        L_0x0023:
            com.bumptech.glide.k r4 = com.bumptech.glide.Glide.m1275a(r1)
            com.bumptech.glide.j r4 = r4.mo9950d()
            r4.mo9935a(r2)
            com.bumptech.glide.q.a r2 = r4.mo10598c(r3)
            com.bumptech.glide.j r2 = (com.bumptech.glide.RequestBuilder) r2
            com.bumptech.glide.q.a r2 = r2.mo10580a(r3)
            com.bumptech.glide.j r2 = (com.bumptech.glide.RequestBuilder) r2
            com.bumptech.glide.j r2 = r2.mo9932a(r0)
            r2.mo9938a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p199j.AvatarUtils.m13394a(android.widget.ImageView, java.lang.String, int, boolean, int):void");
    }

    /* renamed from: a */
    public static Bitmap m13388a(Context context, String str) {
        try {
            RequestBuilder jVar = (RequestBuilder) Glide.m1282e(context).mo9950d().mo10585a((Transformation<Bitmap>) new GlideRoundImage(context, 5));
            jVar.mo9935a(str);
            return (Bitmap) jVar.mo9931I().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
