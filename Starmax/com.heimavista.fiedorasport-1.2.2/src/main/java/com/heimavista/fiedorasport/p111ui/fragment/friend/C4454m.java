package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.animation.ValueAnimator;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.m */
/* compiled from: lambda */
public final /* synthetic */ class C4454m implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: P */
    private final /* synthetic */ BuddyLocFragment f8779P;

    /* renamed from: Q */
    private final /* synthetic */ ValueAnimator f8780Q;

    public /* synthetic */ C4454m(BuddyLocFragment buddyLocFragment, ValueAnimator valueAnimator) {
        this.f8779P = buddyLocFragment;
        this.f8780Q = valueAnimator;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f8779P.mo24689a(this.f8780Q, valueAnimator);
    }
}
