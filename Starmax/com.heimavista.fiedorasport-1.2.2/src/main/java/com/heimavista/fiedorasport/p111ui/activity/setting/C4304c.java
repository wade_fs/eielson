package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.widget.RadioGroup;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.c */
/* compiled from: lambda */
public final /* synthetic */ class C4304c implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a */
    private final /* synthetic */ EditWatchFaceActivity f8297a;

    public /* synthetic */ C4304c(EditWatchFaceActivity editWatchFaceActivity) {
        this.f8297a = editWatchFaceActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.f8297a.mo24470a(radioGroup, i);
    }
}
