package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.band.TickType;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.DeviceManagerActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.ScanActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingAlarmRemindActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingNotificationActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingPowerActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingSedentaryRemindActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingTargetStepActivity;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingWatchFacesActivity;
import com.heimavista.fiedorasport.p111ui.activity.user.FeedbackActivity;
import com.heimavista.fiedorasport.p111ui.activity.user.UserProfileActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListMenuAdapter;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.widget.BatteryView;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.xutils.C5217x;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment */
public class MainMyFragment extends BaseFragment<HomeActivity> {

    /* renamed from: W */
    private ImageView f8839W;

    /* renamed from: X */
    private TextView f8840X;

    /* renamed from: Y */
    private ImageView f8841Y;

    /* renamed from: Z */
    private Button f8842Z;

    /* renamed from: a0 */
    private View f8843a0;

    /* renamed from: b0 */
    private ImageView f8844b0;

    /* renamed from: c0 */
    private TextView f8845c0;

    /* renamed from: d0 */
    private ImageView f8846d0;

    /* renamed from: e0 */
    private TextView f8847e0;

    /* renamed from: f0 */
    private BatteryView f8848f0;

    /* renamed from: g0 */
    private TextView f8849g0;

    /* renamed from: h0 */
    private TextView f8850h0;

    /* renamed from: i0 */
    private ImageView f8851i0;

    /* renamed from: j0 */
    private RecyclerView f8852j0;

    /* renamed from: k0 */
    private ListMenuAdapter f8853k0;

    /* renamed from: l0 */
    private List<MenuItem> f8854l0;

    /* renamed from: m0 */
    private SimpleDateFormat f8855m0 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public boolean f8856n0 = true;

    /* renamed from: o0 */
    private RotateAnimation f8857o0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment$b */
    class C4473b implements OnResultListener<ParamJsonData> {

        /* renamed from: a */
        final /* synthetic */ int f8862a;

        /* renamed from: b */
        final /* synthetic */ boolean f8863b;

        C4473b(MainMyFragment mainMyFragment, int i, boolean z) {
            this.f8862a = i;
            this.f8863b = z;
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            RemindMsgInfo e = BandDataManager.m10563e();
            if (this.f8862a == 1) {
                e.mo22604d(this.f8863b);
            } else {
                e.mo22611g(this.f8863b);
            }
            BandDataManager.m10551a(e);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            RemindMsgInfo e = BandDataManager.m10563e();
            if (this.f8862a == 1) {
                e.mo22604d(!this.f8863b);
            } else {
                e.mo22611g(!this.f8863b);
            }
            BandDataManager.m10551a(e);
        }
    }

    /* renamed from: i */
    private void m14587i() {
        ImageView imageView = this.f8851i0;
        if (imageView != null) {
            imageView.clearAnimation();
            RotateAnimation rotateAnimation = this.f8857o0;
            if (rotateAnimation != null) {
                rotateAnimation.cancel();
                this.f8857o0 = null;
            }
            m14593o();
        }
    }

    /* renamed from: j */
    private List<MenuItem> m14588j() {
        List<MenuItem> list = this.f8854l0;
        if (list == null) {
            this.f8854l0 = new ArrayList();
        } else {
            list.clear();
        }
        if (FSManager.m10323r().mo22847q()) {
            if (JYSDKManager.m10388i().mo22897e()) {
                m14585a(this.f8854l0);
            }
        } else if (FSManager.m10323r().mo22846p() && CRPBleManager.m10639j()) {
            m14585a(this.f8854l0);
        }
        List<MenuItem> list2 = this.f8854l0;
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(1);
        bVar.mo22664b(getString(R$string.feedback));
        list2.add(bVar.mo22661a());
        List<MenuItem> list3 = this.f8854l0;
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(1);
        bVar2.mo22664b(getString(R$string.version));
        bVar2.mo22659a(AppUtils.m946c());
        bVar2.mo22660a(false);
        bVar2.mo22662b();
        list3.add(bVar2.mo22661a());
        return this.f8854l0;
    }

    /* renamed from: k */
    private void m14589k() {
        if (!TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            Constant.m10311a("點擊我的設備");
            mo22759a(DeviceManagerActivity.class);
            return;
        }
        mo22759a(ScanActivity.class);
    }

    /* renamed from: l */
    private void m14590l() {
        List<MenuItem> j = m14588j();
        ListMenuAdapter listMenuAdapter = this.f8853k0;
        if (listMenuAdapter == null) {
            this.f8853k0 = new ListMenuAdapter(mo22771c());
            this.f8853k0.mo24646a((ListMenuAdapter.C4405a) new C4480g(this));
            this.f8853k0.mo22792b((List) j);
            this.f8853k0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4481h(this));
            this.f8852j0.setNestedScrollingEnabled(false);
            this.f8852j0.setAdapter(this.f8853k0);
            return;
        }
        listMenuAdapter.mo22792b((List) j);
    }

    /* renamed from: m */
    private void m14591m() {
        if (BandConfig.m10064m().mo22561f() && !BandConfig.m10064m().mo22558c().contains("BAND-2006")) {
            getChildFragmentManager().beginTransaction().replace(R$id.gad, HvGad.m14915i().mo25016f().mo25037b("BAND-2006", new HvGadViewConfig.C4502b(ScreenUtils.m1265c(), -2).mo24895a()), "ad_user").commit();
        }
    }

    /* renamed from: n */
    public static MainMyFragment m14592n() {
        Bundle bundle = new Bundle();
        MainMyFragment mainMyFragment = new MainMyFragment();
        mainMyFragment.setArguments(bundle);
        return mainMyFragment;
    }

    /* renamed from: o */
    private void m14593o() {
        if (this.f8850h0 != null) {
            long a = TickManager.m10517a(TickType.NONE);
            if (a > 0) {
                this.f8850h0.setText(this.f8855m0.format(new Date(a * 1000)));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: p */
    public void m14594p() {
        if (this.f8856n0) {
            mo22766a(false);
            C5217x.task().postDelayed(new C4482i(this), 500);
        }
    }

    /* renamed from: q */
    private void m14595q() {
        if (this.f8851i0 != null && this.f8857o0 == null) {
            this.f8857o0 = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
            this.f8857o0.setDuration(1000);
            this.f8857o0.setFillAfter(true);
            this.f8857o0.setRepeatMode(1);
            this.f8857o0.setInterpolator(new LinearInterpolator());
            this.f8857o0.setRepeatCount(-1);
            this.f8851i0.startAnimation(this.f8857o0);
        }
    }

    /* renamed from: r */
    private void m14596r() {
        boolean z = false;
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            this.f8842Z.setVisibility(0);
            this.f8841Y.setVisibility(8);
            this.f8843a0.setVisibility(8);
        } else {
            this.f8842Z.setVisibility(8);
            this.f8841Y.setVisibility(0);
            this.f8843a0.setVisibility(0);
            this.f8845c0.setText(FSManager.m10323r().mo22839i());
            this.f8847e0.setTextColor(getResources().getColor(R$color.font_color));
            if (FSManager.m10323r().mo22847q()) {
                z = JYSDKManager.m10388i().mo22897e();
            } else if (FSManager.m10323r().mo22846p()) {
                z = CRPBleManager.m10639j();
            }
            if (z) {
                m14580a(BandDataManager.m10545a());
                this.f8846d0.setImageResource(R$mipmap.icon_bt);
            } else {
                this.f8848f0.setVisibility(8);
                this.f8846d0.setImageResource(R$mipmap.icon_bt_dis);
            }
            this.f8847e0.setText(z ? R$string.device_has_connected : R$string.device_has_not_connect);
            m14593o();
        }
        m14590l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* renamed from: s */
    private void m14597s() {
        if (MemberControl.m17125s().mo27192q()) {
            String g = MemberControl.m17125s().mo27182g();
            if (TextUtils.isEmpty(g)) {
                g = MemberControl.m17125s().mo27185j();
            }
            this.f8840X.setText(g);
            AvatarUtils.m13393a(this.f8839W, MemberControl.m17125s().mo27183h(), R$mipmap.login_user, true);
            return;
        }
        this.f8840X.setText(R$string.login_first);
        this.f8839W.setImageResource(R$mipmap.login_user);
    }

    /* renamed from: t */
    private void m14598t() {
        if (MemberControl.m17125s().mo27192q()) {
            mo22759a(UserProfileActivity.class);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_main_my;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return new String[]{"fiedoraSport.ACTION_onDataSyncing", "fiedoraSport.ACTION_onDataSyncEnd", "fiedoraSport.ACTION_onBleStatusChange", "fiedoraSport.ACTION_onBleBatteryChange", HvApiBasic.ACTION_OTHER_BIND};
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        m14591m();
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.llUser) {
            Constant.m10311a("個人資料點擊");
            m14598t();
        } else if (id == R$id.ivArrow || id == R$id.layoutDetail) {
            m14589k();
        } else if (id == R$id.btnAddDevice) {
            Constant.m10311a("點擊添加設備");
            mo22759a(ScanActivity.class);
        } else if (id == R$id.layoutDevice) {
            Constant.m10311a("點擊刷新");
            HvApp.m13010c().mo24157a("fiedoraSport.ACTION_refreshDataFromServer");
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onResume() {
        super.onResume();
        HvGad.m14915i().mo25016f().mo25036a(this.f8844b0, R$mipmap.app_icon, 8);
        m14597s();
        m14596r();
        if (TextUtils.isEmpty(((HomeActivity) mo22771c()).mo23014v())) {
            m14587i();
        } else {
            m14595q();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment$a */
    class C4472a implements PermissionUtils.C0916b {

        /* renamed from: a */
        final /* synthetic */ int f8858a;

        /* renamed from: b */
        final /* synthetic */ boolean f8859b;

        /* renamed from: c */
        final /* synthetic */ MenuItem f8860c;

        C4472a(int i, boolean z, MenuItem eVar) {
            this.f8858a = i;
            this.f8859b = z;
            this.f8860c = eVar;
        }

        /* renamed from: a */
        public void mo9861a(List<String> list) {
            MainMyFragment.this.m14594p();
            MainMyFragment.this.m14578a(this.f8858a, this.f8859b).request(this.f8858a, this.f8859b);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment, int]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(int, boolean):com.heimavista.api.band.HvApiPostBandSettings
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(com.heimavista.entity.data.e, boolean):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.fragment.home.f]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainMyFragment, int, com.heimavista.entity.data.e, boolean):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo9862a(List<String> list, List<String> list2) {
            if (list.isEmpty()) {
                MainMyFragment.this.m14581a(this.f8858a, this.f8860c, this.f8859b);
                return;
            }
            boolean unused = MainMyFragment.this.f8856n0 = false;
            this.f8860c.mo22645a(false);
            MainMyFragment mainMyFragment = MainMyFragment.this;
            mainMyFragment.mo22763a(mainMyFragment.getString(R$string.permission_denied), MainMyFragment.this.getString(R$string.goto_open), false, (BaseDialog.C4621m) C4479f.f8872a);
        }

        /* renamed from: a */
        static /* synthetic */ void m14606a(boolean z) {
            if (z) {
                PermissionUtils.m1214h();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        ((NestedScrollView) view.findViewById(R$id.layoutContainer)).setPadding(0, BarUtils.m981a(), 0, 0);
        this.f8839W = (ImageView) view.findViewById(R$id.ivAvatar);
        this.f8840X = (TextView) view.findViewById(R$id.tvName);
        this.f8841Y = (ImageView) view.findViewById(R$id.ivArrow);
        this.f8842Z = (Button) view.findViewById(R$id.btnAddDevice);
        this.f8843a0 = view.findViewById(R$id.layoutDevice);
        this.f8844b0 = (ImageView) view.findViewById(R$id.ivProductLogo);
        ImageView imageView = (ImageView) view.findViewById(R$id.ivBracelet);
        this.f8845c0 = (TextView) view.findViewById(R$id.tvDeviceName);
        this.f8846d0 = (ImageView) view.findViewById(R$id.ivBle);
        this.f8847e0 = (TextView) view.findViewById(R$id.tvStatus);
        this.f8848f0 = (BatteryView) view.findViewById(R$id.ivBattery);
        this.f8849g0 = (TextView) view.findViewById(R$id.tvBattery);
        this.f8850h0 = (TextView) view.findViewById(R$id.tvSyncTime);
        this.f8851i0 = (ImageView) view.findViewById(R$id.ivSync);
        this.f8852j0 = (RecyclerView) view.findViewById(R$id.recyclerView);
        View view2 = this.f8843a0;
        mo22767a(this.f8841Y, view2, this.f8842Z, view2);
        mo22770b(R$id.llUser, R$id.layoutDetail);
    }

    /* renamed from: a */
    private void m14580a(int i) {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
            if (i > 0) {
                this.f8848f0.setVisibility(0);
                this.f8849g0.setVisibility(0);
                this.f8848f0.setPower(i);
                this.f8849g0.setText(i + "%");
                if (i < 20) {
                    TextView textView = this.f8849g0;
                    textView.setTextColor(textView.getContext().getResources().getColor(R$color.red));
                    return;
                }
                TextView textView2 = this.f8849g0;
                textView2.setTextColor(textView2.getContext().getResources().getColor(R$color.font_color));
                return;
            }
            this.f8848f0.setVisibility(8);
            this.f8849g0.setVisibility(8);
        }
    }

    /* renamed from: a */
    private void m14585a(List<MenuItem> list) {
        boolean z;
        boolean z2;
        if (Build.VERSION.SDK_INT >= 23) {
            z2 = PermissionUtils.m1193a("android.permission.READ_PHONE_STATE");
            z = PermissionUtils.m1193a("android.permission.RECEIVE_SMS");
        } else {
            z2 = true;
            z = true;
        }
        RemindMsgInfo e = BandDataManager.m10563e();
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(2);
        bVar.mo22657a(R$mipmap.ic_menu_phone);
        bVar.mo22664b(getString(R$string.remind_ring));
        boolean z3 = false;
        bVar.mo22660a(z2 && e.mo22605d());
        list.add(bVar.mo22661a());
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(2);
        bVar2.mo22657a(R$mipmap.ic_menu_msg);
        bVar2.mo22664b(getString(R$string.remind_msg));
        if (z && e.mo22612g()) {
            z3 = true;
        }
        bVar2.mo22660a(z3);
        list.add(bVar2.mo22661a());
        MenuItem.C3676b bVar3 = new MenuItem.C3676b();
        bVar3.mo22663b(2);
        bVar3.mo22657a(R$mipmap.ic_menu_heart);
        bVar3.mo22664b(getString(R$string.remind_auto_heart));
        bVar3.mo22660a(BandDataManager.m10555b("AutoHeart").mo22593g());
        list.add(bVar3.mo22661a());
        if (FSManager.m10323r().mo22847q()) {
            MenuItem.C3676b bVar4 = new MenuItem.C3676b();
            bVar4.mo22663b(2);
            bVar4.mo22657a(R$mipmap.ic_menu_band);
            bVar4.mo22664b(getString(R$string.remind_lost));
            bVar4.mo22660a(e.mo22599a());
            list.add(bVar4.mo22661a());
        }
        if (FSManager.m10323r().mo22846p()) {
            MenuItem.C3676b bVar5 = new MenuItem.C3676b();
            bVar5.mo22663b(1);
            bVar5.mo22657a(R$mipmap.ic_menu_band);
            bVar5.mo22664b(getString(R$string.setting_watch_face));
            list.add(bVar5.mo22661a());
        }
        MenuItem.C3676b bVar6 = new MenuItem.C3676b();
        bVar6.mo22663b(1);
        bVar6.mo22657a(R$mipmap.ic_menu_notification);
        bVar6.mo22664b(getString(R$string.remind_notification));
        list.add(bVar6.mo22661a());
        MenuItem.C3676b bVar7 = new MenuItem.C3676b();
        bVar7.mo22663b(1);
        bVar7.mo22657a(R$mipmap.ic_menu_alarm);
        bVar7.mo22664b(getString(R$string.remind_alarm));
        list.add(bVar7.mo22661a());
        MenuItem.C3676b bVar8 = new MenuItem.C3676b();
        bVar8.mo22663b(1);
        bVar8.mo22657a(R$mipmap.ic_menu_sedentary);
        bVar8.mo22664b(getString(R$string.remind_sedentary));
        bVar8.mo22662b();
        list.add(bVar8.mo22661a());
        list.add(new MenuItem.C3676b().mo22661a());
        MenuItem.C3676b bVar9 = new MenuItem.C3676b();
        bVar9.mo22663b(1);
        bVar9.mo22657a(R$mipmap.ic_menu_step);
        bVar9.mo22664b(getString(R$string.setting_target_step));
        list.add(bVar9.mo22661a());
        MenuItem.C3676b bVar10 = new MenuItem.C3676b();
        bVar10.mo22663b(1);
        bVar10.mo22657a(R$mipmap.ic_menu_power);
        bVar10.mo22664b(getString(R$string.setting_power));
        bVar10.mo22662b();
        list.add(bVar10.mo22661a());
        list.add(new MenuItem.C3676b().mo22661a());
    }

    /* renamed from: a */
    public /* synthetic */ void mo24718a(MenuItem eVar, boolean z) {
        this.f8856n0 = true;
        String d = eVar.mo22652d();
        if (d.equals(getString(R$string.remind_ring))) {
            m14584a("來電提醒", z);
            m14581a(1, eVar, z);
        } else if (d.equals(getString(R$string.remind_msg))) {
            m14584a("短信提醒", z);
            m14581a(2, eVar, z);
        } else if (d.equals(getString(R$string.remind_lost))) {
            m14584a("防丟提醒", z);
            BandDataManager.m10551a(BandDataManager.m10563e().mo22598a(z));
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22893c(z);
            }
        } else if (d.equals(getString(R$string.remind_auto_heart))) {
            m14584a("自動心率偵測", z);
            PeriodTimeInfo b = BandDataManager.m10555b("AutoHeart");
            b.mo22582a(z);
            BandDataManager.m10550a(b);
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22886a(z, 0, 0, 23, 55, 60, 2);
            }
            if (FSManager.m10323r().mo22846p()) {
                ((HomeActivity) mo22771c()).mo23011b(z);
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24717a(View view, MenuItem eVar, int i) {
        if (eVar.mo22653e() == 7) {
            return;
        }
        if (getString(R$string.feedback).equals(eVar.mo22652d())) {
            Constant.m10311a("意見反饋點擊");
            mo22759a(FeedbackActivity.class);
        } else if (eVar.mo22650c() == R$mipmap.ic_menu_notification) {
            Constant.m10311a("點擊消息提醒");
            mo22759a(SettingNotificationActivity.class);
        } else if (eVar.mo22650c() == R$mipmap.ic_menu_alarm) {
            Constant.m10311a("點擊鬧鐘提醒");
            mo22759a(SettingAlarmRemindActivity.class);
        } else if (eVar.mo22650c() == R$mipmap.ic_menu_sedentary) {
            Constant.m10311a("久坐提醒點擊");
            mo22759a(SettingSedentaryRemindActivity.class);
        } else if (eVar.mo22650c() == R$mipmap.ic_menu_band) {
            if (FSManager.m10323r().mo22846p()) {
                mo22759a(SettingWatchFacesActivity.class);
            }
        } else if (eVar.mo22650c() != R$mipmap.ic_menu_test) {
            if (eVar.mo22650c() == R$mipmap.ic_menu_step) {
                Constant.m10311a("目標設定點擊");
                startActivity(SettingTargetStepActivity.m13893b(true));
            } else if (eVar.mo22650c() == R$mipmap.ic_menu_power) {
                Constant.m10311a("省電設定點擊");
                mo22759a(SettingPowerActivity.class);
            } else {
                CommonListItemView commonListItemView = (CommonListItemView) view.findViewById(R$id.listItemView);
                if (commonListItemView != null && commonListItemView.getAccessoryType() == 2) {
                    commonListItemView.getSwitch().toggle();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14581a(int i, MenuItem eVar, boolean z) {
        if (z) {
            String[] strArr = new String[1];
            strArr[0] = i == 1 ? "android.permission-group.PHONE" : "android.permission-group.SMS";
            PermissionUtils b = PermissionUtils.m1196b(strArr);
            b.mo9856a(new C4472a(i, z, eVar));
            b.mo9859a();
            return;
        }
        m14594p();
        m14578a(i, z).request(i, z);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public HvApiPostBandSettings m14578a(int i, boolean z) {
        return new HvApiPostBandSettings(new C4473b(this, i, z));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        super.mo22754a(context, intent);
        String action = intent.getAction();
        if ("fiedoraSport.ACTION_onBleStatusChange".equals(action) || HvApiBasic.ACTION_OTHER_BIND.equals(action)) {
            m14596r();
        }
        if ("fiedoraSport.ACTION_onBleBatteryChange".equals(action)) {
            m14580a(intent.getIntExtra("battery", 0));
        }
        if ("fiedoraSport.ACTION_onDataSyncing".equals(action)) {
            m14595q();
        } else if ("fiedoraSport.ACTION_onDataSyncEnd".equals(action)) {
            m14587i();
        }
    }

    /* renamed from: a */
    private void m14584a(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(z ? "開啟" : "關閉");
        Constant.m10311a(sb.toString());
    }
}
