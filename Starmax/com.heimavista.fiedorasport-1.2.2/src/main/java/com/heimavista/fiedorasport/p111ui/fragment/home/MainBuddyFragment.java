package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.blankj.utilcode.util.BarUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.data.TabEntity;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddyDynamicFragment;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddyLocFragment;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddySportDynamicFragment;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainBuddyFragment */
public class MainBuddyFragment extends BaseFragment<HomeActivity> {
    /* access modifiers changed from: private */

    /* renamed from: W */
    public CommonTabLayout f8807W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public ViewPager2 f8808X;
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public List<BaseFragment> f8809Y;

    /* renamed from: Z */
    private BuddyDynamicFragment f8810Z;

    /* renamed from: a0 */
    private BuddyLocFragment f8811a0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainBuddyFragment$a */
    class C4467a implements OnTabSelectListener {
        C4467a() {
        }

        public void onTabReselect(int i) {
        }

        public void onTabSelect(int i) {
            MainBuddyFragment.this.f8808X.setCurrentItem(i, false);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainBuddyFragment$b */
    class C4468b extends ViewPager2.OnPageChangeCallback {
        C4468b() {
        }

        public void onPageSelected(int i) {
            super.onPageSelected(i);
            MainBuddyFragment.this.f8807W.setCurrentTab(i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainBuddyFragment$c */
    class C4469c extends FragmentStateAdapter {
        C4469c(FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @NonNull
        public Fragment createFragment(int i) {
            return (Fragment) MainBuddyFragment.this.f8809Y.get(i);
        }

        public int getItemCount() {
            return MainBuddyFragment.this.f8809Y.size();
        }
    }

    /* renamed from: i */
    public static MainBuddyFragment m14535i() {
        Bundle bundle = new Bundle();
        MainBuddyFragment mainBuddyFragment = new MainBuddyFragment();
        mainBuddyFragment.setArguments(bundle);
        return mainBuddyFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_main_buddy;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        this.f8809Y = new ArrayList();
        ArrayList arrayList = new ArrayList();
        if (BandConfig.m10064m().mo22565j()) {
            this.f8810Z = BuddyDynamicFragment.m14392l();
            this.f8809Y.add(this.f8810Z);
            arrayList.add(new TabEntity(getString(R$string.friends), R$mipmap.icon_friend_title02, R$mipmap.icon_friend_title01));
        }
        if (BandConfig.m10064m().mo22564i()) {
            this.f8809Y.add(BuddySportDynamicFragment.m14492j());
            arrayList.add(new TabEntity(getString(R$string.motion), R$mipmap.icon_friend_sport02, R$mipmap.icon_friend_sport01));
        }
        if (BandConfig.m10064m().mo22560e()) {
            this.f8811a0 = BuddyLocFragment.m14444k();
            this.f8809Y.add(this.f8811a0);
            arrayList.add(new TabEntity(getString(R$string.location), R$mipmap.icon_local_title02, R$mipmap.icon_local_title01));
        }
        this.f8807W.setTabData(arrayList);
        this.f8807W.setOnTabSelectListener(new C4467a());
        this.f8808X.registerOnPageChangeCallback(new C4468b());
        this.f8808X.setAdapter(new C4469c(mo22771c()));
    }

    public void onPause() {
        BuddyDynamicFragment buddyDynamicFragment = this.f8810Z;
        if (buddyDynamicFragment != null) {
            buddyDynamicFragment.mo24681j();
        }
        BuddyLocFragment buddyLocFragment = this.f8811a0;
        if (buddyLocFragment != null) {
            buddyLocFragment.mo24680i();
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        int currentItem = this.f8808X.getCurrentItem();
        if (this.f8809Y != null) {
            BuddyDynamicFragment buddyDynamicFragment = this.f8810Z;
            if (buddyDynamicFragment != null && currentItem == 0) {
                buddyDynamicFragment.mo24680i();
            }
            if (this.f8811a0 != null && currentItem == this.f8809Y.size() - 1) {
                this.f8811a0.mo24688a(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        ((LinearLayout) view.findViewById(R$id.layoutContainer)).setPadding(0, BarUtils.m981a(), 0, 0);
        this.f8807W = (CommonTabLayout) view.findViewById(R$id.commonTabLayout);
        this.f8808X = (ViewPager2) view.findViewById(R$id.viewpager);
        this.f8808X.setOrientation(0);
        this.f8808X.setUserInputEnabled(false);
    }
}
