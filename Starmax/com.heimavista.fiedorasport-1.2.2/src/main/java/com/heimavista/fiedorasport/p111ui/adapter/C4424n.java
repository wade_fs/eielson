package com.heimavista.fiedorasport.p111ui.adapter;

import com.heimavista.entity.data.MenuItem;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.n */
/* compiled from: lambda */
public final /* synthetic */ class C4424n implements SwitchButton.C4603d {

    /* renamed from: a */
    private final /* synthetic */ ListMenuAdapter f8688a;

    /* renamed from: b */
    private final /* synthetic */ MenuItem f8689b;

    public /* synthetic */ C4424n(ListMenuAdapter listMenuAdapter, MenuItem eVar) {
        this.f8688a = listMenuAdapter;
        this.f8689b = eVar;
    }

    /* renamed from: a */
    public final void mo24507a(SwitchButton switchButton, boolean z) {
        this.f8688a.mo24645a(this.f8689b, switchButton, z);
    }
}
