package com.heimavista.fiedorasport.p111ui.activity.user;

import android.view.View;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.a */
/* compiled from: lambda */
public final /* synthetic */ class C4388a implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ BindSocialActivity f8554P;

    public /* synthetic */ C4388a(BindSocialActivity bindSocialActivity) {
        this.f8554P = bindSocialActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8554P.mo24575a(view, (MenuItem) obj, i);
    }
}
