package com.heimavista.fiedorasport.base;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.base.BaseListViewAdapter.C3692a;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.base.e */
public abstract class BaseListViewAdapter<T, VH extends C3692a> extends BaseAdapter {

    /* renamed from: P */
    private List<T> f6452P;

    /* renamed from: com.heimavista.fiedorasport.base.e$a */
    /* compiled from: BaseListViewAdapter */
    public class C3692a {

        /* renamed from: a */
        private final View f6453a;

        /* renamed from: b */
        private SparseArray<WeakReference<View>> f6454b;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public C3692a(BaseListViewAdapter eVar, ViewGroup viewGroup, int i) {
            this(eVar, LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false));
        }

        /* renamed from: a */
        public final View mo22816a() {
            return this.f6453a;
        }

        public C3692a(BaseListViewAdapter eVar, View view) {
            this.f6454b = new SparseArray<>();
            this.f6453a = view;
        }

        /* renamed from: a */
        public final <V extends View> V mo22817a(@IdRes int i) {
            WeakReference weakReference = this.f6454b.get(i);
            if (weakReference != null && weakReference.get() != null) {
                return (View) weakReference.get();
            }
            V findViewById = this.f6453a.findViewById(i);
            this.f6454b.put(i, new WeakReference(findViewById));
            return findViewById;
        }
    }

    public BaseListViewAdapter(Context context) {
    }

    /* renamed from: a */
    public int mo22808a() {
        List<T> list = this.f6452P;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @NonNull
    /* renamed from: a */
    public abstract VH mo22809a(@NonNull ViewGroup viewGroup, int i);

    /* renamed from: a */
    public abstract void mo22810a(@NonNull VH vh, int i);

    public int getCount() {
        return mo22808a();
    }

    public T getItem(int i) {
        return this.f6452P.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        C3692a aVar;
        if (view == null) {
            aVar = mo22809a(viewGroup, getItemViewType(i));
            aVar.mo22816a().setTag(aVar);
        } else {
            aVar = (C3692a) view.getTag();
        }
        mo22810a(aVar, i);
        return aVar.mo22816a();
    }

    /* renamed from: a */
    public void mo22811a(List<T> list) {
        this.f6452P = list;
        notifyDataSetChanged();
    }
}
