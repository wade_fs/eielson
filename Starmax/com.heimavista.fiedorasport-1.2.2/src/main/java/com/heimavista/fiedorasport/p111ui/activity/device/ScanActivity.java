package com.heimavista.fiedorasport.p111ui.activity.device;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiBind;
import com.heimavista.api.band.HvApiGetConfig;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.data.BleDeviceItem;
import com.heimavista.entity.data.ComparatorBleDeviceItem;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$anim;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.service.JYBleService;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.OnGadStartListener;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.xutils.C5217x;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity */
public class ScanActivity extends BaseActivity {

    /* renamed from: A0 */
    private HttpCancelable f6678A0;

    /* renamed from: B0 */
    private ThreadUtils.C0877e<Void> f6679B0;
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public String f6680b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public String f6681c0;

    /* renamed from: d0 */
    private TextView f6682d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public ImageView f6683e0;
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public ImageView f6684f0;

    /* renamed from: g0 */
    private ImageView f6685g0;

    /* renamed from: h0 */
    private TextView f6686h0;
    /* access modifiers changed from: private */

    /* renamed from: i0 */
    public LinearLayout f6687i0;
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public View f6688j0;
    /* access modifiers changed from: private */

    /* renamed from: k0 */
    public ArrayList<BleDeviceItem> f6689k0;
    /* access modifiers changed from: private */

    /* renamed from: l0 */
    public ArrayAdapter f6690l0;

    /* renamed from: m0 */
    private Animation f6691m0;

    /* renamed from: n0 */
    private Animation f6692n0;

    /* renamed from: o0 */
    private AlphaAnimation f6693o0;
    /* access modifiers changed from: private */

    /* renamed from: p0 */
    public boolean f6694p0 = false;

    /* renamed from: q0 */
    private Runnable f6695q0 = new C3783i(this);

    /* renamed from: r0 */
    private Runnable f6696r0 = new C3784j(this);
    /* access modifiers changed from: private */

    /* renamed from: s0 */
    public ArrayList<String> f6697s0;
    /* access modifiers changed from: private */

    /* renamed from: t0 */
    public boolean f6698t0;

    /* renamed from: u0 */
    private boolean f6699u0 = false;
    /* access modifiers changed from: private */

    /* renamed from: v0 */
    public CountDownTimer f6700v0;
    /* access modifiers changed from: private */

    /* renamed from: w0 */
    public int f6701w0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: x0 */
    public boolean f6702x0;

    /* renamed from: y0 */
    private int f6703y0 = 850;

    /* renamed from: z0 */
    private BroadcastReceiver f6704z0 = new C3767a();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$a */
    class C3767a extends BroadcastReceiver {
        C3767a() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction()) && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", 0) == 12) {
                ScanActivity.this.m10864v();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$b */
    class C3768b implements Animation.AnimationListener {

        /* renamed from: a */
        final /* synthetic */ int f6706a;

        C3768b(int i) {
            this.f6706a = i;
        }

        public void onAnimationEnd(Animation animation) {
            int i = this.f6706a;
            if (i == 0) {
                ScanActivity.this.m10868z();
            } else if (i == 1) {
                ScanActivity.this.f6683e0.setVisibility(8);
            } else if (i == 2) {
                ScanActivity.this.f6684f0.setVisibility(8);
            }
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$c */
    class C3769c implements OnGadStartListener {
        C3769c() {
        }

        /* renamed from: a */
        public /* synthetic */ void mo23045a(boolean z) {
            ScanActivity.this.m10829E();
        }

        public void onCancel() {
            boolean unused = ScanActivity.this.f6702x0 = false;
            ScanActivity.this.m10867y();
            ScanActivity.this.m10829E();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, int, com.heimavista.fiedorasport.ui.activity.device.d]
         candidates:
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(java.lang.String, java.lang.String, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int, long):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, java.lang.String):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, android.os.Bundle, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, int):void
          com.heimavista.fiedorasport.h.y.a(int, int, long):void
          com.heimavista.fiedorasport.h.y.a(long, int, int):void
          com.heimavista.fiedorasport.h.y.a(java.lang.String, java.lang.String, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        public void onError(String str) {
            ScanActivity.this.m10867y();
            boolean unused = ScanActivity.this.f6702x0 = false;
            if (ScanActivity.this.f6701w0 < 5) {
                ScanActivity.this.m10828D();
                return;
            }
            ScanActivity.this.m10865w();
            int unused2 = ScanActivity.this.f6701w0 = 0;
            ScanActivity scanActivity = ScanActivity.this;
            scanActivity.mo22710a(scanActivity.getString(R$string.err_invitation_code_msg), true, (BaseDialog.C4621m) new C3778d(this));
        }

        public void onSuccess() {
            Constant.m10311a("輸入邀請碼");
            ScanActivity.this.m10839b(false);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$d */
    class C3770d implements OnResultListener<ParamJsonData> {
        C3770d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, com.heimavista.fiedorasport.ui.activity.device.f]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            if (fVar.mo25331a("bindUserName")) {
                String a = fVar.mo25325a("bindUserName", "");
                if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
                    ScanActivity scanActivity = ScanActivity.this;
                    scanActivity.mo22707a(scanActivity.getString(R$string.warn), ScanActivity.this.getString(R$string.force_bind_tip, new Object[]{a}), ScanActivity.this.getString(17039379), false, false, (BaseDialog.C4621m) new C3780f(this));
                    return;
                }
                ScanActivity.this.m10866x();
                return;
            }
            ScanActivity.this.m10866x();
        }

        /* renamed from: a */
        public /* synthetic */ void mo23047a(boolean z) {
            if (z) {
                ScanActivity.this.m10839b(true);
            } else {
                ScanActivity.this.m10829E();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            ScanActivity.this.mo22730c(str);
            ScanActivity.this.m10829E();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$f */
    class C3772f extends ThreadUtils.C0877e<Void> {
        C3772f() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: b */
        public Void mo9798b() {
            return null;
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(Void voidR) {
            if (!ScanActivity.this.f6694p0) {
                ScanActivity.this.m10866x();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$g */
    class C3773g implements OnResultListener<ParamJsonData> {
        C3773g() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            ArrayList unused = ScanActivity.this.f6697s0 = BandConfig.m10064m().mo22557b();
            ScanActivity.this.m10842c(true);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$h */
    class C3774h extends CountDownTimer {
        C3774h(long j, long j2) {
            super(j, j2);
        }

        /* renamed from: a */
        public /* synthetic */ void mo23050a(boolean z) {
            if (z) {
                ScanActivity.this.m10842c(true);
            } else {
                ScanActivity.this.m10868z();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, com.heimavista.fiedorasport.ui.activity.device.g]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        public void onFinish() {
            JYSDKManager.m10388i().mo22891b(false);
            if (ScanActivity.this.f6689k0 == null || ScanActivity.this.f6689k0.isEmpty()) {
                if (!ScanActivity.this.isFinishing()) {
                    ScanActivity scanActivity = ScanActivity.this;
                    scanActivity.mo22707a(scanActivity.getString(R$string.warn), ScanActivity.this.getString(R$string.ble_empty_result), ScanActivity.this.getString(R$string.scan_device), false, false, (BaseDialog.C4621m) new C3781g(this));
                }
            } else if (TextUtils.isEmpty(FSManager.m10323r().mo22836f()) || TextUtils.isEmpty(ScanActivity.this.f6680b0) || TextUtils.isEmpty(ScanActivity.this.f6681c0)) {
                if (ScanActivity.this.f6687i0.getVisibility() == 8) {
                    ScanActivity.this.f6688j0.setVisibility(0);
                    ScanActivity.this.f6687i0.setVisibility(0);
                }
                ScanActivity.this.f6690l0.notifyDataSetChanged();
            } else {
                ScanActivity.this.m10839b(false);
            }
            CountDownTimer unused = ScanActivity.this.f6700v0 = (CountDownTimer) null;
        }

        public void onTick(long j) {
        }
    }

    /* renamed from: A */
    private void m10825A() {
        ListView listView = (ListView) findViewById(R$id.listView);
        this.f6687i0.getLayoutParams().width = (int) (((double) ScreenUtils.m1265c()) * 0.7d);
        this.f6687i0.getLayoutParams().height = ScreenUtils.m1264b() / 3;
        this.f6689k0 = new ArrayList<>();
        this.f6690l0 = new ArrayAdapter(this, R$layout.device_listitem_text, 16908308, this.f6689k0);
        listView.setAdapter((ListAdapter) this.f6690l0);
        listView.setOnItemClickListener(new C3782h(this));
    }

    /* renamed from: B */
    private void m10826B() {
        if (!ServiceUtils.m1268a(JYBleService.class)) {
            ServiceUtils.m1270b(JYBleService.class);
        }
        JYSDKManager.m10388i().mo22896e(true);
        JYSDKManager.m10388i().mo22880a(ScanActivity.class.getCanonicalName(), this);
        CRPBleManager.m10606a(ScanActivity.class.getCanonicalName(), this);
        CRPBleManager.m10645p();
        m10867y();
    }

    /* renamed from: C */
    private void m10827C() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        registerReceiver(this.f6704z0, intentFilter);
    }

    /* access modifiers changed from: private */
    /* renamed from: D */
    public void m10828D() {
        C3769c cVar;
        if (TextUtils.isEmpty(FSManager.m10323r().mo22842l())) {
            cVar = new C3769c();
        } else {
            this.f6681c0 = FSManager.m10323r().mo22842l();
            cVar = null;
        }
        if (!TextUtils.isEmpty(this.f6681c0)) {
            this.f6701w0++;
            HvGad.m14915i().mo25004a(this, this.f6681c0, cVar);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public void m10829E() {
        m10865w();
        ArrayList<String> arrayList = this.f6697s0;
        if (arrayList == null || arrayList.isEmpty()) {
            new HvApiGetConfig().request(new C3773g());
        } else {
            m10842c(true);
        }
        m10830F();
    }

    /* access modifiers changed from: private */
    /* renamed from: F */
    public void m10830F() {
        this.f6683e0.setVisibility(0);
        this.f6683e0.setAnimation(this.f6691m0);
        this.f6683e0.getAnimation().start();
        if (!this.f6694p0) {
            this.f6683e0.postDelayed(this.f6695q0, (long) this.f6703y0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: G */
    public void m10831G() {
        this.f6684f0.setVisibility(0);
        this.f6684f0.setAnimation(this.f6692n0);
        this.f6684f0.getAnimation().start();
        if (!this.f6694p0) {
            this.f6684f0.postDelayed(this.f6696r0, (long) this.f6703y0);
        }
    }

    /* renamed from: u */
    private void m10863u() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter == null) {
            mo22730c(getString(R$string.ble_disabled));
        } else if (defaultAdapter.isEnabled()) {
            m10864v();
        } else {
            defaultAdapter.enable();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v */
    public void m10864v() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.LOCATION");
        b.mo9856a(new C3771e());
        b.mo9859a();
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public void m10865w() {
        this.f6683e0.removeCallbacks(this.f6695q0);
        this.f6684f0.removeCallbacks(this.f6696r0);
        this.f6684f0.clearAnimation();
        this.f6683e0.clearAnimation();
        this.f6684f0.setVisibility(8);
        this.f6683e0.setVisibility(8);
    }

    /* access modifiers changed from: private */
    /* renamed from: x */
    public void m10866x() {
        m10865w();
        String str = this.f6680b0;
        if (str != null) {
            if (str.startsWith("F15")) {
                CRPBleManager.m10615b(this.f6681c0);
            } else {
                JYSDKManager.m10388i().mo22882a(this.f6680b0, this.f6681c0, ScanActivity.class.getSimpleName());
            }
            if (this.f6679B0 == null) {
                this.f6679B0 = new C3772f();
            }
            ThreadUtils.m960b(this.f6679B0, 8, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: y */
    public void m10867y() {
        JYSDKManager.m10388i().mo22884a(true);
        CRPBleManager.m10629e();
    }

    /* access modifiers changed from: private */
    /* renamed from: z */
    public void m10868z() {
        this.f6688j0.setVisibility(8);
        this.f6687i0.setVisibility(8);
        if (!FSManager.m10323r().mo22847q()) {
            ServiceUtils.m1271c(JYBleService.class);
        }
        if (!FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10647r();
        }
        mo22717b();
        mo22721b(HomeActivity.class);
    }

    public void onBackPressed() {
        m10842c(false);
        m10868z();
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.tvSkip) {
            m10868z();
        } else if (view.getId() == R$id.ivClose) {
            this.f6689k0.clear();
            this.f6690l0.notifyDataSetChanged();
            m10865w();
            m10868z();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        JYSDKManager.m10388i().mo22896e(false);
        m10842c(false);
        this.f6683e0.removeCallbacks(this.f6695q0);
        this.f6684f0.removeCallbacks(this.f6696r0);
        this.f6692n0 = null;
        this.f6691m0 = null;
        BroadcastReceiver broadcastReceiver = this.f6704z0;
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f6699u0) {
            this.f6699u0 = false;
            m10864v();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_scan;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public List<String> mo22735g() {
        m10827C();
        return super.mo22735g();
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m10842c(boolean z) {
        JYSDKManager.m10388i().mo22891b(z);
        if (z) {
            CRPBleManager.m10646q();
        } else {
            CRPBleManager.m10619c();
        }
        if (z && this.f6700v0 == null) {
            this.f6700v0 = new C3774h(12000, 1000);
        }
        CountDownTimer countDownTimer = this.f6700v0;
        if (countDownTimer != null) {
            countDownTimer.start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.device.e]
     candidates:
      com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(android.widget.AdapterView, android.view.View, int, long):void
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m10845d(boolean z) {
        mo22708a(getString(z ? R$string.ble_warn_gps : R$string.location_permission_denied), getString(R$string.goto_open), false, (BaseDialog.C4621m) new C3779e(this, z));
    }

    /* renamed from: f */
    private Animation.AnimationListener m10848f(int i) {
        return new C3768b(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        m10826B();
        this.f6682d0 = (TextView) findViewById(R$id.tvLinkTips);
        this.f6683e0 = (ImageView) findViewById(R$id.ivRing);
        this.f6684f0 = (ImageView) findViewById(R$id.ivRing2);
        this.f6685g0 = (ImageView) findViewById(R$id.ivBluetooth);
        this.f6686h0 = (TextView) findViewById(R$id.tvSkip);
        this.f6687i0 = (LinearLayout) findViewById(R$id.layoutDevice);
        this.f6688j0 = findViewById(R$id.maskView);
        mo22714a(this.f6686h0, (ImageView) findViewById(R$id.ivClose));
        m10825A();
        m10828D();
    }

    /* renamed from: e */
    public /* synthetic */ void mo23040e(int i) {
        if (i == 0) {
            this.f6694p0 = false;
            if (this.f6686h0.getVisibility() == 8) {
                this.f6686h0.setVisibility(0);
            }
            this.f6682d0.setText(R$string.bind_tips);
        } else if (i == 1) {
            this.f6694p0 = false;
            if (this.f6686h0.getVisibility() == 8) {
                this.f6686h0.setVisibility(0);
            }
            this.f6682d0.setText(R$string.connecting);
        } else if (i == 2) {
            this.f6682d0.setText(R$string.connected);
            this.f6694p0 = true;
            this.f6686h0.setVisibility(8);
            this.f6685g0.setVisibility(0);
            this.f6685g0.setAnimation(this.f6693o0);
            this.f6685g0.startAnimation(this.f6693o0);
            ThreadUtils.C0877e<Void> eVar = this.f6679B0;
            if (eVar != null) {
                ThreadUtils.m952a(eVar);
            }
            m10868z();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public String mo22739j() {
        return getString(R$string.connect_device);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.device.ScanActivity$e */
    class C3771e implements PermissionUtils.C0916b {
        C3771e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.device.ScanActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, int):int
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, android.os.CountDownTimer):android.os.CountDownTimer
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, java.util.ArrayList):java.util.ArrayList
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.a(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.device.ScanActivity.b(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.activity.device.ScanActivity, int]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.b(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.b(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.b(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):void */
        /* renamed from: a */
        public void mo9861a(List<String> list) {
            if (C4222l.m13464c()) {
                boolean unused = ScanActivity.this.f6698t0 = true;
                ScanActivity.this.m10829E();
                return;
            }
            ScanActivity.this.m10845d(true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.device.ScanActivity.b(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.activity.device.ScanActivity, int]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.b(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.b(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.ui.activity.device.ScanActivity.b(com.heimavista.fiedorasport.ui.activity.device.ScanActivity, boolean):void */
        /* renamed from: a */
        public void mo9862a(List<String> list, List<String> list2) {
            if (!list.isEmpty()) {
                ScanActivity.this.m10845d(false);
            } else {
                ScanActivity.this.m10864v();
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo23038a(AdapterView adapterView, View view, int i, long j) {
        m10842c(false);
        BleDeviceItem bVar = this.f6689k0.get(i);
        this.f6680b0 = bVar.mo22625b();
        this.f6681c0 = bVar.mo22622a();
        CountDownTimer countDownTimer = this.f6700v0;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.f6689k0.clear();
        this.f6688j0.setVisibility(8);
        this.f6687i0.setVisibility(8);
        m10828D();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m10839b(boolean z) {
        HttpCancelable bVar = this.f6678A0;
        if (bVar != null) {
            bVar.cancel();
        }
        this.f6678A0 = new HvApiBind(this.f6680b0, this.f6681c0).request(z, new C3770d());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f6685g0.setVisibility(8);
        this.f6683e0.setVisibility(8);
        this.f6684f0.setVisibility(8);
        this.f6693o0 = new AlphaAnimation(0.0f, 1.0f);
        this.f6693o0.setDuration(800);
        this.f6693o0.setAnimationListener(m10848f(0));
        this.f6691m0 = AnimationUtils.loadAnimation(this, R$anim.ring_scale);
        this.f6691m0.setAnimationListener(m10848f(1));
        this.f6692n0 = AnimationUtils.loadAnimation(this, R$anim.ring_scale);
        this.f6692n0.setAnimationListener(m10848f(2));
        m10863u();
    }

    /* renamed from: b */
    public void mo22718b(int i) {
        super.mo22718b(i);
        C5217x.task().post(new C3785k(this, i));
    }

    /* renamed from: a */
    public /* synthetic */ void mo23039a(boolean z, boolean z2) {
        if (z2) {
            if (z) {
                startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            } else {
                PermissionUtils.m1214h();
            }
            this.f6699u0 = true;
            return;
        }
        m10868z();
    }

    /* renamed from: a */
    public void mo22706a(String str, String str2, int i) {
        super.mo22706a(str, str2, i);
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            ArrayList<String> arrayList = this.f6697s0;
            if (arrayList != null && arrayList.size() > 0) {
                Iterator<String> it = this.f6697s0.iterator();
                while (it.hasNext()) {
                    if (str.toUpperCase().startsWith(it.next().toUpperCase())) {
                        Iterator<BleDeviceItem> it2 = this.f6689k0.iterator();
                        boolean z = false;
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            BleDeviceItem next = it2.next();
                            if (next.mo22622a().equalsIgnoreCase(str2)) {
                                z = true;
                                next.mo22623a(i);
                                break;
                            }
                        }
                        if (!z) {
                            this.f6689k0.add(new BleDeviceItem(str, str2, i));
                            Collections.sort(this.f6689k0, new ComparatorBleDeviceItem());
                            return;
                        }
                        return;
                    }
                }
            }
        } else if (str2.equalsIgnoreCase(FSManager.m10323r().mo22842l())) {
            this.f6689k0.clear();
            this.f6689k0.add(new BleDeviceItem(str, str2, i));
            this.f6680b0 = str;
            this.f6681c0 = str2;
        }
    }
}
