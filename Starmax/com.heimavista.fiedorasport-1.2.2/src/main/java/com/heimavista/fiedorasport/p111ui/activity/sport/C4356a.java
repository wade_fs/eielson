package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.graphics.Bitmap;
import com.amap.api.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.a */
/* compiled from: lambda */
public final /* synthetic */ class C4356a implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ SportActivity f8472a;

    /* renamed from: b */
    private final /* synthetic */ LatLng f8473b;

    public /* synthetic */ C4356a(SportActivity sportActivity, LatLng latLng) {
        this.f8472a = sportActivity;
        this.f8473b = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8472a.mo24529a(this.f8473b, bitmap);
    }
}
