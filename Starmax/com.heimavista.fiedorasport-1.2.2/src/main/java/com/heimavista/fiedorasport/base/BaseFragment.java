package com.heimavista.fiedorasport.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p110i.Toast;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.LoadingDialog;
import java.util.Random;
import p119e.p189e.p191b.HvApp;

public abstract class BaseFragment<A extends BaseActivity> extends Fragment implements View.OnClickListener {

    /* renamed from: P */
    private boolean f6419P = false;

    /* renamed from: Q */
    private BroadcastReceiver f6420Q;

    /* renamed from: R */
    private A f6421R;

    /* renamed from: S */
    private View f6422S;

    /* renamed from: T */
    private BaseActivity.C3680b f6423T;

    /* renamed from: U */
    private int f6424U;

    /* renamed from: V */
    private Toast f6425V;

    /* renamed from: com.heimavista.fiedorasport.base.BaseFragment$a */
    class C3681a extends BroadcastReceiver {
        C3681a() {
        }

        public void onReceive(Context context, Intent intent) {
            BaseFragment.this.mo22754a(context, intent);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseFragment$b */
    static /* synthetic */ class C3682b {

        /* renamed from: a */
        static final /* synthetic */ int[] f6427a = new int[Lifecycle.State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                androidx.lifecycle.Lifecycle$State[] r0 = androidx.lifecycle.Lifecycle.State.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a = r0
                int[] r0 = com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.CREATED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.STARTED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.RESUMED     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.heimavista.fiedorasport.base.BaseFragment.C3682b.f6427a     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.DESTROYED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.base.BaseFragment.C3682b.<clinit>():void");
        }
    }

    /* renamed from: i */
    private boolean mo24680i() {
        return isVisible() && mo22771c() != null && !mo22771c().isFinishing();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo22758a(View view, Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22767a(View... viewArr) {
        ClickUtils.m1045a(viewArr, this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22770b(int... iArr) {
        if (this.f6422S != null) {
            for (int i : iArr) {
                View findViewById = this.f6422S.findViewById(i);
                if (findViewById != null) {
                    mo22767a(findViewById);
                }
            }
        }
    }

    /* renamed from: c */
    public A mo22771c() {
        return this.f6421R;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract int mo22772d();

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return null;
    }

    /* renamed from: f */
    public BroadcastReceiver mo22774f() {
        if (this.f6420Q == null) {
            this.f6420Q = new C3681a();
        }
        return this.f6420Q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public abstract void mo22775g();

    /* renamed from: h */
    public void mo22776h() {
        mo22766a(true);
    }

    public void onActivityCreated(@Nullable Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        BaseActivity.C3680b bVar = this.f6423T;
        if (bVar == null || this.f6424U != i) {
            super.onActivityResult(i, i2, intent);
            return;
        }
        bVar.onActivityResult(i2, intent);
        this.f6423T = null;
    }

    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.f6421R = (BaseActivity) requireActivity();
    }

    public void onClick(View view) {
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        String[] e = mo22773e();
        if (e != null && e.length > 0) {
            HvApp.m13010c().mo24156a(mo22774f(), e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.f6422S == null) {
            this.f6422S = layoutInflater.inflate(mo22772d(), viewGroup, false);
        }
        mo22758a(this.f6422S, bundle);
        return this.f6422S;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.f6420Q != null) {
            HvApp.m13010c().mo24155a(this.f6420Q);
        }
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        int i = C3682b.f6427a[getLifecycle().getCurrentState().ordinal()];
        if (i != 1 && i != 2) {
            if (i == 3) {
                if (!this.f6419P) {
                    mo22775g();
                    this.f6419P = true;
                }
            }
        }
    }

    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        LogUtils.m1139c(getClass().getSimpleName() + " onReceive:" + intent.getAction());
    }

    /* renamed from: a */
    public void mo22759a(Class<? extends Activity> cls) {
        startActivity(new Intent(this.f6421R, cls));
    }

    /* renamed from: a */
    public void mo22760a(Class<? extends Activity> cls, ActivityOptionsCompat activityOptionsCompat) {
        A a = this.f6421R;
        ContextCompat.startActivity(a, new Intent(a, cls), activityOptionsCompat.toBundle());
    }

    /* renamed from: a */
    public void mo22756a(Intent intent, BaseActivity.C3680b bVar) {
        mo22755a(intent, (Bundle) null, bVar);
    }

    /* renamed from: b */
    public void mo22769b(Class<? extends Activity> cls) {
        mo22760a(cls, ActivityOptionsCompat.makeSceneTransitionAnimation(this.f6421R, new Pair[0]));
    }

    /* renamed from: a */
    public void mo22755a(Intent intent, Bundle bundle, BaseActivity.C3680b bVar) {
        if (this.f6423T == null) {
            this.f6423T = bVar;
            this.f6424U = new Random().nextInt(255);
            startActivityForResult(intent, this.f6424U, bundle);
        }
    }

    /* renamed from: b */
    public void mo22768b() {
        if (mo24680i()) {
            mo22771c().mo22717b();
        }
    }

    /* renamed from: a */
    public void mo22761a(String str) {
        if (!TextUtils.isEmpty(str)) {
            Toast bVar = this.f6425V;
            if (bVar == null) {
                this.f6425V = Toast.m10684a(mo22771c(), str, 0);
            } else {
                bVar.mo22977a(str);
                this.f6425V.mo22975a(0);
            }
            this.f6425V.mo22974a();
        }
    }

    /* renamed from: a */
    public void mo22757a(View view) {
        if (mo24680i()) {
            mo22771c().hideSoftKeyboard(view);
        }
    }

    /* renamed from: a */
    public void mo22764a(String str, boolean z, BaseDialog.C4621m mVar) {
        mo22763a(str, getString(R$string.confirm), z, mVar);
    }

    /* renamed from: a */
    public void mo22763a(String str, String str2, boolean z, BaseDialog.C4621m mVar) {
        mo22762a(getString(R$string.warn), str, str2, z, false, mVar);
    }

    /* renamed from: a */
    public void mo22762a(String str, String str2, String str3, boolean z, boolean z2, BaseDialog.C4621m mVar) {
        if (mo24680i()) {
            mo22771c().mo22707a(str, str2, str3, z, z2, mVar);
        }
    }

    /* renamed from: a */
    public void mo22766a(boolean z) {
        mo22765a("", false, z, z, null);
    }

    /* renamed from: a */
    public void mo22765a(String str, boolean z, boolean z2, boolean z3, LoadingDialog.C4628a aVar) {
        if (mo24680i()) {
            mo22771c().mo22711a(str, z, z2, z3, aVar);
        }
    }
}
