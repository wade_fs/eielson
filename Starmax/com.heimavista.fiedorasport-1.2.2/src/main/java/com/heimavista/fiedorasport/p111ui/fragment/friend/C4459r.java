package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.r */
/* compiled from: lambda */
public final /* synthetic */ class C4459r implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ BuddyLocFragment f8789a;

    /* renamed from: b */
    private final /* synthetic */ String f8790b;

    /* renamed from: c */
    private final /* synthetic */ LatLng f8791c;

    public /* synthetic */ C4459r(BuddyLocFragment buddyLocFragment, String str, LatLng latLng) {
        this.f8789a = buddyLocFragment;
        this.f8790b = str;
        this.f8791c = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8789a.mo24695a(this.f8790b, this.f8791c, bitmap);
    }
}
