package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.View;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.p111ui.adapter.ListAddFriendsAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.l */
/* compiled from: lambda */
public final /* synthetic */ class C4248l implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ ListAddFriendsActivity f8124P;

    public /* synthetic */ C4248l(ListAddFriendsActivity listAddFriendsActivity) {
        this.f8124P = listAddFriendsActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8124P.mo24386a(view, (ListAddFriendsAdapter.C4398a) obj, i);
    }
}
