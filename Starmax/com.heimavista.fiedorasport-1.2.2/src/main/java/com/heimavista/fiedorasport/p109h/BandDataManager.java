package com.heimavista.fiedorasport.p109h;

import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.FSManager;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import com.sxr.sdk.ble.keepfit.aidl.UserProfile;
import com.tencent.mmkv.MMKV;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.h.w */
public class BandDataManager {

    /* renamed from: a */
    private static MMKV f6552a;

    /* renamed from: a */
    public static int m10545a() {
        return m10562d().mo27024a("battery", 0);
    }

    /* renamed from: b */
    public static void m10557b(int i) {
        m10562d().mo27039b("battery", i);
    }

    /* renamed from: c */
    public static int m10559c() {
        return m10562d().mo27024a("fontScale", 1);
    }

    /* renamed from: d */
    private static MMKV m10562d() {
        if (f6552a == null) {
            f6552a = MMKV.m17083e("fiedsport_band_data");
        }
        return f6552a;
    }

    /* renamed from: e */
    public static RemindMsgInfo m10563e() {
        return (RemindMsgInfo) m10562d().mo27027a(m10548a("remindMsg"), RemindMsgInfo.class, new RemindMsgInfo());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mmkv.MMKV.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.mmkv.MMKV.a(int, java.lang.String):com.tencent.mmkv.MMKV
      com.tencent.mmkv.MMKV.a(com.tencent.mmkv.c, java.lang.String):void
      com.tencent.mmkv.MMKV.a(java.lang.String, int):int
      com.tencent.mmkv.MMKV.a(java.lang.String, java.lang.Class):T
      com.tencent.mmkv.MMKV.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.mmkv.MMKV.a(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.tencent.mmkv.MMKV.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.mmkv.MMKV.a(java.lang.String, boolean):boolean
      com.tencent.mmkv.MMKV.a(java.lang.String, long):long */
    /* renamed from: f */
    public static long m10564f() {
        MMKV d = m10562d();
        return d.mo27025a(MemberControl.m17125s().mo27187l() + "_share_loc_expire", 0L);
    }

    /* renamed from: g */
    public static int m10565g() {
        return m10562d().mo27024a(m10548a("target_step"), 8000);
    }

    /* renamed from: h */
    public static UserProfile m10566h() {
        UserProfile userProfile = (UserProfile) m10562d().mo27026a(m10548a("userProfile"), UserProfile.class);
        if (userProfile != null) {
            return userProfile;
        }
        int j = FSManager.m10323r().mo22840j();
        int n = FSManager.m10323r().mo22844n();
        return new UserProfile(m10565g(), j == 0 ? 170 : j, n == 0 ? 60 : n, 61, 0, "M".equals(MemberControl.m17125s().mo27178d()) ? 1 : 0, FSManager.m10323r().mo22833d());
    }

    /* renamed from: i */
    public static String m10567i() {
        return m10562d().mo27028a(m10548a("watchFaceFilePath"), "");
    }

    /* renamed from: a */
    private static String m10548a(String str) {
        return MemberControl.m17125s().mo27187l() + "_" + str;
    }

    /* renamed from: b */
    public static DeviceProfile m10556b() {
        return (DeviceProfile) m10562d().mo27027a(m10548a("deviceProfile"), DeviceProfile.class, new DeviceProfile(true, true, true, 22, 8, 0, 0));
    }

    /* renamed from: c */
    public static void m10560c(int i) {
        m10562d().mo27039b(m10548a("target_step"), i);
    }

    /* renamed from: a */
    public static void m10552a(AlarmInfoItem alarmInfoItem) {
        MMKV d = m10562d();
        d.mo27032a(m10548a("alarm_" + alarmInfoItem.mo26354a()), alarmInfoItem);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: b */
    public static PeriodTimeInfo m10555b(String str) {
        char c;
        PeriodTimeInfo periodTimeInfo;
        switch (str.hashCode()) {
            case -990169582:
                if (str.equals("QuickView")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 726532791:
                if (str.equals("DoNotDisturb")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 1788058703:
                if (str.equals("Sedentary")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 1965687765:
                if (str.equals("Location")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            periodTimeInfo = PeriodTimeInfo.m10089l();
        } else if (c == 1) {
            periodTimeInfo = PeriodTimeInfo.m10086i();
        } else if (c == 2) {
            periodTimeInfo = PeriodTimeInfo.m10088k();
        } else if (c != 3) {
            periodTimeInfo = PeriodTimeInfo.m10085h();
        } else {
            periodTimeInfo = PeriodTimeInfo.m10087j();
        }
        return (PeriodTimeInfo) m10562d().mo27027a(m10548a(str), PeriodTimeInfo.class, periodTimeInfo);
    }

    /* renamed from: c */
    public static void m10561c(String str) {
        m10562d().mo27041b(m10548a("watchFaceFilePath"), str);
    }

    /* renamed from: a */
    public static AlarmInfoItem m10547a(int i) {
        int i2 = i;
        MMKV d = m10562d();
        AlarmInfoItem alarmInfoItem = r4;
        AlarmInfoItem alarmInfoItem2 = new AlarmInfoItem((long) i2, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, "", false);
        return (AlarmInfoItem) d.mo27027a(m10548a("alarm_" + i2), AlarmInfoItem.class, alarmInfoItem);
    }

    /* renamed from: a */
    public static void m10553a(DeviceProfile deviceProfile) {
        m10562d().mo27032a(m10548a("deviceProfile"), deviceProfile);
    }

    /* renamed from: a */
    public static void m10550a(PeriodTimeInfo periodTimeInfo) {
        m10562d().mo27032a(m10548a(periodTimeInfo.mo22592f()), periodTimeInfo);
    }

    /* renamed from: a */
    public static void m10551a(RemindMsgInfo remindMsgInfo) {
        m10562d().mo27032a(m10548a("remindMsg"), remindMsgInfo);
    }

    /* renamed from: a */
    public static void m10554a(UserProfile userProfile) {
        m10562d().mo27032a(m10548a("userProfile"), userProfile);
    }

    /* renamed from: a */
    public static int m10546a(long j) {
        MMKV d = m10562d();
        return d.mo27024a("lastStep_" + TimeUtils.m998a(j * 1000, "yyyyMMdd"), 0);
    }

    /* renamed from: a */
    public static void m10549a(long j, int i) {
        MMKV d = m10562d();
        d.mo27039b("lastStep_" + TimeUtils.m998a(j * 1000, "yyyyMMdd"), i);
    }

    /* renamed from: b */
    public static void m10558b(long j) {
        MMKV d = m10562d();
        d.mo27040b(MemberControl.m17125s().mo27187l() + "_share_loc_expire", j);
    }
}
