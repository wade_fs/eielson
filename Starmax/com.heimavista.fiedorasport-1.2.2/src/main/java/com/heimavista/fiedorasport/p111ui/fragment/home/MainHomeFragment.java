package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.lifecycle.Lifecycle;
import com.amap.api.location.AMapLocation;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.C2931b;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.heimavista.api.HvApiBasic;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.sport.HvApiGetLikeList;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.DeviceManagerActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.ScanActivity;
import com.heimavista.fiedorasport.p111ui.activity.heart.HeartRateDataActivity;
import com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataActivity;
import com.heimavista.fiedorasport.p111ui.activity.sport.SportDataActivity;
import com.heimavista.fiedorasport.p111ui.activity.step.StepDataActivity;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.p199j.GeocoderUtils;
import com.heimavista.fiedorasport.widget.BatteryView;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.utils.ParamJsonData;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment */
public class MainHomeFragment extends BaseFragment<HomeActivity> {

    /* renamed from: W */
    private SmartRefreshLayout f8815W;

    /* renamed from: X */
    private TextView f8816X;

    /* renamed from: Y */
    private ImageView f8817Y;

    /* renamed from: Z */
    private TextView f8818Z;

    /* renamed from: a0 */
    private View f8819a0;

    /* renamed from: b0 */
    private ImageView f8820b0;

    /* renamed from: c0 */
    private TextView f8821c0;

    /* renamed from: d0 */
    private View f8822d0;

    /* renamed from: e0 */
    private BatteryView f8823e0;

    /* renamed from: f0 */
    private TextView f8824f0;

    /* renamed from: g0 */
    private TextView f8825g0;

    /* renamed from: h0 */
    private ImageView f8826h0;

    /* renamed from: i0 */
    private ImageView f8827i0;

    /* renamed from: j0 */
    private TextView f8828j0;

    /* renamed from: k0 */
    private TextView f8829k0;

    /* renamed from: l0 */
    private TextView f8830l0;

    /* renamed from: m0 */
    private TextView f8831m0;
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public TextView f8832n0;

    /* renamed from: o0 */
    private boolean f8833o0 = false;

    /* renamed from: p0 */
    private boolean f8834p0 = false;

    /* renamed from: q0 */
    private boolean f8835q0 = FSManager.m10325t();

    /* renamed from: r0 */
    private MapFragment f8836r0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment$a */
    class C4470a implements GeocoderUtils.C4221b {
        C4470a() {
        }

        /* renamed from: a */
        public void mo24353a(Throwable th) {
        }

        /* renamed from: a */
        public void mo24354a(List<Address> list) {
            if (list != null && list.size() > 0) {
                Address address = list.get(0);
                String str = address.getAdminArea() + address.getLocality() + address.getSubAdminArea();
                if (str.contains("null")) {
                    str = str.replaceAll("null", "");
                }
                LogUtils.m1139c("madyLoc>>>GeocoderUtils-> curAddress:" + str + " " + address.getAddressLine(0));
                if (MainHomeFragment.this.isVisible()) {
                    MainHomeFragment.this.f8832n0.setText(str);
                }
            }
        }
    }

    /* renamed from: b */
    private void m14548b(int i) {
        SpanUtils a = SpanUtils.m865a(this.f8829k0);
        a.mo9735a(i == 0 ? "--" : String.valueOf(i));
        m14544a(a);
        a.mo9735a(getString(R$string.data_unit_step));
        a.mo9736b();
    }

    /* renamed from: i */
    private void m14551i() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null && !defaultAdapter.isEnabled()) {
            defaultAdapter.enable();
        } else if (!TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            mo22759a(DeviceManagerActivity.class);
        } else {
            mo22759a(ScanActivity.class);
        }
    }

    /* renamed from: j */
    private void m14552j() {
        this.f8815W.mo26069f(true);
        TextView textView = this.f8816X;
        if (textView != null) {
            textView.setVisibility(8);
        }
    }

    /* renamed from: k */
    private void m14553k() {
        if (BandConfig.m10064m().mo22561f() && !BandConfig.m10064m().mo22558c().contains("BAND-1000")) {
            getChildFragmentManager().beginTransaction().replace(R$id.gad, HvGad.m14915i().mo25016f().mo25034a("BAND-1000", new HvGadViewConfig.C4502b(ScreenUtils.m1265c(), -2).mo24895a()), "ad_home").commit();
            this.f8833o0 = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* renamed from: l */
    private void m14554l() {
        this.f8836r0 = MapFragment.m14638a(this.f8835q0, false);
        if (this.f8835q0) {
            this.f8836r0.mo24729a(new C4475b(this));
        } else {
            this.f8836r0.mo24729a(new C4478e(this));
        }
        getChildFragmentManager().beginTransaction().replace(R$id.mapFragment, this.f8836r0, "mapFragment").commit();
    }

    /* renamed from: m */
    public static MainHomeFragment m14555m() {
        Bundle bundle = new Bundle();
        MainHomeFragment mainHomeFragment = new MainHomeFragment();
        mainHomeFragment.setArguments(bundle);
        return mainHomeFragment;
    }

    /* renamed from: n */
    private void m14556n() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.LOCATION");
        b.mo9856a(new C4471b());
        b.mo9859a();
    }

    /* renamed from: o */
    private void m14557o() {
        m14561s();
        m14563u();
        m14562t();
        m14559q();
    }

    /* renamed from: p */
    private void m14558p() {
        this.f8822d0.setVisibility(8);
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            this.f8819a0.setVisibility(0);
            this.f8825g0.setVisibility(0);
            this.f8825g0.setText(R$string.press_to_bind_device);
            this.f8821c0.setText(R$string.has_not_bind_device);
            this.f8821c0.setTextColor(getResources().getColor(R$color.red));
            this.f8820b0.setImageResource(R$mipmap.icon_exclamatory);
            return;
        }
        this.f8825g0.setVisibility(8);
        boolean e = FSManager.m10323r().mo22847q() ? JYSDKManager.m10388i().mo22897e() : false;
        if (FSManager.m10323r().mo22846p()) {
            e = CRPBleManager.m10639j();
        }
        if (e) {
            ((HomeActivity) mo22771c()).mo23018z();
            ((HomeActivity) mo22771c()).mo23010A();
            m14545a(BandDataManager.m10545a());
            this.f8821c0.setText(R$string.device_has_connected);
            this.f8821c0.setTextColor(getResources().getColor(R$color.font_color));
            this.f8820b0.setImageResource(R$mipmap.icon_bt);
            C4222l.m13457a(this.f8826h0, R$color.font_color);
            return;
        }
        String str = ((HomeActivity) mo22771c()).f6639f0;
        if (!TextUtils.isEmpty(str)) {
            this.f8825g0.setVisibility(0);
            this.f8825g0.setText(((HomeActivity) mo22771c()).f6639f0);
            if (getString(R$string.ble_state_off).equals(str)) {
                m14552j();
            }
        }
        this.f8821c0.setText(R$string.device_has_not_connect);
        this.f8821c0.setTextColor(getResources().getColor(R$color.font_color));
        this.f8820b0.setImageResource(R$mipmap.icon_bt_dis);
    }

    /* renamed from: q */
    private void m14559q() {
        String str;
        HeartRateDataDb s = HeartRateDataDb.m13157s();
        String str2 = "--";
        if (s == null) {
            str = str2;
        } else {
            str2 = String.valueOf(s.mo24232o());
            str = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US).format(new Date(s.mo24259p() * 1000));
        }
        SpanUtils a = SpanUtils.m865a(this.f8830l0);
        a.mo9735a(str2);
        m14544a(a);
        a.mo9735a(getString(R$string.data_unit_rate));
        a.mo9732a();
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_13));
        a.mo9736b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* renamed from: r */
    private void m14560r() {
        if (MemberControl.m17125s().mo27192q()) {
            AvatarUtils.m13393a(this.f8817Y, MemberControl.m17125s().mo27183h(), R$mipmap.login_user, true);
            String g = MemberControl.m17125s().mo27182g();
            if (TextUtils.isEmpty(g)) {
                g = MemberControl.m17125s().mo27185j();
            }
            this.f8818Z.setText(g);
            return;
        }
        this.f8817Y.setImageResource(R$mipmap.login_user);
        this.f8818Z.setText(R$string.login_first);
    }

    /* renamed from: s */
    private void m14561s() {
        int i = new SleepDataDb().mo24261a(0).f6258g;
        SpanUtils a = SpanUtils.m865a(this.f8828j0);
        String str = "--";
        a.mo9735a(i == 0 ? str : String.valueOf(i / 60));
        m14544a(a);
        a.mo9735a(getString(R$string.data_unit_hour));
        if (i != 0) {
            str = String.valueOf(i % 60);
        }
        a.mo9735a(str);
        m14544a(a);
        a.mo9735a(getString(R$string.data_unit_min));
        a.mo9736b();
    }

    /* renamed from: t */
    private void m14562t() {
        int i;
        long j;
        SportInfo o = new SportDataDb().mo24232o();
        if (o == null) {
            i = 0;
        } else {
            i = o.f6277h;
        }
        if (o == null) {
            j = 0;
        } else {
            j = o.f6274e * 1000;
        }
        String str = "--";
        if (i == 0) {
            SpanUtils a = SpanUtils.m865a(this.f8831m0);
            a.mo9735a(str);
            m14544a(a);
            a.mo9735a(getString(R$string.data_unit_mill));
            a.mo9736b();
            return;
        }
        SpanUtils a2 = SpanUtils.m865a(this.f8831m0);
        a2.mo9735a(String.format(Locale.getDefault(), "%.2f", Double.valueOf(((double) i) / 1000.0d)));
        m14544a(a2);
        a2.mo9735a(getString(R$string.data_unit_mill));
        a2.mo9732a();
        if (j != 0) {
            str = TimeUtils.m1003a(new Date(j), new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()));
        }
        a2.mo9735a(str);
        a2.mo9736b();
    }

    /* renamed from: u */
    private void m14563u() {
        m14548b(CurStepDataDb.m13136k(MemberControl.m17125s().mo27187l()).mo24250x());
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_main_home;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return new String[]{"fiedoraSport.ACTION_onGetHistoryEnd", "fiedoraSport.ACTION_onMapTypeChanged", "fiedoraSport.ACTION_onBleStatusChange", "fiedoraSport.ACTION_onBleBatteryChange", "fiedoraSport.ACTION_onJYSensorChange", "fiedoraSport.ACTION_onJYStepChange", "fiedoraSport.ACTION_onJYDayDataEnd", "fiedoraSport.ACTION_onJYMultiSportData", "fiedoraSport.ACTION_onGetSleepDataFinish", "fiedoraSport.ACTION_onGetHeartDataFinish", "fiedoraSport.ACTION_onDataSyncing", "fiedoraSport.ACTION_onDataSyncEnd", HvApiBasic.ACTION_OTHER_BIND, "LocManager.action_location_callback"};
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        m14553k();
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.ivAvatar || id == R$id.tvUserName) {
            ((HomeActivity) mo22771c()).mo23017y();
        } else if (id == R$id.ivBracelet || id == R$id.tvStatus || id == R$id.ivStatus || id == R$id.tvBattery || id == R$id.ivBattery || id == R$id.tvBleState) {
            Constant.m10311a("點擊綁定設備");
            m14551i();
        } else if (id == R$id.llSleep) {
            Constant.m10311a("點擊睡眠");
            mo22759a(SleepDataActivity.class);
        } else if (id == R$id.llWalk) {
            Constant.m10311a("點擊步數");
            startActivity(new Intent(mo22771c(), StepDataActivity.class));
        } else if (id == R$id.rlHeartRate) {
            Constant.m10311a("點擊運動");
            mo22759a(HeartRateDataActivity.class);
        } else if (id == R$id.rlSport) {
            Constant.m10311a("點擊心跳");
            mo22759a(SportDataActivity.class);
            new HvApiGetLikeList().request((OnResultListener<ParamJsonData>) null);
        } else if (id == R$id.llPosition) {
            ((HomeActivity) mo22771c()).mo23015w();
        } else if (id == R$id.ivPos) {
            m14556n();
        }
    }

    public void onResume() {
        super.onResume();
        HvGad.m14915i().mo25016f().mo25036a(this.f8827i0, R$mipmap.app_icon, 8);
        LogUtils.m1139c(MainHomeFragment.class.getSimpleName() + " HomeAty onResume");
        m14560r();
        m14558p();
        m14557o();
        if (TextUtils.isEmpty(((HomeActivity) mo22771c()).mo23014v())) {
            m14552j();
        } else {
            m14549b(((HomeActivity) mo22771c()).mo23014v());
        }
        if (this.f8834p0) {
            m14556n();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment$b */
    class C4471b implements PermissionUtils.C0916b {
        C4471b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, int]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, boolean):void */
        /* renamed from: a */
        public void mo9861a(List<String> list) {
            if (!C4222l.m13464c()) {
                MainHomeFragment.this.m14550b(true);
            } else {
                LocManager.m10493g().mo22957b();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, int]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment.a(com.heimavista.fiedorasport.ui.fragment.home.MainHomeFragment, boolean):void */
        /* renamed from: a */
        public void mo9862a(List<String> list, List<String> list2) {
            if (!list.isEmpty()) {
                MainHomeFragment.this.m14550b(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        this.f8815W = (SmartRefreshLayout) view.findViewById(R$id.refreshLayout);
        ((LinearLayout) view.findViewById(R$id.layoutContainer)).setPadding(0, BarUtils.m981a(), 0, 0);
        this.f8816X = (TextView) view.findViewById(R$id.tvSyncTip);
        this.f8817Y = (ImageView) view.findViewById(R$id.ivAvatar);
        this.f8818Z = (TextView) view.findViewById(R$id.tvUserName);
        this.f8819a0 = view.findViewById(R$id.layoutStatus);
        this.f8819a0.setVisibility(0);
        this.f8820b0 = (ImageView) view.findViewById(R$id.ivStatus);
        this.f8821c0 = (TextView) view.findViewById(R$id.tvStatus);
        this.f8822d0 = view.findViewById(R$id.layoutBattery);
        this.f8822d0.setVisibility(8);
        this.f8823e0 = (BatteryView) view.findViewById(R$id.ivBattery);
        this.f8824f0 = (TextView) view.findViewById(R$id.tvBattery);
        this.f8825g0 = (TextView) view.findViewById(R$id.tvBleState);
        this.f8819a0.setVisibility(0);
        this.f8826h0 = (ImageView) view.findViewById(R$id.ivBracelet);
        this.f8827i0 = (ImageView) view.findViewById(R$id.ivProductLogo);
        this.f8828j0 = (TextView) view.findViewById(R$id.tvSleepTime);
        this.f8829k0 = (TextView) view.findViewById(R$id.tvWalk);
        this.f8830l0 = (TextView) view.findViewById(R$id.tvHeartRate);
        this.f8831m0 = (TextView) view.findViewById(R$id.tvSport);
        this.f8832n0 = (TextView) view.findViewById(R$id.tvLocation);
        mo22767a(this.f8817Y, this.f8818Z, this.f8826h0, this.f8821c0, this.f8824f0, this.f8825g0, this.f8823e0);
        mo22770b(R$id.llSleep, R$id.llWalk, R$id.rlHeartRate, R$id.rlSport, R$id.ivPos);
        mo22770b(R$id.llPosition);
        this.f8815W.mo26048a(new C4476c(this));
        m14554l();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14550b(boolean z) {
        mo22763a(getString(z ? R$string.gps_permission_denied : R$string.location_permission_denied), getString(R$string.goto_open), false, new C4477d(this, z));
    }

    /* renamed from: b */
    private void m14549b(String str) {
        this.f8815W.mo26069f(false);
        TextView textView = this.f8816X;
        if (textView != null) {
            textView.setVisibility(0);
            this.f8816X.setText(str);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24715a(RefreshLayout jVar) {
        this.f8815W.mo26062d();
        if (this.f8833o0) {
            m14553k();
        }
        Constant.m10311a("下拉刷新");
        HvApp.m13010c().mo24157a("fiedoraSport.ACTION_refreshDataFromServer");
    }

    /* renamed from: a */
    private void m14545a(int i) {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
            if (i > 0) {
                this.f8822d0.setVisibility(0);
                this.f8825g0.setVisibility(8);
                this.f8823e0.setPower(i);
                this.f8824f0.setText(i + "%");
                if (i < 20) {
                    TextView textView = this.f8824f0;
                    textView.setTextColor(textView.getContext().getResources().getColor(R$color.red));
                    return;
                }
                TextView textView2 = this.f8824f0;
                textView2.setTextColor(textView2.getContext().getResources().getColor(R$color.font_color));
                return;
            }
            this.f8822d0.setVisibility(8);
        }
    }

    /* renamed from: a */
    private SpanUtils m14544a(SpanUtils spanUtils) {
        spanUtils.mo9737b(getResources().getColor(R$color.font_color));
        spanUtils.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        spanUtils.mo9738c();
        return spanUtils;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        AMapLocation aMapLocation;
        super.mo22754a(context, intent);
        String action = intent.getAction();
        if ("fiedoraSport.ACTION_onGetHistoryEnd".equals(action)) {
            m14557o();
        }
        if ("fiedoraSport.ACTION_onBleStatusChange".equals(action) || HvApiBasic.ACTION_OTHER_BIND.equals(action)) {
            m14558p();
        }
        if ("fiedoraSport.ACTION_onBleBatteryChange".equals(action)) {
            m14545a(intent.getIntExtra("battery", 0));
        }
        if ("fiedoraSport.ACTION_onJYSensorChange".equals(action) || "fiedoraSport.ACTION_onGetHeartDataFinish".equals(action)) {
            m14559q();
        }
        if ("fiedoraSport.ACTION_onGetSleepDataFinish".equals(action)) {
            m14561s();
        }
        if ("fiedoraSport.ACTION_onJYStepChange".equals(action)) {
            m14548b(intent.getIntExtra("step", 0));
        }
        if ("fiedoraSport.ACTION_onMapTypeChanged".equals(action)) {
            this.f8832n0.setText("");
            m14554l();
        }
        if ("LocManager.action_location_callback".equals(action) && (aMapLocation = (Location) intent.getParcelableExtra("location")) != null) {
            if (!LocManager.m10494h() || !(aMapLocation instanceof AMapLocation)) {
                GeocoderUtils.m13412a(aMapLocation.getLatitude(), aMapLocation.getLongitude(), new C4470a());
            } else {
                AMapLocation aMapLocation2 = aMapLocation;
                if (isVisible()) {
                    TextView textView = this.f8832n0;
                    textView.setText(aMapLocation2.getProvince() + aMapLocation2.getCity() + aMapLocation2.getDistrict());
                }
            }
            m14546a((Location) aMapLocation);
        }
        if ("fiedoraSport.ACTION_onDataSyncing".equals(action)) {
            m14549b(intent.getStringExtra("text"));
        } else if ("fiedoraSport.ACTION_onDataSyncEnd".equals(action)) {
            m14552j();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24714a(C2880c cVar) {
        cVar.mo18407b().mo18415c(false);
        this.f8834p0 = true;
        m14556n();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24713a(AMap aMap) {
        aMap.getUiSettings().setZoomControlsEnabled(false);
        this.f8834p0 = true;
        m14556n();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24716a(boolean z, boolean z2) {
        if (!z2) {
            return;
        }
        if (z) {
            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        } else {
            PermissionUtils.m1214h();
        }
    }

    /* renamed from: a */
    private void m14546a(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        if (this.f8835q0) {
            this.f8836r0.mo24728a(C2879b.m8106a(new LatLng(latitude, longitude)));
        } else {
            this.f8836r0.mo24726a(CameraUpdateFactory.newLatLng(new com.amap.api.maps.model.LatLng(latitude, longitude)));
        }
        if (mo22771c() != null && !((HomeActivity) mo22771c()).isFinishing()) {
            this.f8836r0.mo24730b();
            AvatarUtils.m13392a(mo22771c(), MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4474a(this, latitude, longitude));
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24712a(double d, double d2, Bitmap bitmap) {
        if (this.f8835q0) {
            this.f8836r0.mo24725a(new MarkerOptions().mo18545a(new LatLng(d, d2)).mo18546a(C2931b.m8343a(bitmap)));
        } else {
            this.f8836r0.mo24724a(new com.amap.api.maps.model.MarkerOptions().position(new com.amap.api.maps.model.LatLng(d, d2)).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
        }
    }
}
