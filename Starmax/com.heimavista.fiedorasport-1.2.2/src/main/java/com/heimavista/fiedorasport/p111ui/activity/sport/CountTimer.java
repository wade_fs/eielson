package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.o */
public class CountTimer {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public long f8490a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public long f8491b = -1;

    /* renamed from: c */
    private long f8492c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public long f8493d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public long f8494e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public volatile int f8495f = 0;

    /* renamed from: g */
    private Handler f8496g = new C4370a();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.o$a */
    /* compiled from: CountTimer */
    class C4370a extends Handler {
        C4370a() {
        }

        public void handleMessage(Message message) {
            synchronized (CountTimer.this) {
                if (CountTimer.this.f8495f == 1) {
                    long unused = CountTimer.this.f8493d = SystemClock.elapsedRealtime();
                    CountTimer.this.mo24537e((CountTimer.this.f8493d - CountTimer.this.f8491b) - CountTimer.this.f8494e);
                    if (CountTimer.this.f8495f == 1) {
                        long b = (CountTimer.this.f8493d + CountTimer.this.f8490a) - SystemClock.elapsedRealtime();
                        while (b < 0) {
                            b += CountTimer.this.f8490a;
                        }
                        sendMessageDelayed(obtainMessage(1), b);
                    }
                }
            }
        }
    }

    public CountTimer(long j) {
        this.f8490a = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24563a(long j) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24565b(long j) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo24567c(long j) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo24569d(long j) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo24537e(long j) {
    }

    /* renamed from: b */
    public synchronized void mo24564b() {
        if (this.f8495f == 1) {
            this.f8496g.removeMessages(1);
            this.f8495f = 2;
            this.f8492c = SystemClock.elapsedRealtime();
            mo24565b((this.f8492c - this.f8491b) - this.f8494e);
        }
    }

    /* renamed from: c */
    public synchronized void mo24566c() {
        if (this.f8495f == 2) {
            this.f8495f = 1;
            mo24567c((this.f8492c - this.f8491b) - this.f8494e);
            long j = this.f8490a - (this.f8492c - this.f8493d);
            this.f8494e += SystemClock.elapsedRealtime() - this.f8492c;
            this.f8496g.sendEmptyMessageDelayed(1, j);
        }
    }

    /* renamed from: d */
    public synchronized void mo24568d() {
        if (this.f8495f != 1) {
            this.f8494e = 0;
            this.f8491b = SystemClock.elapsedRealtime();
            this.f8495f = 1;
            mo24569d(0);
            this.f8496g.sendEmptyMessageDelayed(1, this.f8490a);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo24562a() {
        /*
            r4 = this;
            monitor-enter(r4)
            int r0 = r4.f8495f     // Catch:{ all -> 0x0032 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r4)
            return
        L_0x0007:
            int r0 = r4.f8495f     // Catch:{ all -> 0x0032 }
            android.os.Handler r1 = r4.f8496g     // Catch:{ all -> 0x0032 }
            r2 = 1
            r1.removeMessages(r2)     // Catch:{ all -> 0x0032 }
            r1 = 0
            r4.f8495f = r1     // Catch:{ all -> 0x0032 }
            if (r0 != r2) goto L_0x0022
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0032 }
            long r2 = r4.f8491b     // Catch:{ all -> 0x0032 }
            long r0 = r0 - r2
            long r2 = r4.f8494e     // Catch:{ all -> 0x0032 }
            long r0 = r0 - r2
            r4.mo24563a(r0)     // Catch:{ all -> 0x0032 }
            goto L_0x0030
        L_0x0022:
            r1 = 2
            if (r0 != r1) goto L_0x0030
            long r0 = r4.f8492c     // Catch:{ all -> 0x0032 }
            long r2 = r4.f8491b     // Catch:{ all -> 0x0032 }
            long r0 = r0 - r2
            long r2 = r4.f8494e     // Catch:{ all -> 0x0032 }
            long r0 = r0 - r2
            r4.mo24563a(r0)     // Catch:{ all -> 0x0032 }
        L_0x0030:
            monitor-exit(r4)
            return
        L_0x0032:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p111ui.activity.sport.CountTimer.mo24562a():void");
    }
}
