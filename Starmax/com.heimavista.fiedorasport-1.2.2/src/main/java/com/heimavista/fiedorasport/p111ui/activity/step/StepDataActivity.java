package com.heimavista.fiedorasport.p111ui.activity.step;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.location.LocationActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataActivity */
public class StepDataActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public int f8498b0 = 0;

    /* renamed from: c0 */
    private TextView f8499c0;

    /* renamed from: d0 */
    private TextView f8500d0;

    /* renamed from: e0 */
    private TextView f8501e0;

    /* renamed from: f0 */
    private TextView f8502f0;

    /* renamed from: g0 */
    private TextView f8503g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public TextView f8504h0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataActivity$a */
    class C4371a extends ThreadUtils.C0877e<StepInfo> {
        C4371a() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(StepInfo stepInfo) {
            int i;
            int i2 = 0;
            int i3 = stepInfo == null ? 0 : stepInfo.f6245S;
            if (stepInfo == null) {
                i = 0;
            } else {
                i = stepInfo.f6246T;
            }
            if (stepInfo != null) {
                i2 = stepInfo.f6247U;
            }
            StepDataActivity.this.m14118a(i3, i, i2);
        }

        /* renamed from: b */
        public StepInfo m14136b() {
            if (StepDataActivity.this.f8498b0 != 0) {
                return StepDataDb.m13223f(StepDataActivity.this.f8498b0);
            }
            CurStepDataDb k = CurStepDataDb.m13136k(MemberControl.m17125s().mo27187l());
            return new StepInfo(k.mo24240q(), k.mo24232o(), k.mo24239p(), k.mo24250x(), k.mo24249w(), k.mo24248v());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.step.StepDataActivity$b */
    class C4372b extends ThreadUtils.C0877e<HeartRateDataDb> {
        C4372b() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(HeartRateDataDb dVar) {
            String str;
            int o = dVar == null ? 0 : dVar.mo24232o();
            SpanUtils a = SpanUtils.m865a(StepDataActivity.this.f8504h0);
            a.mo9735a(StepDataActivity.this.getString(R$string.heart_rate));
            a.mo9735a(" ");
            if (o == 0) {
                str = "--";
            } else {
                str = String.valueOf(o);
            }
            a.mo9735a(str);
            a.mo9733a(StepDataActivity.this.getResources().getDimensionPixelSize(R$dimen.font_size_18));
            a.mo9738c();
            a.mo9735a(StepDataActivity.this.getString(R$string.data_unit_rate));
            a.mo9736b();
        }

        /* renamed from: b */
        public HeartRateDataDb m14142b() {
            return HeartRateDataDb.m13155c(StepDataActivity.this.f8498b0);
        }
    }

    /* renamed from: u */
    private void m14121u() {
        Calendar instance = Calendar.getInstance();
        instance.add(6, -this.f8498b0);
        this.f8499c0.setText(TimeUtils.m1003a(instance.getTime(), new SimpleDateFormat("M月dd日", Locale.getDefault())));
    }

    /* renamed from: v */
    private void m14122v() {
        ThreadUtils.m959b(new C4372b());
    }

    /* renamed from: w */
    private void m14123w() {
        ThreadUtils.m959b(new C4371a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_step;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return "";
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.ivBeforeDay) {
            Constant.m10311a("點擊查看前一天");
            this.f8498b0++;
            m14121u();
            m14123w();
            m14122v();
        } else if (id == R$id.ivNextDay) {
            int i = this.f8498b0;
            if (i == 0) {
                mo22730c(getString(R$string.not_yet_arrived_tomorrow));
                return;
            }
            this.f8498b0 = i - 1;
            m14121u();
            m14123w();
            m14122v();
        } else if (id == R$id.llStatistics) {
            Constant.m10311a("點擊統計");
            mo22702a(StepDataChartActivity.class);
        } else if (id == R$id.layoutGames) {
            mo22702a(LocationActivity.class);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8499c0 = (TextView) findViewById(R$id.tvDate);
        this.f8500d0 = (TextView) findViewById(R$id.tvStep);
        this.f8501e0 = (TextView) findViewById(R$id.tvWalkStep);
        this.f8502f0 = (TextView) findViewById(R$id.tvWalkMile);
        this.f8503g0 = (TextView) findViewById(R$id.tvWalkCal);
        this.f8504h0 = (TextView) findViewById(R$id.tvWalkHeart);
        mo22723b(R$id.ivBeforeDay, R$id.ivNextDay, R$id.layoutGames, R$id.llStatistics);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2003", "exad_step");
        m14121u();
        m14123w();
        m14122v();
    }

    /* renamed from: a */
    public void mo22690a(long j, int i, int i2, int i3, int i4) {
        super.mo22690a(j, i, i2, i3, i4);
        m14118a(i, i2, i3);
    }

    /* renamed from: a */
    public void mo22688a(long j, int i, int i2) {
        String str;
        super.mo22688a(j, i, i2);
        SpanUtils a = SpanUtils.m865a(this.f8504h0);
        a.mo9735a(getString(R$string.heart_rate));
        a.mo9735a(" ");
        if (i == 0) {
            str = "--";
        } else {
            str = String.valueOf(i);
        }
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_18));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_rate));
        a.mo9736b();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14118a(int i, int i2, int i3) {
        String str;
        String str2;
        String str3;
        SpanUtils a = SpanUtils.m865a(this.f8500d0);
        String str4 = "--";
        if (i == 0) {
            str = str4;
        } else {
            str = String.valueOf(i);
        }
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_step));
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8501e0);
        a2.mo9735a(getString(R$string.step_today_count));
        a2.mo9735a(" ");
        if (i == 0) {
            str2 = str4;
        } else {
            str2 = String.valueOf(i);
        }
        a2.mo9735a(str2);
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_18));
        a2.mo9738c();
        a2.mo9735a(getString(R$string.data_unit_step));
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8502f0);
        a3.mo9735a(getString(R$string.step_today_mileage));
        a3.mo9735a(" ");
        if (i2 == 0) {
            str3 = str4;
        } else {
            str3 = String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) i2) / 1000.0f));
        }
        a3.mo9735a(str3);
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_18));
        a3.mo9738c();
        a3.mo9735a(getString(R$string.data_unit_mill));
        a3.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8503g0);
        a4.mo9735a(getString(R$string.step_today_cal));
        a4.mo9735a(" ");
        if (i3 != 0) {
            str4 = String.valueOf(i3);
        }
        a4.mo9735a(str4);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_18));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_cal));
        a4.mo9736b();
    }
}
