package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.PickerManager;
import com.heimavista.fiedorasport.p111ui.adapter.ListMenuAdapter;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.utils.ParamJsonData;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.xutils.C5217x;
import p112d.p113a.p114a.p116b.TimePicker;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity */
public class SettingPowerActivity extends BaseActivity implements C3734y.C3735a {

    /* renamed from: b0 */
    private RecyclerView f8265b0;

    /* renamed from: c0 */
    private ListMenuAdapter f8266c0;

    /* renamed from: d0 */
    private DeviceProfile f8267d0;

    /* renamed from: e0 */
    private PeriodTimeInfo f8268e0;

    /* renamed from: f0 */
    private PeriodTimeInfo f8269f0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity$a */
    class C4296a implements OnResultListener<ParamJsonData> {
        C4296a(SettingPowerActivity settingPowerActivity) {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22875a(this);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10602a(this);
        }
        ArrayList arrayList = new ArrayList();
        if (FSManager.m10323r().mo22847q()) {
            this.f8267d0 = BandDataManager.m10556b();
            MenuItem.C3676b bVar = new MenuItem.C3676b();
            bVar.mo22663b(2);
            bVar.mo22664b(getString(R$string.device_light));
            bVar.mo22660a(this.f8267d0.mo26407e());
            arrayList.add(bVar.mo22661a());
            MenuItem.C3676b bVar2 = new MenuItem.C3676b();
            bVar2.mo22663b(2);
            bVar2.mo22664b(getString(R$string.device_vibrate));
            bVar2.mo22660a(this.f8267d0.mo26409g());
            bVar2.mo22662b();
            arrayList.add(bVar2.mo22661a());
            arrayList.add(new MenuItem.C3676b().mo22661a());
            MenuItem.C3676b bVar3 = new MenuItem.C3676b();
            bVar3.mo22663b(2);
            bVar3.mo22664b(getString(R$string.device_quite));
            bVar3.mo22660a(this.f8267d0.mo26409g());
            arrayList.add(bVar3.mo22661a());
            String format = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8267d0.mo26401c()), Integer.valueOf(this.f8267d0.mo26404d()));
            String format2 = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8267d0.mo26395a()), Integer.valueOf(this.f8267d0.mo26398b()));
            MenuItem.C3676b bVar4 = new MenuItem.C3676b();
            bVar4.mo22663b(1);
            bVar4.mo22664b(getString(R$string.device_quite_start_time));
            bVar4.mo22659a(format);
            arrayList.add(bVar4.mo22661a());
            MenuItem.C3676b bVar5 = new MenuItem.C3676b();
            bVar5.mo22663b(1);
            bVar5.mo22664b(getString(R$string.device_quite_end_time));
            bVar5.mo22659a(format2);
            bVar5.mo22662b();
            arrayList.add(bVar5.mo22661a());
            arrayList.add(new MenuItem.C3676b().mo22661a());
            MenuItem.C3676b bVar6 = new MenuItem.C3676b();
            bVar6.mo22663b(4);
            bVar6.mo22664b(getString(FSManager.m10323r().mo22847q() ? R$string.device_quite_tips : R$string.device_quite_crp_tips));
            bVar6.mo22662b();
            arrayList.add(bVar6.mo22661a());
        } else if (FSManager.m10323r().mo22846p()) {
            this.f8268e0 = BandDataManager.m10555b("QuickView");
            this.f8269f0 = BandDataManager.m10555b("DoNotDisturb");
            MenuItem.C3676b bVar7 = new MenuItem.C3676b();
            bVar7.mo22663b(2);
            bVar7.mo22664b(getString(R$string.device_light));
            bVar7.mo22660a(this.f8268e0.mo22593g());
            arrayList.add(bVar7.mo22661a());
            String format3 = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8268e0.mo22585c()), Integer.valueOf(this.f8268e0.mo22587d()));
            String format4 = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8268e0.mo22580a()), Integer.valueOf(this.f8268e0.mo22583b()));
            MenuItem.C3676b bVar8 = new MenuItem.C3676b();
            bVar8.mo22663b(1);
            bVar8.mo22664b(getString(R$string.device_quite_start_time));
            bVar8.mo22659a(format3);
            arrayList.add(bVar8.mo22661a());
            MenuItem.C3676b bVar9 = new MenuItem.C3676b();
            bVar9.mo22663b(1);
            bVar9.mo22664b(getString(R$string.device_quite_end_time));
            bVar9.mo22659a(format4);
            bVar9.mo22662b();
            arrayList.add(bVar9.mo22661a());
            arrayList.add(new MenuItem.C3676b().mo22661a());
            MenuItem.C3676b bVar10 = new MenuItem.C3676b();
            bVar10.mo22663b(2);
            bVar10.mo22664b(getString(R$string.device_quite));
            bVar10.mo22660a(this.f8269f0.mo22593g());
            arrayList.add(bVar10.mo22661a());
            String format5 = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8269f0.mo22585c()), Integer.valueOf(this.f8269f0.mo22587d()));
            String format6 = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(this.f8269f0.mo22580a()), Integer.valueOf(this.f8269f0.mo22583b()));
            MenuItem.C3676b bVar11 = new MenuItem.C3676b();
            bVar11.mo22663b(1);
            bVar11.mo22664b(getString(R$string.device_quite_start_time));
            bVar11.mo22659a(format5);
            arrayList.add(bVar11.mo22661a());
            MenuItem.C3676b bVar12 = new MenuItem.C3676b();
            bVar12.mo22663b(1);
            bVar12.mo22664b(getString(R$string.device_quite_end_time));
            bVar12.mo22659a(format6);
            bVar12.mo22662b();
            arrayList.add(bVar12.mo22661a());
            arrayList.add(new MenuItem.C3676b().mo22661a());
            MenuItem.C3676b bVar13 = new MenuItem.C3676b();
            bVar13.mo22663b(4);
            bVar13.mo22664b(getString(FSManager.m10323r().mo22847q() ? R$string.device_quite_tips : R$string.device_quite_crp_tips));
            bVar13.mo22662b();
            arrayList.add(bVar13.mo22661a());
        }
        this.f8266c0 = new ListMenuAdapter(super);
        this.f8266c0.mo22792b((List) arrayList);
        this.f8266c0.mo24646a((ListMenuAdapter.C4405a) new C4327r(this));
        this.f8266c0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4328s(this));
        this.f8265b0.setAdapter(this.f8266c0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8265b0 = (RecyclerView) findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.setting_power);
    }

    /* renamed from: b */
    private void m13846b(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(z ? "開啟" : "關閉");
        Constant.m10311a(sb.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity.a(com.heimavista.fiedorasport.h.y$b, boolean):void
     arg types: [com.heimavista.fiedorasport.h.y$b, int]
     candidates:
      com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity.a(com.heimavista.entity.data.e, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity.a(com.heimavista.fiedorasport.h.y$b, boolean):void */
    /* renamed from: a */
    public /* synthetic */ void mo24487a(MenuItem eVar, boolean z) {
        if (eVar.mo22652d().equals(getString(R$string.device_light))) {
            m13846b("抬手亮屏", z);
            DeviceProfile deviceProfile = this.f8267d0;
            if (deviceProfile != null) {
                deviceProfile.mo26397a(z);
            }
            PeriodTimeInfo periodTimeInfo = this.f8268e0;
            if (periodTimeInfo != null) {
                periodTimeInfo.mo22582a(z);
                CRPBleManager.m10614b(this.f8268e0);
            }
        } else if (eVar.mo22652d().equals(getString(R$string.device_vibrate))) {
            m13846b("設備震動", z);
            DeviceProfile deviceProfile2 = this.f8267d0;
            if (deviceProfile2 != null) {
                deviceProfile2.mo26403c(z);
                mo22964a(C3734y.C3736b.deviceInfo, true);
            }
        } else if (eVar.mo22652d().equals(getString(R$string.device_quite))) {
            m13846b("勿擾模式", z);
            DeviceProfile deviceProfile3 = this.f8267d0;
            if (deviceProfile3 != null) {
                deviceProfile3.mo26400b(z);
            }
            PeriodTimeInfo periodTimeInfo2 = this.f8269f0;
            if (periodTimeInfo2 != null) {
                periodTimeInfo2.mo22582a(z);
                CRPBleManager.m10600a(this.f8269f0);
            }
        }
        if (this.f8267d0 != null) {
            JYSDKManager.m10388i().mo22877a(this.f8267d0);
        }
        mo22712a(false);
        C5217x.task().postDelayed(new C4317i0(this), 500);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity.a(com.heimavista.entity.data.e, int, boolean, boolean):void
     arg types: [com.heimavista.entity.data.e, int, int, boolean]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.ui.activity.setting.SettingPowerActivity.a(com.heimavista.entity.data.e, int, boolean, boolean):void */
    /* renamed from: a */
    public /* synthetic */ void mo24485a(View view, MenuItem eVar, int i) {
        if (eVar.mo22653e() == 2) {
            CommonListItemView commonListItemView = (CommonListItemView) view.findViewById(R$id.listItemView);
            if (commonListItemView != null && commonListItemView.getAccessoryType() == 2) {
                commonListItemView.getSwitch().toggle();
                return;
            }
            return;
        }
        boolean z = true;
        if (eVar.mo22653e() == 1) {
            boolean z2 = false;
            if (getString(R$string.device_quite_start_time).equals(eVar.mo22652d())) {
                if (i < 3) {
                    z2 = true;
                }
                m13845a(eVar, i, true, z2);
            } else if (getString(R$string.device_quite_end_time).equals(eVar.mo22652d())) {
                if (i >= 3) {
                    z = false;
                }
                m13845a(eVar, i, false, z);
            }
        }
    }

    /* renamed from: a */
    private void m13845a(MenuItem eVar, int i, boolean z, boolean z2) {
        int i2;
        int i3;
        if (z2) {
            PeriodTimeInfo periodTimeInfo = this.f8268e0;
            i3 = z ? periodTimeInfo.mo22585c() : periodTimeInfo.mo22580a();
            PeriodTimeInfo periodTimeInfo2 = this.f8268e0;
            i2 = z ? periodTimeInfo2.mo22587d() : periodTimeInfo2.mo22583b();
        } else {
            DeviceProfile deviceProfile = this.f8267d0;
            if (deviceProfile != null) {
                i3 = z ? deviceProfile.mo26401c() : deviceProfile.mo26395a();
                DeviceProfile deviceProfile2 = this.f8267d0;
                i2 = z ? deviceProfile2.mo26404d() : deviceProfile2.mo26398b();
            } else {
                PeriodTimeInfo periodTimeInfo3 = this.f8269f0;
                i3 = z ? periodTimeInfo3.mo22585c() : periodTimeInfo3.mo22580a();
                PeriodTimeInfo periodTimeInfo4 = this.f8269f0;
                i2 = z ? periodTimeInfo4.mo22587d() : periodTimeInfo4.mo22583b();
            }
        }
        TimePicker a = PickerManager.m10514a(this, i3, i2);
        a.mo23107a(new C4329t(this, eVar, i, z, z2));
        a.mo23129f();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24486a(MenuItem eVar, int i, boolean z, boolean z2, String str, String str2) {
        eVar.mo22644a(str + ":" + str2);
        this.f8266c0.notifyItemChanged(i, eVar);
        try {
            int parseInt = Integer.parseInt(str);
            int parseInt2 = Integer.parseInt(str2);
            String str3 = "開始時間";
            if (this.f8267d0 != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("勿擾模式");
                if (!z) {
                    str3 = "結束時間";
                }
                sb.append(str3);
                Constant.m10311a(sb.toString());
                if (z) {
                    this.f8267d0.mo26402c(parseInt);
                    this.f8267d0.mo26405d(parseInt2);
                } else {
                    this.f8267d0.mo26396a(parseInt);
                    this.f8267d0.mo26399b(parseInt2);
                }
                JYSDKManager.m10388i().mo22877a(this.f8267d0);
                mo22712a(false);
                C5217x.task().postDelayed(new C4317i0(this), 500);
            }
            if (!(this.f8268e0 == null || this.f8269f0 == null)) {
                String str4 = "抬手亮屏";
                if (z) {
                    StringBuilder sb2 = new StringBuilder();
                    if (z2) {
                        str4 = "勿擾模式";
                    }
                    sb2.append(str4);
                    sb2.append(str3);
                    Constant.m10311a(sb2.toString());
                    if (z2) {
                        this.f8268e0.mo22586c(parseInt);
                        this.f8268e0.mo22588d(parseInt2);
                    } else {
                        this.f8269f0.mo22586c(parseInt);
                        this.f8269f0.mo22588d(parseInt2);
                    }
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    if (z2) {
                        str4 = "勿擾模式";
                    }
                    sb3.append(str4);
                    sb3.append("結束時間");
                    Constant.m10311a(sb3.toString());
                    if (z2) {
                        this.f8268e0.mo22581a(parseInt);
                        this.f8268e0.mo22584b(parseInt2);
                    } else {
                        this.f8269f0.mo22581a(parseInt);
                        this.f8269f0.mo22584b(parseInt2);
                    }
                }
                if (z2) {
                    CRPBleManager.m10614b(this.f8268e0);
                } else {
                    CRPBleManager.m10600a(this.f8269f0);
                }
            }
            mo22712a(false);
            C5217x.task().postDelayed(new C4317i0(this), 500);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo22964a(C3734y.C3736b bVar, boolean z) {
        if (bVar == C3734y.C3736b.deviceInfo) {
            DeviceProfile deviceProfile = this.f8267d0;
            if (deviceProfile != null) {
                BandDataManager.m10553a(deviceProfile);
            }
            if (mo22741l()) {
                new HvApiPostBandSettings(new C4296a(this)).requestPostDeviceProfile();
            }
        }
    }
}
