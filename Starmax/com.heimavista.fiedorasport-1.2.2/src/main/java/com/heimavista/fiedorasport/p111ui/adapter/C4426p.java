package com.heimavista.fiedorasport.p111ui.adapter;

import com.heimavista.entity.data.MenuItem;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.p */
/* compiled from: lambda */
public final /* synthetic */ class C4426p implements SwitchButton.C4603d {

    /* renamed from: a */
    private final /* synthetic */ ListNotificationAdapter f8691a;

    /* renamed from: b */
    private final /* synthetic */ MenuItem f8692b;

    public /* synthetic */ C4426p(ListNotificationAdapter listNotificationAdapter, MenuItem eVar) {
        this.f8691a = listNotificationAdapter;
        this.f8692b = eVar;
    }

    /* renamed from: a */
    public final void mo24507a(SwitchButton switchButton, boolean z) {
        this.f8691a.mo24648a(this.f8692b, switchButton, z);
    }
}
