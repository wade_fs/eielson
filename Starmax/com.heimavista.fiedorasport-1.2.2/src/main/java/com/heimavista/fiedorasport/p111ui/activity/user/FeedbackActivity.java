package com.heimavista.fiedorasport.p111ui.activity.user;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import com.blankj.utilcode.util.UriUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiFeedback;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.PhotoUtils;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import java.io.File;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.FeedbackActivity */
public class FeedbackActivity extends BaseActivity {

    /* renamed from: b0 */
    private EditText f8526b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public ImageView f8527c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public File f8528d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public PhotoUtils f8529e0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.FeedbackActivity$a */
    class C4379a implements C4632i<String> {
        C4379a() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            if (i == 0) {
                FeedbackActivity.this.f8529e0.mo24366b(FeedbackActivity.this, 500);
                return;
            }
            File unused = FeedbackActivity.this.f8528d0 = (File) null;
            FeedbackActivity.this.f8527c0.setScaleType(ImageView.ScaleType.CENTER);
            FeedbackActivity.this.f8527c0.setImageResource(R$mipmap.activity_add);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.FeedbackActivity$b */
    class C4380b implements OnResultListener<ParamJsonData> {
        C4380b() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            Constant.m10311a("意見反饋成功");
            FeedbackActivity.this.mo22717b();
            FeedbackActivity.this.finish();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            Constant.m10311a("意見反饋失敗");
            FeedbackActivity.this.mo22717b();
            FeedbackActivity.this.mo22730c(str);
        }
    }

    /* renamed from: u */
    private void m14188u() {
        if (TextUtils.isEmpty(this.f8526b0.getText())) {
            mo22730c(getString(R$string.feedback_tips));
        } else if (mo22741l()) {
            mo22751t();
            HvApiFeedback.C3653a aVar = new HvApiFeedback.C3653a();
            aVar.mo22443a(this.f8526b0.getText().toString());
            File file = this.f8528d0;
            if (file != null) {
                aVar.mo22442a(file);
            }
            new HvApiFeedback().feedback(new C4380b(), aVar.mo22444a());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_feedback;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.feedback);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        Uri data;
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 500 && intent != null && (data = intent.getData()) != null) {
            this.f8527c0.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.f8527c0.setImageURI(data);
            this.f8528d0 = UriUtils.m1052a(data);
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.f6594iv) {
            if (this.f8529e0 == null) {
                this.f8529e0 = new PhotoUtils();
            }
            if (this.f8528d0 == null) {
                this.f8529e0.mo24366b(this, 500);
                return;
            }
            C4629g gVar = new C4629g(this);
            gVar.mo25456a(getString(R$string.album), getString(R$string.delete));
            gVar.mo25453a(new C4379a());
            gVar.mo25418b().show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mo22680a(menu, 16908331, getString(R$string.send), getResources().getColor(R$color.font_color), (Drawable) null, new C4389b(this));
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8526b0 = (EditText) findViewById(R$id.f6593et);
        this.f8527c0 = (ImageView) findViewById(R$id.f6594iv);
        mo22723b(R$id.f6594iv);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24577a(View view) {
        m14188u();
    }
}
