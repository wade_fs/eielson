package com.heimavista.fiedorasport.p109h;

import android.bluetooth.BluetoothDevice;
import android.graphics.Bitmap;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.LongSparseArray;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.crrepa.ble.C1133a;
import com.crrepa.ble.p049d.C1137a;
import com.crrepa.ble.p049d.C1138b;
import com.crrepa.ble.p049d.p052f.C1163a;
import com.crrepa.ble.p049d.p052f.C1165c;
import com.crrepa.ble.p049d.p052f.C1167d;
import com.crrepa.ble.p049d.p052f.C1168e;
import com.crrepa.ble.p049d.p052f.C1170g;
import com.crrepa.ble.p049d.p052f.C1172i;
import com.crrepa.ble.p049d.p052f.C1174j;
import com.crrepa.ble.p049d.p052f.C1175k;
import com.crrepa.ble.p049d.p052f.C1177m;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p049d.p054h.C1185f;
import com.crrepa.ble.p049d.p054h.C1193n;
import com.crrepa.ble.p049d.p054h.C1202w;
import com.crrepa.ble.p049d.p063q.C1260b;
import com.crrepa.ble.p049d.p063q.C1263e;
import com.crrepa.ble.p049d.p063q.C1264f;
import com.crrepa.ble.p049d.p063q.C1267i;
import com.crrepa.ble.p049d.p063q.C1268j;
import com.crrepa.ble.p049d.p063q.C1271m;
import com.crrepa.ble.p049d.p063q.C1272n;
import com.crrepa.ble.p049d.p063q.C1273o;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.crrepa.ble.p072i.p073a.C1306a;
import com.crrepa.ble.p072i.p074b.C1307a;
import com.heimavista.api.band.HvApiPostCurStepData;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.entity.band.TickType;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.UserProfile;
import com.tencent.mmkv.MMKV;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.C5217x;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p193d.p194i.StepDataDb;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;

/* renamed from: com.heimavista.fiedorasport.h.z */
public class CRPBleManager {

    /* renamed from: a */
    private static C1133a f6560a = null;

    /* renamed from: b */
    private static C1138b f6561b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static C1137a f6562c = null;

    /* renamed from: d */
    private static boolean f6563d = false;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static HashMap<String, C3734y> f6564e = new HashMap<>();

    /* renamed from: f */
    private static C3734y.C3735a f6565f;

    /* renamed from: g */
    private static C1273o f6566g = new C3737a();

    /* renamed from: h */
    private static C1272n f6567h = new C3738b();

    /* renamed from: i */
    private static C1271m f6568i = new C3740c();

    /* renamed from: j */
    private static C1268j f6569j = new C3741d();

    /* renamed from: k */
    private static C1264f f6570k = C3720i.f6538a;

    /* renamed from: l */
    private static C1263e f6571l = C3725n.f6543a;

    /* renamed from: m */
    private static C1267i f6572m = C3718g.f6536a;

    /* renamed from: n */
    private static C1260b f6573n = new C3742e();

    /* renamed from: com.heimavista.fiedorasport.h.z$a */
    /* compiled from: CRPBleManager */
    static class C3737a implements C1273o {
        C3737a() {
        }

        /* renamed from: a */
        public void mo11029a(C1175k kVar) {
            CRPBleManager.m10623c("onStepsCategoryChange type=" + kVar.mo10854a() + " interval=" + kVar.mo10856c() + " " + kVar.mo10855b());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.z$b */
    /* compiled from: CRPBleManager */
    static class C3738b implements C1272n {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public HttpCancelable f6574a;

        /* renamed from: b */
        private long f6575b = 0;

        /* renamed from: c */
        private CountDownTimer f6576c;

        /* renamed from: com.heimavista.fiedorasport.h.z$b$a */
        /* compiled from: CRPBleManager */
        class C3739a extends CountDownTimer {

            /* renamed from: a */
            final /* synthetic */ long f6577a;

            /* renamed from: b */
            final /* synthetic */ StepInfo f6578b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            C3739a(long j, long j2, long j3, StepInfo stepInfo) {
                super(j, j2);
                this.f6577a = j3;
                this.f6578b = stepInfo;
            }

            public void onFinish() {
                C3738b bVar = C3738b.this;
                HvApiPostCurStepData hvApiPostCurStepData = new HvApiPostCurStepData();
                long j = this.f6577a;
                StepInfo stepInfo = this.f6578b;
                HttpCancelable unused = bVar.f6574a = hvApiPostCurStepData.request(j, stepInfo.f6245S, stepInfo.f6246T, stepInfo.f6247U);
            }

            public void onTick(long j) {
            }
        }

        C3738b() {
        }

        /* renamed from: b */
        private void m10656b(int i, C1174j jVar) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (i < 0) {
                currentTimeMillis = TimeUtil.m13448d(i).getTimeInMillis() / 1000;
            }
            StepInfo stepInfo = new StepInfo(MemberControl.m17125s().mo27187l(), currentTimeMillis, TimeUtil.m13442a(), jVar.mo10853c(), jVar.mo10852b(), jVar.mo10851a());
            if (i == 0) {
                CurStepDataDb.m13135a(stepInfo);
                if (BuddyListDb.m13245F() > 0 && StepDataDb.m13223f(0).f6245S != stepInfo.f6245S) {
                    if (currentTimeMillis - this.f6575b > 30) {
                        HttpCancelable bVar = this.f6574a;
                        if (bVar != null) {
                            bVar.cancel();
                        }
                        this.f6574a = new HvApiPostCurStepData().request(currentTimeMillis, stepInfo.f6245S, stepInfo.f6246T, stepInfo.f6247U);
                        this.f6575b = currentTimeMillis;
                        CountDownTimer countDownTimer = this.f6576c;
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                            this.f6576c = null;
                        }
                    } else {
                        CountDownTimer countDownTimer2 = this.f6576c;
                        if (countDownTimer2 != null) {
                            countDownTimer2.cancel();
                            this.f6576c = null;
                        }
                        this.f6576c = new C3739a(30000, 1000, currentTimeMillis, stepInfo);
                        this.f6576c.start();
                    }
                }
                for (Map.Entry entry : CRPBleManager.f6564e.entrySet()) {
                    ((C3734y) entry.getValue()).mo22690a(currentTimeMillis, jVar.mo10853c(), jVar.mo10852b(), jVar.mo10851a(), 0);
                }
            }
            StepDataDb.m13218a(stepInfo);
        }

        /* renamed from: a */
        public void mo11028a(C1174j jVar) {
            CRPBleManager.m10623c("onStepChange steps=" + jVar.mo10853c() + " cal=" + jVar.mo10851a() + " distance=" + jVar.mo10852b());
            TickManager.m10524b(TickType.STEP, System.currentTimeMillis() / 1000);
            m10656b(0, jVar);
        }

        /* renamed from: a */
        public void mo11027a(int i, C1174j jVar) {
            CRPBleManager.m10623c("onPastStepChange type=" + i + " steps=" + jVar.mo10853c() + " cal=" + jVar.mo10851a() + " distance=" + jVar.mo10852b());
            if (i == 1) {
                m10656b(-1, jVar);
            } else if (i == 2) {
                m10656b(-2, jVar);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.z$e */
    /* compiled from: CRPBleManager */
    static class C3742e implements C1260b {
        C3742e() {
        }

        /* renamed from: a */
        public void mo11006a(int[] iArr) {
            CRPBleManager.m10623c("ECG onECGChange");
        }

        /* renamed from: b */
        public void mo11007b() {
            CRPBleManager.m10623c("ECG onMeasureComplete");
        }

        public void onCancel() {
            CRPBleManager.m10623c("ECG onCancel");
        }

        /* renamed from: a */
        public void mo11005a(Date date) {
            CRPBleManager.m10623c("ECG onTransComplete");
        }

        /* renamed from: a */
        public void mo11004a() {
            CRPBleManager.m10623c("onFail");
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.z$f */
    /* compiled from: CRPBleManager */
    static class C3743f implements C1307a {
        C3743f() {
        }

        /* renamed from: a */
        public void mo11075a(C1306a aVar) {
            BluetoothDevice a = aVar.mo11073a();
            String name = a.getName();
            String address = a.getAddress();
            int b = aVar.mo11074b();
            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(address)) {
                CRPBleManager.m10623c("scanDevice onScanCallback: [" + name + "][" + address + "][" + b + "]");
                for (Map.Entry entry : CRPBleManager.f6564e.entrySet()) {
                    ((C3734y) entry.getValue()).mo22706a(name, address, b);
                }
            }
        }

        /* renamed from: a */
        public void mo11076a(List<C1306a> list) {
        }
    }

    /* renamed from: a */
    public static int m10584a(int i) {
        return (((63488 & i) >> 8) << 16) + (((i & 2016) >> 3) << 8) + ((i & 31) << 3);
    }

    /* renamed from: c */
    static /* synthetic */ void m10621c(int i, int i2) {
        m10623c("queryGoalStep step=" + i2);
        C3734y.C3735a aVar = f6565f;
        if (aVar != null) {
            aVar.mo22964a(C3734y.C3736b.goalStep, i2 == i);
        }
    }

    /* renamed from: d */
    public static void m10628d(String str) {
        HashMap<String, C3734y> hashMap = f6564e;
        if (hashMap != null) {
            hashMap.remove(str);
        }
        f6565f = null;
    }

    /* renamed from: f */
    static /* synthetic */ void m10633f(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("queryDeviceVersion=");
        sb.append(i);
        sb.append(" ");
        sb.append(i == 0 ? "大陆版" : "国际版");
        m10623c(sb.toString());
    }

    /* renamed from: g */
    private static MMKV m10634g() {
        return MMKV.m17083e("crpWatch-" + FSManager.m10323r().mo22842l());
    }

    /* renamed from: h */
    public static void m10637h(int i) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10763b(i);
            f6562c.mo10742a(new C3713c(i));
        }
    }

    /* renamed from: i */
    public static int m10638i() {
        return m10634g().mo27024a("watch-type", 2);
    }

    /* renamed from: j */
    public static boolean m10639j() {
        m10623c("isConnected=" + f6563d);
        return f6563d;
    }

    /* renamed from: l */
    public static synchronized void m10641l() {
        synchronized (CRPBleManager.class) {
            if (f6562c != null) {
                f6562c.mo10772e();
            }
        }
    }

    /* renamed from: m */
    public static void m10642m() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10732a(60);
        }
    }

    /* renamed from: n */
    public static void m10643n() {
        HashMap<String, C3734y> hashMap = f6564e;
        if (hashMap != null) {
            hashMap.clear();
        }
        f6565f = null;
    }

    /* renamed from: o */
    public static void m10644o() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10777i();
        }
    }

    /* renamed from: p */
    public static void m10645p() {
        if (f6560a == null) {
            f6560a = C1133a.m2876a(HvApp.m13010c());
        }
    }

    /* renamed from: q */
    public static void m10646q() {
        if (f6560a == null) {
            f6560a = C1133a.m2876a(HvApp.m13010c());
        }
        f6560a.mo10720a(new C3743f(), 10000);
    }

    /* renamed from: r */
    public static void m10647r() {
        C1138b bVar = f6561b;
        if (bVar != null) {
            bVar.mo10778a();
        }
        f6562c = null;
        f6561b = null;
        f6560a = null;
        f6563d = false;
        m10643n();
    }

    /* renamed from: s */
    public static void m10648s() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10775g();
        }
    }

    /* renamed from: t */
    public static void m10649t() {
        if (f6562c != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(TickManager.m10517a(TickType.SLEEP) * 1000);
            if (!TimeUtils.m1014c(instance.getTime()) || instance.get(11) < 6) {
                f6562c.mo10770d();
            }
            m10632f();
            m10652w();
            if (TimeUtils.m1007a(HeartRateDataDb.m13158t() * 1000)) {
                f6562c.mo10769c(1);
            } else {
                f6562c.mo10761b();
                f6562c.mo10769c(1);
            }
            f6562c.mo10767c();
        }
    }

    /* renamed from: u */
    public static synchronized void m10650u() {
        synchronized (CRPBleManager.class) {
            if (f6562c != null) {
                f6562c.mo10774f();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.h.z.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.heimavista.fiedorasport.h.z.a(int, int):void
      com.heimavista.fiedorasport.h.z.a(int, int[]):void
      com.heimavista.fiedorasport.h.z.a(com.crrepa.ble.d.a, com.heimavista.entity.data.PeriodTimeInfo):void
      com.heimavista.fiedorasport.h.z.a(com.crrepa.ble.d.f.n, com.crrepa.ble.d.h.w):void
      com.heimavista.fiedorasport.h.z.a(com.crrepa.ble.d.h.f, int):void
      com.heimavista.fiedorasport.h.z.a(com.crrepa.ble.d.h.w, com.crrepa.ble.d.f.n):void
      com.heimavista.fiedorasport.h.z.a(com.heimavista.entity.data.PeriodTimeInfo, boolean):void
      com.heimavista.fiedorasport.h.z.a(java.lang.String, int):void
      com.heimavista.fiedorasport.h.z.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
      com.heimavista.fiedorasport.h.z.a(boolean, boolean):void
      com.heimavista.fiedorasport.h.z.a(int, boolean):void */
    /* renamed from: v */
    public static void m10651v() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10730a();
            m10613b(f6562c);
            m10589a(BandDataManager.m10565g(), false);
            m10592a(f6562c);
            m10593a(f6562c, (PeriodTimeInfo) null);
            m10631e(BandDataManager.m10555b("QuickView"));
            m10627d(BandDataManager.m10555b("DoNotDisturb"));
            m10653x();
            m10596a((C1185f) null);
            m10598a((C1202w) null);
        }
    }

    /* renamed from: w */
    public static void m10652w() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10774f();
            f6562c.mo10731a((byte) 1);
            f6562c.mo10731a((byte) 2);
        }
    }

    /* renamed from: x */
    private static void m10653x() {
        UserProfile h = BandDataManager.m10566h();
        f6562c.mo10737a(new C1177m(h.mo26423f(), h.mo26417c(), h.mo26415b(), h.mo26413a()));
    }

    /* renamed from: e */
    private static void m10631e(PeriodTimeInfo periodTimeInfo) {
        f6562c.mo10766b(periodTimeInfo.mo22593g());
        f6562c.mo10735a(new C1168e(periodTimeInfo.mo22585c(), periodTimeInfo.mo22587d(), periodTimeInfo.mo22580a(), periodTimeInfo.mo22583b()));
    }

    /* renamed from: f */
    public static void m10632f() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10740a(C3726o.f6544a);
        }
    }

    /* renamed from: g */
    public static void m10635g(int i) {
        m10634g().mo27039b("watch-type", i);
    }

    /* renamed from: a */
    public static void m10606a(String str, C3734y yVar) {
        if (f6564e == null) {
            f6564e = new HashMap<>();
        }
        if (!f6564e.containsKey(str)) {
            f6564e.put(str, yVar);
        }
    }

    /* renamed from: b */
    static /* synthetic */ void m10616b(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            m10623c("queryAllAlarmClock:" + ((C1163a) it.next()).toString());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.h.z$d */
    /* compiled from: CRPBleManager */
    static class C3741d implements C1268j {
        C3741d() {
        }

        /* renamed from: a */
        public void mo11018a(int i) {
            CRPBleManager.m10623c("CRPHeartRateChangListener onOnceMeasureComplete " + i);
            HeartRateDataDb.m13151a(MemberControl.m17125s().mo27187l(), System.currentTimeMillis() / 1000, i, 1);
            for (Map.Entry entry : CRPBleManager.f6564e.entrySet()) {
                ((C3734y) entry.getValue()).mo22727c(i);
            }
        }

        /* renamed from: b */
        public void mo11021b(int i) {
            CRPBleManager.m10623c("CRPHeartRateChangListener onMeasuring " + i);
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            for (Map.Entry entry : CRPBleManager.f6564e.entrySet()) {
                ((C3734y) entry.getValue()).mo22687a(currentTimeMillis, i);
            }
        }

        /* renamed from: a */
        public void mo11019a(C1165c cVar) {
            List<Integer> b = cVar.mo10794b();
            StringBuilder sb = new StringBuilder("heart onMeasureComplete " + TimeUtils.m1009b(cVar.mo10795c()));
            sb.append(" interval=");
            sb.append(cVar.mo10796d());
            sb.append(" type=");
            sb.append(cVar.mo10793a());
            sb.append(" data=");
            if (b != null) {
                for (Integer num : b) {
                    sb.append(num);
                    sb.append(",");
                }
            }
            CRPBleManager.m10623c("CRPHeartRateChangListener onMeasureComplete:" + sb.toString());
        }

        /* renamed from: b */
        public void mo11022b(C1165c cVar) {
            int d = cVar.mo10796d();
            List<Integer> b = cVar.mo10794b();
            int size = b.size();
            CRPBleManager.m10623c("CRPHeartRateChangListener on24HourMeasureResult start=" + cVar.mo10795c() + " type=" + cVar.mo10793a() + " interval=" + d + " size=" + size);
            int i = 1440 / d;
            if (i > size) {
                size = i;
            }
            LongSparseArray<Integer> b2 = HeartRateDataDb.m13153b(cVar.mo10795c());
            for (int i2 = 0; i2 < size; i2++) {
                int intValue = b.get(i2).intValue();
                if (intValue > 0) {
                    long c = (cVar.mo10795c() + ((long) (((i2 * d) * 60) * 1000))) / 1000;
                    if (b2.indexOfKey(c) < 0) {
                        b2.put(c, Integer.valueOf(intValue));
                        HeartRateDataDb.m13151a(MemberControl.m17125s().mo27187l(), c, intValue, 0);
                    }
                    CRPBleManager.m10623c("on24HourMeasureResult " + TimeUtils.m1009b(1000 * c) + " rate=" + intValue + " " + b2.indexOfKey(c));
                }
            }
            C5217x.task().post(C3703a.f6479P);
        }

        /* renamed from: a */
        public void mo11020a(List<C1167d> list) {
            if (list != null && !list.isEmpty()) {
                CRPBleManager.m10623c("CRPHeartRateChangListener onMovementMeasureResult:" + list.size());
                Iterator<C1167d> it = list.iterator();
                while (it.hasNext()) {
                    C1167d next = it.next();
                    StringBuilder sb = new StringBuilder();
                    sb.append("rateInfo ");
                    sb.append(next == null ? "is null" : "not null");
                    CRPBleManager.m10623c(sb.toString());
                    if (next != null) {
                        CRPBleManager.m10623c("CRPMovementHeartRateInfo: type=" + next.mo10809f() + ",step=" + next.mo10807e() + ",cal=" + next.mo10797a() + ",distance=" + next.mo10800b() + ",steps=" + next.mo10807e() + ",startTime=" + next.mo10805d() + ",endTime=" + next.mo10803c());
                        int f = next.mo10809f();
                        if (f == 0 || f == 1 || f == 2) {
                            SportInfo dVar = new SportInfo();
                            if (f == 0) {
                                dVar.f6273d = 1;
                            } else if (f == 1) {
                                dVar.f6273d = 0;
                            } else {
                                dVar.f6273d = 2;
                            }
                            dVar.f6278i = next.mo10797a();
                            dVar.f6277h = next.mo10800b();
                            dVar.f6276g = next.mo10810g();
                            dVar.f6274e = next.mo10805d();
                            dVar.f6275f = next.mo10803c();
                            CRPBleManager.m10623c("sport:" + dVar.toString());
                        }
                    }
                }
            }
        }
    }

    /* renamed from: c */
    public static void m10619c() {
        C1133a aVar = f6560a;
        if (aVar != null) {
            aVar.mo10719a();
        }
    }

    /* renamed from: d */
    private static void m10627d(PeriodTimeInfo periodTimeInfo) {
        C1168e eVar = new C1168e(0, 0, 0, 0);
        if (periodTimeInfo.mo22593g()) {
            eVar = new C1168e(periodTimeInfo.mo22585c(), periodTimeInfo.mo22587d(), periodTimeInfo.mo22580a(), periodTimeInfo.mo22583b());
        }
        f6562c.mo10764b(eVar);
    }

    /* renamed from: e */
    public static void m10629e() {
        C1138b bVar = f6561b;
        if (bVar != null) {
            bVar.mo10778a();
        } else if (f6560a != null && !TextUtils.isEmpty(SPUtils.m1243c("crp").mo9871a("device_address"))) {
            f6561b = f6560a.mo10718a(SPUtils.m1243c("crp").mo9871a("device_address"));
            C1138b bVar2 = f6561b;
            if (bVar2 != null) {
                bVar2.mo10778a();
            }
        }
    }

    /* renamed from: h */
    public static C1178n m10636h() {
        MMKV g = m10634g();
        C1178n nVar = new C1178n();
        nVar.mo10862a(g.mo27038b("tColor"));
        nVar.mo10867c(g.mo27038b("tPos"));
        nVar.mo10869d(g.mo27038b("ttContent"));
        nVar.mo10865b(g.mo27038b("tbContent"));
        nVar.mo10863a(g.mo27044c("picmd5"));
        return nVar;
    }

    /* renamed from: b */
    static /* synthetic */ void m10618b(boolean z, boolean z2) {
        m10623c("querySedentaryReminder open=" + z2);
        C3734y.C3735a aVar = f6565f;
        if (aVar != null) {
            aVar.mo22964a(C3734y.C3736b.sedentary, (z && z2) || (!z && !z2));
        }
    }

    /* renamed from: c */
    static /* synthetic */ void m10624c(boolean z) {
        PeriodTimeInfo b = BandDataManager.m10555b("QuickView");
        b.mo22582a(z);
        BandDataManager.m10550a(b);
    }

    /* renamed from: a */
    public static void m10602a(C3734y.C3735a aVar) {
        f6565f = aVar;
    }

    /* renamed from: com.heimavista.fiedorasport.h.z$c */
    /* compiled from: CRPBleManager */
    static class C3740c implements C1271m {
        C3740c() {
        }

        /* renamed from: a */
        private int m10659a(int i) {
            if (i == 1) {
                return 2;
            }
            return i == 2 ? 1 : 3;
        }

        /* renamed from: a */
        private long m10660a(long j, int i) {
            if (i > 600) {
                i -= 1440;
            }
            return j + ((long) (i * 60));
        }

        /* renamed from: a */
        public void mo11026a(C1172i iVar) {
            CRPBleManager.m10623c("onSleepChange total=" + iVar.mo10842e() + "awake=" + iVar.mo10840d() + " light=" + iVar.mo10836b() + " deep=" + iVar.mo10838c());
            TimeUtil.m13448d(-2).add(11, -4);
            m10661a(0, iVar.mo10833a());
            if (CRPBleManager.f6562c != null) {
                CRPBleManager.f6562c.mo10768c((byte) 3);
                CRPBleManager.f6562c.mo10768c((byte) 4);
            }
            TickManager.m10524b(TickType.SLEEP, System.currentTimeMillis() / 1000);
        }

        /* renamed from: a */
        public void mo11025a(int i, C1172i iVar) {
            CRPBleManager.m10623c("onSleepChange type=" + i + " total=" + iVar.mo10842e() + "awake=" + iVar.mo10840d() + " light=" + iVar.mo10836b() + " deep=" + iVar.mo10838c());
            if (i == 3) {
                m10661a(-1, iVar.mo10833a());
            } else if (i == 4) {
                m10661a(-2, iVar.mo10833a());
            }
        }

        /* renamed from: a */
        private void m10661a(int i, List<C1172i.C1173a> list) {
            String str;
            int i2;
            int i3 = i;
            List<C1172i.C1173a> list2 = list;
            String str2 = ":";
            long timeInMillis = TimeUtil.m13448d(i).getTimeInMillis() / 1000;
            CRPBleManager.m10623c("day=" + i3 + " startOfDay=" + TimeUtils.m1009b(timeInMillis * 1000));
            if (list2 != null && !list.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                ArrayList<SleepInfo> arrayList = new ArrayList<>();
                int size = list.size() - 1;
                int i4 = 0;
                int i5 = 0;
                while (i4 < size) {
                    C1172i.C1173a aVar = list2.get(i4);
                    try {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(TtmlNode.START, (aVar.mo10845b() / 60) + str2 + (aVar.mo10845b() % 60));
                        jSONObject.put(TtmlNode.END, (aVar.mo10843a() / 60) + str2 + (aVar.mo10843a() % 60));
                        jSONObject.put("total", aVar.mo10847c());
                        jSONObject.put("type", aVar.mo10849d());
                        jSONArray.put(jSONObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    int i6 = i4 + 1;
                    C1172i.C1173a aVar2 = list2.get(i6);
                    if (i4 == 0) {
                        i5 = aVar.mo10845b();
                    }
                    int i7 = i6;
                    if (aVar.mo10849d() != aVar2.mo10849d()) {
                        int a = aVar.mo10843a();
                        int i8 = a - i5;
                        if (i5 > 600 && a <= 600) {
                            i8 = a - (i5 - 1440);
                        }
                        str = str2;
                        SleepInfo cVar = new SleepInfo(m10660a(timeInMillis, i5), m10659a(aVar.mo10849d()), i8);
                        cVar.f6267f = m10660a(timeInMillis, aVar.mo10843a());
                        arrayList.add(cVar);
                        int b = aVar2.mo10845b();
                        if (i4 == size - 1) {
                            SleepInfo cVar2 = new SleepInfo(m10660a(timeInMillis, aVar2.mo10845b()), m10659a(aVar2.mo10849d()), aVar2.mo10847c());
                            cVar2.f6267f = m10660a(timeInMillis, aVar2.mo10843a());
                            arrayList.add(cVar2);
                        }
                        i5 = b;
                    } else {
                        str = str2;
                        if (i4 == size - 1) {
                            int a2 = aVar2.mo10843a();
                            int i9 = a2 - i5;
                            if (i5 > 600 && a2 <= 600) {
                                i9 = a2 - (i5 - 1440);
                            }
                            i2 = size;
                            SleepInfo cVar3 = new SleepInfo(m10660a(timeInMillis, i5), m10659a(aVar.mo10849d()), i9);
                            cVar3.f6267f = m10660a(timeInMillis, aVar2.mo10843a());
                            arrayList.add(cVar3);
                            size = i2;
                            str2 = str;
                            i4 = i7;
                            list2 = list;
                        }
                    }
                    i2 = size;
                    size = i2;
                    str2 = str;
                    i4 = i7;
                    list2 = list;
                }
                for (SleepInfo cVar4 : arrayList) {
                    CRPBleManager.m10623c("sleepInfo " + TimeUtils.m998a(cVar4.f6266e * 1000, "d h:m") + "~" + TimeUtils.m998a(cVar4.f6267f * 1000, "h:m") + " " + cVar4.f6265d + " " + cVar4.f6268g);
                    SleepDataDb.m13178a(cVar4);
                }
                CRPBleManager.m10623c("SleepInfo day=" + i3 + " " + jSONArray.toString());
            }
            if (i3 == 0) {
                HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onGetSleepDataFinish");
            }
        }
    }

    /* renamed from: a */
    private static C1193n m10585a(boolean z) {
        return new C3724m(z);
    }

    /* renamed from: b */
    private static void m10613b(C1137a aVar) {
        byte b;
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (aVar != null) {
            aVar.mo10762b((byte) 1);
            if ("zh".equalsIgnoreCase(language)) {
                b = "CN".equalsIgnoreCase(country) ? (byte) 1 : 9;
            } else if ("ja".equalsIgnoreCase(language)) {
                b = 2;
            } else if ("ko".equalsIgnoreCase(language)) {
                b = 3;
            } else if ("de".equalsIgnoreCase(language)) {
                b = 4;
            } else if ("fr".equalsIgnoreCase(language)) {
                b = 5;
            } else if ("es".equalsIgnoreCase(language)) {
                b = 6;
            } else if ("ar".equalsIgnoreCase(language)) {
                b = 7;
            } else if ("ru".equalsIgnoreCase(language)) {
                b = 8;
            } else if ("uk".equalsIgnoreCase(language)) {
                b = 10;
            } else if ("it".equalsIgnoreCase(language)) {
                b = 11;
            } else if ("pt".equalsIgnoreCase(language)) {
                b = 12;
            } else if ("dum".equalsIgnoreCase(language)) {
                b = 13;
            } else if ("pl".equalsIgnoreCase(language)) {
                b = 14;
            } else if ("sv".equalsIgnoreCase(language)) {
                b = 15;
            } else if ("fi".equalsIgnoreCase(language)) {
                b = 16;
            } else if ("da".equalsIgnoreCase(language)) {
                b = 17;
            } else if ("no".equalsIgnoreCase(language)) {
                b = 18;
            } else if ("hu".equalsIgnoreCase(language)) {
                b = 19;
            } else if ("cs".equalsIgnoreCase(language)) {
                b = 20;
            } else if ("bg".equalsIgnoreCase(language)) {
                b = 21;
            } else if ("ro".equalsIgnoreCase(language)) {
                b = 22;
            } else if ("sk".equalsIgnoreCase(language)) {
                b = 23;
            } else {
                b = "lv".equalsIgnoreCase(language) ? (byte) 24 : 0;
            }
            aVar.mo10762b("CN".equalsIgnoreCase(country) ^ true ? (byte) 1 : 0);
            aVar.mo10771d(b);
            aVar.mo10743a(C3719h.f6537a);
            aVar.mo10748a(C3716e.f6534a);
        }
    }

    /* renamed from: d */
    static /* synthetic */ void m10626d(int i) {
        BandDataManager.m10557b(i);
        for (Map.Entry<String, C3734y> entry : f6564e.entrySet()) {
            ((C3734y) entry.getValue()).mo22682a(i, 0);
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10608a(boolean z, int i, C1168e eVar) {
        PeriodTimeInfo periodTimeInfo;
        C3734y.C3735a aVar;
        String str = "PeriodTimeInfo PERIOD_TYPE_";
        if (i == 2) {
            str = str + "QUICK_VIEW";
            periodTimeInfo = BandDataManager.m10555b("QuickView");
        } else if (i == 1) {
            str = str + "DO_NOT_DISTRUB";
            PeriodTimeInfo b = BandDataManager.m10555b("DoNotDisturb");
            if (z && (aVar = f6565f) != null) {
                aVar.mo22964a(C3734y.C3736b.deviceInfo, true);
            }
            periodTimeInfo = b;
        } else {
            periodTimeInfo = null;
        }
        if (periodTimeInfo != null) {
            periodTimeInfo.mo22586c(eVar.mo10813c());
            periodTimeInfo.mo22588d(eVar.mo10814d());
            periodTimeInfo.mo22581a(eVar.mo10811a());
            periodTimeInfo.mo22581a(eVar.mo10812b());
            BandDataManager.m10550a(periodTimeInfo);
        }
        m10623c(str + " " + eVar.mo10813c() + ":" + eVar.mo10814d() + "~" + eVar.mo10811a() + ":" + eVar.mo10812b());
    }

    /* renamed from: c */
    public static void m10622c(PeriodTimeInfo periodTimeInfo) {
        BandDataManager.m10550a(periodTimeInfo);
        if (f6562c != null) {
            boolean z = periodTimeInfo != null;
            if (periodTimeInfo == null) {
                periodTimeInfo = BandDataManager.m10555b("Sedentary");
            }
            boolean g = periodTimeInfo.mo22593g();
            f6562c.mo10760a(g);
            f6562c.mo10736a(new C1170g((byte) periodTimeInfo.mo22590e(), (byte) 10, (byte) periodTimeInfo.mo22585c(), (byte) periodTimeInfo.mo22580a()));
            if (z) {
                f6562c.mo10746a(new C3731t(g));
            }
        }
    }

    /* renamed from: d */
    public static void m10625d() {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10776h();
        }
    }

    /* renamed from: c */
    static /* synthetic */ void m10620c(int i) {
        m10623c("queryDeviceBattery battery=" + i);
        BandDataManager.m10557b(i);
        for (Map.Entry<String, C3734y> entry : f6564e.entrySet()) {
            ((C3734y) entry.getValue()).mo22682a(i, 0);
        }
    }

    /* renamed from: a */
    private static void m10592a(C1137a aVar) {
        if (aVar != null) {
            for (int i = 0; i < 3; i++) {
                AlarmInfoItem a = BandDataManager.m10547a(i);
                aVar.mo10734a(new C1163a(i, a.mo26377k(), a.mo26378l(), a.mo26379m() ? 0 : 127, a.mo26373i() > 0));
            }
            aVar.mo10739a(C3714d.f6532a);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m10623c(String str) {
        LogUtils.m1139c("crpBle " + str);
    }

    /* renamed from: a */
    private static void m10593a(C1137a aVar, PeriodTimeInfo periodTimeInfo) {
        if (aVar != null) {
            boolean z = periodTimeInfo != null;
            if (periodTimeInfo == null) {
                periodTimeInfo = BandDataManager.m10555b("Sedentary");
            }
            boolean g = periodTimeInfo.mo22593g();
            aVar.mo10760a(g);
            aVar.mo10736a(new C1170g((byte) periodTimeInfo.mo22590e(), (byte) 10, (byte) periodTimeInfo.mo22585c(), (byte) periodTimeInfo.mo22580a()));
            if (z) {
                aVar.mo10746a(new C3722k(g));
            }
        }
    }

    /* renamed from: a */
    private static void m10589a(int i, boolean z) {
        f6562c.mo10763b(i);
        if (z) {
            f6562c.mo10742a(new C3728q(i));
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10601a(PeriodTimeInfo periodTimeInfo, boolean z) {
        m10623c("queryQuickView open=" + z);
        C3734y.C3735a aVar = f6565f;
        if (aVar != null) {
            aVar.mo22964a(C3734y.C3736b.deviceInfo, z == periodTimeInfo.mo22593g());
        }
    }

    /* renamed from: b */
    public static void m10615b(String str) {
        if (f6560a == null) {
            f6560a = C1133a.m2876a(HvApp.m13010c());
        }
        f6561b = f6560a.mo10718a(str);
        C1138b bVar = f6561b;
        if (bVar != null) {
            f6563d = bVar.mo10780c();
            if (!f6563d) {
                f6562c = f6561b.mo10779b();
                f6562c.mo10750a(C3730s.f6548a);
                f6562c.mo10757a(f6567h);
                f6562c.mo10756a(f6568i);
                f6562c.mo10755a(f6569j);
                f6562c.mo10753a(f6570k);
                f6562c.mo10752a(f6571l);
                f6562c.mo10754a(f6572m);
                f6562c.mo10751a(f6573n);
                f6562c.mo10758a(f6566g);
            }
        }
    }

    /* renamed from: a */
    public static void m10600a(PeriodTimeInfo periodTimeInfo) {
        BandDataManager.m10550a(periodTimeInfo);
        if (f6562c != null && periodTimeInfo != null) {
            m10623c(periodTimeInfo.toString());
            if (!periodTimeInfo.mo22593g()) {
                periodTimeInfo.mo22586c(0);
                periodTimeInfo.mo22588d(0);
                periodTimeInfo.mo22581a(0);
                periodTimeInfo.mo22584b(0);
            }
            f6562c.mo10764b(new C1168e(periodTimeInfo.mo22585c(), periodTimeInfo.mo22587d(), periodTimeInfo.mo22580a(), periodTimeInfo.mo22583b()));
            f6562c.mo10765b(m10585a(true));
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10609a(boolean z, boolean z2) {
        m10623c("querySedentaryReminder open=" + z2);
        C3734y.C3735a aVar = f6565f;
        if (aVar != null) {
            aVar.mo22964a(C3734y.C3736b.sedentary, (z && z2) || (!z && !z2));
        }
    }

    /* renamed from: a */
    public static void m10603a(AlarmInfoItem alarmInfoItem) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            if (alarmInfoItem != null) {
                aVar.mo10734a(new C1163a((int) alarmInfoItem.mo26354a(), alarmInfoItem.mo26377k(), alarmInfoItem.mo26378l(), alarmInfoItem.mo26379m() ? 0 : 127, alarmInfoItem.mo26373i() > 0));
                return;
            }
            for (int i = 0; i < 3; i++) {
                AlarmInfoItem a = BandDataManager.m10547a(i);
                f6562c.mo10734a(new C1163a(i, a.mo26377k(), a.mo26378l(), a.mo26379m() ? 0 : 127, a.mo26373i() > 0));
            }
            f6562c.mo10739a(C3707b.f6516a);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r5 != 3) goto L_0x004c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0062 A[LOOP:0: B:11:0x005c->B:13:0x0062, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004f  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void m10611b(int r5) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "state="
            r0.append(r1)
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            m10623c(r0)
            r0 = 2
            r1 = 1
            r2 = 0
            if (r5 == 0) goto L_0x0049
            if (r5 == r1) goto L_0x004c
            if (r5 == r0) goto L_0x0021
            r3 = 3
            if (r5 == r3) goto L_0x0049
            goto L_0x004c
        L_0x0021:
            com.crrepa.ble.d.a r3 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6562c
            com.heimavista.fiedorasport.h.l r4 = com.heimavista.fiedorasport.p109h.C3723l.f6541a
            r3.mo10745a(r4)
            com.crrepa.ble.d.a r3 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6562c
            com.crrepa.ble.d.h.n r4 = m10585a(r2)
            r3.mo10765b(r4)
            com.crrepa.ble.d.a r3 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6562c
            com.crrepa.ble.d.h.n r4 = m10585a(r2)
            r3.mo10744a(r4)
            com.crrepa.ble.d.a r3 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6562c
            com.heimavista.fiedorasport.h.p r4 = com.heimavista.fiedorasport.p109h.C3727p.f6545a
            r3.mo10747a(r4)
            com.crrepa.ble.d.a r3 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6562c
            com.heimavista.fiedorasport.h.j r4 = com.heimavista.fiedorasport.p109h.C3721j.f6539a
            r3.mo10740a(r4)
            goto L_0x004c
        L_0x0049:
            com.heimavista.fiedorasport.p109h.BandDataManager.m10557b(r2)
        L_0x004c:
            if (r5 != r0) goto L_0x004f
            goto L_0x0050
        L_0x004f:
            r1 = 0
        L_0x0050:
            com.heimavista.fiedorasport.p109h.CRPBleManager.f6563d = r1
            java.util.HashMap<java.lang.String, com.heimavista.fiedorasport.h.y> r0 = com.heimavista.fiedorasport.p109h.CRPBleManager.f6564e
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x005c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0072
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r1 = r1.getValue()
            com.heimavista.fiedorasport.h.y r1 = (com.heimavista.fiedorasport.p109h.C3734y) r1
            r1.mo22718b(r5)
            goto L_0x005c
        L_0x0072:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p109h.CRPBleManager.m10611b(int):void");
    }

    /* renamed from: b */
    static /* synthetic */ void m10617b(boolean z) {
        PeriodTimeInfo b = BandDataManager.m10555b("AutoHeart");
        b.mo22582a(z);
        b.mo22591e(60);
        BandDataManager.m10550a(b);
    }

    /* renamed from: b */
    public static void m10614b(PeriodTimeInfo periodTimeInfo) {
        BandDataManager.m10550a(periodTimeInfo);
        if (f6562c != null && periodTimeInfo != null) {
            m10623c(periodTimeInfo.toString());
            f6562c.mo10766b(periodTimeInfo.mo22593g());
            f6562c.mo10735a(new C1168e(periodTimeInfo.mo22585c(), periodTimeInfo.mo22587d(), periodTimeInfo.mo22580a(), periodTimeInfo.mo22583b()));
            f6562c.mo10745a(new C3732u(periodTimeInfo));
            f6562c.mo10744a(m10585a(false));
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10607a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            m10623c("queryAllAlarmClock:" + ((C1163a) it.next()).toString());
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10588a(int i, int i2) {
        m10623c("queryGoalStep step=" + i2);
        C3734y.C3735a aVar = f6565f;
        if (aVar != null) {
            aVar.mo22964a(C3734y.C3736b.goalStep, i2 == i);
        }
    }

    /* renamed from: a */
    public static void m10605a(String str, int i) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10759a(str, i, ((int) System.currentTimeMillis()) / 1000);
        }
    }

    /* renamed from: a */
    public static void m10587a(byte b) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10773e(b);
        }
    }

    /* renamed from: a */
    public static void m10596a(C1185f fVar) {
        C1137a aVar = f6562c;
        if (aVar == null) {
            fVar.mo10876a(2);
        } else {
            aVar.mo10741a(new C3729r(fVar));
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10597a(C1185f fVar, int i) {
        m10623c("queryDisplayWatchFaceCallback crpWatchFaceType=" + i);
        m10635g(i);
        if (fVar != null) {
            fVar.mo10876a(i);
        }
    }

    /* renamed from: a */
    public static void m10591a(Bitmap bitmap, boolean z, C1275q qVar) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10733a(bitmap, z, qVar);
        }
    }

    /* renamed from: a */
    public static void m10595a(C1178n nVar, C1202w wVar) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10738a(nVar);
            m10598a(wVar);
        }
    }

    /* renamed from: a */
    public static void m10598a(C1202w wVar) {
        C1137a aVar = f6562c;
        if (aVar != null) {
            aVar.mo10749a(new C3717f(wVar));
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m10599a(C1202w wVar, C1178n nVar) {
        m10623c("queryWatchFaceLayoutCallback crpWatchFaceLayoutInfo ");
        m10594a(nVar);
        if (wVar != null) {
            wVar.mo10893a(nVar);
        }
    }

    /* renamed from: a */
    public static void m10594a(C1178n nVar) {
        MMKV g = m10634g();
        g.mo27041b("picmd5", nVar.mo10861a());
        g.mo27039b("tColor", nVar.mo10864b());
        g.mo27039b("tbContent", nVar.mo10866c());
        g.mo27039b("ttContent", nVar.mo10870e());
        g.mo27039b("tPos", nVar.mo10868d());
    }
}
