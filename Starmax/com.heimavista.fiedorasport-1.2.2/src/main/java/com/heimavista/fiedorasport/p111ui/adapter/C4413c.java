package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.c */
/* compiled from: lambda */
public final /* synthetic */ class C4413c implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8654P;

    /* renamed from: Q */
    private final /* synthetic */ int f8655Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8656R;

    public /* synthetic */ C4413c(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8654P = listBuddyDynamicAdapter;
        this.f8655Q = i;
        this.f8656R = dVar;
    }

    public final void onClick(View view) {
        this.f8654P.mo24624g(this.f8655Q, this.f8656R, view);
    }
}
