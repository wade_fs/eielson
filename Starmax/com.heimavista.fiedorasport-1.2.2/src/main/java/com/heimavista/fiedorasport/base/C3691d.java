package com.heimavista.fiedorasport.base;

import com.heimavista.widget.dialog.LoadingDialog;

/* renamed from: com.heimavista.fiedorasport.base.d */
/* compiled from: lambda */
public final /* synthetic */ class C3691d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ BaseActivity f6446P;

    /* renamed from: Q */
    private final /* synthetic */ boolean f6447Q;

    /* renamed from: R */
    private final /* synthetic */ String f6448R;

    /* renamed from: S */
    private final /* synthetic */ boolean f6449S;

    /* renamed from: T */
    private final /* synthetic */ boolean f6450T;

    /* renamed from: U */
    private final /* synthetic */ LoadingDialog.C4628a f6451U;

    public /* synthetic */ C3691d(BaseActivity baseActivity, boolean z, String str, boolean z2, boolean z3, LoadingDialog.C4628a aVar) {
        this.f6446P = baseActivity;
        this.f6447Q = z;
        this.f6448R = str;
        this.f6449S = z2;
        this.f6450T = z3;
        this.f6451U = aVar;
    }

    public final void run() {
        this.f6446P.mo22713a(this.f6447Q, this.f6448R, this.f6449S, this.f6450T, this.f6451U);
    }
}
