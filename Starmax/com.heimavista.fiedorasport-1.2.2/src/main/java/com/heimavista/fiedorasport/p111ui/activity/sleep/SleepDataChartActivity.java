package com.heimavista.fiedorasport.p111ui.activity.sleep;

import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.sleep.CalculateSleepTask;
import com.heimavista.fiedorasport.p199j.PieChartManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p139g.OnChartValueSelectedListener;
import p119e.p189e.p193d.p194i.SleepDataDb;
import p119e.p189e.p197e.p198a.ChartSleepData;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity */
public class SleepDataChartActivity extends BaseActivity {

    /* renamed from: b0 */
    private BarChart f8360b0;

    /* renamed from: c0 */
    private PieChart f8361c0;

    /* renamed from: d0 */
    private TextView f8362d0;

    /* renamed from: e0 */
    private TextView f8363e0;

    /* renamed from: f0 */
    private TextView f8364f0;

    /* renamed from: g0 */
    private TextView f8365g0;

    /* renamed from: h0 */
    private TextView f8366h0;

    /* renamed from: i0 */
    private TextView f8367i0;
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public int f8368j0 = -1;
    /* access modifiers changed from: private */

    /* renamed from: k0 */
    public List<ChartSleepData> f8369k0;
    /* access modifiers changed from: private */

    /* renamed from: l0 */
    public List<ChartSleepData> f8370l0;
    /* access modifiers changed from: private */

    /* renamed from: m0 */
    public List<ChartSleepData> f8371m0;
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public List<ChartSleepData> f8372n0;

    /* renamed from: o0 */
    private CalculateSleepTask.C4344a f8373o0 = CalculateSleepTask.C4344a.DAYS;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$a */
    class C4337a extends ValueFormatter {
        C4337a() {
        }

        /* renamed from: a */
        public String mo23221a(float f, AxisBase aVar) {
            int i = (int) f;
            if (i < SleepDataChartActivity.this.f8369k0.size()) {
                return ((ChartSleepData) SleepDataChartActivity.this.f8369k0.get(i)).f8012c;
            }
            return super.mo23221a(f, aVar);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$c */
    class C4339c extends ThreadUtils.C0877e<List<ChartSleepData>> {

        /* renamed from: U */
        final /* synthetic */ String f8376U;

        C4339c(String str) {
            this.f8376U = str;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24524a((List<ChartSleepData>) ((List) obj));
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24524a(List<ChartSleepData> list) {
            SleepDataChartActivity.this.m13962a(list);
            List unused = SleepDataChartActivity.this.f8370l0 = list;
        }

        /* renamed from: b */
        public List<ChartSleepData> mo9798b() {
            ArrayList arrayList = new ArrayList();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M.d", Locale.getDefault());
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("M月d日", Locale.getDefault());
            Calendar instance = Calendar.getInstance();
            instance.add(6, -30);
            int i = 30;
            while (i > 0) {
                SleepDayInfo a = new SleepDataDb().mo24261a(i - 1);
                ChartSleepData aVar = new ChartSleepData();
                aVar.f8010a = a;
                aVar.f8011b = (float) (30 - i);
                aVar.f8012c = i == 1 ? this.f8376U : simpleDateFormat.format(instance.getTime());
                aVar.f8013d = i == 1 ? this.f8376U : simpleDateFormat2.format(instance.getTime());
                arrayList.add(aVar);
                instance.add(6, 1);
                i--;
            }
            return arrayList;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$d */
    class C4340d extends ThreadUtils.C0877e<List<ChartSleepData>> {

        /* renamed from: U */
        final /* synthetic */ String f8378U;

        /* renamed from: V */
        final /* synthetic */ String f8379V;

        /* renamed from: W */
        final /* synthetic */ String f8380W;

        C4340d(String str, String str2, String str3) {
            this.f8378U = str;
            this.f8379V = str2;
            this.f8380W = str3;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24525a((List<ChartSleepData>) ((List) obj));
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24525a(List<ChartSleepData> list) {
            SleepDataChartActivity.this.m13962a(list);
            List unused = SleepDataChartActivity.this.f8371m0 = list;
        }

        /* renamed from: b */
        public List<ChartSleepData> mo9798b() {
            String str;
            String str2;
            ArrayList arrayList = new ArrayList();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M/d", Locale.getDefault());
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("M月d日", Locale.getDefault());
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            instance.setFirstDayOfWeek(2);
            instance.set(7, instance.getFirstDayOfWeek());
            instance.add(3, -5);
            instance2.setFirstDayOfWeek(2);
            instance2.set(7, instance2.getFirstDayOfWeek());
            instance2.add(3, -5);
            for (int i = 6; i > 0; i--) {
                if (i == 6) {
                    instance2.add(7, 6);
                }
                SleepDayInfo c = new SleepDataDb().mo24264c(i - 1);
                ChartSleepData aVar = new ChartSleepData();
                aVar.f8010a = c;
                aVar.f8011b = (float) (6 - i);
                if (i == 1) {
                    str = this.f8378U;
                } else if (i == 2) {
                    str = this.f8379V;
                } else {
                    str = simpleDateFormat.format(instance.getTime()) + "-" + simpleDateFormat.format(instance2.getTime());
                }
                aVar.f8012c = str;
                if (i == 1) {
                    str2 = this.f8378U;
                } else if (i == 2) {
                    str2 = this.f8379V;
                } else {
                    str2 = simpleDateFormat2.format(instance.getTime()) + this.f8380W + simpleDateFormat2.format(instance2.getTime());
                }
                aVar.f8013d = str2;
                arrayList.add(aVar);
                instance.add(3, 1);
                instance2.add(3, 1);
            }
            return arrayList;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$e */
    class C4341e extends ThreadUtils.C0877e<List<ChartSleepData>> {

        /* renamed from: U */
        final /* synthetic */ String f8382U;

        /* renamed from: V */
        final /* synthetic */ String f8383V;

        C4341e(String str, String str2) {
            this.f8382U = str;
            this.f8383V = str2;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24526a((List<ChartSleepData>) ((List) obj));
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24526a(List<ChartSleepData> list) {
            SleepDataChartActivity.this.m13962a(list);
            List unused = SleepDataChartActivity.this.f8372n0 = list;
        }

        /* renamed from: b */
        public List<ChartSleepData> mo9798b() {
            ArrayList arrayList = new ArrayList();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M月", Locale.getDefault());
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            instance2.add(2, -12);
            instance.add(2, -12);
            int i = 12;
            while (i > 0) {
                instance.add(2, 1);
                instance.set(5, 1);
                instance2.add(2, 2);
                instance2.set(5, 0);
                SleepDayInfo b = new SleepDataDb().mo24263b(i - 1);
                ChartSleepData aVar = new ChartSleepData();
                aVar.f8010a = b;
                aVar.f8011b = (float) (12 - i);
                aVar.f8012c = i == 1 ? this.f8382U : i == 2 ? this.f8383V : simpleDateFormat.format(instance.getTime());
                aVar.f8013d = i == 1 ? this.f8382U : i == 2 ? this.f8383V : simpleDateFormat.format(instance.getTime());
                arrayList.add(aVar);
                i--;
            }
            return arrayList;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$f */
    static /* synthetic */ class C4342f {

        /* renamed from: a */
        static final /* synthetic */ int[] f8385a = new int[CalculateSleepTask.C4344a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.heimavista.fiedorasport.ui.activity.sleep.b$a[] r0 = com.heimavista.fiedorasport.p111ui.activity.sleep.CalculateSleepTask.C4344a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataChartActivity.C4342f.f8385a = r0
                int[] r0 = com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataChartActivity.C4342f.f8385a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.heimavista.fiedorasport.ui.activity.sleep.b$a r1 = com.heimavista.fiedorasport.p111ui.activity.sleep.CalculateSleepTask.C4344a.DAYS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataChartActivity.C4342f.f8385a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.heimavista.fiedorasport.ui.activity.sleep.b$a r1 = com.heimavista.fiedorasport.p111ui.activity.sleep.CalculateSleepTask.C4344a.WEEKS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataChartActivity.C4342f.f8385a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.heimavista.fiedorasport.ui.activity.sleep.b$a r1 = com.heimavista.fiedorasport.p111ui.activity.sleep.CalculateSleepTask.C4344a.MONTHS     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.p111ui.activity.sleep.SleepDataChartActivity.C4342f.<clinit>():void");
        }
    }

    /* renamed from: u */
    private void m13967u() {
        this.f8360b0.setBorderWidth(1.0f);
        this.f8360b0.mo13022a(0.0f, 12.0f, 0.0f, 12.0f);
        this.f8360b0.getAxisRight().mo13233a(false);
        this.f8360b0.getAxisLeft().mo13233a(false);
        this.f8360b0.setScaleEnabled(false);
        this.f8360b0.setNoDataText(getString(R$string.no_sleep_data));
        this.f8360b0.setNoDataTextColor(getResources().getColor(R$color.font_color));
        this.f8360b0.getLegend().mo13233a(false);
        this.f8360b0.setDrawValueAboveBar(false);
        XAxis xAxis = this.f8360b0.getXAxis();
        xAxis.mo13284a(XAxis.C1654a.BOTTOM);
        xAxis.mo13232a(Typeface.DEFAULT);
        xAxis.mo13204c(false);
        xAxis.mo13202b(false);
        xAxis.mo13231a(-1);
        xAxis.mo13230a(10.0f);
        xAxis.mo13207e(1.0f);
        xAxis.mo13205d(-0.5f);
        xAxis.mo13203c(12);
        xAxis.mo13200a(new C4337a());
        this.f8360b0.setOnChartValueSelectedListener(new C4338b());
    }

    /* renamed from: v */
    private void m13968v() {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        for (int i = 0; i < this.f8369k0.size(); i++) {
            ChartSleepData aVar = this.f8369k0.get(i);
            if (aVar.f8010a.f6257f > 0) {
                z = true;
            }
            float f = aVar.f8011b;
            SleepDayInfo bVar = aVar.f8010a;
            arrayList.add(new BarEntry(f, new float[]{(float) bVar.f6254c, (float) bVar.f6256e, (float) bVar.f6255d}));
        }
        if (!z) {
            this.f8360b0.getDescription().mo13233a(true);
        } else {
            this.f8360b0.getDescription().mo13233a(false);
            BarDataSet bVar2 = new BarDataSet(arrayList, "");
            bVar2.mo13342a(false);
            bVar2.mo13344b(false);
            bVar2.mo13343a(getResources().getColor(R$color.sleep_deep), getResources().getColor(R$color.sleep_awake), getResources().getColor(R$color.sleep_light));
            bVar2.mo13334g(getResources().getColor(R$color.sleep_line_select));
            bVar2.mo13326h(PsExtractor.VIDEO_STREAM_MASK);
            bVar2.mo13347c(true);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(bVar2);
            BarData aVar2 = new BarData(arrayList2);
            aVar2.mo13377a(10.0f);
            aVar2.mo13379a(Typeface.DEFAULT);
            if (arrayList.size() < 10) {
                aVar2.mo13322b(0.5f);
            } else {
                aVar2.mo13322b(0.9f);
            }
            this.f8360b0.setData(aVar2);
            this.f8360b0.mo13031b(1000);
            this.f8360b0.mo13023a(0);
        }
        this.f8360b0.invalidate();
    }

    /* renamed from: w */
    private void m13969w() {
        this.f8368j0 = -1;
        int i = C4342f.f8385a[this.f8373o0.ordinal()];
        if (i == 1) {
            List<ChartSleepData> list = this.f8370l0;
            if (list == null || list.isEmpty()) {
                m13970x();
            } else {
                m13962a(this.f8370l0);
            }
        } else if (i == 2) {
            List<ChartSleepData> list2 = this.f8371m0;
            if (list2 == null || list2.isEmpty()) {
                m13972z();
            } else {
                m13962a(this.f8371m0);
            }
        } else if (i == 3) {
            List<ChartSleepData> list3 = this.f8372n0;
            if (list3 == null || list3.isEmpty()) {
                m13971y();
            } else {
                m13962a(this.f8372n0);
            }
        }
    }

    /* renamed from: x */
    private void m13970x() {
        LogUtils.m1139c("startTask day");
        ThreadUtils.m963c(new C4339c(getString(R$string.last_night)));
    }

    /* renamed from: y */
    private void m13971y() {
        LogUtils.m1139c("startTask month");
        ThreadUtils.m963c(new C4341e(getString(R$string.current_month), getString(R$string.last_month)));
    }

    /* renamed from: z */
    private void m13972z() {
        LogUtils.m1139c("startTask week");
        ThreadUtils.m963c(new C4340d(getString(R$string.current_week), getString(R$string.last_week), getString(R$string.f6601to)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_chart_sleep;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8360b0 = (BarChart) findViewById(R$id.barChart);
        this.f8361c0 = (PieChart) findViewById(R$id.pieChart);
        this.f8362d0 = (TextView) findViewById(R$id.tvSleepTime);
        this.f8363e0 = (TextView) findViewById(R$id.tvStart);
        this.f8364f0 = (TextView) findViewById(R$id.tvCenter);
        this.f8365g0 = (TextView) findViewById(R$id.tvEnd);
        this.f8366h0 = (TextView) findViewById(R$id.tvAsleep);
        this.f8367i0 = (TextView) findViewById(R$id.tvRise);
        ((RadioGroup) findViewById(R$id.rbLabel)).setOnCheckedChangeListener(new C4343a(this));
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataChartActivity$b */
    class C4338b implements OnChartValueSelectedListener {
        C4338b() {
        }

        /* renamed from: a */
        public void mo23323a(Entry entry, Highlight dVar) {
            int d = (int) entry.mo13315d();
            if (SleepDataChartActivity.this.f8368j0 != d) {
                int unused = SleepDataChartActivity.this.f8368j0 = d;
                SleepDataChartActivity sleepDataChartActivity = SleepDataChartActivity.this;
                sleepDataChartActivity.m13958a(((ChartSleepData) sleepDataChartActivity.f8369k0.get(d)).f8010a);
                SleepDataChartActivity sleepDataChartActivity2 = SleepDataChartActivity.this;
                sleepDataChartActivity2.mo22701a(((ChartSleepData) sleepDataChartActivity2.f8369k0.get(d)).f8013d);
            }
        }

        /* renamed from: a */
        public void mo23322a() {
            int unused = SleepDataChartActivity.this.f8368j0 = -1;
            SleepDataChartActivity sleepDataChartActivity = SleepDataChartActivity.this;
            sleepDataChartActivity.m13958a(((ChartSleepData) sleepDataChartActivity.f8369k0.get(SleepDataChartActivity.this.f8369k0.size() - 1)).f8010a);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24523a(RadioGroup radioGroup, int i) {
        this.f8360b0.mo13034e();
        if (i == R$id.rb1) {
            this.f8373o0 = CalculateSleepTask.C4344a.DAYS;
            Constant.m10311a("點擊日報表");
        } else if (i == R$id.rb2) {
            this.f8373o0 = CalculateSleepTask.C4344a.WEEKS;
            Constant.m10311a("點擊週報表");
        } else if (i == R$id.rb3) {
            this.f8373o0 = CalculateSleepTask.C4344a.MONTHS;
            Constant.m10311a("點擊月報表");
        }
        m13969w();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2002", "exad_sleep_inside");
        m13967u();
        m13969w();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13962a(List<ChartSleepData> list) {
        List<ChartSleepData> list2 = this.f8369k0;
        if (list2 == null) {
            this.f8369k0 = new ArrayList();
        } else {
            list2.clear();
        }
        this.f8369k0.addAll(list);
        int size = this.f8369k0.size();
        int i = size - 1;
        m13958a(this.f8369k0.get(i).f8010a);
        int min = Math.min(12, size);
        Matrix matrix = new Matrix();
        if (size > 12) {
            matrix.postScale(((float) size) / 12.0f, 1.0f);
        } else {
            matrix.postScale(1.0f, 1.0f);
        }
        this.f8360b0.getViewPortHandler().mo23444a(matrix, this.f8360b0, false);
        this.f8360b0.mo13023a(1000);
        this.f8360b0.mo12953a((float) i);
        this.f8360b0.getXAxis().mo13203c(min);
        m13968v();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13958a(SleepDayInfo bVar) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        SleepDayInfo bVar2 = bVar;
        boolean z = bVar2.f6257f == 0;
        SpanUtils a = SpanUtils.m865a(this.f8362d0);
        a.mo9735a(getString(this.f8373o0 == CalculateSleepTask.C4344a.DAYS ? R$string.day_sleep : R$string.day_sleep_avg));
        a.mo9735a(" ");
        String str8 = "--";
        if (z) {
            str = str8;
        } else {
            str = String.valueOf((bVar2.f6258g / bVar2.f6260i) / 60);
        }
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str2 = str8;
        } else {
            str2 = String.valueOf((bVar2.f6258g / bVar2.f6260i) % 60);
        }
        a.mo9735a(str2);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_min));
        if (this.f8373o0 == CalculateSleepTask.C4344a.DAYS) {
            findViewById(R$id.flSleep).setVisibility(0);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String str9 = "-" + getString(R$string.data_unit_hour) + "-" + getString(R$string.data_unit_min);
            SpanUtils a2 = SpanUtils.m865a(this.f8366h0);
            long j = bVar2.f6252a;
            a2.mo9735a(j > 0 ? simpleDateFormat.format(new Date(j * 1000)) : str9);
            a2.mo9735a(" ");
            a2.mo9735a(getString(R$string.asleep));
            a2.mo9736b();
            SpanUtils a3 = SpanUtils.m865a(this.f8367i0);
            long j2 = bVar2.f6253b;
            if (j2 > 0) {
                str9 = simpleDateFormat.format(new Date(j2 * 1000));
            }
            a3.mo9735a(str9);
            a3.mo9735a(" ");
            a3.mo9735a(getString(R$string.awake));
            a3.mo9736b();
        } else {
            findViewById(R$id.flSleep).setVisibility(8);
        }
        if (this.f8373o0 != CalculateSleepTask.C4344a.DAYS && bVar2.f6257f > 0) {
            a.mo9732a();
            a.mo9735a(this.f8373o0 == CalculateSleepTask.C4344a.WEEKS ? "週累計" : "月累計");
            a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_11));
            a.mo9737b(-7829368);
            a.mo9735a(String.valueOf(bVar2.f6260i));
            a.mo9738c();
            a.mo9735a("天");
            a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_11));
            a.mo9737b(-7829368);
        }
        a.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8363e0);
        if (z) {
            str3 = str8;
        } else {
            str3 = String.valueOf((bVar2.f6254c / bVar2.f6260i) / 60);
        }
        a4.mo9735a(str3);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str4 = str8;
        } else {
            str4 = String.valueOf((bVar2.f6254c / bVar2.f6260i) % 60);
        }
        a4.mo9735a(str4);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_min));
        a4.mo9736b();
        SpanUtils a5 = SpanUtils.m865a(this.f8364f0);
        if (z) {
            str5 = str8;
        } else {
            str5 = String.valueOf((bVar2.f6255d / bVar2.f6260i) / 60);
        }
        a5.mo9735a(str5);
        a5.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a5.mo9738c();
        a5.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str6 = str8;
        } else {
            str6 = String.valueOf((bVar2.f6255d / bVar2.f6260i) % 60);
        }
        a5.mo9735a(str6);
        a5.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a5.mo9738c();
        a5.mo9735a(getString(R$string.data_unit_min));
        a5.mo9736b();
        SpanUtils a6 = SpanUtils.m865a(this.f8365g0);
        if (z) {
            str7 = str8;
        } else {
            str7 = String.valueOf((bVar2.f6256e / bVar2.f6260i) / 60);
        }
        a6.mo9735a(str7);
        a6.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a6.mo9738c();
        a6.mo9735a(getString(R$string.data_unit_hour));
        if (!z) {
            str8 = String.valueOf((bVar2.f6256e / bVar2.f6260i) % 60);
        }
        a6.mo9735a(str8);
        a6.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a6.mo9738c();
        a6.mo9735a(getString(R$string.data_unit_min));
        a6.mo9736b();
        List<ChartSleepData> list = this.f8369k0;
        mo22701a((CharSequence) list.get(list.size() - 1).f8013d);
        m13957a(((float) bVar2.f6257f) * 1.0f, bVar2.f6254c, bVar2.f6255d, bVar2.f6256e);
    }

    /* renamed from: a */
    private void m13957a(float f, int i, int i2, int i3) {
        ArrayList arrayList = new ArrayList();
        float f2 = 0.0f;
        int i4 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
        float f3 = i4 == 0 ? 0.0f : ((float) i) / f;
        arrayList.add(new PieEntry(f3, getString(R$string.deep) + "    |    " + (i / 60) + getString(R$string.data_unit_hour) + (i % 60) + getString(R$string.data_unit_min)));
        float f4 = i4 == 0 ? 0.0f : ((float) i2) / f;
        arrayList.add(new PieEntry(f4, getString(R$string.light) + "    |    " + (i2 / 60) + getString(R$string.data_unit_hour) + (i2 % 60) + getString(R$string.data_unit_min)));
        if (i4 != 0) {
            f2 = ((float) i3) / f;
        }
        arrayList.add(new PieEntry(f2, getString(R$string.awake) + "    |    " + (i3 / 60) + getString(R$string.data_unit_hour) + (i3 % 60) + getString(R$string.data_unit_min)));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_deep)));
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_light)));
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_awake)));
        PieChartManager jVar = new PieChartManager(this.f8361c0, 0, new SpannableStringBuilder(getString(i4 == 0 ? R$string.no_sleep_data : R$string.sleep_ratio)));
        this.f8361c0.getLegend().mo13233a(false);
        jVar.mo24368a(arrayList, arrayList2, "");
    }
}
