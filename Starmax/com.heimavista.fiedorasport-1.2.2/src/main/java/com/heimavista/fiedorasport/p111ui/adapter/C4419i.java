package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.SportBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.i */
/* compiled from: lambda */
public final /* synthetic */ class C4419i implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListDynamicsAdapter f8673P;

    /* renamed from: Q */
    private final /* synthetic */ int f8674Q;

    /* renamed from: R */
    private final /* synthetic */ SportBean f8675R;

    public /* synthetic */ C4419i(ListDynamicsAdapter listDynamicsAdapter, int i, SportBean gVar) {
        this.f8673P = listDynamicsAdapter;
        this.f8674Q = i;
        this.f8675R = gVar;
    }

    public final void onClick(View view) {
        this.f8673P.mo24638c(this.f8674Q, this.f8675R, view);
    }
}
