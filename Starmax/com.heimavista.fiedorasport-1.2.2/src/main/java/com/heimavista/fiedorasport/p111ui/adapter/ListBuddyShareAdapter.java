package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import java.util.ArrayList;
import p119e.p189e.p193d.p195j.BuddyShareDb;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyShareAdapter */
public class ListBuddyShareAdapter extends BaseRecyclerViewAdapter<BuddyShareDb, C4403a> {

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyShareAdapter$a */
    class C4403a extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        ImageView f8606R = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: S */
        TextView f8607S = ((TextView) findViewById(R$id.tvUserName));

        /* renamed from: T */
        TextView f8608T = ((TextView) findViewById(R$id.tvDesc));

        public C4403a(ListBuddyShareAdapter listBuddyShareAdapter, ViewGroup viewGroup, int i) {
            super(listBuddyShareAdapter, viewGroup, i);
        }
    }

    public ListBuddyShareAdapter(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.j.f.a(android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, boolean):void
     arg types: [android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, int]
     candidates:
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer):java.util.List<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.j.f.a(android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, boolean):void */
    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4403a aVar, int i) {
        BuddyShareDb bVar = (BuddyShareDb) getItem(i);
        ArrayList arrayList = new ArrayList();
        if (bVar.mo24320q()) {
            arrayList.add("位置");
        }
        if (bVar.mo24322s()) {
            arrayList.add("步數");
        }
        if (bVar.mo24321r()) {
            arrayList.add("睡眠");
        }
        if (bVar.mo24319p()) {
            arrayList.add("心率");
        }
        int size = arrayList.size();
        if (size == 0) {
            aVar.f8608T.setText("所有不可見");
        } else if (size == 4) {
            aVar.f8608T.setText("所有可見");
        } else {
            StringBuilder sb = new StringBuilder();
            int size2 = arrayList.size();
            for (int i2 = 0; i2 < size2; i2++) {
                sb.append((String) arrayList.get(i2));
                if (i2 < size2 - 1) {
                    sb.append("/");
                }
            }
            sb.append("可見");
            aVar.f8608T.setText(sb.toString());
        }
        UserInfoDb.m13340a(mo22788b(), bVar.mo24232o(), aVar.f8607S, aVar.f8606R, false);
    }

    @NonNull
    public C4403a onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new C4403a(this, viewGroup, R$layout.layout_list_item_buddy_share);
    }
}
