package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.d */
/* compiled from: lambda */
public final /* synthetic */ class C4414d implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8657P;

    /* renamed from: Q */
    private final /* synthetic */ int f8658Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8659R;

    public /* synthetic */ C4414d(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8657P = listBuddyDynamicAdapter;
        this.f8658Q = i;
        this.f8659R = dVar;
    }

    public final void onClick(View view) {
        this.f8657P.mo24619b(this.f8658Q, this.f8659R, view);
    }
}
