package com.heimavista.fiedorasport.p111ui.activity.user;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.e */
/* compiled from: lambda */
public final /* synthetic */ class C4392e implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ UserCommonActivity f8559P;

    /* renamed from: Q */
    private final /* synthetic */ RecyclerView f8560Q;

    public /* synthetic */ C4392e(UserCommonActivity userCommonActivity, RecyclerView recyclerView) {
        this.f8559P = userCommonActivity;
        this.f8560Q = recyclerView;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8559P.mo24587a(this.f8560Q, view, (MenuItem) obj, i);
    }
}
