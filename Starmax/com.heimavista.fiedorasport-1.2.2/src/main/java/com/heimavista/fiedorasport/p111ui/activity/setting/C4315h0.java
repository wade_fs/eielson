package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Intent;
import com.heimavista.fiedorasport.base.BaseActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.h0 */
/* compiled from: lambda */
public final /* synthetic */ class C4315h0 implements BaseActivity.C3680b {

    /* renamed from: a */
    private final /* synthetic */ SettingWatchFacesActivity f8311a;

    public /* synthetic */ C4315h0(SettingWatchFacesActivity settingWatchFacesActivity) {
        this.f8311a = settingWatchFacesActivity;
    }

    public final void onActivityResult(int i, Intent intent) {
        this.f8311a.mo24498a(i, intent);
    }
}
