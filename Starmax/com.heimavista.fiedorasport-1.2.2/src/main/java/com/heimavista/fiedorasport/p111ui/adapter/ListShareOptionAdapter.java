package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListShareOptionAdapter */
public class ListShareOptionAdapter extends BaseRecyclerViewAdapter<MenuItem, C4409b> {

    /* renamed from: i */
    private LayoutInflater f8634i;

    /* renamed from: j */
    private C4408a f8635j;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListShareOptionAdapter$a */
    public interface C4408a {
        /* renamed from: a */
        void mo24506a(int i, MenuItem eVar, boolean z);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListShareOptionAdapter$b */
    class C4409b extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        CommonListItemView f8636R = ((CommonListItemView) findViewById(R$id.listItemView));

        C4409b(ListShareOptionAdapter listShareOptionAdapter, View view) {
            super(view);
            TextView textView = (TextView) findViewById(16908308);
        }
    }

    public ListShareOptionAdapter(BaseActivity baseActivity) {
        super(baseActivity);
        this.f8634i = LayoutInflater.from(baseActivity);
    }

    /* renamed from: a */
    public void mo24653a(C4408a aVar) {
        this.f8635j = aVar;
    }

    public int getItemViewType(int i) {
        return ((MenuItem) getItem(i)).mo22653e();
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4409b bVar, int i) {
        MenuItem eVar = (MenuItem) getItem(i);
        bVar.f8636R.setAccessoryType(2);
        bVar.f8636R.setImageResource(eVar.mo22650c());
        bVar.f8636R.setText(eVar.mo22652d());
        SwitchButton switchButton = bVar.f8636R.getSwitch();
        switchButton.setChecked(eVar.mo22654f());
        switchButton.setEnabled(eVar.mo22655g());
        switchButton.setOnCheckedChangeListener(new C4427q(this, eVar, i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4409b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new C4409b(this, this.f8634i.inflate(R$layout.layout_list_item, viewGroup, false));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24652a(MenuItem eVar, int i, SwitchButton switchButton, boolean z) {
        String d = eVar.mo22652d();
        eVar.mo22645a(z);
        m14353a(d, z);
        C4408a aVar = this.f8635j;
        if (aVar != null) {
            aVar.mo24506a(i, eVar, z);
        }
    }

    /* renamed from: a */
    private void m14353a(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(z ? "開啟" : "關閉");
        Constant.m10311a(sb.toString());
    }
}
