package com.heimavista.fiedorasport;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;
import cat.ereza.customactivityoncrash.p018b.CaocConfig;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.apn.ApnManage;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.p107f.PushHandleApiCall;
import com.heimavista.fiedorasport.p107f.PushHandleRedirect;
import com.heimavista.fiedorasport.p107f.PushHandleWebView;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.MainActivity;
import com.heimavista.fiedorasport.p111ui.activity.crash.CrashActivity;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadDelegateSimple;
import com.tencent.mmkv.MMKV;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.d */
public class FSApp extends HvApp {
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public int f6455Q = 0;

    /* renamed from: com.heimavista.fiedorasport.d$a */
    /* compiled from: FSApp */
    class C3693a implements Application.ActivityLifecycleCallbacks {
        C3693a() {
        }

        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
        }

        public void onActivityDestroyed(@NonNull Activity activity) {
        }

        public void onActivityPaused(@NonNull Activity activity) {
        }

        public void onActivityResumed(@NonNull Activity activity) {
            if ((activity instanceof HomeActivity) && C4222l.m13465d() && BandDataManager.m10555b("Location").mo22593g()) {
                LocManager.m10493g().mo22958c();
            }
        }

        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {
        }

        public void onActivityStarted(@NonNull Activity activity) {
            FSApp.m10314b(FSApp.this);
            LogUtils.m1139c("LifecycleCallback onActivityStarted " + FSApp.this.f6455Q);
            if (FSApp.this.f6455Q == 1) {
                LogUtils.m1139c("app in foreground");
            }
        }

        public void onActivityStopped(@NonNull Activity activity) {
            FSApp.m10315c(FSApp.this);
            LogUtils.m1139c("LifecycleCallback onActivityStopped " + FSApp.this.f6455Q);
            if (FSApp.this.f6455Q == 0) {
                LogUtils.m1139c("app in background");
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.d$b */
    /* compiled from: FSApp */
    static class C3694b extends HvGadDelegateSimple {
        C3694b() {
        }

        /* renamed from: a */
        public int mo22826a(String str) {
            return R$mipmap.def_ad;
        }
    }

    /* renamed from: b */
    static /* synthetic */ int m10314b(FSApp dVar) {
        int i = dVar.f6455Q;
        dVar.f6455Q = i + 1;
        return i;
    }

    /* renamed from: c */
    static /* synthetic */ int m10315c(FSApp dVar) {
        int i = dVar.f6455Q;
        dVar.f6455Q = i - 1;
        return i;
    }

    /* renamed from: d */
    private void m10316d() {
        MMKV e = FSManager.m10323r().mo22835e();
        for (String str : new String[]{AppUtils.m943a() + "_pref", "band_data"}) {
            SharedPreferences sharedPreferences = getSharedPreferences(str, 0);
            LogUtils.m1139c("mady mmkv:" + e.mo27023a(sharedPreferences) + " clear:" + sharedPreferences.edit().clear().commit());
        }
    }

    /* renamed from: e */
    private void m10317e() {
        CaocConfig.C0802a b = CaocConfig.C0802a.m774b();
        b.mo9463a(0);
        b.mo9465a(true);
        b.mo9470c(false);
        b.mo9469b(false);
        b.mo9471d(true);
        b.mo9467b(2000);
        b.mo9468b(MainActivity.class);
        b.mo9464a(CrashActivity.class);
        b.mo9466a();
    }

    /* renamed from: f */
    private void m10318f() {
        LogUtils.C0910d e = LogUtils.m1144e();
        e.mo9838c(mo24159b());
        e.mo9834a("mady");
        e.mo9836b(false);
        e.mo9835a(false);
    }

    /* renamed from: g */
    private void m10319g() {
        ApnManage.m10050a().mo22548a("redirect", new PushHandleRedirect());
        ApnManage.m10050a().mo22548a("webview", new PushHandleWebView());
        ApnManage.m10050a().mo22548a("apiCall", new PushHandleApiCall());
    }

    /* renamed from: h */
    private void m10320h() {
        registerActivityLifecycleCallbacks(new C3693a());
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(context);
    }

    public void onCreate() {
        super.onCreate();
        MMKV.m17080a(this);
        m10318f();
        m10313a((Context) this);
        m10317e();
        m10316d();
        TickManager.m10521a();
        m10319g();
        m10320h();
    }

    /* renamed from: a */
    public static void m10313a(Context context) {
        HvGad.m14915i().mo25001a((FSApp) context.getApplicationContext(), new C3694b());
    }
}
