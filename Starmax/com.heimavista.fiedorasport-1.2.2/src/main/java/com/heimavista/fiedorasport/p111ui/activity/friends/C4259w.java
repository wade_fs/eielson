package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.content.Intent;
import com.heimavista.fiedorasport.base.BaseActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.w */
/* compiled from: lambda */
public final /* synthetic */ class C4259w implements BaseActivity.C3680b {

    /* renamed from: a */
    private final /* synthetic */ SelectFriendsActivity f8139a;

    public /* synthetic */ C4259w(SelectFriendsActivity selectFriendsActivity) {
        this.f8139a = selectFriendsActivity;
    }

    public final void onActivityResult(int i, Intent intent) {
        this.f8139a.mo24405a(i, intent);
    }
}
