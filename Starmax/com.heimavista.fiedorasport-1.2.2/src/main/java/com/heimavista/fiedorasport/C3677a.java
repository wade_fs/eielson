package com.heimavista.fiedorasport;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/* renamed from: com.heimavista.fiedorasport.a */
/* compiled from: lambda */
public final /* synthetic */ class C3677a implements HostnameVerifier {

    /* renamed from: a */
    private final /* synthetic */ App f6404a;

    public /* synthetic */ C3677a(App app) {
        this.f6404a = app;
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        return this.f6404a.mo22676a(str, sSLSession);
    }
}
