package com.heimavista.fiedorasport.p111ui.activity.heart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import p119e.p189e.p193d.p194i.HeartRateDataDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateAdapter */
public class HeartRateAdapter extends BaseRecyclerViewAdapter<C4262a, C4263b> {

    /* renamed from: i */
    private LayoutInflater f8145i;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateAdapter$a */
    static class C4262a {

        /* renamed from: a */
        private int f8146a;

        /* renamed from: b */
        private HeartRateDataDb f8147b;

        public C4262a(int i, HeartRateDataDb dVar) {
            this.f8146a = i;
            this.f8147b = dVar;
        }

        /* renamed from: a */
        public HeartRateDataDb mo24420a() {
            return this.f8147b;
        }

        /* renamed from: b */
        public int mo24421b() {
            return this.f8146a;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateAdapter$b */
    class C4263b extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        TextView f8148R = ((TextView) findViewById(R$id.tvDate));

        /* renamed from: S */
        TextView f8149S = ((TextView) findViewById(R$id.tvHeartRate));

        C4263b(HeartRateAdapter heartRateAdapter, View view) {
            super(view);
        }
    }

    public HeartRateAdapter(Context context) {
        super(context);
        this.f8145i = LayoutInflater.from(context);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4263b bVar, int i) {
        HeartRateDataDb a = ((C4262a) getItem(i)).mo24420a();
        Date date = new Date(a.mo24259p() * 1000);
        int itemViewType = getItemViewType(i);
        if (itemViewType != 1) {
            if (itemViewType == 2) {
                bVar.f8148R.setText(TimeUtils.m1003a(date, new SimpleDateFormat("HH:mm", Locale.getDefault())));
                SpanUtils a2 = SpanUtils.m865a(bVar.f8149S);
                a2.mo9735a(String.valueOf(a.mo24232o()));
                a2.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_18));
                a2.mo9735a(mo22781a(R$string.data_unit_rate));
                a2.mo9736b();
            }
        } else if (TimeUtils.m1014c(date)) {
            bVar.f8148R.setText(R$string.today);
        } else {
            bVar.f8148R.setText(TimeUtils.m1003a(date, new SimpleDateFormat("MM月dd日", Locale.getDefault())));
        }
    }

    public int getItemViewType(int i) {
        return ((C4262a) getItem(i)).mo24421b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4263b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i != 1) {
            return new C4263b(this, this.f8145i.inflate(R$layout.layout_item_heart_rate, viewGroup, false));
        }
        return new C4263b(this, this.f8145i.inflate(R$layout.layout_item_heart_rate_date, viewGroup, false));
    }
}
