package com.heimavista.fiedorasport.p111ui.activity.setting;

import p112d.p113a.p114a.p116b.TimePicker;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.z */
/* compiled from: lambda */
public final /* synthetic */ class C4335z implements TimePicker.C3816b {

    /* renamed from: a */
    private final /* synthetic */ SettingSedentaryRemindActivity f8335a;

    /* renamed from: b */
    private final /* synthetic */ boolean f8336b;

    public /* synthetic */ C4335z(SettingSedentaryRemindActivity settingSedentaryRemindActivity, boolean z) {
        this.f8335a = settingSedentaryRemindActivity;
        this.f8336b = z;
    }

    /* renamed from: a */
    public final void mo23111a(String str, String str2) {
        this.f8335a.mo24491a(this.f8336b, str, str2);
    }
}
