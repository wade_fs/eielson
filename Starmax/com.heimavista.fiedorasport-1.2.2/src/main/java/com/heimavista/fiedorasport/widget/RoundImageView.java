package com.heimavista.fiedorasport.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.heimavista.fiedorasport.lib.R$styleable;

public class RoundImageView extends ImageView {

    /* renamed from: l0 */
    private static final C4493b[] f8963l0 = {C4493b.NORMAL, C4493b.CIRCLE, C4493b.ROUND_RECT};

    /* renamed from: P */
    private Paint f8964P;

    /* renamed from: Q */
    private float f8965Q;

    /* renamed from: R */
    private int f8966R;

    /* renamed from: S */
    private boolean f8967S;

    /* renamed from: T */
    private float f8968T;

    /* renamed from: U */
    private float f8969U;

    /* renamed from: V */
    private float f8970V;

    /* renamed from: W */
    private float f8971W;

    /* renamed from: a0 */
    private C4493b f8972a0;

    /* renamed from: b0 */
    private boolean f8973b0;

    /* renamed from: c0 */
    private String f8974c0;

    /* renamed from: d0 */
    private int f8975d0;

    /* renamed from: e0 */
    private int f8976e0;

    /* renamed from: f0 */
    private int f8977f0;

    /* renamed from: g0 */
    private int f8978g0;

    /* renamed from: h0 */
    private int f8979h0;

    /* renamed from: i0 */
    private int f8980i0;

    /* renamed from: j0 */
    private TextPaint f8981j0;

    /* renamed from: k0 */
    private String f8982k0;

    /* renamed from: com.heimavista.fiedorasport.widget.RoundImageView$a */
    static /* synthetic */ class C4492a {

        /* renamed from: a */
        static final /* synthetic */ int[] f8983a = new int[C4493b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.heimavista.fiedorasport.widget.RoundImageView$b[] r0 = com.heimavista.fiedorasport.widget.RoundImageView.C4493b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.heimavista.fiedorasport.widget.RoundImageView.C4492a.f8983a = r0
                int[] r0 = com.heimavista.fiedorasport.widget.RoundImageView.C4492a.f8983a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.heimavista.fiedorasport.widget.RoundImageView$b r1 = com.heimavista.fiedorasport.widget.RoundImageView.C4493b.CIRCLE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.heimavista.fiedorasport.widget.RoundImageView.C4492a.f8983a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.heimavista.fiedorasport.widget.RoundImageView$b r1 = com.heimavista.fiedorasport.widget.RoundImageView.C4493b.ROUND_RECT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.heimavista.fiedorasport.widget.RoundImageView.C4492a.<clinit>():void");
        }
    }

    /* renamed from: com.heimavista.fiedorasport.widget.RoundImageView$b */
    public enum C4493b {
        NORMAL(0),
        CIRCLE(1),
        ROUND_RECT(2);

        private C4493b(int i) {
        }
    }

    public RoundImageView(Context context) {
        this(context, null);
    }

    /* renamed from: a */
    private void m14683a(Context context, AttributeSet attributeSet) {
        setLayerType(1, null);
        this.f8964P = new Paint();
        this.f8981j0 = new TextPaint();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.RoundImageView);
            this.f8965Q = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_borderWidth, this.f8965Q);
            this.f8966R = obtainStyledAttributes.getColor(R$styleable.RoundImageView_borderColor, this.f8966R);
            this.f8967S = obtainStyledAttributes.getBoolean(R$styleable.RoundImageView_displayBorder, this.f8967S);
            this.f8968T = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_leftTopRadius, this.f8968T);
            this.f8969U = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_rightTopRadius, this.f8969U);
            this.f8970V = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_leftBottomRadius, this.f8970V);
            this.f8971W = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_rightBottomRadius, this.f8971W);
            float dimension = obtainStyledAttributes.getDimension(R$styleable.RoundImageView_radius, 0.0f);
            if (dimension > 0.0f) {
                this.f8971W = dimension;
                this.f8969U = dimension;
                this.f8970V = dimension;
                this.f8968T = dimension;
            }
            int i = obtainStyledAttributes.getInt(R$styleable.RoundImageView_displayType, -1);
            if (i >= 0) {
                this.f8972a0 = f8963l0[i];
            } else {
                this.f8972a0 = C4493b.NORMAL;
            }
            this.f8973b0 = obtainStyledAttributes.getBoolean(R$styleable.RoundImageView_displayLabel, this.f8973b0);
            this.f8974c0 = obtainStyledAttributes.getString(R$styleable.RoundImageView_text);
            this.f8977f0 = obtainStyledAttributes.getColor(R$styleable.RoundImageView_labelBackground, this.f8977f0);
            this.f8976e0 = obtainStyledAttributes.getDimensionPixelSize(R$styleable.RoundImageView_textSize, this.f8976e0);
            this.f8975d0 = obtainStyledAttributes.getColor(R$styleable.RoundImageView_textColor, this.f8975d0);
            this.f8979h0 = obtainStyledAttributes.getDimensionPixelSize(R$styleable.RoundImageView_labelWidth, this.f8979h0);
            this.f8978g0 = obtainStyledAttributes.getInt(R$styleable.RoundImageView_labelGravity, this.f8978g0);
            this.f8980i0 = obtainStyledAttributes.getDimensionPixelSize(R$styleable.RoundImageView_startMargin, this.f8980i0);
            m14682a(obtainStyledAttributes.getInt(R$styleable.RoundImageView_typeface, -1), obtainStyledAttributes.getInt(R$styleable.RoundImageView_textStyle, -1));
            this.f8982k0 = this.f8974c0;
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: b */
    private void m14685b(Canvas canvas) {
        Path path = new Path();
        Path path2 = new Path();
        int i = this.f8978g0;
        if (i == 0) {
            path.moveTo((float) this.f8980i0, 0.0f);
            path.rLineTo(getBevelLineLength(), 0.0f);
            path.lineTo(0.0f, ((float) this.f8980i0) + getBevelLineLength());
            path.rLineTo(0.0f, -getBevelLineLength());
            path.close();
            path2.moveTo(0.0f, ((float) this.f8980i0) + (getBevelLineLength() / 2.0f));
            path2.lineTo(((float) this.f8980i0) + (getBevelLineLength() / 2.0f), 0.0f);
        } else if (i == 1) {
            path.moveTo((float) this.f8980i0, (float) getHeight());
            path.rLineTo(getBevelLineLength(), 0.0f);
            path.lineTo(0.0f, ((float) getHeight()) - (((float) this.f8980i0) + getBevelLineLength()));
            path.rLineTo(0.0f, getBevelLineLength());
            path.close();
            path2.moveTo(0.0f, ((float) getHeight()) - (((float) this.f8980i0) + (getBevelLineLength() / 2.0f)));
            path2.lineTo(((float) this.f8980i0) + (getBevelLineLength() / 2.0f), (float) getHeight());
        } else if (i == 2) {
            path.moveTo((float) (getWidth() - this.f8980i0), 0.0f);
            path.rLineTo(-getBevelLineLength(), 0.0f);
            path.lineTo((float) getWidth(), ((float) this.f8980i0) + getBevelLineLength());
            path.rLineTo(0.0f, -getBevelLineLength());
            path.close();
            path2.moveTo(((float) getWidth()) - (((float) this.f8980i0) + (getBevelLineLength() / 2.0f)), 0.0f);
            path2.lineTo((float) getWidth(), ((float) this.f8980i0) + (getBevelLineLength() / 2.0f));
        } else if (i == 3) {
            path.moveTo((float) (getWidth() - this.f8980i0), (float) getHeight());
            path.rLineTo(-getBevelLineLength(), 0.0f);
            path.lineTo((float) getWidth(), ((float) getHeight()) - (((float) this.f8980i0) + getBevelLineLength()));
            path.rLineTo(0.0f, getBevelLineLength());
            path.close();
            path2.moveTo(((float) getWidth()) - (((float) this.f8980i0) + (getBevelLineLength() / 2.0f)), (float) getHeight());
            path2.lineTo((float) getWidth(), ((float) getHeight()) - (((float) this.f8980i0) + (getBevelLineLength() / 2.0f)));
        }
        this.f8981j0.setAntiAlias(true);
        this.f8981j0.setStyle(Paint.Style.FILL);
        this.f8981j0.setColor(this.f8977f0);
        canvas.drawPath(path, this.f8981j0);
        this.f8981j0.setTextSize((float) this.f8976e0);
        this.f8981j0.setColor(this.f8975d0);
        if (this.f8982k0 == null) {
            this.f8982k0 = "";
        }
        this.f8981j0.setTextAlign(Paint.Align.CENTER);
        float measureText = this.f8981j0.measureText(this.f8982k0);
        float length = new PathMeasure(path2, false).getLength();
        if (measureText > length) {
            float length2 = measureText / ((float) this.f8982k0.length());
            StringBuilder sb = new StringBuilder();
            String str = this.f8982k0;
            sb.append(str.substring(0, str.length() - (((int) Math.floor((double) ((measureText - length) / length2))) + 2)));
            sb.append("...");
            this.f8982k0 = sb.toString();
        }
        Paint.FontMetricsInt fontMetricsInt = this.f8981j0.getFontMetricsInt();
        int i2 = fontMetricsInt.bottom;
        Canvas canvas2 = canvas;
        canvas2.drawTextOnPath(this.f8982k0, path2, 0.0f, (float) (((i2 - fontMetricsInt.top) / 2) - i2), this.f8981j0);
    }

    /* renamed from: c */
    private void m14686c(Canvas canvas) {
        if (this.f8972a0 != C4493b.NORMAL) {
            this.f8964P.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
            Path a = m14680a();
            a.setFillType(Path.FillType.INVERSE_EVEN_ODD);
            canvas.drawPath(a, this.f8964P);
            this.f8964P.setXfermode(null);
        }
        if (this.f8967S) {
            m14684a(canvas);
        }
        if (this.f8973b0) {
            m14685b(canvas);
        }
    }

    private float getBevelLineLength() {
        return (float) Math.sqrt(Math.pow((double) this.f8979h0, 2.0d) * 2.0d);
    }

    public int getBorderColor() {
        return this.f8966R;
    }

    public float getBorderWidth() {
        return this.f8965Q;
    }

    public C4493b getDisplayType() {
        return this.f8972a0;
    }

    public int getLableBackground() {
        return this.f8977f0;
    }

    public int getLableGravity() {
        return this.f8978g0;
    }

    public String getLableText() {
        return this.f8974c0;
    }

    public int getLableWidth() {
        return this.f8979h0;
    }

    public float getLeftBottomRadius() {
        return this.f8970V;
    }

    public float getLeftTopRadius() {
        return this.f8968T;
    }

    public float getRightBottomRadius() {
        return this.f8971W;
    }

    public float getRightTopRadius() {
        return this.f8969U;
    }

    public int getStartMargin() {
        return this.f8980i0;
    }

    public int getTextColor() {
        return this.f8975d0;
    }

    public int getTextSize() {
        return this.f8976e0;
    }

    public Typeface getTypeface() {
        return this.f8981j0.getTypeface();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DrawAllocation"})
    public void onDraw(Canvas canvas) {
        if (getDrawable() != null) {
            m14681a(Math.min(getWidth(), getHeight()) / 2);
            Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap);
            super.onDraw(canvas2);
            this.f8964P.reset();
            this.f8964P.setAntiAlias(true);
            this.f8964P.setDither(true);
            m14686c(canvas2);
            canvas.drawBitmap(createBitmap, 0.0f, 0.0f, this.f8964P);
            createBitmap.recycle();
            return;
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        View.MeasureSpec.getMode(i);
        View.MeasureSpec.getMode(i2);
        if (this.f8972a0 == C4493b.CIRCLE) {
            if (size >= size2) {
                i = i2;
            }
            if (size <= 0) {
                i = i2;
            }
            super.onMeasure(i, i);
            return;
        }
        super.onMeasure(i, i2);
    }

    public void setBorderColor(int i) {
        if (this.f8966R != i) {
            this.f8966R = i;
            if (this.f8967S) {
                postInvalidate();
            }
        }
    }

    public void setBorderWidth(float f) {
        if (this.f8965Q != f) {
            this.f8965Q = f;
            if (this.f8967S) {
                postInvalidate();
            }
        }
    }

    public void setDisplayBorder(boolean z) {
        if (this.f8967S != z) {
            this.f8967S = z;
            postInvalidate();
        }
    }

    public void setDisplayLable(boolean z) {
        if (this.f8973b0 != z) {
            this.f8973b0 = z;
            if (z) {
                postInvalidate();
            }
        }
    }

    public void setDisplayType(C4493b bVar) {
        if (this.f8972a0 != bVar) {
            this.f8972a0 = bVar;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setLableBackground(int i) {
        if (this.f8977f0 != i) {
            this.f8977f0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setLableGravity(int i) {
        if (this.f8978g0 != i) {
            this.f8978g0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setLableText(String str) {
        if (!TextUtils.equals(this.f8974c0, str)) {
            this.f8974c0 = str;
            this.f8982k0 = str;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setLableWidth(int i) {
        if (this.f8979h0 != i) {
            this.f8979h0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setLeftBottomRadius(float f) {
        if (this.f8970V != f) {
            this.f8970V = f;
            if (this.f8972a0 != C4493b.NORMAL) {
                postInvalidate();
            }
        }
    }

    public void setLeftTopRadius(float f) {
        if (this.f8968T != f) {
            this.f8968T = f;
            if (this.f8972a0 != C4493b.NORMAL) {
                postInvalidate();
            }
        }
    }

    public void setRadius(float f) {
        mo24791a(f, f, f, f);
    }

    public void setRightBottomRadius(float f) {
        if (this.f8971W != f) {
            this.f8971W = f;
            if (this.f8972a0 != C4493b.NORMAL) {
                postInvalidate();
            }
        }
    }

    public void setRightTopRadius(float f) {
        if (this.f8969U != f) {
            this.f8969U = f;
            if (this.f8972a0 != C4493b.NORMAL) {
                postInvalidate();
            }
        }
    }

    public void setStartMargin(int i) {
        if (this.f8980i0 != i) {
            this.f8980i0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setTextColor(int i) {
        if (this.f8975d0 != i) {
            this.f8975d0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setTextSize(int i) {
        if (this.f8976e0 != i) {
            this.f8976e0 = i;
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public void setTypeface(Typeface typeface) {
        if (this.f8981j0.getTypeface() != typeface) {
            this.f8981j0.setTypeface(typeface);
            if (this.f8973b0) {
                postInvalidate();
            }
        }
    }

    public RoundImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RoundImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f8964P = null;
        this.f8965Q = 2.0f;
        this.f8966R = Color.parseColor("#8A2BE2");
        this.f8973b0 = false;
        this.f8975d0 = -1;
        this.f8976e0 = 15;
        this.f8977f0 = Color.parseColor("#9FFF0000");
        this.f8978g0 = 2;
        this.f8979h0 = 15;
        this.f8980i0 = 20;
        this.f8981j0 = null;
        m14683a(context, attributeSet);
    }

    /* renamed from: a */
    private void m14681a(int i) {
        float f = (float) i;
        this.f8968T = Math.min(this.f8968T, f);
        this.f8969U = Math.min(this.f8969U, f);
        this.f8970V = Math.min(this.f8970V, f);
        this.f8971W = Math.min(this.f8971W, f);
        int i2 = i / 2;
        this.f8965Q = Math.min(this.f8965Q, (float) i2);
        this.f8979h0 = Math.min(this.f8979h0, i2);
        this.f8976e0 = Math.min(this.f8976e0, this.f8979h0);
        this.f8980i0 = Math.min(this.f8980i0, (int) (((float) (i * 2)) - getBevelLineLength()));
    }

    /* renamed from: a */
    private void m14684a(Canvas canvas) {
        this.f8964P.setStyle(Paint.Style.STROKE);
        this.f8964P.setColor(this.f8966R);
        this.f8964P.setStrokeWidth(this.f8965Q);
        canvas.drawPath(m14680a(), this.f8964P);
    }

    /* renamed from: a */
    private Path m14680a() {
        Path path = new Path();
        float f = this.f8965Q / 2.0f;
        int i = C4492a.f8983a[this.f8972a0.ordinal()];
        if (i == 1) {
            path.addCircle((float) (getWidth() / 2), (float) (getHeight() / 2), ((float) (getWidth() / 2)) - f, Path.Direction.CW);
        } else if (i != 2) {
            RectF rectF = new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            rectF.inset(f, f);
            path.addRect(rectF, Path.Direction.CW);
        } else {
            RectF rectF2 = new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            rectF2.inset(f, f);
            float f2 = this.f8968T;
            float f3 = this.f8969U;
            float f4 = this.f8971W;
            float f5 = this.f8970V;
            path.addRoundRect(rectF2, new float[]{f2, f2, f3, f3, f4, f4, f5, f5}, Path.Direction.CW);
        }
        return path;
    }

    /* renamed from: a */
    public void mo24791a(float f, float f2, float f3, float f4) {
        if (this.f8968T != f || this.f8969U != f2 || this.f8970V != f3 || this.f8971W != f4) {
            this.f8968T = f;
            this.f8969U = f2;
            this.f8970V = f3;
            this.f8971W = f4;
            if (this.f8972a0 != C4493b.NORMAL) {
                postInvalidate();
            }
        }
    }

    /* renamed from: a */
    private void m14682a(int i, int i2) {
        Typeface typeface;
        if (i == 1) {
            typeface = Typeface.SANS_SERIF;
        } else if (i != 2) {
            typeface = i != 3 ? null : Typeface.MONOSPACE;
        } else {
            typeface = Typeface.SERIF;
        }
        mo24792a(typeface, i2);
    }

    /* renamed from: a */
    public void mo24792a(Typeface typeface, int i) {
        Typeface typeface2;
        float f = 0.0f;
        boolean z = false;
        if (i > 0) {
            if (typeface == null) {
                typeface2 = Typeface.defaultFromStyle(i);
            } else {
                typeface2 = Typeface.create(typeface, i);
            }
            setTypeface(typeface2);
            int i2 = (~(typeface2 != null ? typeface2.getStyle() : 0)) & i;
            TextPaint textPaint = this.f8981j0;
            if ((i2 & 1) != 0) {
                z = true;
            }
            textPaint.setFakeBoldText(z);
            TextPaint textPaint2 = this.f8981j0;
            if ((i2 & 2) != 0) {
                f = -0.25f;
            }
            textPaint2.setTextSkewX(f);
            return;
        }
        this.f8981j0.setFakeBoldText(false);
        this.f8981j0.setTextSkewX(0.0f);
        setTypeface(typeface);
    }
}
