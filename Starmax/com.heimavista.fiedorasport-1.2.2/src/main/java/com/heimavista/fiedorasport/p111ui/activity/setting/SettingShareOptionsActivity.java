package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiSetBuddyShareStat;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.adapter.ListShareOptionAdapter;
import com.heimavista.utils.ParamJsonData;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p193d.p195j.BuddyShareDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingShareOptionsActivity */
public class SettingShareOptionsActivity extends BaseActivity {

    /* renamed from: b0 */
    private TextView f8278b0;

    /* renamed from: c0 */
    private RecyclerView f8279c0;

    /* renamed from: d0 */
    private List<MenuItem> f8280d0 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public BuddyShareDb f8281e0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingShareOptionsActivity$a */
    class C4299a implements OnResultListener<ParamJsonData> {
        C4299a() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            SettingShareOptionsActivity.this.mo22717b();
            SettingShareOptionsActivity.this.f8281e0.mo24181l();
            SettingShareOptionsActivity.super.onBackPressed();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            SettingShareOptionsActivity.this.mo22717b();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_share_option;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.setting_share_permission_with_friend);
    }

    public void onBackPressed() {
        String stringExtra = getIntent().getStringExtra("userNbr");
        BuddyShareDb i = BuddyShareDb.m13288i(stringExtra);
        boolean z = i.mo24321r() != this.f8281e0.mo24321r();
        if (i.mo24322s() != this.f8281e0.mo24322s()) {
            z = true;
        }
        if (i.mo24319p() != this.f8281e0.mo24319p()) {
            z = true;
        }
        if (i.mo24320q() != this.f8281e0.mo24320q()) {
            z = true;
        }
        LogUtils.m1139c("mady op=setBu1 " + i.mo24205i());
        LogUtils.m1139c("mady op=setBu2 " + this.f8281e0.mo24205i());
        if (z) {
            mo22751t();
            new HvApiSetBuddyShareStat().request(stringExtra, this.f8281e0.mo24322s(), this.f8281e0.mo24321r(), this.f8281e0.mo24319p(), this.f8281e0.mo24320q(), new C4299a());
            return;
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        ListShareOptionAdapter listShareOptionAdapter = new ListShareOptionAdapter(super);
        listShareOptionAdapter.mo24653a((ListShareOptionAdapter.C4408a) new C4301a0(this));
        this.f8279c0.setAdapter(listShareOptionAdapter);
        String stringExtra = getIntent().getStringExtra("userName");
        this.f8281e0 = BuddyShareDb.m13288i(getIntent().getStringExtra("userNbr"));
        List<MenuItem> list = this.f8280d0;
        MenuItem.C3676b bVar = new MenuItem.C3676b();
        bVar.mo22663b(2);
        bVar.mo22664b(getString(R$string.sleep));
        bVar.mo22660a(this.f8281e0.mo24321r());
        list.add(bVar.mo22661a());
        List<MenuItem> list2 = this.f8280d0;
        MenuItem.C3676b bVar2 = new MenuItem.C3676b();
        bVar2.mo22663b(2);
        bVar2.mo22664b(getString(R$string.sport_step));
        bVar2.mo22660a(this.f8281e0.mo24322s());
        list2.add(bVar2.mo22661a());
        List<MenuItem> list3 = this.f8280d0;
        MenuItem.C3676b bVar3 = new MenuItem.C3676b();
        bVar3.mo22663b(2);
        bVar3.mo22664b(getString(R$string.heart_rate));
        bVar3.mo22660a(this.f8281e0.mo24319p());
        list3.add(bVar3.mo22661a());
        List<MenuItem> list4 = this.f8280d0;
        MenuItem.C3676b bVar4 = new MenuItem.C3676b();
        bVar4.mo22663b(2);
        bVar4.mo22664b(getString(R$string.location));
        bVar4.mo22660a(this.f8281e0.mo24320q());
        list4.add(bVar4.mo22661a());
        listShareOptionAdapter.mo22792b((List) this.f8280d0);
        if (!TextUtils.isEmpty(stringExtra)) {
            SpanUtils a = SpanUtils.m865a(this.f8278b0);
            a.mo9735a(stringExtra);
            a.mo9737b(getResources().getColor(R$color.blue));
            a.mo9735a(getString(R$string.share_data));
            a.mo9736b();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8278b0 = (TextView) findViewById(R$id.tvUserName);
        this.f8279c0 = (RecyclerView) findViewById(R$id.recyclerView);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24495a(int i, MenuItem eVar, boolean z) {
        if (getString(R$string.sleep).equals(eVar.mo22652d())) {
            this.f8281e0.mo24316c(z);
        } else if (getString(R$string.sport_step).equals(eVar.mo22652d())) {
            this.f8281e0.mo24317d(z);
        } else if (getString(R$string.heart_rate).equals(eVar.mo22652d())) {
            this.f8281e0.mo24314a(z);
        } else if (getString(R$string.location).equals(eVar.mo22652d())) {
            this.f8281e0.mo24315b(z ? 1 : 0);
        }
    }
}
