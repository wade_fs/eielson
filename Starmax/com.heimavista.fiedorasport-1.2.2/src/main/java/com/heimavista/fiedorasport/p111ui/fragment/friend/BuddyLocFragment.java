package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.Marker;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.C2931b;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiSetShareStat;
import com.heimavista.api.buddy.HvApiGetBuddyLoc;
import com.heimavista.api.buddy.HvApiSetBuddyShareStat;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.fiedorasport.p110i.Toast;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListBuddyLocAdapter;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.GeocoderUtils;
import com.heimavista.fiedorasport.service.FSJobService;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.SwitchButton;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.BuddyShareDb;
import p119e.p189e.p193d.p195j.LocDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment */
public class BuddyLocFragment extends BaseFragment<HomeActivity> {

    /* renamed from: A0 */
    private Toast f8713A0;

    /* renamed from: W */
    private boolean f8714W = FSManager.m10325t();

    /* renamed from: X */
    private MapFragment f8715X;

    /* renamed from: Y */
    private C2880c f8716Y;

    /* renamed from: Z */
    private ArrayMap<String, C2932c> f8717Z = new ArrayMap<>();

    /* renamed from: a0 */
    private AMap f8718a0;

    /* renamed from: b0 */
    private ArrayMap<String, Marker> f8719b0 = new ArrayMap<>();

    /* renamed from: c0 */
    private View f8720c0;

    /* renamed from: d0 */
    private ConstraintLayout f8721d0;

    /* renamed from: e0 */
    private ConstraintLayout f8722e0;

    /* renamed from: f0 */
    private ImageView f8723f0;

    /* renamed from: g0 */
    private TextView f8724g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public TextView f8725h0;

    /* renamed from: i0 */
    private TextView f8726i0;

    /* renamed from: j0 */
    private TextView f8727j0;

    /* renamed from: k0 */
    private TextView f8728k0;

    /* renamed from: l0 */
    private TextView f8729l0;
    /* access modifiers changed from: private */

    /* renamed from: m0 */
    public View f8730m0;
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public View f8731n0;
    /* access modifiers changed from: private */

    /* renamed from: o0 */
    public boolean f8732o0 = false;
    /* access modifiers changed from: private */

    /* renamed from: p0 */
    public SwitchButton f8733p0;
    /* access modifiers changed from: private */

    /* renamed from: q0 */
    public SwitchButton f8734q0;

    /* renamed from: r0 */
    private ListBuddyLocAdapter f8735r0;
    /* access modifiers changed from: private */

    /* renamed from: s0 */
    public PeriodTimeInfo f8736s0;

    /* renamed from: t0 */
    private float f8737t0 = 0.0f;

    /* renamed from: u0 */
    private String f8738u0 = "";
    /* access modifiers changed from: private */

    /* renamed from: v0 */
    public boolean f8739v0 = true;

    /* renamed from: w0 */
    private ThreadUtils.C0877e<HvApiResponse> f8740w0;

    /* renamed from: x0 */
    private ThreadUtils.C0877e<HvApiResponse> f8741x0;

    /* renamed from: y0 */
    private int f8742y0 = 20;

    /* renamed from: z0 */
    private HttpCancelable f8743z0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$a */
    class C4432a extends RecyclerView.OnScrollListener {
        C4432a(BuddyLocFragment buddyLocFragment) {
        }

        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
        }

        public void onScrolled(@NonNull RecyclerView recyclerView, int i, int i2) {
            super.onScrolled(recyclerView, i, i2);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$b */
    class C4433b implements GeocoderUtils.C4221b {
        C4433b() {
        }

        /* renamed from: a */
        public void mo24353a(Throwable th) {
        }

        /* renamed from: a */
        public void mo24354a(List<Address> list) {
            if (list != null && list.size() > 0) {
                Address address = list.get(0);
                if (BuddyLocFragment.this.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                    BuddyLocFragment.this.f8725h0.setText(address.getAddressLine(0));
                }
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$c */
    class C4434c extends ThreadUtils.C0877e<HvApiResponse> {
        C4434c() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, int]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(e.e.d.j.a, e.e.d.j.a):int
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, com.heimavista.entity.data.PeriodTimeInfo):com.heimavista.entity.data.PeriodTimeInfo
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, java.lang.String):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.animation.ValueAnimator, android.animation.ValueAnimator):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void */
        /* renamed from: a */
        public void mo9795a(HvApiResponse cVar) {
            if (cVar != null && cVar.mo22515d() == 1) {
                BuddyLocFragment.this.m14430b(true);
                if (BuddyLocFragment.this.f8739v0) {
                    BuddyLocFragment.this.m14443j();
                }
            }
        }

        /* renamed from: b */
        public HvApiResponse m14470b() {
            if (BuddyLocFragment.this.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                return new HvApiGetBuddyLoc().requestSync(new HttpParams());
            }
            BuddyLocFragment.this.mo24680i();
            return null;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$d */
    class C4435d extends ThreadUtils.C0877e<HvApiResponse> {
        C4435d() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, int]
         candidates:
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(e.e.d.j.a, e.e.d.j.a):int
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, com.heimavista.entity.data.PeriodTimeInfo):com.heimavista.entity.data.PeriodTimeInfo
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, java.lang.String):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.animation.ValueAnimator, android.animation.ValueAnimator):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
          com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void */
        /* renamed from: a */
        public void mo9795a(HvApiResponse cVar) {
            boolean unused = BuddyLocFragment.this.f8739v0 = false;
            if (cVar != null && cVar.mo22515d() == 1 && BuddyLocFragment.this.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                BuddyLocFragment.this.m14430b(true);
            }
        }

        /* renamed from: b */
        public HvApiResponse m14476b() {
            return new HvApiGetBuddyLoc().requestSync(new HttpParams());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$e */
    class C4436e implements PermissionUtils.C0922e {
        C4436e(BuddyLocFragment buddyLocFragment) {
        }

        /* renamed from: a */
        public void mo9866a() {
            LocManager.m10493g().mo22957b();
        }

        /* renamed from: b */
        public void mo9867b() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$f */
    class C4437f implements OnResultListener<ParamJsonData> {

        /* renamed from: a */
        final /* synthetic */ BuddyShareDb f8747a;

        C4437f(BuddyShareDb bVar) {
            this.f8747a = bVar;
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            BuddyLocFragment.this.mo22768b();
            BuddyShareDb bVar = this.f8747a;
            bVar.mo24315b(bVar.mo24320q() ^ true ? 1 : 0);
            this.f8747a.mo24181l();
            BuddyLocFragment.this.m14434c(this.f8747a.mo24320q());
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            BuddyLocFragment.this.mo22768b();
            BuddyLocFragment.this.mo22761a(str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment$g */
    class C4438g implements OnResultListener<ParamJsonData> {

        /* renamed from: a */
        final /* synthetic */ int f8749a;

        C4438g(int i) {
            this.f8749a = i;
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            BuddyLocFragment buddyLocFragment = BuddyLocFragment.this;
            boolean unused = buddyLocFragment.f8732o0 = !buddyLocFragment.f8732o0;
            if (BuddyLocFragment.this.f8732o0) {
                FSJobService.m10693a();
                LocManager.m10493g().mo22958c();
            } else {
                FSJobService.m10694a(4);
            }
            BuddyLocFragment.this.f8730m0.setEnabled(true);
            BuddyLocFragment.this.f8731n0.setEnabled(true);
            BuddyLocFragment.this.f8733p0.setEnabled(true);
            BuddyLocFragment.this.f8734q0.setEnabled(true);
            BuddyLocFragment.this.f8733p0.setChecked(BuddyLocFragment.this.f8732o0);
            BuddyLocFragment.this.f8734q0.setChecked(BuddyLocFragment.this.f8732o0);
            BuddyLocFragment.this.f8733p0.setEnabled(false);
            BuddyLocFragment.this.f8734q0.setEnabled(false);
            if (BuddyLocFragment.this.f8732o0) {
                BuddyLocFragment.this.m14433c("您的位置將與朋友分享");
                Calendar instance = Calendar.getInstance();
                int i = this.f8749a;
                if (i == 1) {
                    instance.set(11, instance.get(11) + 1);
                } else if (i == 2) {
                    instance.set(11, 0);
                    instance.set(12, 0);
                    instance.set(13, 0);
                    instance.add(6, 1);
                } else {
                    instance.add(1, 50);
                }
                BandDataManager.m10558b(instance.getTimeInMillis());
            } else {
                BandDataManager.m10558b(0L);
                BuddyLocFragment.this.m14433c("位置分享已關閉");
            }
            if (BuddyLocFragment.this.f8736s0 == null) {
                PeriodTimeInfo unused2 = BuddyLocFragment.this.f8736s0 = BandDataManager.m10555b("Location");
            }
            BuddyLocFragment.this.f8736s0.mo22582a(BuddyLocFragment.this.f8732o0);
            BuddyLocFragment.this.f8736s0.mo22591e(this.f8749a);
            BandDataManager.m10550a(BuddyLocFragment.this.f8736s0);
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            BuddyLocFragment.this.f8730m0.setEnabled(true);
            BuddyLocFragment.this.f8731n0.setEnabled(true);
            BuddyLocFragment.this.mo22761a(str);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m14443j() {
        if (this.f8740w0 == null) {
            this.f8740w0 = new C4435d();
        }
        ThreadUtils.m954a(this.f8740w0, 5, TimeUnit.SECONDS);
    }

    /* renamed from: k */
    public static BuddyLocFragment m14444k() {
        Bundle bundle = new Bundle();
        BuddyLocFragment buddyLocFragment = new BuddyLocFragment();
        buddyLocFragment.setArguments(bundle);
        return buddyLocFragment;
    }

    /* renamed from: l */
    private void m14445l() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.LOCATION");
        b.mo9858a(new C4436e(this));
        b.mo9859a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(e.e.d.j.a, e.e.d.j.a):int
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, com.heimavista.entity.data.PeriodTimeInfo):com.heimavista.entity.data.PeriodTimeInfo
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, java.lang.String):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.animation.ValueAnimator, android.animation.ValueAnimator):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void */
    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        int i = 3;
        if (id == R$id.layoutFriends) {
            m14426a(true, ScreenUtils.m1264b() / 3);
        } else if (id == R$id.layoutLocShare || id == R$id.llLocShare) {
            HttpCancelable bVar = this.f8743z0;
            if (bVar != null) {
                bVar.cancel();
            }
            if (this.f8732o0) {
                i = 0;
            }
            m14428b(i);
        } else if (id == R$id.tvFriend) {
            m14426a(false, this.f8721d0.getLayoutParams().height);
        } else if (id == R$id.ivPos) {
            m14445l();
        } else if (id == R$id.ivClose) {
            this.f8722e0.setVisibility(8);
            this.f8738u0 = "";
        } else if (id == R$id.tvShareOptions && !TextUtils.isEmpty(this.f8738u0)) {
            BuddyShareDb i2 = BuddyShareDb.m13288i(this.f8738u0);
            mo22776h();
            new HvApiSetBuddyShareStat().request(this.f8738u0, i2.mo24322s(), i2.mo24321r(), i2.mo24319p(), !i2.mo24320q(), new C4437f(i2));
        }
    }

    public void onPause() {
        mo24680i();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        mo24688a(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return new String[]{"LocManager.action_location_callback"};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        List<BuddyListDb> H = BuddyListDb.m13247H();
        this.f8715X = MapFragment.m14638a(this.f8714W, false);
        if (this.f8714W) {
            this.f8715X.mo24729a(new C4463v(this));
        } else {
            this.f8715X.mo24729a(new C4461t(this));
        }
        getChildFragmentManager().beginTransaction().replace(R$id.mapFragment, this.f8715X, "mapFragment").commit();
        m14425a(H);
        m14445l();
    }

    /* renamed from: i */
    public void mo24680i() {
        m14429b("cancelThreadTask");
        ThreadUtils.C0877e<HvApiResponse> eVar = this.f8740w0;
        if (eVar != null) {
            ThreadUtils.m952a(eVar);
            this.f8740w0 = null;
        }
        ThreadUtils.C0877e<HvApiResponse> eVar2 = this.f8741x0;
        if (eVar2 != null) {
            ThreadUtils.m952a(eVar2);
            this.f8741x0 = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14430b(boolean z) {
        double d;
        boolean z2;
        double d2;
        boolean z3;
        boolean isAtLeast = getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED);
        if (z) {
            this.f8715X.mo24730b();
            String l = MemberControl.m17125s().mo27187l();
            double d3 = 0.0d;
            if (this.f8714W) {
                C2932c cVar = this.f8717Z.get(l);
                if (cVar != null) {
                    d3 = cVar.mo18625a().f4834P;
                    d = cVar.mo18625a().f4835Q;
                    z2 = true;
                } else {
                    d = 0.0d;
                    z2 = false;
                }
                this.f8717Z.clear();
            } else {
                Marker marker = this.f8719b0.get(l);
                if (marker != null) {
                    d3 = marker.getPosition().latitude;
                    d2 = marker.getPosition().longitude;
                    z3 = true;
                } else {
                    d2 = 0.0d;
                    z3 = false;
                }
                this.f8719b0.clear();
            }
            double d4 = d;
            double d5 = d3;
            if (isAtLeast && z2) {
                UserInfoDb.m13341a(getContext(), l, new C4460s(this, d5, d4, l));
            }
        }
        List<BuddyListDb> H = BuddyListDb.m13247H();
        int i = 60;
        boolean z4 = true;
        for (BuddyListDb aVar : H) {
            String x = aVar.mo24311x();
            LocDb h = LocDb.m13307h(x);
            if (h != null && h.mo24327t() > 0) {
                if (FSManager.m10325t()) {
                    LatLng latLng = new LatLng(h.mo24323p(), h.mo24324q());
                    if (isAtLeast) {
                        if (aVar.mo24284B()) {
                            UserInfoDb.m13341a(getContext(), h.mo24328u(), new C4450i(this, x, latLng));
                        } else {
                            this.f8717Z.remove(x);
                        }
                    }
                } else if (FSManager.m10324s()) {
                    com.amap.api.maps.model.LatLng latLng2 = new com.amap.api.maps.model.LatLng(h.mo24323p(), h.mo24324q());
                    if (isAtLeast) {
                        if (aVar.mo24284B()) {
                            UserInfoDb.m13341a(getContext(), h.mo24328u(), new C4458q(this, x, latLng2));
                        } else {
                            this.f8719b0.remove(x);
                        }
                    }
                }
                if (z4 && h.mo24325r() == 1) {
                    i = 20;
                    z4 = false;
                }
            }
        }
        if (i != this.f8742y0) {
            mo24680i();
            int min = Math.min(i, this.f8742y0);
            this.f8742y0 = i;
            mo24688a(min);
        }
        if (isAtLeast) {
            m14425a(H);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m14434c(boolean z) {
        this.f8729l0.setVisibility(BandDataManager.m10555b("Location").mo22593g() ? 0 : 8);
        if (z) {
            this.f8729l0.setText(R$string.stop_share_my_loc);
            this.f8729l0.setTextColor(getResources().getColor(R$color.red));
            return;
        }
        this.f8729l0.setText(R$string.share_my_loc);
        this.f8729l0.setTextColor(getResources().getColor(R$color.blue));
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_buddy_loc;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        this.f8720c0 = view.findViewById(R$id.mapFragment);
        this.f8721d0 = (ConstraintLayout) view.findViewById(R$id.friendContainer);
        this.f8722e0 = (ConstraintLayout) view.findViewById(R$id.dataContainer);
        this.f8723f0 = (ImageView) view.findViewById(R$id.ivAvatar);
        this.f8724g0 = (TextView) view.findViewById(R$id.tvUserName);
        this.f8725h0 = (TextView) view.findViewById(R$id.tvAddress);
        this.f8726i0 = (TextView) view.findViewById(R$id.tvUpdateTime);
        this.f8727j0 = (TextView) view.findViewById(R$id.tvSpeed);
        this.f8728k0 = (TextView) view.findViewById(R$id.tvBattery);
        this.f8729l0 = (TextView) view.findViewById(R$id.tvShareOptions);
        this.f8733p0 = (SwitchButton) view.findViewById(R$id.btnSwitch);
        this.f8734q0 = (SwitchButton) view.findViewById(R$id.f6596sb);
        this.f8736s0 = BandDataManager.m10555b("Location");
        this.f8732o0 = this.f8736s0.mo22593g();
        if (BandDataManager.m10564f() > 0 && (this.f8736s0.mo22590e() == 1 || this.f8736s0.mo22590e() == 2)) {
            if (Calendar.getInstance().getTimeInMillis() > BandDataManager.m10564f()) {
                this.f8736s0.mo22582a(false);
                BandDataManager.m10550a(this.f8736s0);
            }
            this.f8732o0 = this.f8736s0.mo22593g();
        }
        this.f8733p0.setChecked(this.f8732o0);
        this.f8734q0.setChecked(this.f8732o0);
        this.f8733p0.setEnabled(false);
        this.f8734q0.setEnabled(false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R$id.recyclerView);
        this.f8730m0 = view.findViewById(R$id.layoutLocShare);
        this.f8731n0 = view.findViewById(R$id.llLocShare);
        mo22770b(R$id.layoutFriends, R$id.layoutLocShare, R$id.tvFriend, R$id.llLocShare, R$id.ivPos, R$id.ivClose, R$id.tvShareOptions);
        this.f8735r0 = new ListBuddyLocAdapter(getContext());
        this.f8735r0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4451j(this));
        recyclerView.setAdapter(this.f8735r0);
        recyclerView.addOnScrollListener(new C4432a(this));
        this.f8721d0.setVisibility(8);
        this.f8722e0.setVisibility(8);
        this.f8722e0.getLayoutParams().height = ScreenUtils.m1264b() / 3;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m14433c(String str) {
        Toast bVar = this.f8713A0;
        if (bVar == null) {
            this.f8713A0 = Toast.m10683a(mo22771c(), getLayoutInflater().inflate(R$layout.layout_toast_view, (ViewGroup) null), str, 0);
        } else {
            bVar.mo22977a(str);
            this.f8713A0.mo22975a(0);
        }
        this.f8713A0.mo22976a(17, 0, 0);
        this.f8713A0.mo22974a();
    }

    /* renamed from: b */
    public /* synthetic */ void mo24698b(String str, LatLng latLng, Bitmap bitmap) {
        if (this.f8717Z.containsKey(str)) {
            ((C2932c) Objects.requireNonNull(this.f8717Z.get(str))).mo18627a(latLng);
            return;
        }
        C2880c cVar = this.f8716Y;
        if (cVar != null) {
            C2932c a = cVar.mo18399a(new MarkerOptions().mo18545a(latLng).mo18546a(C2931b.m8343a(bitmap)));
            a.mo18628a(str);
            this.f8717Z.put(str, a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(e.e.d.j.a, e.e.d.j.a):int
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, com.heimavista.entity.data.PeriodTimeInfo):com.heimavista.entity.data.PeriodTimeInfo
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, java.lang.String):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.animation.ValueAnimator, android.animation.ValueAnimator):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void */
    /* renamed from: a */
    public /* synthetic */ void mo24690a(View view, BuddyListDb aVar, int i) {
        if (aVar.mo24284B()) {
            m14424a(aVar.mo24311x(), true);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24692a(C2880c cVar) {
        this.f8716Y = cVar;
        this.f8716Y.mo18407b().mo18415c(false);
        this.f8716Y.mo18403a(new C4455n(this));
        this.f8716Y.mo18405a(new C4453l(this));
        m14430b(false);
    }

    /* renamed from: b */
    public /* synthetic */ void mo24697b(String str, com.amap.api.maps.model.LatLng latLng, Bitmap bitmap) {
        if (this.f8719b0.containsKey(str)) {
            ((Marker) Objects.requireNonNull(this.f8719b0.get(str))).setPosition(latLng);
            return;
        }
        AMap aMap = this.f8718a0;
        if (aMap != null) {
            Marker addMarker = aMap.addMarker(new com.amap.api.maps.model.MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
            addMarker.setObject(str);
            this.f8719b0.put(str, addMarker);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24693a(LatLng latLng) {
        this.f8738u0 = "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(e.e.d.j.a, e.e.d.j.a):int
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, com.heimavista.entity.data.PeriodTimeInfo):com.heimavista.entity.data.PeriodTimeInfo
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, java.lang.String):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment, boolean):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(boolean, int):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.animation.ValueAnimator, android.animation.ValueAnimator):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseFragment.a(android.view.View, android.os.Bundle):void
      com.heimavista.fiedorasport.base.BaseFragment.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.ui.fragment.friend.BuddyLocFragment.a(java.lang.String, boolean):void */
    /* renamed from: a */
    public /* synthetic */ boolean mo24696a(C2932c cVar) {
        Object b = cVar.mo18629b();
        if (!(b instanceof String)) {
            return true;
        }
        if (!MemberControl.m17125s().mo27187l().equals(b)) {
            m14424a((String) b, false);
        } else {
            this.f8738u0 = "";
            this.f8737t0 += 0.1f;
            cVar.mo18626a(this.f8737t0);
        }
        C2880c cVar2 = this.f8716Y;
        if (cVar2 == null) {
            return true;
        }
        cVar2.mo18402a(C2879b.m8107a(cVar.mo18625a(), 18.0f));
        return true;
    }

    /* renamed from: b */
    private void m14428b(int i) {
        this.f8730m0.setEnabled(false);
        this.f8731n0.setEnabled(false);
        this.f8743z0 = new HvApiSetShareStat().setShareLoc(!this.f8732o0, new C4438g(i));
    }

    /* renamed from: b */
    private void m14429b(String str) {
        LogUtils.m1139c("BuddyLocF op=getBuddyLoc " + str);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24691a(AMap aMap) {
        this.f8718a0 = aMap;
        this.f8718a0.getUiSettings().setZoomControlsEnabled(false);
        this.f8718a0.setOnMapClickListener(new C4452k(this));
        this.f8718a0.setOnMarkerClickListener(new C4462u(this));
        m14430b(false);
    }

    /* renamed from: a */
    private void m14424a(String str, boolean z) {
        AMap aMap;
        ArrayMap<String, Marker> arrayMap;
        LocDb h = LocDb.m13307h(str);
        if (FSManager.m10325t()) {
            ArrayMap<String, C2932c> arrayMap2 = this.f8717Z;
            if (!(arrayMap2 == null || arrayMap2.get(str) == null)) {
                this.f8737t0 += 0.1f;
                ((C2932c) Objects.requireNonNull(this.f8717Z.get(str))).mo18626a(this.f8737t0);
            }
        } else if (!(!FSManager.m10324s() || (arrayMap = this.f8719b0) == null || arrayMap.get(str) == null)) {
            this.f8737t0 += 0.1f;
            ((Marker) Objects.requireNonNull(this.f8719b0.get(str))).setZIndex(this.f8737t0);
        }
        if (h != null && h.mo24327t() > 0) {
            if (z) {
                if (FSManager.m10325t()) {
                    C2880c cVar = this.f8716Y;
                    if (cVar != null) {
                        cVar.mo18402a(C2879b.m8107a(new LatLng(h.mo24323p(), h.mo24324q()), 18.0f));
                    }
                } else if (FSManager.m10324s() && (aMap = this.f8718a0) != null) {
                    aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new com.amap.api.maps.model.LatLng(h.mo24323p(), h.mo24324q()), 18.0f));
                }
            }
            this.f8738u0 = str;
            this.f8722e0.setVisibility(0);
            SpanUtils a = SpanUtils.m865a(this.f8727j0);
            a.mo9735a(String.format(Locale.getDefault(), "%.0f", Float.valueOf(h.mo24326s())));
            a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
            a.mo9738c();
            a.mo9735a(getString(R$string.data_unit_speed));
            a.mo9736b();
            SpanUtils a2 = SpanUtils.m865a(this.f8728k0);
            a2.mo9735a(h.mo24232o() == 0 ? "--" : String.valueOf(h.mo24232o()));
            a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
            a2.mo9738c();
            a2.mo9735a("%");
            a2.mo9736b();
            GeocoderUtils.m13412a(h.mo24323p(), h.mo24324q(), new C4433b());
            this.f8726i0.setText(getString(R$string.update_time, TimeUtils.m998a(((long) h.mo24327t()) * 1000, "yyyy-MM-dd HH:mm")));
        }
        UserInfoDb.m13339a(mo22771c(), str, this.f8724g0, this.f8723f0);
        m14434c(BuddyShareDb.m13288i(str).mo24320q());
    }

    /* renamed from: a */
    public void mo24688a(int i) {
        m14429b("requestBuddyLoc " + getLifecycle().getCurrentState());
        if (BuddyListDb.m13247H().isEmpty() || !getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            mo24680i();
            return;
        }
        this.f8741x0 = new C4434c();
        ThreadUtils.m953a(this.f8741x0, (long) i, (long) this.f8742y0, TimeUnit.SECONDS);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24687a(double d, double d2, String str, Bitmap bitmap) {
        if (this.f8714W) {
            C2932c a = this.f8715X.mo24725a(new MarkerOptions().mo18545a(new LatLng(d, d2)).mo18546a(C2931b.m8343a(bitmap)));
            if (a != null) {
                a.mo18628a(str);
                this.f8717Z.put(str, a);
                return;
            }
            return;
        }
        Marker a2 = this.f8715X.mo24724a(new com.amap.api.maps.model.MarkerOptions().position(new com.amap.api.maps.model.LatLng(d, d2)).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
        if (a2 != null) {
            a2.setObject(str);
            this.f8719b0.put(str, a2);
        }
    }

    /* renamed from: a */
    private void m14425a(List<BuddyListDb> list) {
        if (this.f8735r0 != null) {
            Collections.sort(list, C4457p.f8785P);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (BuddyListDb aVar : list) {
                LocDb h = LocDb.m13307h(aVar.mo24311x());
                if (h == null) {
                    arrayList2.add(aVar);
                } else if (!aVar.mo24284B() || h.mo24327t() == 0) {
                    arrayList3.add(aVar);
                } else {
                    arrayList.add(aVar);
                }
            }
            list.clear();
            list.addAll(arrayList);
            list.addAll(arrayList3);
            list.addAll(arrayList2);
            this.f8735r0.mo22792b((List) list);
        }
    }

    /* renamed from: a */
    static /* synthetic */ int m14419a(BuddyListDb aVar, BuddyListDb aVar2) {
        LocDb h = LocDb.m13307h(aVar.mo24311x());
        LocDb h2 = LocDb.m13307h(aVar2.mo24311x());
        if (h == null || h2 == null) {
            return 0;
        }
        return h2.mo24327t() - h.mo24327t();
    }

    /* renamed from: a */
    private void m14426a(boolean z, int i) {
        ValueAnimator valueAnimator;
        if (z) {
            valueAnimator = ValueAnimator.ofInt(0, i);
        } else {
            valueAnimator = ValueAnimator.ofInt(i, 0);
        }
        valueAnimator.addUpdateListener(new C4454m(this, valueAnimator));
        valueAnimator.setDuration(300L);
        valueAnimator.start();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24689a(ValueAnimator valueAnimator, ValueAnimator valueAnimator2) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        this.f8721d0.getLayoutParams().height = intValue;
        this.f8721d0.setVisibility(intValue > 0 ? 0 : 8);
        this.f8720c0.requestLayout();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        Location location;
        super.mo22754a(context, intent);
        if ("LocManager.action_location_callback".equals(intent.getAction()) && (location = (Location) intent.getParcelableExtra("location")) != null) {
            String l = MemberControl.m17125s().mo27187l();
            if (FSManager.m10325t()) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                C2880c cVar = this.f8716Y;
                if (cVar != null) {
                    cVar.mo18402a(C2879b.m8106a(latLng));
                    UserInfoDb.m13341a(getContext(), l, new C4459r(this, l, latLng));
                }
                ArrayMap<String, C2932c> arrayMap = this.f8717Z;
                if (!(arrayMap == null || arrayMap.get(l) == null)) {
                    this.f8737t0 += 0.1f;
                    ((C2932c) Objects.requireNonNull(this.f8717Z.get(l))).mo18626a(this.f8737t0);
                }
            } else if (FSManager.m10324s()) {
                com.amap.api.maps.model.LatLng latLng2 = new com.amap.api.maps.model.LatLng(location.getLatitude(), location.getLongitude());
                AMap aMap = this.f8718a0;
                if (aMap != null) {
                    aMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                    UserInfoDb.m13341a(getContext(), l, new C4456o(this, l, latLng2));
                }
                ArrayMap<String, Marker> arrayMap2 = this.f8719b0;
                if (!(arrayMap2 == null || arrayMap2.get(l) == null)) {
                    this.f8737t0 += 0.1f;
                    ((Marker) Objects.requireNonNull(this.f8719b0.get(l))).setZIndex(this.f8737t0);
                }
            }
            ListBuddyLocAdapter listBuddyLocAdapter = this.f8735r0;
            if (listBuddyLocAdapter != null) {
                listBuddyLocAdapter.mo24631a(location);
                this.f8735r0.notifyDataSetChanged();
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24695a(String str, LatLng latLng, Bitmap bitmap) {
        if (this.f8717Z.containsKey(str)) {
            ((C2932c) Objects.requireNonNull(this.f8717Z.get(str))).mo18627a(latLng);
            return;
        }
        C2932c a = this.f8716Y.mo18399a(new MarkerOptions().mo18545a(latLng).mo18546a(C2931b.m8343a(bitmap)));
        a.mo18628a(str);
        this.f8717Z.put(str, a);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24694a(String str, com.amap.api.maps.model.LatLng latLng, Bitmap bitmap) {
        if (this.f8719b0.containsKey(str)) {
            ((Marker) Objects.requireNonNull(this.f8719b0.get(str))).setPosition(latLng);
            return;
        }
        Marker addMarker = this.f8718a0.addMarker(new com.amap.api.maps.model.MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
        addMarker.setObject(str);
        this.f8719b0.put(str, addMarker);
    }
}
