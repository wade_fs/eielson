package com.heimavista.fiedorasport.p111ui.activity.user;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiPostExtraInfo;
import com.heimavista.api.account.HvApiSignout;
import com.heimavista.api.account.HvApiUpdProfile;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.PickerManager;
import com.heimavista.fiedorasport.p111ui.activity.login.LoginVerifyActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListMenuAdapter;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.p199j.ClipboardUtils;
import com.heimavista.fiedorasport.p199j.PhotoUtils;
import com.heimavista.p203ns.NsManage;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import com.heimavista.widget.dialog.EditDialog;
import com.sxr.sdk.ble.keepfit.aidl.UserProfile;
import java.io.File;
import java.util.Calendar;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.C5217x;
import p112d.p113a.p114a.p116b.DatePicker;
import p112d.p113a.p114a.p116b.NumberPicker;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity */
public abstract class UserCommonActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public int f8536b0 = FSManager.m10323r().mo22840j();
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public int f8537c0 = FSManager.m10323r().mo22844n();

    /* renamed from: d0 */
    private String f8538d0 = FSManager.m10323r().mo22837g();
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public String f8539e0 = MemberControl.m17125s().mo27178d();
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public UserProfile f8540f0 = BandDataManager.m10566h();

    /* renamed from: g0 */
    private short f8541g0 = 500;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public short f8542h0 = 501;

    /* renamed from: i0 */
    private short f8543i0 = 503;
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public PhotoUtils f8544j0;

    /* renamed from: k0 */
    private ListMenuAdapter f8545k0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$a */
    class C4382a implements C4632i<String> {
        C4382a() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            if (i == 0) {
                UserCommonActivity.this.m14222F();
            } else if (i == 1) {
                UserCommonActivity.this.m14221E();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$b */
    class C4383b implements C4632i<String> {
        C4383b() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            if (i == 0) {
                String unused = UserCommonActivity.this.f8539e0 = "M";
            } else if (i == 1) {
                String unused2 = UserCommonActivity.this.f8539e0 = "F";
            }
            if (UserCommonActivity.this.f8540f0 != null) {
                UserCommonActivity.this.f8540f0.mo26416b("M".equals(UserCommonActivity.this.f8539e0) ? 1 : 0);
                BandDataManager.m10554a(UserCommonActivity.this.f8540f0);
            }
            if (UserCommonActivity.this.m14220D()) {
                Constant.m10311a("修改性別");
                UserCommonActivity.this.m14219C().updMemGender(UserCommonActivity.this.f8539e0);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$c */
    class C4384c implements PermissionUtils.C0922e {
        C4384c() {
        }

        /* renamed from: a */
        public void mo9866a() {
            if (UserCommonActivity.this.f8544j0 == null) {
                PhotoUtils unused = UserCommonActivity.this.f8544j0 = new PhotoUtils();
            }
            UserCommonActivity.this.f8544j0.mo24362a(UserCommonActivity.this.mo22726c(), UserCommonActivity.this.f8542h0);
        }

        /* renamed from: b */
        public void mo9867b() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$d */
    class C4385d extends NumberPicker.C3811a {

        /* renamed from: a */
        final /* synthetic */ int f8549a;

        /* renamed from: b */
        final /* synthetic */ boolean f8550b;

        /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$d$a */
        class C4386a implements OnResultListener<Void> {
            C4386a() {
            }

            /* renamed from: a */
            public void mo22380a(Void voidR) {
                UserCommonActivity.this.mo22717b();
            }

            /* renamed from: a */
            public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
                UserCommonActivity.this.mo22717b();
                UserCommonActivity.this.mo22730c(str);
            }
        }

        C4385d(int i, boolean z) {
            this.f8549a = i;
            this.f8550b = z;
        }

        /* renamed from: b */
        public void mo23098b(int i, Number number) {
            if (UserCommonActivity.this.m14220D()) {
                int i2 = this.f8549a + i;
                if (this.f8550b) {
                    if (UserCommonActivity.this.f8540f0 != null) {
                        UserCommonActivity.this.f8540f0.mo26420d(i2);
                        BandDataManager.m10554a(UserCommonActivity.this.f8540f0);
                    }
                    int unused = UserCommonActivity.this.f8537c0 = i2;
                } else {
                    if (UserCommonActivity.this.f8540f0 != null) {
                        UserCommonActivity.this.f8540f0.mo26418c(i2);
                        BandDataManager.m10554a(UserCommonActivity.this.f8540f0);
                    }
                    int unused2 = UserCommonActivity.this.f8536b0 = i2;
                }
                Constant.m10311a(this.f8550b ? "修改體重" : "修改身高");
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(ViewHierarchyConstants.DIMENSION_HEIGHT_KEY, UserCommonActivity.this.f8536b0);
                    jSONObject.put("weight", UserCommonActivity.this.f8537c0);
                    new HvApiPostExtraInfo().postExtraInfo(jSONObject, new C4386a());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.user.UserCommonActivity$e */
    class C4387e implements OnResultListener<Void> {
        C4387e() {
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            UserCommonActivity.this.mo22717b();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            UserCommonActivity.this.mo22717b();
            UserCommonActivity.this.mo22730c(str);
        }
    }

    /* renamed from: B */
    private void mo24603B() {
        C4629g gVar = new C4629g(this);
        gVar.mo25456a(getString(R$string.male), getString(R$string.female));
        gVar.mo25453a(new C4383b());
        gVar.mo25418b().show();
    }

    /* access modifiers changed from: private */
    /* renamed from: C */
    public HvApiUpdProfile m14219C() {
        HvApiUpdProfile hvApiUpdProfile = new HvApiUpdProfile();
        hvApiUpdProfile.setListener(new C4387e());
        return hvApiUpdProfile;
    }

    /* access modifiers changed from: private */
    /* renamed from: D */
    public boolean m14220D() {
        if (!mo22741l()) {
            return false;
        }
        mo22712a(true);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public void m14221E() {
        if (this.f8544j0 == null) {
            this.f8544j0 = new PhotoUtils();
        }
        this.f8544j0.mo24366b(this, this.f8541g0);
    }

    /* access modifiers changed from: private */
    /* renamed from: F */
    public void m14222F() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.CAMERA");
        b.mo9858a(new C4384c());
        b.mo9859a();
    }

    /* renamed from: A */
    public void mo24585A() {
        C4629g gVar = new C4629g(this);
        gVar.mo25456a(getString(R$string.take_photo), getString(R$string.album));
        gVar.mo25453a(new C4382a());
        gVar.mo25418b().show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != -1) {
            return;
        }
        if (i == this.f8541g0 && intent != null) {
            m14226a(intent.getData());
        } else if (i == this.f8542h0) {
            m14226a(this.f8544j0.mo24367c(this));
        } else if (i == this.f8543i0 && m14220D()) {
            String a = this.f8544j0.mo24359a();
            if (!TextUtils.isEmpty(a)) {
                if (mo23015w() != null) {
                    AvatarUtils.m13393a(mo23015w(), a, R$mipmap.login_user, false);
                }
                if (BitmapFactory.decodeFile(a) == null) {
                    C5217x.task().postDelayed(new C4393f(this, a), 1000);
                    return;
                }
                String b = this.f8544j0.mo24365b(a, mo22726c());
                if (!TextUtils.isEmpty(b)) {
                    m14219C().updPhoto(new File(b));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public abstract List<MenuItem> mo23013u();

    /* renamed from: v */
    public int mo23014v() {
        return this.f8536b0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: w */
    public ImageView mo23015w() {
        return null;
    }

    /* renamed from: x */
    public String mo24596x() {
        return this.f8538d0;
    }

    /* renamed from: y */
    public String mo24597y() {
        return this.f8539e0;
    }

    /* renamed from: z */
    public int mo24598z() {
        return this.f8537c0;
    }

    /* renamed from: d */
    public /* synthetic */ void mo24591d(String str) {
        String b = this.f8544j0.mo24365b(str, mo22726c());
        if (!TextUtils.isEmpty(b)) {
            m14219C().updPhoto(new File(b));
        }
    }

    /* renamed from: e */
    public void mo24592e(int i) {
        this.f8536b0 = i;
    }

    /* renamed from: f */
    public void mo24594f(int i) {
        this.f8537c0 = i;
    }

    /* renamed from: b */
    private void m14230b(MenuItem eVar) {
        EditDialog cVar = new EditDialog(this);
        cVar.mo25469c(eVar.mo22652d());
        EditDialog cVar2 = cVar;
        cVar2.mo25449d(eVar.mo22646b());
        cVar2.mo25450e(getString(R$string.input) + eVar.mo22652d());
        cVar2.mo25413a(true);
        EditDialog cVar3 = cVar2;
        cVar3.mo25427g((ScreenUtils.m1265c() * 3) / 5);
        EditDialog cVar4 = cVar3;
        cVar4.mo25448a(new C4390c(this, eVar));
        cVar4.mo25411a(new C4394g(this));
        cVar4.mo25418b().show();
    }

    /* renamed from: e */
    public void mo24593e(String str) {
        this.f8538d0 = str;
    }

    /* renamed from: f */
    public void mo24595f(String str) {
        this.f8539e0 = str;
    }

    /* renamed from: e */
    public void mo22733e() {
        super.mo22733e();
        mo22717b();
        this.f8545k0.mo22792b((List) mo23013u());
    }

    /* renamed from: a */
    public void mo24586a(RecyclerView recyclerView) {
        this.f8545k0 = new ListMenuAdapter(super);
        this.f8545k0.mo22792b((List) mo23013u());
        this.f8545k0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4392e(this, recyclerView));
        recyclerView.setAdapter(this.f8545k0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* renamed from: a */
    public /* synthetic */ void mo24587a(RecyclerView recyclerView, View view, MenuItem eVar, int i) {
        if (getString(R$string.member_avatar).equals(eVar.mo22652d())) {
            mo24585A();
        } else if (getString(R$string.member_nickname).equals(eVar.mo22652d())) {
            m14230b(eVar);
        } else if ("ID".equals(eVar.mo22652d())) {
            ClipboardUtils.m13411a(eVar.mo22646b());
            SnackbarUtils a = SnackbarUtils.m927a(recyclerView);
            a.mo9783a(getString(R$string.userid_copied));
            a.mo9782a(-1);
            a.mo9788c();
        } else if (getString(R$string.member_name).equals(eVar.mo22652d())) {
            m14230b(eVar);
        } else if (getString(R$string.member_phone).equals(eVar.mo22652d())) {
            if (TextUtils.isEmpty(eVar.mo22646b())) {
                Intent intent = new Intent(this, LoginVerifyActivity.class);
                intent.putExtra("fromUser", true);
                startActivity(intent);
            }
        } else if (getString(R$string.member_address).equals(eVar.mo22652d())) {
            Constant.m10311a("點擊我的地址");
            startActivity(NsAddressActivity.m14207a(this, NsManage.m15135d().mo25154a()));
        } else if (getString(R$string.member_gender).equals(eVar.mo22652d())) {
            mo24603B();
        } else if (getString(R$string.member_birth).equals(eVar.mo22652d())) {
            m14227a(eVar);
        } else if (getString(R$string.member_height).equals(eVar.mo22652d())) {
            m14232b(false);
        } else if (getString(R$string.member_weight).equals(eVar.mo22652d())) {
            m14232b(true);
        } else if (getString(R$string.bind_social).equals(eVar.mo22652d())) {
            mo22702a(BindSocialActivity.class);
        } else if (getString(R$string.exit).equals(eVar.mo22652d())) {
            mo22712a(false);
            Constant.m10311a("退出登錄");
            new HvApiSignout().request();
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24590b(BaseDialog baseDialog) {
        hideSoftKeyboard(this.f8545k0.mo22795d());
    }

    /* renamed from: b */
    private void m14232b(boolean z) {
        String str;
        int i;
        int i2;
        if (z) {
            i2 = 30;
            i = 150;
            str = "Kg";
        } else {
            str = "cm";
            i2 = 100;
            i = 260;
        }
        NumberPicker a = PickerManager.m10513a(this, str, i2, i, 1);
        if (z) {
            int i3 = this.f8537c0;
            if (i3 == 0) {
                i3 = 60;
            }
            a.mo23095k(i3);
        } else {
            int i4 = this.f8536b0;
            if (i4 == 0) {
                i4 = 170;
            }
            a.mo23095k(i4);
        }
        a.mo23094a((NumberPicker.C3811a) new C4385d(i2, z));
        a.mo23129f();
    }

    /* renamed from: b */
    public void mo22722b(String str) {
        mo22717b();
        super.mo22722b(str);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24588a(MenuItem eVar, String str) {
        if (!TextUtils.isEmpty(str) && m14220D()) {
            if (getString(R$string.member_nickname).equals(eVar.mo22652d())) {
                Constant.m10311a("修改暱稱");
                m14219C().updMemName(str);
            } else if (getString(R$string.member_name).equals(eVar.mo22652d())) {
                Constant.m10311a("修改姓名");
                m14219C().updMemRealName(str);
            }
        }
    }

    /* renamed from: a */
    private void m14227a(MenuItem eVar) {
        int i;
        DatePicker a = PickerManager.m10512a(this);
        int i2 = 1;
        a.mo23064g(1900, 1);
        a.mo23063f(Calendar.getInstance().get(1), 12);
        String b = eVar.mo22646b();
        if (TextUtils.isEmpty(b) || !b.contains("-")) {
            i = 1990;
        } else {
            i = Integer.parseInt(b.substring(0, b.indexOf("-")));
            i2 = Integer.parseInt(b.substring(b.indexOf("-") + 1));
        }
        a.mo23065h(i, i2);
        a.mo23061a(new C4391d(this));
        a.mo23129f();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24589a(String str, String str2) {
        if (m14220D()) {
            Constant.m10311a("修改出生年月");
            try {
                int parseInt = Calendar.getInstance().get(1) - Integer.parseInt(str);
                if (this.f8540f0 != null) {
                    this.f8540f0.mo26414a(parseInt);
                    BandDataManager.m10554a(this.f8540f0);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            HvApiUpdProfile hvApiUpdProfile = new HvApiUpdProfile();
            hvApiUpdProfile.updMemBirthYm(str + str2);
        }
    }

    /* renamed from: a */
    private void m14226a(Uri uri) {
        this.f8544j0.mo24363a(this, uri, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION, this.f8543i0);
    }
}
