package com.heimavista.fiedorasport.p109h;

/* renamed from: com.heimavista.fiedorasport.h.y */
/* compiled from: BleDataCallback */
public interface C3734y {

    /* renamed from: com.heimavista.fiedorasport.h.y$a */
    /* compiled from: BleDataCallback */
    public interface C3735a {
        /* renamed from: a */
        void mo22964a(C3736b bVar, boolean z);
    }

    /* renamed from: com.heimavista.fiedorasport.h.y$b */
    /* compiled from: BleDataCallback */
    public enum C3736b {
        antiLost,
        alarms,
        sedentary,
        goalStep,
        deviceInfo,
        autoHeart
    }

    /* renamed from: a */
    void mo22681a(int i);

    /* renamed from: a */
    void mo22682a(int i, int i2);

    /* renamed from: a */
    void mo22683a(int i, int i2, long j);

    /* renamed from: a */
    void mo22684a(int i, String str, int i2, int i3);

    /* renamed from: a */
    void mo22686a(int i, String str, String str2, String str3, int i2);

    /* renamed from: a */
    void mo22687a(long j, int i);

    /* renamed from: a */
    void mo22688a(long j, int i, int i2);

    /* renamed from: a */
    void mo22689a(long j, int i, int i2, int i3);

    /* renamed from: a */
    void mo22690a(long j, int i, int i2, int i3, int i4);

    /* renamed from: a */
    void mo22705a(String str);

    /* renamed from: a */
    void mo22706a(String str, String str2, int i);

    /* renamed from: b */
    void mo22718b(int i);

    /* renamed from: c */
    void mo22727c(int i);
}
