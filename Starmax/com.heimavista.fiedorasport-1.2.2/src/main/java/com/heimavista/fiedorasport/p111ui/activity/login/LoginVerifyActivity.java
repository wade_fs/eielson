package com.heimavista.fiedorasport.p111ui.activity.login;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.blankj.utilcode.util.RegexUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiBindMobile;
import com.heimavista.api.account.HvApiGetSmsCode;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.ScanActivity;
import com.heimavista.fiedorasport.p111ui.activity.user.FillUserInfoActivity;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.widget.RoundImageView;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginVerifyActivity */
public class LoginVerifyActivity extends BaseActivity {

    /* renamed from: b0 */
    private RoundImageView f8209b0;

    /* renamed from: c0 */
    private TextView f8210c0;

    /* renamed from: d0 */
    private EditText f8211d0;

    /* renamed from: e0 */
    private EditText f8212e0;

    /* renamed from: f0 */
    private TextView f8213f0;
    /* access modifiers changed from: private */

    /* renamed from: g0 */
    public CountDownTimer f8214g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public boolean f8215h0 = false;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginVerifyActivity$a */
    class C4287a implements OnResultListener<Void> {
        C4287a() {
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            Constant.m10311a("手機綁定成功");
            if (LoginVerifyActivity.this.f8215h0) {
                LoginVerifyActivity.this.setResult(-1);
            }
            LoginVerifyActivity.this.mo22717b();
            LoginVerifyActivity.this.onBackPressed();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            Constant.m10311a("手機綁定失敗");
            LoginVerifyActivity.this.mo22717b();
            LoginVerifyActivity.this.mo22730c(str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginVerifyActivity$b */
    class C4288b implements OnResultListener<Void> {
        C4288b() {
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            LoginVerifyActivity.this.mo22717b();
            LoginVerifyActivity loginVerifyActivity = LoginVerifyActivity.this;
            loginVerifyActivity.mo22730c(loginVerifyActivity.getString(R$string.send_code_success));
            LoginVerifyActivity.this.f8214g0.start();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            LoginVerifyActivity.this.mo22717b();
            LoginVerifyActivity.this.mo22730c(str);
        }
    }

    /* renamed from: u */
    private void m13753u() {
        if (mo22741l()) {
            if (RegexUtils.m1235a(this.f8211d0.getText()) || C4222l.m13458a(this.f8211d0.getText())) {
                mo22751t();
                if (this.f8214g0 == null) {
                    this.f8214g0 = C4222l.m13452a(this.f8213f0, 60, getString(R$string.send_code), getString(R$string.code_time_text));
                }
                this.f8213f0.setEnabled(false);
                new HvApiGetSmsCode().request(this.f8211d0.getText().toString(), new C4288b());
                return;
            }
            mo22730c(getString(R$string.err_mobile));
        }
    }

    /* renamed from: v */
    private void m13754v() {
        if (mo22741l()) {
            if (!RegexUtils.m1235a(this.f8211d0.getText()) && !C4222l.m13458a(this.f8211d0.getText())) {
                mo22730c(getString(R$string.err_mobile));
            } else if (TextUtils.isEmpty(this.f8212e0.getText())) {
                mo22730c(getString(R$string.err_empty_code));
            } else {
                String obj = this.f8211d0.getText().toString();
                String obj2 = this.f8212e0.getText().toString();
                mo22751t();
                new HvApiBindMobile().request(obj, obj2, new C4287a());
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_login_verify;
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    public void onBackPressed() {
        if (this.f8215h0) {
            super.onBackPressed();
        } else if (JYSDKManager.m10388i().mo22897e()) {
            mo22702a(HomeActivity.class);
        } else if (TextUtils.isEmpty(FSManager.m10323r().mo22836f()) && FSManager.m10323r().mo22844n() == 0 && FSManager.m10323r().mo22840j() == 0) {
            mo22721b(FillUserInfoActivity.class);
        } else {
            mo22721b(ScanActivity.class);
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.btnLogin) {
            m13754v();
        } else if (id == R$id.tvSkip) {
            onBackPressed();
        } else if (id == R$id.tvCode) {
            m13753u();
        }
    }

    public void onDestroy() {
        CountDownTimer countDownTimer = this.f8214g0;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [com.heimavista.fiedorasport.widget.RoundImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        AvatarUtils.m13393a((ImageView) this.f8209b0, MemberControl.m17125s().mo27183h(), R$mipmap.login_user, true);
        this.f8210c0.setText(MemberControl.m17125s().mo27182g());
        this.f8215h0 = getIntent().getBooleanExtra("fromUser", false);
        if (this.f8215h0) {
            findViewById(R$id.tvSkip).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8209b0 = (RoundImageView) findViewById(R$id.ivAvatar);
        this.f8210c0 = (TextView) findViewById(R$id.tvName);
        this.f8211d0 = (EditText) findViewById(R$id.etAccount);
        this.f8212e0 = (EditText) findViewById(R$id.etCode);
        this.f8213f0 = (TextView) findViewById(R$id.tvCode);
        mo22723b(R$id.btnLogin);
        mo22723b(R$id.tvSkip);
        mo22723b(R$id.tvCode);
    }
}
