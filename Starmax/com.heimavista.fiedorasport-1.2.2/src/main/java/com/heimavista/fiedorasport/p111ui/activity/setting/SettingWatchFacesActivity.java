package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.LogUtils;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p111ui.adapter.ListWatchFaceAdapter;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingWatchFacesActivity */
public class SettingWatchFacesActivity extends BaseActivity implements BaseRecyclerViewAdapter.C3685c<ListWatchFaceAdapter.C4410a> {

    /* renamed from: b0 */
    private ListWatchFaceAdapter f8286b0;

    /* renamed from: c0 */
    private C1178n f8287c0;

    /* renamed from: d0 */
    private List<ListWatchFaceAdapter.C4410a> f8288d0;

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8286b0 = new ListWatchFaceAdapter(this);
        this.f8286b0.mo22784a((BaseRecyclerViewAdapter.C3685c) this);
        this.f8286b0.mo24655a((View.OnClickListener) new C4313g0(this));
        ((RecyclerView) findViewById(R$id.recyclerView)).setAdapter(this.f8286b0);
    }

    /* renamed from: e */
    public /* synthetic */ void mo24503e(int i) {
        LogUtils.m1139c("madyCrp watchFaceType=" + i);
        runOnUiThread(new C4311f0(this, i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.setting_watch_face);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24499a(View view) {
        if (this.f8287c0 != null) {
            Intent intent = new Intent(mo22726c(), EditWatchFaceActivity.class);
            intent.putExtra("timePosition", this.f8287c0.mo10868d());
            intent.putExtra("timeTopContent", this.f8287c0.mo10870e());
            intent.putExtra("timeBottomContent", this.f8287c0.mo10866c());
            intent.putExtra("textColor", this.f8287c0.mo10864b());
            intent.putExtra("picMD5", this.f8287c0.mo10861a());
            mo22696a(intent, new C4315h0(this));
        }
    }

    /* renamed from: f */
    public /* synthetic */ void mo24504f(int i) {
        if (i == 1) {
            this.f8288d0.get(0).mo24659a(true);
            this.f8286b0.notifyItemChanged(0);
        } else if (i == 2) {
            this.f8288d0.get(1).mo24659a(true);
            this.f8286b0.notifyItemChanged(1);
        } else if (i == 3) {
            this.f8288d0.get(2).mo24659a(true);
            this.f8286b0.notifyItemChanged(2);
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo24502b(C1178n nVar) {
        int a = CRPBleManager.m10584a(nVar.mo10864b());
        nVar.mo10862a(Color.rgb(Color.red(a), Color.green(a), Color.blue(a)));
        this.f8287c0 = nVar;
        this.f8288d0.get(0).mo24658a(nVar);
        this.f8286b0.notifyItemChanged(0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24498a(int i, Intent intent) {
        if (intent != null) {
            this.f8287c0 = new C1178n(intent.getIntExtra("timePosition", 0), intent.getIntExtra("timeTopContent", 0), intent.getIntExtra("timeBottomContent", 1), intent.getIntExtra("textColor", -1), intent.getStringExtra("picMD5"));
            List<ListWatchFaceAdapter.C4410a> list = this.f8288d0;
            if (list != null && !list.isEmpty()) {
                int size = this.f8288d0.size();
                int i2 = 0;
                while (i2 < size) {
                    this.f8288d0.get(i2).mo24659a(i2 == 0);
                    if (i2 == 0) {
                        this.f8288d0.get(i2).mo24658a(this.f8287c0);
                    }
                    i2++;
                }
                this.f8286b0.mo22792b((List) this.f8288d0);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        List<ListWatchFaceAdapter.C4410a> list = this.f8288d0;
        if (list == null) {
            this.f8288d0 = new ArrayList();
        } else {
            list.clear();
        }
        int i = CRPBleManager.m10638i();
        this.f8287c0 = CRPBleManager.m10636h();
        ListWatchFaceAdapter.C4410a aVar = new ListWatchFaceAdapter.C4410a(i == 1, (byte) 1);
        aVar.mo24658a(this.f8287c0);
        this.f8288d0.add(aVar);
        this.f8288d0.add(new ListWatchFaceAdapter.C4410a(i == 2, (byte) 2));
        this.f8288d0.add(new ListWatchFaceAdapter.C4410a(i == 3, (byte) 3));
        this.f8286b0.mo22792b((List) this.f8288d0);
        LogUtils.m1139c("madyCrp queryDisplayWatchFace");
        CRPBleManager.m10596a(new C4307d0(this));
        LogUtils.m1139c("madyCrp queryWatchFaceLayout");
        CRPBleManager.m10598a(new C4309e0(this));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24501a(C1178n nVar) {
        LogUtils.m1139c("madyCrp info=" + nVar);
        runOnUiThread(new C4305c0(this, nVar));
    }

    /* renamed from: a */
    public void mo22804a(View view, ListWatchFaceAdapter.C4410a aVar, int i) {
        List<ListWatchFaceAdapter.C4410a> c = this.f8286b0.mo22794c();
        if (c != null) {
            for (ListWatchFaceAdapter.C4410a aVar2 : c) {
                aVar2.mo24659a(aVar2.mo24660b() == aVar.mo24660b());
            }
            this.f8286b0.mo22792b(c);
        }
        CRPBleManager.m10587a(aVar.mo24660b());
    }
}
