package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.View;
import com.heimavista.entity.data.ActivitiesItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.d */
/* compiled from: lambda */
public final /* synthetic */ class C4240d implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ ListActivitiesActivity f8115P;

    public /* synthetic */ C4240d(ListActivitiesActivity listActivitiesActivity) {
        this.f8115P = listActivitiesActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8115P.mo24382a(view, (ActivitiesItem) obj, i);
    }
}
