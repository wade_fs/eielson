package com.heimavista.fiedorasport.widget.header;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.heimavista.fiedorasport.widget.header.a */
public class MaterialProgressDrawable extends Drawable implements Animatable {

    /* renamed from: Y */
    private static final Interpolator f9016Y = new LinearInterpolator();

    /* renamed from: Z */
    static final Interpolator f9017Z = new FastOutSlowInInterpolator();

    /* renamed from: a0 */
    private static final int[] f9018a0 = {-16777216};

    /* renamed from: P */
    private final List<Animation> f9019P = new ArrayList();

    /* renamed from: Q */
    private final C4500c f9020Q = new C4500c(this);

    /* renamed from: R */
    private float f9021R;

    /* renamed from: S */
    private View f9022S;

    /* renamed from: T */
    private Animation f9023T;

    /* renamed from: U */
    float f9024U;

    /* renamed from: V */
    private float f9025V;

    /* renamed from: W */
    private float f9026W;

    /* renamed from: X */
    boolean f9027X;

    /* renamed from: com.heimavista.fiedorasport.widget.header.a$a */
    /* compiled from: MaterialProgressDrawable */
    class C4498a extends Animation {

        /* renamed from: P */
        final /* synthetic */ C4500c f9028P;

        C4498a(C4500c cVar) {
            this.f9028P = cVar;
        }

        public void applyTransformation(float f, Transformation transformation) {
            MaterialProgressDrawable aVar = MaterialProgressDrawable.this;
            if (aVar.f9027X) {
                aVar.mo24859a(f, this.f9028P);
                return;
            }
            float a = aVar.mo24856a(this.f9028P);
            C4500c cVar = this.f9028P;
            float f2 = cVar.f9043l;
            float f3 = cVar.f9042k;
            float f4 = cVar.f9044m;
            MaterialProgressDrawable.this.mo24864b(f, cVar);
            if (f <= 0.5f) {
                this.f9028P.f9035d = f3 + ((0.8f - a) * MaterialProgressDrawable.f9017Z.getInterpolation(f / 0.5f));
            }
            if (f > 0.5f) {
                this.f9028P.f9036e = f2 + ((0.8f - a) * MaterialProgressDrawable.f9017Z.getInterpolation((f - 0.5f) / 0.5f));
            }
            MaterialProgressDrawable.this.mo24863b(f4 + (0.25f * f));
            MaterialProgressDrawable aVar2 = MaterialProgressDrawable.this;
            aVar2.mo24866c((f * 216.0f) + ((aVar2.f9024U / 5.0f) * 1080.0f));
        }
    }

    /* renamed from: com.heimavista.fiedorasport.widget.header.a$b */
    /* compiled from: MaterialProgressDrawable */
    class C4499b implements Animation.AnimationListener {

        /* renamed from: a */
        final /* synthetic */ C4500c f9030a;

        C4499b(C4500c cVar) {
            this.f9030a = cVar;
        }

        public void onAnimationEnd(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
            this.f9030a.mo24888e();
            this.f9030a.mo24886c();
            C4500c cVar = this.f9030a;
            cVar.f9035d = cVar.f9036e;
            MaterialProgressDrawable aVar = MaterialProgressDrawable.this;
            if (aVar.f9027X) {
                aVar.f9027X = false;
                animation.setDuration(1332);
                MaterialProgressDrawable.this.mo24861a(false);
                return;
            }
            aVar.f9024U = (aVar.f9024U + 1.0f) % 5.0f;
        }

        public void onAnimationStart(Animation animation) {
            MaterialProgressDrawable.this.f9024U = 0.0f;
        }
    }

    public MaterialProgressDrawable(View view) {
        this.f9022S = view;
        mo24862a(f9018a0);
        mo24865b(1);
        m14716a();
    }

    /* renamed from: a */
    private int m14715a(float f, int i, int i2) {
        int i3 = (i >> 24) & 255;
        int i4 = (i >> 16) & 255;
        int i5 = (i >> 8) & 255;
        int i6 = i & 255;
        return ((i3 + ((int) (((float) (((i2 >> 24) & 255) - i3)) * f))) << 24) | ((i4 + ((int) (((float) (((i2 >> 16) & 255) - i4)) * f))) << 16) | ((i5 + ((int) (((float) (((i2 >> 8) & 255) - i5)) * f))) << 8) | (i6 + ((int) (f * ((float) ((i2 & 255) - i6)))));
    }

    /* renamed from: a */
    private void m14717a(int i, int i2, float f, float f2, float f3, float f4) {
        float f5 = Resources.getSystem().getDisplayMetrics().density;
        this.f9025V = ((float) i) * f5;
        this.f9026W = ((float) i2) * f5;
        this.f9020Q.mo24882a(0);
        float f6 = f2 * f5;
        this.f9020Q.f9033b.setStrokeWidth(f6);
        C4500c cVar = this.f9020Q;
        cVar.f9038g = f6;
        cVar.f9048q = (double) (f * f5);
        cVar.f9049r = (int) (f3 * f5);
        cVar.f9050s = (int) (f4 * f5);
        cVar.mo24883a((int) this.f9025V, (int) this.f9026W);
        invalidateSelf();
    }

    /* renamed from: b */
    public void mo24865b(int i) {
        if (i == 0) {
            m14717a(56, 56, 12.5f, 3.0f, 12.0f, 6.0f);
        } else {
            m14717a(40, 40, 8.75f, 2.5f, 10.0f, 5.0f);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo24866c(float f) {
        this.f9021R = f;
        invalidateSelf();
    }

    public void draw(@NonNull Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.rotate(this.f9021R, bounds.exactCenterX(), bounds.exactCenterY());
        this.f9020Q.mo24884a(canvas, bounds);
        canvas.restoreToCount(save);
    }

    public int getAlpha() {
        return this.f9020Q.f9051t;
    }

    public int getIntrinsicHeight() {
        return (int) this.f9026W;
    }

    public int getIntrinsicWidth() {
        return (int) this.f9025V;
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        List<Animation> list = this.f9019P;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Animation animation = list.get(i);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void setAlpha(int i) {
        this.f9020Q.f9051t = i;
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f9020Q.f9033b.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public void start() {
        this.f9023T.reset();
        this.f9020Q.mo24888e();
        C4500c cVar = this.f9020Q;
        if (cVar.f9036e != cVar.f9035d) {
            this.f9027X = true;
            this.f9023T.setDuration(666);
            this.f9022S.startAnimation(this.f9023T);
            return;
        }
        cVar.mo24882a(0);
        this.f9020Q.mo24887d();
        this.f9023T.setDuration(1332);
        this.f9022S.startAnimation(this.f9023T);
    }

    public void stop() {
        this.f9022S.clearAnimation();
        this.f9020Q.mo24882a(0);
        this.f9020Q.mo24887d();
        mo24861a(false);
        mo24866c(0.0f);
    }

    /* renamed from: b */
    public void mo24863b(float f) {
        this.f9020Q.f9037f = f;
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo24864b(float f, C4500c cVar) {
        if (f > 0.75f) {
            cVar.f9054w = m14715a((f - 0.75f) / 0.25f, cVar.mo24885b(), cVar.mo24881a());
        }
    }

    /* renamed from: com.heimavista.fiedorasport.widget.header.a$c */
    /* compiled from: MaterialProgressDrawable */
    private class C4500c {

        /* renamed from: a */
        final RectF f9032a = new RectF();

        /* renamed from: b */
        final Paint f9033b = new Paint();

        /* renamed from: c */
        final Paint f9034c = new Paint();

        /* renamed from: d */
        float f9035d = 0.0f;

        /* renamed from: e */
        float f9036e = 0.0f;

        /* renamed from: f */
        float f9037f = 0.0f;

        /* renamed from: g */
        float f9038g = 5.0f;

        /* renamed from: h */
        float f9039h = 2.5f;

        /* renamed from: i */
        int[] f9040i;

        /* renamed from: j */
        int f9041j;

        /* renamed from: k */
        float f9042k;

        /* renamed from: l */
        float f9043l;

        /* renamed from: m */
        float f9044m;

        /* renamed from: n */
        boolean f9045n;

        /* renamed from: o */
        Path f9046o;

        /* renamed from: p */
        float f9047p;

        /* renamed from: q */
        double f9048q;

        /* renamed from: r */
        int f9049r;

        /* renamed from: s */
        int f9050s;

        /* renamed from: t */
        int f9051t;

        /* renamed from: u */
        final Paint f9052u = new Paint(1);

        /* renamed from: v */
        int f9053v;

        /* renamed from: w */
        int f9054w;

        C4500c(MaterialProgressDrawable aVar) {
            this.f9033b.setStrokeCap(Paint.Cap.SQUARE);
            this.f9033b.setAntiAlias(true);
            this.f9033b.setStyle(Paint.Style.STROKE);
            this.f9034c.setStyle(Paint.Style.FILL);
            this.f9034c.setAntiAlias(true);
        }

        /* renamed from: f */
        private int m14730f() {
            return (this.f9041j + 1) % this.f9040i.length;
        }

        /* renamed from: a */
        public void mo24884a(Canvas canvas, Rect rect) {
            RectF rectF = this.f9032a;
            rectF.set(rect);
            float f = this.f9039h;
            rectF.inset(f, f);
            float f2 = this.f9035d;
            float f3 = this.f9037f;
            float f4 = (f2 + f3) * 360.0f;
            float f5 = ((this.f9036e + f3) * 360.0f) - f4;
            if (f5 != 0.0f) {
                this.f9033b.setColor(this.f9054w);
                canvas.drawArc(rectF, f4, f5, false, this.f9033b);
            }
            m14729a(canvas, f4, f5, rect);
            if (this.f9051t < 255) {
                this.f9052u.setColor(this.f9053v);
                this.f9052u.setAlpha(255 - this.f9051t);
                canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), ((float) rect.width()) / 2.0f, this.f9052u);
            }
        }

        /* renamed from: b */
        public int mo24885b() {
            return this.f9040i[this.f9041j];
        }

        /* renamed from: c */
        public void mo24886c() {
            mo24882a(m14730f());
        }

        /* renamed from: d */
        public void mo24887d() {
            this.f9042k = 0.0f;
            this.f9043l = 0.0f;
            this.f9044m = 0.0f;
            this.f9035d = 0.0f;
            this.f9036e = 0.0f;
            this.f9037f = 0.0f;
        }

        /* renamed from: e */
        public void mo24888e() {
            this.f9042k = this.f9035d;
            this.f9043l = this.f9036e;
            this.f9044m = this.f9037f;
        }

        /* renamed from: a */
        private void m14729a(Canvas canvas, float f, float f2, Rect rect) {
            if (this.f9045n) {
                Path path = this.f9046o;
                if (path == null) {
                    this.f9046o = new Path();
                    this.f9046o.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                float f3 = ((float) (((int) this.f9039h) / 2)) * this.f9047p;
                float sin = (float) ((this.f9048q * Math.sin(0.0d)) + ((double) rect.exactCenterY()));
                this.f9046o.moveTo(0.0f, 0.0f);
                this.f9046o.lineTo(((float) this.f9049r) * this.f9047p, 0.0f);
                Path path2 = this.f9046o;
                float f4 = this.f9047p;
                path2.lineTo((((float) this.f9049r) * f4) / 2.0f, ((float) this.f9050s) * f4);
                this.f9046o.offset(((float) ((this.f9048q * Math.cos(0.0d)) + ((double) rect.exactCenterX()))) - f3, sin);
                this.f9046o.close();
                this.f9034c.setColor(this.f9054w);
                canvas.rotate((f + f2) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
                canvas.drawPath(this.f9046o, this.f9034c);
            }
        }

        /* renamed from: a */
        public void mo24882a(int i) {
            this.f9041j = i;
            this.f9054w = this.f9040i[this.f9041j];
        }

        /* renamed from: a */
        public int mo24881a() {
            return this.f9040i[m14730f()];
        }

        /* renamed from: a */
        public void mo24883a(int i, int i2) {
            double d;
            float min = (float) Math.min(i, i2);
            double d2 = this.f9048q;
            if (d2 <= 0.0d || min < 0.0f) {
                d = Math.ceil((double) (this.f9038g / 2.0f));
            } else {
                d = ((double) (min / 2.0f)) - d2;
            }
            this.f9039h = (float) d;
        }
    }

    /* renamed from: a */
    public void mo24861a(boolean z) {
        C4500c cVar = this.f9020Q;
        if (cVar.f9045n != z) {
            cVar.f9045n = z;
            invalidateSelf();
        }
    }

    /* renamed from: a */
    public void mo24857a(float f) {
        C4500c cVar = this.f9020Q;
        if (cVar.f9047p != f) {
            cVar.f9047p = f;
            invalidateSelf();
        }
    }

    /* renamed from: a */
    public void mo24858a(float f, float f2) {
        C4500c cVar = this.f9020Q;
        cVar.f9035d = f;
        cVar.f9036e = f2;
        invalidateSelf();
    }

    /* renamed from: a */
    public void mo24860a(@ColorInt int i) {
        this.f9020Q.f9053v = i;
    }

    /* renamed from: a */
    public void mo24862a(@ColorInt int... iArr) {
        C4500c cVar = this.f9020Q;
        cVar.f9040i = iArr;
        cVar.mo24882a(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public float mo24856a(C4500c cVar) {
        return (float) Math.toRadians(((double) cVar.f9038g) / (cVar.f9048q * 6.283185307179586d));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo24859a(float f, C4500c cVar) {
        mo24864b(f, cVar);
        float a = mo24856a(cVar);
        float f2 = cVar.f9042k;
        float f3 = cVar.f9043l;
        mo24858a(f2 + (((f3 - a) - f2) * f), f3);
        float f4 = cVar.f9044m;
        mo24863b(f4 + ((((float) (Math.floor((double) (cVar.f9044m / 0.8f)) + 1.0d)) - f4) * f));
    }

    /* renamed from: a */
    private void m14716a() {
        C4500c cVar = this.f9020Q;
        C4498a aVar = new C4498a(cVar);
        aVar.setRepeatCount(-1);
        aVar.setRepeatMode(1);
        aVar.setInterpolator(f9016Y);
        aVar.setAnimationListener(new C4499b(cVar));
        this.f9023T = aVar;
    }
}
