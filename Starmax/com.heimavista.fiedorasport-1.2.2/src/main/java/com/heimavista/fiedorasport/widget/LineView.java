package com.heimavista.fiedorasport.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.Nullable;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$string;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class LineView extends View {

    /* renamed from: P */
    private Context f8937P;

    /* renamed from: Q */
    private Paint f8938Q;

    /* renamed from: R */
    private Paint f8939R;

    /* renamed from: S */
    private int f8940S;

    /* renamed from: T */
    private int f8941T;

    /* renamed from: U */
    private int f8942U;

    /* renamed from: V */
    private int f8943V;

    /* renamed from: W */
    private long f8944W = 0;

    /* renamed from: a0 */
    private int f8945a0;

    /* renamed from: b0 */
    private List<SleepInfo> f8946b0 = new ArrayList();

    /* renamed from: c0 */
    private SleepInfo f8947c0;

    /* renamed from: d0 */
    private List<SleepInfo> f8948d0;

    /* renamed from: e0 */
    private SimpleDateFormat f8949e0;

    public LineView(Context context) {
        super(context);
        this.f8937P = context;
        m14670a();
    }

    /* renamed from: a */
    private void m14670a() {
        this.f8938Q = new Paint();
        this.f8938Q.setAntiAlias(true);
        this.f8938Q.setDither(true);
        this.f8938Q.setStyle(Paint.Style.FILL);
        this.f8939R = new Paint();
        this.f8939R.setAntiAlias(true);
        this.f8939R.setDither(true);
        this.f8939R.setStyle(Paint.Style.FILL);
        this.f8939R.setStrokeWidth(8.0f);
        this.f8939R.setTextSize((float) ConvertUtils.m1056b(10.0f));
        this.f8939R.setColor(-1);
        this.f8939R.setTextAlign(Paint.Align.LEFT);
        this.f8940S = getResources().getColor(R$color.sleep_deep);
        this.f8941T = getResources().getColor(R$color.sleep_light);
        this.f8942U = getResources().getColor(R$color.sleep_awake);
        this.f8943V = getResources().getColor(R$color.sleep_line_select);
        this.f8945a0 = ConvertUtils.m1055a(20.0f);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DrawAllocation"})
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        m14671a(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 0 || mode == Integer.MIN_VALUE) {
            i = ScreenUtils.m1265c();
        }
        if (mode2 == 0 || mode2 == Integer.MIN_VALUE) {
            i2 = View.MeasureSpec.makeMeasureSpec(ConvertUtils.m1055a(40.0f), 1073741824);
        }
        super.onMeasure(i, i2);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            getParent().requestDisallowInterceptTouchEvent(true);
            float y = motionEvent.getY();
            float x = motionEvent.getX();
            if (y >= ((float) this.f8945a0) && !this.f8946b0.isEmpty()) {
                float measuredWidth = ((float) this.f8944W) * (x / ((float) getMeasuredWidth()));
                long j = this.f8946b0.get(0).f6266e;
                Iterator<SleepInfo> it = this.f8948d0.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    SleepInfo next = it.next();
                    long j2 = next.f6266e;
                    if (measuredWidth >= ((float) (j2 - j))) {
                        long j3 = next.f6267f;
                        if (measuredWidth <= ((float) (j3 - j))) {
                            SleepInfo cVar = this.f8947c0;
                            if (cVar != null && cVar.f6266e == j2 && cVar.f6267f == j3) {
                                this.f8947c0 = null;
                            } else {
                                this.f8947c0 = next;
                            }
                        }
                    }
                }
            } else {
                this.f8947c0 = null;
            }
        } else if (action == 1) {
            getParent().requestDisallowInterceptTouchEvent(false);
            invalidate();
        }
        return true;
    }

    public void setAwakeColor(int i) {
        this.f8942U = i;
    }

    public void setDeepColor(int i) {
        this.f8940S = i;
    }

    public void setLightColor(int i) {
        this.f8941T = i;
    }

    public void setSelectedColor(int i) {
        this.f8943V = i;
    }

    public void setSleepDataList(List<SleepInfo> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        this.f8946b0 = list;
        this.f8947c0 = null;
        invalidate();
    }

    public LineView(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f8937P = context;
        m14670a();
    }

    public LineView(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f8937P = context;
        m14670a();
    }

    /* renamed from: a */
    private void m14671a(Canvas canvas) {
        SleepInfo cVar;
        SleepInfo cVar2;
        Canvas canvas2 = canvas;
        int i = 1;
        if (this.f8946b0.isEmpty()) {
            this.f8938Q.setStrokeWidth(0.0f);
            this.f8939R.setTextAlign(Paint.Align.CENTER);
            canvas2.drawText(this.f8937P.getString(R$string.no_sleep_data), (float) (getMeasuredWidth() >> 1), (float) ((getMeasuredHeight() + this.f8945a0) >> 1), this.f8939R);
            this.f8944W = 0;
            return;
        }
        if (this.f8948d0 == null) {
            this.f8948d0 = new ArrayList();
        }
        this.f8948d0.clear();
        this.f8948d0.addAll(this.f8946b0);
        this.f8938Q.setColor(-1);
        canvas.drawRect(0.0f, (float) this.f8945a0, (float) getMeasuredWidth(), (float) getMeasuredHeight(), this.f8938Q);
        int i2 = 0;
        long j = this.f8946b0.get(0).f6266e;
        List<SleepInfo> list = this.f8946b0;
        this.f8944W = list.get(list.size() - 1).f6267f - j;
        if (this.f8944W != 0) {
            float measuredWidth = (((float) getMeasuredWidth()) * 1.0f) / ((float) this.f8944W);
            float measuredHeight = (float) getMeasuredHeight();
            this.f8938Q.setStrokeWidth(measuredWidth);
            int size = this.f8946b0.size();
            while (i2 < size) {
                SleepInfo cVar3 = this.f8946b0.get(i2);
                int i3 = cVar3.f6265d;
                if (i3 == i) {
                    this.f8938Q.setColor(this.f8940S);
                } else if (i3 == 2) {
                    this.f8938Q.setColor(this.f8941T);
                } else if (i3 == 3) {
                    this.f8938Q.setColor(this.f8942U);
                }
                long j2 = cVar3.f6266e - j;
                long j3 = cVar3.f6267f - j;
                int i4 = i2 + 1;
                if (i4 < size) {
                    SleepInfo cVar4 = this.f8946b0.get(i4);
                    long j4 = cVar4.f6266e;
                    long j5 = j3;
                    if (j4 - cVar3.f6267f <= 60) {
                        j3 = j4 - j;
                    } else {
                        SleepInfo cVar5 = new SleepInfo();
                        cVar5.f6265d = -1;
                        cVar5.f6266e = cVar3.f6267f;
                        cVar5.f6267f = cVar4.f6266e;
                        this.f8948d0.add(cVar5);
                        cVar = cVar5;
                        j3 = j5;
                        SleepInfo cVar6 = this.f8947c0;
                        if (cVar6 != null && cVar6 == cVar3) {
                            this.f8938Q.setColor(this.f8943V);
                            m14672a(canvas2, ((float) j2) * measuredWidth);
                        }
                        int i5 = i4;
                        canvas.drawRect(((float) j2) * measuredWidth, (float) this.f8945a0, ((float) j3) * measuredWidth, measuredHeight, this.f8938Q);
                        if (cVar != null && (cVar2 = this.f8947c0) != null && cVar2.f6266e == cVar.f6266e && cVar2.f6267f == cVar.f6267f) {
                            this.f8938Q.setColor(this.f8943V);
                            m14672a(canvas2, ((float) (cVar3.f6267f - j)) * measuredWidth);
                            canvas.drawRect(((float) (cVar.f6266e - j)) * measuredWidth, (float) this.f8945a0, ((float) (cVar.f6267f - j)) * measuredWidth, measuredHeight, this.f8938Q);
                        }
                        i2 = i5;
                        i = 1;
                    }
                }
                cVar = null;
                SleepInfo cVar62 = this.f8947c0;
                this.f8938Q.setColor(this.f8943V);
                m14672a(canvas2, ((float) j2) * measuredWidth);
                int i52 = i4;
                canvas.drawRect(((float) j2) * measuredWidth, (float) this.f8945a0, ((float) j3) * measuredWidth, measuredHeight, this.f8938Q);
                this.f8938Q.setColor(this.f8943V);
                m14672a(canvas2, ((float) (cVar3.f6267f - j)) * measuredWidth);
                canvas.drawRect(((float) (cVar.f6266e - j)) * measuredWidth, (float) this.f8945a0, ((float) (cVar.f6267f - j)) * measuredWidth, measuredHeight, this.f8938Q);
                i2 = i52;
                i = 1;
            }
        }
    }

    /* renamed from: a */
    private void m14672a(Canvas canvas, float f) {
        String str;
        this.f8939R.setAntiAlias(true);
        this.f8939R.setDither(true);
        this.f8939R.setTextAlign(Paint.Align.LEFT);
        int i = this.f8947c0.f6265d;
        if (i == 1) {
            str = this.f8937P.getString(R$string.deep);
        } else if (i == 2) {
            str = this.f8937P.getString(R$string.light);
        } else if (i != 3) {
            str = this.f8937P.getString(R$string.no_sleep_data);
        } else {
            str = this.f8937P.getString(R$string.awake);
        }
        if (this.f8949e0 == null) {
            this.f8949e0 = new SimpleDateFormat("HH:mm", Locale.getDefault());
        }
        String str2 = str + ":" + this.f8949e0.format(new Date(this.f8947c0.f6266e * 1000)) + "~" + this.f8949e0.format(new Date(this.f8947c0.f6267f * 1000));
        float measureText = this.f8939R.measureText(str2);
        if (f + measureText > ((float) getMeasuredWidth())) {
            f = ((float) getMeasuredWidth()) - measureText;
        }
        canvas.drawText(str2, f, (float) (this.f8945a0 >> 1), this.f8939R);
    }
}
