package com.heimavista.fiedorasport.service;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.p109h.LocManager;
import p119e.p189e.p191b.HvApp;

public class FSJobService extends JobService {
    /* renamed from: a */
    public static void m10693a() {
        m10696b(4);
    }

    /* renamed from: b */
    public static void m10696b(int i) {
        HvApp c = HvApp.m13010c();
        JobScheduler jobScheduler = (JobScheduler) c.getSystemService("jobscheduler");
        JobInfo.Builder builder = new JobInfo.Builder(i, new ComponentName(c.getPackageName(), FSJobService.class.getName()));
        int i2 = 0;
        builder.setRequiresCharging(false);
        builder.setRequiresDeviceIdle(false);
        builder.setPersisted(true);
        if (i == 4) {
            builder.setPeriodic(1800000);
        } else {
            builder.setMinimumLatency(0);
        }
        JobInfo build = builder.build();
        if (jobScheduler != null) {
            i2 = jobScheduler.schedule(build);
        }
        m10695a("scheduleJob jobId=" + i + " schedule result=" + i2);
    }

    public void onCreate() {
        super.onCreate();
        m10695a("onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
        m10695a("onDestroy");
    }

    public boolean onStartJob(JobParameters jobParameters) {
        int jobId = jobParameters.getJobId();
        m10695a("onStartJob jobId=" + jobId);
        if (jobId != 4) {
            return false;
        }
        LocManager.m10493g().mo22958c();
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        m10695a("onStopJob ");
        return false;
    }

    /* renamed from: a */
    public static void m10694a(int i) {
        JobScheduler jobScheduler = (JobScheduler) HvApp.m13010c().getSystemService("jobscheduler");
        if (jobScheduler != null) {
            jobScheduler.cancel(i);
        }
    }

    /* renamed from: a */
    private static void m10695a(String str) {
        LogUtils.m1139c("FSJob>>>" + FSJobService.class.getSimpleName() + " " + str);
    }
}
