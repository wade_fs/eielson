package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import com.heimavista.entity.data.PeriodTimeInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;
import java.util.ArrayList;
import p019cn.qqtheme.framework.widget.WheelView;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAutoHeartActivity */
public class SettingAutoHeartActivity extends BaseActivity implements C3734y.C3735a {

    /* renamed from: b0 */
    private CommonListItemView f8258b0;

    /* renamed from: c0 */
    private WheelView f8259c0;

    /* renamed from: d0 */
    private PeriodTimeInfo f8260d0;

    /* renamed from: u */
    private void m13827u() {
        BandDataManager.m10550a(this.f8260d0);
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22886a(this.f8260d0.mo22593g(), this.f8260d0.mo22585c(), this.f8260d0.mo22587d(), this.f8260d0.mo22580a(), this.f8260d0.mo22583b(), this.f8260d0.mo22590e(), 1);
        }
        if (!FSManager.m10323r().mo22846p()) {
            return;
        }
        if (this.f8260d0.mo22593g()) {
            CRPBleManager.m10642m();
        } else {
            CRPBleManager.m10625d();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24481a(SwitchButton switchButton, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("自動心率");
        sb.append(z ? "開啟" : "關閉");
        Constant.m10311a(sb.toString());
        this.f8260d0.mo22582a(z);
        m13827u();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22875a(this);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10602a(this);
        }
        this.f8260d0 = BandDataManager.m10555b("AutoHeart");
        this.f8258b0 = (CommonListItemView) findViewById(R$id.livLock);
        this.f8259c0 = (WheelView) findViewById(R$id.wvInterval);
        this.f8259c0.setLineSpaceMultiplier(2.0f);
        this.f8259c0.setTextPadding(5);
        this.f8259c0.setTextSize(16.0f);
        this.f8259c0.setTypeface(Typeface.DEFAULT);
        this.f8259c0.setGravity(17);
        this.f8259c0.setTextSize(16.0f);
        this.f8259c0.mo9640a(getResources().getColor(R$color.font_color_hint), getResources().getColor(R$color.font_color));
        this.f8259c0.setOffset(5);
        this.f8259c0.setCycleDisable(false);
        this.f8259c0.setUseWeight(true);
        this.f8259c0.setTextSizeAutoFit(true);
        this.f8259c0.setOnItemSelectListener(new C4324o(this));
        this.f8258b0.getSwitch().setOnCheckedChangeListener(new C4323n(this));
        this.f8258b0.setOnClickListener(new C4322m(this));
    }

    /* renamed from: e */
    public /* synthetic */ void mo24482e(int i) {
        Constant.m10311a("自動心率設置值");
        this.f8260d0.mo22591e((i + 1) * 5);
        m13827u();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_auto_heart;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.remind_auto_heart);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24480a(View view) {
        this.f8258b0.getSwitch().toggle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        ArrayList arrayList = new ArrayList();
        for (int i = 5; i < 60; i += 5) {
            arrayList.add(Integer.valueOf(i));
        }
        this.f8259c0.setItems(arrayList);
        int e = (this.f8260d0.mo22590e() / 5) - 1;
        if (e < 0) {
            e = 0;
        }
        this.f8259c0.setSelectedIndex(e);
        this.f8258b0.getSwitch().setChecked(this.f8260d0.mo22593g());
    }

    /* renamed from: a */
    public void mo22964a(C3734y.C3736b bVar, boolean z) {
        C3734y.C3736b bVar2 = C3734y.C3736b.autoHeart;
    }
}
