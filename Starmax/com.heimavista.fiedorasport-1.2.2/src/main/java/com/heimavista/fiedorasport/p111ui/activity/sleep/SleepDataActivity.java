package com.heimavista.fiedorasport.p111ui.activity.sleep;

import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.PieChartManager;
import com.heimavista.fiedorasport.widget.LineView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p193d.p194i.SleepDataDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataActivity */
public class SleepDataActivity extends BaseActivity {

    /* renamed from: b0 */
    private TextView f8338b0;

    /* renamed from: c0 */
    private TextView f8339c0;

    /* renamed from: d0 */
    private TextView f8340d0;

    /* renamed from: e0 */
    private TextView f8341e0;

    /* renamed from: f0 */
    private TextView f8342f0;

    /* renamed from: g0 */
    private TextView f8343g0;

    /* renamed from: h0 */
    private TextView f8344h0;

    /* renamed from: i0 */
    private TextView f8345i0;

    /* renamed from: j0 */
    private TextView f8346j0;

    /* renamed from: k0 */
    private TextView f8347k0;

    /* renamed from: l0 */
    private TextView f8348l0;

    /* renamed from: m0 */
    private TextView f8349m0;

    /* renamed from: n0 */
    private PieChart f8350n0;

    /* renamed from: o0 */
    private LineView f8351o0;

    /* renamed from: p0 */
    private View f8352p0;

    /* renamed from: q0 */
    private View f8353q0;

    /* renamed from: r0 */
    private Calendar f8354r0;

    /* renamed from: s0 */
    private SimpleDateFormat f8355s0;

    /* renamed from: t0 */
    private int f8356t0 = -1;
    /* access modifiers changed from: private */

    /* renamed from: u0 */
    public SparseArray<SleepDayInfo> f8357u0 = new SparseArray<>();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sleep.SleepDataActivity$a */
    class C4336a extends ThreadUtils.C0877e<SleepDayInfo> {

        /* renamed from: U */
        final /* synthetic */ int f8358U;

        C4336a(int i) {
            this.f8358U = i;
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(SleepDayInfo bVar) {
            if (bVar != null) {
                SleepDataActivity.this.f8357u0.put(this.f8358U, bVar);
                SleepDataActivity.this.m13941a(bVar);
            }
        }

        /* renamed from: b */
        public SleepDayInfo m13953b() {
            return new SleepDataDb().mo24261a(this.f8358U);
        }
    }

    /* renamed from: e */
    private void m13943e(int i) {
        ThreadUtils.m963c(new C4336a(i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8338b0 = (TextView) findViewById(R$id.tvDate);
        this.f8339c0 = (TextView) findViewById(R$id.tvSleepTime);
        this.f8340d0 = (TextView) findViewById(R$id.tvAsleep);
        this.f8341e0 = (TextView) findViewById(R$id.tvRise);
        this.f8342f0 = (TextView) findViewById(R$id.tvNocturnalSleep);
        this.f8343g0 = (TextView) findViewById(R$id.tvNocturnalSleepAssess);
        this.f8344h0 = (TextView) findViewById(R$id.tvStart);
        this.f8345i0 = (TextView) findViewById(R$id.tvDeepSleepAssess);
        this.f8346j0 = (TextView) findViewById(R$id.tvCenter);
        this.f8347k0 = (TextView) findViewById(R$id.tvLightSleepAssess);
        this.f8348l0 = (TextView) findViewById(R$id.tvEnd);
        this.f8349m0 = (TextView) findViewById(R$id.tvAwakeTimesAssess);
        this.f8350n0 = (PieChart) findViewById(R$id.pieChart);
        this.f8351o0 = (LineView) findViewById(R$id.lineView);
        this.f8352p0 = findViewById(R$id.ivBeforeDay);
        this.f8353q0 = findViewById(R$id.ivNextDay);
        mo22714a(this.f8352p0, this.f8353q0);
        mo22723b(R$id.llStatistics);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_sleep;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.llStatistics) {
            Constant.m10311a("點擊統計");
            mo22702a(SleepDataChartActivity.class);
        } else if (id == R$id.ivBeforeDay || id == R$id.ivNextDay) {
            m13940a(view);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2002", "exad_sleep");
        Paint c = this.f8350n0.mo13033c(7);
        c.setColor(getResources().getColor(R$color.font_color));
        c.setTextSize((float) getResources().getDimensionPixelSize(R$dimen.font_size_18));
        this.f8350n0.setNoDataText(getString(R$string.no_sleep_data));
        this.f8354r0 = Calendar.getInstance();
        m13940a(this.f8352p0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13941a(SleepDayInfo bVar) {
        String str;
        String str2;
        String str3;
        String str4;
        List<SleepInfo> list = bVar.f6261j;
        this.f8351o0.setSleepDataList(list);
        boolean z = bVar.f6257f == 0;
        SpanUtils a = SpanUtils.m865a(this.f8339c0);
        String str5 = "--";
        if (z) {
            str = str5;
        } else {
            str = String.valueOf(bVar.f6258g / 60);
        }
        a.mo9735a(str);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str2 = str5;
        } else {
            str2 = String.valueOf(bVar.f6258g % 60);
        }
        a.mo9735a(str2);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a.mo9738c();
        a.mo9735a(getString(R$string.data_unit_min));
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8348l0);
        a2.mo9735a(getString(R$string.awake_times));
        a2.mo9735a(" ");
        if (z) {
            str3 = str5;
        } else {
            str3 = String.valueOf(bVar.f6259h);
        }
        a2.mo9735a(str3);
        a2.mo9735a(getString(R$string.data_unit_count));
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8349m0);
        a3.mo9735a(getString(bVar.f6259h <= 2 ? R$string.assess_normal : R$string.assess_more));
        a3.mo9737b(getResources().getColor(bVar.f6259h > 2 ? R$color.blue_2 : R$color.font_color));
        a3.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8342f0);
        a4.mo9735a(getString(R$string.nocturnal_sleep));
        a4.mo9735a(" ");
        if (z) {
            str4 = str5;
        } else {
            str4 = String.valueOf(bVar.f6258g / 60);
        }
        a4.mo9735a(str4);
        a4.mo9735a(getString(R$string.data_unit_hour));
        if (!z) {
            str5 = String.valueOf(bVar.f6258g % 60);
        }
        a4.mo9735a(str5);
        a4.mo9735a(getString(R$string.data_unit_min));
        a4.mo9736b();
        SpanUtils a5 = SpanUtils.m865a(this.f8343g0);
        int i = bVar.f6258g;
        a5.mo9735a(getString(i < 360 ? R$string.assess_sleep_too_short : i > 600 ? R$string.assess_sleep_too_much : R$string.assess_normal));
        Resources resources = getResources();
        int i2 = bVar.f6258g;
        a5.mo9737b(resources.getColor((i2 < 360 || i2 > 600) ? R$color.blue_2 : R$color.font_color));
        a5.mo9736b();
        if (z) {
            SpanUtils a6 = SpanUtils.m865a(this.f8340d0);
            a6.mo9735a("-");
            a6.mo9735a(getString(R$string.data_unit_hour));
            a6.mo9735a("-");
            a6.mo9735a(getString(R$string.data_unit_min));
            a6.mo9735a(" ");
            a6.mo9735a(getString(R$string.asleep));
            a6.mo9736b();
            SpanUtils a7 = SpanUtils.m865a(this.f8341e0);
            a7.mo9735a("-");
            a7.mo9735a(getString(R$string.data_unit_hour));
            a7.mo9735a("-");
            a7.mo9735a(getString(R$string.data_unit_min));
            a7.mo9735a(" ");
            a7.mo9735a(getString(R$string.awake));
            a7.mo9736b();
            this.f8343g0.setVisibility(8);
            this.f8345i0.setVisibility(8);
            this.f8347k0.setVisibility(8);
            this.f8349m0.setVisibility(8);
            SpanUtils a8 = SpanUtils.m865a(this.f8344h0);
            a8.mo9735a(getString(R$string.deep_sleep));
            a8.mo9736b();
            SpanUtils a9 = SpanUtils.m865a(this.f8346j0);
            a9.mo9735a(getString(R$string.light_sleep));
            a9.mo9736b();
            SpanUtils a10 = SpanUtils.m865a(this.f8346j0);
            a10.mo9735a(getString(R$string.light_sleep));
            a10.mo9736b();
        } else {
            if (this.f8355s0 == null) {
                this.f8355s0 = new SimpleDateFormat("HH:mm", Locale.getDefault());
            }
            SleepInfo cVar = list.get(0);
            long j = cVar.f6266e;
            if (cVar.f6265d == 3) {
                j = cVar.f6267f + 60;
            }
            SpanUtils a11 = SpanUtils.m865a(this.f8340d0);
            a11.mo9735a(this.f8355s0.format(new Date(j * 1000)));
            a11.mo9735a(" ");
            a11.mo9735a(getString(R$string.asleep));
            a11.mo9736b();
            SpanUtils a12 = SpanUtils.m865a(this.f8341e0);
            a12.mo9735a(this.f8355s0.format(new Date(list.get(list.size() - 1).f6267f * 1000)));
            a12.mo9735a(" ");
            a12.mo9735a(getString(R$string.awake));
            a12.mo9736b();
            this.f8343g0.setVisibility(0);
            this.f8345i0.setVisibility(0);
            this.f8347k0.setVisibility(0);
            this.f8349m0.setVisibility(0);
            int i3 = (bVar.f6254c * 100) / bVar.f6257f;
            SpanUtils a13 = SpanUtils.m865a(this.f8344h0);
            a13.mo9735a(getString(R$string.deep_sleep));
            a13.mo9735a(" ");
            a13.mo9735a(String.format(Locale.getDefault(), "%d", Integer.valueOf(i3)));
            a13.mo9735a("%");
            a13.mo9736b();
            SpanUtils a14 = SpanUtils.m865a(this.f8345i0);
            a14.mo9735a(getString(i3 < 20 ? R$string.assess_sleep_too_short : i3 > 60 ? R$string.assess_sleep_full : R$string.assess_normal));
            a14.mo9737b(getResources().getColor((i3 < 20 || i3 > 60) ? R$color.blue_2 : R$color.font_color));
            a14.mo9736b();
            int i4 = (bVar.f6255d * 100) / bVar.f6257f;
            SpanUtils a15 = SpanUtils.m865a(this.f8346j0);
            a15.mo9735a(getString(R$string.light_sleep));
            a15.mo9735a(" ");
            a15.mo9735a(String.format(Locale.getDefault(), "%d", Integer.valueOf(i4)));
            a15.mo9735a("%");
            a15.mo9736b();
            SpanUtils a16 = SpanUtils.m865a(this.f8347k0);
            a16.mo9735a(getString(i4 > 55 ? R$string.assess_more : R$string.assess_normal));
            a16.mo9737b(getResources().getColor(i4 > 55 ? R$color.blue_2 : R$color.font_color));
            a16.mo9736b();
        }
        m13939a((float) bVar.f6257f, bVar.f6254c, bVar.f6255d, bVar.f6256e);
    }

    /* renamed from: a */
    private void m13939a(float f, int i, int i2, int i3) {
        ArrayList arrayList = new ArrayList();
        float f2 = 0.0f;
        int i4 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
        float f3 = i4 == 0 ? 0.0f : ((float) i) / f;
        arrayList.add(new PieEntry(f3, getString(R$string.deep) + "    |    " + (i / 60) + getString(R$string.data_unit_hour) + (i % 60) + getString(R$string.data_unit_min)));
        float f4 = i4 == 0 ? 0.0f : ((float) i2) / f;
        arrayList.add(new PieEntry(f4, getString(R$string.light) + "    |    " + (i2 / 60) + getString(R$string.data_unit_hour) + (i2 % 60) + getString(R$string.data_unit_min)));
        if (i4 != 0) {
            f2 = ((float) i3) / f;
        }
        arrayList.add(new PieEntry(f2, getString(R$string.awake) + "    |    " + (i3 / 60) + getString(R$string.data_unit_hour) + (i3 % 60) + getString(R$string.data_unit_min)));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_deep)));
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_light)));
        arrayList2.add(Integer.valueOf(getResources().getColor(R$color.sleep_awake)));
        new PieChartManager(this.f8350n0, 500, new SpannableStringBuilder(getString(i4 == 0 ? R$string.no_sleep_data : R$string.sleep_ratio))).mo24368a(arrayList, arrayList2, "");
    }

    /* renamed from: a */
    private void m13940a(View view) {
        int id = view.getId();
        if (id == R$id.ivBeforeDay) {
            Constant.m10311a("點擊查看前一天");
            this.f8356t0++;
            this.f8354r0.add(6, -1);
        } else if (id == R$id.ivNextDay) {
            int i = this.f8356t0;
            if (i == 0) {
                mo22730c(getString(R$string.not_yet_arrived_tomorrow));
                return;
            } else {
                this.f8356t0 = i - 1;
                this.f8354r0.add(6, 1);
            }
        }
        TextView textView = this.f8338b0;
        StringBuilder sb = new StringBuilder();
        sb.append(this.f8356t0 == 0 ? getString(R$string.last_night) : new SimpleDateFormat("M/d", Locale.getDefault()).format(this.f8354r0.getTime()));
        sb.append(getString(R$string.sleep));
        textView.setText(sb.toString());
        if (this.f8357u0.get(this.f8356t0) == null) {
            m13943e(this.f8356t0);
        } else {
            m13941a(this.f8357u0.get(this.f8356t0));
        }
    }
}
