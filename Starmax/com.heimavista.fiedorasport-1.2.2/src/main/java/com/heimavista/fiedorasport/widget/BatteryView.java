package com.heimavista.fiedorasport.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.ViewCompat;
import com.heimavista.fiedorasport.lib.R$styleable;

public class BatteryView extends View {

    /* renamed from: P */
    private int f8897P = 100;

    /* renamed from: Q */
    private int f8898Q;

    /* renamed from: R */
    private int f8899R;

    /* renamed from: S */
    private int f8900S;

    /* renamed from: T */
    private int f8901T;

    /* renamed from: U */
    private boolean f8902U = false;

    public BatteryView(Context context) {
        super(context);
    }

    /* renamed from: a */
    private void m14648a(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(this.f8901T);
        paint.setStyle(Paint.Style.STROKE);
        float f = ((float) this.f8899R) / 20.0f;
        float f2 = f / 2.0f;
        paint.setStrokeWidth(f);
        RectF rectF = new RectF(f2, f2, (((float) this.f8899R) - f) - f2, ((float) this.f8900S) - f2);
        paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        canvas.drawRect(rectF, paint);
        paint.setStrokeWidth(0.0f);
        paint.setStyle(Paint.Style.FILL);
        float f3 = f + 1.0f;
        RectF rectF2 = new RectF(f3, f3, (((((float) this.f8899R) - (2.0f * f)) * ((float) this.f8897P)) / 100.0f) - 1.0f, (((float) this.f8900S) - f) - 1.0f);
        if (this.f8902U) {
            paint.setColor(-16711936);
        } else if (this.f8897P < 20) {
            paint.setColor((int) SupportMenu.CATEGORY_MASK);
        } else {
            paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        }
        canvas.drawRect(rectF2, paint);
        int i = this.f8899R;
        float f4 = ((float) i) - f;
        int i2 = this.f8900S;
        RectF rectF3 = new RectF(f4, ((float) i2) * 0.25f, (float) i, ((float) i2) * 0.75f);
        paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        canvas.drawRect(rectF3, paint);
    }

    /* renamed from: b */
    private void m14649b(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(this.f8901T);
        paint.setStyle(Paint.Style.STROKE);
        float f = ((float) this.f8900S) / 20.0f;
        float f2 = f / 2.0f;
        paint.setStrokeWidth(f);
        int i = (int) (0.5f + f);
        float f3 = (float) i;
        canvas.drawRect(new RectF(f2, f3 + f2, ((float) this.f8899R) - f2, ((float) this.f8900S) - f2), paint);
        paint.setStrokeWidth(0.0f);
        int i2 = this.f8900S;
        RectF rectF = new RectF(f, f3 + f + (((((float) (i2 - i)) - f) * ((float) (100 - this.f8897P))) / 100.0f), ((float) this.f8899R) - f, ((float) i2) - f);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(rectF, paint);
        int i3 = this.f8899R;
        canvas.drawRect(new RectF(((float) i3) / 4.0f, 0.0f, ((float) i3) * 0.75f, f3), paint);
    }

    public int getPower() {
        return this.f8897P;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f8898Q == 0) {
            m14648a(canvas);
        } else {
            m14649b(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.f8899R = getMeasuredWidth();
        this.f8900S = getMeasuredHeight();
    }

    public void setCharging(boolean z) {
        this.f8902U = z;
    }

    public void setColor(int i) {
        this.f8901T = i;
        invalidate();
    }

    public void setPower(int i) {
        this.f8897P = i;
        if (this.f8897P < 0) {
            this.f8897P = 100;
        }
        invalidate();
    }

    public BatteryView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.BatteryView);
        this.f8901T = obtainStyledAttributes.getColor(R$styleable.BatteryView_batteryColor, -1);
        this.f8898Q = obtainStyledAttributes.getInt(R$styleable.BatteryView_batteryOrientation, 0);
        this.f8897P = obtainStyledAttributes.getInt(R$styleable.BatteryView_batteryPower, 50);
        this.f8899R = getMeasuredWidth();
        this.f8900S = getMeasuredHeight();
        obtainStyledAttributes.recycle();
    }
}
