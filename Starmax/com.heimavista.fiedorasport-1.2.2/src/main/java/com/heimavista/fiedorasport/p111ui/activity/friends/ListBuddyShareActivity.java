package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ConvertUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiGetBuddyShare;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingShareOptionsActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListBuddyShareAdapter;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import p119e.p189e.p193d.p195j.BuddyShareDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListBuddyShareActivity */
public class ListBuddyShareActivity extends BaseActivity {

    /* renamed from: b0 */
    private RecyclerView f8091b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public ListBuddyShareAdapter f8092c0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListBuddyShareActivity$a */
    class C4231a implements OnResultListener<ParamJsonData> {
        C4231a() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            if (!ListBuddyShareActivity.this.isFinishing()) {
                ListBuddyShareActivity.this.f8092c0.mo22792b((List) BuddyShareDb.m13290u());
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8091b0 = (RecyclerView) findViewById(R$id.recyclerView);
        ((FrameLayout.LayoutParams) this.f8091b0.getLayoutParams()).topMargin = ConvertUtils.m1055a(1.0f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.recyclerView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.setting_share_permission);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8092c0 = new ListBuddyShareAdapter(this);
        this.f8092c0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4252p(this));
        this.f8091b0.setAdapter(this.f8092c0);
        this.f8092c0.mo22792b((List) BuddyShareDb.m13290u());
        new HvApiGetBuddyShare().request(new C4231a());
    }

    /* renamed from: a */
    public /* synthetic */ void mo24393a(View view, BuddyShareDb bVar, int i) {
        String str;
        Intent intent = new Intent(this, SettingShareOptionsActivity.class);
        UserInfoDb p = UserInfoDb.m13344p(bVar.mo24232o());
        if (p == null) {
            str = "";
        } else {
            str = p.mo24338x();
        }
        intent.putExtra("userName", str);
        intent.putExtra("userNbr", bVar.mo24232o());
        mo22696a(intent, new C4251o(this, i, bVar));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24392a(int i, BuddyShareDb bVar, int i2, Intent intent) {
        this.f8092c0.mo22790b(i, BuddyShareDb.m13288i(bVar.mo24232o()));
    }
}
