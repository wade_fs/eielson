package com.heimavista.fiedorasport.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;

public class PhoneBroadcastReceiver extends BroadcastReceiver {

    /* renamed from: com.heimavista.fiedorasport.receiver.PhoneBroadcastReceiver$a */
    class C3746a extends PhoneStateListener {

        /* renamed from: a */
        final /* synthetic */ Context f6620a;

        C3746a(Context context) {
            this.f6620a = context;
        }

        public void onCallStateChanged(int i, String str) {
            String str2;
            if (i == 0) {
                if (FSManager.m10323r().mo22846p()) {
                    CRPBleManager.m10644o();
                }
                str2 = "挂断";
            } else if (i == 1) {
                PhoneBroadcastReceiver.this.m10690a(this.f6620a, str);
                str2 = "响铃";
            } else if (i != 2) {
                str2 = "";
            } else {
                if (FSManager.m10323r().mo22846p()) {
                    CRPBleManager.m10644o();
                }
                str2 = "接听";
            }
            LogUtils.m1139c("PhoneBroadcastReceiver " + str2 + ": " + str);
            super.onCallStateChanged(i, str);
        }
    }

    public void onReceive(Context context, Intent intent) {
        TelephonyManager telephonyManager;
        boolean d = BandDataManager.m10563e().mo22605d();
        LogUtils.m1139c("PhoneBroadcastReceiver onReceive remind=" + d);
        if (d && intent != null && intent.getAction() != null) {
            LogUtils.m1139c("PhoneBroadcastReceiver", "action=" + intent.getAction());
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                LogUtils.m1139c("PhoneBroadcastReceiver", "去电 phoneNumber=" + intent.getStringExtra("android.intent.extra.PHONE_NUMBER"));
            } else if (intent.getAction().equals("android.intent.action.PHONE_STATE") && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
                telephonyManager.listen(new C3746a(context), 32);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10690a(Context context, String str) {
        if (FSManager.m10323r().mo22846p() && CRPBleManager.m10639j()) {
            CRPBleManager.m10605a(str + "", 0);
        }
        if (FSManager.m10323r().mo22847q() && JYSDKManager.m10388i().mo22897e()) {
            JYSDKManager.m10388i().mo22879a((System.currentTimeMillis() / 1000) + "", 0, str + "", "phone");
        }
    }
}
