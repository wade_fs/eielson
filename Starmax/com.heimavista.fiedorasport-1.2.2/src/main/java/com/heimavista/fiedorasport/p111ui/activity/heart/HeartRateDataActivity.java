package com.heimavista.fiedorasport.p111ui.activity.heart;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.HeartRateDialog;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p111ui.activity.heart.HeartRateAdapter;
import com.heimavista.widget.dialog.BaseDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.xutils.C5217x;
import p119e.p189e.p193d.p194i.HeartRateDataDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateDataActivity */
public class HeartRateDataActivity extends BaseActivity {

    /* renamed from: b0 */
    private TextView f8150b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public TextView f8151c0;

    /* renamed from: d0 */
    private RecyclerView f8152d0;

    /* renamed from: e0 */
    private SmartRefreshLayout f8153e0;

    /* renamed from: f0 */
    private HeartRateAdapter f8154f0;

    /* renamed from: g0 */
    private int f8155g0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public List<HeartRateAdapter.C4262a> f8156h0 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: i0 */
    public BaseDialog f8157i0;

    /* renamed from: j0 */
    private int f8158j0 = 0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateDataActivity$a */
    class C4264a extends ThreadUtils.C0877e<HeartRateDataDb> {
        C4264a() {
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(HeartRateDataDb dVar) {
            String str;
            SpanUtils a = SpanUtils.m865a(HeartRateDataActivity.this.f8151c0);
            if (dVar == null) {
                str = "--";
            } else {
                str = String.valueOf(dVar.mo24232o());
            }
            a.mo9735a(str);
            a.mo9733a(HeartRateDataActivity.this.getResources().getDimensionPixelSize(R$dimen.font_size_30));
            a.mo9735a(HeartRateDataActivity.this.getString(R$string.data_unit_rate));
            a.mo9736b();
        }

        /* renamed from: b */
        public HeartRateDataDb m13652b() {
            return HeartRateDataDb.m13157s();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateDataActivity$b */
    class C4265b extends ThreadUtils.C0877e<List<HeartRateAdapter.C4262a>> {
        C4265b() {
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24426a((List<HeartRateAdapter.C4262a>) ((List) obj));
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24426a(List<HeartRateAdapter.C4262a> list) {
            List unused = HeartRateDataActivity.this.f8156h0 = list;
            HeartRateDataActivity.this.m13633z();
        }

        /* renamed from: b */
        public List<HeartRateAdapter.C4262a> mo9798b() {
            ArrayList arrayList = new ArrayList();
            long j = 0;
            for (HeartRateDataDb dVar : HeartRateDataDb.m13156r()) {
                long p = dVar.mo24259p();
                if (j == 0 || !m13654a(j, p)) {
                    arrayList.add(new HeartRateAdapter.C4262a(1, dVar));
                    j = p;
                }
                arrayList.add(new HeartRateAdapter.C4262a(2, dVar));
            }
            return arrayList;
        }

        /* renamed from: a */
        private boolean m13654a(long j, long j2) {
            return Math.abs(j - j2) < 86400 && (j + ((long) TimeZone.getDefault().getOffset(j))) / 86400 == (j2 + ((long) TimeZone.getDefault().getOffset(j2))) / 86400;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.heart.HeartRateDataActivity$c */
    class C4266c extends CountDownTimer {
        C4266c(long j, long j2) {
            super(j, j2);
        }

        public void onFinish() {
            if (HeartRateDataActivity.this.f8157i0 != null && HeartRateDataActivity.this.f8157i0.isShowing() && !HeartRateDataActivity.this.isFinishing()) {
                HeartRateDataActivity.this.f8157i0.dismiss();
                BaseDialog unused = HeartRateDataActivity.this.f8157i0 = (BaseDialog) null;
            }
        }

        public void onTick(long j) {
        }
    }

    /* renamed from: w */
    private void m13630w() {
        ThreadUtils.m959b(new C4264a());
    }

    /* renamed from: x */
    private void m13631x() {
        if (this.f8157i0 == null) {
            HeartRateDialog fVar = new HeartRateDialog(this);
            fVar.mo25413a(false);
            this.f8157i0 = fVar.mo25418b();
        }
        this.f8157i0.mo25389a(new C4267a(this));
        this.f8157i0.show();
        new C4266c(DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS, 1000).start();
    }

    /* renamed from: y */
    private void m13632y() {
        ThreadUtils.m963c(new C4265b());
    }

    /* access modifiers changed from: private */
    /* renamed from: z */
    public void m13633z() {
        int i = this.f8155g0;
        int size = (i + 1) * 20 <= this.f8156h0.size() ? (this.f8155g0 + 1) * 20 : this.f8156h0.size();
        for (int i2 = i * 20; i2 < size; i2++) {
            this.f8154f0.mo22785a(this.f8156h0.get(i2));
        }
        this.f8153e0.mo26057c();
    }

    /* renamed from: e */
    public /* synthetic */ void mo24424e(int i) {
        SpanUtils a = SpanUtils.m865a(this.f8151c0);
        a.mo9735a(String.valueOf(i));
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a.mo9735a(getString(R$string.data_unit_rate));
        a.mo9736b();
        this.f8155g0 = 0;
        this.f8154f0.mo22782a();
        m13632y();
        C5217x.task().postDelayed(new C4270d(this), 1000);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_heart;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return "";
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.h.a0.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, int):int
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, long):long
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, com.sxr.sdk.ble.keepfit.aidl.a):com.sxr.sdk.ble.keepfit.aidl.a
      com.heimavista.fiedorasport.h.a0.a(long, int):void
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean
      com.heimavista.fiedorasport.h.a0.a(int, int):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.h.a0.a(boolean, int):void */
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.llMeasure) {
            Constant.m10311a("點擊測量");
            if (TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
                mo22730c(getString(R$string.device_has_not_bind));
            } else if (FSManager.m10323r().mo22847q()) {
                if (!JYSDKManager.m10388i().mo22897e()) {
                    mo22730c(getString(R$string.device_has_not_connect));
                    return;
                }
                JYSDKManager.m10388i().mo22885a(true, 20);
                m13631x();
            } else if (!FSManager.m10323r().mo22846p()) {
            } else {
                if (!CRPBleManager.m10639j()) {
                    mo22730c(getString(R$string.device_has_not_connect));
                    return;
                }
                CRPBleManager.m10641l();
                m13631x();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public void mo22750s() {
        super.mo22750s();
    }

    /* renamed from: u */
    public /* synthetic */ void mo23013u() {
        BaseDialog baseDialog;
        if (!isFinishing() && (baseDialog = this.f8157i0) != null && baseDialog.isShowing()) {
            this.f8157i0.dismiss();
        }
    }

    /* renamed from: v */
    public /* synthetic */ void mo23014v() {
        BaseDialog baseDialog;
        if (!isFinishing() && (baseDialog = this.f8157i0) != null && baseDialog.isShowing()) {
            this.f8157i0.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8150b0 = (TextView) findViewById(R$id.tvDate);
        this.f8151c0 = (TextView) findViewById(R$id.tvHeartRate);
        this.f8152d0 = (RecyclerView) findViewById(R$id.recyclerView);
        this.f8153e0 = (SmartRefreshLayout) findViewById(R$id.refreshLayout);
        mo22723b(R$id.llMeasure);
    }

    /* renamed from: c */
    public void mo22727c(int i) {
        runOnUiThread(new C4271e(this, i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2004", "exad_heart");
        this.f8150b0.setText(TimeUtils.m1003a(new Date(), new SimpleDateFormat("M月dd日", Locale.getDefault())));
        m13630w();
        this.f8154f0 = new HeartRateAdapter(this);
        this.f8152d0.setAdapter(this.f8154f0);
        m13632y();
        this.f8153e0.mo26049a(true);
        this.f8153e0.mo26047a(new C4269c(this));
    }

    /* renamed from: b */
    public /* synthetic */ void mo24423b(BaseDialog baseDialog) {
        this.f8157i0 = null;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24422a(RefreshLayout jVar) {
        if (this.f8154f0.mo22794c() == null || this.f8154f0.mo22794c().size() < this.f8156h0.size()) {
            this.f8155g0++;
            m13633z();
            return;
        }
        jVar.mo26038a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.h.a0.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, int):int
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, long):long
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, com.sxr.sdk.ble.keepfit.aidl.a):com.sxr.sdk.ble.keepfit.aidl.a
      com.heimavista.fiedorasport.h.a0.a(long, int):void
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean
      com.heimavista.fiedorasport.h.a0.a(int, int):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.h.a0.a(boolean, int):void */
    /* renamed from: a */
    public void mo22688a(long j, int i, int i2) {
        this.f8158j0++;
        if (this.f8158j0 == 5) {
            m13630w();
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22885a(false, 0);
            }
            this.f8155g0 = 0;
            this.f8154f0.mo22782a();
            m13632y();
            C5217x.task().postDelayed(new C4268b(this), 1000);
            this.f8158j0 = 0;
        }
    }
}
