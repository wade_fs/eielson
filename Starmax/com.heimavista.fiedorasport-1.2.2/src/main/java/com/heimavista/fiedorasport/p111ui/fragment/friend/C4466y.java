package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.app.AlertDialog;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.api.OnResultListener;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.y */
/* compiled from: BuddyDynamicFragment */
class C4466y implements OnResultListener<ParamJsonData> {

    /* renamed from: a */
    final /* synthetic */ AlertDialog f8802a;

    /* renamed from: b */
    final /* synthetic */ FriendBean f8803b;

    /* renamed from: c */
    final /* synthetic */ CharSequence f8804c;

    /* renamed from: d */
    final /* synthetic */ int f8805d;

    /* renamed from: e */
    final /* synthetic */ BuddyDynamicFragment f8806e;

    C4466y(BuddyDynamicFragment buddyDynamicFragment, AlertDialog alertDialog, FriendBean dVar, CharSequence charSequence, int i) {
        this.f8806e = buddyDynamicFragment;
        this.f8802a = alertDialog;
        this.f8803b = dVar;
        this.f8804c = charSequence;
        this.f8805d = i;
    }

    /* renamed from: a */
    public void mo22380a(ParamJsonData fVar) {
        this.f8802a.cancel();
        if (this.f8803b.mo22636a() != null) {
            this.f8803b.mo22636a().mo24301i(this.f8804c.toString());
            LogUtils.m1139c("op=setRemark " + this.f8805d + " " + this.f8803b.mo22636a().mo24306s());
            this.f8806e.f8699X.notifyItemChanged(this.f8805d, this.f8803b);
        }
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f8806e.mo22761a(str);
        this.f8802a.cancel();
    }
}
