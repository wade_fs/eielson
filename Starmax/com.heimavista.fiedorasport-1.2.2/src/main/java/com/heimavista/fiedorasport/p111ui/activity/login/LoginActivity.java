package com.heimavista.fiedorasport.p111ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.facebook.internal.ServerProtocol;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiSmsLogin;
import com.heimavista.api.account.HvApiSocialRegister;
import com.heimavista.api.band.HvApiGetBandInfo;
import com.heimavista.api.band.HvApiGetBandList;
import com.heimavista.api.band.HvApiGetConfig;
import com.heimavista.api.sport.HvApiMyLikeList;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.band.TickType;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.device.ScanActivity;
import com.heimavista.fiedorasport.p111ui.activity.user.FillUserInfoActivity;
import com.heimavista.fiedorasport.p111ui.activity.web.WebActivity;
import com.heimavista.fiedorasport.p111ui.fragment.login.MobileLoginFragment;
import com.heimavista.fiedorasport.p111ui.fragment.login.ThirdPartLoginFragment;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.thirdpart.IThirdPartyLogin;
import com.heimavista.thirdpart.MemberProfile;
import com.heimavista.thirdpart.Platform;
import com.heimavista.thirdpart.ThirdPartyLoginFactory;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import p119e.p189e.p192c.HvAppConfig;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity */
public class LoginActivity extends BaseActivity {

    /* renamed from: b0 */
    private CheckBox f8197b0;

    /* renamed from: c0 */
    private ViewPager2 f8198c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public IThirdPartyLogin f8199d0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$a */
    class C4280a extends FragmentStateAdapter {
        C4280a(LoginActivity loginActivity, FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @NonNull
        public Fragment createFragment(int i) {
            if (i == 0) {
                return ThirdPartLoginFragment.m14630i();
            }
            return MobileLoginFragment.m14623j();
        }

        public int getItemCount() {
            return 2;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$b */
    class C4281b implements OnResultListener<ParamJsonData> {

        /* renamed from: a */
        final /* synthetic */ boolean f8200a;

        C4281b(boolean z) {
            this.f8200a = z;
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            LoginActivity.this.mo22717b();
            if (this.f8200a) {
                LoginActivity.this.mo22721b(HomeActivity.class);
            } else {
                LoginActivity.this.mo24448x();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            LoginActivity.this.mo22717b();
            if (TextUtils.isEmpty(str)) {
                str = LoginActivity.this.getString(R$string.net_err);
            }
            LoginActivity.this.m13718b(str, this.f8200a);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$c */
    class C4282c implements OnResultListener<ParamJsonData> {
        C4282c() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            LoginActivity.this.mo24449y();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            LoginActivity.this.m13710B();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$d */
    class C4283d implements PermissionUtils.C0922e {
        C4283d() {
        }

        /* renamed from: a */
        public void mo9866a() {
            LoginActivity.this.f8199d0.login(Platform.GOOGLE, LoginActivity.this.m13720d("gp#"));
        }

        /* renamed from: b */
        public void mo9867b() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$e */
    class C4284e implements IThirdPartyLogin.Callback {

        /* renamed from: a */
        final /* synthetic */ String f8204a;

        C4284e(String str) {
            this.f8204a = str;
        }

        public void onCancel() {
            LoginActivity loginActivity = LoginActivity.this;
            loginActivity.mo22730c(loginActivity.getString(17039360));
        }

        public void onComplete(MemberProfile memberProfile) {
            LoginActivity.this.m13716a(this.f8204a, memberProfile);
        }

        public void onError(String str) {
            LoginActivity loginActivity = LoginActivity.this;
            loginActivity.mo22730c(LoginActivity.this.getString(R$string.login_failed) + "\n" + str);
        }

        public void onStart() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$f */
    class C4285f implements OnResultListener<Void> {

        /* renamed from: a */
        final /* synthetic */ String f8206a;

        C4285f(String str) {
            this.f8206a = str;
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            Constant.m10311a(this.f8206a + "登入成功");
            FSManager.m10323r().mo22832c(this.f8206a);
            LoginActivity.this.mo24448x();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            Constant.m10311a(this.f8206a + "登入失敗");
            LoginActivity.this.mo22717b();
            if (TextUtils.isEmpty(str)) {
                str = LoginActivity.this.getString(R$string.login_failed);
            }
            LoginActivity.this.mo22730c(str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.login.LoginActivity$g */
    class C4286g implements OnResultListener<Void> {
        C4286g() {
        }

        /* renamed from: a */
        public void mo22380a(Void voidR) {
            Constant.m10311a("手機登入成功");
            FSManager.m10323r().mo22832c("mobile");
            LoginActivity.this.mo24448x();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            Constant.m10311a("手機登入失敗");
            LoginActivity.this.mo22717b();
            LoginActivity.this.mo22730c(str);
        }
    }

    /* renamed from: A */
    private boolean m13709A() {
        if (this.f8197b0.isChecked()) {
            return true;
        }
        SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
        a.mo9783a(getString(R$string.check_privacy_first));
        a.mo9785a(getString(17039370), new C4290b(this));
        a.mo9782a(-2);
        a.mo9786a();
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: B */
    public void m13710B() {
        if (HvAppConfig.m13024a().mo24166a("app", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION).compareTo("1.7.0") >= 0) {
            new HvApiGetBandList().request(m13711C());
        } else {
            new HvApiGetBandInfo().request(m13711C());
        }
    }

    /* renamed from: C */
    private OnResultListener<ParamJsonData> m13711C() {
        return new C4282c();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public IThirdPartyLogin.Callback m13720d(String str) {
        return new C4284e(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return true;
    }

    /* renamed from: e */
    public void mo24447e(@IntRange(from = 0, mo464to = 1) int i) {
        this.f8198c0.setCurrentItem(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_login_new;
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        IThirdPartyLogin iThirdPartyLogin = this.f8199d0;
        if (iThirdPartyLogin != null) {
            iThirdPartyLogin.onActivityResult(i, i2, intent);
        }
    }

    public void onBackPressed() {
        if (this.f8198c0.getCurrentItem() == 1) {
            mo24447e(0);
        } else {
            ActivityUtils.m922a();
        }
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.tvPrivacy) {
            startActivity(WebActivity.m14288a(this, "", HvAppConfig.m13024a().mo24165a("CN".equalsIgnoreCase(getResources().getConfiguration().locale.getCountry()) ? "fs-privacy_zhCN" : "fs-privacy_zhTW")));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* renamed from: u */
    public void mo23013u() {
        if (m13709A()) {
            this.f8199d0.login(Platform.FACEBOOK, m13720d("fb#"));
        }
    }

    /* renamed from: v */
    public void mo23014v() {
        if (m13709A()) {
            if (!C4222l.m13462b(this)) {
                mo22730c(getString(R$string.un_support_google));
                return;
            }
            PermissionUtils b = PermissionUtils.m1196b("android.permission-group.CONTACTS");
            b.mo9858a(new C4283d());
            b.mo9859a();
        }
    }

    /* renamed from: w */
    public void mo23015w() {
        if (m13709A()) {
            this.f8199d0.login(Platform.LINE, m13720d("line#"));
        }
    }

    /* renamed from: x */
    public void mo24448x() {
        TickManager.m10524b(TickType.LOGIN, System.currentTimeMillis() / 1000);
        if (!m13719b(false)) {
            new HvApiMyLikeList().request();
            m13710B();
        }
    }

    /* renamed from: y */
    public void mo24449y() {
        mo22717b();
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f()) && FSManager.m10323r().mo22844n() == 0 && FSManager.m10323r().mo22840j() == 0) {
            mo22721b(FillUserInfoActivity.class);
        } else {
            mo22721b(ScanActivity.class);
        }
    }

    /* renamed from: z */
    public void mo24450z() {
        if (m13709A()) {
            this.f8199d0.login(Platform.WECHAT, m13720d("wx#"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        mo22723b(R$id.tvPrivacy);
        this.f8198c0 = (ViewPager2) findViewById(R$id.viewpager);
        this.f8198c0.setUserInputEnabled(false);
        this.f8197b0 = (CheckBox) findViewById(R$id.checkbox);
        this.f8198c0.setAdapter(new C4280a(this, this));
        this.f8198c0.setCurrentItem(0);
        if (getIntent() != null && getIntent().hasExtra("logoutMsg") && TextUtils.isEmpty(getIntent().getStringExtra("logoutMsg"))) {
            mo22707a(getString(R$string.warn), getIntent().getStringExtra("logoutMsg"), getString(R$string.confirm), true, false, (BaseDialog.C4621m) null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        if (TextUtils.isEmpty(FSManager.m10323r().mo22841k())) {
            if (TextUtils.isEmpty(MemberControl.m17125s().mo27185j())) {
                MemberControl.m17125s().mo27193r();
            } else {
                FSManager.m10323r().mo22832c("mobile");
            }
        }
        if (this.f8199d0 == null) {
            this.f8199d0 = ThirdPartyLoginFactory.create(this);
        }
        this.f8197b0.setChecked(true);
    }

    /* renamed from: b */
    private boolean m13719b(boolean z) {
        if (!BandConfig.m10064m().mo22559d()) {
            return false;
        }
        if (NetworkUtils.m858c()) {
            mo22751t();
            new HvApiGetConfig().request(new C4281b(z));
            return true;
        }
        m13718b(getString(R$string.net_err), z);
        return true;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24446a(boolean z, boolean z2) {
        if (z2) {
            m13719b(z);
        } else {
            finish();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24444a(View view) {
        this.f8197b0.toggle();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13716a(String str, MemberProfile memberProfile) {
        if (mo22741l() && memberProfile != null) {
            HvApiSocialRegister.C3652a aVar = new HvApiSocialRegister.C3652a(str + memberProfile.f9349id, memberProfile.name);
            if (!TextUtils.isEmpty(memberProfile.gender)) {
                aVar.mo22427a(memberProfile.gender);
            }
            if (!TextUtils.isEmpty(memberProfile.photoUrl)) {
                aVar.mo22429b(memberProfile.photoUrl);
            }
            if (!TextUtils.isEmpty(memberProfile.unionId)) {
                aVar.mo22430c(memberProfile.unionId);
            }
            LogUtils.m1139c("mady " + memberProfile.toString());
            mo22751t();
            new HvApiSocialRegister().request(aVar, new C4285f(str.equals("fb#") ? "fb" : str.equals("gp#") ? "google" : str.equals("line#") ? "line" : ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.login.a]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m13718b(String str, boolean z) {
        mo22708a(str, getString(R$string.retry), false, (BaseDialog.C4621m) new C4289a(this, z));
    }

    /* renamed from: a */
    public void mo24445a(EditText editText, EditText editText2) {
        if (m13709A()) {
            if (MemberControl.m17125s().mo27192q()) {
                mo24449y();
            } else if (mo22741l()) {
                if (!RegexUtils.m1235a(editText.getText()) && !C4222l.m13458a(editText.getText())) {
                    mo22730c(getString(R$string.err_mobile));
                } else if (TextUtils.isEmpty(editText2.getText())) {
                    mo22730c(getString(R$string.err_empty_code));
                } else {
                    String obj = editText.getText().toString();
                    String obj2 = editText2.getText().toString();
                    mo22751t();
                    new HvApiSmsLogin().request(obj, obj2, new C4286g());
                }
            }
        }
    }
}
