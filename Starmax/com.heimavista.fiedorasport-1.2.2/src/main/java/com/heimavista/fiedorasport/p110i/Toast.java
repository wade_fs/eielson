package com.heimavista.fiedorasport.p110i;

import android.app.AppOpsManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.view.View;

/* renamed from: com.heimavista.fiedorasport.i.b */
public class Toast {

    /* renamed from: b */
    private static int f6590b;

    /* renamed from: a */
    private Object f6591a;

    private Toast(Context context, String str, int i) {
        if (context instanceof Application) {
            f6590b = 0;
        } else {
            f6590b = m10685a(context) ^ true ? 1 : 0;
        }
        if (f6590b == 1) {
            this.f6591a = EToast2.m10676a(context, str, i);
        } else {
            this.f6591a = android.widget.Toast.makeText(context, str, i);
        }
    }

    /* renamed from: a */
    public static Toast m10683a(Context context, View view, String str, int i) {
        return new Toast(context, str, i);
    }

    /* renamed from: a */
    public static Toast m10684a(Context context, String str, int i) {
        return new Toast(context, str, i);
    }

    /* renamed from: a */
    public void mo22974a() {
        Object obj = this.f6591a;
        if (obj instanceof EToast2) {
            ((EToast2) obj).mo22971b();
        } else if (obj instanceof android.widget.Toast) {
            ((android.widget.Toast) obj).show();
        }
    }

    /* renamed from: a */
    public void mo22977a(CharSequence charSequence) {
        Object obj = this.f6591a;
        if (obj instanceof EToast2) {
            ((EToast2) obj).mo22970a(charSequence);
        } else if (obj instanceof android.widget.Toast) {
            ((android.widget.Toast) obj).setText(charSequence);
        }
    }

    /* renamed from: a */
    public void mo22975a(int i) {
        Object obj = this.f6591a;
        if (obj instanceof EToast2) {
            ((EToast2) obj).mo22968a(i);
        } else if (obj instanceof android.widget.Toast) {
            ((android.widget.Toast) obj).setDuration(i);
        }
    }

    /* renamed from: a */
    public void mo22976a(int i, int i2, int i3) {
        Object obj = this.f6591a;
        if (obj instanceof EToast2) {
            ((EToast2) obj).mo22969a(i, i2, i3);
        } else if (obj instanceof android.widget.Toast) {
            ((android.widget.Toast) obj).setGravity(i, i2, i3);
        }
    }

    /* renamed from: a */
    private static boolean m10685a(Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            String packageName = context.getApplicationContext().getPackageName();
            int i = applicationInfo.uid;
            try {
                Class<?> cls = Class.forName(AppOpsManager.class.getName());
                if (((Integer) cls.getMethod("checkOpNoThrow", Integer.TYPE, Integer.TYPE, String.class).invoke(appOpsManager, Integer.valueOf(((Integer) cls.getDeclaredField("OP_POST_NOTIFICATION").get(Integer.class)).intValue()), Integer.valueOf(i), packageName)).intValue() == 0) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
