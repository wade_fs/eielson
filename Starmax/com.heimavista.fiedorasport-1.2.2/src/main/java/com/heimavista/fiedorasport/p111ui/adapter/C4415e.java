package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.e */
/* compiled from: lambda */
public final /* synthetic */ class C4415e implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8660P;

    /* renamed from: Q */
    private final /* synthetic */ int f8661Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8662R;

    public /* synthetic */ C4415e(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8660P = listBuddyDynamicAdapter;
        this.f8661Q = i;
        this.f8662R = dVar;
    }

    public final void onClick(View view) {
        this.f8660P.mo24615a(this.f8661Q, this.f8662R, view);
    }
}
