package com.heimavista.fiedorasport.p111ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.core.app.NotificationManagerCompat;
import com.blankj.utilcode.util.CacheDiskUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.NotificationUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.api.HvApiResponse;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiGetConfig;
import com.heimavista.api.base.HvApiGetInformList;
import com.heimavista.apn.ApnManage;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.login.LoginActivity;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import org.xutils.C5217x;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.MainActivity */
public class MainActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public boolean f6657b0 = false;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.MainActivity$b */
    class C3759b implements PermissionUtils.C0922e {
        C3759b() {
        }

        /* renamed from: a */
        public void mo9866a() {
            MainActivity.this.m10780u();
        }

        /* renamed from: b */
        public void mo9867b() {
            MainActivity.this.m10780u();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.MainActivity$c */
    class C3760c implements HvApiGetInformList.C3656a {
        C3760c() {
        }

        /* renamed from: a */
        public void mo22483a(int i) {
        }

        /* renamed from: a */
        public void mo22484a(boolean z, HttpParams cVar, HvApiResponse... cVarArr) {
            C5217x.task().post(new C3788f(this, z, cVarArr));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.MainActivity.b(com.heimavista.fiedorasport.ui.activity.MainActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.MainActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.MainActivity.b(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.b(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.b(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.ui.activity.MainActivity.b(com.heimavista.fiedorasport.ui.activity.MainActivity, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.e]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
          com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public /* synthetic */ void mo23028a(boolean z, HvApiResponse[] cVarArr) {
            if (!z) {
                String string = MainActivity.this.getString(R$string.net_err_timeout);
                if (cVarArr != null) {
                    string = cVarArr[0].mo22511b();
                }
                if (string.contains("Invalid")) {
                    SPUtils.m1243c("HvApiBase").mo9873a();
                    SPUtils.m1243c("HvGad").mo9873a();
                    CacheDiskUtils.m1019c().mo9811a();
                    MainActivity.this.mo22722b((String) null);
                    return;
                }
                MainActivity mainActivity = MainActivity.this;
                mainActivity.mo22708a(string, mainActivity.getString(R$string.retry), false, (BaseDialog.C4621m) new C3787e(this));
            } else if (!MemberControl.m17125s().mo27192q()) {
                MainActivity.this.mo22721b(LoginActivity.class);
            } else if (!MainActivity.this.m10779c(true)) {
                MainActivity.this.mo22721b(HomeActivity.class);
            }
        }

        /* renamed from: a */
        public /* synthetic */ void mo23027a(boolean z) {
            if (z) {
                MainActivity.this.m10780u();
            } else {
                MainActivity.this.onBackPressed();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.MainActivity$d */
    class C3761d implements OnResultListener<ParamJsonData> {

        /* renamed from: a */
        final /* synthetic */ boolean f6661a;

        C3761d(boolean z) {
            this.f6661a = z;
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            if (this.f6661a) {
                MainActivity.this.mo22721b(HomeActivity.class);
            } else {
                MainActivity.this.mo22721b(LoginActivity.class);
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            if (TextUtils.isEmpty(str)) {
                str = MainActivity.this.getString(R$string.net_err);
            }
            MainActivity.this.m10776b(str, this.f6661a);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public void m10780u() {
        if (NetworkUtils.m858c()) {
            HvApiGetInformList.requestSingle(new C3760c());
        } else if (!MemberControl.m17125s().mo27192q()) {
            mo22721b(LoginActivity.class);
        } else if (!m10779c(true)) {
            mo22721b(HomeActivity.class);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.h]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* renamed from: v */
    private void m10781v() {
        if (!this.f6657b0) {
            if (NotificationUtils.m1185a()) {
                m10783x();
            } else {
                mo22708a(getString(R$string.notification_unenable), getString(R$string.goto_open), false, (BaseDialog.C4621m) new C3790h(this));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public void m10782w() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.LOCATION", "android.permission-group.PHONE", "android.permission-group.SMS");
        b.mo9858a(new C3759b());
        b.mo9859a();
    }

    /* access modifiers changed from: private */
    /* renamed from: x */
    public void m10783x() {
        PermissionUtils b = PermissionUtils.m1196b("android.permission-group.STORAGE");
        b.mo9858a(new C3758a());
        b.mo9857a(C3789g.f6733a);
        b.mo9859a();
    }

    /* renamed from: y */
    private void m10784y() {
        Intent intent = new Intent();
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
                intent.putExtra("android.provider.extra.CHANNEL_ID", getApplicationInfo().uid);
            } else if (Build.VERSION.SDK_INT >= 21) {
                intent.putExtra("app_package", getPackageName());
                intent.putExtra("app_uid", getApplicationInfo().uid);
            }
            startActivity(intent);
        } catch (Exception unused) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", getPackageName(), null));
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_main;
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        m10781v();
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.MainActivity$a */
    class C3758a implements PermissionUtils.C0922e {
        C3758a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.MainActivity.a(com.heimavista.fiedorasport.ui.activity.MainActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.MainActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.MainActivity.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.MainActivity.a(com.heimavista.fiedorasport.ui.activity.MainActivity, boolean):boolean */
        /* renamed from: a */
        public void mo9866a() {
            boolean unused = MainActivity.this.f6657b0 = true;
            MainActivity.this.m10782w();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.d]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
          com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: b */
        public void mo9867b() {
            MainActivity mainActivity = MainActivity.this;
            mainActivity.mo22708a(mainActivity.getString(R$string.storage_permission_denied), MainActivity.this.getString(R$string.open), false, (BaseDialog.C4621m) new C3766d(this));
        }

        /* renamed from: a */
        public /* synthetic */ void mo23026a(boolean z) {
            if (z) {
                MainActivity.this.m10783x();
            } else {
                MainActivity.this.finish();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m10779c(boolean z) {
        if (!BandConfig.m10064m().mo22559d()) {
            return false;
        }
        new HvApiGetConfig().request(new C3761d(z));
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        if (FSManager.m10323r().mo22828a()) {
            BandConfig.m10064m().mo22555a();
        }
        ApnManage.m10050a().mo22547a(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        NotificationManagerCompat.from(this).deleteNotificationChannel("channel_fs_sport");
    }

    /* renamed from: a */
    public /* synthetic */ void mo23024a(boolean z, boolean z2) {
        if (z2) {
            m10779c(z);
        } else {
            finish();
        }
    }

    /* renamed from: b */
    public /* synthetic */ void mo23025b(boolean z) {
        if (z) {
            m10784y();
        } else {
            m10783x();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
     arg types: [java.lang.String, java.lang.String, int, com.heimavista.fiedorasport.ui.activity.i]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m10776b(String str, boolean z) {
        mo22708a(str, getString(R$string.retry), false, (BaseDialog.C4621m) new C3791i(this, z));
    }
}
