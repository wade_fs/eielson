package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.b */
/* compiled from: lambda */
public final /* synthetic */ class C4412b implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8651P;

    /* renamed from: Q */
    private final /* synthetic */ int f8652Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8653R;

    public /* synthetic */ C4412b(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8651P = listBuddyDynamicAdapter;
        this.f8652Q = i;
        this.f8653R = dVar;
    }

    public final void onClick(View view) {
        this.f8651P.mo24622e(this.f8652Q, this.f8653R, view);
    }
}
