package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.content.DialogInterface;
import android.view.View;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.f */
/* compiled from: lambda */
public final /* synthetic */ class C4447f implements DialogInterface.OnDismissListener {

    /* renamed from: P */
    private final /* synthetic */ BuddyDynamicFragment f8767P;

    /* renamed from: Q */
    private final /* synthetic */ View f8768Q;

    public /* synthetic */ C4447f(BuddyDynamicFragment buddyDynamicFragment, View view) {
        this.f8767P = buddyDynamicFragment;
        this.f8768Q = view;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f8767P.mo24677a(this.f8768Q, dialogInterface);
    }
}
