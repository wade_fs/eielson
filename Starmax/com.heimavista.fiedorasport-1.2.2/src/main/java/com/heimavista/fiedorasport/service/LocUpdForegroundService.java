package com.heimavista.fiedorasport.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.widget.RemoteViews;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.sport.SportActivity;
import java.util.Locale;

public abstract class LocUpdForegroundService extends Service {

    /* renamed from: P */
    private NotificationManager f6622P;

    /* renamed from: Q */
    boolean f6623Q = false;

    /* renamed from: com.heimavista.fiedorasport.service.LocUpdForegroundService$a */
    public abstract class C3747a<T extends LocUpdForegroundService> extends Binder {
        public C3747a(LocUpdForegroundService locUpdForegroundService) {
        }

        /* renamed from: a */
        public abstract T mo23005a();
    }

    /* renamed from: g */
    private void m10700g() {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationManagerCompat from = NotificationManagerCompat.from(this);
            if (from.getNotificationChannel("channel_fs_sport") == null) {
                NotificationChannel notificationChannel = new NotificationChannel("channel_fs_sport", getString(R$string.channel_sport), 3);
                notificationChannel.setDescription(getString(R$string.channel_sport));
                notificationChannel.setShowBadge(false);
                notificationChannel.enableLights(false);
                notificationChannel.enableVibration(false);
                notificationChannel.setSound(null, null);
                from.createNotificationChannel(notificationChannel);
            }
        }
    }

    /* renamed from: h */
    private Notification m10701h() {
        String str;
        NotificationCompat.Builder when = new NotificationCompat.Builder(this, "channel_fs_sport").setSmallIcon(R$mipmap.ic_push).setPriority(1).setSound(null).setCustomContentView(new RemoteViews(getPackageName(), R$layout.layout_notification_sport)).setContentIntent(PendingIntent.getActivity(this, 1, new Intent(this, SportActivity.class), 134217728)).setWhen(System.currentTimeMillis());
        if (Build.VERSION.SDK_INT >= 26) {
            when.setChannelId("channel_fs_sport");
        }
        RemoteViews contentView = when.getContentView();
        contentView.setTextViewText(R$id.tvTime, String.format(Locale.getDefault(), "%02d:%02d:%02d", Integer.valueOf(SportActivity.f8392I0 / 3600), Integer.valueOf((SportActivity.f8392I0 / 60) % 60), Integer.valueOf(SportActivity.f8392I0 % 60)));
        int i = R$id.tvHeartRate;
        StringBuilder sb = new StringBuilder();
        int i2 = SportActivity.f8393J0;
        if (i2 == 0) {
            str = "--";
        } else {
            str = String.valueOf(i2);
        }
        sb.append(str);
        sb.append(getString(R$string.data_unit_rate));
        contentView.setTextViewText(i, sb.toString());
        contentView.setTextViewText(R$id.tvDistance, String.format(Locale.getDefault(), "%.2f", Float.valueOf(SportActivity.f8394K0 / 1000.0f)));
        return when.build();
    }

    /* renamed from: a */
    public abstract void mo22992a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22993a(Location location) {
        mo22994a("New location: " + location);
        Intent intent = new Intent("GMSLocUpdateService.broadcast");
        intent.putExtra("GMSLocUpdateService.location", location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        mo22997d();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract IBinder mo22995b();

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract void mo22996c();

    /* renamed from: d */
    public void mo22997d() {
        if (m10699a(this)) {
            this.f6622P.notify(10003, m10701h());
        }
    }

    /* renamed from: e */
    public abstract void mo22998e();

    /* renamed from: f */
    public abstract void mo22999f();

    @Nullable
    public IBinder onBind(Intent intent) {
        mo22994a("in onBind()");
        stopForeground(true);
        return mo22995b();
    }

    public void onCreate() {
        super.onCreate();
        this.f6622P = (NotificationManager) getSystemService("notification");
        mo22996c();
        m10700g();
    }

    public void onRebind(Intent intent) {
        mo22994a("in onRebind()");
        stopForeground(true);
        super.onRebind(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        mo22994a("Service started");
        return 2;
    }

    public boolean onUnbind(Intent intent) {
        mo22994a("Last client unbound from service");
        if (!this.f6623Q) {
            return true;
        }
        mo22994a("Starting foreground service");
        startForeground(10003, m10701h());
        return true;
    }

    /* renamed from: a */
    private boolean m10699a(Context context) {
        for (ActivityManager.RunningServiceInfo runningServiceInfo : ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (getClass().getName().equals(runningServiceInfo.service.getClassName()) && runningServiceInfo.foreground) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo22994a(String str) {
        LogUtils.m1139c(getClass().getSimpleName(), str);
    }
}
