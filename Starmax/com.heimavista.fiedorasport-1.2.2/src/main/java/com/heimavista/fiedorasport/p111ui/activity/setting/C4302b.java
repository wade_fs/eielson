package com.heimavista.fiedorasport.p111ui.activity.setting;

import com.heimavista.fiedorasport.p111ui.activity.setting.AlarmAdapter;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.b */
/* compiled from: lambda */
public final /* synthetic */ class C4302b implements SwitchButton.C4603d {

    /* renamed from: a */
    private final /* synthetic */ AlarmAdapter f8293a;

    /* renamed from: b */
    private final /* synthetic */ AlarmAdapter.C4292b f8294b;

    /* renamed from: c */
    private final /* synthetic */ int f8295c;

    public /* synthetic */ C4302b(AlarmAdapter alarmAdapter, AlarmAdapter.C4292b bVar, int i) {
        this.f8293a = alarmAdapter;
        this.f8294b = bVar;
        this.f8295c = i;
    }

    /* renamed from: a */
    public final void mo24507a(SwitchButton switchButton, boolean z) {
        this.f8293a.mo24465a(this.f8294b, this.f8295c, switchButton, z);
    }
}
