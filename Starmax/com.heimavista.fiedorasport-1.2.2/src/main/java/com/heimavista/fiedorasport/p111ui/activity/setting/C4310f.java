package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.DialogInterface;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.f */
/* compiled from: lambda */
public final /* synthetic */ class C4310f implements DialogInterface.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ EditWatchFaceActivity f8304P;

    /* renamed from: Q */
    private final /* synthetic */ int f8305Q;

    public /* synthetic */ C4310f(EditWatchFaceActivity editWatchFaceActivity, int i) {
        this.f8304P = editWatchFaceActivity;
        this.f8305Q = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f8304P.mo24468a(this.f8305Q, dialogInterface, i);
    }
}
