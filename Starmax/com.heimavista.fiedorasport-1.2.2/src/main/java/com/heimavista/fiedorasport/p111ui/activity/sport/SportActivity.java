package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.Polyline;
import com.amap.api.maps.model.PolylineOptions;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.C2931b;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.model.C2933d;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiPostSportData;
import com.heimavista.api.band.HvApiPostSportTrace;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.band.SportTrace;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.SportTipDialog;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.fiedorasport.p199j.p200m.GPSConverterUtils;
import com.heimavista.fiedorasport.service.LocUpdForegroundService;
import com.heimavista.fiedorasport.service.LocUpdForegroundServiceA;
import com.heimavista.fiedorasport.service.LocUpdForegroundServiceG;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.xutils.C5217x;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity */
public class SportActivity extends BaseActivity {

    /* renamed from: I0 */
    public static int f8392I0;

    /* renamed from: J0 */
    public static int f8393J0;

    /* renamed from: K0 */
    public static float f8394K0;

    /* renamed from: A0 */
    private List<Integer> f8395A0;

    /* renamed from: B0 */
    private int[] f8396B0 = {0, 0};

    /* renamed from: C0 */
    private int[] f8397C0 = {0, 0};

    /* renamed from: D0 */
    private int f8398D0 = 0;

    /* renamed from: E0 */
    private BroadcastReceiver f8399E0;
    /* access modifiers changed from: private */

    /* renamed from: F0 */
    public LocUpdForegroundService f8400F0 = null;
    /* access modifiers changed from: private */

    /* renamed from: G0 */
    public boolean f8401G0 = false;

    /* renamed from: H0 */
    private final ServiceConnection f8402H0 = new C4345a();

    /* renamed from: b0 */
    private boolean f8403b0 = FSManager.m10325t();

    /* renamed from: c0 */
    private int f8404c0 = 0;

    /* renamed from: d0 */
    private Location f8405d0;

    /* renamed from: e0 */
    private Location f8406e0;
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public CountTimer f8407f0;

    /* renamed from: g0 */
    private TextView f8408g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public TextView f8409h0;

    /* renamed from: i0 */
    private TextView f8410i0;

    /* renamed from: j0 */
    private TextView f8411j0;
    /* access modifiers changed from: private */

    /* renamed from: k0 */
    public ImageView f8412k0;
    /* access modifiers changed from: private */

    /* renamed from: l0 */
    public ImageView f8413l0;
    /* access modifiers changed from: private */

    /* renamed from: m0 */
    public ImageView f8414m0;

    /* renamed from: n0 */
    private BaseDialog f8415n0;

    /* renamed from: o0 */
    private boolean f8416o0 = false;

    /* renamed from: p0 */
    private boolean f8417p0 = false;

    /* renamed from: q0 */
    private C2880c f8418q0;

    /* renamed from: r0 */
    private C2932c f8419r0;

    /* renamed from: s0 */
    private C2933d f8420s0;

    /* renamed from: t0 */
    private List<LatLng> f8421t0;

    /* renamed from: u0 */
    private AMap f8422u0;

    /* renamed from: v0 */
    private Marker f8423v0;

    /* renamed from: w0 */
    private Polyline f8424w0;

    /* renamed from: x0 */
    private List<com.amap.api.maps.model.LatLng> f8425x0;
    /* access modifiers changed from: private */

    /* renamed from: y0 */
    public List<SportTrace> f8426y0 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: z0 */
    public SportInfo f8427z0 = new SportInfo();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$a */
    class C4345a implements ServiceConnection {
        C4345a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.sport.SportActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, com.heimavista.fiedorasport.service.LocUpdForegroundService):com.heimavista.fiedorasport.service.LocUpdForegroundService
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(double, double):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, android.location.Location):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.amap.api.maps.model.LatLng, android.graphics.Bitmap):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.google.android.gms.maps.model.LatLng, android.graphics.Bitmap):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, boolean):boolean */
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LocUpdForegroundService unused = SportActivity.this.f8400F0 = ((LocUpdForegroundService.C3747a) iBinder).mo23005a();
            boolean unused2 = SportActivity.this.f8401G0 = true;
            SportActivity.this.f8400F0.mo22998e();
            SportActivity.this.f8427z0.f6274e = Calendar.getInstance().getTimeInMillis() / 1000;
            if (SportActivity.this.f8407f0 == null) {
                SportActivity.this.m14024w();
                SportActivity.this.f8407f0.mo24568d();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.sport.SportActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, com.heimavista.fiedorasport.service.LocUpdForegroundService):com.heimavista.fiedorasport.service.LocUpdForegroundService
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(double, double):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, android.location.Location):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.amap.api.maps.model.LatLng, android.graphics.Bitmap):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.google.android.gms.maps.model.LatLng, android.graphics.Bitmap):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(boolean, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportActivity, boolean):boolean */
        public void onServiceDisconnected(ComponentName componentName) {
            LocUpdForegroundService unused = SportActivity.this.f8400F0 = (LocUpdForegroundService) null;
            boolean unused2 = SportActivity.this.f8401G0 = false;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$b */
    class C4346b extends CountTimer {
        C4346b(long j) {
            super(j);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.h.a0.a(boolean, int):void
         arg types: [int, int]
         candidates:
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, int):int
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, long):long
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, com.sxr.sdk.ble.keepfit.aidl.a):com.sxr.sdk.ble.keepfit.aidl.a
          com.heimavista.fiedorasport.h.a0.a(long, int):void
          com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean
          com.heimavista.fiedorasport.h.a0.a(int, int):void
          com.heimavista.fiedorasport.h.a0.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
          com.heimavista.fiedorasport.h.a0.a(java.lang.String, java.lang.String):void
          com.heimavista.fiedorasport.h.a0.a(boolean, int):void */
        /* access modifiers changed from: protected */
        /* renamed from: e */
        public void mo24537e(long j) {
            super.mo24537e(j);
            SportActivity.f8392I0 = (int) (j / 1000);
            int i = SportActivity.f8392I0;
            if (i % 30 == 0) {
                if (i % 60 == 0) {
                    if (FSManager.m10323r().mo22847q()) {
                        JYSDKManager.m10388i().mo22885a(false, 0);
                        JYSDKManager.m10388i().mo22885a(true, 60);
                    }
                    if (FSManager.m10323r().mo22846p()) {
                        CRPBleManager.m10641l();
                    }
                }
                if (FSManager.m10323r().mo22847q()) {
                    JYSDKManager.m10388i().mo22888b();
                }
                if (FSManager.m10323r().mo22846p()) {
                    CRPBleManager.m10650u();
                }
            }
            SportActivity.this.f8409h0.setText(String.format(Locale.getDefault(), "%02d:%02d:%02d", Integer.valueOf(SportActivity.f8392I0 / 3600), Integer.valueOf((SportActivity.f8392I0 / 60) % 60), Integer.valueOf(SportActivity.f8392I0 % 60)));
            SportActivity.this.f8427z0.f6276g = SportActivity.f8392I0;
            if (SportActivity.this.f8400F0 != null) {
                SportActivity.this.f8400F0.mo22997d();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$c */
    class C4347c extends BroadcastReceiver {
        C4347c() {
        }

        public void onReceive(Context context, Intent intent) {
            Location location = (Location) intent.getParcelableExtra("GMSLocUpdateService.location");
            if (location != null) {
                SportActivity.this.m14008a(location);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$d */
    class C4348d implements OnResultListener<ParamJsonData> {

        /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$d$a */
        class C4349a implements OnResultListener<ParamJsonData> {
            C4349a() {
            }

            /* renamed from: a */
            public void mo22380a(ParamJsonData fVar) {
                SportActivity.this.m14026y();
            }

            /* renamed from: a */
            public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
                SportActivity.this.mo22730c(str);
                SportActivity.this.m14026y();
            }
        }

        C4348d() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            String a = fVar.mo25325a("sportId", "");
            if (!TextUtils.isEmpty(a)) {
                SportActivity.this.f8427z0.f6272c = a;
                SportDataDb.m13191a(SportActivity.this.f8427z0);
                if (SportActivity.this.f8426y0 == null || SportActivity.this.f8426y0.isEmpty()) {
                    SportActivity.this.m14026y();
                } else {
                    new HvApiPostSportTrace(a, SportActivity.this.f8426y0, new C4349a()).request();
                }
            } else {
                SportActivity.this.m14026y();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            SportActivity.this.mo22717b();
            SportActivity.this.mo22730c(str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$e */
    class C4350e implements Animation.AnimationListener {
        C4350e() {
        }

        public void onAnimationEnd(Animation animation) {
            SportActivity.this.f8412k0.setVisibility(0);
            SportActivity.this.f8414m0.setVisibility(0);
            SportActivity.this.f8413l0.setVisibility(8);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportActivity$f */
    class C4351f implements Animation.AnimationListener {
        C4351f() {
        }

        public void onAnimationEnd(Animation animation) {
            SportActivity.this.f8412k0.setVisibility(8);
            SportActivity.this.f8414m0.setVisibility(8);
            SportActivity.this.f8413l0.setVisibility(0);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: A */
    private void m14000A() {
        this.f8399E0 = new C4347c();
        LocalBroadcastManager.getInstance(this).registerReceiver(this.f8399E0, new IntentFilter("GMSLocUpdateService.broadcast"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.h.a0.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, int):int
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, long):long
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, com.sxr.sdk.ble.keepfit.aidl.a):com.sxr.sdk.ble.keepfit.aidl.a
      com.heimavista.fiedorasport.h.a0.a(long, int):void
      com.heimavista.fiedorasport.h.a0.a(com.heimavista.fiedorasport.h.a0, boolean):boolean
      com.heimavista.fiedorasport.h.a0.a(int, int):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, com.heimavista.fiedorasport.h.y):void
      com.heimavista.fiedorasport.h.a0.a(java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.h.a0.a(boolean, int):void */
    /* renamed from: B */
    private void m14001B() {
        NotificationManagerCompat.from(this).cancel(10003);
        m14022u();
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22885a(false, 0);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10648s();
        }
        f8392I0 = 0;
        this.f8416o0 = false;
    }

    /* renamed from: C */
    private void m14002C() {
        CountTimer oVar = this.f8407f0;
        if (oVar != null) {
            oVar.mo24566c();
        }
    }

    /* renamed from: D */
    private void m14003D() {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22888b();
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10650u();
        }
        this.f8427z0.f6276g = f8392I0;
        String string = getString(R$string.stop_tips);
        SportInfo dVar = this.f8427z0;
        boolean z = dVar.f6277h >= 10 && dVar.f6276g >= 180;
        if (!z) {
            string = getString(R$string.sport_distance_too_short);
        }
        SportTipDialog iVar = new SportTipDialog(this);
        iVar.mo22864d(string);
        iVar.mo22863a(new C4359d(this, z));
        this.f8415n0 = iVar.mo25418b();
        this.f8415n0.show();
    }

    /* renamed from: E */
    private void m14004E() {
        float f = f8394K0;
        if (f > 0.0f && this.f8416o0) {
            float f2 = f / 1000.0f;
            this.f8408g0.setText(String.format(Locale.getDefault(), "%.2f", Float.valueOf(f2)));
            int i = f8392I0;
            this.f8410i0.setText(String.format(Locale.getDefault(), "%d'%d\"", Integer.valueOf((int) ((((float) i) / f2) / 60.0f)), Integer.valueOf((int) ((((float) i) / f2) % 60.0f))));
        }
    }

    /* renamed from: u */
    private void m14022u() {
        CountTimer oVar = this.f8407f0;
        if (oVar != null) {
            oVar.mo24562a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* renamed from: v */
    private void m14023v() {
        MapFragment a = MapFragment.m14638a(this.f8403b0, false);
        if (this.f8403b0) {
            this.f8421t0 = new ArrayList();
            a.mo24729a(new C4362g(this));
        } else {
            this.f8425x0 = new ArrayList();
            a.mo24729a(new C4361f(this));
        }
        getSupportFragmentManager().beginTransaction().replace(R$id.mapFragment, a, "mapFragment").commit();
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public void m14024w() {
        if (this.f8407f0 == null) {
            this.f8407f0 = new C4346b(1000);
        }
    }

    /* renamed from: x */
    private void m14025x() {
        CountTimer oVar = this.f8407f0;
        if (oVar != null) {
            oVar.mo24564b();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: y */
    public void m14026y() {
        mo22717b();
        finish();
    }

    /* renamed from: z */
    private void m14027z() {
        SportInfo dVar = this.f8427z0;
        dVar.f6285p = this.f8395A0;
        dVar.f6275f = Calendar.getInstance().getTimeInMillis() / 1000;
        this.f8427z0.f6276g = f8392I0;
        mo22712a(false);
        SportInfo dVar2 = this.f8427z0;
        int[] iArr = this.f8396B0;
        dVar2.f6278i = iArr[1] - iArr[0];
        new HvApiPostSportData(dVar2, new C4348d()).request();
    }

    public void onBackPressed() {
        BaseDialog baseDialog = this.f8415n0;
        if (baseDialog == null || !baseDialog.isShowing()) {
            m14003D();
        }
    }

    public void onClick(View view) {
        Location location;
        int id = view.getId();
        if (id == R$id.ivPause) {
            if (this.f8416o0) {
                m14025x();
                m14012b(true);
                this.f8416o0 = false;
            }
        } else if (id == R$id.ivStart) {
            this.f8398D0 = 0;
            m14012b(false);
            this.f8416o0 = true;
            m14002C();
        } else if (id == R$id.ivStop) {
            SnackbarUtils a = SnackbarUtils.m927a(view);
            a.mo9783a(getString(R$string.long_click_to_stop));
            a.mo9782a(-1);
            a.mo9786a();
        } else if (id == R$id.ivBack) {
            BaseDialog baseDialog = this.f8415n0;
            if (baseDialog == null || !baseDialog.isShowing()) {
                m14003D();
            }
        } else if (id == R$id.ivPos && (location = this.f8406e0) != null) {
            m14007a(location.getLatitude(), this.f8406e0.getLongitude());
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocUpdForegroundService locUpdForegroundService = this.f8400F0;
        if (locUpdForegroundService != null) {
            locUpdForegroundService.mo22999f();
            this.f8400F0.mo22992a();
        }
        if (this.f8399E0 != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.f8399E0);
        }
        m14001B();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Intent intent;
        super.onStart();
        if (this.f8403b0) {
            intent = new Intent(this, LocUpdForegroundServiceG.class);
        } else {
            intent = new Intent(this, LocUpdForegroundServiceA.class);
        }
        bindService(intent, this.f8402H0, 1);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.f8401G0) {
            unbindService(this.f8402H0);
            this.f8401G0 = false;
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8408g0 = (TextView) findViewById(R$id.tvDistance);
        this.f8409h0 = (TextView) findViewById(R$id.tvSportTime);
        this.f8410i0 = (TextView) findViewById(R$id.tvSportSpeed);
        this.f8411j0 = (TextView) findViewById(R$id.tvSportHeartRate);
        this.f8412k0 = (ImageView) findViewById(R$id.ivStart);
        this.f8413l0 = (ImageView) findViewById(R$id.ivPause);
        this.f8414m0 = (ImageView) findViewById(R$id.ivStop);
        mo22714a(this.f8413l0, this.f8412k0, this.f8414m0);
        mo22723b(R$id.ivPos);
        this.f8414m0.setOnLongClickListener(new C4360e(this));
    }

    /* renamed from: e */
    public /* synthetic */ void mo24534e(int i) {
        SpanUtils a = SpanUtils.m865a(this.f8411j0);
        a.mo9735a(String.valueOf(i));
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a.mo9735a(getString(R$string.data_unit_rate));
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_13));
        a.mo9736b();
        f8393J0 = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_sport;
    }

    /* renamed from: f */
    private void m14016f(int i) {
        if (this.f8395A0 == null) {
            this.f8395A0 = new ArrayList();
        }
        this.f8395A0.add(Integer.valueOf(i));
        SportInfo dVar = this.f8427z0;
        int i2 = dVar.f6280k;
        if (i2 == 0) {
            dVar.f6280k = i;
        } else {
            dVar.f6280k = Math.min(i, i2);
        }
        SportInfo dVar2 = this.f8427z0;
        int i3 = dVar2.f6281l;
        if (i3 == 0) {
            dVar2.f6281l = i;
        } else {
            dVar2.f6281l = Math.max(i, i3);
        }
        SportInfo dVar3 = this.f8427z0;
        int i4 = dVar3.f6279j;
        if (i4 == 0) {
            dVar3.f6279j = i;
        } else {
            dVar3.f6279j = (i4 + i) >> 1;
        }
        if (i > 0) {
            C5217x.task().post(new C4357b(this, i));
        }
    }

    /* renamed from: a */
    public /* synthetic */ boolean mo24533a(View view) {
        BaseDialog baseDialog = this.f8415n0;
        if (baseDialog != null && baseDialog.isShowing()) {
            return false;
        }
        m14003D();
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8404c0 = getIntent().getIntExtra("type", 0);
        this.f8427z0.f6270a = MemberControl.m17125s().mo27187l();
        this.f8427z0.f6271b = TimeUtil.m13442a();
        this.f8427z0.f6273d = this.f8404c0;
        this.f8410i0.setText("--'--\"");
        this.f8409h0.setText(String.format(Locale.getDefault(), "%02d:%02d:%02d", Integer.valueOf(f8392I0 / 3600), Integer.valueOf((f8392I0 / 60) % 60), Integer.valueOf(f8392I0 % 60)));
        SpanUtils a = SpanUtils.m865a(this.f8411j0);
        a.mo9735a("--");
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_17));
        a.mo9735a(getString(R$string.data_unit_rate));
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_13));
        a.mo9736b();
        this.f8405d0 = (Location) getIntent().getParcelableExtra("location");
        this.f8406e0 = this.f8405d0;
        m14023v();
        m14000A();
        if (this.f8405d0 != null) {
            double parseDouble = Double.parseDouble(String.format(Locale.getDefault(), "%.6f", Double.valueOf(this.f8405d0.getLatitude())));
            double parseDouble2 = Double.parseDouble(String.format(Locale.getDefault(), "%.6f", Double.valueOf(this.f8405d0.getLongitude())));
            this.f8405d0.setLatitude(parseDouble);
            this.f8405d0.setLongitude(parseDouble2);
            this.f8427z0.f6274e = Calendar.getInstance().getTimeInMillis() / 1000;
            this.f8416o0 = true;
        }
    }

    /* renamed from: b */
    private void m14012b(boolean z) {
        float f = 1.0f;
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, z ? 1.0f : 0.0f, 1, z ? 0.0f : 1.0f);
        float f2 = z ? 1.0f : 0.0f;
        if (z) {
            f = 0.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f);
        alphaAnimation.setDuration(400);
        translateAnimation.setDuration(400);
        if (z) {
            alphaAnimation.setAnimationListener(new C4350e());
            this.f8413l0.startAnimation(alphaAnimation);
            this.f8412k0.startAnimation(translateAnimation);
            this.f8414m0.startAnimation(translateAnimation);
            return;
        }
        alphaAnimation.setAnimationListener(new C4351f());
        this.f8412k0.startAnimation(translateAnimation);
        this.f8414m0.startAnimation(translateAnimation);
        this.f8413l0.startAnimation(alphaAnimation);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24530a(C2880c cVar) {
        this.f8418q0 = cVar;
        Location location = this.f8405d0;
        if (location != null) {
            m14007a(location.getLatitude(), this.f8405d0.getLongitude());
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24528a(AMap aMap) {
        this.f8422u0 = aMap;
        Location location = this.f8405d0;
        if (location != null) {
            m14007a(location.getLatitude(), this.f8405d0.getLongitude());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m14008a(Location location) {
        if (location != null) {
            double parseDouble = Double.parseDouble(String.format(Locale.getDefault(), "%.6f", Double.valueOf(location.getLatitude())));
            double parseDouble2 = Double.parseDouble(String.format(Locale.getDefault(), "%.6f", Double.valueOf(location.getLongitude())));
            location.setLatitude(parseDouble);
            location.setLongitude(parseDouble2);
            this.f8406e0 = location;
            LogUtils.m1139c("loc>>>" + parseDouble + "," + parseDouble2 + " " + location.getSpeed());
            m14007a(parseDouble, parseDouble2);
            if (this.f8403b0) {
                if (this.f8421t0 == null) {
                    this.f8421t0 = new ArrayList();
                }
            } else if (this.f8425x0 == null) {
                this.f8425x0 = new ArrayList();
            }
            if (location.getSpeed() <= 0.0f) {
                this.f8398D0++;
            } else if (this.f8398D0 > 0 && !this.f8416o0) {
                m14012b(false);
                this.f8416o0 = true;
                m14002C();
                this.f8398D0 = 0;
            }
            if (this.f8398D0 < (this.f8403b0 ? 3 : 5)) {
                this.f8426y0.add(new SportTrace(this.f8404c0, this.f8416o0, location));
                int size = this.f8426y0.size();
                Location location2 = null;
                if (size == 2) {
                    Location a = this.f8426y0.get(0).mo22569a();
                    if (location.getTime() - a.getTime() <= 10000) {
                        if (this.f8403b0) {
                            this.f8421t0.add(new LatLng(a.getLatitude(), a.getLongitude()));
                        } else {
                            this.f8425x0.add(new com.amap.api.maps.model.LatLng(a.getLatitude(), a.getLongitude()));
                        }
                        location2 = a;
                    } else {
                        this.f8426y0.get(0).mo22570a(false);
                    }
                } else if (size > 2) {
                    location2 = this.f8426y0.get(size - 2).mo22569a();
                    if (this.f8403b0) {
                        this.f8421t0.add(new LatLng(parseDouble, parseDouble2));
                    } else {
                        this.f8425x0.add(new com.amap.api.maps.model.LatLng(parseDouble, parseDouble2));
                    }
                }
                if (location2 != null && this.f8416o0) {
                    f8394K0 += GPSConverterUtils.m13467a(parseDouble, parseDouble2, location2.getLatitude(), location2.getLongitude());
                    this.f8427z0.f6277h = (int) f8394K0;
                    m14004E();
                }
                if (this.f8403b0) {
                    if (this.f8418q0 != null) {
                        C2933d dVar = this.f8420s0;
                        if (dVar != null) {
                            dVar.mo18632a();
                        }
                        this.f8420s0 = this.f8418q0.mo18400a(new PolylineOptions().mo18578c(this.f8421t0).mo18580d(Color.parseColor("#f70000")).mo18576a(true).mo18574a(10.0f).mo18581e(2));
                    }
                } else if (this.f8422u0 != null) {
                    Polyline polyline = this.f8424w0;
                    if (polyline != null) {
                        polyline.remove();
                    }
                    this.f8424w0 = this.f8422u0.addPolyline(new com.amap.api.maps.model.PolylineOptions().addAll(this.f8425x0).color(Color.parseColor("#f70000")).geodesic(true).width(10.0f).lineJoinType(PolylineOptions.LineJoinType.LineJoinRound));
                }
            } else if (this.f8416o0) {
                m14025x();
                m14012b(true);
                this.f8416o0 = false;
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24532a(boolean z, boolean z2) {
        if (!z2) {
            return;
        }
        if (!z) {
            finish();
        } else {
            m14027z();
        }
    }

    /* renamed from: a */
    private void m14007a(double d, double d2) {
        if (this.f8403b0) {
            if (this.f8418q0 != null) {
                LatLng latLng = new LatLng(d, d2);
                this.f8418q0.mo18402a(C2879b.m8106a(latLng));
                C2932c cVar = this.f8419r0;
                if (cVar != null) {
                    cVar.mo18627a(latLng);
                } else if (!this.f8417p0) {
                    this.f8417p0 = true;
                    AvatarUtils.m13392a(this, MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4358c(this, latLng));
                }
            }
        } else if (this.f8422u0 != null) {
            com.amap.api.maps.model.LatLng latLng2 = new com.amap.api.maps.model.LatLng(d, d2);
            this.f8422u0.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
            Marker marker = this.f8423v0;
            if (marker != null) {
                marker.setPosition(latLng2);
            } else if (!this.f8417p0) {
                this.f8417p0 = true;
                AvatarUtils.m13392a(this, MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4356a(this, latLng2));
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24531a(LatLng latLng, Bitmap bitmap) {
        this.f8419r0 = this.f8418q0.mo18399a(new MarkerOptions().mo18545a(latLng).mo18546a(C2931b.m8343a(bitmap)));
        this.f8417p0 = false;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24529a(com.amap.api.maps.model.LatLng latLng, Bitmap bitmap) {
        this.f8423v0 = this.f8422u0.addMarker(new com.amap.api.maps.model.MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
        this.f8417p0 = false;
    }

    /* renamed from: a */
    public void mo22690a(long j, int i, int i2, int i3, int i4) {
        int[] iArr = this.f8396B0;
        if (iArr[0] != 0) {
            int[] iArr2 = this.f8397C0;
            if (iArr2[0] != 0) {
                iArr[1] = i3;
                iArr2[1] = i2;
                return;
            }
        }
        this.f8396B0[0] = i3;
        this.f8397C0[0] = i2;
    }

    /* renamed from: a */
    public void mo22688a(long j, int i, int i2) {
        m14016f(i);
    }

    /* renamed from: a */
    public void mo22687a(long j, int i) {
        mo22688a(System.currentTimeMillis() / 1000, i, 0);
    }

    /* renamed from: a */
    public void mo22684a(int i, String str, int i2, int i3) {
        super.mo22684a(i, str, i2, i3);
    }
}
