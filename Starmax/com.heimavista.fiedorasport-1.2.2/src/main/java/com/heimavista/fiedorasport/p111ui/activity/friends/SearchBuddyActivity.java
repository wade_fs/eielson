package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiReqBuddyById;
import com.heimavista.api.buddy.HvApiReqBuddyByNbr;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SearchBuddyActivity */
public class SearchBuddyActivity extends BaseActivity {

    /* renamed from: b0 */
    private String f8095b0 = "";
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public View f8096c0;

    /* renamed from: d0 */
    private EditText f8097d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public ImageView f8098e0;
    /* access modifiers changed from: private */

    /* renamed from: f0 */
    public TextView f8099f0;
    /* access modifiers changed from: private */

    /* renamed from: g0 */
    public Button f8100g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public UserInfoDb f8101h0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SearchBuddyActivity$a */
    class C4232a implements TextWatcher {
        C4232a() {
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (SearchBuddyActivity.this.f8096c0.getVisibility() == 0) {
                SearchBuddyActivity.this.f8096c0.setVisibility(8);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SearchBuddyActivity$b */
    class C4233b implements OnResultListener<ParamJsonData> {
        C4233b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
         arg types: [android.widget.ImageView, java.lang.String, int, int]
         candidates:
          com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
          com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
          com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
          com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            BuddyListDb l;
            SearchBuddyActivity.this.mo22717b();
            try {
                UserInfoDb unused = SearchBuddyActivity.this.f8101h0 = UserInfoDb.m13343c(new JSONObject(fVar.mo25325a("UserInfo", "{}")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (SearchBuddyActivity.this.f8101h0 != null && (l = BuddyListDb.m13250l(SearchBuddyActivity.this.f8101h0.mo24240q())) != null) {
                if (l.mo24312y() == 9) {
                    SearchBuddyActivity searchBuddyActivity = SearchBuddyActivity.this;
                    searchBuddyActivity.mo22730c(searchBuddyActivity.getString(R$string.has_build));
                    return;
                }
                SearchBuddyActivity.this.f8096c0.setVisibility(0);
                SearchBuddyActivity.this.f8099f0.setText(SearchBuddyActivity.this.f8101h0.mo24338x());
                if (l.mo24312y() == 1) {
                    SearchBuddyActivity.this.f8100g0.setText(R$string.invite_tips);
                    SearchBuddyActivity.this.f8100g0.setEnabled(false);
                } else if (l.mo24312y() == 2) {
                    SearchBuddyActivity.this.f8100g0.setText(R$string.invite_un_confirm);
                    SearchBuddyActivity.this.f8100g0.setEnabled(false);
                } else {
                    SearchBuddyActivity.this.f8100g0.setText(R$string.invite);
                    SearchBuddyActivity.this.f8100g0.setEnabled(true);
                }
                if (TextUtils.isEmpty(SearchBuddyActivity.this.f8101h0.mo24336v())) {
                    SearchBuddyActivity.this.f8098e0.setImageResource(R$mipmap.login_user);
                } else {
                    AvatarUtils.m13393a(SearchBuddyActivity.this.f8098e0, SearchBuddyActivity.this.f8101h0.mo24336v(), R$mipmap.login_user, true);
                }
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            SearchBuddyActivity.this.mo22717b();
            SearchBuddyActivity.this.mo22730c(str);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.SearchBuddyActivity$c */
    class C4234c implements OnResultListener<ParamJsonData> {
        C4234c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, com.heimavista.fiedorasport.ui.activity.friends.s]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(android.view.Menu, int, java.lang.String, int, android.graphics.drawable.Drawable, android.view.View$OnClickListener):android.view.MenuItem
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            SearchBuddyActivity.this.mo22717b();
            SearchBuddyActivity searchBuddyActivity = SearchBuddyActivity.this;
            searchBuddyActivity.mo22707a("", searchBuddyActivity.getString(R$string.invite_tips), SearchBuddyActivity.this.getString(R$string.f6600ok), true, false, (BaseDialog.C4621m) new C4255s(this));
        }

        /* renamed from: a */
        public /* synthetic */ void mo24404a(boolean z) {
            if (z) {
                SearchBuddyActivity.this.finish();
            }
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            SearchBuddyActivity.this.mo22717b();
            SearchBuddyActivity.this.mo22730c(str);
        }
    }

    /* renamed from: u */
    private boolean m13575u() {
        Constant.m10311a("用戶ID搜尋");
        this.f8095b0 = "";
        this.f8101h0 = null;
        if (TextUtils.isEmpty(this.f8097d0.getText())) {
            mo22730c(getString(R$string.empty_data_input));
            return true;
        }
        this.f8095b0 = this.f8097d0.getText().toString();
        if (this.f8095b0.equals(FSManager.m10323r().mo22843m())) {
            mo22730c(getString(R$string.is_yours));
            return true;
        }
        mo22712a(false);
        new HvApiReqBuddyById().request(this.f8095b0, new C4233b());
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_search_buddy;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.add_friends_by_userid);
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.btnInvite) {
            if (this.f8101h0 != null) {
                Constant.m10311a("ID搜尋添加好友");
                m13573d(this.f8101h0.mo24240q());
            }
        } else if (view.getId() == R$id.ivSearch) {
            m13575u();
        }
    }

    /* renamed from: d */
    private void m13573d(String str) {
        if (mo22741l()) {
            mo22751t();
            new HvApiReqBuddyByNbr().request(str, new C4234c());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8096c0 = findViewById(R$id.layoutUserInfo);
        this.f8098e0 = (ImageView) findViewById(R$id.ivAvatar);
        this.f8099f0 = (TextView) findViewById(R$id.tvUserName);
        this.f8100g0 = (Button) findViewById(R$id.btnInvite);
        this.f8096c0.setVisibility(8);
        this.f8097d0 = (EditText) findViewById(16908291);
        this.f8097d0.setImeOptions(3);
        this.f8097d0.addTextChangedListener(new C4232a());
        this.f8097d0.setOnEditorActionListener(new C4254r(this));
        mo22723b(R$id.btnInvite, R$id.ivSearch);
    }

    /* renamed from: a */
    public /* synthetic */ boolean mo24398a(TextView textView, int i, KeyEvent keyEvent) {
        if (i == 3) {
            return m13575u();
        }
        return false;
    }
}
