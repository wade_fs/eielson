package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.C4222l;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportAdapter */
public class SportAdapter extends BaseRecyclerViewAdapter<SportInfo, C4352a> {

    /* renamed from: i */
    private LayoutInflater f8435i;

    /* renamed from: j */
    private SimpleDateFormat f8436j = new SimpleDateFormat("HH:mm", Locale.getDefault());

    /* renamed from: k */
    private SimpleDateFormat f8437k = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportAdapter$a */
    class C4352a extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        TextView f8438R = ((TextView) findViewById(R$id.tvDate));

        /* renamed from: S */
        TextView f8439S = ((TextView) findViewById(R$id.tvSportStep));

        /* renamed from: T */
        TextView f8440T = ((TextView) findViewById(R$id.tvSportDistance));

        /* renamed from: U */
        TextView f8441U = ((TextView) findViewById(R$id.tvSportDuration));

        /* renamed from: V */
        TextView f8442V = ((TextView) findViewById(R$id.tvSportAvgHeartRate));

        /* renamed from: W */
        TextView f8443W = ((TextView) findViewById(R$id.tvSportLikes));

        /* renamed from: X */
        View f8444X = findViewById(R$id.divider);

        C4352a(SportAdapter sportAdapter, View view) {
            super(view);
        }
    }

    public SportAdapter(Context context) {
        super(context);
        this.f8435i = LayoutInflater.from(context);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4352a aVar, int i) {
        String str;
        String str2;
        String str3;
        SportInfo dVar = (SportInfo) getItem(i);
        Date date = new Date(dVar.f6274e * 1000);
        if (TimeUtils.m1014c(date)) {
            str = mo22781a(R$string.today) + " " + this.f8436j.format(date);
        } else {
            str = this.f8437k.format(date);
        }
        aVar.f8438R.setText(str);
        String str4 = "--";
        if (dVar.f6273d != 2) {
            int a = C4222l.m13451a(dVar.f6277h);
            SpanUtils a2 = SpanUtils.m865a(aVar.f8439S);
            if (a == 0) {
                str3 = str4;
            } else {
                str3 = String.valueOf(a);
            }
            a2.mo9735a(str3);
            a2.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
            a2.mo9735a(mo22781a(R$string.data_unit_step));
            a2.mo9736b();
        }
        SpanUtils a3 = SpanUtils.m865a(aVar.f8440T);
        if (dVar.f6277h == 0) {
            str2 = str4;
        } else {
            str2 = String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) dVar.f6277h) / 1000.0f));
        }
        a3.mo9735a(str2);
        a3.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
        a3.mo9735a(mo22781a(R$string.data_unit_mill));
        a3.mo9736b();
        int i2 = dVar.f6276g;
        SpanUtils a4 = SpanUtils.m865a(aVar.f8441U);
        int i3 = i2 / 3600;
        boolean z = i3 > 0;
        int i4 = (i2 / 60) % 60;
        boolean z2 = i4 > 0;
        if (z) {
            a4.mo9735a(String.valueOf(i3));
            a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
            a4.mo9735a(mo22781a(R$string.data_unit_hour));
            if (z2) {
                a4.mo9735a(String.valueOf(i4));
                a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a4.mo9735a(mo22781a(R$string.data_unit_min));
            } else {
                a4.mo9735a(String.valueOf(i2 % 60));
                a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
                a4.mo9735a(mo22781a(R$string.data_unit_second));
            }
        } else {
            a4.mo9735a(String.valueOf(i4));
            a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
            a4.mo9735a(mo22781a(R$string.data_unit_min));
            a4.mo9735a(String.valueOf(i2 % 60));
            a4.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
            a4.mo9735a(mo22781a(R$string.data_unit_second));
        }
        a4.mo9736b();
        int i5 = dVar.f6279j;
        if (i5 != 0) {
            str4 = String.valueOf(i5);
        }
        SpanUtils a5 = SpanUtils.m865a(aVar.f8442V);
        a5.mo9735a(str4);
        a5.mo9733a(mo22796e().getDimensionPixelSize(R$dimen.font_size_15));
        a5.mo9735a(mo22781a(R$string.data_unit_rate));
        a5.mo9736b();
        int i6 = 8;
        aVar.f8444X.setVisibility(dVar.f6282m == 0 ? 8 : 0);
        TextView textView = aVar.f8443W;
        if (dVar.f6282m != 0) {
            i6 = 0;
        }
        textView.setVisibility(i6);
        aVar.f8443W.setText(mo22788b().getString(R$string.sport_likes, Integer.valueOf(dVar.f6282m)));
    }

    public int getItemViewType(int i) {
        return ((SportInfo) getItem(i)).f6273d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4352a onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == 2) {
            return new C4352a(this, this.f8435i.inflate(R$layout.layout_item_sport_cycling, viewGroup, false));
        }
        return new C4352a(this, this.f8435i.inflate(R$layout.layout_item_sport, viewGroup, false));
    }
}
