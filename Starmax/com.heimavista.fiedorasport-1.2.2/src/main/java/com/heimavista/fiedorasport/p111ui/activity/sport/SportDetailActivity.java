package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.Guideline;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.LatLngBounds;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.google.android.gms.maps.C2879b;
import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiGetSportTrace;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.band.SportTraceInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.utils.ParamJsonData;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p193d.p194i.SportTraceDataDb;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p218g.MemberControl;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity */
public class SportDetailActivity extends BaseActivity {

    /* renamed from: b0 */
    private ImageView f8456b0;

    /* renamed from: c0 */
    private TextView f8457c0;

    /* renamed from: d0 */
    private TextView f8458d0;

    /* renamed from: e0 */
    private TextView f8459e0;

    /* renamed from: f0 */
    private TextView f8460f0;

    /* renamed from: g0 */
    private TextView f8461g0;

    /* renamed from: h0 */
    private TextView f8462h0;

    /* renamed from: i0 */
    private TextView f8463i0;

    /* renamed from: j0 */
    private TextView f8464j0;

    /* renamed from: k0 */
    private Guideline f8465k0;

    /* renamed from: l0 */
    private Guideline f8466l0;

    /* renamed from: m0 */
    private AMap f8467m0;

    /* renamed from: n0 */
    private C2880c f8468n0;

    /* renamed from: o0 */
    private LatLngBounds.C2927a f8469o0 = null;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity$a */
    class C4354a implements OnResultListener<ParamJsonData> {
        C4354a() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, int]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, boolean):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            SportDetailActivity.this.m14078b(false);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity$b */
    class C4355b implements OnResultListener<ParamJsonData> {
        C4355b() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, boolean):void
         arg types: [com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, int]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity.a(com.heimavista.fiedorasport.ui.activity.sport.SportDetailActivity, boolean):void */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            SportDetailActivity.this.m14078b(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* renamed from: v */
    private void m14079v() {
        boolean t = FSManager.m10325t();
        MapFragment a = MapFragment.m14638a(t, false);
        if (t) {
            a.mo24729a(new C4368m(this));
        } else {
            a.mo24729a(new C4367l(this));
        }
        getSupportFragmentManager().beginTransaction().replace(R$id.mapFragment, a, "mapFragment").commit();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8456b0 = (ImageView) findViewById(R$id.ivAvatar);
        this.f8457c0 = (TextView) findViewById(R$id.tvUserName);
        this.f8458d0 = (TextView) findViewById(R$id.tvSportTime);
        this.f8459e0 = (TextView) findViewById(R$id.tvSportStep);
        this.f8460f0 = (TextView) findViewById(R$id.tvSportDistance);
        this.f8461g0 = (TextView) findViewById(R$id.tvSportAvgHeartRate);
        this.f8462h0 = (TextView) findViewById(R$id.tvSportDuration);
        this.f8463i0 = (TextView) findViewById(R$id.tvSportSpeed);
        this.f8464j0 = (TextView) findViewById(R$id.tvSportCal);
        this.f8465k0 = (Guideline) findViewById(R$id.gl1);
        this.f8466l0 = (Guideline) findViewById(R$id.gl2);
        m14079v();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_sport_detail;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getIntent().getStringExtra("title");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        C2880c cVar = this.f8468n0;
        if (cVar != null) {
            cVar.mo18401a();
        }
        AMap aMap = this.f8467m0;
        if (aMap != null) {
            aMap.clear();
        }
        super.onDestroy();
    }

    /* renamed from: u */
    public /* synthetic */ void mo23013u() {
        try {
            this.f8468n0.mo18402a(C2879b.m8108a(this.f8469o0.mo18538a(), ConvertUtils.m1055a(20.0f)));
        } catch (Exception e) {
            LogUtils.m1131b("android " + e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        String str;
        String stringExtra = getIntent().getStringExtra("sportId");
        String stringExtra2 = getIntent().getStringExtra("userNbr");
        SportInfo b = new SportDataDb().mo24268b(stringExtra, stringExtra2);
        UserInfoDb.m13339a(this, stringExtra2, this.f8457c0, this.f8456b0);
        if (b != null) {
            this.f8458d0.setText(TimeUtils.m998a(b.f6274e * 1000, "yyyy年MM月dd日 HH:mm"));
            String str2 = "--";
            if (b.f6273d == 2) {
                this.f8465k0.setGuidelinePercent(0.0f);
                this.f8466l0.setGuidelinePercent(0.5f);
                findViewById(R$id.tvText).setVisibility(4);
            } else {
                this.f8465k0.setGuidelinePercent(0.33333334f);
                this.f8466l0.setGuidelinePercent(0.6666667f);
                int a = C4222l.m13451a(b.f6277h);
                SpanUtils a2 = SpanUtils.m865a(this.f8459e0);
                if (a == 0) {
                    str = str2;
                } else {
                    str = String.valueOf(a);
                }
                a2.mo9735a(str);
                m14076a(a2);
                a2.mo9735a(getString(R$string.data_unit_step));
                a2.mo9736b();
            }
            int i = b.f6277h;
            SpanUtils a3 = SpanUtils.m865a(this.f8460f0);
            float f = (float) i;
            a3.mo9735a(String.format(Locale.getDefault(), "%.2f", Float.valueOf(f / 1000.0f)));
            m14076a(a3);
            a3.mo9735a(getString(R$string.data_unit_mill));
            a3.mo9736b();
            SpanUtils a4 = SpanUtils.m865a(this.f8461g0);
            int i2 = b.f6279j;
            if (i2 != 0) {
                str2 = String.valueOf(i2);
            }
            a4.mo9735a(str2);
            m14076a(a4);
            a4.mo9735a(getString(R$string.data_unit_rate));
            a4.mo9736b();
            int i3 = b.f6276g;
            SpanUtils a5 = SpanUtils.m865a(this.f8462h0);
            int i4 = i3 / 3600;
            if (i4 > 0) {
                a5.mo9735a(String.valueOf(i4));
                m14076a(a5);
                a5.mo9735a(getString(R$string.data_unit_hour));
            }
            a5.mo9735a(String.valueOf((i3 / 60) % 60));
            m14076a(a5);
            a5.mo9735a(getString(R$string.data_unit_min));
            a5.mo9735a(String.valueOf(i3 % 60));
            m14076a(a5);
            a5.mo9735a(getString(R$string.data_unit_second));
            a5.mo9736b();
            float f2 = (f * 0.001f) / ((((float) i3) * 1.0f) / 3600.0f);
            SpanUtils a6 = SpanUtils.m865a(this.f8463i0);
            a6.mo9735a(String.format(Locale.getDefault(), "%.2f", Float.valueOf(f2)));
            m14076a(a6);
            a6.mo9735a(getString(R$string.data_unit_speed));
            a6.mo9736b();
            SpanUtils a7 = SpanUtils.m865a(this.f8464j0);
            a7.mo9735a(String.valueOf(b.f6278i));
            m14076a(a7);
            a7.mo9735a(getString(R$string.data_unit_cal));
            a7.mo9736b();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14078b(boolean z) {
        String stringExtra = getIntent().getStringExtra("sportId");
        String stringExtra2 = getIntent().getStringExtra("userNbr");
        List<SportTraceInfo> b = new SportTraceDataDb().mo24272b(stringExtra2, stringExtra);
        if (b != null || !z) {
            if (b != null && b.size() > 0) {
                int size = b.size();
                PolylineOptions polylineOptions = null;
                com.amap.api.maps.model.PolylineOptions polylineOptions2 = null;
                LatLngBounds.Builder builder = null;
                for (int i = 0; i < size; i++) {
                    SportTraceInfo fVar = b.get(i);
                    if (this.f8468n0 != null) {
                        LatLng latLng = new LatLng(fVar.f6293e, fVar.f6294f);
                        if (this.f8469o0 == null) {
                            this.f8469o0 = new LatLngBounds.C2927a();
                        }
                        this.f8469o0.mo18537a(latLng);
                        if (polylineOptions == null) {
                            polylineOptions = new PolylineOptions().mo18575a(latLng).mo18580d(Color.parseColor("#f70000"));
                        } else {
                            polylineOptions.mo18575a(latLng);
                        }
                    } else if (this.f8467m0 != null) {
                        com.amap.api.maps.model.LatLng latLng2 = new com.amap.api.maps.model.LatLng(fVar.f6293e, fVar.f6294f);
                        if (builder == null) {
                            builder = new LatLngBounds.Builder();
                        }
                        builder.include(latLng2);
                        if (polylineOptions2 == null) {
                            polylineOptions2 = new com.amap.api.maps.model.PolylineOptions().add(latLng2).color(Color.parseColor("#f70000"));
                        } else {
                            polylineOptions2.add(latLng2);
                        }
                    }
                }
                C2880c cVar = this.f8468n0;
                if (!(cVar == null || polylineOptions == null)) {
                    cVar.mo18400a(polylineOptions);
                    this.f8468n0.mo18402a(C2879b.m8106a(this.f8469o0.mo18538a().mo18532c()));
                    this.f8468n0.mo18404a(new C4369n(this));
                }
                AMap aMap = this.f8467m0;
                if (aMap != null && polylineOptions2 != null) {
                    aMap.addPolyline(polylineOptions2);
                    try {
                        this.f8467m0.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), ConvertUtils.m1055a(20.0f)));
                    } catch (Exception e) {
                        LogUtils.m1131b("android " + e.getMessage());
                    }
                }
            }
        } else if (MemberControl.m17125s().mo27187l().equals(stringExtra2)) {
            new HvApiGetSportTrace().request(stringExtra, new C4354a());
        } else {
            new com.heimavista.api.buddy.HvApiGetSportTrace().request(stringExtra2, stringExtra, new C4355b());
        }
    }

    /* renamed from: a */
    private SpanUtils m14076a(SpanUtils spanUtils) {
        spanUtils.mo9737b(getResources().getColor(R$color.font_color));
        spanUtils.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        spanUtils.mo9738c();
        return spanUtils;
    }

    /* renamed from: a */
    public /* synthetic */ void mo24554a(C2880c cVar) {
        this.f8468n0 = cVar;
        m14078b(true);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24553a(AMap aMap) {
        this.f8467m0 = aMap;
        m14078b(true);
    }
}
