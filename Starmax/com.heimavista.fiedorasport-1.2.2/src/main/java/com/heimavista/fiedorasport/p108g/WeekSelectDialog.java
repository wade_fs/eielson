package com.heimavista.fiedorasport.p108g;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.widget.dialog.UIDialog;
import p119e.p189e.p190a.AnimAction;

/* renamed from: com.heimavista.fiedorasport.g.j */
public final class WeekSelectDialog extends UIDialog<WeekSelectDialog> {

    /* renamed from: m0 */
    private boolean[] f6477m0;

    /* renamed from: n0 */
    private C3702k f6478n0;

    public WeekSelectDialog(Context context) {
        super(context);
        mo25419c(R$layout.dialog_repeat_day);
        mo25416b(AnimAction.f7975e);
        mo25422d(80);
        mo25427g((ScreenUtils.m1265c() * 4) / 5);
        mo24144a(R$id.dialog_negative_btn);
    }

    /* renamed from: a */
    public WeekSelectDialog mo22866a(boolean[] zArr) {
        this.f6477m0 = zArr;
        int[] iArr = {R$id.cbMonday, R$id.cbTuesday, R$id.cbWednesday, R$id.cbThursday, R$id.cbFriday, R$id.cbSaturday, R$id.cbSunday};
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            CheckBox checkBox = (CheckBox) findViewById(iArr[i]);
            checkBox.setChecked(this.f6477m0[i]);
            checkBox.setOnCheckedChangeListener(new C3699c(this, i));
        }
        return this;
    }

    public void onClick(View view) {
        if (view.getId() == R$id.dialog_negative_btn) {
            mo25471h();
            C3702k kVar = this.f6478n0;
            if (kVar != null) {
                kVar.mo22868a(this.f6477m0);
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo22867a(int i, CompoundButton compoundButton, boolean z) {
        this.f6477m0[i] = z;
    }

    /* renamed from: a */
    public WeekSelectDialog mo22865a(C3702k kVar) {
        this.f6478n0 = kVar;
        return this;
    }
}
