package com.heimavista.fiedorasport.lib;

public final class R$array {
    public static final int facebook_permissions = 2130903040;
    public static final int line_scopes = 2130903041;
    public static final int watch_time_content = 2130903042;
    public static final int watch_time_position = 2130903043;

    private R$array() {
    }
}
