package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import com.amap.api.maps.AMap;
import com.amap.api.maps.model.MyLocationStyle;
import com.google.android.gms.maps.C2880c;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.ListActivitiesActivity;
import com.heimavista.fiedorasport.p111ui.activity.location.LocationActivity;
import com.heimavista.fiedorasport.p111ui.fragment.map.MapFragment;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddyActivitiesFragment */
public class BuddyActivitiesFragment extends BaseFragment<HomeActivity> {

    /* renamed from: W */
    private AMap f8696W;

    /* renamed from: X */
    private C2880c f8697X;

    /* renamed from: i */
    private void m14370i() {
        if (this.f8696W == null) {
        }
    }

    /* renamed from: j */
    private void m14371j() {
        if (this.f8697X == null) {
        }
    }

    /* renamed from: k */
    private void m14372k() {
        if (isVisible()) {
            m14370i();
            m14371j();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment
     arg types: [boolean, int]
     candidates:
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(com.google.android.gms.maps.GoogleMapOptions, com.google.android.gms.maps.c):void
      com.heimavista.fiedorasport.ui.fragment.map.MapFragment.a(boolean, boolean):com.heimavista.fiedorasport.ui.fragment.map.MapFragment */
    /* renamed from: l */
    private void m14373l() {
        MapFragment a = MapFragment.m14638a(FSManager.m10325t(), false);
        if (FSManager.m10325t()) {
            a.mo24729a(new C4442a(this));
        } else {
            a.mo24729a(new C4444c(this));
        }
        getChildFragmentManager().beginTransaction().replace(R$id.mapFragment, a, "mapFragment").commit();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        m14373l();
        mo22770b(R$id.layoutActivities, R$id.layoutNewActivity, R$id.ivPos, R$id.ivFlag);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_buddy_activities;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return new String[]{"fiedoraSport.ACTION_onMapTypeChanged"};
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.ivFlag) {
            mo22759a(LocationActivity.class);
        } else if (id == R$id.ivPos) {
            m14372k();
        } else if (id == R$id.layoutActivities) {
            mo22761a(getString(R$string.all_activities));
            mo22769b(ListActivitiesActivity.class);
        }
    }

    public void onResume() {
        super.onResume();
        m14372k();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        super.mo22754a(context, intent);
        if ("fiedoraSport.ACTION_onMapTypeChanged".equals(intent.getAction())) {
            m14373l();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24676a(C2880c cVar) {
        this.f8697X = cVar;
        this.f8697X.mo18407b().mo18415c(false);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24675a(AMap aMap) {
        this.f8696W = aMap;
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        AvatarUtils.m13392a(getContext(), MemberControl.m17125s().mo27183h(), Color.parseColor("#EDBE00"), new C4443b(myLocationStyle));
        myLocationStyle.strokeColor(Color.parseColor("#EDBE00"));
        myLocationStyle.strokeWidth(0.0f);
        myLocationStyle.radiusFillColor(0);
        this.f8696W.setMyLocationStyle(myLocationStyle);
    }
}
