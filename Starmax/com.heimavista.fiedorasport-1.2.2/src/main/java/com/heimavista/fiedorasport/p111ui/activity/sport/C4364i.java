package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.view.View;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.i */
/* compiled from: lambda */
public final /* synthetic */ class C4364i implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ SportDataActivity f8484P;

    public /* synthetic */ C4364i(SportDataActivity sportDataActivity) {
        this.f8484P = sportDataActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8484P.mo24549a(view, (SportInfo) obj, i);
    }
}
