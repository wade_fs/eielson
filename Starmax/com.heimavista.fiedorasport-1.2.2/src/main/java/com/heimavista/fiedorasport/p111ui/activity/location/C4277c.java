package com.heimavista.fiedorasport.p111ui.activity.location;

import android.graphics.Bitmap;
import com.amap.api.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.location.c */
/* compiled from: lambda */
public final /* synthetic */ class C4277c implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ LocationActivity f8193a;

    /* renamed from: b */
    private final /* synthetic */ LatLng f8194b;

    public /* synthetic */ C4277c(LocationActivity locationActivity, LatLng latLng) {
        this.f8193a = locationActivity;
        this.f8194b = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8193a.mo24435a(this.f8194b, bitmap);
    }
}
