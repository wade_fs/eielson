package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.f */
/* compiled from: lambda */
public final /* synthetic */ class C4416f implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8663P;

    /* renamed from: Q */
    private final /* synthetic */ int f8664Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8665R;

    public /* synthetic */ C4416f(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8663P = listBuddyDynamicAdapter;
        this.f8664Q = i;
        this.f8665R = dVar;
    }

    public final void onClick(View view) {
        this.f8663P.mo24623f(this.f8664Q, this.f8665R, view);
    }
}
