package com.heimavista.fiedorasport.p111ui.fragment.friend;

import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddyDynamicFragment;
import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.e */
/* compiled from: lambda */
public final /* synthetic */ class C4446e implements BaseDialog.C4621m {

    /* renamed from: a */
    private final /* synthetic */ BuddyDynamicFragment.C4431c f8765a;

    /* renamed from: b */
    private final /* synthetic */ FriendBean f8766b;

    public /* synthetic */ C4446e(BuddyDynamicFragment.C4431c cVar, FriendBean dVar) {
        this.f8765a = cVar;
        this.f8766b = dVar;
    }

    /* renamed from: a */
    public final void mo23034a(boolean z) {
        this.f8765a.mo24685a(this.f8766b, z);
    }
}
