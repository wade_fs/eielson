package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.s */
/* compiled from: lambda */
public final /* synthetic */ class C4460s implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ BuddyLocFragment f8792a;

    /* renamed from: b */
    private final /* synthetic */ double f8793b;

    /* renamed from: c */
    private final /* synthetic */ double f8794c;

    /* renamed from: d */
    private final /* synthetic */ String f8795d;

    public /* synthetic */ C4460s(BuddyLocFragment buddyLocFragment, double d, double d2, String str) {
        this.f8792a = buddyLocFragment;
        this.f8793b = d;
        this.f8794c = d2;
        this.f8795d = str;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8792a.mo24687a(this.f8793b, this.f8794c, this.f8795d, bitmap);
    }
}
