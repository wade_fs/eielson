package com.heimavista.fiedorasport.p111ui.activity.device;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.heimavista.api.band.HvApiUnbind;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$drawable;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.DisconnectDialog;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.widget.dialog.LoadingDialog;

/* renamed from: com.heimavista.fiedorasport.ui.activity.device.DeviceManagerActivity */
public class DeviceManagerActivity extends BaseActivity {

    /* renamed from: b0 */
    private ImageView f6670b0;

    /* renamed from: c0 */
    private ImageView f6671c0;

    /* renamed from: d0 */
    private TextView f6672d0;

    /* renamed from: e0 */
    private TextView f6673e0;

    /* renamed from: f0 */
    private TextView f6674f0;

    /* renamed from: g0 */
    private Button f6675g0;

    /* renamed from: h0 */
    private Button f6676h0;
    /* access modifiers changed from: private */

    /* renamed from: i0 */
    public boolean f6677i0 = false;

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m10812c(boolean z) {
        String i = FSManager.m10323r().mo22839i();
        String l = FSManager.m10323r().mo22842l();
        this.f6673e0.setText(i);
        this.f6675g0.setVisibility(8);
        if (z || !TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            if (!TextUtils.isEmpty(l)) {
                this.f6674f0.setText(l);
            }
            C4222l.m13457a(this.f6670b0, R$color.font_color);
            if (z) {
                this.f6672d0.setText(R$string.connected);
                this.f6671c0.setImageResource(R$mipmap.icon_bt);
            } else {
                this.f6672d0.setText(R$string.not_connected);
                this.f6671c0.setImageResource(R$mipmap.icon_bt_dis);
                this.f6675g0.setText(R$string.connect_device);
                this.f6675g0.setVisibility(0);
            }
            this.f6671c0.setVisibility(0);
            this.f6673e0.setVisibility(0);
            this.f6674f0.setVisibility(0);
            this.f6676h0.setTextColor(getResources().getColor(R$color.font_color));
            this.f6676h0.setText(R$string.unbind_device);
            this.f6676h0.setBackgroundResource(R$drawable.selector_white);
            return;
        }
        C4222l.m13457a(this.f6670b0, R$color.font_color_hint);
        this.f6672d0.setText(R$string.device_has_not_bind);
        this.f6673e0.setVisibility(4);
        this.f6674f0.setVisibility(4);
        this.f6671c0.setVisibility(8);
        this.f6676h0.setTextColor(-1);
        this.f6676h0.setText(R$string.bind_device);
        this.f6676h0.setBackgroundResource(R$drawable.selector_blue_light);
    }

    /* renamed from: u */
    private void m10813u() {
        if (!TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            DisconnectDialog eVar = new DisconnectDialog(this);
            eVar.mo22852a(new C3775a(this));
            eVar.mo25418b().show();
            return;
        }
        this.f6677i0 = false;
        mo22702a(ScanActivity.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void
     arg types: [?[OBJECT, ARRAY], int, int, int, com.heimavista.fiedorasport.ui.activity.device.c]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(boolean, java.lang.String, boolean, boolean, com.heimavista.widget.dialog.e$a):void
      com.heimavista.fiedorasport.h.y.a(int, java.lang.String, java.lang.String, java.lang.String, int):void
      com.heimavista.fiedorasport.h.y.a(long, int, int, int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, boolean, boolean, com.heimavista.widget.dialog.e$a):void */
    /* renamed from: v */
    private void m10814v() {
        mo22711a((String) null, false, false, true, (LoadingDialog.C4628a) new C3777c(this));
    }

    /* renamed from: e */
    public /* synthetic */ void mo23037e(int i) {
        m10812c(i == 2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_device;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.btnAction) {
            m10813u();
        } else if (view.getId() == R$id.btnDeviceAction && !TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22882a(FSManager.m10323r().mo22839i(), FSManager.m10323r().mo22842l(), DeviceManagerActivity.class.getSimpleName());
            }
            if (FSManager.m10323r().mo22846p()) {
                CRPBleManager.m10615b(FSManager.m10323r().mo22842l());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        boolean e = FSManager.m10323r().mo22847q() ? JYSDKManager.m10388i().mo22897e() : false;
        if (FSManager.m10323r().mo22846p()) {
            e = CRPBleManager.m10639j();
        }
        m10812c(e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f6670b0 = (ImageView) findViewById(R$id.ivBracelet);
        this.f6671c0 = (ImageView) findViewById(R$id.ivBluetooth);
        this.f6672d0 = (TextView) findViewById(R$id.tvConnectStatus);
        this.f6673e0 = (TextView) findViewById(R$id.tvDeviceName);
        this.f6674f0 = (TextView) findViewById(R$id.tvDeviceMacAddress);
        this.f6675g0 = (Button) findViewById(R$id.btnDeviceAction);
        this.f6676h0 = (Button) findViewById(R$id.btnAction);
        mo22714a(this.f6675g0, this.f6676h0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public String mo22739j() {
        return getString(R$string.device_manage);
    }

    /* renamed from: a */
    public /* synthetic */ void mo23035a(DialogInterface dialogInterface) {
        mo22697a((Bundle) null);
    }

    /* renamed from: b */
    public /* synthetic */ void mo23036b(boolean z) {
        if (z) {
            m10814v();
            Constant.m10311a("解綁");
            new HvApiUnbind().request(new C3786l(this));
        }
    }

    /* renamed from: b */
    public void mo22718b(int i) {
        if (!this.f6677i0) {
            runOnUiThread(new C3776b(this, i));
        }
    }
}
