package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.blankj.utilcode.util.BrightnessUtils;
import com.blankj.utilcode.util.IntentUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.heimavista.api.HvApiEncryptUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.zbar.QRCodeUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import p119e.p189e.p218g.MemberControl;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.QrCodeActivity */
public class QrCodeActivity extends BaseActivity {

    /* renamed from: b0 */
    private int f8094b0 = 0;

    /* renamed from: a */
    private void m13560a(Bitmap bitmap, ImageView imageView) {
        imageView.setImageBitmap(bitmap);
        String string = getString(R$string.app_name);
        String string2 = getString(R$string.scan_tip, new Object[]{string});
        SpanUtils a = SpanUtils.m865a((TextView) findViewById(R$id.tvTip));
        a.mo9735a(string2.substring(0, string2.indexOf(string)));
        a.mo9735a(string);
        a.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_15));
        a.mo9735a(string2.substring(string2.indexOf(string) + string.length()));
        a.mo9736b();
        findViewById(R$id.btnShare).setOnClickListener(new C4253q(this, bitmap));
    }

    /* renamed from: e */
    private void m13561e(int i) {
        if (Build.VERSION.SDK_INT < 23) {
            BrightnessUtils.m995a(i);
        } else if (Settings.System.canWrite(getApplicationContext())) {
            BrightnessUtils.m995a(i);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        ImageView imageView = (ImageView) findViewById(R$id.ivQrcode);
        int c = (ScreenUtils.m1265c() * 3) / 5;
        imageView.getLayoutParams().width = c;
        imageView.getLayoutParams().height = c;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("userNbr", MemberControl.m17125s().mo27187l());
            jSONObject.put("userName", MemberControl.m17125s().mo27182g());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        m13560a(QRCodeUtils.m15449a(HvApiEncryptUtils.m10003b(jSONObject.toString(), "fiedoraSport"), c), imageView);
        ((TextView) findViewById(R$id.tvUserId)).setText("ID：" + FSManager.m10323r().mo22843m());
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_qrcode;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.my_qrcode);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        m13561e(this.f8094b0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        m13561e(255);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24397a(Bitmap bitmap, View view) {
        m13559a(bitmap);
    }

    /* renamed from: a */
    private void m13559a(Bitmap bitmap) {
        File file = new File(C4222l.m13454a(), "qrcode.png");
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent a = IntentUtils.m1106a(getString(R$string.my_qrcode), file);
        if (a != null) {
            startActivity(a);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8094b0 = BrightnessUtils.m994a();
    }
}
