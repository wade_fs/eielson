package com.heimavista.fiedorasport.p111ui.activity.friends;

import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.i */
/* compiled from: lambda */
public final /* synthetic */ class C4245i implements BaseDialog.C4621m {

    /* renamed from: a */
    private final /* synthetic */ ListAddFriendsActivity f8120a;

    /* renamed from: b */
    private final /* synthetic */ String f8121b;

    public /* synthetic */ C4245i(ListAddFriendsActivity listAddFriendsActivity, String str) {
        this.f8120a = listAddFriendsActivity;
        this.f8121b = str;
    }

    /* renamed from: a */
    public final void mo23034a(boolean z) {
        this.f8120a.mo24387b(this.f8121b, z);
    }
}
