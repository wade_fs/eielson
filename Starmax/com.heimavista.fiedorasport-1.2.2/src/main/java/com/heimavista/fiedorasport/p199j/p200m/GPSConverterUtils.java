package com.heimavista.fiedorasport.p199j.p200m;

import android.location.Location;

/* renamed from: com.heimavista.fiedorasport.j.m.a */
public class GPSConverterUtils {
    /* renamed from: a */
    public static float m13467a(double d, double d2, double d3, double d4) {
        float[] fArr = new float[1];
        Location.distanceBetween(d, d2, d3, d4, fArr);
        return fArr[0];
    }
}
