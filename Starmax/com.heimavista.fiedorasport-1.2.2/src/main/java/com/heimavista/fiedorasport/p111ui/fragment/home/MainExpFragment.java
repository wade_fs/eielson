package com.heimavista.fiedorasport.p111ui.fragment.home;

import android.os.Bundle;
import android.view.View;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.p203ns.gui.NsCenterFragment;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.MainExpFragment */
public class MainExpFragment extends BaseFragment<HomeActivity> {
    /* renamed from: i */
    public static MainExpFragment m14539i() {
        Bundle bundle = new Bundle();
        MainExpFragment mainExpFragment = new MainExpFragment();
        mainExpFragment.setArguments(bundle);
        return mainExpFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        NsCenterFragment.NsCenterConfig nsCenterConfig = new NsCenterFragment.NsCenterConfig();
        nsCenterConfig.mo25159a(true);
        getChildFragmentManager().beginTransaction().replace(R$id.llNsContainer, NsCenterFragment.m15143a(nsCenterConfig), "ns_fragment").commit();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_main_exp;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
    }
}
