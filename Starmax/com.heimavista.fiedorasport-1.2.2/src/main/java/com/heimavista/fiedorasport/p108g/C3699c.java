package com.heimavista.fiedorasport.p108g;

import android.widget.CompoundButton;

/* renamed from: com.heimavista.fiedorasport.g.c */
/* compiled from: lambda */
public final /* synthetic */ class C3699c implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a */
    private final /* synthetic */ WeekSelectDialog f6464a;

    /* renamed from: b */
    private final /* synthetic */ int f6465b;

    public /* synthetic */ C3699c(WeekSelectDialog jVar, int i) {
        this.f6464a = jVar;
        this.f6465b = i;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.f6464a.mo22867a(this.f6465b, compoundButton, z);
    }
}
