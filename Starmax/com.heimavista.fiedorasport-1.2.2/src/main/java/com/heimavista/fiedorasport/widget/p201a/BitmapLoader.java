package com.heimavista.fiedorasport.widget.p201a;

import android.graphics.Bitmap;

/* renamed from: com.heimavista.fiedorasport.widget.a.a */
public interface BitmapLoader {
    /* renamed from: a */
    Bitmap mo24607a(String str);
}
