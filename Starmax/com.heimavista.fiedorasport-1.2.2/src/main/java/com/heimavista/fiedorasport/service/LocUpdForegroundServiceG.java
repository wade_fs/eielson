package com.heimavista.fiedorasport.service;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import com.blankj.utilcode.util.ServiceUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.location.C2826b;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.C2837g;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.heimavista.fiedorasport.service.LocUpdForegroundService;

public class LocUpdForegroundServiceG extends LocUpdForegroundService {

    /* renamed from: R */
    private LocationRequest f6628R;

    /* renamed from: S */
    private C2826b f6629S;

    /* renamed from: T */
    private C2833e f6630T;

    /* renamed from: U */
    private Handler f6631U;

    /* renamed from: com.heimavista.fiedorasport.service.LocUpdForegroundServiceG$a */
    class C3749a extends LocUpdForegroundService.C3747a<LocUpdForegroundServiceG> {
        C3749a() {
            super(LocUpdForegroundServiceG.this);
        }

        /* renamed from: a */
        public LocUpdForegroundServiceG mo23005a() {
            return LocUpdForegroundServiceG.this;
        }
    }

    /* renamed from: com.heimavista.fiedorasport.service.LocUpdForegroundServiceG$b */
    class C3750b extends C2833e {
        C3750b() {
        }

        /* renamed from: a */
        public void mo18255a(LocationResult locationResult) {
            super.mo18255a(locationResult);
            LocUpdForegroundServiceG.this.mo22993a(locationResult.mo18234c());
        }
    }

    /* renamed from: a */
    public void mo22992a() {
        if (this.f6629S != null) {
            this.f6630T = null;
            this.f6629S = null;
        }
        super.f6623Q = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public IBinder mo22995b() {
        return new C3749a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo22996c() {
        this.f6629S = C2837g.m7977a(this);
        this.f6630T = new C3750b();
        this.f6628R = new LocationRequest();
        this.f6628R.mo18231i(10000);
        this.f6628R.mo18229h(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        this.f6628R.mo18227d(100);
        HandlerThread handlerThread = new HandlerThread(LocUpdForegroundServiceG.class.getSimpleName());
        handlerThread.start();
        this.f6631U = new Handler(handlerThread.getLooper());
    }

    /* renamed from: e */
    public void mo22998e() {
        mo22994a("Requesting location updates");
        super.f6623Q = true;
        ServiceUtils.m1270b(LocUpdForegroundServiceG.class);
        try {
            this.f6629S.mo18252a(this.f6628R, this.f6630T, Looper.myLooper());
        } catch (SecurityException e) {
            super.f6623Q = false;
            mo22994a("Lost location permission. Could not request updates. " + e);
        }
    }

    /* renamed from: f */
    public void mo22999f() {
        mo22994a("Removing location updates");
        try {
            this.f6629S.mo18253a(this.f6630T);
            super.f6623Q = false;
            stopSelf();
        } catch (SecurityException e) {
            super.f6623Q = true;
            mo22994a("Lost location permission. Could not remove updates. " + e);
        }
    }

    public void onDestroy() {
        this.f6631U.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
