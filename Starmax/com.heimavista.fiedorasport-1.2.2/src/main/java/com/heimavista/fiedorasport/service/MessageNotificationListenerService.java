package com.heimavista.fiedorasport.service;

import android.app.Notification;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.entity.data.RemindMsgInfo;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.linecorp.linesdk.BuildConfig;
import com.tencent.p214mm.opensdk.constants.ConstantsAPI;

public class MessageNotificationListenerService extends NotificationListenerService {
    /* renamed from: a */
    private void m10727a(String str, int i, int i2, String str2, String str3) {
        if (FSManager.m10323r().mo22846p() && CRPBleManager.m10639j()) {
            CRPBleManager.m10605a(str3, i2);
        }
        if (FSManager.m10323r().mo22847q() && JYSDKManager.m10388i().mo22897e()) {
            JYSDKManager.m10388i().mo22879a(str, i, str2, str3);
        }
    }

    public void onNotificationPosted(StatusBarNotification statusBarNotification, NotificationListenerService.RankingMap rankingMap) {
        super.onNotificationPosted(statusBarNotification, rankingMap);
    }

    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        super.onNotificationRemoved(statusBarNotification);
    }

    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        super.onNotificationPosted(statusBarNotification);
        String packageName = statusBarNotification.getPackageName();
        Notification notification = statusBarNotification.getNotification();
        LogUtils.m1139c("madyNotify>>>MessageNotificationListenerService", "packageName=" + packageName, "notification=" + notification);
        RemindMsgInfo e = BandDataManager.m10563e();
        if (!TextUtils.isEmpty(packageName) && notification != null && !TextUtils.isEmpty(notification.tickerText)) {
            long currentTimeMillis = System.currentTimeMillis();
            String charSequence = notification.tickerText.toString();
            LogUtils.m1139c("madyNotify>>>MessageNotificationListenerService Notification " + packageName + ", " + charSequence);
            if ("com.tencent.mm".equals(packageName) && e.mo22616i()) {
                m10727a(ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE + currentTimeMillis, 2, 2, getString(R$string.wechat_message), charSequence);
            }
            if ("com.tencent.mobileqq".equals(packageName) && e.mo22608e()) {
                m10727a("qq" + currentTimeMillis, 3, 3, getString(R$string.qq_message), charSequence);
            }
            if (("com.facebook.katana".equals(packageName) || "com.facebook.orca".equals(packageName) || "com.facebook.lite".equals(packageName)) && e.mo22601b()) {
                m10727a("facebook" + currentTimeMillis, 4, 4, getString(R$string.facebook_message), charSequence);
            }
            if ("com.skype.raider".equals(packageName) && e.mo22610f()) {
                m10727a("skype" + currentTimeMillis, 5, 7, getString(R$string.skype_message), charSequence);
            }
            if ("com.twitter.android".equals(packageName) && e.mo22614h()) {
                m10727a("twitter" + currentTimeMillis, 6, 5, getString(R$string.twitter_message), charSequence);
            }
            if ("com.whatsapp".equals(packageName) && e.mo22618j()) {
                m10727a("whatsapp" + currentTimeMillis, 7, 8, getString(R$string.whatsapp_message), charSequence);
            }
            if ((BuildConfig.LINE_APP_PACKAGE_NAME.equals(packageName) || "jp.naver.line".equals(packageName)) && e.mo22603c()) {
                m10727a("line" + currentTimeMillis, 8, 9, getString(R$string.line_message), charSequence);
            }
            if ("com.heimavista.fiedorasport".equals(packageName)) {
                m10727a("fiedorasport" + currentTimeMillis, 10, 128, getString(R$string.app_message), charSequence);
            }
        }
    }
}
