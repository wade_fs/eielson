package com.heimavista.fiedorasport.p108g;

import android.app.AlertDialog;
import android.view.View;

/* renamed from: com.heimavista.fiedorasport.g.a */
/* compiled from: lambda */
public final /* synthetic */ class C3697a implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ InputDialog f6459P;

    /* renamed from: Q */
    private final /* synthetic */ AlertDialog f6460Q;

    public /* synthetic */ C3697a(InputDialog gVar, AlertDialog alertDialog) {
        this.f6459P = gVar;
        this.f6460Q = alertDialog;
    }

    public final void onClick(View view) {
        this.f6459P.mo22859a(this.f6460Q, view);
    }
}
