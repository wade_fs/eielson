package com.heimavista.fiedorasport.p111ui.activity.setting;

import com.heimavista.entity.data.MenuItem;
import p112d.p113a.p114a.p116b.TimePicker;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.t */
/* compiled from: lambda */
public final /* synthetic */ class C4329t implements TimePicker.C3816b {

    /* renamed from: a */
    private final /* synthetic */ SettingPowerActivity f8325a;

    /* renamed from: b */
    private final /* synthetic */ MenuItem f8326b;

    /* renamed from: c */
    private final /* synthetic */ int f8327c;

    /* renamed from: d */
    private final /* synthetic */ boolean f8328d;

    /* renamed from: e */
    private final /* synthetic */ boolean f8329e;

    public /* synthetic */ C4329t(SettingPowerActivity settingPowerActivity, MenuItem eVar, int i, boolean z, boolean z2) {
        this.f8325a = settingPowerActivity;
        this.f8326b = eVar;
        this.f8327c = i;
        this.f8328d = z;
        this.f8329e = z2;
    }

    /* renamed from: a */
    public final void mo23111a(String str, String str2) {
        this.f8325a.mo24486a(this.f8326b, this.f8327c, this.f8328d, this.f8329e, str, str2);
    }
}
