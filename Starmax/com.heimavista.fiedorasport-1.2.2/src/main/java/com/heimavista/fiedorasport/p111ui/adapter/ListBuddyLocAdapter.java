package com.heimavista.fiedorasport.p111ui.adapter;

import android.content.Context;
import android.location.Location;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.fiedorasport.p199j.p200m.GPSConverterUtils;
import java.util.Locale;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.LocDb;
import p119e.p189e.p193d.p195j.UserInfoDb;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyLocAdapter */
public class ListBuddyLocAdapter extends BaseRecyclerViewAdapter<BuddyListDb, C4402a> {

    /* renamed from: i */
    private Location f8600i;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListBuddyLocAdapter$a */
    class C4402a extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        ImageView f8601R = ((ImageView) findViewById(R$id.ivAvatar));

        /* renamed from: S */
        TextView f8602S = ((TextView) findViewById(R$id.tvUserName));

        /* renamed from: T */
        TextView f8603T = ((TextView) findViewById(R$id.tvDistance));

        /* renamed from: U */
        TextView f8604U = ((TextView) findViewById(R$id.tvUpdateTime));

        /* renamed from: V */
        TextView f8605V = ((TextView) findViewById(R$id.tvLocStat));

        public C4402a(ListBuddyLocAdapter listBuddyLocAdapter, ViewGroup viewGroup, int i) {
            super(listBuddyLocAdapter, viewGroup, i);
        }
    }

    public ListBuddyLocAdapter(Context context) {
        super(context);
    }

    /* renamed from: a */
    public void mo24631a(Location location) {
        this.f8600i = location;
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4402a aVar, int i) {
        String str;
        C4402a aVar2 = aVar;
        BuddyListDb aVar3 = (BuddyListDb) getItem(i);
        aVar2.f8603T.setVisibility(8);
        LocDb h = LocDb.m13307h(aVar3.mo24311x());
        boolean z = true;
        if (h != null) {
            long t = (long) h.mo24327t();
            boolean B = aVar3.mo24284B();
            aVar2.f8605V.setVisibility((!B || h.mo24325r() == 0 || t == 0) ? 8 : 0);
            int i2 = (t > 0 ? 1 : (t == 0 ? 0 : -1));
            if (i2 > 0) {
                if (h.mo24325r() == 1) {
                    aVar2.f8605V.setText(R$string.positioning);
                } else if (h.mo24325r() == 2) {
                    aVar2.f8605V.setText(R$string.positioning_failure);
                    aVar2.f8605V.setVisibility(8);
                } else if (h.mo24325r() == 3) {
                    aVar2.f8605V.setText(R$string.not_share_loc);
                }
                Location location = this.f8600i;
                if (location != null) {
                    float a = GPSConverterUtils.m13467a(location.getLatitude(), this.f8600i.getLongitude(), h.mo24323p(), h.mo24324q());
                    if (a >= 1000.0f) {
                        str = String.format(Locale.getDefault(), "距您%.0fkm", Float.valueOf(a / 1000.0f));
                    } else {
                        str = String.format(Locale.getDefault(), "距您%.0fm", Float.valueOf(a));
                    }
                    if (B) {
                        aVar2.f8603T.setText(str);
                        aVar2.f8603T.setVisibility(0);
                    }
                }
            }
            if (i2 > 0 && B) {
                z = false;
            }
            if (!z) {
                aVar2.f8604U.setText(TimeUtil.m13443a(mo22788b(), t * 1000));
            } else {
                aVar2.f8604U.setText(R$string.not_share_loc);
            }
        } else {
            aVar2.f8605V.setVisibility(8);
            aVar2.f8604U.setText(R$string.not_share_loc);
        }
        UserInfoDb.m13340a(mo22788b(), aVar3.mo24311x(), aVar2.f8602S, aVar2.f8601R, z);
    }

    @NonNull
    public C4402a onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new C4402a(this, viewGroup, R$layout.layout_list_item_buddy_loc);
    }
}
