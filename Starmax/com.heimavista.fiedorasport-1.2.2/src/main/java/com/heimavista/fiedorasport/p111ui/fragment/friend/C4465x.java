package com.heimavista.fiedorasport.p111ui.fragment.friend;

import com.heimavista.api.OnResultListener;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.p111ui.adapter.ListBuddyDynamicAdapter;
import com.heimavista.fiedorasport.p111ui.fragment.friend.BuddyDynamicFragment;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.x */
/* compiled from: BuddyDynamicFragment */
class C4465x implements OnResultListener<ParamJsonData> {

    /* renamed from: a */
    final /* synthetic */ FriendBean f8800a;

    /* renamed from: b */
    final /* synthetic */ BuddyDynamicFragment.C4431c f8801b;

    C4465x(BuddyDynamicFragment.C4431c cVar, FriendBean dVar) {
        this.f8801b = cVar;
        this.f8800a = dVar;
    }

    /* renamed from: a */
    public void mo22380a(ParamJsonData fVar) {
        BuddyDynamicFragment.this.mo22768b();
        BuddyDynamicFragment.this.f8699X.mo22791b(this.f8800a);
        if (BuddyListDb.m13246G().isEmpty()) {
            ListBuddyDynamicAdapter a = BuddyDynamicFragment.this.f8699X;
            FriendBean dVar = new FriendBean();
            dVar.mo22632a(0);
            a.mo22790b(0, dVar);
        }
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        BuddyDynamicFragment.this.mo22768b();
        BuddyDynamicFragment.this.mo22761a(str);
    }
}
