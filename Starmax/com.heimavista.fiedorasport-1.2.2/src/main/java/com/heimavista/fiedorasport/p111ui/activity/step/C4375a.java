package com.heimavista.fiedorasport.p111ui.activity.step;

import android.widget.RadioGroup;

/* renamed from: com.heimavista.fiedorasport.ui.activity.step.a */
/* compiled from: lambda */
public final /* synthetic */ class C4375a implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a */
    private final /* synthetic */ StepDataChartActivity f8518a;

    public /* synthetic */ C4375a(StepDataChartActivity stepDataChartActivity) {
        this.f8518a = stepDataChartActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.f8518a.mo24573a(radioGroup, i);
    }
}
