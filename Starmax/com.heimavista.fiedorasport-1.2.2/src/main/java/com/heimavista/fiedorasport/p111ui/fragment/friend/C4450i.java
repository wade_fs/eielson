package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.i */
/* compiled from: lambda */
public final /* synthetic */ class C4450i implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ BuddyLocFragment f8773a;

    /* renamed from: b */
    private final /* synthetic */ String f8774b;

    /* renamed from: c */
    private final /* synthetic */ LatLng f8775c;

    public /* synthetic */ C4450i(BuddyLocFragment buddyLocFragment, String str, LatLng latLng) {
        this.f8773a = buddyLocFragment;
        this.f8774b = str;
        this.f8775c = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8773a.mo24698b(this.f8774b, this.f8775c, bitmap);
    }
}
