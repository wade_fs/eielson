package com.heimavista.fiedorasport.p111ui.activity.sport;

import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.d */
/* compiled from: lambda */
public final /* synthetic */ class C4359d implements BaseDialog.C4621m {

    /* renamed from: a */
    private final /* synthetic */ SportActivity f8478a;

    /* renamed from: b */
    private final /* synthetic */ boolean f8479b;

    public /* synthetic */ C4359d(SportActivity sportActivity, boolean z) {
        this.f8478a = sportActivity;
        this.f8479b = z;
    }

    /* renamed from: a */
    public final void mo23034a(boolean z) {
        this.f8478a.mo24532a(this.f8479b, z);
    }
}
