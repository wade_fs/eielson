package com.heimavista.fiedorasport.p111ui.fragment.home;

import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.home.d */
/* compiled from: lambda */
public final /* synthetic */ class C4477d implements BaseDialog.C4621m {

    /* renamed from: a */
    private final /* synthetic */ MainHomeFragment f8869a;

    /* renamed from: b */
    private final /* synthetic */ boolean f8870b;

    public /* synthetic */ C4477d(MainHomeFragment mainHomeFragment, boolean z) {
        this.f8869a = mainHomeFragment;
        this.f8870b = z;
    }

    /* renamed from: a */
    public final void mo23034a(boolean z) {
        this.f8869a.mo24716a(this.f8870b, z);
    }
}
