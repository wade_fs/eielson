package com.heimavista.fiedorasport.p107f;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.apn.PushHandler;
import com.heimavista.apn.PushMsg;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.web.WebActivity;
import org.json.JSONObject;
import p119e.p189e.p191b.HvNotify;

/* renamed from: com.heimavista.fiedorasport.f.c */
public class PushHandleWebView implements PushHandler {
    /* renamed from: a */
    public void mo22549a(Context context, PushMsg pushMsg) {
        Intent intent;
        Context context2 = context;
        PushMsg pushMsg2 = pushMsg;
        LogUtils.m1139c("madyPushMsg>>>" + pushMsg.toString());
        JSONObject optJSONObject = pushMsg2.f6231U.mo25334d().optJSONObject("ext");
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("url");
            optJSONObject.optBoolean("isLogin");
            intent = WebActivity.m14288a(context2, "", optString);
        } else {
            intent = new Intent(context2, HomeActivity.class);
        }
        intent.putExtra("pushMsg", pushMsg2);
        if (!TextUtils.isEmpty(pushMsg2.f6228R) && !TextUtils.equals("null", pushMsg2.f6228R)) {
            if (TextUtils.equals("null", pushMsg2.f6227Q)) {
                pushMsg2.f6227Q = context2.getString(R$string.app_name);
            }
            HvNotify.m13018b().mo24163a(context, "channel_fs_push", context2.getString(R$string.channel_push), 3, pushMsg2.f6227Q, R$mipmap.ic_push, BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon), PendingIntent.getActivity(context2, 10004, intent, 134217728), pushMsg2.f6228R, pushMsg2.f6227Q, 0, 10004);
        }
    }
}
