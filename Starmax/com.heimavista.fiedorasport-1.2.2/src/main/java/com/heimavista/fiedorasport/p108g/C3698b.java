package com.heimavista.fiedorasport.p108g;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

/* renamed from: com.heimavista.fiedorasport.g.b */
/* compiled from: lambda */
public final /* synthetic */ class C3698b implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ InputDialog f6461P;

    /* renamed from: Q */
    private final /* synthetic */ AlertDialog f6462Q;

    /* renamed from: R */
    private final /* synthetic */ EditText f6463R;

    public /* synthetic */ C3698b(InputDialog gVar, AlertDialog alertDialog, EditText editText) {
        this.f6461P = gVar;
        this.f6462Q = alertDialog;
        this.f6463R = editText;
    }

    public final void onClick(View view) {
        this.f6461P.mo22860a(this.f6462Q, this.f6463R, view);
    }
}
