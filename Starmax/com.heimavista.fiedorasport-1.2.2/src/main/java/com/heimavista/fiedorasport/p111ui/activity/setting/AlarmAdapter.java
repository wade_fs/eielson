package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.widget.SwitchButton;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import java.util.ArrayList;
import java.util.Locale;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.AlarmAdapter */
public class AlarmAdapter extends BaseRecyclerViewAdapter<AlarmInfoItem, C4292b> {

    /* renamed from: i */
    private LayoutInflater f8221i;

    /* renamed from: j */
    private C4291a f8222j;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.AlarmAdapter$a */
    public interface C4291a {
        /* renamed from: a */
        void mo24466a(View view, int i, AlarmInfoItem alarmInfoItem);

        /* renamed from: a */
        void mo24467a(SwitchButton switchButton, int i, boolean z);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.AlarmAdapter$b */
    class C4292b extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        LinearLayout f8223R = ((LinearLayout) findViewById(R$id.llContain));

        /* renamed from: S */
        TextView f8224S = ((TextView) findViewById(R$id.tvTime));

        /* renamed from: T */
        TextView f8225T = ((TextView) findViewById(R$id.tvLabel));

        /* renamed from: U */
        SwitchButton f8226U = ((SwitchButton) findViewById(R$id.btnSwitch));

        C4292b(AlarmAdapter alarmAdapter, View view) {
            super(view);
        }
    }

    public AlarmAdapter(Context context) {
        super(context);
        this.f8221i = LayoutInflater.from(context);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4292b bVar, int i) {
        String str;
        AlarmInfoItem alarmInfoItem = (AlarmInfoItem) getItem(i);
        boolean z = false;
        String format = String.format(Locale.getDefault(), "%1$02d:%2$02d", Integer.valueOf(alarmInfoItem.mo26377k()), Integer.valueOf(alarmInfoItem.mo26378l()));
        if (this.f8222j != null) {
            bVar.f8223R.setOnClickListener(new C4300a(this, i, alarmInfoItem));
        }
        bVar.f8224S.setText(format);
        if (TextUtils.isEmpty(alarmInfoItem.mo26358b())) {
            str = m13768a(alarmInfoItem);
        } else {
            str = alarmInfoItem.mo26358b() + ", " + m13768a(alarmInfoItem);
        }
        bVar.f8225T.setText(str);
        SwitchButton switchButton = bVar.f8226U;
        if (alarmInfoItem.mo26373i() > 0) {
            z = true;
        }
        switchButton.setChecked(z);
        bVar.f8226U.setOnCheckedChangeListener(new C4302b(this, bVar, i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4292b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new C4292b(this, this.f8221i.inflate(R$layout.layout_item_alarm, viewGroup, false));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24462a(int i, AlarmInfoItem alarmInfoItem, View view) {
        this.f8222j.mo24466a(view, i, alarmInfoItem);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24465a(C4292b bVar, int i, SwitchButton switchButton, boolean z) {
        C4291a aVar = this.f8222j;
        if (aVar != null) {
            aVar.mo24467a(bVar.f8226U, i, z);
        }
    }

    /* renamed from: a */
    private String m13768a(AlarmInfoItem alarmInfoItem) {
        boolean z = alarmInfoItem.mo26362d() == 1;
        boolean z2 = alarmInfoItem.mo26371h() == 1;
        boolean z3 = alarmInfoItem.mo26375j() == 1;
        boolean z4 = alarmInfoItem.mo26369g() == 1;
        boolean z5 = alarmInfoItem.mo26360c() == 1;
        boolean z6 = alarmInfoItem.mo26365e() == 1;
        boolean z7 = alarmInfoItem.mo26367f() == 1;
        if (!z && !z2 && !z3 && !z4 && !z5 && !z6 && !z7) {
            alarmInfoItem.mo26357a(true);
            return mo22781a(R$string.only_once);
        } else if (!FSManager.m10323r().mo22847q()) {
            return mo22781a(R$string.everyday);
        } else {
            if (z && z2 && z3 && z4 && z5 && z6 && z7) {
                return mo22781a(R$string.everyday);
            }
            if (z && z2 && z3 && z4 && z5 && !z6 && !z7) {
                return mo22781a(R$string.workday);
            }
            if (!z && !z2 && !z3 && !z4 && !z5 && z6 && z7) {
                return mo22781a(R$string.weekend);
            }
            ArrayList arrayList = new ArrayList();
            if (z) {
                arrayList.add(mo22781a(R$string.Monday));
            }
            if (z2) {
                arrayList.add(mo22781a(R$string.Tuesday));
            }
            if (z3) {
                arrayList.add(mo22781a(R$string.Wednesday));
            }
            if (z4) {
                arrayList.add(mo22781a(R$string.Thursday));
            }
            if (z5) {
                arrayList.add(mo22781a(R$string.Friday));
            }
            if (z6) {
                arrayList.add(mo22781a(R$string.Saturday));
            }
            if (z7) {
                arrayList.add(mo22781a(R$string.Sunday));
            }
            StringBuilder sb = new StringBuilder();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                sb.append((String) arrayList.get(i));
                if (i < size - 1) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
    }

    /* renamed from: a */
    public void mo24463a(C4291a aVar) {
        this.f8222j = aVar;
    }
}
