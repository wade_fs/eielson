package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.graphics.Bitmap;
import android.view.View;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.q */
/* compiled from: lambda */
public final /* synthetic */ class C4253q implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ QrCodeActivity f8131P;

    /* renamed from: Q */
    private final /* synthetic */ Bitmap f8132Q;

    public /* synthetic */ C4253q(QrCodeActivity qrCodeActivity, Bitmap bitmap) {
        this.f8131P = qrCodeActivity;
        this.f8132Q = bitmap;
    }

    public final void onClick(View view) {
        this.f8131P.mo24397a(this.f8132Q, view);
    }
}
