package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.graphics.Bitmap;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.MyLocationStyle;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.b */
/* compiled from: lambda */
public final /* synthetic */ class C4443b implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ MyLocationStyle f8760a;

    public /* synthetic */ C4443b(MyLocationStyle myLocationStyle) {
        this.f8760a = myLocationStyle;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8760a.myLocationIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
    }
}
