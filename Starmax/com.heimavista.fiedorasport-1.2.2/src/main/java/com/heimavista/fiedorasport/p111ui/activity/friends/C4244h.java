package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.content.Intent;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.ListAddFriendsActivity;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.h */
/* compiled from: lambda */
public final /* synthetic */ class C4244h implements BaseActivity.C3680b {

    /* renamed from: a */
    private final /* synthetic */ ListAddFriendsActivity.C4229a f8119a;

    public /* synthetic */ C4244h(ListAddFriendsActivity.C4229a aVar) {
        this.f8119a = aVar;
    }

    public final void onActivityResult(int i, Intent intent) {
        this.f8119a.mo24390a(i, intent);
    }
}
