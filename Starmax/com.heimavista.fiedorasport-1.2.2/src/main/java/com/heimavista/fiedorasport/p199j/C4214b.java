package com.heimavista.fiedorasport.p199j;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: com.heimavista.fiedorasport.j.b */
/* compiled from: lambda */
public final /* synthetic */ class C4214b implements InputFilter {

    /* renamed from: P */
    public static final /* synthetic */ C4214b f8022P = new C4214b();

    private /* synthetic */ C4214b() {
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        return C4222l.m13453a(charSequence, i, i2, spanned, i3, i4);
    }
}
