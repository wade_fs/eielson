package com.heimavista.fiedorasport.p111ui.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.ConvertUtils;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$drawable;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.ListMenuAdapter */
public class ListMenuAdapter extends BaseRecyclerViewAdapter<MenuItem, C4406b> {

    /* renamed from: i */
    private LayoutInflater f8622i;

    /* renamed from: j */
    private C4405a f8623j;

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListMenuAdapter$a */
    public interface C4405a {
        /* renamed from: a */
        void mo24517a(MenuItem eVar, boolean z);
    }

    /* renamed from: com.heimavista.fiedorasport.ui.adapter.ListMenuAdapter$b */
    class C4406b extends BaseRecyclerViewAdapter.ViewHolder {

        /* renamed from: R */
        CommonListItemView f8624R = ((CommonListItemView) findViewById(R$id.listItemView));

        /* renamed from: S */
        TextView f8625S = ((TextView) findViewById(16908308));

        C4406b(ListMenuAdapter listMenuAdapter, View view) {
            super(view);
        }
    }

    public ListMenuAdapter(BaseActivity baseActivity) {
        super(baseActivity);
        this.f8622i = LayoutInflater.from(baseActivity);
    }

    /* renamed from: a */
    public void mo24646a(C4405a aVar) {
        this.f8623j = aVar;
    }

    public int getItemViewType(int i) {
        return ((MenuItem) getItem(i)).mo22653e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(java.lang.String, int, int, int, android.graphics.Bitmap):android.graphics.Bitmap
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean, int):void */
    /* renamed from: a */
    public void onBindViewHolder(@NonNull C4406b bVar, int i) {
        MenuItem eVar = (MenuItem) getItem(i);
        switch (eVar.mo22653e()) {
            case 0:
                bVar.f8624R.setAccessoryType(0);
                bVar.f8624R.setText(eVar.mo22652d());
                bVar.f8624R.setDetailText(eVar.mo22646b());
                return;
            case 1:
                bVar.f8624R.setAccessoryType(1);
                bVar.f8624R.setImageResource(eVar.mo22650c());
                bVar.f8624R.setText(eVar.mo22652d());
                bVar.f8624R.setDetailText(eVar.mo22646b());
                bVar.f8624R.setBackgroundResource(eVar.mo22656h() ? R$drawable.selector_list_item_bg_white : R$drawable.list_item_selector);
                return;
            case 2:
                bVar.f8624R.setAccessoryType(2);
                bVar.f8624R.setImageResource(eVar.mo22650c());
                bVar.f8624R.setText(eVar.mo22652d());
                SwitchButton switchButton = bVar.f8624R.getSwitch();
                switchButton.setChecked(eVar.mo22654f());
                switchButton.setEnabled(eVar.mo22655g());
                switchButton.setOnCheckedChangeListener(new C4424n(this, eVar));
                bVar.f8624R.setBackgroundResource(eVar.mo22656h() ? R$drawable.selector_list_item_bg_white : R$drawable.list_item_selector);
                return;
            case 3:
                bVar.f8624R.setAccessoryType(3);
                bVar.f8624R.setImageResource(eVar.mo22650c());
                bVar.f8624R.setText(eVar.mo22652d());
                bVar.f8624R.mo24738a(eVar.mo22641a());
                return;
            case 4:
            case 5:
                bVar.f8625S.setText(eVar.mo22652d());
                return;
            case 6:
                bVar.f8624R.setAccessoryType(1);
                bVar.f8624R.setImageResource(eVar.mo22650c());
                bVar.f8624R.setText(eVar.mo22652d());
                ImageView detailImageView = bVar.f8624R.getDetailImageView();
                detailImageView.setVisibility(0);
                int a = ConvertUtils.m1055a(50.0f);
                detailImageView.getLayoutParams().width = a;
                detailImageView.getLayoutParams().height = a;
                if (TextUtils.isEmpty(eVar.mo22646b())) {
                    detailImageView.setImageResource(R$mipmap.login_user);
                    return;
                } else {
                    AvatarUtils.m13394a(detailImageView, eVar.mo22646b(), R$mipmap.login_user, true, a);
                    return;
                }
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public C4406b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 6:
                return new C4406b(this, this.f8622i.inflate(R$layout.layout_list_item, viewGroup, false));
            case 4:
                return new C4406b(this, this.f8622i.inflate(R$layout.layout_list_item_text, viewGroup, false));
            case 5:
                return new C4406b(this, this.f8622i.inflate(R$layout.layout_list_item_button, viewGroup, false));
            default:
                return new C4406b(this, this.f8622i.inflate(R$layout.view_divider_10dp, viewGroup, false));
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24645a(MenuItem eVar, SwitchButton switchButton, boolean z) {
        C4405a aVar = this.f8623j;
        if (aVar != null) {
            aVar.mo24517a(eVar, z);
        }
    }
}
