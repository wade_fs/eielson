package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.view.View;
import com.heimavista.entity.data.MenuItem;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.s */
/* compiled from: lambda */
public final /* synthetic */ class C4328s implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ SettingPowerActivity f8324P;

    public /* synthetic */ C4328s(SettingPowerActivity settingPowerActivity) {
        this.f8324P = settingPowerActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8324P.mo24485a(view, (MenuItem) obj, i);
    }
}
