package com.heimavista.fiedorasport.p109h;

import android.app.Activity;
import android.text.TextUtils;
import com.blankj.utilcode.util.ConvertUtils;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.lib.R$style;
import p112d.p113a.p114a.p116b.DatePicker;
import p112d.p113a.p114a.p116b.NumberPicker;
import p112d.p113a.p114a.p116b.TimePicker;
import p112d.p113a.p114a.p116b.WheelPicker;

/* renamed from: com.heimavista.fiedorasport.h.c0 */
public class PickerManager {
    /* renamed from: a */
    public static NumberPicker m10513a(Activity activity, String str, int i, int i2, int i3) {
        NumberPicker cVar = new NumberPicker(activity);
        if (!TextUtils.isEmpty(str)) {
            cVar.mo23101a(str);
        }
        m10515a(cVar, activity);
        cVar.mo23093a(i, i2, i3);
        return cVar;
    }

    /* renamed from: a */
    public static TimePicker m10514a(Activity activity, int i, int i2) {
        TimePicker eVar = new TimePicker(activity, 3);
        m10515a(eVar, activity);
        eVar.mo23109g(0, 0);
        eVar.mo23063f(23, 59);
        eVar.mo23112b(false);
        eVar.mo23108a(activity.getString(R$string.data_unit_hour), activity.getString(R$string.data_unit_min));
        eVar.mo23110h(i, i2);
        return eVar;
    }

    /* renamed from: a */
    public static DatePicker m10512a(Activity activity) {
        DatePicker aVar = new DatePicker(activity, 1);
        m10515a(aVar, activity);
        aVar.mo23062a("年", "月", "");
        return aVar;
    }

    /* renamed from: a */
    private static void m10515a(WheelPicker fVar, Activity activity) {
        fVar.mo23121a(R$style.BottomAnimStyle);
        fVar.mo23134a(false);
        fVar.mo23113c(false);
        fVar.mo23117i(ConvertUtils.m1055a(20.0f));
        fVar.mo23135b(17039360);
        fVar.mo23137c(activity.getResources().getColor(R$color.font_color));
        fVar.mo23138d(R$string.confirm);
        fVar.mo23115g(activity.getResources().getColor(R$color.font_color));
        fVar.mo23139e(activity.getResources().getColor(R$color.font_color));
        fVar.mo23114f(activity.getResources().getColor(R$color.blue_light));
        fVar.mo23116h(3);
    }
}
