package com.heimavista.fiedorasport.p199j;

import android.content.ClipData;
import android.content.ClipboardManager;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.j.e */
public final class ClipboardUtils {
    /* renamed from: a */
    public static void m13411a(CharSequence charSequence) {
        ClipboardManager clipboardManager = (ClipboardManager) HvApp.m13010c().getSystemService("clipboard");
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(ClipData.newPlainText("text", charSequence));
        }
    }
}
