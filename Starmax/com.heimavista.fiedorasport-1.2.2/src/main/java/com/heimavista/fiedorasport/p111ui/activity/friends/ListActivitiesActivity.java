package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.entity.data.ActivitiesItem;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.InputDialog;
import com.heimavista.fiedorasport.p111ui.adapter.ListActivitiesAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.ListActivitiesActivity */
public class ListActivitiesActivity extends BaseActivity {

    /* renamed from: b0 */
    private ListActivitiesAdapter f8084b0;

    /* renamed from: c0 */
    private RecyclerView f8085c0;

    /* renamed from: u */
    private void m13507u() {
        InputDialog gVar = new InputDialog(this);
        gVar.mo22854a(1);
        gVar.mo22861b(getString(R$string.activity_name));
        gVar.mo22857a(getString(R$string.activity_hint));
        gVar.mo22856a(new C4242f(this));
        gVar.mo22855a(new C4241e(this));
        gVar.mo22858a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
    }

    /* renamed from: a */
    public /* synthetic */ void mo24382a(View view, ActivitiesItem aVar, int i) {
        if (aVar.f6343a != 0) {
            Intent intent = new Intent(this, ListChatActivity.class);
            intent.putExtra("title", aVar.f6345c);
            mo22728c(intent);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8085c0 = (RecyclerView) findViewById(R$id.recyclerView);
        this.f8084b0 = new ListActivitiesAdapter(this);
        this.f8084b0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4240d(this));
        this.f8085c0.setAdapter(this.f8084b0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22731d() {
        return R$mipmap.icon_close;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_list_common_with_divider;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.activity);
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    /* renamed from: k */
    public int mo22740k() {
        return 17760263;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 16908331, 0, "").setIcon(R$mipmap.icon_add).setShowAsAction(1);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() == 16908331) {
            m13507u();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24381a(View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        if (!z || TextUtils.isEmpty(charSequence)) {
            alertDialog.cancel();
        } else {
            mo22719b(new Intent(this, SelectFriendsActivity.class), C4243g.f8118a);
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m13506a(int i, Intent intent) {
        if (intent != null && intent.hasExtra("userNbrList")) {
            intent.getStringArrayListExtra("userNbrList");
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24380a(DialogInterface dialogInterface) {
        hideSoftKeyboard(this.f8085c0);
    }
}
