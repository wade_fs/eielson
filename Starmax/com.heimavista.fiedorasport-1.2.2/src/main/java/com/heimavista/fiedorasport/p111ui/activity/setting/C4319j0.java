package com.heimavista.fiedorasport.p111ui.activity.setting;

import com.blankj.utilcode.util.LogUtils;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.heimavista.fiedorasport.p109h.BandDataManager;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.j0 */
/* compiled from: EditWatchFaceActivity */
class C4319j0 implements C1275q {

    /* renamed from: a */
    final /* synthetic */ EditWatchFaceActivity f8315a;

    C4319j0(EditWatchFaceActivity editWatchFaceActivity) {
        this.f8315a = editWatchFaceActivity;
    }

    /* renamed from: a */
    public void mo11034a(int i) {
        if (this.f8315a.f8237l0 != null) {
            this.f8315a.f8237l0.setProgress(i);
        }
        LogUtils.m1139c("mady crpBle watchFaceTrans onTransProgressChanged progress=" + i);
    }

    /* renamed from: b */
    public void mo11035b() {
        this.f8315a.m13784u();
        LogUtils.m1139c("mady crpBle watchFaceTrans onTransProgressStarting");
    }

    public void onError(String str) {
        if (this.f8315a.f8237l0 != null) {
            this.f8315a.f8237l0.dismiss();
        }
        this.f8315a.mo22730c(str);
        LogUtils.m1139c("mady crpBle watchFaceTrans onError " + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity.a(com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity, boolean):boolean
     arg types: [com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity, int]
     candidates:
      com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity.a(android.content.DialogInterface, int):void
      com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity.a(android.widget.RadioGroup, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity.a(com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity, boolean):boolean */
    /* renamed from: a */
    public void mo11033a() {
        if (this.f8315a.f8237l0 != null) {
            this.f8315a.f8237l0.dismiss();
        }
        boolean unused = this.f8315a.f8240o0 = true;
        BandDataManager.m10561c(this.f8315a.f8239n0);
        this.f8315a.m13785v();
        LogUtils.m1139c("mady crpBle watchFaceTrans onTransCompleted");
    }
}
