package com.heimavista.fiedorasport.p111ui.fragment.login;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.base.BaseListViewAdapter;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p111ui.activity.login.LoginActivity;
import java.util.ArrayList;
import p119e.p189e.p192c.HvAppConfig;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.login.ThirdPartLoginFragment */
public class ThirdPartLoginFragment extends BaseFragment<LoginActivity> {

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.login.ThirdPartLoginFragment$a */
    class C4484a implements AdapterView.OnItemClickListener {

        /* renamed from: P */
        final /* synthetic */ C4485b f8881P;

        C4484a(C4485b bVar) {
            this.f8881P = bVar;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            int i2 = ((C4487c) this.f8881P.getItem(i)).f8885a;
            if (i2 == R$mipmap.icon_fb) {
                ((LoginActivity) ThirdPartLoginFragment.this.mo22771c()).mo23013u();
            } else if (i2 == R$mipmap.icon_line) {
                ((LoginActivity) ThirdPartLoginFragment.this.mo22771c()).mo23015w();
            } else if (i2 == R$mipmap.icon_wechat) {
                ((LoginActivity) ThirdPartLoginFragment.this.mo22771c()).mo24450z();
            } else if (i2 == R$mipmap.icon_google) {
                ((LoginActivity) ThirdPartLoginFragment.this.mo22771c()).mo24450z();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.login.ThirdPartLoginFragment$c */
    private class C4487c {

        /* renamed from: a */
        int f8885a;

        /* renamed from: b */
        String f8886b;

        C4487c(ThirdPartLoginFragment thirdPartLoginFragment, int i, String str) {
            this.f8885a = i;
            this.f8886b = str;
        }
    }

    /* renamed from: i */
    public static ThirdPartLoginFragment m14630i() {
        Bundle bundle = new Bundle();
        ThirdPartLoginFragment thirdPartLoginFragment = new ThirdPartLoginFragment();
        thirdPartLoginFragment.setArguments(bundle);
        return thirdPartLoginFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        ListView listView = (ListView) view.findViewById(R$id.listView);
        C4485b bVar = new C4485b(this, getContext());
        ArrayList arrayList = new ArrayList();
        String lowerCase = HvAppConfig.m13024a().mo24165a("fs-thirdPart").toLowerCase();
        if (lowerCase.contains("wx")) {
            arrayList.add(new C4487c(this, R$mipmap.icon_wechat, getString(R$string.login_by_wechat)));
        }
        if (lowerCase.contains("fb")) {
            arrayList.add(new C4487c(this, R$mipmap.icon_fb, getString(R$string.login_by_facebook)));
        }
        if (lowerCase.contains("line")) {
            arrayList.add(new C4487c(this, R$mipmap.icon_line_btn_base, getString(R$string.login_by_line)));
        }
        if (lowerCase.contains("gp")) {
            arrayList.add(new C4487c(this, R$mipmap.icon_google, getString(R$string.login_by_google)));
        }
        bVar.mo22811a(arrayList);
        listView.setAdapter((ListAdapter) bVar);
        listView.setOnItemClickListener(new C4484a(bVar));
        mo22770b(R$id.btnMobileLogin);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fragment_login_third_part;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.btnMobileLogin) {
            ((LoginActivity) mo22771c()).mo24447e(1);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.login.ThirdPartLoginFragment$b */
    class C4485b extends BaseListViewAdapter<C4487c, C4486a> {

        /* renamed from: com.heimavista.fiedorasport.ui.fragment.login.ThirdPartLoginFragment$b$a */
        class C4486a extends BaseListViewAdapter.C3692a {

            /* renamed from: c */
            ImageView f8883c = ((ImageView) mo22817a(R$id.icon));

            /* renamed from: d */
            TextView f8884d = ((TextView) mo22817a(R$id.tvName));

            C4486a(C4485b bVar, ViewGroup viewGroup, int i) {
                super(bVar, viewGroup, i);
            }
        }

        C4485b(ThirdPartLoginFragment thirdPartLoginFragment, Context context) {
            super(context);
        }

        @NonNull
        /* renamed from: a */
        public C4486a mo22809a(@NonNull ViewGroup viewGroup, int i) {
            return new C4486a(this, viewGroup, R$layout.layout_list_item_login_option);
        }

        /* renamed from: a */
        public void mo22810a(@NonNull C4486a aVar, int i) {
            aVar.f8883c.setImageResource(((C4487c) getItem(i)).f8885a);
            aVar.f8884d.setText(((C4487c) getItem(i)).f8886b);
        }
    }
}
