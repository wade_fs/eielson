package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.buddy.HvApiGetSportData;
import com.heimavista.api.sport.HvApiGetLikeCnt;
import com.heimavista.api.sport.HvApiGetLikeList;
import com.heimavista.api.sport.HvApiLike;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.entity.band.TickType;
import com.heimavista.entity.data.SportBean;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p111ui.activity.HomeActivity;
import com.heimavista.fiedorasport.p111ui.activity.friends.ListAddFriendsActivity;
import com.heimavista.fiedorasport.p111ui.activity.sport.SportDetailActivity;
import com.heimavista.fiedorasport.p111ui.adapter.ListDynamicsAdapter;
import com.heimavista.utils.ParamJsonData;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.p206a.RefreshLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import p119e.p189e.p193d.p194i.SportDataDb;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p196k.SportLikeUserDb;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddySportDynamicFragment */
public class BuddySportDynamicFragment extends BaseFragment<HomeActivity> {

    /* renamed from: W */
    private SmartRefreshLayout f8751W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public ListDynamicsAdapter f8752X;

    /* renamed from: Y */
    private List<SportBean> f8753Y;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public int f8754Z = 0;

    /* renamed from: a0 */
    private long f8755a0 = 0;

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddySportDynamicFragment$a */
    class C4439a implements ListDynamicsAdapter.C4404a {

        /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddySportDynamicFragment$a$a */
        class C4440a implements OnResultListener<Void> {
            C4440a() {
            }

            /* renamed from: a */
            public void mo22380a(Void voidR) {
                SportInfo b = ((SportBean) BuddySportDynamicFragment.this.f8752X.getItem(BuddySportDynamicFragment.this.f8754Z)).mo22672b();
                b.f6282m++;
                BuddySportDynamicFragment.this.m14490b(b.f6270a);
            }

            /* renamed from: a */
            public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
                BuddySportDynamicFragment.this.mo22768b();
                BuddySportDynamicFragment.this.mo22761a(str);
            }
        }

        C4439a() {
        }

        /* renamed from: a */
        public void mo24641a(int i, SportBean gVar) {
            Constant.m10311a("運動標籤點擊點擊");
            SportInfo b = gVar.mo22672b();
            Intent intent = new Intent(BuddySportDynamicFragment.this.getContext(), SportDetailActivity.class);
            intent.putExtra("userNbr", b.f6270a);
            intent.putExtra("sportId", b.f6272c);
            BuddySportDynamicFragment buddySportDynamicFragment = BuddySportDynamicFragment.this;
            int i2 = b.f6273d;
            intent.putExtra("title", buddySportDynamicFragment.getString(i2 == 0 ? R$string.run : i2 == 1 ? R$string.walking : R$string.cycling));
            BuddySportDynamicFragment.this.startActivity(intent);
        }

        /* renamed from: b */
        public void mo24642b(int i, SportBean gVar) {
        }

        /* renamed from: c */
        public void mo24643c(int i, SportBean gVar) {
            int unused = BuddySportDynamicFragment.this.f8754Z = i;
            if (SportLikeUserDb.m13374k(gVar.mo22672b().f6272c)) {
                mo24641a(i, gVar);
                return;
            }
            Constant.m10311a("給添加好友點贊");
            BuddySportDynamicFragment.this.mo22766a(false);
            new HvApiLike().request(gVar.mo22672b().f6270a, gVar.mo22672b().f6272c, new C4440a());
        }

        /* renamed from: d */
        public void mo24644d(int i, SportBean gVar) {
            Constant.m10311a("添加好友點擊");
            BuddySportDynamicFragment.this.mo22769b(ListAddFriendsActivity.class);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.BuddySportDynamicFragment$b */
    class C4441b implements OnResultListener<ParamJsonData> {
        C4441b() {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            BuddySportDynamicFragment.this.mo22768b();
            BuddySportDynamicFragment.this.mo22775g();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            BuddySportDynamicFragment.this.mo22768b();
            BuddySportDynamicFragment.this.mo22775g();
        }
    }

    /* renamed from: i */
    private void m14491i() {
        new HvApiGetLikeList().request();
        for (BuddyListDb aVar : BuddyListDb.m13246G()) {
            if (aVar.mo24312y() == 9) {
                new HvApiGetSportData().request(aVar.mo24311x());
            }
        }
    }

    /* renamed from: j */
    public static BuddySportDynamicFragment m14492j() {
        Bundle bundle = new Bundle();
        BuddySportDynamicFragment buddySportDynamicFragment = new BuddySportDynamicFragment();
        buddySportDynamicFragment.setArguments(bundle);
        return buddySportDynamicFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public int mo22772d() {
        return R$layout.fs_fragment_buddy_sport_dynamics;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public String[] mo22773e() {
        return new String[]{HvApiGetLikeList.actionSuccess, HvApiGetLikeList.actionFail, HvApiGetLikeList.actionNewLikes};
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo22775g() {
        List<SportBean> list = this.f8753Y;
        if (list == null) {
            this.f8753Y = new ArrayList();
        } else {
            list.clear();
        }
        for (BuddyListDb aVar : BuddyListDb.m13246G()) {
            if (aVar.mo24312y() == 9) {
                for (SportInfo dVar : new SportDataDb().mo24270h(aVar.mo24311x())) {
                    List<SportBean> list2 = this.f8753Y;
                    int i = 2;
                    if (dVar.f6273d == 2) {
                        i = 3;
                    }
                    list2.add(new SportBean(i, dVar.f6275f, dVar));
                }
            }
        }
        for (SportInfo dVar2 : new SportDataDb().mo24271p()) {
            for (SportLikeUserDb aVar2 : SportLikeUserDb.m13373j(dVar2.f6272c)) {
                this.f8753Y.add(new SportBean(1, aVar2.mo24344p(), aVar2.mo24232o(), dVar2));
            }
        }
        if (!this.f8753Y.isEmpty()) {
            Collections.sort(this.f8753Y);
            this.f8752X.mo22792b((List) this.f8753Y);
        } else if (this.f8752X.getItemCount() == 0) {
            this.f8752X.mo22783a(0, new SportBean(0, null));
        } else {
            this.f8752X.mo22790b(0, new SportBean(0, null));
        }
    }

    public void onResume() {
        super.onResume();
        long currentTimeMillis = System.currentTimeMillis();
        if (System.currentTimeMillis() - this.f8755a0 > 120000) {
            this.f8755a0 = currentTimeMillis;
            this.f8751W.mo26056b();
            TickManager.m10524b(TickType.LIKE_CNT, currentTimeMillis);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m14490b(String str) {
        new HvApiGetLikeCnt().request(str, new C4441b());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22758a(View view, Bundle bundle) {
        this.f8755a0 = TickManager.m10517a(TickType.LIKE_CNT);
        this.f8751W = (SmartRefreshLayout) view.findViewById(R$id.refreshLayout);
        this.f8751W.mo26048a(new C4464w(this));
        this.f8752X = new ListDynamicsAdapter(getContext());
        this.f8752X.mo24636a((ListDynamicsAdapter.C4404a) new C4439a());
        ((RecyclerView) view.findViewById(R$id.recyclerView)).setAdapter(this.f8752X);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24703a(RefreshLayout jVar) {
        m14491i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22754a(Context context, Intent intent) {
        super.mo22754a(context, intent);
        String action = intent.getAction();
        if (HvApiGetLikeList.actionNewLikes.equals(action)) {
            mo22768b();
            this.f8751W.mo26062d();
            mo22775g();
        } else if (HvApiGetLikeList.actionSuccess.equals(action) || HvApiGetLikeList.actionFail.equals(action)) {
            this.f8751W.mo26062d();
        }
    }
}
