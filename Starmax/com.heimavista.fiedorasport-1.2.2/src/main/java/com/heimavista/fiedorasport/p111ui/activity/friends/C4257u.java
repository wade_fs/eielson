package com.heimavista.fiedorasport.p111ui.activity.friends;

import com.heimavista.widget.dialog.BaseDialog;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.u */
/* compiled from: lambda */
public final /* synthetic */ class C4257u implements BaseDialog.C4621m {

    /* renamed from: a */
    private final /* synthetic */ SelectFriendsActivity f8136a;

    /* renamed from: b */
    private final /* synthetic */ String f8137b;

    public /* synthetic */ C4257u(SelectFriendsActivity selectFriendsActivity, String str) {
        this.f8136a = selectFriendsActivity;
        this.f8137b = str;
    }

    /* renamed from: a */
    public final void mo23034a(boolean z) {
        this.f8136a.mo24408b(this.f8137b, z);
    }
}
