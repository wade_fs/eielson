package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.SportBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.m */
/* compiled from: lambda */
public final /* synthetic */ class C4423m implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListDynamicsAdapter f8685P;

    /* renamed from: Q */
    private final /* synthetic */ int f8686Q;

    /* renamed from: R */
    private final /* synthetic */ SportBean f8687R;

    public /* synthetic */ C4423m(ListDynamicsAdapter listDynamicsAdapter, int i, SportBean gVar) {
        this.f8685P = listDynamicsAdapter;
        this.f8686Q = i;
        this.f8687R = gVar;
    }

    public final void onClick(View view) {
        this.f8685P.mo24634a(this.f8686Q, this.f8687R, view);
    }
}
