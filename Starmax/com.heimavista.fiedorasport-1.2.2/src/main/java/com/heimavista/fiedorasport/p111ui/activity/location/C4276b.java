package com.heimavista.fiedorasport.p111ui.activity.location;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.LatLng;
import com.heimavista.fiedorasport.p199j.AvatarUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.location.b */
/* compiled from: lambda */
public final /* synthetic */ class C4276b implements AvatarUtils.C4219e {

    /* renamed from: a */
    private final /* synthetic */ LocationActivity f8191a;

    /* renamed from: b */
    private final /* synthetic */ LatLng f8192b;

    public /* synthetic */ C4276b(LocationActivity locationActivity, LatLng latLng) {
        this.f8191a = locationActivity;
        this.f8192b = latLng;
    }

    /* renamed from: a */
    public final void mo24351a(Bitmap bitmap) {
        this.f8191a.mo24437a(this.f8192b, bitmap);
    }
}
