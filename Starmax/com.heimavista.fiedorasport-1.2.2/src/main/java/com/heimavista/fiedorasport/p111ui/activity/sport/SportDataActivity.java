package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.google.android.exoplayer2.C1750C;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.LocManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import java.util.List;
import java.util.Locale;
import p119e.p189e.p193d.p194i.SportDataDb;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportDataActivity */
public class SportDataActivity extends BaseActivity {

    /* renamed from: b0 */
    private TextView f8445b0;

    /* renamed from: c0 */
    private TextView f8446c0;

    /* renamed from: d0 */
    private TextView f8447d0;

    /* renamed from: e0 */
    private View f8448e0;

    /* renamed from: f0 */
    private ImageView f8449f0;

    /* renamed from: g0 */
    private ImageView f8450g0;

    /* renamed from: h0 */
    private ImageView f8451h0;

    /* renamed from: i0 */
    private RecyclerView f8452i0;
    /* access modifiers changed from: private */

    /* renamed from: j0 */
    public SportAdapter f8453j0;
    /* access modifiers changed from: private */

    /* renamed from: k0 */
    public int f8454k0 = 0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.sport.SportDataActivity$a */
    class C4353a extends ThreadUtils.C0877e<List<SportInfo>> {
        C4353a() {
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo9795a(Object obj) {
            mo24552a((List<SportInfo>) ((List) obj));
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo24552a(List<SportInfo> list) {
            SportDataActivity.this.f8453j0.mo22792b((List) list);
            SportDataActivity.this.f8453j0.notifyDataSetChanged();
        }

        /* renamed from: b */
        public List<SportInfo> mo9798b() {
            return new SportDataDb().mo24269b(SportDataActivity.this.f8454k0);
        }
    }

    /* renamed from: e */
    private void m14053e(@IdRes int i) {
        this.f8454k0 = i == R$id.ivRunning ? 0 : i == R$id.ivWalking ? 1 : 2;
        m14056w();
        m14055v();
        int a = ConvertUtils.m1055a(0.0f);
        int a2 = ConvertUtils.m1055a(3.0f);
        int i2 = i == R$id.ivRunning ? a : a2;
        int i3 = i == R$id.ivWalking ? a : a2;
        if (i != R$id.ivCycling) {
            a = a2;
        }
        this.f8449f0.setPadding(i2, i2, i2, i2);
        this.f8450g0.setPadding(i3, i3, i3, i3);
        this.f8451h0.setPadding(a, a, a, a);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.f8448e0.getLayoutParams();
        layoutParams.reset();
        layoutParams.topToTop = -1;
        layoutParams.startToStart = i;
        layoutParams.endToEnd = i;
        this.f8448e0.setLayoutParams(layoutParams);
    }

    /* renamed from: u */
    private boolean m14054u() {
        int checkSelfPermission = ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION");
        int checkSelfPermission2 = Build.VERSION.SDK_INT >= 29 ? ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_BACKGROUND_LOCATION") : 0;
        if (checkSelfPermission == 0 && checkSelfPermission2 == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: v */
    private void m14055v() {
        ThreadUtils.m963c(new C4353a());
    }

    /* renamed from: w */
    private void m14056w() {
        String str;
        String str2;
        String str3;
        SportInfo a = new SportDataDb().mo24266a(this.f8454k0);
        String str4 = "--";
        if (a.f6276g == 0) {
            str = str4;
        } else {
            str = String.format(Locale.getDefault(), "%.2f", Float.valueOf(((float) a.f6277h) / 1000.0f));
        }
        SpanUtils a2 = SpanUtils.m865a(this.f8445b0);
        a2.mo9735a(str);
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_30));
        a2.mo9738c();
        a2.mo9735a(getString(R$string.data_unit_mill));
        a2.mo9736b();
        int i = a.f6276g;
        SpanUtils a3 = SpanUtils.m865a(this.f8446c0);
        int i2 = i / 3600;
        if (i2 > 0) {
            a3.mo9735a(String.valueOf(i2));
            a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
            a3.mo9738c();
            a3.mo9735a(getString(R$string.data_unit_hour));
        }
        if (i == 0) {
            str2 = str4;
        } else {
            str2 = String.valueOf((i / 60) % 60);
        }
        a3.mo9735a(str2);
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9738c();
        a3.mo9735a(getString(R$string.data_unit_min));
        if (i == 0) {
            str3 = str4;
        } else {
            str3 = String.valueOf(i % 60);
        }
        a3.mo9735a(str3);
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9738c();
        a3.mo9735a(getString(R$string.data_unit_second));
        a3.mo9736b();
        if (a.f6279j != 0) {
            str4 = a.f6279j + "";
        }
        SpanUtils a4 = SpanUtils.m865a(this.f8447d0);
        a4.mo9735a(str4);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9738c();
        a4.mo9735a(getString(R$string.data_unit_rate));
        a4.mo9736b();
    }

    /* renamed from: x */
    private void m14057x() {
        boolean z = true;
        boolean z2 = ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0;
        boolean z3 = Build.VERSION.SDK_INT >= 29 && ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_BACKGROUND_LOCATION") == 0;
        if (!z2 || !z3) {
            z = false;
        }
        if (z) {
            SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
            a.mo9783a(getString(R$string.gps_permission_denied));
            a.mo9785a(getString(R$string.goto_open), new C4366k(this));
            a.mo9782a(-2);
            a.mo9786a();
            return;
        }
        m14058y();
    }

    /* renamed from: y */
    private void m14058y() {
        String[] strArr = {"android.permission.ACCESS_COARSE_LOCATION"};
        if (Build.VERSION.SDK_INT >= 29) {
            strArr = new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_BACKGROUND_LOCATION"};
        }
        ActivityCompat.requestPermissions(this, strArr, 34);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.fiedorasport.ui.activity.sport.SportDataActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void */
    /* renamed from: z */
    private void m14059z() {
        if (TextUtils.isEmpty(FSManager.m10323r().mo22836f()) || TextUtils.isEmpty(FSManager.m10323r().mo22842l())) {
            mo22730c(getString(R$string.device_has_not_bind));
            return;
        }
        if (FSManager.m10323r().mo22847q()) {
            if (!JYSDKManager.m10388i().mo22897e()) {
                mo22730c(getString(R$string.device_has_not_connect));
                return;
            }
        } else if (FSManager.m10323r().mo22846p() && !CRPBleManager.m10639j()) {
            mo22730c(getString(R$string.device_has_not_connect));
            return;
        }
        if (!C4222l.m13464c()) {
            SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
            a.mo9783a(getString(R$string.gps_permission_denied));
            a.mo9785a(getString(R$string.goto_open), new C4363h(this));
            a.mo9782a(-2);
            a.mo9786a();
        } else if (m14054u()) {
            mo22709a(getString(R$string.positioning), false);
            LocManager.m10493g().mo22957b();
        } else {
            m14057x();
        }
    }

    /* renamed from: c */
    public /* synthetic */ void mo24551c(View view) {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_data_sport;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public List<String> mo22735g() {
        List<String> g = super.mo22735g();
        g.add("LocManager.action_location_callback");
        return g;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        int i = R$id.ivRunning;
        if (id == i) {
            m14053e(i);
            return;
        }
        int i2 = R$id.ivWalking;
        if (id == i2) {
            m14053e(i2);
            return;
        }
        int i3 = R$id.ivCycling;
        if (id == i3) {
            m14053e(i3);
        } else if (id == R$id.btnSport) {
            m14059z();
        }
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i == 34 && iArr.length > 0) {
            if (iArr[0] == 0) {
                LocManager.m10493g().mo22957b();
                return;
            }
            SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
            a.mo9783a(getString(R$string.loc_permission_denied_tips));
            a.mo9785a(getString(R$string.setting), new C4365j(this));
            a.mo9782a(-2);
            a.mo9786a();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        m14056w();
        m14055v();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        mo22685a(R$id.gad, "BAND-2005", "exad_sport");
        this.f8453j0 = new SportAdapter(this);
        this.f8453j0.mo22784a((BaseRecyclerViewAdapter.C3685c) new C4364i(this));
        this.f8452i0.setAdapter(this.f8453j0);
        SportInfo o = new SportDataDb().mo24232o();
        if (o != null) {
            int i = o.f6273d;
            if (i == 0) {
                m14053e(R$id.ivRunning);
            } else if (i != 2) {
                m14053e(R$id.ivWalking);
            } else {
                m14053e(R$id.ivCycling);
            }
        } else {
            m14053e(R$id.ivWalking);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8445b0 = (TextView) findViewById(R$id.tvSportDistance);
        this.f8446c0 = (TextView) findViewById(R$id.tvSportTime);
        this.f8447d0 = (TextView) findViewById(R$id.tvSportHeart);
        this.f8448e0 = findViewById(R$id.vIndicator);
        this.f8449f0 = (ImageView) findViewById(R$id.ivRunning);
        this.f8450g0 = (ImageView) findViewById(R$id.ivWalking);
        this.f8451h0 = (ImageView) findViewById(R$id.ivCycling);
        this.f8452i0 = (RecyclerView) findViewById(R$id.recyclerView);
        mo22714a(this.f8449f0);
        mo22714a(this.f8450g0);
        mo22714a(this.f8451h0);
        mo22723b(R$id.btnSport);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24549a(View view, SportInfo dVar, int i) {
        Constant.m10311a("點擊查看運動詳情頁");
        Intent intent = new Intent(this, SportDetailActivity.class);
        intent.putExtra("userNbr", dVar.f6270a);
        intent.putExtra("sportId", dVar.f6272c);
        int i2 = dVar.f6273d;
        intent.putExtra("title", getString(i2 == 0 ? R$string.run : i2 == 1 ? R$string.walking : R$string.cycling));
        startActivity(intent);
    }

    /* renamed from: b */
    public /* synthetic */ void mo24550b(View view) {
        m14058y();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24548a(View view) {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", "com.heimavista.fiedorasport.lib", null));
        intent.setFlags(C1750C.ENCODING_PCM_MU_LAW);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22691a(Context context, Intent intent) {
        super.mo22691a(context, intent);
        if (TextUtils.equals("LocManager.action_location_callback", intent.getAction())) {
            mo22717b();
            if (((Location) intent.getParcelableExtra("location")) != null) {
                Object[] objArr = new Object[1];
                int i = this.f8454k0;
                objArr[0] = i == 0 ? "跑步" : i == 1 ? "健走" : "騎行";
                Constant.m10311a(String.format("點擊%s開始運動", objArr));
                intent.setClass(this, SportActivity.class);
                intent.putExtra("type", this.f8454k0);
                startActivity(intent);
                return;
            }
            SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
            a.mo9783a(getString(R$string.positioning_failure));
            a.mo9782a(-1);
            a.mo9786a();
        }
    }
}
