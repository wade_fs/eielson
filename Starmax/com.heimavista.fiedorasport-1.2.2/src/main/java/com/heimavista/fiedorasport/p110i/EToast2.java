package com.heimavista.fiedorasport.p110i;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import java.util.Timer;
import java.util.TimerTask;

/* renamed from: com.heimavista.fiedorasport.i.a */
public class EToast2 {

    /* renamed from: f */
    private static View f6580f;

    /* renamed from: g */
    private static Timer f6581g;

    /* renamed from: h */
    private static Toast f6582h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static Handler f6583i;

    /* renamed from: a */
    private WindowManager f6584a;

    /* renamed from: b */
    private Long f6585b;

    /* renamed from: c */
    private WindowManager.LayoutParams f6586c;

    /* renamed from: d */
    private Toast f6587d;

    /* renamed from: e */
    private CharSequence f6588e;

    /* renamed from: com.heimavista.fiedorasport.i.a$a */
    /* compiled from: EToast2 */
    class C3744a extends Handler {
        C3744a() {
        }

        public void handleMessage(Message message) {
            EToast2.this.mo22967a();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.i.a$b */
    /* compiled from: EToast2 */
    class C3745b extends TimerTask {
        C3745b(EToast2 aVar) {
        }

        public void run() {
            EToast2.f6583i.sendEmptyMessage(1);
        }
    }

    private EToast2(Context context, CharSequence charSequence, int i) {
        this(context, null, charSequence, i);
    }

    /* renamed from: a */
    public static EToast2 m10676a(Context context, String str, int i) {
        return new EToast2(context, str, i);
    }

    /* renamed from: b */
    public void mo22971b() {
        if (f6582h == null) {
            f6582h = this.f6587d;
            this.f6584a.addView(f6580f, this.f6586c);
            f6581g = new Timer();
        } else {
            f6581g.cancel();
            f6582h.setText(this.f6588e);
        }
        f6581g = new Timer();
        f6581g.schedule(new C3745b(this), this.f6585b.longValue());
    }

    private EToast2(Context context, View view, CharSequence charSequence, int i) {
        Long valueOf = Long.valueOf((long) AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        this.f6585b = valueOf;
        this.f6584a = (WindowManager) context.getSystemService("window");
        this.f6588e = charSequence;
        if (i == 0) {
            this.f6585b = valueOf;
        } else if (i == 1) {
            this.f6585b = 3500L;
        }
        if (f6582h == null) {
            this.f6587d = Toast.makeText(context, charSequence, 0);
            if (view != null) {
                this.f6587d.setView(view);
            }
            f6580f = this.f6587d.getView();
            this.f6586c = new WindowManager.LayoutParams();
            WindowManager.LayoutParams layoutParams = this.f6586c;
            layoutParams.height = -2;
            layoutParams.width = -2;
            layoutParams.format = -3;
            layoutParams.windowAnimations = -1;
            layoutParams.setTitle("EToast2");
            WindowManager.LayoutParams layoutParams2 = this.f6586c;
            layoutParams2.flags = 152;
            layoutParams2.gravity = 81;
            layoutParams2.y = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        }
        if (f6583i == null) {
            f6583i = new C3744a();
        }
    }

    /* renamed from: a */
    public void mo22967a() {
        try {
            this.f6584a.removeView(f6580f);
        } catch (IllegalArgumentException unused) {
        }
        f6581g.cancel();
        f6582h.cancel();
        f6581g = null;
        this.f6587d = null;
        f6582h = null;
        f6580f = null;
        f6583i = null;
    }

    /* renamed from: a */
    public void mo22970a(CharSequence charSequence) {
        this.f6587d.setText(charSequence);
    }

    /* renamed from: a */
    public void mo22969a(int i, int i2, int i3) {
        this.f6587d.setGravity(i, i2, i3);
    }

    /* renamed from: a */
    public void mo22968a(int i) {
        this.f6587d.setDuration(i);
    }
}
