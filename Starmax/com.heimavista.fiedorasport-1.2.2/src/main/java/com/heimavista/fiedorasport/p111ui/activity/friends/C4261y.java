package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.app.AlertDialog;
import android.view.View;
import com.heimavista.api.OnResultListener;
import com.heimavista.utils.ParamJsonData;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.y */
/* compiled from: FriendDataActivity */
class C4261y implements OnResultListener<ParamJsonData> {

    /* renamed from: a */
    final /* synthetic */ AlertDialog f8141a;

    /* renamed from: b */
    final /* synthetic */ CharSequence f8142b;

    /* renamed from: c */
    final /* synthetic */ View f8143c;

    /* renamed from: d */
    final /* synthetic */ FriendDataActivity f8144d;

    C4261y(FriendDataActivity friendDataActivity, AlertDialog alertDialog, CharSequence charSequence, View view) {
        this.f8144d = friendDataActivity;
        this.f8141a = alertDialog;
        this.f8142b = charSequence;
        this.f8143c = view;
    }

    /* renamed from: a */
    public void mo22380a(ParamJsonData fVar) {
        this.f8141a.cancel();
        this.f8144d.f8053c0.setText(this.f8142b.toString());
        String unused = this.f8144d.f8074x0 = this.f8142b.toString();
    }

    /* renamed from: a */
    public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        this.f8144d.mo22730c(str);
        this.f8143c.setEnabled(true);
    }
}
