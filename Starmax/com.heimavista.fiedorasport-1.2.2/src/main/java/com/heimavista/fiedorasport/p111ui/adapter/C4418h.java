package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.h */
/* compiled from: lambda */
public final /* synthetic */ class C4418h implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8670P;

    /* renamed from: Q */
    private final /* synthetic */ int f8671Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8672R;

    public /* synthetic */ C4418h(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8670P = listBuddyDynamicAdapter;
        this.f8671Q = i;
        this.f8672R = dVar;
    }

    public final void onClick(View view) {
        this.f8670P.mo24621d(this.f8671Q, this.f8672R, view);
    }
}
