package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.band.HvApiGetGadId;
import com.heimavista.api.buddy.HvApiDelBuddy;
import com.heimavista.api.buddy.HvApiSetRemark;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$dimen;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.InputDialog;
import com.heimavista.fiedorasport.p111ui.activity.setting.SettingShareOptionsActivity;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.HvGadViewConfig;
import com.heimavista.utils.ParamJsonData;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p139g.OnChartValueSelectedListener;
import p119e.p189e.p193d.p194i.CurStepDataDb;
import p119e.p189e.p193d.p194i.HeartRateDataDb;
import p119e.p189e.p193d.p195j.BuddyListDb;
import p119e.p189e.p193d.p195j.C4208d;
import p119e.p189e.p193d.p195j.C4209e;
import p119e.p189e.p193d.p195j.UserInfoDb;
import p119e.p189e.p197e.p198a.ChartSleepData;
import p119e.p189e.p197e.p198a.ChartStepData;
import p119e.p189e.p219h.HttpCancelable;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity */
public class FriendDataActivity extends BaseActivity {

    /* renamed from: b0 */
    private ImageView f8052b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public TextView f8053c0;

    /* renamed from: d0 */
    private TextView f8054d0;

    /* renamed from: e0 */
    private TextView f8055e0;

    /* renamed from: f0 */
    private TextView f8056f0;

    /* renamed from: g0 */
    private TextView f8057g0;

    /* renamed from: h0 */
    private TextView f8058h0;

    /* renamed from: i0 */
    private TextView f8059i0;

    /* renamed from: j0 */
    private TextView f8060j0;

    /* renamed from: k0 */
    private TextView f8061k0;

    /* renamed from: l0 */
    private TextView f8062l0;

    /* renamed from: m0 */
    private TextView f8063m0;

    /* renamed from: n0 */
    private TextView f8064n0;

    /* renamed from: o0 */
    private TextView f8065o0;

    /* renamed from: p0 */
    private TextView f8066p0;

    /* renamed from: q0 */
    private TextView f8067q0;

    /* renamed from: r0 */
    private BarChart f8068r0;

    /* renamed from: s0 */
    private BarChart f8069s0;
    /* access modifiers changed from: private */

    /* renamed from: t0 */
    public List<ChartSleepData> f8070t0;
    /* access modifiers changed from: private */

    /* renamed from: u0 */
    public List<ChartStepData> f8071u0;
    /* access modifiers changed from: private */

    /* renamed from: v0 */
    public String f8072v0;

    /* renamed from: w0 */
    private int f8073w0 = 10;
    /* access modifiers changed from: private */

    /* renamed from: x0 */
    public String f8074x0 = null;
    /* access modifiers changed from: private */

    /* renamed from: y0 */
    public boolean f8075y0 = false;

    /* renamed from: z0 */
    private HttpCancelable f8076z0;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity$a */
    class C4224a implements OnResultListener<ParamJsonData> {
        C4224a() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            if (fVar.mo25334d().has("gadId")) {
                String optString = fVar.mo25334d().optString("gadId");
                if (!TextUtils.isEmpty(optString)) {
                    FriendDataActivity.this.getSupportFragmentManager().beginTransaction().replace(R$id.gad, HvGad.m14915i().mo25016f().mo25035a(optString, "BAND-3001", new HvGadViewConfig.C4502b(0, 0).mo24895a()), "ex2ad").commit();
                }
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity$b */
    class C4225b implements C4632i<String> {
        C4225b() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void
         arg types: [java.lang.String, int, com.heimavista.fiedorasport.ui.activity.friends.a]
         candidates:
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int, long):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, java.lang.String, java.lang.String):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, android.os.Bundle, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, java.lang.String, int):void
          com.heimavista.fiedorasport.h.y.a(int, int, long):void
          com.heimavista.fiedorasport.h.y.a(long, int, int):void
          com.heimavista.fiedorasport.h.y.a(java.lang.String, java.lang.String, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean, com.heimavista.widget.dialog.BaseDialog$m):void */
        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            String str2;
            if (i == 0) {
                Intent intent = new Intent(FriendDataActivity.this, SettingShareOptionsActivity.class);
                UserInfoDb p = UserInfoDb.m13344p(FriendDataActivity.this.f8072v0);
                if (p == null) {
                    str2 = "";
                } else {
                    str2 = p.mo24338x();
                }
                intent.putExtra("userName", str2);
                intent.putExtra("userNbr", FriendDataActivity.this.f8072v0);
                FriendDataActivity.this.startActivity(intent);
            } else if (i == 1) {
                FriendDataActivity.this.m13484w();
            } else {
                FriendDataActivity friendDataActivity = FriendDataActivity.this;
                friendDataActivity.mo22710a(friendDataActivity.getString(R$string.del_friend_tips), false, (BaseDialog.C4621m) new C4237a(this));
            }
        }

        /* renamed from: a */
        public /* synthetic */ void mo24378a(boolean z) {
            if (z) {
                FriendDataActivity friendDataActivity = FriendDataActivity.this;
                friendDataActivity.m13480d(friendDataActivity.f8072v0);
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity$c */
    class C4226c implements OnResultListener<ParamJsonData> {
        C4226c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.github.mikephil.charting.charts.BarChart, boolean):void
          com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, com.heimavista.entity.band.StepInfo):void
          com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, com.heimavista.entity.band.b):void
          com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, java.lang.String):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, boolean):boolean */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            FriendDataActivity.this.mo22717b();
            boolean unused = FriendDataActivity.this.f8075y0 = true;
            FriendDataActivity.this.finish();
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
            FriendDataActivity.this.mo22717b();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity$d */
    class C4227d extends ValueFormatter {

        /* renamed from: a */
        final /* synthetic */ boolean f8080a;

        C4227d(boolean z) {
            this.f8080a = z;
        }

        /* renamed from: a */
        public String mo23221a(float f, AxisBase aVar) {
            if (this.f8080a) {
                int i = (int) f;
                if (i < FriendDataActivity.this.f8070t0.size()) {
                    return ((ChartSleepData) FriendDataActivity.this.f8070t0.get(i)).f8012c;
                }
            } else {
                int i2 = (int) f;
                if (i2 < FriendDataActivity.this.f8071u0.size()) {
                    return ((ChartStepData) FriendDataActivity.this.f8071u0.get(i2)).f8015b;
                }
            }
            return super.mo23221a(f, aVar);
        }
    }

    /* renamed from: u */
    private void m13482u() {
        Matrix matrix = new Matrix();
        matrix.postScale(1.0f, 1.0f);
        this.f8069s0.getViewPortHandler().mo23444a(matrix, this.f8069s0, false);
        this.f8069s0.mo13023a(1000);
        this.f8069s0.mo12953a((float) (this.f8073w0 - 1));
        this.f8069s0.getXAxis().mo13203c(this.f8073w0);
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        for (int i = 0; i < this.f8070t0.size(); i++) {
            ChartSleepData aVar = this.f8070t0.get(i);
            if (aVar.f8010a.f6257f > 0) {
                z = true;
            }
            float f = aVar.f8011b;
            SleepDayInfo bVar = aVar.f8010a;
            arrayList.add(new BarEntry(f, new float[]{(float) bVar.f6254c, (float) bVar.f6256e, (float) bVar.f6255d}));
        }
        if (!z) {
            this.f8069s0.getDescription().mo13233a(true);
        } else {
            this.f8069s0.getDescription().mo13233a(false);
            BarDataSet bVar2 = new BarDataSet(arrayList, "");
            bVar2.mo13342a(false);
            bVar2.mo13344b(false);
            bVar2.mo13343a(getResources().getColor(R$color.sleep_deep), getResources().getColor(R$color.sleep_awake), getResources().getColor(R$color.sleep_light));
            bVar2.mo13334g(getResources().getColor(R$color.sleep_line_select));
            bVar2.mo13326h(PsExtractor.VIDEO_STREAM_MASK);
            bVar2.mo13347c(true);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(bVar2);
            BarData aVar2 = new BarData(arrayList2);
            aVar2.mo13377a(10.0f);
            aVar2.mo13379a(Typeface.DEFAULT);
            if (arrayList.size() < 10) {
                aVar2.mo13322b(0.5f);
            } else {
                aVar2.mo13322b(0.9f);
            }
            this.f8069s0.setData(aVar2);
            this.f8069s0.mo13031b(1000);
            this.f8069s0.mo13023a(0);
        }
        this.f8069s0.invalidate();
    }

    /* renamed from: v */
    private void m13483v() {
        this.f8068r0.getXAxis().mo13203c(this.f8073w0);
        Matrix matrix = new Matrix();
        matrix.postScale(1.0f, 1.0f);
        this.f8068r0.getViewPortHandler().mo23444a(matrix, this.f8068r0, false);
        this.f8068r0.mo13023a(1000);
        this.f8068r0.mo12953a((float) (this.f8073w0 - 1));
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        for (int i = 0; i < this.f8071u0.size(); i++) {
            ChartStepData bVar = this.f8071u0.get(i);
            if (bVar.f8014a.f6245S > 0) {
                z = true;
            }
            BarEntry barEntry = new BarEntry(bVar.f8018e, bVar.f8017d);
            LogUtils.m1139c("madystep " + bVar.f8015b + " " + bVar.f8017d);
            arrayList.add(barEntry);
        }
        if (!z) {
            this.f8068r0.getDescription().mo13233a(true);
        } else {
            this.f8068r0.getDescription().mo13233a(false);
            BarDataSet bVar2 = new BarDataSet(arrayList, "");
            bVar2.mo13342a(false);
            bVar2.mo13344b(false);
            bVar2.mo13343a(getResources().getColor(R$color.sleep_deep));
            bVar2.mo13334g(getResources().getColor(R$color.sleep_line_select));
            bVar2.mo13326h(PsExtractor.VIDEO_STREAM_MASK);
            bVar2.mo13347c(true);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(bVar2);
            BarData aVar = new BarData(arrayList2);
            aVar.mo13377a(10.0f);
            aVar.mo13379a(Typeface.DEFAULT);
            if (arrayList.size() < 10) {
                aVar.mo13322b(0.5f);
            } else {
                aVar.mo13322b(0.9f);
            }
            this.f8068r0.setData(aVar);
            this.f8068r0.mo13031b(1000);
        }
        this.f8068r0.invalidate();
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public void m13484w() {
        InputDialog gVar = new InputDialog(this);
        gVar.mo22854a(1);
        gVar.mo22861b(getString(R$string.friend_label));
        gVar.mo22857a(getString(R$string.input_label));
        gVar.mo22856a(new C4239c(this));
        gVar.mo22855a(new C4238b(this));
        gVar.mo22858a();
    }

    /* renamed from: x */
    private void m13485x() {
        String[] strArr = {getString(R$string.setting_share_permission), getString(R$string.friend_label), getString(R$string.del_friend)};
        C4629g gVar = new C4629g(this);
        gVar.mo25456a(strArr);
        gVar.mo25453a(new C4225b());
        gVar.mo25418b().show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_friend_data;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo22744o() {
        return true;
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.ivSetting) {
            Constant.m10311a("朋友標籤點擊");
            m13485x();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        Intent intent;
        if (this.f8075y0) {
            intent = new Intent();
            intent.putExtra("actionDelete", true);
        } else if (!TextUtils.isEmpty(this.f8074x0)) {
            intent = new Intent();
            intent.putExtra("remark", this.f8074x0);
        } else {
            intent = null;
        }
        setResult(-1, intent);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m13480d(String str) {
        Constant.m10311a("刪除好友");
        if (mo22741l()) {
            mo22751t();
            new HvApiDelBuddy().request(str, new C4226c());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8072v0 = getIntent().getStringExtra("userNbr");
        this.f8052b0 = (ImageView) findViewById(R$id.ivAvatar);
        this.f8053c0 = (TextView) findViewById(R$id.tvUserName);
        this.f8054d0 = (TextView) findViewById(R$id.tvWalkToday);
        this.f8055e0 = (TextView) findViewById(R$id.tvSleepTimeLastnight);
        this.f8056f0 = (TextView) findViewById(R$id.tvHeartRate);
        this.f8057g0 = (TextView) findViewById(R$id.tvUpdateTime);
        this.f8058h0 = (TextView) findViewById(R$id.tvStepDay);
        this.f8059i0 = (TextView) findViewById(R$id.tvStep);
        this.f8060j0 = (TextView) findViewById(R$id.tvStepDistance);
        this.f8061k0 = (TextView) findViewById(R$id.tvStepCal);
        this.f8062l0 = (TextView) findViewById(R$id.tvSleepTimeLength);
        this.f8063m0 = (TextView) findViewById(R$id.tvSleepTimeDay);
        this.f8064n0 = (TextView) findViewById(R$id.tvDeepSleep);
        this.f8065o0 = (TextView) findViewById(R$id.tvLightSleep);
        this.f8066p0 = (TextView) findViewById(R$id.tvSleepAsleep);
        this.f8067q0 = (TextView) findViewById(R$id.tvSleepRise);
        this.f8068r0 = (BarChart) findViewById(R$id.barChartWalk);
        this.f8069s0 = (BarChart) findViewById(R$id.barChartSleep);
        mo22723b(R$id.ivSetting);
        ((ViewGroup.MarginLayoutParams) ((ImageView) findViewById(R$id.ivSetting)).getLayoutParams()).topMargin = BarUtils.m981a();
        new HvApiGetGadId().getGadId(this.f8072v0, new C4224a());
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity$e */
    class C4228e implements OnChartValueSelectedListener {

        /* renamed from: a */
        final /* synthetic */ boolean f8082a;

        C4228e(boolean z) {
            this.f8082a = z;
        }

        /* renamed from: a */
        public void mo23323a(Entry entry, Highlight dVar) {
            int d = (int) entry.mo13315d();
            if (this.f8082a) {
                FriendDataActivity friendDataActivity = FriendDataActivity.this;
                friendDataActivity.m13471a(((ChartSleepData) friendDataActivity.f8070t0.get(d)).f8010a);
                return;
            }
            FriendDataActivity friendDataActivity2 = FriendDataActivity.this;
            friendDataActivity2.m13470a(((ChartStepData) friendDataActivity2.f8071u0.get(d)).f8014a);
        }

        /* renamed from: a */
        public void mo23322a() {
            if (this.f8082a) {
                FriendDataActivity friendDataActivity = FriendDataActivity.this;
                friendDataActivity.m13471a(((ChartSleepData) friendDataActivity.f8070t0.get(FriendDataActivity.this.f8070t0.size() - 1)).f8010a);
                return;
            }
            FriendDataActivity friendDataActivity2 = FriendDataActivity.this;
            friendDataActivity2.m13470a(((ChartStepData) friendDataActivity2.f8071u0.get(FriendDataActivity.this.f8071u0.size() - 1)).f8014a);
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24373a(View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        if (!z) {
            HttpCancelable bVar = this.f8076z0;
            if (bVar != null) {
                bVar.cancel();
                this.f8076z0 = null;
            }
            alertDialog.cancel();
        } else if (!TextUtils.isEmpty(charSequence)) {
            Constant.m10311a("備註好友");
            view.setEnabled(false);
            this.f8076z0 = new HvApiSetRemark().request(this.f8072v0, charSequence.toString(), new C4261y(this, alertDialog, charSequence, view));
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24372a(DialogInterface dialogInterface) {
        hideSoftKeyboard(this.f8052b0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.github.mikephil.charting.charts.BarChart, boolean):void
     arg types: [com.github.mikephil.charting.charts.BarChart, int]
     candidates:
      com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, com.heimavista.entity.band.StepInfo):void
      com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, com.heimavista.entity.band.b):void
      com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, java.lang.String):void
      com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity, boolean):boolean
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.ui.activity.friends.FriendDataActivity.a(com.github.mikephil.charting.charts.BarChart, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.i.d.a(java.lang.String, boolean):e.e.d.i.d
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.d.a(java.lang.String, int):e.e.d.i.d
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.i.d.a(java.lang.String, boolean):e.e.d.i.d */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        String str;
        String str2;
        String stringExtra = getIntent().getStringExtra("userNbr");
        if (!TextUtils.isEmpty(stringExtra)) {
            BuddyListDb l = BuddyListDb.m13250l(stringExtra);
            boolean D = l.mo24286D();
            boolean C = l.mo24285C();
            boolean A = l.mo24283A();
            this.f8057g0.setText(getString(R$string.last_update, new Object[]{TimeUtils.m998a(l.mo24304q() * 1000, "yyyy-MM-dd HH:mm")}));
            UserInfoDb.m13339a(this, stringExtra, this.f8053c0, this.f8052b0);
            Calendar instance = Calendar.getInstance();
            int i = 6;
            instance.add(6, 1 - this.f8073w0);
            this.f8070t0 = new ArrayList();
            String str3 = "--";
            if (C) {
                int i2 = this.f8073w0;
                while (i2 > 0) {
                    SleepDayInfo a = new C4208d().mo24329a(stringExtra, i2 - 1);
                    String a2 = TimeUtils.m998a(instance.getTimeInMillis() - 86400000, "M.d");
                    ChartSleepData aVar = new ChartSleepData();
                    aVar.f8010a = a;
                    aVar.f8011b = (float) (this.f8073w0 - i2);
                    if (i2 == 1) {
                        aVar.f8012c = getString(R$string.yesterday);
                        aVar.f8013d = getString(R$string.yesterday);
                    } else {
                        aVar.f8012c = a2;
                        aVar.f8013d = a2;
                    }
                    this.f8070t0.add(aVar);
                    instance.add(i, 1);
                    if (i2 == 1) {
                        boolean z = a.f6257f == 0;
                        SpanUtils a3 = SpanUtils.m865a(this.f8055e0);
                        if (z) {
                            str = str3;
                        } else {
                            str = String.valueOf(a.f6258g / 60);
                        }
                        a3.mo9735a(str);
                        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
                        a3.mo9737b(getResources().getColor(R$color.white));
                        a3.mo9735a(getString(R$string.data_unit_hours));
                        if (z) {
                            str2 = str3;
                        } else {
                            str2 = String.valueOf(a.f6258g % 60);
                        }
                        a3.mo9735a(str2);
                        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
                        a3.mo9737b(getResources().getColor(R$color.white));
                        a3.mo9735a(getString(R$string.data_unit_min));
                        a3.mo9736b();
                    }
                    i2--;
                    i = 6;
                }
            }
            m13469a(this.f8069s0, true);
            Calendar instance2 = Calendar.getInstance();
            instance2.add(6, 1 - this.f8073w0);
            this.f8071u0 = new ArrayList();
            if (C) {
                m13482u();
                List<ChartSleepData> list = this.f8070t0;
                m13471a(list.get(list.size() - 1).f8010a);
            } else {
                this.f8069s0.setNoDataText(getString(R$string.not_shared));
                findViewById(R$id.layoutSleepDetail).setVisibility(8);
                this.f8055e0.setText(R$string.not_shared);
            }
            if (D) {
                CurStepDataDb k = CurStepDataDb.m13136k(stringExtra);
                SpanUtils a4 = SpanUtils.m865a(this.f8054d0);
                a4.mo9735a(k.mo24250x() == 0 ? str3 : String.valueOf(k.mo24250x()));
                a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
                a4.mo9737b(getResources().getColor(R$color.white));
                a4.mo9735a(getString(R$string.data_unit_step));
                a4.mo9736b();
                for (int i3 = this.f8073w0; i3 > 0; i3--) {
                    StepInfo a5 = new C4209e().mo24330a(stringExtra, i3 - 1);
                    if (i3 == 1) {
                        CurStepDataDb k2 = CurStepDataDb.m13136k(stringExtra);
                        a5.f6243Q = k2.mo24232o();
                        a5.f6247U = k2.mo24248v();
                        a5.f6246T = k2.mo24249w();
                        if (a5.f6245S < k.mo24250x()) {
                            a5.f6245S = k2.mo24250x();
                        }
                    }
                    String a6 = TimeUtils.m1002a(instance2.getTime(), "M.d");
                    ChartStepData bVar = new ChartStepData();
                    bVar.f8014a = a5;
                    bVar.f8018e = (float) (this.f8073w0 - i3);
                    bVar.f8017d = (float) a5.f6245S;
                    if (i3 == 1) {
                        bVar.f8015b = getString(R$string.today);
                        bVar.f8016c = getString(R$string.today);
                    } else if (i3 == 2) {
                        bVar.f8015b = getString(R$string.yesterday);
                        bVar.f8016c = getString(R$string.yesterday);
                    } else {
                        bVar.f8015b = a6;
                        bVar.f8016c = a6;
                    }
                    this.f8071u0.add(bVar);
                    instance2.add(6, 1);
                }
            }
            m13469a(this.f8068r0, false);
            if (D) {
                m13483v();
                List<ChartStepData> list2 = this.f8071u0;
                m13470a(list2.get(list2.size() - 1).f8014a);
            } else {
                this.f8054d0.setText(R$string.not_shared);
                findViewById(R$id.layoutStepDetail).setVisibility(8);
                this.f8068r0.setNoDataText(getString(R$string.not_shared));
            }
            if (A) {
                HeartRateDataDb a7 = HeartRateDataDb.m13149a(stringExtra, false);
                SpanUtils a8 = SpanUtils.m865a(this.f8056f0);
                if (a7 != null && l.mo24283A()) {
                    str3 = String.valueOf(a7.mo24232o());
                }
                a8.mo9735a(str3);
                a8.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
                a8.mo9737b(getResources().getColor(R$color.white));
                a8.mo9735a(getString(R$string.data_unit_rate));
                a8.mo9736b();
                return;
            }
            this.f8056f0.setText(R$string.not_shared);
        }
    }

    /* renamed from: a */
    private void m13469a(BarChart barChart, boolean z) {
        barChart.setBorderWidth(1.0f);
        barChart.setPadding(0, 0, 0, 0);
        barChart.mo13022a(0.0f, 0.0f, 0.0f, 12.0f);
        barChart.getAxisRight().mo13233a(false);
        barChart.getAxisLeft().mo13233a(false);
        barChart.getAxisLeft().mo13301i(0.0f);
        barChart.getAxisLeft().mo13300h(0.0f);
        barChart.setScaleEnabled(false);
        barChart.setDragEnabled(true);
        barChart.setTouchEnabled(true);
        barChart.getDescription().mo13233a(false);
        barChart.setNoDataText(getString(R$string.no_sleep_data));
        barChart.setNoDataTextColor(getResources().getColor(R$color.font_color));
        barChart.getLegend().mo13233a(false);
        barChart.setDrawValueAboveBar(false);
        barChart.offsetTopAndBottom(0);
        XAxis xAxis = barChart.getXAxis();
        xAxis.mo13284a(XAxis.C1654a.BOTTOM);
        xAxis.mo13232a(Typeface.DEFAULT);
        xAxis.mo13204c(false);
        xAxis.mo13202b(false);
        xAxis.mo13231a(getResources().getColor(R$color.bar_text_color));
        xAxis.mo13230a(10.0f);
        xAxis.mo13206d(true);
        xAxis.mo13207e(1.0f);
        xAxis.mo13205d(-0.5f);
        xAxis.mo13235b(0.0f);
        xAxis.mo13200a(new C4227d(z));
        barChart.setOnChartValueSelectedListener(new C4228e(z));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13470a(StepInfo stepInfo) {
        SpanUtils a = SpanUtils.m865a(this.f8058h0);
        int i = stepInfo.f6245S;
        String str = "--";
        a.mo9735a(i == 0 ? str : String.valueOf(i));
        a.mo9735a(getString(R$string.data_unit_step));
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8059i0);
        int i2 = stepInfo.f6245S;
        a2.mo9735a(i2 == 0 ? str : String.valueOf(i2));
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a2.mo9737b(getResources().getColor(R$color.font_color));
        a2.mo9735a(getString(R$string.data_unit_step));
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8060j0);
        a3.mo9735a(stepInfo.f6246T == 0 ? str : String.format(Locale.getDefault(), "%.2f", Double.valueOf(((double) stepInfo.f6246T) / 1000.0d)));
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9737b(getResources().getColor(R$color.font_color));
        a3.mo9735a(getString(R$string.data_unit_mill));
        a3.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8061k0);
        int i3 = stepInfo.f6247U;
        if (i3 != 0) {
            str = String.valueOf(i3);
        }
        a4.mo9735a(str);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9737b(getResources().getColor(R$color.font_color));
        a4.mo9735a(getString(R$string.data_unit_cal));
        a4.mo9736b();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m13471a(SleepDayInfo bVar) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        boolean z = bVar.f6257f == 0;
        SpanUtils a = SpanUtils.m865a(this.f8063m0);
        String str9 = "--";
        a.mo9735a(z ? str9 : String.valueOf(bVar.f6258g / 60));
        a.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str = str9;
        } else {
            str = String.valueOf(bVar.f6258g % 60);
        }
        a.mo9735a(str);
        a.mo9735a(getString(R$string.data_unit_min));
        a.mo9736b();
        SpanUtils a2 = SpanUtils.m865a(this.f8064n0);
        a2.mo9735a(z ? str9 : String.valueOf(bVar.f6254c / 60));
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a2.mo9737b(getResources().getColor(R$color.font_color));
        a2.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str2 = str9;
        } else {
            str2 = String.valueOf(bVar.f6254c % 60);
        }
        a2.mo9735a(str2);
        a2.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a2.mo9737b(getResources().getColor(R$color.font_color));
        a2.mo9735a(getString(R$string.data_unit_min));
        a2.mo9736b();
        SpanUtils a3 = SpanUtils.m865a(this.f8065o0);
        a3.mo9735a(z ? str9 : String.valueOf(bVar.f6255d / 60));
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9737b(getResources().getColor(R$color.font_color));
        a3.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str3 = str9;
        } else {
            str3 = String.valueOf(bVar.f6255d % 60);
        }
        a3.mo9735a(str3);
        a3.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a3.mo9737b(getResources().getColor(R$color.font_color));
        a3.mo9735a(getString(R$string.data_unit_min));
        a3.mo9736b();
        SpanUtils a4 = SpanUtils.m865a(this.f8062l0);
        if (z) {
            str4 = str9;
        } else {
            str4 = String.valueOf(bVar.f6258g / 60);
        }
        a4.mo9735a(str4);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9737b(getResources().getColor(R$color.font_color));
        a4.mo9735a(getString(R$string.data_unit_hour));
        if (z) {
            str5 = str9;
        } else {
            str5 = String.valueOf(bVar.f6258g % 60);
        }
        a4.mo9735a(str5);
        a4.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a4.mo9737b(getResources().getColor(R$color.font_color));
        a4.mo9735a(getString(R$string.data_unit_min));
        a4.mo9736b();
        Calendar instance = Calendar.getInstance();
        long j = bVar.f6252a;
        List<SleepInfo> list = bVar.f6261j;
        if (list != null && !list.isEmpty()) {
            SleepInfo cVar = bVar.f6261j.get(0);
            if (cVar.f6265d == 3) {
                j = 60 + cVar.f6267f;
            }
        }
        instance.setTimeInMillis(j * 1000);
        SpanUtils a5 = SpanUtils.m865a(this.f8066p0);
        if (bVar.f6252a == 0) {
            str6 = str9;
        } else {
            str6 = String.valueOf(instance.get(11));
        }
        a5.mo9735a(str6);
        a5.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a5.mo9737b(getResources().getColor(R$color.font_color));
        a5.mo9735a(getString(R$string.data_unit_hour));
        if (bVar.f6252a == 0) {
            str7 = str9;
        } else {
            str7 = String.valueOf(instance.get(12));
        }
        a5.mo9735a(str7);
        a5.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a5.mo9737b(getResources().getColor(R$color.font_color));
        a5.mo9735a(getString(R$string.data_unit_min));
        a5.mo9736b();
        Calendar instance2 = Calendar.getInstance();
        instance2.setTimeInMillis(bVar.f6253b * 1000);
        SpanUtils a6 = SpanUtils.m865a(this.f8067q0);
        if (bVar.f6253b == 0) {
            str8 = str9;
        } else {
            str8 = String.valueOf(instance2.get(11));
        }
        a6.mo9735a(str8);
        a6.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a6.mo9737b(getResources().getColor(R$color.font_color));
        a6.mo9735a(getString(R$string.data_unit_hour));
        if (bVar.f6253b != 0) {
            str9 = String.valueOf(instance2.get(12));
        }
        a6.mo9735a(str9);
        a6.mo9733a(getResources().getDimensionPixelSize(R$dimen.font_size_20));
        a6.mo9737b(getResources().getColor(R$color.font_color));
        a6.mo9735a(getString(R$string.data_unit_min));
        a6.mo9736b();
    }
}
