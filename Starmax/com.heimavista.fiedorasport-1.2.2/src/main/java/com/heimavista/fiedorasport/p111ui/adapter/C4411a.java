package com.heimavista.fiedorasport.p111ui.adapter;

import android.view.View;
import com.heimavista.entity.data.FriendBean;

/* renamed from: com.heimavista.fiedorasport.ui.adapter.a */
/* compiled from: lambda */
public final /* synthetic */ class C4411a implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ ListBuddyDynamicAdapter f8648P;

    /* renamed from: Q */
    private final /* synthetic */ int f8649Q;

    /* renamed from: R */
    private final /* synthetic */ FriendBean f8650R;

    public /* synthetic */ C4411a(ListBuddyDynamicAdapter listBuddyDynamicAdapter, int i, FriendBean dVar) {
        this.f8648P = listBuddyDynamicAdapter;
        this.f8649Q = i;
        this.f8650R = dVar;
    }

    public final void onClick(View view) {
        this.f8648P.mo24620c(this.f8649Q, this.f8650R, view);
    }
}
