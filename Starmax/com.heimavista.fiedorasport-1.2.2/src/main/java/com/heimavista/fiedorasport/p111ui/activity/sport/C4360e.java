package com.heimavista.fiedorasport.p111ui.activity.sport;

import android.view.View;

/* renamed from: com.heimavista.fiedorasport.ui.activity.sport.e */
/* compiled from: lambda */
public final /* synthetic */ class C4360e implements View.OnLongClickListener {

    /* renamed from: P */
    private final /* synthetic */ SportActivity f8480P;

    public /* synthetic */ C4360e(SportActivity sportActivity) {
        this.f8480P = sportActivity;
    }

    public final boolean onLongClick(View view) {
        return this.f8480P.mo24533a(view);
    }
}
