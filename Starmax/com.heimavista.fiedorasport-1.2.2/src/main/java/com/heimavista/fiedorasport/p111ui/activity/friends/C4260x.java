package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.View;
import com.heimavista.entity.data.SelectFriendBean;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.x */
/* compiled from: lambda */
public final /* synthetic */ class C4260x implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ SelectFriendsActivity f8140P;

    public /* synthetic */ C4260x(SelectFriendsActivity selectFriendsActivity) {
        this.f8140P = selectFriendsActivity;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8140P.mo24407b(view, (SelectFriendBean) obj, i);
    }
}
