package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.view.View;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter;
import p119e.p189e.p193d.p195j.BuddyListDb;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.j */
/* compiled from: lambda */
public final /* synthetic */ class C4451j implements BaseRecyclerViewAdapter.C3685c {

    /* renamed from: P */
    private final /* synthetic */ BuddyLocFragment f8776P;

    public /* synthetic */ C4451j(BuddyLocFragment buddyLocFragment) {
        this.f8776P = buddyLocFragment;
    }

    /* renamed from: a */
    public final void mo22804a(View view, Object obj, int i) {
        this.f8776P.mo24690a(view, (BuddyListDb) obj, i);
    }
}
