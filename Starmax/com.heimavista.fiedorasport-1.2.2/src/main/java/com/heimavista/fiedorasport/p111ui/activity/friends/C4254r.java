package com.heimavista.fiedorasport.p111ui.activity.friends;

import android.view.KeyEvent;
import android.widget.TextView;

/* renamed from: com.heimavista.fiedorasport.ui.activity.friends.r */
/* compiled from: lambda */
public final /* synthetic */ class C4254r implements TextView.OnEditorActionListener {

    /* renamed from: a */
    private final /* synthetic */ SearchBuddyActivity f8133a;

    public /* synthetic */ C4254r(SearchBuddyActivity searchBuddyActivity) {
        this.f8133a = searchBuddyActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f8133a.mo24398a(textView, i, keyEvent);
    }
}
