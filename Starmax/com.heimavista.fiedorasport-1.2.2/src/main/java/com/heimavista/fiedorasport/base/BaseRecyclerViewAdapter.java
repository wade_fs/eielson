package com.heimavista.fiedorasport.base;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter.ViewHolder;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, VH extends ViewHolder> extends RecyclerView.Adapter<VH> {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public List<T> f6431a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public RecyclerView f6432b;

    /* renamed from: c */
    private Context f6433c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C3685c f6434d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C3686d f6435e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public SparseArray<C3683a> f6436f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public SparseArray<C3684b> f6437g;

    /* renamed from: h */
    private BaseRecyclerViewAdapter<T, VH>.e f6438h;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        /* renamed from: P */
        private SparseArray<WeakReference<View>> f6439P;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public ViewHolder(BaseRecyclerViewAdapter baseRecyclerViewAdapter, ViewGroup viewGroup, int i) {
            this(LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false));
        }

        /* renamed from: b */
        private void m10296b() {
            if (BaseRecyclerViewAdapter.this.f6434d != null) {
                mo22798a().setOnClickListener(this);
            }
            if (BaseRecyclerViewAdapter.this.f6435e != null) {
                mo22798a().setOnLongClickListener(this);
            }
            if (BaseRecyclerViewAdapter.this.f6436f != null) {
                for (int i = 0; i < BaseRecyclerViewAdapter.this.f6436f.size(); i++) {
                    View findViewById = findViewById(BaseRecyclerViewAdapter.this.f6436f.keyAt(i));
                    if (findViewById != null) {
                        findViewById.setOnClickListener(this);
                    }
                }
            }
            if (BaseRecyclerViewAdapter.this.f6437g != null) {
                for (int i2 = 0; i2 < BaseRecyclerViewAdapter.this.f6437g.size(); i2++) {
                    View findViewById2 = findViewById(BaseRecyclerViewAdapter.this.f6437g.keyAt(i2));
                    if (findViewById2 != null) {
                        findViewById2.setOnLongClickListener(this);
                    }
                }
            }
        }

        /* renamed from: a */
        public final View mo22798a() {
            return super.itemView;
        }

        public final <V extends View> V findViewById(@IdRes int i) {
            WeakReference weakReference = this.f6439P.get(i);
            if (weakReference != null && weakReference.get() != null) {
                return (View) weakReference.get();
            }
            V findViewById = mo22798a().findViewById(i);
            this.f6439P.put(i, new WeakReference(findViewById));
            return findViewById;
        }

        public void onClick(View view) {
            C3683a aVar;
            if (view == mo22798a() && BaseRecyclerViewAdapter.this.f6434d != null) {
                BaseRecyclerViewAdapter.this.f6434d.mo22804a(view, BaseRecyclerViewAdapter.this.f6431a.get(getLayoutPosition()), getLayoutPosition());
            } else if (BaseRecyclerViewAdapter.this.f6436f != null && (aVar = (C3683a) BaseRecyclerViewAdapter.this.f6436f.get(view.getId())) != null) {
                aVar.mo22802a(BaseRecyclerViewAdapter.this.f6432b, view, getLayoutPosition());
            }
        }

        public boolean onLongClick(View view) {
            C3684b bVar;
            if (view == mo22798a() && BaseRecyclerViewAdapter.this.f6435e != null) {
                return BaseRecyclerViewAdapter.this.f6435e.mo22805a(view, BaseRecyclerViewAdapter.this.f6431a.get(getLayoutPosition()), getLayoutPosition());
            }
            if (BaseRecyclerViewAdapter.this.f6437g == null || (bVar = (C3684b) BaseRecyclerViewAdapter.this.f6437g.get(view.getId())) == null) {
                return false;
            }
            bVar.mo22803a(BaseRecyclerViewAdapter.this.f6432b, view, getLayoutPosition());
            return false;
        }

        public ViewHolder(View view) {
            super(view);
            this.f6439P = new SparseArray<>();
            m10296b();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter$a */
    public interface C3683a {
        /* renamed from: a */
        void mo22802a(RecyclerView recyclerView, View view, int i);
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter$b */
    public interface C3684b {
        /* renamed from: a */
        void mo22803a(RecyclerView recyclerView, View view, int i);
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter$c */
    public interface C3685c<T> {
        /* renamed from: a */
        void mo22804a(View view, T t, int i);
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter$d */
    public interface C3686d<T> {
        /* renamed from: a */
        boolean mo22805a(View view, T t, int i);
    }

    /* renamed from: com.heimavista.fiedorasport.base.BaseRecyclerViewAdapter$e */
    private class C3687e extends RecyclerView.OnScrollListener {
    }

    public BaseRecyclerViewAdapter(Context context) {
        this.f6433c = context;
    }

    public T getItem(int i) {
        return this.f6431a.get(i);
    }

    public int getItemCount() {
        List<T> list = this.f6431a;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        RecyclerView.LayoutManager a;
        this.f6432b = recyclerView;
        BaseRecyclerViewAdapter<T, VH>.e eVar = this.f6438h;
        if (eVar != null) {
            this.f6432b.addOnScrollListener(eVar);
        }
        if (this.f6432b.getLayoutManager() == null && (a = mo22780a(this.f6433c)) != null) {
            this.f6432b.setLayoutManager(a);
        }
    }

    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        BaseRecyclerViewAdapter<T, VH>.e eVar = this.f6438h;
        if (eVar != null) {
            this.f6432b.removeOnScrollListener(eVar);
        }
        this.f6432b = null;
    }

    /* renamed from: f */
    private void mo24650f() {
        if (this.f6432b != null) {
            throw new IllegalStateException("Binding adapters is not allowed before setting listeners");
        }
    }

    /* renamed from: a */
    public void mo22787a(List<T> list, int i) {
        if (list != null && list.size() != 0) {
            List<T> list2 = this.f6431a;
            if (list2 == null || list2.size() == 0) {
                mo22792b((List) list);
                return;
            }
            this.f6431a.addAll(i, list);
            notifyItemRangeInserted(i, list.size());
        }
    }

    /* renamed from: b */
    public void mo22792b(List<T> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        this.f6431a = list;
        notifyDataSetChanged();
    }

    @Nullable
    /* renamed from: c */
    public List<T> mo22794c() {
        return this.f6431a;
    }

    /* renamed from: d */
    public RecyclerView mo22795d() {
        return this.f6432b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public Resources mo22796e() {
        return this.f6433c.getResources();
    }

    /* renamed from: b */
    public void mo22790b(int i, T t) {
        if (this.f6431a == null) {
            this.f6431a = new ArrayList();
        }
        this.f6431a.set(i, t);
        notifyItemChanged(i);
    }

    /* renamed from: a */
    public void mo22786a(List<T> list) {
        if (list != null && list.size() != 0) {
            List<T> list2 = this.f6431a;
            if (list2 == null || list2.size() == 0) {
                mo22792b((List) list);
                return;
            }
            this.f6431a.addAll(list);
            notifyItemRangeInserted(this.f6431a.size() - list.size(), list.size());
        }
    }

    /* renamed from: b */
    public void mo22791b(T t) {
        int indexOf = this.f6431a.indexOf(t);
        if (indexOf != -1) {
            mo22789b(indexOf);
        }
    }

    /* renamed from: b */
    public void mo22789b(int i) {
        this.f6431a.remove(i);
        notifyItemRemoved(i);
    }

    /* renamed from: a */
    public void mo22782a() {
        List<T> list = this.f6431a;
        if (list != null && list.size() != 0) {
            this.f6431a.clear();
            notifyDataSetChanged();
        }
    }

    /* renamed from: b */
    public void mo22793b(List<T> list, int i) {
        if (list != null && !list.isEmpty() && this.f6431a.removeAll(list)) {
            notifyItemRangeRemoved(i, list.size());
        }
    }

    /* renamed from: a */
    public void mo22785a(T t) {
        if (this.f6431a == null) {
            this.f6431a = new ArrayList();
        }
        mo22783a(this.f6431a.size(), t);
    }

    /* renamed from: b */
    public Context mo22788b() {
        return this.f6433c;
    }

    /* renamed from: a */
    public void mo22783a(int i, T t) {
        if (this.f6431a == null) {
            this.f6431a = new ArrayList();
        }
        if (i < this.f6431a.size()) {
            this.f6431a.add(i, t);
        } else {
            this.f6431a.add(t);
            i = this.f6431a.size() - 1;
        }
        notifyItemInserted(i);
    }

    /* renamed from: a */
    public String mo22781a(@StringRes int i) {
        return this.f6433c.getString(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public RecyclerView.LayoutManager mo22780a(Context context) {
        return new LinearLayoutManager(context);
    }

    /* renamed from: a */
    public void mo22784a(C3685c cVar) {
        mo24650f();
        this.f6434d = cVar;
    }
}
