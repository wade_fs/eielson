package com.heimavista.fiedorasport.service;

import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.sxr.sdk.ble.keepfit.service.BluetoothLeService;
import p119e.p189e.p191b.HvApp;

public class JYBleService extends BluetoothLeService {
    /* renamed from: a */
    private void m10697a(String str) {
        LogUtils.m1139c("madyJY>>>JYBleService " + str);
    }

    /* renamed from: c */
    private void m10698c() {
        String string = getString(R$string.channel_ble);
        NotificationManagerCompat from = NotificationManagerCompat.from(this);
        if (Build.VERSION.SDK_INT >= 26) {
            if (from.getNotificationChannel("channel_fs_jyble") != null) {
                from.deleteNotificationChannel("channel_fs_jyble");
            }
            NotificationChannel notificationChannel = new NotificationChannel("channel_fs_jyble", string, 3);
            notificationChannel.setDescription(getString(R$string.notification_stay_connected_with_band));
            notificationChannel.setShowBadge(false);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);
            notificationChannel.setSound(null, null);
            notificationChannel.setLockscreenVisibility(-1);
            from.createNotificationChannel(notificationChannel);
        }
        startForeground(10006, new NotificationCompat.Builder(this, "channel_fs_jyble").setContentTitle(getString(R$string.app_name)).setContentText(getString(R$string.notification_stay_connected_with_band)).setShowWhen(false).setSound(null).setSmallIcon(R$mipmap.ic_push).setLargeIcon(BitmapFactory.decodeResource(getResources(), R$mipmap.app_icon)).setContentIntent(PendingIntent.getService(this, 1, new Intent(this, JYBleService.class), 134217728)).build());
    }

    public IBinder onBind(Intent intent) {
        m10697a("in onBind()");
        stopForeground(true);
        return super.onBind(intent);
    }

    public void onCreate() {
        super.onCreate();
        m10697a("onCreate");
        JYSDKManager.m10388i().mo22871a();
    }

    public void onDestroy() {
        m10697a("in onDestroy");
        JYSDKManager.m10388i().mo22895d(false);
        stopForeground(true);
        super.onDestroy();
        if (!TextUtils.isEmpty(FSManager.m10323r().mo22836f()) && FSManager.m10323r().mo22847q()) {
            Intent intent = new Intent(HvApp.m13010c(), JYBleService.class);
            if (Build.VERSION.SDK_INT >= 26) {
                startForegroundService(intent);
                m10698c();
                return;
            }
            startService(intent);
        }
    }

    public void onRebind(Intent intent) {
        m10697a("in onRebind()");
        stopForeground(true);
        super.onRebind(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        m10697a("onStartCommand");
        return super.onStartCommand(intent, i, i2);
    }

    public boolean onUnbind(Intent intent) {
        m10697a("Last client unbound from service");
        m10698c();
        return super.onUnbind(intent);
    }
}
