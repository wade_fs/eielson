package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.C3734y;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p111ui.activity.device.ScanActivity;
import java.util.ArrayList;
import p019cn.qqtheme.framework.widget.WheelView;
import p119e.p189e.p191b.HvApp;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingTargetStepActivity */
public class SettingTargetStepActivity extends BaseActivity implements C3734y.C3735a {

    /* renamed from: b0 */
    private WheelView f8283b0;

    /* renamed from: c0 */
    private int f8284c0 = 8000;

    /* renamed from: d0 */
    private boolean f8285d0 = false;

    /* renamed from: b */
    public static Intent m13893b(boolean z) {
        Intent intent = new Intent(HvApp.m13010c(), SettingTargetStepActivity.class);
        intent.putExtra("hasConnect", z);
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f8284c0 = BandDataManager.m10565g();
        ArrayList arrayList = new ArrayList();
        for (int i = 1000; i <= 20000; i += 1000) {
            arrayList.add(Integer.valueOf(i));
        }
        this.f8283b0.setItems(arrayList);
        this.f8283b0.setSelectedIndex((this.f8284c0 / 1000) - 1);
        this.f8283b0.setOnItemSelectListener(new C4303b0(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.ui.activity.setting.SettingTargetStepActivity.a(com.heimavista.fiedorasport.h.y$b, boolean):void
     arg types: [com.heimavista.fiedorasport.h.y$b, int]
     candidates:
      com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
      com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
      com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
      com.heimavista.fiedorasport.h.y.a(int, int):void
      com.heimavista.fiedorasport.h.y.a(long, int):void
      com.heimavista.fiedorasport.ui.activity.setting.SettingTargetStepActivity.a(com.heimavista.fiedorasport.h.y$b, boolean):void */
    /* renamed from: e */
    public /* synthetic */ void mo24497e(int i) {
        this.f8284c0 = (i + 1) * 1000;
        Constant.m10311a("目標設置值");
        if (this.f8285d0) {
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22889b(this.f8284c0);
            }
            if (FSManager.m10323r().mo22846p()) {
                CRPBleManager.m10637h(this.f8284c0);
                return;
            }
            return;
        }
        mo22964a(C3734y.C3736b.goalStep, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_target_step;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(this.f8285d0 ? R$string.setting_target_step : R$string.step_global_setting);
    }

    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R$id.btnAction) {
            mo22721b(ScanActivity.class);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8285d0 = getIntent().getBooleanExtra("hasConnect", false);
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22875a(this);
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10602a(this);
        }
        if (this.f8285d0) {
            findViewById(R$id.btnAction).setVisibility(8);
        } else {
            mo22723b(R$id.btnAction);
        }
        this.f8283b0 = (WheelView) findViewById(R$id.wheelView);
        this.f8283b0.setLineSpaceMultiplier(2.0f);
        this.f8283b0.setTextPadding(5);
        this.f8283b0.setTextSize(16.0f);
        this.f8283b0.setTypeface(Typeface.DEFAULT);
        this.f8283b0.setGravity(17);
        this.f8283b0.setTextSize(18.0f);
        this.f8283b0.mo9640a(getResources().getColor(R$color.font_color_hint), getResources().getColor(R$color.font_color));
        WheelView wheelView = this.f8283b0;
        WheelView.C0840c cVar = new WheelView.C0840c();
        cVar.mo9673a(getResources().getColor(R$color.font_color));
        wheelView.setDividerConfig(cVar);
        this.f8283b0.setOffset(3);
        this.f8283b0.setCycleDisable(true);
        this.f8283b0.setUseWeight(true);
        this.f8283b0.setTextSizeAutoFit(true);
        WheelView wheelView2 = this.f8283b0;
        wheelView2.setLabel("    " + getString(R$string.data_unit_step));
    }

    /* renamed from: a */
    public void mo22964a(C3734y.C3736b bVar, boolean z) {
        if (bVar == C3734y.C3736b.goalStep && z) {
            BandDataManager.m10560c(this.f8284c0);
            new HvApiPostBandSettings(null).requestPostStepGoal(this.f8284c0);
        }
    }
}
