package com.heimavista.fiedorasport.p111ui.fragment.friend;

import android.app.AlertDialog;
import android.view.View;
import com.heimavista.entity.data.FriendBean;
import com.heimavista.fiedorasport.p108g.C3701h;

/* renamed from: com.heimavista.fiedorasport.ui.fragment.friend.h */
/* compiled from: lambda */
public final /* synthetic */ class C4449h implements C3701h {

    /* renamed from: a */
    private final /* synthetic */ BuddyDynamicFragment f8770a;

    /* renamed from: b */
    private final /* synthetic */ FriendBean f8771b;

    /* renamed from: c */
    private final /* synthetic */ int f8772c;

    public /* synthetic */ C4449h(BuddyDynamicFragment buddyDynamicFragment, FriendBean dVar, int i) {
        this.f8770a = buddyDynamicFragment;
        this.f8771b = dVar;
        this.f8772c = i;
    }

    /* renamed from: a */
    public final void mo22862a(View view, AlertDialog alertDialog, boolean z, CharSequence charSequence) {
        this.f8770a.mo24678a(this.f8771b, this.f8772c, view, alertDialog, z, charSequence);
    }
}
