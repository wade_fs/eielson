package com.heimavista.fiedorasport.p111ui.activity.crash;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.p018b.CaocConfig;
import com.blankj.utilcode.util.SnackbarUtils;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p199j.ClipboardUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.crash.CrashActivity */
public class CrashActivity extends BaseActivity {

    /* renamed from: b0 */
    private CaocConfig f6666b0;

    /* renamed from: c0 */
    private AlertDialog f6667c0;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f6666b0 = CustomActivityOnCrash.m711b(getIntent());
        if (this.f6666b0 == null) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        mo22723b(R$id.btn_crash_restart, R$id.btn_crash_log);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_crash;
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.btn_crash_restart) {
            CustomActivityOnCrash.m713b(this, this.f6666b0);
        } else if (id == R$id.btn_crash_log) {
            if (this.f6667c0 == null) {
                this.f6667c0 = new AlertDialog.Builder(this).setTitle(R$string.customactivityoncrash_error_activity_error_details_title).setMessage(CustomActivityOnCrash.m698a(this, getIntent())).setPositiveButton(R$string.customactivityoncrash_error_activity_error_details_close, (DialogInterface.OnClickListener) null).setNeutralButton(R$string.customactivityoncrash_error_activity_error_details_copy, new C3765a(this)).create();
            }
            this.f6667c0.show();
            TextView textView = (TextView) this.f6667c0.findViewById(16908299);
            if (textView != null) {
                textView.setTextSize(2, 12.0f);
            }
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo23032a(DialogInterface dialogInterface, int i) {
        ClipboardUtils.m13411a(CustomActivityOnCrash.m698a(this, getIntent()));
        SnackbarUtils a = SnackbarUtils.m927a(findViewById(R$id.rootView));
        a.mo9783a(getString(R$string.customactivityoncrash_error_activity_error_details_copied));
        a.mo9782a(-1);
        a.mo9786a();
    }
}
