package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import com.blankj.utilcode.util.ScreenUtils;
import com.heimavista.api.band.HvApiPostBandSettings;
import com.heimavista.fiedorasport.Constant;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p108g.WeekSelectDialog;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.widget.CommonListItemView;
import com.heimavista.widget.SwitchButton;
import com.heimavista.widget.dialog.BaseDialog;
import com.heimavista.widget.dialog.C4627d;
import com.heimavista.widget.dialog.C4629g;
import com.heimavista.widget.dialog.C4632i;
import com.heimavista.widget.dialog.EditDialog;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import java.util.ArrayList;
import p019cn.qqtheme.framework.widget.WheelView;
import p112d.p113a.p114a.p118d.DateUtils;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAlarmActivity */
public class SettingAlarmActivity extends BaseActivity {

    /* renamed from: b0 */
    private int f8242b0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public AlarmInfoItem f8243c0;

    /* renamed from: d0 */
    private CommonListItemView f8244d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public CommonListItemView f8245e0;

    /* renamed from: f0 */
    private CommonListItemView f8246f0;

    /* renamed from: g0 */
    private WheelView f8247g0;

    /* renamed from: h0 */
    private WheelView f8248h0;

    /* renamed from: i0 */
    private ArrayList<String> f8249i0 = new ArrayList<>();

    /* renamed from: j0 */
    private ArrayList<String> f8250j0 = new ArrayList<>();

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAlarmActivity$a */
    class C4293a implements C4627d {
        C4293a() {
        }

        /* renamed from: a */
        public void mo24478a(String str) {
            if (!TextUtils.isEmpty(str)) {
                SettingAlarmActivity.this.f8243c0.mo26356a(str);
                SettingAlarmActivity.this.m13802u();
                SettingAlarmActivity.this.f8245e0.setDetailText(SettingAlarmActivity.this.f8243c0.mo26358b());
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.setting.SettingAlarmActivity$b */
    class C4294b implements C4632i<String> {
        C4294b() {
        }

        /* renamed from: a */
        public void mo24375a(BaseDialog baseDialog) {
        }

        /* renamed from: a */
        public void mo24376a(BaseDialog baseDialog, int i, String str) {
            SettingAlarmActivity.this.f8243c0.mo26357a(i == 0);
            SettingAlarmActivity.this.f8243c0.mo26359b(i);
            SettingAlarmActivity.this.f8243c0.mo26368f(i);
            SettingAlarmActivity.this.f8243c0.mo26372h(i);
            SettingAlarmActivity.this.f8243c0.mo26366e(i);
            SettingAlarmActivity.this.f8243c0.mo26355a(i);
            SettingAlarmActivity.this.f8243c0.mo26361c(i);
            SettingAlarmActivity.this.f8243c0.mo26363d(i);
            SettingAlarmActivity.this.m13802u();
            SettingAlarmActivity.this.m13803v();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public void m13802u() {
        BandDataManager.m10552a(this.f8243c0);
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10603a(this.f8243c0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v */
    public void m13803v() {
        if (FSManager.m10323r().mo22847q()) {
            boolean z = this.f8243c0.mo26362d() == 1;
            boolean z2 = this.f8243c0.mo26371h() == 1;
            boolean z3 = this.f8243c0.mo26375j() == 1;
            boolean z4 = this.f8243c0.mo26369g() == 1;
            boolean z5 = this.f8243c0.mo26360c() == 1;
            boolean z6 = this.f8243c0.mo26365e() == 1;
            boolean z7 = this.f8243c0.mo26367f() == 1;
            if (z || z2 || z3 || z4 || z5 || z6 || z7) {
                this.f8243c0.mo26357a(false);
                if (z && z2 && z3 && z4 && z5 && z6 && z7) {
                    this.f8246f0.setDetailText(getString(R$string.everyday));
                } else if (z && z2 && z3 && z4 && z5 && !z6 && !z7) {
                    this.f8246f0.setDetailText(getString(R$string.workday));
                } else if (z || z2 || z3 || z4 || z5 || !z6 || !z7) {
                    ArrayList arrayList = new ArrayList();
                    if (z) {
                        arrayList.add(getString(R$string.Monday));
                    }
                    if (z2) {
                        arrayList.add(getString(R$string.Tuesday));
                    }
                    if (z3) {
                        arrayList.add(getString(R$string.Wednesday));
                    }
                    if (z4) {
                        arrayList.add(getString(R$string.Thursday));
                    }
                    if (z5) {
                        arrayList.add(getString(R$string.Friday));
                    }
                    if (z6) {
                        arrayList.add(getString(R$string.Saturday));
                    }
                    if (z7) {
                        arrayList.add(getString(R$string.Sunday));
                    }
                    StringBuilder sb = new StringBuilder();
                    int size = arrayList.size();
                    for (int i = 0; i < size; i++) {
                        sb.append((String) arrayList.get(i));
                        if (i < size - 1) {
                            sb.append(",");
                        }
                    }
                    this.f8246f0.setDetailText(sb.toString());
                } else {
                    this.f8246f0.setDetailText(getString(R$string.weekend));
                }
            } else {
                this.f8243c0.mo26357a(true);
                this.f8246f0.setDetailText(getString(R$string.only_once));
            }
        } else if (FSManager.m10323r().mo22846p()) {
            this.f8246f0.setDetailText(getString(this.f8243c0.mo26379m() ? R$string.only_once : R$string.everyday));
        }
    }

    /* renamed from: w */
    private void m13804w() {
        EditDialog cVar = new EditDialog(this);
        cVar.mo25475k(R$string.alarm_label);
        EditDialog cVar2 = cVar;
        cVar2.mo25449d(this.f8243c0.mo26358b());
        cVar2.mo25452l(R$string.input_label);
        cVar2.mo25448a(new C4293a());
        cVar2.mo25413a(true);
        EditDialog cVar3 = cVar2;
        cVar3.mo25427g((ScreenUtils.m1265c() * 3) / 5);
        BaseDialog b = cVar3.mo25418b();
        C4222l.m13456a((EditText) b.findViewById(R$id.dialog_edit), 6);
        b.mo25389a(new C4316i(this));
        b.show();
    }

    /* renamed from: x */
    private void m13805x() {
        boolean z = false;
        if (FSManager.m10323r().mo22847q()) {
            boolean[] zArr = new boolean[7];
            zArr[0] = this.f8243c0.mo26362d() == 1;
            zArr[1] = this.f8243c0.mo26371h() == 1;
            zArr[2] = this.f8243c0.mo26375j() == 1;
            zArr[3] = this.f8243c0.mo26369g() == 1;
            zArr[4] = this.f8243c0.mo26360c() == 1;
            zArr[5] = this.f8243c0.mo26365e() == 1;
            if (this.f8243c0.mo26367f() == 1) {
                z = true;
            }
            zArr[6] = z;
            WeekSelectDialog jVar = new WeekSelectDialog(this);
            jVar.mo22866a(zArr);
            jVar.mo22865a(new C4314h(this));
            jVar.mo25418b().show();
        } else if (FSManager.m10323r().mo22846p()) {
            C4629g gVar = new C4629g(this);
            gVar.mo25456a(getString(R$string.only_once), getString(R$string.everyday));
            gVar.mo25453a(new C4294b());
            gVar.mo25418b().show();
        }
    }

    /* renamed from: e */
    public /* synthetic */ void mo24476e(int i) {
        this.f8243c0.mo26374i(i);
        m13802u();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_setting_alarm;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.setting_alarm);
    }

    public void onBackPressed() {
        new HvApiPostBandSettings(null).requestPostAlarms();
        super.onBackPressed();
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.livSwitch) {
            this.f8244d0.getSwitch().toggle();
        } else if (id == R$id.livLabel) {
            Constant.m10311a("鬧鐘提醒設置值");
            m13804w();
        } else if (id == R$id.livRepeat) {
            Constant.m10311a("鬧鐘提醒設置值");
            m13805x();
        }
    }

    /* renamed from: a */
    private void m13798a(WheelView wheelView) {
        wheelView.setLineSpaceMultiplier(2.0f);
        wheelView.setTextPadding(5);
        wheelView.setTextSize(16.0f);
        wheelView.setTypeface(Typeface.DEFAULT);
        wheelView.setGravity(17);
        wheelView.setTextSize(16.0f);
        wheelView.mo9640a(getResources().getColor(R$color.font_color_hint), getResources().getColor(R$color.font_color));
        wheelView.setOffset(5);
        wheelView.setCycleDisable(false);
        wheelView.setUseWeight(true);
        wheelView.setTextSizeAutoFit(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8242b0 = getIntent().getIntExtra("alarmId", 0);
        this.f8243c0 = BandDataManager.m10547a(this.f8242b0);
        this.f8244d0 = (CommonListItemView) findViewById(R$id.livSwitch);
        this.f8245e0 = (CommonListItemView) findViewById(R$id.livLabel);
        this.f8246f0 = (CommonListItemView) findViewById(R$id.livRepeat);
        mo22714a(this.f8244d0, this.f8245e0, this.f8246f0);
        this.f8247g0 = (WheelView) findViewById(R$id.wvHour);
        this.f8248h0 = (WheelView) findViewById(R$id.wvMin);
        m13798a(this.f8247g0);
        m13798a(this.f8248h0);
        this.f8247g0.setOnItemSelectListener(new C4320k(this));
        this.f8248h0.setOnItemSelectListener(new C4321l(this));
    }

    /* renamed from: f */
    public /* synthetic */ void mo24477f(int i) {
        this.f8243c0.mo26376j(i);
        m13802u();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        boolean z = false;
        for (int i = 0; i <= 23; i++) {
            this.f8249i0.add(DateUtils.m11043a(i));
        }
        for (int i2 = 0; i2 <= 59; i2++) {
            this.f8250j0.add(DateUtils.m11043a(i2));
        }
        this.f8247g0.setItems(this.f8249i0);
        this.f8248h0.setItems(this.f8250j0);
        this.f8247g0.setSelectedIndex(this.f8243c0.mo26377k());
        this.f8248h0.setSelectedIndex(this.f8243c0.mo26378l());
        SwitchButton switchButton = this.f8244d0.getSwitch();
        if (this.f8243c0.mo26373i() > 0) {
            z = true;
        }
        switchButton.setChecked(z);
        this.f8244d0.getSwitch().setOnCheckedChangeListener(new C4318j(this));
        this.f8245e0.setDetailText(this.f8243c0.mo26358b());
        m13803v();
    }

    /* renamed from: b */
    public /* synthetic */ void mo24475b(BaseDialog baseDialog) {
        hideSoftKeyboard(this.f8245e0);
    }

    /* renamed from: a */
    public /* synthetic */ void mo24473a(SwitchButton switchButton, boolean z) {
        if (!z) {
            this.f8243c0.mo26370g(0);
        } else if (this.f8243c0.mo26377k() >= 22 || this.f8243c0.mo26377k() <= 1) {
            this.f8243c0.mo26370g(1);
        } else {
            this.f8243c0.mo26370g(2);
        }
        Constant.m10311a(String.format(z ? "設置鬧鐘%d開啟" : "設置鬧鐘%d關閉", Integer.valueOf(this.f8242b0 + 1)));
        m13802u();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24474a(boolean[] zArr) {
        this.f8243c0.mo26359b(zArr[0] ? 1 : 0);
        this.f8243c0.mo26368f(zArr[1] ? 1 : 0);
        this.f8243c0.mo26372h(zArr[2] ? 1 : 0);
        this.f8243c0.mo26366e(zArr[3] ? 1 : 0);
        this.f8243c0.mo26355a(zArr[4] ? 1 : 0);
        this.f8243c0.mo26361c(zArr[5] ? 1 : 0);
        this.f8243c0.mo26363d(zArr[6] ? 1 : 0);
        m13802u();
        m13803v();
    }
}
