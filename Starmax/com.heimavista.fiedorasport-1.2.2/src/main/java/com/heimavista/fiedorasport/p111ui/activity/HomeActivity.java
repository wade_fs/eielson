package com.heimavista.fiedorasport.p111ui.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.heimavista.api.OnResultListener;
import com.heimavista.api.account.HvApiGetProfile;
import com.heimavista.api.band.HvApiGetBandDataTask;
import com.heimavista.api.band.HvApiGetBandSettings;
import com.heimavista.api.band.HvApiPostHeartData;
import com.heimavista.api.band.HvApiPostSleepData;
import com.heimavista.api.band.HvApiPostStepData;
import com.heimavista.apn.PushMsg;
import com.heimavista.entity.band.BandConfig;
import com.heimavista.entity.band.TickType;
import com.heimavista.entity.data.TabEntity;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.base.BaseFragment;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p109h.JYSDKManager;
import com.heimavista.fiedorasport.p109h.TickManager;
import com.heimavista.fiedorasport.p111ui.fragment.home.MainBuddyFragment;
import com.heimavista.fiedorasport.p111ui.fragment.home.MainExpFragment;
import com.heimavista.fiedorasport.p111ui.fragment.home.MainHomeFragment;
import com.heimavista.fiedorasport.p111ui.fragment.home.MainMyFragment;
import com.heimavista.fiedorasport.service.JYBleService;
import com.heimavista.gad.HvGad;
import com.heimavista.gad.OnGadStartListener;
import com.heimavista.gad.info.AcInfo;
import com.heimavista.utils.ParamJsonData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.json.JSONObject;
import org.xutils.C5217x;
import p119e.p189e.p191b.HvApp;
import p119e.p189e.p219h.HttpParams;

/* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity */
public class HomeActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public ViewPager2 f6635b0;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public CommonTabLayout f6636c0;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public List<BaseFragment> f6637d0;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public boolean f6638e0 = false;

    /* renamed from: f0 */
    public String f6639f0;
    /* access modifiers changed from: private */

    /* renamed from: g0 */
    public ArrayList<CustomTabEntity> f6640g0;
    /* access modifiers changed from: private */

    /* renamed from: h0 */
    public int f6641h0 = 0;

    /* renamed from: i0 */
    private BroadcastReceiver f6642i0;

    /* renamed from: j0 */
    private int f6643j0 = 0;

    /* renamed from: k0 */
    private int f6644k0 = 0;

    /* renamed from: l0 */
    private int f6645l0 = 0;

    /* renamed from: m0 */
    private int f6646m0 = 3;

    /* renamed from: n0 */
    private int f6647n0 = 0;

    /* renamed from: o0 */
    private Bundle f6648o0;

    /* renamed from: p0 */
    private String f6649p0 = "";

    /* renamed from: q0 */
    private boolean f6650q0 = false;

    /* renamed from: r0 */
    private boolean f6651r0 = false;

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$a */
    class C3752a extends BroadcastReceiver {
        C3752a() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(intent.getAction())) {
                String stringExtra = intent.getStringExtra("reason");
                if (TextUtils.equals(stringExtra, "homekey")) {
                    LogUtils.m1139c("Pressed the home button");
                } else if (TextUtils.equals(stringExtra, "recentapps")) {
                    LogUtils.m1139c("Pressed the menu button");
                }
            }
            if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
                switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", 0)) {
                    case 10:
                        HomeActivity homeActivity = HomeActivity.this;
                        homeActivity.f6639f0 = homeActivity.getString(R$string.ble_state_off);
                        break;
                    case 11:
                        HomeActivity homeActivity2 = HomeActivity.this;
                        homeActivity2.f6639f0 = homeActivity2.getString(R$string.ble_state_turning_on);
                        break;
                    case 12:
                        HomeActivity.this.f6639f0 = "";
                        FSManager.m10323r().mo22831c();
                        break;
                    case 13:
                        HomeActivity homeActivity3 = HomeActivity.this;
                        homeActivity3.f6639f0 = homeActivity3.getString(R$string.ble_state_turning_off);
                        break;
                }
            }
            if ("fiedoraSport.ACTION_NOTIFY_BUDDY".equals(intent.getAction())) {
                HomeActivity.this.mo23013u();
            }
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$b */
    class C3753b implements OnTabSelectListener {
        C3753b() {
        }

        public void onTabReselect(int i) {
        }

        public void onTabSelect(int i) {
            if (HomeActivity.this.getString(R$string.tab_friends).equals(((CustomTabEntity) HomeActivity.this.f6640g0.get(i)).getTabTitle())) {
                HomeActivity.this.f6636c0.hideMsg(i);
                int unused = HomeActivity.this.f6641h0 = 0;
            }
            HomeActivity.this.f6635b0.setCurrentItem(i, false);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$c */
    class C3754c extends ViewPager2.OnPageChangeCallback {
        C3754c() {
        }

        public void onPageSelected(int i) {
            super.onPageSelected(i);
            HomeActivity.this.f6636c0.setCurrentTab(i);
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$d */
    class C3755d extends FragmentStateAdapter {
        C3755d(FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @NonNull
        public Fragment createFragment(int i) {
            return (Fragment) HomeActivity.this.f6637d0.get(i);
        }

        public int getItemCount() {
            return HomeActivity.this.f6637d0.size();
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$e */
    class C3756e implements OnGadStartListener {
        C3756e(HomeActivity homeActivity) {
        }

        public void onCancel() {
            JYSDKManager.m10388i().mo22884a(true);
            CRPBleManager.m10629e();
        }

        public void onError(String str) {
            JYSDKManager.m10388i().mo22884a(true);
            CRPBleManager.m10629e();
        }

        public void onSuccess() {
        }
    }

    /* renamed from: com.heimavista.fiedorasport.ui.activity.HomeActivity$f */
    class C3757f implements OnResultListener<ParamJsonData> {
        C3757f() {
        }

        /* renamed from: a */
        public void mo22379a(HttpParams cVar, HttpParams cVar2, String str, ParamJsonData fVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.heimavista.fiedorasport.ui.activity.HomeActivity.a(com.heimavista.fiedorasport.ui.activity.HomeActivity, boolean):boolean
         arg types: [com.heimavista.fiedorasport.ui.activity.HomeActivity, int]
         candidates:
          com.heimavista.fiedorasport.ui.activity.HomeActivity.a(com.heimavista.fiedorasport.ui.activity.HomeActivity, int):int
          com.heimavista.fiedorasport.ui.activity.HomeActivity.a(int, int):void
          com.heimavista.fiedorasport.ui.activity.HomeActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(int, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(long, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Context, android.content.Intent):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(android.content.Intent, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.blankj.swipepanel.SwipePanel, int):void
          com.heimavista.fiedorasport.base.BaseActivity.a(com.heimavista.widget.dialog.e$a, com.heimavista.widget.dialog.BaseDialog):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, androidx.core.app.ActivityOptionsCompat):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Class<? extends android.app.Activity>, com.heimavista.fiedorasport.base.BaseActivity$b):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.String, boolean):void
          com.heimavista.fiedorasport.base.BaseActivity.a(java.lang.Runnable, long):boolean
          com.heimavista.fiedorasport.h.y.a(int, int):void
          com.heimavista.fiedorasport.h.y.a(long, int):void
          com.heimavista.fiedorasport.ui.activity.HomeActivity.a(com.heimavista.fiedorasport.ui.activity.HomeActivity, boolean):boolean */
        /* renamed from: a */
        public void mo22380a(ParamJsonData fVar) {
            boolean unused = HomeActivity.this.f6638e0 = true;
            HomeActivity.this.m10731E();
        }
    }

    /* renamed from: B */
    private void m10728B() {
        if (FSManager.m10323r().mo22847q()) {
            if (!JYSDKManager.m10388i().mo22897e()) {
                TickManager.m10524b(TickType.NONE, System.currentTimeMillis() / 1000);
                m10733G();
                return;
            }
            this.f6651r0 = true;
            int min = Math.min(m10734a(TickType.SLEEP), 6);
            int min2 = Math.min(m10734a(TickType.HEART), 6);
            int min3 = Math.min(m10734a(TickType.SPORT), 6);
            this.f6646m0 = min + 3 + min2 + min3;
            m10741d("getBandData difDay " + min + " " + min2 + " " + min3 + " " + this.f6646m0);
            JYSDKManager.m10388i().mo22894d();
            int i = this.f6643j0;
            if (i <= min) {
                this.f6643j0 = i + 1;
                this.f6647n0++;
                int i2 = (this.f6647n0 * 100) / this.f6646m0;
                m10743e(getString(R$string.get_band_data_progress, new Object[]{Math.min(i2, 100) + "%"}));
                JYSDKManager.m10388i().mo22873a(1, min - i);
                return;
            }
            int i3 = this.f6644k0;
            if (i3 <= min2) {
                int i4 = min2 - i3;
                this.f6644k0 = i3 + 1;
                this.f6647n0++;
                int i5 = (this.f6647n0 * 100) / this.f6646m0;
                m10743e(getString(R$string.get_band_data_progress, new Object[]{Math.min(i5, 100) + "%"}));
                JYSDKManager.m10388i().mo22873a(2, i4);
                return;
            }
            TickManager.m10524b(TickType.NONE, System.currentTimeMillis() / 1000);
            int i6 = this.f6645l0;
            if (i6 <= min3) {
                int i7 = min3 - i6;
                this.f6645l0 = i6 + 1;
                this.f6647n0++;
                int i8 = (this.f6647n0 * 100) / this.f6646m0;
                m10743e(getString(R$string.get_band_data_progress, new Object[]{Math.min(i8, 100) + "%"}));
                JYSDKManager.m10388i().mo22872a(i7);
                return;
            }
            m10743e(getString(R$string.get_band_data_progress, new Object[]{"100%"}));
            m10733G();
        } else if (FSManager.m10323r().mo22846p()) {
            TickManager.m10524b(TickType.NONE, System.currentTimeMillis() / 1000);
            if (CRPBleManager.m10639j()) {
                this.f6651r0 = true;
                m10732F();
                return;
            }
            m10733G();
        } else {
            m10733G();
        }
    }

    /* renamed from: C */
    private void m10729C() {
        if (!this.f6650q0) {
            if (NetworkUtils.m858c()) {
                m10743e(getString(R$string.get_server_data_progress, new Object[]{"0%"}));
                this.f6650q0 = true;
                new HvApiGetBandDataTask().mo22477a(new C3763b(this));
                return;
            }
            m10728B();
        }
    }

    /* renamed from: D */
    static /* synthetic */ void m10730D() {
        new HvApiPostHeartData().request();
        new HvApiPostSleepData().request();
        new HvApiPostStepData().request();
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public void m10731E() {
        if (this.f6638e0) {
            if (FSManager.m10323r().mo22847q()) {
                JYSDKManager.m10388i().mo22899g();
            }
            if (FSManager.m10323r().mo22846p()) {
                CRPBleManager.m10651v();
                return;
            }
            return;
        }
        new HvApiGetBandSettings().request(new C3757f());
    }

    /* renamed from: F */
    private void m10732F() {
        CRPBleManager.m10649t();
        m10743e(getString(R$string.read_data_from_band));
        C5217x.task().postDelayed(new C3764c(this), 3000);
    }

    /* access modifiers changed from: private */
    /* renamed from: G */
    public void m10733G() {
        boolean z = false;
        this.f6647n0 = 0;
        this.f6646m0 = 3;
        m10743e("");
        this.f6643j0 = 0;
        this.f6644k0 = 0;
        this.f6645l0 = 0;
        this.f6650q0 = false;
        this.f6651r0 = false;
        if (FSManager.m10323r().mo22847q()) {
            z = JYSDKManager.m10388i().mo22897e();
        } else if (FSManager.m10323r().mo22846p()) {
            z = CRPBleManager.m10639j();
        }
        if (z) {
            new Thread(C3762a.f6663P).start();
        }
    }

    /* renamed from: A */
    public void mo23010A() {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22888b();
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10652w();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo22715a() {
        return true;
    }

    /* renamed from: e */
    public void mo22733e() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.fs_activity_home;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public List<String> mo22735g() {
        List<String> g = super.mo22735g();
        g.add("fiedoraSport.ACTION_refreshDataFromServer");
        g.add("android.intent.action.SCREEN_ON");
        return g;
    }

    public void onBackPressed() {
        ActivityUtils.m922a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View
     arg types: [com.heimavista.fiedorasport.ui.activity.HomeActivity, int]
     candidates:
      com.blankj.utilcode.util.d.a(android.content.Context, int):android.view.View
      com.blankj.utilcode.util.d.a(android.app.Activity, boolean):void
      com.blankj.utilcode.util.d.a(android.view.Window, boolean):void
      com.blankj.utilcode.util.d.a(android.app.Activity, int):android.view.View */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!TextUtils.isEmpty(FSManager.m10323r().mo22836f())) {
            JYSDKManager.m10388i().mo22898f();
            JYSDKManager.m10388i().mo22880a(HomeActivity.class.getCanonicalName(), this);
            CRPBleManager.m10643n();
            CRPBleManager.m10606a(HomeActivity.class.getCanonicalName(), this);
            CRPBleManager.m10645p();
            if (!ServiceUtils.m1268a(JYBleService.class)) {
                ServiceUtils.m1270b(JYBleService.class);
            }
            FSManager.m10323r().mo22831c();
        }
        BarUtils.m982a((Activity) this, getResources().getColor(17170445));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        intentFilter.addAction("fiedoraSport.ACTION_NOTIFY_BUDDY");
        this.f6642i0 = new C3752a();
        registerReceiver(this.f6642i0, intentFilter);
        if (NetworkUtils.m858c()) {
            new HvApiGetProfile().request();
        }
        if (getIntent() != null && getIntent().hasExtra("category")) {
            onNewIntent(getIntent());
        }
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter == null) {
            mo22730c(getString(R$string.ble_disabled));
        } else if (defaultAdapter.isEnabled()) {
            this.f6639f0 = "";
        } else {
            this.f6639f0 = getString(R$string.ble_unopened);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        BroadcastReceiver broadcastReceiver = this.f6642i0;
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        PushMsg pushMsg;
        JSONObject d;
        JSONObject optJSONObject;
        super.onNewIntent(intent);
        if (intent != null && intent.hasExtra("pushMsg") && (pushMsg = (PushMsg) intent.getParcelableExtra("pushMsg")) != null && TextUtils.equals(pushMsg.f6226P, "redirect") && (d = pushMsg.f6231U.mo25334d()) != null && (optJSONObject = d.optJSONObject("ext")) != null) {
            String optString = optJSONObject.optString("pageId");
            if ("buddyList".equalsIgnoreCase(optString)) {
                mo23015w();
            } else if ("expCenter".equalsIgnoreCase(optString)) {
                mo23016x();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo22749r() {
        return false;
    }

    /* renamed from: u */
    public void mo23013u() {
        CommonTabLayout commonTabLayout;
        if (BandConfig.m10064m().mo22562g() && (commonTabLayout = this.f6636c0) != null) {
            int i = this.f6641h0 + 1;
            this.f6641h0 = i;
            commonTabLayout.showMsg(1, i);
        }
    }

    /* renamed from: v */
    public String mo23014v() {
        return this.f6649p0;
    }

    /* renamed from: w */
    public void mo23015w() {
        List<BaseFragment> list;
        if (BandConfig.m10064m().mo22562g() && (list = this.f6637d0) != null && !list.isEmpty()) {
            this.f6635b0.setCurrentItem(1, false);
            HvApp.m13010c().mo24157a("fiedoraSport.ACTION_OPEN_LOCATION_PAGE");
        }
    }

    /* renamed from: x */
    public void mo23016x() {
        List<BaseFragment> list;
        if (BandConfig.m10064m().mo22563h() && (list = this.f6637d0) != null && !list.isEmpty()) {
            this.f6635b0.setCurrentItem(this.f6637d0.size() - 2, false);
        }
    }

    /* renamed from: y */
    public void mo23017y() {
        List<BaseFragment> list = this.f6637d0;
        if (list != null && !list.isEmpty()) {
            this.f6635b0.setCurrentItem(this.f6637d0.size() - 1, false);
        }
    }

    /* renamed from: z */
    public void mo23018z() {
        if (FSManager.m10323r().mo22847q()) {
            JYSDKManager.m10388i().mo22892c();
        }
        if (FSManager.m10323r().mo22846p()) {
            CRPBleManager.m10632f();
        }
    }

    /* renamed from: d */
    private void m10741d(String str) {
        LogUtils.m1139c("mady HomeAty " + getLifecycle().getCurrentState() + " " + str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f6636c0 = (CommonTabLayout) findViewById(R$id.commonTabLayout);
        this.f6635b0 = (ViewPager2) findViewById(R$id.viewpager2);
        this.f6635b0.setUserInputEnabled(false);
        this.f6635b0.setOrientation(0);
    }

    /* renamed from: e */
    public /* synthetic */ void mo23012e(int i) {
        int i2 = R$string.get_server_data_progress;
        m10743e(getString(i2, new Object[]{i + "%"}));
        if (i == 100) {
            this.f6650q0 = false;
            HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onGetHistoryEnd");
            m10728B();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        this.f6637d0 = new ArrayList();
        this.f6640g0 = new ArrayList<>();
        this.f6637d0.add(MainHomeFragment.m14555m());
        this.f6640g0.add(new TabEntity(getString(R$string.tab_status), R$mipmap.tabicon_home_2, R$mipmap.tabicon_home_1));
        BandConfig m = BandConfig.m10064m();
        if (m.mo22562g()) {
            this.f6637d0.add(MainBuddyFragment.m14535i());
            this.f6640g0.add(new TabEntity(getString(R$string.tab_friends), R$mipmap.tabicon_friends_2, R$mipmap.tabicon_friends_1));
        }
        if (m.mo22563h()) {
            this.f6637d0.add(MainExpFragment.m14539i());
            this.f6640g0.add(new TabEntity(getString(R$string.tab_cec), R$mipmap.tabicon_shop_2, R$mipmap.tabicon_shop_1));
        }
        this.f6637d0.add(MainMyFragment.m14592n());
        this.f6640g0.add(new TabEntity(getString(R$string.tab_user), R$mipmap.tabicon_user_2, R$mipmap.tabicon_user_1));
        this.f6636c0.setTabData(this.f6640g0);
        this.f6636c0.setOnTabSelectListener(new C3753b());
        this.f6635b0.registerOnPageChangeCallback(new C3754c());
        this.f6635b0.setAdapter(new C3755d(this));
        m10729C();
        AcInfo b = HvGad.m14915i().mo25008b();
        if (!TextUtils.isEmpty(FSManager.m10323r().mo22842l()) && b == null) {
            HvGad.m14915i().mo25004a(this, FSManager.m10323r().mo22842l(), new C3756e(this));
        }
    }

    /* renamed from: e */
    private void m10743e(String str) {
        this.f6649p0 = str;
        if (TextUtils.isEmpty(str)) {
            HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onDataSyncEnd");
            return;
        }
        if (this.f6648o0 == null) {
            this.f6648o0 = new Bundle();
        }
        this.f6648o0.putString("text", str);
        HvApp.m13010c().mo24158a("fiedoraSport.ACTION_onDataSyncing", this.f6648o0);
    }

    /* renamed from: b */
    public void mo22718b(int i) {
        m10741d("statusCallback status=" + i);
        if (getLifecycle().getCurrentState() != Lifecycle.State.CREATED) {
            if (i != 2) {
                m10733G();
            } else if (!this.f6650q0) {
                m10729C();
            } else if (!this.f6651r0) {
                m10728B();
            }
            Bundle bundle = new Bundle();
            bundle.putInt("status", i);
            HvApp.m13010c().mo24158a("fiedoraSport.ACTION_onBleStatusChange", bundle);
            if (i == 2) {
                m10731E();
            }
        }
    }

    /* renamed from: b */
    public void mo23011b(boolean z) {
        if (z) {
            CRPBleManager.m10642m();
        } else {
            CRPBleManager.m10625d();
        }
    }

    /* renamed from: a */
    public void mo22682a(int i, int i2) {
        m10741d("batteryCallback " + i);
        Bundle bundle = new Bundle();
        bundle.putInt("battery", i);
        bundle.putInt("status", i2);
        HvApp.m13010c().mo24158a("fiedoraSport.ACTION_onBleBatteryChange", bundle);
    }

    /* renamed from: a */
    public void mo22688a(long j, int i, int i2) {
        m10741d("sensorCallback " + j + " " + i);
        HvApp.m13010c().mo24157a("fiedoraSport.ACTION_onJYSensorChange");
    }

    /* renamed from: a */
    public void mo22690a(long j, int i, int i2, int i3, int i4) {
        m10741d("stepCallback " + i);
        Bundle bundle = new Bundle();
        bundle.putInt("step", i);
        HvApp.m13010c().mo24158a("fiedoraSport.ACTION_onJYStepChange", bundle);
    }

    /* renamed from: a */
    public void mo22683a(int i, int i2, long j) {
        m10741d("dayDataEndCallback result=" + i + ", type" + i2);
        m10728B();
    }

    /* renamed from: a */
    public void mo22705a(String str) {
        m10741d("multiSportDataCallback recordDate=" + str);
        m10728B();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22691a(Context context, Intent intent) {
        super.mo22691a(context, intent);
        if ("fiedoraSport.ACTION_refreshDataFromServer".equals(intent.getAction())) {
            m10729C();
        }
    }

    /* renamed from: a */
    private int m10734a(TickType hVar) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 23);
        instance.set(12, 59);
        instance.set(13, 59);
        long timeInMillis = (instance.getTimeInMillis() / 1000) - TickManager.m10517a(hVar);
        if (timeInMillis > 10800) {
            return (int) (timeInMillis / 86400);
        }
        return 0;
    }
}
