package com.heimavista.fiedorasport.base;

import android.util.SparseArray;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class BaseFragmentAdapter extends FragmentPagerAdapter {

    /* renamed from: a */
    private SparseArray<Fragment> f6428a;

    /* renamed from: b */
    private Fragment f6429b;

    /* renamed from: c */
    private FragmentManager f6430c;

    /* renamed from: a */
    public Fragment mo22779a() {
        return this.f6429b;
    }

    public void destroyItem(@NonNull ViewGroup viewGroup, int i, @NonNull Object obj) {
        this.f6430c.beginTransaction().hide((Fragment) obj).commit();
    }

    public int getCount() {
        return this.f6428a.size();
    }

    @NonNull
    public Fragment getItem(int i) {
        return this.f6428a.get(i);
    }

    public void setPrimaryItem(@NonNull ViewGroup viewGroup, int i, @NonNull Object obj) {
        if (mo22779a() != obj) {
            this.f6429b = (Fragment) obj;
        }
        super.setPrimaryItem(viewGroup, i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.FragmentPagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      com.heimavista.fiedorasport.base.BaseFragmentAdapter.instantiateItem(android.view.ViewGroup, int):androidx.fragment.app.Fragment
      androidx.viewpager.widget.PagerAdapter.instantiateItem(android.view.View, int):java.lang.Object
      androidx.fragment.app.FragmentPagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object */
    @NonNull
    public Fragment instantiateItem(@NonNull ViewGroup viewGroup, int i) {
        Fragment fragment = (Fragment) super.instantiateItem(viewGroup, i);
        if (this.f6428a.get(i) == null) {
            this.f6428a.put(i, fragment);
        }
        this.f6430c.beginTransaction().show(fragment).commit();
        return fragment;
    }
}
