package com.heimavista.fiedorasport.p111ui.activity.setting;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.ViewCompat;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.LogUtils;
import com.crrepa.ble.p049d.p052f.C1178n;
import com.crrepa.ble.p049d.p063q.C1275q;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.heimavista.fiedorasport.FSManager;
import com.heimavista.fiedorasport.base.BaseActivity;
import com.heimavista.fiedorasport.lib.R$array;
import com.heimavista.fiedorasport.lib.R$color;
import com.heimavista.fiedorasport.lib.R$id;
import com.heimavista.fiedorasport.lib.R$layout;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.lib.R$string;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p109h.CRPBleManager;
import com.heimavista.fiedorasport.p199j.PhotoUtils;
import com.heimavista.fiedorasport.widget.CommonListItemView;

/* renamed from: com.heimavista.fiedorasport.ui.activity.setting.EditWatchFaceActivity */
public class EditWatchFaceActivity extends BaseActivity {

    /* renamed from: b0 */
    private ImageView f8227b0;

    /* renamed from: c0 */
    private TextView f8228c0;

    /* renamed from: d0 */
    private TextView f8229d0;

    /* renamed from: e0 */
    private TextView f8230e0;

    /* renamed from: f0 */
    private CommonListItemView f8231f0;

    /* renamed from: g0 */
    private CommonListItemView f8232g0;

    /* renamed from: h0 */
    private CommonListItemView f8233h0;

    /* renamed from: i0 */
    private CharSequence[] f8234i0;

    /* renamed from: j0 */
    private CharSequence[] f8235j0;

    /* renamed from: k0 */
    private C1178n f8236k0;
    /* access modifiers changed from: private */

    /* renamed from: l0 */
    public ProgressDialog f8237l0;

    /* renamed from: m0 */
    private PhotoUtils f8238m0;
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public String f8239n0 = "";
    /* access modifiers changed from: private */

    /* renamed from: o0 */
    public boolean f8240o0 = false;

    /* renamed from: p0 */
    private boolean f8241p0 = true;

    /* renamed from: e */
    private String m13782e(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? "" : "30958" : "208 BPM" : "10H36M" : "SAT 0928";
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public void m13784u() {
        if (this.f8237l0 == null) {
            this.f8237l0 = new ProgressDialog(this);
            this.f8237l0.setTitle(R$string.watch_loading);
            this.f8237l0.setProgressStyle(1);
            this.f8237l0.setIndeterminate(false);
            this.f8237l0.setCancelable(false);
        }
        if (!this.f8237l0.isShowing()) {
            this.f8237l0.show();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v */
    public void m13785v() {
        if (this.f8240o0) {
            Intent intent = new Intent();
            intent.putExtra("timePosition", this.f8236k0.mo10868d());
            intent.putExtra("timeTopContent", this.f8236k0.mo10870e());
            intent.putExtra("timeBottomContent", this.f8236k0.mo10866c());
            intent.putExtra("textColor", this.f8236k0.mo10864b());
            intent.putExtra("picMD5", this.f8236k0.mo10861a());
            setResult(-1, intent);
        } else {
            setResult(0);
        }
        finish();
    }

    /* renamed from: w */
    private void m13786w() {
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(-2, -2);
        ConstraintLayout.LayoutParams layoutParams2 = new ConstraintLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = ConvertUtils.m1055a(15.0f);
        ConstraintLayout.LayoutParams layoutParams3 = new ConstraintLayout.LayoutParams(-2, -2);
        int d = this.f8236k0.mo10868d();
        if (d == 0) {
            layoutParams.topToTop = 0;
            layoutParams.endToEnd = R$id.tvTime;
            layoutParams2.topToBottom = R$id.tvTimeTopContent;
            layoutParams2.endToEnd = 0;
            int i = R$id.tvTime;
            layoutParams3.topToBottom = i;
            layoutParams3.endToEnd = i;
        } else if (d == 1) {
            int i2 = R$id.tvTime;
            layoutParams.bottomToTop = i2;
            layoutParams.endToEnd = i2;
            layoutParams2.bottomToTop = R$id.tvTimeBottomContent;
            layoutParams2.endToEnd = 0;
            layoutParams3.bottomToBottom = 0;
            layoutParams3.endToEnd = R$id.tvTime;
        }
        this.f8229d0.setLayoutParams(layoutParams);
        this.f8228c0.setLayoutParams(layoutParams2);
        this.f8230e0.setLayoutParams(layoutParams3);
        this.f8229d0.setText(m13782e(this.f8236k0.mo10870e()));
        this.f8230e0.setText(m13782e(this.f8236k0.mo10866c()));
        this.f8231f0.setDetailText(this.f8234i0[this.f8236k0.mo10868d()]);
        this.f8232g0.setDetailText(this.f8235j0[this.f8236k0.mo10870e()]);
        this.f8233h0.setDetailText(this.f8235j0[this.f8236k0.mo10866c()]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public int mo22734f() {
        return R$layout.activity_edit_watch_face;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public View mo22736h() {
        return findViewById(R$id.rootView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public CharSequence mo22739j() {
        return getString(R$string.watch_edit);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != -1) {
            return;
        }
        if (i == 500 && intent != null) {
            Uri data = intent.getData();
            if (data != null) {
                this.f8238m0.mo24363a(this, data, PsExtractor.VIDEO_STREAM_MASK, PsExtractor.VIDEO_STREAM_MASK, 503);
            }
        } else if (i == 503) {
            this.f8239n0 = this.f8238m0.mo24359a();
            Bitmap decodeFile = BitmapFactory.decodeFile(this.f8239n0);
            if (decodeFile == null) {
                this.f8227b0.setImageBitmap(decodeFile);
                return;
            }
            String b = this.f8238m0.mo24365b(this.f8239n0, this);
            if (!TextUtils.isEmpty(b)) {
                this.f8227b0.setImageBitmap(BitmapFactory.decodeFile(b));
            }
        }
    }

    public void onBackPressed() {
        m13785v();
    }

    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R$id.btnSelect) {
            if (this.f8238m0 == null) {
                this.f8238m0 = new PhotoUtils();
            }
            this.f8238m0.mo24366b(this, 500);
        } else if (id == R$id.btnReset) {
            this.f8236k0.mo10863a("00000000000000000000000000000000");
            this.f8227b0.setImageResource(R$mipmap.icon_watch_face01);
        }
        C1178n nVar = this.f8236k0;
        if (nVar == null) {
            return;
        }
        if (id == R$id.livTimePosition) {
            m13775a(R$string.watch_time_position, this.f8234i0, nVar.mo10868d());
        } else if (id == R$id.livTimeTopContent) {
            m13775a(R$string.watch_time_top_content, this.f8235j0, nVar.mo10870e());
        } else if (id == R$id.livTimeBottomContent) {
            m13775a(R$string.watch_time_bottom_content, this.f8235j0, nVar.mo10866c());
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mo22680a(menu, 16908331, getString(R$string.save), getResources().getColor(R$color.font_color), (Drawable) null, new C4306d(this));
        return super.onCreateOptionsMenu(menu);
    }

    /* renamed from: f */
    private void m13783f(int i) {
        C1178n nVar = this.f8236k0;
        if (nVar != null) {
            nVar.mo10862a(i);
        }
        this.f8228c0.setTextColor(i);
        this.f8229d0.setTextColor(i);
        this.f8230e0.setTextColor(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22720b(Bundle bundle) {
        this.f8227b0 = (ImageView) findViewById(R$id.ivWatchFace);
        this.f8228c0 = (TextView) findViewById(R$id.tvTime);
        this.f8229d0 = (TextView) findViewById(R$id.tvTimeTopContent);
        this.f8230e0 = (TextView) findViewById(R$id.tvTimeBottomContent);
        this.f8231f0 = (CommonListItemView) findViewById(R$id.livTimePosition);
        this.f8232g0 = (CommonListItemView) findViewById(R$id.livTimeTopContent);
        this.f8233h0 = (CommonListItemView) findViewById(R$id.livTimeBottomContent);
        mo22714a(this.f8231f0, this.f8232g0, this.f8233h0);
        mo22723b(R$id.btnSelect, R$id.btnReset);
        ((RadioGroup) findViewById(R$id.radioGroup)).setOnCheckedChangeListener(new C4304c(this));
    }

    /* renamed from: a */
    public /* synthetic */ void mo24470a(RadioGroup radioGroup, int i) {
        if (i == R$id.rbWhite) {
            m13783f(-1);
        } else if (i == R$id.rbBlack) {
            m13783f(ViewCompat.MEASURED_STATE_MASK);
        } else if (i == R$id.rbYellow) {
            m13783f(InputDeviceCompat.SOURCE_ANY);
        } else if (i == R$id.rbRed) {
            m13783f(SupportMenu.CATEGORY_MASK);
        } else if (i == R$id.rbGreen) {
            m13783f(-16711936);
        } else if (i == R$id.rbBlue) {
            m13783f(-16776961);
        } else if (i == R$id.rbCyan) {
            m13783f(-16711681);
        } else if (i == R$id.rbMagenta) {
            m13783f(-65281);
        }
    }

    /* renamed from: b */
    public void mo22718b(int i) {
        super.mo22718b(i);
        LogUtils.m1139c("mady crpBle status=" + i);
        if (i != 2 && this.f8241p0) {
            this.f8241p0 = false;
            CRPBleManager.m10615b(FSManager.m10323r().mo22842l());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22697a(Bundle bundle) {
        Intent intent = getIntent();
        this.f8236k0 = new C1178n(intent.getIntExtra("timePosition", 0), intent.getIntExtra("timeTopContent", 0), intent.getIntExtra("timeBottomContent", 1), intent.getIntExtra("textColor", -1), intent.getStringExtra("picMD5"));
        this.f8234i0 = getResources().getStringArray(R$array.watch_time_position);
        this.f8235j0 = getResources().getStringArray(R$array.watch_time_content);
        this.f8231f0.setDetailText(this.f8234i0[this.f8236k0.mo10868d()]);
        this.f8232g0.setDetailText(this.f8235j0[this.f8236k0.mo10870e()]);
        this.f8233h0.setDetailText(this.f8235j0[this.f8236k0.mo10866c()]);
        String i = BandDataManager.m10567i();
        if (TextUtils.isEmpty(i)) {
            this.f8227b0.setImageResource(R$mipmap.icon_watch_face01);
        } else {
            this.f8227b0.setImageBitmap(BitmapFactory.decodeFile(i));
        }
        m13783f(this.f8236k0.mo10864b());
        m13786w();
    }

    /* renamed from: a */
    private void m13775a(int i, CharSequence[] charSequenceArr, int i2) {
        new AlertDialog.Builder(this).setTitle(i).setSingleChoiceItems(charSequenceArr, i2, new C4310f(this, i)).setPositiveButton(R$string.finish, C4312g.f8308P).create().show();
    }

    /* renamed from: a */
    public /* synthetic */ void mo24468a(int i, DialogInterface dialogInterface, int i2) {
        C1178n nVar = this.f8236k0;
        if (nVar != null) {
            if (i == R$string.watch_time_position) {
                nVar.mo10867c(i2);
            } else if (i == R$string.watch_time_top_content) {
                nVar.mo10869d(i2);
            } else if (i == R$string.watch_time_bottom_content) {
                nVar.mo10865b(i2);
            }
            m13786w();
        }
    }

    /* renamed from: a */
    public /* synthetic */ void mo24469a(View view) {
        this.f8240o0 = true;
        CRPBleManager.m10587a((byte) 1);
        CRPBleManager.m10595a(this.f8236k0, new C4308e(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.h.z.a(android.graphics.Bitmap, boolean, com.crrepa.ble.d.q.q):void
     arg types: [android.graphics.Bitmap, int, com.heimavista.fiedorasport.ui.activity.setting.j0]
     candidates:
      com.heimavista.fiedorasport.h.z.a(boolean, int, com.crrepa.ble.d.f.e):void
      com.heimavista.fiedorasport.h.z.a(android.graphics.Bitmap, boolean, com.crrepa.ble.d.q.q):void */
    /* renamed from: a */
    public /* synthetic */ void mo24471a(C1178n nVar) {
        this.f8236k0 = nVar;
        int a = CRPBleManager.m10584a(this.f8236k0.mo10864b());
        this.f8236k0.mo10862a(Color.rgb(Color.red(a), Color.green(a), Color.blue(a)));
        if (this.f8238m0 != null && !TextUtils.isEmpty(this.f8239n0)) {
            Bitmap decodeFile = BitmapFactory.decodeFile(this.f8239n0);
            if (decodeFile == null) {
                String b = this.f8238m0.mo24365b(this.f8239n0, this);
                if (!TextUtils.isEmpty(b)) {
                    decodeFile = BitmapFactory.decodeFile(b);
                }
            }
            if (decodeFile != null) {
                CRPBleManager.m10591a(decodeFile, true, (C1275q) new C4319j0(this));
                return;
            }
        }
        m13785v();
    }
}
