package com.yanzhenjie.zbar.camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;

public class CameraPreview extends FrameLayout implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public Runnable mAutoFocusTask;
    /* access modifiers changed from: private */
    public CameraManager mCameraManager;
    /* access modifiers changed from: private */
    public Camera.AutoFocusCallback mFocusCallback;
    private CameraScanAnalysis mPreviewCallback;
    private SurfaceView mSurfaceView;

    /* renamed from: com.yanzhenjie.zbar.camera.CameraPreview$a */
    class C4918a implements Camera.AutoFocusCallback {
        C4918a() {
        }

        public void onAutoFocus(boolean z, Camera camera) {
            CameraPreview cameraPreview = CameraPreview.this;
            cameraPreview.postDelayed(cameraPreview.mAutoFocusTask, 1000);
        }
    }

    /* renamed from: com.yanzhenjie.zbar.camera.CameraPreview$b */
    class C4919b implements Runnable {
        C4919b() {
        }

        public void run() {
            CameraPreview.this.mCameraManager.autoFocus(CameraPreview.this.mFocusCallback);
        }
    }

    public CameraPreview(Context context) {
        this(context, null);
    }

    private void startCameraPreview(SurfaceHolder surfaceHolder) {
        try {
            this.mCameraManager.startPreview(surfaceHolder, this.mPreviewCallback);
            this.mCameraManager.autoFocus(this.mFocusCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        stop();
        super.onDetachedFromWindow();
    }

    public void setScanCallback(ScanCallback scanCallback) {
        this.mPreviewCallback.mo27166a(scanCallback);
    }

    public boolean start() {
        try {
            this.mCameraManager.openDriver();
            this.mPreviewCallback.mo27165a();
            if (this.mSurfaceView == null) {
                this.mSurfaceView = new SurfaceView(getContext());
                addView(this.mSurfaceView, new FrameLayout.LayoutParams(-1, -1));
                SurfaceHolder holder = this.mSurfaceView.getHolder();
                holder.addCallback(this);
                holder.setType(3);
            }
            startCameraPreview(this.mSurfaceView.getHolder());
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public void stop() {
        removeCallbacks(this.mAutoFocusTask);
        this.mPreviewCallback.mo27167b();
        this.mCameraManager.stopPreview();
        this.mCameraManager.closeDriver();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (surfaceHolder.getSurface() != null) {
            this.mCameraManager.stopPreview();
            startCameraPreview(surfaceHolder);
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public CameraPreview(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CameraPreview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mFocusCallback = new C4918a();
        this.mAutoFocusTask = new C4919b();
        this.mCameraManager = new CameraManager(context);
        this.mPreviewCallback = new CameraScanAnalysis();
    }
}
