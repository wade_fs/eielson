package com.yanzhenjie.zbar.camera;

import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.yanzhenjie.zbar.Image;
import com.yanzhenjie.zbar.ImageScanner;
import com.yanzhenjie.zbar.Symbol;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: com.yanzhenjie.zbar.camera.a */
class CameraScanAnalysis implements Camera.PreviewCallback {

    /* renamed from: a */
    private ExecutorService f11140a = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ImageScanner f11141b = new ImageScanner();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Handler f11142c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public ScanCallback f11143d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f11144e = true;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public Image f11145f;

    /* renamed from: g */
    private Runnable f11146g = new C4922b();

    /* renamed from: com.yanzhenjie.zbar.camera.a$a */
    /* compiled from: CameraScanAnalysis */
    class C4921a extends Handler {
        C4921a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            if (CameraScanAnalysis.this.f11143d != null) {
                CameraScanAnalysis.this.f11143d.onScanResult((String) message.obj);
            }
        }
    }

    /* renamed from: com.yanzhenjie.zbar.camera.a$b */
    /* compiled from: CameraScanAnalysis */
    class C4922b implements Runnable {
        C4922b() {
        }

        public void run() {
            String str = null;
            if (CameraScanAnalysis.this.f11141b.scanImage(CameraScanAnalysis.this.f11145f) != 0) {
                Iterator<Symbol> it = CameraScanAnalysis.this.f11141b.getResults().iterator();
                while (it.hasNext()) {
                    str = it.next().getData();
                }
            }
            if (!TextUtils.isEmpty(str)) {
                Message obtainMessage = CameraScanAnalysis.this.f11142c.obtainMessage();
                obtainMessage.obj = str;
                obtainMessage.sendToTarget();
                return;
            }
            boolean unused = CameraScanAnalysis.this.f11144e = true;
        }
    }

    CameraScanAnalysis() {
        this.f11141b.setConfig(0, 256, 3);
        this.f11141b.setConfig(0, 257, 3);
        this.f11142c = new C4921a(Looper.getMainLooper());
    }

    public void onPreviewFrame(byte[] bArr, Camera camera) {
        if (this.f11144e) {
            this.f11144e = false;
            Camera.Size previewSize = camera.getParameters().getPreviewSize();
            this.f11145f = new Image(previewSize.width, previewSize.height, "Y800");
            this.f11145f.setData(bArr);
            this.f11140a.execute(this.f11146g);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo27167b() {
        this.f11144e = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27166a(ScanCallback scanCallback) {
        this.f11143d = scanCallback;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo27165a() {
        this.f11144e = true;
    }
}
