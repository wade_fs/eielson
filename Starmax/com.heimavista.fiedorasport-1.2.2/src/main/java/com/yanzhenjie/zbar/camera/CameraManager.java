package com.yanzhenjie.zbar.camera;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import java.io.IOException;

public final class CameraManager {
    private Camera mCamera;
    private final CameraConfiguration mConfiguration;

    public CameraManager(Context context) {
        this.mConfiguration = new CameraConfiguration(context);
    }

    public void autoFocus(Camera.AutoFocusCallback autoFocusCallback) {
        Camera camera = this.mCamera;
        if (camera != null) {
            try {
                camera.autoFocus(autoFocusCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void closeDriver() {
        if (this.mCamera != null) {
            this.mCamera.setPreviewCallback(null);
            this.mCamera.release();
            this.mCamera = null;
        }
    }

    public CameraConfiguration getConfiguration() {
        return this.mConfiguration;
    }

    public boolean isOpen() {
        return this.mCamera != null;
    }

    public synchronized void openDriver() {
        String str;
        if (this.mCamera == null) {
            this.mCamera = Camera.open();
            if (this.mCamera != null) {
                this.mConfiguration.initFromCameraParameters(this.mCamera);
                Camera.Parameters parameters = this.mCamera.getParameters();
                if (parameters == null) {
                    str = null;
                } else {
                    str = parameters.flatten();
                }
                try {
                    this.mConfiguration.setDesiredCameraParameters(this.mCamera, false);
                } catch (RuntimeException unused) {
                    if (str != null) {
                        Camera.Parameters parameters2 = this.mCamera.getParameters();
                        parameters2.unflatten(str);
                        try {
                            this.mCamera.setParameters(parameters2);
                            this.mConfiguration.setDesiredCameraParameters(this.mCamera, true);
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                throw new IOException("The camera is occupied.");
            }
        } else {
            return;
        }
    }

    public void startPreview(SurfaceHolder surfaceHolder, Camera.PreviewCallback previewCallback) {
        Camera camera = this.mCamera;
        if (camera != null) {
            camera.setDisplayOrientation(90);
            this.mCamera.setPreviewDisplay(surfaceHolder);
            this.mCamera.setPreviewCallback(previewCallback);
            this.mCamera.startPreview();
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0007 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopPreview() {
        /*
            r2 = this;
            android.hardware.Camera r0 = r2.mCamera
            if (r0 == 0) goto L_0x000d
            r0.stopPreview()     // Catch:{ Exception -> 0x0007 }
        L_0x0007:
            android.hardware.Camera r0 = r2.mCamera     // Catch:{ IOException -> 0x000d }
            r1 = 0
            r0.setPreviewDisplay(r1)     // Catch:{ IOException -> 0x000d }
        L_0x000d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yanzhenjie.zbar.camera.CameraManager.stopPreview():void");
    }
}
