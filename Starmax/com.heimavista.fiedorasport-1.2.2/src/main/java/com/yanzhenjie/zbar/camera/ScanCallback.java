package com.yanzhenjie.zbar.camera;

public interface ScanCallback {
    void onScanResult(String str);
}
