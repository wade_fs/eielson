package com.facebook.internal;

import com.facebook.FacebookSdk;
import p119e.p120a.p121a.p122a.InstallReferrerClient;
import p119e.p120a.p121a.p122a.InstallReferrerStateListener;

public class InstallReferrerUtil {
    private static final String IS_REFERRER_UPDATED = "is_referrer_updated";

    public interface Callback {
        void onReceiveReferrerUrl(String str);
    }

    private InstallReferrerUtil() {
    }

    private static boolean isUpdated() {
        return FacebookSdk.getApplicationContext().getSharedPreferences(FacebookSdk.APP_EVENT_PREFERENCES, 0).getBoolean(IS_REFERRER_UPDATED, false);
    }

    private static void tryConnectReferrerInfo(final Callback callback) {
        final InstallReferrerClient a = InstallReferrerClient.m11057a(FacebookSdk.getApplicationContext()).mo23147a();
        a.mo23146a(new InstallReferrerStateListener() {
            /* class com.facebook.internal.InstallReferrerUtil.C14961 */

            public void onInstallReferrerServiceDisconnected() {
            }

            public void onInstallReferrerSetupFinished(int i) {
                if (i == 0) {
                    try {
                        String a = a.mo23145a().mo23151a();
                        if (a != null && (a.contains("fb") || a.contains("facebook"))) {
                            callback.onReceiveReferrerUrl(a);
                        }
                        InstallReferrerUtil.updateReferrer();
                    } catch (Exception unused) {
                    }
                } else if (i == 2) {
                    InstallReferrerUtil.updateReferrer();
                }
            }
        });
    }

    public static void tryUpdateReferrerInfo(Callback callback) {
        if (!isUpdated()) {
            tryConnectReferrerInfo(callback);
        }
    }

    /* access modifiers changed from: private */
    public static void updateReferrer() {
        FacebookSdk.getApplicationContext().getSharedPreferences(FacebookSdk.APP_EVENT_PREFERENCES, 0).edit().putBoolean(IS_REFERRER_UPDATED, true).apply();
    }
}
