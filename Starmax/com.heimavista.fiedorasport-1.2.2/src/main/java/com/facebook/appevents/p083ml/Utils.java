package com.facebook.appevents.p083ml;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.nio.charset.Charset;

/* renamed from: com.facebook.appevents.ml.Utils */
public class Utils {
    static String normalizeString(String str) {
        return TextUtils.join(" ", str.trim().split("\\s+"));
    }

    static int[] vectorize(String str, int i) {
        int[] iArr = new int[i];
        byte[] bytes = normalizeString(str).getBytes(Charset.forName(C1750C.UTF8_NAME));
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 < bytes.length) {
                iArr[i2] = bytes[i2] & 255;
            } else {
                iArr[i2] = 0;
            }
        }
        return iArr;
    }
}
