package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;

/* renamed from: com.sxr.sdk.ble.keepfit.aidl.a */
public interface IRemoteService extends IInterface {
    /* renamed from: A */
    int mo26441A();

    /* renamed from: B */
    int mo26442B();

    /* renamed from: C */
    int mo26443C();

    /* renamed from: C */
    int mo26444C(int i);

    /* renamed from: D */
    int mo26445D();

    /* renamed from: G */
    int mo26446G(int i);

    /* renamed from: a */
    int mo26447a(int i, int i2, int i3, int i4, int i5);

    /* renamed from: a */
    int mo26448a(int i, int i2, int i3, int i4, int i5, int i6, int i7);

    /* renamed from: a */
    int mo26449a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8);

    /* renamed from: a */
    int mo26450a(BleClientOption bleClientOption);

    /* renamed from: a */
    int mo26451a(String str);

    /* renamed from: a */
    int mo26452a(String str, String str2);

    /* renamed from: a */
    int mo26453a(boolean z);

    /* renamed from: a */
    int mo26454a(boolean z, int i);

    /* renamed from: a */
    int mo26455a(boolean z, int i, int i2);

    /* renamed from: a */
    int mo26456a(boolean z, int i, int i2, int i3, int i4, int i5, int i6);

    /* renamed from: a */
    int mo26457a(boolean z, String str, String str2);

    /* renamed from: a */
    int mo26458a(String[] strArr, String[] strArr2, boolean z);

    /* renamed from: a */
    void mo26459a(IServiceCallback bVar);

    /* renamed from: a */
    boolean mo26460a(String str, int i, String str2, String str3);

    /* renamed from: b */
    int mo26461b(int i, String str);

    /* renamed from: b */
    int mo26462b(String str, byte[] bArr);

    /* renamed from: b */
    int mo26463b(byte[] bArr);

    /* renamed from: b */
    void mo26464b(IServiceCallback bVar);

    /* renamed from: b */
    void mo26465b(boolean z);

    /* renamed from: c */
    int mo26466c(boolean z);

    /* renamed from: e */
    int mo26467e(int i, int i2);

    /* renamed from: e */
    int mo26468e(boolean z);

    /* renamed from: f */
    int mo26469f(boolean z);

    /* renamed from: g */
    int mo26470g(int i);

    /* renamed from: g */
    int mo26471g(boolean z);

    /* renamed from: h */
    int mo26472h(boolean z);

    /* renamed from: i */
    int mo26473i(int i);

    /* renamed from: l */
    int mo26474l(int i);

    /* renamed from: n */
    int mo26475n(int i);

    /* renamed from: n */
    boolean mo26476n();

    /* renamed from: o */
    int mo26477o();

    /* renamed from: p */
    int mo26478p();

    /* renamed from: q */
    int mo26479q();

    /* renamed from: s */
    int mo26480s();

    /* renamed from: u */
    int mo26481u();

    /* renamed from: v */
    int mo26482v();

    /* renamed from: w */
    int mo26483w();

    /* renamed from: w */
    int mo26484w(int i);

    /* renamed from: x */
    int mo26485x();

    /* renamed from: y */
    String mo26486y();

    /* renamed from: z */
    int mo26487z();

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.a$a */
    /* compiled from: IRemoteService */
    public static abstract class C4760a extends Binder implements IRemoteService {
        public C4760a() {
            attachInterface(this, "com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
        }

        /* renamed from: a */
        public static IRemoteService m15967a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteService)) {
                return new C4761a(iBinder);
            }
            return (IRemoteService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i != 1598968902) {
                boolean z = false;
                switch (i) {
                    case 1:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        mo26464b(IServiceCallback.C4762a.m16039a(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 2:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        mo26459a(IServiceCallback.C4762a.m16039a(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 3:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int z2 = mo26487z();
                        parcel2.writeNoException();
                        parcel2.writeInt(z2);
                        return true;
                    case 4:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a = mo26450a(parcel.readInt() != 0 ? BleClientOption.CREATOR.createFromParcel(parcel) : null);
                        parcel2.writeNoException();
                        parcel2.writeInt(a);
                        return true;
                    case 5:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int w = mo26484w(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(w);
                        return true;
                    case 6:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int h = mo26472h(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(h);
                        return true;
                    case 7:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a2 = mo26452a(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(a2);
                        return true;
                    case 8:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        boolean n = mo26476n();
                        parcel2.writeNoException();
                        parcel2.writeInt(n ? 1 : 0);
                        return true;
                    case 9:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        String y = mo26486y();
                        parcel2.writeNoException();
                        parcel2.writeString(y);
                        return true;
                    case 10:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        mo26465b(z);
                        parcel2.writeNoException();
                        return true;
                    case 11:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int C = mo26443C();
                        parcel2.writeNoException();
                        parcel2.writeInt(C);
                        return true;
                    case 12:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int B = mo26442B();
                        parcel2.writeNoException();
                        parcel2.writeInt(B);
                        return true;
                    case 13:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int x = mo26485x();
                        parcel2.writeNoException();
                        parcel2.writeInt(x);
                        return true;
                    case 14:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int n2 = mo26475n(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(n2);
                        return true;
                    case 15:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int c = mo26466c(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(c);
                        return true;
                    case 16:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a3 = mo26447a(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a3);
                        return true;
                    case 17:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a4 = mo26449a(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a4);
                        return true;
                    case 18:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int A = mo26441A();
                        parcel2.writeNoException();
                        parcel2.writeInt(A);
                        return true;
                    case 19:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int q = mo26479q();
                        parcel2.writeNoException();
                        parcel2.writeInt(q);
                        return true;
                    case 20:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int o = mo26477o();
                        parcel2.writeNoException();
                        parcel2.writeInt(o);
                        return true;
                    case 21:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int i3 = mo26473i(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(i3);
                        return true;
                    case 22:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        boolean a5 = mo26460a(parcel.readString(), parcel.readInt(), parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(a5 ? 1 : 0);
                        return true;
                    case 23:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int a6 = mo26454a(z, parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a6);
                        return true;
                    case 24:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a7 = mo26456a(parcel.readInt() != 0, parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a7);
                        return true;
                    case 25:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int s = mo26480s();
                        parcel2.writeNoException();
                        parcel2.writeInt(s);
                        return true;
                    case 26:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int C2 = mo26444C(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(C2);
                        return true;
                    case 27:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int e = mo26467e(parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(e);
                        return true;
                    case 28:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int p = mo26478p();
                        parcel2.writeNoException();
                        parcel2.writeInt(p);
                        return true;
                    case 29:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int w2 = mo26483w();
                        parcel2.writeNoException();
                        parcel2.writeInt(w2);
                        return true;
                    case 30:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int f = mo26469f(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(f);
                        return true;
                    case 31:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int g = mo26471g(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(g);
                        return true;
                    case 32:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int g2 = mo26470g(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(g2);
                        return true;
                    case 33:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int l = mo26474l(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(l);
                        return true;
                    case 34:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int u = mo26481u();
                        parcel2.writeNoException();
                        parcel2.writeInt(u);
                        return true;
                    case 35:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int a8 = mo26455a(z, parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a8);
                        return true;
                    case 36:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int a9 = mo26457a(z, parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(a9);
                        return true;
                    case 37:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int a10 = mo26453a(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(a10);
                        return true;
                    case 38:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int b = mo26463b(parcel.createByteArray());
                        parcel2.writeNoException();
                        parcel2.writeInt(b);
                        return true;
                    case 39:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int D = mo26445D();
                        parcel2.writeNoException();
                        parcel2.writeInt(D);
                        return true;
                    case 40:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        String[] createStringArray = parcel.createStringArray();
                        String[] createStringArray2 = parcel.createStringArray();
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int a11 = mo26458a(createStringArray, createStringArray2, z);
                        parcel2.writeNoException();
                        parcel2.writeInt(a11);
                        return true;
                    case 41:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int b2 = mo26462b(parcel.readString(), parcel.createByteArray());
                        parcel2.writeNoException();
                        parcel2.writeInt(b2);
                        return true;
                    case 42:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        int e2 = mo26468e(z);
                        parcel2.writeNoException();
                        parcel2.writeInt(e2);
                        return true;
                    case 43:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int G = mo26446G(parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(G);
                        return true;
                    case 44:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a12 = mo26451a(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(a12);
                        return true;
                    case 45:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int v = mo26482v();
                        parcel2.writeNoException();
                        parcel2.writeInt(v);
                        return true;
                    case 46:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int a13 = mo26448a(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeInt(a13);
                        return true;
                    case 47:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                        int b3 = mo26461b(parcel.readInt(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(b3);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                return true;
            }
        }

        /* renamed from: com.sxr.sdk.ble.keepfit.aidl.a$a$a */
        /* compiled from: IRemoteService */
        private static class C4761a implements IRemoteService {

            /* renamed from: a */
            private IBinder f10167a;

            C4761a(IBinder iBinder) {
                this.f10167a = iBinder;
            }

            /* renamed from: A */
            public int mo26441A() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    this.f10167a.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: B */
            public int mo26442B() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    this.f10167a.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo26459a(IServiceCallback bVar) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    this.f10167a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f10167a;
            }

            /* renamed from: b */
            public void mo26464b(IServiceCallback bVar) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    this.f10167a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: e */
            public int mo26467e(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f10167a.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: f */
            public int mo26469f(boolean z) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    this.f10167a.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: g */
            public int mo26470g(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(i);
                    this.f10167a.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: h */
            public int mo26472h(boolean z) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    this.f10167a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: l */
            public int mo26474l(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(i);
                    this.f10167a.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: n */
            public boolean mo26476n() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    boolean z = false;
                    this.f10167a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: o */
            public int mo26477o() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    this.f10167a.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: s */
            public int mo26480s() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    this.f10167a.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: x */
            public int mo26485x() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    this.f10167a.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26450a(BleClientOption bleClientOption) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    if (bleClientOption != null) {
                        obtain.writeInt(1);
                        bleClientOption.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f10167a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo26465b(boolean z) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    this.f10167a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26452a(String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f10167a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26447a(int i, int i2, int i3, int i4, int i5) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    obtain.writeInt(i5);
                    this.f10167a.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public boolean mo26460a(String str, int i, String str2, String str3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    boolean z = false;
                    this.f10167a.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26454a(boolean z, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeInt(i);
                    this.f10167a.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26456a(boolean z, int i, int i2, int i3, int i4, int i5, int i6) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    obtain.writeInt(i5);
                    obtain.writeInt(i6);
                    this.f10167a.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public int mo26457a(boolean z, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IRemoteService");
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f10167a.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
