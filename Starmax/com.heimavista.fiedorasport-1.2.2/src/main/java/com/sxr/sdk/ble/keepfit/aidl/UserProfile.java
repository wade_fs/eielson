package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class UserProfile implements Parcelable {
    public static final Parcelable.Creator<UserProfile> CREATOR = new C4758a();

    /* renamed from: P */
    private int f10150P = 0;

    /* renamed from: Q */
    private int f10151Q = 0;

    /* renamed from: R */
    private int f10152R = 0;

    /* renamed from: S */
    private int f10153S = 0;

    /* renamed from: T */
    private int f10154T = 0;

    /* renamed from: U */
    private int f10155U = 0;

    /* renamed from: V */
    private int f10156V = 0;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.UserProfile$a */
    static class C4758a implements Parcelable.Creator<UserProfile> {
        C4758a() {
        }

        public UserProfile createFromParcel(Parcel parcel) {
            return new UserProfile(parcel);
        }

        public UserProfile[] newArray(int i) {
            return new UserProfile[i];
        }
    }

    public UserProfile(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f10150P = i;
        this.f10151Q = i2;
        this.f10152R = i3;
        this.f10153S = i4;
        this.f10154T = i5;
        this.f10155U = i6;
        this.f10156V = i7;
    }

    /* renamed from: a */
    public int mo26413a() {
        return this.f10156V;
    }

    /* renamed from: b */
    public int mo26415b() {
        return this.f10155U;
    }

    /* renamed from: c */
    public int mo26417c() {
        return this.f10151Q;
    }

    /* renamed from: d */
    public void mo26420d(int i) {
        this.f10152R = i;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public int mo26422e() {
        return this.f10154T;
    }

    /* renamed from: f */
    public int mo26423f() {
        return this.f10152R;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f10150P);
        parcel.writeInt(this.f10151Q);
        parcel.writeInt(this.f10152R);
        parcel.writeInt(this.f10153S);
        parcel.writeInt(this.f10154T);
        parcel.writeInt(this.f10155U);
        parcel.writeInt(this.f10156V);
    }

    /* renamed from: a */
    public void mo26414a(int i) {
        this.f10156V = i;
    }

    /* renamed from: b */
    public void mo26416b(int i) {
        this.f10155U = i;
    }

    /* renamed from: c */
    public void mo26418c(int i) {
        this.f10151Q = i;
    }

    /* renamed from: d */
    public int mo26419d() {
        return this.f10153S;
    }

    public UserProfile(Parcel parcel) {
        this.f10150P = parcel.readInt();
        this.f10151Q = parcel.readInt();
        this.f10152R = parcel.readInt();
        this.f10153S = parcel.readInt();
        this.f10154T = parcel.readInt();
        this.f10155U = parcel.readInt();
        this.f10156V = parcel.readInt();
    }
}
