package com.sxr.sdk.ble.keepfit.service.p212c;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.b */
public class ConvertUtil {
    /* renamed from: a */
    public static byte[] m16333a(int i) {
        return new byte[]{(byte) (i & 255), (byte) ((i >> 8) & 255), (byte) ((i >> 16) & 255), (byte) (i >>> 24)};
    }
}
