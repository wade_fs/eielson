package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.o */
/* compiled from: BluetoothLeService */
class C4785o implements C4771h.C4773b {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10348a;

    C4785o(BluetoothLeService bluetoothLeService) {
        this.f10348a = bluetoothLeService;
    }

    /* renamed from: a */
    public void mo26498a(int i, String str) {
        CrashApp.m16337d("BluetoothLeService", "gear_auth " + i + " " + str);
        if (i != 200) {
            try {
                if (this.f10348a.f10228j0 != null) {
                    this.f10348a.f10228j0.mo22948u(i);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else {
            this.f10348a.m16157a(new JSONObject(str));
            if (this.f10348a.f10228j0 != null) {
                this.f10348a.f10228j0.mo22948u(this.f10348a.f10238o0);
            }
        }
    }
}
