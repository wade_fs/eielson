package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;

/* renamed from: com.sxr.sdk.ble.keepfit.service.g */
/* compiled from: BluetoothLeService */
class C4777g implements Runnable {

    /* renamed from: P */
    final /* synthetic */ String[] f10338P;

    /* renamed from: Q */
    final /* synthetic */ BluetoothLeService f10339Q;

    C4777g(BluetoothLeService bluetoothLeService, String[] strArr) {
        this.f10339Q = bluetoothLeService;
        this.f10338P = strArr;
    }

    public void run() {
        if (this.f10339Q.f10228j0 != null) {
            try {
                this.f10339Q.f10228j0.mo22912a(Integer.parseInt(this.f10338P[0], 16) - 15, (long) (Integer.parseInt(this.f10338P[4] + this.f10338P[3] + this.f10338P[2] + this.f10338P[1], 16) - (this.f10339Q.f10237n1 / 1000)));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
