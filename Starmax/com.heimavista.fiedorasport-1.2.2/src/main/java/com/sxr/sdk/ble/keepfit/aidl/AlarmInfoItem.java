package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class AlarmInfoItem implements Parcelable {
    public static final Parcelable.Creator<AlarmInfoItem> CREATOR = new C4754a();

    /* renamed from: P */
    private long f10119P;

    /* renamed from: Q */
    private int f10120Q;

    /* renamed from: R */
    private int f10121R;

    /* renamed from: S */
    private int f10122S;

    /* renamed from: T */
    private int f10123T;

    /* renamed from: U */
    private int f10124U;

    /* renamed from: V */
    private int f10125V;

    /* renamed from: W */
    private int f10126W;

    /* renamed from: X */
    private int f10127X;

    /* renamed from: Y */
    private int f10128Y;

    /* renamed from: Z */
    private int f10129Z;

    /* renamed from: a0 */
    private boolean f10130a0 = false;

    /* renamed from: b0 */
    private String f10131b0;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem$a */
    static class C4754a implements Parcelable.Creator<AlarmInfoItem> {
        C4754a() {
        }

        public AlarmInfoItem createFromParcel(Parcel parcel) {
            return new AlarmInfoItem(parcel);
        }

        public AlarmInfoItem[] newArray(int i) {
            return new AlarmInfoItem[i];
        }
    }

    public AlarmInfoItem() {
    }

    /* renamed from: a */
    public void mo26357a(boolean z) {
        this.f10130a0 = z;
    }

    /* renamed from: b */
    public void mo26359b(int i) {
        this.f10123T = i;
    }

    /* renamed from: c */
    public int mo26360c() {
        return this.f10127X;
    }

    /* renamed from: d */
    public int mo26362d() {
        return this.f10123T;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public void mo26366e(int i) {
        this.f10126W = i;
    }

    /* renamed from: f */
    public void mo26368f(int i) {
        this.f10124U = i;
    }

    /* renamed from: g */
    public void mo26370g(int i) {
        this.f10120Q = i;
    }

    /* renamed from: h */
    public int mo26371h() {
        return this.f10124U;
    }

    /* renamed from: i */
    public int mo26373i() {
        return this.f10120Q;
    }

    /* renamed from: j */
    public int mo26375j() {
        return this.f10125V;
    }

    /* renamed from: k */
    public int mo26377k() {
        return this.f10121R;
    }

    /* renamed from: l */
    public int mo26378l() {
        return this.f10122S;
    }

    /* renamed from: m */
    public boolean mo26379m() {
        return this.f10130a0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f10119P);
        parcel.writeInt(this.f10120Q);
        parcel.writeInt(this.f10121R);
        parcel.writeInt(this.f10122S);
        parcel.writeInt(this.f10123T);
        parcel.writeInt(this.f10124U);
        parcel.writeInt(this.f10125V);
        parcel.writeInt(this.f10126W);
        parcel.writeInt(this.f10127X);
        parcel.writeInt(this.f10128Y);
        parcel.writeInt(this.f10129Z);
        parcel.writeString(this.f10131b0);
        parcel.writeByte(this.f10130a0 ? (byte) 1 : 0);
    }

    /* renamed from: a */
    public void mo26355a(int i) {
        this.f10127X = i;
    }

    /* renamed from: b */
    public String mo26358b() {
        return this.f10131b0;
    }

    /* renamed from: c */
    public void mo26361c(int i) {
        this.f10128Y = i;
    }

    /* renamed from: d */
    public void mo26363d(int i) {
        this.f10129Z = i;
    }

    /* renamed from: e */
    public int mo26365e() {
        return this.f10128Y;
    }

    /* renamed from: f */
    public int mo26367f() {
        return this.f10129Z;
    }

    /* renamed from: g */
    public int mo26369g() {
        return this.f10126W;
    }

    /* renamed from: h */
    public void mo26372h(int i) {
        this.f10125V = i;
    }

    /* renamed from: i */
    public void mo26374i(int i) {
        this.f10121R = i;
    }

    /* renamed from: j */
    public void mo26376j(int i) {
        this.f10122S = i;
    }

    public AlarmInfoItem(long j, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, String str, boolean z) {
        this.f10119P = j;
        this.f10120Q = i;
        this.f10121R = i2;
        this.f10122S = i3;
        this.f10123T = i4;
        this.f10124U = i5;
        this.f10125V = i6;
        this.f10126W = i7;
        this.f10127X = i8;
        this.f10128Y = i9;
        this.f10129Z = i10;
        this.f10131b0 = str;
        this.f10130a0 = z;
    }

    /* renamed from: a */
    public long mo26354a() {
        return this.f10119P;
    }

    /* renamed from: a */
    public void mo26356a(String str) {
        this.f10131b0 = str;
    }

    public AlarmInfoItem(Parcel parcel) {
        boolean z = false;
        this.f10119P = parcel.readLong();
        this.f10120Q = parcel.readInt();
        this.f10121R = parcel.readInt();
        this.f10122S = parcel.readInt();
        this.f10123T = parcel.readInt();
        this.f10124U = parcel.readInt();
        this.f10125V = parcel.readInt();
        this.f10126W = parcel.readInt();
        this.f10127X = parcel.readInt();
        this.f10128Y = parcel.readInt();
        this.f10129Z = parcel.readInt();
        this.f10131b0 = parcel.readString();
        this.f10130a0 = parcel.readByte() != 0 ? true : z;
    }
}
