package com.sxr.sdk.ble.keepfit.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

/* renamed from: com.sxr.sdk.ble.keepfit.service.q */
/* compiled from: BluetoothLeService */
class C4787q implements BluetoothAdapter.LeScanCallback {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10350a;

    C4787q(BluetoothLeService bluetoothLeService) {
        this.f10350a = bluetoothLeService;
    }

    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        this.f10350a.m16145a(bluetoothDevice, i, bArr);
    }
}
