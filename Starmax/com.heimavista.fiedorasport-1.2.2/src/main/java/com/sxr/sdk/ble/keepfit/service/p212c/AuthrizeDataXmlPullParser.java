package com.sxr.sdk.ble.keepfit.service.p212c;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Xml;
import java.io.InputStream;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.a */
public class AuthrizeDataXmlPullParser {

    /* renamed from: a */
    private Context f10320a;

    public AuthrizeDataXmlPullParser(Context context) {
        this.f10320a = context;
    }

    /* renamed from: a */
    public HashMap<String, String> mo26553a(String str) {
        AssetManager assets = this.f10320a.getAssets();
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            InputStream open = assets.open(str);
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(open, "utf-8");
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                if (eventType == 0) {
                    hashMap = new HashMap<>();
                } else if (eventType == 2) {
                    if (newPullParser.getName().equals("vid")) {
                        hashMap.put("vid", newPullParser.nextText());
                        newPullParser.next();
                    } else if (newPullParser.getName().equals("appid")) {
                        hashMap.put("appid", newPullParser.nextText());
                        newPullParser.next();
                    } else if (newPullParser.getName().equals("secret")) {
                        hashMap.put("secret", newPullParser.nextText());
                        newPullParser.next();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }
}
