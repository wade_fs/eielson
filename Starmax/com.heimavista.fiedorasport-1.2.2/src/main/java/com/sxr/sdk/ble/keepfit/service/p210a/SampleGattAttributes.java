package com.sxr.sdk.ble.keepfit.service.p210a;

import java.util.HashMap;

/* renamed from: com.sxr.sdk.ble.keepfit.service.a.b */
public class SampleGattAttributes {

    /* renamed from: a */
    public static HashMap<String, String> f10259a = new HashMap<>();

    /* renamed from: b */
    public static String f10260b = "00002a37-0000-1000-8000-00805f9b34fb";

    /* renamed from: c */
    public static String f10261c = "00002902-0000-1000-8000-00805f9b34fb";

    /* renamed from: d */
    public static String f10262d = "000033f4-0000-1000-8000-00805f9b34fb";

    /* renamed from: e */
    public static String f10263e = "000033f3-0000-1000-8000-00805f9b34fb";

    static {
        f10259a.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
        f10259a.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        f10259a.put(f10260b, "Heart Rate Measurement");
        f10259a.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
        f10259a.put("000056ff-0000-1000-8000-00805f9b34fb", "HJT ISSC");
        f10259a.put(f10262d, "Receiver");
        f10259a.put(f10263e, "Transport");
    }
}
