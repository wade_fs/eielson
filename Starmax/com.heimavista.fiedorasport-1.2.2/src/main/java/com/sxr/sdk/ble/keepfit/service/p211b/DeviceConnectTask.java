package com.sxr.sdk.ble.keepfit.service.p211b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.Method;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.d */
public class DeviceConnectTask extends AsyncTask<Void, BluetoothGatt, Boolean> {

    /* renamed from: a */
    public Context f10293a;

    /* renamed from: b */
    private final BluetoothSocket f10294b;

    /* renamed from: c */
    private final BluetoothDevice f10295c;

    /* renamed from: d */
    private final C4767c f10296d = new C4767c(this, this.f10293a);

    public DeviceConnectTask(Context context, BluetoothManager bVar) {
        BluetoothSocket bluetoothSocket;
        Log.d("DeviceGattTask", "init");
        this.f10293a = context;
        BluetoothAdapter.getDefaultAdapter();
        this.f10295c = bVar.mo26508c();
        this.f10296d.mo26524a(bVar);
        try {
            bluetoothSocket = this.f10295c.createRfcommSocketToServiceRecord(Statics.f10308b);
        } catch (IOException unused) {
            bluetoothSocket = null;
        }
        this.f10294b = bluetoothSocket;
    }

    /* renamed from: b */
    private boolean m16309b(BluetoothGatt bluetoothGatt) {
        try {
            Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
            if (method != null) {
                return ((Boolean) method.invoke(bluetoothGatt, new Object[0])).booleanValue();
            }
        } catch (Exception unused) {
            Log.e("DeviceGattTask", "An exception occured while refreshing device");
        }
        return false;
    }

    /* renamed from: a */
    public void mo26532a() {
        try {
            this.f10294b.close();
        } catch (IOException unused) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        BluetoothGatt connectGatt = this.f10295c.connectGatt(this.f10293a, false, this.f10296d);
        m16309b(connectGatt);
        connectGatt.connect();
        Log.i("DeviceGattTask", "gatt connect");
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        mo26532a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(BluetoothGatt... bluetoothGattArr) {
        super.onProgressUpdate(bluetoothGattArr);
        BluetoothGattSingleton.m16280a(bluetoothGattArr[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onCancelled(Boolean bool) {
        super.onCancelled(bool);
        mo26532a();
    }

    /* renamed from: a */
    public void mo26533a(BluetoothGatt bluetoothGatt) {
        publishProgress(bluetoothGatt);
    }
}
