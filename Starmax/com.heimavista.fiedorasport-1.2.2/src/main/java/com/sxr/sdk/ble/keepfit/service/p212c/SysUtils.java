package com.sxr.sdk.ble.keepfit.service.p212c;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.heimavista.thirdpart.BuildConfig;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.k */
public class SysUtils {

    /* renamed from: a */
    private static int[] f10333a = {1, 2, 4, 8, 16, 32, 64, 128};

    /* renamed from: a */
    public static Object m16357a() {
        return BuildConfig.VERSION_NAME;
    }

    /* renamed from: a */
    public static void m16363a(String str, byte[] bArr) {
        try {
            File file = new File(str);
            File parentFile = file.getParentFile();
            if (!parentFile.exists() || !parentFile.isDirectory()) {
                parentFile.mkdirs();
            }
            if (file.isFile() && file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            fileOutputStream.write(bArr);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: b */
    public static String m16366b() {
        try {
            return URLEncoder.encode(String.format("%1$s %2$s", Build.MANUFACTURER, Build.MODEL), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: b */
    public static String m16367b(Context context) {
        try {
            Object[] objArr = new Object[2];
            objArr[0] = "Android";
            objArr[1] = Build.VERSION.RELEASE;
            return URLEncoder.encode(String.format("%1$s:%2$s", objArr), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* renamed from: a */
    public static String m16358a(Context context) {
        return "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.DISPLAY.length() % 10) + (Build.HOST.length() % 10) + (Build.ID.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10) + (Build.TAGS.length() % 10) + (Build.TYPE.length() % 10) + (Build.USER.length() % 10);
    }

    /* renamed from: a */
    public static void m16362a(String str, String str2, String str3) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        String str4 = externalStorageDirectory.getPath() + str2;
        Calendar instance = Calendar.getInstance();
        String str5 = instance.get(1) + "-" + (instance.get(2) + 1) + "-" + instance.get(5) + "-" + str3;
        m16356a(str4, str5);
        String str6 = str4 + str5;
        String str7 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + ":" + str + "\r\n";
        try {
            File file = new File(str6);
            if (!file.exists()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Create the file:");
                sb.append(str6);
                Log.d("TestFile", sb.toString());
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rwd");
            randomAccessFile.seek(file.length());
            randomAccessFile.write(str7.getBytes());
            randomAccessFile.close();
        } catch (Exception e) {
            Log.e("TestFile", "Error on write File:" + e);
        }
    }

    /* renamed from: a */
    public static File m16356a(String str, String str2) {
        File file;
        m16361a(str);
        try {
            file = new File(str + str2);
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return file;
            }
        } catch (Exception e2) {
            e = e2;
            file = null;
            e.printStackTrace();
            return file;
        }
        return file;
    }

    /* renamed from: a */
    public static void m16361a(String str) {
        try {
            File file = new File(str);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {
            Log.i("error:", e + "");
        }
    }

    /* renamed from: a */
    public static String m16360a(byte[] bArr) {
        String str = "";
        if (bArr == null) {
            return str;
        }
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() == 1) {
                hexString = '0' + hexString;
            }
            str = str + hexString.toUpperCase() + " ";
        }
        return str;
    }

    /* renamed from: a */
    public static boolean[] m16364a(int i) {
        boolean[] zArr = new boolean[8];
        int i2 = 0;
        while (true) {
            int[] iArr = f10333a;
            if (i2 >= iArr.length) {
                return zArr;
            }
            zArr[i2] = (iArr[i2] & i) != 0;
            i2++;
        }
    }

    /* renamed from: a */
    public static boolean[] m16365a(boolean[]... zArr) {
        int i = 0;
        for (boolean[] zArr2 : zArr) {
            i += zArr2.length;
        }
        boolean[] zArr3 = new boolean[i];
        int i2 = 0;
        for (boolean[] zArr4 : zArr) {
            System.arraycopy(zArr4, 0, zArr3, i2, zArr4.length);
            i2 += zArr4.length;
        }
        return zArr3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0079 A[SYNTHETIC, Splitter:B:34:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0084 A[SYNTHETIC, Splitter:B:40:0x0084] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m16359a(java.io.File r7) {
        /*
            java.lang.String r0 = ""
            if (r7 == 0) goto L_0x008d
            boolean r1 = r7.isFile()
            if (r1 == 0) goto L_0x008d
            boolean r1 = r7.exists()
            if (r1 != 0) goto L_0x0012
            goto L_0x008d
        L_0x0012:
            r1 = 0
            r2 = 8192(0x2000, float:1.14794E-41)
            byte[] r2 = new byte[r2]
            java.lang.String r3 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r3)     // Catch:{ Exception -> 0x0073 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0073 }
            r4.<init>(r7)     // Catch:{ Exception -> 0x0073 }
        L_0x0022:
            int r7 = r4.read(r2)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r1 = -1
            r5 = 0
            if (r7 == r1) goto L_0x002e
            r3.update(r2, r5, r7)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            goto L_0x0022
        L_0x002e:
            byte[] r7 = r3.digest()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            int r1 = r7.length     // Catch:{ Exception -> 0x006e, all -> 0x006b }
        L_0x0033:
            if (r5 >= r1) goto L_0x0067
            byte r2 = r7[r5]     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r2 = r2 & 255(0xff, float:3.57E-43)
            java.lang.String r2 = java.lang.Integer.toHexString(r2)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            int r3 = r2.length()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r6 = 1
            if (r3 != r6) goto L_0x0055
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r3.<init>()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            java.lang.String r6 = "0"
            r3.append(r6)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r3.append(r2)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
        L_0x0055:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r3.<init>()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r3.append(r0)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            r3.append(r2)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            int r5 = r5 + 1
            goto L_0x0033
        L_0x0067:
            r4.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x006b:
            r7 = move-exception
            r1 = r4
            goto L_0x0082
        L_0x006e:
            r7 = move-exception
            r1 = r4
            goto L_0x0074
        L_0x0071:
            r7 = move-exception
            goto L_0x0082
        L_0x0073:
            r7 = move-exception
        L_0x0074:
            r7.printStackTrace()     // Catch:{ all -> 0x0071 }
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0081:
            return r0
        L_0x0082:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ IOException -> 0x0088 }
            goto L_0x008c
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()
        L_0x008c:
            throw r7
        L_0x008d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sxr.sdk.ble.keepfit.service.p212c.SysUtils.m16359a(java.io.File):java.lang.String");
    }
}
