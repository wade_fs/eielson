package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.sxr.sdk.ble.keepfit.aidl.b */
public interface IServiceCallback extends IInterface {
    /* renamed from: A */
    void mo22903A(int i);

    /* renamed from: B */
    void mo22904B(int i);

    /* renamed from: D */
    void mo22905D(int i);

    /* renamed from: E */
    void mo22906E(int i);

    /* renamed from: F */
    void mo22907F(int i);

    /* renamed from: H */
    void mo22908H(int i);

    /* renamed from: a */
    void mo22909a(int i);

    /* renamed from: a */
    void mo22910a(int i, int i2);

    /* renamed from: a */
    void mo22911a(int i, int i2, long j);

    /* renamed from: a */
    void mo22912a(int i, long j);

    /* renamed from: a */
    void mo22913a(int i, long j, int i2, int i3);

    /* renamed from: a */
    void mo22914a(int i, long j, int i2, int i3, int i4, int i5, int i6, int i7);

    /* renamed from: a */
    void mo22915a(int i, String str);

    /* renamed from: a */
    void mo22916a(int i, String str, int i2, int i3);

    /* renamed from: a */
    void mo22917a(int i, String str, String str2, String str3, int i2);

    /* renamed from: a */
    void mo22918a(int i, int[] iArr);

    /* renamed from: a */
    void mo22919a(int i, boolean[] zArr);

    /* renamed from: a */
    void mo22920a(long j, int i);

    /* renamed from: a */
    void mo22921a(String str, String str2, int i);

    /* renamed from: a */
    void mo22922a(String str, byte[] bArr);

    /* renamed from: a */
    void mo22923a(String str, byte[] bArr, int i);

    /* renamed from: a */
    void mo22924a(byte[] bArr);

    /* renamed from: b */
    void mo22925b(int i);

    /* renamed from: b */
    void mo22926b(int i, int i2);

    /* renamed from: b */
    void mo22927b(int i, int i2, int i3, int i4, int i5);

    /* renamed from: b */
    void mo22928b(int i, long j, int i2, int i3);

    /* renamed from: b */
    void mo22929b(int i, String str, int i2, int i3);

    /* renamed from: b */
    void mo22930b(int i, int[] iArr);

    /* renamed from: b */
    void mo22931b(boolean z, String str, String str2);

    /* renamed from: c */
    void mo22932c(int i);

    /* renamed from: c */
    void mo22933c(int i, int i2);

    /* renamed from: d */
    void mo22934d(int i);

    /* renamed from: d */
    void mo22935d(int i, int i2);

    /* renamed from: e */
    void mo22936e(int i);

    /* renamed from: f */
    void mo22937f(int i);

    /* renamed from: h */
    void mo22938h(int i);

    /* renamed from: j */
    void mo22939j(int i);

    /* renamed from: k */
    void mo22940k(int i);

    /* renamed from: m */
    void mo22941m(int i);

    /* renamed from: o */
    void mo22942o(int i);

    /* renamed from: p */
    void mo22943p(int i);

    /* renamed from: q */
    void mo22944q(int i);

    /* renamed from: r */
    void mo22945r(int i);

    /* renamed from: s */
    void mo22946s(int i);

    /* renamed from: t */
    void mo22947t(int i);

    /* renamed from: u */
    void mo22948u(int i);

    /* renamed from: v */
    void mo22949v(int i);

    /* renamed from: x */
    void mo22950x(int i);

    /* renamed from: y */
    void mo22951y(int i);

    /* renamed from: z */
    void mo22952z(int i);

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.b$a */
    /* compiled from: IServiceCallback */
    public static abstract class C4762a extends Binder implements IServiceCallback {
        public C4762a() {
            attachInterface(this, "com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
        }

        /* renamed from: a */
        public static IServiceCallback m16039a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IServiceCallback)) {
                return new C4763a(iBinder);
            }
            return (IServiceCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int i3 = i;
            if (i3 != 1598968902) {
                switch (i3) {
                    case 1:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22907F(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 2:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22921a(parcel.readString(), parcel.readString(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 3:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22952z(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 4:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22948u(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 5:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22915a(parcel.readInt(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 6:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22947t(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 7:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22904B(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 8:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22914a(parcel.readInt(), parcel.readLong(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 9:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22940k(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 10:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22945r(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 11:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22937f(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 12:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22908H(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 13:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22910a(parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 14:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22917a(parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 15:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22934d(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 16:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22944q(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 17:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22906E(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 18:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22928b(parcel.readInt(), parcel.readLong(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 19:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22938h(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 20:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22932c(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 21:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22939j(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 22:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22913a(parcel.readInt(), parcel.readLong(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 23:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22912a(parcel.readInt(), parcel.readLong());
                        parcel2.writeNoException();
                        return true;
                    case 24:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22909a(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 25:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22919a(parcel.readInt(), parcel.createBooleanArray());
                        parcel2.writeNoException();
                        return true;
                    case 26:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22905D(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 27:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22951y(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 28:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22946s(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 29:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22925b(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 30:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22927b(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 31:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22916a(parcel.readInt(), parcel.readString(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 32:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22949v(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 33:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22941m(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 34:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22935d(parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 35:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22929b(parcel.readInt(), parcel.readString(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 36:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22931b(parcel.readInt() != 0, parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 37:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22933c(parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 38:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22936e(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 39:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22924a(parcel.createByteArray());
                        parcel2.writeNoException();
                        return true;
                    case 40:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22922a(parcel.readString(), parcel.createByteArray());
                        parcel2.writeNoException();
                        return true;
                    case 41:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22923a(parcel.readString(), parcel.createByteArray(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 42:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22926b(parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 43:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22918a(parcel.readInt(), parcel.createIntArray());
                        parcel2.writeNoException();
                        return true;
                    case 44:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22920a(parcel.readLong(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 45:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22911a(parcel.readInt(), parcel.readInt(), parcel.readLong());
                        parcel2.writeNoException();
                        return true;
                    case 46:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22930b(parcel.readInt(), parcel.createIntArray());
                        parcel2.writeNoException();
                        return true;
                    case 47:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22942o(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 48:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22950x(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 49:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22943p(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 50:
                        parcel.enforceInterface("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                        mo22903A(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                return true;
            }
        }

        /* renamed from: com.sxr.sdk.ble.keepfit.aidl.b$a$a */
        /* compiled from: IServiceCallback */
        private static class C4763a implements IServiceCallback {

            /* renamed from: a */
            private IBinder f10168a;

            C4763a(IBinder iBinder) {
                this.f10168a = iBinder;
            }

            /* renamed from: A */
            public void mo22903A(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(50, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: B */
            public void mo22904B(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: D */
            public void mo22905D(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: E */
            public void mo22906E(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: F */
            public void mo22907F(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: H */
            public void mo22908H(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22921a(String str, String str2, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.f10168a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f10168a;
            }

            /* renamed from: b */
            public void mo22928b(int i, long j, int i2, int i3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeLong(j);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.f10168a.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: c */
            public void mo22932c(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: d */
            public void mo22934d(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: e */
            public void mo22936e(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(38, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: f */
            public void mo22937f(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: h */
            public void mo22938h(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: j */
            public void mo22939j(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: k */
            public void mo22940k(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: m */
            public void mo22941m(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: o */
            public void mo22942o(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(47, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: p */
            public void mo22943p(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(49, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: q */
            public void mo22944q(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: r */
            public void mo22945r(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: s */
            public void mo22946s(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: t */
            public void mo22947t(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: u */
            public void mo22948u(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: v */
            public void mo22949v(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: x */
            public void mo22950x(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(48, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: z */
            public void mo22952z(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: c */
            public void mo22933c(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f10168a.transact(37, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: d */
            public void mo22935d(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f10168a.transact(34, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22914a(int i, long j, int i2, int i3, int i4, int i5, int i6, int i7) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeLong(j);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    obtain.writeInt(i5);
                    obtain.writeInt(i6);
                    obtain.writeInt(i7);
                    this.f10168a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22925b(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(29, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22927b(int i, int i2, int i3, int i4, int i5) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    obtain.writeInt(i5);
                    this.f10168a.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22910a(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f10168a.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22929b(int i, String str, int i2, int i3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.f10168a.transact(35, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22917a(int i, String str, String str2, String str3, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeInt(i2);
                    this.f10168a.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22931b(boolean z, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f10168a.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22913a(int i, long j, int i2, int i3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeLong(j);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.f10168a.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22926b(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f10168a.transact(42, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22912a(int i, long j) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeLong(j);
                    this.f10168a.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: b */
            public void mo22930b(int i, int[] iArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeIntArray(iArr);
                    this.f10168a.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22909a(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    this.f10168a.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22919a(int i, boolean[] zArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeBooleanArray(zArr);
                    this.f10168a.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22916a(int i, String str, int i2, int i3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.f10168a.transact(31, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22924a(byte[] bArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeByteArray(bArr);
                    this.f10168a.transact(39, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22922a(String str, byte[] bArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    this.f10168a.transact(40, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22923a(String str, byte[] bArr, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeInt(i);
                    this.f10168a.transact(41, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22918a(int i, int[] iArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeIntArray(iArr);
                    this.f10168a.transact(43, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22920a(long j, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeLong(j);
                    obtain.writeInt(i);
                    this.f10168a.transact(44, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo22911a(int i, int i2, long j) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.sxr.sdk.ble.keepfit.aidl.IServiceCallback");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeLong(j);
                    this.f10168a.transact(45, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
