package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.s */
class Ota implements C4771h.C4773b {

    /* renamed from: a */
    final /* synthetic */ String f10364a;

    /* renamed from: b */
    final /* synthetic */ String f10365b;

    /* renamed from: c */
    final /* synthetic */ int f10366c;

    /* renamed from: d */
    final /* synthetic */ C4790v f10367d;

    Ota(C4790v vVar, String str, String str2, int i) {
        this.f10367d = vVar;
        this.f10364a = str;
        this.f10365b = str2;
        this.f10366c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.v.a(com.sxr.sdk.ble.keepfit.service.v, org.json.JSONObject, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.v, org.json.JSONObject, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.v.a(org.json.JSONObject, java.lang.String, int):void
      com.sxr.sdk.ble.keepfit.service.v.a(java.lang.String, java.lang.String, int):void
      com.sxr.sdk.ble.keepfit.service.v.a(com.sxr.sdk.ble.keepfit.service.v, org.json.JSONObject, boolean):void */
    /* renamed from: a */
    public void mo26498a(int i, String str) {
        try {
            String a = this.f10367d.f10380a;
            StringBuilder sb = new StringBuilder();
            sb.append("compareOta ");
            sb.append(i);
            sb.append(" ");
            sb.append(str);
            CrashApp.m16337d(a, sb.toString());
            if (i == 200) {
                JSONObject jSONObject = new JSONObject(str);
                ShareUtil.m16355b(this.f10364a, jSONObject.toString());
                this.f10367d.m16424a(jSONObject, true);
                this.f10367d.m16423a(jSONObject, this.f10365b, this.f10366c);
                return;
            }
            this.f10367d.f10381b.f10228j0.mo22931b(false, new JSONObject().toString(), this.f10367d.f10385f);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }
}
