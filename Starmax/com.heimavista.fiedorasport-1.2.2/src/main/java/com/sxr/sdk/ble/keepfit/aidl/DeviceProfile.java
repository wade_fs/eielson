package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class DeviceProfile implements Parcelable {
    public static final Parcelable.Creator<DeviceProfile> CREATOR = new C4757a();

    /* renamed from: P */
    private boolean f10143P = false;

    /* renamed from: Q */
    private boolean f10144Q = false;

    /* renamed from: R */
    private boolean f10145R = false;

    /* renamed from: S */
    private int f10146S = 0;

    /* renamed from: T */
    private int f10147T = 0;

    /* renamed from: U */
    private int f10148U = 0;

    /* renamed from: V */
    private int f10149V = 0;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.DeviceProfile$a */
    static class C4757a implements Parcelable.Creator<DeviceProfile> {
        C4757a() {
        }

        public DeviceProfile createFromParcel(Parcel parcel) {
            return new DeviceProfile(parcel);
        }

        public DeviceProfile[] newArray(int i) {
            return new DeviceProfile[i];
        }
    }

    public DeviceProfile(boolean z, boolean z2, boolean z3, int i, int i2, int i3, int i4) {
        this.f10143P = z;
        this.f10144Q = z2;
        this.f10145R = z3;
        this.f10146S = i;
        this.f10147T = i2;
        this.f10148U = i3;
        this.f10149V = i4;
    }

    /* renamed from: a */
    public void mo26397a(boolean z) {
        this.f10143P = z;
    }

    /* renamed from: b */
    public void mo26400b(boolean z) {
        this.f10145R = z;
    }

    /* renamed from: c */
    public void mo26403c(boolean z) {
        this.f10144Q = z;
    }

    /* renamed from: d */
    public int mo26404d() {
        return this.f10148U;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public boolean mo26407e() {
        return this.f10143P;
    }

    /* renamed from: f */
    public boolean mo26408f() {
        return this.f10145R;
    }

    /* renamed from: g */
    public boolean mo26409g() {
        return this.f10144Q;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.f10143P ? (byte) 1 : 0);
        parcel.writeByte(this.f10144Q ? (byte) 1 : 0);
        parcel.writeByte(this.f10145R ? (byte) 1 : 0);
        parcel.writeInt(this.f10146S);
        parcel.writeInt(this.f10148U);
        parcel.writeInt(this.f10147T);
        parcel.writeInt(this.f10149V);
    }

    /* renamed from: a */
    public int mo26395a() {
        return this.f10147T;
    }

    /* renamed from: b */
    public int mo26398b() {
        return this.f10149V;
    }

    /* renamed from: c */
    public int mo26401c() {
        return this.f10146S;
    }

    /* renamed from: d */
    public void mo26405d(int i) {
        this.f10148U = i;
    }

    /* renamed from: a */
    public void mo26396a(int i) {
        this.f10147T = i;
    }

    /* renamed from: b */
    public void mo26399b(int i) {
        this.f10149V = i;
    }

    /* renamed from: c */
    public void mo26402c(int i) {
        this.f10146S = i;
    }

    public DeviceProfile() {
    }

    public DeviceProfile(Parcel parcel) {
        boolean z = false;
        this.f10143P = parcel.readByte() != 0;
        this.f10144Q = parcel.readByte() != 0;
        this.f10145R = parcel.readByte() != 0 ? true : z;
        this.f10146S = parcel.readInt();
        this.f10148U = parcel.readInt();
        this.f10147T = parcel.readInt();
        this.f10149V = parcel.readInt();
    }
}
