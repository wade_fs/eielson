package com.sxr.sdk.ble.keepfit.service.p211b;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import java.math.BigInteger;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.c */
/* compiled from: Callback */
public class C4767c extends BluetoothGattCallback {

    /* renamed from: c */
    public static String f10290c = "blueCallback";

    /* renamed from: a */
    private BluetoothManager f10291a;

    /* renamed from: b */
    DeviceConnectTask f10292b;

    public C4767c(DeviceConnectTask dVar, Context context) {
        this.f10292b = dVar;
    }

    /* renamed from: a */
    public void mo26524a(BluetoothManager bVar) {
        this.f10291a = bVar;
    }

    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        int i;
        int i2;
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        int intValue = new BigInteger(bluetoothGattCharacteristic.getValue()).intValue();
        String format = String.format("%#10x", Integer.valueOf(intValue));
        Log.d("changed", format);
        int i3 = -1;
        if (format.trim().equals("0x10")) {
            i2 = 3;
        } else if (format.trim().equals("0x2")) {
            i2 = this.f10291a.f10285t == 1 ? 5 : 8;
        } else if (format.trim().equals("0x3") || format.trim().equals("0x1")) {
            i = intValue;
            i2 = -1;
            if (i2 < 0 || i3 >= 0 || i >= 0) {
                Intent intent = new Intent();
                intent.setAction("BluetoothGattUpdate");
                intent.putExtra("step", i2);
                intent.putExtra("error", i3);
                intent.putExtra("memDevValue", i);
                this.f10292b.f10293a.sendBroadcast(intent);
                this.f10291a.mo26504a(intent);
            }
            return;
        } else {
            i3 = Integer.parseInt(format.trim().replace("0x", ""));
            i2 = -1;
        }
        i = -1;
        if (i2 < 0) {
        }
        Intent intent2 = new Intent();
        intent2.setAction("BluetoothGattUpdate");
        intent2.putExtra("step", i2);
        intent2.putExtra("error", i3);
        intent2.putExtra("memDevValue", i);
        this.f10292b.f10293a.sendBroadcast(intent2);
        this.f10291a.mo26504a(intent2);
    }

    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        int i2;
        int i3 = -1;
        boolean z = true;
        if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10315i)) {
            i2 = 0;
        } else if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10316j)) {
            i2 = 1;
        } else if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10317k)) {
            i2 = 2;
        } else if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10318l)) {
            i2 = 3;
        } else if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10310d)) {
            i2 = -1;
            i3 = 5;
        } else {
            i2 = -1;
            z = false;
        }
        if (z) {
            String str = f10290c;
            Log.d(str, "onCharacteristicRead: " + i2);
            Intent intent = new Intent();
            intent.setAction("BluetoothGattUpdate");
            if (i2 >= 0) {
                intent.putExtra("characteristic", i2);
                intent.putExtra("value", new String(bluetoothGattCharacteristic.getValue()));
            } else {
                intent.putExtra("step", i3);
                intent.putExtra("value", bluetoothGattCharacteristic.getIntValue(20, 0));
            }
            this.f10292b.f10293a.sendBroadcast(intent);
            this.f10291a.mo26504a(intent);
        }
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
    }

    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        String str = f10290c;
        Log.d(str, "onCharacteristicWrite: " + bluetoothGattCharacteristic.getUuid().toString());
        if (i == 0) {
            Log.i(f10290c, "write succeeded");
            int i2 = -1;
            if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10309c)) {
                i2 = 4;
            } else if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10311e)) {
                i2 = this.f10291a.f10285t == 1 ? 5 : 7;
            } else if (!bluetoothGattCharacteristic.getUuid().equals(Statics.f10308b) && bluetoothGattCharacteristic.getUuid().equals(Statics.f10312f) && this.f10291a.f10288w != -1) {
                String str2 = f10290c;
                Log.d(str2, "Next block in chunk " + this.f10291a.f10288w);
                this.f10291a.mo26518h();
            }
            if (i2 > 0) {
                Intent intent = new Intent();
                intent.setAction("BluetoothGattUpdate");
                intent.putExtra("step", i2);
                this.f10292b.f10293a.sendBroadcast(intent);
                this.f10291a.mo26504a(intent);
            }
        } else {
            String str3 = f10290c;
            Log.e(str3, "write failed: " + i);
        }
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
    }

    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        String str = f10290c;
        Log.i(str, "le onConnectionStateChange [" + i2 + "]");
        if (i2 == 2) {
            Log.i(f10290c, "le device connected");
            bluetoothGatt.discoverServices();
        } else if (i2 == 0) {
            Log.i(f10290c, "le device disconnected");
        }
        Intent intent = new Intent();
        intent.setAction("ConnectionState");
        intent.putExtra(ServerProtocol.DIALOG_PARAM_STATE, i2);
        this.f10292b.f10293a.sendBroadcast(intent);
    }

    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        Log.d(f10290c, "onDescriptorWrite");
        if (bluetoothGattDescriptor.getCharacteristic().getUuid().equals(Statics.f10313g)) {
            Intent intent = new Intent();
            intent.setAction("BluetoothGattUpdate");
            intent.putExtra("step", 2);
            this.f10292b.f10293a.sendBroadcast(intent);
            this.f10291a.mo26504a(intent);
        }
        this.f10292b.mo26533a(bluetoothGatt);
    }

    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        Log.i(f10290c, "onServicesDiscovered");
        BluetoothGattSingleton.m16280a(bluetoothGatt);
        Intent intent = new Intent();
        intent.setAction("BluetoothGattUpdate");
        intent.putExtra("step", 0);
        this.f10292b.f10293a.sendBroadcast(intent);
        this.f10291a.mo26504a(intent);
    }
}
