package com.sxr.sdk.ble.keepfit.service.p212c;

import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.g */
/* compiled from: HttpUtil */
class C4770g implements Runnable {

    /* renamed from: P */
    final /* synthetic */ String f10329P;

    /* renamed from: Q */
    final /* synthetic */ JSONObject f10330Q;

    /* renamed from: R */
    final /* synthetic */ C4771h.C4773b f10331R;

    C4770g(String str, JSONObject jSONObject, C4771h.C4773b bVar) {
        this.f10329P = str;
        this.f10330Q = jSONObject;
        this.f10331R = bVar;
    }

    public void run() {
        HttpPost httpPost = new HttpPost(this.f10329P);
        try {
            httpPost.setEntity(new StringEntity(this.f10330Q.toString()));
            HttpResponse execute = new DefaultHttpClient().execute(httpPost);
            this.f10331R.mo26498a(execute.getStatusLine().getStatusCode(), EntityUtils.toString(execute.getEntity()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }
}
