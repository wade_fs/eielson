package com.sxr.sdk.ble.keepfit.service.p211b;

import android.bluetooth.BluetoothGatt;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.a */
public class BluetoothGattSingleton {

    /* renamed from: a */
    private static BluetoothGatt f10265a;

    /* renamed from: a */
    public static BluetoothGatt m16279a() {
        return f10265a;
    }

    /* renamed from: a */
    public static void m16280a(BluetoothGatt bluetoothGatt) {
        f10265a = bluetoothGatt;
    }
}
