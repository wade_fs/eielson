package com.sxr.sdk.ble.keepfit.service;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.SysUtils;

/* renamed from: com.sxr.sdk.ble.keepfit.service.d */
/* compiled from: BluetoothLeService */
class C4774d implements Runnable {

    /* renamed from: P */
    final /* synthetic */ BluetoothLeService f10334P;

    C4774d(BluetoothLeService bluetoothLeService) {
        this.f10334P = bluetoothLeService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    public void run() {
        long unused = this.f10334P.f10225h1 = System.currentTimeMillis();
        while (this.f10334P.f10221f1) {
            try {
                Thread.sleep(100);
                if (this.f10334P.f10195S0) {
                    boolean S = this.f10334P.m16266w();
                    boolean l = this.f10334P.m16268x();
                    int size = this.f10334P.f10191Q0.size();
                    long currentTimeMillis = System.currentTimeMillis();
                    if (!S && this.f10334P.f10216d0 == 1) {
                        this.f10334P.m16204e(false);
                    } else if (this.f10334P.f10189P0) {
                        if (!l) {
                            if (currentTimeMillis - this.f10334P.f10225h1 > DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) {
                                CrashApp.m16337d("BluetoothLeService", "process_cmd_runnable writeCharacteristic time out.");
                                this.f10334P.m16089A();
                            }
                        } else if (S) {
                            if (size > 0) {
                                byte[] bArr = (byte[]) this.f10334P.f10191Q0.remove(0);
                                String hexString = Integer.toHexString(bArr[0] & 255);
                                if (this.f10334P.f10187O0) {
                                    if (!hexString.equalsIgnoreCase("a3")) {
                                    }
                                }
                                boolean g = this.f10334P.m16205e(hexString);
                                StringBuilder sb = new StringBuilder();
                                sb.append("set cmd : ");
                                sb.append(SysUtils.m16360a(bArr));
                                sb.append(", waiting session result ");
                                sb.append(g);
                                CrashApp.m16336c("BluetoothLeService", sb.toString());
                                byte[] unused2 = this.f10334P.f10219e1 = bArr;
                                long unused3 = this.f10334P.f10225h1 = currentTimeMillis;
                                this.f10334P.m16187c(bArr);
                                if (this.f10334P.m16205e(hexString)) {
                                    boolean unused4 = this.f10334P.f10189P0 = false;
                                } else {
                                    this.f10334P.m16204e(false);
                                }
                            }
                        } else if (this.f10334P.f10228j0 != null) {
                            this.f10334P.f10228j0.mo22952z(0);
                        }
                    } else if (this.f10334P.f10187O0) {
                        if (currentTimeMillis - this.f10334P.f10225h1 > 10000) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("process_cmd_runnable time out check sync mode: ");
                            sb2.append(this.f10334P.f10187O0);
                            CrashApp.m16336c("BluetoothLeService", sb2.toString());
                            this.f10334P.m16089A();
                        }
                    } else if (currentTimeMillis - this.f10334P.f10225h1 > DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) {
                        CrashApp.m16336c("BluetoothLeService", "process_cmd_runnable : session time out.");
                        this.f10334P.m16089A();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.f10334P.m16231i(true);
                this.f10334P.m16204e(false);
                CrashApp.m16335b("BluetoothLeService", "process_cmd_runnable: exception");
            }
        }
    }
}
