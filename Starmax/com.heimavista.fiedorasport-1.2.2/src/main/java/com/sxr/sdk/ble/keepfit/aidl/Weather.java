package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class Weather implements Parcelable {
    public static final Parcelable.Creator<Weather> CREATOR = new C4759a();

    /* renamed from: P */
    private int f10157P = 0;

    /* renamed from: Q */
    private int f10158Q = 0;

    /* renamed from: R */
    private int f10159R = 0;

    /* renamed from: S */
    private int f10160S = 0;

    /* renamed from: T */
    private int f10161T = 0;

    /* renamed from: U */
    private int f10162U = 0;

    /* renamed from: V */
    private int f10163V = 0;

    /* renamed from: W */
    private int f10164W = 0;

    /* renamed from: X */
    private int f10165X = 0;

    /* renamed from: Y */
    private int f10166Y = 0;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.Weather$a */
    static class C4759a implements Parcelable.Creator<Weather> {
        C4759a() {
        }

        public Weather createFromParcel(Parcel parcel) {
            return new Weather(parcel);
        }

        public Weather[] newArray(int i) {
            return new Weather[i];
        }
    }

    public Weather(Parcel parcel) {
        this.f10157P = parcel.readInt();
        this.f10158Q = parcel.readInt();
        this.f10159R = parcel.readInt();
        this.f10160S = parcel.readInt();
        this.f10161T = parcel.readInt();
        this.f10162U = parcel.readInt();
        this.f10163V = parcel.readInt();
        this.f10164W = parcel.readInt();
        this.f10165X = parcel.readInt();
        this.f10166Y = parcel.readInt();
    }

    /* renamed from: a */
    public int mo26427a() {
        return this.f10165X;
    }

    /* renamed from: b */
    public int mo26428b() {
        return this.f10162U;
    }

    /* renamed from: c */
    public int mo26429c() {
        return this.f10166Y;
    }

    /* renamed from: d */
    public int mo26430d() {
        return this.f10158Q;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public int mo26432e() {
        return this.f10159R;
    }

    /* renamed from: f */
    public int mo26433f() {
        return this.f10161T;
    }

    /* renamed from: g */
    public int mo26434g() {
        return this.f10160S;
    }

    /* renamed from: h */
    public int mo26435h() {
        return this.f10163V;
    }

    /* renamed from: i */
    public int mo26436i() {
        return this.f10157P;
    }

    /* renamed from: j */
    public int mo26437j() {
        return this.f10164W;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f10157P);
        parcel.writeInt(this.f10158Q);
        parcel.writeInt(this.f10159R);
        parcel.writeInt(this.f10160S);
        parcel.writeInt(this.f10161T);
        parcel.writeInt(this.f10162U);
        parcel.writeInt(this.f10163V);
        parcel.writeInt(this.f10164W);
        parcel.writeInt(this.f10165X);
        parcel.writeInt(this.f10166Y);
    }
}
