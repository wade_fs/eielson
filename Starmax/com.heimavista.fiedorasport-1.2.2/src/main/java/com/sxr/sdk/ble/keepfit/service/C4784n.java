package com.sxr.sdk.ble.keepfit.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import androidx.core.app.NotificationCompat;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;

/* renamed from: com.sxr.sdk.ble.keepfit.service.n */
/* compiled from: BluetoothLeService */
class C4784n extends BroadcastReceiver {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10347a;

    C4784n(BluetoothLeService bluetoothLeService) {
        this.f10347a = bluetoothLeService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.i(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.j(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.j(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.j(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], java.lang.String[], boolean):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothDevice, int, byte[]):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Common.f10357g.equals(action)) {
            this.f10347a.m16093C();
        } else if (Common.f10358h.equals(action)) {
            this.f10347a.m16272z();
        } else if (Common.f10359i.equals(action)) {
            CrashApp.m16337d("BluetoothLeService", "services discovered");
            this.f10347a.m16204e(true);
            long unused = this.f10347a.f10225h1 = System.currentTimeMillis();
            BluetoothLeService bluetoothLeService = this.f10347a;
            if (bluetoothLeService.m16162a(bluetoothLeService.m16258s()) && !this.f10347a.f10195S0) {
                CrashApp.m16336c("BluetoothLeService", "services discovered find useful charactistic.");
                this.f10347a.m16091B();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean unused2 = this.f10347a.f10195S0 = true;
                int unused3 = this.f10347a.f10223g1 = 0;
                this.f10347a.m16260t();
            }
            this.f10347a.m16144a(2, false);
            BluetoothLeService bluetoothLeService2 = this.f10347a;
            bluetoothLeService2.m16156a(bluetoothLeService2.f10206Y, this.f10347a.f10204X);
        } else if (Common.f10360j.equals(action)) {
            CrashApp.m16337d("BluetoothLeService", "restart bluetooth adapter.");
        } else if (Common.f10361k.equals(action)) {
            this.f10347a.m16186c(intent.getStringExtra(Common.f10362l));
        } else if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(action)) {
            CrashApp.m16337d("BluetoothLeService", "ACTION_BLE_STATE_CHANGE " + this.f10347a.f10190Q.getState());
            switch (this.f10347a.f10190Q.getState()) {
                case 10:
                    this.f10347a.mo26497b();
                    this.f10347a.f10183K0.clear();
                    return;
                case 11:
                default:
                    return;
                case 12:
                    this.f10347a.m16194d(false);
                    return;
            }
        } else if (action.equals("ProgressUpdate")) {
            try {
                if (intent.hasExtra("error")) {
                    int intExtra = intent.getIntExtra("error", C4790v.f10378p);
                    this.f10347a.f10228j0.mo22933c(C4790v.f10375m, intExtra);
                    int unused4 = this.f10347a.f10199U0 = 0;
                    if (intExtra == C4790v.f10378p) {
                        this.f10347a.m16194d(false);
                        return;
                    }
                    return;
                }
                int intExtra2 = intent.getIntExtra(NotificationCompat.CATEGORY_PROGRESS, 0);
                if (intExtra2 != this.f10347a.f10199U0) {
                    this.f10347a.f10228j0.mo22933c(C4790v.f10374l, intExtra2);
                    int unused5 = this.f10347a.f10199U0 = intExtra2;
                }
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        } else if (action.equals("android.intent.action.SCREEN_ON")) {
            CrashApp.m16337d("BluetoothLeService", "ACTION_SCREEN_ON");
        } else if (action.equals("android.intent.action.SCREEN_OFF")) {
            CrashApp.m16337d("BluetoothLeService", "ACTION_SCREEN_OFF");
        }
    }
}
