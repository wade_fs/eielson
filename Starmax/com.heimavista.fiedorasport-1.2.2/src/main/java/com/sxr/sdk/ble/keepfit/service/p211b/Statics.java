package com.sxr.sdk.ble.keepfit.service.p211b;

import java.util.UUID;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.f */
public class Statics {

    /* renamed from: a */
    public static final UUID f10307a = UUID.fromString("0000fef5-0000-1000-8000-00805f9b34fb");

    /* renamed from: b */
    public static final UUID f10308b = UUID.fromString("8082caa8-41a6-4021-91c6-56f9b954cc34");

    /* renamed from: c */
    public static final UUID f10309c = UUID.fromString("724249f0-5eC3-4b5f-8804-42345af08651");

    /* renamed from: d */
    public static final UUID f10310d = UUID.fromString("6c53db25-47a1-45fe-a022-7c92fb334fd4");

    /* renamed from: e */
    public static final UUID f10311e = UUID.fromString("9d84b9a3-000c-49d8-9183-855b673fda31");

    /* renamed from: f */
    public static final UUID f10312f = UUID.fromString("457871e8-d516-4ca1-9116-57d0b17b9cb2");

    /* renamed from: g */
    public static final UUID f10313g = UUID.fromString("5f78df94-798c-46f5-990a-b3eb6a065c88");

    /* renamed from: h */
    public static final UUID f10314h = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    /* renamed from: i */
    public static final UUID f10315i = UUID.fromString("00002A29-0000-1000-8000-00805f9b34fb");

    /* renamed from: j */
    public static final UUID f10316j = UUID.fromString("00002A24-0000-1000-8000-00805f9b34fb");

    /* renamed from: k */
    public static final UUID f10317k = UUID.fromString("00002A26-0000-1000-8000-00805f9b34fb");

    /* renamed from: l */
    public static final UUID f10318l = UUID.fromString("00002A28-0000-1000-8000-00805f9b34fb");

    static {
        UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002A25-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002A27-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002A23-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002A2A-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002A50-0000-1000-8000-00805f9b34fb");
    }
}
