package com.sxr.sdk.ble.keepfit.service.p212c;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.j */
public class ShareUtil {

    /* renamed from: a */
    private static SharedPreferences f10332a;

    /* renamed from: a */
    public static void m16354a(Context context) {
        f10332a = context.getSharedPreferences(context.getPackageName(), 0);
    }

    /* renamed from: b */
    public static void m16355b(String str, Object obj) {
        SharedPreferences.Editor edit = f10332a.edit();
        if (obj instanceof String) {
            edit.putString(str, (String) obj);
        } else if (obj instanceof Integer) {
            edit.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof Boolean) {
            edit.putBoolean(str, ((Boolean) obj).booleanValue());
        } else if (obj instanceof Float) {
            edit.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof Long) {
            edit.putLong(str, ((Long) obj).longValue());
        } else {
            edit.putString(str, obj.toString());
        }
        edit.apply();
    }

    /* renamed from: a */
    public static Object m16353a(String str, Object obj) {
        if (obj instanceof String) {
            return f10332a.getString(str, (String) obj);
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(f10332a.getInt(str, ((Integer) obj).intValue()));
        }
        if (obj instanceof Boolean) {
            return Boolean.valueOf(f10332a.getBoolean(str, ((Boolean) obj).booleanValue()));
        }
        if (obj instanceof Float) {
            return Float.valueOf(f10332a.getFloat(str, ((Float) obj).floatValue()));
        }
        if (obj instanceof Long) {
            return Long.valueOf(f10332a.getLong(str, ((Long) obj).longValue()));
        }
        return null;
    }
}
