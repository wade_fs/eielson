package com.sxr.sdk.ble.keepfit.service.p212c;

import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.f */
/* compiled from: HttpUtil */
class C4769f implements Runnable {

    /* renamed from: P */
    final /* synthetic */ String f10327P;

    /* renamed from: Q */
    final /* synthetic */ C4771h.C4773b f10328Q;

    C4769f(String str, C4771h.C4773b bVar) {
        this.f10327P = str;
        this.f10328Q = bVar;
    }

    public void run() {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(this.f10327P));
            this.f10328Q.mo26498a(execute.getStatusLine().getStatusCode(), EntityUtils.toString(execute.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
