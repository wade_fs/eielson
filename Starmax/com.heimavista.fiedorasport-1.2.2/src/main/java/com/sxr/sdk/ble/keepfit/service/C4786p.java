package com.sxr.sdk.ble.keepfit.service;

import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;

/* renamed from: com.sxr.sdk.ble.keepfit.service.p */
/* compiled from: BluetoothLeService */
class C4786p implements Runnable {

    /* renamed from: P */
    final /* synthetic */ BluetoothLeService f10349P;

    C4786p(BluetoothLeService bluetoothLeService) {
        this.f10349P = bluetoothLeService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    public void run() {
        CrashApp.m16336c("BluetoothLeService", "reconnectDeviceRunnable");
        this.f10349P.m16194d(false);
    }
}
