package com.sxr.sdk.ble.keepfit.service;

import android.bluetooth.BluetoothDevice;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.sxr.sdk.ble.keepfit.service.p211b.DeviceConnectTask;
import com.sxr.sdk.ble.keepfit.service.p211b.SuotaManager;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.SysUtils;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.v */
/* compiled from: Ota */
public class C4790v {

    /* renamed from: i */
    public static int f10371i = 1;

    /* renamed from: j */
    public static int f10372j = 2;

    /* renamed from: k */
    public static int f10373k = 3;

    /* renamed from: l */
    public static int f10374l = 4;

    /* renamed from: m */
    public static int f10375m = 5;

    /* renamed from: n */
    public static int f10376n = 0;

    /* renamed from: o */
    public static int f10377o = 50;

    /* renamed from: p */
    public static int f10378p = 100;

    /* renamed from: q */
    public static int f10379q = 101;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f10380a = C4790v.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public BluetoothLeService f10381b;

    /* renamed from: c */
    protected boolean f10382c = false;

    /* renamed from: d */
    protected boolean f10383d = false;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f10384e = "";
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f10385f = "";
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f10386g = false;

    /* renamed from: h */
    private Handler f10387h = new Handler();

    public C4790v(BluetoothLeService bluetoothLeService) {
        this.f10381b = bluetoothLeService;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26580a(String str, String str2, int i) {
        this.f10384e = str2;
        this.f10385f = Environment.getExternalStorageDirectory() + Common.f10355e + str2 + Common.f10353c;
        StringBuilder sb = new StringBuilder();
        sb.append("OTA_JSON");
        sb.append(str2);
        String sb2 = sb.toString();
        String str3 = (String) ShareUtil.m16353a(sb2, "{}");
        String str4 = this.f10380a;
        CrashApp.m16336c(str4, sb2 + " " + str3);
        try {
            if (!str3.equalsIgnoreCase("{}")) {
                JSONObject jSONObject = new JSONObject(str3);
                StringBuilder sb3 = new StringBuilder();
                sb3.append("OTA_TIME");
                sb3.append(str2);
                long longValue = ((Long) ShareUtil.m16353a(sb3.toString(), 0L)).longValue();
                long j = jSONObject.getJSONObject("updateInfo").getLong("expire");
                String str5 = this.f10380a;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("time ");
                sb4.append(longValue);
                sb4.append(" expire ");
                sb4.append(j);
                CrashApp.m16336c(str5, sb4.toString());
                if (System.currentTimeMillis() - longValue < j * 3600 * 1000) {
                    m16423a(jSONObject, str, i);
                    return;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String format = String.format("http://download.keeprapid.com/apps/smartband/keepfit/fwupdater/%s/%s/update.json", Locale.getDefault().getLanguage(), str2);
        String str6 = this.f10380a;
        CrashApp.m16337d(str6, "compareOta url " + format);
        C4771h.m16342a(format, new Ota(this, sb2, str, i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.v.a(com.sxr.sdk.ble.keepfit.service.v, boolean):boolean
      com.sxr.sdk.ble.keepfit.service.v.a(org.json.JSONObject, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16423a(JSONObject jSONObject, String str, int i) {
        String str2;
        JSONObject jSONObject2 = jSONObject;
        String str3 = str;
        String str4 = "-";
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("updateInfo");
            String string = jSONObject3.getString("updateMode");
            char c = 1;
            char c2 = 0;
            if (!this.f10383d) {
                if (string.equals("individual")) {
                    int i2 = 16;
                    long parseLong = Long.parseLong(str3, 16);
                    String str5 = this.f10380a;
                    StringBuilder sb = new StringBuilder();
                    sb.append("lAddr ");
                    sb.append(parseLong);
                    CrashApp.m16336c(str5, sb.toString());
                    JSONArray jSONArray = jSONObject3.getJSONArray("macList");
                    int i3 = 0;
                    boolean z = false;
                    while (i3 < jSONArray.length()) {
                        String string2 = jSONArray.getString(i3);
                        if (string2.contains(str4)) {
                            String str6 = string2.split(str4)[c2];
                            String str7 = string2.split(str4)[c];
                            long parseLong2 = Long.parseLong(str6, i2);
                            long parseLong3 = Long.parseLong(str7, i2);
                            String str8 = this.f10380a;
                            StringBuilder sb2 = new StringBuilder();
                            str2 = str4;
                            sb2.append("lStart ");
                            sb2.append(parseLong2);
                            sb2.append(" lEnd ");
                            sb2.append(parseLong3);
                            CrashApp.m16336c(str8, sb2.toString());
                            if (parseLong2 <= parseLong && parseLong <= parseLong3) {
                            }
                            i3++;
                            str4 = str2;
                            i2 = 16;
                            c = 1;
                            c2 = 0;
                        } else {
                            str2 = str4;
                            if (!string2.equalsIgnoreCase(str3)) {
                                i3++;
                                str4 = str2;
                                i2 = 16;
                                c = 1;
                                c2 = 0;
                            }
                        }
                        z = true;
                        i3++;
                        str4 = str2;
                        i2 = 16;
                        c = 1;
                        c2 = 0;
                    }
                    if (!z && this.f10381b.f10228j0 != null) {
                        this.f10381b.f10228j0.mo22931b(false, jSONObject3.toString(), this.f10385f);
                        return;
                    }
                }
            }
            String string3 = jSONObject3.getString("versionCode");
            int parseInt = string3.length() == 12 ? Integer.parseInt(string3.substring(9, 12)) : 0;
            String str9 = this.f10380a;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("versionFw ");
            sb3.append(parseInt);
            CrashApp.m16336c(str9, sb3.toString());
            if (parseInt <= i || this.f10381b.f10228j0 == null) {
                this.f10381b.f10228j0.mo22931b(false, jSONObject3.toString(), this.f10385f);
                return;
            }
            this.f10381b.f10228j0.mo22931b(true, jSONObject3.toString(), this.f10385f);
            if (this.f10382c) {
                m16424a(jSONObject2, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16424a(JSONObject jSONObject, boolean z) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("updateInfo");
            String string = jSONObject2.getString("fwUrl");
            String string2 = jSONObject2.getString("md5");
            if (this.f10382c) {
                this.f10381b.f10228j0.mo22933c(f10371i, f10376n);
            }
            if (!this.f10386g) {
                File file = new File(this.f10385f);
                if (z || !file.exists() || !file.isFile()) {
                    C4771h.m16341a(string, new C4788t(this, string2));
                    this.f10386g = true;
                    String str = this.f10380a;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fwUrl ");
                    sb.append(string);
                    CrashApp.m16336c(str, sb.toString());
                } else if (!SysUtils.m16359a(file).equalsIgnoreCase(string2)) {
                    if (this.f10382c) {
                        this.f10381b.f10228j0.mo22933c(f10371i, f10379q);
                    }
                } else if (this.f10382c) {
                    m16420a();
                    this.f10381b.f10228j0.mo22933c(f10371i, f10378p);
                }
            } else if (this.f10382c) {
                this.f10381b.f10228j0.mo22933c(f10371i, f10377o);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16420a() {
        try {
            this.f10381b.mo26494a(1);
            this.f10381b.f10228j0.mo22933c(f10372j, f10376n);
            Thread.sleep(3000);
            this.f10381b.mo26497b();
            this.f10381b.f10228j0.mo22933c(f10372j, f10378p);
            this.f10387h.postDelayed(new C4789u(this), 20000);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26579a(BluetoothDevice bluetoothDevice) {
        try {
            this.f10381b.f10228j0.mo22933c(f10373k, f10378p);
            SuotaManager gVar = new SuotaManager(this.f10381b);
            gVar.mo26503a(bluetoothDevice);
            gVar.mo26505a(com.sxr.sdk.ble.keepfit.service.p211b.File.m16316a(this.f10385f));
            gVar.mo26515f(3);
            gVar.mo26510d().mo26546b(PsExtractor.VIDEO_STREAM_MASK);
            gVar.mo26509c(3);
            gVar.mo26511d(5);
            gVar.mo26513e(6);
            gVar.mo26517g(0);
            new DeviceConnectTask(this.f10381b, gVar).execute(new Void[0]);
            this.f10381b.f10228j0.mo22933c(f10374l, f10376n);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
        this.f10382c = false;
    }
}
