package com.sxr.sdk.ble.keepfit.service.p211b;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.g */
public class SuotaManager extends BluetoothManager {
    public SuotaManager(Context context) {
        super(context);
        super.f10285t = 1;
    }

    /* renamed from: h */
    private void m16329h(int i) {
        String format = String.format("%#10x", Integer.valueOf(i));
        String str = super.f10266a;
        Log.d(str, "processMemDevValue() step: " + super.f10286u + ", value: " + format);
        if (super.f10286u == 2) {
            if (i == 1) {
                mo26502a(3);
            } else {
                mo26507b(0);
            }
        }
    }

    /* renamed from: a */
    public void mo26504a(Intent intent) {
        int intExtra = intent.getIntExtra("step", -1);
        int intExtra2 = intent.getIntExtra("error", -1);
        int intExtra3 = intent.getIntExtra("memDevValue", -1);
        if (intExtra2 >= 0) {
            mo26507b(intExtra2);
        } else if (intExtra3 >= 0) {
            m16329h(intExtra3);
        }
        if (intExtra >= 0) {
            super.f10286u = intExtra;
        } else {
            intent.getIntExtra("characteristic", -1);
            intent.getStringExtra("value");
            mo26516g();
        }
        String str = super.f10266a;
        Log.d(str, "step " + super.f10286u);
        int i = super.f10286u;
        if (i == 0) {
            super.f10286u = -1;
            Intent intent2 = new Intent();
            intent2.setAction("BluetoothGattUpdate");
            intent2.putExtra("step", 1);
            mo26504a(intent2);
        } else if (i == 1) {
            mo26506b();
        } else if (i == 2) {
            mo26523m();
        } else if (i == 3) {
            mo26522l();
        } else if (i == 4) {
            mo26521k();
        } else if (i == 5) {
            if (!super.f10280o) {
                mo26518h();
            } else if (!super.f10282q) {
                mo26521k();
            } else if (!super.f10281p) {
                mo26518h();
            } else if (!super.f10283r) {
                mo26519i();
            } else {
                mo26514f();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public int mo26512e() {
        int i = super.f10267b;
        return ((i != 3 ? i != 4 ? -1 : 18 : 19) << 24) | super.f10275j;
    }
}
