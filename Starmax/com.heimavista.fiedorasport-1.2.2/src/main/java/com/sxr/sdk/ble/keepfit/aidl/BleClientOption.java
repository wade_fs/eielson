package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class BleClientOption implements Parcelable {
    public static final Parcelable.Creator<BleClientOption> CREATOR = new C4755a();

    /* renamed from: P */
    private UserProfile f10132P;

    /* renamed from: Q */
    private DeviceProfile f10133Q;

    /* renamed from: R */
    private ArrayList<AlarmInfoItem> f10134R;

    /* renamed from: S */
    private Weather f10135S;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.BleClientOption$a */
    static class C4755a implements Parcelable.Creator<BleClientOption> {
        C4755a() {
        }

        public BleClientOption createFromParcel(Parcel parcel) {
            return new BleClientOption(parcel);
        }

        public BleClientOption[] newArray(int i) {
            return new BleClientOption[i];
        }
    }

    public BleClientOption(UserProfile userProfile, DeviceProfile deviceProfile, ArrayList<AlarmInfoItem> arrayList) {
        this.f10132P = userProfile;
        this.f10133Q = deviceProfile;
        this.f10134R = arrayList;
    }

    /* renamed from: a */
    public DeviceProfile mo26383a() {
        return this.f10133Q;
    }

    /* renamed from: b */
    public ArrayList<AlarmInfoItem> mo26384b() {
        return this.f10134R;
    }

    /* renamed from: c */
    public UserProfile mo26385c() {
        return this.f10132P;
    }

    /* renamed from: d */
    public Weather mo26386d() {
        return this.f10135S;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f10132P, i);
        parcel.writeParcelable(this.f10133Q, i);
        parcel.writeList(this.f10134R);
        parcel.writeParcelable(this.f10135S, i);
    }

    public BleClientOption(Parcel parcel) {
        this.f10132P = (UserProfile) parcel.readParcelable(UserProfile.class.getClassLoader());
        this.f10133Q = (DeviceProfile) parcel.readParcelable(DeviceProfile.class.getClassLoader());
        this.f10134R = parcel.readArrayList(AlarmInfoItem.class.getClassLoader());
        this.f10135S = (Weather) parcel.readParcelable(Weather.class.getClassLoader());
    }
}
