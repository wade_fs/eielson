package com.sxr.sdk.ble.keepfit.service.p212c;

import android.app.Application;
import android.util.Log;
import com.sxr.sdk.ble.keepfit.service.Common;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.c */
public class CrashApp extends Application {

    /* renamed from: P */
    public static boolean f10321P = true;

    /* renamed from: Q */
    public static boolean f10322Q = false;

    /* renamed from: a */
    public static void m16334a(String str, String str2) {
        if (f10321P) {
            Log.d(str, str2);
        }
        if (f10322Q) {
            SysUtils.m16362a("[debug]" + str + ":" + str2, Common.f10355e, Common.f10352b);
        }
    }

    /* renamed from: b */
    public static void m16335b(String str, String str2) {
        if (f10321P) {
            Log.e(str, str2);
        }
        if (f10322Q) {
            SysUtils.m16362a("[error]" + str + ":" + str2, Common.f10355e, Common.f10352b);
        }
    }

    /* renamed from: c */
    public static void m16336c(String str, String str2) {
        if (f10321P) {
            Log.i(str, str2);
        }
        if (f10322Q) {
            SysUtils.m16362a("[info]" + str + ":" + str2, Common.f10355e, Common.f10352b);
        }
    }

    /* renamed from: d */
    public static void m16337d(String str, String str2) {
        if (f10321P) {
            Log.w(str, str2);
        }
        if (f10322Q) {
            SysUtils.m16362a("[warn]" + str + ":" + str2, Common.f10355e, Common.f10352b);
        }
    }
}
