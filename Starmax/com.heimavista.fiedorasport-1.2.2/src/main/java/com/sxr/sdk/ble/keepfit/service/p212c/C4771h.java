package com.sxr.sdk.ble.keepfit.service.p212c;

import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.h */
/* compiled from: HttpUtil */
public class C4771h {

    /* renamed from: com.sxr.sdk.ble.keepfit.service.c.h$a */
    /* compiled from: HttpUtil */
    public interface C4772a {
        /* renamed from: a */
        void mo26557a(int i, byte[] bArr);
    }

    /* renamed from: com.sxr.sdk.ble.keepfit.service.c.h$b */
    /* compiled from: HttpUtil */
    public interface C4773b {
        /* renamed from: a */
        void mo26498a(int i, String str);
    }

    /* renamed from: a */
    public static void m16341a(String str, C4772a aVar) {
        new Thread(new HttpUtil(str, aVar)).start();
    }

    /* renamed from: a */
    public static void m16342a(String str, C4773b bVar) {
        new Thread(new C4769f(str, bVar)).start();
    }

    /* renamed from: a */
    public static void m16343a(String str, JSONObject jSONObject, C4773b bVar) {
        new Thread(new C4770g(str, jSONObject, bVar)).start();
    }
}
