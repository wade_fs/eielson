package com.sxr.sdk.ble.keepfit.service;

import android.os.SystemClock;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;

/* renamed from: com.sxr.sdk.ble.keepfit.service.m */
/* compiled from: BluetoothLeService */
class C4783m implements Runnable {

    /* renamed from: P */
    private boolean f10345P = true;

    /* renamed from: Q */
    final /* synthetic */ BluetoothLeService f10346Q;

    C4783m(BluetoothLeService bluetoothLeService) {
        this.f10346Q = bluetoothLeService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.h(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.h(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.h(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.h(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], java.lang.String[], boolean):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothDevice, int, byte[]):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int, boolean):void */
    public void run() {
        while (this.f10345P) {
            CrashApp.m16336c("BluetoothLeService", "process_state_changing_runnable: running");
            try {
                long currentTimeMillis = System.currentTimeMillis();
                if (this.f10346Q.f10210a0 && currentTimeMillis - this.f10346Q.f10212b0 > AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS) {
                    long unused = this.f10346Q.f10212b0 = currentTimeMillis;
                    boolean unused2 = this.f10346Q.f10210a0 = false;
                    CrashApp.m16336c("BluetoothLeService", "process_state_changing_runnable timeout.");
                    this.f10346Q.m16144a(BluetoothLeService.f10169v1, false);
                }
                SystemClock.sleep(AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
            } catch (Exception e) {
                e.printStackTrace();
                CrashApp.m16335b("BluetoothLeService", "process_state_changing_runnable: exception");
            }
        }
    }
}
