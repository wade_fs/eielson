package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.a */
/* compiled from: BluetoothLeService */
class C4765a implements C4771h.C4773b {

    /* renamed from: a */
    final /* synthetic */ C4778h f10257a;

    C4765a(C4778h hVar) {
        this.f10257a = hVar;
    }

    /* renamed from: a */
    public void mo26498a(int i, String str) {
        CrashApp.m16337d("BluetoothLeService", "validate_sdk " + i + " " + str);
        if (i != 200) {
            try {
                if (this.f10257a.f10340a.f10228j0 != null) {
                    this.f10257a.f10340a.f10228j0.mo22907F(i);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else {
            JSONObject jSONObject = new JSONObject(str);
            int unused = this.f10257a.f10340a.f10238o0 = jSONObject.getInt("errcode");
            if (this.f10257a.f10340a.f10238o0 == 200) {
                ShareUtil.m16355b("SDK_TIME", Long.valueOf(System.currentTimeMillis()));
                JSONObject jSONObject2 = jSONObject.getJSONObject(TtmlNode.TAG_BODY);
                if (jSONObject2.has("expire")) {
                    ShareUtil.m16355b("SDK_EXPIRE", Integer.valueOf(jSONObject2.getInt("expire")));
                }
            }
            if (this.f10257a.f10340a.f10228j0 != null) {
                this.f10257a.f10340a.f10228j0.mo22907F(this.f10257a.f10340a.f10238o0);
            }
        }
    }
}
