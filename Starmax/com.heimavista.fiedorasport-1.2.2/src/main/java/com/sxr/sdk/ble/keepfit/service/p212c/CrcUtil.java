package com.sxr.sdk.ble.keepfit.service.p212c;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.d */
public class CrcUtil {

    /* renamed from: a */
    private static long f10323a;

    /* renamed from: b */
    private static int f10324b;

    /* renamed from: a */
    private static void m16339a(long j) {
        f10323a = m16340b((j ^ ((long) ((int) f10323a))) & 255) ^ (f10323a >> 8);
        f10324b++;
    }

    /* renamed from: b */
    private static long m16340b(long j) {
        for (int i = 0; i < 8; i++) {
            int i2 = ((1 & j) > 0 ? 1 : ((1 & j) == 0 ? 0 : -1));
            j >>= 1;
            if (i2 != 0) {
                j ^= 3988292384L;
            }
        }
        return j;
    }

    /* renamed from: a */
    public static long m16338a(long j, long[] jArr) {
        f10324b = 0;
        if (jArr == null) {
            return 0;
        }
        int length = jArr.length;
        f10323a = j ^ 4294967295L;
        while (length >= 8) {
            for (int i = 0; i < 8; i++) {
                m16339a(jArr[f10324b]);
            }
            length -= 8;
        }
        if (length != 0) {
            do {
                m16339a(jArr[f10324b]);
                length--;
            } while (length != 0);
        }
        return f10323a ^ 4294967295L;
    }
}
