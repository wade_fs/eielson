package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.SysUtils;
import java.io.File;

/* renamed from: com.sxr.sdk.ble.keepfit.service.t */
/* compiled from: Ota */
class C4788t implements C4771h.C4772a {

    /* renamed from: a */
    final /* synthetic */ String f10368a;

    /* renamed from: b */
    final /* synthetic */ C4790v f10369b;

    C4788t(C4790v vVar, String str) {
        this.f10369b = vVar;
        this.f10368a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.v.a(com.sxr.sdk.ble.keepfit.service.v, boolean):boolean
     arg types: [com.sxr.sdk.ble.keepfit.service.v, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.v.a(org.json.JSONObject, boolean):void
      com.sxr.sdk.ble.keepfit.service.v.a(com.sxr.sdk.ble.keepfit.service.v, boolean):boolean */
    /* renamed from: a */
    public void mo26557a(int i, byte[] bArr) {
        if (i == 200) {
            try {
                String a = this.f10369b.f10380a;
                StringBuilder sb = new StringBuilder();
                sb.append("fwUrl onResponse ");
                sb.append(bArr.length);
                CrashApp.m16336c(a, sb.toString());
                SysUtils.m16363a(this.f10369b.f10385f, bArr);
                String a2 = this.f10369b.f10380a;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("fwUrl write ");
                sb2.append(this.f10369b.f10385f);
                CrashApp.m16336c(a2, sb2.toString());
                boolean unused = this.f10369b.f10386g = false;
                if (SysUtils.m16359a(new File(this.f10369b.f10385f)).equalsIgnoreCase(this.f10368a)) {
                    if (this.f10369b.f10382c) {
                        this.f10369b.f10381b.f10228j0.mo22933c(C4790v.f10371i, C4790v.f10378p);
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("OTA_TIME");
                    sb3.append(this.f10369b.f10384e);
                    ShareUtil.m16355b(sb3.toString(), Long.valueOf(System.currentTimeMillis()));
                    if (this.f10369b.f10382c) {
                        this.f10369b.m16420a();
                    }
                } else if (this.f10369b.f10382c) {
                    this.f10369b.f10381b.f10228j0.mo22933c(C4790v.f10371i, C4790v.f10379q);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            this.f10369b.f10381b.f10228j0.mo22933c(C4790v.f10371i, C4790v.f10379q);
        }
    }
}
