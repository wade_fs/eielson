package com.sxr.sdk.ble.keepfit.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class DeviceFunction implements Parcelable {
    public static final Parcelable.Creator<DeviceFunction> CREATOR = new C4756a();

    /* renamed from: P */
    private int f10136P = 0;

    /* renamed from: Q */
    private int f10137Q = 0;

    /* renamed from: R */
    private int f10138R = 0;

    /* renamed from: S */
    private int f10139S = 0;

    /* renamed from: T */
    private int f10140T = 0;

    /* renamed from: U */
    private int f10141U = 0;

    /* renamed from: V */
    private int f10142V = 0;

    /* renamed from: com.sxr.sdk.ble.keepfit.aidl.DeviceFunction$a */
    static class C4756a implements Parcelable.Creator<DeviceFunction> {
        C4756a() {
        }

        public DeviceFunction createFromParcel(Parcel parcel) {
            return new DeviceFunction(parcel);
        }

        public DeviceFunction[] newArray(int i) {
            return new DeviceFunction[i];
        }
    }

    public DeviceFunction(Parcel parcel) {
        this.f10136P = parcel.readInt();
        this.f10137Q = parcel.readInt();
        this.f10138R = parcel.readInt();
        this.f10139S = parcel.readInt();
        this.f10140T = parcel.readInt();
        this.f10141U = parcel.readInt();
        this.f10142V = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f10136P);
        parcel.writeInt(this.f10137Q);
        parcel.writeInt(this.f10138R);
        parcel.writeInt(this.f10139S);
        parcel.writeInt(this.f10140T);
        parcel.writeInt(this.f10141U);
        parcel.writeInt(this.f10142V);
    }
}
