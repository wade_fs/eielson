package com.sxr.sdk.ble.keepfit.service;

import android.os.SystemClock;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.sxr.sdk.ble.keepfit.service.p212c.ConvertUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* renamed from: com.sxr.sdk.ble.keepfit.service.e */
/* compiled from: BluetoothLeService */
class C4775e implements Runnable {

    /* renamed from: P */
    final /* synthetic */ BluetoothLeService f10335P;

    C4775e(BluetoothLeService bluetoothLeService) {
        this.f10335P = bluetoothLeService;
    }

    public void run() {
        try {
            CrashApp.m16337d("BluetoothLeService", "sync_history_data_runnable_once");
            Calendar instance = Calendar.getInstance();
            long timeInMillis = instance.getTimeInMillis();
            long s = (timeInMillis - this.f10335P.f10202W) / 3600000;
            long s2 = (timeInMillis - this.f10335P.f10202W) / DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS;
            StringBuilder sb = new StringBuilder();
            sb.append("time expired hours: ");
            sb.append(String.valueOf(s));
            CrashApp.m16336c("BluetoothLeService", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("time expired minutes: ");
            sb2.append(String.valueOf(s2));
            CrashApp.m16336c("BluetoothLeService", sb2.toString());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            if (s2 <= 1) {
                String format = simpleDateFormat.format(new Date(this.f10335P.f10202W));
                SystemClock.sleep(3000);
                StringBuilder sb3 = new StringBuilder();
                sb3.append("writeCharacteristic : no newer data to sync at ");
                sb3.append(format);
                CrashApp.m16336c("BluetoothLeService", sb3.toString());
                this.f10335P.m16235j(false);
                return;
            }
            instance.setTimeInMillis(this.f10335P.f10202W);
            CrashApp.m16334a("BluetoothLeService", simpleDateFormat.format(instance.getTime()));
            byte[] bArr = new byte[20];
            bArr[0] = -93;
            bArr[1] = (byte) 17;
            int unused = this.f10335P.f10237n1 = this.f10335P.m16120a(instance.getTimeInMillis());
            int timeInMillis2 = (int) ((instance.getTimeInMillis() + ((long) this.f10335P.f10237n1)) / 1000);
            StringBuilder sb4 = new StringBuilder();
            sb4.append("last: ");
            sb4.append(timeInMillis2);
            CrashApp.m16337d("BluetoothLeService", sb4.toString());
            byte[] a = ConvertUtil.m16333a(timeInMillis2);
            bArr[2] = a[3];
            bArr[3] = a[2];
            bArr[4] = a[1];
            bArr[5] = a[0];
            byte[] unused2 = this.f10335P.m16164a(bArr, 20);
            boolean unused3 = this.f10335P.m16175b(bArr);
        } catch (Exception e) {
            e.printStackTrace();
            CrashApp.m16335b("BluetoothLeService", "sync_history_data_runnable: excption");
        }
    }
}
