package com.sxr.sdk.ble.keepfit.service;

import android.os.RemoteException;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* renamed from: com.sxr.sdk.ble.keepfit.service.f */
/* compiled from: BluetoothLeService */
class C4776f implements Runnable {

    /* renamed from: P */
    final /* synthetic */ String[] f10336P;

    /* renamed from: Q */
    final /* synthetic */ BluetoothLeService f10337Q;

    C4776f(BluetoothLeService bluetoothLeService, String[] strArr) {
        this.f10337Q = bluetoothLeService;
        this.f10336P = strArr;
    }

    public void run() {
        if (this.f10337Q.f10228j0 != null) {
            try {
                String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date((((long) (Integer.parseInt(this.f10336P[4] + this.f10336P[3] + this.f10336P[2] + this.f10336P[1], 16) - (this.f10337Q.f10237n1 / 1000))) * 1000) + ((long) 0)));
                int parseInt = Integer.parseInt(this.f10336P[5], 16);
                IServiceCallback bVar = this.f10337Q.f10228j0;
                bVar.mo22916a(2, format, parseInt & 15, (Integer.parseInt(this.f10336P[6], 16) << 4) + (parseInt >> 4));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
