package com.sxr.sdk.ble.keepfit.service;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.sxr.sdk.ble.keepfit.aidl.BleClientOption;
import com.sxr.sdk.ble.keepfit.aidl.IRemoteService;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import com.sxr.sdk.ble.keepfit.service.p212c.AuthrizeDataXmlPullParser;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.SysUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.sxr.sdk.ble.keepfit.service.h */
/* compiled from: BluetoothLeService */
class C4778h extends IRemoteService.C4760a {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10340a;

    C4778h(BluetoothLeService bluetoothLeService) {
        this.f10340a = bluetoothLeService;
    }

    /* renamed from: A */
    public int mo26441A() {
        return this.f10340a.m16249o();
    }

    /* renamed from: B */
    public int mo26442B() {
        return this.f10340a.m16198e();
    }

    /* renamed from: C */
    public int mo26443C() {
        return this.f10340a.m16177c();
    }

    /* renamed from: D */
    public int mo26445D() {
        return this.f10340a.m16251p();
    }

    /* renamed from: G */
    public int mo26446G(int i) {
        return this.f10340a.m16178c(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.k(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.k(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.k(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    /* renamed from: a */
    public int mo26452a(String str, String str2) {
        CrashApp.m16336c("BluetoothLeService", "connectBt " + str2);
        boolean unused = this.f10340a.f10214c0 = false;
        if (this.f10340a.f10238o0 == 200) {
            String unused2 = this.f10340a.f10206Y = str;
            String unused3 = this.f10340a.f10204X = str2;
            ShareUtil.m16355b("DEVICE_ADDRESS", this.f10340a.f10204X);
            this.f10340a.m16194d(false);
        } else {
            Log.e("BluetoothLeService", "called connect error:" + this.f10340a.f10238o0);
        }
        return this.f10340a.f10238o0;
    }

    /* renamed from: b */
    public void mo26465b(boolean z) {
        CrashApp.m16336c("BluetoothLeService", "disconnectBt " + z);
        if (z) {
            String unused = this.f10340a.f10206Y = (String) null;
            String unused2 = this.f10340a.f10204X = (String) null;
            ShareUtil.m16355b("DEVICE_ADDRESS", "");
        }
        boolean unused3 = this.f10340a.f10214c0 = z;
        this.f10340a.mo26497b();
    }

    /* renamed from: c */
    public int mo26466c(boolean z) {
        return this.f10340a.m16222h(z);
    }

    /* renamed from: e */
    public int mo26467e(int i, int i2) {
        return this.f10340a.m16115a(i, i2);
    }

    /* renamed from: f */
    public int mo26469f(boolean z) {
        return this.f10340a.m16216g(z);
    }

    /* renamed from: g */
    public int mo26470g(int i) {
        return this.f10340a.m16190d(i);
    }

    /* renamed from: h */
    public int mo26472h(boolean z) {
        CrashApp.m16336c("BluetoothLeService", "called RemoteService scanDevice");
        if (this.f10340a.f10238o0 == 200) {
            this.f10340a.mo26496a(z);
            int unused = this.f10340a.f10211a1 = 0;
        } else {
            Log.e("BluetoothLeService", "called scanDevice error:" + this.f10340a.f10238o0);
        }
        return this.f10340a.f10238o0;
    }

    /* renamed from: i */
    public int mo26473i(int i) {
        return this.f10340a.mo26494a(i);
    }

    /* renamed from: l */
    public int mo26474l(int i) {
        return this.f10340a.m16206f(i);
    }

    /* renamed from: n */
    public boolean mo26476n() {
        return this.f10340a.m16266w();
    }

    /* renamed from: o */
    public int mo26477o() {
        return this.f10340a.m16096E();
    }

    /* renamed from: p */
    public int mo26478p() {
        return this.f10340a.m16189d();
    }

    /* renamed from: q */
    public int mo26479q() {
        return this.f10340a.m16253q();
    }

    /* renamed from: s */
    public int mo26480s() {
        return this.f10340a.m16098F();
    }

    /* renamed from: u */
    public int mo26481u() {
        return this.f10340a.m16244m();
    }

    /* renamed from: v */
    public int mo26482v() {
        return this.f10340a.m16255r();
    }

    /* renamed from: w */
    public int mo26483w() {
        return this.f10340a.m16094D();
    }

    /* renamed from: x */
    public int mo26485x() {
        return this.f10340a.m16247n();
    }

    /* renamed from: y */
    public String mo26486y() {
        return this.f10340a.f10204X;
    }

    /* renamed from: z */
    public int mo26487z() {
        return this.f10340a.f10238o0;
    }

    /* renamed from: C */
    public int mo26444C(int i) {
        return this.f10340a.m16214g(i);
    }

    /* renamed from: e */
    public int mo26468e(boolean z) {
        return this.f10340a.m16210f(z);
    }

    /* renamed from: g */
    public int mo26471g(boolean z) {
        return this.f10340a.m16183c(z);
    }

    /* renamed from: n */
    public int mo26475n(int i) {
        return this.f10340a.m16199e(i);
    }

    /* renamed from: w */
    public int mo26484w(int i) {
        int unused = this.f10340a.f10215c1 = i;
        return this.f10340a.f10238o0;
    }

    /* renamed from: b */
    public void mo26464b(IServiceCallback bVar) {
        String str;
        PackageManager.NameNotFoundException e;
        String str2 = "";
        CrashApp.m16337d("BluetoothLeService", "registerCallback");
        this.f10340a.f10228j0 = bVar;
        if (System.currentTimeMillis() - ((Long) ShareUtil.m16353a("SDK_TIME", 0L)).longValue() < ((long) (((Integer) ShareUtil.m16353a("SDK_EXPIRE", 24)).intValue() * 3600 * 1000))) {
            BluetoothLeService bluetoothLeService = this.f10340a;
            IServiceCallback bVar2 = bluetoothLeService.f10228j0;
            if (bVar2 != null) {
                bVar2.mo22907F(bluetoothLeService.f10238o0);
                return;
            }
            return;
        }
        HashMap<String, String> a = new AuthrizeDataXmlPullParser(this.f10340a).mo26553a("JySDK.xml");
        this.f10340a.f10232l0 = a.get("vid");
        String unused = this.f10340a.f10234m0 = a.get("appid");
        String unused2 = this.f10340a.f10236n0 = a.get("secret");
        int i = 0;
        try {
            str = this.f10340a.getString(this.f10340a.getPackageManager().getApplicationInfo(this.f10340a.getPackageName(), 0).labelRes);
            try {
                PackageInfo packageInfo = this.f10340a.getPackageManager().getPackageInfo(this.f10340a.getPackageName(), 0);
                i = packageInfo.versionCode;
                str2 = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e2) {
                e = e2;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            e = e3;
            str = str2;
            e.printStackTrace();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action_cmd", "validate_sdk");
            jSONObject.put("seq_id", String.valueOf(System.currentTimeMillis()));
            jSONObject.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, SysUtils.m16357a());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("appid", this.f10340a.f10234m0);
            jSONObject2.put("secret", this.f10340a.f10236n0);
            jSONObject2.put("vid", this.f10340a.f10232l0);
            jSONObject2.put("phone_id", SysUtils.m16358a(this.f10340a));
            jSONObject2.put("phone_name", SysUtils.m16366b());
            jSONObject2.put("package_name", this.f10340a.getPackageName());
            jSONObject2.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, str);
            jSONObject2.put("app_version", str2);
            jSONObject2.put("app_build", i);
            jSONObject2.put("sdk_version", "1.0.6");
            jSONObject2.put("sdk_build", 7);
            jSONObject.put(TtmlNode.TAG_BODY, jSONObject2);
            CrashApp.m16337d("BluetoothLeService", "validate_sdk url " + "https://openapi.keeprapid.com/developer");
            CrashApp.m16337d("BluetoothLeService", "validate_sdk req " + jSONObject.toString());
            C4771h.m16343a("https://openapi.keeprapid.com/developer", jSONObject, new C4765a(this));
        }
        JSONObject jSONObject3 = new JSONObject();
        try {
            jSONObject3.put("action_cmd", "validate_sdk");
            jSONObject3.put("seq_id", String.valueOf(System.currentTimeMillis()));
            jSONObject3.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, SysUtils.m16357a());
            JSONObject jSONObject22 = new JSONObject();
            jSONObject22.put("appid", this.f10340a.f10234m0);
            jSONObject22.put("secret", this.f10340a.f10236n0);
            jSONObject22.put("vid", this.f10340a.f10232l0);
            jSONObject22.put("phone_id", SysUtils.m16358a(this.f10340a));
            jSONObject22.put("phone_name", SysUtils.m16366b());
            jSONObject22.put("package_name", this.f10340a.getPackageName());
            jSONObject22.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, str);
            jSONObject22.put("app_version", str2);
            jSONObject22.put("app_build", i);
            jSONObject22.put("sdk_version", "1.0.6");
            jSONObject22.put("sdk_build", 7);
            jSONObject3.put(TtmlNode.TAG_BODY, jSONObject22);
        } catch (JSONException e4) {
            e4.printStackTrace();
        }
        CrashApp.m16337d("BluetoothLeService", "validate_sdk url " + "https://openapi.keeprapid.com/developer");
        CrashApp.m16337d("BluetoothLeService", "validate_sdk req " + jSONObject3.toString());
        C4771h.m16343a("https://openapi.keeprapid.com/developer", jSONObject3, new C4765a(this));
    }

    /* renamed from: a */
    public int mo26453a(boolean z) {
        return this.f10340a.m16168b(z);
    }

    /* renamed from: a */
    public int mo26457a(boolean z, String str, String str2) {
        CrashApp.f10322Q = z;
        if (str != null && str.length() > 0) {
            Common.f10355e = str;
        }
        if (str2 == null || str2.length() <= 0) {
            return 1;
        }
        Common.f10352b = str2;
        return 1;
    }

    /* renamed from: a */
    public int mo26456a(boolean z, int i, int i2, int i3, int i4, int i5, int i6) {
        return this.f10340a.m16139a(z, i, i2, i3, i4, i5, i6);
    }

    /* renamed from: a */
    public int mo26455a(boolean z, int i, int i2) {
        return this.f10340a.m16138a(z, i, i2);
    }

    /* renamed from: a */
    public int mo26451a(String str) {
        return this.f10340a.m16209f(str);
    }

    /* renamed from: a */
    public int mo26454a(boolean z, int i) {
        return this.f10340a.m16137a(z, i);
    }

    /* renamed from: a */
    public int mo26447a(int i, int i2, int i3, int i4, int i5) {
        return this.f10340a.m16116a(i, i2, i3, i4, i5);
    }

    /* renamed from: a */
    public boolean mo26460a(String str, int i, String str2, String str3) {
        return this.f10340a.m16161a(str, i, str2, str3);
    }

    /* renamed from: a */
    public int mo26450a(BleClientOption bleClientOption) {
        if (this.f10340a.f10238o0 == 200) {
            int unused = this.f10340a.m16122a(bleClientOption);
        } else {
            Log.e("BluetoothLeService", "called setOption error:" + this.f10340a.f10238o0);
        }
        return this.f10340a.f10238o0;
    }

    /* renamed from: a */
    public int mo26448a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        return this.f10340a.m16117a(i, i2, i3, i4, i5, i6, i7);
    }

    /* renamed from: a */
    public int mo26449a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return this.f10340a.m16118a(i, i2, i3, i4, i5, i6, i7, i8);
    }

    /* renamed from: a */
    public int mo26458a(String[] strArr, String[] strArr2, boolean z) {
        return this.f10340a.m16141a(strArr, strArr2, z);
    }

    /* renamed from: a */
    public void mo26459a(IServiceCallback bVar) {
        CrashApp.m16337d("BluetoothLeService", "unregisterCallback");
        this.f10340a.f10228j0 = null;
    }

    /* renamed from: b */
    public int mo26463b(byte[] bArr) {
        return this.f10340a.m16140a(bArr);
    }

    /* renamed from: b */
    public int mo26461b(int i, String str) {
        return this.f10340a.m16119a(i, str);
    }

    /* renamed from: b */
    public int mo26462b(String str, byte[] bArr) {
        return this.f10340a.m16136a(str, bArr);
    }
}
