package com.sxr.sdk.ble.keepfit.service.p212c;

import android.util.Log;
import com.google.android.exoplayer2.C1750C;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.i */
public class ScannerServiceParser {
    /* renamed from: a */
    public static boolean m16350a(byte[] bArr, UUID uuid, boolean z) {
        String uuid2 = uuid != null ? uuid.toString() : null;
        if (bArr == null) {
            return false;
        }
        boolean z2 = !z;
        boolean z3 = uuid2 == null;
        if (z2 && z3) {
            return true;
        }
        int length = bArr.length;
        boolean z4 = z3;
        boolean z5 = z2;
        int i = 0;
        while (i < length) {
            byte b = bArr[i];
            if (b != 0) {
                int i2 = i + 1;
                byte b2 = bArr[i2];
                if (uuid2 != null) {
                    if (b2 == 2 || b2 == 3) {
                        for (int i3 = i2 + 1; i3 < (i2 + b) - 1; i3 += 2) {
                            z4 = z4 || m16351b(uuid2, bArr, i3, 2);
                        }
                    } else if (b2 == 4 || b2 == 5) {
                        for (int i4 = i2 + 1; i4 < (i2 + b) - 1; i4 += 4) {
                            z4 = z4 || m16352c(uuid2, bArr, i4, 4);
                        }
                    } else if (b2 == 6 || b2 == 7) {
                        for (int i5 = i2 + 1; i5 < (i2 + b) - 1; i5 += 16) {
                            z4 = z4 || m16349a(uuid2, bArr, i5, 16);
                        }
                    }
                }
                if (!z5 && b2 == 1) {
                    z5 = (bArr[i2 + 1] & 3) > 0;
                }
                i = i2 + (b - 1) + 1;
            } else if (!z5 || !z4) {
                return false;
            } else {
                return true;
            }
        }
        if (!z5 || !z4) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    private static boolean m16351b(String str, byte[] bArr, int i, int i2) {
        return Integer.toHexString(m16346a(bArr, i)).equals(str.substring(4, 8));
    }

    /* renamed from: c */
    private static boolean m16352c(String str, byte[] bArr, int i, int i2) {
        return Integer.toHexString(m16346a(bArr, (i + i2) - 4)).equals(str.substring(4, 8));
    }

    /* renamed from: a */
    public static String m16347a(byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            byte b = bArr[i];
            if (b == 0) {
                break;
            }
            int i2 = i + 1;
            byte b2 = bArr[i2];
            if (b2 == 9 || b2 == 8) {
                return m16348a(bArr, i2 + 1, b - 1);
            }
            i = i2 + (b - 1) + 1;
        }
        return "";
    }

    /* renamed from: a */
    public static String m16348a(byte[] bArr, int i, int i2) {
        try {
            return new String(bArr, i, i2, C1750C.UTF8_NAME);
        } catch (UnsupportedEncodingException e) {
            Log.e("ScannerServiceParser", "Unable to convert the complete local name to UTF-8", e);
            return null;
        } catch (IndexOutOfBoundsException e2) {
            Log.e("ScannerServiceParser", "Error when reading complete local name", e2);
            return null;
        }
    }

    /* renamed from: a */
    private static boolean m16349a(String str, byte[] bArr, int i, int i2) {
        return Integer.toHexString(m16346a(bArr, (i + i2) - 4)).equals(str.substring(4, 8));
    }

    /* renamed from: a */
    private static int m16346a(byte[] bArr, int i) {
        return ((bArr[i + 1] & 255) << 8) | (bArr[i] & 255);
    }
}
