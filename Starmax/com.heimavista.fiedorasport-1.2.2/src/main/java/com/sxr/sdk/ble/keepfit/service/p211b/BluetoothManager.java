package com.sxr.sdk.ble.keepfit.service.p211b;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.b */
public abstract class BluetoothManager {

    /* renamed from: a */
    public final String f10266a = getClass().getSimpleName();

    /* renamed from: b */
    int f10267b;

    /* renamed from: c */
    int f10268c;

    /* renamed from: d */
    int f10269d;

    /* renamed from: e */
    int f10270e;

    /* renamed from: f */
    int f10271f;

    /* renamed from: g */
    int f10272g;

    /* renamed from: h */
    int f10273h;

    /* renamed from: i */
    int f10274i;

    /* renamed from: j */
    int f10275j;

    /* renamed from: k */
    File f10276k;

    /* renamed from: l */
    Context f10277l;

    /* renamed from: m */
    BluetoothDevice f10278m;

    /* renamed from: n */
    HashMap<Integer, String> f10279n;

    /* renamed from: o */
    boolean f10280o = false;

    /* renamed from: p */
    boolean f10281p = false;

    /* renamed from: q */
    boolean f10282q = false;

    /* renamed from: r */
    boolean f10283r = false;

    /* renamed from: s */
    boolean f10284s = false;

    /* renamed from: t */
    public int f10285t;

    /* renamed from: u */
    protected int f10286u;

    /* renamed from: v */
    int f10287v = 0;

    /* renamed from: w */
    int f10288w = -1;

    /* renamed from: x */
    public Queue f10289x;

    public BluetoothManager(Context context) {
        this.f10277l = context;
        m16284p();
        this.f10289x = new ArrayDeque();
    }

    /* renamed from: n */
    private int m16282n() {
        return (this.f10272g << 16) | (this.f10273h << 8) | this.f10274i;
    }

    /* renamed from: o */
    private int m16283o() {
        return (this.f10268c << 24) | (this.f10269d << 16) | (this.f10270e << 8) | this.f10271f;
    }

    /* renamed from: p */
    private void m16284p() {
        this.f10279n = new HashMap<>();
        this.f10279n.put(3, "Forced exit of SPOTA service. See Table 1");
        this.f10279n.put(4, "Patch Data CRC mismatch.");
        this.f10279n.put(5, "Received patch Length not equal to PATCH_LEN characteristic value.");
        this.f10279n.put(6, "External Memory Error. Writing to external device failed.");
        this.f10279n.put(7, "Internal Memory Error. Not enough internal memory space for patch.");
        this.f10279n.put(8, "Invalid memory device.");
        this.f10279n.put(9, "Application error.");
        this.f10279n.put(11, "Invalid image bank");
        this.f10279n.put(12, "Invalid image header");
        this.f10279n.put(13, "Invalid image size");
        this.f10279n.put(14, "Invalid product header");
        this.f10279n.put(15, "Same Image Error");
        this.f10279n.put(16, " Failed to read from external memory device");
    }

    /* renamed from: a */
    public abstract void mo26504a(Intent intent);

    /* renamed from: a */
    public void mo26505a(File eVar) {
        this.f10276k = eVar;
        this.f10276k.mo26548c(this.f10285t);
    }

    /* renamed from: b */
    public void mo26506b() {
        Log.d(this.f10266a, "- enableNotifications");
        Log.i(this.f10266a, "- Enable notifications for SPOTA_SERV_STATUS characteristic");
        for (BluetoothGattService bluetoothGattService : BluetoothGattSingleton.m16279a().getServices()) {
            String str = this.f10266a;
            Log.i(str, "  Found service: " + bluetoothGattService.getUuid().toString());
            for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
                String str2 = this.f10266a;
                Log.i(str2, "  Found characteristic: " + bluetoothGattCharacteristic.getUuid().toString());
                if (bluetoothGattCharacteristic.getUuid().equals(Statics.f10313g)) {
                    Log.i(this.f10266a, "*** Found SUOTA service");
                    BluetoothGattSingleton.m16279a().setCharacteristicNotification(bluetoothGattCharacteristic, true);
                    BluetoothGattDescriptor descriptor = bluetoothGattCharacteristic.getDescriptor(Statics.f10314h);
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    BluetoothGattSingleton.m16279a().writeDescriptor(descriptor);
                }
            }
        }
    }

    /* renamed from: c */
    public BluetoothDevice mo26508c() {
        return this.f10278m;
    }

    /* renamed from: d */
    public File mo26510d() {
        return this.f10276k;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public abstract int mo26512e();

    /* renamed from: e */
    public void mo26513e(int i) {
        this.f10269d = i;
    }

    /* renamed from: f */
    public void mo26515f(int i) {
        this.f10267b = i;
    }

    /* renamed from: g */
    public void mo26517g(int i) {
        this.f10271f = i;
    }

    /* renamed from: h */
    public float mo26518h() {
        float d = (((float) (this.f10287v + 1)) / ((float) this.f10276k.mo26549d())) * 100.0f;
        if (!this.f10281p) {
            m16281h((int) d);
            Log.d(this.f10266a, "Sending block " + (this.f10287v + 1) + " of " + this.f10276k.mo26549d());
            byte[][] a = this.f10276k.mo26544a(this.f10287v);
            int i = this.f10288w + 1;
            this.f10288w = i;
            boolean z = false;
            if (this.f10288w == a.length - 1) {
                this.f10288w = -1;
                z = true;
            }
            byte[] bArr = a[i];
            "Sending chunk " + ((this.f10287v * this.f10276k.mo26545b()) + i + 1) + " of " + this.f10276k.mo26551f() + " (with " + bArr.length + " bytes)";
            Log.d(this.f10266a, "Sending block " + (this.f10287v + 1) + ", chunk " + (i + 1) + ", blocksize: " + a.length + ", chunksize " + bArr.length);
            BluetoothGattCharacteristic characteristic = BluetoothGattSingleton.m16279a().getService(Statics.f10307a).getCharacteristic(Statics.f10312f);
            characteristic.setValue(bArr);
            characteristic.setWriteType(1);
            boolean writeCharacteristic = BluetoothGattSingleton.m16279a().writeCharacteristic(characteristic);
            String str = this.f10266a;
            StringBuilder sb = new StringBuilder();
            sb.append("writeCharacteristic: ");
            sb.append(writeCharacteristic);
            Log.d(str, sb.toString());
            if (z) {
                if (!this.f10280o) {
                    this.f10287v++;
                } else {
                    this.f10281p = true;
                }
                if (this.f10287v + 1 == this.f10276k.mo26549d()) {
                    this.f10280o = true;
                }
            }
        }
        return d;
    }

    /* renamed from: i */
    public void mo26519i() {
        Log.d(this.f10266a, "sendEndSignal");
        BluetoothGattCharacteristic characteristic = BluetoothGattSingleton.m16279a().getService(Statics.f10307a).getCharacteristic(Statics.f10308b);
        characteristic.setValue(-33554432, 20, 0);
        BluetoothGattSingleton.m16279a().writeCharacteristic(characteristic);
        this.f10283r = true;
    }

    /* renamed from: j */
    public void mo26520j() {
        Log.d(this.f10266a, "sendRebootSignal");
        BluetoothGattCharacteristic characteristic = BluetoothGattSingleton.m16279a().getService(Statics.f10307a).getCharacteristic(Statics.f10308b);
        characteristic.setValue(-50331648, 20, 0);
        BluetoothGattSingleton.m16279a().writeCharacteristic(characteristic);
    }

    /* renamed from: k */
    public void mo26521k() {
        int c = this.f10276k.mo26547c();
        if (this.f10280o) {
            c = this.f10276k.mo26550e() % this.f10276k.mo26547c();
            this.f10282q = true;
        }
        String str = this.f10266a;
        Log.d(str, "setPatchLength: " + c + " - " + String.format("%#4x", Integer.valueOf(c)));
        BluetoothGattCharacteristic characteristic = BluetoothGattSingleton.m16279a().getService(Statics.f10307a).getCharacteristic(Statics.f10311e);
        characteristic.setValue(c, 18, 0);
        BluetoothGattSingleton.m16279a().writeCharacteristic(characteristic);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a  */
    /* renamed from: l */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo26522l() {
        /*
            r6 = this;
            int r0 = r6.f10267b
            r1 = 1
            r2 = 0
            r3 = 3
            if (r0 == r3) goto L_0x0012
            r3 = 4
            if (r0 == r3) goto L_0x000d
            r0 = 0
            r3 = 0
            goto L_0x0018
        L_0x000d:
            int r0 = r6.m16282n()
            goto L_0x0016
        L_0x0012:
            int r0 = r6.m16283o()
        L_0x0016:
            r3 = r0
            r0 = 1
        L_0x0018:
            if (r0 == 0) goto L_0x005b
            java.lang.String r0 = r6.f10266a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "setSpotaGpioMap: "
            r4.append(r5)
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Integer r5 = java.lang.Integer.valueOf(r3)
            r1[r2] = r5
            java.lang.String r5 = "%#10x"
            java.lang.String r1 = java.lang.String.format(r5, r1)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            android.util.Log.d(r0, r1)
            android.bluetooth.BluetoothGatt r0 = com.sxr.sdk.ble.keepfit.service.p211b.BluetoothGattSingleton.m16279a()
            java.util.UUID r1 = com.sxr.sdk.ble.keepfit.service.p211b.Statics.f10307a
            android.bluetooth.BluetoothGattService r0 = r0.getService(r1)
            java.util.UUID r1 = com.sxr.sdk.ble.keepfit.service.p211b.Statics.f10309c
            android.bluetooth.BluetoothGattCharacteristic r0 = r0.getCharacteristic(r1)
            r1 = 20
            r0.setValue(r3, r1, r2)
            android.bluetooth.BluetoothGatt r1 = com.sxr.sdk.ble.keepfit.service.p211b.BluetoothGattSingleton.m16279a()
            r1.writeCharacteristic(r0)
            goto L_0x0062
        L_0x005b:
            java.lang.String r0 = r6.f10266a
            java.lang.String r1 = "Memory type not set."
            android.util.Log.e(r0, r1)
        L_0x0062:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sxr.sdk.ble.keepfit.service.p211b.BluetoothManager.mo26522l():void");
    }

    /* renamed from: m */
    public void mo26523m() {
        BluetoothGattCharacteristic characteristic = BluetoothGattSingleton.m16279a().getService(Statics.f10307a).getCharacteristic(Statics.f10308b);
        int e = mo26512e();
        characteristic.setValue(e, 20, 0);
        BluetoothGattSingleton.m16279a().writeCharacteristic(characteristic);
        String str = this.f10266a;
        Log.i(str, "setSpotaMemDev: " + String.format("%#10x", Integer.valueOf(e)));
    }

    /* renamed from: c */
    public void mo26509c(int i) {
        this.f10270e = i;
    }

    /* renamed from: d */
    public void mo26511d(int i) {
        this.f10268c = i;
    }

    /* renamed from: f */
    public void mo26514f() {
        mo26520j();
        Intent intent = new Intent("ProgressUpdate");
        intent.putExtra("error", 100);
        intent.putExtra(NotificationCompat.CATEGORY_PROGRESS, 100);
        this.f10277l.sendBroadcast(intent);
    }

    /* renamed from: g */
    public void mo26516g() {
        if (this.f10289x.size() >= 1) {
            BluetoothGattSingleton.m16279a().readCharacteristic((BluetoothGattCharacteristic) this.f10289x.poll());
            Log.d(this.f10266a, "readNextCharacteristic");
        }
    }

    /* renamed from: a */
    public void mo26503a(BluetoothDevice bluetoothDevice) {
        this.f10278m = bluetoothDevice;
    }

    /* renamed from: a */
    public void mo26501a() {
        try {
            BluetoothGattSingleton.m16279a().disconnect();
            BluetoothGattSingleton.m16279a().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (this.f10276k != null) {
                this.f10276k.mo26543a();
            }
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26502a(int i) {
        Intent intent = new Intent();
        intent.putExtra("step", i);
        mo26504a(intent);
    }

    /* renamed from: b */
    public void mo26507b(int i) {
        if (!this.f10284s) {
            String str = this.f10279n.get(Integer.valueOf(i));
            mo26520j();
            mo26501a();
            this.f10284s = true;
            Intent intent = new Intent("ProgressUpdate");
            intent.putExtra("error", i);
            intent.putExtra(NotificationCompat.CATEGORY_PROGRESS, 100);
            this.f10277l.sendBroadcast(intent);
        }
    }

    /* renamed from: h */
    private void m16281h(int i) {
        Intent intent = new Intent("ProgressUpdate");
        intent.putExtra(NotificationCompat.CATEGORY_PROGRESS, i);
        this.f10277l.sendBroadcast(intent);
    }
}
