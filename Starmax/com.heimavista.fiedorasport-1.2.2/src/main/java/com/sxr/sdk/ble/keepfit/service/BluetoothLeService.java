package com.sxr.sdk.ble.keepfit.service;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.facebook.internal.ServerProtocol;
import com.google.android.exoplayer2.C1750C;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.BleClientOption;
import com.sxr.sdk.ble.keepfit.aidl.IRemoteService;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import com.sxr.sdk.ble.keepfit.aidl.Weather;
import com.sxr.sdk.ble.keepfit.service.p210a.AncsItem;
import com.sxr.sdk.ble.keepfit.service.p210a.SampleGattAttributes;
import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import com.sxr.sdk.ble.keepfit.service.p212c.ConvertUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import com.sxr.sdk.ble.keepfit.service.p212c.CrcUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.ShareUtil;
import com.sxr.sdk.ble.keepfit.service.p212c.SysUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class BluetoothLeService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: v1 */
    public static int f10169v1;
    /* access modifiers changed from: private */

    /* renamed from: w1 */
    public static int f10170w1;
    /* access modifiers changed from: private */

    /* renamed from: x1 */
    public static Handler f10171x1;

    /* renamed from: y1 */
    private static Runnable f10172y1;

    /* renamed from: A0 */
    private int f10173A0;

    /* renamed from: B0 */
    private int f10174B0;

    /* renamed from: C0 */
    private int f10175C0;

    /* renamed from: D0 */
    private int f10176D0;

    /* renamed from: E0 */
    private Weather f10177E0;

    /* renamed from: F0 */
    private List<AlarmInfoItem> f10178F0 = new ArrayList();

    /* renamed from: G0 */
    private BluetoothGattCallback f10179G0 = new C4781k(this);

    /* renamed from: H0 */
    private Handler f10180H0;

    /* renamed from: I0 */
    private HashMap<String, BluetoothGattService> f10181I0 = new HashMap<>();

    /* renamed from: J0 */
    private Runnable f10182J0;
    /* access modifiers changed from: private */

    /* renamed from: K0 */
    public ArrayList<BluetoothGatt> f10183K0 = new ArrayList<>();

    /* renamed from: L0 */
    private String[] f10184L0 = new String[0];

    /* renamed from: M0 */
    private String[] f10185M0 = new String[0];
    /* access modifiers changed from: private */

    /* renamed from: N0 */
    public boolean f10186N0 = false;
    /* access modifiers changed from: private */

    /* renamed from: O0 */
    public boolean f10187O0 = false;

    /* renamed from: P */
    private BluetoothManager f10188P = null;
    /* access modifiers changed from: private */

    /* renamed from: P0 */
    public boolean f10189P0 = true;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public BluetoothAdapter f10190Q = null;
    /* access modifiers changed from: private */

    /* renamed from: Q0 */
    public List<byte[]> f10191Q0 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: R */
    public BluetoothGatt f10192R = null;

    /* renamed from: R0 */
    private boolean f10193R0 = true;

    /* renamed from: S */
    private boolean f10194S = true;
    /* access modifiers changed from: private */

    /* renamed from: S0 */
    public boolean f10195S0;

    /* renamed from: T */
    private Thread f10196T = null;

    /* renamed from: T0 */
    private BroadcastReceiver f10197T0;

    /* renamed from: U */
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> f10198U = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: U0 */
    public int f10199U0;

    /* renamed from: V */
    private Thread f10200V = null;

    /* renamed from: V0 */
    private ArrayList<AncsItem> f10201V0;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public long f10202W = 0;

    /* renamed from: W0 */
    private int f10203W0;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public String f10204X;

    /* renamed from: X0 */
    private String f10205X0;
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public String f10206Y;

    /* renamed from: Y0 */
    private BluetoothAdapter.LeScanCallback f10207Y0;

    /* renamed from: Z */
    public boolean f10208Z = false;

    /* renamed from: Z0 */
    private ScanCallback f10209Z0;
    /* access modifiers changed from: private */

    /* renamed from: a0 */
    public boolean f10210a0 = false;
    /* access modifiers changed from: private */

    /* renamed from: a1 */
    public int f10211a1;
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public long f10212b0 = 0;

    /* renamed from: b1 */
    private boolean f10213b1;
    /* access modifiers changed from: private */

    /* renamed from: c0 */
    public boolean f10214c0 = false;
    /* access modifiers changed from: private */

    /* renamed from: c1 */
    public int f10215c1;
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public int f10216d0 = 1;

    /* renamed from: d1 */
    private boolean f10217d1;
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public int f10218e0 = 0;
    /* access modifiers changed from: private */

    /* renamed from: e1 */
    public byte[] f10219e1;

    /* renamed from: f0 */
    private Handler f10220f0 = null;
    /* access modifiers changed from: private */

    /* renamed from: f1 */
    public boolean f10221f1;

    /* renamed from: g0 */
    private Handler f10222g0;
    /* access modifiers changed from: private */

    /* renamed from: g1 */
    public int f10223g1;

    /* renamed from: h0 */
    private Runnable f10224h0;
    /* access modifiers changed from: private */

    /* renamed from: h1 */
    public long f10225h1;

    /* renamed from: i0 */
    private Runnable f10226i0;

    /* renamed from: i1 */
    Runnable f10227i1;

    /* renamed from: j0 */
    protected IServiceCallback f10228j0;

    /* renamed from: j1 */
    private boolean f10229j1;

    /* renamed from: k0 */
    private final IRemoteService.C4760a f10230k0 = new C4778h(this);

    /* renamed from: k1 */
    private int f10231k1;

    /* renamed from: l0 */
    protected String f10232l0;

    /* renamed from: l1 */
    private int f10233l1;
    /* access modifiers changed from: private */

    /* renamed from: m0 */
    public String f10234m0;

    /* renamed from: m1 */
    private int f10235m1;
    /* access modifiers changed from: private */

    /* renamed from: n0 */
    public String f10236n0;
    /* access modifiers changed from: private */

    /* renamed from: n1 */
    public int f10237n1;
    /* access modifiers changed from: private */

    /* renamed from: o0 */
    public int f10238o0 = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;

    /* renamed from: o1 */
    private Runnable f10239o1;

    /* renamed from: p0 */
    private int f10240p0 = 0;

    /* renamed from: p1 */
    private boolean f10241p1;

    /* renamed from: q0 */
    private int f10242q0 = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;

    /* renamed from: q1 */
    private C4790v f10243q1;

    /* renamed from: r0 */
    private BleClientOption f10244r0;

    /* renamed from: r1 */
    private int f10245r1;

    /* renamed from: s0 */
    private boolean f10246s0 = false;

    /* renamed from: s1 */
    private Handler f10247s1;

    /* renamed from: t0 */
    private boolean f10248t0 = false;

    /* renamed from: t1 */
    private Runnable f10249t1;

    /* renamed from: u0 */
    private boolean f10250u0 = false;

    /* renamed from: u1 */
    private Runnable f10251u1;

    /* renamed from: v0 */
    private int f10252v0 = 0;

    /* renamed from: w0 */
    private int f10253w0 = 0;

    /* renamed from: x0 */
    private int f10254x0 = 0;

    /* renamed from: y0 */
    private int f10255y0 = 0;

    /* renamed from: z0 */
    private int f10256z0;

    public BluetoothLeService() {
        new C4783m(this);
        this.f10195S0 = false;
        this.f10197T0 = new C4784n(this);
        this.f10199U0 = 0;
        this.f10201V0 = new ArrayList<>();
        this.f10205X0 = "";
        this.f10207Y0 = new C4787q(this);
        this.f10211a1 = 0;
        this.f10213b1 = false;
        this.f10215c1 = 2;
        this.f10217d1 = false;
        this.f10221f1 = true;
        this.f10227i1 = new C4774d(this);
        this.f10229j1 = false;
        this.f10231k1 = -1;
        this.f10233l1 = -1;
        this.f10235m1 = -1;
        this.f10239o1 = new C4775e(this);
        this.f10241p1 = false;
        this.f10243q1 = new C4790v(this);
        this.f10245r1 = 2000;
        this.f10247s1 = new Handler();
    }

    /* renamed from: M */
    static /* synthetic */ int m16106M(BluetoothLeService bluetoothLeService) {
        int i = bluetoothLeService.f10211a1;
        bluetoothLeService.f10211a1 = i + 1;
        return i;
    }

    public IBinder onBind(Intent intent) {
        CrashApp.m16337d("BluetoothLeService", "onBind");
        return this.f10230k0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    public void onCreate() {
        super.onCreate();
        CrashApp.m16336c("BluetoothLeService", "onCreate");
        Common.m16416a(getPackageName());
        ShareUtil.m16354a(this);
        if (this.f10196T == null) {
            this.f10196T = new Thread(this.f10227i1);
            this.f10196T.start();
        }
        m16144a(0, false);
        m16262u();
        registerReceiver(this.f10197T0, m16270y());
        this.f10222g0 = new Handler();
        this.f10204X = (String) ShareUtil.m16353a("DEVICE_ADDRESS", "");
        m16194d(true);
        if (Build.VERSION.SDK_INT >= 21) {
            this.f10209Z0 = new C4779i(this);
        }
    }

    public void onDestroy() {
        CrashApp.m16337d("BluetoothLeService", "onDestroy");
        this.f10187O0 = false;
        this.f10217d1 = true;
        CrashApp.m16336c("BluetoothLeService", "user disconnect device.");
        mo26495a();
        this.f10214c0 = true;
        CrashApp.m16336c("BluetoothLeService", "enter silence mode " + this.f10217d1 + " mUserDisconnected " + this.f10214c0);
        Handler handler = f10171x1;
        if (handler != null) {
            handler.removeCallbacks(f10172y1);
        }
        Handler handler2 = this.f10222g0;
        if (handler2 != null) {
            handler2.removeCallbacks(this.f10224h0);
            this.f10222g0.removeCallbacks(this.f10226i0);
        }
        m16093C();
        m16091B();
        this.f10221f1 = false;
        Thread thread = this.f10196T;
        if (thread != null) {
            thread.interrupt();
            this.f10196T = null;
        }
        CrashApp.m16335b("BluetoothLeService", "Ble Service-onDestroy");
        unregisterReceiver(this.f10197T0);
        stopForeground(true);
        super.onDestroy();
        if (m16266w()) {
            Process.killProcess(Process.myPid());
            CrashApp.m16335b("BluetoothLeService", "killProcess");
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        CrashApp.m16336c("BluetoothLeService", "onStartCommand");
        return 1;
    }

    public boolean onUnbind(Intent intent) {
        CrashApp.m16337d("BluetoothLeService", "onUnbind");
        return super.onUnbind(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public void m16089A() {
        byte[] bArr;
        m16204e(false);
        boolean z = true;
        m16231i(true);
        Intent intent = new Intent(Common.f10356f);
        intent.putExtra("bin_data", this.f10219e1);
        sendBroadcast(intent);
        if (!this.f10229j1 && (bArr = this.f10219e1) != null) {
            String hexString = Integer.toHexString(bArr[0] & 255);
            String[] strArr = {"c6"};
            int i = 0;
            while (true) {
                if (i >= strArr.length) {
                    z = false;
                    break;
                } else if (hexString.equalsIgnoreCase(strArr[i])) {
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                this.f10191Q0.add(0, this.f10219e1);
                CrashApp.m16337d("BluetoothLeService", "resending 1 data " + SysUtils.m16360a(this.f10219e1) + ", bBleCmdSessionCompleted " + this.f10189P0);
            }
        }
        this.f10229j1 = false;
        this.f10219e1 = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: B */
    public void m16091B() {
        this.f10218e0 = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: C */
    public void m16093C() {
        Handler handler = this.f10180H0;
        if (handler != null) {
            handler.removeCallbacks(this.f10182J0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: D */
    public int m16094D() {
        if (!m16197d("sendWeather2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        try {
            if (this.f10177E0 != null) {
                bArr[0] = 34;
                byte[] a = ConvertUtil.m16333a(this.f10177E0.mo26436i());
                bArr[2] = a[0];
                bArr[3] = a[1];
                bArr[4] = a[2];
                bArr[5] = a[3];
                byte[] a2 = ConvertUtil.m16333a(this.f10177E0.mo26430d());
                bArr[6] = a2[0];
                bArr[7] = a2[1];
                byte[] a3 = ConvertUtil.m16333a(this.f10177E0.mo26432e());
                bArr[8] = a3[0];
                bArr[9] = a3[1];
                bArr[10] = (byte) this.f10177E0.mo26434g();
                bArr[11] = (byte) this.f10177E0.mo26433f();
                bArr[12] = (byte) this.f10177E0.mo26428b();
                byte[] a4 = ConvertUtil.m16333a(this.f10177E0.mo26435h());
                bArr[13] = a4[0];
                bArr[14] = a4[1];
                bArr[15] = (byte) this.f10177E0.mo26437j();
                byte[] a5 = ConvertUtil.m16333a(this.f10177E0.mo26427a());
                bArr[16] = a5[0];
                bArr[17] = a5[1];
                bArr[18] = (byte) this.f10177E0.mo26429c();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public int m16096E() {
        if (!m16197d("setAlarms")) {
            return this.f10242q0;
        }
        for (int i = 0; i < this.f10178F0.size(); i++) {
            m16121a(this.f10178F0.get(i));
        }
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: F */
    public int m16098F() {
        if (!m16197d("setDeviceInfo2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        int i = 0;
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        bArr[0] = 27;
        bArr[1] = (byte) (this.f10246s0 ? 1 : 0);
        bArr[2] = (byte) (this.f10248t0 ? 1 : 0);
        if (this.f10250u0) {
            i = 1;
        }
        bArr[5] = (byte) i;
        bArr[6] = (byte) this.f10252v0;
        bArr[7] = (byte) this.f10254x0;
        bArr[8] = (byte) this.f10253w0;
        bArr[9] = (byte) this.f10255y0;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public int m16177c() {
        if (!m16197d("setDeviceTime")) {
            return this.f10242q0;
        }
        Calendar instance = Calendar.getInstance();
        byte[] bArr = new byte[20];
        this.f10237n1 = m16120a(instance.getTimeInMillis());
        int timeInMillis = (int) ((instance.getTimeInMillis() + ((long) this.f10237n1)) / 1000);
        CrashApp.m16337d("BluetoothLeService", "last: " + timeInMillis);
        byte[] a = ConvertUtil.m16333a(timeInMillis);
        bArr[0] = 1;
        bArr[1] = a[0];
        bArr[2] = a[1];
        bArr[3] = a[2];
        bArr[4] = a[3];
        for (int i = 5; i < 20; i++) {
            bArr[i] = 0;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public int m16189d() {
        Locale locale = getResources().getConfiguration().locale;
        String str = locale.getLanguage() + "-" + locale.getCountry();
        Log.i("BluetoothLeService", "setLanguage2 " + str);
        if (!m16197d("setLanguage2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        try {
            byte[] bytes = str.getBytes(C1750C.UTF8_NAME);
            bArr[0] = 33;
            int i2 = 18;
            if (bytes.length <= 19) {
                i2 = bytes.length;
            }
            System.arraycopy(bytes, 0, bArr, 1, i2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public int m16198e() {
        int i;
        if (!m16197d("setUserInfo")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        bArr[0] = 2;
        if (this.f10256z0 == 1) {
            i = this.f10176D0 | 128;
        } else {
            i = 0 | this.f10176D0;
        }
        bArr[1] = (byte) i;
        bArr[2] = (byte) this.f10173A0;
        bArr[3] = (byte) this.f10174B0;
        bArr[4] = (byte) this.f10175C0;
        m16175b(bArr);
        return this.f10242q0;
    }

    @TargetApi(21)
    /* renamed from: f */
    private void m16212f() {
        if (Build.VERSION.SDK_INT >= 21) {
            ScanSettings build = new ScanSettings.Builder().setScanMode(this.f10215c1).build();
            ScanFilter build2 = new ScanFilter.Builder().build();
            ArrayList arrayList = new ArrayList();
            arrayList.add(build2);
            if (this.f10190Q.getBluetoothLeScanner() != null) {
                this.f10190Q.getBluetoothLeScanner().startScan(arrayList, build, this.f10209Z0);
            }
        } else {
            this.f10190Q.startLeScan(this.f10207Y0);
        }
        this.f10213b1 = true;
        CrashApp.m16337d("BluetoothLeService", "startScan " + this.f10211a1);
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m16217g() {
        if (Build.VERSION.SDK_INT < 21) {
            this.f10190Q.stopLeScan(this.f10207Y0);
        } else if (this.f10190Q.getBluetoothLeScanner() != null) {
            this.f10190Q.getBluetoothLeScanner().stopScan(this.f10209Z0);
        }
        this.f10213b1 = false;
        CrashApp.m16337d("BluetoothLeService", "stopScan " + this.f10211a1);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m16225h() {
        m16177c();
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public int m16247n() {
        if (!m16197d("getCurSportData")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 3;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: o */
    public int m16249o() {
        if (!m16197d("getDeviceBatery")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 11;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: p */
    public int m16251p() {
        if (!m16197d("getDeviceCode0")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        bArr[0] = 31;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public int m16253q() {
        if (!m16197d("getDeviceInfo2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 12;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: r */
    public int m16255r() {
        if (!m16197d("getDeviceRssi0")) {
            return this.f10242q0;
        }
        BluetoothGatt bluetoothGatt = this.f10192R;
        if (bluetoothGatt != null) {
            bluetoothGatt.readRemoteRssi();
        }
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: s */
    public List<BluetoothGattService> m16258s() {
        try {
            if (this.f10192R == null) {
                return null;
            }
            return this.f10192R.getServices();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: t */
    public void m16260t() {
        CrashApp.m16336c("BluetoothLeService", "service initCharacteristic.");
        boolean b = m16173b(SampleGattAttributes.f10262d);
        int i = 0;
        while (true) {
            String[] strArr = this.f10184L0;
            if (i >= strArr.length) {
                break;
            }
            m16173b(strArr[i]);
            i++;
        }
        if (!b) {
            CrashApp.m16335b("BluetoothLeService", "connect charactistic error.");
        }
    }

    /* renamed from: u */
    private void m16262u() {
        if (this.f10220f0 == null) {
            this.f10220f0 = new Handler();
        }
        if (f10171x1 == null) {
            f10171x1 = new Handler(getMainLooper());
        }
        if (this.f10180H0 == null) {
            this.f10180H0 = new Handler();
        }
    }

    /* renamed from: v */
    private boolean m16264v() {
        if (this.f10188P == null) {
            this.f10188P = (BluetoothManager) getSystemService("bluetooth");
            if (this.f10188P == null) {
                CrashApp.m16335b("BluetoothLeService", " Unable to initialize BluetoothManager.");
                return false;
            }
        }
        this.f10190Q = this.f10188P.getAdapter();
        BluetoothAdapter bluetoothAdapter = this.f10190Q;
        if (bluetoothAdapter == null) {
            CrashApp.m16335b("BluetoothLeService", " Unable to obtain a BluetoothAdapter.");
            return false;
        } else if (!bluetoothAdapter.isEnabled()) {
            return false;
        } else {
            CrashApp.m16335b("BluetoothLeService", "Initialize BluetoothManager.");
            return true;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public boolean m16266w() {
        int i = f10169v1;
        return (i == 0 || i == 1) ? false : true;
    }

    /* access modifiers changed from: private */
    /* renamed from: x */
    public boolean m16268x() {
        return this.f10194S;
    }

    /* renamed from: y */
    private static IntentFilter m16270y() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Common.f10357g);
        intentFilter.addAction(Common.f10358h);
        intentFilter.addAction(Common.f10359i);
        intentFilter.addAction(Common.f10361k);
        intentFilter.addAction(Common.f10363m);
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("ProgressUpdate");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        return intentFilter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: z */
    public void m16272z() {
        CrashApp.m16336c("BluetoothLeService", "ACTION_GATT_DISCONNECTED");
        m16204e(true);
        m16144a(0, true);
        this.f10212b0 = System.currentTimeMillis();
        this.f10210a0 = true;
        m16091B();
        CrashApp.m16336c("BluetoothLeService", "disconnected");
        mo26497b();
        if (f10170w1 == 129) {
            CrashApp.m16337d("BluetoothLeService", "----129----");
            try {
                Thread.sleep(AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.f10192R.connect();
        } else if (this.f10216d0 == 1 || !this.f10193R0 || this.f10187O0) {
            Handler handler = f10171x1;
            if (handler != null) {
                handler.removeCallbacks(f10172y1);
            }
            f10172y1 = new C4786p(this);
            f10171x1.postDelayed(f10172y1, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        }
        f10170w1 = 0;
    }

    /* renamed from: l */
    private String m16241l() {
        int i;
        if (this.f10201V0.size() != 0) {
            if (this.f10201V0.size() >= 9999) {
                this.f10201V0.remove(0);
                this.f10201V0.clear();
            } else {
                ArrayList<AncsItem> arrayList = this.f10201V0;
                i = Integer.valueOf(arrayList.get(arrayList.size() - 1).mo26499a()).intValue() + 1;
                return String.format(Locale.ENGLISH, "%1$04d", Integer.valueOf(i));
            }
        }
        i = 0;
        return String.format(Locale.ENGLISH, "%1$04d", Integer.valueOf(i));
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public int m16244m() {
        if (!m16197d("getBandFunction20")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 32;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m16231i(boolean z) {
        this.f10194S = z;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m16235j(boolean z) {
        this.f10187O0 = z;
        if (z) {
            m16204e(true);
            this.f10200V = new Thread(this.f10239o1);
            this.f10200V.start();
        } else if (this.f10200V != null) {
            CrashApp.m16337d("BluetoothLeService", "syncHistoryDataNow " + z);
            this.f10200V.interrupt();
            this.f10200V = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m16238k() {
        CrashApp.m16336c("BluetoothLeService", "closeConnection");
        m16204e(true);
        m16144a(0, true);
        this.f10212b0 = System.currentTimeMillis();
        this.f10210a0 = true;
        m16091B();
        CrashApp.m16336c("BluetoothLeService", "close");
        mo26495a();
        f10170w1 = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public int m16222h(boolean z) {
        if (!m16197d("setPhontMode")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 7;
        if (z) {
            bArr[1] = 1;
        } else {
            bArr[1] = 0;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
     arg types: [android.bluetooth.BluetoothGattCharacteristic, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void */
    /* renamed from: b */
    private boolean m16173b(String str) {
        Iterator<ArrayList<BluetoothGattCharacteristic>> it = this.f10198U.iterator();
        while (it.hasNext()) {
            Iterator it2 = it.next().iterator();
            while (true) {
                if (it2.hasNext()) {
                    BluetoothGattCharacteristic bluetoothGattCharacteristic = (BluetoothGattCharacteristic) it2.next();
                    if (str.equalsIgnoreCase(bluetoothGattCharacteristic.getUuid().toString())) {
                        m16146a(bluetoothGattCharacteristic, true);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public int m16214g(int i) {
        if (!m16197d("setHourFormat2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        bArr[0] = 29;
        bArr[1] = (byte) i;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo26497b() {
        try {
            CrashApp.m16335b("BluetoothLeService", "bluetooth gatt disconnect");
            m16093C();
            m16144a(0, false);
            if (this.f10192R == null) {
                CrashApp.m16337d("BluetoothLeService", "mBluetoothGatt is null");
            } else {
                this.f10192R.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public int m16206f(int i) {
        if (!m16197d("setGoal")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        bArr[0] = 26;
        byte[] a = ConvertUtil.m16333a(i);
        bArr[1] = a[0];
        bArr[2] = a[1];
        bArr[3] = a[2];
        bArr[4] = a[3];
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m16204e(boolean z) {
        this.f10189P0 = true;
        if (z && this.f10216d0 == 1) {
            CrashApp.m16336c("BluetoothLeService", "clear cmd list");
            this.f10191Q0.clear();
            this.f10219e1 = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public int m16216g(boolean z) {
        if (!m16197d("setIdleTime")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 5;
        bArr[1] = z ? (byte) 1 : 0;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* renamed from: b */
    private synchronized boolean m16174b(String str, String str2) {
        this.f10206Y = str;
        this.f10204X = str2;
        try {
            CrashApp.m16334a("BluetoothLeService", String.format("trying connect to auto [%1$s] ", str2));
            if (this.f10190Q != null) {
                if (str2 != null) {
                    BluetoothDevice remoteDevice = this.f10190Q.getRemoteDevice(str2);
                    if (remoteDevice == null) {
                        CrashApp.m16337d("BluetoothLeService", "Device not found.  Unable to connect.");
                        m16144a(0, false);
                        return false;
                    }
                    Iterator<BluetoothGatt> it = this.f10183K0.iterator();
                    while (it.hasNext()) {
                        BluetoothGatt next = it.next();
                        if (next != null) {
                            next.close();
                        }
                    }
                    this.f10183K0.clear();
                    this.f10192R = remoteDevice.connectGatt(this, true, this.f10179G0);
                    this.f10183K0.add(this.f10192R);
                    CrashApp.m16336c("BluetoothLeService", "Trying to create a new connection auto");
                    m16144a(1, false);
                    return true;
                }
            }
            m16144a(0, false);
            CrashApp.m16337d("BluetoothLeService", "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            m16144a(0, false);
            return false;
        }
    }

    /* renamed from: d */
    private boolean m16197d(String str) {
        CrashApp.m16336c("BluetoothLeService", "called RemoteService " + str);
        if (this.f10240p0 == 0 || this.f10242q0 == 200) {
            return true;
        }
        CrashApp.m16335b("BluetoothLeService", "called " + str + " error:" + this.f10242q0);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* renamed from: c */
    private synchronized boolean m16188c(String str, String str2) {
        this.f10206Y = str;
        this.f10204X = str2;
        try {
            CrashApp.m16336c("BluetoothLeService", String.format("trying connect to [%1$s].", str2));
            if (this.f10190Q != null) {
                if (str2 != null) {
                    BluetoothDevice remoteDevice = this.f10190Q.getRemoteDevice(str2);
                    if (remoteDevice == null) {
                        CrashApp.m16337d("BluetoothLeService", "Device not found.  Unable to connect.");
                        m16144a(0, false);
                        return false;
                    }
                    this.f10182J0 = new C4782l(this);
                    this.f10180H0.postDelayed(this.f10182J0, 15000);
                    Iterator<BluetoothGatt> it = this.f10183K0.iterator();
                    while (it.hasNext()) {
                        BluetoothGatt next = it.next();
                        if (next != null) {
                            next.close();
                        }
                    }
                    this.f10183K0.clear();
                    this.f10192R = remoteDevice.connectGatt(this, false, this.f10179G0);
                    this.f10183K0.add(this.f10192R);
                    CrashApp.m16336c("BluetoothLeService", "Trying to create a new connection.");
                    m16144a(1, false);
                    return true;
                }
            }
            m16144a(0, false);
            CrashApp.m16337d("BluetoothLeService", "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            m16144a(0, false);
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public boolean m16205e(String str) {
        String str2 = str;
        String[] strArr = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "0A", "0B", "0C", "0D", "0F", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F", "21", "23", "24", "25", "26", "27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F"};
        String[] strArr2 = {"c4", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fb", "fc", "fd", "fe", "ff"};
        if (Common.f10351a == 2) {
            for (String str3 : strArr2) {
                if (str2.equalsIgnoreCase(str3)) {
                    CrashApp.m16334a("BluetoothLeService", "process_cmd_runnable running has response type require session response " + str2);
                    return true;
                }
            }
        } else {
            for (String str4 : strArr) {
                if (str2.equalsIgnoreCase(str4)) {
                    CrashApp.m16334a("BluetoothLeService", "process_cmd_runnable running no response type require session response " + str2);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m16194d(boolean z) {
        this.f10195S0 = false;
        this.f10188P = null;
        this.f10190Q = null;
        this.f10192R = null;
        m16264v();
        String str = this.f10204X;
        if (str == null) {
            return;
        }
        if (z) {
            m16174b(this.f10206Y, str);
        } else {
            m16188c(this.f10206Y, str);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public int m16210f(boolean z) {
        if (!m16197d("setEcgMode0")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        bArr[0] = 42;
        if (z) {
            bArr[1] = 1;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26495a() {
        try {
            CrashApp.m16335b("BluetoothLeService", " bluetooth gatt close");
            m16093C();
            m16144a(0, false);
            if (this.f10192R == null) {
                CrashApp.m16337d("BluetoothLeService", " mBluetoothGatt is null");
                return;
            }
            this.f10192R.close();
            this.f10192R = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public int m16209f(String str) {
        if (!m16197d("setDeviceName0")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        int i = 0;
        bArr[0] = 48;
        byte[] bytes = str.getBytes();
        while (i < bytes.length && i < 11) {
            int i2 = i + 1;
            bArr[i2] = bytes[i];
            i = i2;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public int m16190d(int i) {
        if (!m16197d("getMultipleSportDataByDay")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        bArr[0] = 37;
        bArr[1] = (byte) i;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public int m16199e(int i) {
        if (!m16197d("sendVibrationSignal")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        if (i < 0 || i > 10) {
            i = 10;
        }
        bArr[0] = 4;
        bArr[1] = (byte) i;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16122a(BleClientOption bleClientOption) {
        if (this.f10238o0 != 200) {
            Log.e("BluetoothLeService", "called setOption error:" + this.f10238o0);
            return this.f10238o0;
        }
        this.f10244r0 = bleClientOption;
        CrashApp.m16336c("BluetoothLeService", "setOption");
        BleClientOption bleClientOption2 = this.f10244r0;
        if (bleClientOption2 != null) {
            if (bleClientOption2.mo26383a() != null) {
                this.f10246s0 = this.f10244r0.mo26383a().mo26407e();
                this.f10248t0 = this.f10244r0.mo26383a().mo26409g();
                this.f10250u0 = this.f10244r0.mo26383a().mo26408f();
                this.f10252v0 = this.f10244r0.mo26383a().mo26401c();
                this.f10253w0 = this.f10244r0.mo26383a().mo26395a();
                this.f10254x0 = this.f10244r0.mo26383a().mo26404d();
                this.f10255y0 = this.f10244r0.mo26383a().mo26398b();
            }
            if (this.f10244r0.mo26385c() != null) {
                this.f10173A0 = this.f10244r0.mo26385c().mo26417c();
                this.f10174B0 = this.f10244r0.mo26385c().mo26423f();
                this.f10244r0.mo26385c().mo26419d();
                this.f10175C0 = this.f10244r0.mo26385c().mo26422e();
                this.f10256z0 = this.f10244r0.mo26385c().mo26415b();
                this.f10176D0 = this.f10244r0.mo26385c().mo26413a();
            }
            if (this.f10244r0.mo26384b() != null && this.f10244r0.mo26384b().size() > 0) {
                this.f10178F0 = this.f10244r0.mo26384b();
            }
            if (this.f10244r0.mo26386d() != null) {
                this.f10177E0 = this.f10244r0.mo26386d();
            }
        }
        return this.f10238o0;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m16175b(byte[] bArr) {
        boolean z;
        boolean z2;
        boolean z3;
        byte[] bArr2 = bArr;
        String hexString = Integer.toHexString(bArr2[0] & 255);
        if (this.f10216d0 != 1) {
            String[] strArr = {"F1", "85"};
            int i = 0;
            while (true) {
                if (i >= strArr.length) {
                    z3 = false;
                    break;
                } else if (hexString.equalsIgnoreCase(strArr[i])) {
                    z3 = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!m16266w() && !z3) {
                CrashApp.m16335b("BluetoothLeService", "writeBleCmd : connect state is false, drop cmd " + SysUtils.m16360a(bArr));
                return false;
            }
        } else if (!m16266w()) {
            CrashApp.m16335b("BluetoothLeService", "writeBleCmd : connect state is false, drop cmd " + SysUtils.m16360a(bArr));
            return false;
        }
        if (this.f10217d1 && this.f10216d0 == 1) {
            String[] strArr2 = {"c6"};
            int i2 = 0;
            while (true) {
                if (i2 >= strArr2.length) {
                    z2 = true;
                    break;
                } else if (hexString.equalsIgnoreCase(strArr2[i2])) {
                    z2 = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                CrashApp.m16336c("BluetoothLeService", "writeBleCmd : silence mode, not allow cmd " + SysUtils.m16360a(bArr));
                return false;
            }
        }
        String[] strArr3 = {"a1", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fb", "fc", "fd", "fe", "ff"};
        if (this.f10187O0) {
            int i3 = 0;
            while (true) {
                if (i3 >= strArr3.length) {
                    z = true;
                    break;
                } else if (hexString.equalsIgnoreCase(strArr3[i3])) {
                    z = false;
                    break;
                } else {
                    i3++;
                }
            }
            if (!z) {
                CrashApp.m16336c("BluetoothLeService", "writeBleCmd : sync history mode, not allow cmd " + SysUtils.m16360a(bArr));
                return false;
            }
        }
        this.f10191Q0.add(bArr2);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m16187c(byte[] bArr) {
        m16231i(false);
        try {
            if (this.f10192R == null) {
                CrashApp.m16337d("BluetoothLeService", "mBluetoothGatt null");
                return;
            }
            BluetoothGattService bluetoothGattService = this.f10181I0.get(SampleGattAttributes.f10263e);
            if (bluetoothGattService == null) {
                CrashApp.m16337d("BluetoothLeService", "gap_service null");
                return;
            }
            BluetoothGattCharacteristic characteristic = bluetoothGattService.getCharacteristic(UUID.fromString(SampleGattAttributes.f10263e));
            if (characteristic == null) {
                CrashApp.m16337d("BluetoothLeService", "dev_name null");
                return;
            }
            characteristic.setValue(bArr);
            boolean writeCharacteristic = this.f10192R.writeCharacteristic(characteristic);
            StringBuilder sb = new StringBuilder();
            sb.append("writeCharacteristic ");
            sb.append(writeCharacteristic);
            CrashApp.m16336c("BluetoothLeService", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            CrashApp.m16337d("BluetoothLeService", "writeCharacteristic exception");
            m16231i(true);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(java.lang.String[], boolean):void
     arg types: [java.lang.String[], int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(java.lang.String, java.lang.String):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.b(java.lang.String[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.aidl.b.a(int, long):void
     arg types: [int, int]
     candidates:
      com.sxr.sdk.ble.keepfit.aidl.b.a(int, int):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(int, java.lang.String):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(int, int[]):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(int, boolean[]):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(long, int):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(java.lang.String, byte[]):void
      com.sxr.sdk.ble.keepfit.aidl.b.a(int, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
     arg types: [java.lang.String[], int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m16186c(String str) {
        boolean z;
        Exception exc;
        String str2 = str;
        if (str2 != null) {
            try {
                String[] split = str2.split(" ");
                String str3 = split.length > 1 ? split[0] : "";
                StringBuilder sb = new StringBuilder();
                sb.append("get cmd : ");
                sb.append(str2);
                CrashApp.m16336c("BluetoothLeService", sb.toString());
                if (split.length >= 20) {
                    if (str3.equalsIgnoreCase("01")) {
                        m16189d();
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22947t(1);
                        }
                    } else if (str3.equalsIgnoreCase("81")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22947t(0);
                        }
                    } else if (str3.equalsIgnoreCase("02")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetUserInfo: success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22904B(1);
                        }
                    } else if (str3.equalsIgnoreCase("82")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetUserInfo: failed");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22904B(0);
                        }
                    } else if (str3.equalsIgnoreCase("03")) {
                        CrashApp.m16336c("BluetoothLeService", "ongetcursportdata: success");
                        long parseInt = (long) (Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000));
                        int parseInt2 = Integer.parseInt(split[8] + split[7] + split[6] + split[5], 16);
                        int parseInt3 = Integer.parseInt(split[12] + split[11] + split[10] + split[9], 16);
                        int parseInt4 = Integer.parseInt(split[16] + split[15] + split[14] + split[13], 16);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(split[19]);
                        sb2.append(split[18]);
                        sb2.append(split[17]);
                        int parseInt5 = Integer.parseInt(sb2.toString(), 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22914a(0, parseInt, parseInt2, parseInt3, parseInt4, parseInt5, 0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("13")) {
                        CrashApp.m16336c("BluetoothLeService", "ongetcursportdata: success");
                        int parseInt6 = Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000);
                        int parseInt7 = Integer.parseInt(split[8] + split[7] + split[6] + split[5], 16);
                        int parseInt8 = Integer.parseInt(split[12] + split[11] + split[10] + split[9], 16);
                        int parseInt9 = Integer.parseInt(split[16] + split[15] + split[14] + split[13], 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22914a(1, (long) parseInt6, parseInt7, 0, 0, 0, parseInt8, parseInt9);
                        }
                    } else if (str3.equalsIgnoreCase("83")) {
                        CrashApp.m16336c("BluetoothLeService", "ongetcursportdata: failed");
                    } else if (str3.equalsIgnoreCase("04")) {
                        CrashApp.m16336c("BluetoothLeService", "sendVibrationSignal: success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22940k(1);
                        }
                    } else if (str3.equalsIgnoreCase("84")) {
                        CrashApp.m16336c("BluetoothLeService", "sendVibrationSignal: failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            try {
                                this.f10228j0.mo22940k(0);
                            } catch (Exception e) {
                                exc = e;
                            }
                        }
                    } else if (str3.equalsIgnoreCase("06")) {
                        CrashApp.m16336c("BluetoothLeService", "onGetDeviceAction ");
                        int parseInt10 = Integer.parseInt(split[1], 16);
                        if (parseInt10 == 1) {
                            if (this.f10228j0 != null) {
                                this.f10228j0.mo22909a(1);
                            }
                        } else if (parseInt10 == 2) {
                            if (this.f10228j0 != null) {
                                this.f10228j0.mo22909a(2);
                            }
                        } else if (parseInt10 == 4 && this.f10228j0 != null) {
                            this.f10228j0.mo22909a(4);
                        }
                    } else if (str3.equalsIgnoreCase("07")) {
                        CrashApp.m16336c("BluetoothLeService", "setphotomode success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22945r(1);
                        }
                    } else if (str3.equalsIgnoreCase("87")) {
                        CrashApp.m16336c("BluetoothLeService", "setphotomode failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22945r(0);
                        }
                    } else if (str3.equalsIgnoreCase("08")) {
                        CrashApp.m16336c("BluetoothLeService", "setidletime success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22937f(1);
                        }
                    } else if (str3.equalsIgnoreCase("88")) {
                        CrashApp.m16336c("BluetoothLeService", "setidletime failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22937f(0);
                        }
                    } else if (str3.equalsIgnoreCase("09")) {
                        CrashApp.m16336c("BluetoothLeService", "setsleeptime success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22908H(1);
                        }
                    } else if (str3.equalsIgnoreCase("89")) {
                        CrashApp.m16336c("BluetoothLeService", "setsleeptime failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22908H(0);
                        }
                    } else if (str3.equalsIgnoreCase("0B")) {
                        CrashApp.m16336c("BluetoothLeService", "read battery success");
                        int parseInt11 = Integer.parseInt(split[1], 16);
                        int parseInt12 = Integer.parseInt(split[2], 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22910a(parseInt11, parseInt12);
                        }
                    } else if (str3.equalsIgnoreCase("8B")) {
                        CrashApp.m16336c("BluetoothLeService", "read battery failed");
                    } else if (str3.equalsIgnoreCase("0C")) {
                        CrashApp.m16336c("BluetoothLeService", "read deviceinfo success");
                        int parseInt13 = Integer.parseInt(split[2] + split[1], 16);
                        String str4 = split[3] + split[4] + split[5] + split[6] + split[7] + split[8];
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("mac ");
                        sb3.append(str4);
                        CrashApp.m16336c("BluetoothLeService", sb3.toString());
                        String str5 = split[10] + split[9];
                        String str6 = split[12] + split[11];
                        String str7 = split[19] + split[18] + split[17] + split[16];
                        long a = CrcUtil.m16338a(1247391573, new long[]{Long.parseLong(split[1], 16), Long.parseLong(split[2], 16), Long.parseLong(split[3], 16), Long.parseLong(split[4], 16), Long.parseLong(split[5], 16), Long.parseLong(split[6], 16), Long.parseLong(split[7], 16), Long.parseLong(split[8], 16), Long.parseLong(split[9], 16), Long.parseLong(split[10], 16), Long.parseLong(split[11], 16), Long.parseLong(split[12], 16), Long.parseLong(split[13], 16), Long.parseLong(split[14], 16), Long.parseLong(split[15], 16)});
                        boolean z2 = a == Long.valueOf(str7, 16).longValue();
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("cal crc ");
                        sb4.append(a);
                        sb4.append(" return crc ");
                        sb4.append(Long.valueOf(str7, 16));
                        sb4.append(" result ");
                        sb4.append(z2);
                        Log.i("BluetoothLeService", sb4.toString());
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22917a(parseInt13, str4, str5, str6, z2 ? 1 : 0);
                            if (this.f10241p1) {
                                this.f10243q1.mo26580a(str4, str5 + str6, parseInt13);
                                z = false;
                                this.f10241p1 = false;
                                this.f10243q1.f10383d = parseInt13 == 100;
                            }
                        }
                    } else if (str3.equalsIgnoreCase("8C")) {
                        CrashApp.m16336c("BluetoothLeService", "read battery failed");
                    } else if (str3.equalsIgnoreCase("0D")) {
                        CrashApp.m16336c("BluetoothLeService", "set alarm id success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22934d(1);
                        }
                    } else if (str3.equalsIgnoreCase("8D")) {
                        CrashApp.m16336c("BluetoothLeService", "set alarm id failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22934d(0);
                        }
                    } else if (str3.equalsIgnoreCase("1C")) {
                        CrashApp.m16336c("BluetoothLeService", "set alarm name success");
                    } else if (str3.equalsIgnoreCase("9C")) {
                        CrashApp.m16336c("BluetoothLeService", "set alarm name failed");
                    } else if (str3.equalsIgnoreCase("0E")) {
                        CrashApp.m16336c("BluetoothLeService", "setdevicemode success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22944q(1);
                        }
                    } else if (str3.equalsIgnoreCase("8E")) {
                        CrashApp.m16336c("BluetoothLeService", "setdevicemode failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22944q(0);
                        }
                    } else if (str3.equalsIgnoreCase("10")) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("getdayabyday success ");
                        sb5.append(this.f10237n1);
                        CrashApp.m16336c("BluetoothLeService", sb5.toString());
                        long parseInt14 = (long) (Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000));
                        Date date = new Date(1000 * parseInt14);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        simpleDateFormat.format(date);
                        for (int i = 0; i < 15; i++) {
                            if (this.f10228j0 != null) {
                                int i2 = i + 5;
                                this.f10228j0.mo22913a(1, parseInt14, Integer.parseInt(split[i2], 16), 0);
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append("getdayabyday sport ");
                                sb6.append(parseInt14);
                                sb6.append(" ");
                                sb6.append(simpleDateFormat.format(new Date(1000 * parseInt14)));
                                sb6.append(" step ");
                                sb6.append(Integer.parseInt(split[i2], 16));
                                CrashApp.m16336c("BluetoothLeService", sb6.toString());
                            }
                            parseInt14 += 60;
                        }
                        m16172b(split, true);
                    } else if (str3.equalsIgnoreCase("11")) {
                        CrashApp.m16336c("BluetoothLeService", "getdayabyday success");
                        long parseInt15 = (long) (Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000));
                        Date date2 = new Date(1000 * parseInt15);
                        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        simpleDateFormat2.format(date2);
                        for (int i3 = 0; i3 < 15; i3++) {
                            if (this.f10228j0 != null) {
                                int i4 = i3 + 5;
                                this.f10228j0.mo22913a(2, parseInt15, Integer.parseInt(split[i4], 16), 0);
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append("getdayabyday sleep ");
                                sb7.append(simpleDateFormat2.format(new Date(1000 * parseInt15)));
                                sb7.append(" step ");
                                sb7.append(Integer.parseInt(split[i4], 16));
                                CrashApp.m16336c("BluetoothLeService", sb7.toString());
                            }
                            parseInt15 += 60;
                        }
                        m16172b(split, true);
                    } else if (str3.equalsIgnoreCase("90")) {
                        CrashApp.m16336c("BluetoothLeService", "getdayabyday failed");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22912a(0, 0L);
                        }
                    } else if (str3.equalsIgnoreCase("14")) {
                        long parseInt16 = (long) (Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000));
                        int parseInt17 = Integer.parseInt(split[5], 16);
                        int parseInt18 = Integer.parseInt(split[6], 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22928b(1, parseInt16, parseInt17, parseInt18);
                        }
                    } else if (str3.equalsIgnoreCase("94")) {
                        CrashApp.m16336c("BluetoothLeService", "open hearttest failed");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22928b(0, 0, 0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("15")) {
                        CrashApp.m16336c("BluetoothLeService", "close hearttest success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22928b(1, 0, 0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("95")) {
                        CrashApp.m16336c("BluetoothLeService", "close hearttest failed");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22928b(0, 0, 0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("16")) {
                        CrashApp.m16336c("BluetoothLeService", "getheartdata detail success");
                        if (split[1].equalsIgnoreCase("FF")) {
                            if (this.f10228j0 != null) {
                                this.f10228j0.mo22912a(3, 0L);
                            }
                        } else if (split[1].equalsIgnoreCase("F0")) {
                            this.f10231k1 = Integer.parseInt(split[7] + split[6], 16);
                        } else if (split[1].equalsIgnoreCase("AA")) {
                            this.f10233l1 = Integer.parseInt(split[2], 16);
                            this.f10235m1 = Integer.parseInt(split[8] + split[7], 16);
                        } else if (split[1].equalsIgnoreCase("A0")) {
                            long parseInt19 = (long) (Integer.parseInt(split[5] + split[4] + split[3] + split[2], 16) - (this.f10237n1 / 1000));
                            int round = Math.round(((float) (((((Integer.parseInt(split[8], 16) + Integer.parseInt(split[9], 16)) + Integer.parseInt(split[10], 16)) + Integer.parseInt(split[11], 16)) + Integer.parseInt(split[12], 16)) + Integer.parseInt(split[13], 16))) / 6.0f);
                            if (this.f10228j0 != null) {
                                this.f10228j0.mo22913a(3, parseInt19, 0, round);
                            }
                            long j = parseInt19 + 60;
                            int round2 = Math.round(((float) (((((Integer.parseInt(split[14], 16) + Integer.parseInt(split[15], 16)) + Integer.parseInt(split[16], 16)) + Integer.parseInt(split[17], 16)) + Integer.parseInt(split[18], 16)) + Integer.parseInt(split[19], 16))) / 6.0f);
                            if (this.f10228j0 != null) {
                                this.f10228j0.mo22913a(3, j, 0, round2);
                            }
                            int parseInt20 = Integer.parseInt(split[7] + split[6], 16) + 1;
                            if (this.f10233l1 == this.f10231k1 && this.f10235m1 == parseInt20 && this.f10228j0 != null) {
                                this.f10228j0.mo22912a(3, j);
                            }
                        }
                    } else if (str3.equalsIgnoreCase("96")) {
                        CrashApp.m16336c("BluetoothLeService", "getheartdata detail failed");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22912a(3, 0L);
                        }
                    } else if (str3.equalsIgnoreCase("19")) {
                        CrashApp.m16336c("BluetoothLeService", "autohearttest success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22938h(1);
                        }
                    } else if (str3.equalsIgnoreCase("99")) {
                        CrashApp.m16336c("BluetoothLeService", "autohearttest failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22938h(0);
                        }
                    } else if (str3.equalsIgnoreCase("1B")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22932c(1);
                        }
                    } else if (str3.equalsIgnoreCase("9B")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22932c(0);
                        }
                    } else if (str3.equalsIgnoreCase("1D")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetHourFormat success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22939j(1);
                        }
                    } else if (str3.equalsIgnoreCase("21")) {
                        CrashApp.m16336c("BluetoothLeService", "setLanguage success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22905D(1);
                        }
                    } else if (str3.equalsIgnoreCase("A1")) {
                        CrashApp.m16336c("BluetoothLeService", "setLanguage failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22905D(0);
                        }
                    } else if (str3.equalsIgnoreCase("9D")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetHourFormat failed");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22939j(0);
                        }
                    } else if (str3.equalsIgnoreCase("1F")) {
                        CrashApp.m16336c("BluetoothLeService", "onGetDeviceCode success");
                        if (this.f10228j0 != null) {
                            byte[] bArr = new byte[18];
                            int i5 = 1;
                            while (i5 < split.length - 1 && i5 < 19) {
                                bArr[i5 - 1] = (byte) Integer.parseInt(split[i5], 16);
                                i5++;
                            }
                            this.f10228j0.mo22924a(bArr);
                        }
                    } else if (str3.equalsIgnoreCase("9F")) {
                        CrashApp.m16336c("BluetoothLeService", "onGetDeviceCode failure");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22924a(new byte[18]);
                        }
                    } else if (str3.equalsIgnoreCase("1E")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetDeviceCode success");
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22936e(1);
                        }
                    } else if (str3.equalsIgnoreCase("9E")) {
                        CrashApp.m16336c("BluetoothLeService", "onSetDeviceCode failure");
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22936e(0);
                        }
                    } else if (str3.equalsIgnoreCase("12")) {
                        if (this.f10203W0 == Integer.parseInt(split[2], 16) && this.f10228j0 != null) {
                            this.f10228j0.mo22906E(1);
                        }
                    } else if (str3.equalsIgnoreCase("92")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22906E(0);
                        }
                    } else if (str3.equalsIgnoreCase("20")) {
                        int parseInt21 = Integer.parseInt(split[1], 16);
                        int parseInt22 = Integer.parseInt(split[2], 16);
                        int parseInt23 = Integer.parseInt(split[3], 16);
                        int parseInt24 = Integer.parseInt(split[4], 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22919a(1, SysUtils.m16365a(SysUtils.m16364a(parseInt21), SysUtils.m16364a(parseInt22), SysUtils.m16364a(parseInt23), SysUtils.m16364a(parseInt24)));
                        }
                    } else if (str3.equalsIgnoreCase("A0")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22919a(0, new boolean[0]);
                        }
                    } else if (str3.equalsIgnoreCase("22")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22909a(5);
                        }
                    } else if (str3.equalsIgnoreCase("05")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22946s(1);
                        }
                    } else if (str3.equalsIgnoreCase("85")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22946s(0);
                        }
                    } else if (str3.equalsIgnoreCase("23")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22925b(1);
                        }
                    } else if (str3.equalsIgnoreCase("A3")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22925b(0);
                        }
                    } else if (str3.equalsIgnoreCase("24")) {
                        int parseInt25 = Integer.parseInt(split[1], 16);
                        int parseInt26 = Integer.parseInt(split[2], 16);
                        int parseInt27 = Integer.parseInt(split[3], 16);
                        int parseInt28 = Integer.parseInt(split[4], 16);
                        int parseInt29 = Integer.parseInt(split[5], 16);
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22927b(parseInt25, parseInt26, parseInt27, parseInt28, parseInt29);
                        }
                    } else if (str3.equalsIgnoreCase("25")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22925b(1);
                            long parseInt30 = (long) (Integer.parseInt(split[4] + split[3] + split[2] + split[1], 16) - (this.f10237n1 / 1000));
                            SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            int i6 = 0;
                            while (i6 < 6) {
                                long j2 = parseInt30;
                                String format = simpleDateFormat3.format(new Date((1000 * parseInt30) + ((long) (i6 * 60 * 1000))));
                                int i7 = i6 * 2;
                                int parseInt31 = Integer.parseInt(split[i7 + 5], 16);
                                int i8 = parseInt31 & 15;
                                int parseInt32 = (Integer.parseInt(split[i7 + 6], 16) << 4) + (parseInt31 >> 4);
                                if (this.f10228j0 != null) {
                                    this.f10228j0.mo22916a(1, format, i8, parseInt32);
                                }
                                i6++;
                                parseInt30 = j2;
                            }
                            m16158a(split, true);
                        }
                    } else if (str3.equalsIgnoreCase("A5")) {
                        if (split[1].equalsIgnoreCase("FF") && this.f10228j0 != null) {
                            this.f10228j0.mo22916a(0, "", 0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("1A")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22949v(1);
                        }
                    } else if (str3.equalsIgnoreCase("9A")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22949v(0);
                        }
                    } else if (str3.equalsIgnoreCase("26")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22941m(1);
                        }
                    } else if (str3.equalsIgnoreCase("A6")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22941m(0);
                        }
                    } else if (str3.equalsIgnoreCase("27")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22935d(1, 0);
                        }
                    } else if (str3.equalsIgnoreCase("28")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22935d(2, 0);
                        }
                    } else if (str3.equalsIgnoreCase("29")) {
                        if (this.f10228j0 != null) {
                            int parseInt33 = Integer.parseInt(split[1], 16);
                            this.f10228j0.mo22929b(parseInt33, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date(((long) (Integer.parseInt(split[5] + split[4] + split[3] + split[2], 16) - (this.f10237n1 / 1000))) * 1000)), Integer.parseInt(split[9] + split[8] + split[7] + split[6], 16), Integer.parseInt(split[13] + split[12] + split[11] + split[10], 16));
                        }
                    } else if (str3.equalsIgnoreCase("2a")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22926b(1, Integer.parseInt(split[1], 16));
                        }
                    } else if (str3.equalsIgnoreCase("9a")) {
                        if (this.f10228j0 != null) {
                            z = false;
                            this.f10228j0.mo22926b(0, 0);
                        }
                    } else if (str3.equalsIgnoreCase("2b")) {
                        if (this.f10228j0 != null) {
                            int parseInt34 = Integer.parseInt(split[1], 16);
                            int[] iArr = new int[12];
                            for (int i9 = 0; i9 < 6; i9++) {
                                int i10 = i9 * 3;
                                int parseInt35 = Integer.parseInt(split[i10 + 2], 16);
                                int parseInt36 = Integer.parseInt(split[i10 + 3], 16);
                                int parseInt37 = ((parseInt36 & PsExtractor.VIDEO_STREAM_MASK) >> 4) + (Integer.parseInt(split[i10 + 4], 16) << 4);
                                int i11 = i9 * 2;
                                iArr[i11] = parseInt35 + ((parseInt36 & 15) << 8);
                                iArr[i11 + 1] = parseInt37;
                            }
                            this.f10228j0.mo22918a(parseInt34, iArr);
                        }
                    } else if (str3.equalsIgnoreCase("2c")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22920a(Long.parseLong(split[4] + split[3] + split[2] + split[1], 16) - ((long) (TimeZone.getDefault().getRawOffset() / 1000)), Integer.parseInt(split[5], 16));
                        }
                    } else if (str3.equalsIgnoreCase("2d")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22911a(Integer.parseInt(split[1], 16), Integer.parseInt(split[2], 16), Long.parseLong(split[6] + split[5] + split[4] + split[3], 16) - ((long) (TimeZone.getDefault().getRawOffset() / 1000)));
                        }
                    } else if (str3.equalsIgnoreCase("2e")) {
                        if (this.f10228j0 != null) {
                            int parseInt38 = Integer.parseInt(split[1], 16);
                            int[] iArr2 = new int[12];
                            for (int i12 = 0; i12 < 6; i12++) {
                                int i13 = i12 * 3;
                                int parseInt39 = Integer.parseInt(split[i13 + 2], 16);
                                int parseInt40 = Integer.parseInt(split[i13 + 3], 16);
                                int parseInt41 = ((parseInt40 & PsExtractor.VIDEO_STREAM_MASK) >> 4) + (Integer.parseInt(split[i13 + 4], 16) << 4);
                                int i14 = i12 * 2;
                                iArr2[i14] = parseInt39 + ((parseInt40 & 15) << 8);
                                iArr2[i14 + 1] = parseInt41;
                            }
                            this.f10228j0.mo22930b(parseInt38, iArr2);
                        }
                    } else if (str3.equalsIgnoreCase("30")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22942o(1);
                        }
                    } else if (str3.equalsIgnoreCase("31")) {
                        if (this.f10228j0 != null) {
                            this.f10228j0.mo22943p(1);
                        }
                    } else if (!str3.equalsIgnoreCase("32")) {
                        Log.i("BluetoothLeService", "unknown command");
                        return;
                    } else if (this.f10228j0 != null) {
                        this.f10228j0.mo22903A(1);
                    }
                    m16204e(false);
                    return;
                }
                return;
            } catch (Exception e2) {
                exc = e2;
                z = false;
            }
        } else {
            return;
        }
        m16204e(z);
        exc.printStackTrace();
        PrintWriter printWriter = new PrintWriter(new StringWriter());
        exc.printStackTrace(printWriter);
        printWriter.close();
        CrashApp.m16335b("BluetoothLeService", "read exception: " + printWriter.toString());
        if (this.f10187O0) {
            m16235j(false);
            m16235j(true);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public byte[] m16164a(byte[] bArr, int i) {
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = i - 1;
            if (i2 < i4) {
                i3 += bArr[i2];
                i2++;
            } else {
                bArr[i4] = (byte) i3;
                return bArr;
            }
        }
    }

    /* renamed from: a */
    private int m16121a(AlarmInfoItem alarmInfoItem) {
        if (!m16197d("setNormalAlarm0")) {
            return this.f10242q0;
        }
        alarmInfoItem.mo26354a();
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 13;
        bArr[1] = (byte) ((int) alarmInfoItem.mo26354a());
        bArr[2] = (byte) alarmInfoItem.mo26373i();
        bArr[3] = (byte) alarmInfoItem.mo26377k();
        bArr[4] = (byte) alarmInfoItem.mo26378l();
        bArr[5] = (byte) alarmInfoItem.mo26367f();
        bArr[6] = (byte) alarmInfoItem.mo26362d();
        bArr[7] = (byte) alarmInfoItem.mo26371h();
        bArr[8] = (byte) alarmInfoItem.mo26375j();
        bArr[9] = (byte) alarmInfoItem.mo26369g();
        bArr[10] = (byte) alarmInfoItem.mo26360c();
        bArr[11] = (byte) alarmInfoItem.mo26365e();
        if (alarmInfoItem.mo26379m()) {
            bArr[12] = 1;
        } else {
            bArr[12] = 0;
        }
        m16175b(bArr);
        try {
            byte[] bytes = alarmInfoItem.mo26358b().getBytes(C1750C.UTF8_NAME);
            byte[] bArr2 = new byte[20];
            for (int i2 = 0; i2 < 20; i2++) {
                bArr2[i2] = 0;
            }
            bArr2[0] = 28;
            bArr2[1] = (byte) ((int) alarmInfoItem.mo26354a());
            int i3 = 18;
            if (bytes.length <= 18) {
                i3 = bytes.length;
            }
            System.arraycopy(bytes, 0, bArr2, 2, i3);
            m16175b(bArr2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public int m16168b(boolean z) {
        if (!m16197d("getOtaInfo")) {
            return this.f10242q0;
        }
        this.f10241p1 = true;
        this.f10243q1.f10382c = z;
        m16253q();
        return this.f10242q0;
    }

    /* renamed from: b */
    private void m16172b(String[] strArr, boolean z) {
        Runnable runnable = this.f10251u1;
        if (runnable != null) {
            this.f10247s1.removeCallbacks(runnable);
        }
        this.f10251u1 = new C4777g(this, strArr);
        if (z) {
            this.f10247s1.postDelayed(this.f10251u1, (long) this.f10245r1);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo26494a(int i) {
        int i2 = i;
        if (!m16197d("setDeviceMode2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i3 = 0; i3 < 20; i3++) {
            bArr[i3] = 0;
        }
        bArr[0] = 14;
        if (i2 == 1) {
            bArr[1] = 18;
            bArr[2] = 52;
            bArr[3] = 86;
            bArr[4] = 120;
            bArr[5] = -2;
            bArr[6] = -36;
            bArr[7] = -70;
            bArr[8] = -104;
        } else if (i2 == 2) {
            bArr[1] = -2;
            bArr[2] = -36;
            bArr[3] = -70;
            bArr[4] = -104;
            bArr[5] = 118;
            bArr[6] = 84;
            bArr[7] = 50;
            bArr[8] = 16;
        } else if (i2 == 3) {
            bArr[1] = 18;
            bArr[2] = 52;
            bArr[3] = 86;
            bArr[4] = 120;
            bArr[5] = -102;
            bArr[6] = -68;
            bArr[7] = -34;
            bArr[8] = -16;
        } else if (i2 == 4) {
            bArr[1] = 18;
            bArr[2] = 52;
            bArr[3] = 18;
            bArr[4] = 52;
            bArr[5] = 18;
            bArr[6] = 52;
            bArr[7] = 18;
            bArr[8] = 52;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16144a(int i, boolean z) {
        f10169v1 = i;
        try {
            if (this.f10228j0 != null) {
                this.f10228j0.mo22952z(i);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m16162a(List<BluetoothGattService> list) {
        try {
            CrashApp.m16334a("BluetoothLeService", String.format("discoveryServiceState [%1$s]", Integer.valueOf(this.f10218e0)));
            if (list == null) {
                return false;
            }
            this.f10198U = new ArrayList<>();
            for (BluetoothGattService bluetoothGattService : list) {
                List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
                ArrayList arrayList = new ArrayList();
                for (BluetoothGattCharacteristic bluetoothGattCharacteristic : characteristics) {
                    arrayList.add(bluetoothGattCharacteristic);
                    String uuid = bluetoothGattCharacteristic.getUuid().toString();
                    if (uuid.equalsIgnoreCase(SampleGattAttributes.f10263e)) {
                        this.f10181I0.put(uuid, bluetoothGattService);
                    }
                    for (int i = 0; i < this.f10185M0.length; i++) {
                        if (uuid.equalsIgnoreCase(this.f10185M0[i])) {
                            this.f10181I0.put(uuid, bluetoothGattService);
                            StringBuilder sb = new StringBuilder();
                            sb.append("hmBgs ");
                            sb.append(uuid);
                            CrashApp.m16336c("BluetoothLeService", sb.toString());
                        }
                    }
                }
                this.f10198U.add(arrayList);
            }
            Iterator<ArrayList<BluetoothGattCharacteristic>> it = this.f10198U.iterator();
            boolean z = false;
            boolean z2 = false;
            while (it.hasNext()) {
                Iterator it2 = it.next().iterator();
                while (it2.hasNext()) {
                    String uuid2 = ((BluetoothGattCharacteristic) it2.next()).getUuid().toString();
                    CrashApp.m16336c("BluetoothLeService", String.format("find characteristic [%1$s]", uuid2));
                    if (SampleGattAttributes.f10262d.equalsIgnoreCase(uuid2)) {
                        z = true;
                    }
                    if (SampleGattAttributes.f10263e.equalsIgnoreCase(uuid2)) {
                        z2 = true;
                    }
                }
            }
            CrashApp.m16337d("BluetoothLeService", String.format("displayGattServices: find battery[%1$b], read[%2$b], write[%3$b]", false, Boolean.valueOf(z), Boolean.valueOf(z2)));
            if (z && z2) {
                m16091B();
                m16093C();
                CrashApp.m16336c("BluetoothLeService", String.format("changed discovery state[%1$d] connectState[%2$d] ", Integer.valueOf(this.f10218e0), Integer.valueOf(f10169v1)));
                this.f10214c0 = false;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("enter silence mode ");
                sb2.append(this.f10217d1);
                sb2.append(" mUserDisconnected ");
                sb2.append(this.f10214c0);
                CrashApp.m16336c("BluetoothLeService", sb2.toString());
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    private void m16146a(BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z) {
        try {
            if (this.f10190Q != null) {
                if (this.f10192R != null) {
                    CrashApp.m16336c("BluetoothLeService", String.format("setCharacteristicNotification [%s] enable [%s] result [%s]", bluetoothGattCharacteristic.getUuid().toString(), String.valueOf(z), String.valueOf(this.f10192R.setCharacteristicNotification(bluetoothGattCharacteristic, z))));
                    BluetoothGattDescriptor descriptor = bluetoothGattCharacteristic.getDescriptor(UUID.fromString(SampleGattAttributes.f10261c));
                    if (descriptor != null) {
                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        this.f10192R.writeDescriptor(descriptor);
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("descriptor ");
                    sb.append(descriptor);
                    CrashApp.m16336c("BluetoothLeService", sb.toString());
                    return;
                }
            }
            CrashApp.m16335b("BluetoothLeService", "BluetoothAdapter not initialized");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16141a(String[] strArr, String[] strArr2, boolean z) {
        CrashApp.m16336c("BluetoothLeService", "setUuid0 " + z);
        this.f10184L0 = strArr;
        this.f10185M0 = strArr2;
        this.f10186N0 = z;
        return this.f10238o0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16136a(String str, byte[] bArr) {
        try {
            if (this.f10192R == null) {
                CrashApp.m16337d("BluetoothLeService", "mBluetoothGatt null");
                return 0;
            }
            BluetoothGattService bluetoothGattService = this.f10181I0.get(str);
            if (bluetoothGattService == null) {
                CrashApp.m16337d("BluetoothLeService", "gap_service null");
                return 1;
            }
            BluetoothGattCharacteristic characteristic = bluetoothGattService.getCharacteristic(UUID.fromString(str));
            if (characteristic == null) {
                CrashApp.m16337d("BluetoothLeService", "dev_name null");
                return 2;
            }
            characteristic.setValue(bArr);
            boolean writeCharacteristic = this.f10192R.writeCharacteristic(characteristic);
            StringBuilder sb = new StringBuilder();
            sb.append("writeCharacteristic0");
            sb.append(writeCharacteristic);
            CrashApp.m16336c("BluetoothLeService", sb.toString());
            return this.f10238o0;
        } catch (Exception e) {
            e.printStackTrace();
            CrashApp.m16335b("BluetoothLeService", "writeCharacteristic0 exception");
            return 3;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16154a(String str) {
        sendBroadcast(new Intent(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16155a(String str, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        int i;
        try {
            Intent intent = new Intent(str);
            if (UUID.fromString(SampleGattAttributes.f10260b).equals(bluetoothGattCharacteristic.getUuid())) {
                if ((bluetoothGattCharacteristic.getProperties() & 1) != 0) {
                    i = 18;
                    CrashApp.m16334a("BluetoothLeService", "Heart rate format UINT16.");
                } else {
                    i = 17;
                    CrashApp.m16334a("BluetoothLeService", "Heart rate format UINT8.");
                }
                int intValue = bluetoothGattCharacteristic.getIntValue(i, 1).intValue();
                CrashApp.m16334a("BluetoothLeService", String.format("Received heart rate: %d", Integer.valueOf(intValue)));
                intent.putExtra(Common.f10362l, String.valueOf(intValue));
            } else {
                byte[] value = bluetoothGattCharacteristic.getValue();
                if (value != null && value.length > 0) {
                    StringBuilder sb = new StringBuilder(value.length);
                    int length = value.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        sb.append(String.format("%02X ", Byte.valueOf(value[i2])));
                    }
                    intent.putExtra(Common.f10362l, sb.toString());
                }
            }
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16156a(String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action_cmd", "gear_auth");
            jSONObject.put("seq_id", String.valueOf(System.currentTimeMillis()));
            jSONObject.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, SysUtils.m16357a());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("tid", "");
            jSONObject2.put("mac_id", str2);
            jSONObject2.put("device_name", Uri.encode(str));
            jSONObject2.put("vid", this.f10232l0);
            jSONObject2.put("phone_id", SysUtils.m16358a(this));
            jSONObject2.put("phone_name", SysUtils.m16366b());
            jSONObject2.put("phone_os", SysUtils.m16367b(this));
            jSONObject.put(TtmlNode.TAG_BODY, jSONObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CrashApp.m16337d("BluetoothLeService", "gear_auth url " + "http://api.keeprapid.com:8081/ronaldo-gearcenter");
        CrashApp.m16337d("BluetoothLeService", "gear_auth req " + jSONObject.toString());
        C4771h.m16343a("http://api.keeprapid.com:8081/ronaldo-gearcenter", jSONObject, new C4785o(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16157a(JSONObject jSONObject) {
        try {
            if (jSONObject.getJSONObject(TtmlNode.TAG_BODY).has("auth_flag") && ((Integer) jSONObject.getJSONObject(TtmlNode.TAG_BODY).get("auth_flag")).intValue() != 0) {
                mo26495a();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m16161a(String str, int i, String str2, String str3) {
        String str4 = str;
        int i2 = i;
        String str5 = str2;
        String str6 = str3;
        if (str4 == null || str.length() <= 0 || !this.f10205X0.equals(str4)) {
            CrashApp.m16335b("BluetoothLeService", "not same nl_id , id:" + this.f10205X0 + " , nl_id ：" + str4 + ",type:" + i2 + ",title:" + str5 + " , content :" + str6);
            this.f10205X0 = str4;
            int length = str3.getBytes().length;
            this.f10203W0 = 2;
            if (length % 17 != 0) {
                int i3 = (length / 17) + 1;
                if (i3 <= 4) {
                    this.f10203W0 += i3;
                } else {
                    this.f10203W0 += i3;
                }
            } else if (length == 0) {
                this.f10203W0++;
            } else {
                int i4 = length / 17;
                if (i4 <= 4) {
                    this.f10203W0 += i4;
                } else {
                    this.f10203W0 += i4;
                }
            }
            String l = m16241l();
            AncsItem aVar = r1;
            AncsItem aVar2 = new AncsItem(l, i, 0, str2, str2, str3);
            this.f10201V0.add(aVar);
            byte[] bytes = str3.getBytes();
            for (int i5 = 1; i5 <= this.f10203W0; i5++) {
                byte[] bArr = new byte[20];
                for (int i6 = 0; i6 < 20; i6++) {
                    bArr[i6] = 0;
                }
                bArr[0] = 18;
                bArr[1] = (byte) this.f10203W0;
                bArr[2] = (byte) i5;
                if (i5 == 1) {
                    bArr[3] = 0;
                    bArr[4] = (byte) i2;
                    byte[] bytes2 = l.getBytes();
                    System.arraycopy(bytes2, 0, bArr, 5, bytes2.length);
                } else if (i5 == 2) {
                    byte[] bytes3 = str2.getBytes();
                    if (bytes3.length > 17) {
                        System.arraycopy(bytes3, 0, bArr, 3, 17);
                    } else {
                        System.arraycopy(bytes3, 0, bArr, 3, bytes3.length);
                    }
                } else if (length != 0) {
                    int i7 = (i5 - 3) * 17;
                    int i8 = length - i7;
                    System.arraycopy(bytes, i7, bArr, 3, (i8 > 17 ? i7 + 17 : i7 + i8) - i7);
                }
                m16175b(bArr);
            }
            return true;
        }
        CrashApp.m16335b("BluetoothLeService", "same nl_id , id:" + this.f10205X0 + " , nl_id ：" + str4 + ",type:" + i2 + ",title:" + str5 + " , content :" + str6);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.c.i.a(byte[], java.util.UUID, boolean):boolean
     arg types: [byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.c.i.a(byte[], int, int):java.lang.String
      com.sxr.sdk.ble.keepfit.service.c.i.a(byte[], java.util.UUID, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0061, code lost:
        if (r9.equalsIgnoreCase(r6.f10204X.replace(":", "")) == false) goto L_0x0063;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m16145a(android.bluetooth.BluetoothDevice r7, int r8, byte[] r9) {
        /*
            r6 = this;
            java.lang.String r0 = ","
            java.lang.String r1 = "onLeScan "
            java.lang.String r2 = "BluetoothLeService"
            r3 = 0
            r4 = 0
            boolean r4 = com.sxr.sdk.ble.keepfit.service.p212c.ScannerServiceParser.m16350a(r9, r4, r3)     // Catch:{ Exception -> 0x0092 }
            if (r4 == 0) goto L_0x00a9
            java.lang.String r9 = com.sxr.sdk.ble.keepfit.service.p212c.ScannerServiceParser.m16347a(r9)     // Catch:{ Exception -> 0x0092 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0092 }
            r4.<init>()     // Catch:{ Exception -> 0x0092 }
            r4.append(r1)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r5 = r7.getAddress()     // Catch:{ Exception -> 0x0092 }
            r4.append(r5)     // Catch:{ Exception -> 0x0092 }
            r4.append(r0)     // Catch:{ Exception -> 0x0092 }
            r4.append(r9)     // Catch:{ Exception -> 0x0092 }
            r4.append(r0)     // Catch:{ Exception -> 0x0092 }
            r4.append(r8)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x0092 }
            com.sxr.sdk.ble.keepfit.service.p212c.CrashApp.m16336c(r2, r0)     // Catch:{ Exception -> 0x0092 }
            com.sxr.sdk.ble.keepfit.aidl.b r0 = r6.f10228j0     // Catch:{ Exception -> 0x0092 }
            if (r0 == 0) goto L_0x004d
            com.sxr.sdk.ble.keepfit.aidl.b r0 = r6.f10228j0     // Catch:{ Exception -> 0x0092 }
            android.os.IBinder r0 = r0.asBinder()     // Catch:{ Exception -> 0x0092 }
            boolean r0 = r0.isBinderAlive()     // Catch:{ Exception -> 0x0092 }
            if (r0 == 0) goto L_0x004d
            com.sxr.sdk.ble.keepfit.aidl.b r0 = r6.f10228j0     // Catch:{ Exception -> 0x0092 }
            java.lang.String r4 = r7.getAddress()     // Catch:{ Exception -> 0x0092 }
            r0.mo22921a(r9, r4, r8)     // Catch:{ Exception -> 0x0092 }
        L_0x004d:
            com.sxr.sdk.ble.keepfit.service.v r8 = r6.f10243q1     // Catch:{ Exception -> 0x0092 }
            boolean r8 = r8.f10382c     // Catch:{ Exception -> 0x0092 }
            if (r8 == 0) goto L_0x0063
            java.lang.String r8 = r6.f10204X     // Catch:{ Exception -> 0x0092 }
            java.lang.String r0 = ":"
            java.lang.String r4 = ""
            java.lang.String r8 = r8.replace(r0, r4)     // Catch:{ Exception -> 0x0092 }
            boolean r8 = r9.equalsIgnoreCase(r8)     // Catch:{ Exception -> 0x0092 }
            if (r8 != 0) goto L_0x0071
        L_0x0063:
            com.sxr.sdk.ble.keepfit.service.v r8 = r6.f10243q1     // Catch:{ Exception -> 0x0092 }
            boolean r8 = r8.f10383d     // Catch:{ Exception -> 0x0092 }
            if (r8 == 0) goto L_0x00a9
            java.lang.String r8 = r6.f10206Y     // Catch:{ Exception -> 0x0092 }
            boolean r8 = r9.equalsIgnoreCase(r8)     // Catch:{ Exception -> 0x0092 }
            if (r8 == 0) goto L_0x00a9
        L_0x0071:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0092 }
            r8.<init>()     // Catch:{ Exception -> 0x0092 }
            java.lang.String r9 = "connect ota device "
            r8.append(r9)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r9 = r7.getAddress()     // Catch:{ Exception -> 0x0092 }
            r8.append(r9)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0092 }
            com.sxr.sdk.ble.keepfit.service.p212c.CrashApp.m16336c(r2, r8)     // Catch:{ Exception -> 0x0092 }
            r6.mo26496a(r3)     // Catch:{ Exception -> 0x0092 }
            com.sxr.sdk.ble.keepfit.service.v r8 = r6.f10243q1     // Catch:{ Exception -> 0x0092 }
            r8.mo26579a(r7)     // Catch:{ Exception -> 0x0092 }
            goto L_0x00a9
        L_0x0092:
            r7 = move-exception
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r1)
            java.lang.String r7 = r7.toString()
            r8.append(r7)
            java.lang.String r7 = r8.toString()
            com.sxr.sdk.ble.keepfit.service.p212c.CrashApp.m16335b(r2, r7)
        L_0x00a9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.m16145a(android.bluetooth.BluetoothDevice, int, byte[]):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo26496a(boolean z) {
        if (this.f10190Q != null) {
            CrashApp.m16336c("BluetoothLeService", "scanLeDevice " + z);
            try {
                this.f10208Z = z;
                if (this.f10213b1) {
                    if (this.f10222g0 != null) {
                        this.f10222g0.removeCallbacks(this.f10224h0);
                        this.f10222g0.removeCallbacks(this.f10226i0);
                    }
                    m16217g();
                }
                if (z) {
                    this.f10226i0 = new C4766b(this);
                    this.f10222g0.postDelayed(this.f10226i0, 10000);
                    this.f10224h0 = new C4768c(this);
                    this.f10222g0.postDelayed(this.f10224h0, 15000);
                    m16212f();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16120a(long j) {
        return TimeZone.getDefault().getOffset(System.currentTimeMillis());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16116a(int i, int i2, int i3, int i4, int i5) {
        if (!m16197d("setIdleTime")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i6 = 0; i6 < 20; i6++) {
            bArr[i6] = 0;
        }
        byte[] a = ConvertUtil.m16333a(i);
        bArr[0] = 8;
        bArr[1] = a[0];
        bArr[2] = a[1];
        bArr[3] = a[2];
        bArr[4] = a[3];
        bArr[5] = (byte) i2;
        bArr[6] = (byte) i3;
        bArr[7] = (byte) i4;
        bArr[8] = (byte) i5;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16118a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (!m16197d("setSleepTime")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i9 = 0; i9 < 20; i9++) {
            bArr[i9] = 0;
        }
        bArr[0] = 9;
        bArr[1] = (byte) i;
        bArr[2] = (byte) i2;
        bArr[3] = (byte) i3;
        bArr[4] = (byte) i4;
        bArr[5] = (byte) i5;
        bArr[6] = (byte) i6;
        bArr[7] = (byte) i7;
        bArr[8] = (byte) i8;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16137a(boolean z, int i) {
        if (!m16197d("setHeartRateMode2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i2 = 0; i2 < 20; i2++) {
            bArr[i2] = 0;
        }
        if (z) {
            bArr[0] = 20;
            byte[] a = ConvertUtil.m16333a(i);
            bArr[1] = a[0];
            bArr[2] = a[1];
            bArr[3] = a[2];
            bArr[4] = a[3];
        } else {
            bArr[0] = 21;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16115a(int i, int i2) {
        if (!m16197d("getDataByDay2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i3 = 0; i3 < 20; i3++) {
            bArr[i3] = 0;
        }
        if (i == 1) {
            bArr[0] = 16;
        } else if (i == 2) {
            bArr[0] = 22;
        }
        bArr[1] = (byte) i2;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16139a(boolean z, int i, int i2, int i3, int i4, int i5, int i6) {
        if (!m16197d("setAutoHeartMode2")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i7 = 0; i7 < 20; i7++) {
            bArr[i7] = 0;
        }
        bArr[0] = 25;
        bArr[1] = (byte) i;
        bArr[2] = (byte) i2;
        bArr[3] = (byte) i3;
        bArr[4] = (byte) i4;
        if (z) {
            bArr[5] = 1;
        } else {
            bArr[5] = 0;
        }
        if (i6 >= i5) {
            if (i5 > 1) {
                i6 = i5 - 1;
            } else {
                i5 = 15;
                i6 = 2;
            }
        }
        bArr[6] = (byte) i5;
        bArr[7] = (byte) i6;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16138a(boolean z, int i, int i2) {
        if (!m16197d("setHeartRateArea")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i3 = 0; i3 < 20; i3++) {
            bArr[i3] = 0;
        }
        bArr[0] = 38;
        bArr[1] = z ? (byte) 1 : 0;
        bArr[2] = (byte) i;
        bArr[3] = (byte) i2;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16140a(byte[] bArr) {
        if (!m16197d("setDeviceCode0")) {
            return this.f10242q0;
        }
        byte[] bArr2 = new byte[20];
        int i = 0;
        bArr2[0] = 30;
        while (i < bArr.length && i < 19) {
            int i2 = i + 1;
            bArr2[i2] = bArr[i];
            i = i2;
        }
        m16175b(bArr2);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16117a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        if (!m16197d("setReminder0")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        byte[] a = ConvertUtil.m16333a(i);
        bArr[0] = 49;
        bArr[1] = a[0];
        bArr[2] = a[1];
        bArr[3] = a[2];
        bArr[4] = a[3];
        bArr[5] = (byte) i2;
        bArr[6] = (byte) i3;
        bArr[7] = (byte) i4;
        bArr[8] = (byte) i5;
        bArr[9] = (byte) i6;
        bArr[10] = (byte) i7;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int m16119a(int i, String str) {
        if (!m16197d("setReminderText0")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        int i2 = 0;
        bArr[0] = 50;
        bArr[1] = (byte) i;
        byte[] bytes = str.getBytes();
        while (i2 < bytes.length && i2 < 18) {
            bArr[i2 + 2] = bytes[i2];
            i2++;
        }
        m16175b(bArr);
        return this.f10242q0;
    }

    /* renamed from: a */
    private void m16158a(String[] strArr, boolean z) {
        Runnable runnable = this.f10249t1;
        if (runnable != null) {
            this.f10247s1.removeCallbacks(runnable);
        }
        this.f10249t1 = new C4776f(this, strArr);
        if (z) {
            this.f10247s1.postDelayed(this.f10249t1, (long) this.f10245r1);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public int m16183c(boolean z) {
        if (!m16197d("setIdleTime")) {
            return this.f10242q0;
        }
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        bArr[0] = 35;
        bArr[1] = z ? (byte) 1 : 0;
        m16175b(bArr);
        return this.f10242q0;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public int m16178c(int i) {
        if (!m16197d("getEcgData0")) {
            return this.f10242q0;
        }
        int rawOffset = i + (TimeZone.getDefault().getRawOffset() / 1000);
        byte[] bArr = new byte[20];
        bArr[0] = 44;
        bArr[4] = (byte) ((-16777216 & rawOffset) >> 24);
        bArr[3] = (byte) ((16711680 & rawOffset) >> 16);
        bArr[2] = (byte) ((65280 & rawOffset) >> 8);
        bArr[1] = (byte) (rawOffset & 255);
        m16175b(bArr);
        return this.f10242q0;
    }
}
