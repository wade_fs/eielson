package com.sxr.sdk.ble.keepfit.service;

/* renamed from: com.sxr.sdk.ble.keepfit.service.r */
public class Common {

    /* renamed from: a */
    public static int f10351a = 1;

    /* renamed from: b */
    public static String f10352b = "data.log";

    /* renamed from: c */
    public static String f10353c = ".bin";

    /* renamed from: d */
    public static String f10354d = "";

    /* renamed from: e */
    public static String f10355e = ("/" + f10354d + "/logs/");

    /* renamed from: f */
    public static String f10356f = (f10354d + ".ACTION_NOTIFY_BLE_SEND_CMD_TIMEOUT");

    /* renamed from: g */
    public static String f10357g = (f10354d + ".ACTION_GATT_CONNECTED");

    /* renamed from: h */
    public static String f10358h = (f10354d + ".ACTION_GATT_DISCONNECTED");

    /* renamed from: i */
    public static String f10359i = (f10354d + ".ACTION_GATT_SERVICES_DISCOVERED");

    /* renamed from: j */
    public static String f10360j = (f10354d + ".ACTION_GATT_SERVICES_DISCOVERED_ERROR");

    /* renamed from: k */
    public static String f10361k = (f10354d + ".ACTION_DATA_AVAILABLE");

    /* renamed from: l */
    public static String f10362l = (f10354d + ".EXTRA_DATA");

    /* renamed from: m */
    public static String f10363m = (f10354d + ".ACTION_GATT_BATTERY");

    static {
        f10354d + ".ACTION_NOTIFY_BLE_SLEEPING_MODE_CHANGE";
    }

    /* renamed from: a */
    public static void m16416a(String str) {
        f10354d = str;
        f10356f = f10354d + ".ACTION_NOTIFY_BLE_SEND_CMD_TIMEOUT";
        f10354d + ".ACTION_NOTIFY_BLE_SLEEPING_MODE_CHANGE";
        f10357g = f10354d + ".ACTION_GATT_CONNECTED";
        f10358h = f10354d + ".ACTION_GATT_DISCONNECTED";
        f10359i = f10354d + ".ACTION_GATT_SERVICES_DISCOVERED";
        f10360j = f10354d + ".ACTION_GATT_SERVICES_DISCOVERED_ERROR";
        f10361k = f10354d + ".ACTION_DATA_AVAILABLE";
        f10362l = f10354d + ".EXTRA_DATA";
        f10363m = f10354d + ".ACTION_GATT_BATTERY";
    }
}
