package com.sxr.sdk.ble.keepfit.service;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.os.Build;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;
import java.util.List;

/* renamed from: com.sxr.sdk.ble.keepfit.service.i */
/* compiled from: BluetoothLeService */
class C4779i extends ScanCallback {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10341a;

    C4779i(BluetoothLeService bluetoothLeService) {
        this.f10341a = bluetoothLeService;
    }

    public void onBatchScanResults(List<ScanResult> list) {
        super.onBatchScanResults(list);
    }

    public void onScanFailed(int i) {
        super.onScanFailed(i);
        CrashApp.m16336c("BluetoothLeService", "onScanFailed " + i);
    }

    public void onScanResult(int i, ScanResult scanResult) {
        super.onScanResult(i, scanResult);
        CrashApp.m16334a("BluetoothLeService", "onScanResult " + i);
        if (Build.VERSION.SDK_INT >= 21) {
            this.f10341a.m16145a(scanResult.getDevice(), scanResult.getRssi(), scanResult.getScanRecord().getBytes());
        }
    }
}
