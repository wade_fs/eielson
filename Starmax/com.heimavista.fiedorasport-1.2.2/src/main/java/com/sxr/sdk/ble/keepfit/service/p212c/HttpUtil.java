package com.sxr.sdk.ble.keepfit.service.p212c;

import com.sxr.sdk.ble.keepfit.service.p212c.C4771h;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/* renamed from: com.sxr.sdk.ble.keepfit.service.c.e */
class HttpUtil implements Runnable {

    /* renamed from: P */
    final /* synthetic */ String f10325P;

    /* renamed from: Q */
    final /* synthetic */ C4771h.C4772a f10326Q;

    HttpUtil(String str, C4771h.C4772a aVar) {
        this.f10325P = str;
        this.f10326Q = aVar;
    }

    public void run() {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(this.f10325P));
            this.f10326Q.mo26557a(execute.getStatusLine().getStatusCode(), EntityUtils.toByteArray(execute.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
