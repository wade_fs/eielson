package com.sxr.sdk.ble.keepfit.service;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.os.RemoteException;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.sxr.sdk.ble.keepfit.service.p210a.SampleGattAttributes;
import com.sxr.sdk.ble.keepfit.service.p212c.CrashApp;

/* renamed from: com.sxr.sdk.ble.keepfit.service.k */
/* compiled from: BluetoothLeService */
class C4781k extends BluetoothGattCallback {

    /* renamed from: a */
    final /* synthetic */ BluetoothLeService f10343a;

    C4781k(BluetoothLeService bluetoothLeService) {
        this.f10343a = bluetoothLeService;
    }

    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        CrashApp.m16334a("BluetoothLeService", "onCharacteristicChanged " + bluetoothGattCharacteristic.getUuid());
        if (!this.f10343a.f10186N0) {
            this.f10343a.m16155a(Common.f10361k, bluetoothGattCharacteristic);
        }
        if (this.f10343a.f10228j0 != null) {
            try {
                this.f10343a.f10228j0.mo22922a(bluetoothGattCharacteristic.getUuid().toString(), bluetoothGattCharacteristic.getValue());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        CrashApp.m16336c("BluetoothLeService", "onCharacteristicRead " + bluetoothGattCharacteristic.getUuid() + " " + i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.g(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        CrashApp.m16334a("BluetoothLeService", "onCharacteristicWrite " + bluetoothGattCharacteristic.getUuid() + " " + i);
        try {
            if (this.f10343a.f10228j0 != null) {
                this.f10343a.f10228j0.mo22923a(bluetoothGattCharacteristic.getUuid().toString(), bluetoothGattCharacteristic.getValue(), i);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        this.f10343a.m16231i(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void
     arg types: [com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int]
     candidates:
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, java.lang.String):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, com.sxr.sdk.ble.keepfit.aidl.BleClientOption):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, byte[]):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(boolean, int):int
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, long):long
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.lang.String):java.lang.String
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(int, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(android.bluetooth.BluetoothGattCharacteristic, boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, org.json.JSONObject):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, android.bluetooth.BluetoothGattCharacteristic):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String, java.lang.String):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(java.lang.String[], boolean):void
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, java.util.List):boolean
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(byte[], int):byte[]
      com.sxr.sdk.ble.keepfit.service.BluetoothLeService.a(com.sxr.sdk.ble.keepfit.service.BluetoothLeService, boolean):void */
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("onConnectionStateChange status ");
            sb.append(i);
            sb.append(" state ");
            sb.append(i2);
            CrashApp.m16337d("BluetoothLeService", sb.toString());
            if (i == 257) {
                this.f10343a.f10190Q.disable();
                BluetoothLeService.f10171x1.postDelayed(new C4780j(this), AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
            }
            if (i2 == 2) {
                this.f10343a.mo26496a(false);
                this.f10343a.m16154a(Common.f10357g);
                CrashApp.m16336c("BluetoothLeService", String.format("STATE_CONNECTED state[%1$d] mBleState[%2$d]", Integer.valueOf(this.f10343a.f10218e0), Integer.valueOf(BluetoothLeService.f10169v1)));
                boolean discoverServices = this.f10343a.f10192R.discoverServices();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("discoverServices ");
                sb2.append(discoverServices);
                CrashApp.m16336c("BluetoothLeService", sb2.toString());
            } else if (i2 == 0) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("mUserDisconnected ");
                sb3.append(this.f10343a.f10214c0);
                CrashApp.m16337d("BluetoothLeService", sb3.toString());
                if (this.f10343a.f10214c0) {
                    this.f10343a.m16238k();
                } else if (this.f10343a.f10190Q.getState() != 10) {
                    this.f10343a.m16194d(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        CrashApp.m16337d("BluetoothLeService", "onDescriptorWrite " + i);
        if (bluetoothGattDescriptor.getCharacteristic().getUuid().toString().equals(SampleGattAttributes.f10262d)) {
            this.f10343a.m16225h();
        }
    }

    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        try {
            if (this.f10343a.f10228j0 != null) {
                this.f10343a.f10228j0.mo22950x(i);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        if (i == 0) {
            CrashApp.m16334a("BluetoothLeService", "onServicesDiscovered " + i);
            this.f10343a.m16154a(Common.f10359i);
            return;
        }
        int unused = BluetoothLeService.f10170w1 = i;
        CrashApp.m16335b("BluetoothLeService", "onServicesDiscovered " + i);
        this.f10343a.m16154a(Common.f10360j);
    }
}
