package com.sxr.sdk.ble.keepfit.service.p211b;

import android.os.Environment;
import android.util.Log;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/* renamed from: com.sxr.sdk.ble.keepfit.service.b.e */
public class File {

    /* renamed from: a */
    private InputStream f10297a;

    /* renamed from: b */
    private byte f10298b;

    /* renamed from: c */
    private byte[] f10299c;

    /* renamed from: d */
    private byte[][][] f10300d;

    /* renamed from: e */
    private int f10301e = 0;

    /* renamed from: f */
    private int f10302f;

    /* renamed from: g */
    private int f10303g = -1;

    /* renamed from: h */
    private int f10304h;

    /* renamed from: i */
    private int f10305i;

    /* renamed from: j */
    private int f10306j;

    static {
        Environment.getExternalStorageDirectory().getAbsolutePath() + "/wannafit";
    }

    private File(InputStream inputStream) {
        this.f10297a = inputStream;
        this.f10302f = this.f10297a.available();
    }

    /* renamed from: g */
    private byte m16317g() {
        byte b = 0;
        for (int i = 0; i < this.f10302f; i++) {
            b = (byte) (b ^ Byte.valueOf(this.f10299c[i]).intValue());
        }
        Log.d("crc", "crc: " + String.format("%#10x", Byte.valueOf(b)));
        return b;
    }

    /* renamed from: h */
    private void m16318h() {
        if (this.f10306j == 1) {
            m16319i();
        }
    }

    /* renamed from: i */
    private void m16319i() {
        this.f10300d = new byte[this.f10303g][][];
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = this.f10303g;
            if (i < i4) {
                int i5 = this.f10301e;
                int i6 = i + 1;
                if (i6 == i4) {
                    i5 = this.f10299c.length % i5;
                }
                this.f10300d[i] = new byte[((int) Math.ceil(((double) i5) / 20.0d))][];
                int i7 = i3;
                int i8 = 0;
                int i9 = i2;
                int i10 = 0;
                while (i10 < i5) {
                    int i11 = i7 + 20;
                    byte[] bArr = this.f10299c;
                    int i12 = 20;
                    if (i11 > bArr.length) {
                        i12 = bArr.length - i7;
                    } else if (i10 + 20 > i5) {
                        i12 = this.f10301e % 20;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("total bytes: ");
                    sb.append(this.f10299c.length);
                    sb.append(", offset: ");
                    sb.append(i7);
                    sb.append(", block: ");
                    sb.append(i);
                    sb.append(", chunk: ");
                    int i13 = i8 + 1;
                    sb.append(i13);
                    sb.append(", blocksize: ");
                    sb.append(i5);
                    sb.append(", chunksize: ");
                    sb.append(i12);
                    Log.d("chunk", sb.toString());
                    int i14 = i7 + i12;
                    this.f10300d[i][i8] = Arrays.copyOfRange(this.f10299c, i7, i14);
                    i9++;
                    i10 += 20;
                    i8 = i13;
                    i7 = i14;
                }
                i2 = i9;
                i3 = i7;
                i = i6;
            } else {
                this.f10305i = i2;
                return;
            }
        }
    }

    /* renamed from: a */
    public byte[][] mo26544a(int i) {
        return this.f10300d[i];
    }

    /* renamed from: b */
    public void mo26546b(int i) {
        this.f10301e = i;
        this.f10304h = (int) Math.ceil(((double) i) / 20.0d);
        this.f10303g = (int) Math.ceil(((double) this.f10299c.length) / ((double) this.f10301e));
        m16318h();
    }

    /* renamed from: c */
    public void mo26548c(int i) {
        this.f10306j = i;
        if (i == 1) {
            this.f10299c = new byte[(this.f10302f + 1)];
            this.f10297a.read(this.f10299c);
            this.f10298b = m16317g();
            this.f10299c[this.f10302f] = this.f10298b;
            return;
        }
        this.f10299c = new byte[this.f10302f];
        this.f10297a.read(this.f10299c);
    }

    /* renamed from: d */
    public int mo26549d() {
        return this.f10303g;
    }

    /* renamed from: e */
    public int mo26550e() {
        return this.f10299c.length;
    }

    /* renamed from: f */
    public int mo26551f() {
        return this.f10305i;
    }

    /* renamed from: a */
    public void mo26543a() {
        InputStream inputStream = this.f10297a;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public static File m16316a(String str) {
        return new File(new FileInputStream(str));
    }

    /* renamed from: b */
    public int mo26545b() {
        return this.f10304h;
    }

    /* renamed from: c */
    public int mo26547c() {
        return this.f10301e;
    }
}
