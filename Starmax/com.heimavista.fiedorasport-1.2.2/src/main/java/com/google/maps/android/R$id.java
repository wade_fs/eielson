package com.google.maps.android;

public final class R$id {
    public static final int adjust_height = 2131296349;
    public static final int adjust_width = 2131296350;
    public static final int amu_text = 2131296355;
    public static final int auto = 2131296363;
    public static final int dark = 2131296482;
    public static final int hybrid = 2131296605;
    public static final int icon_only = 2131296608;
    public static final int light = 2131296699;
    public static final int none = 2131296778;
    public static final int normal = 2131296779;
    public static final int satellite = 2131296847;
    public static final int standard = 2131296908;
    public static final int terrain = 2131296934;
    public static final int webview = 2131297104;
    public static final int wide = 2131297107;
    public static final int window = 2131297108;

    private R$id() {
    }
}
