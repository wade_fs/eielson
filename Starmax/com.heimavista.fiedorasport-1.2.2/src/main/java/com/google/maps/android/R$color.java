package com.google.maps.android;

public final class R$color {
    public static final int common_google_signin_btn_text_dark = 2131099717;
    public static final int common_google_signin_btn_text_dark_default = 2131099718;
    public static final int common_google_signin_btn_text_dark_disabled = 2131099719;
    public static final int common_google_signin_btn_text_dark_focused = 2131099720;
    public static final int common_google_signin_btn_text_dark_pressed = 2131099721;
    public static final int common_google_signin_btn_text_light = 2131099722;
    public static final int common_google_signin_btn_text_light_default = 2131099723;
    public static final int common_google_signin_btn_text_light_disabled = 2131099724;
    public static final int common_google_signin_btn_text_light_focused = 2131099725;
    public static final int common_google_signin_btn_text_light_pressed = 2131099726;

    private R$color() {
    }
}
