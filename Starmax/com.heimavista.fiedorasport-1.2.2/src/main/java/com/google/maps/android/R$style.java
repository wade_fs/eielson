package com.google.maps.android;

public final class R$style {
    public static final int amu_Bubble_TextAppearance_Dark = 2131886817;
    public static final int amu_Bubble_TextAppearance_Light = 2131886818;
    public static final int amu_ClusterIcon_TextAppearance = 2131886819;

    private R$style() {
    }
}
