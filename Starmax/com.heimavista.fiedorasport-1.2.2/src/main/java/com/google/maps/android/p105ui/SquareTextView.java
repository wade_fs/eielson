package com.google.maps.android.p105ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/* renamed from: com.google.maps.android.ui.SquareTextView */
public class SquareTextView extends TextView {

    /* renamed from: P */
    private int f6195P = 0;

    /* renamed from: Q */
    private int f6196Q = 0;

    public SquareTextView(Context context) {
        super(context);
    }

    public void draw(Canvas canvas) {
        canvas.translate((float) (this.f6196Q / 2), (float) (this.f6195P / 2));
        super.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int max = Math.max(measuredWidth, measuredHeight);
        if (measuredWidth > measuredHeight) {
            this.f6195P = measuredWidth - measuredHeight;
            this.f6196Q = 0;
        } else {
            this.f6195P = 0;
            this.f6196Q = measuredHeight - measuredWidth;
        }
        setMeasuredDimension(max, max);
    }

    public SquareTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SquareTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
