package com.google.maps.android;

public final class R$drawable {
    public static final int amu_bubble_mask = 2131230813;
    public static final int amu_bubble_shadow = 2131230814;
    public static final int common_full_open_on_phone = 2131230869;
    public static final int common_google_signin_btn_icon_dark = 2131230870;
    public static final int common_google_signin_btn_icon_dark_focused = 2131230871;
    public static final int common_google_signin_btn_icon_dark_normal = 2131230872;
    public static final int common_google_signin_btn_icon_light = 2131230875;
    public static final int common_google_signin_btn_icon_light_focused = 2131230876;
    public static final int common_google_signin_btn_icon_light_normal = 2131230877;
    public static final int common_google_signin_btn_text_dark = 2131230879;
    public static final int common_google_signin_btn_text_dark_focused = 2131230880;
    public static final int common_google_signin_btn_text_dark_normal = 2131230881;
    public static final int common_google_signin_btn_text_light = 2131230884;
    public static final int common_google_signin_btn_text_light_focused = 2131230885;
    public static final int common_google_signin_btn_text_light_normal = 2131230886;

    private R$drawable() {
    }
}
