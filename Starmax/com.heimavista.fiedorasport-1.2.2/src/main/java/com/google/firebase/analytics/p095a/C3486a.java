package com.google.firebase.analytics.p095a;

import android.os.Bundle;
import androidx.annotation.NonNull;

/* renamed from: com.google.firebase.analytics.a.a */
public interface C3486a {
    /* renamed from: a */
    void mo22099a(@NonNull String str, @NonNull String str2, Bundle bundle);

    /* renamed from: a */
    void mo22100a(@NonNull String str, @NonNull String str2, Object obj);
}
