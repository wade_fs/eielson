package com.google.firebase.datatransport;

public final class R$style {
    public static final int TextAppearance_Compat_Notification = 2131886466;
    public static final int TextAppearance_Compat_Notification_Info = 2131886467;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131886469;
    public static final int TextAppearance_Compat_Notification_Time = 2131886472;
    public static final int TextAppearance_Compat_Notification_Title = 2131886474;
    public static final int Widget_Compat_NotificationActionContainer = 2131886705;
    public static final int Widget_Compat_NotificationActionText = 2131886706;
    public static final int Widget_Support_CoordinatorLayout = 2131886814;

    private R$style() {
    }
}
