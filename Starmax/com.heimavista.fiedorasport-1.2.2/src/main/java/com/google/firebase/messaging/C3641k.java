package com.google.firebase.messaging;

import com.google.android.datatransport.cct.C1660a;
import com.google.firebase.C3494c;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingRegistrar;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3882g;

/* renamed from: com.google.firebase.messaging.k */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3641k implements C3511g {

    /* renamed from: a */
    static final C3511g f6181a = new C3641k();

    private C3641k() {
    }

    /* renamed from: a */
    public final Object mo22104a(C3506e eVar) {
        C3494c cVar = (C3494c) eVar.mo22121a(C3494c.class);
        FirebaseInstanceId firebaseInstanceId = (FirebaseInstanceId) eVar.mo22121a(FirebaseInstanceId.class);
        C3882g gVar = (C3882g) eVar.mo22121a(C3882g.class);
        if (gVar == null || !C1660a.f2564g.mo13438a().contains(C3877b.m11673a("json"))) {
            gVar = new FirebaseMessagingRegistrar.C3626a();
        }
        return new FirebaseMessaging(cVar, firebaseInstanceId, gVar);
    }
}
