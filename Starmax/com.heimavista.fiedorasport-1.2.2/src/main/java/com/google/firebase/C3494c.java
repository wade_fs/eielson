package com.google.firebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.collection.ArrayMap;
import androidx.core.p012os.UserManagerCompat;
import com.google.android.gms.common.api.internal.C2051b;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2312c;
import com.google.android.gms.common.util.C2323n;
import com.google.android.gms.common.util.C2324o;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3507f;
import com.google.firebase.components.C3512h;
import com.google.firebase.components.C3516l;
import com.google.firebase.components.C3526s;
import com.google.firebase.components.ComponentDiscoveryService;
import com.google.firebase.p099f.C3549c;
import com.google.firebase.p100g.C3552b;
import com.google.firebase.p102i.C3557a;
import com.google.firebase.p104j.C3620c;
import com.google.firebase.p104j.C3622e;
import com.google.firebase.p104j.C3624g;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.firebase.c */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public class C3494c {
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static final Object f5919i = new Object();

    /* renamed from: j */
    private static final Executor f5920j = new C3498d();

    /* renamed from: k */
    static final Map<String, C3494c> f5921k = new ArrayMap();

    /* renamed from: a */
    private final Context f5922a;

    /* renamed from: b */
    private final String f5923b;

    /* renamed from: c */
    private final C3531d f5924c;

    /* renamed from: d */
    private final C3516l f5925d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final AtomicBoolean f5926e = new AtomicBoolean(false);

    /* renamed from: f */
    private final AtomicBoolean f5927f = new AtomicBoolean();

    /* renamed from: g */
    private final C3526s<C3557a> f5928g;

    /* renamed from: h */
    private final List<C3496b> f5929h = new CopyOnWriteArrayList();

    /* renamed from: com.google.firebase.c$b */
    /* compiled from: com.google.firebase:firebase-common@@19.3.0 */
    public interface C3496b {
        /* renamed from: a */
        void mo22116a(boolean z);
    }

    @TargetApi(14)
    /* renamed from: com.google.firebase.c$c */
    /* compiled from: com.google.firebase:firebase-common@@19.3.0 */
    private static class C3497c implements C2051b.C2052a {

        /* renamed from: a */
        private static AtomicReference<C3497c> f5930a = new AtomicReference<>();

        private C3497c() {
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static void m9568b(Context context) {
            if (C2323n.m5792a() && (context.getApplicationContext() instanceof Application)) {
                Application application = (Application) context.getApplicationContext();
                if (f5930a.get() == null) {
                    C3497c cVar = new C3497c();
                    if (f5930a.compareAndSet(null, cVar)) {
                        C2051b.m4757a(application);
                        C2051b.m4758b().mo16618a(cVar);
                    }
                }
            }
        }

        /* renamed from: a */
        public void mo16631a(boolean z) {
            synchronized (C3494c.f5919i) {
                Iterator it = new ArrayList(C3494c.f5921k.values()).iterator();
                while (it.hasNext()) {
                    C3494c cVar = (C3494c) it.next();
                    if (cVar.f5926e.get()) {
                        cVar.m9553a(z);
                    }
                }
            }
        }
    }

    /* renamed from: com.google.firebase.c$d */
    /* compiled from: com.google.firebase:firebase-common@@19.3.0 */
    private static class C3498d implements Executor {

        /* renamed from: a */
        private static final Handler f5931a = new Handler(Looper.getMainLooper());

        private C3498d() {
        }

        public void execute(@NonNull Runnable runnable) {
            f5931a.post(runnable);
        }
    }

    @TargetApi(24)
    /* renamed from: com.google.firebase.c$e */
    /* compiled from: com.google.firebase:firebase-common@@19.3.0 */
    private static class C3499e extends BroadcastReceiver {

        /* renamed from: b */
        private static AtomicReference<C3499e> f5932b = new AtomicReference<>();

        /* renamed from: a */
        private final Context f5933a;

        public C3499e(Context context) {
            this.f5933a = context;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static void m9571b(Context context) {
            if (f5932b.get() == null) {
                C3499e eVar = new C3499e(context);
                if (f5932b.compareAndSet(null, eVar)) {
                    context.registerReceiver(super, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        public void onReceive(Context context, Intent intent) {
            synchronized (C3494c.f5919i) {
                for (C3494c cVar : C3494c.f5921k.values()) {
                    cVar.m9558j();
                }
            }
            mo22118a();
        }

        /* renamed from: a */
        public void mo22118a() {
            this.f5933a.unregisterReceiver(super);
        }
    }

    protected C3494c(Context context, String str, C3531d dVar) {
        new CopyOnWriteArrayList();
        C2258v.m5629a(context);
        this.f5922a = context;
        C2258v.m5639b(str);
        this.f5923b = str;
        C2258v.m5629a(dVar);
        this.f5924c = dVar;
        List<C3512h> a = C3507f.m9606a(context, ComponentDiscoveryService.class).mo22138a();
        String a2 = C3622e.m9898a();
        Executor executor = f5920j;
        C3503d[] dVarArr = new C3503d[8];
        dVarArr[0] = C3503d.m9582a(context, Context.class, new Class[0]);
        dVarArr[1] = C3503d.m9582a(this, C3494c.class, new Class[0]);
        dVarArr[2] = C3503d.m9582a(dVar, C3531d.class, new Class[0]);
        dVarArr[3] = C3624g.m9902a("fire-android", "");
        dVarArr[4] = C3624g.m9902a("fire-core", "19.3.0");
        dVarArr[5] = a2 != null ? C3624g.m9902a("kotlin", a2) : null;
        dVarArr[6] = C3620c.m9894b();
        dVarArr[7] = C3552b.m9710a();
        this.f5925d = new C3516l(executor, a, dVarArr);
        this.f5928g = new C3526s<>(C3493b.m9545a(this, context));
    }

    /* renamed from: h */
    private void m9556h() {
        C2258v.m5641b(!this.f5927f.get(), "FirebaseApp was deleted");
    }

    @NonNull
    /* renamed from: i */
    public static C3494c m9557i() {
        C3494c cVar;
        synchronized (f5919i) {
            cVar = f5921k.get("[DEFAULT]");
            if (cVar == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + C2324o.m5803a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return cVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m9558j() {
        if (!UserManagerCompat.isUserUnlocked(this.f5922a)) {
            C3499e.m9571b(this.f5922a);
        } else {
            this.f5925d.mo22141a(mo22113f());
        }
    }

    @NonNull
    /* renamed from: c */
    public C3531d mo22109c() {
        m9556h();
        return this.f5924c;
    }

    /* renamed from: d */
    public String mo22110d() {
        return C2312c.m5770c(mo22108b().getBytes(Charset.defaultCharset())) + "+" + C2312c.m5770c(mo22109c().mo22164a().getBytes(Charset.defaultCharset()));
    }

    /* renamed from: e */
    public boolean mo22111e() {
        m9556h();
        return this.f5928g.get().mo22193a();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C3494c)) {
            return false;
        }
        return this.f5923b.equals(((C3494c) obj).mo22108b());
    }

    @VisibleForTesting
    /* renamed from: f */
    public boolean mo22113f() {
        return "[DEFAULT]".equals(mo22108b());
    }

    public int hashCode() {
        return this.f5923b.hashCode();
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("name", this.f5923b);
        a.mo17037a("options", this.f5924c);
        return a.toString();
    }

    @NonNull
    /* renamed from: b */
    public String mo22108b() {
        m9556h();
        return this.f5923b;
    }

    @NonNull
    /* renamed from: a */
    public Context mo22106a() {
        m9556h();
        return this.f5922a;
    }

    @Nullable
    /* renamed from: a */
    public static C3494c m9546a(@NonNull Context context) {
        synchronized (f5919i) {
            if (f5921k.containsKey("[DEFAULT]")) {
                C3494c i = m9557i();
                return i;
            }
            C3531d a = C3531d.m9659a(context);
            if (a == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            C3494c a2 = m9547a(context, a);
            return a2;
        }
    }

    @NonNull
    /* renamed from: a */
    public static C3494c m9547a(@NonNull Context context, @NonNull C3531d dVar) {
        return m9548a(context, dVar, "[DEFAULT]");
    }

    @NonNull
    /* renamed from: a */
    public static C3494c m9548a(@NonNull Context context, @NonNull C3531d dVar, @NonNull String str) {
        C3494c cVar;
        C3497c.m9568b(context);
        String a = m9550a(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (f5919i) {
            boolean z = !f5921k.containsKey(a);
            C2258v.m5641b(z, "FirebaseApp name " + a + " already exists!");
            C2258v.m5630a(context, "Application context cannot be null.");
            cVar = new C3494c(context, a, dVar);
            f5921k.put(a, cVar);
        }
        cVar.m9558j();
        return cVar;
    }

    /* renamed from: a */
    public <T> T mo22107a(Class cls) {
        m9556h();
        return this.f5925d.mo22121a(cls);
    }

    /* renamed from: a */
    static /* synthetic */ C3557a m9549a(C3494c cVar, Context context) {
        return new C3557a(context, cVar.mo22110d(), (C3549c) cVar.f5925d.mo22121a(C3549c.class));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9553a(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (C3496b bVar : this.f5929h) {
            bVar.mo22116a(z);
        }
    }

    /* renamed from: a */
    private static String m9550a(@NonNull String str) {
        return str.trim();
    }
}
