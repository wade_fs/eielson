package com.google.firebase.iid;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.VisibleForTesting;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.internal.C2246s;
import com.google.firebase.C3494c;
import com.google.firebase.p100g.C3553c;
import com.google.firebase.p104j.C3625h;
import java.io.IOException;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.firebase.iid.q0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3598q0 {

    /* renamed from: a */
    private final C3494c f6107a;

    /* renamed from: b */
    private final C3587l f6108b;

    /* renamed from: c */
    private final C3599r f6109c;

    /* renamed from: d */
    private final Executor f6110d;

    /* renamed from: e */
    private final C3625h f6111e;

    /* renamed from: f */
    private final C3553c f6112f;

    C3598q0(C3494c cVar, C3587l lVar, Executor executor, C3625h hVar, C3553c cVar2) {
        this(cVar, lVar, executor, new C3599r(cVar.mo22106a(), lVar), hVar, cVar2);
    }

    /* renamed from: a */
    public final C4065h<String> mo22263a(String str, String str2, String str3) {
        return m9824b(m9821a(str, str2, str3, new Bundle()));
    }

    /* renamed from: b */
    public final C4065h<Void> mo22265b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return m9820a(m9824b(m9821a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    /* renamed from: c */
    public final C4065h<Void> mo22266c(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        String valueOf2 = String.valueOf(str3);
        return m9820a(m9824b(m9821a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @VisibleForTesting
    private C3598q0(C3494c cVar, C3587l lVar, Executor executor, C3599r rVar, C3625h hVar, C3553c cVar2) {
        this.f6107a = cVar;
        this.f6108b = lVar;
        this.f6109c = rVar;
        this.f6110d = executor;
        this.f6111e = hVar;
        this.f6112f = cVar2;
    }

    /* renamed from: a */
    private final C4065h<Bundle> m9821a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.f6107a.mo22109c().mo22164a());
        bundle.putString("gmsv", Integer.toString(this.f6108b.mo22257d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.f6108b.mo22255b());
        bundle.putString("app_ver_name", this.f6108b.mo22256c());
        String a = C2246s.m5540a().mo17035a("firebase-iid");
        if ("UNKNOWN".equals(a)) {
            int i = C2169c.f3548a;
            StringBuilder sb = new StringBuilder(19);
            sb.append("unknown_");
            sb.append(i);
            a = sb.toString();
        }
        String valueOf = String.valueOf(a);
        bundle.putString("cliv", valueOf.length() != 0 ? "fiid-".concat(valueOf) : new String("fiid-"));
        C3553c.C3554a a2 = this.f6112f.mo22189a("fire-iid");
        if (a2 != C3553c.C3554a.NONE) {
            bundle.putString("Firebase-Client-Log-Type", Integer.toString(a2.mo22190a()));
            bundle.putString("Firebase-Client", this.f6111e.mo22305a());
        }
        C4066i iVar = new C4066i();
        this.f6110d.execute(new C3602s0(this, bundle, iVar));
        return iVar.mo23718a();
    }

    /* renamed from: b */
    private final C4065h<String> m9824b(C4065h<Bundle> hVar) {
        return hVar.mo23698a(this.f6110d, new C3606u0(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static String m9822a(Bundle bundle) {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null) {
                return string;
            }
            String string2 = bundle.getString("unregistered");
            if (string2 != null) {
                return string2;
            }
            String string3 = bundle.getString("error");
            if ("RST".equals(string3)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string3 != null) {
                throw new IOException(string3);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    /* renamed from: a */
    private final <T> C4065h<Void> m9820a(C4065h<T> hVar) {
        return hVar.mo23698a(C3566c.m9783a(), new C3600r0(this));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo22264a(Bundle bundle, C4066i iVar) {
        try {
            iVar.mo23720a((Boolean) this.f6109c.mo22267a(bundle));
        } catch (IOException e) {
            iVar.mo23719a(e);
        }
    }
}
