package com.google.firebase.iid;

import android.os.Bundle;

/* renamed from: com.google.firebase.iid.m */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3589m extends C3585k<Bundle> {
    C3589m(int i, int i2, Bundle bundle) {
        super(i, 1, bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22242a(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("data");
        if (bundle2 == null) {
            bundle2 = Bundle.EMPTY;
        }
        mo22250a((Object) bundle2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo22243a() {
        return false;
    }
}
