package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.CallSuper;
import androidx.annotation.MainThread;
import androidx.annotation.VisibleForTesting;
import com.google.android.gms.common.util.p091s.C2329b;
import com.google.firebase.iid.C3567c0;
import com.google.firebase.iid.C3615z;
import java.util.concurrent.ExecutorService;
import p119e.p144d.p145a.p157c.p161c.p164c.C4013a;
import p119e.p144d.p145a.p157c.p161c.p164c.C4014b;
import p119e.p144d.p145a.p157c.p161c.p164c.C4018f;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;
import p119e.p144d.p145a.p157c.p167e.C4069k;

@SuppressLint({"UnwrappedWakefulBroadcastReceiver"})
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public abstract class zzc extends Service {
    @VisibleForTesting

    /* renamed from: P */
    private final ExecutorService f6189P;

    /* renamed from: Q */
    private Binder f6190Q;

    /* renamed from: R */
    private final Object f6191R;

    /* renamed from: S */
    private int f6192S;

    /* renamed from: T */
    private int f6193T;

    public zzc() {
        C4014b a = C4013a.m12016a();
        String valueOf = String.valueOf(getClass().getSimpleName());
        this.f6189P = a.mo23647a(new C2329b(valueOf.length() != 0 ? "Firebase-".concat(valueOf) : new String("Firebase-")), C4018f.f7374a);
        this.f6191R = new Object();
        this.f6193T = 0;
    }

    /* access modifiers changed from: private */
    @MainThread
    /* renamed from: e */
    public final C4065h<Void> mo22348d(Intent intent) {
        if (mo22316b(intent)) {
            return C4069k.m12140a((Object) null);
        }
        C4066i iVar = new C4066i();
        this.f6189P.execute(new C3636f(this, intent, iVar));
        return iVar.mo23718a();
    }

    /* renamed from: f */
    private final void m9978f(Intent intent) {
        if (intent != null) {
            C3615z.m9883a(intent);
        }
        synchronized (this.f6191R) {
            this.f6193T--;
            if (this.f6193T == 0) {
                stopSelfResult(this.f6192S);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Intent mo22310a(Intent intent) {
        return intent;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo22347a(Intent intent, C4065h hVar) {
        m9978f(intent);
    }

    /* renamed from: b */
    public boolean mo22316b(Intent intent) {
        return false;
    }

    /* renamed from: c */
    public abstract void mo22317c(Intent intent);

    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.f6190Q == null) {
            this.f6190Q = new C3567c0(new C3637g(this));
        }
        return this.f6190Q;
    }

    @CallSuper
    public void onDestroy() {
        this.f6189P.shutdown();
        super.onDestroy();
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.f6191R) {
            this.f6192S = i2;
            this.f6193T++;
        }
        Intent a = mo22310a(intent);
        if (a == null) {
            m9978f(intent);
            return 2;
        }
        C4065h<Void> e = mo22348d(a);
        if (e.mo23713d()) {
            m9978f(intent);
            return 2;
        }
        e.mo23700a(C3639i.f6180a, new C3638h(this, intent));
        return 3;
    }
}
