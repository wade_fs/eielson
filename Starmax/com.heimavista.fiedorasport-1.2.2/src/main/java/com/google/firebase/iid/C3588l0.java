package com.google.firebase.iid;

import p119e.p144d.p145a.p157c.p167e.C4053a;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.l0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3588l0 implements C4053a {

    /* renamed from: a */
    private final FirebaseInstanceId f6088a;

    /* renamed from: b */
    private final String f6089b;

    /* renamed from: c */
    private final String f6090c;

    C3588l0(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.f6088a = firebaseInstanceId;
        this.f6089b = str;
        this.f6090c = str2;
    }

    /* renamed from: a */
    public final Object mo16773a(C4065h hVar) {
        return this.f6088a.mo22194a(this.f6089b, this.f6090c, hVar);
    }
}
