package com.google.firebase.iid;

import android.os.Bundle;
import androidx.annotation.NonNull;
import java.io.IOException;
import p119e.p144d.p145a.p157c.p167e.C4053a;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.u0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3606u0 implements C4053a<Bundle, String> {

    /* renamed from: a */
    private final /* synthetic */ C3598q0 f6130a;

    C3606u0(C3598q0 q0Var) {
        this.f6130a = q0Var;
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo16773a(@NonNull C4065h hVar) {
        return C3598q0.m9822a((Bundle) hVar.mo23705a(IOException.class));
    }
}
