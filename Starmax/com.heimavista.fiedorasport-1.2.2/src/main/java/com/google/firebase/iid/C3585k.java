package com.google.firebase.iid;

import android.os.Bundle;
import android.util.Log;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.firebase.iid.k */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
abstract class C3585k<T> {

    /* renamed from: a */
    final int f6079a;

    /* renamed from: b */
    final C4066i<T> f6080b = new C4066i<>();

    /* renamed from: c */
    final int f6081c;

    /* renamed from: d */
    final Bundle f6082d;

    C3585k(int i, int i2, Bundle bundle) {
        this.f6079a = i;
        this.f6081c = i2;
        this.f6082d = bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo22242a(Bundle bundle);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22250a(Object obj) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length());
            sb.append("Finishing ");
            sb.append(valueOf);
            sb.append(" with ");
            sb.append(valueOf2);
            Log.d("MessengerIpcClient", sb.toString());
        }
        this.f6080b.mo23720a((Boolean) obj);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract boolean mo22243a();

    public String toString() {
        int i = this.f6081c;
        int i2 = this.f6079a;
        boolean a = mo22243a();
        StringBuilder sb = new StringBuilder(55);
        sb.append("Request { what=");
        sb.append(i);
        sb.append(" id=");
        sb.append(i2);
        sb.append(" oneWay=");
        sb.append(a);
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22249a(C3583j jVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(jVar);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length());
            sb.append("Failing ");
            sb.append(valueOf);
            sb.append(" with ");
            sb.append(valueOf2);
            Log.d("MessengerIpcClient", sb.toString());
        }
        this.f6080b.mo23719a(jVar);
    }
}
