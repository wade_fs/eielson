package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;
import java.util.Set;

/* renamed from: com.google.firebase.components.a */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
abstract class C3500a implements C3506e {
    C3500a() {
    }

    /* renamed from: a */
    public <T> T mo22121a(Class cls) {
        C3556a b = mo22136b(cls);
        if (b == null) {
            return null;
        }
        return b.get();
    }

    /* renamed from: d */
    public <T> Set<T> mo22122d(Class<T> cls) {
        return mo22137c(cls).get();
    }
}
