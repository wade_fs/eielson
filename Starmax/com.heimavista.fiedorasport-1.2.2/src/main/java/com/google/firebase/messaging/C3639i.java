package com.google.firebase.messaging;

import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.messaging.i */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3639i implements Executor {

    /* renamed from: a */
    static final Executor f6180a = new C3639i();

    private C3639i() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
