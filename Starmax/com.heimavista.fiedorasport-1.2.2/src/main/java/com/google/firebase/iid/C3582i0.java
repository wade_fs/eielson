package com.google.firebase.iid;

import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.iid.i0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3582i0 implements Executor {

    /* renamed from: a */
    static final Executor f6076a = new C3582i0();

    private C3582i0() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
