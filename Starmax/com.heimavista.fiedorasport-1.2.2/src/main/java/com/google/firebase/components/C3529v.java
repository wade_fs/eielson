package com.google.firebase.components;

import com.google.firebase.p099f.C3549c;
import com.google.firebase.p101h.C3556a;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.google.firebase.components.v */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final class C3529v extends C3500a {

    /* renamed from: a */
    private final Set<Class<?>> f5976a;

    /* renamed from: b */
    private final Set<Class<?>> f5977b;

    /* renamed from: c */
    private final Set<Class<?>> f5978c;

    /* renamed from: d */
    private final Set<Class<?>> f5979d;

    /* renamed from: e */
    private final Set<Class<?>> f5980e;

    /* renamed from: f */
    private final C3506e f5981f;

    /* renamed from: com.google.firebase.components.v$a */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    private static class C3530a implements C3549c {
        public C3530a(Set<Class<?>> set, C3549c cVar) {
        }
    }

    C3529v(C3503d<?> dVar, C3506e eVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (C3521n nVar : dVar.mo22123a()) {
            if (nVar.mo22152b()) {
                if (nVar.mo22154d()) {
                    hashSet3.add(nVar.mo22151a());
                } else {
                    hashSet.add(nVar.mo22151a());
                }
            } else if (nVar.mo22154d()) {
                hashSet4.add(nVar.mo22151a());
            } else {
                hashSet2.add(nVar.mo22151a());
            }
        }
        if (!dVar.mo22126d().isEmpty()) {
            hashSet.add(C3549c.class);
        }
        this.f5976a = Collections.unmodifiableSet(hashSet);
        this.f5977b = Collections.unmodifiableSet(hashSet2);
        this.f5978c = Collections.unmodifiableSet(hashSet3);
        this.f5979d = Collections.unmodifiableSet(hashSet4);
        this.f5980e = dVar.mo22126d();
        this.f5981f = eVar;
    }

    /* renamed from: a */
    public <T> T mo22121a(Class<T> cls) {
        if (this.f5976a.contains(cls)) {
            T a = this.f5981f.mo22121a(cls);
            if (!cls.equals(C3549c.class)) {
                return a;
            }
            return new C3530a(this.f5980e, (C3549c) a);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }

    /* renamed from: b */
    public <T> C3556a<T> mo22136b(Class<T> cls) {
        if (this.f5977b.contains(cls)) {
            return this.f5981f.mo22136b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    /* renamed from: c */
    public <T> C3556a<Set<T>> mo22137c(Class<T> cls) {
        if (this.f5979d.contains(cls)) {
            return this.f5981f.mo22137c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    /* renamed from: d */
    public <T> Set<T> mo22122d(Class<T> cls) {
        if (this.f5978c.contains(cls)) {
            return this.f5981f.mo22122d(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }
}
