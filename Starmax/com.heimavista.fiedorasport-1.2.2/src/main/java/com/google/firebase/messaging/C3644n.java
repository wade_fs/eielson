package com.google.firebase.messaging;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p157c.p161c.p164c.C4021i;
import p119e.p144d.p145a.p157c.p161c.p164c.C4022j;
import p119e.p144d.p145a.p157c.p161c.p164c.C4024l;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4069k;

/* renamed from: com.google.firebase.messaging.n */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final class C3644n implements Closeable {

    /* renamed from: P */
    private final URL f6184P;
    @Nullable

    /* renamed from: Q */
    private C4065h<Bitmap> f6185Q;
    @Nullable

    /* renamed from: R */
    private volatile InputStream f6186R;

    private C3644n(URL url) {
        this.f6184P = url;
    }

    @Nullable
    /* renamed from: b */
    public static C3644n m9934b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new C3644n(new URL(str));
        } catch (MalformedURLException unused) {
            String valueOf = String.valueOf(str);
            Log.w("FirebaseMessaging", valueOf.length() != 0 ? "Not downloading image, bad URL: ".concat(valueOf) : new String("Not downloading image, bad URL: "));
            return null;
        }
    }

    /* renamed from: c */
    private final byte[] m9935c() {
        URLConnection openConnection = this.f6184P.openConnection();
        if (openConnection.getContentLength() <= 1048576) {
            InputStream inputStream = openConnection.getInputStream();
            try {
                this.f6186R = inputStream;
                byte[] a = C4021i.m12023a(C4021i.m12022a(inputStream, 1048577));
                if (inputStream != null) {
                    inputStream.close();
                }
                if (Log.isLoggable("FirebaseMessaging", 2)) {
                    int length = a.length;
                    String valueOf = String.valueOf(this.f6184P);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                    sb.append("Downloaded ");
                    sb.append(length);
                    sb.append(" bytes from ");
                    sb.append(valueOf);
                    Log.v("FirebaseMessaging", sb.toString());
                }
                if (a.length <= 1048576) {
                    return a;
                }
                throw new IOException("Image exceeds max size of 1048576");
            } catch (Throwable th) {
                C4024l.m12027a(th, th);
            }
        } else {
            throw new IOException("Content-Length exceeds max size of 1048576");
        }
        throw th;
    }

    /* renamed from: a */
    public final void mo22329a(Executor executor) {
        this.f6185Q = C4069k.m12141a(executor, new C3643m(this));
    }

    public final void close() {
        try {
            C4022j.m12025a(this.f6186R);
        } catch (NullPointerException e) {
            Log.e("FirebaseMessaging", "Failed to close the image download stream.", e);
        }
    }

    /* renamed from: a */
    public final C4065h<Bitmap> mo22328a() {
        C4065h<Bitmap> hVar = this.f6185Q;
        C2258v.m5629a(hVar);
        return hVar;
    }

    /* renamed from: b */
    public final Bitmap mo22330b() {
        String valueOf = String.valueOf(this.f6184P);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Starting download of: ");
        sb.append(valueOf);
        Log.i("FirebaseMessaging", sb.toString());
        byte[] c = m9935c();
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(c, 0, c.length);
        if (decodeByteArray != null) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf2 = String.valueOf(this.f6184P);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 31);
                sb2.append("Successfully downloaded image: ");
                sb2.append(valueOf2);
                Log.d("FirebaseMessaging", sb2.toString());
            }
            return decodeByteArray;
        }
        String valueOf3 = String.valueOf(this.f6184P);
        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf3).length() + 24);
        sb3.append("Failed to decode image: ");
        sb3.append(valueOf3);
        throw new IOException(sb3.toString());
    }
}
