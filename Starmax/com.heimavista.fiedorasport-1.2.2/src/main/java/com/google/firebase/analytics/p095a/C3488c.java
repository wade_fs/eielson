package com.google.firebase.analytics.p095a;

import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.analytics.a.c */
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
final /* synthetic */ class C3488c implements Executor {

    /* renamed from: a */
    static final Executor f5908a = new C3488c();

    private C3488c() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
