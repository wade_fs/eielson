package com.google.firebase.p096e;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.google.firebase.e.d */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public interface C3536d<T> extends C3534b<T, C3537e> {
    /* renamed from: a */
    /* synthetic */ void mo13453a(@Nullable TValue tvalue, @NonNull TContext tcontext);
}
