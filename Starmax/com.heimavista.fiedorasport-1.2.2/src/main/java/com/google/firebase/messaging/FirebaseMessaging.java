package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.google.firebase.C3494c;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.regex.Pattern;
import p119e.p144d.p145a.p146a.C3882g;

/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public class FirebaseMessaging {
    @VisibleForTesting
    @SuppressLint({"FirebaseUnknownNullness"})
    @Nullable

    /* renamed from: b */
    static C3882g f6160b;

    /* renamed from: a */
    private final FirebaseInstanceId f6161a;

    static {
        Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
    }

    FirebaseMessaging(C3494c cVar, FirebaseInstanceId firebaseInstanceId, C3882g gVar) {
        cVar.mo22106a();
        f6160b = gVar;
        this.f6161a = firebaseInstanceId;
    }

    @NonNull
    /* renamed from: a */
    public static synchronized FirebaseMessaging m9904a() {
        FirebaseMessaging instance;
        synchronized (FirebaseMessaging.class) {
            instance = getInstance(C3494c.m9557i());
        }
        return instance;
    }

    @NonNull
    @Keep
    static synchronized FirebaseMessaging getInstance(@NonNull C3494c cVar) {
        FirebaseMessaging firebaseMessaging;
        Class<FirebaseMessaging> cls = FirebaseMessaging.class;
        synchronized (cls) {
            firebaseMessaging = (FirebaseMessaging) cVar.mo22107a(cls);
        }
        return firebaseMessaging;
    }

    /* renamed from: a */
    public void mo22307a(boolean z) {
        this.f6161a.mo22205b(z);
    }
}
