package com.google.firebase.iid;

import android.os.Handler;
import android.os.Message;

/* renamed from: com.google.firebase.iid.e1 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3574e1 implements Handler.Callback {

    /* renamed from: P */
    private final C3565b1 f6064P;

    C3574e1(C3565b1 b1Var) {
        this.f6064P = b1Var;
    }

    public final boolean handleMessage(Message message) {
        return this.f6064P.mo22224a(message);
    }
}
