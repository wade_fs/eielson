package com.google.firebase.iid;

import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.gms.stats.C3277a;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.firebase.iid.z */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3615z {

    /* renamed from: a */
    private static final long f6147a = TimeUnit.MINUTES.toMillis(1);

    /* renamed from: b */
    private static final Object f6148b = new Object();

    /* renamed from: c */
    private static C3277a f6149c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.firebase.iid.z.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.google.firebase.iid.z.a(android.content.Context, android.content.Intent):android.content.ComponentName
      com.google.firebase.iid.z.a(android.content.Intent, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0032, code lost:
        return r4;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.ComponentName m9882a(@androidx.annotation.NonNull android.content.Context r4, @androidx.annotation.NonNull android.content.Intent r5) {
        /*
            java.lang.Object r0 = com.google.firebase.iid.C3615z.f6148b
            monitor-enter(r0)
            com.google.android.gms.stats.a r1 = com.google.firebase.iid.C3615z.f6149c     // Catch:{ all -> 0x0033 }
            r2 = 1
            if (r1 != 0) goto L_0x0015
            com.google.android.gms.stats.a r1 = new com.google.android.gms.stats.a     // Catch:{ all -> 0x0033 }
            java.lang.String r3 = "wake:com.google.firebase.iid.WakeLockHolder"
            r1.<init>(r4, r2, r3)     // Catch:{ all -> 0x0033 }
            com.google.firebase.iid.C3615z.f6149c = r1     // Catch:{ all -> 0x0033 }
            r1.mo19497a(r2)     // Catch:{ all -> 0x0033 }
        L_0x0015:
            java.lang.String r1 = "com.google.firebase.iid.WakeLockHolder.wakefulintent"
            r3 = 0
            boolean r1 = r5.getBooleanExtra(r1, r3)     // Catch:{ all -> 0x0033 }
            m9884a(r5, r2)     // Catch:{ all -> 0x0033 }
            android.content.ComponentName r4 = r4.startService(r5)     // Catch:{ all -> 0x0033 }
            if (r4 != 0) goto L_0x0028
            r4 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            return r4
        L_0x0028:
            if (r1 != 0) goto L_0x0031
            com.google.android.gms.stats.a r5 = com.google.firebase.iid.C3615z.f6149c     // Catch:{ all -> 0x0033 }
            long r1 = com.google.firebase.iid.C3615z.f6147a     // Catch:{ all -> 0x0033 }
            r5.mo19496a(r1)     // Catch:{ all -> 0x0033 }
        L_0x0031:
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            return r4
        L_0x0033:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.C3615z.m9882a(android.content.Context, android.content.Intent):android.content.ComponentName");
    }

    /* renamed from: a */
    private static void m9884a(@NonNull Intent intent, boolean z) {
        intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.firebase.iid.z.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.google.firebase.iid.z.a(android.content.Context, android.content.Intent):android.content.ComponentName
      com.google.firebase.iid.z.a(android.content.Intent, boolean):void */
    /* renamed from: a */
    public static void m9883a(@NonNull Intent intent) {
        synchronized (f6148b) {
            if (f6149c != null && intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false)) {
                m9884a(intent, false);
                f6149c.mo19495a();
            }
        }
    }
}
