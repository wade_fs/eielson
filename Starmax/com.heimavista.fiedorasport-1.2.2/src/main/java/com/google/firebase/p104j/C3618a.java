package com.google.firebase.p104j;

/* renamed from: com.google.firebase.j.a */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
final class C3618a extends C3623f {

    /* renamed from: a */
    private final String f6153a;

    /* renamed from: b */
    private final String f6154b;

    C3618a(String str, String str2) {
        if (str != null) {
            this.f6153a = str;
            if (str2 != null) {
                this.f6154b = str2;
                return;
            }
            throw new NullPointerException("Null version");
        }
        throw new NullPointerException("Null libraryName");
    }

    /* renamed from: a */
    public String mo22300a() {
        return this.f6153a;
    }

    /* renamed from: b */
    public String mo22301b() {
        return this.f6154b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3623f)) {
            return false;
        }
        C3623f fVar = (C3623f) obj;
        if (!this.f6153a.equals(super.mo22300a()) || !this.f6154b.equals(super.mo22301b())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.f6153a.hashCode() ^ 1000003) * 1000003) ^ this.f6154b.hashCode();
    }

    public String toString() {
        return "LibraryVersion{libraryName=" + this.f6153a + ", version=" + this.f6154b + "}";
    }
}
