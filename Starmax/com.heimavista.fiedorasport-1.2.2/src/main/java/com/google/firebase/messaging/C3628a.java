package com.google.firebase.messaging;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.internal.C2258v;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.firebase.messaging.a */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final class C3628a {

    /* renamed from: a */
    private final String f6165a;

    /* renamed from: b */
    private final Intent f6166b;

    /* renamed from: com.google.firebase.messaging.a$a */
    /* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
    static class C3629a implements C3536d<C3628a> {
        C3629a() {
        }

        /* renamed from: a */
        public final /* synthetic */ void mo13453a(Object obj, Object obj2) {
            C3628a aVar = (C3628a) obj;
            C3537e eVar = (C3537e) obj2;
            Intent a = aVar.mo22320a();
            eVar.mo22171a("ttl", C3646p.m9968f(a));
            eVar.mo22173a(NotificationCompat.CATEGORY_EVENT, aVar.mo22321b());
            eVar.mo22173a("instanceId", C3646p.m9963c());
            eVar.mo22171a("priority", C3646p.m9975m(a));
            eVar.mo22173a("packageName", C3646p.m9961b());
            eVar.mo22173a("sdkPlatform", "ANDROID");
            eVar.mo22173a("messageType", C3646p.m9973k(a));
            String j = C3646p.m9972j(a);
            if (j != null) {
                eVar.mo22173a("messageId", j);
            }
            String l = C3646p.m9974l(a);
            if (l != null) {
                eVar.mo22173a("topic", l);
            }
            String g = C3646p.m9969g(a);
            if (g != null) {
                eVar.mo22173a("collapseKey", g);
            }
            if (C3646p.m9971i(a) != null) {
                eVar.mo22173a("analyticsLabel", C3646p.m9971i(a));
            }
            if (C3646p.m9970h(a) != null) {
                eVar.mo22173a("composerLabel", C3646p.m9970h(a));
            }
            String d = C3646p.m9965d();
            if (d != null) {
                eVar.mo22173a("projectNumber", d);
            }
        }
    }

    /* renamed from: com.google.firebase.messaging.a$b */
    /* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
    static final class C3630b implements C3536d<C3631c> {
        C3630b() {
        }

        /* renamed from: a */
        public final /* synthetic */ void mo13453a(Object obj, Object obj2) {
            ((C3537e) obj2).mo22173a("messaging_client_event", ((C3631c) obj).mo22322a());
        }
    }

    /* renamed from: com.google.firebase.messaging.a$c */
    /* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
    static final class C3631c {

        /* renamed from: a */
        private final C3628a f6167a;

        C3631c(@NonNull C3628a aVar) {
            C2258v.m5629a(aVar);
            this.f6167a = aVar;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        /* renamed from: a */
        public final C3628a mo22322a() {
            return this.f6167a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String */
    C3628a(@NonNull String str, @NonNull Intent intent) {
        C2258v.m5631a(str, (Object) "evenType must be non-null");
        this.f6165a = str;
        C2258v.m5630a(intent, "intent must be non-null");
        this.f6166b = intent;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public final Intent mo22320a() {
        return this.f6166b;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public final String mo22321b() {
        return this.f6165a;
    }
}
