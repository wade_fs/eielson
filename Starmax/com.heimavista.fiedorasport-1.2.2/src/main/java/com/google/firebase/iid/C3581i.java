package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

/* renamed from: com.google.firebase.iid.i */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3581i {

    /* renamed from: a */
    private final Messenger f6074a;

    /* renamed from: b */
    private final zzf f6075b;

    C3581i(IBinder iBinder) {
        String interfaceDescriptor = iBinder.getInterfaceDescriptor();
        if ("android.os.IMessenger".equals(interfaceDescriptor)) {
            this.f6074a = new Messenger(iBinder);
            this.f6075b = null;
        } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
            this.f6075b = new zzf(iBinder);
            this.f6074a = null;
        } else {
            String valueOf = String.valueOf(interfaceDescriptor);
            Log.w("MessengerIpcClient", valueOf.length() != 0 ? "Invalid interface descriptor: ".concat(valueOf) : new String("Invalid interface descriptor: "));
            throw new RemoteException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22245a(Message message) {
        Messenger messenger = this.f6074a;
        if (messenger != null) {
            messenger.send(message);
            return;
        }
        zzf zzf = this.f6075b;
        if (zzf != null) {
            zzf.mo22294a(message);
            return;
        }
        throw new IllegalStateException("Both messengers are null");
    }
}
