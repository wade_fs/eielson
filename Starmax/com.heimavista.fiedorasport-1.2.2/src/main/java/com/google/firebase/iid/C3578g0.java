package com.google.firebase.iid;

import android.content.Intent;
import android.util.Log;

/* renamed from: com.google.firebase.iid.g0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3578g0 implements Runnable {

    /* renamed from: P */
    private final C3580h0 f6068P;

    /* renamed from: Q */
    private final Intent f6069Q;

    C3578g0(C3580h0 h0Var, Intent intent) {
        this.f6068P = h0Var;
        this.f6069Q = intent;
    }

    public final void run() {
        C3580h0 h0Var = this.f6068P;
        String action = this.f6069Q.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("FirebaseInstanceId", sb.toString());
        h0Var.mo22244a();
    }
}
