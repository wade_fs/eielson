package com.google.firebase.p096e;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.google.firebase.e.g */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public interface C3539g {
    @NonNull
    /* renamed from: a */
    C3539g mo22175a(@Nullable String str);

    @NonNull
    /* renamed from: a */
    C3539g mo22176a(boolean z);
}
