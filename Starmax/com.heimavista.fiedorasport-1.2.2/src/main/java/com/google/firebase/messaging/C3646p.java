package com.google.firebase.messaging;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.google.firebase.C3494c;
import com.google.firebase.analytics.p095a.C3486a;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.C3628a;
import com.google.firebase.p096e.C3533a;
import com.google.firebase.p096e.C3535c;
import com.google.firebase.p096e.p098i.C3543c;
import p119e.p144d.p145a.p146a.C3878c;
import p119e.p144d.p145a.p146a.C3881f;

/* renamed from: com.google.firebase.messaging.p */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public final class C3646p {

    /* renamed from: a */
    private static final C3533a f6188a;

    static {
        C3543c cVar = new C3543c();
        cVar.mo22178a(C3628a.C3631c.class, new C3628a.C3630b());
        cVar.mo22178a(C3628a.class, new C3628a.C3629a());
        f6188a = cVar.mo22177a();
    }

    /* renamed from: a */
    public static void m9958a(Intent intent, @Nullable C3881f<String> fVar) {
        m9959a("_nr", intent);
        if (fVar != null) {
            try {
                fVar.mo22309a(C3878c.m11675a(f6188a.encode(new C3628a.C3631c(new C3628a("MESSAGE_DELIVERED", intent)))));
            } catch (C3535c unused) {
                Log.d("FirebaseMessaging", "Failed to encode big query analytics payload. Skip sending");
            }
        }
    }

    /* renamed from: b */
    public static void m9962b(Intent intent) {
        m9959a("_nd", intent);
    }

    /* renamed from: c */
    public static void m9964c(Intent intent) {
        m9959a("_nf", intent);
    }

    /* renamed from: d */
    public static boolean m9966d(Intent intent) {
        if (intent == null || m9976n(intent)) {
            return false;
        }
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(intent.getStringExtra("google.c.a.e"));
    }

    /* renamed from: e */
    public static boolean m9967e(Intent intent) {
        if (intent == null || m9976n(intent)) {
            return false;
        }
        return m9960a();
    }

    @NonNull
    /* renamed from: f */
    static int m9968f(Intent intent) {
        Object obj = intent.getExtras().get("google.ttl");
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (!(obj instanceof String)) {
            return 0;
        }
        try {
            return Integer.parseInt((String) obj);
        } catch (NumberFormatException unused) {
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
            sb.append("Invalid TTL: ");
            sb.append(valueOf);
            Log.w("FirebaseMessaging", sb.toString());
            return 0;
        }
    }

    @Nullable
    /* renamed from: g */
    static String m9969g(Intent intent) {
        return intent.getStringExtra("collapse_key");
    }

    @Nullable
    /* renamed from: h */
    static String m9970h(Intent intent) {
        return intent.getStringExtra("google.c.a.c_l");
    }

    @Nullable
    /* renamed from: i */
    static String m9971i(Intent intent) {
        return intent.getStringExtra("google.c.a.m_l");
    }

    @Nullable
    /* renamed from: j */
    static String m9972j(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    @NonNull
    /* renamed from: k */
    static String m9973k(Intent intent) {
        return (intent.getExtras() == null || !C3645o.m9939a(intent.getExtras())) ? "DATA_MESSAGE" : "DISPLAY_NOTIFICATION";
    }

    @Nullable
    /* renamed from: l */
    static String m9974l(Intent intent) {
        String stringExtra = intent.getStringExtra("from");
        if (stringExtra == null || !stringExtra.startsWith("/topics/")) {
            return null;
        }
        return stringExtra;
    }

    @NonNull
    /* renamed from: m */
    static int m9975m(Intent intent) {
        String stringExtra = intent.getStringExtra("google.delivered_priority");
        if (stringExtra == null) {
            if (AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(intent.getStringExtra("google.priority_reduced"))) {
                return 2;
            }
            stringExtra = intent.getStringExtra("google.priority");
        }
        if ("high".equals(stringExtra)) {
            return 1;
        }
        if ("normal".equals(stringExtra)) {
            return 2;
        }
        return 0;
    }

    /* renamed from: n */
    private static boolean m9976n(Intent intent) {
        return "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(intent.getAction());
    }

    @NonNull
    /* renamed from: b */
    static String m9961b() {
        return C3494c.m9557i().mo22106a().getPackageName();
    }

    @NonNull
    /* renamed from: c */
    static String m9963c() {
        return FirebaseInstanceId.getInstance(C3494c.m9557i()).mo22197a();
    }

    @Nullable
    /* renamed from: d */
    static String m9965d() {
        C3494c i = C3494c.m9557i();
        String b = i.mo22109c().mo22165b();
        if (b != null) {
            return b;
        }
        String a = i.mo22109c().mo22164a();
        if (!a.startsWith("1:")) {
            return a;
        }
        String[] split = a.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    /* renamed from: a */
    public static void m9957a(Intent intent) {
        if (intent != null) {
            if (AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(intent.getStringExtra("google.c.a.tc"))) {
                C3486a aVar = (C3486a) C3494c.m9557i().mo22107a(C3486a.class);
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
                }
                if (aVar != null) {
                    String stringExtra = intent.getStringExtra("google.c.a.c_id");
                    aVar.mo22100a("fcm", "_ln", stringExtra);
                    Bundle bundle = new Bundle();
                    bundle.putString(ShareConstants.FEED_SOURCE_PARAM, "Firebase");
                    bundle.putString("medium", "notification");
                    bundle.putString("campaign", stringExtra);
                    aVar.mo22099a("fcm", "_cmp", bundle);
                } else {
                    Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
                }
            } else if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
            }
        }
        m9959a("_no", intent);
    }

    /* renamed from: a */
    static boolean m9960a() {
        ApplicationInfo applicationInfo;
        try {
            C3494c.m9557i();
            Context a = C3494c.m9557i().mo22106a();
            SharedPreferences sharedPreferences = a.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("export_to_big_query")) {
                return sharedPreferences.getBoolean("export_to_big_query", false);
            }
            try {
                PackageManager packageManager = a.getPackageManager();
                if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("delivery_metrics_exported_to_big_query_enabled"))) {
                    return applicationInfo.metaData.getBoolean("delivery_metrics_exported_to_big_query_enabled", false);
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
            return false;
        } catch (IllegalStateException unused2) {
            Log.i("FirebaseMessaging", "FirebaseApp has not being initialized. Device might be in direct boot mode. Skip exporting delivery metrics to Big Query");
            return false;
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    private static void m9959a(String str, Intent intent) {
        Bundle bundle = new Bundle();
        String stringExtra = intent.getStringExtra("google.c.a.c_id");
        if (stringExtra != null) {
            bundle.putString("_nmid", stringExtra);
        }
        String stringExtra2 = intent.getStringExtra("google.c.a.c_l");
        if (stringExtra2 != null) {
            bundle.putString("_nmn", stringExtra2);
        }
        String stringExtra3 = intent.getStringExtra("google.c.a.m_l");
        if (!TextUtils.isEmpty(stringExtra3)) {
            bundle.putString("label", stringExtra3);
        }
        String stringExtra4 = intent.getStringExtra("google.c.a.m_c");
        if (!TextUtils.isEmpty(stringExtra4)) {
            bundle.putString("message_channel", stringExtra4);
        }
        String l = m9974l(intent);
        if (l != null) {
            bundle.putString("_nt", l);
        }
        String stringExtra5 = intent.getStringExtra("google.c.a.ts");
        if (stringExtra5 != null) {
            try {
                bundle.putInt("_nmt", Integer.parseInt(stringExtra5));
            } catch (NumberFormatException e) {
                Log.w("FirebaseMessaging", "Error while parsing timestamp in GCM event", e);
            }
        }
        String stringExtra6 = intent.hasExtra("google.c.a.udt") ? intent.getStringExtra("google.c.a.udt") : null;
        if (stringExtra6 != null) {
            try {
                bundle.putInt("_ndt", Integer.parseInt(stringExtra6));
            } catch (NumberFormatException e2) {
                Log.w("FirebaseMessaging", "Error while parsing use_device_time in GCM event", e2);
            }
        }
        String str2 = (intent.getExtras() == null || !C3645o.m9939a(intent.getExtras())) ? "data" : ServerProtocol.DIALOG_PARAM_DISPLAY;
        if ("_nr".equals(str) || "_nf".equals(str)) {
            bundle.putString("_nmc", str2);
        }
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(valueOf).length());
            sb.append("Logging to scion event=");
            sb.append(str);
            sb.append(" scionPayload=");
            sb.append(valueOf);
            Log.d("FirebaseMessaging", sb.toString());
        }
        C3486a aVar = (C3486a) C3494c.m9557i().mo22107a(C3486a.class);
        if (aVar != null) {
            aVar.mo22099a("fcm", str, bundle);
        } else {
            Log.w("FirebaseMessaging", "Unable to log event: analytics library is missing");
        }
    }
}
