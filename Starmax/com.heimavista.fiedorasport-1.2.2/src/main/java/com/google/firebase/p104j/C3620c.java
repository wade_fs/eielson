package com.google.firebase.p104j;

import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3521n;
import java.util.Iterator;
import java.util.Set;

/* renamed from: com.google.firebase.j.c */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public class C3620c implements C3625h {

    /* renamed from: a */
    private final String f6156a;

    /* renamed from: b */
    private final C3621d f6157b;

    C3620c(Set<C3623f> set, C3621d dVar) {
        this.f6156a = m9893a(set);
        this.f6157b = dVar;
    }

    /* renamed from: b */
    public static C3503d<C3625h> m9894b() {
        C3503d.C3505b a = C3503d.m9579a(C3625h.class);
        a.mo22133a(C3521n.m9638c(C3623f.class));
        a.mo22132a(C3619b.m9890a());
        return a.mo22134b();
    }

    /* renamed from: a */
    public String mo22305a() {
        if (this.f6157b.mo22306a().isEmpty()) {
            return this.f6156a;
        }
        return this.f6156a + ' ' + m9893a(this.f6157b.mo22306a());
    }

    /* renamed from: a */
    private static String m9893a(Set<C3623f> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<C3623f> it = set.iterator();
        while (it.hasNext()) {
            C3623f next = it.next();
            sb.append(next.mo22300a());
            sb.append('/');
            sb.append(next.mo22301b());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    static /* synthetic */ C3625h m9892a(C3506e eVar) {
        return new C3620c(eVar.mo22122d(C3623f.class), C3621d.m9896b());
    }
}
