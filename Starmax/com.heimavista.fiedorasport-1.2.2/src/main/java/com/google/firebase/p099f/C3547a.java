package com.google.firebase.p099f;

/* renamed from: com.google.firebase.f.a */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public class C3547a<T> {

    /* renamed from: a */
    private final Class<T> f6004a;

    /* renamed from: b */
    private final T f6005b;

    /* renamed from: a */
    public T mo22186a() {
        return this.f6005b;
    }

    /* renamed from: b */
    public Class<T> mo22187b() {
        return this.f6004a;
    }

    public String toString() {
        return String.format("Event{type: %s, payload: %s}", this.f6004a, this.f6005b);
    }
}
