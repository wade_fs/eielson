package com.google.firebase.p102i;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.content.ContextCompat;
import com.google.firebase.p099f.C3549c;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.firebase.i.a */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public class C3557a {

    /* renamed from: a */
    private final Context f6016a;

    /* renamed from: b */
    private final SharedPreferences f6017b;

    /* renamed from: c */
    private final AtomicBoolean f6018c = new AtomicBoolean(m9719b());

    public C3557a(Context context, String str, C3549c cVar) {
        this.f6016a = m9718a(context);
        this.f6017b = context.getSharedPreferences("com.google.firebase.common.prefs:" + str, 0);
    }

    /* renamed from: a */
    private static Context m9718a(Context context) {
        return (Build.VERSION.SDK_INT < 24 || ContextCompat.isDeviceProtectedStorage(context)) ? context : ContextCompat.createDeviceProtectedStorageContext(context);
    }

    /* renamed from: b */
    private boolean m9719b() {
        ApplicationInfo applicationInfo;
        if (this.f6017b.contains("firebase_data_collection_default_enabled")) {
            return this.f6017b.getBoolean("firebase_data_collection_default_enabled", true);
        }
        try {
            PackageManager packageManager = this.f6016a.getPackageManager();
            if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.f6016a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_data_collection_default_enabled"))) {
                return applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo22193a() {
        return this.f6018c.get();
    }
}
