package com.google.firebase.messaging;

import androidx.core.app.NotificationCompat;

/* renamed from: com.google.firebase.messaging.c */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public final class C3633c {

    /* renamed from: a */
    public final NotificationCompat.Builder f6168a;

    /* renamed from: b */
    public final String f6169b;

    C3633c(NotificationCompat.Builder builder, String str, int i) {
        this.f6168a = builder;
        this.f6169b = str;
    }
}
