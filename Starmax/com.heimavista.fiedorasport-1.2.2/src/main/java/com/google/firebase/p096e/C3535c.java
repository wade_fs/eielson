package com.google.firebase.p096e;

import androidx.annotation.NonNull;

/* renamed from: com.google.firebase.e.c */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public final class C3535c extends Exception {
    public C3535c(@NonNull String str) {
        super(str);
    }

    public C3535c(@NonNull String str, @NonNull Exception exc) {
        super(str, exc);
    }
}
