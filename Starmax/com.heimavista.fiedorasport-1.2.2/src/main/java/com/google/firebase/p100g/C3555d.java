package com.google.firebase.p100g;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: com.google.firebase.g.d */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
class C3555d {

    /* renamed from: b */
    private static C3555d f6014b;

    /* renamed from: a */
    private final SharedPreferences f6015a;

    private C3555d(Context context) {
        this.f6015a = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
    }

    /* renamed from: a */
    static synchronized C3555d m9715a(Context context) {
        C3555d dVar;
        synchronized (C3555d.class) {
            if (f6014b == null) {
                f6014b = new C3555d(context);
            }
            dVar = f6014b;
        }
        return dVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized boolean mo22192a(String str, long j) {
        if (!this.f6015a.contains(str)) {
            this.f6015a.edit().putLong(str, j).apply();
            return true;
        } else if (j - this.f6015a.getLong(str, -1) < 86400000) {
            return false;
        } else {
            this.f6015a.edit().putLong(str, j).apply();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized boolean mo22191a(long j) {
        return mo22192a("fire-global", j);
    }
}
