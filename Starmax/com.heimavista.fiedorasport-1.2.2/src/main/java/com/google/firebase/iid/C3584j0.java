package com.google.firebase.iid;

import java.util.concurrent.ThreadFactory;

/* renamed from: com.google.firebase.iid.j0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3584j0 implements ThreadFactory {

    /* renamed from: P */
    static final ThreadFactory f6078P = new C3584j0();

    private C3584j0() {
    }

    public final Thread newThread(Runnable runnable) {
        return C3566c.m9782a(runnable);
    }
}
