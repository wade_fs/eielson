package com.google.firebase.messaging;

import p119e.p144d.p145a.p146a.C3880e;

/* renamed from: com.google.firebase.messaging.l */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3642l implements C3880e {

    /* renamed from: a */
    static final C3880e f6182a = new C3642l();

    private C3642l() {
    }

    public final Object apply(Object obj) {
        return ((String) obj).getBytes();
    }
}
