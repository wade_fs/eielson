package com.google.firebase.components;

/* renamed from: com.google.firebase.components.u */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public final class C3528u {
    /* renamed from: a */
    public static void m9653a(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    /* renamed from: b */
    public static void m9654b(boolean z, String str) {
        if (!z) {
            throw new IllegalStateException(str);
        }
    }

    /* renamed from: a */
    public static <T> T m9651a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    /* renamed from: a */
    public static <T> T m9652a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }
}
