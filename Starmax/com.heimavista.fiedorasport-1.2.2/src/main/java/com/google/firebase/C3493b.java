package com.google.firebase;

import android.content.Context;
import com.google.firebase.p101h.C3556a;

/* renamed from: com.google.firebase.b */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
final /* synthetic */ class C3493b implements C3556a {

    /* renamed from: a */
    private final C3494c f5917a;

    /* renamed from: b */
    private final Context f5918b;

    private C3493b(C3494c cVar, Context context) {
        this.f5917a = cVar;
        this.f5918b = context;
    }

    /* renamed from: a */
    public static C3556a m9545a(C3494c cVar, Context context) {
        return new C3493b(cVar, context);
    }

    public Object get() {
        return C3494c.m9549a(this.f5917a, this.f5918b);
    }
}
