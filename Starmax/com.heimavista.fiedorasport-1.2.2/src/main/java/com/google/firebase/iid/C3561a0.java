package com.google.firebase.iid;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import androidx.exifinterface.media.ExifInterface;
import java.io.IOException;
import java.util.Map;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.firebase.iid.a0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3561a0 {
    @GuardedBy("this")

    /* renamed from: a */
    private int f6037a = 0;
    @GuardedBy("this")

    /* renamed from: b */
    private final Map<Integer, C4066i<Void>> f6038b = new ArrayMap();
    @GuardedBy("itself")

    /* renamed from: c */
    private final C3609w f6039c;

    C3561a0(C3609w wVar) {
        this.f6039c = wVar;
    }

    @GuardedBy("this")
    @Nullable
    /* renamed from: b */
    private final String m9764b() {
        String a;
        synchronized (this.f6039c) {
            a = this.f6039c.mo22279a();
        }
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        String[] split = a.split(",");
        if (split.length <= 1 || TextUtils.isEmpty(split[1])) {
            return null;
        }
        return split[1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final synchronized boolean mo22217a() {
        return m9764b() != null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (m9762a(r5, r0) != false) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2 = r4.f6038b.remove(java.lang.Integer.valueOf(r4.f6037a));
        m9763a(r0);
        r4.f6037a++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (r2 == null) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        r2.mo23720a((java.lang.Boolean) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        return true;
     */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo22218a(com.google.firebase.iid.FirebaseInstanceId r5) {
        /*
            r4 = this;
        L_0x0000:
            monitor-enter(r4)
            java.lang.String r0 = r4.m9764b()     // Catch:{ all -> 0x0042 }
            r1 = 1
            if (r0 != 0) goto L_0x0017
            boolean r5 = com.google.firebase.iid.FirebaseInstanceId.m9729k()     // Catch:{ all -> 0x0042 }
            if (r5 == 0) goto L_0x0015
            java.lang.String r5 = "FirebaseInstanceId"
            java.lang.String r0 = "topic sync succeeded"
            android.util.Log.d(r5, r0)     // Catch:{ all -> 0x0042 }
        L_0x0015:
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            return r1
        L_0x0017:
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            boolean r2 = m9762a(r5, r0)
            if (r2 != 0) goto L_0x0020
            r5 = 0
            return r5
        L_0x0020:
            monitor-enter(r4)
            java.util.Map<java.lang.Integer, e.d.a.c.e.i<java.lang.Void>> r2 = r4.f6038b     // Catch:{ all -> 0x003f }
            int r3 = r4.f6037a     // Catch:{ all -> 0x003f }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x003f }
            java.lang.Object r2 = r2.remove(r3)     // Catch:{ all -> 0x003f }
            e.d.a.c.e.i r2 = (p119e.p144d.p145a.p157c.p167e.C4066i) r2     // Catch:{ all -> 0x003f }
            r4.m9763a(r0)     // Catch:{ all -> 0x003f }
            int r0 = r4.f6037a     // Catch:{ all -> 0x003f }
            int r0 = r0 + r1
            r4.f6037a = r0     // Catch:{ all -> 0x003f }
            monitor-exit(r4)     // Catch:{ all -> 0x003f }
            if (r2 == 0) goto L_0x0000
            r0 = 0
            r2.mo23720a(r0)
            goto L_0x0000
        L_0x003f:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003f }
            throw r5
        L_0x0042:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.C3561a0.mo22218a(com.google.firebase.iid.FirebaseInstanceId):boolean");
    }

    /* renamed from: a */
    private final synchronized boolean m9763a(String str) {
        synchronized (this.f6039c) {
            String a = this.f6039c.mo22279a();
            String valueOf = String.valueOf(str);
            if (!a.startsWith(valueOf.length() != 0 ? ",".concat(valueOf) : new String(","))) {
                return false;
            }
            String valueOf2 = String.valueOf(str);
            this.f6039c.mo22280a(a.substring((valueOf2.length() != 0 ? ",".concat(valueOf2) : new String(",")).length()));
            return true;
        }
    }

    @WorkerThread
    /* renamed from: a */
    private static boolean m9762a(FirebaseInstanceId firebaseInstanceId, String str) {
        String[] split = str.split("!");
        if (split.length == 2) {
            String str2 = split[0];
            String str3 = split[1];
            char c = 65535;
            try {
                int hashCode = str2.hashCode();
                if (hashCode != 83) {
                    if (hashCode == 85) {
                        if (str2.equals("U")) {
                            c = 1;
                        }
                    }
                } else if (str2.equals(ExifInterface.LATITUDE_SOUTH)) {
                    c = 0;
                }
                if (c == 0) {
                    firebaseInstanceId.mo22200a(str3);
                    if (FirebaseInstanceId.m9729k()) {
                        Log.d("FirebaseInstanceId", "subscribe operation succeeded");
                    }
                } else if (c == 1) {
                    firebaseInstanceId.mo22204b(str3);
                    if (FirebaseInstanceId.m9729k()) {
                        Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
                    }
                }
            } catch (IOException e) {
                if ("SERVICE_NOT_AVAILABLE".equals(e.getMessage()) || "INTERNAL_SERVER_ERROR".equals(e.getMessage())) {
                    String message = e.getMessage();
                    StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 53);
                    sb.append("Topic operation failed: ");
                    sb.append(message);
                    sb.append(". Will retry Topic operation.");
                    Log.e("FirebaseInstanceId", sb.toString());
                    return false;
                } else if (e.getMessage() == null) {
                    Log.e("FirebaseInstanceId", "Topic operation failed without exception message. Will retry Topic operation.");
                    return false;
                } else {
                    throw e;
                }
            }
        }
        return true;
    }
}
