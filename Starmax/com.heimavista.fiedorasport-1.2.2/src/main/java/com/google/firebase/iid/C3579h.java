package com.google.firebase.iid;

import android.os.Bundle;

/* renamed from: com.google.firebase.iid.h */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3579h extends C3585k<Void> {
    C3579h(int i, int i2, Bundle bundle) {
        super(i, 2, bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22242a(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            mo22250a((Object) null);
        } else {
            mo22249a(new C3583j(4, "Invalid response to one way request"));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo22243a() {
        return true;
    }
}
