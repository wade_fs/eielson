package com.google.firebase.iid;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.p099f.C3547a;
import com.google.firebase.p099f.C3548b;

/* renamed from: com.google.firebase.iid.p0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3596p0 implements C3548b {

    /* renamed from: a */
    private final FirebaseInstanceId.C3558a f6104a;

    C3596p0(FirebaseInstanceId.C3558a aVar) {
        this.f6104a = aVar;
    }

    /* renamed from: a */
    public final void mo22102a(C3547a aVar) {
        FirebaseInstanceId.C3558a aVar2 = this.f6104a;
        synchronized (aVar2) {
            if (aVar2.mo22214a()) {
                FirebaseInstanceId.this.m9730l();
            }
        }
    }
}
