package com.google.firebase.p096e.p098i;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3533a;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3538f;
import com.google.firebase.p096e.C3539g;
import com.google.firebase.p096e.p097h.C3540a;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* renamed from: com.google.firebase.e.i.c */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public final class C3543c implements C3540a<C3543c> {

    /* renamed from: c */
    private static final C3538f<String> f5992c = C3541a.m9672a();

    /* renamed from: d */
    private static final C3538f<Boolean> f5993d = C3542b.m9674a();

    /* renamed from: e */
    private static final C3545b f5994e = new C3545b(null);
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Map<Class<?>, C3536d<?>> f5995a = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Map<Class<?>, C3538f<?>> f5996b = new HashMap();

    /* renamed from: com.google.firebase.e.i.c$a */
    /* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
    class C3544a implements C3533a {
        C3544a() {
        }

        /* renamed from: a */
        public void mo22169a(@NonNull Object obj, @NonNull Writer writer) {
            C3546d dVar = new C3546d(writer, C3543c.this.f5995a, C3543c.this.f5996b);
            dVar.mo22183a(obj);
            dVar.mo22185a();
        }

        public String encode(@NonNull Object obj) {
            StringWriter stringWriter = new StringWriter();
            try {
                mo22169a(obj, stringWriter);
            } catch (IOException unused) {
            }
            return stringWriter.toString();
        }
    }

    /* renamed from: com.google.firebase.e.i.c$b */
    /* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
    private static final class C3545b implements C3538f<Date> {

        /* renamed from: a */
        private static final DateFormat f5998a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);

        static {
            f5998a.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        private C3545b() {
        }

        /* synthetic */ C3545b(C3544a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo22174a(@Nullable Date date, @NonNull C3539g gVar) {
            gVar.mo22175a(f5998a.format(date));
        }
    }

    public C3543c() {
        mo22179a(String.class, f5992c);
        mo22179a(Boolean.class, f5993d);
        mo22179a(Date.class, f5994e);
    }

    @NonNull
    /* renamed from: a */
    public <T> C3543c mo22178a(@NonNull Class cls, @NonNull C3536d dVar) {
        if (!this.f5995a.containsKey(cls)) {
            this.f5995a.put(cls, dVar);
            return this;
        }
        throw new IllegalArgumentException("Encoder already registered for " + cls.getName());
    }

    @NonNull
    /* renamed from: a */
    public <T> C3543c mo22179a(@NonNull Class cls, @NonNull C3538f fVar) {
        if (!this.f5996b.containsKey(cls)) {
            this.f5996b.put(cls, fVar);
            return this;
        }
        throw new IllegalArgumentException("Encoder already registered for " + cls.getName());
    }

    @NonNull
    /* renamed from: a */
    public C3533a mo22177a() {
        return new C3544a();
    }
}
