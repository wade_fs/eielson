package com.google.firebase.iid;

/* renamed from: com.google.firebase.iid.m0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3590m0 implements Runnable {

    /* renamed from: P */
    private final FirebaseInstanceId f6091P;

    C3590m0(FirebaseInstanceId firebaseInstanceId) {
        this.f6091P = firebaseInstanceId;
    }

    public final void run() {
        this.f6091P.mo22212i();
    }
}
