package com.google.firebase.datatransport;

public final class R$string {
    public static final int common_google_play_services_unknown_issue = 2131820683;
    public static final int status_bar_notification_info_overflow = 2131821141;

    private R$string() {
    }
}
