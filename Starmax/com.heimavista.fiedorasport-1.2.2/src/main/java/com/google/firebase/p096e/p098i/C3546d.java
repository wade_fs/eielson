package com.google.firebase.p096e.p098i;

import android.util.Base64;
import android.util.JsonWriter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3535c;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;
import com.google.firebase.p096e.C3538f;
import com.google.firebase.p096e.C3539g;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;

/* renamed from: com.google.firebase.e.i.d */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
final class C3546d implements C3537e, C3539g {

    /* renamed from: a */
    private C3546d f5999a = null;

    /* renamed from: b */
    private boolean f6000b = true;

    /* renamed from: c */
    private final JsonWriter f6001c;

    /* renamed from: d */
    private final Map<Class<?>, C3536d<?>> f6002d;

    /* renamed from: e */
    private final Map<Class<?>, C3538f<?>> f6003e;

    C3546d(@NonNull Writer writer, @NonNull Map<Class<?>, C3536d<?>> map, @NonNull Map<Class<?>, C3538f<?>> map2) {
        this.f6001c = new JsonWriter(writer);
        this.f6002d = map;
        this.f6003e = map2;
    }

    /* renamed from: b */
    private void m9686b() {
        if (this.f6000b) {
            C3546d dVar = this.f5999a;
            if (dVar != null) {
                dVar.m9686b();
                this.f5999a.f6000b = false;
                this.f5999a = null;
                this.f6001c.endObject();
                return;
            }
            return;
        }
        throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
    }

    @NonNull
    /* renamed from: a */
    public C3546d m9698a(@NonNull String str, @Nullable Object obj) {
        m9686b();
        this.f6001c.name(str);
        if (obj == null) {
            this.f6001c.nullValue();
            return this;
        }
        mo22183a(obj);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d m9696a(@NonNull String str, int i) {
        m9686b();
        this.f6001c.name(str);
        mo22181a(i);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d m9697a(@NonNull String str, long j) {
        m9686b();
        this.f6001c.name(str);
        mo22182a(j);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d m9695a(@Nullable String str) {
        m9686b();
        this.f6001c.value(str);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d mo22181a(int i) {
        m9686b();
        this.f6001c.value((long) i);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d mo22182a(long j) {
        m9686b();
        this.f6001c.value(j);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d m9699a(boolean z) {
        m9686b();
        this.f6001c.value(z);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public C3546d mo22184a(@Nullable byte[] bArr) {
        m9686b();
        if (bArr == null) {
            this.f6001c.nullValue();
        } else {
            this.f6001c.value(Base64.encodeToString(bArr, 2));
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.firebase.e.i.d.a(java.lang.String, java.lang.Object):com.google.firebase.e.i.d
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.firebase.e.i.d.a(java.lang.String, int):com.google.firebase.e.e
      com.google.firebase.e.i.d.a(java.lang.String, long):com.google.firebase.e.e
      com.google.firebase.e.i.d.a(java.lang.String, java.lang.Object):com.google.firebase.e.e
      com.google.firebase.e.i.d.a(java.lang.String, int):com.google.firebase.e.i.d
      com.google.firebase.e.i.d.a(java.lang.String, long):com.google.firebase.e.i.d
      com.google.firebase.e.e.a(java.lang.String, int):com.google.firebase.e.e
      com.google.firebase.e.e.a(java.lang.String, long):com.google.firebase.e.e
      com.google.firebase.e.e.a(java.lang.String, java.lang.Object):com.google.firebase.e.e
      com.google.firebase.e.i.d.a(java.lang.String, java.lang.Object):com.google.firebase.e.i.d */
    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public C3546d mo22183a(@Nullable Object obj) {
        if (obj == null) {
            this.f6001c.nullValue();
            return this;
        } else if (obj instanceof Number) {
            this.f6001c.value((Number) obj);
            return this;
        } else {
            int i = 0;
            if (obj.getClass().isArray()) {
                if (obj instanceof byte[]) {
                    mo22184a((byte[]) obj);
                    return this;
                }
                this.f6001c.beginArray();
                if (obj instanceof int[]) {
                    int[] iArr = (int[]) obj;
                    int length = iArr.length;
                    while (i < length) {
                        this.f6001c.value((long) iArr[i]);
                        i++;
                    }
                } else if (obj instanceof long[]) {
                    long[] jArr = (long[]) obj;
                    int length2 = jArr.length;
                    while (i < length2) {
                        mo22182a(jArr[i]);
                        i++;
                    }
                } else if (obj instanceof double[]) {
                    double[] dArr = (double[]) obj;
                    int length3 = dArr.length;
                    while (i < length3) {
                        this.f6001c.value(dArr[i]);
                        i++;
                    }
                } else if (obj instanceof boolean[]) {
                    boolean[] zArr = (boolean[]) obj;
                    int length4 = zArr.length;
                    while (i < length4) {
                        this.f6001c.value(zArr[i]);
                        i++;
                    }
                } else if (obj instanceof Number[]) {
                    Number[] numberArr = (Number[]) obj;
                    int length5 = numberArr.length;
                    while (i < length5) {
                        mo22183a(numberArr[i]);
                        i++;
                    }
                } else {
                    Object[] objArr = (Object[]) obj;
                    int length6 = objArr.length;
                    while (i < length6) {
                        mo22183a(objArr[i]);
                        i++;
                    }
                }
                this.f6001c.endArray();
                return this;
            } else if (obj instanceof Collection) {
                this.f6001c.beginArray();
                for (Object obj2 : (Collection) obj) {
                    mo22183a(obj2);
                }
                this.f6001c.endArray();
                return this;
            } else if (obj instanceof Map) {
                this.f6001c.beginObject();
                for (Map.Entry entry : ((Map) obj).entrySet()) {
                    Object key = entry.getKey();
                    try {
                        m9698a((String) key, entry.getValue());
                    } catch (ClassCastException e) {
                        throw new C3535c(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), e);
                    }
                }
                this.f6001c.endObject();
                return this;
            } else {
                C3536d dVar = this.f6002d.get(obj.getClass());
                if (dVar != null) {
                    this.f6001c.beginObject();
                    dVar.mo13453a(obj, this);
                    this.f6001c.endObject();
                    return this;
                }
                C3538f fVar = this.f6003e.get(obj.getClass());
                if (fVar != null) {
                    fVar.mo22174a(obj, this);
                    return this;
                } else if (obj instanceof Enum) {
                    m9695a(((Enum) obj).name());
                    return this;
                } else {
                    throw new C3535c("Couldn't find encoder for type " + obj.getClass().getCanonicalName());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo22185a() {
        m9686b();
        this.f6001c.flush();
    }
}
