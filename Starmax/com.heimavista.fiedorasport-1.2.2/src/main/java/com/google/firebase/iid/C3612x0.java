package com.google.firebase.iid;

import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.firebase.iid.x0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3612x0 {

    /* renamed from: a */
    private final String f6141a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final long f6142b;

    C3612x0(String str, long j) {
        C2258v.m5629a((Object) str);
        this.f6141a = str;
        this.f6142b = j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo22288a() {
        return this.f6141a;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C3612x0)) {
            return false;
        }
        C3612x0 x0Var = (C3612x0) obj;
        if (this.f6142b != x0Var.f6142b || !this.f6141a.equals(x0Var.f6141a)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f6141a, Long.valueOf(this.f6142b));
    }
}
