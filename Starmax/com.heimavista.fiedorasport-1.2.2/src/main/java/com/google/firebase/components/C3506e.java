package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;
import java.util.Set;

/* renamed from: com.google.firebase.components.e */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public interface C3506e {
    /* renamed from: a */
    <T> T mo22121a(Class cls);

    /* renamed from: b */
    <T> C3556a<T> mo22136b(Class<T> cls);

    /* renamed from: c */
    <T> C3556a<Set<T>> mo22137c(Class<T> cls);

    /* renamed from: d */
    <T> Set<T> mo22122d(Class<T> cls);
}
