package com.google.firebase.iid;

import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.o0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3594o0 implements C3601s {

    /* renamed from: a */
    private final FirebaseInstanceId f6098a;

    /* renamed from: b */
    private final String f6099b;

    /* renamed from: c */
    private final String f6100c;

    /* renamed from: d */
    private final String f6101d;

    C3594o0(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.f6098a = firebaseInstanceId;
        this.f6099b = str;
        this.f6100c = str2;
        this.f6101d = str3;
    }

    /* renamed from: a */
    public final C4065h mo22260a() {
        return this.f6098a.mo22195a(this.f6099b, this.f6100c, this.f6101d);
    }
}
