package com.google.firebase.p104j;

import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;

/* renamed from: com.google.firebase.j.b */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
final /* synthetic */ class C3619b implements C3511g {

    /* renamed from: a */
    private static final C3619b f6155a = new C3619b();

    private C3619b() {
    }

    /* renamed from: a */
    public static C3511g m9890a() {
        return f6155a;
    }

    /* renamed from: a */
    public Object mo22104a(C3506e eVar) {
        return C3620c.m9892a(eVar);
    }
}
