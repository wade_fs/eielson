package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;

/* renamed from: com.google.firebase.components.i */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3513i implements C3556a {

    /* renamed from: a */
    private final C3516l f5951a;

    /* renamed from: b */
    private final C3503d f5952b;

    private C3513i(C3516l lVar, C3503d dVar) {
        this.f5951a = lVar;
        this.f5952b = dVar;
    }

    /* renamed from: a */
    public static C3556a m9614a(C3516l lVar, C3503d dVar) {
        return new C3513i(lVar, dVar);
    }

    public Object get() {
        return this.f5952b.mo22124b().mo22104a(new C3529v(this.f5952b, this.f5951a));
    }
}
