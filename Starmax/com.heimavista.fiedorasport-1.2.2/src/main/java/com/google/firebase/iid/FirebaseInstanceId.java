package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.util.p091s.C2329b;
import com.google.firebase.C3485a;
import com.google.firebase.C3494c;
import com.google.firebase.p099f.C3548b;
import com.google.firebase.p099f.C3550d;
import com.google.firebase.p100g.C3553c;
import com.google.firebase.p104j.C3625h;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4069k;

/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public class FirebaseInstanceId {

    /* renamed from: i */
    private static final long f6019i = TimeUnit.HOURS.toSeconds(8);

    /* renamed from: j */
    private static C3609w f6020j;

    /* renamed from: k */
    private static ScheduledExecutorService f6021k;

    /* renamed from: a */
    private final Executor f6022a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C3494c f6023b;

    /* renamed from: c */
    private final C3587l f6024c;

    /* renamed from: d */
    private final C3598q0 f6025d;

    /* renamed from: e */
    private final C3597q f6026e;

    /* renamed from: f */
    private final C3561a0 f6027f;

    /* renamed from: g */
    private boolean f6028g;

    /* renamed from: h */
    private final C3558a f6029h;

    FirebaseInstanceId(C3494c cVar, C3550d dVar, C3625h hVar, C3553c cVar2) {
        this(cVar, new C3587l(cVar.mo22106a()), C3566c.m9784b(), C3566c.m9784b(), dVar, hVar, cVar2);
    }

    @NonNull
    @Keep
    public static FirebaseInstanceId getInstance(@NonNull C3494c cVar) {
        return (FirebaseInstanceId) cVar.mo22107a(FirebaseInstanceId.class);
    }

    @NonNull
    /* renamed from: j */
    public static FirebaseInstanceId m9728j() {
        return getInstance(C3494c.m9557i());
    }

    /* renamed from: k */
    static boolean m9729k() {
        if (!Log.isLoggable("FirebaseInstanceId", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3);
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public final void m9730l() {
        if (mo22202a(mo22207d()) || this.f6027f.mo22217a()) {
            m9731m();
        }
    }

    /* renamed from: m */
    private final synchronized void m9731m() {
        if (!this.f6028g) {
            mo22199a(0);
        }
    }

    /* renamed from: n */
    private static String m9732n() {
        return f6020j.mo22282b("").mo22288a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final synchronized void mo22201a(boolean z) {
        this.f6028g = z;
    }

    @NonNull
    /* renamed from: b */
    public C4065h<C3560a> mo22203b() {
        return m9724b(C3587l.m9802a(this.f6023b), "*");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final C3494c mo22206c() {
        return this.f6023b;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: d */
    public final C3607v mo22207d() {
        return m9726c(C3587l.m9802a(this.f6023b), "*");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final String mo22208e() {
        return mo22198a(C3587l.m9802a(this.f6023b), "*");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public final synchronized void mo22209f() {
        f6020j.mo22283b();
        if (this.f6029h.mo22214a()) {
            m9731m();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public final boolean mo22210g() {
        return this.f6024c.mo22254a() != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public final void mo22211h() {
        f6020j.mo22284c("");
        m9731m();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public final /* synthetic */ void mo22212i() {
        if (this.f6029h.mo22214a()) {
            m9730l();
        }
    }

    /* renamed from: b */
    private final C4065h<C3560a> m9724b(String str, String str2) {
        return C4069k.m12140a((Object) null).mo23708b(this.f6022a, new C3588l0(this, str, m9727c(str2)));
    }

    @Nullable
    /* renamed from: c */
    private static C3607v m9726c(String str, String str2) {
        return f6020j.mo22278a("", str, str2);
    }

    /* renamed from: c */
    private static String m9727c(String str) {
        return (str.isEmpty() || str.equalsIgnoreCase("fcm") || str.equalsIgnoreCase("gcm")) ? "*" : str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final synchronized void mo22199a(long j) {
        m9723a(new C3613y(this, this.f6024c, this.f6027f, Math.min(Math.max(30L, j << 1), f6019i)), j);
        this.f6028g = true;
    }

    /* renamed from: com.google.firebase.iid.FirebaseInstanceId$a */
    /* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
    private class C3558a {

        /* renamed from: a */
        private boolean f6030a;

        /* renamed from: b */
        private final C3550d f6031b;

        /* renamed from: c */
        private boolean f6032c;
        @Nullable

        /* renamed from: d */
        private C3548b<C3485a> f6033d;
        @Nullable

        /* renamed from: e */
        private Boolean f6034e;

        C3558a(C3550d dVar) {
            this.f6031b = dVar;
        }

        /* renamed from: b */
        private final synchronized void m9752b() {
            if (!this.f6032c) {
                this.f6030a = m9754d();
                this.f6034e = m9753c();
                if (this.f6034e == null && this.f6030a) {
                    this.f6033d = new C3596p0(this);
                    this.f6031b.mo22161a(C3485a.class, this.f6033d);
                }
                this.f6032c = true;
            }
        }

        @Nullable
        /* renamed from: c */
        private final Boolean m9753c() {
            ApplicationInfo applicationInfo;
            Context a = FirebaseInstanceId.this.f6023b.mo22106a();
            SharedPreferences sharedPreferences = a.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                return Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
            }
            try {
                PackageManager packageManager = a.getPackageManager();
                if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_messaging_auto_init_enabled")) {
                    return null;
                }
                return Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }

        /* renamed from: d */
        private final boolean m9754d() {
            try {
                Class.forName("com.google.firebase.messaging.FirebaseMessaging");
                return true;
            } catch (ClassNotFoundException unused) {
                Context a = FirebaseInstanceId.this.f6023b.mo22106a();
                Intent intent = new Intent("com.google.firebase.MESSAGING_EVENT");
                intent.setPackage(a.getPackageName());
                ResolveInfo resolveService = a.getPackageManager().resolveService(intent, 0);
                if (resolveService == null || resolveService.serviceInfo == null) {
                    return false;
                }
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final synchronized boolean mo22214a() {
            m9752b();
            if (this.f6034e == null) {
                return this.f6030a && FirebaseInstanceId.this.f6023b.mo22111e();
            }
            return this.f6034e.booleanValue();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final synchronized void mo22213a(boolean z) {
            m9752b();
            if (this.f6033d != null) {
                this.f6031b.mo22163b(C3485a.class, this.f6033d);
                this.f6033d = null;
            }
            SharedPreferences.Editor edit = FirebaseInstanceId.this.f6023b.mo22106a().getSharedPreferences("com.google.firebase.messaging", 0).edit();
            edit.putBoolean("auto_init", z);
            edit.apply();
            if (z) {
                FirebaseInstanceId.this.m9730l();
            }
            this.f6034e = Boolean.valueOf(z);
        }
    }

    private FirebaseInstanceId(C3494c cVar, C3587l lVar, Executor executor, Executor executor2, C3550d dVar, C3625h hVar, C3553c cVar2) {
        this.f6028g = false;
        if (C3587l.m9802a(cVar) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (f6020j == null) {
                    f6020j = new C3609w(cVar.mo22106a());
                }
            }
            this.f6023b = cVar;
            this.f6024c = lVar;
            this.f6025d = new C3598q0(cVar, lVar, executor, hVar, cVar2);
            this.f6022a = executor2;
            this.f6027f = new C3561a0(f6020j);
            this.f6029h = new C3558a(dVar);
            this.f6026e = new C3597q(executor);
            executor2.execute(new C3590m0(this));
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo22204b(String str) {
        C3607v d = mo22207d();
        if (!mo22202a(d)) {
            m9722a(this.f6025d.mo22266c(m9732n(), d.f6132a, str));
            return;
        }
        throw new IOException("token not available");
    }

    /* renamed from: a */
    static void m9723a(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            if (f6021k == null) {
                f6021k = new ScheduledThreadPoolExecutor(1, new C2329b("FirebaseInstanceId"));
            }
            f6021k.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    /* renamed from: b */
    public final void mo22205b(boolean z) {
        this.f6029h.mo22213a(z);
    }

    @WorkerThread
    @NonNull
    /* renamed from: a */
    public String mo22197a() {
        m9730l();
        return m9732n();
    }

    @WorkerThread
    @Nullable
    /* renamed from: a */
    public String mo22198a(@NonNull String str, @NonNull String str2) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            return ((C3560a) m9722a(m9724b(str, str2))).mo22216a();
        }
        throw new IOException("MAIN_THREAD");
    }

    /* renamed from: a */
    private final <T> T m9722a(C4065h<T> hVar) {
        try {
            return C4069k.m12143a(hVar, 30000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                    mo22209f();
                }
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e);
            }
        } catch (InterruptedException | TimeoutException unused) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22200a(String str) {
        C3607v d = mo22207d();
        if (!mo22202a(d)) {
            m9722a(this.f6025d.mo22265b(m9732n(), d.f6132a, str));
            return;
        }
        throw new IOException("token not available");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo22202a(@Nullable C3607v vVar) {
        return vVar == null || vVar.mo22275a(this.f6024c.mo22255b());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ C4065h mo22194a(String str, String str2, C4065h hVar) {
        String n = m9732n();
        C3607v c = m9726c(str, str2);
        if (!mo22202a(c)) {
            return C4069k.m12140a(new C3616z0(n, c.f6132a));
        }
        return this.f6026e.mo22262a(str, str2, new C3594o0(this, n, str, str2));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ C4065h mo22195a(String str, String str2, String str3) {
        return this.f6025d.mo22263a(str, str2, str3).mo23703a(this.f6022a, new C3592n0(this, str2, str3, str));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ C4065h mo22196a(String str, String str2, String str3, String str4) {
        f6020j.mo22281a("", str, str2, str4, this.f6024c.mo22255b());
        return C4069k.m12140a(new C3616z0(str3, str4));
    }
}
