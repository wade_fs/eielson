package com.google.firebase.datatransport;

import android.content.Context;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;
import p119e.p144d.p145a.p146a.p147i.C3911q;

/* renamed from: com.google.firebase.datatransport.a */
/* compiled from: com.google.firebase:firebase-datatransport@@17.0.3 */
final /* synthetic */ class C3532a implements C3511g {

    /* renamed from: a */
    private static final C3532a f5989a = new C3532a();

    private C3532a() {
    }

    /* renamed from: a */
    public static C3511g m9662a() {
        return f5989a;
    }

    /* renamed from: a */
    public Object mo22104a(C3506e eVar) {
        return C3911q.m11780a((Context) eVar.mo22121a(Context.class));
    }
}
