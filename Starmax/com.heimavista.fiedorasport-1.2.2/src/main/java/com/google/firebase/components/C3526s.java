package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;

/* renamed from: com.google.firebase.components.s */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public class C3526s<T> implements C3556a<T> {

    /* renamed from: c */
    private static final Object f5973c = new Object();

    /* renamed from: a */
    private volatile Object f5974a = f5973c;

    /* renamed from: b */
    private volatile C3556a<T> f5975b;

    public C3526s(C3556a<T> aVar) {
        this.f5975b = aVar;
    }

    public T get() {
        T t = this.f5974a;
        if (t == f5973c) {
            synchronized (this) {
                t = this.f5974a;
                if (t == f5973c) {
                    t = this.f5975b.get();
                    this.f5974a = t;
                    this.f5975b = null;
                }
            }
        }
        return t;
    }
}
