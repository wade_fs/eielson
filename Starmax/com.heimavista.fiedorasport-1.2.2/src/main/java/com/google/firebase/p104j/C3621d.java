package com.google.firebase.p104j;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.google.firebase.j.d */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public class C3621d {

    /* renamed from: b */
    private static volatile C3621d f6158b;

    /* renamed from: a */
    private final Set<C3623f> f6159a = new HashSet();

    C3621d() {
    }

    /* renamed from: b */
    public static C3621d m9896b() {
        C3621d dVar = f6158b;
        if (dVar == null) {
            synchronized (C3621d.class) {
                dVar = f6158b;
                if (dVar == null) {
                    dVar = new C3621d();
                    f6158b = dVar;
                }
            }
        }
        return dVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Set<C3623f> mo22306a() {
        Set<C3623f> unmodifiableSet;
        synchronized (this.f6159a) {
            unmodifiableSet = Collections.unmodifiableSet(this.f6159a);
        }
        return unmodifiableSet;
    }
}
