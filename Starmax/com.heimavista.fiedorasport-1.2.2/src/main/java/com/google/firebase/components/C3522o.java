package com.google.firebase.components;

import java.util.Arrays;
import java.util.List;

/* renamed from: com.google.firebase.components.o */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public class C3522o extends C3523p {
    public C3522o(List<C3503d<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
    }
}
