package com.google.firebase.p104j;

import androidx.annotation.Nullable;
import p228h.KotlinVersion;

/* renamed from: com.google.firebase.j.e */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public final class C3622e {
    @Nullable
    /* renamed from: a */
    public static String m9898a() {
        try {
            return KotlinVersion.f11194T.toString();
        } catch (NoClassDefFoundError unused) {
            return null;
        }
    }
}
