package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.C2323n;
import com.google.firebase.C3494c;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.List;

/* renamed from: com.google.firebase.iid.l */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3587l {

    /* renamed from: a */
    private final Context f6083a;

    /* renamed from: b */
    private String f6084b;

    /* renamed from: c */
    private String f6085c;

    /* renamed from: d */
    private int f6086d;

    /* renamed from: e */
    private int f6087e = 0;

    public C3587l(Context context) {
        this.f6083a = context;
    }

    /* renamed from: e */
    private final synchronized void m9804e() {
        PackageInfo a = m9801a(this.f6083a.getPackageName());
        if (a != null) {
            this.f6084b = Integer.toString(a.versionCode);
            this.f6085c = a.versionName;
        }
    }

    /* renamed from: a */
    public final synchronized int mo22254a() {
        if (this.f6087e != 0) {
            return this.f6087e;
        }
        PackageManager packageManager = this.f6083a.getPackageManager();
        if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
            Log.e("FirebaseInstanceId", "Google Play services missing or without correct permission.");
            return 0;
        }
        if (!C2323n.m5800i()) {
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
            if (queryIntentServices != null && queryIntentServices.size() > 0) {
                this.f6087e = 1;
                return this.f6087e;
            }
        }
        Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
        intent2.setPackage("com.google.android.gms");
        List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
        if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() <= 0) {
            Log.w("FirebaseInstanceId", "Failed to resolve IID implementation package, falling back");
            if (C2323n.m5800i()) {
                this.f6087e = 2;
            } else {
                this.f6087e = 1;
            }
            return this.f6087e;
        }
        this.f6087e = 2;
        return this.f6087e;
    }

    /* renamed from: b */
    public final synchronized String mo22255b() {
        if (this.f6084b == null) {
            m9804e();
        }
        return this.f6084b;
    }

    /* renamed from: c */
    public final synchronized String mo22256c() {
        if (this.f6085c == null) {
            m9804e();
        }
        return this.f6085c;
    }

    /* renamed from: d */
    public final synchronized int mo22257d() {
        PackageInfo a;
        if (this.f6086d == 0 && (a = m9801a("com.google.android.gms")) != null) {
            this.f6086d = a.versionCode;
        }
        return this.f6086d;
    }

    /* renamed from: a */
    public static String m9802a(C3494c cVar) {
        String b = cVar.mo22109c().mo22165b();
        if (b != null) {
            return b;
        }
        String a = cVar.mo22109c().mo22164a();
        if (!a.startsWith("1:")) {
            return a;
        }
        String[] split = a.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    /* renamed from: a */
    public static String m9803a(PublicKey publicKey) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(publicKey.getEncoded());
            digest[0] = (byte) ((digest[0] & 15) + 112);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException unused) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required algorithms");
            return null;
        }
    }

    /* renamed from: a */
    private final PackageInfo m9801a(String str) {
        try {
            return this.f6083a.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("Failed to find package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }
}
