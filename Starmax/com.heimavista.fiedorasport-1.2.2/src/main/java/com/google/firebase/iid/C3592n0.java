package com.google.firebase.iid;

import p119e.p144d.p145a.p157c.p167e.C4064g;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.n0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3592n0 implements C4064g {

    /* renamed from: a */
    private final FirebaseInstanceId f6093a;

    /* renamed from: b */
    private final String f6094b;

    /* renamed from: c */
    private final String f6095c;

    /* renamed from: d */
    private final String f6096d;

    C3592n0(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.f6093a = firebaseInstanceId;
        this.f6094b = str;
        this.f6095c = str2;
        this.f6096d = str3;
    }

    /* renamed from: a */
    public final C4065h mo22259a(Object obj) {
        return this.f6093a.mo22196a(this.f6094b, this.f6095c, this.f6096d, (String) obj);
    }
}
