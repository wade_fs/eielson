package com.google.firebase.components;

/* renamed from: com.google.firebase.components.c */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3502c implements C3511g {

    /* renamed from: a */
    private final Object f5935a;

    private C3502c(Object obj) {
        this.f5935a = obj;
    }

    /* renamed from: a */
    public static C3511g m9577a(Object obj) {
        return new C3502c(obj);
    }

    /* renamed from: a */
    public Object mo22104a(C3506e eVar) {
        Object obj = this.f5935a;
        C3503d.m9583a(obj, eVar);
        return obj;
    }
}
