package com.google.firebase.iid;

import android.os.Binder;
import android.os.Process;
import android.util.Log;

/* renamed from: com.google.firebase.iid.c0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3567c0 extends Binder {

    /* renamed from: a */
    private final C3576f0 f6053a;

    public C3567c0(C3576f0 f0Var) {
        this.f6053a = f0Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22230a(C3580h0 h0Var) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "service received new intent via bind strategy");
            }
            this.f6053a.mo22239a(h0Var.f6070a).mo23700a(C3566c.m9783a(), new C3564b0(h0Var));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
