package com.google.firebase.iid;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public class zzf implements Parcelable {
    public static final Parcelable.Creator<zzf> CREATOR = new C3586k0();

    /* renamed from: P */
    private Messenger f6151P;

    /* renamed from: Q */
    private C3604t0 f6152Q;

    /* renamed from: com.google.firebase.iid.zzf$a */
    /* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
    public static final class C3617a extends ClassLoader {
        /* access modifiers changed from: protected */
        public final Class<?> loadClass(String str, boolean z) {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (!FirebaseInstanceId.m9729k()) {
                return zzf.class;
            }
            Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            return zzf.class;
        }
    }

    public zzf(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f6151P = new Messenger(iBinder);
        } else {
            this.f6152Q = new C3610w0(iBinder);
        }
    }

    /* renamed from: a */
    public final void mo22294a(Message message) {
        Messenger messenger = this.f6151P;
        if (messenger != null) {
            messenger.send(message);
        } else {
            this.f6152Q.mo22273a(message);
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return m9886a().equals(((zzf) obj).m9886a());
        } catch (ClassCastException unused) {
            return false;
        }
    }

    public int hashCode() {
        return m9886a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        Messenger messenger = this.f6151P;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.f6152Q.asBinder());
        }
    }

    /* renamed from: a */
    private final IBinder m9886a() {
        Messenger messenger = this.f6151P;
        return messenger != null ? messenger.getBinder() : this.f6152Q.asBinder();
    }
}
