package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;
import java.util.Set;

/* renamed from: com.google.firebase.components.j */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3514j implements C3556a {

    /* renamed from: a */
    private final Set f5953a;

    private C3514j(Set set) {
        this.f5953a = set;
    }

    /* renamed from: a */
    public static C3556a m9615a(Set set) {
        return new C3514j(set);
    }

    public Object get() {
        return C3516l.m9618a(this.f5953a);
    }
}
