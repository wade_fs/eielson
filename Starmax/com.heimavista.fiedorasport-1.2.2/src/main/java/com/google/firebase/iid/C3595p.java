package com.google.firebase.iid;

import android.util.Pair;
import p119e.p144d.p145a.p157c.p167e.C4053a;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.p */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3595p implements C4053a {

    /* renamed from: a */
    private final C3597q f6102a;

    /* renamed from: b */
    private final Pair f6103b;

    C3595p(C3597q qVar, Pair pair) {
        this.f6102a = qVar;
        this.f6103b = pair;
    }

    /* renamed from: a */
    public final Object mo16773a(C4065h hVar) {
        this.f6102a.mo22261a(this.f6103b, hVar);
        return hVar;
    }
}
