package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import androidx.annotation.NonNull;

/* renamed from: com.google.firebase.iid.w0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3610w0 implements C3604t0 {

    /* renamed from: a */
    private final IBinder f6139a;

    C3610w0(@NonNull IBinder iBinder) {
        this.f6139a = iBinder;
    }

    /* renamed from: a */
    public final void mo22273a(@NonNull Message message) {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
        obtain.writeInt(1);
        message.writeToParcel(obtain, 0);
        try {
            this.f6139a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    @NonNull
    public final IBinder asBinder() {
        return this.f6139a;
    }
}
