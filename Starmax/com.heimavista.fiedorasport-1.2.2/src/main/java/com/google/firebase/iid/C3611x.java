package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/* renamed from: com.google.firebase.iid.x */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3611x extends BroadcastReceiver {

    /* renamed from: a */
    private C3613y f6140a;

    public C3611x(C3613y yVar) {
        this.f6140a = yVar;
    }

    /* renamed from: a */
    public final void mo22286a() {
        if (FirebaseInstanceId.m9729k()) {
            Log.d("FirebaseInstanceId", "Connectivity change received registered");
        }
        this.f6140a.mo22291a().registerReceiver(super, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public final void onReceive(Context context, Intent intent) {
        C3613y yVar = this.f6140a;
        if (yVar != null && yVar.mo22292b()) {
            if (FirebaseInstanceId.m9729k()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.m9723a(this.f6140a, 0);
            this.f6140a.mo22291a().unregisterReceiver(super);
            this.f6140a = null;
        }
    }
}
