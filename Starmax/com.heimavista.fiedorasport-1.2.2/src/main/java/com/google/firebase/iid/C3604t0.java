package com.google.firebase.iid;

import android.os.IInterface;
import android.os.Message;
import androidx.annotation.NonNull;

/* renamed from: com.google.firebase.iid.t0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
interface C3604t0 extends IInterface {
    /* renamed from: a */
    void mo22273a(@NonNull Message message);
}
