package com.google.firebase.messaging;

import java.util.concurrent.Callable;

/* renamed from: com.google.firebase.messaging.m */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3643m implements Callable {

    /* renamed from: a */
    private final C3644n f6183a;

    C3643m(C3644n nVar) {
        this.f6183a = nVar;
    }

    public final Object call() {
        return this.f6183a.mo22330b();
    }
}
