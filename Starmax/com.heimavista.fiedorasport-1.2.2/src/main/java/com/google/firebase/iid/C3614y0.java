package com.google.firebase.iid;

/* renamed from: com.google.firebase.iid.y0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3614y0 extends Exception {
    C3614y0(String str) {
        super(str);
    }

    C3614y0(Exception exc) {
        super(exc);
    }
}
