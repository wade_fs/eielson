package com.google.firebase.analytics.connector.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import com.google.firebase.C3494c;
import com.google.firebase.analytics.p095a.C3486a;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3512h;
import com.google.firebase.components.C3521n;
import com.google.firebase.p099f.C3550d;
import com.google.firebase.p104j.C3624g;
import java.util.Arrays;
import java.util.List;

@Keep
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
public class AnalyticsConnectorRegistrar implements C3512h {
    @SuppressLint({"MissingPermission"})
    @Keep
    public List<C3503d<?>> getComponents() {
        C3503d.C3505b a = C3503d.m9579a(C3486a.class);
        a.mo22133a(C3521n.m9637b(C3494c.class));
        a.mo22133a(C3521n.m9637b(Context.class));
        a.mo22133a(C3521n.m9637b(C3550d.class));
        a.mo22132a(C3492b.f5916a);
        a.mo22135c();
        return Arrays.asList(a.mo22134b(), C3624g.m9902a("fire-analytics", "17.2.2"));
    }
}
