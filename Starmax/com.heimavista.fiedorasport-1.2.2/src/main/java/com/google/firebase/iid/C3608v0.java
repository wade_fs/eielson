package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.core.content.ContextCompat;
import com.google.android.exoplayer2.C1750C;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;
import p119e.p144d.p145a.p157c.p161c.p164c.C4024l;

/* renamed from: com.google.firebase.iid.v0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3608v0 {
    C3608v0() {
    }

    @Nullable
    /* renamed from: c */
    private final C3612x0 m9860c(Context context, String str) {
        try {
            C3612x0 d = m9861d(context, str);
            if (d != null) {
                m9857a(context, str, d);
                return d;
            }
            e = null;
            try {
                C3612x0 a = m9852a(context.getSharedPreferences("com.google.android.gms.appid", 0), str);
                if (a != null) {
                    m9851a(context, str, a, false);
                    return a;
                }
            } catch (C3614y0 e) {
                e = e;
            }
            if (e == null) {
                return null;
            }
            throw e;
        } catch (C3614y0 e2) {
            e = e2;
        }
    }

    @Nullable
    /* renamed from: d */
    private final C3612x0 m9861d(Context context, String str) {
        File e = m9862e(context, str);
        if (!e.exists()) {
            return null;
        }
        try {
            return m9853a(e);
        } catch (C3614y0 | IOException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
                sb.append("Failed to read ID from file, retrying: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            try {
                return m9853a(e);
            } catch (IOException e3) {
                String valueOf2 = String.valueOf(e3);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("IID file exists, but failed to read from it: ");
                sb2.append(valueOf2);
                Log.w("FirebaseInstanceId", sb2.toString());
                throw new C3614y0(e3);
            }
        }
    }

    /* renamed from: e */
    private static File m9862e(Context context, String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString(str.getBytes(C1750C.UTF8_NAME), 11);
                StringBuilder sb = new StringBuilder(String.valueOf(encodeToString).length() + 33);
                sb.append("com.google.InstanceId_");
                sb.append(encodeToString);
                sb.append(".properties");
                str2 = sb.toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        return new File(m9859b(context), str2);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final C3612x0 mo22276a(Context context, String str) {
        C3612x0 c = m9860c(context, str);
        if (c != null) {
            return c;
        }
        return mo22277b(context, str);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final C3612x0 mo22277b(Context context, String str) {
        C3612x0 x0Var = new C3612x0(C3587l.m9803a(C3570d0.m9787a().getPublic()), System.currentTimeMillis());
        C3612x0 a = m9851a(context, str, x0Var, true);
        if (a == null || a.equals(x0Var)) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Generated new key");
            }
            m9857a(context, str, x0Var);
            return x0Var;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Loaded key after generating new one, using loaded one");
        }
        return a;
    }

    /* renamed from: a */
    static void m9856a(Context context) {
        File[] listFiles = m9859b(context).listFiles();
        for (File file : listFiles) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
    }

    /* renamed from: a */
    private static PublicKey m9855a(String str) {
        try {
            try {
                return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 8)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 19);
                sb.append("Invalid key stored ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                throw new C3614y0(e);
            }
        } catch (IllegalArgumentException e2) {
            throw new C3614y0(e2);
        }
    }

    /* renamed from: b */
    private static File m9859b(Context context) {
        File noBackupFilesDir = ContextCompat.getNoBackupFilesDir(context);
        if (noBackupFilesDir != null && noBackupFilesDir.isDirectory()) {
            return noBackupFilesDir;
        }
        Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
        return context.getFilesDir();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0095=Splitter:B:31:0x0095, B:19:0x0057=Splitter:B:19:0x0057} */
    @androidx.annotation.Nullable
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.firebase.iid.C3612x0 m9851a(android.content.Context r9, java.lang.String r10, com.google.firebase.iid.C3612x0 r11, boolean r12) {
        /*
            r8 = this;
            r0 = 3
            java.lang.String r1 = "FirebaseInstanceId"
            boolean r2 = android.util.Log.isLoggable(r1, r0)
            if (r2 == 0) goto L_0x000e
            java.lang.String r2 = "Writing ID to properties file"
            android.util.Log.d(r1, r2)
        L_0x000e:
            java.util.Properties r2 = new java.util.Properties
            r2.<init>()
            java.lang.String r3 = r11.mo22288a()
            java.lang.String r4 = "id"
            r2.setProperty(r4, r3)
            long r3 = r11.f6142b
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r4 = "cre"
            r2.setProperty(r4, r3)
            java.io.File r9 = m9862e(r9, r10)
            r10 = 0
            r9.createNewFile()     // Catch:{ IOException -> 0x00af }
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x00af }
            java.lang.String r4 = "rw"
            r3.<init>(r9, r4)     // Catch:{ IOException -> 0x00af }
            java.nio.channels.FileChannel r9 = r3.getChannel()     // Catch:{ all -> 0x00a5 }
            r9.lock()     // Catch:{ all -> 0x0099 }
            r4 = 0
            if (r12 == 0) goto L_0x0086
            long r6 = r9.size()     // Catch:{ all -> 0x0099 }
            int r12 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r12 <= 0) goto L_0x0086
            r9.position(r4)     // Catch:{ IOException -> 0x005d, y0 -> 0x005b }
            com.google.firebase.iid.x0 r11 = m9854a(r9)     // Catch:{ IOException -> 0x005d, y0 -> 0x005b }
            if (r9 == 0) goto L_0x0057
            r9.close()     // Catch:{ all -> 0x00a5 }
        L_0x0057:
            r3.close()     // Catch:{ IOException -> 0x00af }
            return r11
        L_0x005b:
            r12 = move-exception
            goto L_0x005e
        L_0x005d:
            r12 = move-exception
        L_0x005e:
            boolean r0 = android.util.Log.isLoggable(r1, r0)     // Catch:{ all -> 0x0099 }
            if (r0 == 0) goto L_0x0086
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0099 }
            int r0 = r0.length()     // Catch:{ all -> 0x0099 }
            int r0 = r0 + 58
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0099 }
            r6.<init>(r0)     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = "Tried reading ID before writing new one, but failed with: "
            r6.append(r0)     // Catch:{ all -> 0x0099 }
            r6.append(r12)     // Catch:{ all -> 0x0099 }
            java.lang.String r12 = r6.toString()     // Catch:{ all -> 0x0099 }
            android.util.Log.d(r1, r12)     // Catch:{ all -> 0x0099 }
        L_0x0086:
            r9.truncate(r4)     // Catch:{ all -> 0x0099 }
            java.io.OutputStream r12 = java.nio.channels.Channels.newOutputStream(r9)     // Catch:{ all -> 0x0099 }
            r2.store(r12, r10)     // Catch:{ all -> 0x0099 }
            if (r9 == 0) goto L_0x0095
            r9.close()     // Catch:{ all -> 0x00a5 }
        L_0x0095:
            r3.close()     // Catch:{ IOException -> 0x00af }
            return r11
        L_0x0099:
            r11 = move-exception
            if (r9 == 0) goto L_0x00a4
            r9.close()     // Catch:{ all -> 0x00a0 }
            goto L_0x00a4
        L_0x00a0:
            r9 = move-exception
            p119e.p144d.p145a.p157c.p161c.p164c.C4024l.m12027a(r11, r9)     // Catch:{ all -> 0x00a5 }
        L_0x00a4:
            throw r11     // Catch:{ all -> 0x00a5 }
        L_0x00a5:
            r9 = move-exception
            r3.close()     // Catch:{ all -> 0x00aa }
            goto L_0x00ae
        L_0x00aa:
            r11 = move-exception
            p119e.p144d.p145a.p157c.p161c.p164c.C4024l.m12027a(r9, r11)     // Catch:{ IOException -> 0x00af }
        L_0x00ae:
            throw r9     // Catch:{ IOException -> 0x00af }
        L_0x00af:
            r9 = move-exception
            java.lang.String r9 = java.lang.String.valueOf(r9)
            java.lang.String r11 = java.lang.String.valueOf(r9)
            int r11 = r11.length()
            int r11 = r11 + 21
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>(r11)
            java.lang.String r11 = "Failed to write key: "
            r12.append(r11)
            r12.append(r9)
            java.lang.String r9 = r12.toString()
            android.util.Log.w(r1, r9)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.C3608v0.m9851a(android.content.Context, java.lang.String, com.google.firebase.iid.x0, boolean):com.google.firebase.iid.x0");
    }

    /* renamed from: b */
    private static long m9858b(SharedPreferences sharedPreferences, String str) {
        String string = sharedPreferences.getString(C3609w.m9865a(str, "cre"), null);
        if (string == null) {
            return 0;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    /* renamed from: a */
    private final C3612x0 m9853a(File file) {
        FileChannel channel;
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            channel = fileInputStream.getChannel();
            channel.lock(0, Long.MAX_VALUE, true);
            C3612x0 a = m9854a(channel);
            if (channel != null) {
                channel.close();
            }
            fileInputStream.close();
            return a;
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                C4024l.m12027a(th, th2);
            }
            throw th;
        }
        throw th;
    }

    /* renamed from: a */
    private static C3612x0 m9854a(FileChannel fileChannel) {
        Properties properties = new Properties();
        properties.load(Channels.newInputStream(fileChannel));
        try {
            long parseLong = Long.parseLong(properties.getProperty("cre"));
            String property = properties.getProperty("id");
            if (property == null) {
                String property2 = properties.getProperty("pub");
                if (property2 != null) {
                    property = C3587l.m9803a(m9855a(property2));
                } else {
                    throw new C3614y0("Invalid properties file");
                }
            }
            return new C3612x0(property, parseLong);
        } catch (NumberFormatException e) {
            throw new C3614y0(e);
        }
    }

    @Nullable
    /* renamed from: a */
    private static C3612x0 m9852a(SharedPreferences sharedPreferences, String str) {
        long b = m9858b(sharedPreferences, str);
        String string = sharedPreferences.getString(C3609w.m9865a(str, "id"), null);
        if (string == null) {
            String string2 = sharedPreferences.getString(C3609w.m9865a(str, "|P|"), null);
            if (string2 == null) {
                return null;
            }
            string = C3587l.m9803a(m9855a(string2));
        }
        return new C3612x0(string, b);
    }

    /* renamed from: a */
    private final void m9857a(Context context, String str, C3612x0 x0Var) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (x0Var.equals(m9852a(sharedPreferences, str))) {
                return;
            }
        } catch (C3614y0 unused) {
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing key to shared preferences");
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(C3609w.m9865a(str, "id"), x0Var.mo22288a());
        edit.putString(C3609w.m9865a(str, "cre"), String.valueOf(x0Var.f6142b));
        edit.commit();
    }
}
