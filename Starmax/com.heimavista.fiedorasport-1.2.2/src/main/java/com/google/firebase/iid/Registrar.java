package com.google.firebase.iid;

import androidx.annotation.Keep;
import com.google.firebase.C3494c;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3512h;
import com.google.firebase.components.C3521n;
import com.google.firebase.iid.p103b.C3563a;
import com.google.firebase.p099f.C3550d;
import com.google.firebase.p100g.C3553c;
import com.google.firebase.p104j.C3624g;
import com.google.firebase.p104j.C3625h;
import java.util.Arrays;
import java.util.List;

@Keep
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class Registrar implements C3512h {

    /* renamed from: com.google.firebase.iid.Registrar$a */
    /* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
    private static class C3559a implements C3563a {
        public C3559a(FirebaseInstanceId firebaseInstanceId) {
        }
    }

    @Keep
    public final List<C3503d<?>> getComponents() {
        Class<FirebaseInstanceId> cls = FirebaseInstanceId.class;
        C3503d.C3505b a = C3503d.m9579a(cls);
        a.mo22133a(C3521n.m9637b(C3494c.class));
        a.mo22133a(C3521n.m9637b(C3550d.class));
        a.mo22133a(C3521n.m9637b(C3625h.class));
        a.mo22133a(C3521n.m9637b(C3553c.class));
        a.mo22132a(C3591n.f6092a);
        a.mo22131a();
        C3503d b = a.mo22134b();
        C3503d.C3505b a2 = C3503d.m9579a(C3563a.class);
        a2.mo22133a(C3521n.m9637b(cls));
        a2.mo22132a(C3593o.f6097a);
        return Arrays.asList(b, a2.mo22134b(), C3624g.m9902a("fire-iid", "20.0.2"));
    }
}
