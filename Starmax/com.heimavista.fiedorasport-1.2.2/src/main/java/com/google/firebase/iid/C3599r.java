package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.google.firebase.iid.zzf;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;
import p119e.p144d.p145a.p157c.p167e.C4069k;

/* renamed from: com.google.firebase.iid.r */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3599r {

    /* renamed from: g */
    private static int f6113g;

    /* renamed from: h */
    private static PendingIntent f6114h;

    /* renamed from: a */
    private final SimpleArrayMap<String, C4066i<Bundle>> f6115a = new SimpleArrayMap<>();

    /* renamed from: b */
    private final Context f6116b;

    /* renamed from: c */
    private final C3587l f6117c;

    /* renamed from: d */
    private Messenger f6118d;

    /* renamed from: e */
    private Messenger f6119e;

    /* renamed from: f */
    private zzf f6120f;

    public C3599r(Context context, C3587l lVar) {
        this.f6116b = context;
        this.f6117c = lVar;
        this.f6118d = new Messenger(new C3605u(this, Looper.getMainLooper()));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m9831a(Message message) {
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new zzf.C3617a());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof zzf) {
                        this.f6120f = (zzf) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f6119e = (Messenger) parcelableExtra;
                    }
                }
                Intent intent2 = (Intent) message.obj;
                String action = intent2.getAction();
                if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                    String stringExtra = intent2.getStringExtra("registration_id");
                    if (stringExtra == null) {
                        stringExtra = intent2.getStringExtra("unregistered");
                    }
                    if (stringExtra == null) {
                        String stringExtra2 = intent2.getStringExtra("error");
                        if (stringExtra2 == null) {
                            String valueOf = String.valueOf(intent2.getExtras());
                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
                            sb.append("Unexpected response, no error or registration id ");
                            sb.append(valueOf);
                            Log.w("FirebaseInstanceId", sb.toString());
                            return;
                        }
                        if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            String valueOf2 = String.valueOf(stringExtra2);
                            Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Received InstanceID error ".concat(valueOf2) : new String("Received InstanceID error "));
                        }
                        if (stringExtra2.startsWith("|")) {
                            String[] split = stringExtra2.split("\\|");
                            if (split.length <= 2 || !"ID".equals(split[1])) {
                                String valueOf3 = String.valueOf(stringExtra2);
                                Log.w("FirebaseInstanceId", valueOf3.length() != 0 ? "Unexpected structured response ".concat(valueOf3) : new String("Unexpected structured response "));
                                return;
                            }
                            String str = split[2];
                            String str2 = split[3];
                            if (str2.startsWith(":")) {
                                str2 = str2.substring(1);
                            }
                            m9833a(str, intent2.putExtra("error", str2).getExtras());
                            return;
                        }
                        synchronized (this.f6115a) {
                            for (int i = 0; i < this.f6115a.size(); i++) {
                                m9833a(this.f6115a.keyAt(i), intent2.getExtras());
                            }
                        }
                        return;
                    }
                    Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        Bundle extras = intent2.getExtras();
                        extras.putString("registration_id", group2);
                        m9833a(group, extras);
                        return;
                    } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        String valueOf4 = String.valueOf(stringExtra);
                        Log.d("FirebaseInstanceId", valueOf4.length() != 0 ? "Unexpected response string: ".concat(valueOf4) : new String("Unexpected response string: "));
                        return;
                    } else {
                        return;
                    }
                } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf5 = String.valueOf(action);
                    Log.d("FirebaseInstanceId", valueOf5.length() != 0 ? "Unexpected response action: ".concat(valueOf5) : new String("Unexpected response action: "));
                    return;
                } else {
                    return;
                }
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }

    /* renamed from: b */
    private final Bundle m9834b(Bundle bundle) {
        Bundle c = m9835c(bundle);
        if (c == null || !c.containsKey("google.messenger")) {
            return c;
        }
        Bundle c2 = m9835c(bundle);
        if (c2 == null || !c2.containsKey("google.messenger")) {
            return c2;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f1 A[SYNTHETIC] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Bundle m9835c(android.os.Bundle r8) {
        /*
            r7 = this;
            java.lang.String r0 = m9829a()
            e.d.a.c.e.i r1 = new e.d.a.c.e.i
            r1.<init>()
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r2 = r7.f6115a
            monitor-enter(r2)
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r3 = r7.f6115a     // Catch:{ all -> 0x0128 }
            r3.put(r0, r1)     // Catch:{ all -> 0x0128 }
            monitor-exit(r2)     // Catch:{ all -> 0x0128 }
            com.google.firebase.iid.l r2 = r7.f6117c
            int r2 = r2.mo22254a()
            if (r2 == 0) goto L_0x0120
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "com.google.android.gms"
            r2.setPackage(r3)
            com.google.firebase.iid.l r3 = r7.f6117c
            int r3 = r3.mo22254a()
            r4 = 2
            if (r3 != r4) goto L_0x0033
            java.lang.String r3 = "com.google.iid.TOKEN_REQUEST"
            r2.setAction(r3)
            goto L_0x0038
        L_0x0033:
            java.lang.String r3 = "com.google.android.c2dm.intent.REGISTER"
            r2.setAction(r3)
        L_0x0038:
            r2.putExtras(r8)
            android.content.Context r8 = r7.f6116b
            m9830a(r8, r2)
            java.lang.String r8 = java.lang.String.valueOf(r0)
            int r8 = r8.length()
            int r8 = r8 + 5
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r8)
            java.lang.String r8 = "|ID|"
            r3.append(r8)
            r3.append(r0)
            java.lang.String r8 = "|"
            r3.append(r8)
            java.lang.String r8 = r3.toString()
            java.lang.String r3 = "kid"
            r2.putExtra(r3, r8)
            r8 = 3
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r3 = android.util.Log.isLoggable(r3, r8)
            if (r3 == 0) goto L_0x0098
            android.os.Bundle r3 = r2.getExtras()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r5 = java.lang.String.valueOf(r3)
            int r5 = r5.length()
            int r5 = r5 + 8
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.String r5 = "Sending "
            r6.append(r5)
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            java.lang.String r5 = "FirebaseInstanceId"
            android.util.Log.d(r5, r3)
        L_0x0098:
            android.os.Messenger r3 = r7.f6118d
            java.lang.String r5 = "google.messenger"
            r2.putExtra(r5, r3)
            android.os.Messenger r3 = r7.f6119e
            if (r3 != 0) goto L_0x00a7
            com.google.firebase.iid.zzf r3 = r7.f6120f
            if (r3 == 0) goto L_0x00cd
        L_0x00a7:
            android.os.Message r3 = android.os.Message.obtain()
            r3.obj = r2
            android.os.Messenger r5 = r7.f6119e     // Catch:{ RemoteException -> 0x00bd }
            if (r5 == 0) goto L_0x00b7
            android.os.Messenger r5 = r7.f6119e     // Catch:{ RemoteException -> 0x00bd }
            r5.send(r3)     // Catch:{ RemoteException -> 0x00bd }
            goto L_0x00e0
        L_0x00b7:
            com.google.firebase.iid.zzf r5 = r7.f6120f     // Catch:{ RemoteException -> 0x00bd }
            r5.mo22294a(r3)     // Catch:{ RemoteException -> 0x00bd }
            goto L_0x00e0
        L_0x00bd:
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r8 = android.util.Log.isLoggable(r3, r8)
            if (r8 == 0) goto L_0x00cd
            java.lang.String r8 = "FirebaseInstanceId"
            java.lang.String r3 = "Messenger failed, fallback to startService"
            android.util.Log.d(r8, r3)
        L_0x00cd:
            com.google.firebase.iid.l r8 = r7.f6117c
            int r8 = r8.mo22254a()
            if (r8 != r4) goto L_0x00db
            android.content.Context r8 = r7.f6116b
            r8.sendBroadcast(r2)
            goto L_0x00e0
        L_0x00db:
            android.content.Context r8 = r7.f6116b
            r8.startService(r2)
        L_0x00e0:
            e.d.a.c.e.h r8 = r1.mo23718a()     // Catch:{ InterruptedException | TimeoutException -> 0x0104, ExecutionException -> 0x00fd }
            r1 = 30000(0x7530, double:1.4822E-319)
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException | TimeoutException -> 0x0104, ExecutionException -> 0x00fd }
            java.lang.Object r8 = p119e.p144d.p145a.p157c.p167e.C4069k.m12143a(r8, r1, r3)     // Catch:{ InterruptedException | TimeoutException -> 0x0104, ExecutionException -> 0x00fd }
            android.os.Bundle r8 = (android.os.Bundle) r8     // Catch:{ InterruptedException | TimeoutException -> 0x0104, ExecutionException -> 0x00fd }
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r1 = r7.f6115a
            monitor-enter(r1)
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r2 = r7.f6115a     // Catch:{ all -> 0x00f8 }
            r2.remove(r0)     // Catch:{ all -> 0x00f8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f8 }
            return r8
        L_0x00f8:
            r8 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f8 }
            throw r8
        L_0x00fb:
            r8 = move-exception
            goto L_0x0113
        L_0x00fd:
            r8 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00fb }
            r1.<init>(r8)     // Catch:{ all -> 0x00fb }
            throw r1     // Catch:{ all -> 0x00fb }
        L_0x0104:
            java.lang.String r8 = "FirebaseInstanceId"
            java.lang.String r1 = "No response"
            android.util.Log.w(r8, r1)     // Catch:{ all -> 0x00fb }
            java.io.IOException r8 = new java.io.IOException     // Catch:{ all -> 0x00fb }
            java.lang.String r1 = "TIMEOUT"
            r8.<init>(r1)     // Catch:{ all -> 0x00fb }
            throw r8     // Catch:{ all -> 0x00fb }
        L_0x0113:
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r1 = r7.f6115a
            monitor-enter(r1)
            androidx.collection.SimpleArrayMap<java.lang.String, e.d.a.c.e.i<android.os.Bundle>> r2 = r7.f6115a     // Catch:{ all -> 0x011d }
            r2.remove(r0)     // Catch:{ all -> 0x011d }
            monitor-exit(r1)     // Catch:{ all -> 0x011d }
            throw r8
        L_0x011d:
            r8 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x011d }
            throw r8
        L_0x0120:
            java.io.IOException r8 = new java.io.IOException
            java.lang.String r0 = "MISSING_INSTANCEID_SERVICE"
            r8.<init>(r0)
            throw r8
        L_0x0128:
            r8 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0128 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.C3599r.m9835c(android.os.Bundle):android.os.Bundle");
    }

    /* renamed from: a */
    private static synchronized void m9830a(Context context, Intent intent) {
        synchronized (C3599r.class) {
            if (f6114h == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                f6114h = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", f6114h);
        }
    }

    /* renamed from: a */
    private final void m9833a(String str, Bundle bundle) {
        synchronized (this.f6115a) {
            C4066i remove = this.f6115a.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                Log.w("FirebaseInstanceId", valueOf.length() != 0 ? "Missing callback for ".concat(valueOf) : new String("Missing callback for "));
                return;
            }
            remove.mo23720a((Boolean) bundle);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Bundle mo22267a(Bundle bundle) {
        if (this.f6117c.mo22257d() < 12000000) {
            return m9834b(bundle);
        }
        try {
            return (Bundle) C4069k.m12142a((C4065h) C3562a1.m9769a(this.f6116b).mo22220b(1, bundle));
        } catch (InterruptedException | ExecutionException e) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
                sb.append("Error making request: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            if (!(e.getCause() instanceof C3583j) || ((C3583j) e.getCause()).mo22247a() != 4) {
                return null;
            }
            return m9834b(bundle);
        }
    }

    /* renamed from: a */
    private static synchronized String m9829a() {
        String num;
        synchronized (C3599r.class) {
            int i = f6113g;
            f6113g = i + 1;
            num = Integer.toString(i);
        }
        return num;
    }
}
