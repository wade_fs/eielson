package com.google.firebase.p100g;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3521n;
import com.google.firebase.p100g.C3553c;

/* renamed from: com.google.firebase.g.b */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public class C3552b implements C3553c {

    /* renamed from: a */
    private C3555d f6007a;

    private C3552b(Context context) {
        this.f6007a = C3555d.m9715a(context);
    }

    @NonNull
    /* renamed from: a */
    public C3553c.C3554a mo22189a(@NonNull String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean a = this.f6007a.mo22192a(str, currentTimeMillis);
        boolean a2 = this.f6007a.mo22191a(currentTimeMillis);
        if (a && a2) {
            return C3553c.C3554a.COMBINED;
        }
        if (a2) {
            return C3553c.C3554a.GLOBAL;
        }
        if (a) {
            return C3553c.C3554a.SDK;
        }
        return C3553c.C3554a.NONE;
    }

    @NonNull
    /* renamed from: a */
    public static C3503d<C3553c> m9710a() {
        C3503d.C3505b a = C3503d.m9579a(C3553c.class);
        a.mo22133a(C3521n.m9637b(Context.class));
        a.mo22132a(C3551a.m9708a());
        return a.mo22134b();
    }

    /* renamed from: a */
    static /* synthetic */ C3553c m9711a(C3506e eVar) {
        return new C3552b((Context) eVar.mo22121a(Context.class));
    }
}
