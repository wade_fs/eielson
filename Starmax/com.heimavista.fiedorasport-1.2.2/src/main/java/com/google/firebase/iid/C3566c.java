package com.google.firebase.iid;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.firebase.iid.c */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3566c {

    /* renamed from: a */
    private static final Executor f6052a = C3582i0.f6076a;

    /* renamed from: a */
    static Executor m9783a() {
        return f6052a;
    }

    /* renamed from: b */
    static Executor m9784b() {
        return new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), C3584j0.f6078P);
    }

    /* renamed from: a */
    static final /* synthetic */ Thread m9782a(Runnable runnable) {
        return new Thread(runnable, "firebase-iid-executor");
    }
}
