package com.google.firebase.datatransport;

import android.content.Context;
import androidx.annotation.Keep;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3512h;
import com.google.firebase.components.C3521n;
import java.util.Collections;
import java.util.List;
import p119e.p144d.p145a.p146a.C3882g;

@Keep
/* compiled from: com.google.firebase:firebase-datatransport@@17.0.3 */
public class TransportRegistrar implements C3512h {
    public List<C3503d<?>> getComponents() {
        C3503d.C3505b a = C3503d.m9579a(C3882g.class);
        a.mo22133a(C3521n.m9637b(Context.class));
        a.mo22132a(C3532a.m9662a());
        return Collections.singletonList(a.mo22134b());
    }
}
