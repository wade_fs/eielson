package com.google.firebase.iid;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.VisibleForTesting;
import com.google.android.gms.common.util.p091s.C2329b;
import java.util.concurrent.ScheduledExecutorService;
import p119e.p144d.p145a.p157c.p161c.p164c.C4013a;
import p119e.p144d.p145a.p157c.p161c.p164c.C4018f;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.iid.a1 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3562a1 {

    /* renamed from: e */
    private static C3562a1 f6040e;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Context f6041a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final ScheduledExecutorService f6042b;

    /* renamed from: c */
    private C3565b1 f6043c = new C3565b1(this);

    /* renamed from: d */
    private int f6044d = 1;

    @VisibleForTesting
    private C3562a1(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.f6042b = scheduledExecutorService;
        this.f6041a = context.getApplicationContext();
    }

    /* renamed from: a */
    public static synchronized C3562a1 m9769a(Context context) {
        C3562a1 a1Var;
        synchronized (C3562a1.class) {
            if (f6040e == null) {
                f6040e = new C3562a1(context, C4013a.m12016a().mo23648a(1, new C2329b("MessengerIpcClient"), C4018f.f7374a));
            }
            a1Var = f6040e;
        }
        return a1Var;
    }

    /* renamed from: b */
    public final C4065h<Bundle> mo22220b(int i, Bundle bundle) {
        return m9770a(new C3589m(m9767a(), 1, bundle));
    }

    /* renamed from: a */
    public final C4065h<Void> mo22219a(int i, Bundle bundle) {
        return m9770a(new C3579h(m9767a(), 2, bundle));
    }

    /* renamed from: a */
    private final synchronized <T> C4065h<T> m9770a(C3585k<T> kVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(kVar);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.f6043c.mo22225a((C3585k<?>) kVar)) {
            this.f6043c = new C3565b1(this);
            this.f6043c.mo22225a((C3585k<?>) kVar);
        }
        return kVar.f6080b.mo23718a();
    }

    /* renamed from: a */
    private final synchronized int m9767a() {
        int i;
        i = this.f6044d;
        this.f6044d = i + 1;
        return i;
    }
}
