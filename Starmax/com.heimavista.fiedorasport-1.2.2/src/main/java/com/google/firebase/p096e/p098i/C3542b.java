package com.google.firebase.p096e.p098i;

import com.google.firebase.p096e.C3538f;
import com.google.firebase.p096e.C3539g;

/* renamed from: com.google.firebase.e.i.b */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
final /* synthetic */ class C3542b implements C3538f {

    /* renamed from: a */
    private static final C3542b f5991a = new C3542b();

    private C3542b() {
    }

    /* renamed from: a */
    public static C3538f m9674a() {
        return f5991a;
    }

    /* renamed from: a */
    public void mo22174a(Object obj, Object obj2) {
        ((C3539g) obj2).mo22176a(((Boolean) obj).booleanValue());
    }
}
