package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.util.Log;
import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import java.util.ArrayDeque;
import java.util.Queue;

/* renamed from: com.google.firebase.iid.t */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3603t {

    /* renamed from: e */
    private static C3603t f6124e;
    @Nullable

    /* renamed from: a */
    private String f6125a = null;

    /* renamed from: b */
    private Boolean f6126b = null;

    /* renamed from: c */
    private Boolean f6127c = null;

    /* renamed from: d */
    private final Queue<Intent> f6128d = new ArrayDeque();

    private C3603t() {
    }

    /* renamed from: b */
    public static synchronized C3603t m9840b() {
        C3603t tVar;
        synchronized (C3603t.class) {
            if (f6124e == null) {
                f6124e = new C3603t();
            }
            tVar = f6124e;
        }
        return tVar;
    }

    @Nullable
    /* renamed from: c */
    private final synchronized String m9841c(Context context, Intent intent) {
        if (this.f6125a != null) {
            return this.f6125a;
        }
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService != null) {
            if (resolveService.serviceInfo != null) {
                ServiceInfo serviceInfo = resolveService.serviceInfo;
                if (context.getPackageName().equals(serviceInfo.packageName)) {
                    if (serviceInfo.name != null) {
                        if (serviceInfo.name.startsWith(".")) {
                            String valueOf = String.valueOf(context.getPackageName());
                            String valueOf2 = String.valueOf(serviceInfo.name);
                            this.f6125a = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                        } else {
                            this.f6125a = serviceInfo.name;
                        }
                        return this.f6125a;
                    }
                }
                String str = serviceInfo.packageName;
                String str2 = serviceInfo.name;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 94 + String.valueOf(str2).length());
                sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
                sb.append(str);
                sb.append("/");
                sb.append(str2);
                Log.e("FirebaseInstanceId", sb.toString());
                return null;
            }
        }
        Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
        return null;
    }

    @MainThread
    /* renamed from: a */
    public final Intent mo22270a() {
        return this.f6128d.poll();
    }

    @MainThread
    /* renamed from: a */
    public final int mo22269a(Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Starting service");
        }
        this.f6128d.offer(intent);
        Intent intent2 = new Intent("com.google.firebase.MESSAGING_EVENT");
        intent2.setPackage(context.getPackageName());
        return m9839b(context, intent2);
    }

    /* renamed from: b */
    private final int m9839b(Context context, Intent intent) {
        ComponentName componentName;
        String c = m9841c(context, intent);
        if (c != null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(c);
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Restricting intent to a specific service: ".concat(valueOf) : new String("Restricting intent to a specific service: "));
            }
            intent.setClassName(context.getPackageName(), c);
        }
        try {
            if (mo22271a(context)) {
                componentName = C3615z.m9882a(context, intent);
            } else {
                componentName = context.startService(intent);
                Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
            }
            if (componentName != null) {
                return -1;
            }
            Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
            return 404;
        } catch (SecurityException e) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e);
            return 401;
        } catch (IllegalStateException e2) {
            String valueOf2 = String.valueOf(e2);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf2).length() + 45);
            sb.append("Failed to start service while in background: ");
            sb.append(valueOf2);
            Log.e("FirebaseInstanceId", sb.toString());
            return 402;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo22271a(Context context) {
        if (this.f6126b == null) {
            this.f6126b = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0);
        }
        if (!this.f6126b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.WAKE_LOCK this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.f6126b.booleanValue();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo22272b(Context context) {
        if (this.f6127c == null) {
            this.f6127c = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0);
        }
        if (!this.f6126b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.ACCESS_NETWORK_STATE this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.f6127c.booleanValue();
    }
}
