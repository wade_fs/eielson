package com.google.firebase.iid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import java.io.IOException;

/* renamed from: com.google.firebase.iid.y */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3613y implements Runnable {

    /* renamed from: P */
    private final long f6143P;

    /* renamed from: Q */
    private final PowerManager.WakeLock f6144Q = ((PowerManager) mo22291a().getSystemService("power")).newWakeLock(1, "fiid-sync");

    /* renamed from: R */
    private final FirebaseInstanceId f6145R;

    /* renamed from: S */
    private final C3561a0 f6146S;

    C3613y(FirebaseInstanceId firebaseInstanceId, C3587l lVar, C3561a0 a0Var, long j) {
        this.f6145R = firebaseInstanceId;
        this.f6146S = a0Var;
        this.f6143P = j;
        this.f6144Q.setReferenceCounted(false);
    }

    /* renamed from: c */
    private final boolean m9879c() {
        C3607v d = this.f6145R.mo22207d();
        if (!this.f6145R.mo22202a(d)) {
            return true;
        }
        try {
            String e = this.f6145R.mo22208e();
            if (e == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if ((d == null || (d != null && !e.equals(d.f6132a))) && "[DEFAULT]".equals(this.f6145R.mo22206c().mo22108b())) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(this.f6145R.mo22206c().mo22108b());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Invoking onNewToken for app: ".concat(valueOf) : new String("Invoking onNewToken for app: "));
                }
                Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
                intent.putExtra("token", e);
                Context a = mo22291a();
                Intent intent2 = new Intent(a, FirebaseInstanceIdReceiver.class);
                intent2.setAction("com.google.firebase.MESSAGING_EVENT");
                intent2.putExtra("wrapped_intent", intent);
                a.sendBroadcast(intent2);
            }
            return true;
        } catch (IOException e2) {
            if ("SERVICE_NOT_AVAILABLE".equals(e2.getMessage()) || "INTERNAL_SERVER_ERROR".equals(e2.getMessage())) {
                Log.w("FirebaseInstanceId", "Token retrieval failed without exception message. Will retry token retrieval");
                return false;
            } else if (e2.getMessage() == null) {
                String message = e2.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 52);
                sb.append("Token retrieval failed: ");
                sb.append(message);
                sb.append(". Will retry token retrieval");
                Log.w("FirebaseInstanceId", sb.toString());
                return false;
            } else {
                throw e2;
            }
        } catch (SecurityException unused) {
            Log.w("FirebaseInstanceId", "Token retrieval failed with SecurityException. Will retry token retrieval");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Context mo22291a() {
        return this.f6145R.mo22206c().mo22106a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo22292b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mo22291a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @SuppressLint({"Wakelock"})
    public final void run() {
        if (C3603t.m9840b().mo22271a(mo22291a())) {
            this.f6144Q.acquire();
        }
        try {
            this.f6145R.mo22201a(true);
            if (!this.f6145R.mo22210g()) {
                this.f6145R.mo22201a(false);
                if (C3603t.m9840b().mo22271a(mo22291a())) {
                    this.f6144Q.release();
                }
            } else if (!C3603t.m9840b().mo22272b(mo22291a()) || mo22292b()) {
                if (!m9879c() || !this.f6146S.mo22218a(this.f6145R)) {
                    this.f6145R.mo22199a(this.f6143P);
                } else {
                    this.f6145R.mo22201a(false);
                }
                if (C3603t.m9840b().mo22271a(mo22291a())) {
                    this.f6144Q.release();
                }
            } else {
                new C3611x(this).mo22286a();
                if (C3603t.m9840b().mo22271a(mo22291a())) {
                    this.f6144Q.release();
                }
            }
        } catch (IOException e) {
            String message = e.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 93);
            sb.append("Topic sync or token retrieval failed on hard failure exceptions: ");
            sb.append(message);
            sb.append(". Won't retry the operation.");
            Log.e("FirebaseInstanceId", sb.toString());
            this.f6145R.mo22201a(false);
            if (C3603t.m9840b().mo22271a(mo22291a())) {
                this.f6144Q.release();
            }
        } catch (Throwable th) {
            if (C3603t.m9840b().mo22271a(mo22291a())) {
                this.f6144Q.release();
            }
            throw th;
        }
    }
}
