package com.google.firebase.components;

import com.google.firebase.p099f.C3549c;
import com.google.firebase.p099f.C3550d;
import com.google.firebase.p101h.C3556a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.components.l */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public class C3516l extends C3500a {

    /* renamed from: e */
    private static final C3556a<Set<Object>> f5955e = C3515k.m9616a();

    /* renamed from: a */
    private final Map<C3503d<?>, C3526s<?>> f5956a = new HashMap();

    /* renamed from: b */
    private final Map<Class<?>, C3526s<?>> f5957b = new HashMap();

    /* renamed from: c */
    private final Map<Class<?>, C3526s<Set<?>>> f5958c = new HashMap();

    /* renamed from: d */
    private final C3525r f5959d;

    public C3516l(Executor executor, Iterable<C3512h> iterable, C3503d<?>... dVarArr) {
        this.f5959d = new C3525r(executor);
        ArrayList<C3503d> arrayList = new ArrayList<>();
        arrayList.add(C3503d.m9582a(this.f5959d, C3525r.class, C3550d.class, C3549c.class));
        for (C3512h hVar : iterable) {
            arrayList.addAll(hVar.getComponents());
        }
        for (C3503d<?> dVar : dVarArr) {
            if (dVar != null) {
                arrayList.add(dVar);
            }
        }
        C3517m.m9626a(arrayList);
        for (C3503d dVar2 : arrayList) {
            this.f5956a.put(dVar2, new C3526s(C3513i.m9614a(this, dVar2)));
        }
        m9619a();
        m9620b();
    }

    /* renamed from: b */
    private void m9620b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : this.f5956a.entrySet()) {
            C3503d dVar = (C3503d) entry.getKey();
            if (!dVar.mo22129g()) {
                C3526s sVar = (C3526s) entry.getValue();
                for (Class cls : dVar.mo22125c()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(sVar);
                }
            }
        }
        for (Map.Entry entry2 : hashMap.entrySet()) {
            this.f5958c.put((Class) entry2.getKey(), new C3526s(C3514j.m9615a((Set) entry2.getValue())));
        }
    }

    /* renamed from: c */
    public <T> C3556a<Set<T>> mo22137c(Class<T> cls) {
        C3526s sVar = this.f5958c.get(cls);
        if (sVar != null) {
            return sVar;
        }
        return f5955e;
    }

    /* renamed from: a */
    private void m9619a() {
        for (Map.Entry entry : this.f5956a.entrySet()) {
            C3503d dVar = (C3503d) entry.getKey();
            if (dVar.mo22129g()) {
                C3526s sVar = (C3526s) entry.getValue();
                for (Class cls : dVar.mo22125c()) {
                    this.f5957b.put(cls, sVar);
                }
            }
        }
        m9621c();
    }

    /* renamed from: c */
    private void m9621c() {
        for (C3503d dVar : this.f5956a.keySet()) {
            Iterator<C3521n> it = dVar.mo22123a().iterator();
            while (true) {
                if (it.hasNext()) {
                    C3521n next = it.next();
                    if (next.mo22153c() && !this.f5957b.containsKey(next.mo22151a())) {
                        throw new C3527t(String.format("Unsatisfied dependency for component %s: %s", dVar, next.mo22151a()));
                    }
                }
            }
        }
    }

    /* renamed from: a */
    static /* synthetic */ Set m9618a(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((C3526s) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* renamed from: a */
    public void mo22141a(boolean z) {
        for (Map.Entry entry : this.f5956a.entrySet()) {
            C3503d dVar = (C3503d) entry.getKey();
            C3526s sVar = (C3526s) entry.getValue();
            if (dVar.mo22127e() || (dVar.mo22128f() && z)) {
                sVar.get();
            }
        }
        this.f5959d.mo22159a();
    }

    /* renamed from: b */
    public <T> C3556a<T> mo22136b(Class<T> cls) {
        C3528u.m9652a(cls, "Null interface requested.");
        return this.f5957b.get(cls);
    }
}
