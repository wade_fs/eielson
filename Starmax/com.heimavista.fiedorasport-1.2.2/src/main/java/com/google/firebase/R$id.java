package com.google.firebase;

public final class R$id {
    public static final int action_container = 2131296335;
    public static final int action_divider = 2131296337;
    public static final int action_image = 2131296338;
    public static final int action_text = 2131296344;
    public static final int actions = 2131296345;
    public static final int async = 2131296362;
    public static final int blocking = 2131296380;
    public static final int bottom = 2131296382;
    public static final int chronometer = 2131296444;
    public static final int end = 2131296528;
    public static final int forever = 2131296572;
    public static final int icon = 2131296606;
    public static final int icon_group = 2131296607;
    public static final int info = 2131296616;
    public static final int italic = 2131296619;
    public static final int left = 2131296695;
    public static final int line1 = 2131296700;
    public static final int line3 = 2131296701;
    public static final int none = 2131296778;
    public static final int normal = 2131296779;
    public static final int notification_background = 2131296780;
    public static final int notification_main_column = 2131296781;
    public static final int notification_main_column_container = 2131296782;
    public static final int right = 2131296832;
    public static final int right_icon = 2131296835;
    public static final int right_side = 2131296837;
    public static final int start = 2131296909;
    public static final int tag_transition_group = 2131296931;
    public static final int tag_unhandled_key_event_manager = 2131296932;
    public static final int tag_unhandled_key_listeners = 2131296933;
    public static final int text = 2131296937;
    public static final int text2 = 2131296938;
    public static final int time = 2131296955;
    public static final int title = 2131296956;
    public static final int top = 2131296961;

    private R$id() {
    }
}
