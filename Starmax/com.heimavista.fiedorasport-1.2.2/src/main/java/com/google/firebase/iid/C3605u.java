package com.google.firebase.iid;

import android.os.Looper;
import android.os.Message;
import p119e.p144d.p145a.p157c.p161c.p164c.C4017e;

/* renamed from: com.google.firebase.iid.u */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3605u extends C4017e {

    /* renamed from: a */
    private final /* synthetic */ C3599r f6129a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3605u(C3599r rVar, Looper looper) {
        super(looper);
        this.f6129a = rVar;
    }

    public final void handleMessage(Message message) {
        this.f6129a.m9831a(message);
    }
}
