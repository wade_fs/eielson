package com.google.firebase.iid;

import com.google.firebase.C3494c;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;
import com.google.firebase.p099f.C3550d;
import com.google.firebase.p100g.C3553c;
import com.google.firebase.p104j.C3625h;

/* renamed from: com.google.firebase.iid.n */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3591n implements C3511g {

    /* renamed from: a */
    static final C3511g f6092a = new C3591n();

    private C3591n() {
    }

    /* renamed from: a */
    public final Object mo22104a(C3506e eVar) {
        return new FirebaseInstanceId((C3494c) eVar.mo22121a(C3494c.class), (C3550d) eVar.mo22121a(C3550d.class), (C3625h) eVar.mo22121a(C3625h.class), (C3553c) eVar.mo22121a(C3553c.class));
    }
}
