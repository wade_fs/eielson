package com.google.firebase.components;

/* renamed from: com.google.firebase.components.n */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public final class C3521n {

    /* renamed from: a */
    private final Class<?> f5965a;

    /* renamed from: b */
    private final int f5966b;

    /* renamed from: c */
    private final int f5967c;

    private C3521n(Class<?> cls, int i, int i2) {
        C3528u.m9652a(cls, "Null dependency anInterface.");
        this.f5965a = cls;
        this.f5966b = i;
        this.f5967c = i2;
    }

    /* renamed from: a */
    public static C3521n m9636a(Class<?> cls) {
        return new C3521n(cls, 0, 0);
    }

    /* renamed from: b */
    public static C3521n m9637b(Class<?> cls) {
        return new C3521n(cls, 1, 0);
    }

    /* renamed from: c */
    public static C3521n m9638c(Class<?> cls) {
        return new C3521n(cls, 2, 0);
    }

    /* renamed from: d */
    public boolean mo22154d() {
        return this.f5966b == 2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C3521n)) {
            return false;
        }
        C3521n nVar = (C3521n) obj;
        if (this.f5965a == nVar.f5965a && this.f5966b == nVar.f5966b && this.f5967c == nVar.f5967c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.f5965a.hashCode() ^ 1000003) * 1000003) ^ this.f5966b) * 1000003) ^ this.f5967c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.f5965a);
        sb.append(", type=");
        int i = this.f5966b;
        boolean z = true;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", direct=");
        if (this.f5967c != 0) {
            z = false;
        }
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    /* renamed from: a */
    public Class<?> mo22151a() {
        return this.f5965a;
    }

    /* renamed from: b */
    public boolean mo22152b() {
        return this.f5967c == 0;
    }

    /* renamed from: c */
    public boolean mo22153c() {
        return this.f5966b == 1;
    }
}
