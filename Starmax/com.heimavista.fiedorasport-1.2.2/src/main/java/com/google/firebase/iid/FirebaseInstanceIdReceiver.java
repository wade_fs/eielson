package com.google.firebase.iid;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.google.android.exoplayer2.C1750C;
import com.google.android.gms.common.util.C2323n;

/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class FirebaseInstanceIdReceiver extends WakefulBroadcastReceiver {

    /* renamed from: a */
    private static C3573e0 f6036a;

    @SuppressLint({"InlinedApi"})
    /* renamed from: a */
    public static int m9757a(BroadcastReceiver broadcastReceiver, Context context, Intent intent) {
        boolean z = true;
        boolean z2 = C2323n.m5800i() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & C1750C.ENCODING_PCM_MU_LAW) == 0) {
            z = false;
        }
        if (z2 && !z) {
            return m9759b(broadcastReceiver, context, intent);
        }
        int a = C3603t.m9840b().mo22269a(context, intent);
        if (!C2323n.m5800i() || a != 402) {
            return a;
        }
        m9759b(broadcastReceiver, context, intent);
        return 403;
    }

    /* renamed from: b */
    private final void m9760b(Context context, Intent intent) {
        int i;
        intent.setComponent(null);
        intent.setPackage(context.getPackageName());
        if (Build.VERSION.SDK_INT <= 18) {
            intent.removeCategory(context.getPackageName());
        }
        if ("google.com/iid".equals(intent.getStringExtra("from"))) {
            String stringExtra = intent.getStringExtra("CMD");
            if (stringExtra != null) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(intent.getExtras());
                    StringBuilder sb = new StringBuilder(String.valueOf(stringExtra).length() + 21 + String.valueOf(valueOf).length());
                    sb.append("Received command: ");
                    sb.append(stringExtra);
                    sb.append(" - ");
                    sb.append(valueOf);
                    Log.d("FirebaseInstanceId", sb.toString());
                }
                if ("RST".equals(stringExtra) || "RST_FULL".equals(stringExtra)) {
                    FirebaseInstanceId.m9728j().mo22209f();
                } else if ("SYNC".equals(stringExtra)) {
                    FirebaseInstanceId.m9728j().mo22211h();
                }
            }
            i = -1;
        } else {
            String stringExtra2 = intent.getStringExtra("gcm.rawData64");
            if (stringExtra2 != null) {
                intent.putExtra("rawData", Base64.decode(stringExtra2, 0));
                intent.removeExtra("gcm.rawData64");
            }
            i = m9757a(this, context, intent);
        }
        if (isOrderedBroadcast()) {
            setResultCode(i);
        }
    }

    public final void onReceive(@NonNull Context context, @NonNull Intent intent) {
        if (intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            Intent intent2 = parcelableExtra instanceof Intent ? (Intent) parcelableExtra : null;
            if (intent2 != null) {
                m9760b(context, intent2);
            } else {
                m9760b(context, intent);
            }
        }
    }

    /* renamed from: a */
    private static synchronized C3573e0 m9758a(Context context, String str) {
        C3573e0 e0Var;
        synchronized (FirebaseInstanceIdReceiver.class) {
            if (f6036a == null) {
                f6036a = new C3573e0(context, str);
            }
            e0Var = f6036a;
        }
        return e0Var;
    }

    /* renamed from: b */
    private static int m9759b(BroadcastReceiver broadcastReceiver, Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        if (broadcastReceiver.isOrderedBroadcast()) {
            broadcastReceiver.setResultCode(-1);
        }
        m9758a(context, "com.google.firebase.MESSAGING_EVENT").mo22234a(intent, broadcastReceiver.goAsync());
        return -1;
    }
}
