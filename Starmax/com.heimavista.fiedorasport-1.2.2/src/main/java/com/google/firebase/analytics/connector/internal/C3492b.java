package com.google.firebase.analytics.connector.internal;

import android.content.Context;
import com.google.firebase.C3494c;
import com.google.firebase.analytics.p095a.C3487b;
import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;
import com.google.firebase.p099f.C3550d;

/* renamed from: com.google.firebase.analytics.connector.internal.b */
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
final /* synthetic */ class C3492b implements C3511g {

    /* renamed from: a */
    static final C3511g f5916a = new C3492b();

    private C3492b() {
    }

    /* renamed from: a */
    public final Object mo22104a(C3506e eVar) {
        return C3487b.m9520a((C3494c) eVar.mo22121a(C3494c.class), (Context) eVar.mo22121a(Context.class), (C3550d) eVar.mo22121a(C3550d.class));
    }
}
