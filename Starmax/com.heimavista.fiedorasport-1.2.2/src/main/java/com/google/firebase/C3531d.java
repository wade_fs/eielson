package com.google.firebase;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2264y;
import com.google.android.gms.common.util.C2325p;

/* renamed from: com.google.firebase.d */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public final class C3531d {

    /* renamed from: a */
    private final String f5982a;

    /* renamed from: b */
    private final String f5983b;

    /* renamed from: c */
    private final String f5984c;

    /* renamed from: d */
    private final String f5985d;

    /* renamed from: e */
    private final String f5986e;

    /* renamed from: f */
    private final String f5987f;

    /* renamed from: g */
    private final String f5988g;

    private C3531d(@NonNull String str, @NonNull String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable String str6, @Nullable String str7) {
        C2258v.m5641b(!C2325p.m5805a(str), "ApplicationId must be set.");
        this.f5983b = str;
        this.f5982a = str2;
        this.f5984c = str3;
        this.f5985d = str4;
        this.f5986e = str5;
        this.f5987f = str6;
        this.f5988g = str7;
    }

    @Nullable
    /* renamed from: a */
    public static C3531d m9659a(@NonNull Context context) {
        C2264y yVar = new C2264y(context);
        String a = yVar.mo17046a("google_app_id");
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        return new C3531d(a, yVar.mo17046a("google_api_key"), yVar.mo17046a("firebase_database_url"), yVar.mo17046a("ga_trackingId"), yVar.mo17046a("gcm_defaultSenderId"), yVar.mo17046a("google_storage_bucket"), yVar.mo17046a("project_id"));
    }

    @Nullable
    /* renamed from: b */
    public String mo22165b() {
        return this.f5986e;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C3531d)) {
            return false;
        }
        C3531d dVar = (C3531d) obj;
        if (!C2251t.m5617a(this.f5983b, dVar.f5983b) || !C2251t.m5617a(this.f5982a, dVar.f5982a) || !C2251t.m5617a(this.f5984c, dVar.f5984c) || !C2251t.m5617a(this.f5985d, dVar.f5985d) || !C2251t.m5617a(this.f5986e, dVar.f5986e) || !C2251t.m5617a(this.f5987f, dVar.f5987f) || !C2251t.m5617a(this.f5988g, dVar.f5988g)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C2251t.m5615a(this.f5983b, this.f5982a, this.f5984c, this.f5985d, this.f5986e, this.f5987f, this.f5988g);
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("applicationId", this.f5983b);
        a.mo17037a("apiKey", this.f5982a);
        a.mo17037a("databaseUrl", this.f5984c);
        a.mo17037a("gcmSenderId", this.f5986e);
        a.mo17037a("storageBucket", this.f5987f);
        a.mo17037a("projectId", this.f5988g);
        return a.toString();
    }

    @NonNull
    /* renamed from: a */
    public String mo22164a() {
        return this.f5983b;
    }
}
