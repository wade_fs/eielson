package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.annotation.Size;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2525gd;
import com.google.android.gms.internal.measurement.zzv;
import com.google.android.gms.measurement.internal.C3081j5;
import com.google.android.gms.measurement.internal.C3083j7;
import com.google.android.gms.measurement.internal.C3098ka;
import com.google.firebase.iid.FirebaseInstanceId;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
public final class FirebaseAnalytics {

    /* renamed from: d */
    private static volatile FirebaseAnalytics f5902d;

    /* renamed from: a */
    private final C3081j5 f5903a;

    /* renamed from: b */
    private final C2525gd f5904b;

    /* renamed from: c */
    private final boolean f5905c;

    private FirebaseAnalytics(C3081j5 j5Var) {
        C2258v.m5629a(j5Var);
        this.f5903a = j5Var;
        this.f5904b = null;
        this.f5905c = false;
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.WAKE_LOCK"})
    @NonNull
    @Keep
    public static FirebaseAnalytics getInstance(@NonNull Context context) {
        if (f5902d == null) {
            synchronized (FirebaseAnalytics.class) {
                if (f5902d == null) {
                    if (C2525gd.m6398b(context)) {
                        f5902d = new FirebaseAnalytics(C2525gd.m6384a(context));
                    } else {
                        f5902d = new FirebaseAnalytics(C3081j5.m8751a(context, (zzv) null));
                    }
                }
            }
        }
        return f5902d;
    }

    @Keep
    public static C3083j7 getScionFrontendApiImplementation(Context context, Bundle bundle) {
        C2525gd a;
        if (C2525gd.m6398b(context) && (a = C2525gd.m6385a(context, (String) null, (String) null, (String) null, bundle)) != null) {
            return new C3490b(a);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, android.os.Bundle, boolean):void
     arg types: [java.lang.String, java.lang.String, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, android.os.Bundle, boolean):void */
    /* renamed from: a */
    public final void mo22096a(@Size(max = 40, min = 1) @NonNull String str, @Nullable Bundle bundle) {
        if (this.f5905c) {
            this.f5904b.mo17516a(str, bundle);
        } else {
            this.f5903a.mo19103v().mo19268a("app", str, bundle, true);
        }
    }

    @Keep
    public final String getFirebaseInstanceId() {
        return FirebaseInstanceId.m9728j().mo22197a();
    }

    @MainThread
    @Keep
    public final void setCurrentScreen(@NonNull Activity activity, @Size(max = 36, min = 1) @Nullable String str, @Size(max = 36, min = 1) @Nullable String str2) {
        if (this.f5905c) {
            this.f5904b.mo17513a(activity, str, str2);
        } else if (!C3098ka.m8843a()) {
            this.f5903a.mo19015l().mo19004w().mo19042a("setCurrentScreen must be called from the main thread");
        } else {
            this.f5903a.mo19078E().mo19299a(activity, str, str2);
        }
    }

    private FirebaseAnalytics(C2525gd gdVar) {
        C2258v.m5629a(gdVar);
        this.f5903a = null;
        this.f5904b = gdVar;
        this.f5905c = true;
    }
}
