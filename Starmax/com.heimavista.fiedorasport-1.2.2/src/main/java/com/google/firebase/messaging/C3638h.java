package com.google.firebase.messaging;

import android.content.Intent;
import p119e.p144d.p145a.p157c.p167e.C4057c;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.messaging.h */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3638h implements C4057c {

    /* renamed from: a */
    private final zzc f6178a;

    /* renamed from: b */
    private final Intent f6179b;

    C3638h(zzc zzc, Intent intent) {
        this.f6178a = zzc;
        this.f6179b = intent;
    }

    /* renamed from: a */
    public final void mo16766a(C4065h hVar) {
        this.f6178a.mo22347a(this.f6179b, hVar);
    }
}
