package com.google.firebase.analytics.connector.internal;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.util.C2311b;
import com.google.android.gms.measurement.internal.C3106l6;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/* renamed from: com.google.firebase.analytics.connector.internal.a */
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
public final class C3491a {

    /* renamed from: a */
    private static final List<String> f5911a = Arrays.asList("_e", "_f", "_iap", "_s", "_au", "_ui", "_cd", "app_open");

    /* renamed from: b */
    private static final List<String> f5912b = Arrays.asList("auto", "app", "am");

    /* renamed from: c */
    private static final List<String> f5913c = Arrays.asList("_r", "_dbg");

    /* renamed from: d */
    private static final List<String> f5914d = Arrays.asList((String[]) C2311b.m5767a(C3106l6.f5324a, C3106l6.f5325b));

    /* renamed from: e */
    private static final List<String> f5915e = Arrays.asList("^_ltv_[A-Z]{3}$", "^_cc[1-5]{1}$");

    static {
        new HashSet(Arrays.asList("_in", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "campaign_details", "_ug", "_iapx", "_exp_set", "_exp_clear", "_exp_activate", "_exp_timeout", "_exp_expire"));
    }

    /* renamed from: a */
    public static boolean m9540a(@NonNull String str) {
        return !f5912b.contains(str);
    }

    /* renamed from: a */
    public static boolean m9541a(@NonNull String str, @Nullable Bundle bundle) {
        if (f5911a.contains(str)) {
            return false;
        }
        if (bundle == null) {
            return true;
        }
        for (String str2 : f5913c) {
            if (bundle.containsKey(str2)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static boolean m9542a(@NonNull String str, @NonNull String str2) {
        if ("_ce1".equals(str2) || "_ce2".equals(str2)) {
            return str.equals("fcm") || str.equals("frc");
        }
        if ("_ln".equals(str2)) {
            return str.equals("fcm") || str.equals("fiam");
        }
        if (f5914d.contains(str2)) {
            return false;
        }
        for (String str3 : f5915e) {
            if (str2.matches(str3)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static boolean m9543a(@NonNull String str, @NonNull String str2, @Nullable Bundle bundle) {
        if (!"_cmp".equals(str2)) {
            return true;
        }
        if (!m9540a(str) || bundle == null) {
            return false;
        }
        for (String str3 : f5913c) {
            if (bundle.containsKey(str3)) {
                return false;
            }
        }
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != 101200) {
            if (hashCode != 101230) {
                if (hashCode == 3142703 && str.equals("fiam")) {
                    c = 2;
                }
            } else if (str.equals("fdl")) {
                c = 1;
            }
        } else if (str.equals("fcm")) {
            c = 0;
        }
        if (c == 0) {
            bundle.putString("_cis", "fcm_integration");
            return true;
        } else if (c == 1) {
            bundle.putString("_cis", "fdl_integration");
            return true;
        } else if (c != 2) {
            return false;
        } else {
            bundle.putString("_cis", "fiam_integration");
            return true;
        }
    }
}
