package com.google.firebase.messaging;

import androidx.annotation.Keep;
import androidx.annotation.VisibleForTesting;
import com.google.firebase.C3494c;
import com.google.firebase.components.C3503d;
import com.google.firebase.components.C3512h;
import com.google.firebase.components.C3521n;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.Arrays;
import java.util.List;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3878c;
import p119e.p144d.p145a.p146a.C3880e;
import p119e.p144d.p145a.p146a.C3881f;
import p119e.p144d.p145a.p146a.C3882g;

@Keep
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public class FirebaseMessagingRegistrar implements C3512h {

    @VisibleForTesting
    /* renamed from: com.google.firebase.messaging.FirebaseMessagingRegistrar$a */
    /* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
    public static class C3626a implements C3882g {
        /* renamed from: a */
        public final <T> C3881f<T> mo22308a(String str, Class<T> cls, C3877b bVar, C3880e<T, byte[]> eVar) {
            return new C3627b();
        }
    }

    /* renamed from: com.google.firebase.messaging.FirebaseMessagingRegistrar$b */
    /* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
    private static class C3627b<T> implements C3881f<T> {
        private C3627b() {
        }

        /* renamed from: a */
        public final void mo22309a(C3878c<T> cVar) {
        }
    }

    @Keep
    public List<C3503d<?>> getComponents() {
        C3503d.C3505b a = C3503d.m9579a(FirebaseMessaging.class);
        a.mo22133a(C3521n.m9637b(C3494c.class));
        a.mo22133a(C3521n.m9637b(FirebaseInstanceId.class));
        a.mo22133a(C3521n.m9636a(C3882g.class));
        a.mo22132a(C3641k.f6181a);
        a.mo22131a();
        return Arrays.asList(a.mo22134b());
    }
}
