package com.google.firebase.messaging;

import android.content.Intent;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.firebase.messaging.f */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3636f implements Runnable {

    /* renamed from: P */
    private final zzc f6174P;

    /* renamed from: Q */
    private final Intent f6175Q;

    /* renamed from: R */
    private final C4066i f6176R;

    C3636f(zzc zzc, Intent intent, C4066i iVar) {
        this.f6174P = zzc;
        this.f6175Q = intent;
        this.f6176R = iVar;
    }

    public final void run() {
        zzc zzc = this.f6174P;
        Intent intent = this.f6175Q;
        C4066i iVar = this.f6176R;
        try {
            zzc.mo22317c(intent);
        } finally {
            iVar.mo23720a((Boolean) null);
        }
    }
}
