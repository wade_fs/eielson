package com.google.firebase.messaging;

import android.content.Intent;
import com.google.firebase.iid.C3576f0;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.firebase.messaging.g */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final /* synthetic */ class C3637g implements C3576f0 {

    /* renamed from: a */
    private final zzc f6177a;

    C3637g(zzc zzc) {
        this.f6177a = zzc;
    }

    /* renamed from: a */
    public final C4065h mo22239a(Intent intent) {
        return this.f6177a.mo22348d(intent);
    }
}
