package com.google.firebase.messaging;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.firebase.messaging.q */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public final class C3647q implements Parcelable.Creator<RemoteMessage> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 2) {
                C2248a.m5550F(parcel, a);
            } else {
                bundle = C2248a.m5565f(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new RemoteMessage(bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new RemoteMessage[i];
    }
}
