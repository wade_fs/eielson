package com.google.firebase.iid;

import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.google.firebase.iid.v */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3607v {

    /* renamed from: d */
    private static final long f6131d = TimeUnit.DAYS.toMillis(7);

    /* renamed from: a */
    final String f6132a;

    /* renamed from: b */
    private final String f6133b;

    /* renamed from: c */
    private final long f6134c;

    private C3607v(String str, String str2, long j) {
        this.f6132a = str;
        this.f6133b = str2;
        this.f6134c = j;
    }

    /* renamed from: a */
    static String m9848a(String str, String str2, long j) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("token", str);
            jSONObject.put("appVersion", str2);
            jSONObject.put("timestamp", j);
            return jSONObject.toString();
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
            sb.append("Failed to encode token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    /* renamed from: b */
    static C3607v m9849b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.startsWith("{")) {
            return new C3607v(str, null, 0);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new C3607v(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("Failed to parse token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo22275a(String str) {
        return System.currentTimeMillis() > this.f6134c + f6131d || !str.equals(this.f6133b);
    }
}
