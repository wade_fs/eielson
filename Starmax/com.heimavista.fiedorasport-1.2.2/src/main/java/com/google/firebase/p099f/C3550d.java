package com.google.firebase.p099f;

import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.f.d */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public interface C3550d {
    /* renamed from: a */
    <T> void mo22161a(Class<T> cls, C3548b<? super T> bVar);

    /* renamed from: a */
    <T> void mo22162a(Class<T> cls, Executor executor, C3548b<? super T> bVar);

    /* renamed from: b */
    <T> void mo22163b(Class<T> cls, C3548b<? super T> bVar);
}
