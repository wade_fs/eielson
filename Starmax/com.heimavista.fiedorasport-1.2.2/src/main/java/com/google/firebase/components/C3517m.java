package com.google.firebase.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: com.google.firebase.components.m */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
class C3517m {

    /* renamed from: com.google.firebase.components.m$b */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    private static class C3519b {

        /* renamed from: a */
        private final C3503d<?> f5960a;

        /* renamed from: b */
        private final Set<C3519b> f5961b = new HashSet();

        /* renamed from: c */
        private final Set<C3519b> f5962c = new HashSet();

        C3519b(C3503d<?> dVar) {
            this.f5960a = dVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo22143a(C3519b bVar) {
            this.f5961b.add(bVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo22145b(C3519b bVar) {
            this.f5962c.add(bVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public void mo22146c(C3519b bVar) {
            this.f5962c.remove(bVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: d */
        public boolean mo22148d() {
            return this.f5962c.isEmpty();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3503d<?> mo22142a() {
            return this.f5960a;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public Set<C3519b> mo22144b() {
            return this.f5961b;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public boolean mo22147c() {
            return this.f5961b.isEmpty();
        }
    }

    /* renamed from: com.google.firebase.components.m$c */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    private static class C3520c {

        /* renamed from: a */
        private final Class<?> f5963a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final boolean f5964b;

        public boolean equals(Object obj) {
            if (!(obj instanceof C3520c)) {
                return false;
            }
            C3520c cVar = (C3520c) obj;
            if (!cVar.f5963a.equals(this.f5963a) || cVar.f5964b != this.f5964b) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return ((this.f5963a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.f5964b).hashCode();
        }

        private C3520c(Class<?> cls, boolean z) {
            this.f5963a = cls;
            this.f5964b = z;
        }
    }

    /* renamed from: a */
    static void m9626a(List<C3503d<?>> list) {
        Set<C3519b> b = m9627b(list);
        Set<C3519b> a = m9625a(b);
        int i = 0;
        while (!a.isEmpty()) {
            C3519b next = a.iterator().next();
            a.remove(next);
            i++;
            for (C3519b bVar : next.mo22144b()) {
                bVar.mo22146c(next);
                if (bVar.mo22148d()) {
                    a.add(bVar);
                }
            }
        }
        if (i != list.size()) {
            ArrayList arrayList = new ArrayList();
            for (C3519b bVar2 : b) {
                if (!bVar2.mo22148d() && !bVar2.mo22147c()) {
                    arrayList.add(bVar2.mo22142a());
                }
            }
            throw new C3522o(arrayList);
        }
    }

    /* renamed from: b */
    private static Set<C3519b> m9627b(List<C3503d<?>> list) {
        Set<C3519b> set;
        HashMap hashMap = new HashMap(list.size());
        for (C3503d dVar : list) {
            C3519b bVar = new C3519b(dVar);
            Iterator it = dVar.mo22125c().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class cls = (Class) it.next();
                    C3520c cVar = new C3520c(cls, !dVar.mo22129g());
                    if (!hashMap.containsKey(cVar)) {
                        hashMap.put(cVar, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(cVar);
                    if (set2.isEmpty() || cVar.f5964b) {
                        set2.add(bVar);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", cls));
                    }
                }
            }
        }
        for (Set<C3519b> set3 : hashMap.values()) {
            for (C3519b bVar2 : set3) {
                for (C3521n nVar : bVar2.mo22142a().mo22123a()) {
                    if (nVar.mo22152b() && (set = (Set) hashMap.get(new C3520c(nVar.mo22151a(), nVar.mo22154d()))) != null) {
                        for (C3519b bVar3 : set) {
                            bVar2.mo22143a(bVar3);
                            bVar3.mo22145b(bVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set set4 : hashMap.values()) {
            hashSet.addAll(set4);
        }
        return hashSet;
    }

    /* renamed from: a */
    private static Set<C3519b> m9625a(Set<C3519b> set) {
        HashSet hashSet = new HashSet();
        for (C3519b bVar : set) {
            if (bVar.mo22148d()) {
                hashSet.add(bVar);
            }
        }
        return hashSet;
    }
}
