package com.google.firebase.p100g;

import androidx.annotation.NonNull;

/* renamed from: com.google.firebase.g.c */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
public interface C3553c {

    /* renamed from: com.google.firebase.g.c$a */
    /* compiled from: com.google.firebase:firebase-common@@19.3.0 */
    public enum C3554a {
        NONE(0),
        SDK(1),
        GLOBAL(2),
        COMBINED(3);
        

        /* renamed from: P */
        private final int f6013P;

        private C3554a(int i) {
            this.f6013P = i;
        }

        /* renamed from: a */
        public int mo22190a() {
            return this.f6013P;
        }
    }

    @NonNull
    /* renamed from: a */
    C3554a mo22189a(@NonNull String str);
}
