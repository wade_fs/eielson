package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import androidx.core.content.ContextCompat;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/* renamed from: com.google.firebase.iid.w */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3609w {

    /* renamed from: a */
    private final SharedPreferences f6135a;

    /* renamed from: b */
    private final Context f6136b;

    /* renamed from: c */
    private final C3608v0 f6137c;
    @GuardedBy("this")

    /* renamed from: d */
    private final Map<String, C3612x0> f6138d;

    public C3609w(Context context) {
        this(context, new C3608v0());
    }

    /* renamed from: b */
    private static String m9866b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    /* renamed from: c */
    private final synchronized boolean m9867c() {
        return this.f6135a.getAll().isEmpty();
    }

    /* renamed from: a */
    public final synchronized String mo22279a() {
        return this.f6135a.getString("topic_operation_queue", "");
    }

    private C3609w(Context context, C3608v0 v0Var) {
        this.f6138d = new ArrayMap();
        this.f6136b = context;
        this.f6135a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.f6137c = v0Var;
        File file = new File(ContextCompat.getNoBackupFilesDir(this.f6136b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !m9867c()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    mo22283b();
                    FirebaseInstanceId.m9728j().mo22209f();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    /* renamed from: a */
    public final synchronized void mo22280a(String str) {
        this.f6135a.edit().putString("topic_operation_queue", str).apply();
    }

    /* renamed from: b */
    public final synchronized void mo22283b() {
        this.f6138d.clear();
        C3608v0.m9856a(this.f6136b);
        this.f6135a.edit().clear().commit();
    }

    /* renamed from: c */
    public final synchronized void mo22284c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        SharedPreferences.Editor edit = this.f6135a.edit();
        for (String str2 : this.f6135a.getAll().keySet()) {
            if (str2.startsWith(concat)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    /* renamed from: a */
    static String m9865a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    /* renamed from: a */
    public final synchronized C3607v mo22278a(String str, String str2, String str3) {
        return C3607v.m9849b(this.f6135a.getString(m9866b(str, str2, str3), null));
    }

    /* renamed from: a */
    public final synchronized void mo22281a(String str, String str2, String str3, String str4, String str5) {
        String a = C3607v.m9848a(str4, str5, System.currentTimeMillis());
        if (a != null) {
            SharedPreferences.Editor edit = this.f6135a.edit();
            edit.putString(m9866b(str, str2, str3), a);
            edit.commit();
        }
    }

    /* renamed from: b */
    public final synchronized C3612x0 mo22282b(String str) {
        C3612x0 x0Var;
        C3612x0 x0Var2 = this.f6138d.get(str);
        if (x0Var2 != null) {
            return x0Var2;
        }
        try {
            x0Var = this.f6137c.mo22276a(this.f6136b, str);
        } catch (C3614y0 unused) {
            Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
            FirebaseInstanceId.m9728j().mo22209f();
            x0Var = this.f6137c.mo22277b(this.f6136b, str);
        }
        this.f6138d.put(str, x0Var);
        return x0Var;
    }
}
