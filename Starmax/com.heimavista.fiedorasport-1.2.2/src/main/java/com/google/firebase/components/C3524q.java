package com.google.firebase.components;

import com.google.firebase.p099f.C3547a;
import com.google.firebase.p099f.C3548b;
import java.util.Map;

/* renamed from: com.google.firebase.components.q */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3524q implements Runnable {

    /* renamed from: P */
    private final Map.Entry f5968P;

    /* renamed from: Q */
    private final C3547a f5969Q;

    private C3524q(Map.Entry entry, C3547a aVar) {
        this.f5968P = entry;
        this.f5969Q = aVar;
    }

    /* renamed from: a */
    public static Runnable m9643a(Map.Entry entry, C3547a aVar) {
        return new C3524q(entry, aVar);
    }

    public void run() {
        ((C3548b) this.f5968P.getKey()).mo22102a(this.f5969Q);
    }
}
