package com.google.firebase.analytics.p095a;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.C3485a;
import com.google.firebase.C3494c;
import com.google.firebase.analytics.connector.internal.C3491a;
import com.google.firebase.p099f.C3547a;
import com.google.firebase.p099f.C3550d;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.firebase.analytics.a.b */
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
public class C3487b implements C3486a {

    /* renamed from: b */
    private static volatile C3486a f5906b;

    /* renamed from: a */
    private final AppMeasurement f5907a;

    private C3487b(AppMeasurement appMeasurement) {
        C2258v.m5629a(appMeasurement);
        this.f5907a = appMeasurement;
        new ConcurrentHashMap();
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.WAKE_LOCK"})
    /* renamed from: a */
    public static C3486a m9520a(C3494c cVar, Context context, C3550d dVar) {
        C2258v.m5629a(cVar);
        C2258v.m5629a(context);
        C2258v.m5629a(dVar);
        C2258v.m5629a(context.getApplicationContext());
        if (f5906b == null) {
            synchronized (C3487b.class) {
                if (f5906b == null) {
                    Bundle bundle = new Bundle(1);
                    if (cVar.mo22113f()) {
                        dVar.mo22162a(C3485a.class, C3488c.f5908a, C3489d.f5909a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", cVar.mo22111e());
                    }
                    f5906b = new C3487b(AppMeasurement.m8358a(context, bundle));
                }
            }
        }
        return f5906b;
    }

    /* renamed from: a */
    public void mo22099a(@NonNull String str, @NonNull String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (C3491a.m9540a(str) && C3491a.m9541a(str2, bundle) && C3491a.m9543a(str, str2, bundle)) {
            this.f5907a.logEventInternal(str, str2, bundle);
        }
    }

    /* renamed from: a */
    public void mo22100a(@NonNull String str, @NonNull String str2, Object obj) {
        if (C3491a.m9540a(str) && C3491a.m9542a(str, str2)) {
            this.f5907a.mo18675a(str, str2, obj);
        }
    }

    /* renamed from: a */
    static final /* synthetic */ void m9521a(C3547a aVar) {
        boolean z = ((C3485a) aVar.mo22186a()).f5901a;
        synchronized (C3487b.class) {
            ((C3487b) f5906b).f5907a.mo18676a(z);
        }
    }
}
