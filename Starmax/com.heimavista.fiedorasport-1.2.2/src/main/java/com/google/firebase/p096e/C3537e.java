package com.google.firebase.p096e;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.google.firebase.e.e */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public interface C3537e {
    @NonNull
    /* renamed from: a */
    C3537e mo22171a(@NonNull String str, int i);

    @NonNull
    /* renamed from: a */
    C3537e mo22172a(@NonNull String str, long j);

    @NonNull
    /* renamed from: a */
    C3537e mo22173a(@NonNull String str, @Nullable Object obj);
}
