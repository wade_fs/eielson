package com.google.firebase;

public final class R$color {
    public static final int notification_action_color_filter = 2131099887;
    public static final int notification_icon_bg_color = 2131099888;
    public static final int ripple_material_light = 2131099906;
    public static final int secondary_text_default_material_light = 2131099908;

    private R$color() {
    }
}
