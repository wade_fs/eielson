package com.google.firebase.components;

import androidx.annotation.GuardedBy;
import com.google.firebase.p099f.C3547a;
import com.google.firebase.p099f.C3548b;
import com.google.firebase.p099f.C3549c;
import com.google.firebase.p099f.C3550d;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* renamed from: com.google.firebase.components.r */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
class C3525r implements C3550d, C3549c {
    @GuardedBy("this")

    /* renamed from: a */
    private final Map<Class<?>, ConcurrentHashMap<C3548b<Object>, Executor>> f5970a = new HashMap();
    @GuardedBy("this")

    /* renamed from: b */
    private Queue<C3547a<?>> f5971b = new ArrayDeque();

    /* renamed from: c */
    private final Executor f5972c;

    C3525r(Executor executor) {
        this.f5972c = executor;
    }

    /* renamed from: b */
    private synchronized Set<Map.Entry<C3548b<Object>, Executor>> m9645b(C3547a<?> aVar) {
        Map map;
        map = this.f5970a.get(aVar.mo22187b());
        return map == null ? Collections.emptySet() : map.entrySet();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r0.hasNext() == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r0.next();
        ((java.util.concurrent.Executor) r1.getValue()).execute(com.google.firebase.components.C3524q.m9643a(r1, r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = m9645b(r4).iterator();
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo22160a(com.google.firebase.p099f.C3547a<?> r4) {
        /*
            r3 = this;
            com.google.firebase.components.C3528u.m9651a(r4)
            monitor-enter(r3)
            java.util.Queue<com.google.firebase.f.a<?>> r0 = r3.f5971b     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x000f
            java.util.Queue<com.google.firebase.f.a<?>> r0 = r3.f5971b     // Catch:{ all -> 0x0033 }
            r0.add(r4)     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            return
        L_0x000f:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            java.util.Set r0 = r3.m9645b(r4)
            java.util.Iterator r0 = r0.iterator()
        L_0x0018:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0032
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getValue()
            java.util.concurrent.Executor r2 = (java.util.concurrent.Executor) r2
            java.lang.Runnable r1 = com.google.firebase.components.C3524q.m9643a(r1, r4)
            r2.execute(r1)
            goto L_0x0018
        L_0x0032:
            return
        L_0x0033:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.components.C3525r.mo22160a(com.google.firebase.f.a):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T> void mo22163b(java.lang.Class<T> r2, com.google.firebase.p099f.C3548b<? super T> r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.google.firebase.components.C3528u.m9651a(r2)     // Catch:{ all -> 0x0029 }
            com.google.firebase.components.C3528u.m9651a(r3)     // Catch:{ all -> 0x0029 }
            java.util.Map<java.lang.Class<?>, java.util.concurrent.ConcurrentHashMap<com.google.firebase.f.b<java.lang.Object>, java.util.concurrent.Executor>> r0 = r1.f5970a     // Catch:{ all -> 0x0029 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0011
            monitor-exit(r1)
            return
        L_0x0011:
            java.util.Map<java.lang.Class<?>, java.util.concurrent.ConcurrentHashMap<com.google.firebase.f.b<java.lang.Object>, java.util.concurrent.Executor>> r0 = r1.f5970a     // Catch:{ all -> 0x0029 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0029 }
            java.util.concurrent.ConcurrentHashMap r0 = (java.util.concurrent.ConcurrentHashMap) r0     // Catch:{ all -> 0x0029 }
            r0.remove(r3)     // Catch:{ all -> 0x0029 }
            boolean r3 = r0.isEmpty()     // Catch:{ all -> 0x0029 }
            if (r3 == 0) goto L_0x0027
            java.util.Map<java.lang.Class<?>, java.util.concurrent.ConcurrentHashMap<com.google.firebase.f.b<java.lang.Object>, java.util.concurrent.Executor>> r3 = r1.f5970a     // Catch:{ all -> 0x0029 }
            r3.remove(r2)     // Catch:{ all -> 0x0029 }
        L_0x0027:
            monitor-exit(r1)
            return
        L_0x0029:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.components.C3525r.mo22163b(java.lang.Class, com.google.firebase.f.b):void");
    }

    /* renamed from: a */
    public synchronized <T> void mo22162a(Class<T> cls, Executor executor, C3548b<? super T> bVar) {
        C3528u.m9651a(cls);
        C3528u.m9651a(bVar);
        C3528u.m9651a(executor);
        if (!this.f5970a.containsKey(cls)) {
            this.f5970a.put(cls, new ConcurrentHashMap());
        }
        this.f5970a.get(cls).put(bVar, executor);
    }

    /* renamed from: a */
    public <T> void mo22161a(Class<T> cls, C3548b<? super T> bVar) {
        mo22162a(cls, this.f5972c, bVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo22159a() {
        Queue<C3547a<?>> queue;
        synchronized (this) {
            if (this.f5971b != null) {
                queue = this.f5971b;
                this.f5971b = null;
            } else {
                queue = null;
            }
        }
        if (queue != null) {
            for (C3547a aVar : queue) {
                mo22160a(aVar);
            }
        }
    }
}
