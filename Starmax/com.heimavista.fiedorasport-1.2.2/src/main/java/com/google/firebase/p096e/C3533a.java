package com.google.firebase.p096e;

import androidx.annotation.NonNull;
import java.io.Writer;

/* renamed from: com.google.firebase.e.a */
/* compiled from: com.google.firebase:firebase-encoders-json@@16.0.0 */
public interface C3533a {
    /* renamed from: a */
    void mo22169a(@NonNull Object obj, @NonNull Writer writer);

    @NonNull
    String encode(@NonNull Object obj);
}
