package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.firebase.iid.h0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C3580h0 {

    /* renamed from: a */
    final Intent f6070a;

    /* renamed from: b */
    private final BroadcastReceiver.PendingResult f6071b;

    /* renamed from: c */
    private boolean f6072c = false;

    /* renamed from: d */
    private final ScheduledFuture<?> f6073d;

    C3580h0(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.f6070a = intent;
        this.f6071b = pendingResult;
        this.f6073d = scheduledExecutorService.schedule(new C3578g0(this, intent), 9000, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final synchronized void mo22244a() {
        if (!this.f6072c) {
            this.f6071b.finish();
            this.f6073d.cancel(false);
            this.f6072c = true;
        }
    }
}
