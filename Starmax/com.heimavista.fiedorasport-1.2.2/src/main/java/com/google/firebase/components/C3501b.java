package com.google.firebase.components;

/* renamed from: com.google.firebase.components.b */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3501b implements C3511g {

    /* renamed from: a */
    private final Object f5934a;

    private C3501b(Object obj) {
        this.f5934a = obj;
    }

    /* renamed from: a */
    public static C3511g m9575a(Object obj) {
        return new C3501b(obj);
    }

    /* renamed from: a */
    public Object mo22104a(C3506e eVar) {
        Object obj = this.f5934a;
        C3503d.m9585b(obj, eVar);
        return obj;
    }
}
