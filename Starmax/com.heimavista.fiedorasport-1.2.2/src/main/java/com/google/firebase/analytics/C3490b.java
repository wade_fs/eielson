package com.google.firebase.analytics;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.C2525gd;
import com.google.android.gms.measurement.internal.C3083j7;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.firebase.analytics.b */
/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
final class C3490b implements C3083j7 {

    /* renamed from: a */
    private final /* synthetic */ C2525gd f5910a;

    C3490b(C2525gd gdVar) {
        this.f5910a = gdVar;
    }

    /* renamed from: a */
    public final void mo19113a(String str, String str2, Object obj) {
        this.f5910a.mo17518a(str, str2, obj);
    }

    /* renamed from: b */
    public final void mo19115b(String str, String str2, Bundle bundle) {
        this.f5910a.mo17517a(str, str2, bundle);
    }

    /* renamed from: c */
    public final void mo19116c(String str) {
        this.f5910a.mo17521b(str);
    }

    /* renamed from: d */
    public final void mo19118d(boolean z) {
        this.f5910a.mo17519a(z);
    }

    /* renamed from: e */
    public final String mo19119e() {
        return this.f5910a.mo17526e();
    }

    /* renamed from: f */
    public final String mo19120f() {
        return this.f5910a.mo17520b();
    }

    /* renamed from: g */
    public final String mo19121g() {
        return this.f5910a.mo17509a();
    }

    /* renamed from: t */
    public final long mo19122t() {
        return this.f5910a.mo17524c();
    }

    /* renamed from: a */
    public final Map<String, Object> mo19110a(String str, String str2, boolean z) {
        return this.f5910a.mo17511a(str, str2, z);
    }

    /* renamed from: b */
    public final int mo19114b(String str) {
        return this.f5910a.mo17523c(str);
    }

    /* renamed from: d */
    public final void mo19117d(Bundle bundle) {
        this.f5910a.mo17514a(bundle);
    }

    /* renamed from: a */
    public final String mo19108a() {
        return this.f5910a.mo17525d();
    }

    /* renamed from: a */
    public final void mo19111a(String str) {
        this.f5910a.mo17515a(str);
    }

    /* renamed from: a */
    public final void mo19112a(String str, String str2, Bundle bundle) {
        this.f5910a.mo17522b(str, str2, bundle);
    }

    /* renamed from: a */
    public final List<Bundle> mo19109a(String str, String str2) {
        return this.f5910a.mo17510a(str, str2);
    }
}
