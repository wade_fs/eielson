package com.google.firebase.components;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.VisibleForTesting;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.firebase.components.f */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public final class C3507f<T> {

    /* renamed from: a */
    private final T f5948a;

    /* renamed from: b */
    private final C3510c<T> f5949b;

    /* renamed from: com.google.firebase.components.f$b */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    private static class C3509b implements C3510c<Context> {

        /* renamed from: a */
        private final Class<? extends Service> f5950a;

        /* renamed from: b */
        private Bundle m9609b(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("ComponentDiscovery", "Context has no PackageManager.");
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, this.f5950a), 128);
                if (serviceInfo != null) {
                    return serviceInfo.metaData;
                }
                Log.w("ComponentDiscovery", this.f5950a + " has no service info.");
                return null;
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("ComponentDiscovery", "Application info not found.");
                return null;
            }
        }

        private C3509b(Class<? extends Service> cls) {
            this.f5950a = cls;
        }

        /* renamed from: a */
        public List<String> mo22140a(Context context) {
            Bundle b = m9609b(context);
            if (b == null) {
                Log.w("ComponentDiscovery", "Could not retrieve metadata, returning empty list of registrars.");
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            for (String str : b.keySet()) {
                if ("com.google.firebase.components.ComponentRegistrar".equals(b.get(str)) && str.startsWith("com.google.firebase.components:")) {
                    arrayList.add(str.substring(31));
                }
            }
            return arrayList;
        }
    }

    @VisibleForTesting
    /* renamed from: com.google.firebase.components.f$c */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    interface C3510c<T> {
        /* renamed from: a */
        List<String> mo22140a(T t);
    }

    @VisibleForTesting
    C3507f(T t, C3510c<T> cVar) {
        this.f5948a = t;
        this.f5949b = cVar;
    }

    /* renamed from: a */
    public static C3507f<Context> m9606a(Context context, Class<? extends Service> cls) {
        return new C3507f<>(context, new C3509b(cls));
    }

    /* renamed from: a */
    public List<C3512h> mo22138a() {
        return m9607a(this.f5949b.mo22140a(this.f5948a));
    }

    /* renamed from: a */
    private static List<C3512h> m9607a(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String str : list) {
            try {
                Class<?> cls = Class.forName(str);
                if (!C3512h.class.isAssignableFrom(cls)) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", str, "com.google.firebase.components.ComponentRegistrar"));
                } else {
                    arrayList.add((C3512h) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                }
            } catch (ClassNotFoundException e) {
                Log.w("ComponentDiscovery", String.format("Class %s is not an found.", str), e);
            } catch (IllegalAccessException e2) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", str), e2);
            } catch (InstantiationException e3) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", str), e3);
            } catch (NoSuchMethodException e4) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s", str), e4);
            } catch (InvocationTargetException e5) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s", str), e5);
            }
        }
        return arrayList;
    }
}
