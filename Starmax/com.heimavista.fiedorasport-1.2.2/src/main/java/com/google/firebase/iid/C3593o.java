package com.google.firebase.iid;

import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;
import com.google.firebase.iid.Registrar;

/* renamed from: com.google.firebase.iid.o */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3593o implements C3511g {

    /* renamed from: a */
    static final C3511g f6097a = new C3593o();

    private C3593o() {
    }

    /* renamed from: a */
    public final Object mo22104a(C3506e eVar) {
        return new Registrar.C3559a((FirebaseInstanceId) eVar.mo22121a(FirebaseInstanceId.class));
    }
}
