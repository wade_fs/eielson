package com.google.firebase.components;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.google.firebase.components.d */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
public final class C3503d<T> {

    /* renamed from: a */
    private final Set<Class<? super T>> f5936a;

    /* renamed from: b */
    private final Set<C3521n> f5937b;

    /* renamed from: c */
    private final int f5938c;

    /* renamed from: d */
    private final int f5939d;

    /* renamed from: e */
    private final C3511g<T> f5940e;

    /* renamed from: f */
    private final Set<Class<?>> f5941f;

    /* renamed from: com.google.firebase.components.d$b */
    /* compiled from: com.google.firebase:firebase-components@@16.0.0 */
    public static class C3505b<T> {

        /* renamed from: a */
        private final Set<Class<? super T>> f5942a;

        /* renamed from: b */
        private final Set<C3521n> f5943b;

        /* renamed from: c */
        private int f5944c;

        /* renamed from: d */
        private int f5945d;

        /* renamed from: e */
        private C3511g<T> f5946e;

        /* renamed from: f */
        private Set<Class<?>> f5947f;

        /* access modifiers changed from: private */
        /* renamed from: d */
        public C3505b<T> m9596d() {
            this.f5945d = 1;
            return this;
        }

        /* renamed from: b */
        public C3503d<T> mo22134b() {
            C3528u.m9654b(this.f5946e != null, "Missing required property: factory.");
            return new C3503d(new HashSet(this.f5942a), new HashSet(this.f5943b), this.f5944c, this.f5945d, this.f5946e, this.f5947f);
        }

        /* renamed from: c */
        public C3505b<T> mo22135c() {
            m9593a(2);
            return this;
        }

        @SafeVarargs
        private C3505b(Class<T> cls, Class<? super T>... clsArr) {
            this.f5942a = new HashSet();
            this.f5943b = new HashSet();
            this.f5944c = 0;
            this.f5945d = 0;
            this.f5947f = new HashSet();
            C3528u.m9652a(cls, "Null interface");
            this.f5942a.add(cls);
            for (Class<? super T> cls2 : clsArr) {
                C3528u.m9652a(cls2, "Null interface");
            }
            Collections.addAll(this.f5942a, clsArr);
        }

        /* renamed from: a */
        public C3505b<T> mo22133a(C3521n nVar) {
            C3528u.m9652a(nVar, "Null dependency");
            m9595a(nVar.mo22151a());
            this.f5943b.add(nVar);
            return this;
        }

        /* renamed from: a */
        public C3505b<T> mo22131a() {
            m9593a(1);
            return this;
        }

        /* renamed from: a */
        private C3505b<T> m9593a(int i) {
            C3528u.m9654b(this.f5944c == 0, "Instantiation type has already been set.");
            this.f5944c = i;
            return this;
        }

        /* renamed from: a */
        private void m9595a(Class<?> cls) {
            C3528u.m9653a(!this.f5942a.contains(cls), "Components are not allowed to depend on interfaces they themselves provide.");
        }

        /* renamed from: a */
        public C3505b<T> mo22132a(C3511g gVar) {
            C3528u.m9652a(gVar, "Null factory");
            this.f5946e = gVar;
            return this;
        }
    }

    /* renamed from: a */
    static /* synthetic */ Object m9583a(Object obj, C3506e eVar) {
        return obj;
    }

    /* renamed from: b */
    static /* synthetic */ Object m9585b(Object obj, C3506e eVar) {
        return obj;
    }

    /* renamed from: a */
    public Set<C3521n> mo22123a() {
        return this.f5937b;
    }

    /* renamed from: b */
    public C3511g<T> mo22124b() {
        return this.f5940e;
    }

    /* renamed from: c */
    public Set<Class<? super T>> mo22125c() {
        return this.f5936a;
    }

    /* renamed from: d */
    public Set<Class<?>> mo22126d() {
        return this.f5941f;
    }

    /* renamed from: e */
    public boolean mo22127e() {
        return this.f5938c == 1;
    }

    /* renamed from: f */
    public boolean mo22128f() {
        return this.f5938c == 2;
    }

    /* renamed from: g */
    public boolean mo22129g() {
        return this.f5939d == 0;
    }

    public String toString() {
        return "Component<" + Arrays.toString(this.f5936a.toArray()) + ">{" + this.f5938c + ", type=" + this.f5939d + ", deps=" + Arrays.toString(this.f5937b.toArray()) + "}";
    }

    private C3503d(Set<Class<? super T>> set, Set<C3521n> set2, int i, int i2, C3511g<T> gVar, Set<Class<?>> set3) {
        this.f5936a = Collections.unmodifiableSet(set);
        this.f5937b = Collections.unmodifiableSet(set2);
        this.f5938c = i;
        this.f5939d = i2;
        this.f5940e = gVar;
        this.f5941f = Collections.unmodifiableSet(set3);
    }

    /* renamed from: a */
    public static <T> C3505b<T> m9579a(Class<T> cls) {
        return new C3505b<>(cls, new Class[0]);
    }

    /* renamed from: b */
    public static <T> C3505b<T> m9584b(Class<T> cls) {
        C3505b<T> a = m9579a(cls);
        C3505b unused = a.m9596d();
        return a;
    }

    @SafeVarargs
    /* renamed from: a */
    public static <T> C3505b<T> m9580a(Class<T> cls, Class<? super T>... clsArr) {
        return new C3505b<>(cls, clsArr);
    }

    @SafeVarargs
    /* renamed from: a */
    public static <T> C3503d<T> m9582a(T t, Class<T> cls, Class<? super T>... clsArr) {
        C3505b<T> a = m9580a(cls, clsArr);
        a.mo22132a(C3501b.m9575a((Object) t));
        return a.mo22134b();
    }

    /* renamed from: a */
    public static <T> C3503d<T> m9581a(T t, Class<T> cls) {
        C3505b<T> b = m9584b(cls);
        b.mo22132a(C3502c.m9577a((Object) t));
        return b.mo22134b();
    }
}
