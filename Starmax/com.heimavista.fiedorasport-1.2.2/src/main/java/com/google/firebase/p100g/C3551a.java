package com.google.firebase.p100g;

import com.google.firebase.components.C3506e;
import com.google.firebase.components.C3511g;

/* renamed from: com.google.firebase.g.a */
/* compiled from: com.google.firebase:firebase-common@@19.3.0 */
final /* synthetic */ class C3551a implements C3511g {

    /* renamed from: a */
    private static final C3551a f6006a = new C3551a();

    private C3551a() {
    }

    /* renamed from: a */
    public static C3511g m9708a() {
        return f6006a;
    }

    /* renamed from: a */
    public Object mo22104a(C3506e eVar) {
        return C3552b.m9711a(eVar);
    }
}
