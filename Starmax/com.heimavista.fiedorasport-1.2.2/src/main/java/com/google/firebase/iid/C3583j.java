package com.google.firebase.iid;

/* renamed from: com.google.firebase.iid.j */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public final class C3583j extends Exception {

    /* renamed from: P */
    private final int f6077P;

    public C3583j(int i, String str) {
        super(str);
        this.f6077P = i;
    }

    /* renamed from: a */
    public final int mo22247a() {
        return this.f6077P;
    }
}
