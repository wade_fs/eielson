package com.google.firebase;

public final class R$attr {
    public static final int alpha = 2130968619;
    public static final int coordinatorLayoutStyle = 2130968826;
    public static final int font = 2130968972;
    public static final int fontProviderAuthority = 2130968974;
    public static final int fontProviderCerts = 2130968975;
    public static final int fontProviderFetchStrategy = 2130968976;
    public static final int fontProviderFetchTimeout = 2130968977;
    public static final int fontProviderPackage = 2130968978;
    public static final int fontProviderQuery = 2130968979;
    public static final int fontStyle = 2130968980;
    public static final int fontVariationSettings = 2130968981;
    public static final int fontWeight = 2130968982;
    public static final int keylines = 2130969059;
    public static final int layout_anchor = 2130969075;
    public static final int layout_anchorGravity = 2130969076;
    public static final int layout_behavior = 2130969077;
    public static final int layout_dodgeInsetEdges = 2130969122;
    public static final int layout_insetEdge = 2130969131;
    public static final int layout_keyline = 2130969132;
    public static final int statusBarBackground = 2130969463;
    public static final int ttcIndex = 2130969641;

    private R$attr() {
    }
}
