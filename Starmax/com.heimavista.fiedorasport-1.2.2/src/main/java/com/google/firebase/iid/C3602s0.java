package com.google.firebase.iid;

import android.os.Bundle;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.firebase.iid.s0 */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final /* synthetic */ class C3602s0 implements Runnable {

    /* renamed from: P */
    private final C3598q0 f6121P;

    /* renamed from: Q */
    private final Bundle f6122Q;

    /* renamed from: R */
    private final C4066i f6123R;

    C3602s0(C3598q0 q0Var, Bundle bundle, C4066i iVar) {
        this.f6121P = q0Var;
        this.f6122Q = bundle;
        this.f6123R = iVar;
    }

    public final void run() {
        this.f6121P.mo22264a(this.f6122Q, this.f6123R);
    }
}
