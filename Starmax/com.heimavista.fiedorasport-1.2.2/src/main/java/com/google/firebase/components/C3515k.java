package com.google.firebase.components;

import com.google.firebase.p101h.C3556a;
import java.util.Collections;

/* renamed from: com.google.firebase.components.k */
/* compiled from: com.google.firebase:firebase-components@@16.0.0 */
final /* synthetic */ class C3515k implements C3556a {

    /* renamed from: a */
    private static final C3515k f5954a = new C3515k();

    private C3515k() {
    }

    /* renamed from: a */
    public static C3556a m9616a() {
        return f5954a;
    }

    public Object get() {
        return Collections.emptySet();
    }
}
