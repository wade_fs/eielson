package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.d0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2461d0 extends C2595l4<C2461d0, C2462a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2461d0 zzh;
    private static volatile C2501f6<C2461d0> zzi;
    private int zzc;
    private C2510g0 zzd;
    private C2477e0 zze;
    private boolean zzf;
    private String zzg = "";

    /* renamed from: com.google.android.gms.internal.measurement.d0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2462a extends C2595l4.C2596a<C2461d0, C2462a> implements C2769w5 {
        private C2462a() {
            super(C2461d0.zzh);
        }

        /* renamed from: a */
        public final C2462a mo17396a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2461d0) super.f4288Q).m6177a(str);
            return this;
        }

        /* synthetic */ C2462a(C2413a0 a0Var) {
            this();
        }
    }

    static {
        C2461d0 d0Var = new C2461d0();
        zzh = d0Var;
        C2595l4.m6645a(C2461d0.class, super);
    }

    private C2461d0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6177a(String str) {
        str.getClass();
        this.zzc |= 8;
        this.zzg = str;
    }

    /* renamed from: w */
    public static C2461d0 m6178w() {
        return zzh;
    }

    /* renamed from: n */
    public final boolean mo17388n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final C2510g0 mo17389o() {
        C2510g0 g0Var = this.zzd;
        return g0Var == null ? C2510g0.m6355w() : g0Var;
    }

    /* renamed from: p */
    public final boolean mo17390p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final C2477e0 mo17391q() {
        C2477e0 e0Var = this.zze;
        return e0Var == null ? C2477e0.m6230y() : e0Var;
    }

    /* renamed from: s */
    public final boolean mo17392s() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: t */
    public final boolean mo17393t() {
        return this.zzf;
    }

    /* renamed from: u */
    public final boolean mo17394u() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: v */
    public final String mo17395v() {
        return this.zzg;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2461d0();
            case 2:
                return new C2462a(null);
            case 3:
                return C2595l4.m6643a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\u0007\u0002\u0004\b\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                C2501f6<C2461d0> f6Var = zzi;
                if (f6Var == null) {
                    synchronized (C2461d0.class) {
                        f6Var = zzi;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzh);
                            zzi = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
