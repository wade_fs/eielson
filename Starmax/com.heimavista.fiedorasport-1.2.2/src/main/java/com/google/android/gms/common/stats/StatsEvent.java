package com.google.android.gms.common.stats;

import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable {
    /* renamed from: c */
    public abstract int mo17117c();

    /* renamed from: d */
    public abstract long mo17118d();

    public String toString() {
        long d = mo17118d();
        int c = mo17117c();
        long u = mo17120u();
        String v = mo17121v();
        StringBuilder sb = new StringBuilder(String.valueOf(v).length() + 53);
        sb.append(d);
        sb.append("\t");
        sb.append(c);
        sb.append("\t");
        sb.append(u);
        sb.append(v);
        return sb.toString();
    }

    /* renamed from: u */
    public abstract long mo17120u();

    /* renamed from: v */
    public abstract String mo17121v();
}
