package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1665d;

/* renamed from: com.google.android.datatransport.cct.b.a */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1661a {

    /* renamed from: com.google.android.datatransport.cct.b.a$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C1662a {
        @NonNull
        /* renamed from: a */
        public abstract C1662a mo13444a(int i);

        @NonNull
        /* renamed from: a */
        public abstract C1662a mo13445a(@Nullable String str);

        @NonNull
        /* renamed from: a */
        public abstract C1661a mo13446a();

        @NonNull
        /* renamed from: b */
        public abstract C1662a mo13447b(@Nullable String str);

        @NonNull
        /* renamed from: c */
        public abstract C1662a mo13448c(@Nullable String str);

        @NonNull
        /* renamed from: d */
        public abstract C1662a mo13449d(@Nullable String str);

        @NonNull
        /* renamed from: e */
        public abstract C1662a mo13450e(@Nullable String str);

        @NonNull
        /* renamed from: f */
        public abstract C1662a mo13451f(@Nullable String str);

        @NonNull
        /* renamed from: g */
        public abstract C1662a mo13452g(@Nullable String str);
    }

    @NonNull
    /* renamed from: a */
    public static C1662a m4144a() {
        C1665d.C1667b bVar = new C1665d.C1667b();
        bVar.mo13444a(Integer.MIN_VALUE);
        return bVar;
    }
}
