package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.f9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2504f9 implements C2593l2<C2572j9> {

    /* renamed from: Q */
    private static C2504f9 f4154Q = new C2504f9();

    /* renamed from: P */
    private final C2593l2<C2572j9> f4155P;

    private C2504f9(C2593l2<C2572j9> l2Var) {
        this.f4155P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6336b() {
        return ((C2572j9) f4154Q.mo17285a()).mo17584a();
    }

    /* renamed from: c */
    public static boolean m6337c() {
        return ((C2572j9) f4154Q.mo17285a()).mo17585e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4155P.mo17285a();
    }

    public C2504f9() {
        this(C2579k2.m6601a(new C2553i9()));
    }
}
