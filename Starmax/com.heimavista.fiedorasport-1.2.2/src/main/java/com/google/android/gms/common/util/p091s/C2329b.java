package com.google.android.gms.common.util.p091s;

import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* renamed from: com.google.android.gms.common.util.s.b */
public class C2329b implements ThreadFactory {

    /* renamed from: P */
    private final String f3860P;

    /* renamed from: Q */
    private final ThreadFactory f3861Q;

    public C2329b(String str) {
        this(str, 0);
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.f3861Q.newThread(new C2331d(runnable, 0));
        newThread.setName(this.f3860P);
        return newThread;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    private C2329b(String str, int i) {
        this.f3861Q = Executors.defaultThreadFactory();
        C2258v.m5630a((Object) str, (Object) "Name must not be null");
        this.f3860P = str;
    }
}
