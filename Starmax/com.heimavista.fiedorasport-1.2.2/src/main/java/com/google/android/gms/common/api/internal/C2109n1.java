package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.BinderThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.signin.internal.C3270c;
import com.google.android.gms.signin.internal.zaj;
import java.util.Set;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4049b;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.n1 */
public final class C2109n1 extends C3270c implements C2036f.C2038b, C2036f.C2039c {

    /* renamed from: h */
    private static C2016a.C2017a<? extends C4052e, C4047a> f3392h = C4049b.f7396c;

    /* renamed from: a */
    private final Context f3393a;

    /* renamed from: b */
    private final Handler f3394b;

    /* renamed from: c */
    private final C2016a.C2017a<? extends C4052e, C4047a> f3395c;

    /* renamed from: d */
    private Set<Scope> f3396d;

    /* renamed from: e */
    private C2211e f3397e;

    /* renamed from: f */
    private C4052e f3398f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public C2121q1 f3399g;

    @WorkerThread
    public C2109n1(Context context, Handler handler, @NonNull C2211e eVar) {
        this(context, handler, eVar, f3392h);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: b */
    public final void m5000b(zaj zaj) {
        ConnectionResult c = zaj.mo19492c();
        if (c.mo16482w()) {
            ResolveAccountResponse d = zaj.mo19493d();
            ConnectionResult d2 = d.mo16890d();
            if (!d2.mo16482w()) {
                String valueOf = String.valueOf(d2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.f3399g.mo16686b(d2);
                this.f3398f.mo16528a();
                return;
            }
            this.f3399g.mo16685a(d.mo16889c(), this.f3396d);
        } else {
            this.f3399g.mo16686b(c);
        }
        this.f3398f.mo16528a();
    }

    /* renamed from: H */
    public final C4052e mo16756H() {
        return this.f3398f;
    }

    /* renamed from: I */
    public final void mo16757I() {
        C4052e eVar = this.f3398f;
        if (eVar != null) {
            eVar.mo16528a();
        }
    }

    @WorkerThread
    /* renamed from: L */
    public final void mo16583L(int i) {
        this.f3398f.mo16528a();
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo16758a(C2121q1 q1Var) {
        C4052e eVar = this.f3398f;
        if (eVar != null) {
            eVar.mo16528a();
        }
        this.f3397e.mo16958a(Integer.valueOf(System.identityHashCode(this)));
        C2016a.C2017a<? extends C4052e, C4047a> aVar = this.f3395c;
        Context context = this.f3393a;
        Looper looper = this.f3394b.getLooper();
        C2211e eVar2 = this.f3397e;
        this.f3398f = (C4052e) aVar.mo16371a(context, looper, eVar2, eVar2.mo16967j(), this, this);
        this.f3399g = q1Var;
        Set<Scope> set = this.f3396d;
        if (set == null || set.isEmpty()) {
            this.f3394b.post(new C2113o1(this));
        } else {
            this.f3398f.mo19475b();
        }
    }

    @WorkerThread
    /* renamed from: f */
    public final void mo16584f(@Nullable Bundle bundle) {
        this.f3398f.mo19474a(this);
    }

    @WorkerThread
    public C2109n1(Context context, Handler handler, @NonNull C2211e eVar, C2016a.C2017a<? extends C4052e, C4047a> aVar) {
        this.f3393a = context;
        this.f3394b = handler;
        C2258v.m5630a(eVar, "ClientSettings must not be null");
        this.f3397e = eVar;
        this.f3396d = eVar.mo16966i();
        this.f3395c = aVar;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo16585a(@NonNull ConnectionResult connectionResult) {
        this.f3399g.mo16686b(connectionResult);
    }

    @BinderThread
    /* renamed from: a */
    public final void mo16699a(zaj zaj) {
        this.f3394b.post(new C2117p1(this, zaj));
    }
}
