package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2036f;

/* renamed from: com.google.android.gms.common.api.internal.i0 */
final class C2088i0 implements C2036f.C2038b, C2036f.C2039c {

    /* renamed from: a */
    private final /* synthetic */ C2153z f3343a;

    private C2088i0(C2153z zVar) {
        this.f3343a = zVar;
    }

    /* renamed from: L */
    public final void mo16583L(int i) {
    }

    /* renamed from: a */
    public final void mo16585a(@NonNull ConnectionResult connectionResult) {
        this.f3343a.f3510b.lock();
        try {
            if (this.f3343a.m5194a(connectionResult)) {
                this.f3343a.m5209g();
                this.f3343a.m5204e();
            } else {
                this.f3343a.m5198b(connectionResult);
            }
        } finally {
            this.f3343a.f3510b.unlock();
        }
    }

    /* renamed from: f */
    public final void mo16584f(Bundle bundle) {
        if (this.f3343a.f3526r.mo16968k()) {
            this.f3343a.f3510b.lock();
            try {
                if (this.f3343a.f3519k != null) {
                    this.f3343a.f3519k.mo19474a(new C2077g0(this.f3343a));
                    this.f3343a.f3510b.unlock();
                }
            } finally {
                this.f3343a.f3510b.unlock();
            }
        } else {
            this.f3343a.f3519k.mo19474a(new C2077g0(this.f3343a));
        }
    }

    /* synthetic */ C2088i0(C2153z zVar, C2048a0 a0Var) {
        this(zVar);
    }
}
