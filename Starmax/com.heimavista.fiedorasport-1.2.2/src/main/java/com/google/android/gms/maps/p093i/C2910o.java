package com.google.android.gms.maps.p093i;

import android.os.Parcel;
import p119e.p144d.p145a.p157c.p161c.p165d.C4038g;

/* renamed from: com.google.android.gms.maps.i.o */
public abstract class C2910o extends C4038g implements C2909n {
    public C2910o() {
        super("com.google.android.gms.maps.internal.IOnMapLoadedCallback");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo18485a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        mo18486r();
        parcel2.writeNoException();
        return true;
    }
}
