package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import androidx.annotation.NonNull;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.backends.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C1711c extends C1718h {

    /* renamed from: a */
    private final Context f2694a;

    /* renamed from: b */
    private final C3971a f2695b;

    /* renamed from: c */
    private final C3971a f2696c;

    /* renamed from: d */
    private final String f2697d;

    C1711c(Context context, C3971a aVar, C3971a aVar2, String str) {
        if (context != null) {
            this.f2694a = context;
            if (aVar != null) {
                this.f2695b = aVar;
                if (aVar2 != null) {
                    this.f2696c = aVar2;
                    if (str != null) {
                        this.f2697d = str;
                        return;
                    }
                    throw new NullPointerException("Null backendName");
                }
                throw new NullPointerException("Null monotonicClock");
            }
            throw new NullPointerException("Null wallClock");
        }
        throw new NullPointerException("Null applicationContext");
    }

    /* renamed from: a */
    public Context mo13547a() {
        return this.f2694a;
    }

    @NonNull
    /* renamed from: b */
    public String mo13548b() {
        return this.f2697d;
    }

    /* renamed from: c */
    public C3971a mo13549c() {
        return this.f2696c;
    }

    /* renamed from: d */
    public C3971a mo13550d() {
        return this.f2695b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1718h)) {
            return false;
        }
        C1718h hVar = (C1718h) obj;
        if (!this.f2694a.equals(super.mo13547a()) || !this.f2695b.equals(super.mo13550d()) || !this.f2696c.equals(super.mo13549c()) || !this.f2697d.equals(super.mo13548b())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((((this.f2694a.hashCode() ^ 1000003) * 1000003) ^ this.f2695b.hashCode()) * 1000003) ^ this.f2696c.hashCode()) * 1000003) ^ this.f2697d.hashCode();
    }

    public String toString() {
        return "CreationContext{applicationContext=" + this.f2694a + ", wallClock=" + this.f2695b + ", monotonicClock=" + this.f2696c + ", backendName=" + this.f2697d + "}";
    }
}
