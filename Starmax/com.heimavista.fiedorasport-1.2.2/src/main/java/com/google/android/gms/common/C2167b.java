package com.google.android.gms.common;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.base.R$drawable;
import com.google.android.gms.base.R$string;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.C2078g1;
import com.google.android.gms.common.api.internal.C2080h;
import com.google.android.gms.common.api.internal.zabq;
import com.google.android.gms.common.internal.C2215f;
import com.google.android.gms.common.internal.C2217g;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2318i;
import com.google.android.gms.common.util.C2323n;
import com.google.android.gms.internal.base.C2382h;

/* renamed from: com.google.android.gms.common.b */
public class C2167b extends C2169c {

    /* renamed from: d */
    private static final Object f3543d = new Object();

    /* renamed from: e */
    private static final C2167b f3544e = new C2167b();
    @GuardedBy("mLock")

    /* renamed from: c */
    private String f3545c;

    @SuppressLint({"HandlerLeak"})
    /* renamed from: com.google.android.gms.common.b$a */
    private class C2168a extends C2382h {

        /* renamed from: a */
        private final Context f3546a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C2168a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f3546a = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int c = C2167b.this.mo16831c(this.f3546a);
            if (C2167b.this.mo16833c(c)) {
                C2167b.this.mo16832c(this.f3546a, c);
            }
        }
    }

    /* renamed from: a */
    public static C2167b m5251a() {
        return f3544e;
    }

    /* renamed from: b */
    public boolean mo16830b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a = mo16822a(activity, i, i2, onCancelListener);
        if (a == null) {
            return false;
        }
        m5252a(activity, a, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    /* renamed from: c */
    public void mo16832c(Context context, int i) {
        m5253a(context, i, (String) null, mo16836a(context, i, 0, "n"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final void mo16834d(Context context) {
        new C2168a(context).sendEmptyMessageDelayed(1, 120000);
    }

    /* renamed from: a */
    public Dialog mo16821a(Activity activity, int i, int i2) {
        return mo16822a(activity, i, i2, (DialogInterface.OnCancelListener) null);
    }

    @VisibleForTesting(otherwise = 2)
    /* renamed from: b */
    private final String m5254b() {
        String str;
        synchronized (f3543d) {
            str = this.f3545c;
        }
        return str;
    }

    /* renamed from: a */
    public Dialog mo16822a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return m5250a(activity, i, C2217g.m5472a(activity, mo16825a(activity, i, "d"), i2), onCancelListener);
    }

    /* renamed from: c */
    public int mo16831c(Context context) {
        return super.mo16831c(context);
    }

    /* renamed from: c */
    public final boolean mo16833c(int i) {
        return super.mo16833c(i);
    }

    /* renamed from: a */
    public final boolean mo16827a(Activity activity, @NonNull C2080h hVar, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a = m5250a(activity, i, C2217g.m5473a(hVar, mo16825a(activity, i, "d"), 2), onCancelListener);
        if (a == null) {
            return false;
        }
        m5252a(activity, a, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    /* renamed from: b */
    public final String mo16829b(int i) {
        return super.mo16829b(i);
    }

    /* renamed from: a */
    public final boolean mo16828a(Context context, ConnectionResult connectionResult, int i) {
        PendingIntent a = mo16824a(context, connectionResult);
        if (a == null) {
            return false;
        }
        m5253a(context, connectionResult.mo16475c(), (String) null, GoogleApiActivity.m4635a(context, a, i));
        return true;
    }

    /* renamed from: a */
    public static Dialog m5249a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(C2215f.m5467b(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        m5252a(activity, create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    @Nullable
    /* renamed from: a */
    public final zabq mo16826a(Context context, C2078g1 g1Var) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        zabq zabq = new zabq(g1Var);
        context.registerReceiver(zabq, intentFilter);
        zabq.mo16800a(context);
        if (mo16839a(context, "com.google.android.gms")) {
            return zabq;
        }
        g1Var.mo16700a();
        zabq.mo16799a();
        return null;
    }

    /* renamed from: a */
    public int mo16820a(Context context, int i) {
        return super.mo16820a(context, i);
    }

    @Nullable
    /* renamed from: a */
    public Intent mo16825a(Context context, int i, @Nullable String str) {
        return super.mo16825a(context, i, str);
    }

    @Nullable
    /* renamed from: a */
    public PendingIntent mo16823a(Context context, int i, int i2) {
        return super.mo16823a(context, i, i2);
    }

    @Nullable
    /* renamed from: a */
    public PendingIntent mo16824a(Context context, ConnectionResult connectionResult) {
        if (connectionResult.mo16481v()) {
            return connectionResult.mo16480u();
        }
        return mo16823a(context, connectionResult.mo16475c(), 0);
    }

    /* renamed from: a */
    static Dialog m5250a(Context context, int i, C2217g gVar, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(C2215f.m5467b(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String a = C2215f.m5463a(context, i);
        if (a != null) {
            builder.setPositiveButton(a, gVar);
        }
        String e = C2215f.m5470e(context, i);
        if (e != null) {
            builder.setTitle(e);
        }
        return builder.create();
    }

    /* renamed from: a */
    static void m5252a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            SupportErrorDialogFragment.m4633a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        ErrorDialogFragment.m4629a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @TargetApi(20)
    /* renamed from: a */
    private final void m5253a(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        if (i == 18) {
            mo16834d(context);
        } else if (pendingIntent != null) {
            String d = C2215f.m5469d(context, i);
            String c = C2215f.m5468c(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            NotificationCompat.Builder style = new NotificationCompat.Builder(context).setLocalOnly(true).setAutoCancel(true).setContentTitle(d).setStyle(new NotificationCompat.BigTextStyle().bigText(c));
            if (C2318i.m5785b(context)) {
                C2258v.m5640b(C2323n.m5797f());
                style.setSmallIcon(context.getApplicationInfo().icon).setPriority(2);
                if (C2318i.m5786c(context)) {
                    style.addAction(R$drawable.common_full_open_on_phone, resources.getString(R$string.common_open_on_phone), pendingIntent);
                } else {
                    style.setContentIntent(pendingIntent);
                }
            } else {
                style.setSmallIcon(17301642).setTicker(resources.getString(R$string.common_google_play_services_notification_ticker)).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).setContentText(c);
            }
            if (C2323n.m5800i()) {
                C2258v.m5640b(C2323n.m5800i());
                String b = m5254b();
                if (b == null) {
                    b = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(b);
                    String b2 = C2215f.m5466b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(b, b2, 4));
                    } else if (!b2.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(b2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                style.setChannelId(b);
            }
            Notification build = style.build();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                C2176f.f3569d.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, build);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
