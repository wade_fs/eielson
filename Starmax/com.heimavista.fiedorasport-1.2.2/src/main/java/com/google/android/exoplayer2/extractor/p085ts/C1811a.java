package com.google.android.exoplayer2.extractor.p085ts;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.ts.a */
/* compiled from: lambda */
public final /* synthetic */ class C1811a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1811a f2821a = new C1811a();

    private /* synthetic */ C1811a() {
    }

    public final Extractor[] createExtractors() {
        return Ac3Extractor.m4388a();
    }
}
