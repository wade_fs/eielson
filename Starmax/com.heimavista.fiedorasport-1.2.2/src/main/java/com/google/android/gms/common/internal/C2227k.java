package com.google.android.gms.common.internal;

import android.util.Log;

/* renamed from: com.google.android.gms.common.internal.k */
public final class C2227k {

    /* renamed from: a */
    private final String f3733a;

    /* renamed from: b */
    private final String f3734b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    public C2227k(String str, String str2) {
        C2258v.m5630a((Object) str, (Object) "log tag cannot be null");
        C2258v.m5638a(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        this.f3733a = str;
        if (str2 == null || str2.length() <= 0) {
            this.f3734b = null;
        } else {
            this.f3734b = str2;
        }
    }

    /* renamed from: a */
    public final boolean mo17000a(int i) {
        return Log.isLoggable(this.f3733a, i);
    }

    /* renamed from: b */
    public final void mo17001b(String str, String str2) {
        if (mo17000a(6)) {
            Log.e(str, m5505a(str2));
        }
    }

    /* renamed from: c */
    public final void mo17002c(String str, String str2) {
        if (mo17000a(2)) {
            Log.v(str, m5505a(str2));
        }
    }

    /* renamed from: a */
    public final void mo16998a(String str, String str2) {
        if (mo17000a(3)) {
            Log.d(str, m5505a(str2));
        }
    }

    /* renamed from: a */
    public final void mo16999a(String str, String str2, Throwable th) {
        if (mo17000a(6)) {
            Log.e(str, m5505a(str2), th);
        }
    }

    /* renamed from: a */
    private final String m5505a(String str) {
        String str2 = this.f3734b;
        if (str2 == null) {
            return str;
        }
        return str2.concat(str);
    }

    public C2227k(String str) {
        this(str, null);
    }
}
