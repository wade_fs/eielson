package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.x7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public enum C2787x7 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(C2498f3.f4094Q),
    ENUM(null),
    MESSAGE(null);

    private C2787x7(Object obj) {
    }
}
