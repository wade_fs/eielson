package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2701s0;
import com.google.android.gms.internal.measurement.C2763w0;

/* renamed from: com.google.android.gms.measurement.internal.f */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
interface C3027f {
    /* renamed from: a */
    void mo18993a(C2763w0 w0Var);

    /* renamed from: a */
    boolean mo18994a(long j, C2701s0 s0Var);
}
