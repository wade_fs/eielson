package com.google.android.gms.common.api.internal;

import androidx.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2229l;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: com.google.android.gms.common.api.internal.c0 */
final class C2057c0 extends C2092j0 {

    /* renamed from: Q */
    private final Map<C2016a.C2027f, C2053b0> f3257Q;

    /* renamed from: R */
    final /* synthetic */ C2153z f3258R;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C2057c0(C2153z zVar, Map<C2016a.C2027f, C2053b0> map) {
        super(zVar, null);
        this.f3258R = zVar;
        this.f3257Q = map;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo16643a() {
        C2229l lVar = new C2229l(this.f3258R.f3512d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (C2016a.C2027f fVar : this.f3257Q.keySet()) {
            if (!fVar.mo16536h() || this.f3257Q.get(fVar).f3250c) {
                arrayList2.add(fVar);
            } else {
                arrayList.add(fVar);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = lVar.mo17005a(this.f3258R.f3511c, (C2016a.C2027f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = lVar.mo17005a(this.f3258R.f3511c, (C2016a.C2027f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.f3258R.f3509a.mo16783a(new C2061d0(this, this.f3258R, new ConnectionResult(i, null)));
            return;
        }
        if (this.f3258R.f3521m && this.f3258R.f3519k != null) {
            this.f3258R.f3519k.mo19475b();
        }
        for (C2016a.C2027f fVar2 : this.f3257Q.keySet()) {
            C2197d.C2200c cVar = this.f3257Q.get(fVar2);
            if (!fVar2.mo16536h() || lVar.mo17005a(this.f3258R.f3511c, fVar2) == 0) {
                fVar2.mo16529a(cVar);
            } else {
                this.f3258R.f3509a.mo16783a(new C2068e0(this, this.f3258R, cVar));
            }
        }
    }
}
