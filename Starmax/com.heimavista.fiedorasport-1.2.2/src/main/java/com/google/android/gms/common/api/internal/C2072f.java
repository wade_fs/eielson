package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.R$string;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2190a1;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2264y;

@Deprecated
/* renamed from: com.google.android.gms.common.api.internal.f */
public final class C2072f {

    /* renamed from: d */
    private static final Object f3314d = new Object();

    /* renamed from: e */
    private static C2072f f3315e;

    /* renamed from: a */
    private final String f3316a;

    /* renamed from: b */
    private final Status f3317b;

    /* renamed from: c */
    private final boolean f3318c;

    C2072f(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(R$string.common_google_play_services_unknown_issue));
        boolean z = false;
        if (identifier != 0) {
            this.f3318c = !(resources.getInteger(identifier) != 0 ? true : z);
        } else {
            this.f3318c = false;
        }
        String a = C2190a1.m5350a(context);
        a = a == null ? new C2264y(context).mo17046a("google_app_id") : a;
        if (TextUtils.isEmpty(a)) {
            this.f3317b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.f3316a = null;
            return;
        }
        this.f3316a = a;
        this.f3317b = Status.f3177T;
    }

    /* renamed from: a */
    public static Status m4879a(Context context) {
        Status status;
        C2258v.m5630a(context, "Context must not be null.");
        synchronized (f3314d) {
            if (f3315e == null) {
                f3315e = new C2072f(context);
            }
            status = f3315e.f3317b;
        }
        return status;
    }

    /* renamed from: b */
    public static boolean m4882b() {
        return m4880a("isMeasurementExplicitlyDisabled").f3318c;
    }

    /* renamed from: a */
    public static String m4881a() {
        return m4880a("getGoogleAppId").f3316a;
    }

    /* renamed from: a */
    private static C2072f m4880a(String str) {
        C2072f fVar;
        synchronized (f3314d) {
            if (f3315e != null) {
                fVar = f3315e;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(".");
                throw new IllegalStateException(sb.toString());
            }
        }
        return fVar;
    }
}
