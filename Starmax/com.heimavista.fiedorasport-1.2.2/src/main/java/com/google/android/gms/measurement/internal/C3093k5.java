package com.google.android.gms.measurement.internal;

import androidx.annotation.GuardedBy;
import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.BlockingQueue;

/* renamed from: com.google.android.gms.measurement.internal.k5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3093k5 extends Thread {

    /* renamed from: P */
    private final Object f5294P;

    /* renamed from: Q */
    private final BlockingQueue<C3057h5<?>> f5295Q;
    @GuardedBy("threadLifeCycleLock")

    /* renamed from: R */
    private boolean f5296R = false;

    /* renamed from: S */
    private final /* synthetic */ C3045g5 f5297S;

    public C3093k5(C3045g5 g5Var, String str, BlockingQueue<C3057h5<?>> blockingQueue) {
        this.f5297S = g5Var;
        C2258v.m5629a((Object) str);
        C2258v.m5629a(blockingQueue);
        this.f5294P = new Object();
        this.f5295Q = blockingQueue;
        setName(str);
    }

    /* renamed from: b */
    private final void m8833b() {
        synchronized (this.f5297S.f5157i) {
            if (!this.f5296R) {
                this.f5297S.f5158j.release();
                this.f5297S.f5157i.notifyAll();
                if (this == this.f5297S.f5151c) {
                    C3093k5 unused = this.f5297S.f5151c = null;
                } else if (this == this.f5297S.f5152d) {
                    C3093k5 unused2 = this.f5297S.f5152d = null;
                } else {
                    this.f5297S.mo19015l().mo19001t().mo19042a("Current scheduler thread is neither worker nor network");
                }
                this.f5296R = true;
            }
        }
    }

    /* renamed from: a */
    public final void mo19127a() {
        synchronized (this.f5294P) {
            this.f5294P.notifyAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0075, code lost:
        m8833b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0078, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r0 = 0
        L_0x0001:
            if (r0 != 0) goto L_0x0013
            com.google.android.gms.measurement.internal.g5 r1 = r5.f5297S     // Catch:{ InterruptedException -> 0x000e }
            java.util.concurrent.Semaphore r1 = r1.f5158j     // Catch:{ InterruptedException -> 0x000e }
            r1.acquire()     // Catch:{ InterruptedException -> 0x000e }
            r0 = 1
            goto L_0x0001
        L_0x000e:
            r1 = move-exception
            r5.m8832a(r1)
            goto L_0x0001
        L_0x0013:
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x0081 }
            int r0 = android.os.Process.getThreadPriority(r0)     // Catch:{ all -> 0x0081 }
        L_0x001b:
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.h5<?>> r1 = r5.f5295Q     // Catch:{ all -> 0x0081 }
            java.lang.Object r1 = r1.poll()     // Catch:{ all -> 0x0081 }
            com.google.android.gms.measurement.internal.h5 r1 = (com.google.android.gms.measurement.internal.C3057h5) r1     // Catch:{ all -> 0x0081 }
            if (r1 == 0) goto L_0x0034
            boolean r2 = r1.f5178Q     // Catch:{ all -> 0x0081 }
            if (r2 == 0) goto L_0x002b
            r2 = r0
            goto L_0x002d
        L_0x002b:
            r2 = 10
        L_0x002d:
            android.os.Process.setThreadPriority(r2)     // Catch:{ all -> 0x0081 }
            r1.run()     // Catch:{ all -> 0x0081 }
            goto L_0x001b
        L_0x0034:
            java.lang.Object r1 = r5.f5294P     // Catch:{ all -> 0x0081 }
            monitor-enter(r1)     // Catch:{ all -> 0x0081 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.h5<?>> r2 = r5.f5295Q     // Catch:{ all -> 0x007e }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x007e }
            if (r2 != 0) goto L_0x0053
            com.google.android.gms.measurement.internal.g5 r2 = r5.f5297S     // Catch:{ all -> 0x007e }
            boolean r2 = r2.f5159k     // Catch:{ all -> 0x007e }
            if (r2 != 0) goto L_0x0053
            java.lang.Object r2 = r5.f5294P     // Catch:{ InterruptedException -> 0x004f }
            r3 = 30000(0x7530, double:1.4822E-319)
            r2.wait(r3)     // Catch:{ InterruptedException -> 0x004f }
            goto L_0x0053
        L_0x004f:
            r2 = move-exception
            r5.m8832a(r2)     // Catch:{ all -> 0x007e }
        L_0x0053:
            monitor-exit(r1)     // Catch:{ all -> 0x007e }
            com.google.android.gms.measurement.internal.g5 r1 = r5.f5297S     // Catch:{ all -> 0x0081 }
            java.lang.Object r1 = r1.f5157i     // Catch:{ all -> 0x0081 }
            monitor-enter(r1)     // Catch:{ all -> 0x0081 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.h5<?>> r2 = r5.f5295Q     // Catch:{ all -> 0x007b }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x007b }
            if (r2 != 0) goto L_0x0079
            com.google.android.gms.measurement.internal.g5 r0 = r5.f5297S     // Catch:{ all -> 0x007b }
            com.google.android.gms.measurement.internal.la r0 = r0.mo19013h()     // Catch:{ all -> 0x007b }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5419S0     // Catch:{ all -> 0x007b }
            boolean r0 = r0.mo19146a(r2)     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0074
            r5.m8833b()     // Catch:{ all -> 0x007b }
        L_0x0074:
            monitor-exit(r1)     // Catch:{ all -> 0x007b }
            r5.m8833b()
            return
        L_0x0079:
            monitor-exit(r1)     // Catch:{ all -> 0x007b }
            goto L_0x001b
        L_0x007b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x007b }
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x007e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x007e }
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            r5.m8833b()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3093k5.run():void");
    }

    /* renamed from: a */
    private final void m8832a(InterruptedException interruptedException) {
        this.f5297S.mo19015l().mo19004w().mo19043a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
