package com.google.android.gms.common.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.C2156j;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.internal.b */
public class C2191b {
    @NonNull
    /* renamed from: a */
    public static C2030b m5353a(@NonNull Status status) {
        if (status.mo16517u()) {
            return new C2156j(status);
        }
        return new C2030b(status);
    }
}
