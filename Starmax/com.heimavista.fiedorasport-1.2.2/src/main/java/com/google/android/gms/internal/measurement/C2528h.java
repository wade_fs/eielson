package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.h */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2528h extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ C2538h9 f4198T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4199U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2528h(C2525gd gdVar, C2538h9 h9Var) {
        super(gdVar);
        this.f4199U = gdVar;
        this.f4198T = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4199U.f4192g.getGmpAppId(this.f4198T);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17345b() {
        this.f4198T.mo17292d(null);
    }
}
