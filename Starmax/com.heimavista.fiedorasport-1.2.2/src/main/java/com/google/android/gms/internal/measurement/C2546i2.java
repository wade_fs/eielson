package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.i2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2546i2 {
    /* renamed from: a */
    public static <T> T m6469a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }
}
