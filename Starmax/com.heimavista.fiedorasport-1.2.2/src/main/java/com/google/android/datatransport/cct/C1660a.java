package com.google.android.datatransport.cct;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.C1750C;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.p147i.C3897f;

/* renamed from: com.google.android.datatransport.cct.a */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1660a implements C3897f {

    /* renamed from: c */
    static final String f2560c = C1706f.m4269a("hts/frbslgiggolai.o/0clgbthfra=snpoo", "tp:/ieaeogn.ogepscmvc/o/ac?omtjo_rt3");

    /* renamed from: d */
    static final String f2561d = C1706f.m4269a("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");

    /* renamed from: e */
    private static final String f2562e = C1706f.m4269a("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");

    /* renamed from: f */
    private static final Set<C3877b> f2563f = Collections.unmodifiableSet(new HashSet(Arrays.asList(C3877b.m11673a("proto"), C3877b.m11673a("json"))));

    /* renamed from: g */
    public static final C1660a f2564g = new C1660a(f2561d, f2562e);
    @NonNull

    /* renamed from: a */
    private final String f2565a;
    @Nullable

    /* renamed from: b */
    private final String f2566b;

    static {
        new C1660a(f2560c, null);
    }

    public C1660a(@NonNull String str, @Nullable String str2) {
        this.f2565a = str;
        this.f2566b = str2;
    }

    @NonNull
    /* renamed from: a */
    public static C1660a m4138a(@NonNull byte[] bArr) {
        String str = new String(bArr, Charset.forName(C1750C.UTF8_NAME));
        if (str.startsWith("1$")) {
            String[] split = str.substring(2).split(Pattern.quote("\\"), 2);
            if (split.length == 2) {
                String str2 = split[0];
                if (!str2.isEmpty()) {
                    String str3 = split[1];
                    if (str3.isEmpty()) {
                        str3 = null;
                    }
                    return new C1660a(str2, str3);
                }
                throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
            }
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        throw new IllegalArgumentException("Version marker missing from extras");
    }

    @Nullable
    /* renamed from: b */
    public byte[] mo13439b() {
        return mo13440c();
    }

    @Nullable
    /* renamed from: c */
    public byte[] mo13440c() {
        if (this.f2566b == null && this.f2565a == null) {
            return null;
        }
        Object[] objArr = new Object[4];
        objArr[0] = "1$";
        objArr[1] = this.f2565a;
        objArr[2] = "\\";
        String str = this.f2566b;
        if (str == null) {
            str = "";
        }
        objArr[3] = str;
        return String.format("%s%s%s%s", objArr).getBytes(Charset.forName(C1750C.UTF8_NAME));
    }

    @Nullable
    /* renamed from: d */
    public String mo13441d() {
        return this.f2566b;
    }

    @NonNull
    /* renamed from: e */
    public String mo13442e() {
        return this.f2565a;
    }

    @NonNull
    public String getName() {
        return "cct";
    }

    /* renamed from: a */
    public Set<C3877b> mo13438a() {
        return f2563f;
    }
}
