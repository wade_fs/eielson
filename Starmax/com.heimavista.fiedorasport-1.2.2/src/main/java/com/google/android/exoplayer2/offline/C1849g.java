package com.google.android.exoplayer2.offline;

/* renamed from: com.google.android.exoplayer2.offline.g */
/* compiled from: lambda */
public final /* synthetic */ class C1849g implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ DownloadManager f2850P;

    /* renamed from: Q */
    private final /* synthetic */ DownloadAction[] f2851Q;

    public /* synthetic */ C1849g(DownloadManager downloadManager, DownloadAction[] downloadActionArr) {
        this.f2850P = downloadManager;
        this.f2851Q = downloadActionArr;
    }

    public final void run() {
        this.f2850P.mo14716a(this.f2851Q);
    }
}
