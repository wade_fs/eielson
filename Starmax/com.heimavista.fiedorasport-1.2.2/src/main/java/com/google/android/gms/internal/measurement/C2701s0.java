package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2733u0;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.s0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2701s0 extends C2595l4<C2701s0, C2702a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2701s0 zzi;
    private static volatile C2501f6<C2701s0> zzj;
    private int zzc;
    private C2738u4<C2733u0> zzd = C2595l4.m6649m();
    private String zze = "";
    private long zzf;
    private long zzg;
    private int zzh;

    /* renamed from: com.google.android.gms.internal.measurement.s0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2702a extends C2595l4.C2596a<C2701s0, C2702a> implements C2769w5 {
        private C2702a() {
            super(C2701s0.zzi);
        }

        /* renamed from: a */
        public final C2733u0 mo17863a(int i) {
            return ((C2701s0) super.f4288Q).mo17846b(i);
        }

        /* renamed from: b */
        public final C2702a mo17864b(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7157c(i);
            return this;
        }

        /* renamed from: j */
        public final List<C2733u0> mo17866j() {
            return Collections.unmodifiableList(((C2701s0) super.f4288Q).mo17847n());
        }

        /* renamed from: k */
        public final int mo17867k() {
            return ((C2701s0) super.f4288Q).mo17848o();
        }

        /* renamed from: l */
        public final C2702a mo17868l() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7142A();
            return this;
        }

        /* renamed from: m */
        public final String mo17869m() {
            return ((C2701s0) super.f4288Q).mo17849p();
        }

        /* renamed from: n */
        public final long mo17870n() {
            return ((C2701s0) super.f4288Q).mo17851s();
        }

        /* renamed from: o */
        public final long mo17871o() {
            return ((C2701s0) super.f4288Q).mo17853u();
        }

        /* synthetic */ C2702a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2702a mo17857a(int i, C2733u0 u0Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7143a(i, u0Var);
            return this;
        }

        /* renamed from: b */
        public final C2702a mo17865b(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7155b(j);
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17856a(int i, C2733u0.C2734a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7143a(i, (C2733u0) super.mo17679i());
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17860a(C2733u0 u0Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7152a(u0Var);
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17859a(C2733u0.C2734a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7152a((C2733u0) super.mo17679i());
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17861a(Iterable<? extends C2733u0> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7153a(iterable);
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17862a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7154a(str);
            return this;
        }

        /* renamed from: a */
        public final C2702a mo17858a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2701s0) super.f4288Q).m7144a(j);
            return this;
        }
    }

    static {
        C2701s0 s0Var = new C2701s0();
        zzi = s0Var;
        C2595l4.m6645a(C2701s0.class, super);
    }

    private C2701s0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public final void m7142A() {
        this.zzd = C2595l4.m6649m();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7143a(int i, C2733u0 u0Var) {
        u0Var.getClass();
        m7160z();
        this.zzd.set(i, u0Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7157c(int i) {
        m7160z();
        this.zzd.remove(i);
    }

    /* renamed from: x */
    public static C2702a m7158x() {
        return (C2702a) zzi.mo17668i();
    }

    /* renamed from: z */
    private final void m7160z() {
        if (!this.zzd.mo17938a()) {
            this.zzd = C2595l4.m6642a(this.zzd);
        }
    }

    /* renamed from: b */
    public final C2733u0 mo17846b(int i) {
        return this.zzd.get(i);
    }

    /* renamed from: n */
    public final List<C2733u0> mo17847n() {
        return this.zzd;
    }

    /* renamed from: o */
    public final int mo17848o() {
        return this.zzd.size();
    }

    /* renamed from: p */
    public final String mo17849p() {
        return this.zze;
    }

    /* renamed from: q */
    public final boolean mo17850q() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: s */
    public final long mo17851s() {
        return this.zzf;
    }

    /* renamed from: t */
    public final boolean mo17852t() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: u */
    public final long mo17853u() {
        return this.zzg;
    }

    /* renamed from: v */
    public final boolean mo17854v() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: w */
    public final int mo17855w() {
        return this.zzh;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7155b(long j) {
        this.zzc |= 4;
        this.zzg = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7152a(C2733u0 u0Var) {
        u0Var.getClass();
        m7160z();
        this.zzd.add(u0Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7153a(Iterable<? extends C2733u0> iterable) {
        m7160z();
        C2766w2.m7700a(iterable, this.zzd);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7154a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zze = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7144a(long j) {
        this.zzc |= 2;
        this.zzf = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2701s0();
            case 2:
                return new C2702a(null);
            case 3:
                return C2595l4.m6643a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\b\u0000\u0003\u0002\u0001\u0004\u0002\u0002\u0005\u0004\u0003", new Object[]{"zzc", "zzd", C2733u0.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                C2501f6<C2701s0> f6Var = zzj;
                if (f6Var == null) {
                    synchronized (C2701s0.class) {
                        f6Var = zzj;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzi);
                            zzj = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
