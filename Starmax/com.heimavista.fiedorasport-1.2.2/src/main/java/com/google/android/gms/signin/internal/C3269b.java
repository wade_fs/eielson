package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.signin.internal.b */
public final class C3269b implements Parcelable.Creator<zaa> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                intent = (Intent) C2248a.m5553a(parcel, a, Intent.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zaa(i, i2, intent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zaa[i];
    }
}
