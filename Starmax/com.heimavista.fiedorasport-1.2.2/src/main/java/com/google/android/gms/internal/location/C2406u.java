package com.google.android.gms.internal.location;

import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.location.u */
public interface C2406u<T extends IInterface> {
    /* renamed from: a */
    void mo17226a();

    /* renamed from: b */
    T mo17227b();
}
