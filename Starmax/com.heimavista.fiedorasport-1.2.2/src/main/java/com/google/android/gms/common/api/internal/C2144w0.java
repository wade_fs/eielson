package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.internal.base.C2382h;

/* renamed from: com.google.android.gms.common.api.internal.w0 */
final class C2144w0 extends C2382h {

    /* renamed from: a */
    private final /* synthetic */ C2136u0 f3497a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2144w0(C2136u0 u0Var, Looper looper) {
        super(looper);
        this.f3497a = u0Var;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((C2140v0) message.obj).mo16788a(this.f3497a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GACStateManager", sb.toString());
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
