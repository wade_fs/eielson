package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2555ib;

/* renamed from: com.google.android.gms.measurement.internal.q1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3160q1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5558a = new C3160q1();

    private C3160q1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2555ib.m6489c());
    }
}
