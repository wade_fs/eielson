package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.y6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3253y6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5772P;

    /* renamed from: Q */
    private final /* synthetic */ String f5773Q;

    /* renamed from: R */
    private final /* synthetic */ String f5774R;

    /* renamed from: S */
    private final /* synthetic */ String f5775S;

    /* renamed from: T */
    private final /* synthetic */ boolean f5776T;

    /* renamed from: U */
    private final /* synthetic */ C3154p6 f5777U;

    C3253y6(C3154p6 p6Var, AtomicReference atomicReference, String str, String str2, String str3, boolean z) {
        this.f5777U = p6Var;
        this.f5772P = atomicReference;
        this.f5773Q = str;
        this.f5774R = str2;
        this.f5775S = str3;
        this.f5776T = z;
    }

    public final void run() {
        this.f5777U.f5134a.mo19079F().mo19387a(this.f5772P, this.f5773Q, this.f5774R, this.f5775S, this.f5776T);
    }
}
