package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.util.p091s.C2330c;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.common.api.internal.m1 */
public final class C2105m1 {

    /* renamed from: a */
    private static final ExecutorService f3388a = new ThreadPoolExecutor(0, 4, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new C2330c("GAC_Transform"));

    /* renamed from: a */
    public static ExecutorService m4993a() {
        return f3388a;
    }
}
