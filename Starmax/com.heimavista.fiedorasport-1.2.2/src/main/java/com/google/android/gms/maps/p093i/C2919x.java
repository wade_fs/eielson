package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;

/* renamed from: com.google.android.gms.maps.i.x */
public final class C2919x extends C4032a implements C2899f {
    C2919x(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaFragmentDelegate");
    }

    /* renamed from: a */
    public final void mo18462a(C3988b bVar, StreetViewPanoramaOptions streetViewPanoramaOptions, Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, bVar);
        C4039h.m12044a(a, streetViewPanoramaOptions);
        C4039h.m12044a(a, bundle);
        mo23667b(2, a);
    }

    /* renamed from: b */
    public final void mo18464b(Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, bundle);
        mo23667b(3, a);
    }

    /* renamed from: c */
    public final void mo18465c() {
        mo23667b(14, mo23664a());
    }

    /* renamed from: d */
    public final void mo18466d() {
        mo23667b(6, mo23664a());
    }

    /* renamed from: g */
    public final void mo18467g() {
        mo23667b(7, mo23664a());
    }

    public final void onLowMemory() {
        mo23667b(9, mo23664a());
    }

    public final void onResume() {
        mo23667b(5, mo23664a());
    }

    public final void onStart() {
        mo23667b(13, mo23664a());
    }

    /* renamed from: b */
    public final void mo18463b() {
        mo23667b(8, mo23664a());
    }

    /* renamed from: a */
    public final C3988b mo18459a(C3988b bVar, C3988b bVar2, Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, bVar);
        C4039h.m12043a(a, bVar2);
        C4039h.m12044a(a, bundle);
        Parcel a2 = mo23665a(4, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: a */
    public final void mo18460a(Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, bundle);
        Parcel a2 = mo23665a(10, a);
        if (a2.readInt() != 0) {
            bundle.readFromParcel(a2);
        }
        a2.recycle();
    }

    /* renamed from: a */
    public final void mo18461a(C2916u uVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, uVar);
        mo23667b(12, a);
    }
}
