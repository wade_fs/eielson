package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/* renamed from: com.google.android.gms.auth.api.signin.internal.n */
public final class C2001n {

    /* renamed from: b */
    private static C2001n f3151b;

    /* renamed from: a */
    private C1989b f3152a;

    private C2001n(Context context) {
        this.f3152a = C1989b.m4567a(context);
        this.f3152a.mo16438b();
        this.f3152a.mo16439c();
    }

    /* renamed from: a */
    public static synchronized C2001n m4604a(@NonNull Context context) {
        C2001n b;
        synchronized (C2001n.class) {
            b = m4605b(context.getApplicationContext());
        }
        return b;
    }

    /* renamed from: b */
    private static synchronized C2001n m4605b(Context context) {
        C2001n nVar;
        synchronized (C2001n.class) {
            if (f3151b == null) {
                f3151b = new C2001n(context);
            }
            nVar = f3151b;
        }
        return nVar;
    }

    /* renamed from: a */
    public final synchronized void mo16457a() {
        this.f3152a.mo16436a();
    }

    /* renamed from: a */
    public final synchronized void mo16458a(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.f3152a.mo16437a(googleSignInAccount, googleSignInOptions);
    }
}
