package com.google.android.exoplayer2.mediacodec;

import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public interface MediaCodecSelector {
    public static final MediaCodecSelector DEFAULT = new MediaCodecSelector() {
        /* class com.google.android.exoplayer2.mediacodec.MediaCodecSelector.C18161 */

        public List<MediaCodecInfo> getDecoderInfos(String str, boolean z) {
            List<MediaCodecInfo> decoderInfos = MediaCodecUtil.getDecoderInfos(str, z);
            if (decoderInfos.isEmpty()) {
                return Collections.emptyList();
            }
            return Collections.singletonList(decoderInfos.get(0));
        }

        @Nullable
        public MediaCodecInfo getPassthroughDecoderInfo() {
            return MediaCodecUtil.getPassthroughDecoderInfo();
        }
    };
    public static final MediaCodecSelector DEFAULT_WITH_FALLBACK = new MediaCodecSelector() {
        /* class com.google.android.exoplayer2.mediacodec.MediaCodecSelector.C18172 */

        public List<MediaCodecInfo> getDecoderInfos(String str, boolean z) {
            return MediaCodecUtil.getDecoderInfos(str, z);
        }

        @Nullable
        public MediaCodecInfo getPassthroughDecoderInfo() {
            return MediaCodecUtil.getPassthroughDecoderInfo();
        }
    };

    List<MediaCodecInfo> getDecoderInfos(String str, boolean z);

    @Nullable
    MediaCodecInfo getPassthroughDecoderInfo();
}
