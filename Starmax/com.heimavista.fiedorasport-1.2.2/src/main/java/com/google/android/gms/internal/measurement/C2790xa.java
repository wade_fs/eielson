package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.xa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2790xa implements C2593l2<C2425ab> {

    /* renamed from: Q */
    private static C2790xa f4582Q = new C2790xa();

    /* renamed from: P */
    private final C2593l2<C2425ab> f4583P;

    private C2790xa(C2593l2<C2425ab> l2Var) {
        this.f4583P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7778b() {
        return ((C2425ab) f4582Q.mo17285a()).mo17290a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4583P.mo17285a();
    }

    public C2790xa() {
        this(C2579k2.m6601a(new C2821za()));
    }
}
