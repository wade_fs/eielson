package com.google.android.gms.common.internal.p089z;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.base.C2375a;
import com.google.android.gms.internal.base.C2377c;

/* renamed from: com.google.android.gms.common.internal.z.n */
public final class C2279n extends C2375a implements C2278m {
    C2279n(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    /* renamed from: a */
    public final void mo17050a(C2276k kVar) {
        Parcel H = mo17182H();
        C2377c.m5899a(H, kVar);
        mo17186c(1, H);
    }
}
