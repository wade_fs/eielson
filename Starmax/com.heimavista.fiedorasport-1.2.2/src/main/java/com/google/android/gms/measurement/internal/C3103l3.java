package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2805y9;

/* renamed from: com.google.android.gms.measurement.internal.l3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3103l3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5321a = new C3103l3();

    private C3103l3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2805y9.m7885c());
    }
}
