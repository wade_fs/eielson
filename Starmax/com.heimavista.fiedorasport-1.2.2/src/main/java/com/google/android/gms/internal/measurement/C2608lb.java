package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.lb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2608lb implements C2540hb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4309a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4310b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4309a = c2Var.mo17367a("measurement.personalized_ads_signals_collection_enabled", true);
        f4310b = c2Var.mo17367a("measurement.personalized_ads_property_translation_enabled", true);
    }

    /* renamed from: a */
    public final boolean mo17565a() {
        return f4309a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo17566e() {
        return f4310b.mo18128b().booleanValue();
    }
}
