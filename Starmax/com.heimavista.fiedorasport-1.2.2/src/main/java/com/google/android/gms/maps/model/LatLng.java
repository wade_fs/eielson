package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class LatLng extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<LatLng> CREATOR = new C2941l();

    /* renamed from: P */
    public final double f4834P;

    /* renamed from: Q */
    public final double f4835Q;

    public LatLng(double d, double d2) {
        if (-180.0d > d2 || d2 >= 180.0d) {
            this.f4835Q = ((((d2 - 180.0d) % 360.0d) + 360.0d) % 360.0d) - 180.0d;
        } else {
            this.f4835Q = d2;
        }
        this.f4834P = Math.max(-90.0d, Math.min(90.0d, d));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLng)) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.doubleToLongBits(this.f4834P) == Double.doubleToLongBits(latLng.f4834P) && Double.doubleToLongBits(this.f4835Q) == Double.doubleToLongBits(latLng.f4835Q);
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f4834P);
        long doubleToLongBits2 = Double.doubleToLongBits(this.f4835Q);
        return ((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    public final String toString() {
        double d = this.f4834P;
        double d2 = this.f4835Q;
        StringBuilder sb = new StringBuilder(60);
        sb.append("lat/lng: (");
        sb.append(d);
        sb.append(",");
        sb.append(d2);
        sb.append(")");
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5589a(parcel, 2, this.f4834P);
        C2250b.m5589a(parcel, 3, this.f4835Q);
        C2250b.m5587a(parcel, a);
    }
}
