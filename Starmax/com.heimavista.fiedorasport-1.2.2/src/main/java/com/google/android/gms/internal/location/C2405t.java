package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.internal.location.t */
public final class C2405t implements Parcelable.Creator<zzbh> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        double d = 0.0d;
        double d2 = 0.0d;
        String str = null;
        long j = 0;
        int i = 0;
        short s = 0;
        float f = 0.0f;
        int i2 = 0;
        int i3 = -1;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    str = C2248a.m5573n(parcel2, a);
                    break;
                case 2:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 3:
                    s = C2248a.m5548D(parcel2, a);
                    break;
                case 4:
                    d = C2248a.m5580u(parcel2, a);
                    break;
                case 5:
                    d2 = C2248a.m5580u(parcel2, a);
                    break;
                case 6:
                    f = C2248a.m5582w(parcel2, a);
                    break;
                case 7:
                    i = C2248a.m5585z(parcel2, a);
                    break;
                case 8:
                    i2 = C2248a.m5585z(parcel2, a);
                    break;
                case 9:
                    i3 = C2248a.m5585z(parcel2, a);
                    break;
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new zzbh(str, i, s, d, d2, f, j, i2, i3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbh[i];
    }
}
