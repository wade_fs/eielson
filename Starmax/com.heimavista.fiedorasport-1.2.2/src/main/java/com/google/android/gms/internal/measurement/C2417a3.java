package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.a3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2417a3 {

    /* renamed from: a */
    public int f3971a;

    /* renamed from: b */
    public long f3972b;

    /* renamed from: c */
    public Object f3973c;

    /* renamed from: d */
    public final C2798y3 f3974d;

    C2417a3(C2798y3 y3Var) {
        if (y3Var != null) {
            this.f3974d = y3Var;
            return;
        }
        throw new NullPointerException();
    }
}
