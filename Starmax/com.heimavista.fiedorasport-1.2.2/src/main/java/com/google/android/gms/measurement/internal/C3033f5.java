package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: com.google.android.gms.measurement.internal.f5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C3033f5 {
    /* renamed from: a */
    BroadcastReceiver.PendingResult mo18701a();

    /* renamed from: a */
    void mo18702a(Context context, Intent intent);
}
