package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzv extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzv> CREATOR = new C3122ma();

    /* renamed from: P */
    public String f5836P;

    /* renamed from: Q */
    public String f5837Q;

    /* renamed from: R */
    public zzkq f5838R;

    /* renamed from: S */
    public long f5839S;

    /* renamed from: T */
    public boolean f5840T;

    /* renamed from: U */
    public String f5841U;

    /* renamed from: V */
    public zzan f5842V;

    /* renamed from: W */
    public long f5843W;

    /* renamed from: X */
    public zzan f5844X;

    /* renamed from: Y */
    public long f5845Y;

    /* renamed from: Z */
    public zzan f5846Z;

    zzv(zzv zzv) {
        C2258v.m5629a(zzv);
        this.f5836P = zzv.f5836P;
        this.f5837Q = zzv.f5837Q;
        this.f5838R = zzv.f5838R;
        this.f5839S = zzv.f5839S;
        this.f5840T = zzv.f5840T;
        this.f5841U = zzv.f5841U;
        this.f5842V = zzv.f5842V;
        this.f5843W = zzv.f5843W;
        this.f5844X = zzv.f5844X;
        this.f5845Y = zzv.f5845Y;
        this.f5846Z = zzv.f5846Z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.measurement.internal.zzkq, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.measurement.internal.zzan, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5602a(parcel, 2, this.f5836P, false);
        C2250b.m5602a(parcel, 3, this.f5837Q, false);
        C2250b.m5596a(parcel, 4, (Parcelable) this.f5838R, i, false);
        C2250b.m5592a(parcel, 5, this.f5839S);
        C2250b.m5605a(parcel, 6, this.f5840T);
        C2250b.m5602a(parcel, 7, this.f5841U, false);
        C2250b.m5596a(parcel, 8, (Parcelable) this.f5842V, i, false);
        C2250b.m5592a(parcel, 9, this.f5843W);
        C2250b.m5596a(parcel, 10, (Parcelable) this.f5844X, i, false);
        C2250b.m5592a(parcel, 11, this.f5845Y);
        C2250b.m5596a(parcel, 12, (Parcelable) this.f5846Z, i, false);
        C2250b.m5587a(parcel, a);
    }

    zzv(String str, String str2, zzkq zzkq, long j, boolean z, String str3, zzan zzan, long j2, zzan zzan2, long j3, zzan zzan3) {
        this.f5836P = str;
        this.f5837Q = str2;
        this.f5838R = zzkq;
        this.f5839S = j;
        this.f5840T = z;
        this.f5841U = str3;
        this.f5842V = zzan;
        this.f5843W = j2;
        this.f5844X = zzan2;
        this.f5845Y = j3;
        this.f5846Z = zzan3;
    }
}
