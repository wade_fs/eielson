package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.n6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2633n6 {

    /* renamed from: a */
    private static final Class<?> f4351a = m6904d();

    /* renamed from: b */
    private static final C2469d7<?, ?> f4352b = m6879a(false);

    /* renamed from: c */
    private static final C2469d7<?, ?> f4353c = m6879a(true);

    /* renamed from: d */
    private static final C2469d7<?, ?> f4354d = new C2502f7();

    /* renamed from: a */
    public static void m6888a(Class<?> cls) {
        Class<?> cls2;
        if (!C2595l4.class.isAssignableFrom(cls) && (cls2 = f4351a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    /* renamed from: b */
    public static void m6897b(int i, List<Float> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17993n(i, list, z);
        }
    }

    /* renamed from: c */
    public static void m6901c(int i, List<Long> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17980d(i, list, z);
        }
    }

    /* renamed from: d */
    public static void m6905d(int i, List<Long> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17990k(i, list, z);
        }
    }

    /* renamed from: e */
    public static void m6909e(int i, List<Long> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17989j(i, list, z);
        }
    }

    /* renamed from: f */
    public static void m6912f(int i, List<Long> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17988i(i, list, z);
        }
    }

    /* renamed from: g */
    public static void m6915g(int i, List<Long> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17983e(i, list, z);
        }
    }

    /* renamed from: h */
    public static void m6918h(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17974b(i, list, z);
        }
    }

    /* renamed from: i */
    public static void m6921i(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17986g(i, list, z);
        }
    }

    /* renamed from: j */
    public static void m6924j(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17991l(i, list, z);
        }
    }

    /* renamed from: k */
    public static void m6925k(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17985f(i, list, z);
        }
    }

    /* renamed from: l */
    public static void m6926l(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17987h(i, list, z);
        }
    }

    /* renamed from: m */
    public static void m6927m(int i, List<Integer> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17977c(i, list, z);
        }
    }

    /* renamed from: n */
    public static void m6928n(int i, List<Boolean> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17966a(i, list, z);
        }
    }

    /* renamed from: b */
    public static void m6895b(int i, List<C2498f3> list, C2771w7 w7Var) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17964a(i, list);
        }
    }

    /* renamed from: c */
    static int m6899c(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2549i5) {
            C2549i5 i5Var = (C2549i5) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7240f(i5Var.mo17573b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7240f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: d */
    static int m6903d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2616m4) {
            C2616m4 m4Var = (C2616m4) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7254k(m4Var.mo17733c(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7254k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: e */
    static int m6907e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2616m4) {
            C2616m4 m4Var = (C2616m4) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7237f(m4Var.mo17733c(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7237f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: f */
    static int m6911f(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2616m4) {
            C2616m4 m4Var = (C2616m4) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7241g(m4Var.mo17733c(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7241g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: g */
    static int m6914g(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2616m4) {
            C2616m4 m4Var = (C2616m4) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7245h(m4Var.mo17733c(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7245h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: h */
    static int m6917h(List<?> list) {
        return list.size() << 2;
    }

    /* renamed from: i */
    static int m6920i(List<?> list) {
        return list.size() << 3;
    }

    /* renamed from: j */
    static int m6923j(List<?> list) {
        return list.size();
    }

    /* renamed from: a */
    public static void m6884a(int i, List<Double> list, C2771w7 w7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17992m(i, list, z);
        }
    }

    /* renamed from: h */
    static int m6916h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * C2720t3.m7250i(i, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.g(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.g(int, int):int
      com.google.android.gms.internal.measurement.t3.g(int, long):int */
    /* renamed from: i */
    static int m6919i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * C2720t3.m7243g(i, 0L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.b(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.b(int, double):int
      com.google.android.gms.internal.measurement.t3.b(int, float):int
      com.google.android.gms.internal.measurement.t3.b(int, java.lang.String):int
      com.google.android.gms.internal.measurement.t3.b(int, int):void
      com.google.android.gms.internal.measurement.t3.b(int, long):void
      com.google.android.gms.internal.measurement.t3.b(int, com.google.android.gms.internal.measurement.f3):void
      com.google.android.gms.internal.measurement.t3.b(int, boolean):int */
    /* renamed from: j */
    static int m6922j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * C2720t3.m7226b(i, true);
    }

    /* renamed from: b */
    public static void m6896b(int i, List<?> list, C2771w7 w7Var, C2603l6 l6Var) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17965a(i, list, l6Var);
        }
    }

    /* renamed from: a */
    public static void m6882a(int i, List<String> list, C2771w7 w7Var) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17972b(i, list);
        }
    }

    /* renamed from: b */
    static int m6893b(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2549i5) {
            C2549i5 i5Var = (C2549i5) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7236e(i5Var.mo17573b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7236e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: a */
    public static void m6883a(int i, List<?> list, C2771w7 w7Var, C2603l6 l6Var) {
        if (list != null && !list.isEmpty()) {
            w7Var.mo17973b(i, list, l6Var);
        }
    }

    /* renamed from: c */
    static int m6898c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6899c(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: d */
    static int m6902d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6903d(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: e */
    static int m6906e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6907e(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: f */
    static int m6910f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6911f(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: g */
    static int m6913g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6914g(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: a */
    static int m6877a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C2549i5) {
            C2549i5 i5Var = (C2549i5) list;
            i = 0;
            while (i2 < size) {
                i += C2720t3.m7233d(i5Var.mo17573b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + C2720t3.m7233d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* renamed from: c */
    public static C2469d7<?, ?> m6900c() {
        return f4354d;
    }

    /* renamed from: d */
    private static Class<?> m6904d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: e */
    private static Class<?> m6908e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: b */
    static int m6892b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return m6893b(list) + (size * C2720t3.m7234e(i));
    }

    /* renamed from: a */
    static int m6876a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return m6877a(list) + (list.size() * C2720t3.m7234e(i));
    }

    /* renamed from: b */
    static int m6890b(int i, List<C2498f3> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * C2720t3.m7234e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += C2720t3.m7216a(list.get(i2));
        }
        return e;
    }

    /* renamed from: a */
    static int m6874a(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int e = C2720t3.m7234e(i) * size;
        if (list instanceof C2484e5) {
            C2484e5 e5Var = (C2484e5) list;
            while (i4 < size) {
                Object b = e5Var.mo17325b(i4);
                if (b instanceof C2498f3) {
                    i3 = C2720t3.m7216a((C2498f3) b);
                } else {
                    i3 = C2720t3.m7218a((String) b);
                }
                e += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof C2498f3) {
                    i2 = C2720t3.m7216a((C2498f3) obj);
                } else {
                    i2 = C2720t3.m7218a((String) obj);
                }
                e += i2;
                i4++;
            }
        }
        return e;
    }

    /* renamed from: b */
    static int m6891b(int i, List<C2739u5> list, C2603l6 l6Var) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += C2720t3.m7229c(i, list.get(i3), l6Var);
        }
        return i2;
    }

    /* renamed from: b */
    public static C2469d7<?, ?> m6894b() {
        return f4353c;
    }

    /* renamed from: a */
    static int m6873a(int i, Object obj, C2603l6 l6Var) {
        if (obj instanceof C2451c5) {
            return C2720t3.m7214a(i, (C2451c5) obj);
        }
        return C2720t3.m7224b(i, (C2739u5) obj, l6Var);
    }

    /* renamed from: a */
    static int m6875a(int i, List<?> list, C2603l6 l6Var) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = C2720t3.m7234e(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof C2451c5) {
                i2 = C2720t3.m7215a((C2451c5) obj);
            } else {
                i2 = C2720t3.m7217a((C2739u5) obj, l6Var);
            }
            e += i2;
        }
        return e;
    }

    /* renamed from: a */
    public static C2469d7<?, ?> m6878a() {
        return f4352b;
    }

    /* renamed from: a */
    private static C2469d7<?, ?> m6879a(boolean z) {
        try {
            Class<?> e = m6908e();
            if (e == null) {
                return null;
            }
            return (C2469d7) e.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: a */
    static boolean m6889a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    /* renamed from: a */
    static <T> void m6887a(C2692r5 r5Var, T t, T t2, long j) {
        C2566j7.m6522a(t, j, r5Var.mo17828a(C2566j7.m6544f(t, j), C2566j7.m6544f(t2, j)));
    }

    /* renamed from: a */
    static <T, FT extends C2466d4<FT>> void m6885a(C2418a4 a4Var, Object obj, Object obj2) {
        C2434b4 a = a4Var.mo17267a(obj2);
        if (!a.f3996a.isEmpty()) {
            a4Var.mo17271b(obj).mo17311a(a);
        }
    }

    /* renamed from: a */
    static <T, UT, UB> void m6886a(C2469d7 d7Var, Object obj, Object obj2) {
        d7Var.mo17422a(obj, d7Var.mo17427c(d7Var.mo17419a(obj), d7Var.mo17419a(obj2)));
    }

    /* renamed from: a */
    static <UT, UB> UB m6881a(int i, List<Integer> list, C2661p4 p4Var, UB ub, C2469d7<UT, UB> d7Var) {
        UB ub2;
        if (p4Var == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (!p4Var.mo17363a(intValue)) {
                        ub = m6880a(i, intValue, ub2, d7Var);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (p4Var.mo17363a(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = m6880a(i, intValue2, ub2, d7Var);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    /* renamed from: a */
    static <UT, UB> UB m6880a(int i, int i2, UB ub, C2469d7<UT, UB> d7Var) {
        if (ub == null) {
            ub = d7Var.mo17418a();
        }
        d7Var.mo17420a(ub, i, (long) i2);
        return ub;
    }
}
