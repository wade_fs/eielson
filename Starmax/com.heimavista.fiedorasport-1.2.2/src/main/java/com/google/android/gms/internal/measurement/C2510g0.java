package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.g0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2510g0 extends C2595l4<C2510g0, C2512b> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2510g0 zzh;
    private static volatile C2501f6<C2510g0> zzi;
    private int zzc;
    private int zzd;
    private String zze = "";
    private boolean zzf;
    private C2738u4<String> zzg = C2595l4.m6649m();

    /* renamed from: com.google.android.gms.internal.measurement.g0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public enum C2511a implements C2631n4 {
        UNKNOWN_MATCH_TYPE(0),
        REGEXP(1),
        BEGINS_WITH(2),
        ENDS_WITH(3),
        PARTIAL(4),
        EXACT(5),
        IN_LIST(6);
        

        /* renamed from: P */
        private final int f4170P;

        static {
            new C2577k0();
        }

        private C2511a(int i) {
            this.f4170P = i;
        }

        /* renamed from: a */
        public static C2511a m6366a(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_MATCH_TYPE;
                case 1:
                    return REGEXP;
                case 2:
                    return BEGINS_WITH;
                case 3:
                    return ENDS_WITH;
                case 4:
                    return PARTIAL;
                case 5:
                    return EXACT;
                case 6:
                    return IN_LIST;
                default:
                    return null;
            }
        }

        /* renamed from: e */
        public static C2661p4 m6367e() {
            return C2559j0.f4251a;
        }

        public final String toString() {
            return "<" + C2511a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.f4170P + " name=" + name() + '>';
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.g0$b */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2512b extends C2595l4.C2596a<C2510g0, C2512b> implements C2769w5 {
        private C2512b() {
            super(C2510g0.zzh);
        }

        /* synthetic */ C2512b(C2413a0 a0Var) {
            this();
        }
    }

    static {
        C2510g0 g0Var = new C2510g0();
        zzh = g0Var;
        C2595l4.m6645a(C2510g0.class, super);
    }

    private C2510g0() {
    }

    /* renamed from: w */
    public static C2510g0 m6355w() {
        return zzh;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2510g0();
            case 2:
                return new C2512b(null);
            case 3:
                return C2595l4.m6643a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\f\u0000\u0002\b\u0001\u0003\u0007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", C2511a.m6367e(), "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                C2501f6<C2510g0> f6Var = zzi;
                if (f6Var == null) {
                    synchronized (C2510g0.class) {
                        f6Var = zzi;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzh);
                            zzi = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* renamed from: n */
    public final boolean mo17486n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final C2511a mo17487o() {
        C2511a a = C2511a.m6366a(this.zzd);
        return a == null ? C2511a.UNKNOWN_MATCH_TYPE : a;
    }

    /* renamed from: p */
    public final boolean mo17488p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final String mo17489q() {
        return this.zze;
    }

    /* renamed from: s */
    public final boolean mo17490s() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: t */
    public final boolean mo17491t() {
        return this.zzf;
    }

    /* renamed from: u */
    public final List<String> mo17492u() {
        return this.zzg;
    }

    /* renamed from: v */
    public final int mo17493v() {
        return this.zzg.size();
    }
}
