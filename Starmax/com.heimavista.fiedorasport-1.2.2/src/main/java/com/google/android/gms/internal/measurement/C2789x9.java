package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.x9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2789x9 implements C2593l2<C2773w9> {

    /* renamed from: Q */
    private static C2789x9 f4580Q = new C2789x9();

    /* renamed from: P */
    private final C2593l2<C2773w9> f4581P;

    private C2789x9(C2593l2<C2773w9> l2Var) {
        this.f4581P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7775b() {
        return ((C2773w9) f4580Q.mo17285a()).mo18130a();
    }

    /* renamed from: c */
    public static boolean m7776c() {
        return ((C2773w9) f4580Q.mo17285a()).mo18131e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4581P.mo17285a();
    }

    public C2789x9() {
        this(C2579k2.m6601a(new C2820z9()));
    }
}
