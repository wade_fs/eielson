package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2033e;

/* renamed from: com.google.android.gms.common.api.internal.j1 */
public final class C2093j1 {

    /* renamed from: a */
    public final C2116p0 f3348a;

    /* renamed from: b */
    public final int f3349b;

    /* renamed from: c */
    public final C2033e<?> f3350c;

    public C2093j1(C2116p0 p0Var, int i, C2033e<?> eVar) {
        this.f3348a = p0Var;
        this.f3349b = i;
        this.f3350c = eVar;
    }
}
