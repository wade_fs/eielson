package com.google.android.gms.location;

import android.content.Context;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2033e;
import com.google.android.gms.common.api.internal.C2047a;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.api.internal.C2091j;
import com.google.android.gms.common.api.internal.C2111o;
import com.google.android.gms.internal.location.C2389d;
import com.google.android.gms.internal.location.C2390e;
import com.google.android.gms.internal.location.C2408w;
import com.google.android.gms.internal.location.zzad;
import com.google.android.gms.internal.location.zzbd;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.location.b */
public class C2826b extends C2033e<Object> {

    /* renamed from: com.google.android.gms.location.b$a */
    private static class C2827a extends C2390e {

        /* renamed from: a */
        private final C4066i<Void> f4687a;

        public C2827a(C4066i<Void> iVar) {
            this.f4687a = iVar;
        }

        /* renamed from: a */
        public final void mo17202a(zzad zzad) {
            C2111o.m5040a(zzad.mo16419b(), this.f4687a);
        }
    }

    public C2826b(@NonNull Context context) {
        super(context, C2837g.f4694c, (C2016a.C2020d) null, new C2047a());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final C2389d m7968a(C4066i<Boolean> iVar) {
        return new C2832d0(this, iVar);
    }

    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"})
    /* renamed from: a */
    public C4065h<Void> mo18252a(LocationRequest locationRequest, C2833e eVar, @Nullable Looper looper) {
        zzbd a = zzbd.m5950a(locationRequest);
        C2084i a2 = C2091j.m4935a(eVar, C2408w.m5937a(looper), C2833e.class.getSimpleName());
        return mo16552a(new C2828b0(this, a2, a, a2), new C2830c0(this, a2.mo16721b()));
    }

    /* renamed from: a */
    public C4065h<Void> mo18253a(C2833e eVar) {
        return C2111o.m5039a(mo16551a(C2091j.m4934a(eVar, C2833e.class.getSimpleName())));
    }
}
