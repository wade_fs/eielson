package com.google.android.gms.measurement.internal;

import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.m5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3117m5 implements C3107l7 {

    /* renamed from: a */
    private final C3081j5 f5343a;

    C3117m5(C3081j5 j5Var) {
        this.f5343a = j5Var;
    }

    /* renamed from: a */
    public final void mo19137a(String str, int i, Throwable th, byte[] bArr, Map map) {
        this.f5343a.mo19086a(str, i, th, bArr, map);
    }
}
