package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2683qb;

/* renamed from: com.google.android.gms.measurement.internal.t0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3192t0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5646a = new C3192t0();

    private C3192t0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2683qb.m7093b());
    }
}
