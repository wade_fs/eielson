package com.google.android.gms.auth.api;

public final class R$string {
    public static final int common_google_play_services_enable_button = 2131820675;
    public static final int common_google_play_services_enable_text = 2131820676;
    public static final int common_google_play_services_enable_title = 2131820677;
    public static final int common_google_play_services_install_button = 2131820678;
    public static final int common_google_play_services_install_text = 2131820679;
    public static final int common_google_play_services_install_title = 2131820680;
    public static final int common_google_play_services_notification_channel_name = 2131820681;
    public static final int common_google_play_services_notification_ticker = 2131820682;
    public static final int common_google_play_services_unknown_issue = 2131820683;
    public static final int common_google_play_services_unsupported_text = 2131820684;
    public static final int common_google_play_services_update_button = 2131820685;
    public static final int common_google_play_services_update_text = 2131820686;
    public static final int common_google_play_services_update_title = 2131820687;
    public static final int common_google_play_services_updating_text = 2131820688;
    public static final int common_google_play_services_wear_update_text = 2131820689;
    public static final int common_open_on_phone = 2131820690;
    public static final int common_signin_button_text = 2131820691;
    public static final int common_signin_button_text_long = 2131820692;

    private R$string() {
    }
}
