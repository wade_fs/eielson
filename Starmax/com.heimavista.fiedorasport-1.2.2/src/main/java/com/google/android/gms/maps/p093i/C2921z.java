package com.google.android.gms.maps.p093i;

import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;

/* renamed from: com.google.android.gms.maps.i.z */
public final class C2921z extends C4032a implements C2903h {
    C2921z(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    /* renamed from: k */
    public final void mo18481k(boolean z) {
        Parcel a = mo23664a();
        C4039h.m12045a(a, z);
        mo23667b(3, a);
    }

    /* renamed from: l */
    public final void mo18482l(boolean z) {
        Parcel a = mo23664a();
        C4039h.m12045a(a, z);
        mo23667b(2, a);
    }

    /* renamed from: m */
    public final void mo18483m(boolean z) {
        Parcel a = mo23664a();
        C4039h.m12045a(a, z);
        mo23667b(1, a);
    }
}
