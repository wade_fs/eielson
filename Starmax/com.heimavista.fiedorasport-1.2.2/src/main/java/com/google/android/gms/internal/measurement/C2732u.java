package com.google.android.gms.internal.measurement;

import android.app.Activity;
import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.u */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2732u extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Activity f4508T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd.C2527b f4509U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2732u(C2525gd.C2527b bVar, Activity activity) {
        super(C2525gd.this);
        this.f4509U = bVar;
        this.f4508T = activity;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        C2525gd.this.f4192g.onActivityResumed(C3992d.m11990a(this.f4508T), super.f4194Q);
    }
}
