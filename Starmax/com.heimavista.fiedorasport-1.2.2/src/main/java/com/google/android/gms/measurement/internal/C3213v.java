package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2489ea;

/* renamed from: com.google.android.gms.measurement.internal.v */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3213v implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5690a = new C3213v();

    private C3213v() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2489ea.m6263c());
    }
}
