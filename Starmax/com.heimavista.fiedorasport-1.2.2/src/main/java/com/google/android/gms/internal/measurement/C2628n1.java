package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.database.ContentObserver;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.core.content.PermissionChecker;

/* renamed from: com.google.android.gms.internal.measurement.n1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2628n1 implements C2613m1 {
    @GuardedBy("GservicesLoader.class")

    /* renamed from: c */
    private static C2628n1 f4342c;

    /* renamed from: a */
    private final Context f4343a;

    /* renamed from: b */
    private final ContentObserver f4344b;

    private C2628n1(Context context) {
        this.f4343a = context;
        this.f4344b = new C2658p1(this, null);
        context.getContentResolver().registerContentObserver(C2463d1.f4034a, true, this.f4344b);
    }

    /* renamed from: a */
    static C2628n1 m6863a(Context context) {
        C2628n1 n1Var;
        synchronized (C2628n1.class) {
            if (f4342c == null) {
                f4342c = PermissionChecker.checkSelfPermission(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new C2628n1(context) : new C2628n1();
            }
            n1Var = f4342c;
        }
        return n1Var;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final String mo17308a(String str) {
        if (this.f4343a == null) {
            return null;
        }
        try {
            return (String) C2592l1.m6637a(new C2673q1(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final /* synthetic */ String mo17774b(String str) {
        return C2463d1.m6190a(this.f4343a.getContentResolver(), str, (String) null);
    }

    private C2628n1() {
        this.f4343a = null;
        this.f4344b = null;
    }

    /* renamed from: a */
    static synchronized void m6864a() {
        synchronized (C2628n1.class) {
            if (!(f4342c == null || f4342c.f4343a == null || f4342c.f4344b == null)) {
                f4342c.f4343a.getContentResolver().unregisterContentObserver(f4342c.f4344b);
            }
            f4342c = null;
        }
    }
}
