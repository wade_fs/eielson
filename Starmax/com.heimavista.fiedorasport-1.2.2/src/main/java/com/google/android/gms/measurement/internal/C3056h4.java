package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.h4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3056h4 {

    /* renamed from: a */
    private final int f5173a;

    /* renamed from: b */
    private final boolean f5174b;

    /* renamed from: c */
    private final boolean f5175c;

    /* renamed from: d */
    private final /* synthetic */ C3032f4 f5176d;

    C3056h4(C3032f4 f4Var, int i, boolean z, boolean z2) {
        this.f5176d = f4Var;
        this.f5173a = i;
        this.f5174b = z;
        this.f5175c = z2;
    }

    /* renamed from: a */
    public final void mo19042a(String str) {
        this.f5176d.mo18999a(this.f5173a, this.f5174b, this.f5175c, str, null, null, null);
    }

    /* renamed from: a */
    public final void mo19043a(String str, Object obj) {
        this.f5176d.mo18999a(this.f5173a, this.f5174b, this.f5175c, str, obj, null, null);
    }

    /* renamed from: a */
    public final void mo19044a(String str, Object obj, Object obj2) {
        this.f5176d.mo18999a(this.f5173a, this.f5174b, this.f5175c, str, obj, obj2, null);
    }

    /* renamed from: a */
    public final void mo19045a(String str, Object obj, Object obj2, Object obj3) {
        this.f5176d.mo18999a(this.f5173a, this.f5174b, this.f5175c, str, obj, obj2, obj3);
    }
}
