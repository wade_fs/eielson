package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.auth.api.accounttransfer.b */
public final class C1955b implements Parcelable.Creator<zzo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        ArrayList<String> arrayList = null;
        ArrayList<String> arrayList2 = null;
        ArrayList<String> arrayList3 = null;
        ArrayList<String> arrayList4 = null;
        ArrayList<String> arrayList5 = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel, a);
                    break;
                case 2:
                    arrayList = C2248a.m5575p(parcel, a);
                    break;
                case 3:
                    arrayList2 = C2248a.m5575p(parcel, a);
                    break;
                case 4:
                    arrayList3 = C2248a.m5575p(parcel, a);
                    break;
                case 5:
                    arrayList4 = C2248a.m5575p(parcel, a);
                    break;
                case 6:
                    arrayList5 = C2248a.m5575p(parcel, a);
                    break;
                default:
                    C2248a.m5550F(parcel, a);
                    break;
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzo(i, arrayList, arrayList2, arrayList3, arrayList4, arrayList5);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzo[i];
    }
}
