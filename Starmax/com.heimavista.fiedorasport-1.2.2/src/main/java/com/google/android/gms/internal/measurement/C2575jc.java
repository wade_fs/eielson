package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.jc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2575jc implements C2524gc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4269a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4270b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4271c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4269a = c2Var.mo17367a("measurement.service.sessions.remove_disabled_session_number", true);
        f4270b = c2Var.mo17367a("measurement.service.sessions.session_number_enabled", true);
        f4271c = c2Var.mo17367a("measurement.service.sessions.session_number_backfill_enabled", true);
    }

    /* renamed from: a */
    public final boolean mo17505a() {
        return f4269a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo17506e() {
        return f4270b.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17507f() {
        return f4271c.mo18128b().booleanValue();
    }
}
