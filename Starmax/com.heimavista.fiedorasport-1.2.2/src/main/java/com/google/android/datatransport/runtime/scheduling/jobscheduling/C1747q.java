package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.q */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C1747q {

    /* renamed from: a */
    private final Executor f2771a;

    /* renamed from: b */
    private final C3934c f2772b;

    /* renamed from: c */
    private final C1749s f2773c;

    /* renamed from: d */
    private final C3969b f2774d;

    C1747q(Executor executor, C3934c cVar, C1749s sVar, C3969b bVar) {
        this.f2771a = executor;
        this.f2772b = cVar;
        this.f2773c = sVar;
        this.f2774d = bVar;
    }

    /* renamed from: a */
    public void mo13592a() {
        this.f2771a.execute(C1745o.m4361a(this));
    }

    /* renamed from: a */
    static /* synthetic */ Object m4364a(C1747q qVar) {
        for (C3905l lVar : qVar.f2772b.mo23594s()) {
            qVar.f2773c.mo13561a(lVar, 1);
        }
        return null;
    }
}
