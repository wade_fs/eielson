package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class zzaj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaj> CREATOR = new C2856q();

    /* renamed from: P */
    private final int f4702P;

    /* renamed from: Q */
    private final int f4703Q;

    /* renamed from: R */
    private final long f4704R;

    /* renamed from: S */
    private final long f4705S;

    zzaj(int i, int i2, long j, long j2) {
        this.f4702P = i;
        this.f4703Q = i2;
        this.f4704R = j;
        this.f4705S = j2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && zzaj.class == obj.getClass()) {
            zzaj zzaj = (zzaj) obj;
            return this.f4702P == zzaj.f4702P && this.f4703Q == zzaj.f4703Q && this.f4704R == zzaj.f4704R && this.f4705S == zzaj.f4705S;
        }
    }

    public final int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4703Q), Integer.valueOf(this.f4702P), Long.valueOf(this.f4705S), Long.valueOf(this.f4704R));
    }

    public final String toString() {
        return "NetworkLocationStatus:" + " Wifi status: " + this.f4702P + " Cell status: " + this.f4703Q + " elapsed time NS: " + this.f4705S + " system time ms: " + this.f4704R;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f4702P);
        C2250b.m5591a(parcel, 2, this.f4703Q);
        C2250b.m5592a(parcel, 3, this.f4704R);
        C2250b.m5592a(parcel, 4, this.f4705S);
        C2250b.m5587a(parcel, a);
    }
}
