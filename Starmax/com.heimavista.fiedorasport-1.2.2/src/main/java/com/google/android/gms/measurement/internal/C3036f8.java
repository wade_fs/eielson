package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

/* renamed from: com.google.android.gms.measurement.internal.f8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3036f8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f5137P;

    /* renamed from: Q */
    private final /* synthetic */ C3232w7 f5138Q;

    C3036f8(C3232w7 w7Var, zzm zzm) {
        this.f5138Q = w7Var;
        this.f5137P = zzm;
    }

    public final void run() {
        C3228w3 d = this.f5138Q.f5724d;
        if (d == null) {
            this.f5138Q.mo19015l().mo19001t().mo19042a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            d.mo19197b(this.f5137P);
            this.f5138Q.m9314J();
        } catch (RemoteException e) {
            this.f5138Q.mo19015l().mo19001t().mo19043a("Failed to send measurementEnabled to the service", e);
        }
    }
}
