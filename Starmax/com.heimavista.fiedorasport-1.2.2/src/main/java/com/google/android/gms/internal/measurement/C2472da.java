package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.da */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2472da implements C2593l2<C2456ca> {

    /* renamed from: Q */
    private static C2472da f4052Q = new C2472da();

    /* renamed from: P */
    private final C2593l2<C2456ca> f4053P;

    private C2472da(C2593l2<C2456ca> l2Var) {
        this.f4053P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6221b() {
        return ((C2456ca) f4052Q.mo17285a()).mo17384a();
    }

    /* renamed from: c */
    public static boolean m6222c() {
        return ((C2456ca) f4052Q.mo17285a()).mo17385e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4053P.mo17285a();
    }

    public C2472da() {
        this(C2579k2.m6601a(new C2505fa()));
    }
}
