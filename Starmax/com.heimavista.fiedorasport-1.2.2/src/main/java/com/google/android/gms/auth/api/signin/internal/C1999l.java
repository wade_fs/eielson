package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.l */
final class C1999l extends C1991d {

    /* renamed from: a */
    private final /* synthetic */ C1998k f3150a;

    C1999l(C1998k kVar) {
        this.f3150a = kVar;
    }

    /* renamed from: b */
    public final void mo16446b(Status status) {
        this.f3150a.mo16593a((C2157k) status);
    }
}
