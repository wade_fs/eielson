package com.google.android.gms.measurement;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.MainThread;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.google.android.gms.measurement.internal.C3211u8;
import com.google.android.gms.measurement.internal.C3255y8;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class AppMeasurementService extends Service implements C3255y8 {

    /* renamed from: P */
    private C3211u8<AppMeasurementService> f4929P;

    /* renamed from: a */
    private final C3211u8<AppMeasurementService> m8373a() {
        if (this.f4929P == null) {
            this.f4929P = new C3211u8<>(this);
        }
        return this.f4929P;
    }

    @MainThread
    public final IBinder onBind(Intent intent) {
        return m8373a().mo19338a(intent);
    }

    @MainThread
    public final void onCreate() {
        super.onCreate();
        m8373a().mo19339a();
    }

    @MainThread
    public final void onDestroy() {
        m8373a().mo19343b();
        super.onDestroy();
    }

    @MainThread
    public final void onRebind(Intent intent) {
        m8373a().mo19345c(intent);
    }

    @MainThread
    public final int onStartCommand(Intent intent, int i, int i2) {
        return m8373a().mo19337a(intent, i, i2);
    }

    @MainThread
    public final boolean onUnbind(Intent intent) {
        return m8373a().mo19344b(intent);
    }

    /* renamed from: a */
    public final boolean mo18706a(int i) {
        return stopSelfResult(i);
    }

    /* renamed from: a */
    public final void mo18704a(JobParameters jobParameters, boolean z) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public final void mo18705a(Intent intent) {
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }
}
