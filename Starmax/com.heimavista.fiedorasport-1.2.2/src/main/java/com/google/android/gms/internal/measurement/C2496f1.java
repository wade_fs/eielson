package com.google.android.gms.internal.measurement;

import android.database.ContentObserver;
import android.os.Handler;

/* renamed from: com.google.android.gms.internal.measurement.f1 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2496f1 extends ContentObserver {
    C2496f1(Handler handler) {
        super(null);
    }

    public final void onChange(boolean z) {
        C2463d1.f4038e.set(true);
    }
}
