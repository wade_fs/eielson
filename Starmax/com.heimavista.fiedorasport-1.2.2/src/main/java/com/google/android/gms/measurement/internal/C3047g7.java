package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.g7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3047g7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5160P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5161Q;

    C3047g7(C3154p6 p6Var, boolean z) {
        this.f5161Q = p6Var;
        this.f5160P = z;
    }

    public final void run() {
        boolean c = this.f5161Q.f5134a.mo19089c();
        boolean b = this.f5161Q.f5134a.mo19088b();
        this.f5161Q.f5134a.mo19087a(this.f5160P);
        if (b == this.f5160P) {
            this.f5161Q.f5134a.mo19015l().mo18996B().mo19043a("Default data collection state already set to", Boolean.valueOf(this.f5160P));
        }
        if (this.f5161Q.f5134a.mo19089c() == c || this.f5161Q.f5134a.mo19089c() != this.f5161Q.f5134a.mo19088b()) {
            this.f5161Q.f5134a.mo19015l().mo19006y().mo19044a("Default data collection is different than actual status", Boolean.valueOf(this.f5160P), Boolean.valueOf(c));
        }
        this.f5161Q.m9115M();
    }
}
