package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.l */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1742l implements C3969b.C3970a {

    /* renamed from: a */
    private final C1743m f2752a;

    /* renamed from: b */
    private final C3905l f2753b;

    /* renamed from: c */
    private final int f2754c;

    private C1742l(C1743m mVar, C3905l lVar, int i) {
        this.f2752a = mVar;
        this.f2753b = lVar;
        this.f2754c = i;
    }

    /* renamed from: a */
    public static C3969b.C3970a m4351a(C1743m mVar, C3905l lVar, int i) {
        return new C1742l(mVar, lVar, i);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return this.f2752a.f2758d.mo13561a(this.f2753b, this.f2754c + 1);
    }
}
