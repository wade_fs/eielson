package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.C2042h;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2107n;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p087h.C2178a;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: com.google.android.gms.auth.api.signin.internal.e */
public final class C1992e implements Runnable {

    /* renamed from: R */
    private static final C2178a f3144R = new C2178a("RevokeAccessOperation", new String[0]);

    /* renamed from: P */
    private final String f3145P;

    /* renamed from: Q */
    private final C2107n f3146Q = new C2107n(null);

    private C1992e(String str) {
        C2258v.m5639b(str);
        this.f3145P = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.h.a(com.google.android.gms.common.api.k, com.google.android.gms.common.api.f):com.google.android.gms.common.api.g<R>
     arg types: [com.google.android.gms.common.api.Status, ?[OBJECT, ARRAY]]
     candidates:
      com.google.android.gms.common.api.h.a(com.google.android.gms.common.api.Status, com.google.android.gms.common.api.f):com.google.android.gms.common.api.g<com.google.android.gms.common.api.Status>
      com.google.android.gms.common.api.h.a(com.google.android.gms.common.api.k, com.google.android.gms.common.api.f):com.google.android.gms.common.api.g<R> */
    /* renamed from: a */
    public static C2040g<Status> m4582a(String str) {
        if (str == null) {
            return C2042h.m4722a((C2157k) new Status(4), (C2036f) null);
        }
        C1992e eVar = new C1992e(str);
        new Thread(eVar).start();
        return eVar.f3146Q;
    }

    public final void run() {
        Status status = Status.f3179V;
        try {
            String valueOf = String.valueOf(this.f3145P);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(valueOf.length() != 0 ? "https://accounts.google.com/o/oauth2/revoke?token=".concat(valueOf) : new String("https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.f3177T;
            } else {
                f3144R.mo16858b("Unable to revoke access!", new Object[0]);
            }
            C2178a aVar = f3144R;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Response Code: ");
            sb.append(responseCode);
            aVar.mo16856a(sb.toString(), new Object[0]);
        } catch (IOException e) {
            C2178a aVar2 = f3144R;
            String valueOf2 = String.valueOf(e.toString());
            aVar2.mo16858b(valueOf2.length() != 0 ? "IOException when revoking access: ".concat(valueOf2) : new String("IOException when revoking access: "), new Object[0]);
        } catch (Exception e2) {
            C2178a aVar3 = f3144R;
            String valueOf3 = String.valueOf(e2.toString());
            aVar3.mo16858b(valueOf3.length() != 0 ? "Exception when revoking access: ".concat(valueOf3) : new String("Exception when revoking access: "), new Object[0]);
        }
        this.f3146Q.mo16593a((C2157k) status);
    }
}
