package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.a9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2977a9 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f4956P;

    /* renamed from: Q */
    private final /* synthetic */ C3244x8 f4957Q;

    C2977a9(C3244x8 x8Var, long j) {
        this.f4957Q = x8Var;
        this.f4956P = j;
    }

    public final void run() {
        this.f4957Q.m9356a(this.f4956P);
    }
}
