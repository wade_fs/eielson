package com.google.android.datatransport.runtime.scheduling.jobscheduling;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1726b implements Runnable {

    /* renamed from: P */
    private static final C1726b f2720P = new C1726b();

    private C1726b() {
    }

    /* renamed from: a */
    public static Runnable m4310a() {
        return f2720P;
    }

    public void run() {
        AlarmManagerSchedulerBroadcastReceiver.m4306a();
    }
}
