package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.f */
/* compiled from: lambda */
public final /* synthetic */ class C1792f implements EventDispatcher.Event {

    /* renamed from: a */
    public static final /* synthetic */ C1792f f2808a = new C1792f();

    private /* synthetic */ C1792f() {
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmSessionAcquired();
    }
}
