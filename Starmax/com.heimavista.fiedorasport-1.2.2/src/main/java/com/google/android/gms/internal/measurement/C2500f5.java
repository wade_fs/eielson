package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.f5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2500f5 extends C2467d5 {

    /* renamed from: c */
    private static final Class<?> f4151c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private C2500f5() {
        super();
    }

    /* renamed from: b */
    private static <E> List<E> m6319b(Object obj, long j) {
        return (List) C2566j7.m6544f(obj, j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17416a(Object obj, long j) {
        Object obj2;
        List list = (List) C2566j7.m6544f(obj, j);
        if (list instanceof C2484e5) {
            obj2 = ((C2484e5) list).mo17332t();
        } else if (!f4151c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof C2485e6) || !(list instanceof C2738u4)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                C2738u4 u4Var = (C2738u4) list;
                if (u4Var.mo17938a()) {
                    u4Var.mo17939e();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        C2566j7.m6522a(obj, j, obj2);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* renamed from: a */
    private static <L> List<L> m6318a(Object obj, long j, int i) {
        List list;
        List<L> list2;
        List<L> b = m6319b(obj, j);
        if (b.isEmpty()) {
            if (b instanceof C2484e5) {
                list2 = new C2435b5(i);
            } else if (!(b instanceof C2485e6) || !(b instanceof C2738u4)) {
                list2 = new ArrayList<>(i);
            } else {
                list2 = ((C2738u4) b).mo17320a(i);
            }
            C2566j7.m6522a(obj, j, list2);
            return list2;
        }
        if (f4151c.isAssignableFrom(b.getClass())) {
            List arrayList = new ArrayList(b.size() + i);
            arrayList.addAll(b);
            C2566j7.m6522a(obj, j, arrayList);
            list = arrayList;
        } else if (b instanceof C2486e7) {
            C2435b5 b5Var = new C2435b5(b.size() + i);
            b5Var.addAll((C2486e7) b);
            C2566j7.m6522a(obj, j, b5Var);
            list = b5Var;
        } else if (!(b instanceof C2485e6) || !(b instanceof C2738u4)) {
            return b;
        } else {
            C2738u4 u4Var = (C2738u4) b;
            if (u4Var.mo17938a()) {
                return b;
            }
            C2738u4 a = u4Var.mo17320a(b.size() + i);
            C2566j7.m6522a(obj, j, a);
            return a;
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final <E> void mo17417a(Object obj, Object obj2, long j) {
        List b = m6319b(obj2, j);
        List a = m6318a(obj, j, b.size());
        int size = a.size();
        int size2 = b.size();
        if (size > 0 && size2 > 0) {
            a.addAll(b);
        }
        if (size > 0) {
            b = a;
        }
        C2566j7.m6522a(obj, j, b);
    }
}
