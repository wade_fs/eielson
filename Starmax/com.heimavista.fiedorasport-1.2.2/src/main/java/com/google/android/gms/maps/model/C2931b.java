package com.google.android.gms.maps.model;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p161c.p165d.C4040i;

/* renamed from: com.google.android.gms.maps.model.b */
public final class C2931b {

    /* renamed from: a */
    private static C4040i f4912a;

    /* renamed from: a */
    public static void m8344a(C4040i iVar) {
        if (f4912a == null) {
            C2258v.m5629a(iVar);
            f4912a = iVar;
        }
    }

    /* renamed from: b */
    private static C4040i m8345b() {
        C4040i iVar = f4912a;
        C2258v.m5630a(iVar, "IBitmapDescriptorFactory is not initialized");
        return iVar;
    }

    /* renamed from: a */
    public static C2929a m8342a() {
        try {
            return new C2929a(m8345b().mo23674i());
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public static C2929a m8343a(Bitmap bitmap) {
        try {
            return new C2929a(m8345b().mo23673a(bitmap));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }
}
