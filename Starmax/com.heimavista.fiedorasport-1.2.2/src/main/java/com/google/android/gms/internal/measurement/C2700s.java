package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.s */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2700s extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Bundle f4471T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4472U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2700s(C2525gd gdVar, Bundle bundle) {
        super(gdVar);
        this.f4472U = gdVar;
        this.f4471T = bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4472U.f4192g.setConditionalUserProperty(this.f4471T, super.f4193P);
    }
}
