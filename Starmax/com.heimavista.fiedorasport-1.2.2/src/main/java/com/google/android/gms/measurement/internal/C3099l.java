package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C2258v;
import java.util.Iterator;

/* renamed from: com.google.android.gms.measurement.internal.l */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3099l {

    /* renamed from: a */
    final String f5312a;

    /* renamed from: b */
    final String f5313b;

    /* renamed from: c */
    private final String f5314c;

    /* renamed from: d */
    final long f5315d;

    /* renamed from: e */
    final long f5316e;

    /* renamed from: f */
    final zzam f5317f;

    private C3099l(C3081j5 j5Var, String str, String str2, String str3, long j, long j2, zzam zzam) {
        C2258v.m5639b(str2);
        C2258v.m5639b(str3);
        C2258v.m5629a(zzam);
        this.f5312a = str2;
        this.f5313b = str3;
        this.f5314c = TextUtils.isEmpty(str) ? null : str;
        this.f5315d = j;
        this.f5316e = j2;
        long j3 = this.f5316e;
        if (j3 != 0 && j3 > this.f5315d) {
            j5Var.mo19015l().mo19004w().mo19044a("Event created with reverse previous/current timestamps. appId, name", C3032f4.m8621a(str2), C3032f4.m8621a(str3));
        }
        this.f5317f = zzam;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final C3099l mo19133a(C3081j5 j5Var, long j) {
        return new C3099l(j5Var, this.f5314c, this.f5312a, this.f5313b, this.f5315d, j, this.f5317f);
    }

    public final String toString() {
        String str = this.f5312a;
        String str2 = this.f5313b;
        String valueOf = String.valueOf(this.f5317f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    C3099l(C3081j5 j5Var, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzam zzam;
        C2258v.m5639b(str2);
        C2258v.m5639b(str3);
        this.f5312a = str2;
        this.f5313b = str3;
        this.f5314c = TextUtils.isEmpty(str) ? null : str;
        this.f5315d = j;
        this.f5316e = j2;
        long j3 = this.f5316e;
        if (j3 != 0 && j3 > this.f5315d) {
            j5Var.mo19015l().mo19004w().mo19043a("Event created with reverse previous/current timestamps. appId", C3032f4.m8621a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            zzam = new zzam(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    j5Var.mo19015l().mo19001t().mo19042a("Param name can't be null");
                    it.remove();
                } else {
                    Object a = j5Var.mo19104w().mo19429a(next, bundle2.get(next));
                    if (a == null) {
                        j5Var.mo19015l().mo19004w().mo19043a("Param value can't be null", j5Var.mo19105x().mo18823b(next));
                        it.remove();
                    } else {
                        j5Var.mo19104w().mo19433a(bundle2, next, a);
                    }
                }
            }
            zzam = new zzam(bundle2);
        }
        this.f5317f = zzam;
    }
}
