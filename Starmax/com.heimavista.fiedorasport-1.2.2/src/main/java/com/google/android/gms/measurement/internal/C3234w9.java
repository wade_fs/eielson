package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.w9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3234w9 {

    /* renamed from: a */
    final String f5733a;

    /* renamed from: b */
    final String f5734b;

    /* renamed from: c */
    final String f5735c;

    /* renamed from: d */
    final long f5736d;

    /* renamed from: e */
    final Object f5737e;

    C3234w9(String str, String str2, String str3, long j, Object obj) {
        C2258v.m5639b(str);
        C2258v.m5639b(str3);
        C2258v.m5629a(obj);
        this.f5733a = str;
        this.f5734b = str2;
        this.f5735c = str3;
        this.f5736d = j;
        this.f5737e = obj;
    }
}
