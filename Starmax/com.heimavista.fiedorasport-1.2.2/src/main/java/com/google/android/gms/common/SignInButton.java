package com.google.android.gms.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.base.R$styleable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2260w;
import com.google.android.gms.common.internal.SignInButtonImpl;
import p119e.p144d.p145a.p157c.p160b.C3994f;

public final class SignInButton extends FrameLayout implements View.OnClickListener {

    /* renamed from: P */
    private int f3166P;

    /* renamed from: Q */
    private int f3167Q;

    /* renamed from: R */
    private View f3168R;

    /* renamed from: S */
    private View.OnClickListener f3169S;

    public SignInButton(Context context) {
        this(context, null);
    }

    /* renamed from: a */
    public final void mo16493a(int i, int i2) {
        this.f3166P = i;
        this.f3167Q = i2;
        Context context = getContext();
        View view = this.f3168R;
        if (view != null) {
            removeView(view);
        }
        try {
            this.f3168R = C2260w.m5646a(context, this.f3166P, this.f3167Q);
        } catch (C3994f.C3995a unused) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            int i3 = this.f3166P;
            int i4 = this.f3167Q;
            SignInButtonImpl signInButtonImpl = new SignInButtonImpl(context);
            signInButtonImpl.mo16899a(context.getResources(), i3, i4);
            this.f3168R = signInButtonImpl;
        }
        addView(this.f3168R);
        this.f3168R.setEnabled(isEnabled());
        this.f3168R.setOnClickListener(this);
    }

    public final void onClick(View view) {
        View.OnClickListener onClickListener = this.f3169S;
        if (onClickListener != null && view == this.f3168R) {
            onClickListener.onClick(this);
        }
    }

    public final void setColorScheme(int i) {
        mo16493a(this.f3166P, i);
    }

    public final void setEnabled(boolean z) {
        super.setEnabled(z);
        this.f3168R.setEnabled(z);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.f3169S = onClickListener;
        View view = this.f3168R;
        if (view != null) {
            view.setOnClickListener(this);
        }
    }

    @Deprecated
    public final void setScopes(Scope[] scopeArr) {
        mo16493a(this.f3166P, this.f3167Q);
    }

    public final void setSize(int i) {
        mo16493a(i, this.f3167Q);
    }

    public SignInButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public SignInButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3169S = null;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R$styleable.SignInButton, 0, 0);
        try {
            this.f3166P = obtainStyledAttributes.getInt(R$styleable.SignInButton_buttonSize, 0);
            this.f3167Q = obtainStyledAttributes.getInt(R$styleable.SignInButton_colorScheme, 2);
            obtainStyledAttributes.recycle();
            mo16493a(this.f3166P, this.f3167Q);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }
}
