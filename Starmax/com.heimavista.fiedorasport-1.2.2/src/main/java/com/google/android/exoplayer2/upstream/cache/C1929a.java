package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.upstream.DataSpec;

/* renamed from: com.google.android.exoplayer2.upstream.cache.a */
/* compiled from: lambda */
public final /* synthetic */ class C1929a implements CacheKeyFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1929a f2940a = new C1929a();

    private /* synthetic */ C1929a() {
    }

    public final String buildCacheKey(DataSpec dataSpec) {
        return CacheUtil.getKey(dataSpec);
    }
}
