package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.C2064e;
import java.util.Collections;

/* renamed from: com.google.android.gms.common.api.internal.e1 */
final class C2069e1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ ConnectionResult f3307P;

    /* renamed from: Q */
    private final /* synthetic */ C2064e.C2067c f3308Q;

    C2069e1(C2064e.C2067c cVar, ConnectionResult connectionResult) {
        this.f3308Q = cVar;
        this.f3307P = connectionResult;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.e.c.a(com.google.android.gms.common.api.internal.e$c, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.e$c, int]
     candidates:
      com.google.android.gms.common.api.internal.e.c.a(com.google.android.gms.common.internal.m, java.util.Set<com.google.android.gms.common.api.Scope>):void
      com.google.android.gms.common.api.internal.q1.a(com.google.android.gms.common.internal.m, java.util.Set<com.google.android.gms.common.api.Scope>):void
      com.google.android.gms.common.api.internal.e.c.a(com.google.android.gms.common.api.internal.e$c, boolean):boolean */
    public final void run() {
        if (this.f3307P.mo16482w()) {
            boolean unused = this.f3308Q.f3304e = true;
            if (this.f3308Q.f3300a.mo16538l()) {
                this.f3308Q.m4861a();
                return;
            }
            try {
                this.f3308Q.f3300a.mo16531a(null, Collections.emptySet());
            } catch (SecurityException e) {
                Log.e("GoogleApiManager", "Failed to get service from broker. ", e);
                ((C2064e.C2065a) C2064e.this.f3280X.get(this.f3308Q.f3301b)).mo16585a(new ConnectionResult(10));
            }
        } else {
            ((C2064e.C2065a) C2064e.this.f3280X.get(this.f3308Q.f3301b)).mo16585a(this.f3307P);
        }
    }
}
