package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.measurement.C2459cd;
import com.google.android.gms.internal.measurement.C2475dd;
import com.google.android.gms.internal.measurement.C2554ia;
import com.google.android.gms.internal.measurement.C2589kc;
import com.google.android.gms.internal.measurement.zzv;
import java.util.Map;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p160b.C3992d;

@DynamiteApi
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
public class AppMeasurementDynamiteService extends C2554ia {

    /* renamed from: a */
    C3081j5 f4930a = null;

    /* renamed from: b */
    private Map<Integer, C3130n6> f4931b = new ArrayMap();

    /* renamed from: com.google.android.gms.measurement.internal.AppMeasurementDynamiteService$a */
    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
    class C2965a implements C3094k6 {

        /* renamed from: a */
        private C2459cd f4932a;

        C2965a(C2459cd cdVar) {
            this.f4932a = cdVar;
        }

        /* renamed from: a */
        public final void mo18720a(String str, String str2, Bundle bundle, long j) {
            try {
                this.f4932a.mo17387a(str, str2, bundle, j);
            } catch (RemoteException e) {
                AppMeasurementDynamiteService.this.f4930a.mo19015l().mo19004w().mo19043a("Event interceptor threw exception", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.AppMeasurementDynamiteService$b */
    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
    class C2966b implements C3130n6 {

        /* renamed from: a */
        private C2459cd f4934a;

        C2966b(C2459cd cdVar) {
            this.f4934a = cdVar;
        }

        /* renamed from: a */
        public final void mo18721a(String str, String str2, Bundle bundle, long j) {
            try {
                this.f4934a.mo17387a(str, str2, bundle, j);
            } catch (RemoteException e) {
                AppMeasurementDynamiteService.this.f4930a.mo19015l().mo19004w().mo19043a("Event listener threw exception", e);
            }
        }
    }

    /* renamed from: a */
    private final void m8377a() {
        if (this.f4930a == null) {
            throw new IllegalStateException("Attempting to perform action before initialize.");
        }
    }

    public void beginAdUnitExposure(String str, long j) {
        m8377a();
        this.f4930a.mo19082I().mo19415a(str, j);
    }

    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        m8377a();
        this.f4930a.mo19103v().mo19281c(str, str2, bundle);
    }

    public void endAdUnitExposure(String str, long j) {
        m8377a();
        this.f4930a.mo19082I().mo19416b(str, j);
    }

    public void generateEventId(C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19104w().mo19435a(kcVar, this.f4930a.mo19104w().mo19453t());
    }

    public void getAppInstanceId(C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19014j().mo19028a(new C3011d7(this, kcVar));
    }

    public void getCachedAppInstanceId(C2589kc kcVar) {
        m8377a();
        m8378a(kcVar, this.f4930a.mo19103v().mo19250H());
    }

    public void getConditionalUserProperties(String str, String str2, C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19014j().mo19028a(new C3012d8(this, kcVar, str, str2));
    }

    public void getCurrentScreenClass(C2589kc kcVar) {
        m8377a();
        m8378a(kcVar, this.f4930a.mo19103v().mo19253K());
    }

    public void getCurrentScreenName(C2589kc kcVar) {
        m8377a();
        m8378a(kcVar, this.f4930a.mo19103v().mo19252J());
    }

    public void getGmpAppId(C2589kc kcVar) {
        m8377a();
        m8378a(kcVar, this.f4930a.mo19103v().mo19254L());
    }

    public void getMaxUserProperties(String str, C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19103v();
        C2258v.m5639b(str);
        this.f4930a.mo19104w().mo19434a(kcVar, 25);
    }

    public void getTestFlag(C2589kc kcVar, int i) {
        m8377a();
        if (i == 0) {
            this.f4930a.mo19104w().mo19437a(kcVar, this.f4930a.mo19103v().mo19246D());
        } else if (i == 1) {
            this.f4930a.mo19104w().mo19435a(kcVar, this.f4930a.mo19103v().mo19247E().longValue());
        } else if (i == 2) {
            C3267z9 w = this.f4930a.mo19104w();
            double doubleValue = this.f4930a.mo19103v().mo19249G().doubleValue();
            Bundle bundle = new Bundle();
            bundle.putDouble("r", doubleValue);
            try {
                kcVar.mo17292d(bundle);
            } catch (RemoteException e) {
                w.f5134a.mo19015l().mo19004w().mo19043a("Error returning double value to wrapper", e);
            }
        } else if (i == 3) {
            this.f4930a.mo19104w().mo19434a(kcVar, this.f4930a.mo19103v().mo19248F().intValue());
        } else if (i == 4) {
            this.f4930a.mo19104w().mo19439a(kcVar, this.f4930a.mo19103v().mo19245C().booleanValue());
        }
    }

    public void getUserProperties(String str, String str2, boolean z, C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19014j().mo19028a(new C3025e9(this, kcVar, str, str2, z));
    }

    public void initForTests(Map map) {
        m8377a();
    }

    public void initialize(C3988b bVar, zzv zzv, long j) {
        Context context = (Context) C3992d.m11991e(bVar);
        C3081j5 j5Var = this.f4930a;
        if (j5Var == null) {
            this.f4930a = C3081j5.m8751a(context, zzv);
        } else {
            j5Var.mo19015l().mo19004w().mo19042a("Attempting to initialize multiple times");
        }
    }

    public void isDataCollectionEnabled(C2589kc kcVar) {
        m8377a();
        this.f4930a.mo19014j().mo19028a(new C2990ba(this, kcVar));
    }

    public void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        m8377a();
        this.f4930a.mo19103v().mo19269a(str, str2, bundle, z, z2, j);
    }

    public void logEventAndBundle(String str, String str2, Bundle bundle, C2589kc kcVar, long j) {
        Bundle bundle2;
        m8377a();
        C2258v.m5639b(str2);
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        bundle2.putString("_o", "app");
        this.f4930a.mo19014j().mo19028a(new C3022e6(this, kcVar, new zzan(str2, new zzam(bundle), "app", j), str));
    }

    public void logHealthData(int i, String str, C3988b bVar, C3988b bVar2, C3988b bVar3) {
        Object obj;
        Object obj2;
        m8377a();
        Object obj3 = null;
        if (bVar == null) {
            obj = null;
        } else {
            obj = C3992d.m11991e(bVar);
        }
        if (bVar2 == null) {
            obj2 = null;
        } else {
            obj2 = C3992d.m11991e(bVar2);
        }
        if (bVar3 != null) {
            obj3 = C3992d.m11991e(bVar3);
        }
        this.f4930a.mo19015l().mo18999a(i, true, false, str, obj, obj2, obj3);
    }

    public void onActivityCreated(C3988b bVar, Bundle bundle, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityCreated((Activity) C3992d.m11991e(bVar), bundle);
        }
    }

    public void onActivityDestroyed(C3988b bVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityDestroyed((Activity) C3992d.m11991e(bVar));
        }
    }

    public void onActivityPaused(C3988b bVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityPaused((Activity) C3992d.m11991e(bVar));
        }
    }

    public void onActivityResumed(C3988b bVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityResumed((Activity) C3992d.m11991e(bVar));
        }
    }

    public void onActivitySaveInstanceState(C3988b bVar, C2589kc kcVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        Bundle bundle = new Bundle();
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivitySaveInstanceState((Activity) C3992d.m11991e(bVar), bundle);
        }
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f4930a.mo19015l().mo19004w().mo19043a("Error returning bundle value to wrapper", e);
        }
    }

    public void onActivityStarted(C3988b bVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityStarted((Activity) C3992d.m11991e(bVar));
        }
    }

    public void onActivityStopped(C3988b bVar, long j) {
        m8377a();
        C3059h7 h7Var = this.f4930a.mo19103v().f5547c;
        if (h7Var != null) {
            this.f4930a.mo19103v().mo19244B();
            h7Var.onActivityStopped((Activity) C3992d.m11991e(bVar));
        }
    }

    public void performAction(Bundle bundle, C2589kc kcVar, long j) {
        m8377a();
        kcVar.mo17292d(null);
    }

    public void registerOnMeasurementEventListener(C2459cd cdVar) {
        m8377a();
        C3130n6 n6Var = this.f4931b.get(Integer.valueOf(cdVar.mo17386a()));
        if (n6Var == null) {
            n6Var = new C2966b(cdVar);
            this.f4931b.put(Integer.valueOf(cdVar.mo17386a()), n6Var);
        }
        this.f4930a.mo19103v().mo19263a(n6Var);
    }

    public void resetAnalyticsData(long j) {
        m8377a();
        this.f4930a.mo19103v().mo19280c(j);
    }

    public void setConditionalUserProperty(Bundle bundle, long j) {
        m8377a();
        if (bundle == null) {
            this.f4930a.mo19015l().mo19001t().mo19042a("Conditional user property must not be null");
        } else {
            this.f4930a.mo19103v().mo19261a(bundle, j);
        }
    }

    public void setCurrentScreen(C3988b bVar, String str, String str2, long j) {
        m8377a();
        this.f4930a.mo19078E().mo19299a((Activity) C3992d.m11991e(bVar), str, str2);
    }

    public void setDataCollectionEnabled(boolean z) {
        m8377a();
        this.f4930a.mo19103v().mo19279b(z);
    }

    public void setEventInterceptor(C2459cd cdVar) {
        m8377a();
        C3154p6 v = this.f4930a.mo19103v();
        C2965a aVar = new C2965a(cdVar);
        v.mo18880a();
        v.mo18816x();
        v.mo19014j().mo19028a(new C3220v6(v, aVar));
    }

    public void setInstanceIdProvider(C2475dd ddVar) {
        m8377a();
    }

    public void setMeasurementEnabled(boolean z, long j) {
        m8377a();
        this.f4930a.mo19103v().mo19274a(z);
    }

    public void setMinimumSessionDuration(long j) {
        m8377a();
        this.f4930a.mo19103v().mo19259a(j);
    }

    public void setSessionTimeoutDuration(long j) {
        m8377a();
        this.f4930a.mo19103v().mo19275b(j);
    }

    public void setUserId(String str, long j) {
        m8377a();
        this.f4930a.mo19103v().mo19272a(null, "_id", str, true, j);
    }

    public void setUserProperty(String str, String str2, C3988b bVar, boolean z, long j) {
        m8377a();
        this.f4930a.mo19103v().mo19272a(str, str2, C3992d.m11991e(bVar), z, j);
    }

    public void unregisterOnMeasurementEventListener(C2459cd cdVar) {
        m8377a();
        C3130n6 remove = this.f4931b.remove(Integer.valueOf(cdVar.mo17386a()));
        if (remove == null) {
            remove = new C2966b(cdVar);
        }
        this.f4930a.mo19103v().mo19277b(remove);
    }

    /* renamed from: a */
    private final void m8378a(C2589kc kcVar, String str) {
        this.f4930a.mo19104w().mo19437a(kcVar, str);
    }
}
