package com.google.android.gms.measurement.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: com.google.android.gms.measurement.internal.h5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3057h5<V> extends FutureTask<V> implements Comparable<C3057h5<V>> {

    /* renamed from: P */
    private final long f5177P = C3045g5.f5150l.getAndIncrement();

    /* renamed from: Q */
    final boolean f5178Q;

    /* renamed from: R */
    private final String f5179R;

    /* renamed from: S */
    private final /* synthetic */ C3045g5 f5180S;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3057h5(C3045g5 g5Var, Callable callable, boolean z, String str) {
        super(callable);
        this.f5180S = g5Var;
        C2258v.m5629a((Object) str);
        this.f5179R = str;
        this.f5178Q = z;
        if (this.f5177P == Long.MAX_VALUE) {
            g5Var.mo19015l().mo19001t().mo19042a("Tasks index overflow");
        }
    }

    public final /* synthetic */ int compareTo(@NonNull Object obj) {
        C3057h5 h5Var = (C3057h5) obj;
        boolean z = this.f5178Q;
        if (z != h5Var.f5178Q) {
            return z ? -1 : 1;
        }
        long j = this.f5177P;
        long j2 = h5Var.f5177P;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.f5180S.mo19015l().mo19002u().mo19043a("Two tasks share the same index. index", Long.valueOf(this.f5177P));
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void setException(Throwable th) {
        this.f5180S.mo19015l().mo19001t().mo19043a(this.f5179R, th);
        super.setException(th);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3057h5(C3045g5 g5Var, Runnable runnable, boolean z, String str) {
        super(runnable, null);
        this.f5180S = g5Var;
        C2258v.m5629a((Object) str);
        this.f5179R = str;
        this.f5178Q = false;
        if (this.f5177P == Long.MAX_VALUE) {
            g5Var.mo19015l().mo19001t().mo19042a("Tasks index overflow");
        }
    }
}
