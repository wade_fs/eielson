package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.measurement.t4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class C2723t4 extends IOException {
    public C2723t4(String str) {
        super(str);
    }

    /* renamed from: a */
    static C2723t4 m7311a() {
        return new C2723t4("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    /* renamed from: e */
    static C2723t4 m7312e() {
        return new C2723t4("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    /* renamed from: f */
    static C2723t4 m7313f() {
        return new C2723t4("Protocol message contained an invalid tag (zero).");
    }

    /* renamed from: g */
    static C2768w4 m7314g() {
        return new C2768w4("Protocol message tag had invalid wire type.");
    }

    /* renamed from: h */
    static C2723t4 m7315h() {
        return new C2723t4("Failed to parse the message.");
    }

    /* renamed from: i */
    static C2723t4 m7316i() {
        return new C2723t4("Protocol message had invalid UTF-8.");
    }
}
