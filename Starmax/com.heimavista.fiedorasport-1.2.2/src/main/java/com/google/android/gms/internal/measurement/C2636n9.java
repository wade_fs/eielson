package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.n9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2636n9 implements C2586k9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4357a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4358b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4359c;

    /* renamed from: d */
    private static final C2765w1<Boolean> f4360d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4357a = c2Var.mo17367a("measurement.service.audience.fix_skip_audience_with_failed_filters", true);
        f4358b = c2Var.mo17367a("measurement.audience.refresh_event_count_filters_timestamp", false);
        f4359c = c2Var.mo17367a("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false);
        f4360d = c2Var.mo17367a("measurement.audience.use_bundle_timestamp_for_event_count_filters", false);
    }

    /* renamed from: a */
    public final boolean mo17653a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17654e() {
        return f4357a.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17655f() {
        return f4358b.mo18128b().booleanValue();
    }

    /* renamed from: g */
    public final boolean mo17656g() {
        return f4359c.mo18128b().booleanValue();
    }

    /* renamed from: t */
    public final boolean mo17657t() {
        return f4360d.mo18128b().booleanValue();
    }
}
