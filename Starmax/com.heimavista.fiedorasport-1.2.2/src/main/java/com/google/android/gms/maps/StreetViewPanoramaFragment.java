package com.google.android.gms.maps;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2890a0;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2899f;
import com.google.android.gms.maps.p093i.C2905j;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p145a.p157c.p160b.C3986a;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3993e;

@TargetApi(11)
public class StreetViewPanoramaFragment extends Fragment {

    /* renamed from: P */
    private final C2871b f4747P = new C2871b(super);

    public void onActivityCreated(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(StreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onActivityCreated(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f4747P.m8058a(activity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4747P.mo23621a(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.f4747P.mo23618a(layoutInflater, viewGroup, bundle);
    }

    public void onDestroy() {
        this.f4747P.mo23623b();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f4747P.mo23625c();
        super.onDestroyView();
    }

    @SuppressLint({"NewApi"})
    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitAll().build());
        try {
            super.onInflate(activity, attributeSet, bundle);
            this.f4747P.m8058a(activity);
            this.f4747P.mo23620a(activity, new Bundle(), bundle);
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public void onLowMemory() {
        this.f4747P.mo23626d();
        super.onLowMemory();
    }

    public void onPause() {
        this.f4747P.mo23627e();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f4747P.mo23628f();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(StreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onSaveInstanceState(bundle);
        this.f4747P.mo23624b(bundle);
    }

    public void onStart() {
        super.onStart();
        this.f4747P.mo23629g();
    }

    public void onStop() {
        this.f4747P.mo23630h();
        super.onStop();
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaFragment$b */
    static class C2871b extends C3986a<C2870a> {

        /* renamed from: e */
        private final Fragment f4750e;

        /* renamed from: f */
        private C3993e<C2870a> f4751f;

        /* renamed from: g */
        private Activity f4752g;

        /* renamed from: h */
        private final List<C2886f> f4753h = new ArrayList();

        C2871b(Fragment fragment) {
            this.f4750e = fragment;
        }

        /* renamed from: i */
        private final void m8060i() {
            if (this.f4752g != null && this.f4751f != null && mo23619a() == null) {
                try {
                    C2884d.m8123a(this.f4752g);
                    this.f4751f.mo23633a(new C2870a(this.f4750e, C2892b0.m8145a(this.f4752g).mo18445c(C3992d.m11990a(this.f4752g))));
                    for (C2886f fVar : this.f4753h) {
                        ((C2870a) mo23619a()).mo18387a(fVar);
                    }
                    this.f4753h.clear();
                } catch (RemoteException e) {
                    throw new C2934e(e);
                } catch (C2170d unused) {
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo18361a(C3993e<C2870a> eVar) {
            this.f4751f = eVar;
            m8060i();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public final void m8058a(Activity activity) {
            this.f4752g = activity;
            m8060i();
        }
    }

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaFragment$a */
    static class C2870a implements C2905j {

        /* renamed from: a */
        private final Fragment f4748a;

        /* renamed from: b */
        private final C2899f f4749b;

        public C2870a(Fragment fragment, C2899f fVar) {
            C2258v.m5629a(fVar);
            this.f4749b = fVar;
            C2258v.m5629a(fragment);
            this.f4748a = fragment;
        }

        /* renamed from: a */
        public final void mo18350a(Activity activity, Bundle bundle, Bundle bundle2) {
            try {
                Bundle bundle3 = new Bundle();
                C2890a0.m8135a(bundle2, bundle3);
                this.f4749b.mo18462a(C3992d.m11990a(activity), (StreetViewPanoramaOptions) null, bundle3);
                C2890a0.m8135a(bundle3, bundle2);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18354b(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                Bundle arguments = this.f4748a.getArguments();
                if (arguments != null && arguments.containsKey("StreetViewPanoramaOptions")) {
                    C2890a0.m8136a(bundle2, "StreetViewPanoramaOptions", arguments.getParcelable("StreetViewPanoramaOptions"));
                }
                this.f4749b.mo18464b(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: c */
        public final void mo18355c() {
            try {
                this.f4749b.mo18465c();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: d */
        public final void mo18356d() {
            try {
                this.f4749b.mo18466d();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: g */
        public final void mo18357g() {
            try {
                this.f4749b.mo18467g();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onLowMemory() {
            try {
                this.f4749b.onLowMemory();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onResume() {
            try {
                this.f4749b.onResume();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onStart() {
            try {
                this.f4749b.onStart();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                C3988b a = this.f4749b.mo18459a(C3992d.m11990a(layoutInflater), C3992d.m11990a(viewGroup), bundle2);
                C2890a0.m8135a(bundle2, bundle);
                return (View) C3992d.m11991e(a);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18353b() {
            try {
                this.f4749b.mo18463b();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18351a(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4749b.mo18460a(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18387a(C2886f fVar) {
            try {
                this.f4749b.mo18461a(new C2925m(this, fVar));
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }
    }
}
