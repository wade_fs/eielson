package com.google.android.gms.common.internal;

import android.content.Context;
import android.util.SparseIntArray;
import androidx.annotation.NonNull;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.api.C2016a;

/* renamed from: com.google.android.gms.common.internal.l */
public class C2229l {

    /* renamed from: a */
    private final SparseIntArray f3735a = new SparseIntArray();

    /* renamed from: b */
    private C2169c f3736b;

    public C2229l(@NonNull C2169c cVar) {
        C2258v.m5629a(cVar);
        this.f3736b = cVar;
    }

    /* renamed from: a */
    public int mo17005a(@NonNull Context context, @NonNull C2016a.C2027f fVar) {
        C2258v.m5629a(context);
        C2258v.m5629a(fVar);
        if (!fVar.mo16536h()) {
            return 0;
        }
        int i = fVar.mo16451i();
        int i2 = this.f3735a.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 < this.f3735a.size()) {
                int keyAt = this.f3735a.keyAt(i3);
                if (keyAt > i && this.f3735a.get(keyAt) == 0) {
                    i2 = 0;
                    break;
                }
                i3++;
            } else {
                break;
            }
        }
        if (i2 == -1) {
            i2 = this.f3736b.mo16820a(context, i);
        }
        this.f3735a.put(i, i2);
        return i2;
    }

    /* renamed from: a */
    public void mo17006a() {
        this.f3735a.clear();
    }
}
