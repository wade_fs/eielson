package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.c6 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2998c6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f5008P;

    /* renamed from: Q */
    private final /* synthetic */ String f5009Q;

    /* renamed from: R */
    private final /* synthetic */ String f5010R;

    /* renamed from: S */
    private final /* synthetic */ long f5011S;

    /* renamed from: T */
    private final /* synthetic */ C3141o5 f5012T;

    C2998c6(C3141o5 o5Var, String str, String str2, String str3, long j) {
        this.f5012T = o5Var;
        this.f5008P = str;
        this.f5009Q = str2;
        this.f5010R = str3;
        this.f5011S = j;
    }

    public final void run() {
        String str = this.f5008P;
        if (str == null) {
            this.f5012T.f5496a.mo19239t().mo19078E().mo19300a(this.f5009Q, (C3188s7) null);
            return;
        }
        this.f5012T.f5496a.mo19239t().mo19078E().mo19300a(this.f5009Q, new C3188s7(this.f5010R, str, this.f5011S));
    }
}
