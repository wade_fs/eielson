package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2727t8;

/* renamed from: com.google.android.gms.measurement.internal.x1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3237x1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5740a = new C3237x1();

    private C3237x1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2727t8.m7321c());
    }
}
