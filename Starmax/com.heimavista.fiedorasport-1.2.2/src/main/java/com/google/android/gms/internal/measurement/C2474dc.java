package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.dc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2474dc implements C2426ac {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4056a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.scheduler.task_thread.cleanup_on_exit", false);

    /* renamed from: a */
    public final boolean mo17291a() {
        return f4056a.mo18128b().booleanValue();
    }
}
