package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;
import p119e.p144d.p145a.p157c.p161c.p163b.C4010b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.common.internal.m */
public interface C2231m extends IInterface {

    /* renamed from: com.google.android.gms.common.internal.m$a */
    public static abstract class C2232a extends C4010b implements C2231m {

        /* renamed from: com.google.android.gms.common.internal.m$a$a */
        public static class C2233a extends C4009a implements C2231m {
            C2233a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            /* renamed from: j */
            public final Account mo17009j() {
                Parcel a = mo23642a(2, mo23641a());
                Account account = (Account) C4011c.m12011a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        /* renamed from: a */
        public static C2231m m5514a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof C2231m) {
                return (C2231m) queryLocalInterface;
            }
            return new C2233a(iBinder);
        }
    }

    /* renamed from: j */
    Account mo17009j();
}
