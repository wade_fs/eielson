package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2804y8;

/* renamed from: com.google.android.gms.measurement.internal.u */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3202u implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5667a = new C3202u();

    private C3202u() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2804y8.m7882c());
    }
}
