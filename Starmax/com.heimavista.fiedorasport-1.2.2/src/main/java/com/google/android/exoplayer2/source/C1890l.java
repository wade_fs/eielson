package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.l */
/* compiled from: lambda */
public final /* synthetic */ class C1890l implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2913P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2914Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSource.MediaPeriodId f2915R;

    public /* synthetic */ C1890l(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f2913P = eventDispatcher;
        this.f2914Q = mediaSourceEventListener;
        this.f2915R = mediaPeriodId;
    }

    public final void run() {
        this.f2913P.mo14935b(this.f2914Q, this.f2915R);
    }
}
