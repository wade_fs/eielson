package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.l8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C2605l8 {
    /* renamed from: A */
    long mo17683A();

    /* renamed from: B */
    String mo17684B();

    /* renamed from: C */
    long mo17685C();

    /* renamed from: D */
    long mo17686D();

    /* renamed from: E */
    long mo17687E();

    /* renamed from: F */
    long mo17688F();

    /* renamed from: G */
    long mo17689G();

    /* renamed from: H */
    long mo17690H();

    /* renamed from: I */
    long mo17691I();

    /* renamed from: J */
    long mo17692J();

    /* renamed from: a */
    long mo17693a();

    /* renamed from: e */
    long mo17694e();

    /* renamed from: f */
    String mo17695f();

    /* renamed from: g */
    String mo17696g();

    /* renamed from: h */
    long mo17697h();

    /* renamed from: i */
    long mo17698i();

    /* renamed from: j */
    long mo17699j();

    /* renamed from: k */
    long mo17700k();

    /* renamed from: l */
    long mo17701l();

    /* renamed from: m */
    long mo17702m();

    /* renamed from: n */
    long mo17703n();

    /* renamed from: o */
    long mo17704o();

    /* renamed from: p */
    long mo17705p();

    /* renamed from: q */
    long mo17706q();

    /* renamed from: r */
    long mo17707r();

    /* renamed from: s */
    long mo17708s();

    /* renamed from: t */
    long mo17709t();

    /* renamed from: u */
    long mo17710u();

    /* renamed from: v */
    long mo17711v();

    /* renamed from: w */
    long mo17712w();

    /* renamed from: x */
    long mo17713x();

    /* renamed from: y */
    long mo17714y();

    /* renamed from: z */
    long mo17715z();
}
