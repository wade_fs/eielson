package com.google.android.gms.maps.p093i;

import android.os.IInterface;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4040i;

/* renamed from: com.google.android.gms.maps.i.c0 */
public interface C2894c0 extends IInterface {
    /* renamed from: a */
    C2895d mo18442a(C3988b bVar, GoogleMapOptions googleMapOptions);

    /* renamed from: a */
    C2901g mo18443a(C3988b bVar, StreetViewPanoramaOptions streetViewPanoramaOptions);

    /* renamed from: a */
    void mo18444a(C3988b bVar, int i);

    /* renamed from: c */
    C2899f mo18445c(C3988b bVar);

    /* renamed from: d */
    C2893c mo18446d(C3988b bVar);

    /* renamed from: h */
    C4040i mo18447h();

    /* renamed from: t */
    C2889a mo18448t();
}
