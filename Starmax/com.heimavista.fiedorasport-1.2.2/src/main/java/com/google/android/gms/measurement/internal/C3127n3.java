package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2607la;

/* renamed from: com.google.android.gms.measurement.internal.n3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3127n3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5364a = new C3127n3();

    private C3127n3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2607la.m6743c());
    }
}
