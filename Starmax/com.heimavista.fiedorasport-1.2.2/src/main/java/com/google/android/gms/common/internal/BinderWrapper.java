package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class BinderWrapper implements Parcelable {
    public static final Parcelable.Creator<BinderWrapper> CREATOR = new C2236n0();

    /* renamed from: P */
    private IBinder f3610P;

    public BinderWrapper() {
        this.f3610P = null;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.f3610P);
    }

    private BinderWrapper(Parcel parcel) {
        this.f3610P = null;
        this.f3610P = parcel.readStrongBinder();
    }

    /* synthetic */ BinderWrapper(Parcel parcel, C2236n0 n0Var) {
        this(parcel);
    }
}
