package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class Status extends AbstractSafeParcelable implements C2157k, ReflectedParcelable {
    public static final Parcelable.Creator<Status> CREATOR = new C2166t();

    /* renamed from: T */
    public static final Status f3177T = new Status(0);

    /* renamed from: U */
    public static final Status f3178U = new Status(14);

    /* renamed from: V */
    public static final Status f3179V = new Status(8);

    /* renamed from: W */
    public static final Status f3180W = new Status(15);

    /* renamed from: X */
    public static final Status f3181X = new Status(16);

    /* renamed from: P */
    private final int f3182P;

    /* renamed from: Q */
    private final int f3183Q;
    @Nullable

    /* renamed from: R */
    private final String f3184R;
    @Nullable

    /* renamed from: S */
    private final PendingIntent f3185S;

    static {
        new Status(17);
        new Status(18);
    }

    Status(int i, int i2, @Nullable String str, @Nullable PendingIntent pendingIntent) {
        this.f3182P = i;
        this.f3183Q = i2;
        this.f3184R = str;
        this.f3185S = pendingIntent;
    }

    /* renamed from: b */
    public final Status mo16419b() {
        return this;
    }

    /* renamed from: c */
    public final int mo16512c() {
        return this.f3183Q;
    }

    @Nullable
    /* renamed from: d */
    public final String mo16513d() {
        return this.f3184R;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        if (this.f3182P != status.f3182P || this.f3183Q != status.f3183Q || !C2251t.m5617a(this.f3184R, status.f3184R) || !C2251t.m5617a(this.f3185S, status.f3185S)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f3182P), Integer.valueOf(this.f3183Q), this.f3184R, this.f3185S);
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("statusCode", mo16519w());
        a.mo17037a("resolution", this.f3185S);
        return a.toString();
    }

    /* renamed from: u */
    public final boolean mo16517u() {
        return this.f3185S != null;
    }

    /* renamed from: v */
    public final boolean mo16518v() {
        return this.f3183Q <= 0;
    }

    /* renamed from: w */
    public final String mo16519w() {
        String str = this.f3184R;
        if (str != null) {
            return str;
        }
        return C2032d.m4674a(this.f3183Q);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, mo16512c());
        C2250b.m5602a(parcel, 2, mo16513d(), false);
        C2250b.m5596a(parcel, 3, (Parcelable) this.f3185S, i, false);
        C2250b.m5591a(parcel, 1000, this.f3182P);
        C2250b.m5587a(parcel, a);
    }

    public Status(int i) {
        this(i, null);
    }

    public Status(int i, @Nullable String str) {
        this(1, i, str, null);
    }

    public Status(int i, @Nullable String str, @Nullable PendingIntent pendingIntent) {
        this(1, i, str, pendingIntent);
    }
}
