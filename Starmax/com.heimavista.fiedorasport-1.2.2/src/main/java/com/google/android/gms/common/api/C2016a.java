package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.C2016a.C2020d;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.common.internal.C2258v;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/* renamed from: com.google.android.gms.common.api.a */
public final class C2016a<O extends C2020d> {

    /* renamed from: a */
    private final C2017a<?, O> f3186a;

    /* renamed from: b */
    private final C2028g<?> f3187b;

    /* renamed from: c */
    private final String f3188c;

    /* renamed from: com.google.android.gms.common.api.a$a */
    public static abstract class C2017a<T extends C2027f, O> extends C2026e<T, O> {
        /* renamed from: a */
        public abstract T mo16371a(Context context, Looper looper, C2211e eVar, O o, C2036f.C2038b bVar, C2036f.C2039c cVar);
    }

    /* renamed from: com.google.android.gms.common.api.a$b */
    public interface C2018b {
    }

    /* renamed from: com.google.android.gms.common.api.a$c */
    public static class C2019c<C extends C2018b> {
    }

    /* renamed from: com.google.android.gms.common.api.a$d */
    public interface C2020d {

        /* renamed from: com.google.android.gms.common.api.a$d$a */
        public interface C2021a extends C2023c, C2024d {
            /* renamed from: j */
            Account mo16525j();
        }

        /* renamed from: com.google.android.gms.common.api.a$d$b */
        public interface C2022b extends C2023c {
            /* renamed from: a */
            GoogleSignInAccount mo16526a();
        }

        /* renamed from: com.google.android.gms.common.api.a$d$c */
        public interface C2023c extends C2020d {
        }

        /* renamed from: com.google.android.gms.common.api.a$d$d */
        public interface C2024d extends C2020d {
        }

        /* renamed from: com.google.android.gms.common.api.a$d$e */
        public interface C2025e extends C2023c, C2024d {
        }
    }

    /* renamed from: com.google.android.gms.common.api.a$e */
    public static abstract class C2026e<T extends C2018b, O> {
        /* renamed from: a */
        public int mo16527a() {
            return Integer.MAX_VALUE;
        }

        /* renamed from: a */
        public List<Scope> mo16372a(O o) {
            return Collections.emptyList();
        }
    }

    /* renamed from: com.google.android.gms.common.api.a$f */
    public interface C2027f extends C2018b {
        /* renamed from: a */
        void mo16528a();

        /* renamed from: a */
        void mo16529a(C2197d.C2200c cVar);

        /* renamed from: a */
        void mo16530a(C2197d.C2202e eVar);

        /* renamed from: a */
        void mo16531a(C2231m mVar, Set<Scope> set);

        /* renamed from: a */
        void mo16532a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        /* renamed from: c */
        boolean mo16533c();

        /* renamed from: d */
        boolean mo16450d();

        /* renamed from: e */
        boolean mo16534e();

        /* renamed from: f */
        String mo16535f();

        /* renamed from: h */
        boolean mo16536h();

        /* renamed from: i */
        int mo16451i();

        /* renamed from: j */
        Feature[] mo16537j();

        /* renamed from: k */
        Intent mo16452k();

        /* renamed from: l */
        boolean mo16538l();

        @Nullable
        /* renamed from: m */
        IBinder mo16539m();
    }

    /* renamed from: com.google.android.gms.common.api.a$g */
    public static final class C2028g<C extends C2027f> extends C2019c<C> {
    }

    /* renamed from: com.google.android.gms.common.api.a$h */
    public interface C2029h<T extends IInterface> extends C2018b {
        /* renamed from: a */
        T mo16540a(IBinder iBinder);

        /* renamed from: a */
        void mo16541a(int i, T t);

        /* renamed from: n */
        String mo16542n();

        /* renamed from: o */
        String mo16543o();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.common.api.a$a<C, O>, java.lang.Object, com.google.android.gms.common.api.a$a<?, O>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.common.api.a$g<C>, java.lang.Object, com.google.android.gms.common.api.a$g<?>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <C extends com.google.android.gms.common.api.C2016a.C2027f> C2016a(java.lang.String r2, com.google.android.gms.common.api.C2016a.C2017a<C, O> r3, com.google.android.gms.common.api.C2016a.C2028g<C> r4) {
        /*
            r1 = this;
            r1.<init>()
            java.lang.String r0 = "Cannot construct an Api with a null ClientBuilder"
            com.google.android.gms.common.internal.C2258v.m5630a(r3, r0)
            java.lang.String r0 = "Cannot construct an Api with a null ClientKey"
            com.google.android.gms.common.internal.C2258v.m5630a(r4, r0)
            r1.f3188c = r2
            r1.f3186a = r3
            r1.f3187b = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.C2016a.<init>(java.lang.String, com.google.android.gms.common.api.a$a, com.google.android.gms.common.api.a$g):void");
    }

    /* renamed from: a */
    public final C2019c<?> mo16521a() {
        C2028g<?> gVar = this.f3187b;
        if (gVar != null) {
            return gVar;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    /* renamed from: b */
    public final String mo16522b() {
        return this.f3188c;
    }

    /* renamed from: c */
    public final C2026e<?, O> mo16523c() {
        return this.f3186a;
    }

    /* renamed from: d */
    public final C2017a<?, O> mo16524d() {
        C2258v.m5641b(this.f3186a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.f3186a;
    }
}
