package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.mp4.a */
/* compiled from: lambda */
public final /* synthetic */ class C1804a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1804a f2818a = new C1804a();

    private /* synthetic */ C1804a() {
    }

    public final Extractor[] createExtractors() {
        return FragmentedMp4Extractor.m4385a();
    }
}
