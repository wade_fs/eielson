package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2254u;

/* renamed from: com.google.android.gms.common.internal.h0 */
final class C2220h0 implements C2254u.C2256b {
    C2220h0() {
    }

    /* renamed from: a */
    public final C2030b mo16978a(Status status) {
        return C2191b.m5353a(status);
    }
}
