package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.a */
/* compiled from: lambda */
public final /* synthetic */ class C1787a implements EventDispatcher.Event {

    /* renamed from: a */
    public static final /* synthetic */ C1787a f2801a = new C1787a();

    private /* synthetic */ C1787a() {
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmSessionReleased();
    }
}
