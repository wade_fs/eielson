package com.google.android.gms.internal.measurement;

import android.net.Uri;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;

/* renamed from: com.google.android.gms.internal.measurement.t1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2718t1 {
    @GuardedBy("PhenotypeConstants.class")

    /* renamed from: a */
    private static final ArrayMap<String, Uri> f4494a = new ArrayMap<>();

    /* renamed from: a */
    public static synchronized Uri m7213a(String str) {
        Uri uri;
        synchronized (C2718t1.class) {
            uri = f4494a.get(str);
            if (uri == null) {
                String valueOf = String.valueOf(Uri.encode(str));
                uri = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                f4494a.put(str, uri);
            }
        }
        return uri;
    }
}
