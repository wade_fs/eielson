package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class zzj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzj> CREATOR = new C2825a0();

    /* renamed from: P */
    private boolean f4709P;

    /* renamed from: Q */
    private long f4710Q;

    /* renamed from: R */
    private float f4711R;

    /* renamed from: S */
    private long f4712S;

    /* renamed from: T */
    private int f4713T;

    public zzj() {
        this(true, 50, 0.0f, Long.MAX_VALUE, Integer.MAX_VALUE);
    }

    zzj(boolean z, long j, float f, long j2, int i) {
        this.f4709P = z;
        this.f4710Q = j;
        this.f4711R = f;
        this.f4712S = j2;
        this.f4713T = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzj)) {
            return false;
        }
        zzj zzj = (zzj) obj;
        return this.f4709P == zzj.f4709P && this.f4710Q == zzj.f4710Q && Float.compare(this.f4711R, zzj.f4711R) == 0 && this.f4712S == zzj.f4712S && this.f4713T == zzj.f4713T;
    }

    public final int hashCode() {
        return C2251t.m5615a(Boolean.valueOf(this.f4709P), Long.valueOf(this.f4710Q), Float.valueOf(this.f4711R), Long.valueOf(this.f4712S), Integer.valueOf(this.f4713T));
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=");
        sb.append(this.f4709P);
        sb.append(" mMinimumSamplingPeriodMs=");
        sb.append(this.f4710Q);
        sb.append(" mSmallestAngleChangeRadians=");
        sb.append(this.f4711R);
        long j = this.f4712S;
        if (j != Long.MAX_VALUE) {
            sb.append(" expireIn=");
            sb.append(j - SystemClock.elapsedRealtime());
            sb.append("ms");
        }
        if (this.f4713T != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.f4713T);
        }
        sb.append(']');
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5605a(parcel, 1, this.f4709P);
        C2250b.m5592a(parcel, 2, this.f4710Q);
        C2250b.m5590a(parcel, 3, this.f4711R);
        C2250b.m5592a(parcel, 4, this.f4712S);
        C2250b.m5591a(parcel, 5, this.f4713T);
        C2250b.m5587a(parcel, a);
    }
}
