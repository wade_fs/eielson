package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.l6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
interface C2603l6<T> {
    /* renamed from: a */
    int mo17276a(T t);

    /* renamed from: a */
    T mo17277a();

    /* renamed from: a */
    void mo17278a(Object obj, C2771w7 w7Var);

    /* renamed from: a */
    void mo17279a(T t, byte[] bArr, int i, int i2, C2417a3 a3Var);

    /* renamed from: a */
    boolean mo17280a(Object obj, Object obj2);

    /* renamed from: b */
    void mo17281b(T t, T t2);

    /* renamed from: b */
    boolean mo17282b(T t);

    /* renamed from: c */
    void mo17283c(T t);

    /* renamed from: d */
    int mo17284d(T t);
}
