package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.job.JobInfo;
import androidx.annotation.RequiresApi;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1728d;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import p119e.p144d.p145a.p146a.C3879d;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.g */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C1733g {

    /* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.g$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static class C1734a {

        /* renamed from: a */
        private C3971a f2734a;

        /* renamed from: b */
        private Map<C3879d, C1735b> f2735b = new HashMap();

        /* renamed from: a */
        public C1734a mo13584a(C3971a aVar) {
            this.f2734a = aVar;
            return this;
        }

        /* renamed from: a */
        public C1734a mo13583a(C3879d dVar, C1735b bVar) {
            this.f2735b.put(dVar, bVar);
            return this;
        }

        /* renamed from: a */
        public C1733g mo13585a() {
            if (this.f2734a == null) {
                throw new NullPointerException("missing required property: clock");
            } else if (this.f2735b.keySet().size() >= C3879d.values().length) {
                Map<C3879d, C1735b> map = this.f2735b;
                this.f2735b = new HashMap();
                return C1733g.m4325a(this.f2734a, map);
            } else {
                throw new IllegalStateException("Not all priorities have been configured");
            }
        }
    }

    /* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.g$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static abstract class C1735b {

        /* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.g$b$a */
        /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
        public static abstract class C1736a {
            /* renamed from: a */
            public abstract C1736a mo13575a(long j);

            /* renamed from: a */
            public abstract C1736a mo13576a(Set<C1737c> set);

            /* renamed from: a */
            public abstract C1735b mo13577a();

            /* renamed from: b */
            public abstract C1736a mo13578b(long j);
        }

        /* renamed from: d */
        public static C1736a m4336d() {
            C1728d.C1730b bVar = new C1728d.C1730b();
            bVar.mo13576a(Collections.emptySet());
            return bVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract long mo13569a();

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public abstract Set<C1737c> mo13570b();

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public abstract long mo13571c();
    }

    /* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.g$c */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public enum C1737c {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    /* renamed from: a */
    public static C1733g m4324a(C3971a aVar) {
        C1734a c = m4328c();
        C3879d dVar = C3879d.f7175P;
        C1735b.C1736a d = C1735b.m4336d();
        d.mo13575a(30000);
        d.mo13578b(86400000);
        c.mo13583a(dVar, d.mo13577a());
        C3879d dVar2 = C3879d.HIGHEST;
        C1735b.C1736a d2 = C1735b.m4336d();
        d2.mo13575a(1000);
        d2.mo13578b(86400000);
        c.mo13583a(dVar2, d2.mo13577a());
        C3879d dVar3 = C3879d.VERY_LOW;
        C1735b.C1736a d3 = C1735b.m4336d();
        d3.mo13575a(86400000);
        d3.mo13578b(86400000);
        d3.mo13576a(m4326a(C1737c.NETWORK_UNMETERED, C1737c.DEVICE_IDLE));
        c.mo13583a(dVar3, d3.mo13577a());
        c.mo13584a(aVar);
        return c.mo13585a();
    }

    /* renamed from: c */
    public static C1734a m4328c() {
        return new C1734a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract C3971a mo13564a();

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract Map<C3879d, C1735b> mo13565b();

    /* renamed from: a */
    static C1733g m4325a(C3971a aVar, Map<C3879d, C1735b> map) {
        return new C1727c(aVar, map);
    }

    /* renamed from: a */
    public long mo13581a(C3879d dVar, long j, int i) {
        long a = j - mo13564a().mo23604a();
        C1735b bVar = mo13565b().get(dVar);
        return Math.min(Math.max(((long) Math.pow(2.0d, (double) (i - 1))) * bVar.mo13569a(), a), bVar.mo13571c());
    }

    @RequiresApi(api = 21)
    /* renamed from: a */
    public JobInfo.Builder mo13582a(JobInfo.Builder builder, C3879d dVar, long j, int i) {
        builder.setMinimumLatency(mo13581a(dVar, j, i));
        m4327a(builder, mo13565b().get(dVar).mo13570b());
        return builder;
    }

    @RequiresApi(api = 21)
    /* renamed from: a */
    private void m4327a(JobInfo.Builder builder, Set<C1737c> set) {
        if (set.contains(C1737c.NETWORK_UNMETERED)) {
            builder.setRequiredNetworkType(2);
        } else {
            builder.setRequiredNetworkType(1);
        }
        if (set.contains(C1737c.DEVICE_CHARGING)) {
            builder.setRequiresCharging(true);
        }
        if (set.contains(C1737c.DEVICE_IDLE)) {
            builder.setRequiresDeviceIdle(true);
        }
    }

    /* renamed from: a */
    private static <T> Set<T> m4326a(T... tArr) {
        return Collections.unmodifiableSet(new HashSet(Arrays.asList(tArr)));
    }
}
