package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2683qb;

/* renamed from: com.google.android.gms.measurement.internal.y0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3247y0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5765a = new C3247y0();

    private C3247y0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Integer.valueOf((int) C2683qb.m7095d());
    }
}
