package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2044i;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.C2158l;
import com.google.android.gms.common.api.C2159m;
import com.google.android.gms.common.api.C2160n;
import com.google.android.gms.common.api.C2161o;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2258v;
import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.common.api.internal.s1 */
public final class C2129s1<R extends C2157k> extends C2161o<R> implements C2158l<R> {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C2160n<? super R, ? extends C2157k> f3438a;

    /* renamed from: b */
    private C2129s1<? extends C2157k> f3439b;

    /* renamed from: c */
    private volatile C2159m<? super R> f3440c;

    /* renamed from: d */
    private final Object f3441d;

    /* renamed from: e */
    private Status f3442e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final WeakReference<C2036f> f3443f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final C2137u1 f3444g;

    /* renamed from: b */
    private final void m5078b(Status status) {
        synchronized (this.f3441d) {
            if (this.f3438a != null) {
                Status a = this.f3438a.mo16812a(status);
                C2258v.m5630a(a, "onFailure must not return null");
                this.f3439b.m5075a(a);
            } else if (m5080b()) {
                this.f3440c.mo16810a(status);
            }
        }
    }

    /* renamed from: a */
    public final void mo16767a(R r) {
        synchronized (this.f3441d) {
            if (!r.mo16419b().mo16518v()) {
                m5075a(r.mo16419b());
                m5079b((C2157k) r);
            } else if (this.f3438a != null) {
                C2105m1.m4993a().submit(new C2133t1(this, r));
            } else if (m5080b()) {
                this.f3440c.mo16811b(r);
            }
        }
    }

    /* renamed from: b */
    private final boolean m5080b() {
        return (this.f3440c == null || this.f3443f.get() == null) ? false : true;
    }

    /* renamed from: a */
    private final void m5075a(Status status) {
        synchronized (this.f3441d) {
            this.f3442e = status;
            m5078b(this.f3442e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m5079b(C2157k kVar) {
        if (kVar instanceof C2044i) {
            try {
                ((C2044i) kVar).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(kVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16775a() {
        this.f3440c = null;
    }
}
