package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class Feature extends AbstractSafeParcelable {
    public static final Parcelable.Creator<Feature> CREATOR = new C2285l();

    /* renamed from: P */
    private final String f3163P;
    @Deprecated

    /* renamed from: Q */
    private final int f3164Q;

    /* renamed from: R */
    private final long f3165R;

    public Feature(String str, int i, long j) {
        this.f3163P = str;
        this.f3164Q = i;
        this.f3165R = j;
    }

    /* renamed from: c */
    public String mo16487c() {
        return this.f3163P;
    }

    /* renamed from: d */
    public long mo16488d() {
        long j = this.f3165R;
        return j == -1 ? (long) this.f3164Q : j;
    }

    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Feature) {
            Feature feature = (Feature) obj;
            if (((mo16487c() == null || !mo16487c().equals(feature.mo16487c())) && (mo16487c() != null || feature.mo16487c() != null)) || mo16488d() != feature.mo16488d()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        return C2251t.m5615a(mo16487c(), Long.valueOf(mo16488d()));
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("name", mo16487c());
        a.mo17037a(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, Long.valueOf(mo16488d()));
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5602a(parcel, 1, mo16487c(), false);
        C2250b.m5591a(parcel, 2, this.f3164Q);
        C2250b.m5592a(parcel, 3, mo16488d());
        C2250b.m5587a(parcel, a);
    }
}
