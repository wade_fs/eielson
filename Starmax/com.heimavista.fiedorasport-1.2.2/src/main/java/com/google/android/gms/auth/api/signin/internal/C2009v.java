package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.auth.api.signin.internal.v */
public final class C2009v implements Parcelable.Creator<SignInConfiguration> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        GoogleSignInOptions googleSignInOptions = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 != 5) {
                C2248a.m5550F(parcel, a);
            } else {
                googleSignInOptions = (GoogleSignInOptions) C2248a.m5553a(parcel, a, GoogleSignInOptions.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new SignInConfiguration(str, googleSignInOptions);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
