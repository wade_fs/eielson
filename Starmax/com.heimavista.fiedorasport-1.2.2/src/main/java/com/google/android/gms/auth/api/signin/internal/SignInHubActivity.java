package com.google.android.gms.auth.api.signin.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.Status;

@KeepName
public class SignInHubActivity extends FragmentActivity {

    /* renamed from: U */
    private static boolean f3131U = false;

    /* renamed from: P */
    private boolean f3132P = false;

    /* renamed from: Q */
    private SignInConfiguration f3133Q;

    /* renamed from: R */
    private boolean f3134R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public int f3135S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public Intent f3136T;

    /* renamed from: com.google.android.gms.auth.api.signin.internal.SignInHubActivity$a */
    private class C1987a implements LoaderManager.LoaderCallbacks<Void> {
        private C1987a() {
        }

        public final Loader<Void> onCreateLoader(int i, Bundle bundle) {
            return new zze(SignInHubActivity.this, C2036f.m4692h());
        }

        public final /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            SignInHubActivity signInHubActivity = SignInHubActivity.this;
            signInHubActivity.setResult(signInHubActivity.f3135S, SignInHubActivity.this.f3136T);
            SignInHubActivity.this.finish();
        }

        public final void onLoaderReset(Loader<Void> loader) {
        }
    }

    /* renamed from: a */
    private final void m4560a() {
        getSupportLoaderManager().initLoader(0, null, new C1987a());
        f3131U = false;
    }

    /* renamed from: d */
    private final void m4562d(int i) {
        Status status = new Status(i);
        Intent intent = new Intent();
        intent.putExtra("googleSignInStatus", status);
        setResult(0, intent);
        finish();
        f3131U = false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.f3132P) {
            setResult(0);
            if (i == 40962) {
                if (intent != null) {
                    SignInAccount signInAccount = (SignInAccount) intent.getParcelableExtra("signInAccount");
                    if (signInAccount != null && signInAccount.mo16412c() != null) {
                        GoogleSignInAccount c = signInAccount.mo16412c();
                        C2001n.m4604a(this).mo16458a(this.f3133Q.mo16428c(), c);
                        intent.removeExtra("signInAccount");
                        intent.putExtra("googleSignInAccount", c);
                        this.f3134R = true;
                        this.f3135S = i2;
                        this.f3136T = intent;
                        m4560a();
                        return;
                    } else if (intent.hasExtra("errorCode")) {
                        int intExtra = intent.getIntExtra("errorCode", 8);
                        if (intExtra == 13) {
                            intExtra = 12501;
                        }
                        m4562d(intExtra);
                        return;
                    }
                }
                m4562d(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String action = intent.getAction();
        if ("com.google.android.gms.auth.NO_IMPL".equals(action)) {
            m4562d(12500);
        } else if (action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN") || action.equals("com.google.android.gms.auth.APPAUTH_SIGN_IN")) {
            this.f3133Q = (SignInConfiguration) intent.getBundleExtra("config").getParcelable("config");
            if (this.f3133Q == null) {
                Log.e("AuthSignInClient", "Activity started with invalid configuration.");
                setResult(0);
                finish();
                return;
            }
            if (!(bundle == null)) {
                this.f3134R = bundle.getBoolean("signingInGoogleApiClients");
                if (this.f3134R) {
                    this.f3135S = bundle.getInt("signInResultCode");
                    this.f3136T = (Intent) bundle.getParcelable("signInResultData");
                    m4560a();
                }
            } else if (f3131U) {
                setResult(0);
                m4562d(12502);
            } else {
                f3131U = true;
                Intent intent2 = new Intent(action);
                if (action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN")) {
                    intent2.setPackage("com.google.android.gms");
                } else {
                    intent2.setPackage(getPackageName());
                }
                intent2.putExtra("config", this.f3133Q);
                try {
                    startActivityForResult(intent2, 40962);
                } catch (ActivityNotFoundException unused) {
                    this.f3132P = true;
                    Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
                    m4562d(17);
                }
            }
        } else {
            String valueOf = String.valueOf(intent.getAction());
            Log.e("AuthSignInClient", valueOf.length() != 0 ? "Unknown action: ".concat(valueOf) : new String("Unknown action: "));
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("signingInGoogleApiClients", this.f3134R);
        if (this.f3134R) {
            bundle.putInt("signInResultCode", this.f3135S);
            bundle.putParcelable("signInResultData", this.f3136T);
        }
    }
}
