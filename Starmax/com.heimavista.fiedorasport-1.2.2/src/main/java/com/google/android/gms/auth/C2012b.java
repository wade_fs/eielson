package com.google.android.gms.auth;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.auth.b */
public final class C2012b implements Parcelable.Creator<AccountChangeEventsRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        String str = null;
        Account account = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 == 3) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 != 4) {
                C2248a.m5550F(parcel, a);
            } else {
                account = (Account) C2248a.m5553a(parcel, a, Account.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new AccountChangeEventsRequest(i, i2, str, account);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AccountChangeEventsRequest[i];
    }
}
