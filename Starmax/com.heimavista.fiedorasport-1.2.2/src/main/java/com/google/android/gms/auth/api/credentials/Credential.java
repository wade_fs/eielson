package com.google.android.gms.auth.api.credentials;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Collections;
import java.util.List;

public class Credential extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<Credential> CREATOR = new C1963b();

    /* renamed from: P */
    private final String f3029P;
    @Nullable

    /* renamed from: Q */
    private final String f3030Q;
    @Nullable

    /* renamed from: R */
    private final Uri f3031R;

    /* renamed from: S */
    private final List<IdToken> f3032S;
    @Nullable

    /* renamed from: T */
    private final String f3033T;
    @Nullable

    /* renamed from: U */
    private final String f3034U;
    @Nullable

    /* renamed from: V */
    private final String f3035V;
    @Nullable

    /* renamed from: W */
    private final String f3036W;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String */
    Credential(String str, String str2, Uri uri, List<IdToken> list, String str3, String str4, String str5, String str6) {
        List<IdToken> list2;
        C2258v.m5630a((Object) str, (Object) "credential identifier cannot be null");
        String trim = str.trim();
        C2258v.m5631a(trim, (Object) "credential identifier cannot be empty");
        if (str3 == null || !TextUtils.isEmpty(str3)) {
            if (str4 != null) {
                boolean z = false;
                if (!TextUtils.isEmpty(str4)) {
                    Uri parse = Uri.parse(str4);
                    if (parse.isAbsolute() && parse.isHierarchical() && !TextUtils.isEmpty(parse.getScheme()) && !TextUtils.isEmpty(parse.getAuthority()) && ("http".equalsIgnoreCase(parse.getScheme()) || "https".equalsIgnoreCase(parse.getScheme()))) {
                        z = true;
                    }
                }
                if (!Boolean.valueOf(z).booleanValue()) {
                    throw new IllegalArgumentException("Account type must be a valid Http/Https URI");
                }
            }
            if (TextUtils.isEmpty(str4) || TextUtils.isEmpty(str3)) {
                if (str2 != null && TextUtils.isEmpty(str2.trim())) {
                    str2 = null;
                }
                this.f3030Q = str2;
                this.f3031R = uri;
                if (list == null) {
                    list2 = Collections.emptyList();
                } else {
                    list2 = Collections.unmodifiableList(list);
                }
                this.f3032S = list2;
                this.f3029P = trim;
                this.f3033T = str3;
                this.f3034U = str4;
                this.f3035V = str5;
                this.f3036W = str6;
                return;
            }
            throw new IllegalArgumentException("Password and AccountType are mutually exclusive");
        }
        throw new IllegalArgumentException("Password must not be empty if set");
    }

    @Nullable
    /* renamed from: c */
    public String mo16326c() {
        return this.f3034U;
    }

    @Nullable
    /* renamed from: d */
    public String mo16327d() {
        return this.f3036W;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Credential)) {
            return false;
        }
        Credential credential = (Credential) obj;
        return TextUtils.equals(this.f3029P, credential.f3029P) && TextUtils.equals(this.f3030Q, credential.f3030Q) && C2251t.m5617a(this.f3031R, credential.f3031R) && TextUtils.equals(this.f3033T, credential.f3033T) && TextUtils.equals(this.f3034U, credential.f3034U);
    }

    public int hashCode() {
        return C2251t.m5615a(this.f3029P, this.f3030Q, this.f3031R, this.f3033T, this.f3034U);
    }

    @Nullable
    /* renamed from: u */
    public String mo16330u() {
        return this.f3035V;
    }

    /* renamed from: v */
    public String mo16331v() {
        return this.f3029P;
    }

    /* renamed from: w */
    public List<IdToken> mo16332w() {
        return this.f3032S;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5602a(parcel, 1, mo16331v(), false);
        C2250b.m5602a(parcel, 2, mo16334x(), false);
        C2250b.m5596a(parcel, 3, (Parcelable) mo16336z(), i, false);
        C2250b.m5614c(parcel, 4, mo16332w(), false);
        C2250b.m5602a(parcel, 5, mo16335y(), false);
        C2250b.m5602a(parcel, 6, mo16326c(), false);
        C2250b.m5602a(parcel, 9, mo16330u(), false);
        C2250b.m5602a(parcel, 10, mo16327d(), false);
        C2250b.m5587a(parcel, a);
    }

    @Nullable
    /* renamed from: x */
    public String mo16334x() {
        return this.f3030Q;
    }

    @Nullable
    /* renamed from: y */
    public String mo16335y() {
        return this.f3033T;
    }

    @Nullable
    /* renamed from: z */
    public Uri mo16336z() {
        return this.f3031R;
    }
}
