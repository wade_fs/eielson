package com.google.android.gms.internal.location;

import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.LocationAvailability;

/* renamed from: com.google.android.gms.internal.location.l */
final class C2397l implements C2084i.C2086b<C2833e> {

    /* renamed from: a */
    private final /* synthetic */ LocationAvailability f3928a;

    C2397l(C2395j jVar, LocationAvailability locationAvailability) {
        this.f3928a = locationAvailability;
    }

    /* renamed from: a */
    public final void mo16725a() {
    }

    /* renamed from: a */
    public final /* synthetic */ void mo16726a(Object obj) {
        ((C2833e) obj).mo18254a(this.f3928a);
    }
}
