package com.google.android.gms.common.p087h;

import android.util.Log;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2227k;
import java.util.Locale;

/* renamed from: com.google.android.gms.common.h.a */
public class C2178a {

    /* renamed from: a */
    private final String f3573a;

    /* renamed from: b */
    private final String f3574b;

    /* renamed from: c */
    private final int f3575c;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2178a(java.lang.String r7, java.lang.String... r8) {
        /*
            r6 = this;
            int r0 = r8.length
            if (r0 != 0) goto L_0x0006
            java.lang.String r8 = ""
            goto L_0x0036
        L_0x0006:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 91
            r0.append(r1)
            int r1 = r8.length
            r2 = 0
        L_0x0012:
            if (r2 >= r1) goto L_0x0028
            r3 = r8[r2]
            int r4 = r0.length()
            r5 = 1
            if (r4 <= r5) goto L_0x0022
            java.lang.String r4 = ","
            r0.append(r4)
        L_0x0022:
            r0.append(r3)
            int r2 = r2 + 1
            goto L_0x0012
        L_0x0028:
            r8 = 93
            r0.append(r8)
            r8 = 32
            r0.append(r8)
            java.lang.String r8 = r0.toString()
        L_0x0036:
            r6.<init>(r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.p087h.C2178a.<init>(java.lang.String, java.lang.String[]):void");
    }

    /* renamed from: c */
    private final String m5313c(String str, @Nullable Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(Locale.US, str, objArr);
        }
        return this.f3574b.concat(str);
    }

    /* renamed from: a */
    public boolean mo16857a(int i) {
        return this.f3575c <= i;
    }

    /* renamed from: b */
    public void mo16858b(String str, @Nullable Object... objArr) {
        Log.e(this.f3573a, m5313c(str, objArr));
    }

    /* renamed from: a */
    public void mo16856a(String str, @Nullable Object... objArr) {
        if (mo16857a(3)) {
            Log.d(this.f3573a, m5313c(str, objArr));
        }
    }

    private C2178a(String str, String str2) {
        this.f3574b = str2;
        this.f3573a = str;
        new C2227k(str);
        int i = 2;
        while (7 >= i && !Log.isLoggable(this.f3573a, i)) {
            i++;
        }
        this.f3575c = i;
    }
}
