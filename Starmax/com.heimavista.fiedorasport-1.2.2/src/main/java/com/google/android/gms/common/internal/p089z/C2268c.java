package com.google.android.gms.common.internal.p089z;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;

/* renamed from: com.google.android.gms.common.internal.z.c */
final class C2268c extends C2016a.C2017a<C2275j, Object> {
    C2268c() {
    }

    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        return new C2275j(context, looper, eVar, bVar, cVar);
    }
}
