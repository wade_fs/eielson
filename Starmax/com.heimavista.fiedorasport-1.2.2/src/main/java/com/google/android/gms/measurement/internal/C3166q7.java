package com.google.android.gms.measurement.internal;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.annotation.WorkerThread;

/* renamed from: com.google.android.gms.measurement.internal.q7 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3166q7 extends C3157p9 {
    public C3166q7(C3145o9 o9Var) {
        super(o9Var);
    }

    @WorkerThread
    /* renamed from: a */
    public final byte[] mo19291a(@NonNull zzan zzan, @Size(min = 1) String str) {
        mo18881c();
        this.f5134a.mo19093g();
        throw null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        return false;
    }
}
