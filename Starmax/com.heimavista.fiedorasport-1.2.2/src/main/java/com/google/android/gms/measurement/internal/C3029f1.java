package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2556ic;

/* renamed from: com.google.android.gms.measurement.internal.f1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3029f1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5119a = new C3029f1();

    private C3029f1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2556ic.m6493d());
    }
}
