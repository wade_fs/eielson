package com.google.android.gms.internal.location;

import android.content.Context;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.C2835f;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.location.i */
public final class C2394i {

    /* renamed from: a */
    private final C2406u<C2392g> f3921a;

    /* renamed from: b */
    private boolean f3922b = false;

    /* renamed from: c */
    private final Map<C2084i.C2085a<C2835f>, C2399n> f3923c = new HashMap();

    /* renamed from: d */
    private final Map<C2084i.C2085a<Object>, C2398m> f3924d = new HashMap();

    /* renamed from: e */
    private final Map<C2084i.C2085a<C2833e>, C2395j> f3925e = new HashMap();

    public C2394i(Context context, C2406u<C2392g> uVar) {
        this.f3921a = uVar;
    }

    /* renamed from: a */
    private final C2395j m5915a(C2084i<C2833e> iVar) {
        C2395j jVar;
        synchronized (this.f3925e) {
            jVar = this.f3925e.get(iVar.mo16721b());
            if (jVar == null) {
                jVar = new C2395j(iVar);
            }
            this.f3925e.put(iVar.mo16721b(), jVar);
        }
        return jVar;
    }

    /* renamed from: a */
    public final void mo17207a() {
        synchronized (this.f3923c) {
            for (C2399n nVar : this.f3923c.values()) {
                if (nVar != null) {
                    this.f3921a.mo17227b().mo17204a(zzbf.m5952a(nVar, (C2389d) null));
                }
            }
            this.f3923c.clear();
        }
        synchronized (this.f3925e) {
            for (C2395j jVar : this.f3925e.values()) {
                if (jVar != null) {
                    this.f3921a.mo17227b().mo17204a(zzbf.m5951a(jVar, (C2389d) null));
                }
            }
            this.f3925e.clear();
        }
        synchronized (this.f3924d) {
            for (C2398m mVar : this.f3924d.values()) {
                if (mVar != null) {
                    this.f3921a.mo17227b().mo17205a(new zzo(2, null, mVar.asBinder(), null));
                }
            }
            this.f3924d.clear();
        }
    }

    /* renamed from: a */
    public final void mo17208a(C2084i.C2085a<C2833e> aVar, C2389d dVar) {
        this.f3921a.mo17226a();
        C2258v.m5630a(aVar, "Invalid null listener key");
        synchronized (this.f3925e) {
            C2395j remove = this.f3925e.remove(aVar);
            if (remove != null) {
                remove.mo17212H();
                this.f3921a.mo17227b().mo17204a(zzbf.m5951a(remove, dVar));
            }
        }
    }

    /* renamed from: a */
    public final void mo17209a(zzbd zzbd, C2084i<C2833e> iVar, C2389d dVar) {
        this.f3921a.mo17226a();
        this.f3921a.mo17227b().mo17204a(new zzbf(1, zzbd, null, null, m5915a(iVar).asBinder(), dVar != null ? dVar.asBinder() : null));
    }

    /* renamed from: a */
    public final void mo17210a(boolean z) {
        this.f3921a.mo17226a();
        this.f3921a.mo17227b().mo17206n(z);
        this.f3922b = z;
    }

    /* renamed from: b */
    public final void mo17211b() {
        if (this.f3922b) {
            mo17210a(false);
        }
    }
}
