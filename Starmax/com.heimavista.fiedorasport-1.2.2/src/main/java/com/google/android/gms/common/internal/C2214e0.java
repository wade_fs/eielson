package com.google.android.gms.common.internal;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2197d;

/* renamed from: com.google.android.gms.common.internal.e0 */
final class C2214e0 implements C2197d.C2198a {

    /* renamed from: P */
    private final /* synthetic */ C2036f.C2038b f3708P;

    C2214e0(C2036f.C2038b bVar) {
        this.f3708P = bVar;
    }

    /* renamed from: L */
    public final void mo16940L(int i) {
        this.f3708P.mo16583L(i);
    }

    /* renamed from: f */
    public final void mo16941f(@Nullable Bundle bundle) {
        this.f3708P.mo16584f(bundle);
    }
}
