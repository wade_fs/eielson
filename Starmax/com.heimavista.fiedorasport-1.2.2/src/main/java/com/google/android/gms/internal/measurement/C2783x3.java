package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.x3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2783x3 extends C2813z2<Double> implements C2738u4<Double>, C2485e6, RandomAccess {

    /* renamed from: Q */
    private double[] f4567Q;

    /* renamed from: R */
    private int f4568R;

    static {
        new C2783x3(new double[0], 0).mo17939e();
    }

    C2783x3() {
        this(new double[10], 0);
    }

    /* renamed from: b */
    private final void m7765b(int i) {
        if (i < 0 || i >= this.f4568R) {
            throw new IndexOutOfBoundsException(m7766c(i));
        }
    }

    /* renamed from: c */
    private final String m7766c(int i) {
        int i2 = this.f4568R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: a */
    public final void mo18133a(double d) {
        mo18179b();
        int i = this.f4568R;
        double[] dArr = this.f4567Q;
        if (i == dArr.length) {
            double[] dArr2 = new double[(((i * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.f4567Q = dArr2;
        }
        double[] dArr3 = this.f4567Q;
        int i2 = this.f4568R;
        this.f4568R = i2 + 1;
        dArr3[i2] = d;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double doubleValue = ((Double) obj).doubleValue();
        mo18179b();
        if (i < 0 || i > (i2 = this.f4568R)) {
            throw new IndexOutOfBoundsException(m7766c(i));
        }
        double[] dArr = this.f4567Q;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.f4567Q, i, dArr2, i + 1, this.f4568R - i);
            this.f4567Q = dArr2;
        }
        this.f4567Q[i] = doubleValue;
        this.f4568R++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Double> collection) {
        mo18179b();
        C2647o4.m6961a(collection);
        if (!(collection instanceof C2783x3)) {
            return super.addAll(collection);
        }
        C2783x3 x3Var = (C2783x3) collection;
        int i = x3Var.f4568R;
        if (i == 0) {
            return false;
        }
        int i2 = this.f4568R;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.f4567Q;
            if (i3 > dArr.length) {
                this.f4567Q = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(x3Var.f4567Q, 0, this.f4567Q, this.f4568R, x3Var.f4568R);
            this.f4568R = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2783x3)) {
            return super.equals(obj);
        }
        C2783x3 x3Var = (C2783x3) obj;
        if (this.f4568R != x3Var.f4568R) {
            return false;
        }
        double[] dArr = x3Var.f4567Q;
        for (int i = 0; i < this.f4568R; i++) {
            if (Double.doubleToLongBits(this.f4567Q[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        m7765b(i);
        return Double.valueOf(this.f4567Q[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.f4568R; i2++) {
            i = (i * 31) + C2647o4.m6959a(Double.doubleToLongBits(this.f4567Q[i2]));
        }
        return i;
    }

    public final boolean remove(Object obj) {
        mo18179b();
        for (int i = 0; i < this.f4568R; i++) {
            if (obj.equals(Double.valueOf(this.f4567Q[i]))) {
                double[] dArr = this.f4567Q;
                System.arraycopy(dArr, i + 1, dArr, i, (this.f4568R - i) - 1);
                this.f4568R--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        mo18179b();
        if (i2 >= i) {
            double[] dArr = this.f4567Q;
            System.arraycopy(dArr, i2, dArr, i, this.f4568R - i2);
            this.f4568R -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        mo18179b();
        m7765b(i);
        double[] dArr = this.f4567Q;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    public final int size() {
        return this.f4568R;
    }

    private C2783x3(double[] dArr, int i) {
        this.f4567Q = dArr;
        this.f4568R = i;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        m7765b(i);
        double[] dArr = this.f4567Q;
        double d = dArr[i];
        int i2 = this.f4568R;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.f4568R--;
        this.modCount++;
        return Double.valueOf(d);
    }

    /* renamed from: a */
    public final /* synthetic */ C2738u4 mo17320a(int i) {
        if (i >= this.f4568R) {
            return new C2783x3(Arrays.copyOf(this.f4567Q, i), this.f4568R);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        mo18133a(((Double) obj).doubleValue());
        return true;
    }
}
