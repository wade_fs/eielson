package com.google.android.gms.internal.measurement;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.u4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2738u4<E> extends List<E>, RandomAccess {
    /* renamed from: a */
    C2738u4<E> mo17320a(int i);

    /* renamed from: a */
    boolean mo17938a();

    /* renamed from: e */
    void mo17939e();
}
