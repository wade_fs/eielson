package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.k8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3096k8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5303P;

    /* renamed from: Q */
    private final /* synthetic */ String f5304Q;

    /* renamed from: R */
    private final /* synthetic */ String f5305R;

    /* renamed from: S */
    private final /* synthetic */ String f5306S;

    /* renamed from: T */
    private final /* synthetic */ zzm f5307T;

    /* renamed from: U */
    private final /* synthetic */ C3232w7 f5308U;

    C3096k8(C3232w7 w7Var, AtomicReference atomicReference, String str, String str2, String str3, zzm zzm) {
        this.f5308U = w7Var;
        this.f5303P = atomicReference;
        this.f5304Q = str;
        this.f5305R = str2;
        this.f5306S = str3;
        this.f5307T = zzm;
    }

    public final void run() {
        synchronized (this.f5303P) {
            try {
                C3228w3 d = this.f5308U.f5724d;
                if (d == null) {
                    this.f5308U.mo19015l().mo19001t().mo19045a("(legacy) Failed to get conditional properties; not connected to service", C3032f4.m8621a(this.f5304Q), this.f5305R, this.f5306S);
                    this.f5303P.set(Collections.emptyList());
                    this.f5303P.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f5304Q)) {
                    this.f5303P.set(d.mo19184a(this.f5305R, this.f5306S, this.f5307T));
                } else {
                    this.f5303P.set(d.mo19185a(this.f5304Q, this.f5305R, this.f5306S));
                }
                this.f5308U.m9314J();
                this.f5303P.notify();
            } catch (RemoteException e) {
                try {
                    this.f5308U.mo19015l().mo19001t().mo19045a("(legacy) Failed to get conditional properties; remote exception", C3032f4.m8621a(this.f5304Q), this.f5305R, e);
                    this.f5303P.set(Collections.emptyList());
                    this.f5303P.notify();
                } catch (Throwable th) {
                    this.f5303P.notify();
                    throw th;
                }
            }
        }
    }
}
