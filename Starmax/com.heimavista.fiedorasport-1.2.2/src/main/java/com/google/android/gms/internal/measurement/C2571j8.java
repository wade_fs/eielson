package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.j8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2571j8 implements C2585k8 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4268a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true);

    /* renamed from: a */
    public final boolean mo17607a() {
        return f4268a.mo18128b().booleanValue();
    }
}
