package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.v8 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3222v8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3145o9 f5704P;

    /* renamed from: Q */
    private final /* synthetic */ Runnable f5705Q;

    C3222v8(C3211u8 u8Var, C3145o9 o9Var, Runnable runnable) {
        this.f5704P = o9Var;
        this.f5705Q = runnable;
    }

    public final void run() {
        this.f5704P.mo19237q();
        this.f5704P.mo19217a(this.f5705Q);
        this.f5704P.mo19236p();
    }
}
