package com.google.android.material.shape;

@Deprecated
public class ShapePathModel extends ShapeAppearanceModel {
    @Deprecated
    public void setAllCorners(CornerTreatment cornerTreatment) {
        super.topLeftCorner = cornerTreatment;
        super.topRightCorner = cornerTreatment;
        super.bottomRightCorner = cornerTreatment;
        super.bottomLeftCorner = cornerTreatment;
    }

    @Deprecated
    public void setAllEdges(EdgeTreatment edgeTreatment) {
        super.leftEdge = edgeTreatment;
        super.topEdge = edgeTreatment;
        super.rightEdge = edgeTreatment;
        super.bottomEdge = edgeTreatment;
    }

    @Deprecated
    public void setBottomEdge(EdgeTreatment edgeTreatment) {
        super.bottomEdge = edgeTreatment;
    }

    @Deprecated
    public void setBottomLeftCorner(CornerTreatment cornerTreatment) {
        super.bottomLeftCorner = cornerTreatment;
    }

    @Deprecated
    public void setBottomRightCorner(CornerTreatment cornerTreatment) {
        super.bottomRightCorner = cornerTreatment;
    }

    @Deprecated
    public void setCornerTreatments(CornerTreatment cornerTreatment, CornerTreatment cornerTreatment2, CornerTreatment cornerTreatment3, CornerTreatment cornerTreatment4) {
        super.topLeftCorner = cornerTreatment;
        super.topRightCorner = cornerTreatment2;
        super.bottomRightCorner = cornerTreatment3;
        super.bottomLeftCorner = cornerTreatment4;
    }

    @Deprecated
    public void setEdgeTreatments(EdgeTreatment edgeTreatment, EdgeTreatment edgeTreatment2, EdgeTreatment edgeTreatment3, EdgeTreatment edgeTreatment4) {
        super.leftEdge = edgeTreatment;
        super.topEdge = edgeTreatment2;
        super.rightEdge = edgeTreatment3;
        super.bottomEdge = edgeTreatment4;
    }

    @Deprecated
    public void setLeftEdge(EdgeTreatment edgeTreatment) {
        super.leftEdge = edgeTreatment;
    }

    @Deprecated
    public void setRightEdge(EdgeTreatment edgeTreatment) {
        super.rightEdge = edgeTreatment;
    }

    @Deprecated
    public void setTopEdge(EdgeTreatment edgeTreatment) {
        super.topEdge = edgeTreatment;
    }

    @Deprecated
    public void setTopLeftCorner(CornerTreatment cornerTreatment) {
        super.topLeftCorner = cornerTreatment;
    }

    @Deprecated
    public void setTopRightCorner(CornerTreatment cornerTreatment) {
        super.topRightCorner = cornerTreatment;
    }
}
