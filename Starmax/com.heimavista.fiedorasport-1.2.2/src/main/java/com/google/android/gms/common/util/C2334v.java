package com.google.android.gms.common.util;

import android.os.Looper;

/* renamed from: com.google.android.gms.common.util.v */
public final class C2334v {
    /* renamed from: a */
    public static boolean m5825a() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
