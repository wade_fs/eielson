package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Collections;
import java.util.List;

public final class LocationSettingsRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<LocationSettingsRequest> CREATOR = new C2851n();

    /* renamed from: P */
    private final List<LocationRequest> f4675P;

    /* renamed from: Q */
    private final boolean f4676Q;

    /* renamed from: R */
    private final boolean f4677R;

    /* renamed from: S */
    private zzae f4678S;

    LocationSettingsRequest(List<LocationRequest> list, boolean z, boolean z2, zzae zzae) {
        this.f4675P = list;
        this.f4676Q = z;
        this.f4677R = z2;
        this.f4678S = zzae;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.location.zzae, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5614c(parcel, 1, Collections.unmodifiableList(this.f4675P), false);
        C2250b.m5605a(parcel, 2, this.f4676Q);
        C2250b.m5605a(parcel, 3, this.f4677R);
        C2250b.m5596a(parcel, 5, (Parcelable) this.f4678S, i, false);
        C2250b.m5587a(parcel, a);
    }
}
