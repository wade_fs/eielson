package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.r9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2696r9 implements C2593l2<C2681q9> {

    /* renamed from: Q */
    private static C2696r9 f4465Q = new C2696r9();

    /* renamed from: P */
    private final C2593l2<C2681q9> f4466P;

    private C2696r9(C2593l2<C2681q9> l2Var) {
        this.f4466P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7134b() {
        return ((C2681q9) f4465Q.mo17285a()).mo17835a();
    }

    /* renamed from: c */
    public static boolean m7135c() {
        return ((C2681q9) f4465Q.mo17285a()).mo17836e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4466P.mo17285a();
    }

    public C2696r9() {
        this(C2579k2.m6601a(new C2728t9()));
    }
}
