package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.rb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2698rb implements C2654ob {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4469a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.config.string.always_update_disk_on_set", false);

    /* renamed from: a */
    public final boolean mo17803a() {
        return f4469a.mo18128b().booleanValue();
    }
}
