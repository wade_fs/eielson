package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.backends.i */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
class C1719i {

    /* renamed from: a */
    private final Context f2702a;

    /* renamed from: b */
    private final C3971a f2703b;

    /* renamed from: c */
    private final C3971a f2704c;

    C1719i(Context context, C3971a aVar, C3971a aVar2) {
        this.f2702a = context;
        this.f2703b = aVar;
        this.f2704c = aVar2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C1718h mo13555a(String str) {
        return C1718h.m4292a(this.f2702a, this.f2703b, this.f2704c, str);
    }
}
