package com.google.android.gms.common.internal;

import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.common.internal.r */
public interface C2244r extends IInterface {
    /* renamed from: a */
    C3988b mo16975a(C3988b bVar, SignInButtonConfig signInButtonConfig);
}
