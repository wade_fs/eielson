package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2789x9;

/* renamed from: com.google.android.gms.measurement.internal.h3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3055h3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5172a = new C3055h3();

    private C3055h3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2789x9.m7776c());
    }
}
