package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.f */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2493f extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4092T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4093U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2493f(C2525gd gdVar, String str) {
        super(gdVar);
        this.f4093U = gdVar;
        this.f4092T = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4093U.f4192g.endAdUnitExposure(this.f4092T, super.f4194Q);
    }
}
