package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.wc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2776wc implements C2731tc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4560a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.collection.service.update_with_analytics_fix", false);

    /* renamed from: a */
    public final boolean mo17920a() {
        return f4560a.mo18128b().booleanValue();
    }
}
