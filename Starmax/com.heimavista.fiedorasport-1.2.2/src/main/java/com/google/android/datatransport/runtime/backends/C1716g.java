package com.google.android.datatransport.runtime.backends;

/* renamed from: com.google.android.datatransport.runtime.backends.g */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C1716g {

    /* renamed from: com.google.android.datatransport.runtime.backends.g$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public enum C1717a {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    /* renamed from: a */
    public static C1716g m4287a(long j) {
        return new C1710b(C1717a.OK, j);
    }

    /* renamed from: c */
    public static C1716g m4288c() {
        return new C1710b(C1717a.FATAL_ERROR, -1);
    }

    /* renamed from: d */
    public static C1716g m4289d() {
        return new C1710b(C1717a.TRANSIENT_ERROR, -1);
    }

    /* renamed from: a */
    public abstract long mo13542a();

    /* renamed from: b */
    public abstract C1717a mo13543b();
}
