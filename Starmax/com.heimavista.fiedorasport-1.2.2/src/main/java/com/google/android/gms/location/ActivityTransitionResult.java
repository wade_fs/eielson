package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Collections;
import java.util.List;

public class ActivityTransitionResult extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ActivityTransitionResult> CREATOR = new C2863x();

    /* renamed from: P */
    private final List<ActivityTransitionEvent> f4653P;

    public ActivityTransitionResult(List<ActivityTransitionEvent> list) {
        C2258v.m5630a(list, "transitionEvents list can't be null.");
        if (!list.isEmpty()) {
            for (int i = 1; i < list.size(); i++) {
                C2258v.m5636a(list.get(i).mo18197d() >= list.get(i + -1).mo18197d());
            }
        }
        this.f4653P = Collections.unmodifiableList(list);
    }

    /* renamed from: c */
    public List<ActivityTransitionEvent> mo18207c() {
        return this.f4653P;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ActivityTransitionResult.class != obj.getClass()) {
            return false;
        }
        return this.f4653P.equals(((ActivityTransitionResult) obj).f4653P);
    }

    public int hashCode() {
        return this.f4653P.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5614c(parcel, 1, mo18207c(), false);
        C2250b.m5587a(parcel, a);
    }
}
