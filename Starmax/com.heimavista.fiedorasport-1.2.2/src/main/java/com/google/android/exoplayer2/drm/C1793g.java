package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.g */
/* compiled from: lambda */
public final /* synthetic */ class C1793g implements EventDispatcher.Event {

    /* renamed from: a */
    public static final /* synthetic */ C1793g f2809a = new C1793g();

    private /* synthetic */ C1793g() {
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmKeysRestored();
    }
}
