package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.view.View;
import androidx.collection.ArraySet;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import p119e.p144d.p145a.p157c.p166d.C4047a;

/* renamed from: com.google.android.gms.common.internal.e */
public final class C2211e {

    /* renamed from: a */
    private final Account f3689a;

    /* renamed from: b */
    private final Set<Scope> f3690b;

    /* renamed from: c */
    private final Set<Scope> f3691c;

    /* renamed from: d */
    private final Map<C2016a<?>, C2213b> f3692d;

    /* renamed from: e */
    private final String f3693e;

    /* renamed from: f */
    private final String f3694f;

    /* renamed from: g */
    private final C4047a f3695g;

    /* renamed from: h */
    private final boolean f3696h;

    /* renamed from: i */
    private Integer f3697i;

    /* renamed from: com.google.android.gms.common.internal.e$a */
    public static final class C2212a {

        /* renamed from: a */
        private Account f3698a;

        /* renamed from: b */
        private ArraySet<Scope> f3699b;

        /* renamed from: c */
        private Map<C2016a<?>, C2213b> f3700c;

        /* renamed from: d */
        private int f3701d = 0;

        /* renamed from: e */
        private View f3702e;

        /* renamed from: f */
        private String f3703f;

        /* renamed from: g */
        private String f3704g;

        /* renamed from: h */
        private C4047a f3705h = C4047a.f7385X;

        /* renamed from: i */
        private boolean f3706i;

        /* renamed from: a */
        public final C2212a mo16969a(Account account) {
            this.f3698a = account;
            return this;
        }

        /* renamed from: b */
        public final C2212a mo16973b(String str) {
            this.f3703f = str;
            return this;
        }

        /* renamed from: a */
        public final C2212a mo16971a(Collection<Scope> collection) {
            if (this.f3699b == null) {
                this.f3699b = new ArraySet<>();
            }
            this.f3699b.addAll(collection);
            return this;
        }

        /* renamed from: a */
        public final C2212a mo16970a(String str) {
            this.f3704g = str;
            return this;
        }

        /* renamed from: a */
        public final C2211e mo16972a() {
            return new C2211e(this.f3698a, this.f3699b, this.f3700c, this.f3701d, this.f3702e, this.f3703f, this.f3704g, this.f3705h, this.f3706i);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.e$b */
    public static final class C2213b {

        /* renamed from: a */
        public final Set<Scope> f3707a;
    }

    public C2211e(Account account, Set<Scope> set, Map<C2016a<?>, C2213b> map, int i, View view, String str, String str2, C4047a aVar, boolean z) {
        this.f3689a = account;
        this.f3690b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        this.f3692d = map == null ? Collections.EMPTY_MAP : map;
        this.f3693e = str;
        this.f3694f = str2;
        this.f3695g = aVar;
        this.f3696h = z;
        HashSet hashSet = new HashSet(this.f3690b);
        for (C2213b bVar : this.f3692d.values()) {
            hashSet.addAll(bVar.f3707a);
        }
        this.f3691c = Collections.unmodifiableSet(hashSet);
    }

    /* renamed from: a */
    public final Account mo16956a() {
        return this.f3689a;
    }

    @Deprecated
    /* renamed from: b */
    public final String mo16959b() {
        Account account = this.f3689a;
        if (account != null) {
            return account.name;
        }
        return null;
    }

    /* renamed from: c */
    public final Account mo16960c() {
        Account account = this.f3689a;
        if (account != null) {
            return account;
        }
        return new Account("<<default account>>", "com.google");
    }

    /* renamed from: d */
    public final Set<Scope> mo16961d() {
        return this.f3691c;
    }

    /* renamed from: e */
    public final Integer mo16962e() {
        return this.f3697i;
    }

    /* renamed from: f */
    public final Map<C2016a<?>, C2213b> mo16963f() {
        return this.f3692d;
    }

    /* renamed from: g */
    public final String mo16964g() {
        return this.f3694f;
    }

    /* renamed from: h */
    public final String mo16965h() {
        return this.f3693e;
    }

    /* renamed from: i */
    public final Set<Scope> mo16966i() {
        return this.f3690b;
    }

    /* renamed from: j */
    public final C4047a mo16967j() {
        return this.f3695g;
    }

    /* renamed from: k */
    public final boolean mo16968k() {
        return this.f3696h;
    }

    /* renamed from: a */
    public final void mo16958a(Integer num) {
        this.f3697i = num;
    }

    /* renamed from: a */
    public final Set<Scope> mo16957a(C2016a<?> aVar) {
        C2213b bVar = this.f3692d.get(aVar);
        if (bVar == null || bVar.f3707a.isEmpty()) {
            return this.f3690b;
        }
        HashSet hashSet = new HashSet(this.f3690b);
        hashSet.addAll(bVar.f3707a);
        return hashSet;
    }
}
