package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.common.api.t */
public final class C2166t implements Parcelable.Creator<Status> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        String str = null;
        PendingIntent pendingIntent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) C2248a.m5553a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 1000) {
                C2248a.m5550F(parcel, a);
            } else {
                i = C2248a.m5585z(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new Status(i, i2, str, pendingIntent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new Status[i];
    }
}
