package com.google.android.datatransport.cct.p084b;

import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1661a;

/* renamed from: com.google.android.datatransport.cct.b.d */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1665d extends C1661a {

    /* renamed from: a */
    private final int f2574a;

    /* renamed from: b */
    private final String f2575b;

    /* renamed from: c */
    private final String f2576c;

    /* renamed from: d */
    private final String f2577d;

    /* renamed from: e */
    private final String f2578e;

    /* renamed from: f */
    private final String f2579f;

    /* renamed from: g */
    private final String f2580g;

    /* renamed from: h */
    private final String f2581h;

    /* renamed from: com.google.android.datatransport.cct.b.d$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1667b extends C1661a.C1662a {

        /* renamed from: a */
        private Integer f2582a;

        /* renamed from: b */
        private String f2583b;

        /* renamed from: c */
        private String f2584c;

        /* renamed from: d */
        private String f2585d;

        /* renamed from: e */
        private String f2586e;

        /* renamed from: f */
        private String f2587f;

        /* renamed from: g */
        private String f2588g;

        /* renamed from: h */
        private String f2589h;

        C1667b() {
        }

        /* renamed from: a */
        public C1661a.C1662a mo13444a(int i) {
            this.f2582a = Integer.valueOf(i);
            return super;
        }

        /* renamed from: b */
        public C1661a.C1662a mo13447b(@Nullable String str) {
            this.f2589h = str;
            return super;
        }

        /* renamed from: c */
        public C1661a.C1662a mo13448c(@Nullable String str) {
            this.f2584c = str;
            return super;
        }

        /* renamed from: d */
        public C1661a.C1662a mo13449d(@Nullable String str) {
            this.f2588g = str;
            return super;
        }

        /* renamed from: e */
        public C1661a.C1662a mo13450e(@Nullable String str) {
            this.f2583b = str;
            return super;
        }

        /* renamed from: f */
        public C1661a.C1662a mo13451f(@Nullable String str) {
            this.f2587f = str;
            return super;
        }

        /* renamed from: g */
        public C1661a.C1662a mo13452g(@Nullable String str) {
            this.f2586e = str;
            return super;
        }

        /* renamed from: a */
        public C1661a.C1662a mo13445a(@Nullable String str) {
            this.f2585d = str;
            return super;
        }

        /* renamed from: a */
        public C1661a mo13446a() {
            String str = "";
            if (this.f2582a == null) {
                str = str + " sdkVersion";
            }
            if (str.isEmpty()) {
                return new C1665d(this.f2582a.intValue(), this.f2583b, this.f2584c, this.f2585d, this.f2586e, this.f2587f, this.f2588g, this.f2589h, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* synthetic */ C1665d(int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, C1666a aVar) {
        this.f2574a = i;
        this.f2575b = str;
        this.f2576c = str2;
        this.f2577d = str3;
        this.f2578e = str4;
        this.f2579f = str5;
        this.f2580g = str6;
        this.f2581h = str7;
    }

    @Nullable
    /* renamed from: b */
    public String mo13454b() {
        return this.f2577d;
    }

    @Nullable
    /* renamed from: c */
    public String mo13455c() {
        return this.f2581h;
    }

    @Nullable
    /* renamed from: d */
    public String mo13456d() {
        return this.f2576c;
    }

    @Nullable
    /* renamed from: e */
    public String mo13457e() {
        return this.f2580g;
    }

    public boolean equals(Object obj) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1661a)) {
            return false;
        }
        C1665d dVar = (C1665d) obj;
        if (this.f2574a == dVar.f2574a && ((str = this.f2575b) != null ? str.equals(dVar.f2575b) : dVar.f2575b == null) && ((str2 = this.f2576c) != null ? str2.equals(dVar.f2576c) : dVar.f2576c == null) && ((str3 = this.f2577d) != null ? str3.equals(dVar.f2577d) : dVar.f2577d == null) && ((str4 = this.f2578e) != null ? str4.equals(dVar.f2578e) : dVar.f2578e == null) && ((str5 = this.f2579f) != null ? str5.equals(dVar.f2579f) : dVar.f2579f == null) && ((str6 = this.f2580g) != null ? str6.equals(dVar.f2580g) : dVar.f2580g == null)) {
            String str7 = this.f2581h;
            if (str7 == null) {
                if (dVar.f2581h == null) {
                    return true;
                }
            } else if (str7.equals(dVar.f2581h)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    /* renamed from: f */
    public String mo13459f() {
        return this.f2575b;
    }

    @Nullable
    /* renamed from: g */
    public String mo13460g() {
        return this.f2579f;
    }

    @Nullable
    /* renamed from: h */
    public String mo13461h() {
        return this.f2578e;
    }

    public int hashCode() {
        int i = (this.f2574a ^ 1000003) * 1000003;
        String str = this.f2575b;
        int i2 = 0;
        int hashCode = (i ^ (str == null ? 0 : str.hashCode())) * 1000003;
        String str2 = this.f2576c;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.f2577d;
        int hashCode3 = (hashCode2 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        String str4 = this.f2578e;
        int hashCode4 = (hashCode3 ^ (str4 == null ? 0 : str4.hashCode())) * 1000003;
        String str5 = this.f2579f;
        int hashCode5 = (hashCode4 ^ (str5 == null ? 0 : str5.hashCode())) * 1000003;
        String str6 = this.f2580g;
        int hashCode6 = (hashCode5 ^ (str6 == null ? 0 : str6.hashCode())) * 1000003;
        String str7 = this.f2581h;
        if (str7 != null) {
            i2 = str7.hashCode();
        }
        return hashCode6 ^ i2;
    }

    /* renamed from: i */
    public int mo13463i() {
        return this.f2574a;
    }

    public String toString() {
        return "AndroidClientInfo{sdkVersion=" + this.f2574a + ", model=" + this.f2575b + ", hardware=" + this.f2576c + ", device=" + this.f2577d + ", product=" + this.f2578e + ", osBuild=" + this.f2579f + ", manufacturer=" + this.f2580g + ", fingerprint=" + this.f2581h + "}";
    }
}
