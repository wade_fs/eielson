package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ga */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2522ga implements C2539ha {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4178a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.logging.improved_messaging_q4_2019_service", true);

    /* renamed from: a */
    public final boolean mo17503a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17504e() {
        return f4178a.mo18128b().booleanValue();
    }
}
