package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.maps.model.o */
public final class C2944o implements Parcelable.Creator<PatternItem> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        Float f = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                f = C2248a.m5583x(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new PatternItem(i, f);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PatternItem[i];
    }
}
