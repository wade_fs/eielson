package com.google.android.gms.measurement.internal;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import com.google.android.gms.internal.measurement.C2520g8;

/* renamed from: com.google.android.gms.measurement.internal.x8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3244x8 extends C2995c3 {
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Handler f5760c;

    /* renamed from: d */
    protected C3073i9 f5761d = new C3073i9(this);

    /* renamed from: e */
    protected C3049g9 f5762e = new C3049g9(this);

    /* renamed from: f */
    private C3001c9 f5763f = new C3001c9(this);

    C3244x8(C3081j5 j5Var) {
        super(j5Var);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: C */
    public final void m9355C() {
        mo18881c();
        if (this.f5760c == null) {
            this.f5760c = new C2520g8(Looper.getMainLooper());
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: a */
    public final void m9356a(long j) {
        mo18881c();
        m9355C();
        mo19015l().mo18996B().mo19043a("Activity resumed, time", Long.valueOf(j));
        this.f5763f.mo18830a();
        this.f5762e.mo19034a(j);
        C3073i9 i9Var = this.f5761d;
        i9Var.f5221a.mo18881c();
        if (i9Var.f5221a.f5134a.mo19089c()) {
            if (i9Var.f5221a.mo19013h().mo19146a(C3135o.f5418S)) {
                i9Var.f5221a.mo19012g().f5627y.mo19332a(false);
            }
            i9Var.mo19069a(i9Var.f5221a.mo19017o().mo17132a(), false);
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: b */
    public final void m9360b(long j) {
        mo18881c();
        m9355C();
        mo19015l().mo18996B().mo19043a("Activity paused, time", Long.valueOf(j));
        this.f5763f.mo18831b();
        this.f5762e.mo19037b(j);
        C3073i9 i9Var = this.f5761d;
        if (i9Var.f5221a.mo19013h().mo19146a(C3135o.f5418S)) {
            i9Var.f5221a.mo19012g().f5627y.mo19332a(true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return false;
    }

    /* access modifiers changed from: protected */
    @MainThread
    /* renamed from: B */
    public final void mo19396B() {
        mo19014j().mo19028a(new C2977a9(this, mo19017o().elapsedRealtime()));
    }

    /* renamed from: a */
    public final boolean mo19397a(boolean z, boolean z2, long j) {
        return this.f5762e.mo19035a(z, z2, j);
    }
}
