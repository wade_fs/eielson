package com.google.android.gms.internal.measurement;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: com.google.android.gms.internal.measurement.f3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2498f3 implements Serializable, Iterable<Byte> {

    /* renamed from: Q */
    public static final C2498f3 f4094Q = new C2660p3(C2647o4.f4368b);

    /* renamed from: R */
    private static final C2594l3 f4095R = (C2797y2.m7826a() ? new C2646o3(null) : new C2562j3(null));

    /* renamed from: P */
    private int f4096P = 0;

    static {
        new C2532h3();
    }

    C2498f3() {
    }

    /* renamed from: a */
    public static C2498f3 m6303a(byte[] bArr, int i, int i2) {
        m6305b(i, i + i2, bArr.length);
        return new C2660p3(f4095R.mo17587a(bArr, i, i2));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static int m6304b(byte b) {
        return b & 255;
    }

    /* renamed from: b */
    static int m6305b(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    /* renamed from: c */
    static C2630n3 m6306c(int i) {
        return new C2630n3(i, null);
    }

    /* renamed from: a */
    public abstract byte mo17468a(int i);

    /* renamed from: a */
    public abstract int mo17469a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract int mo17470a(int i, int i2, int i3);

    /* renamed from: a */
    public abstract C2498f3 mo17471a(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract String mo17472a(Charset charset);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17473a(C2449c3 c3Var);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract byte mo17474b(int i);

    /* renamed from: e */
    public final String mo17475e() {
        return mo17469a() == 0 ? "" : mo17472a(C2647o4.f4367a);
    }

    public abstract boolean equals(Object obj);

    /* renamed from: f */
    public abstract boolean mo17477f();

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public final int mo17478g() {
        return this.f4096P;
    }

    public final int hashCode() {
        int i = this.f4096P;
        if (i == 0) {
            int a = mo17469a();
            i = mo17470a(a, 0, a);
            if (i == 0) {
                i = 1;
            }
            this.f4096P = i;
        }
        return i;
    }

    public /* synthetic */ Iterator iterator() {
        return new C2482e3(this);
    }

    public final String toString() {
        Locale locale = Locale.ROOT;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.toHexString(System.identityHashCode(this));
        objArr[1] = Integer.valueOf(mo17469a());
        objArr[2] = mo17469a() <= 50 ? C2817z6.m7928a(this) : String.valueOf(C2817z6.m7928a(mo17471a(0, 47))).concat("...");
        return String.format(locale, "<ByteString@%s size=%d contents=\"%s\">", objArr);
    }

    /* renamed from: a */
    public static C2498f3 m6302a(String str) {
        return new C2660p3(str.getBytes(C2647o4.f4367a));
    }
}
