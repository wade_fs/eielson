package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.backends.C1716g;

/* renamed from: com.google.android.datatransport.runtime.backends.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C1710b extends C1716g {

    /* renamed from: a */
    private final C1716g.C1717a f2692a;

    /* renamed from: b */
    private final long f2693b;

    C1710b(C1716g.C1717a aVar, long j) {
        if (aVar != null) {
            this.f2692a = aVar;
            this.f2693b = j;
            return;
        }
        throw new NullPointerException("Null status");
    }

    /* renamed from: a */
    public long mo13542a() {
        return this.f2693b;
    }

    /* renamed from: b */
    public C1716g.C1717a mo13543b() {
        return this.f2692a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1716g)) {
            return false;
        }
        C1716g gVar = (C1716g) obj;
        if (!this.f2692a.equals(super.mo13543b()) || this.f2693b != super.mo13542a()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.f2693b;
        return ((this.f2692a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return "BackendResponse{status=" + this.f2692a + ", nextRequestWaitMillis=" + this.f2693b + "}";
    }
}
