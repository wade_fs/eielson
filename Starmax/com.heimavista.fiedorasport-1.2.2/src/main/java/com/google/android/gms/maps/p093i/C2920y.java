package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;

/* renamed from: com.google.android.gms.maps.i.y */
public final class C2920y extends C4032a implements C2901g {
    C2920y(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
    }

    /* renamed from: a */
    public final void mo18471a(Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, bundle);
        Parcel a2 = mo23665a(7, a);
        if (a2.readInt() != 0) {
            bundle.readFromParcel(a2);
        }
        a2.recycle();
    }

    /* renamed from: b */
    public final void mo18474b(Bundle bundle) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, bundle);
        mo23667b(2, a);
    }

    /* renamed from: c */
    public final void mo18475c() {
        mo23667b(11, mo23664a());
    }

    /* renamed from: d */
    public final void mo18476d() {
        mo23667b(4, mo23664a());
    }

    public final C3988b getView() {
        Parcel a = mo23665a(8, mo23664a());
        C3988b a2 = C3988b.C3989a.m11981a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    public final void onLowMemory() {
        mo23667b(6, mo23664a());
    }

    public final void onResume() {
        mo23667b(3, mo23664a());
    }

    public final void onStart() {
        mo23667b(10, mo23664a());
    }

    /* renamed from: b */
    public final void mo18473b() {
        mo23667b(5, mo23664a());
    }

    /* renamed from: a */
    public final void mo18472a(C2916u uVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, uVar);
        mo23667b(9, a);
    }
}
