package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

/* renamed from: com.google.android.gms.common.api.internal.o1 */
final class C2113o1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2109n1 f3415P;

    C2113o1(C2109n1 n1Var) {
        this.f3415P = n1Var;
    }

    public final void run() {
        this.f3415P.f3399g.mo16686b(new ConnectionResult(4));
    }
}
