package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class StreetViewPanoramaOrientation extends AbstractSafeParcelable {
    public static final Parcelable.Creator<StreetViewPanoramaOrientation> CREATOR = new C2951v();

    /* renamed from: P */
    public final float f4892P;

    /* renamed from: Q */
    public final float f4893Q;

    /* renamed from: com.google.android.gms.maps.model.StreetViewPanoramaOrientation$a */
    public static final class C2928a {

        /* renamed from: a */
        public float f4894a;

        /* renamed from: b */
        public float f4895b;

        /* renamed from: a */
        public final C2928a mo18605a(float f) {
            this.f4894a = f;
            return this;
        }

        /* renamed from: b */
        public final C2928a mo18607b(float f) {
            this.f4895b = f;
            return this;
        }

        /* renamed from: a */
        public final StreetViewPanoramaOrientation mo18606a() {
            return new StreetViewPanoramaOrientation(this.f4895b, this.f4894a);
        }
    }

    public StreetViewPanoramaOrientation(float f, float f2) {
        boolean z = -90.0f <= f && f <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f);
        C2258v.m5637a(z, sb.toString());
        this.f4892P = f + 0.0f;
        this.f4893Q = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaOrientation)) {
            return false;
        }
        StreetViewPanoramaOrientation streetViewPanoramaOrientation = (StreetViewPanoramaOrientation) obj;
        return Float.floatToIntBits(this.f4892P) == Float.floatToIntBits(streetViewPanoramaOrientation.f4892P) && Float.floatToIntBits(this.f4893Q) == Float.floatToIntBits(streetViewPanoramaOrientation.f4893Q);
    }

    public int hashCode() {
        return C2251t.m5615a(Float.valueOf(this.f4892P), Float.valueOf(this.f4893Q));
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("tilt", Float.valueOf(this.f4892P));
        a.mo17037a("bearing", Float.valueOf(this.f4893Q));
        return a.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5590a(parcel, 2, this.f4892P);
        C2250b.m5590a(parcel, 3, this.f4893Q);
        C2250b.m5587a(parcel, a);
    }
}
