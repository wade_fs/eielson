package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2595l4.C2596a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.android.gms.internal.measurement.l4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2595l4<MessageType extends C2595l4<MessageType, BuilderType>, BuilderType extends C2596a<MessageType, BuilderType>> extends C2766w2<MessageType, BuilderType> {
    private static Map<Object, C2595l4<?, ?>> zzd = new ConcurrentHashMap();
    protected C2453c7 zzb = C2453c7.m6143d();
    private int zzc = -1;

    /* renamed from: com.google.android.gms.internal.measurement.l4$b */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static abstract class C2597b<MessageType extends C2597b<MessageType, BuilderType>, BuilderType> extends C2595l4<MessageType, BuilderType> implements C2769w5 {
        protected C2434b4<C2600e> zzc = C2434b4.m6069g();

        /* access modifiers changed from: package-private */
        /* renamed from: n */
        public final C2434b4<C2600e> mo17681n() {
            if (this.zzc.mo17312b()) {
                this.zzc = (C2434b4) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.l4$c */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static class C2598c<T extends C2595l4<T, ?>> extends C2782x2<T> {
        public C2598c(T t) {
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.l4$d */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static class C2599d<ContainingType extends C2739u5, Type> extends C2767w3<ContainingType, Type> {
    }

    /* renamed from: com.google.android.gms.internal.measurement.l4$e */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2600e implements C2466d4<C2600e> {
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        /* renamed from: e */
        public final C2679q7 mo17413e() {
            throw new NoSuchMethodError();
        }

        /* renamed from: f */
        public final C2787x7 mo17414f() {
            throw new NoSuchMethodError();
        }

        /* renamed from: g */
        public final boolean mo17415g() {
            throw new NoSuchMethodError();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.google.android.gms.internal.measurement.l4$f */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static final class C2601f {

        /* renamed from: a */
        public static final int f4290a = 1;

        /* renamed from: b */
        public static final int f4291b = 2;

        /* renamed from: c */
        public static final int f4292c = 3;

        /* renamed from: d */
        public static final int f4293d = 4;

        /* renamed from: e */
        public static final int f4294e = 5;

        /* renamed from: f */
        public static final int f4295f = 6;

        /* renamed from: g */
        public static final int f4296g = 7;

        /* renamed from: h */
        private static final /* synthetic */ int[] f4297h = {f4290a, f4291b, f4292c, f4293d, f4294e, f4295f, f4296g};

        /* renamed from: i */
        public static final int f4298i = 1;

        /* renamed from: j */
        public static final int f4299j = 2;

        /* renamed from: k */
        public static final int f4300k = 1;

        /* renamed from: l */
        public static final int f4301l = 2;

        static {
            int[] iArr = {f4298i, f4299j};
            int[] iArr2 = {f4300k, f4301l};
        }

        /* renamed from: a */
        public static int[] m6677a() {
            return (int[]) f4297h.clone();
        }
    }

    /* renamed from: k */
    protected static C2706s4 m6647k() {
        return C2616m4.m6772g();
    }

    /* renamed from: l */
    protected static C2691r4 m6648l() {
        return C2549i5.m6476g();
    }

    /* renamed from: m */
    protected static <E> C2738u4<E> m6649m() {
        return C2535h6.m6449g();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Object mo17247a(int i, Object obj, Object obj2);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17659a(int i) {
        this.zzc = i;
    }

    /* renamed from: b */
    public final /* synthetic */ C2785x5 mo17661b() {
        C2596a aVar = (C2596a) mo17247a(C2601f.f4294e, (Object) null, (Object) null);
        aVar.mo17672a(this);
        return aVar;
    }

    /* renamed from: d */
    public final /* synthetic */ C2785x5 mo17662d() {
        return (C2596a) mo17247a(C2601f.f4294e, (Object) null, (Object) null);
    }

    /* renamed from: e */
    public final int mo17663e() {
        if (this.zzc == -1) {
            this.zzc = C2550i6.m6481a().mo17582a(this).mo17284d(this);
        }
        return this.zzc;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return C2550i6.m6481a().mo17582a(this).mo17280a(this, (C2595l4) obj);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public final int mo17665g() {
        return this.zzc;
    }

    /* renamed from: h */
    public final boolean mo17666h() {
        return m6646a(this, Boolean.TRUE.booleanValue());
    }

    public int hashCode() {
        int i = super.zza;
        if (i != 0) {
            return i;
        }
        super.zza = C2550i6.m6481a().mo17582a(this).mo17276a(this);
        return super.zza;
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public final <MessageType extends C2595l4<MessageType, BuilderType>, BuilderType extends C2596a<MessageType, BuilderType>> BuilderType mo17668i() {
        return (C2596a) mo17247a(C2601f.f4294e, (Object) null, (Object) null);
    }

    /* renamed from: j */
    public final BuilderType mo17669j() {
        BuilderType buildertype = (C2596a) mo17247a(C2601f.f4294e, (Object) null, (Object) null);
        buildertype.mo17672a(this);
        return buildertype;
    }

    public String toString() {
        return C2816z5.m7924a(this, super.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
     arg types: [com.google.android.gms.internal.measurement.l4, com.google.android.gms.internal.measurement.v3]
     candidates:
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void */
    /* renamed from: a */
    public final void mo17660a(C2720t3 t3Var) {
        C2550i6.m6481a().mo17582a(this).mo17278a((Object) this, (C2771w7) C2752v3.m7403a(t3Var));
    }

    /* renamed from: com.google.android.gms.internal.measurement.l4$a */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static abstract class C2596a<MessageType extends C2595l4<MessageType, BuilderType>, BuilderType extends C2596a<MessageType, BuilderType>> extends C2751v2<MessageType, BuilderType> {

        /* renamed from: P */
        private final MessageType f4287P;

        /* renamed from: Q */
        protected MessageType f4288Q;

        /* renamed from: R */
        protected boolean f4289R = false;

        protected C2596a(MessageType messagetype) {
            this.f4287P = messagetype;
            this.f4288Q = (C2595l4) messagetype.mo17247a(C2601f.f4293d, null, null);
        }

        /* renamed from: b */
        private final BuilderType m6662b(byte[] bArr, int i, int i2, C2798y3 y3Var) {
            if (this.f4289R) {
                mo17676f();
                this.f4289R = false;
            }
            try {
                C2550i6.m6481a().mo17582a((Object) this.f4288Q).mo17279a(this.f4288Q, bArr, 0, i2 + 0, new C2417a3(y3Var));
                return this;
            } catch (C2723t4 e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw C2723t4.m7311a();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* renamed from: a */
        public final BuilderType mo17672a(MessageType messagetype) {
            if (this.f4289R) {
                mo17676f();
                this.f4289R = false;
            }
            m6661a(this.f4288Q, messagetype);
            return this;
        }

        public /* synthetic */ Object clone() {
            C2596a aVar = (C2596a) this.f4287P.mo17247a(C2601f.f4294e, null, null);
            aVar.mo17672a((C2595l4) mo17680r());
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public void mo17676f() {
            MessageType messagetype = (C2595l4) this.f4288Q.mo17247a(C2601f.f4293d, null, null);
            m6661a(messagetype, this.f4288Q);
            this.f4288Q = messagetype;
        }

        /* renamed from: g */
        public MessageType mo17680r() {
            if (this.f4289R) {
                return this.f4288Q;
            }
            MessageType messagetype = this.f4288Q;
            C2550i6.m6481a().mo17582a((Object) messagetype).mo17283c(messagetype);
            this.f4289R = true;
            return this.f4288Q;
        }

        /* renamed from: h */
        public final MessageType mo17679i() {
            MessageType messagetype = (C2595l4) mo17680r();
            if (messagetype.mo17666h()) {
                return messagetype;
            }
            throw new C2421a7(messagetype);
        }

        /* renamed from: a */
        private static void m6661a(MessageType messagetype, MessageType messagetype2) {
            C2550i6.m6481a().mo17582a((Object) messagetype).mo17281b(messagetype, messagetype2);
        }

        /* renamed from: a */
        public final /* synthetic */ C2751v2 mo17674a(byte[] bArr, int i, int i2, C2798y3 y3Var) {
            m6662b(bArr, 0, i2, y3Var);
            return super;
        }

        /* renamed from: a */
        public final /* synthetic */ C2751v2 mo17673a(byte[] bArr, int i, int i2) {
            m6662b(bArr, 0, i2, C2798y3.m7828a());
            return super;
        }

        /* renamed from: a */
        public final /* synthetic */ C2739u5 mo17658a() {
            return this.f4287P;
        }
    }

    /* renamed from: a */
    static <T extends C2595l4<?, ?>> T m6640a(Class<T> cls) {
        T t = (C2595l4) zzd.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (C2595l4) zzd.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (C2595l4) ((C2595l4) C2566j7.m6515a(cls)).mo17247a(C2601f.f4295f, (Object) null, (Object) null);
            if (t != null) {
                zzd.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    /* renamed from: a */
    protected static <T extends C2595l4<?, ?>> void m6645a(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    /* renamed from: a */
    protected static Object m6643a(C2739u5 u5Var, String str, Object[] objArr) {
        return new C2565j6(u5Var, str, objArr);
    }

    /* renamed from: a */
    static Object m6644a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /* renamed from: a */
    protected static final <T extends C2595l4<T, ?>> boolean m6646a(T t, boolean z) {
        byte byteValue = ((Byte) t.mo17247a(C2601f.f4290a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean b = C2550i6.m6481a().mo17582a((Object) t).mo17282b(t);
        if (z) {
            t.mo17247a(C2601f.f4291b, b ? t : null, null);
        }
        return b;
    }

    /* renamed from: a */
    protected static C2691r4 m6641a(C2691r4 r4Var) {
        int size = r4Var.size();
        return r4Var.mo17574c(size == 0 ? 10 : size << 1);
    }

    /* renamed from: a */
    protected static <E> C2738u4<E> m6642a(C2738u4<E> u4Var) {
        int size = u4Var.size();
        return u4Var.mo17320a(size == 0 ? 10 : size << 1);
    }

    /* renamed from: a */
    public final /* synthetic */ C2739u5 mo17658a() {
        return (C2595l4) mo17247a(C2601f.f4295f, (Object) null, (Object) null);
    }
}
