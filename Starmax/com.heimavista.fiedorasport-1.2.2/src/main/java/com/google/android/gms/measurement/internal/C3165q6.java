package com.google.android.gms.measurement.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.measurement.internal.q6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3165q6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f5566P;

    /* renamed from: Q */
    private final /* synthetic */ String f5567Q;

    /* renamed from: R */
    private final /* synthetic */ long f5568R;

    /* renamed from: S */
    private final /* synthetic */ Bundle f5569S;

    /* renamed from: T */
    private final /* synthetic */ boolean f5570T;

    /* renamed from: U */
    private final /* synthetic */ boolean f5571U;

    /* renamed from: V */
    private final /* synthetic */ boolean f5572V;

    /* renamed from: W */
    private final /* synthetic */ String f5573W;

    /* renamed from: X */
    private final /* synthetic */ C3154p6 f5574X;

    C3165q6(C3154p6 p6Var, String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.f5574X = p6Var;
        this.f5566P = str;
        this.f5567Q = str2;
        this.f5568R = j;
        this.f5569S = bundle;
        this.f5570T = z;
        this.f5571U = z2;
        this.f5572V = z3;
        this.f5573W = str3;
    }

    public final void run() {
        this.f5574X.mo19266a(this.f5566P, this.f5567Q, this.f5568R, this.f5569S, this.f5570T, this.f5571U, this.f5572V, this.f5573W);
    }
}
