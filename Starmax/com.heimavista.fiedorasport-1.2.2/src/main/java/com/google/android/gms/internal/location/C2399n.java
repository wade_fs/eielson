package com.google.android.gms.internal.location;

import android.location.Location;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.location.C2835f;
import com.google.android.gms.location.C2850m0;

/* renamed from: com.google.android.gms.internal.location.n */
final class C2399n extends C2850m0 {

    /* renamed from: a */
    private final C2084i<C2835f> f3929a;

    public final synchronized void onLocationChanged(Location location) {
        this.f3929a.mo16720a(new C2400o(this, location));
    }
}
