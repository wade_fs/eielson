package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

/* renamed from: com.google.android.gms.auth.api.signin.internal.b */
public class C1989b {

    /* renamed from: c */
    private static final Lock f3140c = new ReentrantLock();

    /* renamed from: d */
    private static C1989b f3141d;

    /* renamed from: a */
    private final Lock f3142a = new ReentrantLock();

    /* renamed from: b */
    private final SharedPreferences f3143b;

    private C1989b(Context context) {
        this.f3143b = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    /* renamed from: a */
    public static C1989b m4567a(Context context) {
        C2258v.m5629a(context);
        f3140c.lock();
        try {
            if (f3141d == null) {
                f3141d = new C1989b(context.getApplicationContext());
            }
            return f3141d;
        } finally {
            f3140c.unlock();
        }
    }

    /* renamed from: b */
    public GoogleSignInAccount mo16438b() {
        return m4566a(m4571c("defaultGoogleSignInAccount"));
    }

    /* renamed from: c */
    public GoogleSignInOptions mo16439c() {
        return m4569b(m4571c("defaultGoogleSignInAccount"));
    }

    /* renamed from: d */
    public String mo16440d() {
        return m4571c("refreshToken");
    }

    /* renamed from: e */
    public final void mo16441e() {
        String c = m4571c("defaultGoogleSignInAccount");
        m4572d("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(c)) {
            m4572d(m4570b("googleSignInAccount", c));
            m4572d(m4570b("googleSignInOptions", c));
        }
    }

    /* renamed from: d */
    private final void m4572d(String str) {
        this.f3142a.lock();
        try {
            this.f3143b.edit().remove(str).apply();
        } finally {
            this.f3142a.unlock();
        }
    }

    /* renamed from: b */
    private final GoogleSignInOptions m4569b(String str) {
        String c;
        if (!TextUtils.isEmpty(str) && (c = m4571c(m4570b("googleSignInOptions", str))) != null) {
            try {
                return GoogleSignInOptions.m4524b(c);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    /* renamed from: c */
    private final String m4571c(String str) {
        this.f3142a.lock();
        try {
            return this.f3143b.getString(str, null);
        } finally {
            this.f3142a.unlock();
        }
    }

    /* renamed from: b */
    private static String m4570b(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    /* renamed from: a */
    public void mo16437a(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        C2258v.m5629a(googleSignInAccount);
        C2258v.m5629a(googleSignInOptions);
        m4568a("defaultGoogleSignInAccount", googleSignInAccount.mo16382C());
        C2258v.m5629a(googleSignInAccount);
        C2258v.m5629a(googleSignInOptions);
        String C = googleSignInAccount.mo16382C();
        m4568a(m4570b("googleSignInAccount", C), googleSignInAccount.mo16383D());
        m4568a(m4570b("googleSignInOptions", C), googleSignInOptions.mo16405z());
    }

    /* renamed from: a */
    private final void m4568a(String str, String str2) {
        this.f3142a.lock();
        try {
            this.f3143b.edit().putString(str, str2).apply();
        } finally {
            this.f3142a.unlock();
        }
    }

    /* renamed from: a */
    private final GoogleSignInAccount m4566a(String str) {
        String c;
        if (!TextUtils.isEmpty(str) && (c = m4571c(m4570b("googleSignInAccount", str))) != null) {
            try {
                return GoogleSignInAccount.m4508b(c);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo16436a() {
        this.f3142a.lock();
        try {
            this.f3143b.edit().clear().apply();
        } finally {
            this.f3142a.unlock();
        }
    }
}
