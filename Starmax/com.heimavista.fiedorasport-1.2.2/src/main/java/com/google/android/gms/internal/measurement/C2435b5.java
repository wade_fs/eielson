package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.b5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2435b5 extends C2813z2<String> implements C2484e5, RandomAccess {

    /* renamed from: R */
    private static final C2435b5 f3999R;

    /* renamed from: Q */
    private final List<Object> f4000Q;

    static {
        C2435b5 b5Var = new C2435b5();
        f3999R = b5Var;
        super.mo17939e();
    }

    public C2435b5() {
        this(10);
    }

    /* renamed from: a */
    public final void mo17321a(C2498f3 f3Var) {
        mo18179b();
        this.f4000Q.add(f3Var);
        this.modCount++;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        mo18179b();
        this.f4000Q.add(i, (String) obj);
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    /* renamed from: b */
    public final Object mo17325b(int i) {
        return this.f4000Q.get(i);
    }

    public final void clear() {
        mo18179b();
        this.f4000Q.clear();
        this.modCount++;
    }

    /* renamed from: g */
    public final List<?> mo17327g() {
        return Collections.unmodifiableList(this.f4000Q);
    }

    public final /* synthetic */ Object get(int i) {
        Object obj = this.f4000Q.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof C2498f3) {
            C2498f3 f3Var = (C2498f3) obj;
            String e = f3Var.mo17475e();
            if (f3Var.mo17477f()) {
                this.f4000Q.set(i, e);
            }
            return e;
        }
        byte[] bArr = (byte[]) obj;
        String b = C2647o4.m6965b(bArr);
        if (C2647o4.m6964a(bArr)) {
            this.f4000Q.set(i, b);
        }
        return b;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        mo18179b();
        return m6077a(this.f4000Q.set(i, (String) obj));
    }

    public final int size() {
        return this.f4000Q.size();
    }

    /* renamed from: t */
    public final C2484e5 mo17332t() {
        return mo17938a() ? new C2486e7(this) : this;
    }

    public C2435b5(int i) {
        this(new ArrayList(i));
    }

    public final boolean addAll(int i, Collection<? extends String> collection) {
        mo18179b();
        if (collection instanceof C2484e5) {
            collection = ((C2484e5) collection).mo17327g();
        }
        boolean addAll = this.f4000Q.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        Object remove = this.f4000Q.remove(i);
        this.modCount++;
        return m6077a(remove);
    }

    private C2435b5(ArrayList<Object> arrayList) {
        this.f4000Q = arrayList;
    }

    /* renamed from: a */
    private static String m6077a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof C2498f3) {
            return ((C2498f3) obj).mo17475e();
        }
        return C2647o4.m6965b((byte[]) obj);
    }

    /* renamed from: a */
    public final /* synthetic */ C2738u4 mo17320a(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f4000Q);
            return new C2435b5(arrayList);
        }
        throw new IllegalArgumentException();
    }
}
