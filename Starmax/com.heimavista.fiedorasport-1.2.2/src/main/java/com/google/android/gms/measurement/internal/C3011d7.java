package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.d7 */
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
final class C3011d7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2589kc f5050P;

    /* renamed from: Q */
    private final /* synthetic */ AppMeasurementDynamiteService f5051Q;

    C3011d7(AppMeasurementDynamiteService appMeasurementDynamiteService, C2589kc kcVar) {
        this.f5051Q = appMeasurementDynamiteService;
        this.f5050P = kcVar;
    }

    public final void run() {
        this.f5051Q.f4930a.mo19079F().mo19375a(this.f5050P);
    }
}
