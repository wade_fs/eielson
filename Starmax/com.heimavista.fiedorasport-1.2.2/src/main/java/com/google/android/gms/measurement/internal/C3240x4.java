package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.MainThread;
import com.google.android.gms.internal.measurement.C2534h5;
import com.google.android.gms.internal.measurement.C2561j2;

/* renamed from: com.google.android.gms.measurement.internal.x4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3240x4 implements ServiceConnection {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final String f5749P;

    /* renamed from: Q */
    final /* synthetic */ C3251y4 f5750Q;

    C3240x4(C3251y4 y4Var, String str) {
        this.f5750Q = y4Var;
        this.f5749P = str;
    }

    @MainThread
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.f5750Q.f5768a.mo19015l().mo19004w().mo19042a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            C2561j2 a = C2534h5.m6446a(iBinder);
            if (a == null) {
                this.f5750Q.f5768a.mo19015l().mo19004w().mo19042a("Install Referrer Service implementation was not found");
                return;
            }
            this.f5750Q.f5768a.mo19015l().mo18996B().mo19042a("Install Referrer Service connected");
            this.f5750Q.f5768a.mo19014j().mo19028a(new C2973a5(this, a, this));
        } catch (Exception e) {
            this.f5750Q.f5768a.mo19015l().mo19004w().mo19043a("Exception occurred while calling Install Referrer API", e);
        }
    }

    @MainThread
    public final void onServiceDisconnected(ComponentName componentName) {
        this.f5750Q.f5768a.mo19015l().mo18996B().mo19042a("Install Referrer Service disconnected");
    }
}
