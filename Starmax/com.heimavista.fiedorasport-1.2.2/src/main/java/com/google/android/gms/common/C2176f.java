package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.internal.C2190a1;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2318i;
import com.google.android.gms.common.util.C2323n;
import com.google.android.gms.common.util.C2326q;
import com.google.android.gms.common.util.C2333u;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.android.gms.common.f */
public class C2176f {
    @Deprecated

    /* renamed from: a */
    public static final int f3566a = 12451000;

    /* renamed from: b */
    private static boolean f3567b = false;

    /* renamed from: c */
    private static boolean f3568c = false;

    /* renamed from: d */
    static final AtomicBoolean f3569d = new AtomicBoolean();

    /* renamed from: e */
    private static final AtomicBoolean f3570e = new AtomicBoolean();

    @Deprecated
    /* renamed from: a */
    public static String m5295a(int i) {
        return ConnectionResult.m4623a(i);
    }

    @Deprecated
    /* renamed from: b */
    public static boolean m5300b(int i) {
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @Deprecated
    /* renamed from: b */
    public static boolean m5301b(Context context, int i) {
        return C2326q.m5806a(context, i);
    }

    /* renamed from: c */
    public static Context m5302c(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* renamed from: d */
    public static Resources m5304d(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.o[]):com.google.android.gms.common.o
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.v
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    /* renamed from: e */
    public static boolean m5305e(Context context) {
        if (!f3568c) {
            try {
                PackageInfo b = C2283c.m5685a(context).mo17060b("com.google.android.gms", 64);
                C2177g.m5307a(context);
                if (b == null || C2177g.m5310a(b, false) || !C2177g.m5310a(b, true)) {
                    f3567b = false;
                } else {
                    f3567b = true;
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e);
            } finally {
                f3568c = true;
            }
        }
        return f3567b || !C2318i.m5783a();
    }

    @TargetApi(18)
    /* renamed from: f */
    public static boolean m5306f(Context context) {
        Bundle applicationRestrictions;
        return C2323n.m5795d() && (applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName())) != null && ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(applicationRestrictions.getString("restricted_profile"));
    }

    @Deprecated
    /* renamed from: a */
    public static int m5293a(Context context, int i) {
        try {
            context.getResources().getString(R$string.common_google_play_services_unknown_issue);
        } catch (Throwable unused) {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName()) && !f3570e.get()) {
            int b = C2190a1.m5351b(context);
            if (b != 0) {
                int i2 = f3566a;
                if (b != i2) {
                    StringBuilder sb = new StringBuilder(320);
                    sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                    sb.append(i2);
                    sb.append(" but found ");
                    sb.append(b);
                    sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            }
        }
        return m5294a(context, !C2318i.m5786c(context) && !C2318i.m5787d(context), i);
    }

    @Deprecated
    /* renamed from: b */
    public static int m5299b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }

    @Deprecated
    /* renamed from: c */
    public static boolean m5303c(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return m5298a(context, "com.google.android.gms");
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.o[]):com.google.android.gms.common.o
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.v
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    /* renamed from: a */
    private static int m5294a(Context context, boolean z, int i) {
        C2258v.m5636a(i >= 0);
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("GooglePlayServicesUtil", "Google Play Store is missing.");
                return 9;
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            C2177g.m5307a(context);
            if (!C2177g.m5310a(packageInfo2, true)) {
                Log.w("GooglePlayServicesUtil", "Google Play services signature invalid.");
                return 9;
            } else if (z && (!C2177g.m5310a(packageInfo, true) || !packageInfo.signatures[0].equals(packageInfo2.signatures[0]))) {
                Log.w("GooglePlayServicesUtil", "Google Play Store signature invalid.");
                return 9;
            } else if (C2333u.m5824a(packageInfo2.versionCode) < C2333u.m5824a(i)) {
                int i2 = packageInfo2.versionCode;
                StringBuilder sb = new StringBuilder(77);
                sb.append("Google Play services out of date.  Requires ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                Log.w("GooglePlayServicesUtil", sb.toString());
                return 2;
            } else {
                ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
                if (applicationInfo == null) {
                    try {
                        applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", e);
                        return 1;
                    }
                }
                if (!applicationInfo.enabled) {
                    return 3;
                }
                return 0;
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 1;
        }
    }

    @TargetApi(19)
    @Deprecated
    /* renamed from: a */
    public static boolean m5297a(Context context, int i, String str) {
        return C2326q.m5807a(context, i, str);
    }

    @Deprecated
    /* renamed from: a */
    public static void m5296a(Context context) {
        if (!f3569d.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(10436);
                }
            } catch (SecurityException unused) {
            }
        }
    }

    @TargetApi(21)
    /* renamed from: a */
    static boolean m5298a(Context context, String str) {
        boolean equals = str.equals("com.google.android.gms");
        if (C2323n.m5798g()) {
            try {
                for (PackageInstaller.SessionInfo sessionInfo : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(sessionInfo.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            return applicationInfo.enabled && !m5306f(context);
        } catch (PackageManager.NameNotFoundException unused2) {
        }
    }
}
