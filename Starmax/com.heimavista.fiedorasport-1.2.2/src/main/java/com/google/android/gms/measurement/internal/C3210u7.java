package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.u7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3210u7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5683P;

    /* renamed from: Q */
    private final /* synthetic */ long f5684Q;

    /* renamed from: R */
    private final /* synthetic */ C3188s7 f5685R;

    /* renamed from: S */
    private final /* synthetic */ C3188s7 f5686S;

    /* renamed from: T */
    private final /* synthetic */ C3177r7 f5687T;

    C3210u7(C3177r7 r7Var, boolean z, long j, C3188s7 s7Var, C3188s7 s7Var2) {
        this.f5687T = r7Var;
        this.f5683P = z;
        this.f5684Q = j;
        this.f5685R = s7Var;
        this.f5686S = s7Var2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.s7, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005d, code lost:
        if (com.google.android.gms.measurement.internal.C3267z9.m9428c(r10.f5685R.f5638a, r10.f5686S.f5638a) != false) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.la r0 = r0.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.C3135o.f5430Y
            boolean r0 = r0.mo19146a(r1)
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0029
            boolean r0 = r10.f5683P
            if (r0 == 0) goto L_0x001c
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.s7 r0 = r0.f5590c
            if (r0 == 0) goto L_0x001c
            r0 = 1
            goto L_0x001d
        L_0x001c:
            r0 = 0
        L_0x001d:
            if (r0 == 0) goto L_0x0039
            com.google.android.gms.measurement.internal.r7 r3 = r10.f5687T
            com.google.android.gms.measurement.internal.s7 r4 = r3.f5590c
            long r5 = r10.f5684Q
            r3.m9193a(r4, r2, r5)
            goto L_0x0039
        L_0x0029:
            boolean r0 = r10.f5683P
            if (r0 == 0) goto L_0x0038
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.s7 r3 = r0.f5590c
            if (r3 == 0) goto L_0x0038
            long r4 = r10.f5684Q
            r0.m9193a(r3, r2, r4)
        L_0x0038:
            r0 = 0
        L_0x0039:
            com.google.android.gms.measurement.internal.s7 r3 = r10.f5685R
            if (r3 == 0) goto L_0x005f
            long r4 = r3.f5640c
            com.google.android.gms.measurement.internal.s7 r6 = r10.f5686S
            long r7 = r6.f5640c
            int r9 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x005f
            java.lang.String r3 = r3.f5639b
            java.lang.String r4 = r6.f5639b
            boolean r3 = com.google.android.gms.measurement.internal.C3267z9.m9428c(r3, r4)
            if (r3 == 0) goto L_0x005f
            com.google.android.gms.measurement.internal.s7 r3 = r10.f5685R
            java.lang.String r3 = r3.f5638a
            com.google.android.gms.measurement.internal.s7 r4 = r10.f5686S
            java.lang.String r4 = r4.f5638a
            boolean r3 = com.google.android.gms.measurement.internal.C3267z9.m9428c(r3, r4)
            if (r3 != 0) goto L_0x0060
        L_0x005f:
            r1 = 1
        L_0x0060:
            if (r1 == 0) goto L_0x00c3
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            com.google.android.gms.measurement.internal.s7 r3 = r10.f5686S
            com.google.android.gms.measurement.internal.C3177r7.m9192a(r3, r1, r2)
            com.google.android.gms.measurement.internal.s7 r2 = r10.f5685R
            if (r2 == 0) goto L_0x008b
            java.lang.String r2 = r2.f5638a
            if (r2 == 0) goto L_0x0079
            java.lang.String r3 = "_pn"
            r1.putString(r3, r2)
        L_0x0079:
            com.google.android.gms.measurement.internal.s7 r2 = r10.f5685R
            java.lang.String r2 = r2.f5639b
            java.lang.String r3 = "_pc"
            r1.putString(r3, r2)
            com.google.android.gms.measurement.internal.s7 r2 = r10.f5685R
            long r2 = r2.f5640c
            java.lang.String r4 = "_pi"
            r1.putLong(r4, r2)
        L_0x008b:
            com.google.android.gms.measurement.internal.r7 r2 = r10.f5687T
            com.google.android.gms.measurement.internal.la r2 = r2.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5430Y
            boolean r2 = r2.mo19146a(r3)
            if (r2 == 0) goto L_0x00b6
            if (r0 == 0) goto L_0x00b6
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.x8 r0 = r0.mo18889u()
            com.google.android.gms.measurement.internal.g9 r0 = r0.f5762e
            long r2 = r0.mo19036b()
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b6
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.z9 r0 = r0.mo19011f()
            r0.mo19432a(r1, r2)
        L_0x00b6:
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.p6 r0 = r0.mo18884m()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_vs"
            r0.mo19278b(r2, r3, r1)
        L_0x00c3:
            com.google.android.gms.measurement.internal.r7 r0 = r10.f5687T
            com.google.android.gms.measurement.internal.s7 r1 = r10.f5686S
            r0.f5590c = r1
            com.google.android.gms.measurement.internal.w7 r0 = r0.mo18886q()
            com.google.android.gms.measurement.internal.s7 r1 = r10.f5686S
            r0.mo19379a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3210u7.run():void");
    }
}
