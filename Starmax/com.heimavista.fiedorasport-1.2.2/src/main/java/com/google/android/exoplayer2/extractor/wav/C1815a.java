package com.google.android.exoplayer2.extractor.wav;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.wav.a */
/* compiled from: lambda */
public final /* synthetic */ class C1815a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1815a f2826a = new C1815a();

    private /* synthetic */ C1815a() {
    }

    public final Extractor[] createExtractors() {
        return WavExtractor.m4392a();
    }
}
