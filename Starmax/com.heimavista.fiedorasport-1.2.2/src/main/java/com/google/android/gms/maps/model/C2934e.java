package com.google.android.gms.maps.model;

import android.os.RemoteException;

/* renamed from: com.google.android.gms.maps.model.e */
public final class C2934e extends RuntimeException {
    public C2934e(RemoteException remoteException) {
        super(remoteException);
    }
}
