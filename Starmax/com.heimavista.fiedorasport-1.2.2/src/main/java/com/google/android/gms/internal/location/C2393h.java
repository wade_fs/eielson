package com.google.android.gms.internal.location;

import android.os.IBinder;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.location.h */
public final class C2393h extends C2383a implements C2392g {
    C2393h(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    /* renamed from: a */
    public final void mo17204a(zzbf zzbf) {
        Parcel H = mo17193H();
        C2409x.m5939a(H, zzbf);
        mo17194a(59, H);
    }

    /* renamed from: a */
    public final void mo17205a(zzo zzo) {
        Parcel H = mo17193H();
        C2409x.m5939a(H, zzo);
        mo17194a(75, H);
    }

    /* renamed from: n */
    public final void mo17206n(boolean z) {
        Parcel H = mo17193H();
        C2409x.m5940a(H, z);
        mo17194a(12, H);
    }
}
