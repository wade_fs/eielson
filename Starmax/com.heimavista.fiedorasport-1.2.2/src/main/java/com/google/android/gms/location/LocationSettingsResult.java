package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class LocationSettingsResult extends AbstractSafeParcelable implements C2157k {
    public static final Parcelable.Creator<LocationSettingsResult> CREATOR = new C2853o();

    /* renamed from: P */
    private final Status f4679P;

    /* renamed from: Q */
    private final LocationSettingsStates f4680Q;

    public LocationSettingsResult(Status status, LocationSettingsStates locationSettingsStates) {
        this.f4679P = status;
        this.f4680Q = locationSettingsStates;
    }

    /* renamed from: b */
    public final Status mo16419b() {
        return this.f4679P;
    }

    /* renamed from: c */
    public final LocationSettingsStates mo18241c() {
        return this.f4680Q;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Status, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.location.LocationSettingsStates, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 1, (Parcelable) mo16419b(), i, false);
        C2250b.m5596a(parcel, 2, (Parcelable) mo18241c(), i, false);
        C2250b.m5587a(parcel, a);
    }
}
