package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.h8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2537h8 implements C2593l2<C2585k8> {

    /* renamed from: Q */
    private static C2537h8 f4217Q = new C2537h8();

    /* renamed from: P */
    private final C2593l2<C2585k8> f4218P;

    private C2537h8(C2593l2<C2585k8> l2Var) {
        this.f4218P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6451b() {
        return ((C2585k8) f4217Q.mo17285a()).mo17607a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4218P.mo17285a();
    }

    public C2537h8() {
        this(C2579k2.m6601a(new C2571j8()));
    }
}
