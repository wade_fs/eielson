package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.gms.common.internal.C2194c;
import com.google.android.gms.internal.base.zak;

/* renamed from: com.google.android.gms.common.images.a */
public abstract class C2185a {

    /* renamed from: a */
    protected int f3602a;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16870a(Context context, Bitmap bitmap, boolean z) {
        C2194c.m5354a(bitmap);
        mo16872a(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16872a(Drawable drawable, boolean z, boolean z2, boolean z3);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16871a(Context context, zak zak, boolean z) {
        int i = this.f3602a;
        mo16872a(i != 0 ? context.getResources().getDrawable(i) : null, z, false, false);
    }
}
