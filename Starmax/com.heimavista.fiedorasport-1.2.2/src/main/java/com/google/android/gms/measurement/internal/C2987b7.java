package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.b7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2987b7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f4986P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f4987Q;

    C2987b7(C3154p6 p6Var, AtomicReference atomicReference) {
        this.f4987Q = p6Var;
        this.f4986P = atomicReference;
    }

    public final void run() {
        synchronized (this.f4986P) {
            try {
                this.f4986P.set(Long.valueOf(this.f4987Q.mo19013h().mo19143a(this.f4987Q.mo18885p().mo18800B(), C3135o.f5400J)));
                this.f4986P.notify();
            } catch (Throwable th) {
                this.f4986P.notify();
                throw th;
            }
        }
    }
}
