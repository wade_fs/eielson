package com.google.android.gms.internal.measurement;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.e7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2486e7 extends AbstractList<String> implements C2484e5, RandomAccess {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final C2484e5 f4074P;

    public C2486e7(C2484e5 e5Var) {
        this.f4074P = e5Var;
    }

    /* renamed from: a */
    public final void mo17321a(C2498f3 f3Var) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: b */
    public final Object mo17325b(int i) {
        return this.f4074P.mo17325b(i);
    }

    /* renamed from: g */
    public final List<?> mo17327g() {
        return this.f4074P.mo17327g();
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.f4074P.get(i);
    }

    public final Iterator<String> iterator() {
        return new C2519g7(this);
    }

    public final ListIterator<String> listIterator(int i) {
        return new C2536h7(this, i);
    }

    public final int size() {
        return this.f4074P.size();
    }

    /* renamed from: t */
    public final C2484e5 mo17332t() {
        return this;
    }
}
