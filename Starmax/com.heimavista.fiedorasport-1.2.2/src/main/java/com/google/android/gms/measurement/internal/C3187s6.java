package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.s6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3187s6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f5633P;

    /* renamed from: Q */
    private final /* synthetic */ String f5634Q;

    /* renamed from: R */
    private final /* synthetic */ Object f5635R;

    /* renamed from: S */
    private final /* synthetic */ long f5636S;

    /* renamed from: T */
    private final /* synthetic */ C3154p6 f5637T;

    C3187s6(C3154p6 p6Var, String str, String str2, Object obj, long j) {
        this.f5637T = p6Var;
        this.f5633P = str;
        this.f5634Q = str2;
        this.f5635R = obj;
        this.f5636S = j;
    }

    public final void run() {
        this.f5637T.mo19270a(this.f5633P, this.f5634Q, this.f5635R, this.f5636S);
    }
}
