package com.google.android.datatransport.cct;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1661a;
import com.google.android.datatransport.cct.p084b.C1663b;
import com.google.android.datatransport.cct.p084b.C1682k;
import com.google.android.datatransport.cct.p084b.C1684m;
import com.google.android.datatransport.cct.p084b.C1688o;
import com.google.android.datatransport.cct.p084b.C1689p;
import com.google.android.datatransport.cct.p084b.C1692r;
import com.google.android.datatransport.cct.p084b.C1695t;
import com.google.android.datatransport.cct.p084b.C1696u;
import com.google.android.datatransport.runtime.backends.C1714f;
import com.google.android.datatransport.runtime.backends.C1716g;
import com.google.android.datatransport.runtime.backends.C1724m;
import com.google.android.exoplayer2.C1750C;
import com.google.firebase.p096e.C3533a;
import com.google.firebase.p096e.C3535c;
import com.tencent.bugly.BuglyStrategy;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.p147i.C3898g;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.p148t.C3915a;
import p119e.p144d.p145a.p146a.p147i.p149u.C3917b;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.cct.e */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1703e implements C1724m {

    /* renamed from: a */
    private final C3533a f2676a = C1688o.m4221a();

    /* renamed from: b */
    private final ConnectivityManager f2677b;

    /* renamed from: c */
    final URL f2678c;

    /* renamed from: d */
    private final C3971a f2679d;

    /* renamed from: e */
    private final C3971a f2680e;

    /* renamed from: f */
    private final int f2681f;

    /* renamed from: com.google.android.datatransport.cct.e$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1704a {

        /* renamed from: a */
        final URL f2682a;

        /* renamed from: b */
        final C1682k f2683b;
        @Nullable

        /* renamed from: c */
        final String f2684c;

        C1704a(URL url, C1682k kVar, @Nullable String str) {
            this.f2682a = url;
            this.f2683b = kVar;
            this.f2684c = str;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1704a mo13532a(URL url) {
            return new C1704a(url, this.f2683b, this.f2684c);
        }
    }

    /* renamed from: com.google.android.datatransport.cct.e$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1705b {

        /* renamed from: a */
        final int f2685a;
        @Nullable

        /* renamed from: b */
        final URL f2686b;

        /* renamed from: c */
        final long f2687c;

        C1705b(int i, @Nullable URL url, long j) {
            this.f2685a = i;
            this.f2686b = url;
            this.f2687c = j;
        }
    }

    C1703e(Context context, C3971a aVar, C3971a aVar2) {
        this.f2677b = (ConnectivityManager) context.getSystemService("connectivity");
        this.f2678c = m4265a(C1660a.f2560c);
        this.f2679d = aVar2;
        this.f2680e = aVar;
        this.f2681f = 40000;
    }

    /* renamed from: a */
    public C3899h mo13531a(C3899h hVar) {
        int i;
        int i2;
        NetworkInfo activeNetworkInfo = this.f2677b.getActiveNetworkInfo();
        C3899h.C3900a h = hVar.mo23553h();
        h.mo23554a("sdk-version", Build.VERSION.SDK_INT);
        h.mo23556a("model", Build.MODEL);
        h.mo23556a("hardware", Build.HARDWARE);
        h.mo23556a("device", Build.DEVICE);
        h.mo23556a("product", Build.PRODUCT);
        h.mo23556a("os-uild", Build.ID);
        h.mo23556a("manufacturer", Build.MANUFACTURER);
        h.mo23556a("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        h.mo23555a("tz-offset", (long) (TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        if (activeNetworkInfo == null) {
            i = C1696u.C1699c.f2671i0.mo13527a();
        } else {
            i = activeNetworkInfo.getType();
        }
        h.mo23554a("net-type", i);
        if (activeNetworkInfo == null) {
            i2 = C1696u.C1698b.f2630Q.mo13526a();
        } else {
            i2 = activeNetworkInfo.getSubtype();
            if (i2 == -1) {
                i2 = C1696u.C1698b.f2650k0.mo13526a();
            } else if (C1696u.C1698b.m4254a(i2) == null) {
                i2 = 0;
            }
        }
        h.mo23554a("mobile-subtype", i2);
        return h.mo23514a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.a.i.t.a.a(java.lang.String, java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.String, java.io.IOException]
     candidates:
      e.d.a.a.i.t.a.a(java.lang.String, java.lang.String, java.lang.Object):void
      e.d.a.a.i.t.a.a(java.lang.String, java.lang.String, java.lang.Object[]):void
      e.d.a.a.i.t.a.a(java.lang.String, java.lang.String, java.lang.Throwable):void */
    /* renamed from: a */
    public C1716g mo13530a(C1714f fVar) {
        C1689p.C1690a aVar;
        HashMap hashMap = new HashMap();
        for (C3899h hVar : fVar.mo13534a()) {
            String f = hVar.mo23505f();
            if (!hashMap.containsKey(f)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(hVar);
                hashMap.put(f, arrayList);
            } else {
                ((List) hashMap.get(f)).add(hVar);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry entry : hashMap.entrySet()) {
            C3899h hVar2 = (C3899h) ((List) entry.getValue()).get(0);
            C1692r.C1693a a = C1692r.m4236a();
            a.mo13506a(C1663b.f2567P);
            a.mo13505a(this.f2680e.mo23604a());
            a.mo13511b(this.f2679d.mo23604a());
            C1684m.C1685a a2 = C1684m.m4216a();
            a2.mo13475a(C1684m.C1686b.f2629Q);
            C1661a.C1662a a3 = C1661a.m4144a();
            a3.mo13444a(hVar2.mo23550b("sdk-version"));
            a3.mo13450e(hVar2.mo23549a("model"));
            a3.mo13448c(hVar2.mo23549a("hardware"));
            a3.mo13445a(hVar2.mo23549a("device"));
            a3.mo13452g(hVar2.mo23549a("product"));
            a3.mo13451f(hVar2.mo23549a("os-uild"));
            a3.mo13449d(hVar2.mo23549a("manufacturer"));
            a3.mo13447b(hVar2.mo23549a("fingerprint"));
            a2.mo13474a(a3.mo13446a());
            a.mo13507a(a2.mo13476a());
            try {
                a.mo13524b(Integer.valueOf((String) entry.getKey()).intValue());
            } catch (NumberFormatException unused) {
                a.mo13525b((String) entry.getKey());
            }
            ArrayList arrayList3 = new ArrayList();
            for (C3899h hVar3 : (List) entry.getValue()) {
                C3898g c = hVar3.mo23502c();
                C3877b b = c.mo23545b();
                if (b.equals(C3877b.m11673a("proto"))) {
                    aVar = C1689p.m4223a(c.mo23544a());
                } else if (b.equals(C3877b.m11673a("json"))) {
                    aVar = C1689p.m4222a(new String(c.mo23544a(), Charset.forName(C1750C.UTF8_NAME)));
                } else {
                    C3915a.m11795b("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", b);
                }
                aVar.mo13488a(hVar3.mo23503d());
                aVar.mo13493b(hVar3.mo23506g());
                aVar.mo13494c(hVar3.mo23551c("tz-offset"));
                C1696u.C1697a a4 = C1696u.m4250a();
                a4.mo13522a(C1696u.C1699c.m4256a(hVar3.mo23550b("net-type")));
                a4.mo13521a(C1696u.C1698b.m4254a(hVar3.mo23550b("mobile-subtype")));
                aVar.mo13489a(a4.mo13523a());
                if (hVar3.mo23501b() != null) {
                    aVar.mo13487a(hVar3.mo23501b().intValue());
                }
                arrayList3.add(aVar.mo13492a());
            }
            a.mo13509a(arrayList3);
            arrayList2.add(a.mo13510a());
        }
        C1682k a5 = C1682k.m4213a(arrayList2);
        String str = null;
        URL url = this.f2678c;
        if (fVar.mo13535b() != null) {
            try {
                C1660a a6 = C1660a.m4138a(fVar.mo13535b());
                if (a6.mo13441d() != null) {
                    str = a6.mo13441d();
                }
                if (a6.mo13442e() != null) {
                    url = m4265a(a6.mo13442e());
                }
            } catch (IllegalArgumentException unused2) {
                return C1716g.m4288c();
            }
        }
        try {
            C1705b bVar = (C1705b) C3917b.m11796a(5, new C1704a(url, a5, str), C1701c.m4259a(this), C1702d.m4260a());
            if (bVar.f2685a == 200) {
                return C1716g.m4287a(bVar.f2687c);
            }
            int i = bVar.f2685a;
            if (i < 500) {
                if (i != 404) {
                    return C1716g.m4288c();
                }
            }
            return C1716g.m4289d();
        } catch (IOException e) {
            C3915a.m11793a("CctTransportBackend", "Could not make request to the backend", (Throwable) e);
            return C1716g.m4289d();
        }
    }

    /* renamed from: a */
    private static URL m4265a(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid url: " + str, e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C1705b m4263a(C1704a aVar) {
        GZIPOutputStream gZIPOutputStream;
        InputStream inputStream;
        C3915a.m11792a("CctTransportBackend", "Making request to: %s", aVar.f2682a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) aVar.f2682a.openConnection();
        httpURLConnection.setConnectTimeout(BuglyStrategy.C4791a.MAX_USERDATA_VALUE_LENGTH);
        httpURLConnection.setReadTimeout(this.f2681f);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("User-Agent", String.format("datatransport/%s android/", "2.2.0"));
        httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        String str = aVar.f2684c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        WritableByteChannel newChannel = Channels.newChannel(httpURLConnection.getOutputStream());
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            try {
                this.f2676a.mo22169a(aVar.f2683b, new OutputStreamWriter(gZIPOutputStream));
                gZIPOutputStream.close();
                newChannel.write(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()));
                int responseCode = httpURLConnection.getResponseCode();
                StringBuilder sb = new StringBuilder();
                sb.append("Status Code: ");
                sb.append(responseCode);
                C3915a.m11791a("CctTransportBackend", sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Content-Type: ");
                sb2.append(httpURLConnection.getHeaderField("Content-Type"));
                C3915a.m11791a("CctTransportBackend", sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Content-Encoding: ");
                sb3.append(httpURLConnection.getHeaderField("Content-Encoding"));
                C3915a.m11791a("CctTransportBackend", sb3.toString());
                if (!(responseCode == 302 || responseCode == 301)) {
                    if (responseCode != 307) {
                        if (responseCode != 200) {
                            C1705b bVar = new C1705b(responseCode, null, 0);
                            newChannel.close();
                            return bVar;
                        }
                        String headerField = httpURLConnection.getHeaderField("Content-Encoding");
                        if (headerField == null || !headerField.equals("gzip")) {
                            inputStream = httpURLConnection.getInputStream();
                        } else {
                            inputStream = new GZIPInputStream(httpURLConnection.getInputStream());
                        }
                        C1705b bVar2 = new C1705b(responseCode, null, C1695t.m4248a(new InputStreamReader(inputStream)).mo13512a());
                        inputStream.close();
                        newChannel.close();
                        return bVar2;
                    }
                }
                C1705b bVar3 = new C1705b(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0);
                newChannel.close();
                return bVar3;
            } catch (C3535c | IOException e) {
                C3915a.m11793a("CctTransportBackend", "Couldn't encode request, returning with 400", e);
                C1705b bVar4 = new C1705b(400, null, 0);
                gZIPOutputStream.close();
                newChannel.close();
                return bVar4;
            }
        } catch (Throwable th) {
            newChannel.close();
            throw th;
        }
    }

    /* renamed from: a */
    static /* synthetic */ C1704a m4262a(C1704a aVar, C1705b bVar) {
        URL url = bVar.f2686b;
        if (url == null) {
            return null;
        }
        C3915a.m11792a("CctTransportBackend", "Following redirect to: %s", url);
        return aVar.mo13532a(bVar.f2686b);
    }
}
