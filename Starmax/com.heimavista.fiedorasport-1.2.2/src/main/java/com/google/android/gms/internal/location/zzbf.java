package com.google.android.gms.internal.location;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.location.C2842i0;
import com.google.android.gms.location.C2844j0;
import com.google.android.gms.location.C2848l0;
import com.google.android.gms.location.C2850m0;

public final class zzbf extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbf> CREATOR = new C2404s();

    /* renamed from: P */
    private int f3944P;

    /* renamed from: Q */
    private zzbd f3945Q;

    /* renamed from: R */
    private C2848l0 f3946R;

    /* renamed from: S */
    private PendingIntent f3947S;

    /* renamed from: T */
    private C2842i0 f3948T;

    /* renamed from: U */
    private C2389d f3949U;

    zzbf(int i, zzbd zzbd, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        this.f3944P = i;
        this.f3945Q = zzbd;
        C2389d dVar = null;
        this.f3946R = iBinder == null ? null : C2850m0.m7985a(iBinder);
        this.f3947S = pendingIntent;
        this.f3948T = iBinder2 == null ? null : C2844j0.m7982a(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            dVar = queryLocalInterface instanceof C2389d ? (C2389d) queryLocalInterface : new C2391f(iBinder3);
        }
        this.f3949U = dVar;
    }

    /* renamed from: a */
    public static zzbf m5951a(C2842i0 i0Var, @Nullable C2389d dVar) {
        return new zzbf(2, null, null, null, i0Var.asBinder(), dVar != null ? dVar.asBinder() : null);
    }

    /* renamed from: a */
    public static zzbf m5952a(C2848l0 l0Var, @Nullable C2389d dVar) {
        return new zzbf(2, null, l0Var.asBinder(), null, null, dVar != null ? dVar.asBinder() : null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.location.zzbd, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3944P);
        C2250b.m5596a(parcel, 2, (Parcelable) this.f3945Q, i, false);
        C2848l0 l0Var = this.f3946R;
        IBinder iBinder = null;
        C2250b.m5594a(parcel, 3, l0Var == null ? null : l0Var.asBinder(), false);
        C2250b.m5596a(parcel, 4, (Parcelable) this.f3947S, i, false);
        C2842i0 i0Var = this.f3948T;
        C2250b.m5594a(parcel, 5, i0Var == null ? null : i0Var.asBinder(), false);
        C2389d dVar = this.f3949U;
        if (dVar != null) {
            iBinder = dVar.asBinder();
        }
        C2250b.m5594a(parcel, 6, iBinder, false);
        C2250b.m5587a(parcel, a);
    }
}
