package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.m8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2620m8 implements C2593l2<C2605l8> {

    /* renamed from: Q */
    private static C2620m8 f4333Q = new C2620m8();

    /* renamed from: P */
    private final C2593l2<C2605l8> f4334P;

    private C2620m8(C2593l2<C2605l8> l2Var) {
        this.f4334P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: A */
    public static long m6797A() {
        return ((C2605l8) f4333Q.mo17285a()).mo17701l();
    }

    /* renamed from: B */
    public static long m6798B() {
        return ((C2605l8) f4333Q.mo17285a()).mo17690H();
    }

    /* renamed from: C */
    public static long m6799C() {
        return ((C2605l8) f4333Q.mo17285a()).mo17692J();
    }

    /* renamed from: D */
    public static long m6800D() {
        return ((C2605l8) f4333Q.mo17285a()).mo17707r();
    }

    /* renamed from: E */
    public static long m6801E() {
        return ((C2605l8) f4333Q.mo17285a()).mo17687E();
    }

    /* renamed from: F */
    public static long m6802F() {
        return ((C2605l8) f4333Q.mo17285a()).mo17713x();
    }

    /* renamed from: G */
    public static long m6803G() {
        return ((C2605l8) f4333Q.mo17285a()).mo17683A();
    }

    /* renamed from: H */
    public static long m6804H() {
        return ((C2605l8) f4333Q.mo17285a()).mo17710u();
    }

    /* renamed from: b */
    public static long m6805b() {
        return ((C2605l8) f4333Q.mo17285a()).mo17712w();
    }

    /* renamed from: c */
    public static long m6806c() {
        return ((C2605l8) f4333Q.mo17285a()).mo17689G();
    }

    /* renamed from: d */
    public static long m6807d() {
        return ((C2605l8) f4333Q.mo17285a()).mo17691I();
    }

    /* renamed from: e */
    public static long m6808e() {
        return ((C2605l8) f4333Q.mo17285a()).mo17686D();
    }

    /* renamed from: f */
    public static long m6809f() {
        return ((C2605l8) f4333Q.mo17285a()).mo17688F();
    }

    /* renamed from: g */
    public static long m6810g() {
        return ((C2605l8) f4333Q.mo17285a()).mo17714y();
    }

    /* renamed from: h */
    public static String m6811h() {
        return ((C2605l8) f4333Q.mo17285a()).mo17684B();
    }

    /* renamed from: i */
    public static long m6812i() {
        return ((C2605l8) f4333Q.mo17285a()).mo17711v();
    }

    /* renamed from: j */
    public static long m6813j() {
        return ((C2605l8) f4333Q.mo17285a()).mo17693a();
    }

    /* renamed from: k */
    public static long m6814k() {
        return ((C2605l8) f4333Q.mo17285a()).mo17694e();
    }

    /* renamed from: l */
    public static String m6815l() {
        return ((C2605l8) f4333Q.mo17285a()).mo17695f();
    }

    /* renamed from: m */
    public static String m6816m() {
        return ((C2605l8) f4333Q.mo17285a()).mo17696g();
    }

    /* renamed from: n */
    public static long m6817n() {
        return ((C2605l8) f4333Q.mo17285a()).mo17709t();
    }

    /* renamed from: o */
    public static long m6818o() {
        return ((C2605l8) f4333Q.mo17285a()).mo17697h();
    }

    /* renamed from: p */
    public static long m6819p() {
        return ((C2605l8) f4333Q.mo17285a()).mo17706q();
    }

    /* renamed from: q */
    public static long m6820q() {
        return ((C2605l8) f4333Q.mo17285a()).mo17705p();
    }

    /* renamed from: r */
    public static long m6821r() {
        return ((C2605l8) f4333Q.mo17285a()).mo17698i();
    }

    /* renamed from: s */
    public static long m6822s() {
        return ((C2605l8) f4333Q.mo17285a()).mo17700k();
    }

    /* renamed from: t */
    public static long m6823t() {
        return ((C2605l8) f4333Q.mo17285a()).mo17702m();
    }

    /* renamed from: u */
    public static long m6824u() {
        return ((C2605l8) f4333Q.mo17285a()).mo17685C();
    }

    /* renamed from: v */
    public static long m6825v() {
        return ((C2605l8) f4333Q.mo17285a()).mo17704o();
    }

    /* renamed from: w */
    public static long m6826w() {
        return ((C2605l8) f4333Q.mo17285a()).mo17703n();
    }

    /* renamed from: x */
    public static long m6827x() {
        return ((C2605l8) f4333Q.mo17285a()).mo17715z();
    }

    /* renamed from: y */
    public static long m6828y() {
        return ((C2605l8) f4333Q.mo17285a()).mo17708s();
    }

    /* renamed from: z */
    public static long m6829z() {
        return ((C2605l8) f4333Q.mo17285a()).mo17699j();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4334P.mo17285a();
    }

    public C2620m8() {
        this(C2579k2.m6601a(new C2651o8()));
    }
}
