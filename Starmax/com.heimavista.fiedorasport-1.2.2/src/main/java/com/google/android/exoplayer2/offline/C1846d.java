package com.google.android.exoplayer2.offline;

/* renamed from: com.google.android.exoplayer2.offline.d */
/* compiled from: lambda */
public final /* synthetic */ class C1846d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ DownloadManager f2845P;

    /* renamed from: Q */
    private final /* synthetic */ DownloadAction[] f2846Q;

    public /* synthetic */ C1846d(DownloadManager downloadManager, DownloadAction[] downloadActionArr) {
        this.f2845P = downloadManager;
        this.f2846Q = downloadActionArr;
    }

    public final void run() {
        this.f2845P.mo14718b(this.f2846Q);
    }
}
