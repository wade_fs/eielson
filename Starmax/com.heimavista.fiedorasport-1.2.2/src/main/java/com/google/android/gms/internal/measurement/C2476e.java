package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.e */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2476e extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4057T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4058U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2476e(C2525gd gdVar, String str) {
        super(gdVar);
        this.f4058U = gdVar;
        this.f4057T = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4058U.f4192g.beginAdUnitExposure(this.f4057T, super.f4194Q);
    }
}
