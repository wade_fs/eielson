package com.google.android.gms.maps;

import android.os.RemoteException;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.model.C2933d;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.p093i.C2891b;
import com.google.android.gms.maps.p093i.C2907l;
import com.google.android.gms.maps.p093i.C2909n;
import com.google.android.gms.maps.p093i.C2913r;
import p119e.p144d.p145a.p157c.p161c.p165d.C4043l;

/* renamed from: com.google.android.gms.maps.c */
public final class C2880c {

    /* renamed from: a */
    private final C2891b f4788a;

    /* renamed from: b */
    private C2888h f4789b;

    /* renamed from: com.google.android.gms.maps.c$a */
    public interface C2881a {
        /* renamed from: a */
        void mo18408a(LatLng latLng);
    }

    /* renamed from: com.google.android.gms.maps.c$b */
    public interface C2882b {
        /* renamed from: r */
        void mo18409r();
    }

    /* renamed from: com.google.android.gms.maps.c$c */
    public interface C2883c {
        /* renamed from: a */
        boolean mo18410a(C2932c cVar);
    }

    public C2880c(C2891b bVar) {
        C2258v.m5629a(bVar);
        this.f4788a = bVar;
    }

    /* renamed from: a */
    public final void mo18402a(C2878a aVar) {
        try {
            this.f4788a.mo18427b(aVar.mo18398a());
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: b */
    public final C2888h mo18407b() {
        try {
            if (this.f4789b == null) {
                this.f4789b = new C2888h(this.f4788a.mo18421F());
            }
            return this.f4789b;
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final C2933d mo18400a(PolylineOptions polylineOptions) {
        try {
            return new C2933d(this.f4788a.mo18423a(polylineOptions));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final C2932c mo18399a(MarkerOptions markerOptions) {
        try {
            C4043l a = this.f4788a.mo18422a(markerOptions);
            if (a != null) {
                return new C2932c(a);
            }
            return null;
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final void mo18401a() {
        try {
            this.f4788a.clear();
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"})
    /* renamed from: a */
    public final void mo18406a(boolean z) {
        try {
            this.f4788a.mo18429o(z);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final void mo18403a(@Nullable C2881a aVar) {
        if (aVar == null) {
            try {
                this.f4788a.mo18424a((C2907l) null);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        } else {
            this.f4788a.mo18424a(new C2962t(this, aVar));
        }
    }

    /* renamed from: a */
    public final void mo18405a(@Nullable C2883c cVar) {
        if (cVar == null) {
            try {
                this.f4788a.mo18426a((C2913r) null);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        } else {
            this.f4788a.mo18426a(new C2960r(this, cVar));
        }
    }

    /* renamed from: a */
    public final void mo18404a(C2882b bVar) {
        if (bVar == null) {
            try {
                this.f4788a.mo18425a((C2909n) null);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        } else {
            this.f4788a.mo18425a(new C2961s(this, bVar));
        }
    }
}
