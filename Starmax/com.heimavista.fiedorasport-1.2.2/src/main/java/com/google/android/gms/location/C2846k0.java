package com.google.android.gms.location;

import android.os.IBinder;
import com.google.android.gms.internal.location.C2383a;

/* renamed from: com.google.android.gms.location.k0 */
public final class C2846k0 extends C2383a implements C2842i0 {
    C2846k0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }
}
