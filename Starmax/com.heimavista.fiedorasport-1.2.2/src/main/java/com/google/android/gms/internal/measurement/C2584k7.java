package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.k7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2584k7 {
    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m6616b(byte b, char[] cArr, int i) {
        cArr[i] = (char) b;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static boolean m6619d(byte b) {
        return b >= 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static boolean m6620e(byte b) {
        return b < -32;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public static boolean m6621f(byte b) {
        return b < -16;
    }

    /* renamed from: g */
    private static boolean m6622g(byte b) {
        return b > -65;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m6615b(byte b, byte b2, char[] cArr, int i) {
        if (b < -62 || m6622g(b2)) {
            throw C2723t4.m7316i();
        }
        cArr[i] = (char) (((b & 31) << 6) | (b2 & 63));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m6614b(byte b, byte b2, byte b3, char[] cArr, int i) {
        if (m6622g(b2) || ((b == -32 && b2 < -96) || ((b == -19 && b2 >= -96) || m6622g(b3)))) {
            throw C2723t4.m7316i();
        }
        cArr[i] = (char) (((b & 15) << 12) | ((b2 & 63) << 6) | (b3 & 63));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m6613b(byte b, byte b2, byte b3, byte b4, char[] cArr, int i) {
        if (m6622g(b2) || (((b << 28) + (b2 + 112)) >> 30) != 0 || m6622g(b3) || m6622g(b4)) {
            throw C2723t4.m7316i();
        }
        byte b5 = ((b & 7) << 18) | ((b2 & 63) << 12) | ((b3 & 63) << 6) | (b4 & 63);
        cArr[i] = (char) ((b5 >>> 10) + 55232);
        cArr[i + 1] = (char) ((b5 & 1023) + 56320);
    }
}
