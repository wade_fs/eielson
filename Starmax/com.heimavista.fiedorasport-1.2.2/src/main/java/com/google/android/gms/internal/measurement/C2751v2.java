package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2751v2;
import com.google.android.gms.internal.measurement.C2766w2;

/* renamed from: com.google.android.gms.internal.measurement.v2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2751v2<MessageType extends C2766w2<MessageType, BuilderType>, BuilderType extends C2751v2<MessageType, BuilderType>> implements C2785x5 {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract BuilderType mo17672a(C2766w2 w2Var);

    /* renamed from: a */
    public abstract BuilderType mo17673a(byte[] bArr, int i, int i2);

    /* renamed from: a */
    public abstract BuilderType mo17674a(byte[] bArr, int i, int i2, C2798y3 y3Var);

    /* renamed from: a */
    public final /* synthetic */ C2785x5 mo17951a(C2739u5 u5Var) {
        if (mo17658a().getClass().isInstance(u5Var)) {
            return mo17672a((C2766w2) u5Var);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    /* renamed from: a */
    public final /* synthetic */ C2785x5 mo17953a(byte[] bArr, C2798y3 y3Var) {
        mo17674a(bArr, 0, bArr.length, y3Var);
        return this;
    }

    /* renamed from: a */
    public final /* synthetic */ C2785x5 mo17952a(byte[] bArr) {
        mo17673a(bArr, 0, bArr.length);
        return this;
    }
}
