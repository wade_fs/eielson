package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.SimpleArrayMap;
import com.google.android.gms.base.R$string;
import com.google.android.gms.common.C2175e;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2318i;

/* renamed from: com.google.android.gms.common.internal.f */
public final class C2215f {

    /* renamed from: a */
    private static final SimpleArrayMap<String, String> f3709a = new SimpleArrayMap<>();

    @NonNull
    /* renamed from: a */
    public static String m5463a(Context context, int i) {
        Resources resources = context.getResources();
        if (i == 1) {
            return resources.getString(R$string.common_google_play_services_install_button);
        }
        if (i == 2) {
            return resources.getString(R$string.common_google_play_services_update_button);
        }
        if (i != 3) {
            return resources.getString(17039370);
        }
        return resources.getString(R$string.common_google_play_services_enable_button);
    }

    @NonNull
    /* renamed from: b */
    public static String m5467b(Context context, int i) {
        Resources resources = context.getResources();
        String a = m5462a(context);
        if (i == 1) {
            return resources.getString(R$string.common_google_play_services_install_text, a);
        } else if (i != 2) {
            if (i == 3) {
                return resources.getString(R$string.common_google_play_services_enable_text, a);
            } else if (i == 5) {
                return m5465a(context, "common_google_play_services_invalid_account_text", a);
            } else {
                if (i == 7) {
                    return m5465a(context, "common_google_play_services_network_error_text", a);
                }
                if (i == 9) {
                    return resources.getString(R$string.common_google_play_services_unsupported_text, a);
                } else if (i == 20) {
                    return m5465a(context, "common_google_play_services_restricted_profile_text", a);
                } else {
                    switch (i) {
                        case 16:
                            return m5465a(context, "common_google_play_services_api_unavailable_text", a);
                        case 17:
                            return m5465a(context, "common_google_play_services_sign_in_failed_text", a);
                        case 18:
                            return resources.getString(R$string.common_google_play_services_updating_text, a);
                        default:
                            return resources.getString(com.google.android.gms.common.R$string.common_google_play_services_unknown_issue, a);
                    }
                }
            }
        } else if (C2318i.m5786c(context)) {
            return resources.getString(R$string.common_google_play_services_wear_update_text);
        } else {
            return resources.getString(R$string.common_google_play_services_update_text, a);
        }
    }

    @NonNull
    /* renamed from: c */
    public static String m5468c(Context context, int i) {
        if (i == 6) {
            return m5465a(context, "common_google_play_services_resolution_required_text", m5462a(context));
        }
        return m5467b(context, i);
    }

    @NonNull
    /* renamed from: d */
    public static String m5469d(Context context, int i) {
        String str;
        if (i == 6) {
            str = m5464a(context, "common_google_play_services_resolution_required_title");
        } else {
            str = m5470e(context, i);
        }
        return str == null ? context.getResources().getString(R$string.common_google_play_services_notification_ticker) : str;
    }

    @Nullable
    /* renamed from: e */
    public static String m5470e(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                return resources.getString(R$string.common_google_play_services_install_title);
            case 2:
                return resources.getString(R$string.common_google_play_services_update_title);
            case 3:
                return resources.getString(R$string.common_google_play_services_enable_title);
            case 4:
            case 6:
            case 18:
                return null;
            case 5:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return m5464a(context, "common_google_play_services_invalid_account_title");
            case 7:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return m5464a(context, "common_google_play_services_network_error_title");
            case 8:
                Log.e("GoogleApiAvailability", "Internal error occurred. Please see logs for detailed information");
                return null;
            case 9:
                Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                return null;
            case 10:
                Log.e("GoogleApiAvailability", "Developer error occurred. Please see logs for detailed information");
                return null;
            case 11:
                Log.e("GoogleApiAvailability", "The application is not licensed to the user.");
                return null;
            case 12:
            case 13:
            case 14:
            case 15:
            case 19:
            default:
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected error code ");
                sb.append(i);
                Log.e("GoogleApiAvailability", sb.toString());
                return null;
            case 16:
                Log.e("GoogleApiAvailability", "One of the API components you attempted to connect to is not available.");
                return null;
            case 17:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return m5464a(context, "common_google_play_services_sign_in_failed_title");
            case 20:
                Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
                return m5464a(context, "common_google_play_services_restricted_profile_title");
        }
    }

    /* renamed from: a */
    public static String m5462a(Context context) {
        String packageName = context.getPackageName();
        try {
            return C2283c.m5685a(context).mo17061b(packageName).toString();
        } catch (PackageManager.NameNotFoundException | NullPointerException unused) {
            String str = context.getApplicationInfo().name;
            return TextUtils.isEmpty(str) ? packageName : str;
        }
    }

    /* renamed from: a */
    private static String m5465a(Context context, String str, String str2) {
        Resources resources = context.getResources();
        String a = m5464a(context, str);
        if (a == null) {
            a = resources.getString(com.google.android.gms.common.R$string.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, a, str2);
    }

    @Nullable
    /* renamed from: a */
    private static String m5464a(Context context, String str) {
        synchronized (f3709a) {
            String str2 = f3709a.get(str);
            if (str2 != null) {
                return str2;
            }
            Resources d = C2175e.m5292d(context);
            if (d == null) {
                return null;
            }
            int identifier = d.getIdentifier(str, "string", "com.google.android.gms");
            if (identifier == 0) {
                String valueOf = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf.length() != 0 ? "Missing resource: ".concat(valueOf) : new String("Missing resource: "));
                return null;
            }
            String string = d.getString(identifier);
            if (TextUtils.isEmpty(string)) {
                String valueOf2 = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf2.length() != 0 ? "Got empty resource: ".concat(valueOf2) : new String("Got empty resource: "));
                return null;
            }
            f3709a.put(str, string);
            return string;
        }
    }

    /* renamed from: b */
    public static String m5466b(Context context) {
        return context.getResources().getString(R$string.common_google_play_services_notification_channel_name);
    }
}
