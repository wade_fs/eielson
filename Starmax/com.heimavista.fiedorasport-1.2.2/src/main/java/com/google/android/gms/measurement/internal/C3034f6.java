package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2314e;

/* renamed from: com.google.android.gms.measurement.internal.f6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
class C3034f6 implements C3058h6 {

    /* renamed from: a */
    protected final C3081j5 f5134a;

    C3034f6(C3081j5 j5Var) {
        C2258v.m5629a(j5Var);
        this.f5134a = j5Var;
    }

    /* renamed from: a */
    public void mo18880a() {
        this.f5134a.mo19092f();
    }

    /* renamed from: b */
    public void mo19008b() {
        this.f5134a.mo19014j().mo19008b();
    }

    /* renamed from: c */
    public void mo18881c() {
        this.f5134a.mo19014j().mo18881c();
    }

    /* renamed from: d */
    public C3063i mo19009d() {
        return this.f5134a.mo19080G();
    }

    /* renamed from: e */
    public C2996c4 mo19010e() {
        return this.f5134a.mo19105x();
    }

    /* renamed from: f */
    public C3267z9 mo19011f() {
        return this.f5134a.mo19104w();
    }

    /* renamed from: g */
    public C3185s4 mo19012g() {
        return this.f5134a.mo19098p();
    }

    /* renamed from: h */
    public C3110la mo19013h() {
        return this.f5134a.mo19097m();
    }

    /* renamed from: j */
    public C3045g5 mo19014j() {
        return this.f5134a.mo19014j();
    }

    /* renamed from: l */
    public C3032f4 mo19015l() {
        return this.f5134a.mo19015l();
    }

    /* renamed from: n */
    public Context mo19016n() {
        return this.f5134a.mo19016n();
    }

    /* renamed from: o */
    public C2314e mo19017o() {
        return this.f5134a.mo19017o();
    }

    /* renamed from: r */
    public C3098ka mo19018r() {
        return this.f5134a.mo19018r();
    }
}
