package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.n8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3132n8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3228w3 f5378P;

    /* renamed from: Q */
    private final /* synthetic */ C3144o8 f5379Q;

    C3132n8(C3144o8 o8Var, C3228w3 w3Var) {
        this.f5379Q = o8Var;
        this.f5378P = w3Var;
    }

    public final void run() {
        synchronized (this.f5379Q) {
            boolean unused = this.f5379Q.f5505P = false;
            if (!this.f5379Q.f5507R.mo19368B()) {
                this.f5379Q.f5507R.mo19015l().mo18996B().mo19042a("Connected to service");
                this.f5379Q.f5507R.mo19380a(this.f5378P);
            }
        }
    }
}
