package com.google.android.gms.common.api.internal;

import androidx.annotation.WorkerThread;
import com.google.android.gms.common.api.C2016a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.common.api.internal.f0 */
final class C2073f0 extends C2092j0 {

    /* renamed from: Q */
    private final ArrayList<C2016a.C2027f> f3319Q;

    /* renamed from: R */
    private final /* synthetic */ C2153z f3320R;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C2073f0(C2153z zVar, ArrayList<C2016a.C2027f> arrayList) {
        super(zVar, null);
        this.f3320R = zVar;
        this.f3319Q = arrayList;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo16643a() {
        this.f3320R.f3509a.f3482n.f3374q = this.f3320R.m5213i();
        ArrayList<C2016a.C2027f> arrayList = this.f3319Q;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            C2016a.C2027f fVar = arrayList.get(i);
            i++;
            fVar.mo16531a(this.f3320R.f3523o, this.f3320R.f3509a.f3482n.f3374q);
        }
    }
}
