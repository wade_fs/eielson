package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2775wb;

/* renamed from: com.google.android.gms.measurement.internal.s1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3182s1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5600a = new C3182s1();

    private C3182s1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2775wb.m7756b());
    }
}
