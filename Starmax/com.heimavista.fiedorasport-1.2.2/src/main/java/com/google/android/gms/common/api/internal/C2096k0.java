package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import java.util.Collections;

/* renamed from: com.google.android.gms.common.api.internal.k0 */
public final class C2096k0 implements C2132t0 {

    /* renamed from: a */
    private final C2136u0 f3356a;

    public C2096k0(C2136u0 u0Var) {
        this.f3356a = u0Var;
    }

    /* renamed from: L */
    public final void mo16739L(int i) {
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16740a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    /* renamed from: a */
    public final void mo16741a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z) {
    }

    /* renamed from: a */
    public final boolean mo16742a() {
        return true;
    }

    /* renamed from: b */
    public final void mo16743b() {
        this.f3356a.mo16786h();
    }

    /* renamed from: c */
    public final void mo16744c() {
        for (C2016a.C2027f fVar : this.f3356a.f3474f.values()) {
            fVar.mo16528a();
        }
        this.f3356a.f3482n.f3374q = Collections.emptySet();
    }

    /* renamed from: f */
    public final void mo16745f(Bundle bundle) {
    }
}
