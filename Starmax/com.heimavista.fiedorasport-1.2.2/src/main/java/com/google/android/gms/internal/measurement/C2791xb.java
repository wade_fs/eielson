package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.xb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2791xb implements C2745ub {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4584a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.collection.log_event_and_bundle_v2", true);

    /* renamed from: a */
    public final boolean mo17946a() {
        return f4584a.mo18128b().booleanValue();
    }
}
