package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2541hc;

/* renamed from: com.google.android.gms.measurement.internal.j1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3077j1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5227a = new C3077j1();

    private C3077j1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2541hc.m6463d());
    }
}
