package com.google.android.datatransport.runtime.backends;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.google.android.datatransport.runtime.backends.k */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
class C1721k implements C1713e {

    /* renamed from: a */
    private final C1722a f2708a;

    /* renamed from: b */
    private final C1719i f2709b;

    /* renamed from: c */
    private final Map<String, C1724m> f2710c;

    C1721k(Context context, C1719i iVar) {
        this(new C1722a(context), iVar);
    }

    @Nullable
    public synchronized C1724m get(String str) {
        if (this.f2710c.containsKey(str)) {
            return this.f2710c.get(str);
        }
        C1712d a = this.f2708a.mo13557a(str);
        if (a == null) {
            return null;
        }
        C1724m create = a.create(this.f2709b.mo13555a(str));
        this.f2710c.put(str, create);
        return create;
    }

    C1721k(C1722a aVar, C1719i iVar) {
        this.f2710c = new HashMap();
        this.f2708a = aVar;
        this.f2709b = iVar;
    }

    /* renamed from: com.google.android.datatransport.runtime.backends.k$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static class C1722a {

        /* renamed from: a */
        private final Context f2711a;

        /* renamed from: b */
        private Map<String, String> f2712b = null;

        C1722a(Context context) {
            this.f2711a = context;
        }

        /* renamed from: b */
        private static Bundle m4301b(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("BackendRegistry", "Context has no PackageManager.");
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, TransportBackendDiscovery.class), 128);
                if (serviceInfo != null) {
                    return serviceInfo.metaData;
                }
                Log.w("BackendRegistry", "TransportBackendDiscovery has no service info.");
                return null;
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("BackendRegistry", "Application info not found.");
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        @Nullable
        /* renamed from: a */
        public C1712d mo13557a(String str) {
            String str2 = m4299a().get(str);
            if (str2 == null) {
                return null;
            }
            try {
                return (C1712d) Class.forName(str2).asSubclass(C1712d.class).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException e) {
                Log.w("BackendRegistry", String.format("Class %s is not found.", str2), e);
                return null;
            } catch (IllegalAccessException e2) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e2);
                return null;
            } catch (InstantiationException e3) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e3);
                return null;
            } catch (NoSuchMethodException e4) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e4);
                return null;
            } catch (InvocationTargetException e5) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e5);
                return null;
            }
        }

        /* renamed from: a */
        private Map<String, String> m4299a() {
            if (this.f2712b == null) {
                this.f2712b = m4300a(this.f2711a);
            }
            return this.f2712b;
        }

        /* renamed from: a */
        private Map<String, String> m4300a(Context context) {
            Bundle b = m4301b(context);
            if (b == null) {
                Log.w("BackendRegistry", "Could not retrieve metadata, returning empty list of transport backends.");
                return Collections.emptyMap();
            }
            HashMap hashMap = new HashMap();
            for (String str : b.keySet()) {
                Object obj = b.get(str);
                if ((obj instanceof String) && str.startsWith("backend:")) {
                    for (String str2 : ((String) obj).split(",", -1)) {
                        String trim = str2.trim();
                        if (!trim.isEmpty()) {
                            hashMap.put(trim, str.substring(8));
                        }
                    }
                }
            }
            return hashMap;
        }
    }
}
