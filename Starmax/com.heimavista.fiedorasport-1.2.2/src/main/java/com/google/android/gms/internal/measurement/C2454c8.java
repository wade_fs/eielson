package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.c8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2454c8 implements C2470d8 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4025a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.module.collection.conditionally_omit_admob_app_id", true);

    /* renamed from: a */
    public final boolean mo17383a() {
        return f4025a.mo18128b().booleanValue();
    }
}
