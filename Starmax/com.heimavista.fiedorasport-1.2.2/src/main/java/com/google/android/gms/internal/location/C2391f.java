package com.google.android.gms.internal.location;

import android.os.IBinder;

/* renamed from: com.google.android.gms.internal.location.f */
public final class C2391f extends C2383a implements C2389d {
    C2391f(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }
}
