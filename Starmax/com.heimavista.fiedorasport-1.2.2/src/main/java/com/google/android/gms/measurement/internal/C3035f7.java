package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.f7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3035f7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f5135P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5136Q;

    C3035f7(C3154p6 p6Var, long j) {
        this.f5136Q = p6Var;
        this.f5135P = j;
    }

    public final void run() {
        this.f5136Q.mo19012g().f5618p.mo19327a(this.f5135P);
        this.f5136Q.mo19015l().mo18995A().mo19043a("Minimum session duration set", Long.valueOf(this.f5135P));
    }
}
