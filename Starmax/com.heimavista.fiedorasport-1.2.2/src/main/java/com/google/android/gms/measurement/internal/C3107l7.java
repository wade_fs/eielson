package com.google.android.gms.measurement.internal;

import androidx.annotation.WorkerThread;
import java.util.List;
import java.util.Map;

@WorkerThread
/* renamed from: com.google.android.gms.measurement.internal.l7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
interface C3107l7 {
    /* renamed from: a */
    void mo19137a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
}
