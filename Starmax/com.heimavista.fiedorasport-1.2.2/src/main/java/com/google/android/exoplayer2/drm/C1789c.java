package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.c */
/* compiled from: lambda */
public final /* synthetic */ class C1789c implements EventDispatcher.Event {

    /* renamed from: a */
    private final /* synthetic */ DefaultDrmSessionManager.MissingSchemeDataException f2803a;

    public /* synthetic */ C1789c(DefaultDrmSessionManager.MissingSchemeDataException missingSchemeDataException) {
        this.f2803a = missingSchemeDataException;
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmSessionManagerError(this.f2803a);
    }
}
