package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.z0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2810z0 extends C2595l4<C2810z0, C2811a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2810z0 zzf;
    private static volatile C2501f6<C2810z0> zzg;
    private int zzc;
    private int zzd;
    private C2691r4 zze = C2595l4.m6648l();

    /* renamed from: com.google.android.gms.internal.measurement.z0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2811a extends C2595l4.C2596a<C2810z0, C2811a> implements C2769w5 {
        private C2811a() {
            super(C2810z0.zzf);
        }

        /* renamed from: a */
        public final C2811a mo18175a(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2810z0) super.f4288Q).m7898c(i);
            return this;
        }

        /* renamed from: j */
        public final C2811a mo18178j() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2810z0) super.f4288Q).m7902v();
            return this;
        }

        /* synthetic */ C2811a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2811a mo18176a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2810z0) super.f4288Q).m7892a(j);
            return this;
        }

        /* renamed from: a */
        public final C2811a mo18177a(Iterable<? extends Long> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2810z0) super.f4288Q).m7897a(iterable);
            return this;
        }
    }

    static {
        C2810z0 z0Var = new C2810z0();
        zzf = z0Var;
        C2595l4.m6645a(C2810z0.class, super);
    }

    private C2810z0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7892a(long j) {
        m7901u();
        this.zze.mo17575g(j);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7898c(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    /* renamed from: s */
    public static C2811a m7899s() {
        return (C2811a) zzf.mo17668i();
    }

    /* renamed from: u */
    private final void m7901u() {
        if (!this.zze.mo17938a()) {
            this.zze = C2595l4.m6641a(this.zze);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v */
    public final void m7902v() {
        this.zze = C2595l4.m6648l();
    }

    /* renamed from: b */
    public final long mo18170b(int i) {
        return this.zze.mo17573b(i);
    }

    /* renamed from: n */
    public final boolean mo18171n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo18172o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final List<Long> mo18173p() {
        return this.zze;
    }

    /* renamed from: q */
    public final int mo18174q() {
        return this.zze.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7897a(Iterable<? extends Long> iterable) {
        m7901u();
        C2766w2.m7700a(iterable, this.zze);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2810z0();
            case 2:
                return new C2811a(null);
            case 3:
                return C2595l4.m6643a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                C2501f6<C2810z0> f6Var = zzg;
                if (f6Var == null) {
                    synchronized (C2810z0.class) {
                        f6Var = zzg;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzf);
                            zzg = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
