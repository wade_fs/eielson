package com.google.android.gms.measurement.internal;

import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.s9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3190s9 implements Callable<String> {

    /* renamed from: a */
    private final /* synthetic */ zzm f5643a;

    /* renamed from: b */
    private final /* synthetic */ C3145o9 f5644b;

    C3190s9(C3145o9 o9Var, zzm zzm) {
        this.f5644b = o9Var;
        this.f5643a = zzm;
    }

    public final /* synthetic */ Object call() {
        C3021e5 c = this.f5644b.mo19226c(this.f5643a);
        if (c != null) {
            return c.mo18969m();
        }
        this.f5644b.mo19015l().mo19004w().mo19042a("App info was null when attempting to get app instance id");
        return null;
    }
}
