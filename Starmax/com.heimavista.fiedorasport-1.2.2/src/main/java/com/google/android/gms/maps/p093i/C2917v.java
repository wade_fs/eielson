package com.google.android.gms.maps.p093i;

import p119e.p144d.p145a.p157c.p161c.p165d.C4038g;

/* renamed from: com.google.android.gms.maps.i.v */
public abstract class C2917v extends C4038g implements C2916u {
    public C2917v() {
        super("com.google.android.gms.maps.internal.IOnStreetViewPanoramaReadyCallback");
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [android.os.IInterface] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo18485a(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) {
        /*
            r1 = this;
            r5 = 1
            if (r2 != r5) goto L_0x0026
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x000b
            r2 = 0
            goto L_0x001f
        L_0x000b:
            java.lang.String r3 = "com.google.android.gms.maps.internal.IStreetViewPanoramaDelegate"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r0 = r3 instanceof com.google.android.gms.maps.p093i.C2897e
            if (r0 == 0) goto L_0x0019
            r2 = r3
            com.google.android.gms.maps.i.e r2 = (com.google.android.gms.maps.p093i.C2897e) r2
            goto L_0x001f
        L_0x0019:
            com.google.android.gms.maps.i.w r3 = new com.google.android.gms.maps.i.w
            r3.<init>(r2)
            r2 = r3
        L_0x001f:
            r1.mo18489a(r2)
            r4.writeNoException()
            return r5
        L_0x0026:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2917v.mo18485a(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
