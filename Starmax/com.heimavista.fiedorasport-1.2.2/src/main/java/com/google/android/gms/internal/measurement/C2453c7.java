package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.Arrays;

/* renamed from: com.google.android.gms.internal.measurement.c7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2453c7 {

    /* renamed from: f */
    private static final C2453c7 f4019f = new C2453c7(0, new int[0], new Object[0], false);

    /* renamed from: a */
    private int f4020a;

    /* renamed from: b */
    private int[] f4021b;

    /* renamed from: c */
    private Object[] f4022c;

    /* renamed from: d */
    private int f4023d;

    /* renamed from: e */
    private boolean f4024e;

    private C2453c7() {
        this(0, new int[8], new Object[8], true);
    }

    /* renamed from: a */
    static C2453c7 m6141a(C2453c7 c7Var, C2453c7 c7Var2) {
        int i = c7Var.f4020a + c7Var2.f4020a;
        int[] copyOf = Arrays.copyOf(c7Var.f4021b, i);
        System.arraycopy(c7Var2.f4021b, 0, copyOf, c7Var.f4020a, c7Var2.f4020a);
        Object[] copyOf2 = Arrays.copyOf(c7Var.f4022c, i);
        System.arraycopy(c7Var2.f4022c, 0, copyOf2, c7Var.f4020a, c7Var2.f4020a);
        return new C2453c7(i, copyOf, copyOf2, true);
    }

    /* renamed from: d */
    public static C2453c7 m6143d() {
        return f4019f;
    }

    /* renamed from: e */
    static C2453c7 m6144e() {
        return new C2453c7();
    }

    /* renamed from: b */
    public final void mo17379b(C2771w7 w7Var) {
        if (this.f4020a != 0) {
            if (w7Var.mo17954a() == C2595l4.C2601f.f4300k) {
                for (int i = 0; i < this.f4020a; i++) {
                    m6142a(this.f4021b[i], this.f4022c[i], w7Var);
                }
                return;
            }
            for (int i2 = this.f4020a - 1; i2 >= 0; i2--) {
                m6142a(this.f4021b[i2], this.f4022c[i2], w7Var);
            }
        }
    }

    /* renamed from: c */
    public final int mo17380c() {
        int i;
        int i2 = this.f4023d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.f4020a; i4++) {
            int i5 = this.f4021b[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = C2720t3.m7235e(i6, ((Long) this.f4022c[i4]).longValue());
            } else if (i7 == 1) {
                i = C2720t3.m7243g(i6, ((Long) this.f4022c[i4]).longValue());
            } else if (i7 == 2) {
                i = C2720t3.m7228c(i6, (C2498f3) this.f4022c[i4]);
            } else if (i7 == 3) {
                i = (C2720t3.m7234e(i6) << 1) + ((C2453c7) this.f4022c[i4]).mo17380c();
            } else if (i7 == 5) {
                i = C2720t3.m7250i(i6, ((Integer) this.f4022c[i4]).intValue());
            } else {
                throw new IllegalStateException(C2723t4.m7314g());
            }
            i3 += i;
        }
        this.f4023d = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof C2453c7)) {
            return false;
        }
        C2453c7 c7Var = (C2453c7) obj;
        int i = this.f4020a;
        if (i == c7Var.f4020a) {
            int[] iArr = this.f4021b;
            int[] iArr2 = c7Var.f4021b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.f4022c;
                Object[] objArr2 = c7Var.f4022c;
                int i3 = this.f4020a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i = this.f4020a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.f4021b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.f4022c;
        int i7 = this.f4020a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    private C2453c7(int i, int[] iArr, Object[] objArr, boolean z) {
        this.f4023d = -1;
        this.f4020a = i;
        this.f4021b = iArr;
        this.f4022c = objArr;
        this.f4024e = z;
    }

    /* renamed from: a */
    public final void mo17374a() {
        this.f4024e = false;
    }

    /* renamed from: b */
    public final int mo17378b() {
        int i = this.f4023d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.f4020a; i3++) {
            i2 += C2720t3.m7232d(this.f4021b[i3] >>> 3, (C2498f3) this.f4022c[i3]);
        }
        this.f4023d = i2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17376a(C2771w7 w7Var) {
        if (w7Var.mo17954a() == C2595l4.C2601f.f4301l) {
            for (int i = this.f4020a - 1; i >= 0; i--) {
                w7Var.mo17961a(this.f4021b[i] >>> 3, this.f4022c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.f4020a; i2++) {
            w7Var.mo17961a(this.f4021b[i2] >>> 3, this.f4022c[i2]);
        }
    }

    /* renamed from: a */
    private static void m6142a(int i, Object obj, C2771w7 w7Var) {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            w7Var.mo17976c(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            w7Var.mo17979d(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            w7Var.mo17960a(i2, (C2498f3) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                w7Var.mo17981e(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(C2723t4.m7314g());
        } else if (w7Var.mo17954a() == C2595l4.C2601f.f4300k) {
            w7Var.mo17955a(i2);
            ((C2453c7) obj).mo17379b(w7Var);
            w7Var.mo17968b(i2);
        } else {
            w7Var.mo17968b(i2);
            ((C2453c7) obj).mo17379b(w7Var);
            w7Var.mo17955a(i2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17377a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.f4020a; i2++) {
            C2816z5.m7927a(sb, i, String.valueOf(this.f4021b[i2] >>> 3), this.f4022c[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17375a(int i, Object obj) {
        if (this.f4024e) {
            int i2 = this.f4020a;
            if (i2 == this.f4021b.length) {
                int i3 = this.f4020a + (i2 < 4 ? 8 : i2 >> 1);
                this.f4021b = Arrays.copyOf(this.f4021b, i3);
                this.f4022c = Arrays.copyOf(this.f4022c, i3);
            }
            int[] iArr = this.f4021b;
            int i4 = this.f4020a;
            iArr[i4] = i;
            this.f4022c[i4] = obj;
            this.f4020a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
