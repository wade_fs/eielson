package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.C2589kc;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.measurement.internal.j8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3084j8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f5266P;

    /* renamed from: Q */
    private final /* synthetic */ String f5267Q;

    /* renamed from: R */
    private final /* synthetic */ zzm f5268R;

    /* renamed from: S */
    private final /* synthetic */ C2589kc f5269S;

    /* renamed from: T */
    private final /* synthetic */ C3232w7 f5270T;

    C3084j8(C3232w7 w7Var, String str, String str2, zzm zzm, C2589kc kcVar) {
        this.f5270T = w7Var;
        this.f5266P = str;
        this.f5267Q = str2;
        this.f5268R = zzm;
        this.f5269S = kcVar;
    }

    public final void run() {
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            C3228w3 d = this.f5270T.f5724d;
            if (d == null) {
                this.f5270T.mo19015l().mo19001t().mo19044a("Failed to get conditional properties; not connected to service", this.f5266P, this.f5267Q);
                return;
            }
            arrayList = C3267z9.m9425b(d.mo19184a(this.f5266P, this.f5267Q, this.f5268R));
            this.f5270T.m9314J();
            this.f5270T.mo19011f().mo19438a(this.f5269S, arrayList);
        } catch (RemoteException e) {
            this.f5270T.mo19015l().mo19001t().mo19045a("Failed to get conditional properties; remote exception", this.f5266P, this.f5267Q, e);
        } finally {
            this.f5270T.mo19011f().mo19438a(this.f5269S, arrayList);
        }
    }
}
