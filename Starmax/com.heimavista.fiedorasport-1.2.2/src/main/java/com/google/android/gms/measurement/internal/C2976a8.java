package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* renamed from: com.google.android.gms.measurement.internal.a8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2976a8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f4953P;

    /* renamed from: Q */
    private final /* synthetic */ boolean f4954Q;

    /* renamed from: R */
    private final /* synthetic */ C3232w7 f4955R;

    C2976a8(C3232w7 w7Var, zzm zzm, boolean z) {
        this.f4955R = w7Var;
        this.f4953P = zzm;
        this.f4954Q = z;
    }

    public final void run() {
        C3228w3 d = this.f4955R.f5724d;
        if (d == null) {
            this.f4955R.mo19015l().mo19001t().mo19042a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            d.mo19199d(this.f4953P);
            if (this.f4954Q) {
                this.f4955R.mo18888t().mo18728D();
            }
            this.f4955R.mo19381a(d, (AbstractSafeParcelable) null, this.f4953P);
            this.f4955R.m9314J();
        } catch (RemoteException e) {
            this.f4955R.mo19015l().mo19001t().mo19043a("Failed to send app launch to the service", e);
        }
    }
}
