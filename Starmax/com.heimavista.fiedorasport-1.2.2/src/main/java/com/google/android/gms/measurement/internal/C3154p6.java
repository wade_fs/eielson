package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import androidx.core.app.NotificationCompat;
import com.facebook.internal.ServerProtocol;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.api.internal.C2072f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2315f;
import com.google.android.gms.common.util.C2325p;
import com.google.android.gms.internal.measurement.C2458cc;
import com.google.android.gms.internal.measurement.C2472da;
import com.google.android.gms.internal.measurement.C2504f9;
import com.google.android.gms.internal.measurement.C2607la;
import com.google.android.gms.internal.measurement.C2696r9;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.p6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3154p6 extends C2995c3 {

    /* renamed from: c */
    protected C3059h7 f5547c;

    /* renamed from: d */
    private C3094k6 f5548d;

    /* renamed from: e */
    private final Set<C3130n6> f5549e = new CopyOnWriteArraySet();

    /* renamed from: f */
    private boolean f5550f;

    /* renamed from: g */
    private final AtomicReference<String> f5551g = new AtomicReference<>();

    /* renamed from: h */
    protected boolean f5552h = true;

    protected C3154p6(C3081j5 j5Var) {
        super(j5Var);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: M */
    public final void m9115M() {
        if (mo19013h().mo19146a(C3135o.f5437b0)) {
            mo18881c();
            String a = mo19012g().f5621s.mo19346a();
            if (a != null) {
                if ("unset".equals(a)) {
                    mo19270a("app", "_npa", (Object) null, mo19017o().mo17132a());
                } else {
                    mo19270a("app", "_npa", Long.valueOf(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(a) ? 1 : 0), mo19017o().mo17132a());
                }
            }
        }
        if (!this.f5134a.mo19089c() || !this.f5552h) {
            mo19015l().mo18995A().mo19042a("Updating Scion state (FE)");
            mo18886q().mo19369C();
            return;
        }
        mo19015l().mo18995A().mo19042a("Recording app launch after enabling measurement for the first time (FE)");
        mo19251I();
        if (C2607la.m6742b() && mo19013h().mo19146a(C3135o.f5415Q0)) {
            mo18889u().f5761d.mo19068a();
        }
        if (C2696r9.m7134b() && mo19013h().mo19146a(C3135o.f5427W0)) {
            if (!(this.f5134a.mo19101t().f5768a.mo19098p().f5613k.mo19326a() > 0)) {
                this.f5134a.mo19101t().mo19401a();
            }
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: c */
    public final void m9127c(boolean z) {
        mo18881c();
        mo18880a();
        mo18816x();
        mo19015l().mo18995A().mo19043a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        mo19012g().mo19311b(z);
        m9115M();
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: d */
    public final void m9128d(Bundle bundle) {
        Bundle bundle2 = bundle;
        mo18881c();
        mo18816x();
        C2258v.m5629a(bundle);
        C2258v.m5639b(bundle2.getString("name"));
        if (!this.f5134a.mo19089c()) {
            mo19015l().mo18996B().mo19042a("Conditional property not cleared since app measurement is disabled");
            return;
        }
        zzkq zzkq = new zzkq(bundle2.getString("name"), 0, null, null);
        try {
            zzan a = mo19011f().mo19428a(bundle2.getString("app_id"), bundle2.getString("expired_event_name"), bundle2.getBundle("expired_event_params"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), bundle2.getLong("creation_timestamp"), true, false);
            zzkq zzkq2 = zzkq;
            mo18886q().mo19384a(new zzv(bundle2.getString("app_id"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), zzkq2, bundle2.getLong("creation_timestamp"), bundle2.getBoolean("active"), bundle2.getString("trigger_event_name"), null, bundle2.getLong("trigger_timeout"), null, bundle2.getLong("time_to_live"), a));
        } catch (IllegalArgumentException unused) {
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return false;
    }

    /* renamed from: B */
    public final void mo19244B() {
        if (mo19016n().getApplicationContext() instanceof Application) {
            ((Application) mo19016n().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.f5547c);
        }
    }

    /* renamed from: C */
    public final Boolean mo19245C() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) mo19014j().mo19026a(atomicReference, 15000, "boolean test flag value", new C3176r6(this, atomicReference));
    }

    /* renamed from: D */
    public final String mo19246D() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) mo19014j().mo19026a(atomicReference, 15000, "String test flag value", new C3264z6(this, atomicReference));
    }

    /* renamed from: E */
    public final Long mo19247E() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) mo19014j().mo19026a(atomicReference, 15000, "long test flag value", new C2987b7(this, atomicReference));
    }

    /* renamed from: F */
    public final Integer mo19248F() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) mo19014j().mo19026a(atomicReference, 15000, "int test flag value", new C2975a7(this, atomicReference));
    }

    /* renamed from: G */
    public final Double mo19249G() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) mo19014j().mo19026a(atomicReference, 15000, "double test flag value", new C3023e7(this, atomicReference));
    }

    @Nullable
    /* renamed from: H */
    public final String mo19250H() {
        mo18880a();
        return this.f5551g.get();
    }

    @WorkerThread
    /* renamed from: I */
    public final void mo19251I() {
        mo18881c();
        mo18880a();
        mo18816x();
        if (this.f5134a.mo19095i()) {
            if (mo19013h().mo19146a(C3135o.f5481x0)) {
                C3110la h = mo19013h();
                h.mo19018r();
                Boolean b = h.mo19148b("google_analytics_deferred_deep_link_enabled");
                if (b != null && b.booleanValue()) {
                    mo19015l().mo18995A().mo19042a("Deferred Deep Link feature enabled.");
                    mo19014j().mo19028a(new C3142o6(this));
                }
            }
            mo18886q().mo19371E();
            this.f5552h = false;
            String z = mo19012g().mo19321z();
            if (!TextUtils.isEmpty(z)) {
                mo19009d().mo18903k();
                if (!z.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", z);
                    mo19267a("auto", "_ou", bundle);
                }
            }
        }
    }

    @Nullable
    /* renamed from: J */
    public final String mo19252J() {
        C3188s7 C = this.f5134a.mo19078E().mo19296C();
        if (C != null) {
            return C.f5638a;
        }
        return null;
    }

    @Nullable
    /* renamed from: K */
    public final String mo19253K() {
        C3188s7 C = this.f5134a.mo19078E().mo19296C();
        if (C != null) {
            return C.f5639b;
        }
        return null;
    }

    @Nullable
    /* renamed from: L */
    public final String mo19254L() {
        if (this.f5134a.mo19074A() != null) {
            return this.f5134a.mo19074A();
        }
        try {
            return C2072f.m4881a();
        } catch (IllegalStateException e) {
            this.f5134a.mo19015l().mo19001t().mo19043a("getGoogleAppId failed with exception", e);
            return null;
        }
    }

    /* renamed from: a */
    public final void mo19274a(boolean z) {
        mo18816x();
        mo18880a();
        mo19014j().mo19028a(new C2999c7(this, z));
    }

    /* renamed from: b */
    public final void mo19279b(boolean z) {
        mo18816x();
        mo18880a();
        mo19014j().mo19028a(new C3047g7(this, z));
    }

    /* renamed from: a */
    public final void mo19259a(long j) {
        mo18880a();
        mo19014j().mo19028a(new C3035f7(this, j));
    }

    /* renamed from: b */
    public final void mo19275b(long j) {
        mo18880a();
        mo19014j().mo19028a(new C3071i7(this, j));
    }

    /* renamed from: c */
    public final void mo19280c(long j) {
        mo19264a((String) null);
        mo19014j().mo19028a(new C3198t6(this, j));
    }

    /* renamed from: a */
    public final void mo19268a(String str, String str2, Bundle bundle, boolean z) {
        mo19269a(str, str2, bundle, false, true, mo19017o().mo17132a());
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19278b(String str, String str2, Bundle bundle) {
        mo18880a();
        mo18881c();
        mo19265a(str, str2, mo19017o().mo17132a(), bundle);
    }

    /* renamed from: a */
    public final void mo19267a(String str, String str2, Bundle bundle) {
        mo19269a(str, str2, bundle, true, true, mo19017o().mo17132a());
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19265a(String str, String str2, long j, Bundle bundle) {
        mo18880a();
        mo18881c();
        mo19266a(str, str2, j, bundle, true, this.f5548d == null || C3267z9.m9430f(str2), false, null);
    }

    /* renamed from: c */
    public final void mo19281c(String str, String str2, Bundle bundle) {
        mo18880a();
        m9125b((String) null, str, str2, bundle);
    }

    /* renamed from: b */
    private final void m9124b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        mo19014j().mo19028a(new C3165q6(this, str, str2, j, C3267z9.m9424b(bundle), z, z2, z3, str3));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: c */
    public final void m9126c(Bundle bundle) {
        Bundle bundle2 = bundle;
        mo18881c();
        mo18816x();
        C2258v.m5629a(bundle);
        C2258v.m5639b(bundle2.getString("name"));
        C2258v.m5639b(bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN));
        C2258v.m5629a(bundle2.get("value"));
        if (!this.f5134a.mo19089c()) {
            mo19015l().mo18996B().mo19042a("Conditional property not set since app measurement is disabled");
            return;
        }
        zzkq zzkq = new zzkq(bundle2.getString("name"), bundle2.getLong("triggered_timestamp"), bundle2.get("value"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN));
        try {
            zzan a = mo19011f().mo19428a(bundle2.getString("app_id"), bundle2.getString("triggered_event_name"), bundle2.getBundle("triggered_event_params"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), 0, true, false);
            mo18886q().mo19384a(new zzv(bundle2.getString("app_id"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), zzkq, bundle2.getLong("creation_timestamp"), false, bundle2.getString("trigger_event_name"), mo19011f().mo19428a(bundle2.getString("app_id"), bundle2.getString("timed_out_event_name"), bundle2.getBundle("timed_out_event_params"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), 0, true, false), bundle2.getLong("trigger_timeout"), a, bundle2.getLong("time_to_live"), mo19011f().mo19428a(bundle2.getString("app_id"), bundle2.getString("expired_event_name"), bundle2.getBundle("expired_event_params"), bundle2.getString(TtmlNode.ATTR_TTS_ORIGIN), 0, true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.s7, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void */
    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19266a(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        C3188s7 s7Var;
        int i;
        long j2;
        ArrayList arrayList;
        Bundle bundle2;
        String str9;
        boolean z4;
        Class<?> cls;
        List<String> H;
        String str10 = str;
        String str11 = str2;
        Bundle bundle3 = bundle;
        C2258v.m5639b(str);
        C2258v.m5629a(bundle);
        mo18881c();
        mo18816x();
        if (!this.f5134a.mo19089c()) {
            mo19015l().mo18995A().mo19042a("Event not sent since app measurement is disabled");
        } else if (!mo19013h().mo19146a(C3135o.f5455k0) || (H = mo18885p().mo18806H()) == null || H.contains(str11)) {
            int i2 = 0;
            if (!this.f5550f) {
                this.f5550f = true;
                try {
                    if (!this.f5134a.mo19077D()) {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService", true, mo19016n().getClassLoader());
                    } else {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService");
                    }
                    try {
                        cls.getDeclaredMethod("initialize", Context.class).invoke(null, mo19016n());
                    } catch (Exception e) {
                        mo19015l().mo19004w().mo19043a("Failed to invoke Tag Manager's initialize() method", e);
                    }
                } catch (ClassNotFoundException unused) {
                    mo19015l().mo19007z().mo19042a("Tag Manager is not found and thus will not be used");
                }
            }
            if (mo19013h().mo19146a(C3135o.f5485z0) && "_cmp".equals(str11) && bundle3.containsKey("gclid")) {
                mo19270a("auto", "_lgclid", bundle3.getString("gclid"), mo19017o().mo17132a());
            }
            if (z3) {
                mo19018r();
                if (!"_iap".equals(str11)) {
                    C3267z9 w = this.f5134a.mo19104w();
                    int i3 = 2;
                    if (w.mo19444a(NotificationCompat.CATEGORY_EVENT, str11)) {
                        if (!w.mo19446a(NotificationCompat.CATEGORY_EVENT, C3082j6.f5264a, str11)) {
                            i3 = 13;
                        } else if (w.mo19443a(NotificationCompat.CATEGORY_EVENT, 40, str11)) {
                            i3 = 0;
                        }
                    }
                    if (i3 != 0) {
                        mo19015l().mo19003v().mo19043a("Invalid public event name. Event will not be logged (FE)", mo19010e().mo18822a(str11));
                        this.f5134a.mo19104w();
                        this.f5134a.mo19104w().mo19431a(i3, "_ev", C3267z9.m9413a(str11, 40, true), str11 != null ? str2.length() : 0);
                        return;
                    }
                }
            }
            mo19018r();
            C3188s7 B = mo18887s().mo19295B();
            if (B != null && !bundle3.containsKey("_sc")) {
                B.f5641d = true;
            }
            C3177r7.m9192a(B, bundle3, z && z3);
            boolean equals = "am".equals(str10);
            boolean f = C3267z9.m9430f(str2);
            if (z && this.f5548d != null && !f && !equals) {
                mo19015l().mo18995A().mo19044a("Passing event to registered event handler (FE)", mo19010e().mo18822a(str11), mo19010e().mo18819a(bundle3));
                this.f5548d.mo18720a(str, str2, bundle, j);
            } else if (this.f5134a.mo19095i()) {
                int a = mo19011f().mo19423a(str11);
                if (a != 0) {
                    mo19015l().mo19003v().mo19043a("Invalid event name. Event will not be logged (FE)", mo19010e().mo18822a(str11));
                    mo19011f();
                    String a2 = C3267z9.m9413a(str11, 40, true);
                    if (str11 != null) {
                        i2 = str2.length();
                    }
                    this.f5134a.mo19104w().mo19441a(str3, a, "_ev", a2, i2);
                    return;
                }
                List a3 = C2315f.m5777a((Object[]) new String[]{"_o", "_sn", "_sc", "_si"});
                String str12 = str11;
                Bundle a4 = mo19011f().mo19427a(str3, str2, bundle, a3, z3, true);
                C3188s7 s7Var2 = (a4 == null || !a4.containsKey("_sc") || !a4.containsKey("_si")) ? null : new C3188s7(a4.getString("_sn"), a4.getString("_sc"), Long.valueOf(a4.getLong("_si")).longValue());
                C3188s7 s7Var3 = s7Var2 == null ? B : s7Var2;
                String str13 = "_ae";
                if (mo19013h().mo19146a(C3135o.f5430Y)) {
                    mo19018r();
                    if (mo18887s().mo19295B() != null && str13.equals(str12)) {
                        long b = mo18889u().f5762e.mo19036b();
                        if (b > 0) {
                            mo19011f().mo19432a(a4, b);
                        }
                    }
                }
                if (C2504f9.m6336b() && mo19013h().mo19146a(C3135o.f5413P0)) {
                    if (!"auto".equals(str10) && "_ssr".equals(str12)) {
                        C3267z9 f2 = mo19011f();
                        String string = a4.getString("_ffr");
                        if (C2325p.m5805a(string)) {
                            str9 = null;
                        } else {
                            str9 = string.trim();
                        }
                        if (C3267z9.m9428c(str9, f2.mo19012g().f5604B.mo19346a())) {
                            f2.mo19015l().mo18995A().mo19042a("Not logging duplicate session_start_with_rollout event");
                            z4 = false;
                        } else {
                            f2.mo19012g().f5604B.mo19347a(str9);
                            z4 = true;
                        }
                        if (!z4) {
                            return;
                        }
                    } else if (str13.equals(str12)) {
                        String a5 = mo19011f().mo19012g().f5604B.mo19346a();
                        if (!TextUtils.isEmpty(a5)) {
                            a4.putString("_ffr", a5);
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(a4);
                long nextLong = mo19011f().mo19454u().nextLong();
                if (!mo19013h().mo19146a(C3135o.f5418S) || mo19012g().f5624v.mo19326a() <= 0 || !mo19012g().mo19309a(j) || !mo19012g().f5627y.mo19333a()) {
                    str4 = "_o";
                } else {
                    mo19015l().mo18996B().mo19042a("Current session is expired, remove the session number, ID, and engagement time");
                    if (mo19013h().mo19146a(C3135o.f5412P)) {
                        str4 = "_o";
                        mo19270a("auto", "_sid", (Object) null, mo19017o().mo17132a());
                    } else {
                        str4 = "_o";
                    }
                    if (mo19013h().mo19146a(C3135o.f5414Q)) {
                        mo19270a("auto", "_sno", (Object) null, mo19017o().mo17132a());
                    }
                    if (C2458cc.m6163b() && mo19013h().mo19146a(C3135o.f5465p0)) {
                        mo19270a("auto", "_se", (Object) null, mo19017o().mo17132a());
                    }
                }
                if (!mo19013h().mo19146a(C3135o.f5416R) || a4.getLong("extend_session", 0) != 1) {
                    str5 = str12;
                } else {
                    mo19015l().mo18996B().mo19042a("EXTEND_SESSION param attached: initiate a new session or extend the current active session");
                    str5 = str12;
                    this.f5134a.mo19100s().f5761d.mo19069a(j, true);
                }
                String[] strArr = (String[]) a4.keySet().toArray(new String[bundle.size()]);
                Arrays.sort(strArr);
                int length = strArr.length;
                int i4 = 0;
                int i5 = 0;
                while (i5 < length) {
                    int i6 = length;
                    String str14 = strArr[i5];
                    Object obj = a4.get(str14);
                    mo19011f();
                    String[] strArr2 = strArr;
                    Bundle[] a6 = C3267z9.m9423a(obj);
                    if (a6 != null) {
                        a4.putInt(str14, a6.length);
                        int i7 = 0;
                        while (i7 < a6.length) {
                            Bundle bundle4 = a6[i7];
                            C3177r7.m9192a(s7Var3, bundle4, true);
                            int i8 = i4;
                            long j3 = nextLong;
                            Bundle bundle5 = bundle4;
                            ArrayList arrayList3 = arrayList2;
                            Bundle a7 = mo19011f().mo19427a(str3, "_ep", bundle5, a3, z3, false);
                            a7.putString("_en", str5);
                            a7.putLong("_eid", j3);
                            a7.putString("_gn", str14);
                            a7.putInt("_ll", a6.length);
                            a7.putInt("_i", i7);
                            arrayList3.add(a7);
                            i7++;
                            a4 = a4;
                            arrayList2 = arrayList3;
                            nextLong = j3;
                            i5 = i5;
                            s7Var3 = s7Var3;
                            str13 = str13;
                            i4 = i8;
                        }
                        s7Var = s7Var3;
                        i = i5;
                        j2 = nextLong;
                        arrayList = arrayList2;
                        str8 = str13;
                        bundle2 = a4;
                        i4 += a6.length;
                    } else {
                        s7Var = s7Var3;
                        i = i5;
                        j2 = nextLong;
                        arrayList = arrayList2;
                        str8 = str13;
                        bundle2 = a4;
                    }
                    i5 = i + 1;
                    strArr = strArr2;
                    a4 = bundle2;
                    arrayList2 = arrayList;
                    nextLong = j2;
                    length = i6;
                    s7Var3 = s7Var;
                    str13 = str8;
                }
                int i9 = i4;
                long j4 = nextLong;
                ArrayList arrayList4 = arrayList2;
                String str15 = str13;
                Bundle bundle6 = a4;
                if (i9 != 0) {
                    bundle6.putLong("_eid", j4);
                    bundle6.putInt("_epc", i9);
                }
                int i10 = 0;
                while (i10 < arrayList4.size()) {
                    Bundle bundle7 = (Bundle) arrayList4.get(i10);
                    if (i10 != 0) {
                        str7 = "_ep";
                        str6 = str;
                    } else {
                        str6 = str;
                        str7 = str5;
                    }
                    String str16 = str4;
                    bundle7.putString(str16, str6);
                    if (z2) {
                        bundle7 = mo19011f().mo19426a(bundle7);
                    }
                    Bundle bundle8 = bundle7;
                    if (!C2472da.m6221b() || !mo19013h().mo19146a(C3135o.f5429X0)) {
                        mo19015l().mo18995A().mo19044a("Logging event (FE)", mo19010e().mo18822a(str5), mo19010e().mo18819a(bundle8));
                    }
                    ArrayList arrayList5 = arrayList4;
                    String str17 = str5;
                    mo18886q().mo19382a(new zzan(str7, new zzam(bundle8), str, j), str3);
                    if (!equals) {
                        for (C3130n6 n6Var : this.f5549e) {
                            n6Var.mo18721a(str, str2, new Bundle(bundle8), j);
                        }
                    }
                    i10++;
                    str4 = str16;
                    arrayList4 = arrayList5;
                    str5 = str17;
                }
                String str18 = str5;
                mo19018r();
                if (mo18887s().mo19295B() != null && str15.equals(str18)) {
                    mo18889u().mo19397a(true, true, mo19017o().elapsedRealtime());
                }
            }
        } else {
            mo19015l().mo18995A().mo19044a("Dropping non-safelisted event. event name, origin", str11, str10);
        }
    }

    /* renamed from: b */
    public final void mo19277b(C3130n6 n6Var) {
        mo18880a();
        mo18816x();
        C2258v.m5629a(n6Var);
        if (!this.f5549e.remove(n6Var)) {
            mo19015l().mo19004w().mo19042a("OnEventListener had not been registered");
        }
    }

    /* renamed from: b */
    public final void mo19276b(Bundle bundle) {
        C2258v.m5629a(bundle);
        C2258v.m5639b(bundle.getString("app_id"));
        mo18882i();
        throw null;
    }

    /* renamed from: b */
    private final void m9122b(Bundle bundle, long j) {
        C2258v.m5629a(bundle);
        C3046g6.m8687a(bundle, "app_id", String.class, null);
        C3046g6.m8687a(bundle, TtmlNode.ATTR_TTS_ORIGIN, String.class, null);
        C3046g6.m8687a(bundle, "name", String.class, null);
        C3046g6.m8687a(bundle, "value", Object.class, null);
        C3046g6.m8687a(bundle, "trigger_event_name", String.class, null);
        C3046g6.m8687a(bundle, "trigger_timeout", Long.class, 0L);
        C3046g6.m8687a(bundle, "timed_out_event_name", String.class, null);
        C3046g6.m8687a(bundle, "timed_out_event_params", Bundle.class, null);
        C3046g6.m8687a(bundle, "triggered_event_name", String.class, null);
        C3046g6.m8687a(bundle, "triggered_event_params", Bundle.class, null);
        C3046g6.m8687a(bundle, "time_to_live", Long.class, 0L);
        C3046g6.m8687a(bundle, "expired_event_name", String.class, null);
        C3046g6.m8687a(bundle, "expired_event_params", Bundle.class, null);
        C2258v.m5639b(bundle.getString("name"));
        C2258v.m5639b(bundle.getString(TtmlNode.ATTR_TTS_ORIGIN));
        C2258v.m5629a(bundle.get("value"));
        bundle.putLong("creation_timestamp", j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (mo19011f().mo19447b(string) != 0) {
            mo19015l().mo19001t().mo19043a("Invalid conditional user property name", mo19010e().mo18824c(string));
        } else if (mo19011f().mo19448b(string, obj) != 0) {
            mo19015l().mo19001t().mo19044a("Invalid conditional user property value", mo19010e().mo18824c(string), obj);
        } else {
            Object c = mo19011f().mo19450c(string, obj);
            if (c == null) {
                mo19015l().mo19001t().mo19044a("Unable to normalize conditional user property value", mo19010e().mo18824c(string), obj);
                return;
            }
            C3046g6.m8688a(bundle, c);
            long j2 = bundle.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle.getString("trigger_event_name")) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong("time_to_live");
                if (j3 > 15552000000L || j3 < 1) {
                    mo19015l().mo19001t().mo19044a("Invalid conditional user property time to live", mo19010e().mo18824c(string), Long.valueOf(j3));
                } else {
                    mo19014j().mo19028a(new C3209u6(this, bundle));
                }
            } else {
                mo19015l().mo19001t().mo19044a("Invalid conditional user property timeout", mo19010e().mo18824c(string), Long.valueOf(j2));
            }
        }
    }

    /* renamed from: b */
    private final void m9125b(String str, String str2, String str3, Bundle bundle) {
        long a = mo19017o().mo17132a();
        C2258v.m5639b(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong("creation_timestamp", a);
        if (str3 != null) {
            bundle2.putString("expired_event_name", str3);
            bundle2.putBundle("expired_event_params", bundle);
        }
        mo19014j().mo19028a(new C3242x6(this, bundle2));
    }

    /* renamed from: b */
    private final ArrayList<Bundle> m9120b(String str, String str2, String str3) {
        if (mo19014j().mo19031t()) {
            mo19015l().mo19001t().mo19042a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (C3098ka.m8843a()) {
            mo19015l().mo19001t().mo19042a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.f5134a.mo19014j().mo19026a(atomicReference, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, "get conditional user properties", new C3231w6(this, atomicReference, str, str2, str3));
            List list = (List) atomicReference.get();
            if (list != null) {
                return C3267z9.m9425b(list);
            }
            mo19015l().mo19001t().mo19043a("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    /* renamed from: b */
    private final Map<String, Object> m9121b(String str, String str2, String str3, boolean z) {
        if (mo19014j().mo19031t()) {
            mo19015l().mo19001t().mo19042a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (C3098ka.m8843a()) {
            mo19015l().mo19001t().mo19042a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.f5134a.mo19014j().mo19026a(atomicReference, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, "get user properties", new C3253y6(this, atomicReference, str, str2, str3, z));
            List<zzkq> list = (List) atomicReference.get();
            if (list == null) {
                mo19015l().mo19001t().mo19043a("Timed out waiting for handle get user properties, includeInternal", Boolean.valueOf(z));
                return Collections.emptyMap();
            }
            ArrayMap arrayMap = new ArrayMap(list.size());
            for (zzkq zzkq : list) {
                arrayMap.put(zzkq.f5808Q, zzkq.mo19469a());
            }
            return arrayMap;
        }
    }

    /* renamed from: a */
    public final void mo19269a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        mo18880a();
        m9124b(str == null ? "app" : str, str2, j, bundle == null ? new Bundle() : bundle, z2, !z2 || this.f5548d == null || C3267z9.m9430f(str2), !z, null);
    }

    /* renamed from: a */
    public final void mo19271a(String str, String str2, Object obj, boolean z) {
        mo19272a(str, str2, obj, z, mo19017o().mo17132a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String */
    /* renamed from: a */
    public final void mo19272a(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = mo19011f().mo19447b(str2);
        } else {
            C3267z9 f = mo19011f();
            if (f.mo19444a("user property", str2)) {
                if (!f.mo19446a("user property", C3106l6.f5324a, str2)) {
                    i = 15;
                } else if (f.mo19443a("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            mo19011f();
            String a = C3267z9.m9413a(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.f5134a.mo19104w().mo19431a(i, "_ev", a, i2);
        } else if (obj != null) {
            int b = mo19011f().mo19448b(str2, obj);
            if (b != 0) {
                mo19011f();
                String a2 = C3267z9.m9413a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.f5134a.mo19104w().mo19431a(b, "_ev", a2, i2);
                return;
            }
            Object c = mo19011f().mo19450c(str2, obj);
            if (c != null) {
                m9119a(str3, str2, j, c);
            }
        } else {
            m9119a(str3, str2, j, (Object) null);
        }
    }

    /* renamed from: a */
    private final void m9119a(String str, String str2, long j, Object obj) {
        mo19014j().mo19028a(new C3187s6(this, str, str2, obj, j));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0087  */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19270a(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            com.google.android.gms.common.internal.C2258v.m5639b(r9)
            com.google.android.gms.common.internal.C2258v.m5639b(r10)
            r8.mo18881c()
            r8.mo18880a()
            r8.mo18816x()
            com.google.android.gms.measurement.internal.la r0 = r8.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.C3135o.f5437b0
            boolean r0 = r0.mo19146a(r1)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L_0x006f
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x006f
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x005f
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x005f
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.String r10 = r0.toLowerCase(r10)
            java.lang.String r11 = "false"
            boolean r10 = r11.equals(r10)
            r2 = 1
            if (r10 == 0) goto L_0x0044
            r4 = r2
            goto L_0x0046
        L_0x0044:
            r4 = 0
        L_0x0046:
            java.lang.Long r10 = java.lang.Long.valueOf(r4)
            com.google.android.gms.measurement.internal.s4 r0 = r8.mo19012g()
            com.google.android.gms.measurement.internal.v4 r0 = r0.f5621s
            long r4 = r10.longValue()
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x005a
            java.lang.String r11 = "true"
        L_0x005a:
            r0.mo19347a(r11)
            r6 = r10
            goto L_0x006d
        L_0x005f:
            if (r11 != 0) goto L_0x006f
            com.google.android.gms.measurement.internal.s4 r10 = r8.mo19012g()
            com.google.android.gms.measurement.internal.v4 r10 = r10.f5621s
            java.lang.String r0 = "unset"
            r10.mo19347a(r0)
            r6 = r11
        L_0x006d:
            r3 = r1
            goto L_0x0071
        L_0x006f:
            r3 = r10
            r6 = r11
        L_0x0071:
            com.google.android.gms.measurement.internal.j5 r10 = r8.f5134a
            boolean r10 = r10.mo19089c()
            if (r10 != 0) goto L_0x0087
            com.google.android.gms.measurement.internal.f4 r9 = r8.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo18996B()
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.mo19042a(r10)
            return
        L_0x0087:
            com.google.android.gms.measurement.internal.j5 r10 = r8.f5134a
            boolean r10 = r10.mo19095i()
            if (r10 != 0) goto L_0x0090
            return
        L_0x0090:
            com.google.android.gms.measurement.internal.zzkq r10 = new com.google.android.gms.measurement.internal.zzkq
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            com.google.android.gms.measurement.internal.w7 r9 = r8.mo18886q()
            r9.mo19383a(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3154p6.mo19270a(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19264a(@Nullable String str) {
        this.f5551g.set(str);
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19262a(C3094k6 k6Var) {
        C3094k6 k6Var2;
        mo18881c();
        mo18880a();
        mo18816x();
        if (!(k6Var == null || k6Var == (k6Var2 = this.f5548d))) {
            C2258v.m5641b(k6Var2 == null, "EventInterceptor already set.");
        }
        this.f5548d = k6Var;
    }

    /* renamed from: a */
    public final void mo19263a(C3130n6 n6Var) {
        mo18880a();
        mo18816x();
        C2258v.m5629a(n6Var);
        if (!this.f5549e.add(n6Var)) {
            mo19015l().mo19004w().mo19042a("OnEventListener already registered");
        }
    }

    /* renamed from: a */
    public final void mo19260a(Bundle bundle) {
        mo19261a(bundle, mo19017o().mo17132a());
    }

    /* renamed from: a */
    public final void mo19261a(Bundle bundle, long j) {
        C2258v.m5629a(bundle);
        mo18880a();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            mo19015l().mo19004w().mo19042a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        m9122b(bundle2, j);
    }

    /* renamed from: a */
    public final void mo19273a(String str, String str2, String str3, Bundle bundle) {
        C2258v.m5639b(str);
        mo18882i();
        throw null;
    }

    /* renamed from: a */
    public final ArrayList<Bundle> mo19255a(String str, String str2) {
        mo18880a();
        return m9120b((String) null, str, str2);
    }

    /* renamed from: a */
    public final ArrayList<Bundle> mo19256a(String str, String str2, String str3) {
        C2258v.m5639b(str);
        mo18882i();
        throw null;
    }

    /* renamed from: a */
    public final Map<String, Object> mo19258a(String str, String str2, boolean z) {
        mo18880a();
        return m9121b((String) null, str, str2, z);
    }

    /* renamed from: a */
    public final Map<String, Object> mo19257a(String str, String str2, String str3, boolean z) {
        C2258v.m5639b(str);
        mo18882i();
        throw null;
    }
}
