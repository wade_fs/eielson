package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.h */
/* compiled from: lambda */
public final /* synthetic */ class C1794h implements EventDispatcher.Event {

    /* renamed from: a */
    public static final /* synthetic */ C1794h f2810a = new C1794h();

    private /* synthetic */ C1794h() {
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmKeysLoaded();
    }
}
