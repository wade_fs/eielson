package com.google.android.gms.common.api.internal;

import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.u */
final class C2135u {

    /* renamed from: a */
    private final C2063d2<?> f3467a;

    /* renamed from: b */
    private final C4066i<Boolean> f3468b;

    /* renamed from: a */
    public final C4066i<Boolean> mo16780a() {
        return this.f3468b;
    }

    /* renamed from: b */
    public final C2063d2<?> mo16781b() {
        return this.f3467a;
    }
}
