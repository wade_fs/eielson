package com.google.android.material.transition;

import android.content.Context;
import android.transition.Fade;
import android.transition.Transition;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.animation.AnimationUtils;

@RequiresApi(21)
public class MaterialFade extends MaterialTransitionSet<Fade> {
    private static final long DEFAULT_DURATION_ENTER = 150;
    private static final long DEFAULT_DURATION_ENTER_FADE = 45;
    private static final long DEFAULT_DURATION_RETURN = 75;
    private static final float DEFAULT_START_SCALE = 0.8f;

    private MaterialFade(boolean z) {
        setDuration(z ? 150 : 75);
        setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
    }

    @NonNull
    public static MaterialFade create(@NonNull Context context) {
        return create(context, true);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Transition getDefaultSecondaryTransition() {
        Scale scale = new Scale();
        scale.setMode(1);
        scale.setIncomingStartScale(DEFAULT_START_SCALE);
        return scale;
    }

    @Nullable
    public /* bridge */ /* synthetic */ Transition getSecondaryTransition() {
        return super.getSecondaryTransition();
    }

    public /* bridge */ /* synthetic */ void setSecondaryTransition(@Nullable Transition transition) {
        super.setSecondaryTransition(transition);
    }

    @NonNull
    public static MaterialFade create(@NonNull Context context, boolean z) {
        MaterialFade materialFade = new MaterialFade(z);
        super.initialize(context);
        if (z) {
            ((Fade) super.getPrimaryTransition()).setDuration(DEFAULT_DURATION_ENTER_FADE);
        }
        return materialFade;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Fade getDefaultPrimaryTransition() {
        return new Fade();
    }
}
