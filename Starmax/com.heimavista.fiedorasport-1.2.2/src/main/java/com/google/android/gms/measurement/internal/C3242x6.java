package com.google.android.gms.measurement.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.measurement.internal.x6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3242x6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ Bundle f5754P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5755Q;

    C3242x6(C3154p6 p6Var, Bundle bundle) {
        this.f5755Q = p6Var;
        this.f5754P = bundle;
    }

    public final void run() {
        this.f5755Q.m9128d(this.f5754P);
    }
}
