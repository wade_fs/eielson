package com.google.android.gms.auth.api;

import com.google.android.gms.auth.api.proxy.C1972a;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.internal.auth.C2370f;
import com.google.android.gms.internal.auth.C2373i;

/* renamed from: com.google.android.gms.auth.api.b */
public final class C1959b {

    /* renamed from: a */
    private static final C2016a.C2028g<C2370f> f3025a = new C2016a.C2028g<>();

    /* renamed from: b */
    private static final C2016a.C2017a<C2370f, C1960c> f3026b = new C1969d();

    /* renamed from: c */
    public static final C2016a<C1960c> f3027c = new C2016a<>("Auth.PROXY_API", f3026b, f3025a);

    /* renamed from: d */
    public static final C1972a f3028d = new C2373i();
}
