package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.i.d */
public interface C2895d extends IInterface {
    /* renamed from: a */
    void mo18449a(Bundle bundle);

    /* renamed from: a */
    void mo18450a(C2911p pVar);

    /* renamed from: b */
    void mo18451b();

    /* renamed from: b */
    void mo18452b(Bundle bundle);

    /* renamed from: c */
    void mo18453c();

    /* renamed from: d */
    void mo18454d();

    C3988b getView();

    void onLowMemory();

    void onResume();

    void onStart();
}
