package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2473db;

/* renamed from: com.google.android.gms.measurement.internal.e3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3019e3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5062a = new C3019e3();

    private C3019e3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2473db.m6224b());
    }
}
