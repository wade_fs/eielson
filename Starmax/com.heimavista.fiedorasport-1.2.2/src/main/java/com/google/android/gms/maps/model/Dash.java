package com.google.android.gms.maps.model;

public final class Dash extends PatternItem {

    /* renamed from: R */
    public final float f4820R;

    public final String toString() {
        float f = this.f4820R;
        StringBuilder sb = new StringBuilder(30);
        sb.append("[Dash: length=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }
}
