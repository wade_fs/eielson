package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.maps.model.z */
public final class C2955z implements Parcelable.Creator<TileOverlayOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        IBinder iBinder = null;
        boolean z = false;
        float f = 0.0f;
        boolean z2 = true;
        float f2 = 0.0f;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                iBinder = C2248a.m5584y(parcel, a);
            } else if (a2 == 3) {
                z = C2248a.m5577r(parcel, a);
            } else if (a2 == 4) {
                f = C2248a.m5582w(parcel, a);
            } else if (a2 == 5) {
                z2 = C2248a.m5577r(parcel, a);
            } else if (a2 != 6) {
                C2248a.m5550F(parcel, a);
            } else {
                f2 = C2248a.m5582w(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new TileOverlayOptions(iBinder, z, f, z2, f2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new TileOverlayOptions[i];
    }
}
