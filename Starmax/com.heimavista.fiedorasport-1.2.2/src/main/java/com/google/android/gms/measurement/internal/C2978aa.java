package com.google.android.gms.measurement.internal;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

/* renamed from: com.google.android.gms.measurement.internal.aa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2978aa extends SSLSocket {

    /* renamed from: P */
    private final SSLSocket f4958P;

    C2978aa(C3256y9 y9Var, SSLSocket sSLSocket) {
        this.f4958P = super;
    }

    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f4958P.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void bind(SocketAddress socketAddress) {
        this.f4958P.bind(socketAddress);
    }

    public final synchronized void close() {
        this.f4958P.close();
    }

    public final void connect(SocketAddress socketAddress) {
        this.f4958P.connect(socketAddress);
    }

    public final boolean equals(Object obj) {
        return this.f4958P.equals(obj);
    }

    public final SocketChannel getChannel() {
        return this.f4958P.getChannel();
    }

    public final boolean getEnableSessionCreation() {
        return this.f4958P.getEnableSessionCreation();
    }

    public final String[] getEnabledCipherSuites() {
        return this.f4958P.getEnabledCipherSuites();
    }

    public final String[] getEnabledProtocols() {
        return this.f4958P.getEnabledProtocols();
    }

    public final InetAddress getInetAddress() {
        return this.f4958P.getInetAddress();
    }

    public final InputStream getInputStream() {
        return this.f4958P.getInputStream();
    }

    public final boolean getKeepAlive() {
        return this.f4958P.getKeepAlive();
    }

    public final InetAddress getLocalAddress() {
        return this.f4958P.getLocalAddress();
    }

    public final int getLocalPort() {
        return this.f4958P.getLocalPort();
    }

    public final SocketAddress getLocalSocketAddress() {
        return this.f4958P.getLocalSocketAddress();
    }

    public final boolean getNeedClientAuth() {
        return this.f4958P.getNeedClientAuth();
    }

    public final boolean getOOBInline() {
        return this.f4958P.getOOBInline();
    }

    public final OutputStream getOutputStream() {
        return this.f4958P.getOutputStream();
    }

    public final int getPort() {
        return this.f4958P.getPort();
    }

    public final synchronized int getReceiveBufferSize() {
        return this.f4958P.getReceiveBufferSize();
    }

    public final SocketAddress getRemoteSocketAddress() {
        return this.f4958P.getRemoteSocketAddress();
    }

    public final boolean getReuseAddress() {
        return this.f4958P.getReuseAddress();
    }

    public final synchronized int getSendBufferSize() {
        return this.f4958P.getSendBufferSize();
    }

    public final SSLSession getSession() {
        return this.f4958P.getSession();
    }

    public final int getSoLinger() {
        return this.f4958P.getSoLinger();
    }

    public final synchronized int getSoTimeout() {
        return this.f4958P.getSoTimeout();
    }

    public final String[] getSupportedCipherSuites() {
        return this.f4958P.getSupportedCipherSuites();
    }

    public final String[] getSupportedProtocols() {
        return this.f4958P.getSupportedProtocols();
    }

    public final boolean getTcpNoDelay() {
        return this.f4958P.getTcpNoDelay();
    }

    public final int getTrafficClass() {
        return this.f4958P.getTrafficClass();
    }

    public final boolean getUseClientMode() {
        return this.f4958P.getUseClientMode();
    }

    public final boolean getWantClientAuth() {
        return this.f4958P.getWantClientAuth();
    }

    public final boolean isBound() {
        return this.f4958P.isBound();
    }

    public final boolean isClosed() {
        return this.f4958P.isClosed();
    }

    public final boolean isConnected() {
        return this.f4958P.isConnected();
    }

    public final boolean isInputShutdown() {
        return this.f4958P.isInputShutdown();
    }

    public final boolean isOutputShutdown() {
        return this.f4958P.isOutputShutdown();
    }

    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f4958P.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void sendUrgentData(int i) {
        this.f4958P.sendUrgentData(i);
    }

    public final void setEnableSessionCreation(boolean z) {
        this.f4958P.setEnableSessionCreation(z);
    }

    public final void setEnabledCipherSuites(String[] strArr) {
        this.f4958P.setEnabledCipherSuites(strArr);
    }

    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.f4958P.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.f4958P.setEnabledProtocols(strArr);
    }

    public final void setKeepAlive(boolean z) {
        this.f4958P.setKeepAlive(z);
    }

    public final void setNeedClientAuth(boolean z) {
        this.f4958P.setNeedClientAuth(z);
    }

    public final void setOOBInline(boolean z) {
        this.f4958P.setOOBInline(z);
    }

    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.f4958P.setPerformancePreferences(i, i2, i3);
    }

    public final synchronized void setReceiveBufferSize(int i) {
        this.f4958P.setReceiveBufferSize(i);
    }

    public final void setReuseAddress(boolean z) {
        this.f4958P.setReuseAddress(z);
    }

    public final synchronized void setSendBufferSize(int i) {
        this.f4958P.setSendBufferSize(i);
    }

    public final void setSoLinger(boolean z, int i) {
        this.f4958P.setSoLinger(z, i);
    }

    public final synchronized void setSoTimeout(int i) {
        this.f4958P.setSoTimeout(i);
    }

    public final void setTcpNoDelay(boolean z) {
        this.f4958P.setTcpNoDelay(z);
    }

    public final void setTrafficClass(int i) {
        this.f4958P.setTrafficClass(i);
    }

    public final void setUseClientMode(boolean z) {
        this.f4958P.setUseClientMode(z);
    }

    public final void setWantClientAuth(boolean z) {
        this.f4958P.setWantClientAuth(z);
    }

    public final void shutdownInput() {
        this.f4958P.shutdownInput();
    }

    public final void shutdownOutput() {
        this.f4958P.shutdownOutput();
    }

    public final void startHandshake() {
        this.f4958P.startHandshake();
    }

    public final String toString() {
        return this.f4958P.toString();
    }

    public final void connect(SocketAddress socketAddress, int i) {
        this.f4958P.connect(socketAddress, i);
    }
}
