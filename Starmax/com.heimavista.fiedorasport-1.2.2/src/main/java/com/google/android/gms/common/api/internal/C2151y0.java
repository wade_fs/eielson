package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.C2051b;

/* renamed from: com.google.android.gms.common.api.internal.y0 */
final class C2151y0 implements C2051b.C2052a {

    /* renamed from: a */
    private final /* synthetic */ C2064e f3508a;

    C2151y0(C2064e eVar) {
        this.f3508a = eVar;
    }

    /* renamed from: a */
    public final void mo16631a(boolean z) {
        this.f3508a.f3284b0.sendMessage(this.f3508a.f3284b0.obtainMessage(1, Boolean.valueOf(z)));
    }
}
