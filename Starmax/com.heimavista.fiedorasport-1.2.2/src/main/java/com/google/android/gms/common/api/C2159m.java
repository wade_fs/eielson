package com.google.android.gms.common.api;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.m */
public abstract class C2159m<R extends C2157k> implements C2158l<R> {
    /* renamed from: a */
    public abstract void mo16810a(@NonNull Status status);

    /* renamed from: b */
    public abstract void mo16811b(@NonNull R r);
}
