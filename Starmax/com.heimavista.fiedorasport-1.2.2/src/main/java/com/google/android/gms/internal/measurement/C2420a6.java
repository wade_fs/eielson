package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.a6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2420a6<T> implements C2603l6<T> {

    /* renamed from: a */
    private final C2739u5 f3976a;

    /* renamed from: b */
    private final C2469d7<?, ?> f3977b;

    /* renamed from: c */
    private final boolean f3978c;

    /* renamed from: d */
    private final C2418a4<?> f3979d;

    private C2420a6(C2469d7<?, ?> d7Var, C2418a4<?> a4Var, C2739u5 u5Var) {
        this.f3977b = d7Var;
        this.f3978c = a4Var.mo17270a(u5Var);
        this.f3979d = a4Var;
        this.f3976a = u5Var;
    }

    /* renamed from: a */
    static <T> C2420a6<T> m6002a(C2469d7<?, ?> d7Var, C2418a4<?> a4Var, C2739u5 u5Var) {
        return new C2420a6<>(d7Var, a4Var, u5Var);
    }

    /* renamed from: b */
    public final void mo17281b(T t, T t2) {
        C2633n6.m6886a(this.f3977b, t, t2);
        if (this.f3978c) {
            C2633n6.m6885a(this.f3979d, t, t2);
        }
    }

    /* renamed from: c */
    public final void mo17283c(T t) {
        this.f3977b.mo17423b(t);
        this.f3979d.mo17272c(t);
    }

    /* renamed from: d */
    public final int mo17284d(T t) {
        C2469d7<?, ?> d7Var = this.f3977b;
        int c = d7Var.mo17426c(d7Var.mo17419a(t)) + 0;
        return this.f3978c ? c + this.f3979d.mo17267a((Object) t).mo17318f() : c;
    }

    /* renamed from: a */
    public final T mo17277a() {
        return this.f3976a.mo17662d().mo17680r();
    }

    /* renamed from: a */
    public final boolean mo17280a(T t, T t2) {
        if (!this.f3977b.mo17419a(t).equals(this.f3977b.mo17419a(t2))) {
            return false;
        }
        if (this.f3978c) {
            return this.f3979d.mo17267a((Object) t).equals(this.f3979d.mo17267a((Object) t2));
        }
        return true;
    }

    /* renamed from: b */
    public final boolean mo17282b(T t) {
        return this.f3979d.mo17267a((Object) t).mo17316e();
    }

    /* renamed from: a */
    public final int mo17276a(T t) {
        int hashCode = this.f3977b.mo17419a(t).hashCode();
        return this.f3978c ? (hashCode * 53) + this.f3979d.mo17267a((Object) t).hashCode() : hashCode;
    }

    /* renamed from: a */
    public final void mo17278a(T t, C2771w7 w7Var) {
        Iterator<Map.Entry<?, Object>> c = this.f3979d.mo17267a((Object) t).mo17313c();
        if (!c.hasNext()) {
            C2469d7<?, ?> d7Var = this.f3977b;
            d7Var.mo17424b(d7Var.mo17419a(t), w7Var);
            return;
        }
        ((C2466d4) c.next().getKey()).mo17414f();
        throw null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.google.android.gms.internal.measurement.l4$d} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo17279a(T r10, byte[] r11, int r12, int r13, com.google.android.gms.internal.measurement.C2417a3 r14) {
        /*
            r9 = this;
            r0 = r10
            com.google.android.gms.internal.measurement.l4 r0 = (com.google.android.gms.internal.measurement.C2595l4) r0
            com.google.android.gms.internal.measurement.c7 r1 = r0.zzb
            com.google.android.gms.internal.measurement.c7 r2 = com.google.android.gms.internal.measurement.C2453c7.m6143d()
            if (r1 != r2) goto L_0x0011
            com.google.android.gms.internal.measurement.c7 r1 = com.google.android.gms.internal.measurement.C2453c7.m6144e()
            r0.zzb = r1
        L_0x0011:
            com.google.android.gms.internal.measurement.l4$b r10 = (com.google.android.gms.internal.measurement.C2595l4.C2597b) r10
            r10.mo17681n()
            r10 = 0
            r0 = r10
        L_0x0018:
            if (r12 >= r13) goto L_0x00a4
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r11, r12, r14)
            int r2 = r14.f3971a
            r12 = 11
            r3 = 2
            if (r2 == r12) goto L_0x0051
            r12 = r2 & 7
            if (r12 != r3) goto L_0x004c
            com.google.android.gms.internal.measurement.a4<?> r12 = r9.f3979d
            com.google.android.gms.internal.measurement.y3 r0 = r14.f3974d
            com.google.android.gms.internal.measurement.u5 r3 = r9.f3976a
            int r5 = r2 >>> 3
            java.lang.Object r12 = r12.mo17268a(r0, r3, r5)
            r0 = r12
            com.google.android.gms.internal.measurement.l4$d r0 = (com.google.android.gms.internal.measurement.C2595l4.C2599d) r0
            if (r0 != 0) goto L_0x0043
            r3 = r11
            r5 = r13
            r6 = r1
            r7 = r14
            int r12 = com.google.android.gms.internal.measurement.C2433b3.m6048a(r2, r3, r4, r5, r6, r7)
            goto L_0x0018
        L_0x0043:
            com.google.android.gms.internal.measurement.C2550i6.m6481a()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x004c:
            int r12 = com.google.android.gms.internal.measurement.C2433b3.m6047a(r2, r11, r4, r13, r14)
            goto L_0x0018
        L_0x0051:
            r12 = 0
            r2 = r10
        L_0x0053:
            if (r4 >= r13) goto L_0x0099
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r11, r4, r14)
            int r5 = r14.f3971a
            int r6 = r5 >>> 3
            r7 = r5 & 7
            if (r6 == r3) goto L_0x007b
            r8 = 3
            if (r6 == r8) goto L_0x0065
            goto L_0x0090
        L_0x0065:
            if (r0 != 0) goto L_0x0072
            if (r7 != r3) goto L_0x0090
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6063e(r11, r4, r14)
            java.lang.Object r2 = r14.f3973c
            com.google.android.gms.internal.measurement.f3 r2 = (com.google.android.gms.internal.measurement.C2498f3) r2
            goto L_0x0053
        L_0x0072:
            com.google.android.gms.internal.measurement.C2550i6.m6481a()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x007b:
            if (r7 != 0) goto L_0x0090
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r11, r4, r14)
            int r12 = r14.f3971a
            com.google.android.gms.internal.measurement.a4<?> r0 = r9.f3979d
            com.google.android.gms.internal.measurement.y3 r5 = r14.f3974d
            com.google.android.gms.internal.measurement.u5 r6 = r9.f3976a
            java.lang.Object r0 = r0.mo17268a(r5, r6, r12)
            com.google.android.gms.internal.measurement.l4$d r0 = (com.google.android.gms.internal.measurement.C2595l4.C2599d) r0
            goto L_0x0053
        L_0x0090:
            r6 = 12
            if (r5 == r6) goto L_0x0099
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6047a(r5, r11, r4, r13, r14)
            goto L_0x0053
        L_0x0099:
            if (r2 == 0) goto L_0x00a1
            int r12 = r12 << 3
            r12 = r12 | r3
            r1.mo17375a(r12, r2)
        L_0x00a1:
            r12 = r4
            goto L_0x0018
        L_0x00a4:
            if (r12 != r13) goto L_0x00a7
            return
        L_0x00a7:
            com.google.android.gms.internal.measurement.t4 r10 = com.google.android.gms.internal.measurement.C2723t4.m7315h()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2420a6.mo17279a(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.a3):void");
    }
}
