package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.util.a */
/* compiled from: lambda */
public final /* synthetic */ class C1935a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ EventDispatcher.HandlerAndListener f2941P;

    /* renamed from: Q */
    private final /* synthetic */ EventDispatcher.Event f2942Q;

    public /* synthetic */ C1935a(EventDispatcher.HandlerAndListener handlerAndListener, EventDispatcher.Event event) {
        this.f2941P = handlerAndListener;
        this.f2942Q = event;
    }

    public final void run() {
        this.f2941P.mo16101a(this.f2942Q);
    }
}
