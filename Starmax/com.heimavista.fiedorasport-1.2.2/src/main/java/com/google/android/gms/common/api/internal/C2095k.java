package com.google.android.gms.common.api.internal;

import androidx.annotation.Nullable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2018b;
import com.google.android.gms.common.api.internal.C2084i;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.k */
public abstract class C2095k<A extends C2016a.C2018b, L> {

    /* renamed from: a */
    private final C2084i<L> f3353a;

    /* renamed from: b */
    private final Feature[] f3354b = null;

    /* renamed from: c */
    private final boolean f3355c = false;

    protected C2095k(C2084i<L> iVar) {
        this.f3353a = iVar;
    }

    /* renamed from: a */
    public void mo16734a() {
        this.f3353a.mo16719a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16735a(A a, C4066i<Void> iVar);

    /* renamed from: b */
    public C2084i.C2085a<L> mo16736b() {
        return this.f3353a.mo16721b();
    }

    @Nullable
    /* renamed from: c */
    public Feature[] mo16737c() {
        return this.f3354b;
    }

    /* renamed from: d */
    public final boolean mo16738d() {
        return this.f3355c;
    }
}
