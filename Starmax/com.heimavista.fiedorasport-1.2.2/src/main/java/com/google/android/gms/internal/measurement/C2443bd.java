package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.measurement.bd */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2443bd extends C2545i1 implements C2589kc {
    public C2443bd() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17344a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        mo17292d((Bundle) C2670q.m7044a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
