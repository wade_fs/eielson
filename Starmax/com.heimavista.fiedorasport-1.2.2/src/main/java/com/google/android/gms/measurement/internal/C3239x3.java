package com.google.android.gms.measurement.internal;

import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.google.android.gms.measurement.internal.x3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3239x3<V> {

    /* renamed from: g */
    private static final Object f5742g = new Object();

    /* renamed from: a */
    private final String f5743a;

    /* renamed from: b */
    private final C3217v3<V> f5744b;

    /* renamed from: c */
    private final V f5745c;

    /* renamed from: d */
    private final Object f5746d;
    @GuardedBy("overrideLock")

    /* renamed from: e */
    private volatile V f5747e;
    @GuardedBy("cachingLock")

    /* renamed from: f */
    private volatile V f5748f;

    private C3239x3(@NonNull String str, @NonNull V v, @NonNull V v2, @Nullable C3217v3<V> v3Var) {
        this.f5746d = new Object();
        this.f5747e = null;
        this.f5748f = null;
        this.f5743a = str;
        this.f5745c = v;
        this.f5744b = v3Var;
    }

    /* renamed from: a */
    public final String mo19390a() {
        return this.f5743a;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 171 */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:28|29|30|(1:32)|33|34|4a) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0021, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r4 = com.google.android.gms.measurement.internal.C3135o.f5434a.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r0.f5748f = r1;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0048 */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0062 A[SYNTHETIC, Splitter:B:51:0x0062] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V mo19389a(@androidx.annotation.Nullable V r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.f5746d
            monitor-enter(r0)
            monitor-exit(r0)     // Catch:{ all -> 0x0072 }
            if (r4 == 0) goto L_0x0007
            return r4
        L_0x0007:
            com.google.android.gms.measurement.internal.ka r4 = com.google.android.gms.measurement.internal.C3206u3.f5671a
            if (r4 != 0) goto L_0x000e
            V r4 = r3.f5745c
            return r4
        L_0x000e:
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3239x3.f5742g
            monitor-enter(r4)
            boolean r0 = com.google.android.gms.measurement.internal.C3098ka.m8843a()     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x0022
            V r0 = r3.f5748f     // Catch:{ all -> 0x006d }
            if (r0 != 0) goto L_0x001e
            V r0 = r3.f5745c     // Catch:{ all -> 0x006d }
            goto L_0x0020
        L_0x001e:
            V r0 = r3.f5748f     // Catch:{ all -> 0x006d }
        L_0x0020:
            monitor-exit(r4)     // Catch:{ all -> 0x006d }
            return r0
        L_0x0022:
            monitor-exit(r4)     // Catch:{ all -> 0x006d }
            java.util.List r4 = com.google.android.gms.measurement.internal.C3135o.f5434a     // Catch:{ SecurityException -> 0x005a }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ SecurityException -> 0x005a }
        L_0x002b:
            boolean r0 = r4.hasNext()     // Catch:{ SecurityException -> 0x005a }
            if (r0 == 0) goto L_0x005b
            java.lang.Object r0 = r4.next()     // Catch:{ SecurityException -> 0x005a }
            com.google.android.gms.measurement.internal.x3 r0 = (com.google.android.gms.measurement.internal.C3239x3) r0     // Catch:{ SecurityException -> 0x005a }
            boolean r1 = com.google.android.gms.measurement.internal.C3098ka.m8843a()     // Catch:{ SecurityException -> 0x005a }
            if (r1 != 0) goto L_0x0052
            r1 = 0
            com.google.android.gms.measurement.internal.v3<V> r2 = r0.f5744b     // Catch:{ IllegalStateException -> 0x0048 }
            if (r2 == 0) goto L_0x0048
            com.google.android.gms.measurement.internal.v3<V> r2 = r0.f5744b     // Catch:{ IllegalStateException -> 0x0048 }
            java.lang.Object r1 = r2.mo18723a()     // Catch:{ IllegalStateException -> 0x0048 }
        L_0x0048:
            java.lang.Object r2 = com.google.android.gms.measurement.internal.C3239x3.f5742g     // Catch:{ SecurityException -> 0x005a }
            monitor-enter(r2)     // Catch:{ SecurityException -> 0x005a }
            r0.f5748f = r1     // Catch:{ all -> 0x004f }
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            goto L_0x002b
        L_0x004f:
            r4 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            throw r4     // Catch:{ SecurityException -> 0x005a }
        L_0x0052:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException     // Catch:{ SecurityException -> 0x005a }
            java.lang.String r0 = "Refreshing flag cache must be done on a worker thread."
            r4.<init>(r0)     // Catch:{ SecurityException -> 0x005a }
            throw r4     // Catch:{ SecurityException -> 0x005a }
        L_0x005a:
        L_0x005b:
            com.google.android.gms.measurement.internal.v3<V> r4 = r3.f5744b
            if (r4 != 0) goto L_0x0062
            V r4 = r3.f5745c
            return r4
        L_0x0062:
            java.lang.Object r4 = r4.mo18723a()     // Catch:{ SecurityException -> 0x006a, IllegalStateException -> 0x0067 }
            return r4
        L_0x0067:
            V r4 = r3.f5745c
            return r4
        L_0x006a:
            V r4 = r3.f5745c
            return r4
        L_0x006d:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006d }
            throw r0
        L_0x0070:
            monitor-exit(r0)     // Catch:{ all -> 0x0072 }
            throw r4
        L_0x0072:
            r4 = move-exception
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3239x3.mo19389a(java.lang.Object):java.lang.Object");
    }
}
