package com.google.android.gms.maps;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2890a0;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2893c;
import com.google.android.gms.maps.p093i.C2904i;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p145a.p157c.p160b.C3986a;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3993e;

public class SupportMapFragment extends Fragment {

    /* renamed from: P */
    private final C2875b f4772P = new C2875b(super);

    public void onActivityCreated(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(SupportMapFragment.class.getClassLoader());
        }
        super.onActivityCreated(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f4772P.m8086a(activity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4772P.mo23621a(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View a = this.f4772P.mo23618a(layoutInflater, viewGroup, bundle);
        a.setClickable(true);
        return a;
    }

    public void onDestroy() {
        this.f4772P.mo23623b();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f4772P.mo23625c();
        super.onDestroyView();
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitAll().build());
        try {
            super.onInflate(activity, attributeSet, bundle);
            this.f4772P.m8086a(activity);
            GoogleMapOptions a = GoogleMapOptions.m7989a(activity, attributeSet);
            Bundle bundle2 = new Bundle();
            bundle2.putParcelable("MapOptions", a);
            this.f4772P.mo23620a(activity, bundle2, bundle);
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public void onLowMemory() {
        this.f4772P.mo23626d();
        super.onLowMemory();
    }

    public void onPause() {
        this.f4772P.mo23627e();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f4772P.mo23628f();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(SupportMapFragment.class.getClassLoader());
        }
        super.onSaveInstanceState(bundle);
        this.f4772P.mo23624b(bundle);
    }

    public void onStart() {
        super.onStart();
        this.f4772P.mo23629g();
    }

    public void onStop() {
        this.f4772P.mo23630h();
        super.onStop();
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }

    /* renamed from: com.google.android.gms.maps.SupportMapFragment$b */
    static class C2875b extends C3986a<C2874a> {

        /* renamed from: e */
        private final Fragment f4775e;

        /* renamed from: f */
        private C3993e<C2874a> f4776f;

        /* renamed from: g */
        private Activity f4777g;

        /* renamed from: h */
        private final List<C2885e> f4778h = new ArrayList();

        C2875b(Fragment fragment) {
            this.f4775e = fragment;
        }

        /* renamed from: i */
        private final void m8088i() {
            if (this.f4777g != null && this.f4776f != null && mo23619a() == null) {
                try {
                    C2884d.m8123a(this.f4777g);
                    C2893c d = C2892b0.m8145a(this.f4777g).mo18446d(C3992d.m11990a(this.f4777g));
                    if (d != null) {
                        this.f4776f.mo23633a(new C2874a(this.f4775e, d));
                        for (C2885e eVar : this.f4778h) {
                            ((C2874a) mo23619a()).mo18396a(eVar);
                        }
                        this.f4778h.clear();
                    }
                } catch (RemoteException e) {
                    throw new C2934e(e);
                } catch (C2170d unused) {
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo18361a(C3993e<C2874a> eVar) {
            this.f4776f = eVar;
            m8088i();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public final void m8086a(Activity activity) {
            this.f4777g = activity;
            m8088i();
        }
    }

    /* renamed from: com.google.android.gms.maps.SupportMapFragment$a */
    static class C2874a implements C2904i {

        /* renamed from: a */
        private final Fragment f4773a;

        /* renamed from: b */
        private final C2893c f4774b;

        public C2874a(Fragment fragment, C2893c cVar) {
            C2258v.m5629a(cVar);
            this.f4774b = cVar;
            C2258v.m5629a(fragment);
            this.f4773a = fragment;
        }

        /* renamed from: a */
        public final void mo18350a(Activity activity, Bundle bundle, Bundle bundle2) {
            GoogleMapOptions googleMapOptions = (GoogleMapOptions) bundle.getParcelable("MapOptions");
            try {
                Bundle bundle3 = new Bundle();
                C2890a0.m8135a(bundle2, bundle3);
                this.f4774b.mo18433a(C3992d.m11990a(activity), googleMapOptions, bundle3);
                C2890a0.m8135a(bundle3, bundle2);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18354b(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                Bundle arguments = this.f4773a.getArguments();
                if (arguments != null && arguments.containsKey("MapOptions")) {
                    C2890a0.m8136a(bundle2, "MapOptions", arguments.getParcelable("MapOptions"));
                }
                this.f4774b.mo18435b(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: c */
        public final void mo18355c() {
            try {
                this.f4774b.mo18436c();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: d */
        public final void mo18356d() {
            try {
                this.f4774b.mo18437d();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: g */
        public final void mo18357g() {
            try {
                this.f4774b.mo18438g();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onLowMemory() {
            try {
                this.f4774b.onLowMemory();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onResume() {
            try {
                this.f4774b.onResume();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onStart() {
            try {
                this.f4774b.onStart();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                C3988b a = this.f4774b.mo18430a(C3992d.m11990a(layoutInflater), C3992d.m11990a(viewGroup), bundle2);
                C2890a0.m8135a(bundle2, bundle);
                return (View) C3992d.m11991e(a);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18353b() {
            try {
                this.f4774b.mo18434b();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18351a(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4774b.mo18431a(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18396a(C2885e eVar) {
            try {
                this.f4774b.mo18432a(new C2958p(this, eVar));
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }
    }
}
