package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.l5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3105l5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3118m6 f5322P;

    /* renamed from: Q */
    private final /* synthetic */ C3081j5 f5323Q;

    C3105l5(C3081j5 j5Var, C3118m6 m6Var) {
        this.f5323Q = j5Var;
        this.f5322P = m6Var;
    }

    public final void run() {
        this.f5323Q.m8755a(this.f5322P);
        this.f5323Q.mo19083a();
    }
}
