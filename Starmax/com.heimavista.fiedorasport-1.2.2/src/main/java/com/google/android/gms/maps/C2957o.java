package com.google.android.gms.maps;

import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.p093i.C2897e;
import com.google.android.gms.maps.p093i.C2917v;

/* renamed from: com.google.android.gms.maps.o */
final class C2957o extends C2917v {

    /* renamed from: a */
    private final /* synthetic */ C2886f f4916a;

    C2957o(StreetViewPanoramaView.C2872a aVar, C2886f fVar) {
        this.f4916a = fVar;
    }

    /* renamed from: a */
    public final void mo18489a(C2897e eVar) {
        this.f4916a.mo18412a(new C2887g(eVar));
    }
}
