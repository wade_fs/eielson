package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.common.server.converter.a */
public final class C2294a implements Parcelable.Creator<zaa> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        StringToIntConverter stringToIntConverter = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 != 2) {
                C2248a.m5550F(parcel, a);
            } else {
                stringToIntConverter = (StringToIntConverter) C2248a.m5553a(parcel, a, StringToIntConverter.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zaa(i, stringToIntConverter);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zaa[i];
    }
}
