package com.google.android.gms.internal.measurement;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Size;
import com.facebook.internal.ServerProtocol;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.api.internal.C2072f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2314e;
import com.google.android.gms.common.util.C2317h;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.google.android.gms.measurement.internal.C3130n6;
import com.google.android.gms.measurement.p094a.C2963a;
import com.tencent.bugly.Bugly;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.internal.measurement.gd */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
public class C2525gd {

    /* renamed from: h */
    private static volatile C2525gd f4179h = null;

    /* renamed from: i */
    private static Boolean f4180i = null;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static Boolean f4181j = null;

    /* renamed from: k */
    private static boolean f4182k = false;

    /* renamed from: l */
    private static Boolean f4183l = null;

    /* renamed from: m */
    private static String f4184m = "use_dynamite_api";

    /* renamed from: n */
    private static String f4185n = "allow_remote_dynamite";
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final String f4186a;

    /* renamed from: b */
    protected final C2314e f4187b;

    /* renamed from: c */
    private final ExecutorService f4188c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public List<Pair<C3130n6, Object>> f4189d;

    /* renamed from: e */
    private int f4190e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f4191f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public C2574jb f4192g;

    /* renamed from: com.google.android.gms.internal.measurement.gd$a */
    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
    abstract class C2526a implements Runnable {

        /* renamed from: P */
        final long f4193P;

        /* renamed from: Q */
        final long f4194Q;

        /* renamed from: R */
        private final boolean f4195R;

        C2526a(C2525gd gdVar) {
            this(true);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract void mo17293a();

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void mo17345b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.Exception, boolean, boolean):void
         arg types: [com.google.android.gms.internal.measurement.gd, java.lang.Exception, int, boolean]
         candidates:
          com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
          com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.Exception, boolean, boolean):void */
        public void run() {
            if (C2525gd.this.f4191f) {
                mo17345b();
                return;
            }
            try {
                mo17293a();
            } catch (Exception e) {
                C2525gd.this.m6391a(e, false, this.f4195R);
                mo17345b();
            }
        }

        C2526a(boolean z) {
            this.f4193P = C2525gd.this.f4187b.mo17132a();
            this.f4194Q = C2525gd.this.f4187b.elapsedRealtime();
            this.f4195R = z;
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.gd$b */
    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
    class C2527b implements Application.ActivityLifecycleCallbacks {
        C2527b() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            C2525gd.this.m6388a(new C2715t(this, activity, bundle));
        }

        public final void onActivityDestroyed(Activity activity) {
            C2525gd.this.m6388a(new C2793y(this, activity));
        }

        public final void onActivityPaused(Activity activity) {
            C2525gd.this.m6388a(new C2777x(this, activity));
        }

        public final void onActivityResumed(Activity activity) {
            C2525gd.this.m6388a(new C2732u(this, activity));
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            C2538h9 h9Var = new C2538h9();
            C2525gd.this.m6388a(new C2809z(this, activity, h9Var));
            Bundle b = h9Var.mo17564b(50);
            if (b != null) {
                bundle.putAll(b);
            }
        }

        public final void onActivityStarted(Activity activity) {
            C2525gd.this.m6388a(new C2747v(this, activity));
        }

        public final void onActivityStopped(Activity activity) {
            C2525gd.this.m6388a(new C2762w(this, activity));
        }
    }

    private C2525gd(Context context, String str, String str2, String str3, Bundle bundle) {
        if (str == null || !m6399b(str2, str3)) {
            this.f4186a = "FA";
        } else {
            this.f4186a = str;
        }
        this.f4187b = C2317h.m5780c();
        this.f4188c = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue());
        new C2963a(this);
        boolean z = false;
        if (!(!m6405f(context) || m6407g())) {
            this.f4191f = true;
            Log.w(this.f4186a, "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection.");
            return;
        }
        if (!m6399b(str2, str3)) {
            if (str2 == null || str3 == null) {
                if ((str2 == null) ^ (str3 == null ? true : z)) {
                    Log.w(this.f4186a, "Specified origin or custom app id is null. Both parameters will be ignored.");
                }
            } else {
                Log.v(this.f4186a, "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy");
            }
        }
        m6388a(new C2428b(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            Log.w(this.f4186a, "Unable to register lifecycle notifications. Application null.");
        } else {
            application.registerActivityLifecycleCallbacks(new C2527b());
        }
    }

    /* renamed from: a */
    public static C2525gd m6384a(@NonNull Context context) {
        return m6385a(context, (String) null, (String) null, (String) null, (Bundle) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static boolean m6399b(String str, String str2) {
        return (str2 == null || str == null || m6407g()) ? false : true;
    }

    /* renamed from: f */
    private static boolean m6405f(Context context) {
        try {
            C2072f.m4879a(context);
            if (C2072f.m4881a() != null) {
                return true;
            }
            return false;
        } catch (IllegalStateException unused) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public static int m6406g(Context context) {
        return DynamiteModule.m5841b(context, ModuleDescriptor.MODULE_ID);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public static int m6408h(Context context) {
        return DynamiteModule.m5835a(context, ModuleDescriptor.MODULE_ID);
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public static void m6409i(Context context) {
        synchronized (C2525gd.class) {
            try {
                if (f4180i != null && f4181j != null) {
                    return;
                }
                if (m6394a(context, "app_measurement_internal_disable_startup_flags")) {
                    f4180i = false;
                    f4181j = false;
                    return;
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
                f4180i = Boolean.valueOf(sharedPreferences.getBoolean(f4184m, false));
                f4181j = Boolean.valueOf(sharedPreferences.getBoolean(f4185n, false));
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.remove(f4184m);
                edit.remove(f4185n);
                edit.apply();
            } catch (Exception e) {
                Log.e("FA", "Exception reading flag from SharedPreferences.", e);
                f4180i = false;
                f4181j = false;
            }
        }
    }

    /* renamed from: c */
    public final long mo17524c() {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2558j(this, h9Var));
        Long l = (Long) C2538h9.m6453a(h9Var.mo17564b(500), Long.class);
        if (l != null) {
            return l.longValue();
        }
        long nextLong = new Random(System.nanoTime() ^ this.f4187b.mo17132a()).nextLong();
        int i = this.f4190e + 1;
        this.f4190e = i;
        return nextLong + ((long) i);
    }

    /* renamed from: d */
    public final String mo17525d() {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2543i(this, h9Var));
        return h9Var.mo17563a(500);
    }

    /* renamed from: e */
    public final String mo17526e() {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2590l(this, h9Var));
        return h9Var.mo17563a(500);
    }

    /* renamed from: a */
    public static C2525gd m6385a(Context context, String str, String str2, String str3, Bundle bundle) {
        C2258v.m5629a(context);
        if (f4179h == null) {
            synchronized (C2525gd.class) {
                if (f4179h == null) {
                    f4179h = new C2525gd(context, str, str2, str3, bundle);
                }
            }
        }
        return f4179h;
    }

    /* renamed from: g */
    private static boolean m6407g() {
        try {
            Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* renamed from: b */
    public final void mo17522b(String str, String str2, Bundle bundle) {
        m6388a(new C2557id(this, str, str2, bundle));
    }

    /* renamed from: b */
    public final void mo17521b(String str) {
        m6388a(new C2493f(this, str));
    }

    /* renamed from: b */
    public final String mo17520b() {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2509g(this, h9Var));
        return h9Var.mo17563a(50);
    }

    /* renamed from: c */
    public final int mo17523c(String str) {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2625n(this, str, h9Var));
        Integer num = (Integer) C2538h9.m6453a(h9Var.mo17564b(10000), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    /* renamed from: b */
    public static boolean m6398b(Context context) {
        m6409i(context);
        synchronized (C2525gd.class) {
            if (!f4182k) {
                try {
                    String str = (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, "measurement.dynamite.enabled", "");
                    if (ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(str)) {
                        f4183l = true;
                    } else if (Bugly.SDK_IS_DEV.equals(str)) {
                        f4183l = false;
                    } else {
                        f4183l = null;
                    }
                    f4182k = true;
                } catch (Exception e) {
                    try {
                        Log.e("FA", "Unable to call SystemProperties.get()", e);
                        f4183l = null;
                    } finally {
                        f4182k = true;
                    }
                }
            }
        }
        Boolean bool = f4183l;
        if (bool == null) {
            bool = f4180i;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6388a(C2526a aVar) {
        this.f4188c.execute(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gd.a(java.lang.Exception, boolean, boolean):void
     arg types: [com.google.android.gms.dynamite.DynamiteModule$a, int, int]
     candidates:
      com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.String, java.lang.String):boolean
      com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.internal.measurement.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.measurement.gd.a(java.lang.Exception, boolean, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final C2574jb mo17508a(Context context, boolean z) {
        DynamiteModule.C2339b bVar;
        if (z) {
            try {
                bVar = DynamiteModule.f3889l;
            } catch (DynamiteModule.C2338a e) {
                m6391a((Exception) e, true, false);
                return null;
            }
        } else {
            bVar = DynamiteModule.f3887j;
        }
        return C2554ia.asInterface(DynamiteModule.m5837a(context, bVar, ModuleDescriptor.MODULE_ID).mo17143a("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6391a(Exception exc, boolean z, boolean z2) {
        this.f4191f |= z;
        if (z) {
            Log.w(this.f4186a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            mo17512a(5, "Error with data collection. Data lost.", exc, (Object) null, (Object) null);
        }
        Log.w(this.f4186a, "Error with data collection. Data lost.", exc);
    }

    /* renamed from: a */
    public final void mo17516a(@NonNull String str, Bundle bundle) {
        m6392a(null, str, bundle, false, true, null);
    }

    /* renamed from: a */
    public final void mo17517a(String str, String str2, Bundle bundle) {
        m6392a(str, str2, bundle, true, true, null);
    }

    /* renamed from: a */
    private final void m6392a(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l) {
        m6388a(new C2685r(this, l, str, str2, bundle, z, z2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.Exception, boolean, boolean):void
      com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    /* renamed from: a */
    public final void mo17518a(String str, String str2, Object obj) {
        m6393a(str, str2, obj, true);
    }

    /* renamed from: a */
    private final void m6393a(String str, String str2, Object obj, boolean z) {
        m6388a(new C2656p(this, str, str2, obj, z));
    }

    /* renamed from: a */
    public final void mo17514a(Bundle bundle) {
        m6388a(new C2700s(this, bundle));
    }

    /* renamed from: a */
    public final List<Bundle> mo17510a(String str, String str2) {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2444c(this, str, str2, h9Var));
        List<Bundle> list = (List) C2538h9.m6453a(h9Var.mo17564b(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    /* renamed from: a */
    public final void mo17513a(Activity activity, String str, String str2) {
        m6388a(new C2460d(this, activity, str, str2));
    }

    /* renamed from: a */
    public final void mo17515a(String str) {
        m6388a(new C2476e(this, str));
    }

    /* renamed from: a */
    public final String mo17509a() {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2528h(this, h9Var));
        return h9Var.mo17563a(500);
    }

    /* renamed from: a */
    public final Map<String, Object> mo17511a(String str, String str2, boolean z) {
        C2538h9 h9Var = new C2538h9();
        m6388a(new C2576k(this, str, str2, z, h9Var));
        Bundle b = h9Var.mo17564b(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        if (b == null || b.size() == 0) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(b.size());
        for (String str3 : b.keySet()) {
            Object obj = b.get(str3);
            if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                hashMap.put(str3, obj);
            }
        }
        return hashMap;
    }

    /* renamed from: a */
    public final void mo17512a(int i, String str, Object obj, Object obj2, Object obj3) {
        m6388a(new C2610m(this, false, 5, str, obj, null, null));
    }

    /* renamed from: a */
    public final void mo17519a(boolean z) {
        m6388a(new C2640o(this, z));
    }

    /* renamed from: a */
    private static boolean m6394a(Context context, @Size(min = 1) String str) {
        C2258v.m5639b(str);
        try {
            ApplicationInfo a = C2283c.m5685a(context).mo17055a(context.getPackageName(), 128);
            if (a != null) {
                if (a.metaData != null) {
                    return a.metaData.getBoolean(str);
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }
}
