package com.google.android.gms.internal.measurement;

import android.net.Uri;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.s1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2703s1 {

    /* renamed from: a */
    private final Map<String, Map<String, String>> f4473a;

    C2703s1(Map<String, Map<String, String>> map) {
        this.f4473a = map;
    }

    /* renamed from: a */
    public final String mo17872a(Uri uri, String str, String str2, String str3) {
        if (uri != null) {
            str = uri.toString();
        } else if (str == null) {
            return null;
        }
        Map map = this.f4473a.get(str);
        if (map == null) {
            return null;
        }
        if (str2 != null) {
            String valueOf = String.valueOf(str2);
            String valueOf2 = String.valueOf(str3);
            str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        }
        return (String) map.get(str3);
    }
}
