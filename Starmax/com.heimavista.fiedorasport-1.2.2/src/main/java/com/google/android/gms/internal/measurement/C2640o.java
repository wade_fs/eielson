package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.o */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2640o extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ boolean f4364T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4365U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2640o(C2525gd gdVar, boolean z) {
        super(gdVar);
        this.f4365U = gdVar;
        this.f4364T = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4365U.f4192g.setDataCollectionEnabled(this.f4364T);
    }
}
