package com.google.android.gms.maps.p093i;

import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;
import p119e.p144d.p145a.p157c.p161c.p165d.C4040i;
import p119e.p144d.p145a.p157c.p161c.p165d.C4041j;

/* renamed from: com.google.android.gms.maps.i.d0 */
public final class C2896d0 extends C4032a implements C2894c0 {
    C2896d0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2895d mo18442a(p119e.p144d.p145a.p157c.p160b.C3988b r3, com.google.android.gms.maps.GoogleMapOptions r4) {
        /*
            r2 = this;
            android.os.Parcel r0 = r2.mo23664a()
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12043a(r0, r3)
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12044a(r0, r4)
            r3 = 3
            android.os.Parcel r3 = r2.mo23665a(r3, r0)
            android.os.IBinder r4 = r3.readStrongBinder()
            if (r4 != 0) goto L_0x0017
            r4 = 0
            goto L_0x002b
        L_0x0017:
            java.lang.String r0 = "com.google.android.gms.maps.internal.IMapViewDelegate"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.maps.p093i.C2895d
            if (r1 == 0) goto L_0x0025
            r4 = r0
            com.google.android.gms.maps.i.d r4 = (com.google.android.gms.maps.p093i.C2895d) r4
            goto L_0x002b
        L_0x0025:
            com.google.android.gms.maps.i.g0 r0 = new com.google.android.gms.maps.i.g0
            r0.<init>(r4)
            r4 = r0
        L_0x002b:
            r3.recycle()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2896d0.mo18442a(e.d.a.c.b.b, com.google.android.gms.maps.GoogleMapOptions):com.google.android.gms.maps.i.d");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2899f mo18445c(p119e.p144d.p145a.p157c.p160b.C3988b r4) {
        /*
            r3 = this;
            android.os.Parcel r0 = r3.mo23664a()
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12043a(r0, r4)
            r4 = 8
            android.os.Parcel r4 = r3.mo23665a(r4, r0)
            android.os.IBinder r0 = r4.readStrongBinder()
            if (r0 != 0) goto L_0x0015
            r0 = 0
            goto L_0x0029
        L_0x0015:
            java.lang.String r1 = "com.google.android.gms.maps.internal.IStreetViewPanoramaFragmentDelegate"
            android.os.IInterface r1 = r0.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.maps.p093i.C2899f
            if (r2 == 0) goto L_0x0023
            r0 = r1
            com.google.android.gms.maps.i.f r0 = (com.google.android.gms.maps.p093i.C2899f) r0
            goto L_0x0029
        L_0x0023:
            com.google.android.gms.maps.i.x r1 = new com.google.android.gms.maps.i.x
            r1.<init>(r0)
            r0 = r1
        L_0x0029:
            r4.recycle()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2896d0.mo18445c(e.d.a.c.b.b):com.google.android.gms.maps.i.f");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2893c mo18446d(p119e.p144d.p145a.p157c.p160b.C3988b r4) {
        /*
            r3 = this;
            android.os.Parcel r0 = r3.mo23664a()
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12043a(r0, r4)
            r4 = 2
            android.os.Parcel r4 = r3.mo23665a(r4, r0)
            android.os.IBinder r0 = r4.readStrongBinder()
            if (r0 != 0) goto L_0x0014
            r0 = 0
            goto L_0x0028
        L_0x0014:
            java.lang.String r1 = "com.google.android.gms.maps.internal.IMapFragmentDelegate"
            android.os.IInterface r1 = r0.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.maps.p093i.C2893c
            if (r2 == 0) goto L_0x0022
            r0 = r1
            com.google.android.gms.maps.i.c r0 = (com.google.android.gms.maps.p093i.C2893c) r0
            goto L_0x0028
        L_0x0022:
            com.google.android.gms.maps.i.f0 r1 = new com.google.android.gms.maps.i.f0
            r1.<init>(r0)
            r0 = r1
        L_0x0028:
            r4.recycle()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2896d0.mo18446d(e.d.a.c.b.b):com.google.android.gms.maps.i.c");
    }

    /* renamed from: h */
    public final C4040i mo18447h() {
        Parcel a = mo23665a(5, mo23664a());
        C4040i a2 = C4041j.m12049a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: t */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2889a mo18448t() {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.mo23664a()
            r1 = 4
            android.os.Parcel r0 = r4.mo23665a(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0011
            r1 = 0
            goto L_0x0025
        L_0x0011:
            java.lang.String r2 = "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.maps.p093i.C2889a
            if (r3 == 0) goto L_0x001f
            r1 = r2
            com.google.android.gms.maps.i.a r1 = (com.google.android.gms.maps.p093i.C2889a) r1
            goto L_0x0025
        L_0x001f:
            com.google.android.gms.maps.i.t r2 = new com.google.android.gms.maps.i.t
            r2.<init>(r1)
            r1 = r2
        L_0x0025:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2896d0.mo18448t():com.google.android.gms.maps.i.a");
    }

    /* renamed from: a */
    public final void mo18444a(C3988b bVar, int i) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, bVar);
        a.writeInt(i);
        mo23667b(6, a);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2901g mo18443a(p119e.p144d.p145a.p157c.p160b.C3988b r3, com.google.android.gms.maps.StreetViewPanoramaOptions r4) {
        /*
            r2 = this;
            android.os.Parcel r0 = r2.mo23664a()
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12043a(r0, r3)
            p119e.p144d.p145a.p157c.p161c.p165d.C4039h.m12044a(r0, r4)
            r3 = 7
            android.os.Parcel r3 = r2.mo23665a(r3, r0)
            android.os.IBinder r4 = r3.readStrongBinder()
            if (r4 != 0) goto L_0x0017
            r4 = 0
            goto L_0x002b
        L_0x0017:
            java.lang.String r0 = "com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.maps.p093i.C2901g
            if (r1 == 0) goto L_0x0025
            r4 = r0
            com.google.android.gms.maps.i.g r4 = (com.google.android.gms.maps.p093i.C2901g) r4
            goto L_0x002b
        L_0x0025:
            com.google.android.gms.maps.i.y r0 = new com.google.android.gms.maps.i.y
            r0.<init>(r4)
            r4 = r0
        L_0x002b:
            r3.recycle()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2896d0.mo18443a(e.d.a.c.b.b, com.google.android.gms.maps.StreetViewPanoramaOptions):com.google.android.gms.maps.i.g");
    }
}
