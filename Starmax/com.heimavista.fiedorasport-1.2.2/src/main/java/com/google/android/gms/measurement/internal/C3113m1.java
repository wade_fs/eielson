package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2682qa;

/* renamed from: com.google.android.gms.measurement.internal.m1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3113m1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5340a = new C3113m1();

    private C3113m1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2682qa.m7090b());
    }
}
