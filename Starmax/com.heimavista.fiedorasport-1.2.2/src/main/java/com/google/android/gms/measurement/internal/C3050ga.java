package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2445c0;

/* renamed from: com.google.android.gms.measurement.internal.ga */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3050ga extends C3086ja {

    /* renamed from: g */
    private C2445c0 f5167g;

    /* renamed from: h */
    private final /* synthetic */ C3002ca f5168h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3050ga(C3002ca caVar, String str, int i, C2445c0 c0Var) {
        super(str, i);
        this.f5168h = caVar;
        this.f5167g = c0Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final int mo19038a() {
        return this.f5167g.mo17348o();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo19040b() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final boolean mo19041c() {
        return this.f5167g.mo17352t();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r10v3, types: [java.lang.Integer] */
    /* JADX WARN: Type inference failed for: r10v9, types: [java.lang.Integer] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0135, code lost:
        if (r8.booleanValue() == false) goto L_0x0137;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0137, code lost:
        r10 = false;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03d5  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03d8  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x03e0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03e1  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo19039a(java.lang.Long r18, java.lang.Long r19, com.google.android.gms.internal.measurement.C2701s0 r20, long r21, com.google.android.gms.measurement.internal.C3087k r23, boolean r24) {
        /*
            r17 = this;
            r0 = r17
            com.google.android.gms.measurement.internal.ca r1 = r0.f5168h
            com.google.android.gms.measurement.internal.la r1 = r1.mo19013h()
            java.lang.String r2 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5457l0
            boolean r1 = r1.mo19152d(r2, r3)
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.la r2 = r2.mo19013h()
            java.lang.String r3 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5459m0
            boolean r2 = r2.mo19152d(r3, r4)
            boolean r3 = com.google.android.gms.internal.measurement.C2606l9.m6736b()
            r4 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r4)
            r6 = 0
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r6)
            if (r3 == 0) goto L_0x0040
            com.google.android.gms.measurement.internal.ca r3 = r0.f5168h
            com.google.android.gms.measurement.internal.la r3 = r3.mo19013h()
            java.lang.String r8 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.C3135o.f5479w0
            boolean r3 = r3.mo19152d(r8, r9)
            if (r3 == 0) goto L_0x0040
            r3 = 1
            goto L_0x0041
        L_0x0040:
            r3 = 0
        L_0x0041:
            if (r2 == 0) goto L_0x0052
            if (r1 == 0) goto L_0x0052
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17357y()
            if (r2 == 0) goto L_0x0052
            r2 = r23
            long r8 = r2.f5282e
            goto L_0x0054
        L_0x0052:
            r8 = r21
        L_0x0054:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            r10 = 2
            boolean r2 = r2.mo19000a(r10)
            r10 = 0
            if (r2 == 0) goto L_0x00b6
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            int r11 = r0.f5273b
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            com.google.android.gms.internal.measurement.c0 r12 = r0.f5167g
            boolean r12 = r12.mo17347n()
            if (r12 == 0) goto L_0x0085
            com.google.android.gms.internal.measurement.c0 r12 = r0.f5167g
            int r12 = r12.mo17348o()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            goto L_0x0086
        L_0x0085:
            r12 = r10
        L_0x0086:
            com.google.android.gms.measurement.internal.ca r13 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r13 = r13.mo19010e()
            com.google.android.gms.internal.measurement.c0 r14 = r0.f5167g
            java.lang.String r14 = r14.mo17349p()
            java.lang.String r13 = r13.mo18822a(r14)
            java.lang.String r14 = "Evaluating filter. audience, filter, event"
            r2.mo19045a(r14, r11, r12, r13)
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            com.google.android.gms.measurement.internal.ca r11 = r0.f5168h
            com.google.android.gms.measurement.internal.v9 r11 = r11.mo19171i()
            com.google.android.gms.internal.measurement.c0 r12 = r0.f5167g
            java.lang.String r11 = r11.mo19353a(r12)
            java.lang.String r12 = "Filter definition"
            r2.mo19043a(r12, r11)
        L_0x00b6:
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17347n()
            if (r2 == 0) goto L_0x0422
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            int r2 = r2.mo17348o()
            r11 = 256(0x100, float:3.59E-43)
            if (r2 <= r11) goto L_0x00ca
            goto L_0x0422
        L_0x00ca:
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17354v()
            com.google.android.gms.internal.measurement.c0 r11 = r0.f5167g
            boolean r11 = r11.mo17355w()
            if (r1 == 0) goto L_0x00e2
            com.google.android.gms.internal.measurement.c0 r1 = r0.f5167g
            boolean r1 = r1.mo17357y()
            if (r1 == 0) goto L_0x00e2
            r1 = 1
            goto L_0x00e3
        L_0x00e2:
            r1 = 0
        L_0x00e3:
            if (r2 != 0) goto L_0x00ec
            if (r11 != 0) goto L_0x00ec
            if (r1 == 0) goto L_0x00ea
            goto L_0x00ec
        L_0x00ea:
            r1 = 0
            goto L_0x00ed
        L_0x00ec:
            r1 = 1
        L_0x00ed:
            if (r24 == 0) goto L_0x0119
            if (r1 != 0) goto L_0x0119
            com.google.android.gms.measurement.internal.ca r1 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            int r2 = r0.f5273b
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            com.google.android.gms.internal.measurement.c0 r3 = r0.f5167g
            boolean r3 = r3.mo17347n()
            if (r3 == 0) goto L_0x0113
            com.google.android.gms.internal.measurement.c0 r3 = r0.f5167g
            int r3 = r3.mo17348o()
            java.lang.Integer r10 = java.lang.Integer.valueOf(r3)
        L_0x0113:
            java.lang.String r3 = "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            r1.mo19044a(r3, r2, r10)
            return r4
        L_0x0119:
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            java.lang.String r11 = r20.mo17849p()
            boolean r12 = r2.mo17352t()
            if (r12 == 0) goto L_0x013a
            com.google.android.gms.internal.measurement.e0 r12 = r2.mo17353u()
            java.lang.Boolean r8 = com.google.android.gms.measurement.internal.C3086ja.m8815a(r8, r12)
            if (r8 != 0) goto L_0x0131
            goto L_0x03c9
        L_0x0131:
            boolean r8 = r8.booleanValue()
            if (r8 != 0) goto L_0x013a
        L_0x0137:
            r10 = r7
            goto L_0x03c9
        L_0x013a:
            java.util.HashSet r8 = new java.util.HashSet
            r8.<init>()
            java.util.List r9 = r2.mo17350q()
            java.util.Iterator r9 = r9.iterator()
        L_0x0147:
            boolean r12 = r9.hasNext()
            if (r12 == 0) goto L_0x0180
            java.lang.Object r12 = r9.next()
            com.google.android.gms.internal.measurement.d0 r12 = (com.google.android.gms.internal.measurement.C2461d0) r12
            java.lang.String r13 = r12.mo17395v()
            boolean r13 = r13.isEmpty()
            if (r13 == 0) goto L_0x0178
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            java.lang.String r8 = "null or empty param name in filter. event"
            r2.mo19043a(r8, r7)
            goto L_0x03c9
        L_0x0178:
            java.lang.String r12 = r12.mo17395v()
            r8.add(r12)
            goto L_0x0147
        L_0x0180:
            androidx.collection.ArrayMap r9 = new androidx.collection.ArrayMap
            r9.<init>()
            java.util.List r12 = r20.mo17847n()
            java.util.Iterator r12 = r12.iterator()
        L_0x018d:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x021a
            java.lang.Object r13 = r12.next()
            com.google.android.gms.internal.measurement.u0 r13 = (com.google.android.gms.internal.measurement.C2733u0) r13
            java.lang.String r14 = r13.mo17922o()
            boolean r14 = r8.contains(r14)
            if (r14 == 0) goto L_0x018d
            boolean r14 = r13.mo17925s()
            if (r14 == 0) goto L_0x01c1
            java.lang.String r14 = r13.mo17922o()
            boolean r15 = r13.mo17925s()
            if (r15 == 0) goto L_0x01bc
            long r15 = r13.mo17926t()
            java.lang.Long r13 = java.lang.Long.valueOf(r15)
            goto L_0x01bd
        L_0x01bc:
            r13 = r10
        L_0x01bd:
            r9.put(r14, r13)
            goto L_0x018d
        L_0x01c1:
            boolean r14 = r13.mo17927u()
            if (r14 == 0) goto L_0x01df
            java.lang.String r14 = r13.mo17922o()
            boolean r15 = r13.mo17927u()
            if (r15 == 0) goto L_0x01da
            double r15 = r13.mo17928v()
            java.lang.Double r13 = java.lang.Double.valueOf(r15)
            goto L_0x01db
        L_0x01da:
            r13 = r10
        L_0x01db:
            r9.put(r14, r13)
            goto L_0x018d
        L_0x01df:
            boolean r14 = r13.mo17923p()
            if (r14 == 0) goto L_0x01f1
            java.lang.String r14 = r13.mo17922o()
            java.lang.String r13 = r13.mo17924q()
            r9.put(r14, r13)
            goto L_0x018d
        L_0x01f1:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r9 = r13.mo17922o()
            java.lang.String r8 = r8.mo18823b(r9)
            java.lang.String r9 = "Unknown value for param. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x021a:
            java.util.List r2 = r2.mo17350q()
            java.util.Iterator r2 = r2.iterator()
        L_0x0222:
            boolean r8 = r2.hasNext()
            if (r8 == 0) goto L_0x03c8
            java.lang.Object r8 = r2.next()
            com.google.android.gms.internal.measurement.d0 r8 = (com.google.android.gms.internal.measurement.C2461d0) r8
            boolean r12 = r8.mo17392s()
            if (r12 == 0) goto L_0x023c
            boolean r12 = r8.mo17393t()
            if (r12 == 0) goto L_0x023c
            r12 = 1
            goto L_0x023d
        L_0x023c:
            r12 = 0
        L_0x023d:
            java.lang.String r13 = r8.mo17395v()
            boolean r14 = r13.isEmpty()
            if (r14 == 0) goto L_0x0262
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            java.lang.String r8 = "Event has empty param name. event"
            r2.mo19043a(r8, r7)
            goto L_0x03c9
        L_0x0262:
            java.lang.Object r14 = r9.get(r13)
            boolean r15 = r14 instanceof java.lang.Long
            if (r15 == 0) goto L_0x02af
            boolean r15 = r8.mo17390p()
            if (r15 != 0) goto L_0x0295
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18823b(r13)
            java.lang.String r9 = "No number filter for long param. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x0295:
            java.lang.Long r14 = (java.lang.Long) r14
            long r13 = r14.longValue()
            com.google.android.gms.internal.measurement.e0 r8 = r8.mo17391q()
            java.lang.Boolean r8 = com.google.android.gms.measurement.internal.C3086ja.m8815a(r13, r8)
            if (r8 != 0) goto L_0x02a7
            goto L_0x03c9
        L_0x02a7:
            boolean r8 = r8.booleanValue()
            if (r8 != r12) goto L_0x0222
            goto L_0x0137
        L_0x02af:
            boolean r15 = r14 instanceof java.lang.Double
            if (r15 == 0) goto L_0x02f8
            boolean r15 = r8.mo17390p()
            if (r15 != 0) goto L_0x02de
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18823b(r13)
            java.lang.String r9 = "No number filter for double param. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x02de:
            java.lang.Double r14 = (java.lang.Double) r14
            double r13 = r14.doubleValue()
            com.google.android.gms.internal.measurement.e0 r8 = r8.mo17391q()
            java.lang.Boolean r8 = com.google.android.gms.measurement.internal.C3086ja.m8814a(r13, r8)
            if (r8 != 0) goto L_0x02f0
            goto L_0x03c9
        L_0x02f0:
            boolean r8 = r8.booleanValue()
            if (r8 != r12) goto L_0x0222
            goto L_0x0137
        L_0x02f8:
            boolean r15 = r14 instanceof java.lang.String
            if (r15 == 0) goto L_0x037d
            boolean r15 = r8.mo17388n()
            if (r15 == 0) goto L_0x0313
            java.lang.String r14 = (java.lang.String) r14
            com.google.android.gms.internal.measurement.g0 r8 = r8.mo17389o()
            com.google.android.gms.measurement.internal.ca r13 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r13 = r13.mo19015l()
            java.lang.Boolean r8 = com.google.android.gms.measurement.internal.C3086ja.m8819a(r14, r8, r13)
            goto L_0x0329
        L_0x0313:
            boolean r15 = r8.mo17390p()
            if (r15 == 0) goto L_0x0359
            java.lang.String r14 = (java.lang.String) r14
            boolean r15 = com.google.android.gms.measurement.internal.C3223v9.m9272a(r14)
            if (r15 == 0) goto L_0x0335
            com.google.android.gms.internal.measurement.e0 r8 = r8.mo17391q()
            java.lang.Boolean r8 = com.google.android.gms.measurement.internal.C3086ja.m8817a(r14, r8)
        L_0x0329:
            if (r8 != 0) goto L_0x032d
            goto L_0x03c9
        L_0x032d:
            boolean r8 = r8.booleanValue()
            if (r8 != r12) goto L_0x0222
            goto L_0x0137
        L_0x0335:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18823b(r13)
            java.lang.String r9 = "Invalid param value for number filter. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x0359:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18823b(r13)
            java.lang.String r9 = "No filter for String param. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x037d:
            if (r14 != 0) goto L_0x03a4
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r9 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r9 = r9.mo19010e()
            java.lang.String r9 = r9.mo18823b(r13)
            java.lang.String r10 = "Missing param for filter. event, param"
            r2.mo19044a(r10, r8, r9)
            goto L_0x0137
        L_0x03a4:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            com.google.android.gms.measurement.internal.ca r7 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19010e()
            java.lang.String r7 = r7.mo18822a(r11)
            com.google.android.gms.measurement.internal.ca r8 = r0.f5168h
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19010e()
            java.lang.String r8 = r8.mo18823b(r13)
            java.lang.String r9 = "Unknown param type. event, param"
            r2.mo19044a(r9, r7, r8)
            goto L_0x03c9
        L_0x03c8:
            r10 = r5
        L_0x03c9:
            com.google.android.gms.measurement.internal.ca r2 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            if (r10 != 0) goto L_0x03d8
            java.lang.String r7 = "null"
            goto L_0x03d9
        L_0x03d8:
            r7 = r10
        L_0x03d9:
            java.lang.String r8 = "Event filter result"
            r2.mo19043a(r8, r7)
            if (r10 != 0) goto L_0x03e1
            return r6
        L_0x03e1:
            r0.f5274c = r5
            boolean r2 = r10.booleanValue()
            if (r2 != 0) goto L_0x03ea
            return r4
        L_0x03ea:
            r0.f5275d = r5
            if (r1 == 0) goto L_0x0421
            boolean r1 = r20.mo17850q()
            if (r1 == 0) goto L_0x0421
            long r1 = r20.mo17851s()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17355w()
            if (r2 == 0) goto L_0x0413
            if (r3 == 0) goto L_0x0410
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17352t()
            if (r2 == 0) goto L_0x0410
            r1 = r18
        L_0x0410:
            r0.f5277f = r1
            goto L_0x0421
        L_0x0413:
            if (r3 == 0) goto L_0x041f
            com.google.android.gms.internal.measurement.c0 r2 = r0.f5167g
            boolean r2 = r2.mo17352t()
            if (r2 == 0) goto L_0x041f
            r1 = r19
        L_0x041f:
            r0.f5276e = r1
        L_0x0421:
            return r4
        L_0x0422:
            com.google.android.gms.measurement.internal.ca r1 = r0.f5168h
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19004w()
            java.lang.String r2 = r0.f5272a
            java.lang.Object r2 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r2)
            com.google.android.gms.internal.measurement.c0 r3 = r0.f5167g
            boolean r3 = r3.mo17347n()
            if (r3 == 0) goto L_0x0444
            com.google.android.gms.internal.measurement.c0 r3 = r0.f5167g
            int r3 = r3.mo17348o()
            java.lang.Integer r10 = java.lang.Integer.valueOf(r3)
        L_0x0444:
            java.lang.String r3 = java.lang.String.valueOf(r10)
            java.lang.String r5 = "Invalid event filter ID. appId, id"
            r1.mo19044a(r5, r2, r3)
            com.google.android.gms.measurement.internal.ca r1 = r0.f5168h
            com.google.android.gms.measurement.internal.la r1 = r1.mo19013h()
            java.lang.String r2 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5473t0
            boolean r1 = r1.mo19152d(r2, r3)
            if (r1 == 0) goto L_0x045e
            return r6
        L_0x045e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3050ga.mo19039a(java.lang.Long, java.lang.Long, com.google.android.gms.internal.measurement.s0, long, com.google.android.gms.measurement.internal.k, boolean):boolean");
    }
}
