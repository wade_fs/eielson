package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2819z8;

/* renamed from: com.google.android.gms.measurement.internal.c2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2994c2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5001a = new C2994c2();

    private C2994c2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2819z8.m7931b());
    }
}
