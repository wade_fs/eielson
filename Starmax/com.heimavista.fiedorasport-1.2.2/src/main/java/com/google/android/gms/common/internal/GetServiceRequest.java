package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class GetServiceRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<GetServiceRequest> CREATOR = new C2241p0();

    /* renamed from: P */
    private final int f3613P;

    /* renamed from: Q */
    private final int f3614Q;

    /* renamed from: R */
    private int f3615R;

    /* renamed from: S */
    String f3616S;

    /* renamed from: T */
    IBinder f3617T;

    /* renamed from: U */
    Scope[] f3618U;

    /* renamed from: V */
    Bundle f3619V;

    /* renamed from: W */
    Account f3620W;

    /* renamed from: X */
    Feature[] f3621X;

    /* renamed from: Y */
    Feature[] f3622Y;

    /* renamed from: Z */
    private boolean f3623Z;

    public GetServiceRequest(int i) {
        this.f3613P = 4;
        this.f3615R = C2169c.f3548a;
        this.f3614Q = i;
        this.f3623Z = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Scope[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.Feature[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3613P);
        C2250b.m5591a(parcel, 2, this.f3614Q);
        C2250b.m5591a(parcel, 3, this.f3615R);
        C2250b.m5602a(parcel, 4, this.f3616S, false);
        C2250b.m5594a(parcel, 5, this.f3617T, false);
        C2250b.m5607a(parcel, 6, (Parcelable[]) this.f3618U, i, false);
        C2250b.m5593a(parcel, 7, this.f3619V, false);
        C2250b.m5596a(parcel, 8, (Parcelable) this.f3620W, i, false);
        C2250b.m5607a(parcel, 10, (Parcelable[]) this.f3621X, i, false);
        C2250b.m5607a(parcel, 11, (Parcelable[]) this.f3622Y, i, false);
        C2250b.m5605a(parcel, 12, this.f3623Z);
        C2250b.m5587a(parcel, a);
    }

    GetServiceRequest(int i, int i2, int i3, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, Feature[] featureArr, Feature[] featureArr2, boolean z) {
        this.f3613P = i;
        this.f3614Q = i2;
        this.f3615R = i3;
        if ("com.google.android.gms".equals(str)) {
            this.f3616S = "com.google.android.gms";
        } else {
            this.f3616S = str;
        }
        if (i < 2) {
            this.f3620W = iBinder != null ? C2188a.m5349a(C2231m.C2232a.m5514a(iBinder)) : null;
        } else {
            this.f3617T = iBinder;
            this.f3620W = account;
        }
        this.f3618U = scopeArr;
        this.f3619V = bundle;
        this.f3621X = featureArr;
        this.f3622Y = featureArr2;
        this.f3623Z = z;
    }
}
