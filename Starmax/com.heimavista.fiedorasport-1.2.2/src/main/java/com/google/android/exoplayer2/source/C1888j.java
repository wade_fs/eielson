package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.j */
/* compiled from: lambda */
public final /* synthetic */ class C1888j implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2906P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2907Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSource.MediaPeriodId f2908R;

    public /* synthetic */ C1888j(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f2906P = eventDispatcher;
        this.f2907Q = mediaSourceEventListener;
        this.f2908R = mediaPeriodId;
    }

    public final void run() {
        this.f2906P.mo14937c(this.f2907Q, this.f2908R);
    }
}
