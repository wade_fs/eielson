package com.google.android.gms.measurement.internal;

import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;
import java.util.List;
import java.util.Map;

@WorkerThread
/* renamed from: com.google.android.gms.measurement.internal.o4 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3140o4 implements Runnable {

    /* renamed from: P */
    private final C3104l4 f5490P;

    /* renamed from: Q */
    private final int f5491Q;

    /* renamed from: R */
    private final Throwable f5492R;

    /* renamed from: S */
    private final byte[] f5493S;

    /* renamed from: T */
    private final String f5494T;

    /* renamed from: U */
    private final Map<String, List<String>> f5495U;

    private C3140o4(String str, C3104l4 l4Var, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        C2258v.m5629a(l4Var);
        this.f5490P = l4Var;
        this.f5491Q = i;
        this.f5492R = th;
        this.f5493S = bArr;
        this.f5494T = str;
        this.f5495U = map;
    }

    public final void run() {
        this.f5490P.mo19135a(this.f5494T, this.f5491Q, this.f5492R, this.f5493S, this.f5495U);
    }
}
