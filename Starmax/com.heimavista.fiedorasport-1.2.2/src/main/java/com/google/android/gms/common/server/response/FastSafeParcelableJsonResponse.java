package com.google.android.gms.common.server.response;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;

public abstract class FastSafeParcelableJsonResponse extends FastJsonResponse implements SafeParcelable {
    /* renamed from: a */
    public Object mo17086a(String str) {
        return null;
    }

    /* renamed from: b */
    public boolean mo17087b(String str) {
        return false;
    }

    public final int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!getClass().isInstance(obj)) {
            return false;
        }
        FastJsonResponse fastJsonResponse = (FastJsonResponse) obj;
        for (FastJsonResponse.Field field : mo16320a().values()) {
            if (mo16321b(field)) {
                if (!super.mo16321b(field) || !mo16319a(field).equals(super.mo16319a(field))) {
                    return false;
                }
            } else if (super.mo16321b(field)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        for (FastJsonResponse.Field field : mo16320a().values()) {
            if (mo16321b(field)) {
                i = (i * 31) + mo16319a(field).hashCode();
            }
        }
        return i;
    }
}
