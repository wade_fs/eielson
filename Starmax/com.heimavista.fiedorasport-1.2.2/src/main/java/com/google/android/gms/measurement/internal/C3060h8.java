package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;

/* renamed from: com.google.android.gms.measurement.internal.h8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3060h8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5182P;

    /* renamed from: Q */
    private final /* synthetic */ boolean f5183Q;

    /* renamed from: R */
    private final /* synthetic */ zzv f5184R;

    /* renamed from: S */
    private final /* synthetic */ zzm f5185S;

    /* renamed from: T */
    private final /* synthetic */ zzv f5186T;

    /* renamed from: U */
    private final /* synthetic */ C3232w7 f5187U;

    C3060h8(C3232w7 w7Var, boolean z, boolean z2, zzv zzv, zzm zzm, zzv zzv2) {
        this.f5187U = w7Var;
        this.f5182P = z;
        this.f5183Q = z2;
        this.f5184R = zzv;
        this.f5185S = zzm;
        this.f5186T = zzv2;
    }

    public final void run() {
        C3228w3 d = this.f5187U.f5724d;
        if (d == null) {
            this.f5187U.mo19015l().mo19001t().mo19042a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.f5182P) {
            this.f5187U.mo19381a(d, this.f5183Q ? null : this.f5184R, this.f5185S);
        } else {
            try {
                if (TextUtils.isEmpty(this.f5186T.f5836P)) {
                    d.mo19194a(this.f5184R, this.f5185S);
                } else {
                    d.mo19193a(this.f5184R);
                }
            } catch (RemoteException e) {
                this.f5187U.mo19015l().mo19001t().mo19043a("Failed to send conditional user property to the service", e);
            }
        }
        this.f5187U.m9314J();
    }
}
