package com.google.android.gms.common;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.C2257u0;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2259v0;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.common.o */
abstract class C2288o extends C2259v0 {

    /* renamed from: a */
    private int f3783a;

    protected C2288o(byte[] bArr) {
        C2258v.m5636a(bArr.length == 25);
        this.f3783a = Arrays.hashCode(bArr);
    }

    /* renamed from: b */
    protected static byte[] m5691b(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: H */
    public abstract byte[] mo17067H();

    /* renamed from: e */
    public final C3988b mo17042e() {
        return C3992d.m11990a(mo17067H());
    }

    public boolean equals(Object obj) {
        C3988b e;
        if (obj != null && (obj instanceof C2257u0)) {
            try {
                C2257u0 u0Var = (C2257u0) obj;
                if (u0Var.mo17043f() == hashCode() && (e = u0Var.mo17042e()) != null) {
                    return Arrays.equals(mo17067H(), (byte[]) C3992d.m11991e(e));
                }
                return false;
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            }
        }
        return false;
    }

    /* renamed from: f */
    public final int mo17043f() {
        return hashCode();
    }

    public int hashCode() {
        return this.f3783a;
    }
}
