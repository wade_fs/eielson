package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.MainThread;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.google.android.gms.measurement.internal.C3033f5;
import com.google.android.gms.measurement.internal.C3262z4;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class AppMeasurementReceiver extends WakefulBroadcastReceiver implements C3033f5 {

    /* renamed from: a */
    private C3262z4 f4928a;

    @MainThread
    /* renamed from: a */
    public final void mo18702a(Context context, Intent intent) {
        WakefulBroadcastReceiver.startWakefulService(context, intent);
    }

    @MainThread
    public final void onReceive(Context context, Intent intent) {
        if (this.f4928a == null) {
            this.f4928a = new C3262z4(this);
        }
        this.f4928a.mo19417a(context, intent);
    }

    /* renamed from: a */
    public final BroadcastReceiver.PendingResult mo18701a() {
        return goAsync();
    }
}
