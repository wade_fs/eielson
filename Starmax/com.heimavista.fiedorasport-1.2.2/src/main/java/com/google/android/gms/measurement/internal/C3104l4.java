package com.google.android.gms.measurement.internal;

import androidx.annotation.WorkerThread;
import java.util.List;
import java.util.Map;

@WorkerThread
/* renamed from: com.google.android.gms.measurement.internal.l4 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
interface C3104l4 {
    /* renamed from: a */
    void mo19135a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
}
