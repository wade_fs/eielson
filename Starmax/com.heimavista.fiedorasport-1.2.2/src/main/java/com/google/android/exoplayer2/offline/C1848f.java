package com.google.android.exoplayer2.offline;

import com.google.android.exoplayer2.offline.DownloadManager;

/* renamed from: com.google.android.exoplayer2.offline.f */
/* compiled from: lambda */
public final /* synthetic */ class C1848f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ DownloadManager.Task f2848P;

    /* renamed from: Q */
    private final /* synthetic */ Throwable f2849Q;

    public /* synthetic */ C1848f(DownloadManager.Task task, Throwable th) {
        this.f2848P = task;
        this.f2849Q = th;
    }

    public final void run() {
        this.f2848P.mo14735a(this.f2849Q);
    }
}
