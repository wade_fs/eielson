package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.n7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2634n7 {
    C2634n7() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract int mo17756a(int i, byte[] bArr, int i2, int i3);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract int mo17757a(CharSequence charSequence, byte[] bArr, int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract String mo17758a(byte[] bArr, int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo17778b(byte[] bArr, int i, int i2) {
        return mo17756a(0, bArr, i, i2) == 0;
    }
}
