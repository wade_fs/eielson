package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.p090j.C2283c;

/* renamed from: com.google.android.gms.common.internal.a1 */
public final class C2190a1 {

    /* renamed from: a */
    private static Object f3637a = new Object();

    /* renamed from: b */
    private static boolean f3638b;

    /* renamed from: c */
    private static String f3639c;

    /* renamed from: d */
    private static int f3640d;

    /* renamed from: a */
    public static String m5350a(Context context) {
        m5352c(context);
        return f3639c;
    }

    /* renamed from: b */
    public static int m5351b(Context context) {
        m5352c(context);
        return f3640d;
    }

    /* renamed from: c */
    private static void m5352c(Context context) {
        synchronized (f3637a) {
            if (!f3638b) {
                f3638b = true;
                try {
                    Bundle bundle = C2283c.m5685a(context).mo17055a(context.getPackageName(), 128).metaData;
                    if (bundle != null) {
                        f3639c = bundle.getString("com.google.app.id");
                        f3640d = bundle.getInt("com.google.android.gms.version");
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Log.wtf("MetadataValueReader", "This should never happen.", e);
                }
            }
        }
    }
}
