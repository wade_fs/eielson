package com.google.android.datatransport.runtime.backends;

import androidx.annotation.Nullable;
import com.google.android.datatransport.runtime.backends.C1714f;
import java.util.Arrays;
import p119e.p144d.p145a.p146a.p147i.C3899h;

/* renamed from: com.google.android.datatransport.runtime.backends.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C1707a extends C1714f {

    /* renamed from: a */
    private final Iterable<C3899h> f2688a;

    /* renamed from: b */
    private final byte[] f2689b;

    /* renamed from: a */
    public Iterable<C3899h> mo13534a() {
        return this.f2688a;
    }

    @Nullable
    /* renamed from: b */
    public byte[] mo13535b() {
        return this.f2689b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1714f)) {
            return false;
        }
        C1714f fVar = (C1714f) obj;
        if (this.f2688a.equals(super.mo13534a())) {
            if (Arrays.equals(this.f2689b, fVar instanceof C1707a ? ((C1707a) fVar).f2689b : super.mo13535b())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return ((this.f2688a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f2689b);
    }

    public String toString() {
        return "BackendRequest{events=" + this.f2688a + ", extras=" + Arrays.toString(this.f2689b) + "}";
    }

    /* renamed from: com.google.android.datatransport.runtime.backends.a$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C1709b extends C1714f.C1715a {

        /* renamed from: a */
        private Iterable<C3899h> f2690a;

        /* renamed from: b */
        private byte[] f2691b;

        C1709b() {
        }

        /* renamed from: a */
        public C1714f.C1715a mo13539a(Iterable<C3899h> iterable) {
            if (iterable != null) {
                this.f2690a = iterable;
                return super;
            }
            throw new NullPointerException("Null events");
        }

        /* renamed from: a */
        public C1714f.C1715a mo13540a(@Nullable byte[] bArr) {
            this.f2691b = bArr;
            return super;
        }

        /* renamed from: a */
        public C1714f mo13541a() {
            String str = "";
            if (this.f2690a == null) {
                str = str + " events";
            }
            if (str.isEmpty()) {
                return new C1707a(this.f2690a, this.f2691b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    private C1707a(Iterable<C3899h> iterable, @Nullable byte[] bArr) {
        this.f2688a = iterable;
        this.f2689b = bArr;
    }
}
