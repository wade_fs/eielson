package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.common.stats.e */
public final class C2307e implements Parcelable.Creator<WakeLockEvent> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        ArrayList<String> arrayList = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f = 0.0f;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel2, a);
                    break;
                case 2:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 3:
                case 7:
                case 9:
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
                case 4:
                    str = C2248a.m5573n(parcel2, a);
                    break;
                case 5:
                    i3 = C2248a.m5585z(parcel2, a);
                    break;
                case 6:
                    arrayList = C2248a.m5575p(parcel2, a);
                    break;
                case 8:
                    j2 = C2248a.m5546B(parcel2, a);
                    break;
                case 10:
                    str3 = C2248a.m5573n(parcel2, a);
                    break;
                case 11:
                    i2 = C2248a.m5585z(parcel2, a);
                    break;
                case 12:
                    str2 = C2248a.m5573n(parcel2, a);
                    break;
                case 13:
                    str4 = C2248a.m5573n(parcel2, a);
                    break;
                case 14:
                    i4 = C2248a.m5585z(parcel2, a);
                    break;
                case 15:
                    f = C2248a.m5582w(parcel2, a);
                    break;
                case 16:
                    j3 = C2248a.m5546B(parcel2, a);
                    break;
                case 17:
                    str5 = C2248a.m5573n(parcel2, a);
                    break;
                case 18:
                    z = C2248a.m5577r(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new WakeLockEvent(i, j, i2, str, i3, arrayList, str2, j2, i4, str3, str4, f, j3, str5, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new WakeLockEvent[i];
    }
}
