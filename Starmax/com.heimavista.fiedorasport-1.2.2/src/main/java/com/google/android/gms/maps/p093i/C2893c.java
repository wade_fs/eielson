package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IInterface;
import com.google.android.gms.maps.GoogleMapOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.i.c */
public interface C2893c extends IInterface {
    /* renamed from: a */
    C3988b mo18430a(C3988b bVar, C3988b bVar2, Bundle bundle);

    /* renamed from: a */
    void mo18431a(Bundle bundle);

    /* renamed from: a */
    void mo18432a(C2911p pVar);

    /* renamed from: a */
    void mo18433a(C3988b bVar, GoogleMapOptions googleMapOptions, Bundle bundle);

    /* renamed from: b */
    void mo18434b();

    /* renamed from: b */
    void mo18435b(Bundle bundle);

    /* renamed from: c */
    void mo18436c();

    /* renamed from: d */
    void mo18437d();

    /* renamed from: g */
    void mo18438g();

    void onLowMemory();

    void onResume();

    void onStart();
}
