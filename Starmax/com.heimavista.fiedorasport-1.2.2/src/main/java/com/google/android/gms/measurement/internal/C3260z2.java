package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2774wa;

/* renamed from: com.google.android.gms.measurement.internal.z2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3260z2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5787a = new C3260z2();

    private C3260z2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2774wa.m7754c());
    }
}
