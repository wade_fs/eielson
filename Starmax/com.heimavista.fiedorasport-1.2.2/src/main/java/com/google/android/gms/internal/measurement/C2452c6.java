package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.c6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2452c6 {

    /* renamed from: a */
    private static final C2436b6 f4017a = m6140c();

    /* renamed from: b */
    private static final C2436b6 f4018b = new C2468d6();

    /* renamed from: a */
    static C2436b6 m6138a() {
        return f4017a;
    }

    /* renamed from: b */
    static C2436b6 m6139b() {
        return f4018b;
    }

    /* renamed from: c */
    private static C2436b6 m6140c() {
        try {
            return (C2436b6) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
