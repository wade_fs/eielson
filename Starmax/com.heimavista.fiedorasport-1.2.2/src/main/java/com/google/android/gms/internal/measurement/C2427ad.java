package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.measurement.ad */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2427ad extends C2412a implements C2589kc {
    C2427ad(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    /* renamed from: d */
    public final void mo17292d(Bundle bundle) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, bundle);
        mo17246b(1, H);
    }
}
