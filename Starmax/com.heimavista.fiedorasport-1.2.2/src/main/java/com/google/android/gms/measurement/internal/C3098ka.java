package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Looper;

/* renamed from: com.google.android.gms.measurement.internal.ka */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3098ka {
    C3098ka(Context context) {
    }

    /* renamed from: a */
    public static boolean m8843a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
