package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2669pc;

/* renamed from: com.google.android.gms.measurement.internal.p2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3150p2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5539a = new C3150p2();

    private C3150p2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2669pc.m7042b());
    }
}
