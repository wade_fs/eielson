package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.t9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2728t9 implements C2681q9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4506a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.sdk.referrer.delayed_install_referrer_api", false);

    /* renamed from: a */
    public final boolean mo17835a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17836e() {
        return f4506a.mo18128b().booleanValue();
    }
}
