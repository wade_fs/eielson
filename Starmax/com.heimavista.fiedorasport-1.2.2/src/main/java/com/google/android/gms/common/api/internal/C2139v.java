package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: com.google.android.gms.common.api.internal.v */
public class C2139v extends C2036f {

    /* renamed from: b */
    private final String f3485b;

    public C2139v(String str) {
        this.f3485b = str;
    }

    /* renamed from: a */
    public ConnectionResult mo16563a() {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: b */
    public C2040g<Status> mo16569b() {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: c */
    public void mo16571c() {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: d */
    public void mo16572d() {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: a */
    public void mo16565a(@NonNull C2036f.C2039c cVar) {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: b */
    public void mo16570b(@NonNull C2036f.C2039c cVar) {
        throw new UnsupportedOperationException(this.f3485b);
    }

    /* renamed from: a */
    public void mo16567a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.f3485b);
    }
}
