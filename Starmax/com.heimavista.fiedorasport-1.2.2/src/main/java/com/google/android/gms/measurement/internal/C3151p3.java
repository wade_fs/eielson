package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2442bc;

/* renamed from: com.google.android.gms.measurement.internal.p3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3151p3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5540a = new C3151p3();

    private C3151p3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2442bc.m6096b());
    }
}
