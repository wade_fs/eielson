package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class SignInButtonConfig extends AbstractSafeParcelable {
    public static final Parcelable.Creator<SignInButtonConfig> CREATOR = new C2234m0();

    /* renamed from: P */
    private final int f3633P;

    /* renamed from: Q */
    private final int f3634Q;

    /* renamed from: R */
    private final int f3635R;
    @Deprecated

    /* renamed from: S */
    private final Scope[] f3636S;

    SignInButtonConfig(int i, int i2, int i3, Scope[] scopeArr) {
        this.f3633P = i;
        this.f3634Q = i2;
        this.f3635R = i3;
        this.f3636S = scopeArr;
    }

    /* renamed from: c */
    public int mo16895c() {
        return this.f3634Q;
    }

    /* renamed from: d */
    public int mo16896d() {
        return this.f3635R;
    }

    @Deprecated
    /* renamed from: u */
    public Scope[] mo16897u() {
        return this.f3636S;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Scope[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3633P);
        C2250b.m5591a(parcel, 2, mo16895c());
        C2250b.m5591a(parcel, 3, mo16896d());
        C2250b.m5607a(parcel, 4, (Parcelable[]) mo16897u(), i, false);
        C2250b.m5587a(parcel, a);
    }

    public SignInButtonConfig(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, null);
    }
}
