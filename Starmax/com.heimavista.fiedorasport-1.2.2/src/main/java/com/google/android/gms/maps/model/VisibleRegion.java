package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class VisibleRegion extends AbstractSafeParcelable {
    public static final Parcelable.Creator<VisibleRegion> CREATOR = new C2930a0();

    /* renamed from: P */
    public final LatLng f4906P;

    /* renamed from: Q */
    public final LatLng f4907Q;

    /* renamed from: R */
    public final LatLng f4908R;

    /* renamed from: S */
    public final LatLng f4909S;

    /* renamed from: T */
    public final LatLngBounds f4910T;

    public VisibleRegion(LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.f4906P = latLng;
        this.f4907Q = latLng2;
        this.f4908R = latLng3;
        this.f4909S = latLng4;
        this.f4910T = latLngBounds;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VisibleRegion)) {
            return false;
        }
        VisibleRegion visibleRegion = (VisibleRegion) obj;
        return this.f4906P.equals(visibleRegion.f4906P) && this.f4907Q.equals(visibleRegion.f4907Q) && this.f4908R.equals(visibleRegion.f4908R) && this.f4909S.equals(visibleRegion.f4909S) && this.f4910T.equals(visibleRegion.f4910T);
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f4906P, this.f4907Q, this.f4908R, this.f4909S, this.f4910T);
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("nearLeft", this.f4906P);
        a.mo17037a("nearRight", this.f4907Q);
        a.mo17037a("farLeft", this.f4908R);
        a.mo17037a("farRight", this.f4909S);
        a.mo17037a("latLngBounds", this.f4910T);
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) this.f4906P, i, false);
        C2250b.m5596a(parcel, 3, (Parcelable) this.f4907Q, i, false);
        C2250b.m5596a(parcel, 4, (Parcelable) this.f4908R, i, false);
        C2250b.m5596a(parcel, 5, (Parcelable) this.f4909S, i, false);
        C2250b.m5596a(parcel, 6, (Parcelable) this.f4910T, i, false);
        C2250b.m5587a(parcel, a);
    }
}
