package com.google.android.gms.signin.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.internal.base.C2375a;
import com.google.android.gms.internal.base.C2377c;

/* renamed from: com.google.android.gms.signin.internal.g */
public final class C3274g extends C2375a implements C3273f {
    C3274g(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    /* renamed from: J */
    public final void mo19483J(int i) {
        Parcel H = mo17182H();
        H.writeInt(i);
        mo17185b(7, H);
    }

    /* renamed from: a */
    public final void mo19484a(C2231m mVar, int i, boolean z) {
        Parcel H = mo17182H();
        C2377c.m5899a(H, mVar);
        H.writeInt(i);
        C2377c.m5901a(H, z);
        mo17185b(9, H);
    }

    /* renamed from: a */
    public final void mo19485a(zah zah, C3271d dVar) {
        Parcel H = mo17182H();
        C2377c.m5900a(H, zah);
        C2377c.m5899a(H, dVar);
        mo17185b(12, H);
    }
}
