package com.google.android.datatransport.cct;

import com.google.android.datatransport.cct.C1703e;
import p119e.p144d.p145a.p146a.p147i.p149u.C3916a;

/* renamed from: com.google.android.datatransport.cct.c */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final /* synthetic */ class C1701c implements C3916a {

    /* renamed from: a */
    private final C1703e f2674a;

    private C1701c(C1703e eVar) {
        this.f2674a = eVar;
    }

    /* renamed from: a */
    public static C3916a m4259a(C1703e eVar) {
        return new C1701c(eVar);
    }

    public Object apply(Object obj) {
        return this.f2674a.m4263a((C1703e.C1704a) obj);
    }
}
