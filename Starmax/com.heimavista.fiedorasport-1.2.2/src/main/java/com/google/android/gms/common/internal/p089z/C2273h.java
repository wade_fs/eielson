package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.internal.C2056c;

/* renamed from: com.google.android.gms.common.internal.z.h */
abstract class C2273h<R extends C2157k> extends C2056c<R, C2275j> {
    public C2273h(C2036f fVar) {
        super(C2266a.f3766c, fVar);
    }
}
