package com.google.android.datatransport.cct.p084b;

import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1689p;
import java.util.Arrays;

/* renamed from: com.google.android.datatransport.cct.b.g */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1672g extends C1689p {

    /* renamed from: a */
    private final long f2595a;

    /* renamed from: b */
    private final int f2596b;

    /* renamed from: c */
    private final long f2597c;

    /* renamed from: d */
    private final byte[] f2598d;

    /* renamed from: e */
    private final String f2599e;

    /* renamed from: f */
    private final long f2600f;

    /* renamed from: g */
    private final C1696u f2601g;

    /* renamed from: com.google.android.datatransport.cct.b.g$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1674b extends C1689p.C1690a {

        /* renamed from: a */
        private Long f2602a;

        /* renamed from: b */
        private Integer f2603b;

        /* renamed from: c */
        private Long f2604c;

        /* renamed from: d */
        private byte[] f2605d;

        /* renamed from: e */
        private String f2606e;

        /* renamed from: f */
        private Long f2607f;

        /* renamed from: g */
        private C1696u f2608g;

        C1674b() {
        }

        /* renamed from: a */
        public C1689p.C1690a mo13488a(long j) {
            this.f2602a = Long.valueOf(j);
            return super;
        }

        /* renamed from: b */
        public C1689p.C1690a mo13493b(long j) {
            this.f2604c = Long.valueOf(j);
            return super;
        }

        /* renamed from: c */
        public C1689p.C1690a mo13494c(long j) {
            this.f2607f = Long.valueOf(j);
            return super;
        }

        /* renamed from: a */
        public C1689p.C1690a mo13487a(int i) {
            this.f2603b = Integer.valueOf(i);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1689p.C1690a mo13491a(@Nullable byte[] bArr) {
            this.f2605d = bArr;
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1689p.C1690a mo13490a(@Nullable String str) {
            this.f2606e = str;
            return super;
        }

        /* renamed from: a */
        public C1689p.C1690a mo13489a(@Nullable C1696u uVar) {
            this.f2608g = uVar;
            return super;
        }

        /* renamed from: a */
        public C1689p mo13492a() {
            String str = "";
            if (this.f2602a == null) {
                str = str + " eventTimeMs";
            }
            if (this.f2603b == null) {
                str = str + " eventCode";
            }
            if (this.f2604c == null) {
                str = str + " eventUptimeMs";
            }
            if (this.f2607f == null) {
                str = str + " timezoneOffsetSeconds";
            }
            if (str.isEmpty()) {
                return new C1672g(this.f2602a.longValue(), this.f2603b.intValue(), this.f2604c.longValue(), this.f2605d, this.f2606e, this.f2607f.longValue(), this.f2608g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* synthetic */ C1672g(long j, int i, long j2, byte[] bArr, String str, long j3, C1696u uVar, C1673a aVar) {
        this.f2595a = j;
        this.f2596b = i;
        this.f2597c = j2;
        this.f2598d = bArr;
        this.f2599e = str;
        this.f2600f = j3;
        this.f2601g = uVar;
    }

    /* renamed from: a */
    public long mo13477a() {
        return this.f2595a;
    }

    /* renamed from: b */
    public long mo13478b() {
        return this.f2597c;
    }

    /* renamed from: c */
    public long mo13479c() {
        return this.f2600f;
    }

    /* renamed from: d */
    public int mo13480d() {
        return this.f2596b;
    }

    @Nullable
    /* renamed from: e */
    public C1696u mo13481e() {
        return this.f2601g;
    }

    public boolean equals(Object obj) {
        byte[] bArr;
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1689p)) {
            return false;
        }
        C1689p pVar = (C1689p) obj;
        if (this.f2595a == super.mo13477a()) {
            C1672g gVar = (C1672g) pVar;
            if (this.f2596b == gVar.f2596b && this.f2597c == super.mo13478b()) {
                byte[] bArr2 = this.f2598d;
                if (pVar instanceof C1672g) {
                    bArr = gVar.f2598d;
                } else {
                    bArr = gVar.f2598d;
                }
                if (Arrays.equals(bArr2, bArr) && ((str = this.f2599e) != null ? str.equals(gVar.f2599e) : gVar.f2599e == null) && this.f2600f == super.mo13479c()) {
                    C1696u uVar = this.f2601g;
                    if (uVar == null) {
                        if (gVar.f2601g == null) {
                            return true;
                        }
                    } else if (uVar.equals(gVar.f2601g)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Nullable
    /* renamed from: f */
    public byte[] mo13483f() {
        return this.f2598d;
    }

    @Nullable
    /* renamed from: g */
    public String mo13484g() {
        return this.f2599e;
    }

    public int hashCode() {
        long j = this.f2595a;
        long j2 = this.f2597c;
        int hashCode = (((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f2596b) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ Arrays.hashCode(this.f2598d)) * 1000003;
        String str = this.f2599e;
        int i = 0;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j3 = this.f2600f;
        int i2 = (((hashCode ^ hashCode2) * 1000003) ^ ((int) ((j3 >>> 32) ^ j3))) * 1000003;
        C1696u uVar = this.f2601g;
        if (uVar != null) {
            i = uVar.hashCode();
        }
        return i2 ^ i;
    }

    public String toString() {
        return "LogEvent{eventTimeMs=" + this.f2595a + ", eventCode=" + this.f2596b + ", eventUptimeMs=" + this.f2597c + ", sourceExtension=" + Arrays.toString(this.f2598d) + ", sourceExtensionJsonProto3=" + this.f2599e + ", timezoneOffsetSeconds=" + this.f2600f + ", networkConnectionInfo=" + this.f2601g + "}";
    }
}
