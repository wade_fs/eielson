package com.google.android.gms.common.api.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.common.api.internal.y */
final class C2150y extends C2140v0 {

    /* renamed from: b */
    private final /* synthetic */ C2143w f3507b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2150y(C2143w wVar, C2132t0 t0Var) {
        super(t0Var);
        this.f3507b = wVar;
    }

    /* renamed from: a */
    public final void mo16645a() {
        this.f3507b.f3495a.f3483o.mo16729a((Bundle) null);
    }
}
