package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2018b;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2262x;

/* renamed from: com.google.android.gms.common.api.internal.c */
public abstract class C2056c<R extends C2157k, A extends C2016a.C2018b> extends BasePendingResult<R> implements C2060d<R> {

    /* renamed from: q */
    private final C2016a.C2019c<A> f3255q;

    /* renamed from: r */
    private final C2016a<?> f3256r;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected C2056c(@NonNull C2016a<?> aVar, @NonNull C2036f fVar) {
        super(fVar);
        C2258v.m5630a(fVar, "GoogleApiClient must not be null");
        C2258v.m5630a(aVar, "Api must not be null");
        this.f3255q = aVar.mo16521a();
        this.f3256r = aVar;
    }

    /* renamed from: a */
    private void m4770a(@NonNull RemoteException remoteException) {
        mo16640c(new Status(8, remoteException.getLocalizedMessage(), null));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16456a(@NonNull A a);

    /* renamed from: b */
    public final void mo16638b(@NonNull A a) {
        if (a instanceof C2262x) {
            a = ((C2262x) a).mo16448D();
        }
        try {
            mo16456a((C2016a.C2018b) a);
        } catch (DeadObjectException e) {
            m4770a((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            m4770a(e2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo16639b(@NonNull R r) {
    }

    /* renamed from: c */
    public final void mo16640c(@NonNull Status status) {
        C2258v.m5637a(!status.mo16518v(), "Failed result must not be success");
        C2157k a = mo16455a(status);
        mo16593a(a);
        mo16639b(a);
    }

    /* renamed from: g */
    public final C2016a<?> mo16641g() {
        return this.f3256r;
    }

    /* renamed from: h */
    public final C2016a.C2019c<A> mo16642h() {
        return this.f3255q;
    }

    /* renamed from: a */
    public /* bridge */ /* synthetic */ void mo16637a(Object obj) {
        super.mo16593a((C2157k) obj);
    }
}
