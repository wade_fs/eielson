package com.google.android.exoplayer2.extractor.p085ts;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.ts.c */
/* compiled from: lambda */
public final /* synthetic */ class C1813c implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1813c f2823a = new C1813c();

    private /* synthetic */ C1813c() {
    }

    public final Extractor[] createExtractors() {
        return PsExtractor.m4390a();
    }
}
