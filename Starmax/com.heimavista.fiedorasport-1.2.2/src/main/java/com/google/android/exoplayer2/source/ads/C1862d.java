package com.google.android.exoplayer2.source.ads;

import com.google.android.exoplayer2.source.ads.AdsMediaSource;

/* renamed from: com.google.android.exoplayer2.source.ads.d */
/* compiled from: lambda */
public final /* synthetic */ class C1862d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AdsMediaSource.ComponentListener f2862P;

    /* renamed from: Q */
    private final /* synthetic */ AdsMediaSource.AdLoadException f2863Q;

    public /* synthetic */ C1862d(AdsMediaSource.ComponentListener componentListener, AdsMediaSource.AdLoadException adLoadException) {
        this.f2862P = componentListener;
        this.f2863Q = adLoadException;
    }

    public final void run() {
        this.f2862P.mo15088a(this.f2863Q);
    }
}
