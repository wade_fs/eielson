package com.google.android.gms.internal.measurement;

import java.io.Serializable;

/* renamed from: com.google.android.gms.internal.measurement.k2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2579k2 {
    /* renamed from: a */
    public static <T> C2593l2<T> m6600a(C2593l2 l2Var) {
        if ((l2Var instanceof C2614m2) || (l2Var instanceof C2629n2)) {
            return l2Var;
        }
        if (l2Var instanceof Serializable) {
            return new C2629n2(l2Var);
        }
        return new C2614m2(l2Var);
    }

    /* renamed from: a */
    public static <T> C2593l2<T> m6601a(Object obj) {
        return new C2659p2(obj);
    }
}
