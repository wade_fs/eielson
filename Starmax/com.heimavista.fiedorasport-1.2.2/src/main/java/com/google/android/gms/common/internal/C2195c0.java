package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

/* renamed from: com.google.android.gms.common.internal.c0 */
final class C2195c0 extends C2217g {

    /* renamed from: P */
    private final /* synthetic */ Intent f3641P;

    /* renamed from: Q */
    private final /* synthetic */ Activity f3642Q;

    /* renamed from: R */
    private final /* synthetic */ int f3643R;

    C2195c0(Intent intent, Activity activity, int i) {
        this.f3641P = intent;
        this.f3642Q = activity;
        this.f3643R = i;
    }

    /* renamed from: a */
    public final void mo16904a() {
        Intent intent = this.f3641P;
        if (intent != null) {
            this.f3642Q.startActivityForResult(intent, this.f3643R);
        }
    }
}
