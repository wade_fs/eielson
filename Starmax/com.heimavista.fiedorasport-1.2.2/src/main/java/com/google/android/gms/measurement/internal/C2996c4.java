package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2804y8;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.c4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2996c4 extends C3010d6 {

    /* renamed from: c */
    private static final AtomicReference<String[]> f5003c = new AtomicReference<>();

    /* renamed from: d */
    private static final AtomicReference<String[]> f5004d = new AtomicReference<>();

    /* renamed from: e */
    private static final AtomicReference<String[]> f5005e = new AtomicReference<>();

    C2996c4(C3081j5 j5Var) {
        super(j5Var);
    }

    /* renamed from: t */
    private final boolean m8423t() {
        mo19018r();
        return this.f5134a.mo19107z() && this.f5134a.mo19015l().mo19000a(3);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public final String mo18822a(String str) {
        if (str == null) {
            return null;
        }
        if (!m8423t()) {
            return str;
        }
        return m8421a(str, C3082j6.f5265b, C3082j6.f5264a, f5003c);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: b */
    public final String mo18823b(String str) {
        if (str == null) {
            return null;
        }
        if (!m8423t()) {
            return str;
        }
        return m8421a(str, C3070i6.f5212b, C3070i6.f5211a, f5004d);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: c */
    public final String mo18824c(String str) {
        if (str == null) {
            return null;
        }
        if (!m8423t()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return m8421a(str, C3106l6.f5325b, C3106l6.f5324a, f5005e);
        }
        return "experiment_id" + "(" + str + ")";
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        return false;
    }

    @Nullable
    /* renamed from: a */
    private static String m8421a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        C2258v.m5629a(strArr);
        C2258v.m5629a(strArr2);
        C2258v.m5629a(atomicReference);
        C2258v.m5636a(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (C3267z9.m9428c(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public final String mo18821a(zzan zzan) {
        if (zzan == null) {
            return null;
        }
        if (!m8423t()) {
            return zzan.toString();
        }
        return "origin=" + zzan.f5805R + ",name=" + mo18822a(zzan.f5803P) + ",params=" + m8420a(zzan.f5804Q);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public final String mo18820a(C3099l lVar) {
        if (lVar == null) {
            return null;
        }
        if (!m8423t()) {
            return lVar.toString();
        }
        return "Event{appId='" + lVar.f5312a + "', name='" + mo18822a(lVar.f5313b) + "', params=" + m8420a(lVar.f5317f) + "}";
    }

    @Nullable
    /* renamed from: a */
    private final String m8420a(zzam zzam) {
        if (zzam == null) {
            return null;
        }
        if (!m8423t()) {
            return zzam.toString();
        }
        return mo18819a(zzam.mo19462e());
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public final String mo18819a(Bundle bundle) {
        String str;
        if (bundle == null) {
            return null;
        }
        if (!m8423t()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Bundle[{");
        for (String str2 : bundle.keySet()) {
            if (sb.length() != 8) {
                sb.append(", ");
            }
            sb.append(mo18823b(str2));
            sb.append("=");
            if (!C2804y8.m7881b() || !mo19013h().mo19146a(C3135o.f5433Z0)) {
                sb.append(bundle.get(str2));
            } else {
                Object obj = bundle.get(str2);
                if (obj instanceof Bundle) {
                    str = m8422a(new Object[]{obj});
                } else if (obj instanceof Object[]) {
                    str = m8422a((Object[]) obj);
                } else if (obj instanceof ArrayList) {
                    str = m8422a(((ArrayList) obj).toArray());
                } else {
                    str = String.valueOf(obj);
                }
                sb.append(str);
            }
        }
        sb.append("}]");
        return sb.toString();
    }

    @Nullable
    /* renamed from: a */
    private final String m8422a(Object[] objArr) {
        String str;
        if (objArr == null) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object obj : objArr) {
            if (obj instanceof Bundle) {
                str = mo18819a((Bundle) obj);
            } else {
                str = String.valueOf(obj);
            }
            if (str != null) {
                if (sb.length() != 1) {
                    sb.append(", ");
                }
                sb.append(str);
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
