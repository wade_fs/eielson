package com.google.android.material.transition;

import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionSet;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(19)
abstract class MaterialTransitionSet<T extends Transition> extends TransitionSet {
    @NonNull
    protected Context context;
    @NonNull
    private T primaryTransition;
    @Nullable
    private Transition secondaryTransition;

    MaterialTransitionSet() {
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public abstract T getDefaultPrimaryTransition();

    /* access modifiers changed from: package-private */
    @Nullable
    public abstract Transition getDefaultSecondaryTransition();

    @NonNull
    public T getPrimaryTransition() {
        return this.primaryTransition;
    }

    @Nullable
    public Transition getSecondaryTransition() {
        return this.secondaryTransition;
    }

    /* access modifiers changed from: protected */
    public void initialize(Context context2) {
        this.context = context2;
        this.primaryTransition = getDefaultPrimaryTransition();
        addTransition(this.primaryTransition);
        setSecondaryTransition(getDefaultSecondaryTransition());
    }

    public void setSecondaryTransition(@Nullable Transition transition) {
        TransitionUtils.maybeRemoveTransition(super, this.secondaryTransition);
        this.secondaryTransition = transition;
        TransitionUtils.maybeAddTransition(super, this.secondaryTransition);
    }
}
