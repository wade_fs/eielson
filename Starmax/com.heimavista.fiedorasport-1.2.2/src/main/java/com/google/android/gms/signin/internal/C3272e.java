package com.google.android.gms.signin.internal;

import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.base.C2376b;
import com.google.android.gms.internal.base.C2377c;

/* renamed from: com.google.android.gms.signin.internal.e */
public abstract class C3272e extends C2376b implements C3271d {
    public C3272e() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo17049a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i == 3) {
            mo19479a((ConnectionResult) C2377c.m5898a(parcel, ConnectionResult.CREATOR), (zaa) C2377c.m5898a(parcel, zaa.CREATOR));
        } else if (i == 4) {
            mo19481c((Status) C2377c.m5898a(parcel, Status.CREATOR));
        } else if (i == 6) {
            mo19482d((Status) C2377c.m5898a(parcel, Status.CREATOR));
        } else if (i == 7) {
            mo19480a((Status) C2377c.m5898a(parcel, Status.CREATOR), (GoogleSignInAccount) C2377c.m5898a(parcel, GoogleSignInAccount.CREATOR));
        } else if (i != 8) {
            return false;
        } else {
            mo16699a((zaj) C2377c.m5898a(parcel, zaj.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
