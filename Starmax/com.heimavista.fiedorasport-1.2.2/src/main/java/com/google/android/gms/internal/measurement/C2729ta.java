package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ta */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2729ta implements C2744ua {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4507a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.collection.firebase_global_collection_flag_enabled", true);

    /* renamed from: a */
    public final boolean mo17919a() {
        return f4507a.mo18128b().booleanValue();
    }
}
