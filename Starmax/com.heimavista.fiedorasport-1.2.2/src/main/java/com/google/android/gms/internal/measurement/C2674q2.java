package com.google.android.gms.internal.measurement;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.android.gms.internal.measurement.q2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2674q2 {

    /* renamed from: a */
    private final ConcurrentHashMap<C2719t2, List<Throwable>> f4426a = new ConcurrentHashMap<>(16, 0.75f, 10);

    /* renamed from: b */
    private final ReferenceQueue<Throwable> f4427b = new ReferenceQueue<>();

    C2674q2() {
    }

    /* renamed from: a */
    public final List<Throwable> mo17826a(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.f4427b.poll();
        while (poll != null) {
            this.f4426a.remove(poll);
            poll = this.f4427b.poll();
        }
        List<Throwable> list = this.f4426a.get(new C2719t2(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.f4426a.putIfAbsent(new C2719t2(th, this.f4427b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
