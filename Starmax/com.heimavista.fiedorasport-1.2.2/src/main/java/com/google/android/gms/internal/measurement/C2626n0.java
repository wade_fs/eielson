package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2611m0;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.n0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2626n0 extends C2595l4<C2626n0, C2627a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2626n0 zzl;
    private static volatile C2501f6<C2626n0> zzm;
    private int zzc;
    private long zzd;
    private String zze = "";
    private int zzf;
    private C2738u4<C2641o0> zzg = C2595l4.m6649m();
    private C2738u4<C2611m0> zzh = C2595l4.m6649m();
    private C2738u4<C2429b0> zzi = C2595l4.m6649m();
    private String zzj = "";
    private boolean zzk;

    /* renamed from: com.google.android.gms.internal.measurement.n0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2627a extends C2595l4.C2596a<C2626n0, C2627a> implements C2769w5 {
        private C2627a() {
            super(C2626n0.zzl);
        }

        /* renamed from: a */
        public final C2611m0 mo17769a(int i) {
            return ((C2626n0) super.f4288Q).mo17760b(i);
        }

        /* renamed from: j */
        public final int mo17771j() {
            return ((C2626n0) super.f4288Q).mo17766t();
        }

        /* renamed from: k */
        public final List<C2429b0> mo17772k() {
            return Collections.unmodifiableList(((C2626n0) super.f4288Q).mo17767u());
        }

        /* renamed from: l */
        public final C2627a mo17773l() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2626n0) super.f4288Q).m6847z();
            return this;
        }

        /* synthetic */ C2627a(C2591l0 l0Var) {
            this();
        }

        /* renamed from: a */
        public final C2627a mo17770a(int i, C2611m0.C2612a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2626n0) super.f4288Q).m6841a(i, (C2611m0) super.mo17679i());
            return this;
        }
    }

    static {
        C2626n0 n0Var = new C2626n0();
        zzl = n0Var;
        C2595l4.m6645a(C2626n0.class, super);
    }

    private C2626n0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6841a(int i, C2611m0 m0Var) {
        m0Var.getClass();
        if (!this.zzh.mo17938a()) {
            this.zzh = C2595l4.m6642a(this.zzh);
        }
        this.zzh.set(i, m0Var);
    }

    /* renamed from: w */
    public static C2627a m6844w() {
        return (C2627a) zzl.mo17668i();
    }

    /* renamed from: x */
    public static C2626n0 m6845x() {
        return zzl;
    }

    /* access modifiers changed from: private */
    /* renamed from: z */
    public final void m6847z() {
        this.zzi = C2595l4.m6649m();
    }

    /* renamed from: b */
    public final C2611m0 mo17760b(int i) {
        return this.zzh.get(i);
    }

    /* renamed from: n */
    public final boolean mo17761n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final long mo17762o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final boolean mo17763p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final String mo17764q() {
        return this.zze;
    }

    /* renamed from: s */
    public final List<C2641o0> mo17765s() {
        return this.zzg;
    }

    /* renamed from: t */
    public final int mo17766t() {
        return this.zzh.size();
    }

    /* renamed from: u */
    public final List<C2429b0> mo17767u() {
        return this.zzi;
    }

    /* renamed from: v */
    public final boolean mo17768v() {
        return this.zzk;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2591l0.f4286a[i - 1]) {
            case 1:
                return new C2626n0();
            case 2:
                return new C2627a(null);
            case 3:
                return C2595l4.m6643a(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\u0004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\b\u0003\b\u0007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", C2641o0.class, "zzh", C2611m0.class, "zzi", C2429b0.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                C2501f6<C2626n0> f6Var = zzm;
                if (f6Var == null) {
                    synchronized (C2626n0.class) {
                        f6Var = zzm;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzl);
                            zzm = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
