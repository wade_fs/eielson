package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.C1994g;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.auth.api.f */
final class C1971f extends C2016a.C2017a<C1994g, GoogleSignInOptions> {
    C1971f() {
    }

    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, @Nullable Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        return new C1994g(context, looper, eVar, (GoogleSignInOptions) obj, bVar, cVar);
    }

    /* renamed from: a */
    public final /* synthetic */ List mo16372a(@Nullable Object obj) {
        GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
        if (googleSignInOptions == null) {
            return Collections.emptyList();
        }
        return googleSignInOptions.mo16399u();
    }
}
