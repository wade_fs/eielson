package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.wa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2774wa implements C2593l2<C2759va> {

    /* renamed from: Q */
    private static C2774wa f4556Q = new C2774wa();

    /* renamed from: P */
    private final C2593l2<C2759va> f4557P;

    private C2774wa(C2593l2<C2759va> l2Var) {
        this.f4557P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7753b() {
        return ((C2759va) f4556Q.mo17285a()).mo18004a();
    }

    /* renamed from: c */
    public static boolean m7754c() {
        return ((C2759va) f4556Q.mo17285a()).mo18005e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4557P.mo17285a();
    }

    public C2774wa() {
        this(C2579k2.m6601a(new C2806ya()));
    }
}
