package com.google.android.gms.measurement.internal;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.stats.C2303a;
import com.google.android.gms.internal.measurement.C2561j2;
import com.google.android.gms.internal.measurement.C2696r9;

/* renamed from: com.google.android.gms.measurement.internal.a5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2973a5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2561j2 f4946P;

    /* renamed from: Q */
    private final /* synthetic */ ServiceConnection f4947Q;

    /* renamed from: R */
    private final /* synthetic */ C3240x4 f4948R;

    C2973a5(C3240x4 x4Var, C2561j2 j2Var, ServiceConnection serviceConnection) {
        this.f4948R = x4Var;
        this.f4946P = j2Var;
        this.f4947Q = serviceConnection;
    }

    public final void run() {
        C3240x4 x4Var = this.f4948R;
        C3251y4 y4Var = x4Var.f5750Q;
        String a = x4Var.f5749P;
        C2561j2 j2Var = this.f4946P;
        ServiceConnection serviceConnection = this.f4947Q;
        Bundle a2 = y4Var.mo19400a(a, j2Var);
        y4Var.f5768a.mo19014j().mo18881c();
        if (a2 != null) {
            long j = a2.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                y4Var.f5768a.mo19015l().mo19001t().mo19042a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a2.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    y4Var.f5768a.mo19015l().mo19001t().mo19042a("No referrer defined in Install Referrer response");
                } else {
                    y4Var.f5768a.mo19015l().mo18996B().mo19043a("InstallReferrer API result", string);
                    C3267z9 w = y4Var.f5768a.mo19104w();
                    String valueOf = String.valueOf(string);
                    Bundle a3 = w.mo19425a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a3 == null) {
                        y4Var.f5768a.mo19015l().mo19001t().mo19042a("No campaign params defined in Install Referrer result");
                    } else {
                        String string2 = a3.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a2.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                y4Var.f5768a.mo19015l().mo19001t().mo19042a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a3.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == y4Var.f5768a.mo19098p().f5613k.mo19326a()) {
                            y4Var.f5768a.mo19018r();
                            y4Var.f5768a.mo19015l().mo18996B().mo19042a("Install Referrer campaign has already been logged");
                        } else if (!C2696r9.m7134b() || !y4Var.f5768a.mo19097m().mo19146a(C3135o.f5427W0) || y4Var.f5768a.mo19089c()) {
                            y4Var.f5768a.mo19098p().f5613k.mo19327a(j);
                            y4Var.f5768a.mo19018r();
                            y4Var.f5768a.mo19015l().mo18996B().mo19043a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a3.putString("_cis", "referrer API");
                            y4Var.f5768a.mo19103v().mo19267a("auto", "_cmp", a3);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            C2303a.m5745a().mo17123a(y4Var.f5768a.mo19016n(), serviceConnection);
        }
    }
}
