package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.v8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2757v8 implements C2772w8 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4539a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4540b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4539a = c2Var.mo17367a("measurement.sdk.dynamite.allow_remote_dynamite", false);
        c2Var.mo17367a("measurement.collection.init_params_control_enabled", true);
        f4540b = c2Var.mo17367a("measurement.sdk.dynamite.use_dynamite2", false);
        c2Var.mo17365a("measurement.id.sdk.dynamite.use_dynamite", 0L);
    }

    /* renamed from: a */
    public final boolean mo18002a() {
        return f4539a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo18003e() {
        return f4540b.mo18128b().booleanValue();
    }
}
