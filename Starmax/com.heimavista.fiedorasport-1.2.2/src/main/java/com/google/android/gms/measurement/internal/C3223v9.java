package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2414a1;
import com.google.android.gms.internal.measurement.C2445c0;
import com.google.android.gms.internal.measurement.C2461d0;
import com.google.android.gms.internal.measurement.C2477e0;
import com.google.android.gms.internal.measurement.C2489ea;
import com.google.android.gms.internal.measurement.C2494f0;
import com.google.android.gms.internal.measurement.C2510g0;
import com.google.android.gms.internal.measurement.C2635n8;
import com.google.android.gms.internal.measurement.C2671q0;
import com.google.android.gms.internal.measurement.C2686r0;
import com.google.android.gms.internal.measurement.C2701s0;
import com.google.android.gms.internal.measurement.C2733u0;
import com.google.android.gms.internal.measurement.C2748v0;
import com.google.android.gms.internal.measurement.C2763w0;
import com.google.android.gms.internal.measurement.C2785x5;
import com.google.android.gms.internal.measurement.C2794y0;
import com.google.android.gms.internal.measurement.C2798y3;
import com.google.android.gms.internal.measurement.C2804y8;
import com.google.android.gms.internal.measurement.C2810z0;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/* renamed from: com.google.android.gms.measurement.internal.v9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3223v9 extends C3157p9 {
    C3223v9(C3145o9 o9Var) {
        super(o9Var);
    }

    /* renamed from: b */
    static Object m9274b(C2701s0 s0Var, String str) {
        C2733u0 a = m9261a(s0Var, str);
        if (a == null) {
            return null;
        }
        if (a.mo17923p()) {
            return a.mo17924q();
        }
        if (a.mo17925s()) {
            return Long.valueOf(a.mo17926t());
        }
        if (a.mo17927u()) {
            return Double.valueOf(a.mo17928v());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19357a(C2414a1.C2415a aVar, Object obj) {
        C2258v.m5629a(obj);
        aVar.mo17262j();
        aVar.mo17263k();
        aVar.mo17264l();
        if (obj instanceof String) {
            aVar.mo17261b((String) obj);
        } else if (obj instanceof Long) {
            aVar.mo17260b(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.mo17257a(((Double) obj).doubleValue());
        } else {
            mo19015l().mo19001t().mo19043a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final byte[] mo19362c(byte[] bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            mo19015l().mo19001t().mo19043a("Failed to gzip content", e);
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        return false;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: u */
    public final List<Integer> mo19363u() {
        Map<String, String> a = C3135o.m8956a(this.f5360b.mo19016n());
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = C3135o.f5406M.mo19389a(null).intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (((String) next.getKey()).startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt((String) next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            mo19015l().mo19004w().mo19043a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    mo19015l().mo19004w().mo19043a("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final byte[] mo19361b(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            mo19015l().mo19001t().mo19043a("Failed to ungzip content", e);
            throw e;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19358a(C2733u0.C2734a aVar, Object obj) {
        C2258v.m5629a(obj);
        aVar.mo17935j();
        aVar.mo17936k();
        aVar.mo17937l();
        if (obj instanceof String) {
            aVar.mo17934b((String) obj);
        } else if (obj instanceof Long) {
            aVar.mo17932a(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.mo17931a(((Double) obj).doubleValue());
        } else {
            mo19015l().mo19001t().mo19043a("Ignoring invalid (type) event param value", obj);
        }
    }

    /* renamed from: a */
    static C2733u0 m9261a(C2701s0 s0Var, String str) {
        for (C2733u0 u0Var : s0Var.mo17847n()) {
            if (u0Var.mo17922o().equals(str)) {
                return u0Var;
            }
        }
        return null;
    }

    /* renamed from: a */
    static void m9265a(C2701s0.C2702a aVar, String str, Object obj) {
        List<C2733u0> j = aVar.mo17866j();
        int i = 0;
        while (true) {
            if (i >= j.size()) {
                i = -1;
                break;
            } else if (str.equals(j.get(i).mo17922o())) {
                break;
            } else {
                i++;
            }
        }
        C2733u0.C2734a y = C2733u0.m7347y();
        y.mo17933a(str);
        if (obj instanceof Long) {
            y.mo17932a(((Long) obj).longValue());
        } else if (obj instanceof String) {
            y.mo17934b((String) obj);
        } else if (obj instanceof Double) {
            y.mo17931a(((Double) obj).doubleValue());
        }
        if (i >= 0) {
            aVar.mo17856a(i, y);
        } else {
            aVar.mo17859a(y);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo19355a(C2748v0 v0Var) {
        if (v0Var == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        for (C2763w0 w0Var : v0Var.mo17948n()) {
            if (w0Var != null) {
                m9266a(sb, 1);
                sb.append("bundle {\n");
                if (w0Var.mo18047n()) {
                    m9270a(sb, 1, "protocol_version", Integer.valueOf(w0Var.mo18021P()));
                }
                m9270a(sb, 1, "platform", w0Var.mo18043j0());
                if (w0Var.mo18056r0()) {
                    m9270a(sb, 1, "gmp_version", Long.valueOf(w0Var.mo18049o()));
                }
                if (w0Var.mo18051p()) {
                    m9270a(sb, 1, "uploading_gmp_version", Long.valueOf(w0Var.mo18053q()));
                }
                if (w0Var.mo18024S()) {
                    m9270a(sb, 1, "dynamite_version", Long.valueOf(w0Var.mo18025T()));
                }
                if (w0Var.mo18015J()) {
                    m9270a(sb, 1, "config_version", Long.valueOf(w0Var.mo18016K()));
                }
                m9270a(sb, 1, "gmp_app_id", w0Var.mo18007B());
                m9270a(sb, 1, "admob_app_id", w0Var.mo18023R());
                m9270a(sb, 1, "app_id", w0Var.mo18052p0());
                m9270a(sb, 1, "app_version", w0Var.mo18054q0());
                if (w0Var.mo18012G()) {
                    m9270a(sb, 1, "app_version_major", Integer.valueOf(w0Var.mo18013H()));
                }
                m9270a(sb, 1, "firebase_instance_id", w0Var.mo18011F());
                if (w0Var.mo18061w()) {
                    m9270a(sb, 1, "dev_cert_hash", Long.valueOf(w0Var.mo18062x()));
                }
                m9270a(sb, 1, "app_store", w0Var.mo18050o0());
                if (w0Var.mo18031Z()) {
                    m9270a(sb, 1, "upload_timestamp_millis", Long.valueOf(w0Var.mo18032a0()));
                }
                if (w0Var.mo18034b0()) {
                    m9270a(sb, 1, "start_timestamp_millis", Long.valueOf(w0Var.mo18036c0()));
                }
                if (w0Var.mo18037d0()) {
                    m9270a(sb, 1, "end_timestamp_millis", Long.valueOf(w0Var.mo18038e0()));
                }
                if (w0Var.mo18039f0()) {
                    m9270a(sb, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(w0Var.mo18040g0()));
                }
                if (w0Var.mo18041h0()) {
                    m9270a(sb, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(w0Var.mo18042i0()));
                }
                m9270a(sb, 1, "app_instance_id", w0Var.mo18060v());
                m9270a(sb, 1, "resettable_device_id", w0Var.mo18057s());
                m9270a(sb, 1, "device_id", w0Var.mo18014I());
                m9270a(sb, 1, "ds_id", w0Var.mo18019N());
                if (w0Var.mo18058t()) {
                    m9270a(sb, 1, "limited_ad_tracking", Boolean.valueOf(w0Var.mo18059u()));
                }
                m9270a(sb, 1, "os_version", w0Var.mo18044k0());
                m9270a(sb, 1, "device_model", w0Var.mo18045l0());
                m9270a(sb, 1, "user_default_language", w0Var.mo18046m0());
                if (w0Var.mo18055r()) {
                    m9270a(sb, 1, "time_zone_offset_minutes", Integer.valueOf(w0Var.mo18048n0()));
                }
                if (w0Var.mo18063y()) {
                    m9270a(sb, 1, "bundle_sequential_index", Integer.valueOf(w0Var.mo18064z()));
                }
                if (w0Var.mo18008C()) {
                    m9270a(sb, 1, "service_upload", Boolean.valueOf(w0Var.mo18009D()));
                }
                m9270a(sb, 1, "health_monitor", w0Var.mo18006A());
                if (w0Var.mo18017L() && w0Var.mo18018M() != 0) {
                    m9270a(sb, 1, "android_id", Long.valueOf(w0Var.mo18018M()));
                }
                if (w0Var.mo18020O()) {
                    m9270a(sb, 1, "retry_counter", Integer.valueOf(w0Var.mo18022Q()));
                }
                List<C2414a1> X = w0Var.mo18029X();
                if (X != null) {
                    for (C2414a1 a1Var : X) {
                        if (a1Var != null) {
                            m9266a(sb, 2);
                            sb.append("user_property {\n");
                            Double d = null;
                            m9270a(sb, 2, "set_timestamp_millis", a1Var.mo17248n() ? Long.valueOf(a1Var.mo17249o()) : null);
                            m9270a(sb, 2, "name", mo19010e().mo18824c(a1Var.mo17250p()));
                            m9270a(sb, 2, "string_value", a1Var.mo17252s());
                            m9270a(sb, 2, "int_value", a1Var.mo17253t() ? Long.valueOf(a1Var.mo17254u()) : null);
                            if (a1Var.mo17255v()) {
                                d = Double.valueOf(a1Var.mo17256w());
                            }
                            m9270a(sb, 2, "double_value", d);
                            m9266a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<C2671q0> E = w0Var.mo18010E();
                String p0 = w0Var.mo18052p0();
                if (E != null) {
                    for (C2671q0 q0Var : E) {
                        if (q0Var != null) {
                            m9266a(sb, 2);
                            sb.append("audience_membership {\n");
                            if (q0Var.mo17813n()) {
                                m9270a(sb, 2, "audience_id", Integer.valueOf(q0Var.mo17814o()));
                            }
                            if (q0Var.mo17818t()) {
                                m9270a(sb, 2, "new_audience", Boolean.valueOf(q0Var.mo17819u()));
                            }
                            m9269a(sb, 2, "current_data", q0Var.mo17815p(), p0);
                            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || q0Var.mo17816q()) {
                                m9269a(sb, 2, "previous_data", q0Var.mo17817s(), p0);
                            }
                            m9266a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<C2701s0> V = w0Var.mo18027V();
                if (V != null) {
                    for (C2701s0 s0Var : V) {
                        if (s0Var != null) {
                            m9266a(sb, 2);
                            sb.append("event {\n");
                            m9270a(sb, 2, "name", mo19010e().mo18822a(s0Var.mo17849p()));
                            if (s0Var.mo17850q()) {
                                m9270a(sb, 2, "timestamp_millis", Long.valueOf(s0Var.mo17851s()));
                            }
                            if (s0Var.mo17852t()) {
                                m9270a(sb, 2, "previous_timestamp_millis", Long.valueOf(s0Var.mo17853u()));
                            }
                            if (s0Var.mo17854v()) {
                                m9270a(sb, 2, "count", Integer.valueOf(s0Var.mo17855w()));
                            }
                            if (s0Var.mo17848o() != 0) {
                                m9271a(sb, 2, s0Var.mo17847n());
                            }
                            m9266a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                m9266a(sb, 1);
                sb.append("}\n");
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo19353a(C2445c0 c0Var) {
        if (c0Var == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        if (c0Var.mo17347n()) {
            m9270a(sb, 0, "filter_id", Integer.valueOf(c0Var.mo17348o()));
        }
        m9270a(sb, 0, "event_name", mo19010e().mo18822a(c0Var.mo17349p()));
        String a = m9263a(c0Var.mo17354v(), c0Var.mo17355w(), c0Var.mo17357y());
        if (!a.isEmpty()) {
            m9270a(sb, 0, "filter_type", a);
        }
        if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || c0Var.mo17352t()) {
            m9268a(sb, 1, "event_count_filter", c0Var.mo17353u());
        }
        if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || c0Var.mo17351s() > 0) {
            sb.append("  filters {\n");
            for (C2461d0 d0Var : c0Var.mo17350q()) {
                m9267a(sb, 2, d0Var);
            }
        }
        m9266a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo19354a(C2494f0 f0Var) {
        if (f0Var == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        if (f0Var.mo17458n()) {
            m9270a(sb, 0, "filter_id", Integer.valueOf(f0Var.mo17459o()));
        }
        m9270a(sb, 0, "property_name", mo19010e().mo18824c(f0Var.mo17460p()));
        String a = m9263a(f0Var.mo17462s(), f0Var.mo17463t(), f0Var.mo17465v());
        if (!a.isEmpty()) {
            m9270a(sb, 0, "filter_type", a);
        }
        m9267a(sb, 1, f0Var.mo17461q());
        sb.append("}\n");
        return sb.toString();
    }

    /* renamed from: a */
    private static String m9263a(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    /* renamed from: a */
    private final void m9271a(StringBuilder sb, int i, List<C2733u0> list) {
        if (list != null) {
            int i2 = i + 1;
            for (C2733u0 u0Var : list) {
                if (u0Var != null) {
                    m9266a(sb, i2);
                    sb.append("param {\n");
                    Double d = null;
                    if (!C2804y8.m7881b() || !mo19013h().mo19146a(C3135o.f5433Z0)) {
                        m9270a(sb, i2, "name", mo19010e().mo18823b(u0Var.mo17922o()));
                        m9270a(sb, i2, "string_value", u0Var.mo17924q());
                        m9270a(sb, i2, "int_value", u0Var.mo17925s() ? Long.valueOf(u0Var.mo17926t()) : null);
                        if (u0Var.mo17927u()) {
                            d = Double.valueOf(u0Var.mo17928v());
                        }
                        m9270a(sb, i2, "double_value", d);
                    } else {
                        m9270a(sb, i2, "name", u0Var.mo17921n() ? mo19010e().mo18823b(u0Var.mo17922o()) : null);
                        m9270a(sb, i2, "string_value", u0Var.mo17923p() ? u0Var.mo17924q() : null);
                        m9270a(sb, i2, "int_value", u0Var.mo17925s() ? Long.valueOf(u0Var.mo17926t()) : null);
                        if (u0Var.mo17927u()) {
                            d = Double.valueOf(u0Var.mo17928v());
                        }
                        m9270a(sb, i2, "double_value", d);
                        if (u0Var.mo17930x() > 0) {
                            m9271a(sb, i2, u0Var.mo17929w());
                        }
                    }
                    m9266a(sb, i2);
                    sb.append("}\n");
                }
            }
        }
    }

    /* renamed from: a */
    private static void m9269a(StringBuilder sb, int i, String str, C2794y0 y0Var, String str2) {
        if (y0Var != null) {
            m9266a(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (y0Var.mo18151q() != 0) {
                m9266a(sb, 4);
                sb.append("results: ");
                int i2 = 0;
                for (Long l : y0Var.mo18150p()) {
                    int i3 = i2 + 1;
                    if (i2 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l);
                    i2 = i3;
                }
                sb.append(10);
            }
            if (y0Var.mo18149o() != 0) {
                m9266a(sb, 4);
                sb.append("status: ");
                int i4 = 0;
                for (Long l2 : y0Var.mo18148n()) {
                    int i5 = i4 + 1;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l2);
                    i4 = i5;
                }
                sb.append(10);
            }
            if (y0Var.mo18153t() != 0) {
                m9266a(sb, 4);
                sb.append("dynamic_filter_timestamps: {");
                int i6 = 0;
                for (C2686r0 r0Var : y0Var.mo18152s()) {
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb.append(", ");
                    }
                    sb.append(r0Var.mo17837n() ? Integer.valueOf(r0Var.mo17838o()) : null);
                    sb.append(":");
                    sb.append(r0Var.mo17839p() ? Long.valueOf(r0Var.mo17840q()) : null);
                    i6 = i7;
                }
                sb.append("}\n");
            }
            if (y0Var.mo18155v() != 0) {
                m9266a(sb, 4);
                sb.append("sequence_filter_timestamps: {");
                int i8 = 0;
                for (C2810z0 z0Var : y0Var.mo18154u()) {
                    int i9 = i8 + 1;
                    if (i8 != 0) {
                        sb.append(", ");
                    }
                    sb.append(z0Var.mo18171n() ? Integer.valueOf(z0Var.mo18172o()) : null);
                    sb.append(": [");
                    int i10 = 0;
                    for (Long l3 : z0Var.mo18173p()) {
                        long longValue = l3.longValue();
                        int i11 = i10 + 1;
                        if (i10 != 0) {
                            sb.append(", ");
                        }
                        sb.append(longValue);
                        i10 = i11;
                    }
                    sb.append("]");
                    i8 = i9;
                }
                sb.append("}\n");
            }
            m9266a(sb, 3);
            sb.append("}\n");
        }
    }

    /* renamed from: a */
    private final void m9268a(StringBuilder sb, int i, String str, C2477e0 e0Var) {
        if (e0Var != null) {
            m9266a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (e0Var.mo17430n()) {
                m9270a(sb, i, "comparison_type", e0Var.mo17431o().name());
            }
            if (e0Var.mo17432p()) {
                m9270a(sb, i, "match_as_float", Boolean.valueOf(e0Var.mo17433q()));
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || e0Var.mo17434s()) {
                m9270a(sb, i, "comparison_value", e0Var.mo17435t());
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || e0Var.mo17436u()) {
                m9270a(sb, i, "min_comparison_value", e0Var.mo17437v());
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || e0Var.mo17438w()) {
                m9270a(sb, i, "max_comparison_value", e0Var.mo17439x());
            }
            m9266a(sb, i);
            sb.append("}\n");
        }
    }

    /* renamed from: a */
    private final void m9267a(StringBuilder sb, int i, C2461d0 d0Var) {
        if (d0Var != null) {
            m9266a(sb, i);
            sb.append("filter {\n");
            if (d0Var.mo17392s()) {
                m9270a(sb, i, "complement", Boolean.valueOf(d0Var.mo17393t()));
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || d0Var.mo17394u()) {
                m9270a(sb, i, "param_name", mo19010e().mo18823b(d0Var.mo17395v()));
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || d0Var.mo17388n()) {
                int i2 = i + 1;
                C2510g0 o = d0Var.mo17389o();
                if (o != null) {
                    m9266a(sb, i2);
                    sb.append("string_filter");
                    sb.append(" {\n");
                    if (o.mo17486n()) {
                        m9270a(sb, i2, "match_type", o.mo17487o().name());
                    }
                    if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || o.mo17488p()) {
                        m9270a(sb, i2, "expression", o.mo17489q());
                    }
                    if (o.mo17490s()) {
                        m9270a(sb, i2, "case_sensitive", Boolean.valueOf(o.mo17491t()));
                    }
                    if (o.mo17493v() > 0) {
                        m9266a(sb, i2 + 1);
                        sb.append("expression_list {\n");
                        for (String str : o.mo17492u()) {
                            m9266a(sb, i2 + 2);
                            sb.append(str);
                            sb.append("\n");
                        }
                        sb.append("}\n");
                    }
                    m9266a(sb, i2);
                    sb.append("}\n");
                }
            }
            if (!C2489ea.m6262b() || !mo19013h().mo19146a(C3135o.f5431Y0) || d0Var.mo17390p()) {
                m9268a(sb, i + 1, "number_filter", d0Var.mo17391q());
            }
            m9266a(sb, i);
            sb.append("}\n");
        }
    }

    /* renamed from: a */
    private static void m9266a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    /* renamed from: a */
    private static void m9270a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            m9266a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        mo19015l().mo19001t().mo19042a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T extends android.os.Parcelable> T mo19352a(byte[] r5, android.os.Parcelable.Creator r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 != 0) goto L_0x0004
            return r0
        L_0x0004:
            android.os.Parcel r1 = android.os.Parcel.obtain()
            int r2 = r5.length     // Catch:{ a -> 0x001c }
            r3 = 0
            r1.unmarshall(r5, r3, r2)     // Catch:{ a -> 0x001c }
            r1.setDataPosition(r3)     // Catch:{ a -> 0x001c }
            java.lang.Object r5 = r6.createFromParcel(r1)     // Catch:{ a -> 0x001c }
            android.os.Parcelable r5 = (android.os.Parcelable) r5     // Catch:{ a -> 0x001c }
            r1.recycle()
            return r5
        L_0x001a:
            r5 = move-exception
            goto L_0x002d
        L_0x001c:
            com.google.android.gms.measurement.internal.f4 r5 = r4.mo19015l()     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ all -> 0x001a }
            java.lang.String r6 = "Failed to load parcelable from buffer"
            r5.mo19042a(r6)     // Catch:{ all -> 0x001a }
            r1.recycle()
            return r0
        L_0x002d:
            r1.recycle()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3223v9.mo19352a(byte[], android.os.Parcelable$Creator):android.os.Parcelable");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final boolean mo19360a(zzan zzan, zzm zzm) {
        C2258v.m5629a(zzan);
        C2258v.m5629a(zzm);
        if (!C2635n8.m6933b() || !mo19013h().mo19146a(C3135o.f5409N0)) {
            if (!TextUtils.isEmpty(zzm.f5815Q) || !TextUtils.isEmpty(zzm.f5831g0)) {
                return true;
            }
            mo19018r();
            return false;
        } else if (!TextUtils.isEmpty(zzm.f5815Q) || !TextUtils.isEmpty(zzm.f5831g0)) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: a */
    static boolean m9272a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    /* renamed from: a */
    static boolean m9273a(List<Long> list, int i) {
        if (i >= (list.size() << 6)) {
            return false;
        }
        return ((1 << (i % 64)) & list.get(i / 64).longValue()) != 0;
    }

    /* renamed from: a */
    static List<Long> m9264a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    j |= 1 << i2;
                }
            }
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final List<Long> mo19356a(List<Long> list, List<Integer> list2) {
        int i;
        ArrayList arrayList = new ArrayList(list);
        for (Integer num : list2) {
            if (num.intValue() < 0) {
                mo19015l().mo19004w().mo19043a("Ignoring negative bit index to be cleared", num);
            } else {
                int intValue = num.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    mo19015l().mo19004w().mo19044a("Ignoring bit index greater than bitSet size", num, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (num.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            int i2 = size2;
            i = size;
            size = i2;
            if (size >= 0 && ((Long) arrayList.get(size)).longValue() == 0) {
                size2 = size - 1;
            }
        }
        return arrayList.subList(0, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19359a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(mo19017o().mo17132a() - j) > j2;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final long mo19351a(byte[] bArr) {
        C2258v.m5629a(bArr);
        mo19011f().mo18881c();
        MessageDigest y = C3267z9.m9433y();
        if (y != null) {
            return C3267z9.m9410a(y.digest(bArr));
        }
        mo19015l().mo19001t().mo19042a("Failed to get MD5");
        return 0;
    }

    /* renamed from: a */
    static <Builder extends C2785x5> Builder m9262a(C2785x5 x5Var, byte[] bArr) {
        C2798y3 b = C2798y3.m7829b();
        if (b != null) {
            x5Var.mo17953a(bArr, b);
            return x5Var;
        }
        x5Var.mo17952a(bArr);
        return x5Var;
    }

    /* renamed from: a */
    static int m9260a(C2763w0.C2764a aVar, String str) {
        if (aVar == null) {
            return -1;
        }
        for (int i = 0; i < aVar.mo18113n(); i++) {
            if (str.equals(aVar.mo18084d(i).mo17250p())) {
                return i;
            }
        }
        return -1;
    }
}
