package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.converter.zaa;
import com.google.android.gms.common.util.C2312c;
import com.google.android.gms.common.util.C2321l;
import com.google.android.gms.common.util.C2322m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class FastJsonResponse {

    public static class Field<I, O> extends AbstractSafeParcelable {
        public static final C2298a CREATOR = new C2298a();

        /* renamed from: P */
        private final int f3799P;

        /* renamed from: Q */
        protected final int f3800Q;

        /* renamed from: R */
        protected final boolean f3801R;

        /* renamed from: S */
        protected final int f3802S;

        /* renamed from: T */
        protected final boolean f3803T;

        /* renamed from: U */
        protected final String f3804U;

        /* renamed from: V */
        protected final int f3805V;

        /* renamed from: W */
        protected final Class<? extends FastJsonResponse> f3806W;

        /* renamed from: X */
        private final String f3807X;

        /* renamed from: Y */
        private zak f3808Y;
        /* access modifiers changed from: private */

        /* renamed from: Z */
        public C2297a<I, O> f3809Z;

        Field(int i, int i2, boolean z, int i3, boolean z2, String str, int i4, String str2, zaa zaa) {
            this.f3799P = i;
            this.f3800Q = i2;
            this.f3801R = z;
            this.f3802S = i3;
            this.f3803T = z2;
            this.f3804U = str;
            this.f3805V = i4;
            if (str2 == null) {
                this.f3806W = null;
                this.f3807X = null;
            } else {
                this.f3806W = SafeParcelResponse.class;
                this.f3807X = str2;
            }
            if (zaa == null) {
                this.f3809Z = null;
            } else {
                this.f3809Z = zaa.mo17084c();
            }
        }

        /* renamed from: b */
        public static Field<Integer, Integer> m5713b(String str, int i) {
            return new Field(0, false, 0, false, str, i, null, null);
        }

        /* renamed from: v */
        private final String m5717v() {
            String str = this.f3807X;
            if (str == null) {
                return null;
            }
            return str;
        }

        /* renamed from: w */
        private final zaa m5718w() {
            C2297a<I, O> aVar = this.f3809Z;
            if (aVar == null) {
                return null;
            }
            return zaa.m5701a(aVar);
        }

        /* renamed from: a */
        public final void mo17090a(zak zak) {
            this.f3808Y = zak;
        }

        /* renamed from: c */
        public int mo17091c() {
            return this.f3805V;
        }

        /* renamed from: d */
        public final boolean mo17092d() {
            return this.f3809Z != null;
        }

        public String toString() {
            C2251t.C2252a a = C2251t.m5616a(this);
            a.mo17037a("versionCode", Integer.valueOf(this.f3799P));
            a.mo17037a("typeIn", Integer.valueOf(this.f3800Q));
            a.mo17037a("typeInArray", Boolean.valueOf(this.f3801R));
            a.mo17037a("typeOut", Integer.valueOf(this.f3802S));
            a.mo17037a("typeOutArray", Boolean.valueOf(this.f3803T));
            a.mo17037a("outputFieldName", this.f3804U);
            a.mo17037a("safeParcelFieldId", Integer.valueOf(this.f3805V));
            a.mo17037a("concreteTypeName", m5717v());
            Class<? extends FastJsonResponse> cls = this.f3806W;
            if (cls != null) {
                a.mo17037a("concreteType.class", cls.getCanonicalName());
            }
            C2297a<I, O> aVar = this.f3809Z;
            if (aVar != null) {
                a.mo17037a("converterName", aVar.getClass().getCanonicalName());
            }
            return a.toString();
        }

        /* renamed from: u */
        public final Map<String, Field<?, ?>> mo17094u() {
            C2258v.m5629a((Object) this.f3807X);
            C2258v.m5629a(this.f3808Y);
            return this.f3808Y.mo17110b(this.f3807X);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
         arg types: [android.os.Parcel, int, java.lang.String, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
         arg types: [android.os.Parcel, int, com.google.android.gms.common.server.converter.zaa, int, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
        public void writeToParcel(Parcel parcel, int i) {
            int a = C2250b.m5586a(parcel);
            C2250b.m5591a(parcel, 1, this.f3799P);
            C2250b.m5591a(parcel, 2, this.f3800Q);
            C2250b.m5605a(parcel, 3, this.f3801R);
            C2250b.m5591a(parcel, 4, this.f3802S);
            C2250b.m5605a(parcel, 5, this.f3803T);
            C2250b.m5602a(parcel, 6, this.f3804U, false);
            C2250b.m5591a(parcel, 7, mo17091c());
            C2250b.m5602a(parcel, 8, m5717v(), false);
            C2250b.m5596a(parcel, 9, (Parcelable) m5718w(), i, false);
            C2250b.m5587a(parcel, a);
        }

        /* renamed from: b */
        public static <T extends FastJsonResponse> Field<ArrayList<T>, ArrayList<T>> m5714b(String str, int i, Class<T> cls) {
            return new Field(11, true, 11, true, str, i, cls, null);
        }

        /* renamed from: c */
        public static Field<String, String> m5715c(String str, int i) {
            return new Field(7, false, 7, false, str, i, null, null);
        }

        /* renamed from: d */
        public static Field<ArrayList<String>, ArrayList<String>> m5716d(String str, int i) {
            return new Field(7, true, 7, true, str, i, null, null);
        }

        /* renamed from: a */
        public final I mo17089a(O o) {
            return this.f3809Z.mo17075a(o);
        }

        /* renamed from: a */
        public static Field<byte[], byte[]> m5710a(String str, int i) {
            return new Field(8, false, 8, false, str, i, null, null);
        }

        /* renamed from: a */
        public static <T extends FastJsonResponse> Field<T, T> m5711a(String str, int i, Class<T> cls) {
            return new Field(11, false, 11, false, str, i, cls, null);
        }

        private Field(int i, boolean z, int i2, boolean z2, String str, int i3, Class<? extends FastJsonResponse> cls, C2297a<I, O> aVar) {
            this.f3799P = 1;
            this.f3800Q = i;
            this.f3801R = z;
            this.f3802S = i2;
            this.f3803T = z2;
            this.f3804U = str;
            this.f3805V = i3;
            this.f3806W = cls;
            if (cls == null) {
                this.f3807X = null;
            } else {
                this.f3807X = cls.getCanonicalName();
            }
            this.f3809Z = aVar;
        }
    }

    /* renamed from: com.google.android.gms.common.server.response.FastJsonResponse$a */
    public interface C2297a<I, O> {
        /* renamed from: a */
        I mo17075a(O o);
    }

    /* renamed from: a */
    protected static <O, I> I m5703a(Field<I, O> field, Object obj) {
        return field.f3809Z != null ? field.mo17089a(obj) : obj;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Object mo17086a(String str);

    /* renamed from: a */
    public abstract Map<String, Field<?, ?>> mo16320a();

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo16321b(Field field) {
        if (field.f3802S != 11) {
            return mo17087b(field.f3804U);
        }
        if (field.f3803T) {
            String str = field.f3804U;
            throw new UnsupportedOperationException("Concrete type arrays not supported");
        }
        String str2 = field.f3804U;
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract boolean mo17087b(String str);

    public String toString() {
        Map<String, Field<?, ?>> a = mo16320a();
        StringBuilder sb = new StringBuilder(100);
        for (String str : a.keySet()) {
            Field field = a.get(str);
            if (mo16321b(field)) {
                Object a2 = m5703a(field, mo16319a(field));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(str);
                sb.append("\":");
                if (a2 != null) {
                    switch (field.f3802S) {
                        case 8:
                            sb.append("\"");
                            sb.append(C2312c.m5768a((byte[]) a2));
                            sb.append("\"");
                            continue;
                        case 9:
                            sb.append("\"");
                            sb.append(C2312c.m5769b((byte[]) a2));
                            sb.append("\"");
                            continue;
                        case 10:
                            C2322m.m5791a(sb, (HashMap) a2);
                            continue;
                        default:
                            if (!field.f3801R) {
                                m5704a(sb, field, a2);
                                break;
                            } else {
                                ArrayList arrayList = (ArrayList) a2;
                                sb.append("[");
                                int size = arrayList.size();
                                for (int i = 0; i < size; i++) {
                                    if (i > 0) {
                                        sb.append(",");
                                    }
                                    Object obj = arrayList.get(i);
                                    if (obj != null) {
                                        m5704a(sb, field, obj);
                                    }
                                }
                                sb.append("]");
                                continue;
                            }
                    }
                } else {
                    sb.append("null");
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo16319a(Field field) {
        String str = field.f3804U;
        if (field.f3806W == null) {
            return mo17086a(str);
        }
        C2258v.m5642b(mo17086a(str) == null, "Concrete field shouldn't be value object: %s", field.f3804U);
        boolean z = field.f3803T;
        try {
            char upperCase = Character.toUpperCase(str.charAt(0));
            String substring = str.substring(1);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 4);
            sb.append("get");
            sb.append(upperCase);
            sb.append(substring);
            return getClass().getMethod(sb.toString(), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    private static void m5704a(StringBuilder sb, Field field, Object obj) {
        int i = field.f3800Q;
        if (i == 11) {
            sb.append(((FastJsonResponse) field.f3806W.cast(obj)).toString());
        } else if (i == 7) {
            sb.append("\"");
            sb.append(C2321l.m5790a((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }
}
