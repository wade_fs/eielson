package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.datatransport.runtime.backends.C1713e;
import com.google.android.datatransport.runtime.backends.C1714f;
import com.google.android.datatransport.runtime.backends.C1716g;
import com.google.android.datatransport.runtime.backends.C1724m;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p148t.C3915a;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3945h;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.m */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C1743m {

    /* renamed from: a */
    private final Context f2755a;

    /* renamed from: b */
    private final C1713e f2756b;

    /* renamed from: c */
    private final C3934c f2757c;

    /* renamed from: d */
    private final C1749s f2758d;

    /* renamed from: e */
    private final Executor f2759e;

    /* renamed from: f */
    private final C3969b f2760f;

    /* renamed from: g */
    private final C3971a f2761g;

    public C1743m(Context context, C1713e eVar, C3934c cVar, C1749s sVar, Executor executor, C3969b bVar, C3971a aVar) {
        this.f2755a = context;
        this.f2756b = eVar;
        this.f2757c = cVar;
        this.f2758d = sVar;
        this.f2759e = executor;
        this.f2760f = bVar;
        this.f2761g = aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo13590a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f2755a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* renamed from: a */
    public void mo13589a(C3905l lVar, int i, Runnable runnable) {
        this.f2759e.execute(C1738h.m4344a(this, lVar, i, runnable));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        r5.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r2.f2758d.mo13561a(r3, r4 + 1);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0024 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void m4356a(com.google.android.datatransport.runtime.scheduling.jobscheduling.C1743m r2, p119e.p144d.p145a.p146a.p147i.C3905l r3, int r4, java.lang.Runnable r5) {
        /*
            e.d.a.a.i.w.b r0 = r2.f2760f     // Catch:{ a -> 0x0024 }
            e.d.a.a.i.v.j.c r1 = r2.f2757c     // Catch:{ a -> 0x0024 }
            r1.getClass()     // Catch:{ a -> 0x0024 }
            e.d.a.a.i.w.b$a r1 = com.google.android.datatransport.runtime.scheduling.jobscheduling.C1741k.m4349a(r1)     // Catch:{ a -> 0x0024 }
            r0.mo23602a(r1)     // Catch:{ a -> 0x0024 }
            boolean r0 = r2.mo13590a()     // Catch:{ a -> 0x0024 }
            if (r0 != 0) goto L_0x001e
            e.d.a.a.i.w.b r0 = r2.f2760f     // Catch:{ a -> 0x0024 }
            e.d.a.a.i.w.b$a r1 = com.google.android.datatransport.runtime.scheduling.jobscheduling.C1742l.m4351a(r2, r3, r4)     // Catch:{ a -> 0x0024 }
            r0.mo23602a(r1)     // Catch:{ a -> 0x0024 }
            goto L_0x002b
        L_0x001e:
            r2.mo13588a(r3, r4)     // Catch:{ a -> 0x0024 }
            goto L_0x002b
        L_0x0022:
            r2 = move-exception
            goto L_0x002f
        L_0x0024:
            com.google.android.datatransport.runtime.scheduling.jobscheduling.s r2 = r2.f2758d     // Catch:{ all -> 0x0022 }
            int r4 = r4 + 1
            r2.mo13561a(r3, r4)     // Catch:{ all -> 0x0022 }
        L_0x002b:
            r5.run()
            return
        L_0x002f:
            r5.run()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.datatransport.runtime.scheduling.jobscheduling.C1743m.m4356a(com.google.android.datatransport.runtime.scheduling.jobscheduling.m, e.d.a.a.i.l, int, java.lang.Runnable):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13588a(C3905l lVar, int i) {
        C1716g gVar;
        C1724m mVar = this.f2756b.get(lVar.mo23531a());
        Iterable<C3945h> iterable = (Iterable) this.f2760f.mo23602a(C1739i.m4345a(this, lVar));
        if (iterable.iterator().hasNext()) {
            if (mVar == null) {
                C3915a.m11792a("Uploader", "Unknown backend for %s, deleting event batch for it...", lVar);
                gVar = C1716g.m4288c();
            } else {
                ArrayList arrayList = new ArrayList();
                for (C3945h hVar : iterable) {
                    arrayList.add(hVar.mo23579a());
                }
                C1714f.C1715a c = C1714f.m4281c();
                c.mo13539a(arrayList);
                c.mo13540a(lVar.mo23532b());
                gVar = mVar.mo13530a(c.mo13541a());
            }
            this.f2760f.mo23602a(C1740j.m4347a(this, gVar, iterable, lVar, i));
        }
    }

    /* renamed from: a */
    static /* synthetic */ Object m4354a(C1743m mVar, C1716g gVar, Iterable iterable, C3905l lVar, int i) {
        if (gVar.mo13543b() == C1716g.C1717a.TRANSIENT_ERROR) {
            mVar.f2757c.mo23591b(iterable);
            mVar.f2758d.mo13561a(lVar, i + 1);
            return null;
        }
        mVar.f2757c.mo23589a(iterable);
        if (gVar.mo13543b() == C1716g.C1717a.OK) {
            mVar.f2757c.mo23588a(lVar, mVar.f2761g.mo23604a() + gVar.mo13542a());
        }
        if (!mVar.f2757c.mo23592c(lVar)) {
            return null;
        }
        mVar.f2758d.mo13561a(lVar, 1);
        return null;
    }
}
