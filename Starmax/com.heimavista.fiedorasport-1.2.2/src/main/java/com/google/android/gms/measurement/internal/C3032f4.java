package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.GuardedBy;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.f4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3032f4 extends C3010d6 {
    /* access modifiers changed from: private */

    /* renamed from: c */
    public char f5122c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public long f5123d = -1;
    @GuardedBy("this")

    /* renamed from: e */
    private String f5124e;

    /* renamed from: f */
    private final C3056h4 f5125f = new C3056h4(this, 6, false, false);

    /* renamed from: g */
    private final C3056h4 f5126g = new C3056h4(this, 6, true, false);

    /* renamed from: h */
    private final C3056h4 f5127h = new C3056h4(this, 6, false, true);

    /* renamed from: i */
    private final C3056h4 f5128i = new C3056h4(this, 5, false, false);

    /* renamed from: j */
    private final C3056h4 f5129j = new C3056h4(this, 5, true, false);

    /* renamed from: k */
    private final C3056h4 f5130k = new C3056h4(this, 5, false, true);

    /* renamed from: l */
    private final C3056h4 f5131l = new C3056h4(this, 4, false, false);

    /* renamed from: m */
    private final C3056h4 f5132m = new C3056h4(this, 3, false, false);

    /* renamed from: n */
    private final C3056h4 f5133n = new C3056h4(this, 2, false, false);

    C3032f4(C3081j5 j5Var) {
        super(j5Var);
    }

    /* renamed from: D */
    private final String m8617D() {
        String str;
        String str2;
        synchronized (this) {
            if (this.f5124e == null) {
                if (this.f5134a.mo19076C() != null) {
                    str2 = this.f5134a.mo19076C();
                } else {
                    mo19013h().mo19018r();
                    str2 = "FA";
                }
                this.f5124e = str2;
            }
            str = this.f5124e;
        }
        return str;
    }

    /* renamed from: a */
    protected static Object m8621a(String str) {
        if (str == null) {
            return null;
        }
        return new C3092k4(str);
    }

    /* renamed from: b */
    private static String m8625b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return str;
        }
        return str.substring(0, lastIndexOf);
    }

    /* renamed from: A */
    public final C3056h4 mo18995A() {
        return this.f5132m;
    }

    /* renamed from: B */
    public final C3056h4 mo18996B() {
        return this.f5133n;
    }

    /* renamed from: C */
    public final String mo18997C() {
        Pair<String, Long> a = mo19012g().f5606d.mo19364a();
        if (a == null || a == C3185s4.f5602C) {
            return null;
        }
        String valueOf = String.valueOf(a.second);
        String str = (String) a.first;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        return false;
    }

    /* renamed from: t */
    public final C3056h4 mo19001t() {
        return this.f5125f;
    }

    /* renamed from: u */
    public final C3056h4 mo19002u() {
        return this.f5126g;
    }

    /* renamed from: v */
    public final C3056h4 mo19003v() {
        return this.f5127h;
    }

    /* renamed from: w */
    public final C3056h4 mo19004w() {
        return this.f5128i;
    }

    /* renamed from: x */
    public final C3056h4 mo19005x() {
        return this.f5129j;
    }

    /* renamed from: y */
    public final C3056h4 mo19006y() {
        return this.f5130k;
    }

    /* renamed from: z */
    public final C3056h4 mo19007z() {
        return this.f5131l;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo18999a(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && mo19000a(i)) {
            mo18998a(i, m8623a(false, str, obj, obj2, obj3));
        }
        if (!z2 && i >= 5) {
            C2258v.m5629a((Object) str);
            C3045g5 u = this.f5134a.mo19102u();
            if (u == null) {
                mo18998a(6, "Scheduler not set. Not logging error/warn");
            } else if (!super.mo18906s()) {
                mo18998a(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i < 0) {
                    i = 0;
                }
                u.mo19028a(new C3068i4(this, i >= 9 ? 8 : i, str, obj, obj2, obj3));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo19000a(int i) {
        return Log.isLoggable(m8617D(), i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo18998a(int i, String str) {
        Log.println(i, m8617D(), str);
    }

    /* renamed from: a */
    static String m8623a(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String a = m8622a(z, obj);
        String a2 = m8622a(z, obj2);
        String a3 = m8622a(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(a)) {
            sb.append(str2);
            sb.append(a);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a2)) {
            sb.append(str2);
            sb.append(a2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a3)) {
            sb.append(str2);
            sb.append(a3);
        }
        return sb.toString();
    }

    /* renamed from: a */
    private static String m8622a(boolean z, Object obj) {
        String className;
        String str = "";
        if (obj == null) {
            return str;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        int i = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l = (Long) obj;
            if (Math.abs(l.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) == '-') {
                str = "-";
            }
            String valueOf = String.valueOf(Math.abs(l.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str.length() + 43 + str.length());
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
                String b = m8625b(C3081j5.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i];
                    if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && m8625b(className).equals(b)) {
                        sb2.append(": ");
                        sb2.append(stackTraceElement);
                        break;
                    }
                    i++;
                }
                return sb2.toString();
            } else if (obj instanceof C3092k4) {
                return ((C3092k4) obj).f5293a;
            } else {
                if (z) {
                    return "-";
                }
                return String.valueOf(obj);
            }
        }
    }
}
