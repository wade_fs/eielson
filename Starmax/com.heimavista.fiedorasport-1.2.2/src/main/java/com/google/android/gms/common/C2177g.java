package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;

/* renamed from: com.google.android.gms.common.g */
public class C2177g {

    /* renamed from: b */
    private static C2177g f3571b;

    /* renamed from: a */
    private final Context f3572a;

    private C2177g(Context context) {
        this.f3572a = context.getApplicationContext();
    }

    /* renamed from: a */
    public static C2177g m5307a(Context context) {
        C2258v.m5629a(context);
        synchronized (C2177g.class) {
            if (f3571b == null) {
                C2286m.m5689a(context);
                f3571b = new C2177g(context);
            }
        }
        return f3571b;
    }

    /* renamed from: a */
    public boolean mo16854a(int i) {
        C2335v vVar;
        String[] a = C2283c.m5685a(this.f3572a).mo17059a(i);
        if (a != null && a.length != 0) {
            vVar = null;
            for (String str : a) {
                vVar = m5309a(str, i);
                if (vVar.f3871a) {
                    break;
                }
            }
        } else {
            vVar = C2335v.m5826a("no pkgs");
        }
        vVar.mo17140b();
        return vVar.f3871a;
    }

    /* renamed from: a */
    public static boolean m5310a(PackageInfo packageInfo, boolean z) {
        C2288o oVar;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                oVar = m5308a(packageInfo, C2291r.f3787a);
            } else {
                oVar = m5308a(packageInfo, C2291r.f3787a[0]);
            }
            if (oVar != null) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.o[]):com.google.android.gms.common.o
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.v
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    /* renamed from: a */
    public boolean mo16855a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (m5310a(packageInfo, false)) {
            return true;
        }
        if (m5310a(packageInfo, true)) {
            if (C2176f.m5305e(this.f3572a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    /* renamed from: a */
    private final C2335v m5309a(String str, int i) {
        try {
            PackageInfo a = C2283c.m5685a(this.f3572a).mo17056a(str, 64, i);
            boolean e = C2176f.m5305e(this.f3572a);
            if (a == null) {
                return C2335v.m5826a("null pkg");
            }
            if (a.signatures.length != 1) {
                return C2335v.m5826a("single cert required");
            }
            C2289p pVar = new C2289p(a.signatures[0].toByteArray());
            String str2 = a.packageName;
            C2335v a2 = C2286m.m5687a(str2, pVar, e, false);
            return (!a2.f3871a || a.applicationInfo == null || (a.applicationInfo.flags & 2) == 0 || !C2286m.m5687a(str2, pVar, false, true).f3871a) ? a2 : C2335v.m5826a("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return C2335v.m5826a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    /* renamed from: a */
    private static C2288o m5308a(PackageInfo packageInfo, C2288o... oVarArr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        C2289p pVar = new C2289p(signatureArr[0].toByteArray());
        for (int i = 0; i < oVarArr.length; i++) {
            if (oVarArr[i].equals(pVar)) {
                return oVarArr[i];
            }
        }
        return null;
    }
}
