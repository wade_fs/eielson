package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.r6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3176r6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5588P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5589Q;

    C3176r6(C3154p6 p6Var, AtomicReference atomicReference) {
        this.f5589Q = p6Var;
        this.f5588P = atomicReference;
    }

    public final void run() {
        synchronized (this.f5588P) {
            try {
                this.f5588P.set(Boolean.valueOf(this.f5589Q.mo19013h().mo19155f(this.f5589Q.mo18885p().mo18800B())));
                this.f5588P.notify();
            } catch (Throwable th) {
                this.f5588P.notify();
                throw th;
            }
        }
    }
}
