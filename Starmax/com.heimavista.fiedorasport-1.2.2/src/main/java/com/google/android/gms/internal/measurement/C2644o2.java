package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.o2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2644o2 {

    /* renamed from: a */
    private static final C2689r2 f4366a;

    /* renamed from: com.google.android.gms.internal.measurement.o2$a */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2645a extends C2689r2 {
        C2645a() {
        }

        /* renamed from: a */
        public final void mo17787a(Throwable th, Throwable th2) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    static {
        /*
            java.lang.Integer r0 = m6954a()     // Catch:{ all -> 0x002c }
            if (r0 == 0) goto L_0x0014
            int r1 = r0.intValue()     // Catch:{ all -> 0x002a }
            r2 = 19
            if (r1 < r2) goto L_0x0014
            com.google.android.gms.internal.measurement.u2 r1 = new com.google.android.gms.internal.measurement.u2     // Catch:{ all -> 0x002a }
            r1.<init>()     // Catch:{ all -> 0x002a }
            goto L_0x0064
        L_0x0014:
            java.lang.String r1 = "com.google.devtools.build.android.desugar.runtime.twr_disable_mimic"
            boolean r1 = java.lang.Boolean.getBoolean(r1)     // Catch:{ all -> 0x002a }
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0024
            com.google.android.gms.internal.measurement.s2 r1 = new com.google.android.gms.internal.measurement.s2     // Catch:{ all -> 0x002a }
            r1.<init>()     // Catch:{ all -> 0x002a }
            goto L_0x0064
        L_0x0024:
            com.google.android.gms.internal.measurement.o2$a r1 = new com.google.android.gms.internal.measurement.o2$a     // Catch:{ all -> 0x002a }
            r1.<init>()     // Catch:{ all -> 0x002a }
            goto L_0x0064
        L_0x002a:
            r1 = move-exception
            goto L_0x002e
        L_0x002c:
            r1 = move-exception
            r0 = 0
        L_0x002e:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.Class<com.google.android.gms.internal.measurement.o2$a> r3 = com.google.android.gms.internal.measurement.C2644o2.C2645a.class
            java.lang.String r3 = r3.getName()
            java.lang.String r4 = java.lang.String.valueOf(r3)
            int r4 = r4.length()
            int r4 = r4 + 133
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy "
            r5.append(r4)
            r5.append(r3)
            java.lang.String r3 = "will be used. The error is: "
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r2.println(r3)
            java.io.PrintStream r2 = java.lang.System.err
            r1.printStackTrace(r2)
            com.google.android.gms.internal.measurement.o2$a r1 = new com.google.android.gms.internal.measurement.o2$a
            r1.<init>()
        L_0x0064:
            com.google.android.gms.internal.measurement.C2644o2.f4366a = r1
            if (r0 != 0) goto L_0x0069
            goto L_0x006c
        L_0x0069:
            r0.intValue()
        L_0x006c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2644o2.<clinit>():void");
    }

    /* renamed from: a */
    public static void m6955a(Throwable th, Throwable th2) {
        f4366a.mo17787a(th, th2);
    }

    /* renamed from: a */
    private static Integer m6954a() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }
}
