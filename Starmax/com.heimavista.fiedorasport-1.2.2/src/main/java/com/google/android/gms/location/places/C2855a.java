package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.location.places.a */
public final class C2855a implements Parcelable.Creator<PlaceReport> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        int i = 0;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 == 3) {
                str2 = C2248a.m5573n(parcel, a);
            } else if (a2 != 4) {
                C2248a.m5550F(parcel, a);
            } else {
                str3 = C2248a.m5573n(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new PlaceReport(i, str, str2, str3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PlaceReport[i];
    }
}
