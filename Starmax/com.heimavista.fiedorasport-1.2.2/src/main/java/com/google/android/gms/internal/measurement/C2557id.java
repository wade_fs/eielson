package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.id */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2557id extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4245T;

    /* renamed from: U */
    private final /* synthetic */ String f4246U;

    /* renamed from: V */
    private final /* synthetic */ Bundle f4247V;

    /* renamed from: W */
    private final /* synthetic */ C2525gd f4248W;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2557id(C2525gd gdVar, String str, String str2, Bundle bundle) {
        super(gdVar);
        this.f4248W = gdVar;
        this.f4245T = str;
        this.f4246U = str2;
        this.f4247V = bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4248W.f4192g.clearConditionalUserProperty(this.f4245T, this.f4246U, this.f4247V);
    }
}
