package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.location.C2829c;
import java.util.Locale;

public final class zzbh extends AbstractSafeParcelable implements C2829c {
    public static final Parcelable.Creator<zzbh> CREATOR = new C2405t();

    /* renamed from: P */
    private final String f3950P;

    /* renamed from: Q */
    private final long f3951Q;

    /* renamed from: R */
    private final short f3952R;

    /* renamed from: S */
    private final double f3953S;

    /* renamed from: T */
    private final double f3954T;

    /* renamed from: U */
    private final float f3955U;

    /* renamed from: V */
    private final int f3956V;

    /* renamed from: W */
    private final int f3957W;

    /* renamed from: X */
    private final int f3958X;

    public zzbh(String str, int i, short s, double d, double d2, float f, long j, int i2, int i3) {
        if (str == null || str.length() > 100) {
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "requestId is null or too long: ".concat(valueOf) : new String("requestId is null or too long: "));
        } else if (f <= 0.0f) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("invalid radius: ");
            sb.append(f);
            throw new IllegalArgumentException(sb.toString());
        } else if (d > 90.0d || d < -90.0d) {
            StringBuilder sb2 = new StringBuilder(42);
            sb2.append("invalid latitude: ");
            sb2.append(d);
            throw new IllegalArgumentException(sb2.toString());
        } else if (d2 > 180.0d || d2 < -180.0d) {
            StringBuilder sb3 = new StringBuilder(43);
            sb3.append("invalid longitude: ");
            sb3.append(d2);
            throw new IllegalArgumentException(sb3.toString());
        } else {
            int i4 = i & 7;
            if (i4 != 0) {
                this.f3952R = s;
                this.f3950P = str;
                this.f3953S = d;
                this.f3954T = d2;
                this.f3955U = f;
                this.f3951Q = j;
                this.f3956V = i4;
                this.f3957W = i2;
                this.f3958X = i3;
                return;
            }
            StringBuilder sb4 = new StringBuilder(46);
            sb4.append("No supported transition specified: ");
            sb4.append(i);
            throw new IllegalArgumentException(sb4.toString());
        }
    }

    /* renamed from: c */
    public final String mo17234c() {
        return this.f3950P;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zzbh)) {
            return false;
        }
        zzbh zzbh = (zzbh) obj;
        return this.f3955U == zzbh.f3955U && this.f3953S == zzbh.f3953S && this.f3954T == zzbh.f3954T && this.f3952R == zzbh.f3952R;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f3953S);
        long doubleToLongBits2 = Double.doubleToLongBits(this.f3954T);
        return ((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + Float.floatToIntBits(this.f3955U)) * 31) + this.f3952R) * 31) + this.f3956V;
    }

    public final String toString() {
        Locale locale = Locale.US;
        Object[] objArr = new Object[9];
        objArr[0] = this.f3952R != 1 ? null : "CIRCLE";
        objArr[1] = this.f3950P.replaceAll("\\p{C}", "?");
        objArr[2] = Integer.valueOf(this.f3956V);
        objArr[3] = Double.valueOf(this.f3953S);
        objArr[4] = Double.valueOf(this.f3954T);
        objArr[5] = Float.valueOf(this.f3955U);
        objArr[6] = Integer.valueOf(this.f3957W / 1000);
        objArr[7] = Integer.valueOf(this.f3958X);
        objArr[8] = Long.valueOf(this.f3951Q);
        return String.format(locale, "Geofence[%s id:%s transitions:%d %.6f, %.6f %.0fm, resp=%ds, dwell=%dms, @%d]", objArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5602a(parcel, 1, mo17234c(), false);
        C2250b.m5592a(parcel, 2, this.f3951Q);
        C2250b.m5604a(parcel, 3, this.f3952R);
        C2250b.m5589a(parcel, 4, this.f3953S);
        C2250b.m5589a(parcel, 5, this.f3954T);
        C2250b.m5590a(parcel, 6, this.f3955U);
        C2250b.m5591a(parcel, 7, this.f3956V);
        C2250b.m5591a(parcel, 8, this.f3957W);
        C2250b.m5591a(parcel, 9, this.f3958X);
        C2250b.m5587a(parcel, a);
    }
}
