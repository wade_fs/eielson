package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Locale;

public final class WebImage extends AbstractSafeParcelable {
    public static final Parcelable.Creator<WebImage> CREATOR = new C2187c();

    /* renamed from: P */
    private final int f3598P;

    /* renamed from: Q */
    private final Uri f3599Q;

    /* renamed from: R */
    private final int f3600R;

    /* renamed from: S */
    private final int f3601S;

    WebImage(int i, Uri uri, int i2, int i3) {
        this.f3598P = i;
        this.f3599Q = uri;
        this.f3600R = i2;
        this.f3601S = i3;
    }

    /* renamed from: c */
    public final int mo16863c() {
        return this.f3601S;
    }

    /* renamed from: d */
    public final Uri mo16864d() {
        return this.f3599Q;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof WebImage)) {
            WebImage webImage = (WebImage) obj;
            return C2251t.m5617a(this.f3599Q, webImage.f3599Q) && this.f3600R == webImage.f3600R && this.f3601S == webImage.f3601S;
        }
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f3599Q, Integer.valueOf(this.f3600R), Integer.valueOf(this.f3601S));
    }

    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", Integer.valueOf(this.f3600R), Integer.valueOf(this.f3601S), this.f3599Q.toString());
    }

    /* renamed from: u */
    public final int mo16868u() {
        return this.f3600R;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3598P);
        C2250b.m5596a(parcel, 2, (Parcelable) mo16864d(), i, false);
        C2250b.m5591a(parcel, 3, mo16868u());
        C2250b.m5591a(parcel, 4, mo16863c());
        C2250b.m5587a(parcel, a);
    }
}
