package com.google.android.datatransport.cct;

import com.google.android.datatransport.cct.C1703e;
import p119e.p144d.p145a.p146a.p147i.p149u.C3918c;

/* renamed from: com.google.android.datatransport.cct.d */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final /* synthetic */ class C1702d implements C3918c {

    /* renamed from: a */
    private static final C1702d f2675a = new C1702d();

    private C1702d() {
    }

    /* renamed from: a */
    public static C3918c m4260a() {
        return f2675a;
    }

    /* renamed from: a */
    public Object mo13529a(Object obj, Object obj2) {
        return C1703e.m4262a((C1703e.C1704a) obj, (C1703e.C1705b) obj2);
    }
}
