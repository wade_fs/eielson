package com.google.android.gms.base;

public final class R$styleable {
    public static final int[] LoadingImageView = {2130968753, 2130969017, 2130969018};
    public static final int LoadingImageView_circleCrop = 0;
    public static final int LoadingImageView_imageAspectRatio = 1;
    public static final int LoadingImageView_imageAspectRatioAdjust = 2;
    public static final int[] SignInButton = {2130968704, 2130968786, 2130969337};
    public static final int SignInButton_buttonSize = 0;
    public static final int SignInButton_colorScheme = 1;
    public static final int SignInButton_scopeUris = 2;

    private R$styleable() {
    }
}
