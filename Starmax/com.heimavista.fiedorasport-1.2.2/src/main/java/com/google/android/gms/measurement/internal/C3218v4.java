package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.v4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3218v4 {

    /* renamed from: a */
    private final String f5694a;

    /* renamed from: b */
    private boolean f5695b;

    /* renamed from: c */
    private String f5696c;

    /* renamed from: d */
    private final /* synthetic */ C3185s4 f5697d;

    public C3218v4(C3185s4 s4Var, String str, String str2) {
        this.f5697d = s4Var;
        C2258v.m5639b(str);
        this.f5694a = str;
    }

    @WorkerThread
    /* renamed from: a */
    public final String mo19346a() {
        if (!this.f5695b) {
            this.f5695b = true;
            this.f5696c = this.f5697d.mo19315t().getString(this.f5694a, null);
        }
        return this.f5696c;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19347a(String str) {
        if (this.f5697d.mo19013h().mo19146a(C3135o.f5417R0) || !C3267z9.m9428c(str, this.f5696c)) {
            SharedPreferences.Editor edit = this.f5697d.mo19315t().edit();
            edit.putString(this.f5694a, str);
            edit.apply();
            this.f5696c = str;
        }
    }
}
