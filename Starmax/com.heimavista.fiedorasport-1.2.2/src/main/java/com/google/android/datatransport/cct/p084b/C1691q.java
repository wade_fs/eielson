package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.q */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1691q implements C3536d<C1672g> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        C1672g gVar = (C1672g) obj;
        C3537e eVar = (C3537e) obj2;
        eVar.mo22172a("eventTimeMs", gVar.mo13477a());
        eVar.mo22172a("eventUptimeMs", gVar.mo13478b());
        eVar.mo22172a("timezoneOffsetSeconds", gVar.mo13479c());
        if (gVar.mo13483f() != null) {
            eVar.mo22173a("sourceExtension", gVar.mo13483f());
        }
        if (gVar.mo13484g() != null) {
            eVar.mo22173a("sourceExtensionJsonProto3", gVar.mo13484g());
        }
        if (gVar.mo13480d() != Integer.MIN_VALUE) {
            eVar.mo22171a("eventCode", gVar.mo13480d());
        }
        if (gVar.mo13481e() != null) {
            eVar.mo22173a("networkConnectionInfo", gVar.mo13481e());
        }
    }
}
