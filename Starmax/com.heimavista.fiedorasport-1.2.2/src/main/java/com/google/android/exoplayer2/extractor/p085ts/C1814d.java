package com.google.android.exoplayer2.extractor.p085ts;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.ts.d */
/* compiled from: lambda */
public final /* synthetic */ class C1814d implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1814d f2824a = new C1814d();

    private /* synthetic */ C1814d() {
    }

    public final Extractor[] createExtractors() {
        return TsExtractor.m4391a();
    }
}
