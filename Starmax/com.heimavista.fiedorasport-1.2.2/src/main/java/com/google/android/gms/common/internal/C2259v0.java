package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4010b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.common.internal.v0 */
public abstract class C2259v0 extends C4010b implements C2257u0 {
    public C2259v0() {
        super("com.google.android.gms.common.internal.ICertData");
    }

    /* renamed from: a */
    public static C2257u0 m5644a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ICertData");
        if (queryLocalInterface instanceof C2257u0) {
            return (C2257u0) queryLocalInterface;
        }
        return new C2261w0(iBinder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17015a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i == 1) {
            C3988b e = mo17042e();
            parcel2.writeNoException();
            C4011c.m12012a(parcel2, e);
        } else if (i != 2) {
            return false;
        } else {
            int f = mo17043f();
            parcel2.writeNoException();
            parcel2.writeInt(f);
        }
        return true;
    }
}
