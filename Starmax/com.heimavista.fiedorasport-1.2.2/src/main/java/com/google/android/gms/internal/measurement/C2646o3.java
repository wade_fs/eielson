package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.o3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2646o3 implements C2594l3 {
    private C2646o3() {
    }

    /* renamed from: a */
    public final byte[] mo17587a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* synthetic */ C2646o3(C2482e3 e3Var) {
        this();
    }
}
