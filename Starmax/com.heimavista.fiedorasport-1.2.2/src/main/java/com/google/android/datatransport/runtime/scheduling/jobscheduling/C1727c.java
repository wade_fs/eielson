package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import java.util.Map;
import p119e.p144d.p145a.p146a.C3879d;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C1727c extends C1733g {

    /* renamed from: a */
    private final C3971a f2721a;

    /* renamed from: b */
    private final Map<C3879d, C1733g.C1735b> f2722b;

    C1727c(C3971a aVar, Map<C3879d, C1733g.C1735b> map) {
        if (aVar != null) {
            this.f2721a = aVar;
            if (map != null) {
                this.f2722b = map;
                return;
            }
            throw new NullPointerException("Null values");
        }
        throw new NullPointerException("Null clock");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C3971a mo13564a() {
        return this.f2721a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Map<C3879d, C1733g.C1735b> mo13565b() {
        return this.f2722b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1733g)) {
            return false;
        }
        C1733g gVar = (C1733g) obj;
        if (!this.f2721a.equals(super.mo13564a()) || !this.f2722b.equals(super.mo13565b())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.f2721a.hashCode() ^ 1000003) * 1000003) ^ this.f2722b.hashCode();
    }

    public String toString() {
        return "SchedulerConfig{clock=" + this.f2721a + ", values=" + this.f2722b + "}";
    }
}
