package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2790xa;

/* renamed from: com.google.android.gms.measurement.internal.d1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3005d1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5038a = new C3005d1();

    private C3005d1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2790xa.m7778b());
    }
}
