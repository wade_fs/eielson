package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;

public final class StringToIntConverter extends AbstractSafeParcelable implements FastJsonResponse.C2297a<String, Integer> {
    public static final Parcelable.Creator<StringToIntConverter> CREATOR = new C2295b();

    /* renamed from: P */
    private final int f3791P;

    /* renamed from: Q */
    private final HashMap<String, Integer> f3792Q;

    /* renamed from: R */
    private final SparseArray<String> f3793R;

    StringToIntConverter(int i, ArrayList<zaa> arrayList) {
        this.f3791P = i;
        this.f3792Q = new HashMap<>();
        this.f3793R = new SparseArray<>();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaa zaa2 = arrayList.get(i2);
            i2++;
            zaa zaa3 = zaa2;
            mo17074a(zaa3.f3795Q, zaa3.f3796R);
        }
    }

    /* renamed from: a */
    public final StringToIntConverter mo17074a(String str, int i) {
        this.f3792Q.put(str, Integer.valueOf(i));
        this.f3793R.put(i, str);
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3791P);
        ArrayList arrayList = new ArrayList();
        for (String str : this.f3792Q.keySet()) {
            arrayList.add(new zaa(str, this.f3792Q.get(str).intValue()));
        }
        C2250b.m5614c(parcel, 2, arrayList, false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17075a(Object obj) {
        String str = this.f3793R.get(((Integer) obj).intValue());
        return (str != null || !this.f3792Q.containsKey("gms_unknown")) ? str : "gms_unknown";
    }

    public static final class zaa extends AbstractSafeParcelable {
        public static final Parcelable.Creator<zaa> CREATOR = new C2296c();

        /* renamed from: P */
        private final int f3794P;

        /* renamed from: Q */
        final String f3795Q;

        /* renamed from: R */
        final int f3796R;

        zaa(int i, String str, int i2) {
            this.f3794P = i;
            this.f3795Q = str;
            this.f3796R = i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
         arg types: [android.os.Parcel, int, java.lang.String, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
          com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
        public final void writeToParcel(Parcel parcel, int i) {
            int a = C2250b.m5586a(parcel);
            C2250b.m5591a(parcel, 1, this.f3794P);
            C2250b.m5602a(parcel, 2, this.f3795Q, false);
            C2250b.m5591a(parcel, 3, this.f3796R);
            C2250b.m5587a(parcel, a);
        }

        zaa(String str, int i) {
            this.f3794P = 1;
            this.f3795Q = str;
            this.f3796R = i;
        }
    }

    public StringToIntConverter() {
        this.f3791P = 1;
        this.f3792Q = new HashMap<>();
        this.f3793R = new SparseArray<>();
    }
}
