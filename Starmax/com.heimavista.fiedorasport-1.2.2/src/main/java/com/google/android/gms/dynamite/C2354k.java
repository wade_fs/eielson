package com.google.android.gms.dynamite;

import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.dynamite.k */
public interface C2354k extends IInterface {
    /* renamed from: a */
    C3988b mo17153a(C3988b bVar, String str, int i, C3988b bVar2);

    /* renamed from: b */
    C3988b mo17154b(C3988b bVar, String str, int i, C3988b bVar2);
}
