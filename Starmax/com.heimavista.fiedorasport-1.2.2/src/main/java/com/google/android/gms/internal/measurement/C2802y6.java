package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.y6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2802y6 implements C2437b7 {

    /* renamed from: a */
    private final /* synthetic */ C2498f3 f4614a;

    C2802y6(C2498f3 f3Var) {
        this.f4614a = f3Var;
    }

    /* renamed from: a */
    public final int mo17335a() {
        return this.f4614a.mo17469a();
    }

    /* renamed from: a */
    public final byte mo17334a(int i) {
        return this.f4614a.mo17468a(i);
    }
}
