package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.gms.common.internal.C2258v;

public class SupportErrorDialogFragment extends DialogFragment {

    /* renamed from: P */
    private Dialog f3170P = null;

    /* renamed from: Q */
    private DialogInterface.OnCancelListener f3171Q = null;

    /* renamed from: a */
    public static SupportErrorDialogFragment m4633a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        SupportErrorDialogFragment supportErrorDialogFragment = new SupportErrorDialogFragment();
        C2258v.m5630a(dialog, "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        supportErrorDialogFragment.f3170P = dialog2;
        if (onCancelListener != null) {
            supportErrorDialogFragment.f3171Q = onCancelListener;
        }
        return supportErrorDialogFragment;
    }

    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f3171Q;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f3170P == null) {
            setShowsDialog(false);
        }
        return this.f3170P;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
