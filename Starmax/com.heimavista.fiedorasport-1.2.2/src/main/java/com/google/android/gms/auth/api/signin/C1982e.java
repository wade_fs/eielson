package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

/* renamed from: com.google.android.gms.auth.api.signin.e */
final /* synthetic */ class C1982e implements Comparator {

    /* renamed from: P */
    static final Comparator f3124P = new C1982e();

    private C1982e() {
    }

    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).mo16507c().compareTo(((Scope) obj2).mo16507c());
    }
}
