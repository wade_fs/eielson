package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.mp4.b */
/* compiled from: lambda */
public final /* synthetic */ class C1805b implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1805b f2819a = new C1805b();

    private /* synthetic */ C1805b() {
    }

    public final Extractor[] createExtractors() {
        return Mp4Extractor.m4386a();
    }
}
