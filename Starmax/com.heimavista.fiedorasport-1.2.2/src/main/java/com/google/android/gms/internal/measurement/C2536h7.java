package com.google.android.gms.internal.measurement;

import java.util.ListIterator;

/* renamed from: com.google.android.gms.internal.measurement.h7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2536h7 implements ListIterator<String> {

    /* renamed from: P */
    private ListIterator<String> f4214P = this.f4216R.f4074P.listIterator(this.f4215Q);

    /* renamed from: Q */
    private final /* synthetic */ int f4215Q;

    /* renamed from: R */
    private final /* synthetic */ C2486e7 f4216R;

    C2536h7(C2486e7 e7Var, int i) {
        this.f4216R = e7Var;
        this.f4215Q = i;
    }

    public final /* synthetic */ void add(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    public final boolean hasNext() {
        return this.f4214P.hasNext();
    }

    public final boolean hasPrevious() {
        return this.f4214P.hasPrevious();
    }

    public final /* synthetic */ Object next() {
        return this.f4214P.next();
    }

    public final int nextIndex() {
        return this.f4214P.nextIndex();
    }

    public final /* synthetic */ Object previous() {
        return this.f4214P.previous();
    }

    public final int previousIndex() {
        return this.f4214P.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }
}
