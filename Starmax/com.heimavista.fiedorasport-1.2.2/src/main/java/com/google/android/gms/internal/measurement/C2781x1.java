package com.google.android.gms.internal.measurement;

import android.util.Log;

/* renamed from: com.google.android.gms.internal.measurement.x1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2781x1 extends C2765w1<Boolean> {
    C2781x1(C2448c2 c2Var, String str, Boolean bool) {
        super(c2Var, str, bool, null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ Object mo17265a(Object obj) {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (C2463d1.f4036c.matcher(str).matches()) {
                return true;
            }
            if (C2463d1.f4037d.matcher(str).matches()) {
                return false;
            }
        }
        String a = super.mo18127a();
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 28 + String.valueOf(valueOf).length());
        sb.append("Invalid boolean value for ");
        sb.append(a);
        sb.append(": ");
        sb.append(valueOf);
        Log.e("PhenotypeFlag", sb.toString());
        return null;
    }
}
