package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.r0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2686r0 extends C2595l4<C2686r0, C2687a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2686r0 zzf;
    private static volatile C2501f6<C2686r0> zzg;
    private int zzc;
    private int zzd;
    private long zze;

    /* renamed from: com.google.android.gms.internal.measurement.r0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2687a extends C2595l4.C2596a<C2686r0, C2687a> implements C2769w5 {
        private C2687a() {
            super(C2686r0.zzf);
        }

        /* renamed from: a */
        public final C2687a mo17841a(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2686r0) super.f4288Q).m7105b(i);
            return this;
        }

        /* synthetic */ C2687a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2687a mo17842a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2686r0) super.f4288Q).m7102a(j);
            return this;
        }
    }

    static {
        C2686r0 r0Var = new C2686r0();
        zzf = r0Var;
        C2595l4.m6645a(C2686r0.class, super);
    }

    private C2686r0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7102a(long j) {
        this.zzc |= 2;
        this.zze = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7105b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    /* renamed from: s */
    public static C2687a m7106s() {
        return (C2687a) zzf.mo17668i();
    }

    /* renamed from: n */
    public final boolean mo17837n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo17838o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final boolean mo17839p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final long mo17840q() {
        return this.zze;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2686r0();
            case 2:
                return new C2687a(null);
            case 3:
                return C2595l4.m6643a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                C2501f6<C2686r0> f6Var = zzg;
                if (f6Var == null) {
                    synchronized (C2686r0.class) {
                        f6Var = zzg;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzf);
                            zzg = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
