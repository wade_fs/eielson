package com.google.android.gms.internal.measurement;

import java.nio.charset.Charset;

/* renamed from: com.google.android.gms.internal.measurement.p3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
class C2660p3 extends C2615m3 {

    /* renamed from: S */
    protected final byte[] f4417S;

    C2660p3(byte[] bArr) {
        if (bArr != null) {
            this.f4417S = bArr;
            return;
        }
        throw new NullPointerException();
    }

    /* renamed from: a */
    public byte mo17468a(int i) {
        return this.f4417S[i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public byte mo17474b(int i) {
        return this.f4417S[i];
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C2498f3) || mo17469a() != ((C2498f3) obj).mo17469a()) {
            return false;
        }
        if (mo17469a() == 0) {
            return true;
        }
        if (!(obj instanceof C2660p3)) {
            return obj.equals(this);
        }
        C2660p3 p3Var = (C2660p3) obj;
        int g = mo17478g();
        int g2 = p3Var.mo17478g();
        if (g == 0 || g2 == 0 || g == g2) {
            return mo17808a(p3Var, 0, mo17469a());
        }
        return false;
    }

    /* renamed from: f */
    public final boolean mo17477f() {
        int t = mo17571t();
        return C2604l7.m6696a(this.f4417S, t, mo17469a() + t);
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public int mo17571t() {
        return 0;
    }

    /* renamed from: a */
    public int mo17469a() {
        return this.f4417S.length;
    }

    /* renamed from: a */
    public final C2498f3 mo17471a(int i, int i2) {
        int b = C2498f3.m6305b(0, i2, mo17469a());
        if (b == 0) {
            return C2498f3.f4094Q;
        }
        return new C2547i3(this.f4417S, mo17571t(), b);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17473a(C2449c3 c3Var) {
        c3Var.mo17368a(this.f4417S, mo17571t(), mo17469a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final String mo17472a(Charset charset) {
        return new String(this.f4417S, mo17571t(), mo17469a(), charset);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo17808a(C2498f3 f3Var, int i, int i2) {
        if (i2 > f3Var.mo17469a()) {
            int a = mo17469a();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(a);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > f3Var.mo17469a()) {
            int a2 = f3Var.mo17469a();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(a2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(f3Var instanceof C2660p3)) {
            return f3Var.mo17471a(0, i2).equals(mo17471a(0, i2));
        } else {
            C2660p3 p3Var = (C2660p3) f3Var;
            byte[] bArr = this.f4417S;
            byte[] bArr2 = p3Var.f4417S;
            int t = mo17571t() + i2;
            int t2 = mo17571t();
            int t3 = p3Var.mo17571t();
            while (t2 < t) {
                if (bArr[t2] != bArr2[t3]) {
                    return false;
                }
                t2++;
                t3++;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final int mo17470a(int i, int i2, int i3) {
        return C2647o4.m6958a(i, this.f4417S, mo17571t(), i3);
    }
}
