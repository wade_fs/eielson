package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.y7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3254y7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5778P;

    /* renamed from: Q */
    private final /* synthetic */ zzm f5779Q;

    /* renamed from: R */
    private final /* synthetic */ C3232w7 f5780R;

    C3254y7(C3232w7 w7Var, AtomicReference atomicReference, zzm zzm) {
        this.f5780R = w7Var;
        this.f5778P = atomicReference;
        this.f5779Q = zzm;
    }

    public final void run() {
        synchronized (this.f5778P) {
            try {
                C3228w3 d = this.f5780R.f5724d;
                if (d == null) {
                    this.f5780R.mo19015l().mo19001t().mo19042a("Failed to get app instance id");
                    this.f5778P.notify();
                    return;
                }
                this.f5778P.set(d.mo19198c(this.f5779Q));
                String str = (String) this.f5778P.get();
                if (str != null) {
                    this.f5780R.mo18884m().mo19264a(str);
                    this.f5780R.mo19012g().f5614l.mo19347a(str);
                }
                this.f5780R.m9314J();
                this.f5778P.notify();
            } catch (RemoteException e) {
                try {
                    this.f5780R.mo19015l().mo19001t().mo19043a("Failed to get app instance id", e);
                    this.f5778P.notify();
                } catch (Throwable th) {
                    this.f5778P.notify();
                    throw th;
                }
            }
        }
    }
}
