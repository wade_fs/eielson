package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

/* renamed from: com.google.android.gms.measurement.internal.e8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3024e8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3188s7 f5103P;

    /* renamed from: Q */
    private final /* synthetic */ C3232w7 f5104Q;

    C3024e8(C3232w7 w7Var, C3188s7 s7Var) {
        this.f5104Q = w7Var;
        this.f5103P = s7Var;
    }

    public final void run() {
        C3228w3 d = this.f5104Q.f5724d;
        if (d == null) {
            this.f5104Q.mo19015l().mo19001t().mo19042a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.f5103P == null) {
                d.mo19188a(0, (String) null, (String) null, this.f5104Q.mo19016n().getPackageName());
            } else {
                d.mo19188a(this.f5103P.f5640c, this.f5103P.f5638a, this.f5103P.f5639b, this.f5104Q.mo19016n().getPackageName());
            }
            this.f5104Q.m9314J();
        } catch (RemoteException e) {
            this.f5104Q.mo19015l().mo19001t().mo19043a("Failed to send current screen to the service", e);
        }
    }
}
