package com.google.android.gms.internal.base;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

/* renamed from: com.google.android.gms.internal.base.d */
public interface C2378d {
    /* renamed from: a */
    ExecutorService mo17189a(int i, ThreadFactory threadFactory, int i2);
}
