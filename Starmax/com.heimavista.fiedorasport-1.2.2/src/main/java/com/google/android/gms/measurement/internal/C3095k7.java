package com.google.android.gms.measurement.internal;

import android.net.Uri;

/* renamed from: com.google.android.gms.measurement.internal.k7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3095k7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5298P;

    /* renamed from: Q */
    private final /* synthetic */ Uri f5299Q;

    /* renamed from: R */
    private final /* synthetic */ String f5300R;

    /* renamed from: S */
    private final /* synthetic */ String f5301S;

    /* renamed from: T */
    private final /* synthetic */ C3059h7 f5302T;

    C3095k7(C3059h7 h7Var, boolean z, Uri uri, String str, String str2) {
        this.f5302T = h7Var;
        this.f5298P = z;
        this.f5299Q = uri;
        this.f5300R = str;
        this.f5301S = str2;
    }

    public final void run() {
        this.f5302T.m8719a(this.f5298P, this.f5299Q, this.f5300R, this.f5301S);
    }
}
