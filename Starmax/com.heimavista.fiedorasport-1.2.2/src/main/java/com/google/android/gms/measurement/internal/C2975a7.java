package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.a7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2975a7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f4951P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f4952Q;

    C2975a7(C3154p6 p6Var, AtomicReference atomicReference) {
        this.f4952Q = p6Var;
        this.f4951P = atomicReference;
    }

    public final void run() {
        synchronized (this.f4951P) {
            try {
                this.f4951P.set(Integer.valueOf(this.f4952Q.mo19013h().mo19147b(this.f4952Q.mo18885p().mo18800B(), C3135o.f5402K)));
                this.f4951P.notify();
            } catch (Throwable th) {
                this.f4951P.notify();
                throw th;
            }
        }
    }
}
