package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.c3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
abstract class C2995c3 extends C3008d4 {

    /* renamed from: b */
    private boolean f5002b;

    C2995c3(C3081j5 j5Var) {
        super(j5Var);
        this.f5134a.mo19084a(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public abstract boolean mo18725A();

    /* access modifiers changed from: protected */
    /* renamed from: v */
    public void mo18808v() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: w */
    public final boolean mo18815w() {
        return this.f5002b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: x */
    public final void mo18816x() {
        if (!mo18815w()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    /* renamed from: y */
    public final void mo18817y() {
        if (this.f5002b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!mo18725A()) {
            this.f5134a.mo19094h();
            this.f5002b = true;
        }
    }

    /* renamed from: z */
    public final void mo18818z() {
        if (!this.f5002b) {
            mo18808v();
            this.f5134a.mo19094h();
            this.f5002b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
