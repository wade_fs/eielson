package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.common.server.response.c */
public final class C2300c implements Parcelable.Creator<zak> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        ArrayList arrayList = null;
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                arrayList = C2248a.m5562c(parcel, a, zal.CREATOR);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                str = C2248a.m5573n(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zak(i, arrayList, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zak[i];
    }
}
