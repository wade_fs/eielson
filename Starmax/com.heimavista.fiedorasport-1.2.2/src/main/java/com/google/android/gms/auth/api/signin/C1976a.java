package com.google.android.gms.auth.api.signin;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.signin.internal.C1995h;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2191b;
import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4069k;

/* renamed from: com.google.android.gms.auth.api.signin.a */
public final class C1976a {
    /* renamed from: a */
    public static C1978c m4547a(@NonNull Activity activity, @NonNull GoogleSignInOptions googleSignInOptions) {
        C2258v.m5629a(googleSignInOptions);
        return new C1978c(activity, googleSignInOptions);
    }

    /* renamed from: a */
    public static C4065h<GoogleSignInAccount> m4548a(@Nullable Intent intent) {
        C1981d a = C1995h.m4592a(intent);
        if (a == null) {
            return C4069k.m12139a((Exception) C2191b.m5353a(Status.f3179V));
        }
        if (!a.mo16419b().mo16518v() || a.mo16418a() == null) {
            return C4069k.m12139a((Exception) C2191b.m5353a(a.mo16419b()));
        }
        return C4069k.m12140a(a.mo16418a());
    }
}
