package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2778x0;

/* renamed from: com.google.android.gms.internal.measurement.c1 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2447c1 implements C2661p4 {

    /* renamed from: a */
    static final C2661p4 f4009a = new C2447c1();

    private C2447c1() {
    }

    /* renamed from: a */
    public final boolean mo17363a(int i) {
        return C2778x0.C2780b.m7762a(i) != null;
    }
}
