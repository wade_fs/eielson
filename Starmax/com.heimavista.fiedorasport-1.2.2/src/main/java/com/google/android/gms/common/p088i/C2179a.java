package com.google.android.gms.common.p088i;

import java.util.concurrent.ScheduledExecutorService;

/* renamed from: com.google.android.gms.common.i.a */
public class C2179a {

    /* renamed from: a */
    private static C2180a f3576a;

    /* renamed from: com.google.android.gms.common.i.a$a */
    public interface C2180a {
        /* renamed from: a */
        ScheduledExecutorService mo16859a();
    }

    /* renamed from: a */
    public static synchronized C2180a m5317a() {
        C2180a aVar;
        synchronized (C2179a.class) {
            if (f3576a == null) {
                f3576a = new C2181b();
            }
            aVar = f3576a;
        }
        return aVar;
    }
}
