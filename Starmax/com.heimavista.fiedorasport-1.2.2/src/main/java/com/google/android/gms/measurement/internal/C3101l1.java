package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2607la;

/* renamed from: com.google.android.gms.measurement.internal.l1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3101l1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5319a = new C3101l1();

    private C3101l1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2607la.m6745e());
    }
}
