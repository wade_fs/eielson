package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.C1977b;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.f */
public final class C1993f implements C1977b {
    /* renamed from: a */
    public final C2040g<Status> mo16414a(C2036f fVar) {
        return C1995h.m4596b(fVar, fVar.mo16573e(), false);
    }
}
