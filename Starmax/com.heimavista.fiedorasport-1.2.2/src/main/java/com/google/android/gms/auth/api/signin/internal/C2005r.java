package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.p092authapi.C2357b;
import com.google.android.gms.internal.p092authapi.C2358c;

/* renamed from: com.google.android.gms.auth.api.signin.internal.r */
public abstract class C2005r extends C2357b implements C2004q {
    public C2005r() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo16461a(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 101:
                mo16444a((GoogleSignInAccount) C2358c.m5879a(parcel, GoogleSignInAccount.CREATOR), (Status) C2358c.m5879a(parcel, Status.CREATOR));
                throw null;
            case 102:
                mo16445a((Status) C2358c.m5879a(parcel, Status.CREATOR));
                break;
            case 103:
                mo16446b((Status) C2358c.m5879a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
