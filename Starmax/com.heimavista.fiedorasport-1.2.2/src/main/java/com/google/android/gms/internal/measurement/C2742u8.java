package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.u8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2742u8 implements C2695r8 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4516a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.upload.disable_is_uploader", true);

    /* renamed from: a */
    public final boolean mo17844a() {
        return f4516a.mo18128b().booleanValue();
    }
}
