package com.google.android.gms.common.api;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2020d;
import com.google.android.gms.common.api.internal.C2047a;
import com.google.android.gms.common.api.internal.C2056c;
import com.google.android.gms.common.api.internal.C2063d2;
import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.api.internal.C2074f1;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.api.internal.C2095k;
import com.google.android.gms.common.api.internal.C2103m;
import com.google.android.gms.common.api.internal.C2109n1;
import com.google.android.gms.common.api.internal.C2115p;
import com.google.android.gms.common.api.internal.C2131t;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2258v;
import java.util.Collections;
import java.util.Set;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.android.gms.common.api.e */
public class C2033e<O extends C2016a.C2020d> {

    /* renamed from: a */
    private final Context f3190a;

    /* renamed from: b */
    private final C2016a<O> f3191b;

    /* renamed from: c */
    private final O f3192c;

    /* renamed from: d */
    private final C2063d2<O> f3193d;

    /* renamed from: e */
    private final Looper f3194e;

    /* renamed from: f */
    private final int f3195f;

    /* renamed from: g */
    private final C2036f f3196g;

    /* renamed from: h */
    protected final C2064e f3197h;

    /* renamed from: com.google.android.gms.common.api.e$a */
    public static class C2034a {

        /* renamed from: a */
        public final C2103m f3198a;

        /* renamed from: b */
        public final Looper f3199b;

        static {
            new C2035a().mo16562a();
        }

        private C2034a(C2103m mVar, Account account, Looper looper) {
            this.f3198a = mVar;
            this.f3199b = looper;
        }

        /* renamed from: com.google.android.gms.common.api.e$a$a */
        public static class C2035a {

            /* renamed from: a */
            private C2103m f3200a;

            /* renamed from: b */
            private Looper f3201b;

            /* renamed from: a */
            public C2035a mo16561a(C2103m mVar) {
                C2258v.m5630a(mVar, "StatusExceptionMapper must not be null.");
                this.f3200a = mVar;
                return this;
            }

            /* renamed from: a */
            public C2035a mo16560a(Looper looper) {
                C2258v.m5630a(looper, "Looper must not be null.");
                this.f3201b = looper;
                return this;
            }

            /* renamed from: a */
            public C2034a mo16562a() {
                if (this.f3200a == null) {
                    this.f3200a = new C2047a();
                }
                if (this.f3201b == null) {
                    this.f3201b = Looper.getMainLooper();
                }
                return new C2034a(this.f3200a, this.f3201b);
            }
        }
    }

    protected C2033e(@NonNull Context context, C2016a<O> aVar, Looper looper) {
        C2258v.m5630a(context, "Null context is not permitted.");
        C2258v.m5630a(aVar, "Api must not be null.");
        C2258v.m5630a(looper, "Looper must not be null.");
        this.f3190a = context.getApplicationContext();
        this.f3191b = aVar;
        this.f3192c = null;
        this.f3194e = looper;
        this.f3193d = C2063d2.m4786a(aVar);
        this.f3196g = new C2074f1(this);
        this.f3197h = C2064e.m4790a(this.f3190a);
        this.f3195f = this.f3197h.mo16659b();
        new C2047a();
    }

    /* renamed from: a */
    private final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T m4675a(int i, @NonNull T t) {
        t.mo16599f();
        this.f3197h.mo16657a(this, i, t);
        return t;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public C2211e.C2212a mo16553b() {
        Account account;
        Set<Scope> set;
        GoogleSignInAccount a;
        GoogleSignInAccount a2;
        C2211e.C2212a aVar = new C2211e.C2212a();
        O o = this.f3192c;
        if (!(o instanceof C2016a.C2020d.C2022b) || (a2 = ((C2016a.C2020d.C2022b) o).mo16526a()) == null) {
            O o2 = this.f3192c;
            account = o2 instanceof C2016a.C2020d.C2021a ? ((C2016a.C2020d.C2021a) o2).mo16525j() : null;
        } else {
            account = a2.mo16384c();
        }
        aVar.mo16969a(account);
        O o3 = this.f3192c;
        if (!(o3 instanceof C2016a.C2020d.C2022b) || (a = ((C2016a.C2020d.C2022b) o3).mo16526a()) == null) {
            set = Collections.emptySet();
        } else {
            set = a.mo16380A();
        }
        aVar.mo16971a(set);
        aVar.mo16970a(this.f3190a.getClass().getName());
        aVar.mo16973b(this.f3190a.getPackageName());
        return aVar;
    }

    /* renamed from: c */
    public final C2016a<O> mo16554c() {
        return this.f3191b;
    }

    /* renamed from: d */
    public O mo16555d() {
        return this.f3192c;
    }

    /* renamed from: e */
    public Context mo16556e() {
        return this.f3190a;
    }

    /* renamed from: f */
    public final int mo16557f() {
        return this.f3195f;
    }

    /* renamed from: g */
    public Looper mo16558g() {
        return this.f3194e;
    }

    /* renamed from: h */
    public final C2063d2<O> mo16559h() {
        return this.f3193d;
    }

    /* renamed from: a */
    public <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16549a(@NonNull T t) {
        m4675a(1, t);
        return t;
    }

    @Deprecated
    /* renamed from: a */
    public <A extends C2016a.C2018b, T extends C2095k<A, ?>, U extends C2115p<A, ?>> C4065h<Void> mo16552a(@NonNull T t, U u) {
        C2258v.m5629a((Object) t);
        C2258v.m5629a((Object) u);
        C2258v.m5630a(t.mo16736b(), "Listener has already been released.");
        C2258v.m5630a(u.mo16762a(), "Listener has already been released.");
        C2258v.m5637a(t.mo16736b().equals(u.mo16762a()), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.f3197h.mo16652a(this, t, u);
    }

    /* renamed from: a */
    public C4065h<Boolean> mo16551a(@NonNull C2084i.C2085a<?> aVar) {
        C2258v.m5630a(aVar, "Listener key cannot be null.");
        return this.f3197h.mo16651a(this, aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.t.a(android.app.Activity, com.google.android.gms.common.api.internal.e, com.google.android.gms.common.api.internal.d2<?>):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.internal.e, com.google.android.gms.common.api.internal.d2<O>]
     candidates:
      com.google.android.gms.common.api.internal.g2.a(int, int, android.content.Intent):void
      com.google.android.gms.common.api.internal.LifecycleCallback.a(int, int, android.content.Intent):void
      com.google.android.gms.common.api.internal.t.a(android.app.Activity, com.google.android.gms.common.api.internal.e, com.google.android.gms.common.api.internal.d2<?>):void */
    @MainThread
    public C2033e(@NonNull Activity activity, C2016a aVar, @Nullable C2016a.C2020d dVar, C2034a aVar2) {
        C2258v.m5630a(activity, "Null activity is not permitted.");
        C2258v.m5630a(aVar, "Api must not be null.");
        C2258v.m5630a(aVar2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f3190a = activity.getApplicationContext();
        this.f3191b = aVar;
        this.f3192c = dVar;
        this.f3194e = aVar2.f3199b;
        this.f3193d = C2063d2.m4787a(this.f3191b, this.f3192c);
        this.f3196g = new C2074f1(this);
        this.f3197h = C2064e.m4790a(this.f3190a);
        this.f3195f = this.f3197h.mo16659b();
        C2103m mVar = aVar2.f3198a;
        if (!(activity instanceof GoogleApiActivity)) {
            C2131t.m5120a(activity, this.f3197h, (C2063d2<?>) this.f3193d);
        }
        this.f3197h.mo16656a(this);
    }

    @WorkerThread
    /* renamed from: a */
    public C2016a.C2027f mo16547a(Looper looper, C2064e.C2065a<O> aVar) {
        return this.f3191b.mo16524d().mo16371a(this.f3190a, looper, mo16553b().mo16972a(), this.f3192c, aVar, aVar);
    }

    /* renamed from: a */
    public C2036f mo16548a() {
        return this.f3196g;
    }

    /* renamed from: a */
    public C2109n1 mo16550a(Context context, Handler handler) {
        return new C2109n1(context, handler, mo16553b().mo16972a());
    }

    public C2033e(@NonNull Context context, C2016a aVar, @Nullable C2016a.C2020d dVar, C2034a aVar2) {
        C2258v.m5630a(context, "Null context is not permitted.");
        C2258v.m5630a(aVar, "Api must not be null.");
        C2258v.m5630a(aVar2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f3190a = context.getApplicationContext();
        this.f3191b = aVar;
        this.f3192c = dVar;
        this.f3194e = aVar2.f3199b;
        this.f3193d = C2063d2.m4787a(this.f3191b, this.f3192c);
        this.f3196g = new C2074f1(this);
        this.f3197h = C2064e.m4790a(this.f3190a);
        this.f3195f = this.f3197h.mo16659b();
        C2103m mVar = aVar2.f3198a;
        this.f3197h.mo16656a(this);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2033e(@androidx.annotation.NonNull android.app.Activity r2, com.google.android.gms.common.api.C2016a r3, @androidx.annotation.Nullable com.google.android.gms.common.api.C2016a.C2020d r4, com.google.android.gms.common.api.internal.C2103m r5) {
        /*
            r1 = this;
            com.google.android.gms.common.api.e$a$a r0 = new com.google.android.gms.common.api.e$a$a
            r0.<init>()
            r0.mo16561a(r5)
            android.os.Looper r5 = r2.getMainLooper()
            r0.mo16560a(r5)
            com.google.android.gms.common.api.e$a r5 = r0.mo16562a()
            r1.<init>(r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.C2033e.<init>(android.app.Activity, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.internal.m):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2033e(@androidx.annotation.NonNull android.content.Context r2, com.google.android.gms.common.api.C2016a r3, @androidx.annotation.Nullable com.google.android.gms.common.api.C2016a.C2020d r4, com.google.android.gms.common.api.internal.C2103m r5) {
        /*
            r1 = this;
            com.google.android.gms.common.api.e$a$a r0 = new com.google.android.gms.common.api.e$a$a
            r0.<init>()
            r0.mo16561a(r5)
            com.google.android.gms.common.api.e$a r5 = r0.mo16562a()
            r1.<init>(r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.C2033e.<init>(android.content.Context, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.internal.m):void");
    }
}
