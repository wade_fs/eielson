package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.C2158l;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.google.android.gms.common.api.internal.v1 */
public final class C2141v1 {

    /* renamed from: d */
    public static final Status f3487d = new Status(8, "The connection to Google Play services was lost");

    /* renamed from: e */
    private static final BasePendingResult<?>[] f3488e = new BasePendingResult[0];

    /* renamed from: a */
    final Set<BasePendingResult<?>> f3489a = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));

    /* renamed from: b */
    private final C2152y1 f3490b = new C2145w1(this);

    /* renamed from: c */
    private final Map<C2016a.C2019c<?>, C2016a.C2027f> f3491c;

    public C2141v1(Map<C2016a.C2019c<?>, C2016a.C2027f> map) {
        this.f3491c = map;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16790a(BasePendingResult<? extends C2157k> basePendingResult) {
        this.f3489a.add(basePendingResult);
        basePendingResult.mo16592a(this.f3490b);
    }

    /* renamed from: b */
    public final void mo16791b() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.f3489a.toArray(f3488e)) {
            basePendingResult.mo16594b(f3487d);
        }
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.google.android.gms.common.api.l, com.google.android.gms.common.api.internal.w1, com.google.android.gms.common.api.internal.y1, com.google.android.gms.common.api.r] */
    /* renamed from: a */
    public final void mo16789a() {
        BasePendingResult[] basePendingResultArr = (BasePendingResult[]) this.f3489a.toArray(f3488e);
        for (BasePendingResult basePendingResult : basePendingResultArr) {
            ? r5 = 0;
            basePendingResult.mo16592a((C2152y1) r5);
            if (basePendingResult.mo16597d() != null) {
                basePendingResult.mo16588a((C2158l) r5);
                IBinder m = this.f3491c.get(((C2056c) basePendingResult).mo16642h()).mo16539m();
                if (basePendingResult.mo16596c()) {
                    basePendingResult.mo16592a(new C2149x1(basePendingResult, r5, m, r5));
                } else if (m == null || !m.isBinderAlive()) {
                    basePendingResult.mo16592a((C2152y1) r5);
                    basePendingResult.mo16591a();
                    r5.mo16815a(basePendingResult.mo16597d().intValue());
                } else {
                    C2149x1 x1Var = new C2149x1(basePendingResult, r5, m, r5);
                    basePendingResult.mo16592a(x1Var);
                    try {
                        m.linkToDeath(x1Var, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.mo16591a();
                        r5.mo16815a(basePendingResult.mo16597d().intValue());
                    }
                }
                this.f3489a.remove(basePendingResult);
            } else if (basePendingResult.mo16598e()) {
                this.f3489a.remove(basePendingResult);
            }
        }
    }
}
