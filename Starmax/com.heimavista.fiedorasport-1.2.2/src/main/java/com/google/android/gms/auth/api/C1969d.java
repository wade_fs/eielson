package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.internal.auth.C2370f;

/* renamed from: com.google.android.gms.auth.api.d */
final class C1969d extends C2016a.C2017a<C2370f, C1960c> {
    C1969d() {
    }

    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        return new C2370f(context, looper, eVar, (C1960c) obj, bVar, cVar);
    }
}
