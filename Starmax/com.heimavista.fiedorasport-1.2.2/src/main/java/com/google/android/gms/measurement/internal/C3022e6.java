package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.e6 */
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
final class C3022e6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2589kc f5097P;

    /* renamed from: Q */
    private final /* synthetic */ zzan f5098Q;

    /* renamed from: R */
    private final /* synthetic */ String f5099R;

    /* renamed from: S */
    private final /* synthetic */ AppMeasurementDynamiteService f5100S;

    C3022e6(AppMeasurementDynamiteService appMeasurementDynamiteService, C2589kc kcVar, zzan zzan, String str) {
        this.f5100S = appMeasurementDynamiteService;
        this.f5097P = kcVar;
        this.f5098Q = zzan;
        this.f5099R = str;
    }

    public final void run() {
        this.f5100S.f4930a.mo19079F().mo19376a(this.f5097P, this.f5098Q, this.f5099R);
    }
}
