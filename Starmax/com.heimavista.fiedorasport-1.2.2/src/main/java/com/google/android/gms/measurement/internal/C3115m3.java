package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2668pb;

/* renamed from: com.google.android.gms.measurement.internal.m3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3115m3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5342a = new C3115m3();

    private C3115m3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2668pb.m7040b());
    }
}
