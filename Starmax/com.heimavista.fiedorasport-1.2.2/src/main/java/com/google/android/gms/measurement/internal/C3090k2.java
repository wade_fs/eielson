package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2458cc;

/* renamed from: com.google.android.gms.measurement.internal.k2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3090k2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5291a = new C3090k2();

    private C3090k2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2458cc.m6170i());
    }
}
